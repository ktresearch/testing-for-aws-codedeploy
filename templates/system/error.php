<?php
/**
 * @package     Joomla.Site
 * @subpackage  Template.system
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
if (isset($this->error)) {
    if (isset($_GET['showerror'])) {
        var_dump($this->error->getCode());
        var_dump($this->error->getMessage());
        die;
    } else
        header('Location: /404.html');
//    header('Location: /component/joomdle/errpage');
    exit;
}