<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

// Create shortcuts to some parameters.
$params  = $this->item->params;
$images  = json_decode($this->item->images);
$urls    = json_decode($this->item->urls);
$canEdit = $params->get('access-edit');
$user    = JFactory::getUser();
$info    = $params->get('info_block_position', 0);
JHtml::_('behavior.caption');
//PA 2.0
$document = JFactory::getDocument();
$document->addStyleSheet('templates/t3_bs3_blank/css/learning_recommender.css');
$document->addStyleSheet('templates/t3_bs3_tablet_desktop_template/css/hikashop_cart.css');
$document->addCustomTag('<script src="/templates/t3_bs3_tablet_desktop_template/js/learning_recommender_modal.js"></script>');
$document->addCustomTag('<script src="/templates/t3_bs3_tablet_desktop_template/js/learning_recommender_modal_box.js"></script>');
$document->setMetaData( 'robots', 'noindex' );

?>
	<div class="row emptycart-title">
		  <h3><?php echo JText::_('KD_MY_CART'); ?></h3>
	</div>
	<div class="row emptycart-content">
		<div class="col-sm-6 col-md-4 col-centered">
	    		<div class="thumbnail">
	      			<img src="./images/courseicon_hd.png" alt="...">
	      			<div class="caption">
	        			<span><?php echo JText::_('KD_MY_CART_EMPTY'); ?></span>
	      			</div>
                                <div class="empty-action">
                                    <a href="./featured"><?php echo JText::_('KD_MY_CART_SEARCH_COURSE'); ?></a>
                                </div>
	    		</div>
  		</div>
	</div>
<?php
?>
