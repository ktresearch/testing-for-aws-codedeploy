<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

// Create shortcuts to some parameters.
$params  = $this->item->params;
$images  = json_decode($this->item->images);
$urls    = json_decode($this->item->urls);
$canEdit = $params->get('access-edit');
$user    = JFactory::getUser();
$info    = $params->get('info_block_position', 0);
JHtml::_('behavior.caption');
//PA 2.0
$document = JFactory::getDocument();
$document->addStyleSheet('templates/t3_bs3_blank/css/learning_recommender.css');
$document->addStyleSheet('templates/t3_bs3_blank/css/product_hika_mobile.css');
$document->addCustomTag('<script src="/templates/t3_bs3_tablet_desktop_template/js/learning_recommender_modal.js"></script>');
$document->addCustomTag('<script src="/templates/t3_bs3_tablet_desktop_template/js/learning_recommender_modal_box.js"></script>');
$document->setMetaData( 'robots', 'noindex' );

// language /vn or /en
if(strcmp((JFactory::getLanguage()->getTag()),'vi-VN')==0) {
	$langcurrent = 'vn/';
}



$app2= JFactory::getApplication();
$order_id=$app2->getUserState('com_hikashop.order_id');
if(!include_once(rtrim(JPATH_ADMINISTRATOR,DS).DS.'components'.DS.'com_hikashop'.DS.'helpers'.DS.'helper.php')) return true;
$class=hikashop_get('class.order');
$order=$class->get($order_id);
$total_quality=0;
if(!empty($order)){
	if($order->order_payment_method=='paypal'){
		if($order->order_status=='created'){
			?>
			<div class="hikashop_checkout_header navbar-fixed-top">
					<div style='height:50px;width:33.33333333%;float:left;text-align:center;position:relative;' class='<?php echo "completed_step"; ?>'>
						<a><span><?php echo JText::_('KD_ITEMS'); ?></span></a>
						<i class="fa fa-caret-up" aria-hidden="true"></i>
					</div>
					<div style='height:50px;width:33.33333333%;float:left;text-align:center;position:relative;' class='<?php echo "completed_step"; ?>'>
						<a><span><?php echo JText::_('KD_PAYMENT_STEP'); ?></span></a>
						<i class="fa fa-caret-up" aria-hidden="true"></i>
					</div>
					<div style='height:50px;width:33.33333333%;float:left;text-align:center;position:relative;'>
						<a><span><?php echo JText::_('KD_CONFIRMATION'); ?></span></a>
						<i class="fa fa-caret-up" aria-hidden="true"></i>
					</div>
				</div>
			<div class="row" style="text-align:center;margin-left:0px;margin-right:0px;padding:20px 20px 0px;padding-top:50px;">
				<?php echo JText::_('KD_PAYMENT_CANCEL'); ?><br/>
			</div>
			<div class="row" style="text-align:center;margin-left:0px;margin-right:0px;">
				<a href="<?php echo JURI::root().''.$langcurrent.'featured';?>" class="view_invoice" style="width:180px;display:block;margin:auto;background-size:8%;margin-top:10px;padding:5px;"><?php echo JText::_('KD_CONTINUE_SHOPPING'); ?></a>
			</div>
				<div class="row" style="text-align:center;margin-left:0px;margin-right:0px;">
				<a href="<?php echo 'mylearning/list.html';?>" class="view_invoice" style="width:180px;display:block;margin:auto;background-size:8%;margin-top:10px;padding:5px;"><?php echo JText::_('KD_START_LEARNING'); ?></a>
			</div>
			<?php
		}elseif ($order->order_status!='confirmed') {
			?>
				<div class="hikashop_checkout_header navbar-fixed-top">
					<div style='height:50px;width:33.33333333%;float:left;text-align:center;position:relative;' class='<?php echo "completed_step"; ?>'>
						<a><span><?php echo JText::_('KD_ITEMS'); ?></span></a>
						<i class="fa fa-caret-up" aria-hidden="true"></i>
					</div>
					<div style='height:50px;width:33.33333333%;float:left;text-align:center;position:relative;' class='<?php echo "completed_step"; ?>'>
						<a><span><?php echo JText::_('KD_PAYMENT_STEP'); ?></span></a>
						<i class="fa fa-caret-up" aria-hidden="true"></i>
					</div>
					<div style='height:50px;width:33.33333333%;float:left;text-align:center;position:relative;'>
						<a><span><?php echo JText::_('KD_CONFIRMATION'); ?></span></a>
						<i class="fa fa-caret-up" aria-hidden="true"></i>
					</div>
				</div>
			<div class="row" style="text-align:center;margin-left:0px;margin-right:0px;padding:20px 20px 0px;padding-top:50px;">
				<?php echo JText::_('KD_PAYMENT_CANCEL'); ?><br/>
			</div>
			<div class="row" style="text-align:center;margin-left:0px;margin-right:0px;">
				<a href="<?php echo JURI::root().''.$langcurrent.'featured';?>" class="view_invoice" style="width:180px;display:block;margin:auto;background-size:8%;margin-top:10px;padding:5px;"><?php echo JText::_('KD_CONTINUE_SHOPPING'); ?></a>
			</div>
				<div class="row" style="text-align:center;margin-left:0px;margin-right:0px;">
				<a href="<?php echo 'mylearning/list.html';?>" class="view_invoice" style="width:180px;display:block;margin:auto;background-size:8%;margin-top:10px;padding:5px;"><?php echo JText::_('KD_START_LEARNING'); ?></a>
			</div>
			<?php
		}
	}
}
?>
