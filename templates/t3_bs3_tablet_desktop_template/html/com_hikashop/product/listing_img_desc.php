<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.1
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

$height = $this->newSizes->height;
$width = $this->newSizes->width;
$link = hikashop_contentLink('product&task=show&cid='.$this->row->product_id.'&name='.$this->row->alias.$this->itemid.$this->category_pathway,$this->row);
if (strpos($this->row->product_code,'bundle_') !== false || strpos($this->row->product_code,'prog_') !== false)
    $isCommonCourse = false;
else
    $isCommonCourse = true;

if (!empty($this->row->extraData->top)) echo implode("\r\n",$this->row->extraData->top);
?>
<div class="hika_listing_img_desc">
    <div class="courseItem <?php echo $isCommonCourse ? 'isCommonCourse' : ''; ?>" onclick="location.href='<?php echo $link; ?>'">
        <?php
        if (!empty($this->row->file_path)) {
            $img = $this->image->getThumbnail(@$this->row->file_path, array('width' =>"400px" , 'height' => "300px"), null);
            $file_path = str_replace('\\','/',$img->path);
            $image_path = $this->image->uploadFolder_url.$file_path;
        } else
            $image_path = "/courses/theme/parenthexis/pix/nocourseimg.jpg";

        $description_text = JHTML::_('content.prepare', preg_replace('#<hr *id="system-readmore" */>#i', '', $this->row->product_description));
        $title = (strlen($this->row->product_name) > 110) ? substr($this->row->product_name, 0, 110).'...' : $this->row->product_name;
        $des = (strlen(strip_tags($description_text)) > 110) ? substr(strip_tags($description_text), 0, 110).'...' : strip_tags($description_text);
        ?>
        <div class="courseImage" style="background-image: url('<?php echo $image_path; ?>')">
            <?php
            $dpContent = [];
            if ($this->params->get('show_price', '-1') == '-1') {
                $config =& hikashop_config();
                $this->params->set('show_price', $config->get('show_price'));
            }
            if ($this->params->get('show_price')) {
                $this->setLayout('listing_price');
                $money = $this->loadTemplate();
                $dpContent[] = $money;
            }
            if (!empty($this->row->estimated_duration)) {
                $dpContent[] = $this->row->estimated_duration.'h';
            }
            ?>
            <div class="courseDurationPrice"><?php echo implode(' • ', $dpContent); ?></div>
        </div>
        <div class="courseContent">
            <div class="courseTitle"><?php echo $title; ?></div>
            <p class="courseDescription"><?php echo $des; ?></p>
        </div>
    </div>
</div>
<?php if(!empty($this->row->extraData->bottom)) { echo implode("\r\n",$this->row->extraData->bottom); } ?>
