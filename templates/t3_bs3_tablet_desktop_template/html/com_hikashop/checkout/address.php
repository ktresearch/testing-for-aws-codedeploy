<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.1
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
if($this->identified) {
	$config = hikashop_config();
	$address_selector = (int)$config->get('checkout_address_selector', 0);

	$mainId = 'hikashop_checkout_address_billing_only';
	$leftId = 'hikashop_checkout_billing_address';
	$mainClass = 'hikashop_checkout_address_billing_only';
	$leftClass = 'hikashop_checkout_billing_address';
	if($this->has_shipping) {
		$mainId = 'hikashop_checkout_address';
		$leftId = 'hikashop_checkout_address_left_part';
		$mainClass = 'hikashop_checkout_address';
		$leftClass = 'hikashop_checkout_address_left_part';
	}
	if(HIKASHOP_RESPONSIVE) {
		$mainClass .= ' '.HK_GRID_ROW;
		$leftClass .= ' '.HK_GRID_COL_6;
	}
	$user_id = hikashop_loadUser();
	$db = JFactory::getDBO();
	$db->setQuery('SELECT * FROM '.hikashop_table('address').' WHERE address_published=1 AND address_user_id='.$user_id);
	$address_object = $db->loadObjectList();
	$db->setQuery('SELECT options FROM par_community_fields WHERE type="country"');
	$countryList=explode("\n",$db->loadResult());
	if(!empty($address_object)){
			$app=JFactory::getApplication();
			$app->setUserState( HIKASHOP_COMPONENT.'.shipping_address',$address_object[0]->address_id );
			$app->setUserState( HIKASHOP_COMPONENT.'.billing_address',$address_object[0]->address_id );
	}else{
		$db->setQuery('INSERT INTO '.hikashop_table('address').'(address_user_id) VALUES ('.(int)$user_id.')');
		$db->execute();
		$db->setQuery('SELECT * FROM '.hikashop_table('address').' WHERE address_published=1 AND address_user_id='.$user_id);
		$address_object = $db->loadObjectList();
	}
	if($this->step==1){
?>
<div class="row billing-address" style="margin-left:14px;margin-right:14px;padding:20px;text-align:center;font-family:'Open Sans', OpenSans, sans-serif;color:#126DB6;font-weight:400;font-size:18px;">
    <span><?php echo JText::_('HIKASHOP_BILLING_ADDRESS'); ?></span>
</div>
<div class="row" style="margin-left:14px;margin-right:14px;">
	<input class="hikashop_address_custom_text address_fields" id="address_full_name" type="text" placeholder="<?php echo JText::_('KD_FULLNAME'); ?>" value="<?php echo $address_object[0]->full_name; ?>"/>
	<input class="hikashop_address_custom_text address_fields" id="address_contact_number" type="text" placeholder="<?php echo JText::_('KD_CONTACT_NUMBER'); ?>" value="<?php echo $address_object[0]->contact_number; ?>"/>
	<input class="hikashop_address_custom_text address_fields" id="address_email" type="text" placeholder="<?php echo JText::_('KD_EMAIL_ADDRESS'); ?>" value="<?php echo $address_object[0]->email; ?>"/>
	<input class="hikashop_address_custom_text address_fields" id="address_home_address" type="text" placeholder="<?php echo JText::_('KD_HOME_ADDRESS'); ?>"  value="<?php echo $address_object[0]->home_address; ?>"/>
	<select class="hikashop_address_custom_text address_fields" id="address_country" style="color:#555555;height:40px;padding:10px !important;padding-left:20px !important;">
		<option value="" disabled selected><?php echo JText::_('KD_SELECT_COUNTRY'); ?></option>
 	<?php
 		foreach ($countryList as $i => $country) {
 			if($country==$address_object[0]->country){
 				echo "<option value='".$country."'selected>".$country."</option>";
 			}else{
 				echo "<option value='".$country."'>".$country."</option>";
 			}

 		}
 	?>
 	</select>
	<input class="hikashop_address_custom_text address_fields" id="address_postal_code" type="text" placeholder="<?php echo JText::_('KD_POSTAL_CODE'); ?>" value="<?php echo $address_object[0]->postal_code; ?>" />
</div>
<?php $token = hikashop_getFormToken(); ?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.hikashop_address_custom_text').change(function(){
			var full_name=jQuery("#address_full_name").val();
			var contact_number=jQuery("#address_contact_number").val();
			var email_address=jQuery("#address_email").val();
			var home_address=jQuery("#address_home_address").val();
			var country=jQuery("#address_country option:selected").text();
			var postal_code=jQuery("#address_postal_code").val();
			var data={
				full_name:full_name,
				contact_number:contact_number,
				email_address:email_address,
				home_address:home_address,
				country:country,
				postal_code:postal_code
			};
			jQuery.ajax({
				url:'<?php echo hikashop_completeLink("address&task=savecustom&".$token."=1"); ?>',
				type:'post',
				dataType:'json',
				data:data,
				success:function(response){
					if(response=="Invalid contact number"){
						jQuery("#address_contact_number").css({
							"border-color":"red"
						});
					}else{
						jQuery("#address_contact_number").css({
							"border-color":"#cccccc"
						});
					}
					if(response=="Invalid email format"){
						jQuery("#address_email").css({
							"border-color":"red"
						});
					}else{
						jQuery("#address_email").css({
							"border-color":"#cccccc"
						});
					}
				}
			});
		});
	});
</script>
<?php
	}else{
		?>
		<style type="text/css">
			.hikashop_checkout_page form{
				margin-top:0px !important;
			}
		</style>
		<div class="row billing-address" style="margin:0px;padding-left:15px;padding-top:15px;font-weight:300;color:#878787;line-height:15px;font-size: 16px;">
                    <span><?php echo JText::_('HIKASHOP_BILLING_ADDRESS'); ?></span>
		</div>
		<div class="row" style="margin:15px;">
			<div class="col-xs-9">
				<?php echo $address_object[0]->full_name; ?>, <?php echo $address_object[0]->home_address; ?> <?php echo $address_object[0]->country; ?>, <?php echo $address_object[0]->postal_code; ?>
			</div>
			<div class="col-xs-3" style="text-align:right;">
				<a href="index.php?option=com_hikashop&ctrl=checkout&task=step&step=1">
					<img src="/media/com_hikashop/images/edit-icon.png" alt="edit" style="width:20px;"/>
				</a>
			</div>
		</div>
		<?php
	}
?>
<?php
}else{
}
