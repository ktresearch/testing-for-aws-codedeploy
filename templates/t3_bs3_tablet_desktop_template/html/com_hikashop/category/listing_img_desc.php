<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.1
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
if ($this->row->learningprovidercircleid) {
    $link = JRoute::_('index.php?option=com_community&view=groups&task=viewabout&groupid='.$this->row->learningprovidercircleid);
} else
$link = hikashop_contentLink('product&task=listing&cid='.$this->row->category_id.'&name='.$this->row->alias, $this->row);
if ($this->config->get('thumbnail',1)) {
    $image_options = array('default' => true,'forcesize'=>$this->config->get('image_force_size',true),'scale'=>$this->config->get('image_scale_mode','inside'));
    $img = $this->image->getThumbnail(@$this->row->file_path, array('width' => $this->image->main_thumbnail_x, 'height' => $this->image->main_thumbnail_y), $image_options);
    $imgurl = $img->origin_url;
} else $imgurl = '';

$description_text = JHTML::_('content.prepare', preg_replace('#<hr *id="system-readmore" */>#i', '', $this->row->category_description));
$title = (strlen($this->row->category_name) > 65) ? substr($this->row->category_name, 0, 65).'...' : $this->row->category_name;
$des = (strlen(strip_tags($description_text)) > 110) ? substr(strip_tags($description_text), 0, 110).'...' : strip_tags($description_text);
?>
<div class="hika_listing_img_desc">
    <div class="categoryItem" onclick="location.href='<?php echo $link; ?>'">
        <div class="hikashop_category_listing_image" style="background-image: url('<?php echo $imgurl; ?>')">
            <!-- CATEGORY IMG -->
            <!--EO CATEGORY IMG -->
        </div>
        <div class="hikashop_category_listing_middle">
            <div class="hikashop_category_title">
                <!-- CATEGORY NAME -->
                <?php
                echo $title;
                if ($this->params->get('number_of_products',0)) {
                    //echo ' ('.$this->row->number_of_products.')';
                }
                ?>
                <!-- EO CATEGORY NAME -->
            </div>
            <div class="hikashop_category_listing_description">
                <!-- CATEGORY DESC -->
                <?php
                echo $des;
                ?>
                <!-- EO CATEGORY DESC -->
            </div>
        </div>
    </div>
</div>