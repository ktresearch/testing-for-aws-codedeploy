<?php
/**
 *------------------------------------------------------------------------------
 * @package       T3 Framework for Joomla!
 *------------------------------------------------------------------------------
 * @copyright     Copyright (C) 2004-2013 JoomlArt.com. All Rights Reserved.
 * @license       GNU General Public License version 2 or later; see LICENSE.txt
 * @authors       JoomlArt, JoomlaBamboo, (contribute to this project at github
 *                & Google group to become co-author)
 * @Google group: https://groups.google.com/forum/#!forum/t3fw
 * @Link:         http://t3-framework.org
 *------------------------------------------------------------------------------
 */

defined('_JEXEC') or die;
$app = JFactory::getApplication();
$session = JFactory::getSession();
$menu = $app->getMenu();

$ishomepage = false;
if ($menu->getActive() == $menu->getDefault( 'en-GB' )) {
    $ishomepage = true;
} elseif ($menu->getActive() == $menu->getDefault( 'vi-VN' )) {
    $ishomepage = true;
} elseif($menu->getActive() == $menu->getDefault( 'zh-CN' )) {
    $ishomepage = true;
}
$isGuest = (JFactory::getUser()->guest == 1) ? true : false;

$isNewsfeed = (JRequest::getVar('option') == 'com_community' && JRequest::getVar('task') == 'feeds') ? true : false;

JFactory::highlightMenuItem();
?>

<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>"
      class='<jdoc:include type="pageclass" /> tab <?php echo !empty($_SERVER['HTTP_REFERER']) ? $session->get('currentMenuItem') : '';?> <?php echo implode($this->htmlTagClass, ' '); ?>'>

<head>
    <jdoc:include type="head" />
    <?php $this->loadBlock('head') ?>
    <?php $this->addCss('home') ?>
</head>

<body>

<?php if (!$isGuest) : ?>

    <div class="t3-wrapper"> <!-- Need this wrapper for off-canvas menu. Remove if you don't use of-canvas -->

<!--        <div class="tab left-column">

            <?php // $this->loadBlock('left-column') ?>

        </div>-->

        <div class="right-column <?php echo $isNewsfeed ? 'lazyload' : '' ?>" <?php echo $isNewsfeed ? 'data-src="'.JUri::base().'images/social-feeds-bg3.jpg"' : '' ?> <?php if($ishomepage || $isGuest) { echo 'style="background-color: #FFF";'; }?>>

            <div class="header">
                <?php $this->loadBlock('header') ?>
            </div>

            <?php $this->loadBlock('mainbody-home-2') ?>

            <?php $this->loadBlock('mainbody') ?>

            <?php // $this->loadBlock('spotlight-2') ?>

            <?php $this->loadBlock('navhelper') ?>

            <?php $this->loadBlock('footer') ?>

            <?php $this->loadBlock('mainnav') ?>

        </div>
    </div>

<?php else : ?>

    <div class="t3-wrapper"> <!-- Need this wrapper for off-canvas menu. Remove if you don't use of-canvas -->

        <div class="login-register-page">

            <div class="header">
                <?php $this->loadBlock('header') ?>
            </div>

            <?php $this->loadBlock('mainbody-home-2') ?>

            <?php $this->loadBlock('mainbody') ?>

            <?php $this->loadBlock('navhelper') ?>

        </div>

    </div>

<?php endif; ?>

</body>

</html>

<script>
    jQuery(document).ready(function() {
        jQuery('.navbar-header .topmenu-bar .navbar-nav >li').each(function(){
            if(jQuery(this).hasClass('active')){
                jQuery(this).removeClass('active');
            }
        });
    });
</script>