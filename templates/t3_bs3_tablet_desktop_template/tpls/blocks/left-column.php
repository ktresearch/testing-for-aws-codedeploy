<?php
/**
 * Created by PhpStorm.
 * User: kydonvn
 * Date: 3/14/16
 * Time: 11:42 AM
 */

defined('_JEXEC') or die;
?>

<!-- LEFT COLUMN -->
<div class="left-column-content">
	<a class="navbar-brand" href="<?php echo JURI::root( false );?>">
        <img alt="Parenthexis" src="templates/t3_bs3_tablet_desktop_template/images/PARENTHEXIS_LOGO_WHITE.png">
        </a>
    <?php
    $user =& JFactory::getUser();
    if($user->id):
    ?>
    <jdoc:include type="<?php echo $this->getParam('navigation_type', 'megamenu') ?>" name="<?php echo $this->getParam('mm_type', 'tabmenu') ?>" />
    <?php endif; ?>
</div>
<!-- //LEFT COLUMN -->
