<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$user = JFactory::getUser();
require_once(JPATH_ROOT.'/components/com_community/libraries/core.php');
require_once(JPATH_SITE.'/components/com_hikashop/helpers/lpapi.php');
$user2 = CFactory::getUser();
$notifModel = CFactory::getModel('notification');
$myParams             = $user2->getParams();

$session                = JFactory::getSession();
$first_time_login = $session->get('first_time');
$defaultNotification = 0;
$defaulNotif = $notifModel->getDefaultNotification();
if($first_time_login) {
    if($defaulNotif) {
        $defaultNotification = 1;
    }
}
$cart = LpApiController::countcoursecart($user2->id);
$class_defalt = '';
if($first_time_login) {
    $class_defalt = 'not-read';
}
$newNotificationCount = $notifModel->getNotificationCount($user2->id,'0',$myParams->get('lastnotificationlist',''));

$toolbar = CToolbarLibrary::getInstance();
$newEventInviteCount = $toolbar->getTotalNotifications('events');
$newFriendInviteCount = $toolbar->getTotalNotifications('friends');
$newGroupInviteCount = $toolbar->getTotalNotifications('groups');
$newMessageCount = $toolbar->getTotalNotifications('inbox');

$newNotificationCount += $defaultNotification;
//$newNotificationCount += $newEventInviteCount;
//$newNotificationCount += $newFriendInviteCount;
//$newNotificationCount += $newGroupInviteCount;
//$newNotificationCount += $newMessageCount;

$ntf_class = '';
if ($newNotificationCount != 0) {
    $ntf_class = 'have-ntf';
}

$app = JFactory::getApplication();
$menu = $app->getMenu();
$ishomepage = false;
if ($menu->getActive() == $menu->getDefault( 'en-GB' )) {
    $ishomepage = true;
}
elseif ($menu->getActive() == $menu->getDefault( 'vi-VN' )) {
    $ishomepage = true;
} elseif($menu->getActive() == $menu->getDefault( 'zh-CN' )) {
    $ishomepage = true;
}
$isGuest = false;
if (JFactory::getUser()->guest == 1) {
    $isGuest = true;
}
require_once(JPATH_SITE.'/components/com_siteadminbln/models/account.php');
$isSiteAdmin = false;
$model = new SiteadminblnModelAccount();
if (count($model->checkSiteAdminBLn($user->id)) > 0) {
    $isSiteAdmin = true;
} 
?>

    <!-- MAIN NAVIGATION -->
    <nav id="t3-mainnav" class="wrap navbar navbar-default t3-mainnav navbar-fixed-top <?php echo ($ishomepage) ? ' home ':' not-home '; echo ($isGuest) ? ' isGuest ' : ''; ?>">
        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <?php
                $css="width: calc(50% - 30px);";
//                if (!$ishomepage && !$isGuest) {
                    $css =  'width: calc(50% - 80px);';
                    ?>
                    <a class="navbar-back" href="<?php echo JURI::root( false );?>">
                        <img alt="Parenthexis" src="templates/t3_bs3_tablet_desktop_template/images/parenthexis-logo.png">
                    </a>
<!--                    <a class="navbar-back" href="javascript:void(0);" onclick="goBack();">
                        <img alt="Parenthexis" src="templates/t3_bs3_tablet_desktop_template/images/parenthexis-logo.png">
                    </a>-->
                <?php // } ?>
                <?php
                ?>
                <div class="topmenu-bar">
                <?php
                $user =& JFactory::getUser();
                if($user->id):
                ?>
                <jdoc:include type="<?php echo $this->getParam('navigation_type', 'megamenu') ?>" name="<?php echo $this->getParam('mm_type', 'tabmenu') ?>" />
                <?php endif; ?>
                </div>
                <!--<a class="navbar-title" style="<?php // echo $css; ?>"><span id="title-mainnav"></span></a>-->
                
                <button type="button" class="navbar-menu-icon">
                    <div class="bg-second-img icon-bars"></div>
                </button>
                <div class="sidemenu-bar">
                    <img class="sidemenu-cancel" src="templates/t3_bs3_tablet_desktop_template/images/icon-cancel.png" alt="Cancel"/>
                    <jdoc:include type="<?php echo $this->getParam('navigation_type', 'megamenu') ?>" name="<?php echo $this->getParam('mm_type', 'mainmenu') ?>" />
                </div>
                <?php if (!is_null($user->username)) {?>
                    <a class="navbar-notification <?php echo ' '.$ntf_class; ?>" data-placement='bottom' href="javascript:">
                        <?php if ($newNotificationCount != 0) { ?>
                            <div class="bg-second-img icon-ntf ntf-have"></div>
                            <span class="notification-count" style="<?php echo ($newNotificationCount  > 9) ? 'font-size: 8px;': '';?>"><?php echo $newNotificationCount;?></span>
                        <?php } else { ?>
                            <div class="bg-second-img icon-ntf"></div>
                        <?php } ?>
                        <?php //} else { ?>
                        <!-- <img src="templates/t3_bs3_tablet_desktop_template/images/NoNotification.png" alt="<?php echo $user2->getDisplayName() ; ?>"> -->
                    </a>
                <?php } ?>
                
                <?php //if (!is_null(JFactory::getUser()->username)) {
                    if($_REQUEST['view'] != '') {
                ?>
                <div class="navbar-search-all" id="navbar-search-all-id">
                    <div class="search-centre-icon" id="searchbox">
                        <div class="bg-second-img icon-search search-all" id="toggle-search"></div>
                    </div>
                </div>
                    <div class="search-all-type" id="content-search" style="display: none; ">
                    <div class="search-choose-category">
                        <div class="choose-category-title">
                            <span><?php echo JText::_('SEARCH_CHOOSE_CATEGORY'); ?></span>
                            <div class="bg-second-img icon-search"></div>
                        </div>
                        <div class="search-list-category">
                        <!--                            Search icon Learning-->
                            <div class="category-box search-courses" id="searchboxlearning">
                                <span><?php echo JText::_('SEARCH_CATEGORY_LEARNING'); ?></span>
                                <!-- <img class="search-icon-course" src="images/course.png" alt="learning"> -->
                                <div class="search-icon-course bg-img"></div>
                            </div>
                        <!--                            Search icon Circle-->
                            <div class="category-box search-circles" id="searchboxcircle">
                                <span><?php echo JText::_('SEARCH_CATEGORY_CIRCLES'); ?></span>
                                <!-- <img class="search-icon-circle" src="images/circle.png" alt="circle"> -->
                                <div class="search-icon-circle bg-img"></div>
                            </div>
                        <!--                                Search icon Friend-->
                            <div class="category-box search-friends" id="searchboxfriend">
                                <span><?php echo JText::_('SEARCH_CATEGORY_FRIENDS'); ?></span>
                                <!-- <img class="search-icon-friend" src="images/friend.png" alt="friend" > -->
                                <div class="search-icon-friend bg-img"></div>
                            </div>
                        </div>
                    </div>
                    <div class="bg-second-img icon-cancel-search" id="search-cancel"></div>
                </div>
                
                <!--<img class="" id="input-icon-back" src="images/ArrowIcon.png" alt="" onclick="">-->
                <div class="search-all-type-form" style="display: none;">
                    <div class="box-search" style="display: none;" id="searchlearning">
                        <form id="searchCourse" class="frmSearch" method="get"
                              action="<?php echo Juri::base() .'courses/search.html'; ?>">
                            <div class="input-group">
                                <div class="icon-addon addon-lg">
<!--                                    <input type="hidden" name="option" value="com_joomdle">-->
<!--                                    <input type="hidden" name="view" value="searchcourse">-->
                                    <input type="text" name="search" class="search-text" value=""
                                           size="30" placeholder="<?php echo JText::_('SEARCH_PLACEHOLDER_SEARCH'); ?>"/>
<!--                                    <img class="input-icon bg-img" id="input-icon-learning" src="images/MyLearningIcon_White.png" alt="" onclick="">-->
                                    <div class="input-icon bg-img" id="input-icon-learning"></div>
                                    <!--<img class="input-icon-search" id="input-icon-search" src="images/ico-search.png" alt="" onclick="">-->
                                    <!--<input class="input-icon-search" id="input-icon-search" type="image" src="images/ico-search.png" alt="Submit" />-->
                                    <button class="bg-second-img w22px icon-search" type="submit"></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="box-search" style="display: none;" id="searchcircle">
                        <form method="GET" class="frmSearch" 
                              action="<?php echo Juri::base() .'circles/search.html'; ?>">
                            <div class="input-group">
                                <div class="icon-addon addon-lg">
                                    <input placeholder="<?php echo JText::_('SEARCH_PLACEHOLDER_SEARCH'); ?>" type="text" class="search-text" value=""
                                           size="30" id="txtKeyword" name="search">
<!--                                    <img class="input-icon bg-img" id="input-icon-circle" src="images/MyCirclesIcon_White.png" alt="" onclick="">-->
                                    <div class="input-icon bg-img" id="input-icon-circle"></div>
                                    <!--<img class="input-icon-search" id="input-icon-search" src="images/ico-search.png" alt="" onclick="">-->
                                    <!--<input class="input-icon-search" id="input-icon-search" type="image" src="images/ico-search.png" alt="Submit" />-->
                                    <button class="bg-second-img w22px icon-search" type="submit"></button>
                                    <!--                                <label for="search" class="glyphicon glyphicon-search"></label>-->
                                </div>
<!--                                --><?php //echo JHTML::_('form.token') ?>
                            </div>
                            <input type="hidden" name="task" value="search">
                        </form>
                    </div>
                    <div class="box-search" style="display: none;" id="searchfriend">
                        <form method="get" class="frmSearch" action="<?php echo Juri::base() .'friends/search.html'; ?>">
                            <div class="input-group">
                                <div class="icon-addon addon-lg">
                                    <input placeholder="<?php echo JText::_('SEARCH_PLACEHOLDER_SEARCH'); ?>" type="text"
                                           class="search-text" size="30"
                                           name="q" class="joms-input--search"
                                           value="<?php echo (isset($searchQuery)) ? $searchQuery : ''; ?>">
<!--                                    <img class="input-icon bg-img" id="input-icon-friend" src="images/MyFriendsIcon_White.png" alt="" onclick="">-->
                                    <div class="input-icon bg-img" id="input-icon-friend"></div>
                                    <input type="hidden" name="task" value="display">
                                    <!--<img class="input-icon-search" id="input-icon-search" src="images/ico-search.png" alt="" onclick="">-->
                                    <!--<input class="input-icon-search" id="input-icon-search" type="image" src="images/ico-search.png" alt="Submit" />-->
                                    <button class="bg-second-img w22px icon-search" type="submit"></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="bg-second-img icon-cancel-search" id="search-cancel2"></div>
                    
                </div>
                <?php } ?>

<!--                <div class="navbar-faqs --><?php //echo (is_null($user->username)) ? 'hideNotifIcon':''; ?><!--">-->
<!--                <div class="navbar-faqs --><?php //echo (is_null($user->username)) ? 'hideNotifIcon':''; ?><!--">-->
<!--                    <a href="--><?php //echo JURI::root( true ) . 'component/fsf/faq';?><!--">-->
<!--                        <img src="images/FAQ.png" />-->
<!--                    </a>-->
<!--                </div>-->
<!--            </div>-->

            <?php if ($this->getParam('navigation_collapse_enable')) : ?>
                <div class="t3-navbar-collapse navbar-collapse collapse"></div>
            <?php endif ?>
            <!-- Notification -->
            <div id="popover_content_wrapper" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static">
                <div class="modal-dialog modal-sm">
                    <?php
                    //                    if($newNotificationCount > 1) {
                    $document = JFactory::getDocument();
                    $renderer = $document->loadRenderer('module');
                    $module = JModuleHelper::getModule('mod_notifications');
                    echo $renderer->render($module);
                    ?>
                    <?php // } ?>
                    <div class="joms-stream__container joms-stream--discussion notifications default <?php echo $class_defalt; ?>">
                        <?php
                        $defaulNotif['title']=JText::_('COM_COMMUNITY_WELCOME_PARENTHEXIS');

                        /*
                        if(strcmp((JFactory::getLanguage()->getTag()),'vi-VN')==0) {
                            $defaulNotif['id']=11;
                        }
                        if(strcmp((JFactory::getLanguage()->getTag()),'en-GB')==0){
                            $defaulNotif['id']=7;
                        }
                        $href = JURI::base().'component/content/article/?id='. $defaulNotif['id'];
                        */
                        $href = JURI::base();
                        ?>
                        <a href="<?php echo $href; ?>"><?php echo $defaulNotif['title'].', '.$user2->getDisplayName(); ?> !</a>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <div class="lgt-notif"></div>
    <div class="lgt-notif-error"></div>
    <div class="modal" id="search-message">
        <div class="modal-content">
        <p id = "error"></p>
        <div class="closesearch" style="opacity: 1;"><img src="/images/deleteIcon.png"></div>
        </div>
    </div>

<!--<script src="./plugins/system/t3/base-bs3/bootstrap/js/bootstrap.js" type="text/javascript"></script>-->
<script src="<?php echo JURI::root(true) ?>/components/com_community/templates/jomsocial/assets/js/jquery.tagsinput.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo JURI::root(true) ?>/components/com_community/templates/jomsocial/assets/css/jquery.tagsinput.css" />
<script>
    jQuery(document).ready(function() {
//            var title = document.title;
//            document.getElementById("title-mainnav").innerHTML = title;
            var modal = document.getElementById('search-message');
            var er = document.getElementById('error');
            jQuery('.closesearch').click(function(){
                modal.style.display = "none"; 
            });
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            };
            var isSiteAdmin = <?php echo $isSiteAdmin ? 'true' : 'false';?>;
            if (!isSiteAdmin) { 
                jQuery(".siteadmin").parent().remove();
            }
            
            jQuery("#searchlearning .frmSearch").submit(function() {
                var txtSearch = jQuery('#searchlearning input.search-text').val();
                if (txtSearch == '') {
                    lgtCreatePopup('oneButton', {content: "<?php echo JText::_('SEARCH_ERROR_MESSAGE');?>"}, function () {
                        lgtRemovePopup();
                    });
                    return false;
                }
            });
            jQuery("#searchcircle .frmSearch").submit(function() {
                var txtSearch = jQuery('#searchcircle input.search-text').val();
                if (txtSearch == '') {
                    lgtCreatePopup('oneButton', {content: "<?php echo JText::_('SEARCH_ERROR_MESSAGE');?>"}, function () {
                        lgtRemovePopup();
                    });
                    return false;
                }
            });
            jQuery("#searchfriend .frmSearch").submit(function() {
                var txtSearch = jQuery('#searchfriend input.search-text').val();
                if (txtSearch == '') {
                    lgtCreatePopup('oneButton', {content: "<?php echo JText::_('SEARCH_ERROR_MESSAGE');?>"}, function () {
                        lgtRemovePopup();
                    });
                    return false;
                }
            });
            var width = jQuery(window).width();
            jQuery('#toggle-search').click(function() {
                jQuery("#content-search").show();
                jQuery('.navbar-menu-icon').hide();
                jQuery('.navbar-notification').toggleClass('hide');
                jQuery('.navbar-search-all').toggleClass('hide');
                if (width >= 930 && width <= 1005) {
                    jQuery(".topmenu-bar .navbar-nav li").last().hide();
                }
                if (width >= 830 && width < 930) {
                    jQuery(".topmenu-bar .navbar-nav li").last().hide();
                    jQuery(".topmenu-bar .navbar-nav li:nth-last-child(2)").last().hide();
                }
                if (width >= 754 && width < 830) {
                    jQuery(".topmenu-bar .navbar-nav li").last().hide();
                    jQuery(".topmenu-bar .navbar-nav li:nth-last-child(2)").last().hide();
                    jQuery(".topmenu-bar .navbar-nav li:nth-last-child(3)").last().hide();
                }
            });
            jQuery('#search-cancel').click(function() {
                jQuery("#content-search").hide();
                jQuery('.navbar-menu-icon').show();
                jQuery('.navbar-notification').removeClass('hide');
                jQuery('.navbar-search-all').removeClass('hide');
                if (width >= 930 && width <= 1005) {
                    jQuery(".topmenu-bar .navbar-nav li").last().show();
                }
                if (width >= 830 && width < 930) {
                    jQuery(".topmenu-bar .navbar-nav li").last().show();
                    jQuery(".topmenu-bar .navbar-nav li:nth-last-child(2)").last().show();
                }
                if (width >= 754 && width < 830) {
                    jQuery(".topmenu-bar .navbar-nav li").last().show();
                    jQuery(".topmenu-bar .navbar-nav li:nth-last-child(2)").last().show();
                    jQuery(".topmenu-bar .navbar-nav li:nth-last-child(3)").last().show();
                }
            });
            <?php if ($user2->id > 0) {?>
            jQuery('.navbar-header .sidemenu-bar .t3-megamenu ul.navbar-nav').prepend('<li class="navbar-profile"><a style="font-weight:bold;" href="<?php echo JURI::root( true ) .'/component/community/profile'; ?>"><img class="navbar-avatar-sidebar" src="<?php echo $user2->getAvatar();?>"/><span><?php echo htmlspecialchars($user2->getDisplayName(), ENT_QUOTES);?></span></a></li>');
            <?php } ?>

            jQuery('.lgt-notif, .lgt-notif-error').click(function() {
                jQuery(this).removeClass('lgt-visible');
            });

            jQuery('#searchboxlearning').click(function() {
                jQuery("#content-search").hide();
                jQuery("#searchlearning").show();
                jQuery(".search-all-type-form").show();
                jQuery(".navbar-search-all").addClass('hide');
            });

            jQuery('#search-cancel2').click(function() {
                jQuery(".search-all-type-form").hide();
                jQuery("#searchlearning").hide();
                jQuery("#searchcircle").hide();
                jQuery("#searchfriend").hide();
                jQuery('.navbar-menu-icon').show();
                jQuery('.navbar-notification').removeClass('hide');
                jQuery('.navbar-search-all').removeClass('hide');
                if (width >= 930 && width <= 1005) {
                    jQuery(".topmenu-bar .navbar-nav li").last().show();
                }
                if (width >= 830 && width < 930) {
                    jQuery(".topmenu-bar .navbar-nav li").last().show();
                    jQuery(".topmenu-bar .navbar-nav li:nth-last-child(2)").last().show();
                }
                if (width >= 754 && width < 830) {
                    jQuery(".topmenu-bar .navbar-nav li").last().show();
                    jQuery(".topmenu-bar .navbar-nav li:nth-last-child(2)").last().show();
                    jQuery(".topmenu-bar .navbar-nav li:nth-last-child(3)").last().show();
                }
            });


            jQuery('#searchboxcircle').click(function() {
                jQuery("#content-search").hide();
                jQuery("#searchcircle").show();
                jQuery(".search-all-type-form").show();
                jQuery(".navbar-search-all").addClass('hide');
            });

            jQuery('#searchboxfriend').click(function() {
                jQuery("#content-search").hide();
                jQuery("#searchfriend").show();
                jQuery(".search-all-type-form").show();
                jQuery(".navbar-search-all").addClass('hide');
            });

            jQuery('#t3-mainnav .navbar-menu-icon').click(function() {
                jQuery(this).toggleClass('show');
//                jQuery('.t3-wrapper .right-column').toggleClass('show-secondmenu');
//                jQuery('.t3-wrapper .tab.left-column').toggleClass('show-secondmenu');
                jQuery('.navbar-fixed-top').toggleClass('show-secondmenu');
//                jQuery('.course-menu, .changePosition').toggleClass('show-secondmenu');
                jQuery('.joomdle-overview-header').toggleClass('show-secondmenu');
                jQuery('#community-wrap .jomsocial .joms-landing > .btn-group.btn-group-justified, .com_community.view-groups.task-adddiscussion .discussion-buttons, .com_community.view-groups.task-viewdiscussion .joms-page > .joms-comment__reply').toggleClass('show-secondmenu');
                jQuery('.navbar-fixed-top .sidemenu-bar .t3-megamenu > ul > li').removeClass('active');
                jQuery('.navbar-fixed-top .t3-megamenu').toggleClass('visible');
                if (width >= 769) {
                jQuery('.navbar-notification').hide();
                jQuery('.search-all').hide();
                jQuery('.sidemenu-cancel').toggleClass('active');
                }
            });
            jQuery(".sidemenu-bar .sidemenu-cancel").click(function() {
                jQuery('#t3-mainnav .navbar-menu-icon').removeClass('show');
//                    jQuery('.t3-wrapper .right-column').removeClass('show-secondmenu');
//                    jQuery('.t3-wrapper .tab.left-column').removeClass('show-secondmenu');
                    jQuery('.navbar-fixed-top').removeClass('show-secondmenu');
                    jQuery('.joomdle-overview-header').removeClass('show-secondmenu');
                    jQuery('#community-wrap .jomsocial .joms-landing > .btn-group.btn-group-justified, .com_community.view-groups.task-adddiscussion .discussion-buttons, .com_community.view-groups.task-viewdiscussion .joms-page > .joms-comment__reply').removeClass('show-secondmenu');
                    jQuery('.navbar-fixed-top .sidemenu-bar .t3-megamenu > ul > li').removeClass('active');
                    jQuery('.navbar-fixed-top .t3-megamenu').removeClass('visible');
                    jQuery('.navbar-notification').show();
                    jQuery('.search-all').show();
                    jQuery('.sidemenu-cancel').removeClass('active');
            });
            var per = '<?php echo $user2->_permission;?>';
            if(per == 1) jQuery('.navbar-header .t3-megamenu li:nth-child(3)').addClass('hidden');
            jQuery('.navbar-fixed-top .t3-megamenu .logout-link').parent().css({"bottom": "20px", "position": "fixed"});
            jQuery('.navbar-header .t3-megamenu .cart-link').append('<span class = "noti_cart"><?php  echo count($cart); ?></span>');
            // jQuery('.t3-mainbody,.wrap.t3-sl').click(function() {
            //         jQuery('#t3-mainnav .navbar-menu-icon').removeClass('show');
            //         jQuery('.t3-wrapper .right-column').removeClass('show-secondmenu');
            //         jQuery('.t3-wrapper .tab.left-column').removeClass('show-secondmenu');
            //         jQuery('.navbar-fixed-top').removeClass('show-secondmenu');
            //         jQuery('.course-menu, .changePosition').removeClass('show-secondmenu');
            //         jQuery('#community-wrap .jomsocial .joms-landing > .btn-group.btn-group-justified, .com_community.view-groups.task-adddiscussion .discussion-buttons, .com_community.view-groups.task-viewdiscussion .joms-page > .joms-comment__reply').removeClass('show-secondmenu');
            //         jQuery('.navbar-fixed-top .t3-megamenu > ul > li').removeClass('active');
            //         jQuery('.navbar-fixed-top .t3-megamenu').removeClass('visible');
            // });
            jQuery(document).click(function(e) {
                if (!jQuery(e.target).hasClass('navbar-menu-icon') && !jQuery(e.target).parent().hasClass('navbar-menu-icon')) {
                    jQuery('#t3-mainnav .navbar-menu-icon').removeClass('show');
//                    jQuery('.t3-wrapper .right-column').removeClass('show-secondmenu');
//                    jQuery('.t3-wrapper .tab.left-column').removeClass('show-secondmenu');
                    jQuery('.navbar-fixed-top').removeClass('show-secondmenu');
//                    jQuery('.course-menu, .changePosition').removeClass('show-secondmenu');
                    jQuery('.joomdle-overview-header').removeClass('show-secondmenu');
                    jQuery('#community-wrap .jomsocial .joms-landing > .btn-group.btn-group-justified, .com_community.view-groups.task-adddiscussion .discussion-buttons, .com_community.view-groups.task-viewdiscussion .joms-page > .joms-comment__reply').removeClass('show-secondmenu');
                    jQuery('.navbar-fixed-top .sidemenu-bar .t3-megamenu > ul > li').removeClass('active');
                    jQuery('.navbar-fixed-top .t3-megamenu').removeClass('visible');
                    jQuery('.navbar-notification').show();
                    jQuery('.search-all').show();
                    jQuery('.sidemenu-cancel').removeClass('active');
                }
            });
            // notification
            jQuery('.navbar-notification').popover({
                html : true,
                content: function() {
                    return jQuery('#popover_content_wrapper').html();
                }
            });
            // end show
            jQuery('.navbar-notification').click(function (e) {
                e.stopPropagation();
            });
            jQuery(document).click(function (e) {
                if ((jQuery('.popover').length == 1 && jQuery('.popover').has(e.target).length == 0) || jQuery(e.target).is('.close')) {
                    jQuery('.navbar-notification').popover('hide');
                }
            });
            // end hide
            // update notification count
            jQuery('.navbar-notification.have-ntf').on('click', function() {
                jQuery('.notification-count').css('display', 'none');
                jQuery('.navbar-notification .icon-ntf').removeClass('ntf-have');
                jQuery('.joomdle-mylearning .notification').css('display', 'none');

                var title = document.title;
                var count = title.split(" ")[0];
                if ( (count.indexOf("(") != -1) && (count.indexOf(")") != -1) ) {
                    var countNum = count.replace('(', '');
                    countNum = countNum.replace(')', '');
                    if (jQuery.isNumeric(countNum)) {
                        document.title = title.replace(count+' ', '');
                    }
                }

                jQuery('.ntf-have').attr("src", "/templates/t3_bs3_tablet_desktop_template/images/NoNotification.png");

                var user = '<?php echo $user2->id; ?>';
                jQuery.ajax({
                    type: 'post',
                    url: 'index.php?option=com_community&view=profile&task=ajaxReadAllNotification',
                    data: {userid: user},
                    success: function(data) {

                    }
                });
                 jQuery.ajax({
                    type: 'post',
                    url: 'index.php?option=com_community&view=profile&task=ajaxReadGroupNotification',
                    data: {userid: user},
                    success: function(data) {
                    }
            });
            });
        }(jQuery));

        var processing = false;
        window.onload = function() {
            window.addEventListener("beforeunload", function (e) {
                if (!processing) {
                    return undefined;
                }

                var confirmationMessage = 'It looks like something is processing.'
                                        + 'If you leave before finishing, you will not see result.';

                (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
            });
        };

        function lgtCreatePopup(type, data, buttonCallback) {
            var popupHTML = '';
            var extraClass = data.extraClass ? data.extraClass : '';
            var header = data.header ? data.header : '';
            var content = data.content ? data.content : '';
            var subcontent = data.subcontent ? data.subcontent : '';

            switch(type) {
                case 'confirm':
                    var cancelText = data.cancelText ? data.cancelText : 'Cancel';
                    var yesText = data.yesText ? data.yesText : 'Yes';

                    popupHTML = '<div class="confirmPopup lgtPopup hienPopup'+extraClass+'">'+
                    '<div class="popupContent">'+
                    '<p class="popup-content">'+content+'</p>'+
                        '<p class="popup-subcontent">'+subcontent+'</p>'+
                    '</div>'+
                    '<div class="buttons">'+
                        '<button type="button" class="btCancelPopup" onclick="lgtRemovePopup()">'+cancelText+'</button>'+
                    '<button type="button" class="btYesPopup">'+yesText+'</button>'+
                    '</div>'+
                    '</div>';
                    break;
                case 'withCloseButton':
                    popupHTML = '<div class="withCloseButtonPopup lgtPopup hienPopup'+extraClass+'">'+
                    '<div class="popupContent">'+
                    '<p class="popup-content">'+content+'</p>'+
                        '<p class="popup-subcontent">'+subcontent+'</p>'+
                    '</div>'+
                        '<button type="button" class="lgtPopupClose" onclick="lgtRemovePopup()">CLOSE</button>'+
                    '</div>';
                    break;
                case 'oneButton':
                    var buttonText = data.buttonText ? data.buttonText : 'Close';
                    popupHTML = '<div class="oneButtonPopup lgtPopup hienPopup'+extraClass+'">'+
                    '<div class="popupContent">'+
                    '<p class="popup-content">'+content+'</p>'+
                        '<p class="popup-subcontent">'+subcontent+'</p>'+
                    '</div>'+
                    '<button type="button">'+buttonText+'</button>'+
                    '</div>';
                    break;
                case 'loading': 
                    popupHTML = '<div class="lgtAlert lgtPopup '+extraClass+' alert alert-loading alert-dismissible">'+
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+
                        '<strong>'+header+'<span class="dotone">.</span><span class="dottwo">.</span><span class="dotthree">.</span></strong><span>'+content+'</span>'+
                        '</div>';
                    break;
                case 'warning': 
                    popupHTML = '<div class="lgtAlert lgtPopup '+extraClass+' alert alert-warning alert-dismissible">'+
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+
                        '<strong>'+header+'</strong><span>'+content+'</span>'+
                        '</div>';
                    break;
                default:
                    popupHTML = '<div class="notification notCloseButtonPopup lgtPopup '+extraClass+'">'+
                    '<div class="popupContent">'+
                    '<p class="popup-content">'+content+'</p>'+
                        '<p class="popup-subcontent">'+subcontent+'</p>'+
                    '</div>'+
                    '</div>';
                    break;
            }
            jQuery(popupHTML).prependTo('body').fadeIn();
            if (type != 'loading' && type != 'warning') jQuery('body').addClass('lgtOverlay');

            if (buttonCallback && typeof(buttonCallback) === "function") {
                switch (type) {
                    case 'confirm':
                        jQuery('.confirmPopup .btYesPopup').off().on('click', function() {
                            jQuery(this).parent().parent('.lgtPopup').fadeOut(function() {
                                jQuery(this).remove();
                                if (jQuery('.lgtPopup').length == 0) jQuery('body').removeClass('lgtOverlay');
                            });
                            buttonCallback();
                        });
                        break;
                    case 'oneButton':
                        jQuery('.oneButtonPopup button').off().on('click', function() {
                            jQuery(this).parent().parent('.lgtPopup').fadeOut(function() {
                                jQuery(this).remove();
                                if (jQuery('.lgtPopup').length == 0) jQuery('body').removeClass('lgtOverlay');
                            });
                            buttonCallback();
                        });
                        break;
                }
            }
        }

        function lgtRemovePopup() {
            jQuery('.lgtPopup').fadeOut(function() {
                jQuery(this).remove();
                if (jQuery('.lgtPopup').length == 0) jQuery('body').removeClass('lgtOverlay');
            });
        }

        jQuery(document).on('click', '.lgtPopupClose', function(e) {
            jQuery(this).parents('.lgtPopup').fadeOut(function() {
                jQuery(this).remove();
                if (jQuery('.lgtPopup').length == 0) jQuery('body').removeClass('lgtOverlay');
            });
        }).on('click', '.lgtPopup .btCancelPopup', function(e) {
            jQuery(this).parents('.lgtPopup').fadeOut(function() {
                jQuery(this).remove();
                if (jQuery('.lgtPopup').length == 0) jQuery('body').removeClass('lgtOverlay');
            });
        });

        function goBack() {
            console.log(document.referrer);
            if (document.referrer == <?php echo '\''.JUri::base().'\''?>+'component/community/' ||
                document.referrer == <?php echo '\''.JUri::base().'\''?>+'component/community' ) {
                console.log(1);
                window.location.href=<?php echo '\''.JUri::base().'\''?>;
            }
            else {
                if( this.phonegapNavigationEnabled &&
                    nav &&
                    nav.app &&
                    nav.app.backHistory ){
                    nav.app.backHistory();
                } else {
                    var str = document.referrer;
                    var link = str.includes("viewabout");
                    if(link)
                        window.location.href = document.referrer;
                    else window.history.back();
                }
            }
        }
        function goBackAdmin() {
            window.location.href = "<?php echo JRoute::_('index.php?option=com_siteadminbln', false);?>";
        }
        function goBackCourse(){
            var str = window.location.href;
            var link = str.includes("course");
            if(link)
                window.location.href= '<?php echo JURI::root().'manage' ?>';
            else window.history.back();
        }
    </script>
    <!-- //MAIN NAVIGATION -->
<?php
//if ($newNotificationCount != 0) {
//    JFactory::getDocument()->setTitle('('.$newNotificationCount.') '.JFactory::getDocument()->getTitle());
//} ?>

 <script>
     jQuery(document).ready(function($){
        var originalPos;
        $(document).on("scroll", function(e){
         if($('body').find('.joomdle-course').length !== 0) {
            e.preventDefault();
            var detachTop = $(".joomdle-course").height();
            if($("#nav").hasClass("affixed")){
                if($(document).scrollTop() <= 100) {
                if(detachTop < 600) {
                    $(".joomdle-course").height(detachTop - originalPos);
                }
                    $("#nav").removeClass("affixed");
                return;
            }
            }

            if((originalPos = $(document).scrollTop()) >= ($("#nav").offset().top - $("#nav").height())){
                if(originalPos < 300) {
                    $(".joomdle-course").height(800);
                } else {
                    $(".joomdle-course").height(700);
                }
                $("#nav").addClass("affixed");
                return false;
            }
         }

            if($('body').find('.joms-main').length !== 0) {
                e.preventDefault();
                var detachTop = $(".joms-main").height();
                if($(".jomsocial").hasClass("affixed")){
                    if($(document).scrollTop() <= 125) {
                        if(detachTop < 600) {
                            $(".joms-main").height(detachTop - originalPos);
                        }
                        $(".jomsocial").removeClass("affixed");
                        return;
                    }
                }

                var h = $(".course-menu ").height();
                if (h === null) {
                    h = 10;
                }
                if((originalPos = $(document).scrollTop()) >= ($(".joms-focus__title").offset().top - $(".joms-focus__title").height() - h)) {
                    $(".jomsocial").addClass("affixed");
                    return false;
                }
            }
            
            if($('body').find('.contentpane').length !== 0) {
                e.preventDefault();
                var detachTop_scorm = $(".contentpane").height();
                if($("#nav").hasClass("affixed")){
                    if($(document).scrollTop() <= 100) {
                        if(detachTop_scorm < 600) {
                            $(".contentpane").height(detachTop_scorm - originalPos);
                        }
                        $("#nav").removeClass("affixed");
                        return;
                    }
                }

                if((originalPos = $(document).scrollTop()) >= ($("#nav").offset().top - $("#nav").height())){
                    if(originalPos < 300) {
                        $(".contentpane").height(800);
                    } else {
                        $(".contentpane").height(700);
            }
                    $("#nav").addClass("affixed");
                    return false;
                }
            }
        });
        // $('#keywords').tagsInput();
        // $('#field17').tagsInput();
        $('#LpTermCheckbox').on('click', function(e) {
            if($('.LpTermCondition').hasClass('checked')) {
                $('.rsform-submit-button').addClass('disabled');
            } else {
               $('.rsform-submit-button').removeClass('disabled'); 
            }
            $(this).toggleClass('checked').find('.LpTermCondition').prop('checked', $(this).hasClass('checked'));
        });
    }(jQuery));
    
 </script>
