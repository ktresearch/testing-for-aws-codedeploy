<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<div id="t3-mainbody" class="container t3-mainbody">
    <div class="row">
    <div class="first-login-message">
            <h2><?php echo JText::_('T3_WELCOME_TITLE'); ?></h2>
            <p>
                <?php echo JText::_('T3_WELCOME_CONTENT'); ?>
            </p>
    </div>
    <div class="guide-faq">
        <div class="faq-content">
            <img src="./images/Live-Help-Icon-393072.jpg" />
            <p>
                Need Help?
            </p>
            <p>
                Check out our FAQ
            </p>
            <button type="button" class="btn-actions">Dismiss</button>
        </div>
    </div>
    </div>
</div>