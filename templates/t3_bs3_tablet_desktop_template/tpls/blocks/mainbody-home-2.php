<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$session          = JFactory::getSession();
$first_time_login = $session->get('first_time');
?>

<div class="home">

	<?php if ($this->countModules('home-1')) : ?>
		<!-- HOME SL 1 -->
		<div class="wrap t3-sl t3-sl-1 <?php $this->_c('home-1') ?>">
			<jdoc:include type="modules" name="<?php $this->_p('home-1') ?>" style="raw" />
		</div>
		<!-- //HOME SL 1 -->
	<?php endif ?>

	<?php if ($this->countModules('home-2')) : ?>
		<!-- HOME SL 2 -->
		<div class="wrap t3-sl t3-sl-2 <?php $this->_c('home-2') ?>">
			<div class="container">
				<div class="search">
					<jdoc:include type="modules" name="<?php $this->_p('home-2b') ?>" style="raw" />
				</div>
				<div class="status">
					<jdoc:include type="modules" name="<?php $this->_p('home-2') ?>" style="raw" />
					<?php // if ($first_time_login) {?>
					<!--<a href="index.php/connect/profile/edit" class="create-your-profile"><?php // echo JText::_('JCREATE_YOUR_PROFILE');?></a>-->
					<?php // }?>
				</div>
			</div>
		</div>
		<!-- //HOME SL 2 -->
	<?php endif ?>
	<?php if ($this->countModules('home-4')) : ?>
		<!-- HOME SL 4 -->
		<div class="wrap t3-sl t3-sl-4 <?php $this->_c('home-4') ?>">
			<div class="container">
				<jdoc:include type="modules" name="<?php $this->_p('home-4') ?>" style="raw" />
			</div>
		</div>
		<!-- //HOME SL 4 -->
	<?php endif ?>

	<?php if ($this->countModules('home-3')) : ?>
		<!-- HOME SL 3 -->
		<?php
		if ($first_time_login) {
			JFactory::getDocument()->setTitle(
				JText::sprintf('JFRONTPAGE_TITLE', JFactory::getUser()->name)
			);

		} else {
			/*JFactory::getDocument()->setTitle(
                JText::sprintf('JWELCOME_BACK', JFactory::getUser()->name)
            );*/
			// change title page
			JFactory::getDocument()->setTitle(
				JText::sprintf('JWELCOME_HOMEPAGE')
			);
		}
		?>
		<div class="wrap t3-sl t3-sl-3 <?php $this->_c('home-3') ?>">
			<div class="container">
				<jdoc:include type="modules" name="<?php $this->_p('home-3') ?>" style="raw" />
			</div>
		</div>
		<!-- //HOME SL 3 -->
	<?php endif ?>


</div>