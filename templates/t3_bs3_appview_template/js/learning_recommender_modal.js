(function($) {

	// Defining our jQuery plugin

	$.fn.paulund_modal_box = function(prop) {

		// Default parameters

		var options = $.extend({
			height: "550",
			width: "900",
			title: "JQuery Modal Box Demo",
			description: "Example of how to create a modal box.",
			top: "4%",
			left: "17%",
		}, prop);

		return this.click(function(e) {
			add_block_page();
			add_popup_box();
			add_styles();

			$('.paulund_modal_box').fadeIn();
		});

		function add_styles() {
			$('.paulund_modal_box').css({
				'position': 'relative',//'absolute',
				//'left': options.left,
				'margin':'auto',
				'text-align':'center',
				'vertical-align':'middle',
				'top': '100px',//options.top,
				'display': 'none',
				'height': options.height + 'px',
				'width': options.width + 'px',
				'border': '1px solid #fff',
				'box-shadow': '0px 2px 7px #292929',
				'-moz-box-shadow': '0px 2px 7px #292929',
				'-webkit-box-shadow': '0px 2px 7px #292929',
				'border-radius': '10px',
				'-moz-border-radius': '10px',
				'-webkit-border-radius': '10px',
				'background': '#f2f2f2',
				'z-index': '50',
			});
			$('.paulund_modal_close').css({
				'position': 'relative',
				'float': 'right',
				'display': 'block',
				'height': '43px',
				'width': '35px',
				'background': 'url(../templates/t3_bs3_tablet_desktop_template/images/recommender/login_close.png) no-repeat',
				'background-size': 'contain',
				'z-index': '2',
				'top': '4px',
				'right': '4px'
			});
			/*Block page overlay*/
			var pageHeight = $(document).height();
			var pageWidth = $(window).width();

			$('.paulund_block_page').css({
				'position': 'absolute',
				'top': '0',
				'left': '0',
				'background-color': 'rgba(0,0,0,0.6)',
				'height': pageHeight,
				'width': pageWidth,
				'z-index': '1032'
			});
			$('.paulund_inner_modal_box').css({
				'background-color': '#fff',
				'height': (options.height) + 'px',
				'width': (options.width) + 'px',
				'border-radius': '10px',
				'-moz-border-radius': '10px',
				'-webkit-border-radius': '10px',
				'position': 'absolute',
				'z-index': 0
			});
		}

		function add_block_page() {
			var block_page = $('<div class="paulund_block_page"></div>');
			$(block_page).appendTo('body');
		}

		function add_popup_box() {
			var firstPop = $(
				'<div id="step1" class="inner_bubble"><span class="stepTitle">Tell us more about you...</span><div class="1a firstBubble"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/1a.png"/></div><div class="1b firstBubble"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/1b.png"/></div><div class="1c firstBubble"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/1c.png"/></div></div>'
			);
			var secondPop = $(
				'<div id="step2" class="inner_bubble"><span class="stepTitle">I am...</span><div class="2a secondBubble"><a href="/bundle/first-time-at-work" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/2a.png"/></a></div><div class="2b secondBubble"><a href="/bundle/returning-to-work" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/2b.png"/></a></div><div style="clear:both;"></div><div class="2c secondBubble"><a href="/bundle/changing-careers" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/2c.png"/></a></div><div class="2d secondBubble"><a href="/bundle/becoming-a-manager" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/2d.png"/></a></div><div class="startAgain"><a >Start Again</a></div> </div>'
			);
			var thirdPop = $(
				'<div id="step3" class="inner_bubble"><span class="stepTitle">I want to learn about...</span><div class="3a threeBubble"><a href="/bundle/communication" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/3a.png"/></a></div><div class="3b threeBubble"><a href="/bundle/creativity-and-critical-thinking" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/3b.png"/></a></div><div class="3c threeBubble"><a href="/bundle/leadership-and-management" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/3c.png"/></a></div><div class="3d threeBubble"><a href="/bundle/negotiation" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/3d.png"/></a></div><div class="3e threeBubble"><a href="/bundle/sales-management" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/3e.png"/></a></div><div class="3f threeBubble"><a href="/bundle/customer-relationship-management" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/3f.png"/></a></div><div class="3g threeBubble"><a href="/bundle/strategic-thinking-and-business-planning" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/3g.png"/></a></div><div class="3h threeBubble"><a href="/bundle/project-management" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/3h.png"/></a></div><div class="3i threeBubble"><a href="/bundle/personal-effectiveness" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/3i.png"/></a></div><div class="3j threeBubble"><a href="/bundle/change-management" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/3j.png"/></a></div><div class="startAgain"><a >Start Again</a></div> </div>'
			);
			var fourthPop = $(
				'<div id="step4" class="inner_bubble"><span class="stepTitle">I want to build my skills in...</span><div class="4a fourBubble"><a href="/bundle/project-management-deep-skills" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/4a.png"/></a></div><div class="4b fourBubble"><a href="/bundle/hr-management" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/4b.png"/></a></div><div class="4c fourBubble"><a href="/bundle/cisco" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/4c.png"/></a></div><div class="4d fourBubble"><a href="/bundle/linux" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/4d.png"/></a></div><div class="4e fourBubble"><a href="/bundle/vmware" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/4e.png"/></a></div><div class="4f fourBubble"><a href="/bundle/six-sigma" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/4f.png"/></a></div><div class="4g fourBubble"><a href="/bundle/microsoft" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/4g.png"/></a></div><div class="4h fourBubble"><a href="/bundle/oracle" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/4h.png"/></a></div><div class="4i fourBubble"><a href="/bundle/comptia" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/4i.png"/></a></div><div class="4j fourBubble"><a href="/bundle/itilr" target="_blank"><img src="../templates/t3_bs3_tablet_desktop_template/images/recommender/4j.png"/></a></div><div class="startAgain"><a >Start Again</a></div> </div>'
			);
			var fifthPop = $(
				'<div id="step5" class="inner_bubble"><span class="stepTitle">Done! Click the button below to view your recommended course packets.</span><a class="finalSumit"  target="_blank"><div class="3a threeBubble"><span class="circleText">VIEW BUNDLES</span></div></a><div class="tryAgain"><a class="tryAgainA" style="color:#251F5B;cursor:pointer">Try Again</a></div></div>'
			);
			var pop_up = $(
				'<div class="paulund_modal_box"><a  class="paulund_modal_close"></a><div class="paulund_inner_modal_box"></div></div>'
			);
			$(pop_up).appendTo('.paulund_block_page');
			$(firstPop).appendTo('.paulund_inner_modal_box');
			$(secondPop).appendTo('.paulund_inner_modal_box');
			$(thirdPop).appendTo('.paulund_inner_modal_box');
			$(fourthPop).appendTo('.paulund_inner_modal_box');
			$(fifthPop).appendTo('.paulund_inner_modal_box');
			$(secondPop).css({
				'display': 'none'
			});
			$(thirdPop).css({
				'display': 'none'
			});
			$(fourthPop).css({
				'display': 'none'
			});
			$(fifthPop).css({
				'display': 'none'
			});
			$('.1a').click(function() {
				$('#step1').css({
					'display': 'none'
				});
				$('#step2').css({
					'display': 'block'
				});
			});
			$('.1b').click(function() {
				$('#step1').css({
					'display': 'none'
				});
				$('#step3').css({
					'display': 'block'
				});
			});
			$('.1c').click(function() {
				$('#step1').css({
					'display': 'none'
				});
				$('#step4').css({
					'display': 'block'
				});
			});
			$('.startAgain a').click(function() {
				$('#step2').css({
					'display': 'none'
				});
				$('#step3').css({
					'display': 'none'
				});
				$('#step4').css({
					'display': 'none'
				});
				$('#step5').css({
					'display': 'none'
				});
				$('#step1').css({
					'display': 'block'
				});

			});

			$('.paulund_modal_close').click(function() {
				$(this).parent().fadeOut().remove();
				$('.paulund_block_page').fadeOut().remove();
			});
		}

		return this;
	};

})(jQuery);
