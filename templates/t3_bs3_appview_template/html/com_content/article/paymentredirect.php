<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

// Create shortcuts to some parameters.
$params  = $this->item->params;
$images  = json_decode($this->item->images);
$urls    = json_decode($this->item->urls);
$canEdit = $params->get('access-edit');
$user    = JFactory::getUser();
$info    = $params->get('info_block_position', 0);
JHtml::_('behavior.caption');
//PA 2.0
$document = JFactory::getDocument();
$document->addStyleSheet('templates/t3_bs3_blank/css/learning_recommender.css');
$document->addStyleSheet('templates/t3_bs3_blank/css/product_hika_mobile.css');
$document->addCustomTag('<script src="/templates/t3_bs3_tablet_desktop_template/js/learning_recommender_modal.js"></script>');
$document->addCustomTag('<script src="/templates/t3_bs3_tablet_desktop_template/js/learning_recommender_modal_box.js"></script>');
$document->setMetaData( 'robots', 'noindex' );

// language /vn or /en
	if(strcmp((JFactory::getLanguage()->getTag()),'vi-VN')==0) {
	    $langcurrent = 'vn/';
	}
	
	
$app2= JFactory::getApplication();
$order_id=$app2->getUserState('com_hikashop.order_id');
if(!include_once(rtrim(JPATH_ADMINISTRATOR,DS).DS.'components'.DS.'com_hikashop'.DS.'helpers'.DS.'helper.php')) return true;
$class=hikashop_get('class.order');
$currencyClass = hikashop_get('class.currency');
$imageHelper = hikashop_get('helper.image');
$productHelper = hikashop_get('class.product');
$order=$class->get($order_id);
$total_quality=0;


if(!empty($order)){
	if($order->order_payment_method=="enetscredit"){
		$url=strtok($_SERVER["SERVER_NAME"],'?');
		if(substr($url, 0, 1)=="/"){
			$url="localhost".$url;
		}
		$url_string=$url."/index.php?option=com_hikashop&ctrl=checkout&task=notify&notif_payment=enetscredit&tmpl=component&".$_SERVER['QUERY_STRING'];
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $url_string);
		curl_exec($ch);
		curl_close($ch);
		$order=$class->get($order_id);
		if($order->order_status=='confirmed'){
			?>
			<div class="purchasing">
			<div class="row purchasing-thanks">
                            <p>
                                <?php echo JText::_('KD_THANKS_PURCHASE'); ?>
                            </p>
                            <p>
				 <?php echo JText::_('KD_PAYMENT_SUCCESSFULL'); ?>
                            </p>
			</div>
			<div class="row purchasing-button">
                            <div>
                                <a href="index.php?option=com_hikashop&ctrl=order&task=show&cid=<?php echo $order_id;?>" class="view_invoice"><?php echo JText::_('KD_CHECKOUT_VIEW_INVOICE'); ?></a>
                            </div>
                            <div>
				<a href="<?php echo JURI::root().''.$langcurrent.'featured';?>" class="view_invoice" style="width:180px;margin:auto;background-size:8%;margin-top:10px;padding:5px;padding-left: 25px;padding-right: 30px;">
                                <span><?php echo JText::_('KD_CONTINUE_SHOPPING'); ?></span>
				</a>
                            </div>
                            <div>
				<a href="<?php echo 'mylearning/list.html';?>" class="view_invoice" style="width:180px;margin:auto;background-size:8%;margin-top:10px;padding:5px;padding-left: 40px;padding-right: 45px;margin-left: 22px;">
					<span><?php echo JText::_('KD_START_LEARNING'); ?></span>
				</a>
                            </div>
			</div>
                        </div>
                        <div class="row purchasing-summary-title">
                            <span><?php echo JText::_('KD_PURCHA_SUMMARY_TITLE'); ?></span>
                        </div>
                        <div class="row purchasing-order">
                            <div class="order-number">
                                <div class="col-xs-6"><span class="oder-number-text"><?php echo JText::_('HIKASHOP_ORDER'); ?></span>&nbsp;<span class="oder-number-value"><?php echo $order->order_number; ?></span></div>
                                <div class="col-xs-6"><span class="oder-number-text"><?php echo JText::_('DATE'); ?></span>&nbsp;<span class="oder-number-value"><?php echo date('d/m/Y',  $order->order_created); ?></span></div>
                            </div>
                        </div>
                        <div class="row order-product">
                            <span><?php echo JText::_('KD_PURCHA_SUMMARY_PRODUCT'); ?></span>
                        </div>
			<?php
			$class->loadProducts($order);
			if(!empty($order->products)){
				foreach($order->products as $i=>$order_product) {
					$total_quality+=1;
					$class2=hikashop_get('class.product');
					$imageHelper = hikashop_get('helper.image');
					$query = 'SELECT * from '.hikashop_table('file')." WHERE file_type='product' AND file_ref_id=".$order_product->product_id;
					$database->setQuery($query);
					$images =$database->loadObjectList();
					$product=$class2->get($order_product->product_id);
					?>
					<div class="row cart-item">
						<?php

					if(!empty($images[0]->file_path)){
						$img = $imageHelper->getThumbnail(@$images[0]->file_path, array('width' =>"400px" , 'height' => "300px"), null);
						$file_path=str_replace('\\','/',$img->path);
						$image_path=$imageHelper->uploadFolder_url.$file_path;
					}else{
						$image_path="http://placehold.it/350x150";
					}

                                        ?>
                                        <!--<div class="col-xs-4 img-course" style="background-image:url('<?php echo $image_path; ?>');"></div>-->
					<div class="col-xs-8 cart-course-title">
                                        <div>
                                            <span class="checkout_product_title"><?php echo $order_product->order_product_name; ?></span>
                                        </div>
<!--                                        <div>
                                            <span class="description"><?php echo JHtml::_('string.truncate', strip_tags($order_product->product_description), 150, true, $allowHtml = true); ?></span>
                                        </div>-->
                                        </div>
					<div class="col-xs-3 checkout_price_detail" style="padding:0px;text-align:right;">
                                            <?php echo $currencyClass->format($order_product->order_product_price,$order->order_currency_id);?>
					</div>
					</div>
					<?php
                                        if(count($order->products)-1!= $i){
                                            echo "<hr style='background-color:#dbe7f1;'/>";
                                        }

				}
			}
			?>
                        <hr class="end-total" />
                        <div class="row cart-total" style="margin-left:14px;margin-right:14px;margin-bottom:14px;font-size:16px;font-weight:600;">
                                <div class="col-xs-4" style="padding:0px;"></div>
                                <div class="col-xs-8 total-summary" style="text-align:right;padding-right:0px;">
                                    <span class="total-text">
                                        <?php echo JText::_('KD_TOTAL_COST'); ?>
                                    </span>
                                    <span class="total-currentcy">
                                        <?php
                                                echo $currencyClass->format($order->order_full_price,$order->order_currency_id);
                                        ?>                        
                                    </span>
                                </div>
                        </div>
                        <?php 
                            if(!empty($order->order_payment_method)) {
                                ?>
                                <div class="row order-payment-method">
                                    <span class="oder-number-text"><?php echo JText::_('KD_ORDER_PAYMENT_MOD'); ?></span>&nbsp;<span class="oder-number-value"><?php echo $order->order_payment_method; ?></span>
                                </div>
                                <?php 
                            }
                        ?>
			<?php
		}
	}
	if($order->order_payment_method=='paypal'){
		if($order->order_status=='confirmed'){
			?>
			<div class="purchasing">
			<div class="row purchasing-thanks">
                            <p>
                                <?php echo JText::_('KD_THANKS_PURCHASE'); ?>
                            </p>
                            <p>
				 <?php echo JText::_('KD_PAYMENT_SUCCESSFULL'); ?>
                            </p>
			</div>
			<div class="row purchasing-button">
                            <div>
                                <a href="index.php?option=com_hikashop&ctrl=order&task=show&cid=<?php echo $order_id;?>" class="view_invoice"><?php echo JText::_('KD_CHECKOUT_VIEW_INVOICE'); ?></a>
                            </div>
                            <div>
				<a href="<?php echo JURI::root().''.$langcurrent.'featured';?>" class="view_invoice" style="width:180px;margin:auto;background-size:8%;margin-top:10px;padding:5px;padding-left: 25px;padding-right: 30px;">
                                <span><?php echo JText::_('KD_CONTINUE_SHOPPING'); ?></span>
				</a>
                            </div>
                            <div>
				<a href="<?php echo 'mylearning/list.html';?>" class="view_invoice" style="width:180px;margin:auto;background-size:8%;margin-top:10px;padding:5px;padding-left: 40px;padding-right: 45px;margin-left: 22px;">
					<span><?php echo JText::_('KD_START_LEARNING'); ?></span>
				</a>
                            </div>
			</div>
                        </div>
                        <div class="row purchasing-summary-title">
                            <span><?php echo JText::_('KD_PURCHA_SUMMARY_TITLE'); ?></span>
                        </div>
                        <div class="row purchasing-order">
                            <div class="order-number">
                                <div class="col-xs-6"><span class="oder-number-text"><?php echo JText::_('HIKASHOP_ORDER'); ?></span>&nbsp;<span class="oder-number-value"><?php echo $order->order_number; ?></span></div>
                                <div class="col-xs-6"><span class="oder-number-text"><?php echo JText::_('DATE'); ?></span>&nbsp;<span class="oder-number-value"><?php echo date('d/m/Y',  $order->order_created); ?></span></div>
                            </div>
                        </div>
                        <div class="row order-product">
                            <span><?php echo JText::_('KD_PURCHA_SUMMARY_PRODUCT'); ?></span>
                        </div>
			<?php
			$class->loadProducts($order);
			if(!empty($order->products)){
				foreach($order->products as $i=>$order_product) {
					$total_quality+=1;
					$class2=hikashop_get('class.product');
					$imageHelper = hikashop_get('helper.image');
					$query = 'SELECT * from '.hikashop_table('file')." WHERE file_type='product' AND file_ref_id=".$order_product->product_id;
					$database->setQuery($query);
					$images =$database->loadObjectList();
					$product=$class2->get($order_product->product_id);
					?>
					<div class="row cart-item">
						<?php

					if(!empty($images[0]->file_path)){
						$img = $imageHelper->getThumbnail(@$images[0]->file_path, array('width' =>"400px" , 'height' => "300px"), null);
						$file_path=str_replace('\\','/',$img->path);
						$image_path=$imageHelper->uploadFolder_url.$file_path;
					}else{
						$image_path="http://placehold.it/350x150";
					}

                                        ?>
                                        <!--<div class="col-xs-4 img-course" style="background-image:url('<?php echo $image_path; ?>');"></div>-->
					<div class="col-xs-8 cart-course-title">
                                        <div>
                                            <span class="checkout_product_title"><?php echo $order_product->order_product_name; ?></span>
                                        </div>
<!--                                        <div>
                                            <span class="description"><?php echo JHtml::_('string.truncate', strip_tags($order_product->product_description), 150, true, $allowHtml = true); ?></span>
                                        </div>-->
                                        </div>
					<div class="col-xs-3 checkout_price_detail" style="padding:0px;text-align:right;">
                                            <?php echo $currencyClass->format($order_product->order_product_price,$order->order_currency_id);?>
					</div>
					</div>
					<?php
                                        if(count($order->products)-1!= $i){
                                            echo "<hr style='background-color:#dbe7f1;'/>";
                                        }

				}
			}
			?>
                        <hr class="end-total" />
                        <div class="row cart-total">
                                <div class="col-xs-4" style="padding:0px;"></div>
                                <div class="col-xs-8 total-summary" style="text-align:right;padding-right:0px;">
                                    <span class="total-text">
                                        <?php echo JText::_('KD_TOTAL_COST'); ?>
                                    </span>
                                    <span class="total-currentcy">
                                        <?php
                                                echo $currencyClass->format($order->order_full_price,$order->order_currency_id);
                                        ?>                        
                                    </span>
                                </div>
                        </div>
                        <?php 
                            if(!empty($order->order_payment_method)) {
                                ?>
                                <div class="row order-payment-method">
                                    <span class="oder-number-text"><?php echo JText::_('KD_ORDER_PAYMENT_MOD'); ?></span>&nbsp;<span class="oder-number-value"><?php echo $order->order_payment_method; ?></span>
                                </div>
                                <?php 
                            }
                        ?>
			<?php
		}elseif ($order->order_status=='created') {
			?>
				<div class="hikashop_checkout_header navbar-fixed-top">
					<div style='height:50px;width:33.33333333%;float:left;text-align:center;position:relative;' class='<?php echo "completed_step"; ?>'>
						<a><span><?php echo JText::_('KD_ITEMS'); ?></span></a>
						<i class="fa fa-caret-up" aria-hidden="true"></i>
					</div>
					<div style='height:50px;width:33.33333333%;float:left;text-align:center;position:relative;' class='<?php echo "completed_step"; ?>'>
						<a><span><?php echo JText::_('KD_PAYMENT_STEP'); ?></span></a>
						<i class="fa fa-caret-up" aria-hidden="true"></i>
					</div>
					<div style='height:50px;width:33.33333333%;float:left;text-align:center;position:relative;' class='<?php echo "completed_step"; ?>'>
						<a><span><?php echo JText::_('KD_CONFIRMATION'); ?></span></a>
						<i class="fa fa-caret-up" aria-hidden="true"></i>
					</div>
				</div>
			<div class="row" style="text-align:center;margin-left:0px;margin-right:0px;padding:60px 20px 0px;font-size:16px;font-weight:600;">
				<?php echo JText::_('KD_PAYMENT_SUCCESSFULL'); ?> <?php echo JText::_('KD_THANKS_PURCHASE'); ?>
			</div>
			<div class="row" style="text-align:center;margin-left:0px;margin-right:0px;margin-top:10px;padding-bottom:20px;">
				<a href="index.php?option=com_hikashop&ctrl=order&task=show&cid=<?php echo $order_id;?>" class="view_invoice" style="background-image:none;padding:5px;width:150px;display:block;margin:auto;"><?php echo JText::_('KD_CHECKOUT_VIEW_INVOICE'); ?></a>
			</div>
			<?php
			$class->loadProducts($order);
			if(!empty($order->products)){
				foreach($order->products as $order_product) {
					$product = $productHelper->get($order_product->product_id);
					$total_quality+=1;
					?>
					<div class="row" style="margin-left:14px;margin-right:14px;margin-bottom: 14px;">
						<div class="col-xs-2" style="padding:0px;">
							<?php
								$db  = JFactory::getDBO();
			                    $query = "SELECT * FROM #__hikashop_file WHERE file_ref_id =".$product->product_id;
			                    $db->setQuery($query);
			                    $load_files = $db->loadObjectList();
			                    if(!empty($load_files)){

			                        $product->file_path = $load_files[0]->file_path;
			                    }

			                    $image_path="";
								if(!empty($product->file_path)){
									$img =$imageHelper->getThumbnail(@$product->file_path, array('width' =>"400px" , 'height' => "300px"), null);
									$file_path=str_replace('\\','/',$img->path);
									$image_path=$imageHelper->uploadFolder_url.$file_path;
								}else{
									$image_path="http://placehold.it/350x150";
								}
							?>
							<img src="<?php echo $image_path; ?>" class="img-responsive" />
						</div>
						<div class="col-xs-7">
							<span  style="display:inline-block;padding-left:10px;" class="checkout_product_title"><?php echo $order_product->order_product_name; ?></span>
						</div>
						<div class="col-xs-3 checkout_price_detail" style="padding:0px;text-align:right;">
							<?php echo $currencyClass->format($order_product->order_product_price,$order->order_currency_id);?>
						</div>
					</div>
					<?php
				}
			}
			?>
			<div class="row" style="margin-left:14px;margin-right:14px;font-size:16px;font-weight:600;">
				<div class="col-xs-8" style="padding:0px;">
					<?php echo JText::_('KD_NUMBER_OF_ITEMS'); ?>
				</div>
				<div class="col-xs-4" style="text-align:right;padding-right:0px;">
					<?php
						echo $total_quality;
					?>
				</div>
			</div>

			<div class="row" style="margin-left:14px;margin-right:14px;font-size:16px;font-weight:600;">
				<div class="col-xs-8" style="padding:0px;">
					<?php echo JText::_('KD_TOTAL_COST'); ?>
				</div>
				<div class="col-xs-4" style="text-align:right;padding-right:0px;">
					<?php echo $currencyClass->format($order->order_full_price,$order->order_currency_id);?>
				</div>
			</div>
			<div class="row" style="text-align:center;margin-left:0px;margin-right:0px;">

				<a href="<?php echo JURI::root().''.$langcurrent.'featured';?>" class="view_invoice" style="width:180px;margin:auto;background-size:8%;margin-top:10px;padding:5px;padding-left: 25px;padding-right: 30px;">
					<span><?php echo JText::_('KD_CONTINUE_SHOPPING'); ?></span>
				</a>
				<a href="<?php echo 'mylearning/list.html';?>" class="view_invoice" style="width:180px;margin:auto;background-size:8%;margin-top:10px;padding:5px;padding-left: 40px;padding-right: 45px;margin-left: 22px;">
					<span><?php echo JText::_('KD_START_LEARNING'); ?></span>
				</a>
			</div>
			<?php
		}
	}
}
?>
