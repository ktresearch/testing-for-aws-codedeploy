<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.1
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
$link=hikashop_contentLink('product&task=listing&cid='.$this->row->category_id.'&name='.$this->row->alias,$this->row);
?>
<div class="hika_listing_img_desc" style="font-family:'Open Sans';">
		<div class="row" style="margin:0px;padding:15px;display:block;text-decoration:none;color:black;background-color:<?php echo $bkColor; ?>" onclick="location.href='<?php echo $link; ?>'">
			<div class="col-xs-3 hikashop_product_listing_image" style="padding:0px;">
				<!-- CATEGORY IMG -->
				<?php if($this->config->get('thumbnail',1)){ ?>
					<?php
						$image_options = array('default' => true,'forcesize'=>$this->config->get('image_force_size',true),'scale'=>$this->config->get('image_scale_mode','inside'));
						$img = $this->image->getThumbnail(@$this->row->file_path, array('width' => $this->image->main_thumbnail_x, 'height' => $this->image->main_thumbnail_y), $image_options);
						if($img->success) {
							echo '<img class="hikashop_product_listing_image" style="width:100%;" title="'.$this->escape(@$this->row->file_description).'" alt="'.$this->escape(@$this->row->file_name).'" src="'.$img->url.'"/>';
						}
					?>
				<?php } ?>
				<!--EO CATEGORY IMG -->
			</div>
			<div class="col-xs-9 hikashop_product_listing_middle">
				<div class="row hikashop_product_title" style="color:#126DB6;font-weight:700;margin:0px;">
					<!-- CATEGORY NAME -->
								<?php

								echo $this->row->category_name;
								if($this->params->get('number_of_products',0)){
									echo ' ('.$this->row->number_of_products.')';
								}
								?>
					<!-- EO CATEGORY NAME -->
				</div>
				<div class="row hikashop_product_listing_description" style="margin:0px;">
					<!-- CATEGORY DESC -->
						<?php
						echo preg_replace('#<hr *id="system-readmore" */>.*#is','',$this->row->category_description);
						?>
					<!-- EO CATEGORY DESC -->
				</div>
			</div>
		</div>

</div>