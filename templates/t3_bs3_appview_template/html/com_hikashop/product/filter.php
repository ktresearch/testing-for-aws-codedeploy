<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.1
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<?php
	//PA 2.0
$document = JFactory::getDocument();
$document->addStyleSheet('templates/t3_bs3_blank/css/learning_recommender.css');
$document->addCustomTag('<script src="/templates/t3_bs3_tablet_desktop_template/js/learning_recommender_modal.js"></script>');
$document->addCustomTag('<script src="/templates/t3_bs3_tablet_desktop_template/js/learning_recommender_modal_box.js"></script>');

?>
<?php
$user = JFactory::getUser();        // Get the user object
if($user->id != 0){
 ?>
	<div class="row menu-barhikashop" style="margin:0px;">
		<div style="width:50%;float:left;background:#9da4a6;height:46px;text-align:center;line-height:35px;color:#d2d3d4;">
			<a href="<?php echo JRoute::_('index.php?option=com_joomdle&view=mylearning');?>" style="line-height:46px;color:#ffffff;font-family:'Open Sans', OpenSans, sans-serif;font-size:18px;font-weight:normal;"><?php echo JText::_('MY_COURSES'); ?></a>
		</div>
		<div style="width:50%;float:left;background:#e1e1e1;height:36px;line-height:35px;color:#458abf;text-align:center;">
			<?php
            			if(strcmp((JFactory::getLanguage()->getTag()),'vi-VN')==0) {
            				?>
            				<a href="<?php echo JRoute::_('/dac-sac');?>" style="line-height:36px;color:#126DB6;font-family:'Open Sans', OpenSans, sans-serif;font-size:13px;font-weight: normal;"><?php echo JText::_('COM_JOOMDLE_LEARNING_CATALOGUE'); ?></a>
            				<?php
            			}
            			if(strcmp((JFactory::getLanguage()->getTag()),'en-GB')==0){
            				?>
            				<a href="<?php echo JRoute::_('/featured');?>" style="line-height:36px;color:#126DB6;font-family:'Open Sans', OpenSans, sans-serif;font-size:13px;font-weight: normal;"><?php echo JText::_('COM_JOOMDLE_LEARNING_CATALOGUE'); ?></a>

            				<?php
            			}
            			?>
		</div>
	</div>
<?php
}
?>
<div class="row search-bar-catalog" style="margin:0px;padding:7px 14px 7px 14px;">
	<input class="search-catalog" type="text" name="keywork" size="30" id="search-catalog" />
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.search-catalog').val(jQuery("#filter_text_CourseFilter_1").val());
		if(jQuery('.search-catalog').val()!=''){
			jQuery("#search-catalog").css('cssText','background-image:none !important;');
		}
		jQuery(".search-catalog").keyup(function(e){
			if(e.which==13){
				jQuery("#filter_text_CourseFilter_1").val(jQuery('.search-catalog').val());
				jQuery(".hikashop_filter_main_div form[name='<?php echo 'hikashop_filter_form_' . $this->params->get('main_div_name'); ?>']").submit();
			}
			if(jQuery(".search-catalog").val().length){
				jQuery("#search-catalog").css('cssText','background-image:none !important;');
			}else{
				jQuery("#search-catalog").css('cssText','background-image:url("/media/com_hikashop/images/SearchIcon_726D6D.png") !important');
			}
		});
	});
</script>
<?php
	// showing category_menu ( Module ) in Product Detail Screen

	jimport('joomla.application.module.helper');
		$modules = JModuleHelper::getModules('category_menu');
		foreach($modules as $module)
		{
			echo JModuleHelper::renderModule($module);
		}

?>
<?php
if(!empty($this->filters)){
	$count=0;
	$filterActivated=false;
	$widthPercent=(100/$this->maxColumn)-1;
	$widthPercent=round($widthPercent);
	static $i = 0;
	$i++;
	$filters = array();
	$url = hikashop_currentURL();
	if(!empty($this->params) && $this->params->get('module') == 'mod_hikashop_filter' && ($this->params->get('force_redirect',0) || (empty($this->currentId) && (JRequest::getVar('option','')!='com_hikashop'|| !in_array(JRequest::getVar('ctrl','product'),array('product','category')) ||JRequest::getVar('task','listing')!='listing')))){
		$type = 'category';
		if(!HIKASHOP_J30){
			$menuClass = hikashop_get('class.menus');
			$menuData = $menuClass->get($this->params->get('itemid',0));
			if(@$menuData->hikashop_params['content_type']=='product')
				$type = 'product';
		}else{
			$app = JFactory::getApplication();
			$menuItem = $app->getMenu()->getActive();
			$hkParams = false;
			if(isset($menuItem->params))
				$hkParams = $menuItem->params->get('hk_category',false);
			if(!$hkParams)
				$type = 'product';
		}
		$url = hikashop_completeLink($type.'&task=listing&Itemid='.$this->params->get('itemid',0));
	}else{
		$url = preg_replace('#&return_url=[^&]+#i','',$url);
	}

	foreach($this->filters as $filter){
		if((empty($this->displayedFilters) || in_array($filter->filter_namekey,$this->displayedFilters)) && ($this->filterClass->cleanFilter($filter))){
			$filters[]=$filter;
		}
		$selected[]=$this->filterTypeClass->display($filter, '', $this);
	}

	foreach($selected as $data){
		if(!empty($data)) $filterActivated=true;
	}

	if(!$filterActivated && empty($this->rows) && $this->params->get('module') != 'mod_hikashop_filter') return;

	if(!count($filters)) return; ?>

	<div class="hikashop_filter_main_div hikashop_filter_main_div_<?php echo $this->params->get('main_div_name'); ?>">
			<?php
		$datas = array();
		if(isset($this->listingQuery)){
			$html=array();
			$datas=$this->filterClass->getProductList($this, $filters);
		}

		foreach($filters as $key => $filter){
			$html[$key]=$this->filterClass->displayFilter($filter, $this->params->get('main_div_name'), $this, $datas);
		}

		if($this->displayFieldset){ ?>
			<div class="row" style="margin:0px;">


		<?php } ?>

		<form action="<?php echo $url; ?>" method="post" name="<?php echo 'hikashop_filter_form_' . $this->params->get('main_div_name'); ?>" enctype="multipart/form-data">
		<?php while($count<$this->maxFilter+1){
			$height='';
			if(!empty($filters[$count]->filter_height)){
				$height='min-height:'.$filters[$count]->filter_height.'px;';
			}else if(!empty($this->heightConfig)){
				$height='min-height:'.$this->heightConfig.'px;';
			}
			if(!empty($html[$count])){
				if($filters[$count]->filter_options['column_width']>$this->maxColumn) $filters[$count]->filter_options['column_width'] = $this->maxColumn;
				 ?>
				<div class="hikashop_filter_main hikashop_filter_main_<?php echo $filters[$count]->filter_namekey; ?>" style="<?php echo $height; ?> float:left; width:<?php echo $widthPercent*$filters[$count]->filter_options['column_width']?>%;" >
					<?php //echo $this->filterClass->displayFilter($this->filters[$count], $this->params->get('main_div_name'), $this);
					echo '<div class="hikashop_filter_'.$filters[$count]->filter_namekey.'">'.$html[$count].'</div>';
					?>
				</div>
				<?php
			}
			$count++;
		}
		if($this->buttonPosition=='inside'){
			if($this->showButton ){
				echo '<div class="hikashop_filter_button_inside" style="float:left; margin-right:10px;">';
				echo $this->cart->displayButton(JText::_('FILTER'),'filter',$this->params,$url,'document.getElementById(\'hikashop_filtered_'.$this->params->get('main_div_name').'\').value=\'1\';document.forms[\'hikashop_filter_form_'. $this->params->get('main_div_name').'\'].submit(); return false;','id="hikashop_filter_button_'. $this->params->get('main_div_name').'"');
				echo '</div>';
			}
			if($this->showResetButton ){
				echo '<div class="hikashop_reset_button_inside" style="float:left;">';
				echo $this->cart->displayButton(JText::_('RESET'),'reset_filter',$this->params,$url,'document.getElementById(\'hikashop_reseted_'.$this->params->get('main_div_name').'\').value=\'1\';document.forms[\'hikashop_filter_form_'. $this->params->get('main_div_name').'\'].submit(); return false;','id="hikashop_reset_button_'. $this->params->get('main_div_name').'"');
				echo '</div>';
			}
		}else{
			echo '<div class="hikashop_filter_button_inside" style="display:none">';
			echo $this->cart->displayButton(JText::_('FILTER'),'filter',$this->params,$url,'document.getElementById(\'hikashop_filtered_'.$this->params->get('main_div_name').'\').value=\'1\';document.forms[\'hikashop_filter_form_'. $this->params->get('main_div_name').'\'].submit(); return false;','id="hikashop_filter_button_'. $this->params->get('main_div_name').'_inside"');
			echo '</div>';
		} ?>
		<input type="hidden" name="return_url" value="<?php echo $url;?>"/>
		<input type="hidden" name="filtered" id="hikashop_filtered_<?php echo $this->params->get('main_div_name');?>" value="1" />
		<input type="hidden" name="reseted" id="hikashop_reseted_<?php echo $this->params->get('main_div_name');?>" value="0" />
	</form>
	<?php
	if($this->displayFieldset){ ?>
			</div>
	<?php }
	if($this->buttonPosition!='inside'){
		$style='style="margin-right:10px;"';
		if($this->buttonPosition=='right'){ $style='style="float:right; margin-left:10px;"'; }
		if($this->showButton){
			echo '<span class="hikashop_filter_button_outside" '.$style.'>';
			echo $this->cart->displayButton(JText::_('FILTER'),'filter',$this->params,$url,'document.getElementById(\'hikashop_filtered_'.$this->params->get('main_div_name').'\').value=\'1\';document.forms[\'hikashop_filter_form_'. $this->params->get('main_div_name').'\'].submit(); return false;','id="hikashop_filter_button_'. $this->params->get('main_div_name').'"');
			echo '</span>';
		}
		if($this->showResetButton){
			echo '<span class="hikashop_reset_button_outside" '.$style.' >';
			echo $this->cart->displayButton(JText::_('RESET'),'reset_filter',$this->params,$url,'document.getElementById(\'hikashop_reseted_'.$this->params->get('main_div_name').'\').value=\'1\';document.forms[\'hikashop_filter_form_'. $this->params->get('main_div_name').'\'].submit(); return false;','id="hikashop_reset_button_'. $this->params->get('main_div_name').'"');
			echo '</span>';
		}
	} ?>
	</div>
	<?php
		// showing category_menu ( Module ) in Product Detail Screen
/*
		jimport('joomla.application.module.helper');
			$modules = JModuleHelper::getModules('category_menu');
			foreach($modules as $module)
			{
				echo JModuleHelper::renderModule($module);
			}
*/
	?>
	<div class="row" style="margin:0px;">
		<a class="paulund_modal help-choose-button">Launch Course Recommender</a>
	</div>
<?php } ?>
