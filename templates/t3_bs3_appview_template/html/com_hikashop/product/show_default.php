<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.1
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<?php
	//PA 2.0
$document = JFactory::getDocument();
$document->addStyleSheet('templates/t3_bs3_blank/css/product_hika_mobile.css');
$image_link='/images/com_hikashop/upload/thumbnails/100x100f/barcode.png';
?>
<?php
			$database =& JFactory::getDBO();
			$query = 'SELECT * from '.hikashop_table('category')." WHERE category_name='Learning Provider' ";
			$database->setQuery($query);
			$categories =$database->loadObjectList();
			foreach ($this->categories as $category) {
				if(!empty($categories) && $categories[0]->category_id==$category->category_parent_id){
						$database->setQuery('SELECT * FROM '.hikashop_table('file').' WHERE file_type = "category" AND file_ref_id = '.(int)$category->category_id);
						$image = $database->loadObject();
						$fileClass = hikashop_get('helper.image');
						//TODO: clean up file path using hikashop file class
						if($image){
							$imgString='<img src="/images/com_hikashop/upload/'.$image->file_path.'" style="max-width:75%;margin:auto;display:block;object-fit:contain;height50px;"/>';
						}else{
							$imgString= " ";
						}
				}

			}
?>
<div class="hikashop_product_detail" style="padding:14px;">
	<div class="row">
		<div class="col-xs-12 col-md-10" style="min-height:50px;text-align:center;font-family:'Open Sans';font-weight:600;color:#126DB6;font-size:14px;">
			<!-- PRODUCT TITLE -->
			<span>
			<?php
				if (hikashop_getCID('product_id')!=$this->element->product_id && isset ($this->element->main->product_name))
					echo $this->element->main->product_name;
					else
					echo $this->element->product_name;
			?>
			<!-- PRODUCT TITLE -->
			</span>
		</div>
	</div>
	<div class="row">
		<?php

					if(!empty($this->element->images[0]->file_path)){
						$img = $this->image->getThumbnail(@$this->element->images[0]->file_path, array('width' =>"400px" , 'height' => "300px"), null);
						$file_path=str_replace('\\','/',$img->path);
						$image_path=$this->image->uploadFolder_url.$file_path;
					}else{
						$image_path="http://placehold.it/350x150";
					}

				?>
			<div class="col-xs-6 col-md-6 hikashop_product_listing_image" style="margin-left:15px;height:100px;background-image:url('<?php echo $image_path; ?>');background-repeat:no-repeat;background-size:cover;background-position:center;">

			</div>
		<div class="col-xs-5 col-md-5">
				<?php
					if(!empty($imgString)){
						echo $imgString;
					}
				?>
				<!--<img src="http://placehold.it/350x150" style="max-width:75%;margin:auto;display:block;"/>-->
			<div class="row" style="text-align:center;line-height:30px;color:#494949">
				Duration :
				<?php
				 if(!empty($this->element->estimated_duration)){
					 $est_time=(double)$this->element->estimated_duration;
					 $suffix=" Hours";
					 if($est_time <= 1)
						 $suffix=" Hour";
					 echo $this->element->estimated_duration.$suffix;
				 }
			 	?>
			</div>
		</div>
	</div>
	<div class="row" style="padding:14px;font-weight:300;">
		<?php
		echo JHTML::_('content.prepare',preg_replace('#<hr *id="system-readmore" */>#i','',$this->element->product_description));
		?>
	</div>
	<?php if(!empty($this->element->lesson_objective)){ ?>
		<div class="row" style="margin:0px;">
			<p style="font-family: 'Open Sans', OpenSans-Semibold, sans-serif;font-weight: 600;font-size:16px;color:#126db6;margin-bottom: 20px;">
				<?php echo JText::_('KD_LEARNING_OUTCOMES'); ?>
			</p>
			<?php
				echo JHTML::_('content.prepare',preg_replace('#<hr *id="system-readmore" */>#i','',$this->element->lesson_objective));
			?>
		</div>
	<?php } ?>
	<?php if(!empty($this->element->target_audience)){ ?>
		<div class="row" style="margin:0px;padding-bottom:20px;">
			<p style="font-family: 'Open Sans', OpenSans-Semibold, sans-serif;font-weight: 600;font-size:16px;color:#126db6;margin-bottom: 20px;padding-top:20px;">
				<?php echo JText::_('KD_TARGET_AUDIENCE'); ?>
			</p>
			<?php
				echo JHTML::_('content.prepare',preg_replace('#<hr *id="system-readmore" */>#i','',$this->element->target_audience));
			?>
		</div>
	<?php } ?>
	<?php
		if($this->element->hide_price_addtocart){
			if(!empty($this->element->parent_bundle_id)){
				echo "This course will be available in the following programme(s)<br/>";
				$parentBundleList=explode(',',$this->element->parent_bundle_id);
				foreach ($parentBundleList as $key => $value) {
					$database =& JFactory::getDBO();
					$query = 'SELECT * from '.hikashop_table('product')." WHERE product_code='".$value."' ";
					$database->setQuery($query);
					$parentBundle=$database->loadObject();
					$link = hikashop_contentLink('product&task=show&cid='.$parentBundle->product_id,$parentBundle);
					echo "<a href='".$link."'>".$parentBundle->product_name."</a><br/>";
				}
			}
		}
	?>
	<?php
			if(!$this->element->hide_price_addtocart){
				?>
	<div clas="row">
		<div id="hikashop_product_right_part" class="hikashop_product_right_part <?php echo HK_GRID_COL_6; ?>">
	<?php
	if(!empty($this->element->extraData->rightMiddle))
		echo implode("\r\n",$this->element->extraData->rightMiddle);
	?>
	<?php
		$this->setLayout('show_block_dimensions');
		echo $this->loadTemplate();
	?><br />
	<?php
	if($this->params->get('characteristic_display') != 'list') {
		$this->setLayout('show_block_characteristic');
		echo $this->loadTemplate();
		?>
		<br />
	<?php
	}

	$form = ',0';
	if (!$this->config->get('ajax_add_to_cart', 1)) {
		$form = ',\'hikashop_product_form\'';
	}
	if (hikashop_level(1) && !empty ($this->element->options)) {
	?>
		<div id="hikashop_product_options" class="hikashop_product_options">
			<?php
			$this->setLayout('option');
			echo $this->loadTemplate();
			?>
		</div>
		<br />
		<?php
		$form = ',\'hikashop_product_form\'';
		if ($this->config->get('redirect_url_after_add_cart', 'stay_if_cart') == 'ask_user') {
		?>
			<input type="hidden" name="popup" value="1"/>
		<?php
		}
	}
	if (!$this->params->get('catalogue') && ($this->config->get('display_add_to_cart_for_free_products') || ($this->config->get('display_add_to_wishlist_for_free_products', 1) && hikashop_level(1) && $this->params->get('add_to_wishlist') && $config->get('enable_wishlist', 1)) || !empty($this->element->prices))) {
		if (!empty ($this->itemFields)) {
			$form = ',\'hikashop_product_form\'';
			if ($this->config->get('redirect_url_after_add_cart', 'stay_if_cart') == 'ask_user') {
			?>
				<input type="hidden" name="popup" value="1"/>
			<?php
			}
			$this->setLayout('show_block_custom_item');
			echo $this->loadTemplate();
		}
	}
	$this->formName = $form;
	if($this->params->get('show_price')){ ?>
		<span id="hikashop_product_price_with_options_main" class="hikashop_product_price_with_options_main">
		</span>
	<?php }
	if(empty ($this->element->characteristics) || $this->params->get('characteristic_display')!='list'){ ?>
		<div id="hikashop_product_quantity_main" class="hikashop_product_quantity_main">
			<?php
			$this->row = & $this->element;
			$this->ajax = 'if(hikashopCheckChangeForm(\'item\',\'hikashop_product_form\')){ return hikashopModifyQuantity(\'' . $this->row->product_id . '\',field,1' . $form . ',\'cart\'); } else { return false; }';
			$this->setLayout('quantity');
			echo $this->loadTemplate();
			?>
		</div>
	<?php } ?>
	<div id="hikashop_product_contact_main" class="hikashop_product_contact_main">
		<?php
		$contact = $this->config->get('product_contact',0);
		if (hikashop_level(1) && ($contact == 2 || ($contact == 1 && !empty ($this->element->product_contact)))) {
			$empty = '';
			$params = new HikaParameter($empty);
			global $Itemid;
			$url_itemid='';
			if(!empty($Itemid)){
				$url_itemid='&Itemid='.$Itemid;
			}
			echo $this->cart->displayButton(JText :: _('CONTACT_US_FOR_INFO'), 'contact_us', $params, hikashop_completeLink('product&task=contact&cid=' . $this->element->product_id.$url_itemid), 'window.location=\'' . hikashop_completeLink('product&task=contact&cid=' . $this->element->product_id.$url_itemid) . '\';return false;');
		}
		?>
	</div>
	<?php
	if(!empty($this->fields)){
		$this->setLayout('show_block_custom_main');
		echo $this->loadTemplate();
	}

	if(HIKASHOP_J30) {
		$this->setLayout('show_block_tags');
		echo $this->loadTemplate();
	}

	?>
	<span id="hikashop_product_id_main" class="hikashop_product_id_main">
		<input type="hidden" name="product_id" value="<?php echo $this->element->product_id; ?>" />
	</span>
	<?php
	if(!empty($this->element->extraData->rightEnd))
		echo implode("\r\n",$this->element->extraData->rightEnd);
	?>
</div>
	</div>
	<?php
}
	?>
	<div class="row">
		<div class="hikashop_product_cart">
		<?php
			// showing minicart_position ( Module ) in Product Detail Screen
			jimport('joomla.application.module.helper');
				$modules = JModuleHelper::getModules('minicart_position');
				foreach($modules as $module)
				{
					echo JModuleHelper::renderModule($module);
				}
		?>
	</div>
	</div>
</div>
