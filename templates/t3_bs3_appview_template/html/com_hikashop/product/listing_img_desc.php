<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.1
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
$height = $this->newSizes->height;
$width = $this->newSizes->width;
$link = hikashop_contentLink('product&task=show&cid='.$this->row->product_id.'&name='.$this->row->alias.$this->itemid.$this->category_pathway,$this->row);
if(strpos($this->row->product_code,'bundle_')!==false || strpos($this->row->product_code,'prog_')!==false ){
	$bkColor="#C5C7C9";
}else{
	$bkColor="#FFFFFF";
}
if(!empty($this->row->extraData->top)) { echo implode("\r\n",$this->row->extraData->top); }
?>
<div class="hika_listing_img_desc" style="font-family:'Open Sans';">
	<a href="<?php echo $link;?>" style="display:block;text-decoration:none;color:black;background-color:<?php echo $bkColor; ?>">
		
		<?php

					if(!empty($this->row->file_path)){
						$img = $this->image->getThumbnail(@$this->row->file_path, array('width' =>"400px" , 'height' => "300px"), null);
						$file_path=str_replace('\\','/',$img->path);
						$image_path=$this->image->uploadFolder_url.$file_path;
					}else{
						$image_path="http://placehold.it/350x150";
					}

				?>
			<div class="row" style="height:225px;background-image:url('<?php echo $image_path; ?>');background-repeat:no-repeat;background-size:100% 100%;background-position:center;">
			
			</div>
		<div class="row"  style="margin:0px;margin-top:15px;">
			<div class="col-xs-8" style="color:#126DB6;font-weight:700;">
				<!-- PRODUCT NAME & LINK -->
					<?php if($this->params->get('link_to_product_page',1)){ ?>

					<?php } ?>
					<?php
							echo $this->row->product_name;
					?>
					<?php if($this->params->get('link_to_product_page',1)){ ?>

					<?php } ?>
					<!-- PRODUCT NAME & LINK -->
			</div>
			<div class="col-xs-4">
				<div class="row" style="margin-right:15px;text-align:right;">
					<!-- PRICE -->
					<?php
						if($this->params->get('show_price','-1')=='-1'){
							$config =& hikashop_config();
							$this->params->set('show_price',$config->get('show_price'));
						}
						if($this->params->get('show_price')){
							$this->setLayout('listing_price');
							echo $this->loadTemplate();
						}
					?>
					<!-- PRICE -->
				</div>
				<div class="row" style="margin-right:15px;text-align:right !important;">
					<!-- ESTIMATED COURSE DURATION -->
					<?php
						if(!empty($this->row->estimated_duration)){
							$est_time=(double)$this->row->estimated_duration;
							$suffix="h";
							if($est_time <= 1)
								$suffix="h";
								echo $this->row->estimated_duration.$suffix;
						}
					?>
					<!-- ESTIMATED COURSE DURATION -->
				</div>
			</div>
		</div>
		<div class="row"  style="margin:0px;margin-top:10px;margin-bottom:10px;">
			<div class="col-md-1" style="margin-bottom:15px;max-height:60px;overflow:hidden;word-wrap:break-word;text-overflow:ellipsis;-o-text-overflow:ellipsis;-webkit-line-clamp:3;display:-webkit-box;-webkit-box-orient:vertical;">
				<!-- PRODUCT DESCRIPTION -->
					<?php
					$description_text=strip_tags(JHTML::_('content.prepare',preg_replace('#<hr *id="system-readmore" */>#i','',$this->row->product_description)));
								if (str_word_count($description_text, 0) > 20) {
										$words = str_word_count($description_text,2);
										$pos = array_keys($words);
										$text = substr($description_text, 0, $pos[20]) . '...';
								}else{
										$text=$description_text;
								}
								echo strip_tags($text);
				?>
				<!-- PRODUCT DESCRIPTION -->
			</div>
		</div>
	</a>
</div>
<?php if(!empty($this->row->extraData->bottom)) { echo implode("\r\n",$this->row->extraData->bottom); } ?>
