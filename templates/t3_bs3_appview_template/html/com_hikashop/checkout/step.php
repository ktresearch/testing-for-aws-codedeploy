<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.1
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
// PA 2.0
$document = JFactory::getDocument();
$document->addStyleSheet('templates/t3_bs3_blank/css/product_hika.css');
$document->addStyleSheet('templates/t3_bs3_blank/css/hikashop_cart.css');
	$stepZero="";
	$stepOne="";
	$stepTwo="";
	$completeOne="";
	$completeTwo="";
	$completeThree="";
	if($this->step==0){
		$stepZero="active";
	}elseif($this->step==1){
		$stepOne="active";
		$completeOne="completed_step";
	}elseif($this->step==2){
		$stepTwo="active";
		$completeOne="completed_step";
		$completeTwo="completed_step";
	}
?>
<div id="hikashop_checkout_page" class="hikashop_checkout_page hikashop_checkout_page_step<?php echo $this->step; ?>">
        <?php if($this->step == 0) { ?>
        <div class="row cart-title">
		  <h3><?php echo JText::_('MY_CART'); ?></h3>
	</div>
        <?php } ?>
	<?php
	if(hikashop_level(1)){
		$open_hour = $this->config->get('store_open_hour',0);
		$close_hour = $this->config->get('store_close_hour',0);
		$open_minute = $this->config->get('store_open_minute',0);
		$close_minute = $this->config->get('store_close_minute',0);
		if($open_hour!=$close_hour || $open_minute!=$close_minute){
			function getCurrentDate($format = '%H'){
				if(version_compare(JVERSION,'1.6.0','>=')) $format = str_replace(array('%H','%M'),array('H','i'),$format);
				return (int)JHTML::_('date',time()- date('Z'),$format,null);
			}
			$current_hour = hikashop_getDate(time(),'%H');
			$current_minute = hikashop_getDate(time(),'%M');
			$closed=false;
			if($open_hour<$close_hour || ($open_hour==$close_hour && $open_minute<$close_minute)){
				if($current_hour<$open_hour || ($current_hour==$open_hour && $current_minute<$open_minute)){
					$closed=true;
				}
				if($close_hour<$current_hour || ($current_hour==$close_hour && $close_minute<$current_minute)){
					$closed=true;

				}
			}else{
				$closed=true;

				if($current_hour<$close_hour || ($current_hour==$close_hour && $current_minute<$close_minute)){
					$closed=false;
				}
				if($open_hour<$current_hour || ($current_hour==$open_hour && $open_minute<$current_minute)){
					$closed=false;

				}
			}
			if($closed){
				$app=& JFactory::getApplication();
				$app->enqueueMessage(JText::sprintf('THE_STORE_IS_ONLY_OPEN_FROM_X_TO_X',$open_hour.':'.sprintf('%02d', $open_minute),$close_hour.':'.sprintf('%02d', $close_minute)));
				echo '</div>';
				return;
			}
		}
	}

	global $Itemid;
	$checkout_itemid = $this->config->get('checkout_itemid');
	if(!empty($checkout_itemid )){
		$Itemid = $checkout_itemid ;
	}
	$url_itemid='';
	if(!empty($Itemid)){
		$url_itemid='&Itemid='.$Itemid;
	}

	if(empty($this->noform)){
		?>
		<form action="<?php echo hikashop_completeLink('checkout&task=step&step='.($this->step+1).$url_itemid); ?>" method="post" name="hikashop_checkout_form" enctype="multipart/form-data" onsubmit="if('function' == typeof(hikashopSubmitForm)) { hikashopSubmitForm('hikashop_checkout_form'); return false; } else { return true; }">
		<?php
	}
	$dispatcher = JDispatcher::getInstance();
	$this->nextButton = true;
	foreach($this->layouts as $layout) {
		$layout = trim($layout);
		if($layout == 'end') {
			$this->continueShopping = '';
		}
		if(substr($layout, 0, 4) != 'plg.') {
			$this->setLayout($layout);
			echo $this->loadTemplate();
		} else {
			$html = '';
			$dispatcher->trigger('onCheckoutStepDisplay', array($layout, &$html, &$this));
			if(!empty($html)) {
				echo $html;
			}
		}
	}
	if(empty($this->noform)){
		?>
		<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>"/>
		<input type="hidden" name="option" value="com_hikashop"/>
		<input type="hidden" name="ctrl" value="checkout"/>
		<input type="hidden" name="task" value="step"/>
		<input type="hidden" name="previous" value="<?php echo $this->step; ?>"/>
		<input type="hidden" name="step" value="<?php echo $this->step+1; ?>"/>
		<input type="hidden" id="hikashop_validate" name="validate" value="0"/>
		<?php echo JHTML::_( 'form.token' ); ?>
		<input type="hidden" name="unique_id" value="[<?php echo md5(uniqid())?>]"/>
		<br style="clear:both"/>
		<div class="row cart-button" style="margin-left:14px;margin-right:14px;">
		<?php
			if($this->step==1){
			echo $this->cart->displayButton(JText::_('KD_CHECKOUT_PREV_BUTTON'),'prev',$this->params, hikashop_completeLink('checkout&task=step&step=0'),'','id="hikashop_checkout_prev_button"');
		}elseif ($this->step==2) {
			echo $this->cart->displayButton(JText::_('KD_CHECKOUT_PREV_BUTTON'),'prev',$this->params, hikashop_completeLink('checkout&task=step&step=1'),'','id="hikashop_checkout_prev_button"');
		}
		if($this->nextButton)
		{
			if($this->step ==0){
				$checkout_next_button=JText::_('KD_CHECKOUT_STEP_CHECKOUT');
			}else
			
				$checkout_next_button=JText::_('KD_CHECKOUT_CONFIRM_BUTTON');
				/*
			if($this->step == (count($this->steps) - 2)) {
				$checkout_next_button = JText::_('CHECKOUT_BUTTON_FINISH');
				if($checkout_next_button == 'CHECKOUT_BUTTON_FINISH')
					$checkout_next_button = JText::_('NEXT');
			} else
				$checkout_next_button = JText::_('NEXT');
				*/
                        if($this->step ==0){        
                            if(strpos($this->continueShopping,'Itemid')===false){
                                    if(strpos($this->continueShopping,'index.php?')!==false){
                                            $this->continueShopping.=$url_itemid;
                                    }
                            }
                            if(!preg_match('#^https?://#',$this->continueShopping)) $this->continueShopping = JURI::base().ltrim($this->continueShopping,'/');
                            echo $this->cart->displayButton(JText::_('CONTINUE_SHOPPING'),'continue_shopping',$this->params,JRoute::_($this->continueShopping),'window.location=\''.JRoute::_('/featured').'\';return false;','id="hikashop_checkout_shopping_button"');
                        }
                        
			echo $this->cart->displayButton($checkout_next_button,'next',$this->params, hikashop_completeLink('checkout&task=step&step='.$this->step+1),'if(hikashopCheckChangeForm(\'order\',\'hikashop_checkout_form\')){ if(hikashopCheckMethods()){ document.getElementById(\'hikashop_validate\').value=1; this.disabled = true; document.forms[\'hikashop_checkout_form\'].submit();}} return false;','id="hikashop_checkout_next_button"');
			$button = $this->config->get('button_style','normal');
			 	if ($button=='css')
					echo '<input type="submit" style="position: absolute; left: -9999px; width: 1px; height: 1px;"/></input>';
                        
		}
		?>
	</div>
		</form>
		<?php
		/*if($this->continueShopping){
			if(strpos($this->continueShopping,'Itemid')===false){
				if(strpos($this->continueShopping,'index.php?')!==false){
					$this->continueShopping.=$url_itemid;
				}
			}
			if(!preg_match('#^https?://#',$this->continueShopping)) $this->continueShopping = JURI::base().ltrim($this->continueShopping,'/');
			echo $this->cart->displayButton(JText::_('CONTINUE_SHOPPING'),'continue_shopping',$this->params,JRoute::_($this->continueShopping),'window.location=\''.JRoute::_($this->continueShopping).'\';return false;','id="hikashop_checkout_shopping_button"');
		}*/
	}
	?>
</div>
<div class="clear_both"></div>
<?php

if(JRequest::getWord('tmpl','')=='component'){
	exit;
}
