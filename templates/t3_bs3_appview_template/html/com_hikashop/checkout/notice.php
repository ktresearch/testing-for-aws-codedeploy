<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.1
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
$doc = JFactory::getDocument();
// PA 2.0
$doc->addStyleSheet('templates/t3_bs3_blank/css/product_hika_mobile.css');
$doc->setMetaData( 'robots', 'noindex' );
$app = JFactory::getApplication();
$wishlist_id = $app->getUserState( HIKASHOP_COMPONENT.'.wishlist_id','0');
$cart_type = JRequest::getVar('cart_type','');
if(empty($cart_type))
	$cart_type = $app->getUserState( HIKASHOP_COMPONENT.'.popup_cart_type','cart');
$app->setUserState( HIKASHOP_COMPONENT.'.popup_cart_type','cart');
// language /vn or /en
if(strcmp((JFactory::getLanguage()->getTag()),'vi-VN')==0) {
    $langcurrent = 'vn/';
}

?>
<?php
$user = & JFactory::getUser();
if($user->id==0){
?>
    	<span class="row notice-title"><?php echo JText::_('KD_SUCCESSFULLY_ADDED_TO_CART'); ?></span>
      <a href="/index.php?option=com_community" class="row notice-button" target="_top"><?php echo JText::_('KD_LOGIN'); ?><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
      <a href="/index.php?option=com_community&view=register" class="row notice-button" target="_top"><?php echo JText::_('KD_REGISTER'); ?><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
  <?php
}else{
  ?>
    	<span class="row notice-title"><?php echo JText::_('KD_SUCCESSFULLY_ADDED_TO_CART'); ?></span>
      <a href="/index.php?option=com_hikashop&ctrl=checkout&task=step&step=0" class="row notice-button" target="_top"><?php echo JText::_('KD_VIEW_SHOPPING_CART'); ?><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
      <a href="<?php echo JURI::root().''.$langcurrent.'featured';?>" class="row notice-button" target="_top"><?php echo JText::_('KD_CONTINUE_SHOPPING'); ?><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
  <?php
}
  ?>
