<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.
?>
<div class="category_mainmenu">
<?php
	foreach ($list as $i => $item) {
		if($item->parent_id==1){
			echo "<div class='category_menu' onclick='' style='cursor:pointer;height:50px;width:33.33333333%;float:left;text-align:center;'><span class='category_menu_title' data-title='".$item->title."' data-linktoredirect='".$item->flink."'>".$item->title."</span><ul class='nav nav-pills nav-stacked category_submenu'>";
		foreach ($list as $ii => $itemInner) {
			if($itemInner->parent_id==$item->id){
				    echo "<li class='dropdown'><span style='cursor:pointer;' class='dropdown-toggle' data-toggle='dropdown' data-title='".$itemInner->title."' data-linktoredirect='".$itemInner->flink."'>".$itemInner->title." <i class='fa fa-plus-circle submenu_circle_icon'></i></span><ul class='dropdown-menu'>";
						foreach ($list as $iii => $itemSecondInner) {
							if($itemSecondInner->parent_id==$itemInner->id){
									echo "<li><a href='".$itemSecondInner->flink."'>".$itemSecondInner->title."</a><hr class='submenu_line'/></li>";
							}
						}
						echo "</ul><hr class='separator_line'/></li>";
			}
		}
					echo "</ul></div>";
	}
	}
 ?>
 </div>

 <script type="text/javascript">
	jQuery(document).on("click",".category_menu",function(e){
			jQuery('.category_menu').find('.categorymenu_caret').remove();
		jQuery('.category_submenu .dropdown').find('.submenu_circle_icon').removeClass('fa-minus-circle');
		jQuery('.category_submenu .dropdown').find('.submenu_circle_icon').addClass('fa-plus-circle');
			jQuery('.category_submenu .dropdown').find('.fa-caret-up').remove();
		//jQuery('.category_submenu',this).toggle();
		if(jQuery(this).hasClass('active')){
			jQuery(this).removeClass('active');
			jQuery(this).find('.categorymenu_caret').remove();
			jQuery('.category_submenu',this).css('display','none');
		}else{
			jQuery('.category_menu').removeClass('active');
			jQuery(this).addClass('active');
			jQuery(this).append('<div class="arrow-up categorymenu_caret"></div>');
			jQuery('.category_submenu').css('display','none');
			jQuery('.category_submenu',this).css('display','block');
		}

	});
	jQuery(".category_menu").each(function(index){
		if(jQuery(this).find('.category_submenu li').length==0){
			var child_menu=jQuery(this).find('.category_menu_title').children();
			jQuery(this).find('.category_menu_title').html("<a class='category_menu_title_link' href='"+jQuery(this).find('.category_menu_title').data('linktoredirect')+"'>"+jQuery(this).find('.category_menu_title').data('title')+"</a>").append(child_menu);
		}
	});
	jQuery( ".category_submenu .dropdown" ).each(function( index ) {
		if(jQuery( this ).find('.dropdown-menu li').length==0){
			jQuery( this ).find('.submenu_circle_icon').css('display','none');
			jQuery(this).find('.dropdown-menu').remove();
			var children_submenu=jQuery(this).find('.dropdown-toggle').children();
			jQuery(this).find('.dropdown-toggle').html("<a href='"+jQuery(this).find('.dropdown-toggle').data('linktoredirect')+"'>"+jQuery(this).find('.dropdown-toggle').data('title')+"</a>").append(children_submenu);

		}
	});
	jQuery(".category_submenu .dropdown").on("click",function(e){
			jQuery('.category_submenu .dropdown').find('.fa-caret-up').remove();
		jQuery('.category_submenu .dropdown').find('.submenu_circle_icon').removeClass('fa-minus-circle');
		jQuery('.category_submenu .dropdown').find('.submenu_circle_icon').addClass('fa-plus-circle');
		if(!jQuery(this).hasClass('open')){
			jQuery(this).find('.submenu_circle_icon').removeClass('fa-plus-circle');
			jQuery(this).find('.submenu_circle_icon').addClass('fa-minus-circle');
				if(jQuery( this ).find('.dropdown-menu li').length!=0){
			jQuery(this).append('<i class="fa fa-caret-up submenu_caret" aria-hidden="true"></i>');
		}
		}else{
			jQuery(this).find('.submenu_circle_icon').addClass('fa-plus-circle');
			jQuery(this).find('.submenu_circle_icon').removeClass('fa-minus-circle');
			jQuery(this).find('.fa-caret-up').remove();
		}
	});
	jQuery(document).ready(function(){
		var currentPage=window.location.pathname.split('/');
		console.log(currentPage);
		if(jQuery.inArray('featured', currentPage) > -1){
			jQuery(jQuery('.category_menu')[0]).append('<div class="arrow-up categorymenu_caret"></div>');
			jQuery(jQuery('.category_menu')[0]).find('.category_menu_title').css('color','white');
			jQuery(jQuery('.category_menu')[0]).find('.category_menu_title_link').css('color','white');
		}else if(jQuery.inArray('providers', currentPage) > -1){
			jQuery(jQuery('.category_menu')[1]).append('<div class="arrow-up categorymenu_caret"></div>');
			jQuery(jQuery('.category_menu')[1]).find('.category_menu_title').css('color','white');
			jQuery(jQuery('.category_menu')[1]).find('.category_menu_title_link').css('color','white');
		}else if(jQuery.inArray('categories', currentPage) > -1){
			jQuery(jQuery('.category_menu')[2]).append('<div class="arrow-up categorymenu_caret"></div>');
			jQuery(jQuery('.category_menu')[2]).find('.category_menu_title').css('color','white');
		}

		jQuery("nav#t3-mainnav.navbar-fixed-bottom .t3-megamenu a[href='/learning'] img").attr("src","/images/MyLearningIcon.png");
		jQuery("nav#t3-mainnav.navbar-fixed-bottom .t3-megamenu a[href='/learning'] .image-title").css({
			"color":"#126DB6"
		});
	});
 </script>
