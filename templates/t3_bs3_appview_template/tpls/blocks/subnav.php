<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<!-- SUB NAVIGATION -->
        <div class="t3-submenu">
                    <?php 
                    // $app      = JFactory::getApplication();
                    // $menu     = $app->getMenu();
                    // $items = $menu->getItems('menutype','submenu', false);
                    // $items = is_array($items) ? $items : array();
                    ?>
                    <!-- <ul class="subnav"> -->
                    <?php
                    //foreach($items as $item) {
                        ?>
                        <!-- <li class="sub-item"><a href="<?php //echo $item->link; ?>"><img src="<?php //echo $item->params->get('menu_image', ''); ?>" /><?php //echo $item->title; ?></a></li> -->
                        <?php
                    //}
                    
                    ?>
                    <!-- </ul> -->
                    
                    <jdoc:include type="megamenu" name="submenu" />

        </div>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                var oldsrc = jQuery('#t3-mainnav.navbar-fixed-bottom  .t3-megamenu > ul > li.active > a > img').attr('src');
                if (typeof (oldsrc) != 'undefined') {
                    var newsrc = oldsrc.replace(/_555555/g, '');
                    jQuery('#t3-mainnav.navbar-fixed-bottom  .t3-megamenu > ul > li.active > a > img').attr('src',  newsrc );
                }

                if ( jQuery('html').hasClass('com_joomdle') && ( jQuery('html').hasClass('view-mylearning') 
                    || jQuery('html').hasClass('view-course') 
                    || jQuery('html').hasClass('view-wrapper') 
                    || jQuery('html').hasClass('view-courseprogram')
                    || jQuery('html').hasClass('view-progress') ) ) {
                    var oldsrc2 = jQuery(' #t3-mainnav.navbar-fixed-bottom .t3-megamenu > ul > li:nth-of-type(2) > a > img').attr('src');
                    if (typeof (oldsrc2) != 'undefined') {
                        var newsrc2 = oldsrc2.replace(/_555555/g, '');
                        jQuery(' #t3-mainnav.navbar-fixed-bottom .t3-megamenu > ul > li:nth-of-type(2) > a > img').attr('src',  newsrc2 );
                    }
                }

                var str1 = window.location.href;
                var str2 = 'fromcircle=1';
                if ( ( jQuery('html').hasClass('com_community') && ( jQuery('html').hasClass('view-groups') ) ) || (str1.indexOf(str2) != -1) ) {
                    var oldsrc2 = jQuery(' #t3-mainnav.navbar-fixed-bottom .t3-megamenu > ul > li:first-of-type > a > img').attr('src');
                    if (typeof (oldsrc2) != 'undefined') {
                        var newsrc2 = oldsrc2.replace(/_555555/g, '');
                        jQuery(' #t3-mainnav.navbar-fixed-bottom .t3-megamenu > ul > li:first-of-type > a > img').attr('src',  newsrc2 );
                        jQuery(' #t3-mainnav.navbar-fixed-bottom .t3-megamenu > ul > li:nth-of-type(2) > a').css({'color':'#126DB6'});
                    }
                }
                if ( jQuery('html').hasClass('com_community') && ( jQuery('html').hasClass('view-friends') ) ) {
                    var oldsrc2 = jQuery(' #t3-mainnav.navbar-fixed-bottom .t3-megamenu > ul > li:nth-of-type(3) > a > img').attr('src');
                    if (typeof (oldsrc2) != 'undefined') {
                        var newsrc2 = oldsrc2.replace(/_555555/g, '');
                        jQuery(' #t3-mainnav.navbar-fixed-bottom .t3-megamenu > ul > li:nth-of-type(3) > a > img').attr('src',  newsrc2 );
                    }
                }
                if ( jQuery('html').hasClass('com_community') && ( jQuery('html').hasClass('view-events') ) ) {
                    var oldsrc2 = jQuery(' #t3-mainnav.navbar-fixed-bottom .t3-megamenu > ul > li:nth-of-type(4) > a > img').attr('src');
                    if (typeof (oldsrc2) != 'undefined') {
                        var newsrc2 = oldsrc2.replace(/_555555/g, '');
                        jQuery(' #t3-mainnav.navbar-fixed-bottom .t3-megamenu > ul > li:nth-of-type(4) > a > img').attr('src',  newsrc2 );
                    }
                }
            });
        </script>
<!-- //SUB NAVIGATION -->
