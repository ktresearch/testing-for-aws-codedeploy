<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
require_once(JPATH_ROOT.'/components/com_community/libraries/core.php');
$user = JFactory::getUser();
$user2 = CFactory::getUser();

$notifModel = CFactory::getModel('notification');
$myParams             = $user2->getParams();

$session                = JFactory::getSession();
$first_time_login = $session->get('first_time');
$defaulNotif = $notifModel->getDefaultNotification();
$defaultNotification = 0;
if($first_time_login) {
    if($defaulNotif) { 
        $defaultNotification = 1;
    }
}
$class_defalt = '';
if($first_time_login) {
    $class_defalt = 'not-read';
}

$newNotificationCount = $notifModel->getNotificationCount($user2->id,'0', $myParams->get('lastnotificationlist',''));

$toolbar = CToolbarLibrary::getInstance();
$newEventInviteCount = $toolbar->getTotalNotifications('events');
$newFriendInviteCount = $toolbar->getTotalNotifications('friends');
$newGroupInviteCount = $toolbar->getTotalNotifications('groups');
$newMessageCount = $toolbar->getTotalNotifications('inbox');

$newNotificationCount += $defaultNotification;
//$newNotificationCount += $newEventInviteCount;
//$newNotificationCount += $newFriendInviteCount;
//$newNotificationCount += $newGroupInviteCount;
//$newNotificationCount += $newMessageCount;

$ntf_class = '';
if ($newNotificationCount != 0) {
    $ntf_class = 'have-ntf';
}

if ($newNotificationCount != 0) {
    JFactory::getDocument()->setTitle('('.$newNotificationCount.') '.JFactory::getDocument()->getTitle());
}
$app = JFactory::getApplication();
$menu = $app->getMenu();
$ishomepage = false;
if ($menu->getActive() == $menu->getDefault( 'en-GB' )) {
	$ishomepage = true;
}
elseif ($menu->getActive() == $menu->getDefault( 'vi-VN' )) {
	$ishomepage = true;
} elseif($menu->getActive() == $menu->getDefault( 'zh-CN' )) {
    $ishomepage = true;
}
$isGuest = false;
if (JFactory::getUser()->guest == 1) {
    $isGuest = true;
}

$option = JFactory::getApplication()->input->getCmd('option');
if($option != 'com_community'){ 
    $document = JFactory::getDocument();
    $document->addStyleSheet('./components/com_community/templates/jomsocial/assets/css/style.css');
}
?>

<!-- MAIN NAVIGATION -->
<nav id="t3-mainnav" class="wrap navbar navbar-default t3-mainnav navbar-fixed-top ">
	<div class="container">
        
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <?php if($ishomepage || $isGuest) { ?>
                    <a class="navbar-brand" href="index.php">
                        <img alt="Parenthexis" src="templates/t3_bs3_blank/images/PARENTHEXIS_LOGO_WHITE.png">
                    </a>
                <?php } else { ?>    
                    <a class="navbar-back" href="javascript:void(0);" onclick="goBack();">
                        <img alt="Back" src="templates/t3_bs3_blank/images/ArrowIcon.png">
                    </a>
                <?php } ?>

                <div class="navbar-avatar">
                    <?php if (!is_null(JFactory::getUser()->username)) {?>
                        <a href="<?php echo JURI::root( true ) .'component/community/profile'; ?>">
                            <img src="<?php echo $user2->getAvatar() . '?_=' . time(); ?>" alt="<?php echo $user2->getDisplayName() ; ?>">
                         </a>   
                    <?php } else { ?>
                        <img src="templates/t3_bs3_blank/images/icon-notification.png" style="display: none;" />
                    <?php } ?>
               </div>

                <?php if ($this->getParam('navigation_collapse_enable', 1) && $this->getParam('responsive', 1)) : ?>
                        <?php // $this->addScript(T3_URL.'/js/nav-collapse.js'); ?>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".t3-navbar-collapse">
                                <i class="fa fa-bars"></i>
                        </button>
                <?php endif ?>

                <?php if ($this->getParam('addon_offcanvas_enable')) : ?>
                        <?php $this->loadBlock ('off-canvas') ?>
                <?php endif ?>

                <?php if (!is_null($user->username)) { ?>
                    <a class="navbar-notification <?php echo $ntf_class; ?>" data-placement='bottom' href="javascript:"> 
                        <?php if ($newNotificationCount != 0) { ?>
                            <img class="ntf-have" src="templates/t3_bs3_blank/images/NotificationIcon.png" alt="<?php echo $user2->getDisplayName() ; ?>">
                            <span class="notification-count" style="<?php echo ($newNotificationCount  > 9) ? 'font-size: 8px;': '';?>"><?php echo $newNotificationCount;?></span>
                        <?php } else { ?>
                                   <img src="templates/t3_bs3_blank/images/NoNotification.png" alt="<?php echo $user2->getDisplayName() ; ?>">
                            <?php }?>
                    <?php //} else { ?>
                            <!-- <img src="templates/t3_bs3_blank/images/NoNotification.png" alt="<?php echo $user2->getDisplayName() ; ?>"> -->
                    </a>
                <?php } ?>
                
                <a class="navbar-faqs <?php echo (is_null($user->username)) ? 'hideNotifIcon':''; ?>" href="<?php echo JURI::root( true ) . 'component/fsf/faq';?>">
                    <img src="images/FAQ.png" />
                </a>

            </div>

            <?php if ($this->getParam('navigation_collapse_enable')) : ?>
                <div class="t3-navbar-collapse navbar-collapse collapse">
                    <jdoc:include type="<?php echo $this->getParam('navigation_type', 'megamenu') ?>" name="<?php echo $this->getParam('mm_type', 'mainmenu') ?>" />
                </div>
            <?php endif ?>

		
                <!-- Load sub menu -->
                <?php // $this->loadBlock('subnav') ?>
                
              <div id="popover_content_wrapper" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static">
                <div class="modal-dialog modal-sm">
                    <?php 
//                    if($newNotificationCount > 1) {
                    $document = JFactory::getDocument();
                    $renderer = $document->loadRenderer('module');
                    $module = JModuleHelper::getModule('mod_notifications');
                    echo $renderer->render($module);
                    ?>
                    <?php // } ?>
                    <div class="joms-stream__container joms-stream--discussion notifications default <?php echo $class_defalt; ?>">
                        <a href="<?php echo JURI::base(); ?>component/content/article/?id=<?php echo $defaulNotif['id']; ?>"><?php echo $defaulNotif['title'].', '.$user2->getDisplayName(); ?> !</a> 
                    </div>
                </div>
             </div>  

	</div>
</nav>
<script src="./plugins/system/t3/base-bs3/bootstrap/js/bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#t3-mainnav .navbar-menu-icon').click(function() {
            jQuery(this).toggleClass('show');
            jQuery('.t3-wrapper .right-column').toggleClass('show-secondmenu');
            jQuery('.t3-wrapper .tab.left-column').toggleClass('show-secondmenu');
            jQuery('.navbar-fixed-top').toggleClass('show-secondmenu');
            jQuery('.course-menu, .changePosition').toggleClass('show-secondmenu');
            jQuery('.joomdle-overview-header').toggleClass('show-secondmenu');
            jQuery('#community-wrap .jomsocial .joms-landing > .btn-group.btn-group-justified, .com_community.view-groups.task-adddiscussion .discussion-buttons, .com_community.view-groups.task-viewdiscussion .joms-page > .joms-comment__reply').toggleClass('show-secondmenu');
            jQuery('.navbar-fixed-top .t3-megamenu > ul > li').removeClass('active');
            jQuery('.navbar-fixed-top .t3-megamenu').toggleClass('visible');
        });
        jQuery('.navbar-fixed-top .t3-megamenu .logout-link').parent().css({"bottom": "20px", "position": "fixed"});
        // jQuery('.t3-mainbody,.wrap.t3-sl').click(function() {
        //         jQuery('#t3-mainnav .navbar-menu-icon').removeClass('show');
        //         jQuery('.t3-wrapper .right-column').removeClass('show-secondmenu');
        //         jQuery('.t3-wrapper .tab.left-column').removeClass('show-secondmenu');
        //         jQuery('.navbar-fixed-top').removeClass('show-secondmenu');
        //         jQuery('.course-menu, .changePosition').removeClass('show-secondmenu');
        //         jQuery('#community-wrap .jomsocial .joms-landing > .btn-group.btn-group-justified, .com_community.view-groups.task-adddiscussion .discussion-buttons, .com_community.view-groups.task-viewdiscussion .joms-page > .joms-comment__reply').removeClass('show-secondmenu');
        //         jQuery('.navbar-fixed-top .t3-megamenu > ul > li').removeClass('active');
        //         jQuery('.navbar-fixed-top .t3-megamenu').removeClass('visible');
        // });
        jQuery(document).click(function(e) {
            if (!jQuery(e.target).hasClass('navbar-menu-icon')) {
                jQuery('#t3-mainnav .navbar-menu-icon').removeClass('show');
                jQuery('.t3-wrapper .right-column').removeClass('show-secondmenu');
                jQuery('.t3-wrapper .tab.left-column').removeClass('show-secondmenu');
                jQuery('.navbar-fixed-top').removeClass('show-secondmenu');
                jQuery('.course-menu, .changePosition').removeClass('show-secondmenu');
                jQuery('.joomdle-overview-header').removeClass('show-secondmenu');
                jQuery('#community-wrap .jomsocial .joms-landing > .btn-group.btn-group-justified, .com_community.view-groups.task-adddiscussion .discussion-buttons, .com_community.view-groups.task-viewdiscussion .joms-page > .joms-comment__reply').removeClass('show-secondmenu');
            jQuery('.navbar-fixed-top .t3-megamenu > ul > li').removeClass('active');
                jQuery('.navbar-fixed-top .t3-megamenu').removeClass('visible');
            }    
        });
        // notification
        jQuery('.navbar-notification').popover({
            html : true,
            content: function() {
              return jQuery('#popover_content_wrapper').html();  
            }
        });
        // end show
        jQuery('.navbar-notification').click(function (e) {
            e.stopPropagation();
        });
        jQuery(document).click(function (e) {
            if ((jQuery('.popover').has(e.target).length == 0) || jQuery(e.target).is('.close')) {
                jQuery('.navbar-notification').popover('hide');
            }
        });
        // end hide
        // update notification count
        jQuery('.navbar-notification.have-ntf').on('click', function() {
            jQuery('.notification-count').css('display', 'none');
            var title = document.title;
            var count = title.split(" ")[0];
            if ( (count.indexOf("(") != -1) && (count.indexOf(")") != -1) ) {
                var countNum = count.replace('(', '');
                countNum = countNum.replace(')', '');
                if (jQuery.isNumeric(countNum)) {
                    document.title = title.replace(count+' ', '');
                }    
            }
            
            jQuery('.ntf-have').attr("src", "/templates/t3_bs3_tablet_desktop_template/images/NoNotification.png");
            
            var user = '<?php echo $user2->id; ?>';
            jQuery.ajax({
                type: 'post',
                url: 'index.php?option=com_community&view=profile&task=ajaxReadAllNotification',
                data: {userid: user},
                success: function(data) {
                    
                }
            });
        });

    }(jQuery));
    
    function notification_list(link, div_id){
        jQuery.ajax({
            url: link,
            success: function(response){
                jQuery('#'+div_id).html(response);
            }
        });
        return '<div id="'+div_id+'"><ul class="joms-popover--toolbar-general"><li class="joms-js--loading" style="display:block"><img src="<?php echo JURI::root(true); ?>/components/com_community/assets/ajax-loader.gif" alt="loader"></li></ul></div>';
    }
    function goBack() {
        // window.history.back();
        var nav = window.navigator;
        if( this.phonegapNavigationEnabled &&
            nav &&
            nav.app &&
            nav.app.backHistory ){
            nav.app.backHistory();
        } else {
            window.history.back();
        }
    }
</script>
<!-- //MAIN NAVIGATION -->
