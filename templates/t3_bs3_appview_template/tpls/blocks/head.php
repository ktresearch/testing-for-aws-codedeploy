<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<!-- META FOR IOS & HANDHELD -->
<?php if ($this->getParam('responsive', 1)): ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<style type="text/stylesheet">
		@-webkit-viewport   { width: device-width; }
		@-moz-viewport      { width: device-width; }
		@-ms-viewport       { width: device-width; }
		@-o-viewport        { width: device-width; }
		@viewport           { width: device-width; }
	</style>
	<link rel="stylesheet" href="templates/t3_bs3_appview_template/local/css/home.css" type="text/css">
    <link rel="stylesheet" href="templates/t3_bs3_appview_template/local/css/template.css" type="text/css">
	<script type="text/javascript">
		//<![CDATA[
		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
			var msViewportStyle = document.createElement("style");
			msViewportStyle.appendChild(
				document.createTextNode("@-ms-viewport{width:auto!important}")
			);
			document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
		}
		//]]>
	</script>
 	<?php //$this->addStyleSheet(T3_TEMPLATE_URL . '/fonts/open-sans/OpenSans-Regular.ttf'); ?>
 	<link href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
<?php endif ?>
<meta name="HandheldFriendly" content="true"/>
<meta name="apple-mobile-web-app-capable" content="YES"/>
<meta http-equiv="Cache-control" content="public">
<!-- //META FOR IOS & HANDHELD -->

<?php
// SYSTEM CSS
// $this->addStyleSheet(JURI::base(true) . '/templates/system/css/system.css');
?>

<?php
// T3 BASE HEAD
$this->addHead();
?>

<?php
// CUSTOM CSS
if (is_file(T3_TEMPLATE_PATH . '/css/custom.css')) {
	$this->addStyleSheet(T3_TEMPLATE_URL . '/css/custom.css');
}
//if (!is_null(JFactory::getUser()->username)) :
//    require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/content.php');
//    $u = array();
//    $u[]['username'] = JFactory::getUser()->username;
//    $muser = JoomdleHelperContent::call_method('check_moodle_users', $u);
//    $isMoodleAdmin = ($muser[0]['admin'] == 1) ? true : false;
//    JFactory::getUser()->set('isMoodleAdmin', $isMoodleAdmin);
//endif;
?>

<!-- Le HTML5 shim and media query for IE8 support -->
<!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script type="text/javascript" src="<?php echo T3_URL ?>/js/respond.min.js"></script>
<![endif]-->

<!-- You can add Google Analytics here or use T3 Injection feature -->
