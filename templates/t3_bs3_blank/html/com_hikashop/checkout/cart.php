<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.1
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
$this->setLayout('listing_price');
$this->params->set('show_quantity_field', 0);
$comp_description = $this->params->get('comp_description');
if(empty($comp_description)){
	$this->params->set('comp_description',JText::_('CART_EMPTY'));
}
if($this->paymentType == 'no'){
	$this->nextButton = false;
}
$app = JFactory::getApplication();
?>
<div id="hikashop_checkout_cart" class="hikashop_checkout_cart">
<?php
	if(empty($this->rows)) {
		echo $this->params->get('comp_description');
	} else {
		if($this->config->get('print_cart',0)&&JRequest::getVar('tmpl','')!='component'){ ?>
			<div class="hikashop_checkout_cart_print_link">
				<a title="<?php echo JText::_('HIKA_PRINT');?>" class="modal" rel="{handler: 'iframe', size: {x: 760, y: 480}}" href="<?php echo hikashop_completeLink('checkout&task=printcart',true); ?>">
					<img src="<?php echo HIKASHOP_IMAGES; ?>print.png" alt="<?php echo JText::_('HIKA_PRINT');?>"/>
				</a>
			</div>
<?php
		}
		$total_quality=0;
		foreach ($this->rows as $i => $row) {
			if(!empty($row->cart_product_quantity)){
				$total_quality+=$row->cart_product_quantity;
			}
		}
		foreach($this->rows as $i => $row) {
			if(empty($row->cart_product_quantity)) continue;
			if(!empty($row->product_min_per_order)) {
				if($row->product_min_per_order>$row->cart_product_quantity) {
					$this->nextButton = false;
					$app->enqueueMessage(JText::sprintf('YOU_NEED_TO_ORDER_AT_LEAST_X_X',$row->product_min_per_order,$row->product_name));
				}
			}
		}
		$row_count = 4;
?>
	<br/>
	
	<?php
        foreach($this->rows as $i => $row) {
	?>
	<div class="row cart-item">

		<?php

		if (!empty($row->file_path)) {
                   $img = $this->image->getThumbnail(@$row->file_path, array('width' => "400px", 'height' => "300px"), null);
                   $file_path = str_replace('\\', '/', $img->path);
                   $image_path = $this->image->uploadFolder_url . $file_path;
                } else {
                    $image_path = "http://placehold.it/350x150";
                }
                ?>
		<div class="img-course" style="background-image:url('<?php echo $image_path; ?>');"></div>
                <div class="row cart-content">
                <div class="col-xs-8 cart-course-title">
			<span class="checkout_product_title"><?php echo $row->product_name; ?></span>
		</div>
                <div class="col-xs-2 cart-course-duration">
                    <span><?php if(!empty($row->estimated_duration)) echo $row->estimated_duration .'h'; ?></span>
                </div>
		<div class="col-xs-2 checkout_price_detail" style="padding:0px;text-align:right;">
			
			<!-- PRICE -->
			<?php
			$this->row =& $row;
			$this->unit = true;
			echo $this->loadTemplate();
			?>
			<!-- PRICE -->
		</div>
                </div>
                <div class="row course-description">
                   <span class="description"><?php echo JHtml::_('string.truncate', strip_tags($row->product_description), 100, true, $allowHtml = true); ?></span>
                </div>
                <?php
                    if ($this->step == 0) {
                ?>
                    <a class="hikashop_no_print" href="<?php echo hikashop_completeLink('product&task=updatecart&product_id=' . $row->product_id . '&quantity=0&return_url=' . urlencode(base64_encode(urldecode($this->params->get('url'))))); ?>" onclick="var qty_field = document.getElementById('hikashop_checkout_quantity_<?php echo $row->cart_product_id; ?>'); if(true){qty_field.value=0; <?php echo $input; ?> qty_field.form.submit();} return false;" title="<?php echo JText::_('HIKA_DELETE'); ?>">
                        <span class="remove-item"><?php echo JText::_('HK_CART_REMOVE'); ?></span>
                    </a>
                <?php
                }
               ?>
	</div>
	<?php
			if(count($this->rows)-1!=$i){
					echo "<hr style='background-color:#dbe7f1;'/>";
			}

		}
	 ?>
        <hr class="end-total" />
        <?php
	if($this->step==0){
	?>

	<div class="row cart-total">
		<div class="col-xs-6" style="padding:0px;"></div>
		<div class="col-xs-6 total-summary" style="text-align:right;padding-right:0px;">
                    <span class="total-text">
			<?php echo JText::_('KD_TOTAL_COST'); ?>
                    </span>
                    <span class="total-currentcy">
			<?php
				echo $this->currencyHelper->format($this->full_total->prices[0]->price_value_with_tax,$this->full_total->prices[0]->price_currency_id);
			?>                        
                    </span>
		</div>
	</div>
        <?php } ?>
	
	<?php
	if($this->step==2){
	?>
<!--	<div class="row" style="margin-left:14px;margin-right:14px;font-size:16px;font-weight:600;margin-top:20px;">
		<div class="col-xs-8" style="padding:0px;">
			<?php // echo JText::_('KD_NUMBER_OF_ITEMS'); ?>
		</div>
		<div class="col-xs-4" style="text-align:right;padding-right:0px;">
			<?php
//				echo $total_quality;
			?>
		</div>
	</div>-->

	<div class="row cart-total">
		<div class="col-xs-6" style="padding:0px;"></div>
		<div class="col-xs-6 total-summary" style="text-align:right;padding-right:0px;">
                    <span class="total-text">
			<?php echo JText::_('KD_TOTAL_COST'); ?>
                    </span>
                    <span class="total-currentcy">
			<?php
				echo $this->currencyHelper->format($this->full_total->prices[0]->price_value_with_tax,$this->full_total->prices[0]->price_currency_id);
			?>                        
                    </span>
		</div>
	</div>
	<?php
		}
		if(empty($this->disable_modifications) && $this->params->get('show_quantity')){ ?>
			<noscript>
				<input id="hikashop_checkout_cart_quantity_button" class="btn button" type="submit" name="refresh" value="<?php echo JText::_('REFRESH_CART');?>"/>
			</noscript>
		<?php }
	}
        /*
	$config =& hikashop_config();
	if(hikashop_level(1) && $config->get('checkout_convert_cart') && $config->get('enable_wishlist') && (($config->get('hide_wishlist_guest', 1) && hikashop_loadUser() != null) || !$config->get('hide_wishlist_guest', 1))){
		$this->params->set('cart_type','wishlist');
		echo $this->cart->displayButton(JText::_('CART_TO_WISHLIST'),'wishlist',$this->params,hikashop_completeLink('cart&task=convert&cart_type=cart&cart_id='.$this->full_cart->cart_id.$url_itemid),'window.location.href = \''.hikashop_completeLink('cart&task=convert&cart_type=cart&cart_id='.$this->full_cart->cart_id.$url_itemid).'\';return false;');
	}*/
?>
</div>
