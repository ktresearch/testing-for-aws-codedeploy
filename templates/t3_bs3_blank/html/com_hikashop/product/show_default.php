<?php
/**
 * @package HikaShop for Joomla!
 * @version 2.6.1
 * @author  hikashop.com
 * @copyright   (C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

$document = JFactory::getDocument();
$document->addStyleSheet('templates/t3_bs3_blank/css/product_hika_mobile.css');
$image_link='/images/com_hikashop/upload/thumbnails/100x100f/barcode.png';

$database =& JFactory::getDBO();
$query = 'SELECT * from '.hikashop_table('category')." WHERE category_name='Learning Provider' ";
$database->setQuery($query);
$categories = $database->loadObjectList();
foreach ($this->categories as $category) {
    if (!empty($categories) && $categories[0]->category_id == $category->category_parent_id){
        $database->setQuery('SELECT * FROM '.hikashop_table('file').' WHERE file_type = "category" AND file_ref_id = '.(int)$category->category_id);
        $image = $database->loadObject();
        $fileClass = hikashop_get('helper.image');
        //TODO: clean up file path using hikashop file class
        if ($image){
            $imgString = '<img src="'.$this->image->uploadFolder_url.$image->file_path.'" class="provider_thumbnail" style="object-fit:contain;"/>';
        }else{
            $imgString = " ";
        }
    }

}
$session = JFactory::getSession();
$device = $session->get('device');
$is_mobile = $device == 'mobile' ? true : false;
?>
<div class="hikashop_product_detail">
    <?php
    $dpContent = [];
    if (!$this->element->hide_price_addtocart) {
        $this->row = $this->element;
        $this->setLayout('listing_price');
        $dpContent[] = $this->loadTemplate();
    }
    if (!empty($this->element->estimated_duration)) {
        $dpContent[] = $this->element->estimated_duration.'h';
    }
    ?>

    <div class="">
        <?php
        if (!empty($this->element->images[0]->file_path)) {
            $img = $this->image->getThumbnail(@$this->element->images[0]->file_path, array('width' =>"650px" , 'height' => "488px"), null);
            $file_path = str_replace('\\','/',$img->path);
            $image_path = $this->image->uploadFolder_url.$file_path;
        } else {
            $image_path = "/courses/theme/parenthexis/pix/nocourseimg.jpg";
        }
        ?>
        <div class="hikashop_product_listing_image" style="<?php echo 'background-image:url('.$image_path.')';?>">
            <div class="productDurationPrice"><?php echo implode(' • ', $dpContent); ?></div>

            <span class="productTitleBar">
                <a class="back" href="javascript:void(0);" onclick="goBack();"><img src="<?php echo JUri::base(); ?>images/Back-button.png"></a>
                <div class="productTitle">
                    <span><?php
                        if (hikashop_getCID('product_id') != $this->element->product_id && isset ($this->element->main->product_name))
                            echo $this->element->main->product_name;
                        else
                            echo $this->element->product_name;
                        ?></span>
                </div>
            </span>
        </div>
        <div class="provider_col">
            <?php
            if (!empty($imgString)) {
                echo $imgString;
            }
            ?>
        </div>
    </div>
    <div class="product_detail_description">
        <p class="heading"><?php echo JText::_('COURSE_DESCRIPTION'); ?></p>
        <?php
        echo strip_tags( JHTML::_('content.prepare',preg_replace('#<hr *id="system-readmore" */>#i','',$this->element->product_description)) );
        ?>
    </div>

    <?php
    if (!empty($this->element->lesson_objective)) { ?>
        <div class="product_learning_outcomes">
            <p class="heading"><?php echo JText::_('LEARNING_OUTCOMES'); ?></p>
            <p><?php
                echo strip_tags( $this->element->lesson_objective );
                ?>
            </p>
        </div>
    <?php } ?>

    <?php if (!empty($this->element->target_audience)) { ?>
        <div class="product_target_audience">
            <p class="heading"><?php echo JText::_('TARGET_AUDIENCE'); ?></p>
            <p><?php
                echo strip_tags( $this->element->target_audience );
                ?></p>
        </div>
    <?php } ?>

    <?php if (!empty($this->element->course_validity)) {
        $courseValidity = $this->element->course_validity / (30 * 24 * 3600);
        ?>
        <div class="product_course_validity">
            <p class="heading"><?php echo JText::_('COURSE_VALIDITY'); ?></p>
            <p><?php
                echo ($courseValidity == 1) ?  $courseValidity.' '.JText::_('MONTH') : $courseValidity.' '.JText::_('MONTHS');
                ?></p>
        </div>
    <?php } ?>

    <?php
    if ($this->element->hide_price_addtocart){
        if(!empty($this->element->parent_bundle_id)){
            echo JText::_('KD_COURSE_AVAILABLE_IN_PROGRAM')."<br/>";
            $parentBundleList=explode(',',$this->element->parent_bundle_id);
            foreach ($parentBundleList as $key => $value) {
                $database =& JFactory::getDBO();
                $query = 'SELECT * from '.hikashop_table('product')." WHERE product_code='".$value."' ";
                $database->setQuery($query);
                $parentBundle=$database->loadObject();
                $link = hikashop_contentLink('product&task=show&cid='.$parentBundle->product_id,$parentBundle);
                echo "<a href='".$link."'>".$parentBundle->product_name."</a><br/>";
            }
        }
    }

    if (!$this->element->hide_price_addtocart) {
        ?>

        <div id="hikashop_product_right_part" class="product_button_add_to_cart hikashop_product_right_part <?php echo HK_GRID_COL_6; ?>">
            <?php
            if(!empty($this->element->extraData->rightMiddle))
                echo implode("\r\n",$this->element->extraData->rightMiddle);
            ?>
            <?php
            $this->setLayout('show_block_dimensions');
            echo $this->loadTemplate();
            ?><br />
            <?php
            if($this->params->get('characteristic_display') != 'list') {
                $this->setLayout('show_block_characteristic');
                echo $this->loadTemplate();
                ?>
                <br />
            <?php
            }

            $form = ',0';
            if (!$this->config->get('ajax_add_to_cart', 1)) {
                $form = ',\'hikashop_product_form\'';
            }
            if (hikashop_level(1) && !empty ($this->element->options)) {
                ?>
                <div id="hikashop_product_options" class="hikashop_product_options">
                    <?php
                    $this->setLayout('option');
                    echo $this->loadTemplate();
                    ?>
                </div>
                <br />

                <?php
                $form = ',\'hikashop_product_form\'';
                if ($this->config->get('redirect_url_after_add_cart', 'stay_if_cart') == 'ask_user') {
                    ?>
                    <input type="hidden" name="popup" value="1"/>
                <?php
                }
            }
            if (!$this->params->get('catalogue') && ($this->config->get('display_add_to_cart_for_free_products') || ($this->config->get('display_add_to_wishlist_for_free_products', 1) && hikashop_level(1) && $this->params->get('add_to_wishlist') && $config->get('enable_wishlist', 1)) || !empty($this->element->prices))) {
                if (!empty ($this->itemFields)) {
                    $form = ',\'hikashop_product_form\'';
                    if ($this->config->get('redirect_url_after_add_cart', 'stay_if_cart') == 'ask_user') {
                        ?>
                        <input type="hidden" name="popup" value="1"/>
                    <?php
                    }
                    $this->setLayout('show_block_custom_item');
                    echo $this->loadTemplate();
                }
            }
            $this->formName = $form;
            if($this->params->get('show_price')){ ?>
                <span id="hikashop_product_price_with_options_main" class="hikashop_product_price_with_options_main"></span>
            <?php }
            if(empty ($this->element->characteristics) || $this->params->get('characteristic_display')!='list'){ ?>
                <div id="hikashop_product_quantity_main" class="hikashop_product_quantity_main">
                    <?php
                    $this->row = & $this->element;
                    $this->ajax = 'if(hikashopCheckChangeForm(\'item\',\'hikashop_product_form\')){ return hikashopModifyQuantity(\'' . $this->row->product_id . '\',field,1' . $form . ',\'cart\'); } else { return false; }';
                    $this->setLayout('quantity');
                    echo $this->loadTemplate();
                    ?>
                </div>
            <?php } ?>
            <div id="hikashop_product_contact_main" class="hikashop_product_contact_main">
                <?php
                $contact = $this->config->get('product_contact',0);
                if (hikashop_level(1) && ($contact == 2 || ($contact == 1 && !empty ($this->element->product_contact)))) {
                    $empty = '';
                    $params = new HikaParameter($empty);
                    global $Itemid;
                    $url_itemid='';
                    if(!empty($Itemid)){
                        $url_itemid='&Itemid='.$Itemid;
                    }
                    echo $this->cart->displayButton(JText :: _('CONTACT_US_FOR_INFO'), 'contact_us', $params, hikashop_completeLink('product&task=contact&cid=' . $this->element->product_id.$url_itemid), 'window.location=\'' . hikashop_completeLink('product&task=contact&cid=' . $this->element->product_id.$url_itemid) . '\';return false;');
                }
                ?>
            </div>
            <?php
            if(!empty($this->fields)){
                $this->setLayout('show_block_custom_main');
                echo $this->loadTemplate();
            }

            if(HIKASHOP_J30) {
                $this->setLayout('show_block_tags');
                echo $this->loadTemplate();
            }

            ?>
            <span id="hikashop_product_id_main" class="hikashop_product_id_main">
                <input type="hidden" name="product_id" value="<?php echo $this->element->product_id; ?>" />
            </span>
            <?php
            if(!empty($this->element->extraData->rightEnd))
                echo implode("\r\n",$this->element->extraData->rightEnd);
            ?>
        </div>

        <div class="">
            <div class="hikashop_product_cart">
                <?php
                // showing minicart_position ( Module ) in Product Detail Screen
                jimport('joomla.application.module.helper');
                $modules = JModuleHelper::getModules('minicart_position');
                foreach($modules as $module) {
                    echo JModuleHelper::renderModule($module);
                }
                ?>
            </div>
        </div>
    <?php
    }
    ?>
</div>
