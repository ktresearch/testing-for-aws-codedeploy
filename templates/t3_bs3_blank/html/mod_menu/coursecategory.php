<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.
?>
<div class="category_mainmenu">
    <?php
    foreach ($list as $i => $item) {
        if ($item->parent_id == 1) {
            echo "<div class='category_menu'><a class='category_menu_title' href='".$item->flink."'>".$item->title."</a>";
            echo "</div>";
        }
    }
    ?>
</div>