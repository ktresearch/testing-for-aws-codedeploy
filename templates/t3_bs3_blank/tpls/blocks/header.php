<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// get params
$sitename  = $this->params->get('sitename');
$slogan    = $this->params->get('slogan', '');
$logotype  = $this->params->get('logotype', 'text');
$logoimage = $logotype == 'image' ? $this->params->get('logoimage', T3Path::getUrl('images/logo.png', '', true)) : '';
$logoimgsm = ($logotype == 'image' && $this->params->get('enable_logoimage_sm', 0)) ? $this->params->get('logoimage_sm', T3Path::getUrl('images/logo-sm.png', '', true)) : false;

if (!$sitename) {
	$sitename = JFactory::getConfig()->get('sitename');
}

$logosize = 'col-sm-12';
if ($headright = $this->countModules('head-search or languageswitcherload')) {
	$logosize = 'col-sm-8';
}

?>

<!-- HEADER -->
<header id="t3-header" class="container t3-header">
<script type="text/javascript" src="<?php echo JURI::root(true) ?>/components/com_community/templates/jomsocial/assets/js/jquery.tagsinput.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo JURI::root(true) ?>/components/com_community/templates/jomsocial/assets/css/jquery.tagsinput.css" />
	<div class="row">
		<?php if ($headright): ?>
			<div class="col-xs-12 col-sm-4">
				<?php if ($this->countModules('head-search')) : ?>
					<!-- HEAD SEARCH -->
					<div class="head-search <?php $this->_c('head-search') ?>">
						<jdoc:include type="modules" name="<?php $this->_p('head-search') ?>" style="raw" />
					</div>
					<!-- //HEAD SEARCH -->
				<?php endif ?>

				<?php if ($this->countModules('languageswitcherload')) : ?>
					<!-- LANGUAGE SWITCHER -->
					<div class="languageswitcherload">
						<jdoc:include type="modules" name="<?php $this->_p('languageswitcherload') ?>" style="raw" />
					</div>
					<!-- //LANGUAGE SWITCHER -->
				<?php endif ?>
			</div>
		<?php endif ?>

	</div>
</header>
<!-- //HEADER -->
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#keywords').tagsInput();
        jQuery('#field17').tagsInput();
        jQuery('#LpTermCheckbox').on('click', function(e) {
            if(jQuery('.LpTermCondition').hasClass('checked')) {
                jQuery('.rsform-submit-button').addClass('disabled');
            }else {
               jQuery('.rsform-submit-button').removeClass('disabled'); 
            }
            jQuery(this).toggleClass('checked').find('.LpTermCondition').prop('checked', jQuery(this).hasClass('checked'));
        });
    });
</script>