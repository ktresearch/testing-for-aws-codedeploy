<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
require_once(JPATH_ROOT.'/components/com_community/libraries/core.php');
require_once(JPATH_SITE.'/components/com_hikashop/helpers/lpapi.php');

$user = JFactory::getUser();
$user2 = CFactory::getUser();
$cart = LpApiController::countcoursecart($user2->id);

$notifModel = CFactory::getModel('notification');
$myParams             = $user2->getParams();

$session                = JFactory::getSession();
$first_time_login = $session->get('first_time');
$defaulNotif = $notifModel->getDefaultNotification();
$defaultNotification = 0;
if($first_time_login) {
    if($defaulNotif) {
        $defaultNotification = 1;
    }
}
$class_defalt = '';
if($first_time_login) {
    $class_defalt = 'not-read';
}

$newNotificationCount = $notifModel->getNotificationCount($user2->id,'0', $myParams->get('lastnotificationlist',''));

$toolbar = CToolbarLibrary::getInstance();
$newEventInviteCount = $toolbar->getTotalNotifications('events');
$newFriendInviteCount = $toolbar->getTotalNotifications('friends');
$newGroupInviteCount = $toolbar->getTotalNotifications('groups');
$newMessageCount = $toolbar->getTotalNotifications('inbox');

$newNotificationCount += $defaultNotification;
//$newNotificationCount += $newEventInviteCount;
//$newNotificationCount += $newFriendInviteCount;
//$newNotificationCount += $newGroupInviteCount;
//$newNotificationCount += $newMessageCount;

$ntf_class = '';
if ($newNotificationCount != 0) {
    $ntf_class = 'have-ntf';
}

//if ($newNotificationCount != 0) {
//    JFactory::getDocument()->setTitle('('.$newNotificationCount.') '.JFactory::getDocument()->getTitle());
//}
$app = JFactory::getApplication();
$menu = $app->getMenu();
$ishomepage = false;
if ($menu->getActive() == $menu->getDefault( 'en-GB' )) {
    $ishomepage = true;
}
elseif ($menu->getActive() == $menu->getDefault( 'vi-VN' )) {
    $ishomepage = true;
} elseif($menu->getActive() == $menu->getDefault( 'zh-CN' )) {
    $ishomepage = true;
}
$isGuest = false;
if (JFactory::getUser()->guest == 1) {
    $isGuest = true;
}

require_once(JPATH_SITE.'/components/com_siteadminbln/models/account.php');
$isSiteAdmin = false;
$model = new SiteadminblnModelAccount();
if (count($model->checkSiteAdminBLn($user->id)) > 0) {
    $isSiteAdmin = true;
} 

$option = JFactory::getApplication()->input->getCmd('option');
if($option != 'com_community'){
    $document = JFactory::getDocument();
    $document->addStyleSheet('./components/com_community/templates/jomsocial/assets/css/style.css');
}
?>

<!-- MAIN NAVIGATION -->
<nav id="t3-mainnav" class="wrap navbar navbar-default t3-mainnav navbar-fixed-top ">
    <div class="container">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <?php // if($ishomepage || $isGuest) { ?>
                <a class="navbar-brand" href="index.php">
                    <img alt="Parenthexis" src="templates/t3_bs3_blank/images/parenthexis-logo.png">
                </a>
            <?php // } else { ?>
<!--                <a class="navbar-back" href="javascript:void(0);" onclick="goBack();">
                    <img alt="Back" src="templates/t3_bs3_blank/images/ArrowIcon.png">
                </a>-->
            <?php // } ?>
            
            <?php if ($this->getParam('navigation_collapse_enable', 1) && $this->getParam('responsive', 1)) : ?>
                <?php // $this->addScript(T3_URL.'/js/nav-collapse.js'); ?>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".t3-navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            <?php endif ?>

            <?php if ($this->getParam('addon_offcanvas_enable')) : ?>
                <?php $this->loadBlock ('off-canvas') ?>
            <?php endif ?>

            <?php if (!is_null($user->username)) { ?>
                <a class="navbar-notification <?php echo $ntf_class; ?>" data-placement='bottom' href="javascript:">
                    <?php if ($newNotificationCount != 0) { ?>
                        <div class="bg-second-img icon-ntf ntf-have"></div>
                        <span class="notification-count" style="<?php echo ($newNotificationCount  > 9) ? 'font-size: 8px;': '';?>"><?php echo $newNotificationCount;?></span>
            <?php } else { ?>
                        <div class="bg-second-img icon-ntf"></div>
                    <?php }?>
                    <?php //} else { ?>
                    <!-- <img src="templates/t3_bs3_blank/images/NoNotification.png" alt="<?php echo $user2->getDisplayName() ; ?>"> -->
                </a>
            <?php } ?>

            <?php //if (!is_null(JFactory::getUser()->username)) {
                    if($_REQUEST['view'] != '') {
            ?>

                <div class="navbar-search-all" id="navbar-search-all-id">
                    <div class="search-centre-icon" id="searchbox">
                        <div class="bg-second-img w22px icon-search search-all" id="toggle-search"></div>
                    </div>
                </div>
                <div class="search-all-type" id="content-search" style="display:none; ">
                    <div class="search-choose-category">
                        <div class="choose-category-title">
                            <span><?php echo JText::_('SEARCH_CHOOSE_CATEGORY'); ?></span>
                            <div class="bg-second-img w22px icon-search"></div>
                        </div>
                        <div class="search-list-category">
                            <!--                            Search icon Learning-->
                            <div class="category-box search-courses" id="searchboxlearning">
                                <span><?php echo JText::_('SEARCH_CATEGORY_LEARNING'); ?></span>
                                <!-- <img class="search-icon-course" id="searchboxlearning" src="images/course.png" alt="learning" onclick=""> -->
                                <div class="search-icon-course bg-img"></div>
                            </div>
                            <!--                            Search icon Circle-->
                            <div class="category-box search-circles" id="searchboxcircle">
                                <span><?php echo JText::_('SEARCH_CATEGORY_CIRCLES'); ?></span>
                                <!-- <img class="search-icon-circle" id="searchboxcircle" src="images/circle.png" alt="circle" onclick=""> -->
                                <div class="search-icon-circle bg-img"></div>
                            </div>
                            <!--                                Search icon Friend-->
                            <div class="category-box search-friends" id="searchboxfriend">
                                <span><?php echo JText::_('SEARCH_CATEGORY_FRIENDS'); ?></span>
                                <!-- <img class="search-icon-friend" id="searchboxfriend" src="images/friend.png" alt="friend" onclick=""> -->
                                <div class="search-icon-friend bg-img"></div>
                            </div>
                        </div>
                    </div>
                    <div class="bg-second-img w22px icon-cancel-search" id="search-cancel"></div>
            </div>
            <!--<img class="" id="input-icon-back" src="images/ArrowIcon.png" alt="" onclick="">-->
            <div class="search-all-type-form" style="display: none;">
                <div class="box-search" style="display: none;" id="searchlearning">
                    <form id="searchCourse" class="frmSearch" method="get"
                          action="<?php echo Juri::base() .'courses/search.html'; ?>">

                            <div class="input-group">
                                <div class="icon-addon addon-lg">
                                    <!--                                <input type="hidden" name="option" value="com_joomdle">-->
                                    <!--                                <input type="hidden" name="view" value="searchcourse">-->
                                    <input type="text" name="search" class="search-text" value=""
                                           size="30" placeholder="<?php echo JText::_('SEARCH_PLACEHOLDER_SEARCH'); ?>"/>
<!--                                    <img class="input-icon bg-img" id="input-icon-learning" src="images/MyLearningIcon_White.png" alt="" onclick="">-->
                                    <div class="input-icon bg-img" id="input-icon-learning"></div>
                                    <!--<input class="input-icon-search" id="input-icon-search" type="image" src="images/ico-search.png" alt="Submit" />-->
                                    <button class="bg-second-img w22px icon-search" type="submit"></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="box-search" style="display: none;" id="searchcircle">
                        <form method="GET" class="frmSearch"
                              action="<?php echo Juri::base() .'circles/search.html'; ?>">
                            <div class="input-group">
                                <div class="icon-addon addon-lg">
                                    <input placeholder="<?php echo JText::_('SEARCH_PLACEHOLDER_SEARCH'); ?>" type="text" class="search-text" value=""
                                           size="30" id="txtKeyword" name="search">
<!--                                    <img class="input-icon bg-img" id="input-icon-circle"  src="images/MyCirclesIcon_White.png" alt="" onclick="">-->
                                    <div class="input-icon bg-img" id="input-icon-circle"></div>
                                    <!--<input class="input-icon-search" id="input-icon-search" type="image" src="images/ico-search.png" alt="Submit" />-->
                                    <button class="bg-second-img w22px icon-search" type="submit"></button>
                                    <!--                                <label for="search" class="glyphicon glyphicon-search"></label>-->
                                </div>
                                <!--                            --><?php //echo JHTML::_('form.token') ?>
                            </div>
                            <input type="hidden" name="task" value="search">
                        </form>
                    </div>
                    <div class="box-search" style="display: none;" id="searchfriend">
                        <form method="get" class="frmSearch" action="<?php echo Juri::base() .'friends/search.html'; ?>">
                            <div class="icon-addon addon-lg">
                                <input placeholder="<?php echo JText::_('SEARCH_PLACEHOLDER_SEARCH'); ?>" type="text"
                                       class="search-text"
                                       name="q" class="joms-input--search"
                                       value="<?php echo (isset($searchQuery)) ? $searchQuery : ''; ?>">
                            <img class="input-icon" id="input-icon-friend" src="images/MyFriendsIcon_White.png" alt="" onclick="">
                            <input type="hidden" name="task" value="display">
                            <input class="input-icon-search" id="input-icon-search" type="image" src="images/ico-search.png" alt="Submit" />
                        </div>
                    </form>
                </div>
                <img class="search-icon-cancel" id="search-cancel2" src="images/icon-search-cancel.png" alt="Cancel"/>
                
            </div>
            <?php } ?>

<!--            <a class="navbar-faqs --><?php //echo (is_null($user->username)) ? 'hideNotifIcon':''; ?><!--" href="--><?php //echo JURI::root( true ) . 'component/fsf/faq';?><!--">-->
<!--                <img src="images/FAQ.png" />-->
<!--            </a>-->

        </div>

        <?php if ($this->getParam('navigation_collapse_enable')) : ?>
            <div class="t3-navbar-collapse navbar-collapse collapse">
                <jdoc:include type="<?php echo $this->getParam('navigation_type', 'megamenu') ?>" name="<?php echo $this->getParam('mm_type', 'mainmenu') ?>" />
            </div>
        <?php endif ?>


        <!-- Load sub menu -->
        <?php // $this->loadBlock('subnav') ?>

        <div id="popover_content_wrapper" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static">
            <div class="modal-dialog modal-sm">
                <?php
                $document = JFactory::getDocument();
                $renderer = $document->loadRenderer('module');
                $module = JModuleHelper::getModule('mod_notifications');
                echo $renderer->render($module);
//                $href = JURI::base().'component/content/article/?id='.$defaulNotif['id'];
                $href = CRoute::_('index.php?option=com_community&view=groups&task=mygroups');
                ?>
                <div class="joms-stream__container joms-stream--discussion notifications default <?php echo $class_defalt; ?>">
                    <a href="<?php echo $href; ?>"><?php echo $defaulNotif['title'].', '.$user2->getDisplayName(); ?> !</a>
                </div>
            </div>
        </div>

    </div>
</nav>

<div class="lgt-notif"></div>
<div class="lgt-notif-error"></div>
<div class="modal" id="search-message">
    <div class="modal-content">
    <p id = "error"></p>
    <div class="closesearch" style="opacity: 1;"><img src="/images/deleteIcon.png"></div>
    </div>
</div>

<!--<script src="./plugins/system/t3/base-bs3/bootstrap/js/bootstrap.js" type="text/javascript"></script>-->
<script src="<?php echo JURI::root(true) ?>/components/com_community/templates/jomsocial/assets/js/jquery.tagsinput.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo JURI::root(true) ?>/components/com_community/templates/jomsocial/assets/css/jquery.tagsinput.css" />
<script type="text/javascript">
    jQuery(document).ready(function() {
     var per = '<?php echo $user2->_permission;?>';
        window.addEventListener("load", function() {
            if(per == 1) {
                
                jQuery(".navbar-fixed-bottom ul li").css('display','flex');
                jQuery(".navbar-fixed-bottom ul li:nth-child(3) ").addClass('hidden');
                jQuery(".navbar-fixed-bottom ul li").css({"width": "30%","padding-left":"40px"});
            }
            else jQuery(".navbar-fixed-bottom ul li").css('display','flex');
        }, false);
     
        <?php if ($user2->id > 0) {?>
        jQuery('.t3-megamenu ul.navbar-nav').prepend('<li class="navbar-profile"><a style="font-weight:bold" href="<?php echo JURI::root( true ) .'/component/community/profile'; ?>"><img class="navbar-avatar" src="<?php echo $user2->getAvatar();?>"/><span><?php echo htmlspecialchars($user2->getDisplayName(), ENT_QUOTES);?></span></a></li>');
        <?php } ?>

        jQuery('.lgt-notif,.lgt-notif-error').click(function() {
            jQuery(this).removeClass('lgt-visible');
        });

        var isSiteAdmin = <?php echo $isSiteAdmin ? 'true' : 'false';?>;
        if (!isSiteAdmin) { 
            jQuery(".siteadmin").parent().remove();
        }

        var modal = document.getElementById('search-message');
        var er = document.getElementById('error');
        jQuery('.closesearch').click(function(){
            modal.style.display = "none"; 
        });
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
        jQuery("#searchlearning .frmSearch").submit(function() {
            var txtSearch = jQuery('#searchlearning input.search-text').val();
            if (txtSearch == '') {
                lgtCreatePopup('oneButton', {content: "<?php echo JText::_('SEARCH_ERROR_MESSAGE');?>"}, function () {
                    lgtRemovePopup();
                });
                return false;
            }
        });
        jQuery("#searchcircle .frmSearch").submit(function() {
            var txtSearch = jQuery('#searchcircle input.search-text').val();
            if (txtSearch == '') {
                lgtCreatePopup('oneButton', {content: "<?php echo JText::_('SEARCH_ERROR_MESSAGE');?>"}, function () {
                    lgtRemovePopup();
                });
                return false;
            }
        });
        jQuery("#searchfriend .frmSearch").submit(function() {
            var txtSearch = jQuery('#searchfriend input.search-text').val();
            if (txtSearch == '') {
                lgtCreatePopup('oneButton', {content: "<?php echo JText::_('SEARCH_ERROR_MESSAGE');?>"}, function () {
                    lgtRemovePopup();
                });
                return false;
            }
        });

        jQuery('#toggle-search').click(function() {
            jQuery("#content-search").show();
            jQuery('.navbar-toggle').hide();
            jQuery('.navbar-notification').addClass('hide');
            jQuery('.navbar-search-all').addClass('hide');
            jQuery(".navbar-brand").hide();
        });
        jQuery('#search-cancel').click(function() {
            jQuery("#content-search").hide();
            jQuery('.navbar-toggle').show();
            jQuery('.navbar-notification').removeClass('hide');
            jQuery('.navbar-search-all').removeClass('hide');
            jQuery(".navbar-brand").show();
        });
        jQuery('#searchboxlearning').click(function() {
            jQuery("#content-search").hide();
            jQuery("#searchlearning").show();
            jQuery(".search-all-type-form").show();
            jQuery(".navbar-search-all").addClass('hide');
        });

        jQuery('#search-cancel2').click(function() {
            jQuery(".search-all-type-form").hide();
            jQuery("#searchlearning").hide();
            jQuery("#searchcircle").hide();
            jQuery("#searchfriend").hide();
            jQuery('.navbar-toggle').show();
            jQuery('.navbar-notification').removeClass('hide');
            jQuery('.navbar-search-all').removeClass('hide');
            jQuery(".navbar-brand").show();
        });


        jQuery('#searchboxcircle').click(function() {
            jQuery("#content-search").hide();
            jQuery("#searchcircle").show();
            jQuery(".search-all-type-form").show();
            jQuery(".navbar-search-all").addClass('hide');
        });

        jQuery('#searchboxfriend').click(function() {
            jQuery("#content-search").hide();
            jQuery("#searchfriend").show();
            jQuery(".search-all-type-form").show();
            jQuery(".navbar-search-all").addClass('hide');
        });

        jQuery('#t3-mainnav .navbar-toggle').click(function() {
            jQuery('.navbar-fixed-bottom').toggleClass('show-secondmenu');
            jQuery('.navbar-fixed-top .t3-megamenu > ul > li').removeClass('active');
        });

        var valueWidth = jQuery( window ).width();

        if (valueWidth < 415) {
            jQuery('.navbar-fixed-top .t3-megamenu .logout-link').parent().css({"bottom": "20px", "position": "fixed", "width":"100%","margin-bottom":""});
        } else {
            jQuery('.navbar-fixed-top .t3-megamenu .logout-link').parent().css({"bottom": "", "position": "", "width":"","margin-bottom":"65px"});
        }

        window.addEventListener("resize", function() {
            var valueWidth = jQuery( window ).width();

            if (valueWidth < 415) {
                jQuery('.navbar-fixed-top .t3-megamenu .logout-link').parent().css({"bottom": "20px", "position": "fixed", "width":"100%","margin-bottom":""});
            } else {
                jQuery('.navbar-fixed-top .t3-megamenu .logout-link').parent().css({"bottom": "", "position": "", "width":"", "margin-bottom":"65px"});
            }

        }, false);

        jQuery('.navbar-notification').popover({
            html : true,
            content: function() {
                return jQuery('#popover_content_wrapper').html();
            }
        });
        // end show
        jQuery('.navbar-notification').click(function (e) {
            e.stopPropagation();
        });
        jQuery(document).click(function (e) {
            if ((jQuery('.popover').length == 1 && jQuery('.popover').has(e.target).length == 0) || jQuery(e.target).is('.close')) {
                jQuery('.navbar-notification').popover('hide');
            }
        });
        
        jQuery('.t3-megamenu .cart-link').append('<span class = "noti_cart"><?php  echo count($cart); ?></span>');
        // end hide
        // update notification count
        jQuery('.navbar-notification.have-ntf').on('click', function() {
            jQuery('.notification-count').css('display', 'none');
            jQuery('.navbar-notification .icon-ntf').removeClass('ntf-have');
            jQuery('.joomdle-mylearning .notification').css('display', 'none');

            var title = document.title;
            var count = title.split(" ")[0];
            if ( (count.indexOf("(") != -1) && (count.indexOf(")") != -1) ) {
                var countNum = count.replace('(', '');
                countNum = countNum.replace(')', '');
                if (jQuery.isNumeric(countNum)) {
                    document.title = title.replace(count+' ', '');
                }
            }

            jQuery('.ntf-have').attr("src", "/templates/t3_bs3_tablet_desktop_template/images/NoNotification.png");

            var user = '<?php echo $user2->id; ?>';
            jQuery.ajax({
                type: 'post',
                url: 'index.php?option=com_community&view=profile&task=ajaxReadAllNotification',
                data: {userid: user},
                success: function(data) {

                }
            });
             jQuery.ajax({
                type: 'post',
                url: 'index.php?option=com_community&view=profile&task=ajaxReadGroupNotification',
                data: {userid: user},
                success: function(data) {
                 }
        });
        });

    }(jQuery));

    var processing = false;
    window.onload = function() {
        window.addEventListener("beforeunload", function (e) {
            if (!processing) {
                return undefined;
            }

            var confirmationMessage = 'It looks like something is processing.'
                                    + 'If you leave before finishing, you will not see result.';

            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
        });
    };

    function lgtCreatePopup(type, data, buttonCallback) {
        var popupHTML = '';
        var extraClass = data.extraClass ? data.extraClass : '';
        var header = data.header ? data.header : '';
        var content = data.content ? data.content : '';
        var subcontent = data.subcontent ? data.subcontent : '';

        switch(type) {
            case 'confirm':
                var cancelText = data.cancelText ? data.cancelText : 'Cancel';
                var yesText = data.yesText ? data.yesText : 'Yes';

                popupHTML = '<div class="confirmPopup lgtPopup hienPopup'+extraClass+'">'+
                '<div class="popupContent">'+
                '<p class="popup-content">'+content+'</p>'+
                '<p class="popup-subcontent">'+subcontent+'</p>'+
                '</div>'+
                '<div class="buttons">'+
                '<button type="button" class="btCancelPopup" onclick="lgtRemovePopup()">'+cancelText+'</button>'+
                '<button type="button" class="btYesPopup">'+yesText+'</button>'+
                '</div>'+
                '</div>';
                break;
            case 'withCloseButton':
                popupHTML = '<div class="withCloseButtonPopup lgtPopup hienPopup'+extraClass+'">'+
                '<div class="popupContent">'+
                '<p class="popup-content">'+content+'</p>'+
                '<p class="popup-subcontent">'+subcontent+'</p>'+
                '</div>'+
                '<button type="button" class="lgtPopupClose" onclick="lgtRemovePopup()">CLOSE</button>'+
                '</div>';
                break;
            case 'oneButton':
                var buttonText = data.buttonText ? data.buttonText : 'Close';
                popupHTML = '<div class="oneButtonPopup lgtPopup hienPopup'+extraClass+'">'+
                '<div class="popupContent">'+
                '<p class="popup-content">'+content+'</p>'+
                '<p class="popup-subcontent">'+subcontent+'</p>'+
                '</div>'+
                '<button type="button">'+buttonText+'</button>'+
                '</div>';
                break;
            case 'loading':
                popupHTML = '<div class="lgtAlert lgtPopup '+extraClass+' alert alert-loading alert-dismissible">'+
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+
                '<strong>'+header+'<span class="dotone">.</span><span class="dottwo">.</span><span class="dotthree">.</span></strong><span>'+content+'</span>'+
                '</div>';
                break;
            case 'warning':
                popupHTML = '<div class="lgtAlert lgtPopup '+extraClass+' alert alert-warning alert-dismissible">'+
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+
                '<strong>'+header+'</strong><span>'+content+'</span>'+
                '</div>';
                break;
            default:
                popupHTML = '<div class="notification notCloseButtonPopup lgtPopup '+extraClass+'">'+
                '<div class="popupContent">'+
                '<p class="popup-content">'+content+'</p>'+
                '<p class="popup-subcontent">'+subcontent+'</p>'+
                '</div>'+
                '</div>';
                break;
        }
        jQuery(popupHTML).prependTo('body').fadeIn();
        if (type != 'loading' && type != 'warning') jQuery('body').addClass('lgtOverlay');

        if (buttonCallback && typeof(buttonCallback) === "function") {
            switch (type) {
                case 'confirm':
                    jQuery('.confirmPopup .btYesPopup').off().on('click', function() {
                        jQuery(this).parent().parent('.lgtPopup').fadeOut(function() {
                            jQuery(this).remove();
                            if (jQuery('.lgtPopup').length == 0) jQuery('body').removeClass('lgtOverlay');
                        });
                        buttonCallback();
                    });
                    break;
                case 'oneButton':
                    jQuery('.oneButtonPopup button').off().on('click', function() {
                        jQuery(this).parent().parent('.lgtPopup').fadeOut(function() {
                            jQuery(this).remove();
                            if (jQuery('.lgtPopup').length == 0) jQuery('body').removeClass('lgtOverlay');
                        });
                        buttonCallback();
                    });
                    break;
            }
        }
    }

    function lgtRemovePopup() {
        jQuery('.lgtPopup').fadeOut(function() {
            jQuery(this).remove();
            if (jQuery('.lgtPopup').length == 0) jQuery('body').removeClass('lgtOverlay');
        });
    }

    jQuery(document).on('click', '.lgtPopupClose', function(e) {
        jQuery(this).parents('.lgtPopup').fadeOut(function() {
            jQuery(this).remove();
            if (jQuery('.lgtPopup').length == 0) jQuery('body').removeClass('lgtOverlay');
        });
    }).on('click', '.lgtPopup .btCancelPopup', function(e) {
        jQuery(this).parents('.lgtPopup').fadeOut(function() {
            jQuery(this).remove();
            if (jQuery('.lgtPopup').length == 0) jQuery('body').removeClass('lgtOverlay');
        });
    });

    function notification_list(link, div_id){
        jQuery.ajax({
            url: link,
            success: function(response){
                jQuery('#'+div_id).html(response);
            }
        });
        return '<div id="'+div_id+'"><ul class="joms-popover--toolbar-general"><li class="joms-js--loading" style="display:block"><img src="<?php echo JURI::root(true); ?>/components/com_community/assets/ajax-loader.gif" alt="loader"></li></ul></div>';
    }
    function goBack() {
        console.log(document.referrer);
        if (document.referrer == <?php echo '\''.JUri::base().'\''?>+'component/community/' ||
            document.referrer == <?php echo '\''.JUri::base().'\''?>+'component/community' ) {
            console.log(1);
            window.location.href=<?php echo '\''.JUri::base().'\''?>;
        }
        else {
            if( this.phonegapNavigationEnabled &&
                nav &&
                nav.app &&
                nav.app.backHistory ){
                nav.app.backHistory();
            } else {
                var str = document.referrer;
                var link = str.includes("viewabout");
                if(link)
                    window.location.href = document.referrer;
                else window.history.back();
            }
        }
    }
    function goBackAdmin() {
        window.location.href = "<?php echo JRoute::_('index.php?option=com_siteadminbln', false);?>";
    }
    function goBackCourse(){
        var str = window.location.href;
        var link = str.includes("course");
        if(link)
            window.location.href= '<?php echo JURI::root().'manage' ?>';
        else window.history.back();
    }
    //  show/hide 3 type search when click icon

</script>
<!-- //MAIN NAVIGATION -->
<script>
    jQuery(document).ready(function($){
        var originalPos;
        $(document).on("scroll", function(e){
            if($('body').find('.joomdle-course').length !== 0) {
            var detachTop = $(".joomdle-course").height();
            if($("#nav").hasClass("affixed")){
                console.log('scroll top');
                if($(document).scrollTop() <= 10) {
                    if(detachTop < 600) {
                        $(".joomdle-course").height(detachTop - originalPos);
                    }
                    $("#nav").removeClass("affixed");
                return;
                }
            }
            if((originalPos = $(document).scrollTop()) >= ($("#nav").offset().top - $("#nav").height())){
                if(originalPos < 300) {
                    $(".joomdle-course").height(100);
                } else {
                    $(".joomdle-course").height(0);
                }
                $("#nav").addClass("affixed");
                return false;
            }
        }
        
            if($('body').find('.joms-main').length !== 0) {
                e.preventDefault();
                var detachTop = $(".joms-main").height();
                if($(".jomsocial").hasClass("affixed")){
                    if($(document).scrollTop() <= 106) {
                        if(detachTop < 600) {
//                            $(".joms-main").height(detachTop - originalPos);
                        }
                        $(".jomsocial").removeClass("affixed");
                        return;
                    }
                }

                var h = $(".course-menu ").height();
                if (h === null) {
                    h = 10;
                }
                if((originalPos = $(document).scrollTop()) >= ($(".joms-focus__title").offset().top - $(".joms-focus__title").height() - h)) {
//                    if(originalPos < 300) {
//                        $(".joms-main").height(800);
//                    } else {
//                        $(".joms-main").height(700);
//                    }
                    $(".jomsocial").addClass("affixed");
                    return false;
                }
            }
            
            if($('body').find('.contentpane').length !== 0) {
                var detachTop_scorm = $(".contentpane").height();
                if($("#nav").hasClass("affixed")){
                    console.log('scroll top');
                    if($(document).scrollTop() <= 10) {
                        if(detachTop_scorm < 600) {
                            $(".contentpane").height(detachTop_scorm - originalPos);
                        }
                        $("#nav").removeClass("affixed");
                        return;
                    }
                }
                if((originalPos = $(document).scrollTop()) >= ($("#nav").offset().top - $("#nav").height())){
                    if(originalPos < 300) {
                        $(".contentpane").height(100);
                    } else {
                        $(".contentpane").height(0);
                    }
                    $("#nav").addClass("affixed");
                    return false;
                }
            }
        });
    }(jQuery));

    jQuery(document).on('DOMNodeInserted', function(e) {
        jQuery(".view-groups.task-search #t3-mainnav.navbar-fixed-bottom .t3-megamenu > ul > li:nth-of-type(1) > a > img").attr("src","/images/MyCirclesIcon_555555.png");
        jQuery(".view-search.task-display #t3-mainnav.navbar-fixed-bottom .t3-megamenu > ul > li:nth-of-type(4) > a > img").attr("src","/images/MyFriendsIcon_555555.png");
    });

 </script>