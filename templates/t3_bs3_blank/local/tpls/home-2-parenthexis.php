<?php
/**
 *------------------------------------------------------------------------------
 * @package   T3 Framework for Joomla!
 *------------------------------------------------------------------------------
 * @copyright Copyright (C) 2004-2013 JoomlArt.com. All Rights Reserved.
 * @license   GNU General Public License; http://www.gnu.org/licenses/gpl.html
 * @author      JoomlArt, JoomlaBamboo
 *                If you want to be come co-authors of this project, please follow
 *                our guidelines at http://t3-framework.org/contribute
 *------------------------------------------------------------------------------
 */

defined('_JEXEC') or die;
$isGuest = (JFactory::getUser()->guest == 1) ? true : false;
$session = JFactory::getSession();

JFactory::highlightMenuItem();
?>

<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>"
      class='<jdoc:include type="pageclass" /> mob <?php echo !empty($_SERVER['HTTP_REFERER']) ? $session->get('currentMenuItem') : '';?> <?php echo implode($this->htmlTagClass, ' '); ?>'>

<head>
    <jdoc:include type="head" />
    <?php $this->loadBlock('head') ?>
    <?php $this->addCss('home') ?>
    <?php $this->addCss('submenu') ?>
</head>

<body>

<div class="t3-wrapper"> <!-- Need this wrapper for off-canvas menu. Remove if you don't use of-canvas -->

    <?php if (!$isGuest) : ?>

        <?php $this->loadBlock('header') ?>

        <?php $this->loadBlock('mainnav') ?>

        <?php $this->loadBlock('mainbody-home-2') ?>

        <?php $this->loadBlock('footer') ?>

    <?php else : ?>

        <div class="login-register-page">

            <?php $this->loadBlock('header') ?>

            <?php $this->loadBlock('mainbody-home-2') ?>

            <?php $this->loadBlock('footer') ?>

        </div>

    <?php endif; ?>

</div>

</body>
</html>

<script type="text/javascript">
    jQuery(document).ready(function() {

        jQuery('#t3-mainnav .t3-submenu .t3-megamenu .navbar-nav >li').each(function () {
            if (jQuery(this).hasClass('active')) {
                jQuery(this).removeClass('active');
            }
        });
    });
</script>