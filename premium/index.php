<!DOCTYPE html>
<!-- saved from url=(0048)http://parenthexis.com/component/community/ -->
<html xmlns:fb="http://ogp.me/ns/fb#" lang="en-gb" dir="ltr" class="com_community j34 mm-hover tab touch joms-no-touch">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Welcome to ASO</title>
<link href="http://www.parenthexis.com/templates/t3_bs3_tablet_desktop_template/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
<link rel="stylesheet" href="css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
<link rel="stylesheet" href="css/override.css" type="text/css">
<link rel="stylesheet" href="css/carousel.css" type="text/css">
<link rel="stylesheet" href="css/custom.css" type="text/css">
<link rel="stylesheet" href="css/old.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/classic.combined.css" type="text/css">
<link rel="stylesheet" href="css/template.css" type="text/css">
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" href="css/home.css" type="text/css">
<script src="js/loader.js" type="text/javascript"></script>
<script src="js/bundle(1).js" type="text/javascript"></script>
<script src="js/toolkit.min.js" type="text/javascript"></script>
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/jquery-noconflict.js" type="text/javascript"></script>
<script src="js/jquery-migrate.min.js" type="text/javascript"></script>
<script src="js/jquery.tap.min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
<script src="js/menu.js" type="text/javascript"></script>

<meta property="og:type" content="website">
<meta property="og:url" content="http://parenthexis.com/component/community/">
<meta property="og:title" content="Welcome to Parenthexis">
<meta property="og:see_also" content="/profile/frontpage">
<meta property="og:see_also" content="/circles/display">
<meta property="og:see_also" content="/profile/photos/display">
<meta property="og:see_also" content="/profile/videos/display">
<meta property="og:see_also" content="/my-events/display">
<meta property="og:site_name" content="Parenthexis">
<link rel="alternate" type="application/rss+xml" title="Recent activities feed" href="http://parenthexis.com/profile/frontpage?format=feed">
<meta property="og:image" content="http://parenthexis.com/components/com_community/assets/frontpage-image-default.jpg">
<style type="text/css">video { width: 100% !important; height: auto !important; }</style>

    
<!-- META FOR IOS & HANDHELD -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <style type="text/stylesheet">
    @-webkit-viewport   { width: device-width; }
    @-moz-viewport      { width: device-width; }
    @-ms-viewport       { width: device-width; }
    @-o-viewport        { width: device-width; }
    @viewport           { width: device-width; }
  </style>
  <script type="text/javascript">
    //<![CDATA[
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
      var msViewportStyle = document.createElement("style");
      msViewportStyle.appendChild(
        document.createTextNode("@-ms-viewport{width:auto!important}")
      );
      document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
    }
    //]]>
  </script>
<meta name="HandheldFriendly" content="true">
<meta name="apple-mobile-web-app-capable" content="YES">
<!-- //META FOR IOS & HANDHELD -->


<!-- Le HTML5 shim and media query for IE8 support -->
<!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script type="text/javascript" src="/plugins/system/t3/base-bs3/js/respond.min.js"></script>
<![endif]-->

<!-- You can add Google Analytics here or use T3 Injection feature -->
</head>

<body>

<div class="t3-wrapper"> <!-- Need this wrapper for off-canvas menu. Remove if you don't use of-canvas -->
<div class="right-column">
<div class="header">  
  <!-- HEADER -->
  <header id="t3-header" class="container t3-header">
    <div class="row">

      <!-- LOGO -->
      <div class="col-xs-12 col-sm-12 logo">
        <div class="logo-text">
          <a href="http://parenthexis.com/" title="Parenthexis">
                <span>Parenthexis</span>
          </a>
          <small class="site-slogan"></small>
        </div>
      </div>
      <!-- //LOGO -->
      
    </div>
  </header>
  <!-- //HEADER -->
</div>   

<div id="t3-mainbody" class="container t3-mainbody">
<div class="row">
    
<!-- MAIN CONTENT -->
<div id="t3-content" class="t3-content col-xs-12">
            
<div id="community-wrap" class="jomsocial-wrapper on-t3_bs3_tablet_desktop_template ltr c"><div class="jomsocial">
<script type="text/javascript">joms.filters && joms.filters.bind();</script>

<style type="text/css">
    body {
        background: #f8f8f8;
    }
    .joms-landing .joms-landing__action {
        background: transparent;
    }
    body, .home, #t3-mainbody, #t3-content {
        padding-bottom: 0;
    }
    .mob #community-wrap .jomsocial .joms-landing,
    .mob #system-message {
        margin-top: 34px;
    }
    .input-group .icon-addon .form-control {
        background-position: 10px;
        text-align: left;
        background-color: #f8f8f8;
    }
    .mob #system-message {
        margin-bottom: -34px;
    }
    .mob .alert-warning {
        margin-bottom: 0;
    }

</style>

<div class="joms-landing"> 
  <section class="section-white">

      <form action="http://parenthexis.com/categories" >
          <div class="input-group" style="text-align: center;">
            <h2 class="slogan" style="text-align: center;">ASO Learning Network. Achieve more with Learning Circles. Start your journey with us!</h2>
          </div>
      </form>
      
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="">
          <div class="carousel-inner" style="background-color: #000000;">
            <div class="item active">
              <img style="opacity: 0.69;" src="images/1.png" alt="...">
              <div class="carousel-caption">
                <h2 style="font-size: 12px; text-align: center;">Join Parenthexis now - It's free!</h2>
              </div>
            </div>
            <div class="item">
              <img style="opacity: 0.69;" src="images/2.png" alt="...">
              <div class="carousel-caption">
                <h2 style="font-size: 12px; text-align: center;">Stretch your learning dollars</h2>
              </div>
            </div>
            <div class="item">
              <img style="opacity: 0.69;" src="images/3.png" alt="...">
              <div class="carousel-caption">
                <h2 style="font-size: 12px; text-align: center;">Customised your learning experience</h2>
              </div>
            </div>
          </div>
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
              <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
            </ol>
      </div>          
  </section>
    
  <div class="row">
      <div class="col-sm-4" style="padding-left: 30px; padding-top: 20px; padding-right: 25px">
         <div class="panel panel-primary" style="border:none">
              <div class="panel-heading" style="font-size: 20px; background-color: #126db6;">Announcement</div>
              <div class="panel-body">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
              </div>
          </div>
          <div>
          <span class="glyphicon glyphicon-envelope" aria-hidden="true" style="color:#126db6"></span> Mail: contact@aso.com
          </div>
          <div>
          <span class="glyphicon glyphicon-globe" aria-hidden="true" style="color:#126db6"></span> Website: www.aso.com
          </div>
      </div>

      <div class="col-sm-8" style="padding-right: 40px; padding-left: 10px">
        <div class="joms-landing__action">
        <form class="joms-form joms-js-form--login" name="login" id="form-login">
            <div class="label-los">
                <span style="color: #126db6"><strong>Login</strong></span>
                <span style="color: #126db6">or</span>
                <button type="button" style="background-color:#126db6; border-color:#126db6; font-family: 'Open Sans', OpenSans-Semibold, sans-serif;font-weight: 600;" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal">
                  Sign up</button>          
            </div>
                        
            <div class="joms-input--append">                
                <input class="joms-input" type="text" placeholder="Email Address" name="username" autocapitalize="off">
            </div>
            <div class="joms-input--append">                
                <input class="joms-input" type="password" placeholder="Password" name="password" autocapitalize="off">
            </div>

            <button class="joms-button--login" data-toggle="modal" data-target="#myModal" onclick="return false;">Login</button>

            <div class="remember-reset">
                            <div class="joms-checkbox" style="float: left">
                    <div class="checked-status checked" style="height: 25px;vertical-align: top;"></div>
                    <input type="checkbox" value="yes" id="remember" name="remember" style="display: none;">
                    <span style="margin-left: 0;">Stay logged in</span>
            </div>
            <div style="float: right; padding-top: 7px">
            <a style="color: #7f8c8d" href="http://parenthexis.com/component/users/?view=reset" tabindex="6" data-toggle="modal" data-target="#myModal">Forgot Password?</a>  
            </div>
            </div>
                     
            <input type="hidden" name="option" value="com_users">
            <input type="hidden" name="task" value="user.login">
            <input type="hidden" name="return" value="aW5kZXgucGhwP29wdGlvbj1jb21fY29tbXVuaXR5JnZpZXc9ZnJvbnRwYWdl">
            <div class="joms-js--token"><input type="hidden" name="176b4b8f102a096799ff2357eeb46171" value="1"></div>

        </form>
          
        <script>
            jQuery(document).ready(function() {
                jQuery('.checked-status, .remember-reset > .joms-checkbox > span').click(function() {
                    if (jQuery('.remember-reset > .joms-checkbox > input').val() == 'yes') {
                        jQuery('.remember-reset > .joms-checkbox > input').val('no');
                    } else {
                        jQuery('.remember-reset > .joms-checkbox > input').val('yes')
                    }
                    jQuery('.checked-status').toggleClass('checked');
                });
            });
            joms.onStart(function( $ ) {
                $('.joms-js-form--login').on( 'submit', function( e ) {
                    e.preventDefault();
                    e.stopPropagation();
                    joms.ajax({
                        func: 'system,ajaxGetLoginFormToken',
                        data: [],
                        callback: function( json ) {
                            var form = $('.joms-js-form--login');
                            if ( json.token ) {
                                form.find('.joms-js--token input').prop('name', json.token);
                            }
                            form.off('submit').submit();
                        }
                    });
                });
            });
        </script>
    </div> 

    </div>
</div>
    
</div>
  
<div class="poweredby" style="margin: auto; text-align: center; padding-top: 50px; padding-bottom: 20px">
    <p>Powered by ParentheXis</p>
</div>
    
<script>
    joms_filter_params = {"filter":"","value":"default_value","hashtag":false};
</script>

</div></div>
<script>window.joms && joms.fixUI && joms.fixUI();</script>

    </div>
    <!-- //MAIN CONTENT -->

  </div>
</div>

<!-- MAIN NAVIGATION -->
<nav id="t3-mainnav" class="wrap navbar navbar-default t3-mainnav navbar-fixed-top not-home">
<div class="container">

<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
    
<a class="navbar-title">
<img alt="ASO" src="images/ASO-COLLEGE-GROUP_logo.png" height="80" width="180">    
<span style="margin-left: 170px">Welcome to ASO</span>
</a>

<button type="button" class="navbar-menu-icon">
    <img src="images/icon-menu.png" alt="Menu">
</button>

<div class="t3-megamenu" data-responsive="true">

<ul class="nav navbar-nav level0">

  <li data-id="283" data-level="1">
    <a class="" href="http://parenthexis.com/about-us" data-target="#"><img src="images/Untitled-1-Recovered.png" alt="About us">
    <span class="image-title">About us</span>  
    </a>
  </li>

  <li data-id="284" data-level="1">
    <a class="" href="http://parenthexis.com/terms-of-use" data-target="#"><img src="images/Termsofuse.png" alt="Terms of Use">
      <span class="image-title">Terms of Use</span>  
    </a>
  </li>

  <li data-id="285" data-level="1">
    <a class="" href="http://parenthexis.com/privacy-policy" data-target="#"><img src="images/privacy.png" alt="Privacy Policy">
      <span class="image-title">Privacy Policy</span>  
    </a>
  </li>

  <li data-id="286" data-level="1">
    <a class="" href="http://parenthexis.com/copyright" data-target="#"><img src="images/copyright.png" alt="Copyright">
      <span class="image-title">Copyright</span>  
    </a>
  </li>

  <li data-id="264" data-level="1">
    <a class="" href="http://parenthexis.com/faq" data-target="#"><img src="images/FAQ.png" alt="FAQ">
      <span class="image-title">FAQ</span>  
    </a>
  </li>

  <li data-id="287" data-level="1" style="bottom: 20px; position: fixed;">
    <a class="logout-link" href="http://parenthexis.com/logout" data-target="#">Logout </a>
  </li>

</ul>

</div>

  <a class="navbar-faqs" href="http://parenthexis.com/index.php?option=com_fsf&amp;view=faq">
      <img src="images/FAQ(1).png">
  </a>

</div>
        
<!-- Notification -->            
<div id="popover_content_wrapper" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static">
<div class="modal-dialog modal-sm">
<script>
  (function($) {
      $('div.notifications.not-read').on('click', function() {
         var id = $(this).attr('id');
         var action_url = $(this).children('input').val();
         $.ajax({
            type: 'post' ,
            url: 'index.php?option=com_community&view=profile&task=ajaxUpdateNotification',
            data: {id: id},
            success: function(data) {
                // nothing
                if(action_url) {
                    window.location.href = action_url;
                }
            }
         });
      });
  })(jQuery);
</script>
<div class="joms-stream__container joms-stream--discussion notifications default ">
  <a href="http://parenthexis.com/index.php?option=com_content&amp;view=article&amp;id=">, a Guest !</a> 
</div>

</div>
</div> 
</div>
</nav>

<script src="js/bootstrap.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function() {
        jQuery('#t3-mainnav .navbar-menu-icon').click(function() {
            jQuery(this).toggleClass('show');
            jQuery('.t3-wrapper .right-column').toggleClass('show-secondmenu');
            jQuery('.t3-wrapper .tab.left-column').toggleClass('show-secondmenu');
            jQuery('.navbar-fixed-top').toggleClass('show-secondmenu');
            jQuery('.course-menu, .changePosition').toggleClass('show-secondmenu');
            jQuery('.joomdle-overview-header').toggleClass('show-secondmenu');
            jQuery('#community-wrap .jomsocial .joms-landing > .btn-group.btn-group-justified, .com_community.view-groups.task-adddiscussion .discussion-buttons, .com_community.view-groups.task-viewdiscussion .joms-page > .joms-comment__reply').toggleClass('show-secondmenu');
            jQuery('.navbar-fixed-top .t3-megamenu > ul > li').removeClass('active');
            jQuery('.navbar-fixed-top .t3-megamenu').toggleClass('visible');
        });
        jQuery('.navbar-fixed-top .t3-megamenu .logout-link').parent().css({"bottom": "20px", "position": "fixed"});
        jQuery('.t3-mainbody').click(function() {
                jQuery('#t3-mainnav .navbar-menu-icon').removeClass('show');
                jQuery('.t3-wrapper .right-column').removeClass('show-secondmenu');
                jQuery('.t3-wrapper .tab.left-column').removeClass('show-secondmenu');
                jQuery('.navbar-fixed-top').removeClass('show-secondmenu');
                jQuery('.course-menu, .changePosition').removeClass('show-secondmenu');
                jQuery('#community-wrap .jomsocial .joms-landing > .btn-group.btn-group-justified, .com_community.view-groups.task-adddiscussion .discussion-buttons, .com_community.view-groups.task-viewdiscussion .joms-page > .joms-comment__reply').removeClass('show-secondmenu');
                jQuery('.navbar-fixed-top .t3-megamenu > ul > li').removeClass('active');
                jQuery('.navbar-fixed-top .t3-megamenu').removeClass('visible');
        });
        jQuery('.wrap.t3-sl').click(function() {
                jQuery('#t3-mainnav .navbar-menu-icon').removeClass('show');
                jQuery('.t3-wrapper .right-column').removeClass('show-secondmenu');
                jQuery('.t3-wrapper .tab.left-column').removeClass('show-secondmenu');
                jQuery('.navbar-fixed-top').removeClass('show-secondmenu');
                jQuery('.course-menu, .changePosition').removeClass('show-secondmenu');
                jQuery('#community-wrap .jomsocial .joms-landing > .btn-group.btn-group-justified, .com_community.view-groups.task-adddiscussion .discussion-buttons, .com_community.view-groups.task-viewdiscussion .joms-page > .joms-comment__reply').removeClass('show-secondmenu');
                jQuery('.navbar-fixed-top .t3-megamenu > ul > li').removeClass('active');
                jQuery('.navbar-fixed-top .t3-megamenu').removeClass('visible');
        });
        // notification
        jQuery('.navbar-notification').popover({
            html : true,
            content: function() {
              return jQuery('#popover_content_wrapper').html();  
            }
        });
        // end show
        jQuery('.navbar-notification').click(function (e) {
            e.stopPropagation();
        });
        jQuery(document).click(function (e) {
            if ((jQuery('.popover').has(e.target).length == 0) || jQuery(e.target).is('.close')) {
                jQuery('.navbar-notification').popover('hide');
            }
        });
        // end hide
        // update notification count
        jQuery('.navbar-notification.have-ntf').on('click', function() {
            jQuery('.notification-count').css('display', 'none');
            var title = document.title;
            var count = title.split(" ")[0];
            if ( (count.indexOf("(") != -1) && (count.indexOf(")") != -1) ) {
                var countNum = count.replace('(', '');
                countNum = countNum.replace(')', '');
                if (jQuery.isNumeric(countNum)) {
                    document.title = title.replace(count+' ', '');
                }    
            }

            jQuery('.ntf-have').attr("src", "/templates/t3_bs3_tablet_desktop_template/images/NoNotification.png");
            
            var user = '0';
            jQuery.ajax({
                type: 'post',
                url: 'index.php?option=com_community&view=profile&task=ajaxReadAllNotification',
                data: {userid: user},
                success: function(data) { 
                }
            });
        });
    }(jQuery));

    function goBack() {
        // window.history.back();
        var nav = window.navigator;
        if( this.phonegapNavigationEnabled &&
            nav &&
            nav.app &&
            nav.app.backHistory ){
            nav.app.backHistory();
        } else {
            window.history.back();
        }
    }
</script>
<!-- //MAIN NAVIGATION -->

</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="background-color: #126db6; color: #fff">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ParentheXis</h4>
      </div>
      <div class="modal-body">
        <p>This feature will be available from end of August onwards</p>
      </div>
     
    </div>

  </div>
</div>

</body>
</html>

