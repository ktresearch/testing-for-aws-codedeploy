<?php
/**
 * @version            1.1.0
 * @package            Joomdle - Jomsocial social groups
 * @copyright  Qontori Pte Ltd
 * @license            http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class  plgJoomdlesocialgroupsJoomdlesocialgroupsjomsocial extends JPlugin
{
    private $socialgroups = 'jomsocial';

    function integration_enabled ()
    {
        // Don't run if not configured as shop
        $params = JComponentHelper::getParams( 'com_joomdle' );
        $socialgroups = $params->get( 'socialgroups' );
        return  ($socialgroups == $this->socialgroups);
    }

    // Joomdle events
    public function onGetSocialGroupsExtension ()
    {
        $option['jomsocial'] = "Jomsocial";

        return $option;
    }

    public function onAddSocialGroup ($name, $description, $course_id, $username = null, $lpusername = null, $pic_url = '', $pic_name = '', $enableCreateChats = 1)
    {
        if (!$this->integration_enabled ())
            return false;

        if (file_exists (JPATH_ADMINISTRATOR.'/components/com_community/tables/cache.php'))
            require_once(JPATH_ADMINISTRATOR.'/components/com_community/tables/cache.php');
        require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
        require_once(JPATH_SITE.'/components/com_community/models/groups.php');

        $mainframe = JFactory::getApplication();
        $config = CFactory::getConfig();
        $my = CFactory::getUser();

        // Get my current data.
        $validated      = true;

        $group          = JTable::getInstance( 'Group' , 'CTable' );
        $model = CFactory::getModel('groups');

        // @rule: Test for emptyness
        if( empty( $name ) )
        {
            $validated = false;
        }

        // @rule: Test if group exists
        /*if( $model->groupExist( $name ) )
		{
				$validated = false;
        }*/

        // @rule: Test for emptyness
        if( empty( $description ) )
        {
            $validated = false;
        }

        $categoryId = $this->get_main_category ();
        if( empty( $categoryId ) )
        {
            $validated      = false;
        }

        if($validated)
        {
            // Assertions
            // Category Id must not be empty and will cause failure on this group if its empty.
            CError::assert( $categoryId , '', '!empty', __FILE__ , __LINE__ );


            // @rule: Retrieve params and store it back as raw string
            $params    = new CParameter( '' );


            $discussordering                        = JRequest::getVar( 'discussordering' , DISCUSSION_ORDER_BYLASTACTIVITY , 'REQUEST' );
            $params->set('discussordering' , $discussordering );

            $photopermission                        = JRequest::getVar( 'photopermission' , GROUP_PHOTO_PERMISSION_ADMINS , 'REQUEST' );
            $params->set('photopermission' , $photopermission );

            $videopermission                        = JRequest::getVar( 'videopermission' , GROUP_PHOTO_PERMISSION_ADMINS , 'REQUEST' );
            $params->set('videopermission' , $videopermission );

            $eventpermission                        = JRequest::getVar( 'eventpermission' , GROUP_EVENT_PERMISSION_ALL , 'REQUEST' );
            $params->set('eventpermission' , $eventpermission );

            $grouprecentphotos                      = JRequest::getVar( 'grouprecentphotos' , GROUP_PHOTO_RECENT_LIMIT , 'REQUEST' );
            $params->set('grouprecentphotos' , $grouprecentphotos );

            $grouprecentvideos                      = JRequest::getVar( 'grouprecentvideos' , GROUP_VIDEO_RECENT_LIMIT , 'REQUEST' );
            $params->set('grouprecentvideos' , $grouprecentvideos );

            $newmembernotification          = JRequest::getVar( 'newmembernotification' , '1' , 'REQUEST' );
            $params->set('newmembernotification' , $newmembernotification );

            $joinrequestnotification        = JRequest::getVar( 'joinrequestnotification' , '1' , 'REQUEST' );
            $params->set('joinrequestnotification' , $joinrequestnotification );

            $params->set('course_id' , $course_id );

            $params->set('enableCreateChats' , $enableCreateChats );

            CFactory::load('helpers' , 'owner' );
            $group->name            = $name;
            $group->description     = $description;
            $group->categoryid      = $categoryId;

            if ($lpusername) {
                $group->ownerid    = JFactory::getUser($lpusername)->id;
            } else {
                if (!$username)
                    $group->ownerid         = JoomdleHelperSystem::get_admin_id ();
                else $group->ownerid    = JFactory::getUser($username)->id;
            }

            $group->created         = gmdate('Y-m-d H:i:s');
            $group->approvals       = 1;
            $group->params          = $params->toString();

            $group->published       = 1;

            $group->unlisted        = 1;

            // Store the group now.
            $group->store();

            // Since this is storing groups, we also need to store the creator / admin
            // into the groups members table
            $member                         = JTable::getInstance( 'GroupMembers' , 'CTable' );
            $member->groupid        = $group->id;
            $member->memberid       = $group->ownerid;

            // Creator should always be 1 as approved as they are the creator.
            $member->approved       = 1;

            // @todo: Setup required permissions in the future
            $member->permissions    = '1';

            $member->store();

            // Update by KV
            // Add manager
            if ($lpusername) {
                if ($username) {
//                                        $this->onAddSocialGroupMember($username, 1, $course_id);
                    $user_id = JUserHelper::getUserId($username);
                    $memberm                 = JTable::getInstance( 'GroupMembers' , 'CTable' );
                    $memberm->groupid        = $group->id;
                    $memberm->memberid       = $user_id;

                    // Creator should always be 1 as approved as they are the creator.
                    $memberm->approved       = 1;

                    // @todo: Setup required permissions in the future
                    $memberm->permissions    = '1';

                    $memberm->store();
                }
            }

            // Increment the member count
            $group->updateStats();
            $group->store();
        }

        ////////////////////////////////////////////
        //start image processing
        // Get a hash for the file name.
        //if ($pic_name) $this->updateCircleImage($group->id, $pic_url, $pic_name);

        ////////////////////////////////////////////
        ////////////////////////////////////////////

        return "OK";
    }

    public function testResize($orgW, $orgH, $newW, $newH, $minVal, $maxVal)
    {
        $newValue = 0;
        if ($newH == 0) {
            /* New height value */
            $newValue = round(($newW * $orgH) / $orgW);
        } elseif ($newW == 0) {
            /* New width value */
            $newValue = round(($newH * $orgW) / $orgH);
        } else {
            return false;
        }
        return ($newValue >= $minVal && $newValue <= $maxVal) ? true : false;
    }

    public function onCopySocialGroup ($old_course_id, $new_course_id)
    {
        require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
        require_once(JPATH_SITE.'/components/com_community/models/groups.php');

        $mainframe = JFactory::getApplication();

        $old_groupid = $this->get_group_by_course_id($old_course_id);

        if ($old_groupid) {
            $new_groupid = $this->get_group_by_course_id($new_course_id);

            $db = JFactory::getDBO();

            $query = 'SELECT avatar, thumb, cover' .
                ' FROM #__community_groups' .
                " WHERE id = $old_groupid";

            $db->setQuery( $query );
            $old_group = $db->loadObjectList();

            $imgname = $old_group[0]->avatar;
            $covername = JApplication::getHash($old_group[0]->cover . time());

            $data_root = $mainframe->getCfg('dataroot');
            $config = CFactory::getConfig();

            $fileName = JApplication::getHash($imgname . time());
            $hashFileName = JString::substr($fileName, 0, 24);
            $hashFileCover = JString::substr($covername, 0, 24);

            $storage = $data_root . '/' . $config->getString('imagefolder');
            $storageImage = $storage . '/avatar/group/' . $hashFileName . '.jpg';
            $storageThumbnail = $storage . '/avatar/group/thumb_' . $hashFileName . '.jpg';
            $storageCover = $storage . '/cover/group/' . $new_groupid . '/' .  $hashFileCover . '.jpg';

            if ($old_group[0]->avatar && $old_group[0]->avatar != '') {
                $image = $config->getString('imagefolder') . '/avatar/group/' . $hashFileName . '.jpg' ;
            } else {
                $image = '';
            }
            if ($old_group[0]->thumb && $old_group[0]->thumb != '') {
                $thumbnail = $config->getString('imagefolder') . '/avatar/group/' . 'thumb_' . $hashFileName . '.jpg' ;
            } else {
                $thumbnail = '';
            }
            if ($old_group[0]->cover && $old_group[0]->cover != '') {
                $cover = $config->getString('imagefolder') . '/cover/group/' . $new_groupid . '/' . $hashFileCover . '.jpg' ;
            } else {
                $cover = '';
            }

            if (!is_dir($storage . '/cover/group/')) {
                mkdir($storage . '/cover/group/', 0777);
            }
            if (!is_dir($storage . '/cover/group/' . $new_groupid)) {
                mkdir($storage . '/cover/group/' . $new_groupid, 0777);
            }

            if (file_exists($data_root.'/'.$imgname)) {
                copy($data_root.'/'.$imgname, $storageImage);
            }
            if (file_exists($data_root.'/'.$old_group[0]->thumb)) {
                copy($data_root.'/'.$old_group[0]->thumb, $storageThumbnail);
            }
            if (file_exists($data_root.'/'.$old_group[0]->cover)) {
                copy($data_root.'/'.$old_group[0]->cover, $storageCover);
            }

            $query2 = 'UPDATE #__community_groups ' .
                "SET avatar='$image', thumb='$thumbnail', cover='$cover'" .
                " WHERE id='$new_groupid'";
            $db->setQuery( $query2 );
            $db->Query();
        }

        return "OK";
    }

    public function onUpdateSocialGroup ($name, $description, $course_id, $pic_url = '', $pic_name = '', $enableCreateChats = 1)
    {
        if (!$this->integration_enabled ())
            return false;

        $id = $this->get_group_by_course_id ($course_id);

        require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
        $group = JTable::getInstance( 'Group' , 'CTable' );
        $group->load($id);

        $group->name = $name;
        $group->description = $description;

        $params = $group->getParams();
        $params->set('enableCreateChats', $enableCreateChats);
        $group->params = $params->toString();
        $group->store();

        if ($pic_name) $this->updateCircleImage($id, $pic_url, $pic_name);

        return "OK";
    }

    public function onDeleteSocialGroup ($course_id, $course_name)
    {
        if (!$this->integration_enabled ())
            return false;

        if (file_exists (JPATH_ADMINISTRATOR.'/components/com_community/tables/cache.php'))
            require_once(JPATH_ADMINISTRATOR.'components/com_community/tables/cache.php');
        require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
        require_once(JPATH_SITE.'/components/com_community/models/groups.php');

        $group_id = $this->get_group_by_course_id ($course_id);
        if(!$group_id){
            $group_id = $this->get_group_by_course_name ($course_id, $course_name);
        }

        if ($group_id) {
            CommunityModelGroups::deleteGroupBulletins($group_id);
            CommunityModelGroups::deleteGroupMembers($group_id);
            CommunityModelGroups::deleteGroupWall($group_id);
            CommunityModelGroups::deleteGroupDiscussions($group_id);
            CommunityModelGroups::deleteGroupMedia($group_id);

            $group  = JTable::getInstance( 'Group' , 'CTable' );
            $group->load( $group_id );

            $group->delete( $group_id);
        }

        return "OK";
    }

    function onAddSocialGroupMember ($username, $permissions, $course_id)
    {
        if (!$this->integration_enabled ())
            return false;

        if (file_exists (JPATH_ADMINISTRATOR.'/components/com_community/tables/cache.php'))
            require_once(JPATH_ADMINISTRATOR.'/components/com_community/tables/cache.php');
        require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
        require_once(JPATH_SITE.'/components/com_community/models/groups.php');

        $group          = JTable::getInstance( 'Group' , 'CTable' );
        $groupModel = CFactory::getModel('groups');
        $member         = JTable::getInstance( 'GroupMembers' , 'CTable' );

        $group_id = $this->get_group_by_course_id ($course_id);

        if (!$group_id)
            return "NO GROUP";

        $group->load( $group_id );

        //$my =  CFactory::getUser($username);
        $user_id = JUserHelper::getUserId($username);
        $my =  JFactory::getUser($user_id);

        $params         = $group->getParams();

        // Set the properties for the members table
        $member->groupid        = $group->id;
        $member->memberid       = $my->id;

        CFactory::load( 'helpers' , 'owner' );

        /* kludge: remove when fixing call_method fns */
        if ($permissions == -1)
            $permissions = 0;

        $member->permissions    = $permissions;
        $member->approved       = '1';
        $member->date           = time();

        $store  = $member->store();

        // Add assertion if storing fails
        CError::assert( $store , true , 'eq' , __FILE__ , __LINE__ );

        $group->updateStats();
        $group->store();

        return "OK";
    }

    function onRemoveSocialGroupMember ($username, $course_id)
    {
        if (!$this->integration_enabled ())
            return false;

        if (file_exists (JPATH_ADMINISTRATOR.'/components/com_community/tables/cache.php'))
            require_once(JPATH_ADMINISTRATOR.'/components/com_community/tables/cache.php');
        require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
        require_once(JPATH_SITE.'/components/com_community/models/groups.php');

        $group          = JTable::getInstance( 'Group' , 'CTable' );
        $groupModel = CFactory::getModel('groups');
        $member         = JTable::getInstance( 'GroupMembers' , 'CTable' );

        $group_id = $this->get_group_by_course_id ($course_id);

        if (!$group_id)
            return;

        $group->load( $group_id );

        $user_id = JUserHelper::getUserId($username);
        $my =  JFactory::getUser($user_id);

        $db = JFactory::getDBO();

        $query = 'DELETE from #__community_groups_members'
            . ' WHERE groupid=' . $db->Quote( $group_id ).' and memberid='.$db->Quote( $my->id )
        ;

        $db->setQuery( $query );
        if (!$db->query()) {
            return JError::raiseWarning( 500, $db->getError() );
        }

        $query = "UPDATE #__community_groups SET membercount=membercount-1 WHERE id =".$db->Quote( $group_id);
        $db->setQuery( $query );
        if (!$db->query()) {
            return JError::raiseWarning( 500, $db->getError() );
        }
        return "OK";
    }

    function onAddBLNMember ($username)
    {
        if (!$this->integration_enabled ())
            return false;

        if (file_exists (JPATH_ADMINISTRATOR.'/components/com_community/tables/cache.php'))
            require_once(JPATH_ADMINISTRATOR.'/components/com_community/tables/cache.php');
        require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
        require_once(JPATH_SITE.'/components/com_community/models/groups.php');

        $groupModel = CFactory::getModel('groups');

        $user_id = JUserHelper::getUserId($username);

        $BLNCircle = $groupModel->getBLNCircle();
        if (!empty($BLNCircle)) {
            $groupModel->addUserToCircle($user_id, $BLNCircle->id);
        }

        return "OK";
    }

    function getBLNMoodleCategoryId ($username)
    {
        require_once(JPATH_SITE.'/components/com_siteadminbln/models/account.php');

        $accountModel = new SiteadminblnModelAccount();

        $blnmoodlecategoryid = $accountModel->getBLNMoodleCategoryId();

        return $blnmoodlecategoryid;
    }

    function get_main_category ()
    {
        $comp_params = JComponentHelper::getParams( 'com_joomdle' );
        $id = $comp_params->get( 'jomsocial_groups_category', 1 );

        return $id;
    }

    function onGetGroupByCourseId ($course_id)
    {
        if (!$this->integration_enabled ())
            return false;

        return $this->get_group_by_course_id ($course_id);
    }

    function get_group_by_course_id ($course_id)
    {
        $db = JFactory::getDBO();

        $searchEscaped1 = $db->Quote( "%\"course_id\":" . $db->escape( $course_id, true ) . "}%", false );
        $searchEscaped2 = $db->Quote( "%\"course_id\":" . $db->escape( $course_id, true ) . ",%", false );
        $query = 'SELECT id' .
            ' FROM #__community_groups' .
            " WHERE params LIKE $searchEscaped1 OR params LIKE $searchEscaped2 LIMIT 1";

        $db->setQuery( $query );
        $result = $db->loadResult();

        return $result;
    }

    function get_publish_group_by_course_id ($course_id)
    {
        $db = JFactory::getDBO();

        $searchEscaped1 = $db->Quote( "%\"course_id\":" . $db->escape( $course_id, true ) . "}%", false );
        $searchEscaped2 = $db->Quote( "%\"course_id\":" . $db->escape( $course_id, true ) . ",%", false );
        $query = 'SELECT published' .
            ' FROM #__community_groups' .
            " WHERE params LIKE $searchEscaped1 OR params LIKE $searchEscaped2 LIMIT 1";

        $db->setQuery( $query );
        $result = $db->loadResult();

        return $result;
    }


    function get_activity_count_circle ($groupid, $userid)
    {
        require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');

        $count_noti = My_Groups::groupActivities($groupid, $userid);

        return $count_noti;
    }

    function get_group_by_course_name ($course_id, $course_name)
    {
        $db = JFactory::getDBO();
        $searchEscaped1 = $db->Quote( "%\"course_id\":" . $db->escape( $course_id, true ) . "}%", false );
        $searchEscaped2 = $db->Quote( "%\"course_id\":" . $db->escape( $course_id, true ) . ",%", false );
        $query = 'SELECT id' .
            ' FROM #__community_groups' .
            " WHERE name = \"$course_name\" OR  params LIKE $searchEscaped1 OR params LIKE $searchEscaped2 LIMIT 1";
        $db->setQuery( $query );
        $result = $db->loadResult();
        return $result;
    }

    function onGetGroupUrl ($group_id)
    {
        if (!$this->integration_enabled ())
            return false;

        $url = JRoute::_("index.php?option=com_community&view=groups&task=viewgroup&groupid=$group_id");
        return $url;
    }

    function updateCircleImage($groupid, $pic_url, $pic_name) {
        if (file_exists (JPATH_ADMINISTRATOR.'/components/com_community/tables/cache.php'))
            require_once(JPATH_ADMINISTRATOR.'/components/com_community/tables/cache.php');
        require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
        require_once(JPATH_SITE.'/components/com_community/models/groups.php');

        $mainframe = JFactory::getApplication();
        $config = CFactory::getConfig();
        $my = CFactory::getUser();
        $tmp_name = $pic_name;
        $type = 'group';
        $group = JTable::getInstance(ucfirst($type), 'CTable');
        $group->load($groupid);

        switch (pathinfo(strtolower($pic_name), PATHINFO_EXTENSION)) {
            case 'jpg':
                $fileType = 'image/jpeg';
                break;
            case 'png':
                $fileType = 'image/png';
                break;
            case 'gif':
                $fileType = 'image/gif';
                break;
            case 'jpeg':
                $fileType = 'image/jpeg';
                break;
        }
        $fileName = JApplication::getHash($pic_name . time());

        $hashFileName = JString::substr($fileName, 0, 24);
        $avatarFolder = ($type != 'profile' && $type != '') ? $type . '/' : '';

        //avatar store path
        $storage = $mainframe->getCfg('dataroot') . '/' . $config->getString('imagefolder') . '/avatar' . '/' . $avatarFolder;

        if (!JFolder::exists($storage)) {
            JFolder::create($storage);
        }
        $storageImage = $storage . '/' . $hashFileName . CImageHelper::getExtension($fileType);

        $image = $config->getString(
                'imagefolder'
            ) . '/avatar/' . $avatarFolder . $hashFileName . CImageHelper::getExtension($fileType);

        /**
         * reverse image use for cropping feature
         * @uses <type>-<hashFileName>.<ext>
         */
        $storageReserve = $storage . '/' . $type . '-' . $hashFileName . CImageHelper::getExtension($fileType);

        // filename for stream attachment
        $imageAttachment = $config->getString(
                'imagefolder'
            ) . '/avatar/' . $hashFileName . '_stream_' . CImageHelper::getExtension($fileType);

        //avatar thumbnail path
        $storageThumbnail = $storage . '/thumb_' . $hashFileName . CImageHelper::getExtension($fileType);
        $thumbnail = $config->getString(
                'imagefolder'
            ) . '/avatar/' . $avatarFolder . 'thumb_' . $hashFileName . CImageHelper::getExtension($fileType);

        //Minimum height/width checking for Avatar uploads
        list($currentWidth, $currentHeight) = getimagesize($pic_url);
        if ($currentWidth < COMMUNITY_AVATAR_PROFILE_WIDTH || $currentHeight < COMMUNITY_AVATAR_PROFILE_HEIGHT) {
            return JText::sprintf(
                'COM_COMMUNITY_ERROR_MINIMUM_AVATAR_DIMENSION',
                COMMUNITY_AVATAR_PROFILE_WIDTH,
                COMMUNITY_AVATAR_PROFILE_HEIGHT
            );
        }

        /**
         * Generate square avatar
         */

        if (!CImageHelper::createThumb(
            $pic_url,
            $storageImage,
            $fileType,
            COMMUNITY_AVATAR_PROFILE_WIDTH,
            COMMUNITY_AVATAR_PROFILE_HEIGHT
        )
        ) {
            return JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $storageImage);
        }

        // Generate thumbnail
        if (!CImageHelper::createThumb($pic_url, $storageThumbnail, $fileType,COMMUNITY_SMALL_AVATAR_WIDTH,COMMUNITY_SMALL_AVATAR_WIDTH)) {
            return JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $storageImage);
        }

        /**
         * Generate large image use for avatar thumb cropping
         * It must be larget than profile avatar size because we'll use it for profile avatar recrop also
         */
        $newWidth = 0;
        $newHeight = 0;
        if ($currentWidth >= $currentHeight) {
            if ($this->testResize(
                $currentWidth,
                $currentHeight,
                COMMUNITY_AVATAR_RESERVE_WIDTH,
                0,
                COMMUNITY_AVATAR_PROFILE_WIDTH,
                COMMUNITY_AVATAR_RESERVE_WIDTH
            )
            ) {
                $newWidth = COMMUNITY_AVATAR_RESERVE_WIDTH;
                $newHeight = 0;
            } else {
                $newWidth = 0;
                $newHeight = COMMUNITY_AVATAR_RESERVE_HEIGHT;
            }
        } else {
            if ($this->testResize(
                $currentWidth,
                $currentHeight,
                0,
                COMMUNITY_AVATAR_RESERVE_HEIGHT,
                COMMUNITY_AVATAR_PROFILE_HEIGHT,
                COMMUNITY_AVATAR_RESERVE_HEIGHT
            )
            ) {
                $newWidth = 0;
                $newHeight = COMMUNITY_AVATAR_RESERVE_HEIGHT;
            } else {
                $newWidth = COMMUNITY_AVATAR_RESERVE_WIDTH;
                $newHeight = 0;
            }
        }

        if (!CImageHelper::resizeProportional(
            $pic_url,
            $storageReserve,
            $fileType,
            $newWidth,
            $newHeight
        )
        ) {
            return JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $storageReserve);
        }

        /*
         * Generate photo to be stored in default avatar album
         * notes: just in case this need to be used in registration, just get the code below.
         */

        $originalPath = $storage . 'original_' . $hashFileName . CImageHelper::getExtension(
                $fileType
            );
        $originalImage = 'original_' . $hashFileName . CImageHelper::getExtension($fileType);

        $fullImagePath = $storage . $hashFileName . CImageHelper::getExtension($fileType);
        $thumbPath = $storage . 'thumb_' . $hashFileName . CImageHelper::getExtension(
                $fileType
            );

        // Generate full image
        if (!CImageHelper::resizeProportional($pic_url, $fullImagePath, $fileType, 1024)) {
            return JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $tmp_name);
        }

        CPhotos::generateThumbnail($pic_url, $thumbPath, $fileType);

        if (!JFile::copy($pic_url, $originalPath)) {
            return;
        }

        $album = JTable::getInstance('Album', 'CTable');

        //create the avatar default album if it does not exists
        if (!$albumId = $album->isAvatarAlbumExists($group->id, $type)) {
            $albumId = $album->addAvatarAlbum($group->id, $type);
        }

        //store this picture into default avatar album
        $now = new JDate();
        $photo = JTable::getInstance('Photo', 'CTable');

        $photo->albumid = $albumId;
        $photo->image = str_replace($mainframe->getCfg('dataroot') . '/', '', $fullImagePath);
        $photo->caption = $tmp_name;
        $photo->filesize = filesize($tmp_name);
        $photo->creator = $my->id;
        $photo->created = $now->toSql();
        $photo->published = 1;
        $photo->thumbnail = str_replace($mainframe->getCfg('dataroot') . '/', '', $thumbPath);
        $photo->original = str_replace($mainframe->getCfg('dataroot') . '/', '', $originalPath);

        if ($photo->store()) {
            $album->load($albumId);
            $album->photoid = $photo->id;
            $album->setParam('thumbnail', $photo->thumbnail);
            $album->store();
        }

        $group->setImage($image, 'avatar');
        $group->setImage($thumbnail, 'thumb');
        $group->setImage($originalImage, 'origimage');
    }
}
