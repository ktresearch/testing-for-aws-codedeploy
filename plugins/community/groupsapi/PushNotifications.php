<?php
// Server file
class PushNotifications {

    // (Android)API access key from Google API's Console.
    // px22
//	private static $API_ACCESS_KEY = 'AIzaSyDBlnk3zLMa7IzxGyd1Qe3ii7B9Wi4v5SA';
    // sit4
    private static $API_ACCESS_KEY = 'AIzaSyAFRebuLtDxAv3G3YrK0A-qp8ir433coA8';
    // (iOS) Private key's passphrase.
    private static $passphrase = 'joashp';
    // (Windows Phone 8) The name of our push channel.
    private static $channelName = "joashp";

    // Change the above three vriables as per your app.

    public function send($to, $message) {
        $fields = array(
            'to' => $to,
            'data' => $message,
        );
        return $this->sendPushNotification($fields);
    }

    // Sending message to a topic by topic name
    public function sendToTopic($to, $message) {
        $fields = array(
            'to' => '/topics/' . $to,
            'data' => $message,
        );
        return $this->sendPushNotification($fields);
    }

    // sending push message to multiple users by firebase registration ids
    public function sendMultiple($registration_ids, $message) {
        $fields = array(
            'to' => $registration_ids,
            'data' => $message,
        );

        return $this->sendPushNotification($fields);
    }
    // Sends Push notification for Android users
    public function sendPushNotification($fields) {
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array(
            'Authorization: key=' . self::$API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);

        return $result;
    }

    public function sendPushGCM ($fields) {
        // Set POST variables
        $url = 'https://gcm-http.googleapis.com/gcm/send';

        $headers = array(
            'Authorization: key=' . self::$API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);

        return $result;
    }


    // Sends Push notification for iOS users
    public function iOS($data, $devicetoken) {

        $deviceToken = $devicetoken;

        $ctx = stream_context_create();
        // ck.pem is your certificate file
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', self::$passphrase);

        $err = null;
        // Open a connection to the APNS server
        $fp = stream_socket_client(
            'ssl://gateway.sandbox.push.apple.com:2195', $err,
            $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        // Create the payload body
        $body['aps'] = array(
            'alert' => array(
                'title' => $data['mtitle'],
                'body' => $data['mdesc'],
            ),
            'sound' => 'default'
        );

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        // Close the connection to the server
        fclose($fp);

        if (!$result)
            return 'Message not delivered' . PHP_EOL;
        else
            return 'Message successfully delivered' . PHP_EOL;

    }

}
class push {

    // push message title
    private $title;
    private $message;
    private $image;
    // push message payload
    private $data;
    // flag indicating whether to show the push
    // notification or not
    // this flag will be useful when perform some opertation
    // in background when push is recevied
    private $is_background;
    private $flag;
    private $type;
    private $id;
    private $group_id;
    private $icon;
    private $sendid;
    private $sendtoid;
    private $imagetype;

    function __construct() {

    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function setImage($imageUrl) {
        $this->image = $imageUrl;
    }
    public function setImageType($ext) {
        $this->imagetype = $ext;
    }

    public function setPayload($data) {
        $this->data = $data;
    }

    public function setFlag($flag){
        $this->flag = $flag;
    }

    public function setIsBackground($is_background) {
        $this->is_background = $is_background;
    }
    public function setType($type) {
        $this->type = $type;
    }
    public function setID($id) {
        $this->id = $id;
    }
    public function setGroupID($group_id) {
        $this->group_id = $group_id;
    }
    public function setIcon($icon) {
        $this->icon = $icon;
    }
    public function setSendID($sendid) {
        $this->sendid = $sendid;
    }
    public function setSendToID($sendtoid) {
        $this->sendtoid = $sendtoid;
    }

    public function getPush() {
        $res = array();
        $res['notification']['title'] = $this->title;
        $res['notification']['is_background'] = $this->is_background;
        $res['notification']['message'] = $this->message;
        $res['notification']['image'] = $this->image;
        $res['notification']['icon'] = $this->icon;
        $res['notification']['payload'] = $this->data;
        $res['notification']['flag'] = $this->flag;
        $res['notification']['timestamp'] = date('Y-m-d G:i:s');
        $res['data']['noti_type'] = (string) $this->type;
        $res['data']['noti_id'] = (string) $this->id;
        $res['data']['group_id'] = (string) $this->group_id;
        $res['data']['sendtoid'] = (int) $this->sendtoid;
        $res['data']['sendid'] = (int) $this->sendid;
        if($this->image) {
            $res['data']['mediaUrl'] = (string) $this->image;
            $res['data']['mediaType'] = (string) $this->imagetype;
        }
        else {
            $res['data']['mediaUrl'] = (string) '';
            $res['data']['mediaType'] = (string) '';
        }
        return $res;
    }
}
?>