<?php

/**
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Joomla Authentication plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	Authentication.joomla
 * @since 1.5
 */

function utf8ize($d) {
    if (is_array($d)) {
        foreach ($d as $k => $v) {
            unset($d[$k]);
            $d[utf8ize($k)] = utf8ize($v);
        }
    } else if (is_object($d)) {
        $objVars = get_object_vars($d);
        foreach($objVars as $key => $value) {
            $d->$key = utf8ize($value);
        }
    } else if (is_string ($d)) {
        return mb_convert_encoding($d,"UTF-8","auto");
    }
    return $d;
}

class plgCommunityGroupsapi extends JPlugin {

    /**
     * This method should handle any authentication and report back to the subject
     *
     * @access	public
     * @param	array	Array holding the user credentials
     * @param	array	Array of extra options
     * @param	object	Authentication response object
     * @return	boolean
     * @since 1.5
     */
    function plgCommunityGroupsapi(&$subject, $params) {
        parent::__construct($subject, $params);
        $this->loadLanguage();
    }

    function onCommunityAPI(&$type, &$resource, &$response, &$sorting, &$action) {
        $my = CFactory::getUser();
        $jinput = JFactory::getApplication()->input;
        $response = array();

        if ($my->id != 0) {
            if ($resource == 'groups' && $type == 'api') { //begin groups
                // list all groups
                if ($action == 'allgroups') {

                    $categoryId = JRequest::getInt('categoryid', 0);
                    $limit = JRequest::getInt('limit', 0);
                    $page = JRequest::getInt('page', 0);
                    $category = JTable::getInstance('GroupCategory', 'CTable');
                    $category->load($categoryId);
                    $response = $this->getAllGroupsApi($categoryId, $sorting, $limit, $page);
                }
                // list my groups
                else if ($action == 'mygroups') {
                    $response = $this->getMyGroups($my->id, $sorting);
                }
                // list my social circle
                else if ($action == 'mysocialcircle') {
                    $limit = $jinput->get('limit', 0, 'POST');
                    $page = $jinput->get('page', 0, 'POST');
                    $response = $this->getMySocialCircles($my->id, $sorting, $page, $limit);
                }
                // list my learning circle
                else if ($action == 'mylearningcircle') {
                    $limit = $jinput->get('limit', 0, 'POST');
                    $page = $jinput->get('page', 0, 'POST');
                    $response = $this->getMyLearningCircles($my->id, $sorting, $page, $limit);
                }
                // list my LP circles
                else if ($action == 'mylpcircles') {
                    $limit = $jinput->get('limit', 0, 'POST');
                    $page = $jinput->get('page', 0, 'POST');
                    $response = $this->getMyLPCircles($my->id, $sorting, $page, $limit);
                }
                // list sub-circle (child groups)
                elseif($action == 'list_subcircle') {
                    $parentid = $jinput->get('parentid', 0, 'GET');
                    $response = $this->listSubCircle($parentid);
                }
                elseif($action == 'circleTree') {
                    $parentid = $jinput->get('parentid', 0, 'POST');
                    $response = $this->circleTreeData($parentid);
                }
                elseif($action == 'own_circle') {
                    $username = $jinput->get('username', '', 'POST');
                    $limit = $jinput->get('limit', 0, 'POST');
                    $page = $jinput->get('page', 0, 'POST');
                    $response = $this->getOwnCircle($username, $page, $limit);
                }
                elseif($action == 'getallchat') {
                    $username = $jinput->get('username', '', 'POST');
                    $response = $this->getAllChats($username);
                }
                // data home page
                else if ($action == 'homepage') {
                    $username = JRequest::getCmd('username', '', 'GET');
                    $response = $this->getDataHomePage($username);
                }
                // search group with keyword or category
                else if ($action == 'search') {

                    $search = $jinput->get('keyword', '', 'GET');
                    $search_type = $jinput->get('search_type', '', 'GET');
                    $search_page = $jinput->get('page', '', 'GET');
                    $limit = $jinput->get('limit', '', 'GET');

                    $response = $this->searchGroups($search, $search_type, $my->id, $search_page, $limit);
                }
                elseif($action == 'search_course') {
                    $keyword = $jinput->get('keyword', '', 'POST');
                    $limit = $jinput->get('limit', 0, 'POST');
                    $page = $jinput->get('page', 0, 'POST');
                    $response = $this->searchCourses($keyword, $page, $limit);
                }

                // Request join to group
                else if ($action == 'join_group') {
                    $groupId = JRequest::getCmd('groupid', 0, 'POST');
                    $response = $this->joinGroup($groupId);
                }
                // view my group
                elseif ($action == 'viewmygroup') {

                    $groupid = JRequest::getCmd('groupid', 0, 'GET');
                    $response = $this->viewMyGroup($groupid);
                }
                elseif ($action == 'viewgroup_pre_joining') {
                    $groupId = JRequest::getCmd('groupid', 0, 'GET');
                    // check group public or not
                    $response = $this->viewGruopPre($groupId);
                }
                elseif($action == 'circle_header') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $username = $jinput->get('username', '', 'POST');
                    $response = $this->headerCircle($groupid, $username);
                }
                // Create new circle
                elseif($action == 'newcircle') {
                    $title = $jinput->get('title', '', 'POST');
                    $about = $jinput->get('about', '', 'POST');
                    $keywords = $jinput->get('keywords', '', 'POST');
                    $privacy_public = $jinput->get('privacy_public', 0, 'POST');
                    $privacy_private = $jinput->get('privacy_private', 0, 'POST');
                    $privacy_cecret = $jinput->get('privacy_cecret', 0, 'POST');
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $parentid = $jinput->get('parentid', 0, 'POST');
                    $cover = $jinput->files->get('cover');
                    $avatar = $jinput->files->get('avatar');
                    $categoryid = 0;
                    $response = $this->createCircle($title, $about, $keywords, $privacy_public, $privacy_private, $groupid, $categoryid, $email = '', $parentid, $privacy_cecret, $cover, $avatar);
                }
                // get course publish to circle
                elseif($action == 'course_publish_circle') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $response = $this->getCoursePublishCircle($groupid);
                }
                elseif($action == 'course_certificate') {
                    $courseid = $jinput->get('courseid', 0, 'POST');
                    $username = $jinput->get('username', '', 'POST');
                    $response = $this->getCourseCertificate($courseid, $username);
                }
                // List my group members
                elseif ($action == 'group_member') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $page = $jinput->get('page', 0, 'POST');
                    $limit = $jinput->get('limit', 0, 'POST');
                    $response = $this->getMembersGroup($groupid, $page, $limit);
                }
                elseif($action == 'group_manage') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $response = $this->getManageMembers($groupid);
                }
                elseif($action == 'member_circle_published') {
                    // groupid is: 12,13,14...
                    $groupid = $jinput->get('groupid', '', 'POST');
                    $page = $jinput->get('page', 0, 'POST');
                    $limit = $jinput->get('limit', 0, 'POST');
                    $response = $this->getMembersGroupPublished($groupid, $page, $limit);
                }
                // view discussion
                elseif ($action == 'view_discussion') {
                    $groupid = JRequest::getCmd('groupid', 0, 'GET');
                    $response = $this->viewGroupDiscussion($groupid);
                }
                // post reply discussion
                elseif ($action == 'post_reply_discussion') {
                    $discussion_id = $jinput->get('discussion_id', 0, 'POST');
                    $content = $jinput->get('content', '', 'POST');
                    $type_reply = $jinput->get('type_reply', '', 'POST');

                    $upload = false;
                    $platform = '';
                    if($_FILES['content']['tmp_name']) {
                        $content = $jinput->files->get('content');
                        $upload = true;
                    }
                    $response = $this->postReplyDiscussion($discussion_id, $content, $upload, $type_reply, $platform);
                }
                // view my group update
                elseif ($action == 'mygroupupdate') {
                    $userid = JRequest::getCmd('userid', '', 'GET');
                    $response = $this->getViewGroupUpdate($userid);
                }
                // view viewbulletins
                elseif ($action == 'announcement_circle') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $response = $this->getViewBulletins($groupid);
                }
                // add discussions
                elseif ($action == 'add_discussion') {
                    $groupid = $jinput->get('groupid', '', 'POST');
                    $title = $jinput->get('title', '', 'POST');
                    $content = $jinput->get('content', '', 'POST');
                    $notify_me = $jinput->get('notify_me', 0, 'POST');
                    $topicid = $jinput->get('topicid', 0, 'POST');
                    $topic_avatar = $jinput->files->get('topic_avatar');
                    $response = $this->saveDiscussionGroup($groupid, $title, $content, $notify_me, $topicid, $topic_avatar);
                }
                // get notifications
                elseif ($action == 'notification') {
                    $response = $this->getNotification();
                }
                // count notifications
                elseif ($action == 'count_notification') {
                    $response = $this->countNotification();
                }
                // read all notification
                elseif($action == 'readAllNotification') {
                    $response = $this->readAllNotification();
                }
                // read only once notification
                elseif($action == 'readNotification') {
                    $notification_id = $jinput->get('notification', 0, 'POST');
                    $response = $this->readNotification($notification_id);
                }

                // leaved group
                elseif ($action == 'leavegroup') {
                    $groupid = JRequest::getCmd('groupid', 0, 'POST');
                    $response = $this->leaveGroupapi($groupid);
                }
                // view discussion
                elseif ($action == 'viewtopicdiscussion') {
                    $groupid = $jinput->get('groupid', 0, 'GET');
                    $topicid = $jinput->get('topicid', 0, 'GET');
                    $response = $this->viewDiscussionTopic($groupid, $topicid);
                }
                // delete topic
                elseif($action == 'deletetopic') {
                    $topicid = $jinput->get('topicid', 0, 'POST');
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $response = $this->deleteTopic($topicid, $groupid);
                }
                elseif($action == 'listtopic') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $username = $jinput->get('username', '', 'POST');
                    $response = $this->lisTopicOfCircle($groupid, $username);
                }
                // add member
                elseif($action == 'addmember') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $member = $jinput->get('member', '', 'POST');
                    $response = $this->addMember($groupid, $member);
                }
                elseif($action == 'lastreply') {
                    $id = $jinput->get('messageid', 0, 'POST');
                    $response = $this->getLastReplybyID($id);
                }
                elseif($action  == 'assignNormalCircleManage') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $member = $jinput->get('member', '', 'POST');
                    $response = $this->assignNormalCircleManage($groupid, $member);
                }
                elseif($action == 'assignManage') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $data = $jinput->get('data', '', 'POST');
                    $response = $this->assignManage($groupid, $data);
                }
                elseif($action == 'assign') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $data = $jinput->get('data', '0', 'POST');
                    $username = $jinput->get('username', '', 'POST');
                    $type = $jinput->get('type', '', 'POST');
                    $response = $this->assign($groupid, $data, $username, $type);
                }
                // make circle admin
                elseif($action == 'make_revert_circleadmin') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $memberid = $jinput->get('memberid', 0, 'POST');
                    $type = $jinput->get('type', '', 'POST');
                    $role = $jinput->get('role', '', 'POST');
                    $response = $this->makeORrevertAdminCircle($groupid, $memberid, $type, $role);
                }
                // remove member
                elseif($action == 'remove_member') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $memberid = $jinput->get('memberid', 0, 'POST');
                    $ban = $jinput->get('ban', 0, 'POST');
                    $response = $this->removeMemberCircle($groupid, $memberid, $ban);
                }
                // ban member
                elseif($action == 'ban_unban_member') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $memberid = $jinput->get('memberid', 0, 'POST');
                    $type = $jinput->get('type', 0, 'POST');
                    $response = $this->banUnbanMember($groupid, $memberid, $type);
                }
                elseif($action == 'all_request_join_circle') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $username = $jinput->get('username', '', 'POST');
                    $page = $jinput->get('page', 0, 'POST');
                    $limit = $jinput->get('limit', 0, 'POST');
                    $response = $this->allRequestJoinCircle($groupid, $username, $page, $limit);
                }
                elseif($action == 'delete_request_join') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $userid = $jinput->get('userid', '', 'POST');
                    $response = $this->removeRequestJoin($groupid, $userid);
                }

                // lock topic
                elseif($action == 'locktopic') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $topicid = $jinput->get('topicid', 0, 'POST');
                    $status = $jinput->get('status', 0, 'POST');
                    $response = $this->lockTopic($groupid, $topicid, $status);
                }
                // load all friend not in circle
                elseif($action == 'loadfriend') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $response = $this->loadFriend($groupid);
                }
                // event api
                elseif($action == 'eventview') {
                    $groupid = $jinput->get('groupid', 0, 'GET');
                    $eventid = $jinput->get('eventid', 0, 'GET');
                    $response = $this->ViewEvent($groupid, $eventid);
                }
                // new event
                elseif($action == 'newevent') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $title = $jinput->get('title', '', 'POST');
                    $about_event = $jinput->get('about', '', 'POST');
                    $startdate = $jinput->get('startdate', '', 'POST');
                    $starttime = $jinput->get('starttime', '', 'POST');
                    $enddate = $jinput->get('enddate', '', 'POST');
                    $endtime = $jinput->get('endtime', '', 'POST');
                    $location = $jinput->get('location', '', 'POST');
                    $eventid = $jinput->get('eventid', 0, 'POST');
                    $event_avatar = $jinput->files->get('event_avatar');
                    $response = $this->addEvent($groupid, $title, $about_event, $startdate, $starttime, $enddate, $endtime, $location, $eventid, $event_avatar);
                }
                // delete event
                elseif($action == 'delete_event') {
                    $eventid = $jinput->get('eventid', 0, 'POST');
                    $response = $this->deleteEvent($eventid);
                }
                elseif($action == 'group_event') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $response = $this->getEventsGroup($groupid, null);
                }

                // upload avatar
                // 1 - event avatar; 2 - profile avatar; 3 - group avatar
                elseif($action == 'avatar_photo') {
                    $id = $jinput->get('id', 0, 'POST');
                    $photo = $jinput->files->get('photo');
                    $type = $jinput->get('type', '', 'POST');
                    $response = $this->uploadAvatar($id, $photo, $type);
                }
                // upload cover photo
                elseif($action == 'cover_photo') {
                    $id = $jinput->get('id', 0, 'POST');
                    $cover = $jinput->files->get('photo');
                    $type = $jinput->get('type', '', 'POST');
                    $response = $this->uploadCover($id, $cover, $type);
                }
                // Going event
                elseif($action == 'going_event') {
                    $id = $jinput->get('id', 0, 'POST');
                    $username = $jinput->get('username', '', 'POST');
                    $status = $jinput->get('status', 0, 'POST');
                    $response = $this->goingEvent($id, $username, $status);
                }
                // My event
                elseif($action == 'myevent') {
                    $response = $this->myEvent();
                }
                // List Friend
                elseif($action == 'myfriend') {
                    $courseid = $jinput->get('course', 0, 'GET');
                    $categoryid = $jinput->get('categoryid', 0, 'GET');
                    $response = $this->myFriends($courseid, $categoryid);
                }
                // List all friend
                elseif($action == 'allfriend') {
                    $limit = $jinput->get('limit', 0, 'POST');
                    $page = $jinput->get('page', 0, 'POST');
                    $response = $this->allFriend($limit, $page);
                }
                elseif($action == 'friend_request') {
                    $username = $jinput->get('username', '', 'POST');
                    $response = $this->getFriendRequest($username);
                }
                // Search People
                elseif($action == 'searchfriend') {
                    $keyword = $jinput->get('keyword', '', 'POST');
                    $type_search = $jinput->get('type_search', '', 'POST');
                    $page = $jinput->get('page', '', 'POST');
                    $limit = $jinput->get('limit', '', 'POST');
                    $response = $this->searchFriend($keyword, $type_search, $page, $limit);
                }
                // add Friend
                elseif($action == 'addfriend') {
                    $userid = $jinput->get('userid', 0, 'POST');
                    $message = $jinput->get('message', '', 'POST');
                    $response = $this->addFriend($userid, $message);
                }
                // approve and reject friend
                elseif($action == 'approvedfriend') {
                    $friendid = $jinput->get('friendid', 0, 'POST');
                    $type_request = $jinput->get('type_request', '', 'POST');
                    $response = $this->approvedFriend($friendid, $type_request);
                }
                // block friend
                elseif($action == 'removefriend') {
                    $friendid = $jinput->get('friendid', 0, 'POST');
                    $block  = $jinput->get('block', 0, 'POST');
                    $response = $this->removeFriend($friendid, $block);
                }
                elseif($action == 'block_unblock_friend') {
                    $friendid = $jinput->get('friendid', 0, 'POST');
                    $status = $jinput->get('status', '', 'POST');
                    $response = $this->blockUnblockFriend($friendid, $status);
                }
                // Announcement
                elseif($action == 'addAnnouncement') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $message = $jinput->get('message', '', 'POST');
                    $enddate = $jinput->get('enddate', '', 'POST');
                    $endtime = $jinput->get('endtime', '', 'POST');
                    $announcementid = $jinput->get('announcementid', 0, 'POST');
                    $timezone = $jinput->get('timezone', '', 'POST');
                    $response = $this->addAnnouncement($groupid, $message, $enddate, $endtime, $announcementid, $timezone);
                }
                // Delete announcement
                elseif($action == 'deleteAnnouncement') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $announcementid = $jinput->get('announcementid', 0, 'POST');
                    $response = $this->deleteAnnouncement($groupid, $announcementid);
                }

                // Part 2: Learning Provider
                elseif($action == 'lp_register') {
                    $lp_circle_name = $jinput->get('lp_circle_name', '', 'POST');
                    $lp_organisation = $jinput->get('lp_organisation', '', 'POST');
                    $lp_owner_name = $jinput->get('lp_owner_name', '', 'POST');
                    $lp_circle_about = $jinput->get('lp_circle_about', '', 'POST');
                    $lp_keyword = $jinput->get('lp_keyword', '', 'POST');
                    $lp_email_add = $jinput->get('lp_email_add', '', 'POST');
                    $lp_home_add = $jinput->get('lp_home_add', '', 'POST');
                    $lp_postal_code = $jinput->get('lp_postal_code', '', 'POST');
                    $lp_id = $jinput->get('lp_id', 0, 'POST');
                    $response = $this->LP_register($lp_circle_name, $lp_organisation, $lp_owner_name, $lp_circle_about, $lp_keyword, $lp_email_add, $lp_home_add, $lp_postal_code, $lp_id, null, null);
                }
                elseif($action == 'lp_edit') {
                    $lp_circle_name = $jinput->get('lp_circle_name',  '', 'POST');
                    $lp_circle_about = $jinput->get('lp_circle_about', '', 'POST');
                    $lp_circle_keyword = $jinput->get('lp_keyword', '', 'POST');
                    $lp_circle_email = $jinput->get('lp_email', '', 'POST');
                    $lp_circle_address = $jinput->get('lp_address', '', 'POST');
                    $lp_circle_postal = $jinput->get('lp_postal_code', '', 'POST');
                    $lp_circle_avatar = $jinput->files->get('avatar');
                    $lp_circle_cover = $jinput->files->get('cover');
                    $lp_id = $jinput->get('lp_id', 0, 'POST');
                    $response = $this->LP_register($lp_circle_name, null, null, $lp_circle_about, $lp_circle_keyword, $lp_circle_email, $lp_circle_address, $lp_circle_postal, $lp_id, $lp_circle_avatar, $lp_circle_cover);
                }
                elseif($action == 'lp_list') {
                    $limit = $jinput->get('limit', 0, 'POST');
                    $page = $jinput->get('page', 0, 'POST');
                    $response = $this->LP_list($limit, $page);
                }
                elseif($action == 'lp_about') {
                    $lp_circle_id = $jinput->get('lp_circle_id', 0, 'POST');
                    $response = $this->LP_about($lp_circle_id);
                }
                elseif($action == 'get_course_learningprovider') {
                    $username = $jinput->get('username', '', 'POST');
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $response = $this->getCourseOfLP($username, $groupid);
                }
                elseif($action == 'test_push') {
                    $device = $jinput->get('device', '', 'POST');
                    $message = $jinput->get('message', '', 'POST');
                    $title = $jinput->get('title', '', 'POST');
                    $response = $this->push_notification($device, $title, $message);
                }
                elseif ($action == 'countNotification') {
                    $username = $jinput->get('username', '', 'POST');
                    $response = $this->countNotifications($username);
                }
                // addition for only BLN
                // Added 06 Sept 2018 by kydonvn
                elseif($action == 'archived') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $type = $jinput->get('type', '', 'POST');
                    $response = $this->archiveCircle($groupid, $type);
                }
                elseif($action == 'remove_circle') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $response = $this->removeCircle($groupid);
                }
                elseif($action == 'listarchive') {
                    $limit = $jinput->get('limit', 0, 'POST');
                    $page = $jinput->get('page', 0, 'POST');
                    $response = $this->getCircleArchive($my->id, $page, $limit);
                }
                // nothing action
                else {
                    $response['status'] = false;
                    $response['error_message'] = 'system can not be found action';
                }
            }
            // view profiles members
            else if ($resource == 'profile' && $type == 'api') { // begin view profile members
                if ($action == 'viewprofile') {
                    $userid = JRequest::getCmd('userid', 0, 'GET');
                    $response = $this->getProfile($userid);
                }
                // edit profile
                elseif ($action == 'update_profile') {
                    $userid = $jinput->get('userid', 0, 'POST');
                    $fullname = $jinput->get('fullname', '', 'POST');
                    $aboutme = $jinput->get('aboutme', '', 'POST');
                    $position = $jinput->get('position', '', 'POST');
                    $interests = $jinput->get('interests', '', 'POST');
                    $response = $this->updateProfile($userid,$fullname, $aboutme, $position, $interests);
                }
                // edit account
                elseif($action == 'update_account') {
                    $userid = $jinput->get('userid', 0, 'POST');
                    $name = $jinput->get('name', 0, 'POST');
                    $profile = $jinput->get('profile', '', 'POST');
                    $interests = $jinput->get('interests', '', 'POST');
                    $fullname = $jinput->get('fullname', '', 'POST');
                    $email = $jinput->get('email', '', 'POST');
                    $old_pwd = $jinput->get('old_password', '', 'POST');
                    $new_pwd = $jinput->get('new_password', '', 'POST');
                    $renew_pwd = $jinput->get('renew_password', '', 'POST');
                    $number = $jinput->get('number', '', 'POST');
                    $address = $jinput->get('address', '', 'POST');
                    $response = $this->updateAccount($userid, $fullname, $email, $old_pwd, $new_pwd, $renew_pwd, $number, $address, $name, $profile, $interests);
                }
                elseif($action == 'listPreferences') {
                    $userid = $jinput->get('userid', 0, 'POST');
                    $response = $this->listPreferences($userid);
                }
                // update preferences
                elseif($action == 'preferences') {
                    $pref_addme_friend = $jinput->get('pref_addme_friend', 0, 'POST');
                    $pref_accept_myinvite_friend = $jinput->get('pref_accept_myinvite_friend', 0, 'POST');
                    $pref_invite_join_circle = $jinput->get('pref_invite_join_circle', 0, 'POST');
                    $pref_add_comment_topic = $jinput->get('pref_add_comment_topic', 0, 'POST');
                    $pref_approve_me_member_circle = $jinput->get('pref_approve_me_member_circle', 0, 'POST');
                    $response = $this->updatePreferences($pref_addme_friend, $pref_accept_myinvite_friend, $pref_invite_join_circle, $pref_add_comment_topic, $pref_approve_me_member_circle);
                }

            }
            // new post wall
            else if ($resource == 'wall' && $type == 'api') {
                if ($action == 'newpostwall') {
                    $user_id = JRequest::getCmd('userid', 0, 'POST');
                    $content = JRequest::getString('message', '', 'POST');
                    $type = JRequest::getString('type', 'message', 'POST');
                    if ($type == 'photo') {
                        $file = $jinput->files->get('file');
                    } elseif ($type == 'video') {
                        $file = JRequest::getString('file', '', 'POST');
                    }
                    $response = $this->postWall($user_id, $content, $type, $file);
                }
                // edit status
                elseif ($action == 'editstatus') {
                    $user_id = JRequest::getCmd('userid', 0, 'POST');
                    $id = JRequest::getCmd('activityid', 0, 'POST');
                    $content = JRequest::getString('message', '', 'POST');
                    $type = JRequest::getString('type', 'message', 'POST');
                    $response = $this->editStatus($user_id, $id, $content, $type);
                }
                // get data social feeds
                elseif ($action == 'newshomepage') {
                    $username = JRequest::getString('username', '', 'POST');
                    $limit = JRequest::getCmd('limit', 0, 'POST');
                    $page = JRequest::getCmd('page', 0, 'POST');
                    $response = $this->getNewsFeedHomePage($username, $limit, $page);
                }
                // hide activity
                elseif ($action == 'hideactivity') {
                    $username = JRequest::getString('username', '', 'POST');
                    $activityId = JRequest::getCmd('activityid', '', 'POST');
                    $response = $this->hideActivity($activityId, $username);
                }
                // delete activity
                elseif ($action == 'deleteactivity') {
                    $username = JRequest::getString('username', '', 'POST');
                    $activityId = JRequest::getCmd('activityid', '', 'POST');
                    $response = $this->deleteActivity($activityId, $username);
                }
            } else {
                $response['status'] = false;
                $response['error_message'] = 'system can not be found action. Please check Url';
            }
        } else {
            if($action == 'allgroups_nologin') {
                $limit = JRequest::getInt('limit', 0);
                $page = JRequest::getInt('page', 0);
                $category = JTable::getInstance('GroupCategory', 'CTable');
                $category->load($categoryId);
                $response = $this->getAllGroupsApi($categoryId, $sorting, $limit, $page);
            }// view a group pre-joining with group id
            elseif ($action == 'viewgroup_pre_joining') {
                $groupId = JRequest::getCmd('groupid', 0, 'GET');
                // check group public or not
                $response = $this->viewGruopPre($groupId);
            }
            elseif($action == 'login_template') {
                $response = $this->loginTemplate();
            }
            else {
                $response['status'] = false;
                $response['error_message'] = 'please login first';
            }
        }

        if ($action == 'userid') {
            $username = $_GET['username'];//JRequest::getCmd('username', '', 'GET');
            $response = $this->getUserid($username);
        }
        header('Content-type: application/json; charset=UTF-8');
        $respondse_standar = json_encode($response);
        if(json_last_error() == 5){
            $respondse_clean = utf8ize($response);
            echo json_encode($respondse_clean);
            die;
        } else {
            echo $respondse_standar;
            die();
        }
    }
    /**
     * @param $mixed
     * @return array|string
     */


    public function getAllGroupsApi($category, $sorting, $limit, $page) {
        require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');

        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $model = CFactory::getModel('groups');

        // Get group in category and it's children.
        $categories = $model->getAllCategories();
        $categoryIds = CCategoryHelper::getCategoryChilds($categories, $category);
        if ((int) $category > 0) {
            $categoryIds[] = (int) $category;
        }
        $eventsModel = CFactory::getModel('Events');
        // Get pagination object
        $data->pagination = $model->getPagination();
        // It is safe to pass 0 as the category id as the model itself checks for this value.
        $groups = $model->getAllGroupsMobile($categoryIds, $sorting, null, $limit, false, $page);
        if ($groups) {
            foreach ($groups as $k => $r) {
                // get category name
                if ($r->categoryname == '') {
                    $category = JTable::getInstance('GroupCategory', 'CTable');
                    $category->load($r->categoryid);

                    $groups[$k]->categoryname = (string) $category->name;
                }
                $groups[$k]->lastaccess = (string) $r->lastaccess;
                // get group admin name
                $user_owner = '';
                if ($r->ownername == '') {
                    $user_owner = CFactory::getUser($r->ownerid);

                    $groups[$k]->ownername = $user_owner->getDisplayName();
                }

                if ($r->avatar && $r->avatar != "")
                    $groups[$k]->avatar = $root.'/'.$r->avatar;
                $groups[$k]->activity_count = My_Groups::groupActivities($r->id);
                // count group event
                $totalEvents = $eventsModel->getTotalGroupEvents($r->id);
                $groups[$k]->eventCount = $totalEvents;
                $groups[$k]->group_type = 'social_circle';
            }
            $data = new StdClass;
            $data->status = true;
            $data->message = 'Get All Circle success';
            $data->groups = $groups;
        } else {
            $data = new StdClass;
            $data->status = false;
            $data->message = 'Get All Circle failed';
            $data->groups = $groups;
        }


        return $data;
    }

    public function getMyGroups($userid, $sorted) {
        $groups = $this->getGroup($userid, $sorted);
        $data = new stdClass();
        if ($groups) {
            $data->status = true;
            $data->message = 'Get My Circle success.';
            $data->groups = $groups;
        } else {
            $data->status = false;
            $data->message = 'Get My Circle failed';
            $data->groups = $groups;
        }
        return $data;
    }

    public function getMySocialCircles($userid, $sorted, $page, $limit) {
        require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');
        $groupsModel = CFactory::getModel('groups');
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $socialcircle = 0;
        if ($page && $page > 0) {
            $limitstart = ($page - 1) * $limit;
        } else {
            $limitstart = null;
        }
        $groups = $groupsModel->getCircles($userid, 1, $limitstart, $limit, $socialcircle, 'member');

        $eventsModel = CFactory::getModel('Events');
        $groups_arr = array();
        foreach ($groups as $k => $r) {
            $table = JTable::getInstance('Group', 'CTable');
            $table->load($r->id);

            $groups[$k]->id = $r->id;
            $groups[$k]->ownerid = $r->ownerid;
            $groups[$k]->name = $r->name;
            $groups[$k]->description = $r->description;
            $groups[$k]->summary = $r->summary;
            $groups[$k]->created = $r->created;

            if ($r->avatar == '') {
                $groups[$k]->avatar = '';
            } else {
                $groups[$k]->avatar = $table->getOriginalAvatar();
            }

            $groups[$k]->membercount = $r->membercount;
            $groups[$k]->discusscount = $r->discusscount;
            $groups[$k]->params = $r->params;
            $groups[$k]->lastaccess = (string) $r->lastaccess;
            $groups[$k]->parentid = $r->parentid;
            $groups[$k]->unlisted = $r->unlisted;
            $groups[$k]->approvals = $r->approvals;

            $isMember = $groupsModel->isMember($userid, $r->id);
            $groups[$k]->ismemberin = $isMember;
            // get category name
            if ($r->categoryname == '') {
                $category = JTable::getInstance('GroupCategory', 'CTable');
                $category->load($r->categoryid);

                $groups[$k]->categoryname = (string)$category->name;
            }

            $group->approvals = $r->approvals;
            // get group admin name
            if ($r->ownerid > 0) {
                $user_owner = CFactory::getUser($r->ownerid);

                $groups[$k]->ownername = $user_owner->getDisplayName();
            }

            $groups[$k]->activity_count = My_Groups::groupActivities($r->id);
            // count group event
            $totalEvents = $eventsModel->getTotalGroupEvents($r->id);
            $groups[$k]->eventCount = $totalEvents;
            $groups[$k]->isFacilitator = (boolean)false;
            $groups[$k]->group_type = 'social_circle';
            $groups[$k]->remoteid = 0;
        }
//        $groups_result = array_values($groups);
//        $groups = $this->getGroup($userid, $sorted, 0, $page, $limit);
        $data = new stdClass();
        if ($groups) {
            $data->status = true;
            $data->message = 'Get My Circle success.';
            $data->circlecount = $groupsModel->getCirclesCount($userid, $socialcircle,'member', false);
            $data->groups = $groups;
        } else {
            $data->status = false;
            $data->message = 'Get My Circle failed';
            $data->circlecount = 0;
            $data->groups = array();
        }
        return $data;
    }

    public function getOwnCircle($username, $page, $limit)
    {
        require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');
        $groupsModel = CFactory::getModel('groups');
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');


        $userid = JUserHelper::getUserId($username);
        $my = CFactory::getUser($userid);
        $socialcircle = 0;
        if ($page && $page > 0) {
            $limitstart = ($page - 1) * $limit;
        } else {
            $limitstart = null;
        }
        $canCreate = true;
        if($my->_permission) {
            $canCreate = false;
        }
        $groups = $groupsModel->getCircles($my->id, true, $limitstart, $limit, $socialcircle, 'owner', false);
        $eventsModel = CFactory::getModel('Events');
        foreach ($groups as $k => $r) {
            $table = JTable::getInstance('Group', 'CTable');
            $table->load($r->id);
            $groups[$k]->id = $r->id;
            $groups[$k]->ownerid = $r->ownerid;
            $groups[$k]->name = $r->name;
            $groups[$k]->description = $r->description;
            $groups[$k]->summary = $r->summary;
            $groups[$k]->created = $r->created;

            // check circle has subcircle
            $groups[$k]->hasSubcircle = false;
            $sub = $groupsModel->checkSubgroup($r->id);
            if($sub) {
                $groups[$k]->hasSubcircle = true;
            }
            if ($r->avatar == '') {
                $groups[$k]->avatar = '';
            } else {
                $groups[$k]->avatar = $table->getOriginalAvatar();
            }

            $groups[$k]->membercount = $r->membercount;
            $groups[$k]->discusscount = $r->discusscount;
            $groups[$k]->params = $r->params;
            $groups[$k]->lastaccess = (string) $r->lastaccess;
            $groups[$k]->parentid = $r->parentid;
            $groups[$k]->unlisted = $r->unlisted;
            $groups[$k]->approvals = $r->approvals;

            $isMember = $groupsModel->isMember($userid, $r->id);
            $groups[$k]->ismemberin = $isMember;
            $groups[$k]->params = $r->params;
            // get category name
            if ($r->categoryid == '') {
                $category = JTable::getInstance('GroupCategory', 'CTable');
                $category->load($r->categoryid);

                $groups[$k]->categoryname = (string)$category->name;
            }

            $groups[$k]->approvals = $r->approvals;
            // get group admin name
            if ($r->ownerid > 0) {
                $user_owner = CFactory::getUser($r->ownerid);

                $groups[$k]->ownername = $user_owner->getDisplayName();
            }

            $groups[$k]->activity_count = My_Groups::groupActivities($r->id);
            // count group event
            $totalEvents = $eventsModel->getTotalGroupEvents($r->id);
            $groups[$k]->eventCount = $totalEvents;
            $groups[$k]->isFacilitator = (boolean)false;
            $groups[$k]->group_type = 'social_circle';
            $groups[$k]->remoteid = 0;
        }
        $data = new stdClass();
        if ($groups) {
            $data->status = true;
            $data->message = 'Get Own circle success.';
            $data->canCreate = $canCreate;
            $data->circlecount = $groupsModel->getCirclesCount($my->id, $socialcircle, 'owner');
            $data->groups = $groups;
        } else {
            $data->status = false;
            $data->message = 'Get own Circle failed';
            $data->canCreate = $canCreate;
            $data->circlecount = 0;
            $data->groups = array();
        }
        return $data;
    }

    public function getMyLearningCircles($userid, $sorted, $page, $limit) {
        require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');
        $groupsModel = CFactory::getModel('groups');
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $coursecircle = 6;
        if ($page && $page > 0) {
            $limitstart = ($page - 1) * $limit;
        } else {
            $limitstart = null;
        }
        $groups = $groupsModel->getCirclescourse($userid, 1, $limitstart, $limit, $coursecircle);

        $eventsModel = CFactory::getModel('Events');
        $groups_arr = array();
        foreach ($groups as $k => $r) {
            $table = JTable::getInstance('Group', 'CTable');
            $table->load($r->id);

            $groups[$k]->id = $r->id;
            $groups[$k]->ownerid = $r->ownerid;
            $groups[$k]->name = $r->name;
            $groups[$k]->description = $r->description;
            $groups[$k]->summary = $r->summary;
            $groups[$k]->created = $r->created;

            if ($r->avatar == '') {
                $groups[$k]->avatar = '';
            } else {
                $groups[$k]->avatar = $table->getOriginalAvatar();
            }

            $groups[$k]->membercount = $r->membercount;
            $groups[$k]->discusscount = $r->discusscount;
            $groups[$k]->params = $r->params;
            $groups[$k]->lastaccess = (string) $r->lastaccess;
            $groups[$k]->parentid = $r->parentid;
            $groups[$k]->unlisted = $r->unlisted;
            $groups[$k]->approvals = $r->approvals;

            $isMember = $groupsModel->isMember($userid, $r->id);
            $groups[$k]->ismemberin = $isMember;
            $groups[$k]->params = $r->params;
            // get category name
            if ($r->categoryname == '') {
                $category = JTable::getInstance('GroupCategory', 'CTable');
                $category->load($r->categoryid);

                $groups[$k]->categoryname = (string)$category->name;
            }

            $groups[$k]->approvals = $r->approvals;
            // get group admin name
            if ($r->ownerid > 0) {
                $user_owner = CFactory::getUser($r->ownerid);

                $groups[$k]->ownername = $user_owner->getDisplayName();
            }

            $groups[$k]->activity_count = My_Groups::groupActivities($r->id);
            // count group event
            $totalEvents = $eventsModel->getTotalGroupEvents($r->id);
            $groups[$k]->eventCount = $totalEvents;
            $groups[$k]->isFacilitator = (boolean)false;
            $is_publish = true;
            $is_course_cirlce = true;
            $groups[$k]->remoteid = 0;
            $params = json_decode($r->params);
            if(isset($params->course_id) && $params->course_id > 0) {
                if(is_object($params->course_id)) {
                    $courseid = $params->course_id->data;
                } else {
                    $courseid = $params->course_id;
                }
                $groups[$k]->remoteid = $courseid;
                require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
                $my = JFactory::getUser($userid);
                if($courseid > 0) {
                    $course_info = JoomdleHelperContent::call_method('get_course_info', (int)$courseid, (string)$my->username);
                    $groups[$k]->course_publish = $course_info['visible'];
                    if($course_info['visible'] == 0) {
                        $is_publish = false;
                    }
                }

//                $check_facilitator = JoomdleHelperContent::call_method('check_facilitator', (int)$courseid, (string)$my->username);
                $groups[$k]->isFacilitator = (boolean) $course_info['facilitatedcourse'];
            } else {
                $is_course_cirlce = false;
            }
            $groups[$k]->group_type = 'course_circle';
            if(!$is_publish || !$is_course_cirlce) {
                unset($groups[$k]);
            }

        }
        $groups_result = array_values($groups);

        $data = new stdClass();
        if ($groups) {
            $data->status = true;
            $data->message = 'Get My Circle success.';
            $data->circlecount = $groupsModel->countgetCirclescourse($userid, $coursecircle, false, false);
            $data->groups = $groups_result;
        } else {
            $data->status = false;
            $data->message = 'Get My Circle failed';
            $data->circlecount = 0;
            $data->groups = array();
        }
        return $data;
    }

    private function getGroup($userid, $sorted, $parentid = 0) {
        require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');
        $groupsModel = CFactory::getModel('groups');

        $categoryid = 0;
        $groups = $groupsModel->getCircles($userid, 0, true, null, $categoryid, '');

        $eventsModel = CFactory::getModel('Events');
        foreach ($groups as $k => $r) {
            $table = JTable::getInstance('Group', 'CTable');
            $table->load($r->id);

            $is_publish = true;
            $params = json_decode($r->params);
            // get category name
            if ($r->categoryname == '') {
                $category = JTable::getInstance('GroupCategory', 'CTable');
                $category->load($r->categoryid);

                $groups[$k]->categoryname = (string)$category->name;
            }
            $groups[$k]->parentid = $r->parentid;
            $groups[$k]->approvals = $r->approvals;
            // get group admin name
            if ($r->ownername == '') {
                $user_owner = CFactory::getUser($r->ownerid);

                $groups[$k]->ownername = $user_owner->getDisplayName();
            }
            if ($r->avatar == '') {
                $groups[$k]->avatar = '';
            } else {
                $groups[$k]->avatar = $table->getOriginalAvatar();
            }
            $groups[$k]->activity_count = My_Groups::groupActivities($r->id);
            // count group event
            $totalEvents = $eventsModel->getTotalGroupEvents($r->id);
            $groups[$k]->eventCount = $totalEvents;
            $groups[$k]->isFacilitator = (boolean)false;
            if(isset($params->course_id) && $params->course_id > 0) {
                $groups[$k]->group_type = 'course_circle';
                if(is_object($params->course_id)) {
                    $courseid = $params->course_id->data;
                } else {
                    $courseid = $params->course_id;
                }
                $groups[$k]->remoteid = $courseid;
                require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
                $my = JFactory::getUser($userid);
                if($courseid > 0) {
                    $course_info = JoomdleHelperContent::call_method('get_course_info', (int)$courseid, (string)$my->username);
                    $groups[$k]->course_publish = $course_info['visible'];
                    if($course_info['visible'] == 0) {
                        $is_publish = false;
                    }
                }

//                    $check_facilitator = JoomdleHelperContent::call_method('check_facilitator', (int)$courseid, (string)$my->username);
//                    $groups[$k]->isFacilitator = (boolean) $check_facilitator->isFacilitator;
                $groups[$k]->isFacilitator = (boolean) $course_info['facilitatedcourse'];
            } else {
                $groups[$k]->group_type = 'social_circle';
                $groups[$k]->remoteid = 0;
            }
            if(!$is_publish) {
                unset($groups[$k]);
            }
        }
        $groups = array_values($groups);
        return $groups;
    }
    public function listSubCircle($parentid) {
        $my = JFactory::getUser();
        $return = $this->getGroup($my->id, '', $parentid);
        $response->status = true;
        $response->message = 'List subcircle';
        $response->groups = $return;
        return $response;
    }

    public function searchGroups($search, $search_type, $userid, $page, $limit) {
        require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $model = CFactory::getModel('groups');
        $data = new stdClass();
        if ((!empty($search))) {
            //            JRequest::checkToken('get') or jexit(JText::_('COM_COMMUNITY_INVALID_TOKEN'));
            if ($page && $page > 0) {
                $position = ($page - 1) * $limit;
            } else {
                $position = 0;
            }
            $appsLib = CAppPlugins::getInstance();
            $saveSuccess = $appsLib->triggerEvent('onFormSave', array('jsform-groups-search'));
            $eventsModel = CFactory::getModel('Events');
            if (empty($saveSuccess) || !in_array(false, $saveSuccess)) {
                $posted = true;
//                    $groups = $model->getAllGroupsMobile(array(0,7), null, $search, $limit, false, $page);
                $my = CFactory::getUser();
                $listgroups = array();
                $mygroups = $model->getCircles($my->id, 0, 0, 0, 0, false);
                if ($mygroups) {
                    foreach ($mygroups as $gr) {
                        $listgroups[] = $gr->id;
                    }
                }

                $groups = $model->getAllGroupsMobile($catId, null, $search, $limit, false, false, true, false, $position, implode(',', $listgroups));

                if ($groups) {
                    foreach ($groups as $k => $r) {
                        $table = JTable::getInstance('Group', 'CTable');
                        $table->load($r->id);
                        // get category name
                        if ($r->categoryid != '') {
                            $category = JTable::getInstance('GroupCategory', 'CTable');
                            $category->load($r->categoryid);

                            $groups[$k]->categoryname = (string) $category->name;
                        }
                        $groups[$k]->categoryname = (string) '';
                        $params = json_decode($r->params);
                        // get group admin name
                        $user_owner = '';
                        if ($r->ownerid != '') {
                            $user_owner = CFactory::getUser($r->ownerid);

                            $groups[$k]->ownername = $user_owner->getDisplayName();
                        }
                        if(empty($groups[$k]->avatar)) {
                            $groups[$k]->avatar = '';
                        } else {
                            $groups[$k]->avatar = $table->getOriginalAvatar();
                        }

                        $groups[$k]->activity_count = My_Groups::groupActivities($r->id);
                        // count group event
                        $totalEvents = $eventsModel->getTotalGroupEvents($r->id);
                        $groups[$k]->eventCount = $totalEvents;

                        $groups[$k]->group_type = 'social_circle';
                        $groups[$k]->remoteid = 0;
                        if (($r->unlisted == 1 && $r->approvals == COMMUNITY_PRIVATE_GROUP) || $r->categoryid == 6) {
                            unset($groups[$k]);  //Unset secret group and course group
                        } //else if (isset(json_decode($group->params)->course_id)) unset($groups[$key]); //Unset Course Group
                    }
                    $data->status = true;
                    $data->message = 'Search Circle success';
                    $data->groupcount = $model->getGroupsSearchTotal();
                    $data->groups = $groups;
                }
                else {
                    $data->status = false;
                    $data->message = 'Search Circle failed';
                    $data->groupcount = $model->getGroupsSearchTotal();
                    $data->groups = $groups;
                }
            }
        } else {
            $data->status = false;
            $data->message = 'Keyword empty';
            $data->groupcount = $model->getGroupsSearchTotal();
            $data->groups = $groups;
        }
        return $data;
    }

    public function viewGruopPre($groupid) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        $bulletinModel = CFactory::getModel('bulletins');
        $bulletins = $bulletinModel->getBulletins($group->id);
        $group->announcementcount = count($bulletins);
        $group->description = strip_tags($group->description);
        //check if group is public or private
        if(($group->unlisted == 0 && $group->approvals == 0)) { // || ($group->unlisted == 0 && $group->approvals == 1)
            return $this->viewMyGroup($group->id);
        } else {

            if ($group->avatar == '') {
                $group->avatar = JURI::base() . 'components/com_community/assets/group.png';
            } else {
                $group->avatar = $root . '/' . $group->avatar;
            }
            // get category name
            if ($group->categoryname == '') {
                $category = JTable::getInstance('GroupCategory', 'CTable');
                $category->load($group->categoryid);

                $group->categoryname = $category->name;
            }
            // get group admin name
            $user_owner = CFactory::getUser($group->ownerid);
            if ($group->ownername == '') {

                $group->ownername = $user_owner->getDisplayName();
            }
            if($user_owner->_avatar == '') {
                $group->owneravatar = JURI::base() . 'components/com_community/assets/user-Male.png';
            } else {
                $group->owneravatar = $root.'/'.$user_owner->_avatar;
            }
            return array (
                'status' => true,
                'message' => 'View Circle Pre-joining success ',
                'groupDetail' =>$group,
            );
        }
    }

    public function viewMyGroup($groupid) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $data_root = $mainframe->getCfg('dataroot');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        if(empty($group->id)) {
            return array(
                'status' => false,
                'message' => 'Circle not found.'
            );
        }
//        print_r($group);
        $params = $group->getParams();
        $groupModel = CFactory::getModel('groups');
        // get user current
        $my = CFactory::getUser();
        // get members list
        $members = $this->getMembersGroup($group->id);

        // Test if the current user is admin
        $isAdmin = $groupModel->isAdmin($my->id, $group->id);
        // update last access group
        $groupModel->memberLastaccessGroup($group->id, $my->id);

        // Test if the current browser is a member of the group
        $isMember = $groupModel->isMember($my->id, $group->id);
        $waitingApproval = false;
        // If I have tried to join this group, but not yet approved, display a notice
        if ($groupModel->isWaitingAuthorization($my->id, $group->id)) {
            $waitingApproval = true;
        }

        // added new field for show and hide course tab
        $isCourseShare = false;
        $courseids = $groupModel->getPublishCoursesToSocial($group->id);

        if($courseids) {
            $isCourseShare = true;
        }
        if(empty($group->path)) {
            $groupModel->buildPath($group->parentid, $group->id);
        }

        $group->description = strip_tags($group->description);
        if ($group->avatar == '') {
            $group->avatar = JURI::base() . 'components/com_community/assets/group.png';
        } else {
            $group->avatar = $root . '/' . $group->avatar;
        }
        if (strpos($group->cover, 'cover-group-default.png') !== false) {
            $group->cover = $group->cover;
        } else {
            $group->cover = $root . '/' . $group->cover;
        }

        $group->allow_assign_facilitator = false;

        $is_course_group = $groupModel->getIsCourseGroup($group->id);
        $group->course_id = 0;
        $group->circle_course_published = array();
        if(isset($is_course_group) && !empty($is_course_group)) {
            $group->course_id = $is_course_group;
            // check course had toggle on facilitator
            require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');
            $course_data = JoomdleHelperContent::call_method('get_course_info', (int)$group->course_id, $my->username);
            if($course_data['facilitatedcourse'] == 1) {
                $group->allow_assign_facilitator = true;
            }
            $circle_course_published = $this->getCircleCoursePublished($group->course_id);
            if($circle_course_published) {
                foreach($circle_course_published as $k=>$val) {
                    $group->circle_course_published[$k] = $val;
                }
            }
        }
        // get category name
        if ($group->categoryname == '') {
            $category = JTable::getInstance('GroupCategory', 'CTable');
            $category->load($group->categoryid);

            $group->categoryname = $category->name;
        }
        // get group admin name

        $user_owner = CFactory::getUser($group->ownerid);
        if ($group->ownername == '') {

            $group->ownername = $user_owner->getDisplayName();
        }

        if($user_owner->_avatar == '') {
            $group->owneravatar = JURI::base() . 'components/com_community/assets/user-Male.png';
        } else {
            $group->owneravatar = $root.'/'.$user_owner->_avatar;
        }
        // Get like
        $likes = new CLike();
        $totalLikes = $likes->getLikeCount('groups', $group->id);

        // get discussion
        $discussModel = CFactory::getModel('discussions');
        $discussions = $discussModel->getDiscussionTopics($group->id, '10', $params->get('discussordering', DISCUSSION_ORDER_BYLASTACTIVITY));
        for ($i = 0; $i < count($discussions); $i++) {
            $row = $discussions[$i];
            $avatar = CFactory::getUser($row->creator)->_avatar;
            if($avatar == '') {
                $row->avatar_creator = JURI::base() . 'components/com_community/assets/user-Male.png';
            } else {
                $row->avatar_creator = $root.'/'.$avatar;
            }
            if($row->avatar == '') {
                $row->avatar = JURI::base() . 'components/com_community/assets/user-Male.png';
            } else {
                $row->avatar = $root.'/'.$row->avatar;
            }
            if($row->thumb == '') {
                $row->thumb = JURI::base() . 'components/com_community/assets/user-Male.png';
            } else {
                $row->thumb = $root.'/'.$row->thumb;
            }
            $row->count = 0;
            $json_data = json_decode(file_get_contents($data_root . '/chats/' . $row->id . '/data.json', true));
            if ($json_data) {
                foreach ($json_data->data as $js) {
                    if ($my->id == $js->userid) {
                        $row->count = $js->count;
                    }
                }
            }
            $lastreplytypequery = $this->getLastReply($row->id);

            if($lastreplytypequery) {
                $lastreplytype = $lastreplytypequery->params;
                if(empty($lastreplytype)) {
                    $row->lastreplytype = 'comment';
                    $row->lastmessage = (string) $lastreplytypequery->comment;
                } else {
                    if(strpos($lastreplytype,'attached_photo_id') !== false) {
                        $row->lastreplytype = 'uploadphoto';
                        $photo_wall = $this->getPhotoUpload((int) json_decode($lastreplytype)->attached_photo_id);
                        $row->lastmessage = (string) basename("$photo_wall->original");
                    }
                    if(strpos($lastreplytype,'attached_file_id') !== false) {
                        $row->lastreplytype = 'uploadfile';
                        $file = $this->getFileUpload((int) json_decode($lastreplytype)->attached_file_id);
                        $row->lastmessage = (string) $file->name;
                    }
                    if (strpos($lastreplytype, 'website') !== false) {
                        $row->lastreplytype = 'comment';
                        $row->lastmessage = json_decode($lastreplytype)->url;
                    }
                }
                $row->lastmessageby = (int) $lastreplytypequery->post_by;
                $lastreplyuser = CFactory::getUser((int) $lastreplytypequery->post_by);
                $row->lastmessagebyname = $lastreplyuser->name;
            } else {
                $row->lastmessage = (string) $row->message;
                $row->lastmessageby = (int) $row->creator;
                $lastreplyuser = CFactory::getUser((int) $row->creator);
                $row->lastmessagebyname = $lastreplyuser->name;
                $row->lastreplytype = 'comment';
            }
        }
        // get Bulletins
        $bulletinModel = CFactory::getModel('bulletins');
        $bulletins = $bulletinModel->getBulletins($group->id, 0, 0);
        $blutin = array();
        // get timezone server
        $serverTimeZone = date_default_timezone_get();
        $serverObjTime = new DateTimeZone($serverTimeZone);
        $sverTime = new DateTime('now', $serverObjTime);
        $serveroffset = $serverObjTime->getOffset($sverTime);

        if($bulletins) {
            for($i = 0; $i < count($bulletins); $i++) {
                if(isset($bulletins[$i]->timezone)) {
                    $userTimeZone = $bulletins[$i]->timezone;
                } else {
                    $userTimeZone = date_default_timezone_get();
                }
                $enddate = date('Y-m-d H:i', $bulletins[$i]->endtime);
                //get user timezone
                $usertimezone = new DateTimeZone($userTimeZone);
                $gmtTimezone = new DateTimeZone('GMT');
                $myDateTime = new DateTime($enddate, $gmtTimezone);
                $offset = $usertimezone->getOffset($myDateTime);

                $timeUnequal = ($serveroffset - $offset);
                $enddateUser = $bulletins[$i]->endtime + $timeUnequal;


                if($enddateUser > time()) {
                    $Brow = new stdClass();
                    $Brow->id = $bulletins[$i]->id;
                    $Brow->groupid = $bulletins[$i]->groupid;
                    $Brow->created_by = $bulletins[$i]->created_by;
                    $Brow->published = $bulletins[$i]->published;
                    $Brow->title = $bulletins[$i]->title;
                    $Brow->message = $bulletins[$i]->message;
                    $Brow->date = $bulletins[$i]->date;
                    $Brow->params = $bulletins[$i]->params;
                    $Brow->endtime = $bulletins[$i]->endtime;
                    $Brow->timezone = $bulletins[$i]->timezone;
                    $Brow->dateend = date('Y-m-d', $bulletins[$i]->endtime);
                    $Brow->timeend = date('H:i', $bulletins[$i]->endtime);
                    $blutin[] = $Brow;
                }
            }
        }
        // list album for group display
//        $albums = $this->getAlbums($group->id, $params);

        // list videos
//        $videos = $this->getListVideoGroup($group->id, $params);

        //events
        $events = $this->getEventsGroup($group->id, $params);

        $myGroup = array(
            'status' => true,
            'message' => 'View detail success',
            'groupDetail' => $group,
            'members' => $members,
            'isAdmin' => $isAdmin,
            'isMember' => $isMember,
            'isCourseShare' => $isCourseShare,
            'waitingApproval' => $waitingApproval,
            'totalLike' => $totalLikes,
            'discussionstopic' => $discussions,
            'announcementlist' => $blutin,
            'announcementcount' => count($blutin),
//            'albums' => $albums,
//            'videos' => $videos,
            'events' => $events,
        );
        return $myGroup;
    }

    public function joinGroup($groupId) {
        $objResponse = array();

        // Add assertion to the group id since it must be specified in the post request
//                CError::assert($groupId, '', '!empty', __FILE__, __LINE__);
        // Get the current user's object
        $my = CFactory::getUser();
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);
        // Load necessary tables
        $groupModel = CFactory::getModel('groups');
        if ($groupModel->isMember($my->id, $groupId)) {
            $objResponse['status'] = false;
            $objResponse['pending'] = false;
            $objResponse['message'] = JText::_('COM_COMMUNITY_GROUPS_ALREADY_MEMBER');
        } else {
            $member = $this->_saveMemberApi($groupId);

            if ($member->approved) {
                $objResponse['status'] = true;
                $objResponse['pending'] = false;
                $objResponse['message'] = JText::_('COM_COMMUNITY_GROUPS_JOIN_SUCCESS');

                $params = new CParameter('');
                $params->set('circle',$group->name);
                $params->set('url', 'index.php?option=com_community&view=groups&task=viewabout&groupid=' . $group->id);
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewabout&groupid=' . $group->id);

                //CNotificationLibrary::add('groups_join_request', $my->id, $group->ownerid, JText::sprintf('COM_COMMUNITY_GROUP_JOIN_REQUEST_SUBJECT'), '', 'groups.joinrequest', $params );
                CNotificationLibrary::add('groups_member_join', $my->id, $group->ownerid, JText::sprintf('COM_COMMUNITY_NEWS_GROUP_JOIN'), '', 'groups.joinrequest', $params );

            } else {
                $objResponse['status'] = true;
                $objResponse['pending'] = true;
                $objResponse['message'] = JText::_('COM_COMMUNITY_GROUPS_APPROVAL_NEED');

                $params = new CParameter('');
                $params->set('group',$group->name);
                $params->set('url', 'index.php?option=com_community&view=groups&task=invitemembers&groupid=' . $group->id);
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=invitemembers&groupid=' . $group->id);
                $params->set('url_approve', 'index.php?option=com_community&view=groups&task=memberapprove&groupid=' . $group->id .'&memberid=' . $my->id);
                $params->set('url_reject', 'index.php?option=com_community&view=groups&task=memberreject&groupid=' . $group->id .'&memberid=' . $my->id);

                //CNotificationLibrary::add('groups_join_request', $my->id, $group->ownerid, JText::sprintf('COM_COMMUNITY_GROUP_JOIN_REQUEST_SUBJECT'), '', 'groups.joinrequest', $params );
                CNotificationLibrary::add('groups_member_join', $my->id, $group->ownerid, JText::sprintf('COM_COMMUNITY_GROUP_JOIN_REQUEST_SUBJECT'), '', 'groups.joinrequest', $params );
            }
        }

        // push notification
//        if($group->ownerid == $my->id) {
        $message_send = array(
            'mtitle' => $my->name .' requested to join ' . $group->name.'',
            'mdesc' => $group->description
        );
        $payload = $this->pushNotification($message_send, $group->ownerid, $group->id, $group->id, 'join_circle');
        $objResponse['push_data'] = $payload['android'];
        $objResponse['push_ios'] = $payload['ios'];
//        }
        return $objResponse;
    }

    private function _saveMemberApi($groupId) {

        $group = JTable::getInstance('Group', 'CTable');
        $member = JTable::getInstance('GroupMembers', 'CTable');

        $group->load($groupId);
        $my = CFactory::getUser();

        // Set the properties for the members table
        $member->groupid = $group->id;
        $member->memberid = $my->id;

        // @rule: If approvals is required, set the approved status accordingly.
        $member->approved = ( $group->approvals == COMMUNITY_PRIVATE_GROUP ) ? '0' : 1;

        // @rule: Special users should be able to join the group regardless if it requires approval or not
        $member->approved = COwnerHelper::isCommunityAdmin() ? 1 : $member->approved;

        // @rule: Invited users should be able to join the group immediately.
        $groupInvite = JTable::getInstance('GroupInvite', 'CTable');
        $keys = array('groupid' => $groupId, 'userid' => $my->id);
        if ($groupInvite->load($keys)) {
            $member->approved = 1;
        }

        //@todo: need to set the privileges
        $member->permissions = '0';

        $member->store();
        $owner = CFactory::getUser($group->ownerid);

        //trigger for onGroupJoin
//            $this->triggerGroupEvents('onGroupJoin', $group, $my->id);
        // Update user group list
        $my->updateGroupList();

        // Test if member is approved, then we add logging to the activities.
        if ($member->approved) {

            CGroups::joinApproved($groupId, $my->id);
        }
        return $member;
    }

    private function getListVideoGroup($groupId, $params) {

        $result = array();
        $videoModel = CFactory::getModel('videos');
        $tmpVideos = $videoModel->getGroupVideos($groupId, '', $params->get('grouprecentvideos', GROUP_VIDEO_RECENT_LIMIT));
        $videos = array();

        if ($tmpVideos) {
            foreach ($tmpVideos as $videoEntry) {
                $video = JTable::getInstance('Video', 'CTable');
                $video->bind($videoEntry);
                $videos[] = $video;
            }
        }

        $totalVideos = $videoModel->total ? $videoModel->total : 0;
        $result['total'] = $totalVideos;
        $result['data'] = $videos;
        return $result;
    }

    private function getAlbums($groupid, $params) {
        $result_album = array();
        $photosModel = CFactory::getModel('photos');

        $albums = $photosModel->getGroupAlbums($groupid, true, false, $params->get('grouprecentphotos', GROUP_PHOTO_RECENT_LIMIT));

        $db = JFactory::getDBO();
        $where = 'WHERE a.' . $db->quoteName('groupid') . ' = ' . $db->quote($groupid);

        $totalAlbums = $photosModel->getAlbumCount($where);

        $result_album['total'] = $totalAlbums;
        $result_album['data'] = $albums;
        return $result_album;
    }

    private function getEventsGroup($groupid, $param) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        if(empty($group->id)) {
            return $response = array(
                'status' => false,
                'message' => 'Circle not found',
                'events' => array(),
                'totalEvent' => 0
            );
        }
        $params = $group->getParams();

        $eventsModel = CFactory::getModel('Events');
        $tmpEvents = $eventsModel->getGroupEvents($group->id, $params->get('grouprecentevents', GROUP_EVENT_RECENT_LIMIT));
        $totalEvents = $eventsModel->getTotalGroupEvents($groupid);
        $events = array();
        foreach ($tmpEvents as $event) {
            $table = JTable::getInstance('Event', 'CTable');
            $table->bind($event);
            $events[] = $table;
        }
//        print_r($events);die;
        $my = CFactory::getUser();
        $eventMembers = JTable::getInstance('EventMembers', 'CTable');
        $response = array();
        for($i = 0; $i < count($events); $i++ ) {
            $users = CFactory::getUser($events[$i]->creator);
            $row = new stdClass();
            $row->id = $events[$i]->id;
            $row->groupid = $events[$i]->contentid;
            $row->title = $events[$i]->title;
            $row->description = $events[$i]->description;
            $row->location = $events[$i]->location;
            $row->unlisted = $events[$i]->unlisted;
            $row->admin_id = $events[$i]->creator;
            $row->admin_name = $users->getDisplayName();
            $avatar = $users->_avatar;
            if($avatar == '') {
                $row->admin_avatar = JURI::base() . 'components/com_community/assets/user-Male.png';
            } else {
                $row->admin_avatar = $root.'/'.$avatar;
            }
            $row->startdate = $events[$i]->startdate;
            $row->enddate = $events[$i]->enddate;
            $row->permission = $events[$i]->permission;
            $keys = array('eventId' => $events[$i]->id, 'memberId' => $my->id);
            $eventMembers->load($keys);
            $row->status = $eventMembers->status;
            if($events[$i]->avatar != NULL) {
                $row->avatar = $root.'/'.$events[$i]->avatar;
            } else {
                $row->avatar = "";
            }
            if($group->cover != NULL) {
                $row->cover = $root.'/'.$group->cover;
            } else {
                $row->cover = "";
            }
            $row->created = $events[$i]->created;
            $row->ongoing = (int)$events[$i]->confirmedcount;
            $response[] = $row;
        }
        return $eventsGroup = array(
            'status' => true,
            'message' => 'List event',
            'events' => $response,
            'totalEvent' => $totalEvents,
        );
    }

    private function getMembersGroup($groupid, $page, $limit) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $groupModel = CFactory::getModel('groups');

        $position = ($page - 1) * $limit;

        $approvedMembers = $groupModel->getMembers($groupid, $limit, true, false, false, $position);
        CError::assert($approvedMembers, 'array', 'istype', __FILE__, __LINE__);
        // retrict data and get avatar of member
        $model = CFactory::getModel('friends');
        $group = $groupModel->getGroup($groupid);

        require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');

        $my = CFactory::getUser();
        for ($i = 0; $i < count($approvedMembers); $i++) {
            $row = $approvedMembers[$i];

            $user = CFactory::getUser($row->id);
            $isFriend = CFriendsHelper::isConnected($row->id, $my->id);
            $row->isFriend = $isFriend;

            $avatar = $user->_avatar;
            if($avatar == '') {
                $row->avatar = JURI::base() . 'components/com_community/assets/user-Male.png';
            } else {
                $row->avatar = $root.'/'.$avatar;
            }
            $row->username = $user->username;
            $row->manage = $groupModel->isAdmin($row->id, $groupid);

            $isOwner = $groupModel->isCreator($row->id, $groupid);
            $row->owner = $isOwner;

            $connection = $model->getFriendConnection($my->id, $row->id);
            $row->requestMessage = '';
            $row->pendingRequest = false;
            if($connection && !$isFriend) {
                $row->pendingRequest = true;
                $row->requestMessage = $connection[0]->msg;
            }
            $row->myRequest = false;
            if($my->id == $connection[0]->connect_from && !$isFriend) {
                $row->myRequest = true;
            }
            $params = json_decode($group->params);
            if(isset($params->course_id)) {
                $course_role = JoomdleHelperContent::call_method('get_user_role', (int) $params->course_id, $user->username);

                if($course_role['roles']) {
                    foreach ($course_role['roles'] as $cr) {
                        $row->roles_course = json_decode($cr['role']);
                    }
                }
            }
            $roles = array();
            if($group->moodlecategoryid) {
                $roles = JoomdleHelperContent::call_method('get_user_category_role', (int) $group->moodlecategoryid, $user->username);
                $row->roles = (array)$roles['role'];
            } else {
                $row->roles = array();
            }
        }
        return array(
            'status' => true,
            'message' => 'Success',
            'totalMembers' => count($approvedMembers),
            'members' => $approvedMembers,
        );
    }

    private function getMembersGroupPublished($groupid, $page, $limit) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $groupModel = CFactory::getModel('groups');

        $position = ($page - 1) * $limit;

        // retrict data and get avatar of member
        $model = CFactory::getModel('friends');

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        // $groupid is string: 123,234,...
        $params = json_decode($group->params);
        if(isset($params->course_id) && $params->course_id > 0) {
            $circintlepublished = $groupModel->getShareNameGroups((int) $params->course_id, null);

            $approvedMembers = $groupModel->getMembersCirclePublished($circintlepublished[0]->id, $limit, $position);
        } else {
            $approvedMembers = $groupModel->getMembers($groupid, $limit, true, false, false, $position);
        }

        require_once(JPATH_ROOT . '/administrator/components/com_joomdle/helpers/content.php');

        $my = CFactory::getUser();
        for ($i = 0; $i < count($approvedMembers); $i++) {
            $row = $approvedMembers[$i];

            $user = CFactory::getUser($row->id);
            $isFriend = CFriendsHelper::isConnected($row->id, $my->id);
            $row->isFriend = $isFriend;

            $avatar = $user->_avatar;
            if ($avatar == '') {
                $row->avatar = JURI::base() . 'components/com_community/assets/user-Male.png';
            } else {
                $row->avatar = $root . '/' . $avatar;
            }
            $row->username = $user->username;
            $row->name = $user->name;
            $row->manage = $groupModel->isAdmin($row->id, $groupid);

            $isOwner = $groupModel->isCreator($row->id, $groupid);
            $row->owner = $isOwner;

            $connection = $model->getFriendConnection($my->id, $row->id);
            $row->requestMessage = '';
            $row->pendingRequest = false;
            if ($connection && !$isFriend) {
                $row->pendingRequest = true;
                $row->requestMessage = $connection[0]->msg;
            }
            $row->myRequest = false;
            if ($my->id == $connection[0]->connect_from && !$isFriend) {
                $row->myRequest = true;
            }
            $params = json_decode($group->params);
            if (isset($params->course_id)) {
                $course_role = JoomdleHelperContent::call_method('get_user_role', (int) $params->course_id, $user->username);

                if ($course_role['roles']) {
                    foreach ($course_role['roles'] as $cr) {
                        $row->roles_course = json_decode($cr['role']);
                    }
                }
            }
            $roles = array();
            if ($group->moodlecategoryid) {
                $roles = JoomdleHelperContent::call_method('get_user_category_role', (int) $group->moodlecategoryid, $user->username);
                $row->roles = (array) $roles['role'];
            } else {
                $row->roles = array();
            }
        }
        return array(
            'status' => true,
            'message' => 'Success',
            'totalMembers' => count($approvedMembers),
            'members' => $approvedMembers,
        );
    }

    private function getAllmemberGruop($groupid) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $groupModel = CFactory::getModel('groups');
        $members = $groupModel->getAllMember($groupid);
        for ($i = 0; $i < count($members); $i++) {
            $row = $members[$i];
            $avatar = CFactory::getUser($row->id)->_avatar;
            if($avatar == '') {
                $row->avatar = JURI::base() . 'components/com_community/assets/user-Male.png';
            } else {
                $row->avatar = $root.'/'.$avatar;
            }
        }
        return $members;
    }

    private function getProfile($userid) {
//            jimport('joomla.utilities.arrayhelper');
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $data = new stdClass();
        $model = CFactory::getModel('profile');
        $my = CFactory::getUser();
        // Test if userid is 0, check if the user is viewing its own profile.
        if ($userid == 0 && $my->id != 0) {
            $userid = $my->id;

            // We need to set the 'userid' var so that other code that uses
            // JRequest::getVar will work properly
            JRequest::setVar('userid', $userid);
        }
        $user = CFactory::getUser($userid);
        if (!isset($user->id) || empty($user->id) || empty($user->username)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_USER_NOT_FOUND');
            return $response;
        }

        if($userid != $my->id) {
            $friendModel = CFactory::getModel('friends');
            $connection = $friendModel->getFriendConnection($my->id, $user->id);
            $isFriend = CFriendsHelper::isConnected($user->id, $my->id);
        }
//        print_r($connection); die;
        $params = $user->getParams();
        $config = CFactory::getConfig();

        CFactory::setActiveProfile($userid);

        $id = $user->id;

        $result = array();

        $data->user = $user;
        $data->apps = array();

        $groupmodel = CFactory::getModel('groups');
//            $data->groups = $groupmodel->getGroupsCount($user->id);
        $data->course = "";

        $wall = CFactory::getModel('wall');

//        $walls = $wall->getPostUserslist($user->id);

        $profile = $model->getViewableProfile($user->id);

        $tmpGroups = $this->getGroup($user->id, null, $limit);

        require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');
        if ($userid) {
            $certificates = JoomdleHelperContent::call_method("my_certificates", $user->username,'normal');
        } else {
            $certificates = JoomdleHelperContent::call_method("my_certificates", $my->username,'normal');
        }

        if(empty($user->_avatar)) {
            $avatar = JURI::base() . '/'.'components/com_community/assets/user-Male.png';
        } else {
            $avatar = $root.'/'.$user->_avatar;
        }
        if(empty($user->_cover)) {
            $cover = "";
        } else {
            $cover = $root.'/'.$user->_cover;
        }
        $result['id'] = $profile['id'];
        $result['name'] = $profile['name'];
        $result['username'] = $user->username;
//        $result['password'] = $user->password;
        if(!empty($user->usertype)) {
            $result['usertype'] = $user->usertype;
        } else {
            $result['usertype'] = '';
        }
        $block = CFactory::getModel('block');
        if($block->getBlockStatus($my->id, $user->id)) {
            $result['isblock'] = true;
        } else {
            $result['isblock'] = false;
        }
        $result['alias'] = $user->_alias;
        $result['email'] = $profile['email'];
        $result['avatar'] = $avatar;
        $result['cover'] = $cover;
        $result['status'] = $user->getStatus();
        $result['posted_on'] = $user->_posted_on;
        $result['defaultCover'] = (empty($user->_cover)) ? true : false;
        $result['coverPostion'] = $params->get('coverPosition', 0);
        foreach ($profile['fields']['Other Information'] as $interest) {
            if(!empty($interest['value'])) {
                $result['interest'] = $interest['value'];
            } else {
                $result['interest'] = '';
            }
        }
        $result['isFriend'] = (boolean) $isFriend;
        $result['requestMessage'] = '';
        $result['pendingRequest'] = false;
        if ($connection && !$isFriend) {
            $result['pendingRequest'] = true;
            $result['requestMessage'] = $connection[0]->msg;
        }
        $result['myRequest'] = false;
        if ($my->id == $connection[0]->connect_from && !$isFriend) {
            $result['myRequest'] = true;
        }
        $result['groupscount'] = $groupmodel->getCirclesCount($profile['id'], 0, '', false);
        $result['groups'] = $tmpGroups;
//        $result['courses'] = '';
//        $result['coursescount'] = '';
//        $eventmodel = CFactory::getModel('events');
//        $result['events'] = $eventmodel->getEventsCount($profile['id']);

        $result['friendscount'] = $user->_friendcount;
        $result['friends'] = $user->_friends;

        $videoModel = CFactory::getModel('Videos');
        $result['videos'] = $videoModel->getVideosCount($profile['id']);

        $photosModel = CFactory::getModel('photos');
        $result['photos'] = $photosModel->getPhotosCount($profile['id']);

        foreach ($profile['fields']['Basic Information'] as $basic) {
            $infomation = new stdClass();
            $infomation->name = $basic['name'];
            if(!empty($basic['value'])) {
                $infomation->value = $basic['value'];
            } else {
                $infomation->value = '';
            }
            $result['basic_infomation'][] = $infomation;
        }
        $result['contact_infomation'] = array();
        foreach ($profile['fields']['Contact Information'] as $contact) {
            $contatct_info = new stdClass();
            $contatct_info->name = $contact['name'];
            if(!empty($contact['value'])) {
                $contatct_info->value = $contact['value'];
            } else {
                $contatct_info->value = '';
            }
            $result['contact_infomation'][] = $contatct_info;
        }
        $educations = array();
        if($certificates) {
            foreach ($certificates as $cer) {
                $education = new stdClass();
                $education->coursename = $cer['coursename'];
                $education->coursid = $cer['courseid'];
                $education->certificateFile = $cer['downloadFiles'];
                $education->certificateUrl = $cer['url'];
                $education->certificateThumb = JFactory::getApplication()->getCfg('wwwrootfile').'/images/certificates/'. md5($user->username.$cer['courseid']).'.jpg?_='.time();
                $education->certificateid = $cer['certificateid'];
                $educations[] = $education;
            }
//        } else {
//            $result['education'][] = new stdClass();
        }
        $result['education'] = $educations;
        /* is featured */
//        $modelFeatured = CFactory::getModel('Featured');
//        $result['featured'] = $modelFeatured->isExists(FEATURED_USERS, $profile['id']);

        // Assign videoId
//            $result['profilevideo'] = $user->videoid;
//            $video = JTable::getInstance('Video', 'CTable');
//            $video->load($profile['profilevideo']);
//            $result['profilevideoTitle'] = $video->getTitle();
//        $result['posts_count'] = count($walls);
//        $result['posts_wall'] = $walls;
        $response = new stdClass();
        $response->status = true;
        $response->message = 'Profile loaded.';
        $response->profile = $result;
        return $response;
    }

    private function updateProfile($userid,$fullname, $aboutme, $position, $interests) {

        if(empty($userid)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Invalid user.';
            return $reponse;
        }

        $my = CFactory::getUser();
        if($my->id != $userid) {
            $reponse['status'] = false;
            $reponse['message'] = 'You can not edit another user.';
            return $reponse;
        }
        if(empty($fullname)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Name can not be blank.';
            return $reponse;
        }
        if(empty($aboutme)) {
            $reponse['status'] = false;
            $reponse['message'] = 'About me can not be blank.';
            return $reponse;
        }
        $model = CFactory::getModel('profile');

        // save Full name
        $user_u = CFactory::getUser($my->id);
        $user_u->name = $fullname;

        $user_u->save();

        $post = array();
        $post[4] = $aboutme;
        $post[17] = $interests;
//        $post[19] = $position;
        //change value field position for production database
        $post[22] = $position;
//        $post[8] = JRequest::getString('address', '', 'POST');
//        $post[9] = JRequest::getString('state', '', 'POST');
//        $post[10] = JRequest::getString('city', '', 'POST');
//        $post[11] = JRequest::getString('country', '', 'POST');
//        $post[12] = JRequest::getString('website', '', 'POST');
//        $post[14] = JRequest::getString('collect', '', 'POST');
//        $post[15] = JRequest::getString('graduation_year', '', 'POST');

        $values = array();
//            $profiles = $model->getEditableProfile($my->id, $my->getProfileType());
//            foreach ($profiles['fields'] as $group => $fields) {

        foreach ($post as $key => $data) {
            $fieldValue = new stdClass();
            // Get value from posted data and map it to the field.
            // Here we need to prepend the 'field' before the id because in the form, the 'field' is prepended to the id.
            // Grab raw, unfiltered data
            // Retrieve the privacy data for this particular field.
            $fieldValue->access = 0;
            $fieldValue->value = $data;

            if (get_magic_quotes_gpc()) {
                $fieldValue->value = stripslashes($fieldValue->value);
            }

            $values[$key] = $fieldValue;
        }
        $valuesCode = array();


        foreach ($values as $key => $val) {
            $fieldCode = $model->getFieldCode($key);

            if ($fieldCode) {
                // For backward compatibility, we can't pass in an object. We need it to behave
                // like 1.8.x where we only pass values.
                $valuesCode[$fieldCode] = $val->value;
            }
        }
        $saveSuccess = false;
        $saveSuccessMessage = 'Error update profile';

        $appsLib = CAppPlugins::getInstance();
        $appsLib->loadApplications();

        // Trigger before onBeforeUserProfileUpdate
        $args = array();
        $args[] = $my->id;
        $args[] = $valuesCode;
        $result = $appsLib->triggerEvent('onBeforeProfileUpdate', $args);
        // make sure none of the $result is false
        if (!$result || (!in_array(false, $result) )) {
            $saveSuccess = true;
            $saveSuccessMessage = 'Your settings have been saved';
            $model->saveProfile($my->id, $values);
        }
        $response['status'] = $saveSuccess;
        $response['message'] = $saveSuccessMessage;
        return $response;
    }

    private function updateAccount($userid, $fullname, $email, $old_pwd, $new_pwd, $renew_pwd, $number, $address, $name, $profile, $interests) {
        $mainframe = JFactory::getApplication();

        if(empty($userid)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Invalid user.';
            return $reponse;
        }
        $model = CFactory::getModel('profile');
        $my = CFactory::getUser();
        if($my->id != $userid) {
            $reponse['status'] = false;
            $reponse['message'] = 'You can not edit another user.';
            return $reponse;
        }
        if(empty($email)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Email can not empty.';
            return $reponse;
        }
        if(empty($name)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Fullname can not empty.';
            return $reponse;
        }
        $post[4] = $profile;
        $post[6] = $number;
        $post[8] = $address;
        $post[17] = $interests;
        $values = array();

        // If update password
        $changePassword = false;
        if (JString::strlen($old_pwd) || JString::strlen($new_pwd) || JString::strlen($renew_pwd)) {
            $credentials['username'] = $my->username;
            $credentials['password'] = $old_pwd;

            $options = array ('skip_joomdlehooks' => '1');
            $checkpassword = $mainframe->checkUser($credentials, $options);
            if ($checkpassword->status === JAuthentication::STATUS_FAILURE) {
                $reponse['status'] = false;
                $msg = JText::_('OLD_PASSWORD_INVALID');
                $reponse['message'] = $msg;
                return $reponse;
            } else if ($new_pwd != $renew_pwd) {
                $reponse['status'] = false;
                $msg = JText::_('PASSWORDS_DO_NOT_MATCH');
                $reponse['message'] = $msg;
                return $reponse;
            } else {
                $changePassword = true;

                //Jooomla 3.2.0 fix. TO be remove in future
                if (version_compare(JVERSION, '3.2.0', '>=')) {
                    $salt = JUserHelper::genRandomPassword(32);
                    $crypt = JUserHelper::getCryptedPassword($new_pwd, $salt);
                    $password = $crypt . ':' . $salt;
                } else {
                    // Don't re-encrypt the password
                    // JUser bind has encrypted the password
                    if(class_exists(JUserHelper) && method_exists(JUserHelper,'hashpassword')){
                        $password = JUserHelper::hashPassword($new_pwd);
                    }else{
                        $password = $new_pwd;
                    }
                }
            }
        }
        // save Full name
        $user_u = CFactory::getUser($my->id);
        $user_u->name = $name;
        $user_u->email = $email;

        if($changePassword){
            $user_u->set('password', $password);
        }
        /* Save for CUser */
        $user_u->save();

        foreach ($post as $key => $data) {
            $fieldValue = new stdClass();
            // Get value from posted data and map it to the field.
            // Here we need to prepend the 'field' before the id because in the form, the 'field' is prepended to the id.
            // Grab raw, unfiltered data
            // Retrieve the privacy data for this particular field.
            $fieldValue->access = 0;
            $fieldValue->value = $data;

            if (get_magic_quotes_gpc()) {
                $fieldValue->value = stripslashes($fieldValue->value);
            }

            $values[$key] = $fieldValue;
        }
        $valuesCode = array();

        foreach ($values as $key => $val) {
            $fieldCode = $model->getFieldCode($key);

            if ($fieldCode) {
                // For backward compatibility, we can't pass in an object. We need it to behave
                // like 1.8.x where we only pass values.
                $valuesCode[$fieldCode] = $val->value;
            }
        }
        $saveSuccess = false;
        $saveSuccessMessage = 'Error update profile';

        $appsLib = CAppPlugins::getInstance();
        $appsLib->loadApplications();

        // Trigger before onBeforeUserProfileUpdate
        $args = array();
        $args[] = $my->id;
        $args[] = $valuesCode;
        $result = $appsLib->triggerEvent('onBeforeProfileUpdate', $args);
        // make sure none of the $result is false
        if (!$result || (!in_array(false, $result) )) {
            $saveSuccess = true;
            $saveSuccessMessage = 'Your settings have been saved';
            $model->saveProfile($my->id, $values);
        }
        $response['status'] = $saveSuccess;
        $response['message'] = $saveSuccessMessage;
        return $response;
    }

    private function updatePreferences($pref_addme_friend, $pref_accept_myinvite_friend, $pref_invite_join_circle, $pref_add_comment_topic, $pref_approve_me_member_circle) {
        $my = CFactory::getUser();
        if ($my->id == 0) {
            $response['status'] = false;
            $response['message'] = 'User blocked.';
            return $response;
        }
        $params = $my->getParams();
        $params->set('activityLimit', 20);
        $params->set('profileLikes', 1);
        $params->set('showOnlineStatus', 1);
        $params->set('etype_friends_request_connection', $pref_addme_friend);
        $params->set('etype_friends_create_connection', $pref_accept_myinvite_friend);
        $params->set('etype_groups_invite', $pref_invite_join_circle);
        $params->set('etype_groups_member_approved', $pref_approve_me_member_circle);
        $params->set('etype_groups_discussion_reply', $pref_add_comment_topic);
        if($my->save('params')) {
            $response['status'] = true;
            $response['message'] = JText::_('COM_COMMUNITY_PREFERENCES_SETTINGS_SAVED');
        } else {
            $response['status'] = false;
            $response['message'] = 'Preferences save failed.';
        }
        return $response;
    }

    private function listPreferences($userid) {
        $my = CFactory::getUser();
        if($my->id != $userid) {
            $response['status'] = false;
            $response['message'] = 'User blocked.';
            return $response;
        }
        $params = $my->_cparams;
        $data = new stdClass();
        foreach ($params as $key => $val) {
            if($key == 'etype_friends_request_connection') {
                $data->pref_addme_friend = $val;
            }
            if($key == 'etype_friends_create_connection') {
                $data->pref_accept_myinvite_friend = $val;
            }
            if($key == 'etype_groups_invite') {
                $data->pref_invite_join_circle = $val;
            }
            if($key == 'etype_groups_member_approved') {
                $data->pref_approve_me_member_circle = $val;
            }
            if($key == 'etype_groups_discussion_reply') {
                $data->pref_add_comment_topic = $val;
            }
        }
        $response['status'] = true;
        $response['message'] = 'Preferences loaded.';
        $response['preferences'] = $data;
        return $response;
    }

    private function viewGroupDiscussion($id) {
        $document = JFactory::getDocument();

        $my = CFactory::getUser();
        $model = CFactory::getModel('discussions');

        // Load the group
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($id);
        $params = $group->getParams();

        //check if group is valid
        if ($group->id == 0) {
            echo JText::_('COM_COMMUNITY_GROUPS_ID_NOITEM');
            return;
        }


        //display notice if the user is not a member of the group
        if ($group->approvals == 1 && !($group->isMember($my->id) ) && !COwnerHelper::isCommunityAdmin()) {
//                    $this->noAccess(JText::_('COM_COMMUNITY_GROUPS_PRIVATE_NOTICE'));
            return;
        }
        $discussions = $model->getDiscussionTopics($group->id, 0, $params->get('discussordering', DISCUSSION_ORDER_BYLASTACTIVITY));

        for ($i = 0; $i < count($discussions); $i++) {
            $row = $discussions[$i];
            $row->message = strip_tags($discussions[$i]->message);
            $row->user = CFactory::getUser($row->creator);
            $row->lastreplyuser = CFactory::getUser($row->lastmessageby);

            if ($row->lastreplyuser->block) {
                $row->title = $row->lastmessage = JText::_('COM_COMMUNITY_CENSORED');
            }
//                                print_r($row->message);
        }
        return $discussions;
    }

    private function postReplyDiscussion($uniqueId, $message, $upload, $type_reply, $platform) {

        $response = array();
        $mainframe = JFactory::getApplication();
        $data_root = $mainframe->getCfg('dataroot');
        $my = CFactory::getUser();

        // Load models
        $group = JTable::getInstance('Group', 'CTable');
        $discussionModel = CFactory::getModel('Discussions');
        $groupModel = CFactory::getModel('groups');
        $discussion = JTable::getInstance('Discussion', 'CTable');
        //$message		= strip_tags( $message );
        $discussion->load($uniqueId);
        if(empty($discussion->id)) {
            $response['status'] = false;
            $response['error_message'] = 'Discussion not found.';
            return $response;
        }
        $group->load($discussion->groupid);

        $config = CFactory::getConfig();
        if($upload) {
            $ext = pathinfo($_FILES['content']['name'], PATHINFO_EXTENSION);
            if(empty($ext)) {
                $response['status'] = false;
                $response['error_message'] = 'Sorry. Please chose image or document.';
                return $response;
            }
            $extension = array('jpg', 'png', 'gif', 'jpeg', 'JPG', 'bmp', 'PNG');
            $extension_document = array('doc', 'docx', 'xls', 'xlsx', 'odt', 'pdf', 'ppt');
            if(in_array($ext, $extension) && ($type_reply == 'document' || $type_reply == 'comment' || $type_reply == 'ios')) {
                $response['status'] = false;
                $response['error_message'] = 'Sorry. Please check type reply.';
                return $response;
            } else if(in_array($ext, $extension_document) && ($type_reply == 'image' || $type_reply == 'comment' || $type_reply == 'ios')) {
                $response['status'] = false;
                $response['error_message'] = 'Sorry. Please check type reply.';
                return $response;
            }
        } else {
            if($type_reply == 'image' || $type_reply == 'document' || $type_reply == 'ios') {
                $response['status'] = false;
                $response['error_message'] = 'Sorry. Please check type reply.';
                return $response;
            }
        }
        $photo_id = 0;
        $file_id = 0;
        $content = $message;
        $type_comment = 'comment';
        // upload file
        if($upload) {

            // upload image
            if($type_reply == 'image') {
                if($_FILES['content']['type'] == "application/octet-stream" && ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG')) {
                    if($ext == 'jpeg') {
                        $image_type = 'image/jpeg';
                    }
                    if($ext == 'jpg') {
                        $image_type = 'image/jpg';
                    }
                    if($ext == 'JPG') {
                        $image_type = 'image/JPG';
                    }
                }
                elseif($_FILES['content']['type'] == "application/octet-stream" && ($ext == 'png' || $ext == 'PNG')) {
                    $image_type = 'image/png';

                } else {
                    $image_type = $_FILES['content']['type'];
                }
                $uploadFolder = $data_root . '/' . $config->getString('imagefolder') . '/comments/';

                if (!JFolder::exists($uploadFolder)) {
                    JFolder::create($uploadFolder);
                }

                $originalPath = $uploadFolder . 'original_' . md5($my->id . '_comment' . time()) . CImageHelper::getExtension(
                        $image_type
                    );
                $fullImagePath = $uploadFolder . md5($my->id . '_comment' . time()) . CImageHelper::getExtension($image_type);
                $thumbPath = $uploadFolder . 'thumb_' . md5($my->id . '_comment' . time()) . CImageHelper::getExtension(
                        $image_type
                    );
                //Minimum height/width checking for Avatar uploads
                list($currentWidth, $currentHeight) = getimagesize($_FILES['content']['tmp_name']);
                if ($currentWidth < COMMUNITY_AVATAR_PROFILE_WIDTH || $currentHeight < COMMUNITY_AVATAR_PROFILE_HEIGHT) {
                    $this->_showUploadError(
                        true,
                        JText::sprintf(
                            'COM_COMMUNITY_ERROR_MINIMUM_AVATAR_DIMENSION',
                            COMMUNITY_AVATAR_PROFILE_WIDTH,
                            COMMUNITY_AVATAR_PROFILE_HEIGHT
                        )
                    );
                    return;
                }
                $newWidth = 0;
                $newHeight = 0;
                if ($currentWidth >= $currentHeight) {
                    if ($this->testResize(
                        $currentWidth,
                        $currentHeight,
                        COMMUNITY_AVATAR_RESERVE_WIDTH,
                        0,
                        COMMUNITY_AVATAR_PROFILE_WIDTH,
                        COMMUNITY_AVATAR_RESERVE_WIDTH
                    )
                    ) {
                        $newWidth = COMMUNITY_AVATAR_RESERVE_WIDTH;
                        $newHeight = 0;
                    } else {
                        $newWidth = 0;
                        $newHeight = COMMUNITY_AVATAR_RESERVE_HEIGHT;
                    }
                } else {
                    if ($this->testResize(
                        $currentWidth,
                        $currentHeight,
                        0,
                        COMMUNITY_AVATAR_RESERVE_HEIGHT,
                        COMMUNITY_AVATAR_PROFILE_HEIGHT,
                        COMMUNITY_AVATAR_RESERVE_HEIGHT
                    )
                    ) {
                        $newWidth = 0;
                        $newHeight = COMMUNITY_AVATAR_RESERVE_HEIGHT;
                    } else {
                        $newWidth = COMMUNITY_AVATAR_RESERVE_WIDTH;
                        $newHeight = 0;
                    }
                }
                // Generate full image
                if (!CImageHelper::resizeProportional($_FILES['content']['tmp_name'], $fullImagePath, $image_type, $newWidth, $newHeight)) {
                    $msg['error'] = JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $_FILES['content']['tmp_name']);
                    echo json_encode($msg);
                    exit;
                }

                CPhotos::generateThumbnail($_FILES['content']['tmp_name'], $thumbPath, $image_type);

                if (!JFile::copy($_FILES['content']['tmp_name'], $originalPath)) {
                    exit;
                }

                $now = new JDate();

                //lets set the photo data into the table
                $photo = JTable::getInstance('Photo', 'CTable');
                $photo->albumid = '-1'; // -1 for albumless
                //            $photo->image = str_replace(JPATH_ROOT . '/', '', $fullImagePath);
                $photo->image = str_replace($data_root . '/', '', $fullImagePath);
                $photo->caption = $_FILES['content']['name'];
                $photo->filesize = $_FILES['content']['size'];
                $photo->creator = $my->id;
                //            $photo->original = str_replace(JPATH_ROOT . '/', '', $originalPath);
                $photo->original = str_replace($data_root . '/', '', $originalPath);
                $photo->created = $now->toSql();
                $photo->status = 'temp';
                //            $photo->thumbnail = str_replace(JPATH_ROOT . '/', '', $thumbPath);
                $photo->thumbnail = str_replace($data_root . '/', '', $thumbPath);

                //save to table success, return the info needed
                if ($photo->store()) {
                    $info = array(
                        'thumb_url' => $photo->getThumbURI(),
                        'photo_id' => $photo->id
                    );
                } else {
                    $info = array('error' => 1);
                }
                $photo_id = $photo->id;

                $content = $mainframe->getCfg('wwwrootfile') . '/' . $photo->original;
                $type_comment = 'image';
            }
            else {
                $now = new JDate();
                $ext = pathinfo($_FILES['content']['name']);

                $table = JTable::getInstance('File', 'CTable');

                $file = new stdClass();
                $file->creator = $my->id;
                $file->filesize = sprintf("%u", $_FILES['content']['size']);
                // $file->name = JString::substr($_file['name'], 0, JString::strlen($_file['name']) - (JString::strlen($ext['extension']) + 1));
                $file->name = $_FILES['content']['name'];
                $file->created = $now->toSql();
                $file->type = CFileHelper::getExtensionIcon(CFileHelper::getFileExtension($_FILES['content']['name']));
                $fileName = JApplication::getHash($_FILES['content']['name'] . time()) . JString::substr($_FILES['content']['name'], JString::strlen($_FILES['content']['name']) - (JString::strlen($ext['extension']) + 1));

                $file->discussionid = $discussion->id;
                $file->groupid = $discussion->groupid;

                // fix if discussion folder is already exits
                if (!is_dir($data_root . '/images/files/discussion/'.$discussion->id)) {
                    mkdir($data_root . '/images/files/discussion/'.$discussion->id, 0777);
                }

                $file->filepath = 'images/files' . '/discussion/' . $file->discussionid . '/' . $fileName;
                JFile::copy($_FILES['content']['tmp_name'], $mainframe->getCfg('dataroot') . '/images/files' . '/discussion/' . $discussion->id . '/' . $fileName);

                $table->bind($file);

                $table->store();
                $file_id = $table->id;
                $content = $mainframe->getCfg('wwwrootfile') . '/' . $file->filepath;
                $type_comment = 'document';
            }
        }
        // Save the wall content
        $wall = CWallLibrary::saveWall($discussion->id, $message, 'discussions', $my, ($my->id == $discussion->creator), 'groups,discussion', '', 0, $photo_id, $file_id);

        $date = JFactory::getDate();

        $discussion->lastreplied = $date->toSql();
        $discussion->store();
        // @rule: only add the activities of the wall if the group is not private.
        //if( $group->approvals == COMMUNITY_PUBLIC_GROUP ) {
        // Build the URL
        $discussURL = CUrl::build('groups', 'viewdiscussion', array('groupid' => $discussion->groupid, 'topicid' => $discussion->id), true);

        $act = new stdClass();
        $act->cmd = 'group.discussion.reply';
        $act->actor = $my->id;
        $act->target = 0;
        $act->title = '';
        $act->content = $message;
        $act->app = 'groups.discussion.reply';
        $act->cid = $discussion->id;
        $act->groupid = $group->id;
        $act->group_access = $group->approvals;

        $act->like_id = $wall->id;
        $act->like_type = 'groups.discussion.reply';

        $params = new CParameter('');
        $params->set('action', 'group.discussion.reply');
        $params->set('wallid', $wall->id);
        $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
        $params->set('group_name', $group->name);
        $params->set('discuss_url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $discussion->groupid . '&topicid=' . $discussion->id);

        // Add activity log
        CActivityStream::add($act, $params->toString());


        // Get repliers for this discussion and notify the discussion creator too
        $users = $discussionModel->getRepliers($discussion->id, $group->id);
        $users[] = $discussion->creator;

        // Make sure that each person gets only 1 email
        $users = array_unique($users);

        // The person who post this, should not be getting notification email
        $key = array_search($my->id, $users);

        if ($key !== false && isset($users[$key])) {
            unset($users[$key]);
        }
        $limit = 0;
        $members = $groupModel->getMembers($group->id, $limit, true, false, true, false, NUll);
        $member_notifi = array();
        if(!empty($members)) {
            foreach ($members as $mem) {
                $member_notifi[] = $mem->id;
                $member_store[] = $mem->id;
            }
        }
        $member_notifi = array_unique($member_notifi);
        $key_mem = array_search($my->id, $member_notifi);
        if ($key_mem !== false && isset($member_notifi[$key_mem])) {
            unset($member_notifi[$key_mem]);
        }

        // Add notification
        $params = new CParameter('');
        $params->set('url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $discussion->groupid . '&topicid=' . $discussion->id);
        $params->set('message', $message);
        $params->set('title', $discussion->title);
        $params->set('discussion', $discussion->title);
        $params->set('discussion_url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $discussion->groupid . '&topicid=' . $discussion->id);
        CNotificationLibrary::add('groups_discussion_reply', $my->id, $users, JText::_('COM_COMMUNITY_GROUP_NEW_DISCUSSION_REPLY_SUBJECT'), '', 'groups.discussion.reply', $params);


        // push notification
        $message_send = array(
//                'mtitle' => str_replace('{actor}', $my->getDisplayName(), JText::_('COM_COMMUNITY_GROUP_NEW_DISCUSSION_REPLY_SUBJECT')),
            'mtitle' => $my->getDisplayName() .' replied to discussion '.$discussion->title,
            'mdesc'  => $content,
        );
//        $push_json = $this->pushNotificationTopic($discussion->id, $message_send, $member_notifi);
        $push_json = $this->pushNotificationTopic($discussion->id, $wall->id, $message_send, $member_notifi, '', $group->id, $type_comment, $ext);
        //end push

        /*
         *  Store count message to file
         */

        if (!is_dir($data_root . '/chats')) {
            mkdir($data_root . '/chats', 0777);
        }
        if (!is_dir($data_root . '/chats/'.$discussion->id)) {
            mkdir($data_root . '/chats/'.$discussion->id, 0777);
        }
        if (!is_file($data_root . '/chats/'.$discussion->id.'/data.json')) {
            fopen($data_root . '/chats/'.$discussion->id.'/data.json', 'wb');
        }
        $data = array();
        if(!empty($member_store)) {
            foreach($member_store as $u) {
                $res['userid'] = $u;
                if($my->id == $u) {
                    $res['count'] = 0;
                    $res['view'] = 0;
                } else {
                    $res['count'] = 1;
                    $res['view'] = 1;
                }
                $data[] = $res;
            }
        }

        /*
         * read file first and get current data if already exits
         */
        $already_data = json_decode(file_get_contents($data_root . '/chats/'.$discussion->id.'/data.json', true));

        if($already_data->data) {
            $json_al = array();
            foreach ($already_data->data as $al) {
                $member_already_store[] = $al->userid;
                $json_re['userid'] = $al->userid;
                if($al->userid == $my->id) {
                    $json_re['count'] = 0;
                    $json_re['view'] = 0;
                } else {
                    $json_re['count'] = $al->count + 1;
                    $json_re['view'] = 1;
                }
                $json_al[] = $json_re;
            }
            if(count($member_store) > count($member_already_store)) {
                $member_push = array_diff($member_store, $member_already_store);
                $json_push = array();
                foreach ($member_push as $m) {
                    $json_push['userid'] = $m;
                    $json_push['count'] = 1;
                    $json_push['view'] = 1;
                }
                array_push($json_al, $json_push);
            }
            $json_data = json_encode(array('data' => $json_al));
            file_put_contents($data_root . '/chats/'.$discussion->id.'/data.json', $json_data);
        }
        else {
            $json_data = json_encode(array('data' => $data));
            file_put_contents($data_root . '/chats/'.$discussion->id.'/data.json', $json_data);
        }

        //add user points
        //CFactory::load( 'libraries' , 'userpoints' );
        CUserPoints::assignPoint('group.discussion.reply');

        $config = CFactory::getConfig();
        $order = $config->get('group_discuss_order');
        $order = ($order == 'DESC') ? 'prepend' : 'append';

        $wall_model = CFactory::getModel('wall');
        $walls_content = $wall_model->get($wall->id);

        $walls_content = (array) $walls_content;
        $user_reply = CFactory::getUser($walls_content['post_by']);

        $walls_content['avatar'] = $user_reply->getAvatar();
        $params_wall = $walls_content['params'];
        $type = 'comment';
        if (strpos($walls_content['params'], 'attached_photo_id') !== false) {
            $type = 'uploadphoto';
            $photo_wall = $this->getPhotoUpload((int) json_decode($params_wall)->attached_photo_id);
            $walls_content['photo'] = $mainframe->getCfg('wwwrootfile') . '/' . $photo_wall->original;
            $walls_content['thumbnail'] = $mainframe->getCfg('wwwrootfile') . '/' . $photo_wall->thumbnail;
        }
        if (strpos($walls_content['params'], 'website') !== false) {
            $type = 'comment';
        }
        if (strpos($walls_content['params'], 'attached_file_id') !== false) {
            $type = 'uploadfile';
            $file_wall = $this->getFileUpload((int) json_decode($params_wall)->attached_file_id);
            $walls_content['filename'] = $file_wall->name;
            $walls_content['filepath'] = $mainframe->getCfg('wwwrootfile') . '/' . $file_wall->filepath;
        }
        $walls_content['type'] = $type;

        $response['status'] = true;
        $response['message'] = 'success';
        $response['content'] = $walls_content;
        $response['push_data'] = $push_json['android'];
        $response['push_ios'] = $push_json['ios'];
        return $response;
    }

    private function getViewGroupUpdate($userId) {
        $groupModel = CFactory::getModel('groups');
        $my = CFactory::getUser($userId);
        $document = JFactory::getDocument();
        //get the groups of current user
        $userGroupArr = $groupModel->getGroupIds($my->id);

        $groupInfoArr = array(); //to store all the groups info that belongs to current user

        foreach ($userGroupArr as $userGrp) {
            $table = JTable::getInstance('Group', 'CTable');
            $table->load($userGrp);


            $groupInfoArr[] = array('thumb' => $table->getThumbAvatar());
        }

        $latestParticipatedDiscussion = $groupModel->getGroupDiscussionLastActive($userId);

        return $latestParticipatedDiscussion;
    }

    private function getViewBulletins($id) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $my = CFactory::getUser();

        // Load the group
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($id);
        if ($group->id == 0) {
            echo JText::_('COM_COMMUNITY_GROUPS_ID_NOITEM');
            return $response = array(
                'status' => false,
                'message' => JText::_('COM_COMMUNITY_GROUPS_ID_NOITEM'),
                'announcements' => array()
            );
        }
        if ($group->approvals == 1 && !($group->isMember($my->id) ) && !COwnerHelper::isCommunityAdmin()) {
//                $this->noAccess(JText::_('COM_COMMUNITY_GROUPS_PRIVATE_NOTICE'));
            return;
        }
        $model = CFactory::getModel('bulletins');
        $bulletins = $model->getBulletins($group->id, 0, 0);
        $blutin = array();
        // get timezone server
        $serverTimeZone = date_default_timezone_get();
        $serverObjTime = new DateTimeZone($serverTimeZone);
        $sverTime = new DateTime('now', $serverObjTime);
        $serveroffset = $serverObjTime->getOffset($sverTime);

        if($bulletins) {
            for($i = 0; $i < count($bulletins); $i++) {
                if(isset($bulletins[$i]->timezone)) {
                    $userTimeZone = $bulletins[$i]->timezone;
                } else {
                    $userTimeZone = date_default_timezone_get();
                }
                $enddate = date('Y-m-d H:i', $bulletins[$i]->endtime);
                //get user timezone
                $usertimezone = new DateTimeZone($userTimeZone);
                $gmtTimezone = new DateTimeZone('GMT');
                $myDateTime = new DateTime($enddate, $gmtTimezone);
                $offset = $usertimezone->getOffset($myDateTime);

                $timeUnequal = ($serveroffset - $offset);
                $enddateUser = $bulletins[$i]->endtime + $timeUnequal;


                if($enddateUser > time()) {
                    $Brow = new stdClass();
                    $Brow->id = $bulletins[$i]->id;
                    $Brow->groupid = $bulletins[$i]->groupid;
                    $Brow->groupname = $group->name;
                    $Brow->created_by = $bulletins[$i]->created_by;
                    $user = CFactory::getUser($bulletins[$i]->created_by);
                    $Brow->userfullname = $user->name;
                    $avatar = $user->_avatar;
                    if($avatar == '') {
                        $Brow->avatar = JURI::base() . 'components/com_community/assets/user-Male.png';
                    } else {
                        $Brow->avatar = $root.'/'.$avatar;
                    }
                    $Brow->published = $bulletins[$i]->published;
                    $Brow->title = $bulletins[$i]->title;
                    $Brow->message = $bulletins[$i]->message;
                    $Brow->date = $bulletins[$i]->date;
                    $Brow->params = $bulletins[$i]->params;
                    $Brow->endtime = $bulletins[$i]->endtime;
                    $Brow->timezone = $bulletins[$i]->timezone;
                    $Brow->dateend = date('d/m/Y', $bulletins[$i]->endtime);
                    $Brow->timeend = date('g:i A', $bulletins[$i]->endtime);
                    $blutin[] = $Brow;
                }
            }
        }
        $response['status'] = true;
        $response['message'] = 'List announcements';
        $response['announcements'] = $blutin;

        return $response;
    }

    private function saveDiscussionGroup($groupid, $title, $content, $notify_me, $topicid, $avatar) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $groupsModel = CFactory::getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        if(!$group->id) {
            return array(
                'status' => false,
                'message' => 'Circle not found or has been deleted.'
            );
        }
        $creator = CFactory::getUser($group->ownerid);

        $group->ownername = $creator->getDisplayName();

        if($group->published == 0) {
            return array(
                'status' => false,
                'message' => 'Circle '.$group->name.' has been archived. Please contact '.$group->ownername.' for more information.'
            );
        }
        $inputFilter = CFactory::getInputFilter(true);

        $my = CFactory::getUser();
        $config = CFactory::getConfig();
        $isBanned = $group->isBanned($my->id);

        $discussion = JTable::getInstance('Discussion', 'CTable');
        if ($my->id == 0) {
            return $this->blockUnregister();
        }
        $isNew = !$topicid ? true : false;
        if ($isNew) {
            $discussion->creator = $my->id;
        }
        $now = new JDate();
        $discussion->creator = $my->id;
        $discussion->groupid = $groupid;
        $discussion->title = strip_tags($title);
        $discussion->message = $inputFilter->clean($content);
        $discussion->created = $now->toSql();
        $discussion->lock = 0;
        $discussion->lastreplied = $now->toSql();
        if(!$isNew) {
            $discussion->id = $topicid;
        }
        $appsLib = CAppPlugins::getInstance();
        $saveSuccess = $appsLib->triggerEvent('onFormSave', array('jsform-groups-discussionform'));
        $validated = true;

        $data = new stdClass();
        if (empty($saveSuccess) || !in_array(false, $saveSuccess)) {
            $config = CFactory::getConfig();

            // @rule: Spam checks
            if ($config->get('antispam_akismet_discussions')) {
                //CFactory::load( 'libraries' , 'spamfilter' );

                $filter = CSpamFilter::getFilter();
                $filter->setAuthor($my->getDisplayName());
                $filter->setMessage($discussion->title . ' ' . $discussion->message);
                $filter->setEmail($my->email);
                $filter->setURL(CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id));
                $filter->setType('message');
                $filter->setIP($_SERVER['REMOTE_ADDR']);

                if ($filter->isSpam()) {
                    $validated = false;
                    $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_DISCUSSIONS_MARKED_SPAM'), 'error');
                }
            }

            if (empty($discussion->title)) {
                $validated = false;
                $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_TITLE_EMPTY'), 'error');
            }

            if (empty($discussion->message)) {
                $validated = false;
                $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_BODY_EMPTY'), 'error');
            }

            if ($validated) {
                //CFactory::load( 'models' , 'discussions' );

                $params = new CParameter('');

                $params->set('filepermission-member', JRequest::getInt('filepermission-member', 0));
                $params->set('notify-creator', $notify_me);

                $discussion->params = $params->toString();

                $discussion->store();

                if ($isNew) {
                    $group = JTable::getInstance('Group', 'CTable');
                    $group->load($groupid);

                    // Add logging.
                    $url = CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupid);


                    $act = new stdClass();
                    $act->cmd = 'group.discussion.create';
                    $act->actor = $my->id;
                    $act->target = 0;
                    $act->title = ''; //JText::sprintf('COM_COMMUNITY_GROUPS_NEW_GROUP_DISCUSSION' , '{group_url}' , $group->name );
                    $act->content = $discussion->message;
                    $act->app = 'groups.discussion';
                    $act->cid = $discussion->id;
                    $act->groupid = $group->id;
                    $act->group_access = $group->approvals;

                    $act->like_id = CActivities::LIKE_SELF;
                    $act->comment_id = CActivities::COMMENT_SELF;
                    $act->like_type = 'groups.discussion';
                    $act->comment_type = 'groups.discussion';

                    $params = new CParameter('');
                    $params->set('action', 'group.discussion.create');
                    $params->set('topic_id', $discussion->id);
                    $params->set('topic', $discussion->title);
                    $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                    $params->set('topic_url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $group->id . '&topicid=' . $discussion->id);

                    CActivityStream::add($act, $params->toString());

                    //@rule: Add notification for group members whenever a new discussion created.
                    $config = CFactory::getConfig();

                    if ($config->get('groupdiscussnotification') == 1) {
                        $model = $this->getModel('groups');
                        $members = $model->getMembers($groupid, null);
                        $admins = $model->getAdmins($groupid, null);

                        $membersArray = array();

                        foreach ($members as $row) {
                            $membersArray[] = $row->id;
                        }

                        foreach ($admins as $row) {
                            $membersArray[] = $row->id;
                        }
                        unset($members);
                        unset($admins);

                        // Add notification
                        //CFactory::load( 'libraries' , 'notification' );

                        $params = new CParameter('');
                        $params->set('url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $group->id . '&topicid=' . $discussion->id);
                        $params->set('group', $group->name);
                        $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                        $params->set('discussion', $discussion->title);
                        $params->set('discussion_url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $group->id . '&topicid=' . $discussion->id);
                        $params->set('user', $my->getDisplayName());
                        $params->set('subject', $discussion->title);
                        $params->set('message', $discussion->message);

                        CNotificationLibrary::addMultiple('groups_create_discussion', $discussion->creator, $membersArray, JText::sprintf('COM_COMMUNITY_NEW_DISCUSSION_NOTIFICATION_EMAIL_SUBJECT', $group->name), '', 'groups.discussion', $params);
                    }
                }
                // add avatar
                if(!empty($avatar['name'])) {
                    if(isset($avatar)) {
                        $uploaded = $this->uploadAvatar($discussion->id, $avatar, 'discussion', 'create');
                    }
                }
                $discussion->load($discussion->id);

                //get avatar of user create
                if ($discussion->avatar != NULL) {
                    $discussion->avatar = $root . '/' . $discussion->avatar;
                } else {
                    $discussion->avatar = "";
                }

                if(empty($group->avatar)) {
                    $discussion->cover = '';
                } else {
                    $discussion->cover = $root.'/'.$group->avatar;
                }
                $discussion->isAdmin = false;
                $discussion->isMember = false;
                if($my->id == $discussion->creator) {
                    $discussion->isAdmin = true;
                }
                $isMember = $groupsModel->isMember($my->id, $group->id);
                if($isMember) {
                    $discussion->isMember = true;
                }
                $params = json_decode($group->params);
                $circle_type = 'social_circle';
                $course_id = 0;
                if(isset($params->course_id) && $params->course_id > 0) {
                    $circle_type = 'course_circle';
                    $course_id = $params->course_id;
                }
                $discussion->remoteid = $course_id;
                $discussion->circle_type = $circle_type;
                $discussion->count = 0;
                $discussion->courseid = $course_id;
                $creator = CFactory::getUser($discussion->creator);
                $discussion->createdby = "Created by You";
                $discussion->lastmessage = "";
                $discussion->lastmessageby = $discussion->creator;
                $discussion->lastmessagebyname = $creator->getDisplayName();
                $discussion->lastreplytype = 'comment';
                //add user points
                //CFactory::load( 'libraries' , 'userpoints' );
                CUserPoints::assignPoint('group.discussion.create');
            }
            $data->status = true;
            $data->message = 'Success';
            $data->topic = $discussion;
        } else {
            $data->status = false;
            $data->message = 'Failed';
            $data->topic = $discussion;
        }
        return $data;
    }

    private function getNotification() {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $my = CFactory::getUser();
        $modelNotification = CFactory::getModel('notification');
        $notifications = $modelNotification->getNotification($my->id, '0', 0);

        $response = array();
        if($notifications) {
            $response['status'] = true;
            $response['message'] = 'Notification loaded.';
            for ($i = 0; $i < count($notifications); $i++) {
                $row = $notifications[$i];
                $row->content_id = 0;
                $row->groupid = 0;
                if(isset($row->params)) {
                    $string = json_decode($row->params);
                    if($row->cmd_type == 'notif_groups_member_join' || $row->cmd_type == 'notif_groups_invite') {
                        $jinput = JFactory::getApplication()->input;
                        $username = $jinput->get('username', '', 'POST');
                        $group_url = explode("=",$string->group_url);
                        $row->content_id = end($group_url);
                        $row->groupid = end($group_url);
                        $row->requestMessage = '';
                        $row->isFriend = false;
                        $row->reject = false;
                        $row->group_detail = $this->headerCircle($row->groupid, $username);
                    }
                    if($row->cmd_type == 'notif_events_invite' || $row->cmd_type == 'notif_groups_create_event') {
                        $event_url = explode("=", $string->event_url);
                        $row->content_id = end($event_url);
                        $group_url = explode("=", $string->group_url);
                        $row->groupid = end($group_url);
                        if($row->cmd_type == 'notif_events_invite') {
                            $event = JTable::getInstance('Event', 'CTable');
                            $event->load((int)$row->content_id);
                            $row->groupid = $event->contentid;
                        }
                        $row->requestMessage = '';
                        $row->isFriend = false;
                        $row->reject = false;
                    }
                    if($row->cmd_type == 'notif_friends_request_connection') {
                        $actor_url = explode("=", $string->actor_url);
                        $row->content_id = end($actor_url);
                        $model = CFactory::getModel('friends');
                        $connection = $model->getFriendConnection($my->id, $row->content_id);
                        $row->requestMessage = (string) $connection[0]->msg;
                        if($connection[0]->status == 1) {
                            $row->isFriend = true;
                            $row->reject = false;
                        } else {
                            $row->isFriend = false;
                            $row->reject = false;
                        }

                        if(!$connection) {
                            $row->reject = true;
                        }
                    }
                    if($row->cmd_type == 'notif_groups_create_news') {
                        $announcement_url = explode("=", $string->announcement_url);
                        $row->content_id = end($announcement_url);
                        $group_url = explode("=", $string->group_url);
                        $row->groupid = end($group_url);
                        $row->requestMessage = '';
                        $row->isFriend = false;
                        $row->reject = false;
                    }
                    if($row->cmd_type == 'notif_groups_discussion_reply' || $row->cmd_type == 'notif_groups_discussion_newfile') {
                        $discussion_url = explode("=", $string->discussion_url);
                        $row->content_id = end($discussion_url);
                        $discussion = JTable::getInstance('Discussion', 'CTable');
                        $discussion->load((int)($row->content_id));
                        $row->groupid = $discussion->groupid;
                        $row->requestMessage = '';
                        $row->isFriend = false;
                        $row->reject = false;
                    }
                    if($row->cmd_type == 'notif_course_enrol' || $row->cmd_type == 'course_subscribe_lp' || $row->cmd_type == 'course_subscribe_non' || $row->cmd_type == 'notif_course_sendapprove' || $row->cmd_type == 'notif_course_approval' || $row->cmd_type == 'notif_course_unapproval') {
                        $courseenrol_url = explode("=", $string->course_url);
                        $row->content_id = end($courseenrol_url);
                        $row->courseid = end($courseenrol_url);
                        if (($row->cmd_type == 'notif_course_enrol' && strpos($row->content, 'Content Creator') !== false) || $row->cmd_type == 'notif_course_sendapprove') {
                            $row->creator_app = true;
                        }
                        if ($row->cmd_type == 'notif_course_enrol' && strpos($row->content, 'Content Creator') !== false) {
                            $row->cmd_type = 'notif_course_enrol_cc';
                        }
                    }
                }
                $row->username = CFactory::getUser($row->actor)->name;
                if(CFactory::getUser($row->actor)->_avatar) {
                    $avatar = $root.'/'.CFactory::getUser($row->actor)->_avatar;
                } else {
                    $avatar = JURI::base() . '/'.'components/com_community/assets/default.jpg';
                }
                $row->avatar = $avatar;
                $row->content = CContentHelper::injectTags($row->content, $row->params, false);
            }
            $response['notification'] = (array) $notifications;
            return $response;
        } else {
            $response['status'] = false;
            $response['message'] = 'Notification not loaded.';
            $response['notification'] = (array) $notifications;
            return $response;
        }
    }

    private function countNotification() {
        $my = CFactory::getUser();
        $modelNotification = CFactory::getModel('notification');
        $notifications = $modelNotification->getNotification($my->id, '0', 0);
        if (count($notifications) > 0) {
            $count->count_notification = count($notifications);
        } else {
            $count->count_notification = 0;
        }

        return $count;
    }

    private function countNotifications($username) {
        jimport('joomla.user.helper');
        if ($username == '') {
            $my = CFactory::getUser();
            $userid = $my->id;
        } else {
            $userid = JUserHelper::getUserId($username);
        }
        $modelNotification = CFactory::getModel('notification');
        $notifications = $modelNotification->getNotificationCount($userid, '0', '');
        if ($notifications > 0) {
            $count->count_notification = (int)$notifications;
        } else {
            $count->count_notification = 0;
        }

        return $count;
    }

    private function readAllNotification() {
        $my = CFactory::getUser();
        if($my->id == 0) {
            $response['status'] = false;
            $response['message'] = 'User not login';
            return $response;
        }
        $modelNotification = CFactory::getModel('notification');
        $notifications = $modelNotification->getNotification($my->id);
        foreach ($notifications as $notifi) {
            $modelNotification->readNotification($notifi->id, $my->id);
        }
        $response['status'] = true;
        $response['message'] = 'Notification updated.';
        return $response;
    }

    private function readNotification($notification) {
        if(empty($notification)) {
            $response['status'] = false;
            $response['message'] = 'Notification can not be empty';
            return $response;
        }
        $my = CFactory::getUser();
        if($my->id == 0) {
            $response['status'] = false;
            $response['message'] = 'User not login';
            return $response;
        }
        $modelNotification = CFactory::getModel('notification');
        $modelNotification->updateStatus($notification);

        $response['status'] = true;
        $response['message'] = 'Notification updated';
        return $response;
    }

    private function getUserid($username) {
        jimport('joomla.user.helper');
        $user_id = JUserHelper::getUserId($username);

        $model = CFactory::getModel('profile');
        CFactory::setActiveProfile($user_id);
        $user = CFactory::getUser($user_id);
        $profile = $model->getViewableProfile($user_id);
        $groupmodel = CFactory::getModel('groups');
        $notifModel = CFactory::getModel('notification');
        $userParams   = $user->getParams();

//        $wall = CFactory::getModel('wall');
//        $walls = $wall->getPostListUser($user_id);

        $result = array();
        $result['user_community_id'] = $user_id;
        $result['username'] = $username;
        $result['fullname'] = $user->name;
        $result['profileimageurl'] = $user->getAvatar();
        $result['profilecoverurl'] = $user->getCover();
        $result['status'] = $user->getStatus();
        $result['interest'] = $profile['fields']['Other Information'][0]['value'];
        $result['alias'] = $user->_alias;

        $result['notifycount'] = $notifModel->getNotificationCount($user_id,'0',$userParams->get('lastnotificationlist',''));

        $result['groupscount'] = $groupmodel->getGroupsCount($profile['id']);
        $result['groups'] = $user->_groups;
        $result['birthday'] = $profile['fields']['Basic Information'][1]['value'];
//        $result['posts_count'] = count($walls);

        return $result;
    }

    private function leaveGroupapi($groupId) {
        $objResponse = array();
        $model = CFactory::getModel('groups');
        $my = CFactory::getUser();

        if (!$my->authorise('community.leave', 'groups.' . $groupId)) {
            $errorMsg = $my->authoriseErrorMsg();
            if ($errorMsg == 'blockUnregister') {
                return $this->blockUnregister();
            }
        }

        $group = JTable::getInstance('Group', 'CTable');
        $aa = $group->load($groupId);

        $data = new stdClass();
        $data->groupid = $groupId;
        $data->memberid = $my->id;

        $model->removeMember($data);


        $my->updateGroupList();

        // STore the group and update the data
        $group->updateStats();
        $group->store();

        //delete invitation
        $invitation = JTable::getInstance('Invitation', 'CTable');
        $invitation->deleteInvitation($groupId, $my->id, 'groups,inviteUsers');
        $objResponse['status'] = true;
        $objResponse['message'] = 'You have successfully left the group.';
        return $objResponse;
    }
    //acbe8d5aa5bfde2cdfc91d53a7a0efc7
    private function viewDiscussionTopic($groupId, $topicId) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $data_root = $mainframe->getCfg('dataroot');

        $groupModel = CFactory::getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $discussion = JTable::getInstance('Discussion', 'CTable');
        $my = CFactory::getUser();

        $model = CFactory::getModel('wall');
        $isOwnerCircle = $groupModel->isCreator($my->id,$groupId);
        $isAdmin = $groupModel->isAdmin($my->id, $groupId);
        $group->load($groupId);
        $isMember = $groupModel->isMember($my->id, $group->id);
        $discussion->load($topicId);
        if($discussion->id == 0) {
            return array(
                'status' => false,
                'message' => 'Topic not found or has been deleted.'
            );
        }
        $isOwner = false;
        if($my->id == $discussion->creator) {
            $isOwner = true;
        }
        $isManageTopic = false;
        if($isAdmin || (!$isAdmin && ($discussion->creator == $my->id))) {
            $isManageTopic = true;
        }
        $discussion_detail = array();
        $params = json_decode($group->params);
        $circle_type = 'social_circle';
        $course_id = 0;
        if(isset($params->course_id) && $params->course_id > 0) {
            $circle_type = 'course_circle';
            $course_id = $params->course_id;
        }

        $appType = 'discussions';
        $order = 'DESC';
        $walls = $model->getPost($appType, $discussion->id, null, 0, $order);

        $result = new stdClass();
        $result->id = $discussion->id;
        $result->groupid = $discussion->groupid;
        $result->groupname = $group->name;
        $result->creator = $discussion->creator;
        $result->isowner = $isOwner;
        $result->ismanager = $isManageTopic;
        $result->ismember = $isMember;
        $result->created = $discussion->created;
        $result->title = $discussion->title;
        $result->message = strip_tags($discussion->message);
        $result->lock = $discussion->lock;
        $result->params = $discussion->params;
        $result->circle_type = $circle_type;
        $result->courseid = $course_id;
        $user_creator = CFactory::getUser($discussion->creator);
        if($discussion->avatar) {
            $result->avatar = $root.'/'.$discussion->avatar;
        } else {
            $result->avatar = '';
        }
        if(empty($group->avatar)) {
            $result->cover = '';
        } else {
            $result->cover = $root.'/'.$group->avatar;
        }

        $result->parentid = $discussion->parentid;
        $result->lastreplied = $discussion->lastreplied;


        $discussion_detail['status'] = true;
        $discussion_detail['message'] = 'Success';
        $discussion_detail['discussion'] = $result;


//            $discussion_detail['reply'] = $walls;
        // update by luyenvt Sep 22, 2014 - Imformation of Creator User
        $result->creator_username = $user_creator->getDisplayName();
        $result->creator_avatar = $user_creator->getAvatar();

        if ($walls) {
            foreach ($walls as $k => $r) {
                // avatar get image default
                if ($r->avatar == '') {
                    $user_reply = CFactory::getUser($r->post_by);

                    $walls[$k]->avatar = $user_reply->getAvatar();
                }
                // if upload file or photo: get file or photo
                $params = $walls[$k]->params;
                $type = 'comment';
                if (strpos($walls[$k]->params,'attached_photo_id') !== false) {
                    $type = 'uploadphoto';
                    $photo = $this->getPhotoUpload((int) json_decode($params)->attached_photo_id);
                    $walls[$k]->photo = $mainframe->getCfg('wwwrootfile').'/'.$photo->original;
                    $walls[$k]->thumbnail = $mainframe->getCfg('wwwrootfile').'/'.$photo->thumbnail;
                }
                if(strpos($walls[$k]->params,'website') !== false) {
                    $type = 'uploadlink';
                }
                if(strpos($walls[$k]->params,'attached_file_id') !== false) {
                    $type = 'uploadfile';
                    $file = $this->getFileUpload((int) json_decode($params)->attached_file_id);
                    $walls[$k]->filename = $file->name;
                    $walls[$k]->filepath = $mainframe->getCfg('wwwrootfile').'/'.$file->filepath;
                }
                $walls[$k]->type = $type;

            }
        }
        /*
         * update notification count
         */
        $already_data = json_decode(file_get_contents($data_root . '/chats/'.$discussion->id.'/data.json', true));

        if($already_data->data) {
            $json_al = array();
            foreach ($already_data->data as $al) {
                $json_re['userid'] = $al->userid;
                if($my->id == $al->userid) {
                    $json_re['count'] = 0;
                    $json_re['view'] = 0;
                } else {
                    $json_re['count'] = $al->count;
                    $json_re['view'] = $al->view;
                }
                $json_al[] = $json_re;
            }
            $json_data = json_encode(array('data' => $json_al));
            file_put_contents($data_root . '/chats/'.$discussion->id.'/data.json', $json_data);
        }

        $discussion_detail['reply'] = $walls;
        return $discussion_detail;
    }

    private function postWall($uniqueId, $message, $type_post = '', $file = '') {

        require_once(JPATH_SITE.'/components/com_community/helpers/videos.php');
        $response = array();

        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $my = CFactory::getUser($uniqueId);
        $user = CFactory::getUser($uniqueId);
        $config = CFactory::getConfig();

        JPlugin::loadLanguage('plg_community_walls', JPATH_ADMINISTRATOR);

        // Load libraries
        //CFactory::load( 'models' , 'photos' );
        //CFactory::load( 'libraries' , 'wall' );
        //CFactory::load( 'helpers' , 'url' );
        //CFactory::load( 'libraries', 'activities' );

        $message = JString::trim($message);
        $message = strip_tags($message);

        $status		= CFactory::getModel('status');

        // Check data post
        if ($type_post == 'message') {
            if (!isset($message) || $message == '') {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_PROFILE_STATUS_MESSAGE_EMPTY');
                $response['data'] = null;
                return $response;
            }
        } else if ($type_post == 'photo') {
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            if(empty($ext)) {
                $response['status'] = false;
                $response['message'] = 'Sorry. Please chose image.';
                $response['data'] = null;
                return $response;
            }
            $extension = array('jpg', 'png', 'gif', 'jpeg', 'JPG', 'bmp', 'PNG', 'tiff');
            if(!in_array($ext, $extension)) {
                $response['status'] = false;
                $response['message'] = 'Sorry. Please check type image (jpg, png, gif, jpeg, bmp, tiff).';
                $response['data'] = null;
                return $response;
            }
        } else if ($type_post == 'video') {
            if (!isset($file) || trim($file) == '') {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_PROFILE_STATUS_LINK_EMPTY');
                $response['data'] = null;
                return $response;
            }
        }

        if (isset($type_post) && !empty($type_post) && $type_post != '') {
            // Upload photo
            if ($type_post == 'photo') {
                $photoupload = CPhotosHelper::APIphotoPreview($uniqueId);
                $id = $photoupload['data']->id; //var_dump($id);die;
            } else if ($type_post == 'video') {
                $videoupload = CVideosHelper::APILinkVideo($uniqueId, $file);
                $id = $videoupload['data']->video->id; //var_dump($type_post);die;
            }
        }
        // add post
        $attachment = array();
        $attachment['type'] = $type_post;
        $attachment['id'] = $id;
        $attachment['privacy'] = 0;
        $attachment['target'] = $uniqueId;
        $attachment['element'] = 'profile';
        $attachment['filter'] = 'active-profile';
        $result = CSystemHelper::ApiStreamAdd($user->username, $message, $attachment);

        // @rule: Send notification to the profile user.
        if ($my->id != $user->id) {
            //CFactory::load( 'libraries' , 'notification' );

            $params = new CParameter('');
            $params->set('url', 'index.php?option=com_community&view=profile&userid=' . $user->id);
            $params->set('message', $message);

            CNotificationLibrary::add('profile_submit_wall', $my->id, $user->id, JText::sprintf('PLG_WALLS_NOTIFY_EMAIL_SUBJECT', $my->getDisplayName()), '', 'profile.wall', $params);
        }
        if($result) {
            $ObjResponse = new stdClass();

            $Objactor = new stdClass();
            $Objactor->actor_id = $my->id;
            $Objactor->actor_name = $my->username;
            $Objactor->actor_avatar = $my->username;
            $actor_info = CFactory::getUser($my->id);
            $Objactor->actor_name = $actor_info->getDisplayName();
            if ($actor_info->_avatar && $actor_info->_avatar != '') {
                $Objactor->actor_avatar = $root . '/' . $actor_info->_avatar;
            } else {
                $Objactor->actor_avatar = '';
            }
            $profile_status = null;
            if($result['data']->comment_type == 'profile.status') {
                $profile_status = new stdClass();
                $ObjResponse->title = ''.CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_PROFILE_UPDATE_STATUS'));
                $profile_status->message = $result['data']->title;
            }
            $profile_video = null;
            if($result['data']->comment_type == 'videos.linking') {
                $act_video = JTable::getInstance('Video', 'CTable');
                $act_video->load($result['data']->cid);
//                        $oRow->video = $act->video;
                $profile_video = new stdClass();
                $ObjResponse->title = ''.CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_PROFILE_ADDED_VIDEO'));
                $profile_video->video_url = $act_video->path;
                $profile_video->message = $result['data']->title;
            }
            $photoObj = null;
            if($result['data']->comment_type == 'photos') {
//                $act_album = JTable::getInstance('Album', 'CTable');
//                $act_album->load($result['data']->cid);
                $params_photo = json_decode($result['data']->params);
                $photo = JTable::getInstance('Photo', 'CTable');
                $photo->load($params_photo->photoid);
                $photoObj = new stdClass();
                $ObjResponse->title = CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_PROFILE_ADDED_PHOTO'));
                $photoObj->photo_url = $root . '/' . $photo->original;
                $photoObj->message = $result['data']->title;
            }

            $ObjResponse->id = $result['data']->id;
            $ObjResponse->cid = $result['data']->actors;
            $ObjResponse->actors = $result['data']->cid;
            $ObjResponse->target = $result['data']->target;
            $ObjResponse->access = $result['data']->access;
            $ObjResponse->content = $result['data']->content;
            $ObjResponse->type = $result['data']->comment_type;
            $ObjResponse->actor = $Objactor;
            $ObjResponse->group = null;
            $ObjResponse->group_update = null;
            $ObjResponse->discussion = null;
            $ObjResponse->discussion_reply = null;
            $ObjResponse->announcemnet = null;
            $ObjResponse->events = null;
            $ObjResponse->events_update = null;
            $ObjResponse->group_join = null;
            $ObjResponse->video_link = $profile_video;
            $ObjResponse->photos = $photoObj;
            $ObjResponse->profile_status = $profile_status;
            $ObjResponse->profile_upload_avatar = null;
            $ObjResponse->profile_upload_cover = null;
            $ObjResponse->friend_connect = null;
            $ObjResponse->created = $result['data']->created;
            $ObjResponse->location = $result['data']->location;
            $ObjResponse->isFriend = false;
            $ObjResponse->isMyGroup = false;
            $ObjResponse->isMyEvent = false;
            $ObjResponse->isFeatured = false;

            $response['status'] = true;
            $response['message'] = 'success';
            $response['data'] = $ObjResponse;
        }

        return $response;
    }

    private function editStatus($uniqueId, $id, $message, $type_post = '') {
        require_once(JPATH_SITE.'/components/com_community/helpers/videos.php');
        $response = array();

        $my = CFactory::getUser($uniqueId);
        JPlugin::loadLanguage('plg_community_walls', JPATH_ADMINISTRATOR);

        $message = JString::trim($message);
        $message = strip_tags($message);

        // Check data post
        if ($type_post == 'message') {
            if (!isset($message) || $message == '') {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_PROFILE_STATUS_MESSAGE_EMPTY');
                $response['data'] = null;
                return $response;
            }
        } else {
            $response['status'] = false;
            $response['message'] = 'Sorry. You don\'t have permission.';
            $response['data'] = null;
            return $response;
        }

        // edit post
        $modal = CFactory::getModel('activities');
        // check exist stream
        $activity = $modal->getActivityStream($id);
        if (!empty($activity)) {
            //do edit function
            $condition = array('id' => $id);
            $update = array('title' => $message);
            $modal->update($condition, $update);

            $response['status'] = true;
            $response['message'] = 'Status updated.';
            $response['activityid'] = $id;
        } else {
            $response['status'] = false;
            $response['message'] = 'Not found activity.';
            $response['activityid'] = $id;
        }

        $response['status'] = true;
        $response['message'] = 'Status updated.';
        $response['data'] = array('id' => $id, 'message' => $message);

        return $response;
    }


    public function getDataHomePage($username) {
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');

//        if (!$username || $username == '') {
//            $user = CFactory::getUser();
//            $username = $user->username;
//        } else {
//            $userid = CUserHelper::getUserId($username);
        $user = CFactory::getUser();
        if ($user->id) {
            $username = $user->username;
        } else {
            $response['status'] = false;
            $response['error_message'] = 'Username not found.';
            header('Content-type: application/json; charset=UTF-8');
            header("Status", true, 400);
            echo json_encode($response);
            exit;
        }
//        }
        // Get My learning block
        $enrollable_only = null;
        $order = 'fullname ASC';
        $time = time();
        $lasttime = time() - 60 * 60 * 24 * 7;
        $swhere = ' AND lastaccess BETWEEN '.$lasttime.' AND '.$time.' ';
        $cursos = JoomdleHelperContent::getCourseList( (int) $enrollable_only,  $order, 0, $username, $where = '', $swhere = '', 'app');

        // Get My Circle block
        $model              = CFactory::getModel('groups');
        $limit = 5;

        //1 = my groups
        if (!$user->id) {
            //since this is my group only and if there is no userid provided, it should be empty
            $tmpGroups = array();
        } else {
            // $model->setState('limit', $limit); // limit the results
            $tmpGroups = $this->getGroup((int)$user->id, null, 0);
//            $tmpGroups = $model->getGroups($user->id, null, $limit);

        }

        // Get My Friends block
        $haveFriends = ($user->_friendcount > 0) ? true : false;

        // Check update user info
        $updatedProfie = true;
        $aboutMe = $user->getInfo('FIELD_ABOUTME');
        $gender = $user->getInfo('FIELD_GENDER');
        $birthDate = $user->getInfo('FIELD_BIRTHDATE');
        $avatar = $user->_avatar;
        $cover = $user->_cover;
        $interests = $user->getInfo('FIELD_INTEREST');
        $position = $user->getInfo('FIELD_POSITION');
//        $number = $user->getInfo('FIELD_MOBILE');
//        $address = $user->getInfo('FIELD_ADDRESS');
//        $postcode = $user->getInfo('FIELD_POSTCODE');
        if (empty($aboutMe) || empty($interests) || empty($position)) {
            $updatedProfie = false;
        }

        $data = new StdClass;
        $data->status = true;
        $data->message = 'Get data success';
        $data->updatedprofile = $updatedProfie;
        $data->courses = $cursos;
        $data->circles = $tmpGroups;
        $data->havefriends = $haveFriends;

        return $data;
    }
    private function getPhotoUpload($id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query
            ->select($db->quoteName(array('id', 'creator', 'image', 'thumbnail', 'original')))
            ->from($db->quoteName('#__community_photos'))
            ->where($db->quoteName('id')."=".$db->quote($id));

        $db->setQuery($query);
        $rows = $db->loadObject();
        return $rows;
    }
    private function getFileUpload($id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query
            ->select($db->quoteName(array('id', 'name', 'filepath')))
            ->from($db->quoteName('#__community_files'))
            ->where($db->quoteName('id')."=".$db->quote($id));

        $db->setQuery($query);
        $rows = $db->loadObject();
        return $rows;
    }
    private function createCircle($title, $about, $keywords, $privacy_public, $privacy_private, $groupid, $categoryid, $email, $parentid, $privacy_cecret = 0, $cover, $avatar) {
        $response = array();
        $my = CFactory::getUser();
        $group = JTable::getInstance('Group', 'CTable');
        $model = CFactory::getModel('groups');
        $config = CFactory::getConfig();
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        
        if($my->_permission) {
            return array(
                    'status' => false,
                    'message' => 'You don\'t have a permission to create circle, please contact with administrator. Thanks!'
            );
        }
        
        // create new circle
        $isNew = true;
        $message_status = 'created';
        // update circle
        if($groupid <> 0) {
            $group->load($groupid);
            $isNew = false;
            $message_status = 'updated';
            // check circle is found
            if (empty($group->id)) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_GROUPS_NOT_FOUND_ERROR');
                $response['groupid'] = $group->id;
                return $response;
            }
            $is_course_group = $model->getIsCourseGroup($group->id);
        }

        if($isNew) {
            if ($model->groupExist($title)) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_GROUPS_NAME_TAKEN_ERROR');
                $response['groupid'] = 0;
                return $response;
            }
            if (empty($title)) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_GROUPS_EMPTY_NAME_ERROR');
                $response['groupid'] = 0;
                return $response;
            }
            if (empty($about)) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_GROUPS_DESCRIPTION_EMPTY_ERROR');
                $response['groupid'] = 0;
                return $response;
            }

        } else {
            if(empty($title)) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_GROUPS_EMPTY_NAME_ERROR');
                $response['groupid'] = $group->id;
                return $response;
            }
            if (empty($about)) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_GROUPS_DESCRIPTION_EMPTY_ERROR');
                $response['groupid'] = 0;
                return $response;
            }
        }
        // check if create subcircle user not member of parent circle
        if($isNew) {
            if ($parentid != 0) {
                $member = JTable::getInstance('GroupMembers', 'CTable');

                $keys = array('groupId' => $parentid, 'memberId' => $my->id);
                $member->load($keys);
                if(!$member->memberid) {
                    $response['status'] = false;
                    $response['message'] = 'You doesn\'t have permission create subcircle.';
                    $response['groupid'] = 0;
                    return $response;
                }
            }
        }
        $params = new CParameter('');
        $params->set('discussordering', 0);
        $params->set('photopermission', GROUP_PHOTO_PERMISSION_ADMINS);
        $params->set('videopermission', GROUP_VIDEO_PERMISSION_ADMINS);
        $params->set('eventpermission', 2);
        $params->set('grouprecentphotos', GROUP_PHOTO_RECENT_LIMIT);
        $params->set('grouprecentvideos', GROUP_VIDEO_RECENT_LIMIT);
        $params->set('grouprecentevents', GROUP_EVENT_RECENT_LIMIT);
        $params->set('newmembernotification', 1);
        $params->set('joinrequestnotification', 1);
        $params->set('wallnotification', 1);
        $params->set('groupdiscussionfilesharing', 1);
        $params->set('groupannouncementfilesharing', 1);

        $now = new JDate();

        // Bind the post with the table first
        $group->name = $title;
        $group->description = $about;
        $group->categoryid = $categoryid;
        $group->summary = $about;
        $group->email = $email;
        $group->keyword = $keywords;
        $group->subgroup = 0;
        $group->website = '';
        $group->ownerid = $my->id;
        $group->created = $now->toSql();
//        $group->cover = JURI::base() . 'components/com_community/assets/cover-group-default.png';
        if ($privacy_cecret) {
            $group->unlisted = $privacy_cecret;
            $group->approvals = $privacy_cecret;
        } else if ($privacy_private && $privacy_cecret == 0 && $privacy_public == 0) {
            $group->unlisted = 0;
            $group->approvals = $privacy_private;
        } else if($privacy_public && $privacy_private && !$privacy_cecret) {
            $group->unlisted = 1;
            $group->approvals = 1;
        }
        else {
            $group->unlisted = 0;
            $group->approvals = 0;
        }
//        if($privacy_private && $privacy_cecret) {
//            $group->unlisted = 1;
//        } else {
//            $group->unlisted = 0;
//        }
//        $group->unlisted = $privacy_private;
//        $group->approvals = $privacy_public;

        $group->params = $params->toString();

        // @rule: check if moderation is turned on.
        $group->published = ( $config->get('moderategroupcreation') ) ? 0 : 1;
        $pathparent = '';
        if($isNew) {
            if(isset($parentid)) {
                $group->parentid = $parentid;
                $parentCircle = JTable::getInstance('Group', 'CTable');
                $parentCircle->load($parentid);
                $pathparent =  $parentCircle->path;
            } else {
                $group->parentid = 0;
            }
        }
        // we here save the group 1st. else the group->id will be missing and causing the member connection and activities broken.
        if(!$isNew) {
            $group->id = $group->id;
            $group->updateStats();
            if($privacy_private == 1 && $privacy_cecret == 1){
                $model->changePrivacyToClose($group->id, $group->path);
            }
        }
        $group->store();

        //store path
        if($isNew) {
        $group->path = $pathparent.'/'.$group->id;
        $group->store();
        }
        if($isNew) {
            // Since this is storing groups, we also need to store the creator / admin
            // into the groups members table
            $member = JTable::getInstance('GroupMembers', 'CTable');
            $member->groupid = $group->id;
            $member->memberid = $group->ownerid;

            // Creator should always be 1 as approved as they are the creator.
            $member->approved = 1;

            // @todo: Setup required permissions in the future
            $member->permissions = '1';
            $member->store();
            // @rule: Only add into activity once a group is created and it is published.
            if ($group->published && !$group->unlisted) {

                $act = new stdClass();
                $act->cmd = 'group.create';
                $act->actor = $my->id;
                $act->target = 0;
                //$act->title       = JText::sprintf('COM_COMMUNITY_GROUPS_NEW_GROUP' , '{group_url}' , $group->name );
                $act->title = JText::sprintf('COM_COMMUNITY_GROUPS_NEW_GROUP_CATEGORY', '{group_url}', $group->name, '{category_url}', $group->getCategoryName());
                $act->content = ( $group->approvals == 0) ? $group->description : '';
                $act->app = 'groups';
                $act->cid = $group->id;
                $act->groupid = $group->id;
                $act->group_access = $group->approvals;

                // Allow comments
                $act->comment_type = 'groups.create';
                $act->like_type = 'groups.create';
                $act->comment_id = CActivities::COMMENT_SELF;
                $act->like_id = CActivities::LIKE_SELF;

                // Store the group now.
                $group->updateStats();
                $group->store();

                $params = new CParameter('');
                $params->set('action', 'group.create');
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                //Chris: removed categoryid
                $params->set('category_url', 'index.php?option=com_community&view=groups&task=display&categoryid=' . $group->categoryid);

                // Add activity logging
                CActivityStream::add($act, $params->toString());
            }
        } else {

            $params = new CParameter('');
            $params->set('action', 'group.update');
            if($is_course_group) {
                $params->set('course_id', $is_course_group);
            }
            //add user points
            if(CUserPoints::assignPoint('group.updated')){
                $act = new stdClass();
                $act->cmd = 'group.update';
                $act->actor = $my->id;
                $act->target = 0;
                $act->title = ''; //JText::sprintf('COM_COMMUNITY_GROUPS_GROUP_UPDATED' , '{group_url}' , $group->name );
                $act->content = '';
                $act->app = 'groups.update';
                $act->cid = $group->id;
                $act->groupid = $group->id;
                $act->group_access = $group->approvals;

                // Add activity logging. Delete old ones
                CActivityStream::remove($act->app, $act->cid);
                CActivityStream::add($act, $params->toString());
            }
        }

        // need a new function
        if (!empty($cover['name'])) {
            $uploaded = $this->uploadCover($group->id, $cover, 'group');
        }
        if (!empty($avatar['name'])) {
            $uploaded = $this->uploadAvatar($group->id, $avatar, 'group', 'create');
        }
        $group->load($group->id);
        if(!empty($group->avatar)) {
            $group->avatar = $root.'/'.$group->avatar;
        }
        if(!empty($group->cover)) {
            $group->cover = $root.'/'.$group->cover;
        }
        $response['status'] = true;
        $response['message'] = 'circle '.$message_status.' success';
        $response['groupid'] = $group->id;
        $response['groupdetail'] = $group;
        return $response;
    }

    private function LP_register($lp_cirlce_name, $lp_origisition, $lp_owner_name, $lp_circle_about, $lp_keyword, $lp_email, $lp_home_add, $lp_postal_code, $groupid, $avatar, $cover) {
        $my = CFactory::getUser();
        $db= JFactory::getDBO();
        $group = JTable::getInstance('Group', 'CTable');
        $model = CFactory::getModel('groups');

        if($groupid == 0) {
            $resgister = $this->submissionLP($lp_cirlce_name, $lp_origisition, $lp_owner_name, $lp_circle_about, $lp_keyword, $lp_email, $lp_home_add, $lp_postal_code);
            if($resgister) {
                return $response = array(
                    'status' => true,
                    'message' => 'Thank you for contacting us. We will get back to you as soon as possible.'
                );
            } else {
                return $response = array(
                    'status' => false,
                    'message' => 'Learning Provider register failed.'
                );
            }
        } else {
            $circle = $model->getGroup($groupid);
            if(empty($circle)) {
                return $response = array(
                    'status' => false,
                    'message' => 'Learning Provider pending approval or not found.'
                );
            }
            $categoryid = 7;
            $response = array();
            $lp_circle = $this->createCircle($lp_cirlce_name, $lp_circle_about, $lp_keyword, 0, 0, $groupid, $categoryid, $lp_email, 0, 0, $cover, $avatar);
            if($lp_circle) {
                return $response = array(
                    'status' => true,
                    'message' => 'Learning Provider update successful.'
                );
            }
        }
        /*
         * Change logic for Learning Provider registration
         *
         *
        // create category course
        if($lp_circle['status'] == true) {
            // Part 1: Create category of course and assign manage into category
            if($groupid == 0) {
                require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
                $categories = array();
                $categories['name'] = $lp_name;
                $categories['parent'] = 0;
                $categories['idnumber'] = '';
                $categories['description'] = $lp_desc;
                $categories['descriptionformat'] = 1;
                $categories['username'] = $my->username;
                $cirlce_cate[] = $categories;
                $category_course = JoomdleHelperContent::call_method('create_categories', $cirlce_cate);
                if($category_course['status'] == true) {
                    $catid = $category_course['categories'][0]['id'];
                    $query =    'UPDATE '.$db->quoteName('#__community_groups')
                                .'SET '.$db->quoteName('moodlecategoryid').'='.$db->Quote($catid)
                                .'WHERE '.$db->quoteName('id').'='.$db->Quote($lp_circle['groupid']);

                    $db->setQuery($query);
                    $db->query();
                }
                // Part 2: Create category Hikashop
                $querySelect = 'SELECT category_id FROM #__hikashop_category WHERE category_name="Learning Provider" and category_parent_id=1';
                $db->setQuery($querySelect);
                $lp_categories = $db->loadObjectList();
                if(!empty($lp_categories)){
                    $category_namekey='product_'.time().'_'.rand();

                    $queryFindRight = 'SELECT MAX(category_right) as category_right FROM #__hikashop_category WHERE category_parent_id='.$lp_categories[0]->category_id;
                    $db->setQuery($queryFindRight);
                    $categoryRight = $db->loadObjectList();

                    if(!empty($categoryRight)){
                        $query ='INSERT IGNORE INTO #__hikashop_category (category_type, category_namekey, category_name,category_left,category_right,category_parent_id,category_created,category_modified,category_published,learningprovidercircleid) VALUES (\'product\',\''.$category_namekey.'\', \''.$lp_name.'\','.((int)$categoryRight[0]->category_right+1).','.((int)$categoryRight[0]->category_right+2).',\''.$lp_categories[0]->category_id.'\','.time().','.time().',1,'.(int)$lp_circle['groupid'].')';
                        $db->setQuery($query);
                        $db->query();
                    }

                }
                $response['status'] = true;
                $response['message'] = 'Learning Provider register successful.';
            } else {
                $response['status'] = true;
                $response['message'] = 'Learning Provider update successful.';
            }
        } else {
            $response['status'] = false;
            $response['message'] = $lp_circle['message'];
        }*/

    }

    // submission for LP register
    // update logic: After registration LP need system admin confirmation
    private function submissionLP($lp_name, $lp_origisition, $lp_yourname, $lp_desc, $lp_keyword, $lp_email, $lp_home_add, $lp_postal_code) {
        require_once(JPATH_SITE.'/components/com_rsform/controller.php');
        require_once(JPATH_ADMINISTRATOR.'/components/com_rsform/helpers/rsform.php');
        $my = CFactory::getUser();
        $submissionID = $this->addSubmission($my->username, $my->id);

        $term = 'I have read and I accept the Terms & Conditions associated with becoming a Learning Provider with Parenthexis';
        $submit = 'Submit';
        $formid = 5;
        $fieldSubmission = array(
            'LearningProviderName'=>$lp_name,
            'NameOfOrganisation'=>$lp_origisition,
            'CircleOwner' => $lp_yourname,
            'AboutLearningProvider'=> $lp_desc,
            'keywords'=>$lp_keyword,
            'EmailAddress'=>$lp_email,
            'BillingAddress'=>$lp_home_add,
            'PostalCode'=>$lp_postal_code,
            'Submit'=>$submit,
            'formId'=>$formid
        );
        if($submissionID) {
            foreach ($fieldSubmission as $key=>$val) {
                $this->addSubmissionValue($key, $val, $submissionID, $formid);
            }
            // send email
            RSFormProHelper::sendSubmissionEmails($submissionID);
            return true;
        } else {
            return false;
        }

    }

    private function getEmailAdmin() {
        $lpId = 5;
        $db = JFactory::getDbo();
        $db->setQuery("SELECT AdminEmailFrom FROM `#__rsform_forms` WHERE `FormId` = '".(int) $lpId."'");
        $AdminEmail = $db->loadObjectList();
        $db->execute();
        $AdminEmailFrom = $db->escape($AdminEmail[0]->AdminEmailFrom);
        return $AdminEmailFrom;
    }

    private function addSubmission($username, $userid) {
        $db = JFactory::getDbo();
        $ip = getenv('HTTP_CLIENT_IP')?:
            getenv('HTTP_X_FORWARDED_FOR')?:
                getenv('HTTP_X_FORWARDED')?:
                    getenv('HTTP_FORWARDED_FOR')?:
                        getenv('HTTP_FORWARDED')?:
                            getenv('REMOTE_ADDR');

        $query  = 'INSERT INTO '. $db->quoteName('#__rsform_submissions')
            .' SET ' . $db->quoteName('Formid').' = ' . $db->Quote(5)
            . ', '. $db->quoteName('DateSubmitted').' = ' . $db->Quote(date("Y-m-d H:i:s"))
            . ', '. $db->quoteName('UserIp').' = '. $db->Quote($ip)
            . ', '. $db->quoteName('Username').' = ' . $db->Quote($username)
            . ', '. $db->quoteName('UserId').' = ' . $db->Quote($userid)
            . ', '. $db->quoteName('Lang').' = ' . $db->Quote("en-GB")
            . ', '. $db->quoteName('confirmed').' = ' . $db->Quote(0)
            . ', '. $db->quoteName('LearningProvider').' = ' . $db->Quote(0);
        $db->setQuery($query);
        $db->query();
        if ($db->getErrorNum())
        {
            JError::raiseError(500, $db->stderr());
        }
        $submissionid = $db->insertid();
        return $submissionid;
    }

    private function addSubmissionValue($field, $value, $subid, $formid) {
        $db = JFactory::getDbo();
        $query = 'INSERT INTO '.$db->quoteName('#__rsform_submission_values')
            .' SET '.$db->quoteName('FormId').' = '.$db->Quote($formid)
            .', '.$db->quoteName('SubmissionId').' = '.$db->Quote($subid)
            .', '.$db->quoteName('FieldName').' = '.$db->Quote($field)
            .', '.$db->quoteName('FieldValue').' = '.$db->Quote($value);
        $db->setQuery($query);
        $db->query();
        if($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }
        return true;
    }

    private function LP_list($limit, $page) {
        require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');
        $my = JFactory::getUser();
        $lp_category = 7; // hard code
        $sorting = '';
        $model = CFactory::getModel('groups');
        $eventsModel = CFactory::getModel('Events');
        $groups = $model->getAllGroupsMobile($lp_category,$sorting, null, $limit, false, $page);
        if($groups) {
            foreach ($groups as $k => $r) {
                // get category name
                if($r->moodlecategoryid == 0) {unset($groups[$k]); continue;};
                $groups[$k]->avatar = (string) $r->avatar;
                $groups[$k]->lastaccess = (string) $r->lastaccess;
                if ($r->categoryname == '') {
                    $category = JTable::getInstance('GroupCategory', 'CTable');
                    $category->load($r->categoryid);

                    $groups[$k]->categoryname = (string) $category->name;
                }
                // get group admin name
//                    $user_owner = '';
//                    if ($r->ownername == '') {
                $user_owner = CFactory::getUser($r->ownerid);

                $groups[$k]->ownername = (string) $user_owner->getDisplayName();
//                    }

                $groups[$k]->activity_count = My_Groups::groupActivities($r->id);

                // count group event
                $totalEvents = $eventsModel->getTotalGroupEvents($r->id);
                $groups[$k]->eventCount = $totalEvents;

                $groups[$k]->moodlecategoryid = (int) $r->moodlecategoryid;

            }
            $data = new StdClass;
            $data->status = true;
            $data->message = 'success';
            $data->groups = array_values($groups);
        } else {
            $data = new StdClass;
            $data->status = true;
            $data->message = 'Empty course';
            $data->groups = $groups;
        }
        return $data;
    }
    private function LP_about($lp_circle_id) {
        $my = CFactory::getUser();
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($lp_circle_id);

        $groupModel = CFactory::getModel('groups');
        // get members list
        $members = $this->getMembersGroup($group->id);
        if($members['members']) {
            $members_group = $this->member_array_unique($members['members']);
        }
        // Test if the current user is admin
        $isAdmin = $groupModel->isAdmin($my->id, $group->id);

        $group->description = strip_tags($group->description);
        if ($group->avatar == '') {
            $group->avatar = JURI::base() . 'components/com_community/assets/group.png';
        } else {
            $group->avatar = $root . '/' . $group->avatar;
        }
        if ($group->cover == '') {
            $group->cover = JURI::base() . 'components/com_community/assets/cover-group-default.png';
        } else {
            $group->cover = $root . '/' . $group->cover;
        }
        if ($group->ownername == '') {
            $user_owner = CFactory::getUser($group->ownerid);

            $group->ownername = $user_owner->getDisplayName();
        }
        if($group->moodlecategoryid) {
            require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
            $user_role = JoomdleHelperContent::call_method('get_user_category_role', (int) $group->moodlecategoryid, $my->username);
            if($user_role) {
                foreach ($user_role['role'] as $role) {
                    if($role['roleid'] == 0) {
                        $rolename = 'leaner';
                        $roleid = 5;
                    } elseif($role['roleid'] == 1) {
                        $rolename = 'manager';
                        $roleid = $role['roleid'];
                    } elseif($role['roleid'] == 2) {
                        $rolename = 'coursecreator';
                        $roleid = $role['roleid'];
                    } elseif($role['roleid'] == 3) {
                        $rolename = 'contentcreator';
                        $roleid = $role['roleid'];
                    } elseif($role['roleid'] == 4) {
                        $rolename = 'facilitator';
                        $roleid = $role['roleid'];
                    }
                }
            }
        } else {
            $rolename = 'Circle is not LP Circle.';
            $roleid = 0;
        }
        $return->name = $group->name;
        $return->lp_circle_name = $group->name;
        $return->lp_circle_about = $group->description;
        $return->description = $group->description;
        $return->avatar = $group->avatar;
        $return->cover = $group->cover;
        $return->keyword = $group->keyword;
        $return->postalcode = '';
        $return->isAdmin = $isAdmin;
        $return->ownername = $group->ownername;
        $return->ownerid = $group->ownerid;
        $return->permission_role = $rolename;
        $return->permission_id = $roleid;

        $response->status = true;
        $response->message = 'success.';
        $response->about = $return;
        $response->member = $members_group;
        return $response;

    }
    // get course of Learning Provider
    public function getCourseOfLP ($username, $groupid)
    {
        if(empty($groupid)) {
            return array(
                'status' => false,
                'message' => 'Sorry! Group id can not be empty.'
            );
        }
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        if ($group->id == 0) {
            return array(
                'status' =>false,
                'message' => JText::_('COM_COMMUNITY_GROUPS_ID_NOITEM')
            );
        }
        if($group->moodlecategoryid == 0) {
            return array(
                'status' => false,
                'message' => 'Sorry! This circle is not Learning Provider Circle.'
            );
        }
        $groupsModel = CFactory::getModel('groups');
        $my = CFactory::getUser();

        require_once(JPATH_ADMINISTRATOR.'/components/com_hikashop/helpers/helper.php' );
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        require_once(JPATH_SITE.'/components/com_hikashop/helpers/lpapi.php');

        $moodlecategoryid = $group->moodlecategoryid;
        $isManager = false;
        $groupuserrole = $groupsModel->getMemberRole($group->id, $my->id);
        $courseids = $groupsModel->getPublishCoursesToSocial($group->id);
        $lpHikashopCategory = LpApiController::getLPHikaCategory($group->id);

        if (strpos($groupuserrole, 'manager') !== false) {
            $isManager = true;
            $isGuest = false;
        }
        $isSuperAdmin = COwnerHelper::isCommunityAdmin();
        $isAdmin = $groupsModel->isAdmin($my->id, $group->id);

        $arrLPCourseIds = [];
        $lpPublished = [];
        $lpUnpublished = [];
        $isPublic = 0;

        $newcourse = [];
        $pendingcourse = [];
        $approvedcourse = [];
        $editingCourses = [];
        $facilitatingCourses = [];

        $lpCourses = JoomdleHelperContent::call_method ('learning_provider_courses', $username, (string)$moodlecategoryid);
        if ($isSuperAdmin || $isAdmin || $isManager) {
            $hikashopCourses = LpApiController::getHikashopCourses($group->id);
            foreach ($lpCourses['courses'] as $course) {
                $arrLPCourseIds[] = $course['id'];
                $arrAllRoles = [];

                foreach ($course['assignedUsers'] as $value) {
                    $decode_user_roles = json_decode($value['role']);

                    foreach ($decode_user_roles as $dur) {
                        if ($dur->sortname == 'editingteacher')
                            $course['ccUsername'] = $value['username'];
                        array_push($arrAllRoles, $dur->sortname);
                    }
                }

                $allRoles = implode(', ', $arrAllRoles);

                $roles = JoomdleHelperContent::call_method ('get_user_role', (int)$course['id'], $username);
                foreach ($hikashopCourses as $hikacourse) {
                    if ($hikacourse->product_code == $course['id']) {
                        $currentUserRoles = array();
                        foreach ($roles['roles'] as $role) {
                            $user_roles = $role['role'];
                            $decode_user_roles = json_decode($user_roles);
                            foreach ($decode_user_roles as $dur) {
                                array_push($currentUserRoles, $dur->sortname);
                            }
                            $varCurrentRoles = implode(', ', $currentUserRoles);
                            $posmanager = strpos($varCurrentRoles, "manager");
                            $poscontentcreator = strpos($varCurrentRoles, "coursecreator");

                            if ($posmanager !== false || $poscontentcreator !== false) {
                                if ($hikacourse->product_published == 1) {
                                    $course['hikashopId'] = $hikacourse->product_id;
                                    $course['hikashopAlias'] = $hikacourse->product_alias;
                                    $lpPublished[] = $course;
                                } else {
                                    $pos = strpos($allRoles, "editingteacher");
                                    if (!$pos) {
                                        $course['hikashopId'] = $hikacourse->product_id;
                                        $lpUnpublished[] = $course;
                                    }
                                }
                            }
                        }
                    }
                }

                $hikaCourse = LpApiController::isCourseInHika($course['id']);
                $course['hasEditingTeacher'] = in_array('editingteacher', $arrAllRoles) ? true : false;

                if (!$course['hasEditingTeacher'] && !$course['pending_approve'] && !$course['approved'] && !$course['unapproved'] &&
                    empty($hikaCourse) &&
                    ( ($course['course_status'] != 'published') || (!$course['published'] && !$course['unpublished']) ))
                    $newcourse[] = $course;

                if (($course['unapproved'] || $course['hasEditingTeacher']) &&
                    !$course['pending_approve'] && !$course['approved'] && !$course['published'])
                    $editingCourses[] = $course;

                if ($course['pending_approve'])
                    $pendingcourse[] = $course;

                if ($course['approved'] && !count($groupsModel->checkProductInHikashop($course['id'])) > 0)
                    $approvedcourse[] = $course;
            }
        } else {
            $hikashopCourses = LpApiController::getHikashopCourses($group->id, 1);
            $arrPublishedCourseIds = [];
            $hikaCourses = [];
            foreach ($hikashopCourses as $hikacourse) {
                $arrPublishedCourseIds[] = $hikacourse->product_code;
                $hikaCourses[$hikacourse->product_code] = $hikacourse;
            }
            foreach ($lpCourses['courses'] as $course) {
                $arrLPCourseIds[] = $course['id'];
                foreach ($course['assignedUsers'] as $value) {
                    if ($value['username'] != $username)
                        continue;
                    else {
                        $decode_user_roles = json_decode($value['role']);

                        foreach ($decode_user_roles as $dur) {
                            if ($dur->sortname == 'editingteacher') {
                                if ($course['pending_approve'])
                                    $pendingcourse[] = $course;
                                else
                                    $editingCourses[] = $course;
                                $isContentCreator = true;
                                $isGuest = false;
                                break;
                            }
                        }
                        break;
                    }
                }

                if ($course['isFacilitator']) {
                    $facilitatingCourses[] = $course;
                    $isFacilitator = true;
                    $isGuest = false;
                }

                if (!in_array($course['id'], $arrPublishedCourseIds))
                    continue;
                $course['hikashopId'] = $hikaCourses[$course['id']]->product_id;
                $course['hikashopAlias'] = $hikaCourses[$course['id']]->product_alias;
                $lpPublished[] = $course;
            }
        }
        $composing_courses = array_merge($newcourse, $editingCourses);
        $data['composing_courses'] = $composing_courses;
        $data['pendding_courses'] = $pendingcourse;
        $data['approved_courses'] = $approvedcourse;
        $data['facilitator_courses'] = $facilitatingCourses;
        $data['editing_courses'] = $editingCourses;
        $data['published_courses'] = $lpPublished;
        $data['unpublished_courses'] = $lpUnpublished;
        if($lpCourses) {
            $response['status'] = true;
            $response['message'] = 'Success.';
            $response['data'] = $data;
        } else {
            $response['status'] = true;
            $response['message'] = 'Success.';
            $response['data'] = $data;
        }
        return $response;
    }

    function member_array_unique($array, $keep_key_assoc = false)
    {
        $duplicate_keys = array();
        $tmp         = array();

        foreach ($array as $key=>$val)
        {
            // convert objects to arrays, in_array() does not support objects
            if (is_object($val))
                $val = (array)$val;

            if (!in_array($val, $tmp))
                $tmp[] = $val;
            else
                $duplicate_keys[] = $key;
        }

        foreach ($duplicate_keys as $key)
            unset($array[$key]);

        return $keep_key_assoc ? $array : array_values($array);
    }

    private function deleteTopic($topicid, $groupid) {
        $my = CFactory::getUser();
        if ($my->id == 0) {
            return $this->blockUnregister();
        }
        $mainframe = JFactory::getApplication();

        $config = CFactory::getConfig();

        $data_root = $mainframe->getCfg('dataroot');
        $folder = $data_root . '/' . $config->getString('imagefolder') . '/comments/';
        $groupsModel = CFactory::getModel('groups');
        $wallModel = CFactory::getModel('wall');
        $activityModel = CFactory::getModel('activities');
        $fileModel = CFactory::getModel('files');
        $discussion = JTable::getInstance('Discussion', 'CTable');
        $discussion->load($topicid);

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        if(!$group->id) {
            return array(
                'status' => false,
                'message' => 'Circle not found or has been deleted.'
            );
        }
        $creator = CFactory::getUser($group->ownerid);

        $group->ownername = $creator->getDisplayName();

        if($group->published == 0) {
            return array(
                'status' => false,
                'message' => 'Circle '.$group->name.' has been archived. Please contact '.$group->ownername.' for more information.'
            );
        }
        $isGroupAdmin = $groupsModel->isAdmin($my->id, $group->id);

        if ($my->id == $discussion->creator || $isGroupAdmin) {

            if ($discussion->delete()) {
                // Remove the replies to this discussion as well since we no longer need them
                $wallModel->deleteAllChildPosts($topicid, 'discussions');
                // Remove from activity stream
                CActivityStream::remove('groups.discussion', $topicid);
                // Remove Discussion Files
                $fileModel->alldelete($topicid, 'discussion');
                // Assuming all files are deleted, remove the folder if exists
                if (JFolder::exists(JPATH_ROOT . $topicid)) {
                    JFolder::delete($folder . $topicid);
                }
                $response['status'] = true;
                $response['message'] = 'Topic deleted';
            }
        } else {
            $response['status'] = false;
            $response['message'] = 'Delete topic failed';
        }

        return $response;
    }
    private function addMember($groupid, $memberid) {
        $group          = JTable::getInstance( 'Group' , 'CTable' );
        $groupModel = CFactory::getModel('groups');
        $member         = JTable::getInstance( 'GroupMembers' , 'CTable' );
        $my = CFactory::getUser();
        if ($my->id == 0) {
            return $this->blockUnregister();
        }
        if (!$groupid) {
            $response['status'] = false;
            $response['message'] = 'No group seleted';
        }
        if(empty($memberid)) {
            $response['status'] = false;
            $response['message'] = 'No member seleted';
        }
        $group->load($groupid);
        
        if(!$group->id) {
            return array(
                'status' => false,
                'message' => 'Circle not found or has been deleted.'
            );
        }
        $creator = CFactory::getUser($group->ownerid);

        $group->ownername = $creator->getDisplayName();

        if($group->published == 0) {
            return array(
                'status' => false,
                'message' => 'Circle '.$group->name.' has been archived. Please contact '.$group->ownername.' for more information.'
            );
        }

        $isAdmin = $groupModel->isAdmin($my->id, $group->id);

        if((!$isAdmin)) {
            return $response = array(
                'status' => false,
                'message' => 'You don\'t have a permission to assign.',
            );
        }
        $member_id = explode(',', $memberid);

        // check circle is learning circle
        $params = json_decode($group->params);
        if (isset($params->course_id) && $params->course_id > 0) {
            $courseid = (int) $params->course_id;
            require_once(JPATH_ROOT . '/administrator/components/com_joomdle/helpers/content.php');
            $act = 1;
            $data = array() ;
            foreach ($member_id as $key=>$val) {
                $user = CFactory::getUser($val);
                $ass['rid'] = 5;
                $ass['frname'] = $user->username;
                $ass['status'] = 1;
                $data[] = $ass;
            }
            // update role for manage
//                $course_role = JoomdleHelperContent::call_method('get_user_role', (int) $params->course_id, $username);
//                print_r($course_role);die;
//                $data_manger = '[{ "rid" : "4",  "frname" : '.$username.',  "status" : 1}]';
//                JoomdleHelperContent::call_method('set_user_role', $act, (int) $params->course_id, $username, $data_manger);
            $assign_learner = JoomdleHelperContent::call_method('set_user_role', $act, $courseid, $my->username, json_encode($data));
        }
        foreach ($member_id as $key=>$value) {
            $member->groupid        = $group->id;
            $member->memberid       = $value;

            $member->permissions    = 0;
            $member->approved       = '1';
            $member->date           = time();

            $user = CFactory::getUser($value);

            $exits = $member->exists();
            if($exits) {
                $message = 'You are now a member of '.$group->name;
            } else {
                $message = $creator->name. ' has added you to '.$group->name;
            }

            if($member->store()) {

                // email notification
                $params = new CParameter('');
                $params->set('url', CRoute::getExternalURL('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id));
                $params->set('group', $group->name);
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);

                CNotificationLibrary::add('groups_member_approved', $group->ownerid, $user->id, JText::sprintf('COM_COMMUNITY_GROUP_MEMBER_APPROVED_EMAIL_SUBJECT'), '', 'groups.memberapproved', $params);

//                $group->updateStats();
//                $group->store();
                
                $response['status'] = true;
                $response['message'] = 'Member added success';

                // push notification
                $message_send = array(
                    'mtitle' => $message,
                    'mdesc' => $group->description
                );
                $pay_load = $this->pushNotification($message_send, $user->id, $group->id, $group->id, 'circle_added_member', '');
                $response['push_data'] = $pay_load['android'];
                $response['push_ios'] = $pay_load['ios'];
            }else {
                $response['status'] = false;
                $response['message'] = 'Member not added';
                $response['push_data'] = new stdClass();
                $response['push_ios'] = new stdClass();
            }
        }

        return $response;
    }


    private function lockTopic($groupid, $topicid, $status) {
        $mainframe = JFactory::getApplication();

        $my = CFactory::getUser();
        if ($my->id == 0) {
            return $this->blockUnregister();
        }
        if (empty($topicid) || empty($groupid)) {
            echo JText::_('COM_COMMUNITY_INVALID_ID');
            return;
        }
        $groupsModel = CFactory::getModel('groups');
        $wallModel = CFactory::getModel('wall');
        $discussion = JTable::getInstance('Discussion', 'CTable');
        $discussion->load($topicid);

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        $isGroupAdmin = $groupsModel->isAdmin($my->id, $group->id);
        if ($my->id == $discussion->creator || $isGroupAdmin || COwnerHelper::isCommunityAdmin()) {
            $lockStatus = $status;
            $confirmMsg = $lockStatus ? JText::_('COM_COMMUNITY_DISCUSSION_LOCKED') : JText::_('COM_COMMUNITY_DISCUSSION_UNLOCKED');

            if ($discussion->lock($topicid, $lockStatus)) {
                $response['status'] = true;
                $response['message'] = $confirmMsg;
            }
        } else {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_NOT_ALLOWED_TO_LOCK_GROUP_TOPIC');
        }
        return $response;
    }

    private function loadFriend($groupid) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $my = CFactory::getUser();
        if (empty($groupid) || empty($groupid)) {
            return array(
                'status' => false,
                'message' => JText::_('COM_COMMUNITY_INVALID_ID')
            );
        }
        $groupsModel = CFactory::getModel('Groups');

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);

        $params = json_decode($group->params);
        if(isset($params->course_id) && $params->course_id > 0) {
            $circintlepublished = $groupsModel->getShareNameGroups((int) $params->course_id, null);
            $frendlist = $groupsModel->getAllMember($circintlepublished[0]->id);
        } else{
        $frendlist = $groupsModel->getAllMember($group->parentid);
        }
        $listFriend = array();
        $data = new stdClass();
        if($frendlist) {
            foreach ($frendlist as $friend) {
                if($friend->id != $group->ownerid) {
                $friends = new stdClass();
                    $friends->id = $friend->id;
                    $user = CFactory::getUser($friend->id);
                $avatar = $user->_avatar;
                if($avatar == '') {
                    $friends->avatar = JURI::base() . 'components/com_community/assets/user-Male.png';
                } else {
                    $friends->avatar = $root.'/'.$avatar;
                }

                $friends->name = $user->getDisplayName();
                $listFriend[] = $friends;
            }
            }
            $data->status = true;
            $data->message = 'Friend load success';
            $data->friend = $listFriend;
        } else {
            $data->status = false;
            $data->message = 'Friend load failed';
            $data->friend = $listFriend;
        }
        
        return $data;
    }
    // circle admin
    private function makeORrevertAdminCircle($groupid, $memberid, $type, $role) {
        $my = CFactory::getUser();

        if(empty($groupid)) {
            $response['status'] = false;
            $response['message'] = 'Circle is not available.';
            return $response;
        }
        if(empty($memberid)) {
            $response['status'] = false;
            $response['message'] = 'Member is not available.';
            return $response;
        }
        $model = CFactory::getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        $type_arr = array('makeAdmin', 'revertAdmin');
        if(!in_array($type, $type_arr) || empty($type)) {
            $response['status'] = false;
            $response['message'] = 'Request doesn\'t exits.';
            return $response;
        }
        if($type == 'makeAdmin') {
            $doAdd = true;
            $status = 1;
            $roleid = 1;
            $message = JText::_('COM_COMMUNITY_GROUPS_NEW_ADMIN_MESSAGE');
        } else if($type == 'revertAdmin') {
            $doAdd = false;
            $status = 0;
            $message = JText::_('COM_COMMUNITY_GROUPS_NEW_USER_MESSAGE');
            if($role == 'manager') {
                $roleid = 1;
            }
            elseif($role == 'facilitator') {
                $roleid = 4;
            }
        }
        if (!$my->authorise('community.edit', 'groups.admin.' . $groupid, $group)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_PERMISSION_DENIED_WARNING');
            return $response;
        } else {
            $member = JTable::getInstance('GroupMembers', 'CTable');

            $keys = array('groupId' => $group->id, 'memberId' => $memberid);
            $member->load($keys);
            $member->permissions = $doAdd ? 1 : 0;

            $user = CFactory::getUser($memberid);
            $params = json_decode($group->params);
            if (isset($params->course_id) && $params->course_id > 0) {
                $courseid = (int) $params->course_id;
                require_once(JPATH_ROOT . '/administrator/components/com_joomdle/helpers/content.php');
                $remove_role = JoomdleHelperContent::call_method('remove_user_role', $user->username, $courseid, $roleid, true);
            }
            if($member->store()) {
                $response['status'] = true;
                $response['message'] = $message;
                return $response;
            } else {
                $response['status'] = false;
                $response['message'] = 'Circle Make Amin failed.';
                return $response;
            }
        }

    }
    // a10b3fe5306ac640003910101f5c2603
    //remove member
    private function removeMemberCircle($groupid, $memberid, $ban) {
        if(empty($groupid)) {
            $response['status'] = false;
            $response['message'] = 'Circle can not be empty.';
            return $response;
        }
        if(empty($memberid)) {
            $response['status'] = false;
            $response['message'] = 'Member can not be empty.';
            return $response;
        }
        $model = CFactory::getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        $ban_arr = array('0', '1');
        if(!in_array($ban, $ban_arr)) {
            $response['status'] = false;
            $response['message'] = 'Request remove with ban doesn\'t exits.';
            return $response;
        }
        $my = CFactory::getUser();
        $isAdmin = $model->isAdmin($my->id, $group->id);
        if(!$ban) {
            if (!$isAdmin) {
                $errorMsg = $my->authoriseErrorMsg();
                if ($errorMsg == 'blockUnregister') {
                    $response['status'] = false;
                    $response['message'] = $errorMsg;
                    return $response;
                } else {
                    $response['status'] = false;
                    $response['message'] = $errorMsg;
                    return $response;
                }
            } else {
                $groupMember = JTable::getInstance('GroupMembers', 'CTable');
                $keys = array('groupId' => $groupid, 'memberId' => $memberid);
                $groupMember->load($keys);
                $user = CFactory::getUser($memberid);
                // if is learning circle
                $params = json_decode($group->params);
                if(isset($params->course_id) && $params->course_id > 0) {
                    $courseid = (int) $params->course_id;
                    require_once(JPATH_ROOT . '/administrator/components/com_joomdle/helpers/content.php');
                    /*$act = 1;
                    $data_assign = array();
                    $ass['rid'] = 5;
                    $ass['frname'] = $user->username;
                    $ass['status'] = 0;
                    $data_assign[] = $ass;
                    $assign_learner = JoomdleHelperContent::call_method('set_user_role', $act, $courseid, $my->username, json_encode($data_assign));*/
                    $assign_learner = JoomdleHelperContent::call_method('unenrol_user', $user->username, $courseid);
                }
                $data = new stdClass();

                $data->groupid = $groupid;
                $data->memberid = $memberid;

                $model->removeMember($data);
                $user->updateGroupList(true);
                $this->triggerGroupEvents('onGroupLeave', $group, $memberid);

                //add user points
                CUserPoints::assignPoint('group.member.remove', $memberid);

                //delete invitation
                $invitation = JTable::getInstance('Invitation', 'CTable');
                $invitation->deleteInvitation($groupid, $memberid, 'groups,inviteUsers');


            }

            // Store the group and update the data
            $group->updateStats();
            $group->store();
            $response['status'] = true;
            $response['message'] = JText::_('COM_COMMUNITY_GROUPS_MEMBERS_DELETE_SUCCESS');
            return $response;
        } else {
            return $this->banUnbanMember($groupid, $memberid, 'ban');
        }
    }
    // ban and unban member
    private function banUnbanMember($groupid, $memberid, $type) {
        if(empty($groupid)) {
            $response['status'] = false;
            $response['message'] = 'Circle can not be empty.';
            return $response;
        }
        if(empty($memberid)) {
            $response['status'] = false;
            $response['message'] = 'Member can not be empty.';
            return $response;
        }
        $type_arr = array('ban', 'unban');
        if(!in_array($type, $type_arr) || empty($type)) {
            $response['status'] = false;
            $response['message'] = 'Request doesn\'t exits.';
            return $response;
        }
        if($type == 'ban') {
            $doBan = true;
        } elseif($type == 'unban') {
            $doBan = false;
        }
        $my = CFactory::getUser();

        $groupModel = CFactory::getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);

        $creator = CFactory::getUser($group->ownerid);

        $group->ownername = $creator->getDisplayName();
        if(!$group->id) {
            return array(
                'status' => false,
                'message' => 'Circle not found or has been deleted.'
            );
        }
        if($group->published == 0) {
            return array(
                'status' => false,
                'message' => 'Circle '.$group->name.' has been archived. Please contact '.$group->ownername.' for more information.'
            );
        }
        if (!$my->authorise('community.update', 'groups.member.ban.' . $groupid, $group)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_PERMISSION_DENIED_WARNING');
            return $response;
        } else {
            $member = JTable::getInstance('GroupMembers', 'CTable');
            $keys = array('groupId' => $group->id, 'memberId' => $memberid);
            $member->load($keys);

            $member->permissions = ($doBan) ? COMMUNITY_GROUP_BANNED : COMMUNITY_GROUP_MEMBER;

            $member->store();

            $group->updateStats();

            $group->store();
            $params = json_decode($group->params);
            $user = CFactory::getUser($memberid);
            if ($doBan) { //if user is banned, display the appropriate response and color code
                //trigger for onGroupBanned
                $this->triggerGroupEvents('onGroupBanned', $group, $memberid);
                // if is learning circle
                if(isset($params->course_id) && $params->course_id > 0) {
                    $courseid = (int) $params->course_id;
                    require_once(JPATH_ROOT . '/administrator/components/com_joomdle/helpers/content.php');
                    /*$act = 1;
                    $data_assign = array();
                    $ass['rid'] = 5;
                    $ass['frname'] = $user->username;
                    $ass['status'] = 0;
                    $data_assign[] = $ass;*/
                    $assign_learner = JoomdleHelperContent::call_method('unenrol_user', $user->username, $courseid);
                }
                $response['status'] = true;
                $response['message'] = JText::_('COM_COMMUNITY_GROUPS_MEMBER_BEEN_BANNED');
                return $response;
            } else {
                //trigger for onGroupUnbanned
                $this->triggerGroupEvents('onGroupUnbanned', $group, $memberid);
                if(isset($params->course_id) && $params->course_id > 0) {
                    $courseid = (int) $params->course_id;
                    require_once(JPATH_ROOT . '/administrator/components/com_joomdle/helpers/content.php');
                    $act = 1;
                    $data_assign = array();
                    $ass['rid'] = 5;
                    $ass['frname'] = $user->username;
                    $ass['status'] = 1;
                    $data_assign[] = $ass;
                    $assign_learner = JoomdleHelperContent::call_method('set_user_role', $act, $courseid, $my->username, json_encode($data_assign));
                }
                $response['status'] = true;
                $response['message'] = JText::_('COM_COMMUNITY_GROUPS_MEMBER_BEEN_UNBANNED');
                return $response;
            }
        }
    }

    public function triggerGroupEvents($eventName, &$args, $target = null) {
        CError::assert($args, 'object', 'istype', __FILE__, __LINE__);

        require_once( COMMUNITY_COM_PATH . '/libraries/apps.php' );
        $appsLib = CAppPlugins::getInstance();
        $appsLib->loadApplications();

        $params = array();
        $params[] = $args;

        if (!is_null($target))
            $params[] = $target;

        $appsLib->triggerEvent($eventName, $params);
        return true;
    }

    private function ViewEvent($groupid, $eventid) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        if(empty($groupid)) {
            return array(
                'status' => false,
                'message' => 'Circle can not empty.',
            );
        }
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        if($group->id == 0) {
            return array(
                'status' => false,
                'message' => 'Cirlce not found.'
            );
        }
        $my = CFactory::getUser();
        if (empty($eventid)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_EVENTS_NOT_AVAILABLE_ERROR');
            return $response;
        }
        $event = JTable::getInstance('Event', 'CTable');
        if($event->load($eventid)) {
            if($event->unlisted && !$event->isMember($my->id) && !$event->getUserStatus($my->id) == 0){
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_EVENTS_UNLISTED_ERROR');
                return $response;
            }
            if (!$event->isPublished()) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_EVENTS_UNDER_MODERATION');
                return $response;
            }

            $eventMembers = JTable::getInstance('EventMembers', 'CTable');
            $keys = array('eventId' => $event->id, 'memberId' => $my->id);
            $eventMembers->load($keys);

            $eventDetail = new stdClass();
            $eventDetail->id = $event->id;
            $eventDetail->title = $event->title;
            $eventDetail->description = $event->description;
            $eventDetail->location = $event->location;
            $eventDetail->unlisted = $event->unlisted;
            $eventDetail->admin_id = $event->creator;
            $user = CFactory::getUser($event->creator);
            $eventDetail->admin_name = $user->getDisplayName();
            $avatar = $user->_avatar;
            if($avatar == '') {
                $eventDetail->admin_avatar = JURI::base() . 'components/com_community/assets/user-Male.png';
            } else {
                $eventDetail->admin_avatar = $root.'/'.$avatar;
            }
            $eventDetail->startdate = $event->startdate;
            $eventDetail->enddate = $event->enddate;
            $eventDetail->permission = $event->permission;
            if($my->id == $event->creator) {
                $eventDetail->isAdminEvent = true;
                $eventDetail->status = 1;
            } else {
                $eventDetail->isAdminEvent = false;
                $eventDetail->status = $eventMembers->status;
            }
            $eventDetail->cover_circle = $group->getCover();
            if($event->avatar == NULL) {
                $eventDetail->avatar = "";
            } else {
                $eventDetail->avatar = $root . '/'.$event->avatar;
            }
            if($event->cover == NULL) {
                $eventDetail->cover = "";
            } else {
                $eventDetail->cover = $root . '/'.$event->cover;
            }
            $eventDetail->created = $event->created;

            // begin get attending
            $type = 1;
            $guestsIds = $event->getMembers($type, 0, false, $approval);

            $userids = array();
            foreach ($guestsIds as $uid) {
                $userids[] = $uid->id;
            }
            CFactory::loadUsers($userids);
            $attending = array();
            $data = new stdClass();

            for ($i = 0; $i < count($guestsIds); $i++) {
                $user_attend = CFactory::getUser($guestsIds[$i]->id);
                $guests = new stdClass();
                $guests->userid = $user_attend->id;
                $guests->name = $user_attend->getDisplayName();
                $avatar_attend = $user_attend->_avatar;
                if($avatar_attend == '') {
                    $guests->avatar = JURI::base() . 'components/com_community/assets/user-Male.png';
                } else {
                    $guests->avatar = $root.'/'.$avatar_attend;
                }
                $guests->isMe = ($my->id == $guestsIds[$i]->id) ? true : false;
                $guests->isAdmin = $event->isAdmin($guestsIds[$i]->id);
                $guests->statusType = $guestsIds[$i]->statusCode;
                $attending[] = $guests;
            }
            $eventDetail->attending = $attending;
            $data->status = true;
            $data->message = 'Event Detail loaded';
            $data->event = $eventDetail;
        } else {
            $data->status = false;
            $data->message = 'Event Detail not loaded';
            $data->event = $eventDetail;
        }
        return $data;
    }
    private function addEvent($groupid, $title, $about, $startdate, $starttime, $enddate, $endtime, $location, $eventid, $avatar) {
        $my = JFactory::getUser();
        $config = CFactory::getConfig();
        $model = CFactory::getModel('events');
        $event = JTable::getInstance('Event', 'CTable');
        $isbanned = false;
        $isNew = ($eventid == 0) ? true : false;

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        // Check if the current user is banned from this group
        $isbanned = $group->isBanned($my->id);
        if($group->id == 0) {
            $response['status'] = false;
            $response['message'] = 'Circle not found.';
            $response['eventid'] = $eventid;
            return $response;
        }
        $user_owner = CFactory::getUser($group->ownerid);

        $group->ownername = $user_owner->getDisplayName();

        if($group->published == 0) {
            return array(
                'status' => false,
                'message' => 'Circle '.$group->name.' has been archived. Please contact '.$group->ownername.' for more information.'
            );
        }
        if ($isbanned) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_ACCESS_FORBIDDEN');
            $response['eventid'] = 0;
            return $response;
        }
        if (empty($title)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_EVENTS_TITLE_ERROR');
            $response['eventid'] = 0;
            return $response;
        }
        //check for user daily limit first, then check for the total limit
        if (CFactory::getConfig()->get("limit_events_perday") <= CFactory::getModel("events")->getTotalToday($my->id)) {
            $eventLimit = CFactory::getConfig()->get("limit_events_perday");
            $response['status'] = false;
            $response['message'] = JText::sprintf('COM_COMMUNITY_EVENTS_DAILY_LIMIT', $eventLimit);
            $response['eventid'] = 0;
            return $response;
        } else {
            if (CLimitsHelper::exceededEventCreation($my->id)) {
                $eventLimit = $config->get('eventcreatelimit');
                $response['status'] = false;
                $response['message'] = JText::sprintf('COM_COMMUNITY_EVENTS_LIMIT', $eventLimit);
                $response['eventid'] = 0;
                return $response;
            }
        }
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$startdate)) {
            $datestart_arr = explode('/', $startdate);
            $day = $datestart_arr[0];
            $month = $datestart_arr[1];
            $year = $datestart_arr[2];
            $startdate = $year .'-'.$month.'-'.$day;
        }
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$enddate)) {
            $dateend_arr = explode('/', $enddate);
            $dayend = $dateend_arr[0];
            $monthend = $dateend_arr[1];
            $yearend = $dateend_arr[2];
            $enddate = $yearend . '-'.$monthend.'-'.$dayend;
        }

        if (empty($startdate)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_EVENTS_STARTDATE_ERROR');
            $response['eventid'] = 0;
            return $response;
        }
        if (empty($enddate)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_EVENTS_ENDDATE_ERROR');
            $response['eventid'] = 0;
            return $response;
        }
        if((time()-(60*60*24)) > strtotime($startdate) && $isNew) {
            $response['status'] = false;
            $response['message'] = 'Start date invalid';
            $response['eventid'] = 0;
            return $response;
        }
        if((time()-(60*60*24)) > strtotime($enddate)) {
            $response['status'] = false;
            $response['message'] = 'End date invalid';
            $response['eventid'] = 0;
            return $response;
        }
        if(strtotime($startdate) > strtotime($enddate)) {
            $response['status'] = false;
            $response['message'] = 'End date must bigger start date';
            $response['eventid'] = 0;
            return $response;
        }
        if (empty($location)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_EVENTS_LOCATION_ERR0R');
            $response['eventid'] = 0;
            return $response;
        }
        $d1 = new DateTime("$startdate $starttime");
        $d2 = new DateTime("$enddate $endtime");
        if($d1 == $d2) {
            $response['status'] = false;
            $response['message'] = 'End time can not equal start time.';
            $response['eventid'] = 0;
            return $response;
        }
        $params = new CParameter('');
        $params->set('eventrecentphotos', 6);
        $params->set('eventrecentvideos', 6);
        $params->set('photopermission', -1);
        $params->set('videopermission', -1);



        $event->title = $title;
        $event->description = $about;
        $event->startdate = $startdate . ' ' . date("G:i", strtotime($starttime)) . ':00';
        $event->enddate = $enddate . ' ' . date("G:i", strtotime($endtime)) . ':00';
        $event->location = $location;
        $event->params = $params->toString();
        $event->creator = $my->id;
        $event->catid = 1;
        $event->published = 1;
        $event->created = JFactory::getDate()->toSql();
        $handler = CEventHelper::getHandler($event);
        $event->contentid = $handler->getContentId();
        $event->type = $handler->getType();
        $event->offset = 0;
        $status = 'create';
        if(!$isNew) {
            $event->id = $eventid;
            $status = 'update';
        }
        if($event->store()) {

            if($isNew && !$event->isRecurring()) {
                $members = $this->getMembersGroup($group->id);
                // add member to event
                $member = JTable::getInstance('EventMembers', 'CTable');

                $member->eventid = $event->id;
                $member->memberid = $my->id;
                $member->created = JFactory::getDate()->toSql();

                // Creator should always be 1 as approved as they are the creator.
                $member->status = COMMUNITY_EVENT_STATUS_ATTEND;

                // @todo: Setup required permissions in the future
                $member->permission = '1';
                $member->store();

                $receptions = array();
                if($members['members']) {
                    foreach ($members['members'] as $mem) {
                        if ($mem->id != $my->id) {
                            $receptions[] = $mem->id;
                        }
                    }
                }

                $event->updateGuestStats();
                $event->store();
            }
            if($isNew) {
                $action_str = 'events.create';
                CUserPoints::assignPoint($action_str);
                CEvents::addGroupNotification($event);
            }

            // add avatar
            if(!empty($avatar['name'])) {
                if(isset($avatar)) {
                    $uploaded = $this->uploadAvatar($event->id, $avatar, 'event', 'create');
                }
            }
            // add Stream
            CEvents::addEventStream($event);

            // push notification
            $creator = JFactory::getUser($event->creator);
            $message_send = array(
                'mtitle' => $creator->name.' has created an event '.$event->title.'! in '.$group->name,
                'mdesc'  => $about
            );
            $payload = $this->pushNotification($message_send, $receptions, $event->id, $group->id, 'event');

            $eventMembers = JTable::getInstance('EventMembers', 'CTable');
            $mainframe = JFactory::getApplication();

            $root = $mainframe->getCfg('wwwrootfile');

            $event->load($event->id);

            $users = CFactory::getUser($event->creator);
            $row = new stdClass();
            $row->id = $event->id;
            $row->groupid = $event->contentid;
            $row->title = $event->title;
            $row->description = $event->description;
            $row->location = $event->location;
            $row->unlisted = $event->unlisted;
            $row->admin_id = $event->creator;
            $row->admin_name = $users->getDisplayName();
            $avatar = $users->_avatar;
            if($avatar == '') {
                $row->admin_avatar = JURI::base() . 'components/com_community/assets/user-Male.png';
            } else {
                $row->admin_avatar = $root.'/'.$avatar;
            }
            $row->startdate = $event->startdate;
            $row->enddate = $event->enddate;
            $row->permission = $event->permission;
            $keys = array('eventId' => $event->id, 'memberId' => $my->id);
            $eventMembers->load($keys);
            $row->status = $eventMembers->status;
            if($event->avatar != NULL) {
                $row->avatar = $root.'/'.$event->avatar;
            } else {
                $row->avatar = "";
            }
            if($row->cover != NULL) {
                $row->cover = $root.'/'.$row->cover;
            } else {
                $row->cover = "";
            }
            $row->created = $event->created;
            $row->isAdminEvent = false;
            if($my->id == $event->creator) {
                $row->isAdminEvent = true;
            }

            $type = 1;
            $guestsIds = $event->getMembers($type, 0, false, $approval);

            $userids = array();
            foreach ($guestsIds as $uid) {
                $userids[] = $uid->id;
            }
            CFactory::loadUsers($userids);
            $attending = array();
            for ($i = 0; $i < count($guestsIds); $i++) {
                $user_attend = CFactory::getUser($guestsIds[$i]->id);
                $guests = new stdClass();
                $guests->userid = $user_attend->id;
                $guests->name = $user_attend->getDisplayName();
                $avatar_attend = $user_attend->_avatar;
                if($avatar_attend == '') {
                    $guests->avatar = JURI::base() . 'components/com_community/assets/user-Male.png';
                } else {
                    $guests->avatar = $root.'/'.$avatar_attend;
                }
                $guests->isMe = ($my->id == $guestsIds[$i]->id) ? true : false;
                $guests->isAdmin = $event->isAdmin($guestsIds[$i]->id);
                $guests->statusType = $guestsIds[$i]->statusCode;
                $attending[] = $guests;
            }
            $row->attending = $attending;
            $response['status'] = true;
            $response['message'] = 'Event '.$status.' success';
            $response['eventid'] = $event->id;
            $response['eventinfo'] = $row;
            $response['push_data'] = $payload['android'];
            $response['push_ios'] = $payload['ios'];
            return $response;
        } else {
            $response['status'] = false;
            $response['message'] = 'Event create failed';
            $response['eventid'] = 0;
            $response['push_data'] = new stdClass();
            $response['push_ios'] = new stdClass();
            return $response;
        }
    }
    // Begin delete event
    private function deleteEvent($eventid) {

        $model = CFactory::getModel('events');
        $event = JTable::getInstance('Event', 'CTable');
        if(empty($eventid)) {
            $response['status'] = false;
            $response['message'] = 'Sorry, we cannot find the event.';
            return $response;
        }
        if(!$event->load($eventid)) {
            $response['status'] = false;
            $response['message'] = 'Sorry, we cannot find the specified event.';
            return $response;
        }
        $action = '';
        // delete all member in event
        $event->deleteAllMembers($action);

        $eventData = $event;
        // delete event
        $deleted = $event->delete();
        if($deleted) {
            $featured = new CFeatured(FEATURED_EVENTS);

            // Delete avatar
            $this->deleteEventAvatar($eventData);
            // Delete featuer
            $featured->delete($eventid);

            $response['status'] = true;
            $response['message'] = 'Event Deleted';
        } else {
            $response['status'] = false;
            $response['message'] = 'Sorry, we cannot find the specified event.';
        }
        return $response;
    }
    public function deleteEventAvatar($eventData, $series = false)
    {
        jimport('joomla.filesystem.file');

        $model = CFactory::getModel('events');

        $avatarInused = $model->isImageInUsed($eventData->avatar, 'avatar', $eventData->id, $series);
        if ($eventData->avatar != "components/com_community/assets/eventAvatar.png" && !empty($eventData->avatar) && !$avatarInused) {
            $path = explode('/', $eventData->avatar);

            $file = JPATH_ROOT . '/' . $path[0] . '/' . $path[1] . '/' . $path[2] . '/' . $path[3];
            if (JFile::exists($file)) {
                JFile::delete($file);
            }
        }

        $thumbInused = $model->isImageInUsed($eventData->thumb, 'thumb', $eventData->id, $series);
        if ($eventData->thumb != "components/com_community/assets/event_thumb.png" && !empty($eventData->avatar) && !$thumbInused) {
            //$path = explode('/', $eventData->avatar);
            //$file = JPATH_ROOT .'/'. $path[0] .'/'. $path[1] .'/'. $path[2] .'/'. $path[3];
            $file = JPATH_ROOT . '/' . CString::str_ireplace('/', '/', $eventData->thumb);
            if (JFile::exists($file)) {
                JFile::delete($file);
            }
        }
    }

    private function uploadAvatar($id, $photoAvatar, $type, $create = null) {
        $params = new JRegistry();
        $mainframe = JFactory::getApplication();
        $document = JFactory::getDocument();
        // Check type upload
        $cTable = JTable::getInstance(ucfirst($type), 'CTable');
        $cTable->load($id);
        if($type == "event") {
            $model = CFactory::getModel('events');
            $handler = CEventHelper::getHandler($cTable);
        }
        $data_root = $mainframe->getCfg('dataroot');

        if(empty($id)) {
            $response['status'] = false;
            $response['message'] = 'Sorry. '.$type.' can not be empty.';
            return $response;
        }
        if(empty($photoAvatar) && empty($create)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_NO_POST_DATA');
            return $response;
        }

        if(empty($type)) {
            $response['status'] = false;
            $response['message'] = 'Type can not be empty.';
            return $response;
        }
        if($type == 'event') {
            if (!$handler->manageable()) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_ACCESS_FORBIDDEN');
                return $response;
            }
        }
        /*if (!CImageHelper::isValidType($_FILES['photo']['type'])) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_IMAGE_FILE_NOT_SUPPORTED');
            $response['file_type'] = $_FILES['photo']['type'];
            return $response;
        }*/
        if($type=="profile"){
            $my = CFactory::getUser($id);
        }else{
            $my = CFactory::getUser();
        }
        $config = CFactory::getConfig();

        $uploadLimit = (double)$config->get('maxuploadsize');
        $uploadLimit = ($uploadLimit * 1024 * 1024);
        if (filesize($photoAvatar['tmp_name']) > $uploadLimit && $uploadLimit != 0) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_VIDEOS_IMAGE_FILE_SIZE_EXCEEDED_MB');
            return $response;
        }

        $ext = pathinfo($photoAvatar['name'], PATHINFO_EXTENSION);
        if ($photoAvatar['type'] == "application/octet-stream" && ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG')) {
            if ($ext == 'jpeg') {
                $image_type = 'image/jpeg';
            }
            if ($ext == 'jpg') {
                $image_type = 'image/jpg';
            }
            if ($ext == 'JPG') {
                $image_type = 'image/JPG';
            }
        } elseif ($photoAvatar['type'] == "application/octet-stream" && ($ext == 'png' || $ext == 'PNG')) {
            $image_type = 'image/png';
        } else {
            $image_type = $photoAvatar['type'];
        }

        CImageHelper::autoRotate($photoAvatar['tmp_name']);
        // @todo: configurable width?
        $imageMaxWidth = 1024;

        // Get a hash for the file name.
        $fileName = JApplication::getHash($photoAvatar['tmp_name'] . time());
        $hashFileName = JString::substr($fileName, 0, 24);
        $avatarFolder = ($type != 'profile' && $type != '') ? $type . '/' : '';

        // @todo: configurable path for avatar storage?
        $storage = $data_root . '/' . $config->getString('imagefolder') . '/avatar/'.$avatarFolder;
        $storageImage = $storage . '/'. $hashFileName . CImageHelper::getExtension($image_type);

        $image = $config->getString(
                'imagefolder'
            ) . '/avatar/'.$avatarFolder . $hashFileName . CImageHelper::getExtension($image_type);

        $storageThumbnail = $storage . '/thumb_' . $hashFileName . CImageHelper::getExtension(
                $photoAvatar['type']
            );
        $thumbnail = $config->getString(
                'imagefolder'
            ) . '/avatar/'. $avatarFolder . 'thumb_' . $hashFileName . CImageHelper::getExtension(
                $photoAvatar['type']
            );
        $storageReserve = $storage . '/' . $type . '-' . $hashFileName . CImageHelper::getExtension($image_type);
        $imageAttachment = $config->getString(
                'imagefolder'
            ) . '/avatar/' . $hashFileName . '_stream_' . CImageHelper::getExtension($image_type);

        //Minimum height/width checking for Avatar uploads
        list($currentWidth, $currentHeight) = getimagesize($photoAvatar['tmp_name']);
//            if ($currentWidth < COMMUNITY_AVATAR_PROFILE_WIDTH || $currentHeight < COMMUNITY_AVATAR_PROFILE_HEIGHT) {
//                $response['status'] = false;
//                $response['message'] = JText::sprintf(
//                    'COM_COMMUNITY_ERROR_MINIMUM_AVATAR_DIMENSION',
//                    COMMUNITY_AVATAR_PROFILE_WIDTH,
//                    COMMUNITY_AVATAR_PROFILE_HEIGHT
//                );
//                return $response;
//            }

        /**
         * Generate square avatar
         */
        if (!CImageHelper::createThumb(
            $photoAvatar['tmp_name'], $storageImage, $image_type, COMMUNITY_AVATAR_PROFILE_WIDTH, COMMUNITY_AVATAR_PROFILE_HEIGHT
        )
        ) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE');
            return $response;
        }

        // Generate thumbnail
        if (!CImageHelper::createThumb($photoAvatar['tmp_name'], $storageThumbnail, $image_type, COMMUNITY_SMALL_AVATAR_WIDTH, COMMUNITY_SMALL_AVATAR_WIDTH)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE');
            return $response;
        }
        /**
         * Generate large image use for avatar thumb cropping
         * It must be larget than profile avatar size because we'll use it for profile avatar recrop also
         */
        $newWidth = 0;
        $newHeight = 0;
        if ($currentWidth >= $currentHeight) {
            if ($this->testResize(
                $currentWidth, $currentHeight, COMMUNITY_AVATAR_RESERVE_WIDTH, 0, COMMUNITY_AVATAR_PROFILE_WIDTH, COMMUNITY_AVATAR_RESERVE_WIDTH
            )
            ) {
                $newWidth = COMMUNITY_AVATAR_RESERVE_WIDTH;
                $newHeight = 0;
            } else {
                $newWidth = 0;
                $newHeight = COMMUNITY_AVATAR_RESERVE_HEIGHT;
            }
        } else {
            if ($this->testResize(
                $currentWidth, $currentHeight, 0, COMMUNITY_AVATAR_RESERVE_HEIGHT, COMMUNITY_AVATAR_PROFILE_HEIGHT, COMMUNITY_AVATAR_RESERVE_HEIGHT
            )
            ) {
                $newWidth = 0;
                $newHeight = COMMUNITY_AVATAR_RESERVE_HEIGHT;
            } else {
                $newWidth = COMMUNITY_AVATAR_RESERVE_WIDTH;
                $newHeight = 0;
            }
        }

        if (!CImageHelper::resizeProportional(
            $photoAvatar['tmp_name'], $storageReserve, $image_type, $newWidth, $newHeight
        )
        ) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE');
            return $response;
        }

        $album = JTable::getInstance('Album', 'CTable');

        //create the avatar default album if it does not exists
        if (!$albumId = $album->isAvatarAlbumExists($id, $type)) {
            $albumId = $album->addAvatarAlbum($id, $type);
        }
        // Autorotate avatar based on EXIF orientation value
        if ($photoAvatar['type'] == 'image/jpeg') {
            $orientation = CImageHelper::getOrientation($photoAvatar['tmp_name']);
            CImageHelper::autoRotate($storageImage, $orientation);
            CImageHelper::autoRotate($storageThumbnail, $orientation);
        }
        $originalName = 'original_' . md5($my->id . '_avatar' . time()) . CImageHelper::getExtension(
                $photoAvatar['type']
            );
        $originalPath = $storage .$originalName;
        if (!JFile::copy($photoAvatar['tmp_name'], $originalPath)) {
            exit;
        }
        // Update the event with the new image
        $cTable->setImage($image, 'avatar');
        $cTable->setImage($thumbnail, 'thumb');

        $now = new JDate();
        $photo = JTable::getInstance('Photo', 'CTable');

        $photo->albumid = $albumId;
//            $photo->image = str_replace(JPATH_ROOT . '/', '', $fullImagePath);
        $photo->image = str_replace($mainframe->getCfg('dataroot') . '/', '', $storageImage);
        $photo->caption = $photoAvatar['photo']['name'];
        $photo->filesize = $photoAvatar['size'];
        $photo->creator = $my->id;
        $photo->created = $now->toSql();
        $photo->published = 1;
        $photo->thumbnail = str_replace($mainframe->getCfg('dataroot') . '/', '', $storageThumbnail);
        $photo->original = str_replace($mainframe->getCfg('dataroot') . '/', '', $storageImage);

        if ($photo->store()) {
            $album->load($albumId);
            $album->photoid = $photo->id;
            $album->setParam('thumbnail', $photo->thumbnail);
            $album->store();
        }
        if ($type == 'profile') {
            $profileType = $my->getProfileType();
            $multiprofile = JTable::getInstance('MultiProfile', 'CTable');
            $multiprofile->load($profileType);

            $useWatermark = $profileType != COMMUNITY_DEFAULT_PROFILE && $config->get(
                'profile_multiprofile'
            ) && !empty($multiprofile->watermark) ? true : false;
            if ($useWatermark && $multiprofile->watermark) {
                JFile::copy(
                    $storageImage,
                    $mainframe->getCfg('dataroot') . '/images/watermarks/original' . '/' . md5(
                        $my->id . '_avatar'
                    ) . CImageHelper::getExtension($image_type)
                );
                JFile::copy(
                    $storageThumbnail,
                    $mainframe->getCfg('dataroot') . '/images/watermarks/original' . '/' . md5(
                        $my->id . '_thumb'
                    ) . CImageHelper::getExtension($image_type)
                );
                $watermarkPath = $mainframe->getCfg('dataroot') . '/' . CString::str_ireplace('/', '/', $multiprofile->watermark);
                list($watermarkWidth, $watermarkHeight) = getimagesize($watermarkPath);
                list($avatarWidth, $avatarHeight) = getimagesize($storageImage);
                list($thumbWidth, $thumbHeight) = getimagesize($storageThumbnail);

                $watermarkImage = $storageImage;
                $watermarkThumbnail = $storageThumbnail;

                // Avatar Properties
                $avatarPosition = CImageHelper::getPositions(
                    $multiprofile->watermark_location,
                    $avatarWidth,
                    $avatarHeight,
                    $watermarkWidth,
                    $watermarkHeight
                );

                // The original image file will be removed from the system once it generates a new watermark image.
                CImageHelper::addWatermark(
                    $storageImage,
                    $watermarkImage,
                    $image_type,
                    $watermarkPath,
                    $avatarPosition->x,
                    $avatarPosition->y
                );

                //Thumbnail Properties
                $thumbPosition = CImageHelper::getPositions(
                    $multiprofile->watermark_location,
                    $thumbWidth,
                    $thumbHeight,
                    $watermarkWidth,
                    $watermarkHeight
                );

                // The original thumbnail file will be removed from the system once it generates a new watermark image.
                CImageHelper::addWatermark(
                    $storageThumbnail,
                    $watermarkThumbnail,
                    $image_type,
                    $watermarkPath,
                    $thumbPosition->x,
                    $thumbPosition->y
                );

                $my->set('_watermark_hash', $multiprofile->watermark_hash);
            }

            // We need to make a copy of current avatar and set it as stream 'attachement'
            // which will only gets deleted once teh stream is deleted

            $my->_cparams->set('avatar_photo_id', $photo->id); //we also set the id of the avatar photo

            $my->save();

            JFile::copy($image, $imageAttachment);
            $params->set('attachment', $imageAttachment);
        }
        if (empty($saveAction)) {
            $cTable->setImage($image, 'avatar');
            $cTable->setImage($thumbnail, 'thumb');
            if($type == 'group') {
                $cTable->setImage($originalName, 'origimage');
            }
        } else {
            // This is for event recurring save option ( current / future event )
            $cTable->setImage($image, 'avatar', $saveAction);
            $cTable->setImage($thumbnail, 'thumb', $saveAction);
            if($type == 'group') {
                $cTable->setImage($originalName, 'origimage');
            }
        }
        if($type == 'event') {

            if ($handler->isPublic()) {
                $actor = $my->id;
                $target = 0;
                $content = '<img class="event-thumb" src="' . JURI::root(true) . '/' . $image . '" style="border: 1px solid #eee;margin-right: 3px;" />';
                $cid = $cTable->id;
                $app = 'events';
                $act = $handler->getActivity('events.avatar.upload', $actor, $target, $content, $cid, $app);
                $act->eventid = $cTable->id;

                $params->set(
                    'event_url', $handler->getFormattedLink(
                    'index.php?option=com_community&view=events&task=viewevent&eventid=' . $cTable->id, false, true, false
                )
                );


                CActivityStream::add($act, $params->toString());
            }

            //add user points
            CUserPoints::assignPoint('event.avatar.upload');
        }
        $generateStream = true;
        switch ($type) {
            case 'profile':

                /**
                 * Generate activity stream
                 * @todo Should we use CApiActivities::add
                 */
                // do not have to generate a stream if the user is not the user itself (eg admin change user avatar)
                if(CUserPoints::assignPoint('profile.avatar.upload') && $my->id == CFactory::getUser()->id){
                    $act = new stdClass();
                    $act->cmd = 'profile.avatar.upload';
                    $act->actor = $my->id;
                    $act->target = 0;
                    $act->title = '';
                    $act->content = '';
                    $act->access = $my->_cparams->get("privacyPhotoView", 0);
                    $act->app = 'profile.avatar.upload'; /* Profile app */
                    $act->cid = (isset($photo->id) && $photo->id) ? $photo->id : 0 ;
                    $act->verb = 'upload'; /* We uploaded new avatar - NOT change avatar */
                    $act->params = $params;
                    $params->set('photo_id', $photo->id);
                    $params->set('album_id', $photo->albumid);
                    $act->comment_id = CActivities::COMMENT_SELF;
                    $act->comment_type = 'profile.avatar.upload';
                    $act->like_id = CActivities::LIKE_SELF;
                    $act->like_type = 'profile.avatar.upload';
                }
                else{
                    $generateStream = false;
                }
                break;
            case 'event':
                //CUserPoints::assignPoint('events.avatar.upload'); @disabled since 4.0
                /**
                 * Generate activity stream
                 * @todo Should we use CApiActivities::add
                 */
                $act = new stdClass();
                $act->cmd = 'events.avatar.upload';
                $act->actor = $my->id;
                $act->target = 0;
                $act->title = '';
                $act->content = '';
                $act->app = 'events.avatar.upload'; /* Events app */
                $act->cid = $id;
                $act->eventid = $id;
                $act->verb = 'update'; /* We do update */

                $act->comment_id = CActivities::COMMENT_SELF;
                $act->comment_type = 'events.avatar.upload';
                $act->like_id = CActivities::LIKE_SELF;
                $act->like_type = 'events.avatar.upload';
                break;
            case 'group':
                /**
                 * Generate activity stream
                 * @todo Should we use CApiActivities::add
                 */

                if(CUserPoints::assignPoint('group.avatar.upload')){
                    $act = new stdClass();
                    $act->cmd = 'groups.avatar.upload';
                    $act->actor = $my->id;
                    $act->target = 0;
                    $act->title = '';
                    $act->content = '';
                    $act->app = 'groups.avatar.upload'; /* Groups app */
                    $act->cid = $id;
                    $act->groupid = $id;
                    $act->verb = 'update'; /* We do update */
                    $params->set('photo_id', $photo->id);
                    $params->set('album_id', $photo->albumid);

                    $act->comment_id = CActivities::COMMENT_SELF;
                    $act->comment_type = 'groups.avatar.upload';
                    $act->like_id = CActivities::LIKE_SELF;
                    $act->like_type = 'groups.avatar.upload';
                    $generateStream = true;
                }else{
                    $generateStream = false;
                }

                break;
            case 'discussion':
                //CUserPoints::assignPoint('events.avatar.upload'); @disabled since 4.0
                /**
                 * Generate activity stream
                 * @todo Should we use CApiActivities::add
                 */
                $act = new stdClass();
                $act->cmd = 'discussion.avatar.upload';
                $act->actor = $my->id;
                $act->target = 0;
                $act->title = '';
                $act->content = '';
                $act->app = 'discussion.avatar.upload'; /* Events app */
                $act->cid = $id;
                $act->eventid = $id;
                $act->verb = 'update'; /* We do update */

                $act->comment_id = CActivities::COMMENT_SELF;
                $act->comment_type = 'discussion.avatar.upload';
                $act->like_id = CActivities::LIKE_SELF;
                $act->like_type = 'discussion.avatar.upload';


                break;
        }
        //we only generate stream if the uploader is the user himself, not admin or anyone else
        if (((isset($act) && $my->id == $id) || $type != 'profile') && $generateStream) {
            // $return = CApiActivities::add($act);

            /**
             * use internal Stream instead use for 3rd part API
             */
            $return = CActivityStream::add($act, $params->toString());

            //add the reference to the activity so that we can do something when someone update the avatar
            if ($type == 'profile') {
                // overwrite the params because some of the param might be updated through $my object above
                $cTableParams = $my->_cparams;
            } else {
                $cTableParams = new JRegistry($cTable->params);
            }

            $cTableParams->set('avatar_activity_id', $return->id);

            $cTable->params = $cTableParams->toString();
            $cTable->store();
        }
        if( $type != 'profile') {
            CActivityStream::add($act, $params->toString());
        }
        $response['status'] = true;
        $response['message'] = 'Upload success avatar';
        $response['path'] = $mainframe->getCfg('wwwrootfile') . '/' . str_replace($data_root . '/', '', $image);
        return $response;
    }

    // Begin upload cover photo
    private function uploadCover($id, $cover, $type) {
        $mainframe = JFactory::getApplication();
        $data_root = $mainframe->getCfg('dataroot');
        $config = CFactory::getConfig();
        $my = CFactory::getUser();
        $now = new JDate();

        if (!CImageHelper::checkImageSize(filesize($cover['tmp_name']))) {

            $response['status'] = false;
            $response['message'] = JText::sprintf('COM_COMMUNITY_VIDEOS_IMAGE_FILE_SIZE_EXCEEDED_MB',CFactory::getConfig()->get('maxuploadsize'));
            return $response;
        }
        $ext = pathinfo($cover['name'], PATHINFO_EXTENSION);
        if ($cover['type'] == "application/octet-stream" && ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG')) {
            if ($ext == 'jpeg') {
                $image_type = 'image/jpeg';
            }
            if ($ext == 'jpg') {
                $image_type = 'image/jpg';
            }
            if ($ext == 'JPG') {
                $image_type = 'image/JPG';
            }
        } elseif ($cover['type'] == "application/octet-stream" && ($ext == 'png' || $ext == 'PNG')) {
            $image_type = 'image/png';
        } else {
            $image_type = $cover['type'];
        }
        //check if file is allwoed
        /*if (!CImageHelper::isValidType($_FILES['cover']['type'])) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_IMAGE_FILE_NOT_SUPPORTED');
            return $response;
        }*/
        CImageHelper::autoRotate($cover['tmp_name']);

        $album = JTable::getInstance('Album', 'CTable');

        if (!$albumId = $album->isCoverExist($type, $id)) {
            $albumId = $album->addCoverAlbum($type, $id);
        }

        $imgMaxWidht = 1140;

        // Get a hash for the file name.
//            $fileName = JApplication::getHash($cover['tmp_name'] . time());
//            $hashFileName = JString::substr($fileName, 0, 24);

        if (!JFolder::exists(
            $data_root . '/' . $config->getString('imagefolder') . '/cover/' . $type . '/' . $id . '/'
        )
        ) {
            JFolder::create(
                $data_root . '/' . $config->getString('imagefolder') . '/cover/' . $type . '/' . $id . '/'
            );
        }
        $dest = $data_root . '/' . $config->getString('imagefolder') . '/cover/' . $type . '/' . $id . '/' . md5(
                $type . '_cover' . time()
            ) . CImageHelper::getExtension($image_type);

        $thumbPath = $data_root . '/' . $config->getString(
                'imagefolder'
            ) . '/cover/' . $type . '/' . $id . '/thumb_' . md5(
                $type . '_cover' . time()
            ) . CImageHelper::getExtension($image_type);

        if (!CImageHelper::resizeProportional($cover['tmp_name'], $dest, $image_type, $imgMaxWidht)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE');
            return $response;
        }

//            CPhotos::generateThumbnail($cover['tmp_name'], $thumbPath, $cover['type']);

        $cTable = JTable::getInstance(ucfirst($type), 'CTable');
        $cTable->load($id);

        if ($cTable->setCover(str_replace($data_root . '/', '', $dest))) {
            $photo = JTable::getInstance('Photo', 'CTable');

            $photo->albumid = $albumId;
//                $photo->image = str_replace(JPATH_ROOT . '/', '', $dest);
            $photo->image = str_replace($data_root . '/', '', $dest);
            $photo->caption = $cover['name'];
            $photo->filesize = $cover['size'];
            $photo->creator = $my->id;
            $photo->created = $now->toSql();
            $photo->published = 1;
//                $photo->thumbnail = str_replace(JPATH_ROOT . '/', '', $thumbPath);
            $photo->thumbnail = str_replace($data_root . '/', '', $thumbPath);

            if ($photo->store()) {
                $album->load($albumId);
                $album->photoid = $photo->id;
                $album->store();
            }

            $response['status'] = true;
//                $msg['path'] = JURI::root() . str_replace(JPATH_ROOT . '/', '', $dest);
            $response['message'] = 'Upload success cover';
            $response['path'] = $mainframe->getCfg('wwwrootfile') . '/' . str_replace($data_root . '/', '', $dest);

            // Generate activity stream.
            $act = new stdClass();
            $act->cmd = 'cover.upload';
            $act->actor = $my->id;
            $act->target = 0;
            $act->title = '';
            $act->content = '';
            $act->access = ($type == 'profile') ? $my->_cparams->get("privacyPhotoView") : 0;
            $act->app = 'cover.upload';
            $act->cid = $photo->id;
            $act->comment_id = CActivities::COMMENT_SELF;
            $act->comment_type = 'cover.upload';
            $act->groupid = ($type == 'group') ? $id : 0;
            $act->eventid = ($type == 'event') ? $id : 0;
            $act->group_access = ($type == 'group') ? $cTable->approvals : 0;
            $act->event_access = ($type == 'event') ? $cTable->permission : 0;
            $act->like_id = CActivities::LIKE_SELF;
            $act->like_type = 'cover.upload';

            $params = new JRegistry();
//                $params->set('attachment', str_replace(JPATH_ROOT . '/', '', $dest));
            $params->set('attachment', str_replace($data_root . '/', '', $dest));
            $params->set('type', $type);
            $params->set('album_id', $albumId);
            $params->set('photo_id', $photo->id);

            //assign points based on types.
            switch($type){
                case 'group':
                    $addStream = CUserPoints::assignPoint('group.cover.upload');
                    break;
                case 'event':
                    $addStream =  CUserPoints::assignPoint('event.cover.upload');
                    break;
                default:
                    $addStream = CUserPoints::assignPoint('profile.cover.upload');
            }

            if ($type == 'event') {
                $event = JTable::getInstance('Event', 'CTable');
                $event->load($id);

                $group = JTable::getInstance('Group', 'CTable');
                $group->load($event->contentid);

                if ($group->approvals == 1) {
                    $addStream = false;
                }
            }

            if ($addStream) {
                // Add activity logging
                if( $type != 'profile' || ($type=='profile' && $id == $my->id) ) {
                    CActivityStream::add($act, $params->toString());
                }
            }
            return $response;
        }
    }
    public function testResize($orgW, $orgH, $newW, $newH, $minVal, $maxVal)
    {
        $newValue = 0;
        if ($newH == 0) {
            /* New height value */
            $newValue = round(($newW * $orgH) / $orgW);
        } elseif ($newW == 0) {
            /* New width value */
            $newValue = round(($newH * $orgW) / $orgH);
        } else {
            return false;
        }
        return ($newValue >= $minVal && $newValue <= $maxVal) ? true : false;
    }
    private function goingEvent($id, $username, $status) {
        if(empty($id)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Event ID can not blank';
            return $reponse;
        }
        if(empty($username)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Username can not blank';
            return $reponse;
        }
        $status_array = array('1', '2', '3');
        if(!in_array($status, $status_array)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Status Invalid';
            return $reponse;
        }
        $event = JTable::getInstance('Event', 'CTable');
        $event->load($id);

        $my = CFactory::getUser();
        $invitedCount = 0;

        $user_id = JUserHelper::getUserId($username);

        $date = JFactory::getDate();
        $eventMember = JTable::getInstance('EventMembers', 'CTable');
        $eventMember->eventid = $event->id;
        $eventMember->memberid = $user_id;
        $eventMember->status = $status;
        $eventMember->invited_by = $my->id;
        $eventMember->permission = 3;
        $eventMember->created = $date->toSql();

        if($status == 1) {
            $message = 'Going event success';
        } else if($status == 2) {
            $message = 'Can\'t going event success';
        } else {
            $message = 'Maybe going event success';
        }

        if($eventMember->store()) {
            $invitedCount++;

            //now update the invited count in event
            $event->invitedcount = $event->invitedcount + $invitedCount;
            $event->store();
            $reponse['status'] = true;
            $reponse['message'] = $message;
        } else {
            $reponse['status'] = false;
            $reponse['message'] = 'Going event failed';
        }
        return $reponse;
    }
    private function myEvent() {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $model = CFactory::getModel('events');
        $my = CFactory::getUser();
        $sorted = 'startdate';
        $events = $model->getEvents(null, $my->id, $sorted);

        $eventMembers = JTable::getInstance('EventMembers', 'CTable');

        $response = new stdClass();
        if($events) {
            foreach ($events as $event) {
                $keys = array('eventId' => $event->id, 'memberId' => $my->id);
                $eventMembers->load($keys);
                $data = array();
                $data['id'] = $event->id;
                $data['title'] = $event->title;
                $data['group'] = $event->contentid;
                $data['description'] = $event->description;
                $data['location'] = $event->location;
                $data['startdate'] = $event->startdate;
                $data['enddate'] = $event->enddate;
                $data['created'] = $event->created;
                $data['status'] = $eventMembers->status;
                $user_attend = CFactory::getUser($event->creator);
                $data['owner'] = $user_attend->getDisplayName();
                $datas[] = $data;
            }
            $response->status = true;
            $response->message = 'Event loaded';
            $response->events = $datas;
        } else {
            $response->status = false;
            $response->message = 'Event not loaded';
            $response->events = $datas;
        }
        return $response;
    }
    private function addAnnouncement($groupid, $message, $enddate, $endtime, $id, $timezone) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $my = CFactory::getUser();

        // check input
        if(empty($groupid)) {
            $response['status'] = false;
            $response['message'] = 'Circle does not empty.';
            return $response;
        }
        $title = $my->name;
        if(empty($message)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_GROUPS_BULLETIN_BODY_EMPTY');
            return $response;
        }
        if(empty($enddate)) {
            $response['status'] = false;
            $response['message'] = 'End date can not blank.';
            return $response;
        }
        if(!preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/", $enddate)) {
            return array(
                'status' => false,
                'message' => 'date format is wrong, please post with param is dd/mm/yyyy format'
            );
        }

        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$enddate)) {
            $datestart_arr = explode('/', $enddate);
            $day = $datestart_arr[0];
            $month = $datestart_arr[1];
            $year = $datestart_arr[2];
            $enddate =  $day.'-'.$month.'-'.$year;
        }
        if(empty($endtime)) {
            $response['status'] = false;
            $response['message'] = 'End time can not blank.';
            return $response;
        }
        //=== Parse timezone ==//
        if(!isset($timezone)) {
            $timezone = date_default_timezone_get();
        }
        // get timezone server
        /*
        $serverTimeZone = date_default_timezone_get();
        $serverObjTime = new DateTimeZone($serverTimeZone);
        $sverTime = new DateTime('now', $serverObjTime);
        $serveroffset = $serverObjTime->getOffset($sverTime);
        //get user timezone
        $usertimezone = new DateTimeZone($timezone);
        $gmtTimezone = new DateTimeZone('GMT');
        $myDateTime = new DateTime($enddate.' '.$endtime, $gmtTimezone);
        $offset = $usertimezone->getOffset($myDateTime);
        // unequal time betwen server timezone and user timezone
        $timeUnequal = ($serveroffset - $offset);
        $finalTime = strtotime($enddate.' '.$endtime) + ($timeUnequal);
        */
        $finalTime = strtotime($enddate.' '.$endtime);

        //== End ==//

//        $timeZone = $my->getParam('timezone', 'UTC');
//        $myDate = JDate::getInstance($enddate.' '.$endtime, $timeZone);
//        $dateParse = $myDate->toUnix();
        $created = new DateTime("now", new DateTimeZone($timezone) );
        $date_created = $created->format('Y-m-d H:i:s');


        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        if(!$group->id) {
            return array(
                'status' => false,
                'message' => 'Circle not found or has been deleted.'
            );
        }
        $user_owner = CFactory::getUser($group->ownerid);

        $group->ownername = $user_owner->getDisplayName();

        if($group->published == 0) {
            return array(
                'status' => false,
                'message' => 'Circle '.$group->name.' has been archived. Please contact '.$group->ownername.' for more information.'
            );
        }
        $params = new CParameter('');

        $bulletin = JTable::getInstance('Bulletin', 'CTable');
//        $endtime = (string)date('g:i A', $backuptime);
        if($id) {
            $bulletin->load($id);
            $bulletin->params = $params->toString();
            if (!empty($bulletin->endtime)) {
                $enddate = date('d-m-Y', $bulletin->endtime);
                $bulletin->endtime = $finalTime;
            }
        }
        $bulletin->title = $title;
        $bulletin->message = $message;
        $bulletin->groupid = $groupid;
        $bulletin->date = $date_created;
        $bulletin->created_by = $my->id;
        $bulletin->endtime = $finalTime;
        $bulletin->timezone = $timezone;
        $bulletin->store();

        // Send notification to all user
        $model = CFactory::getModel('groups');
        $memberCount = $model->getMembersCount($groupid);
        $members = $model->getMembers($groupid, $memberCount, true, false, true);

        $membersArray = array();

        foreach ($members as $row) {
//            if ($row->id != $my->id) {
            $membersArray[] = $row->id;
//            }
        }
        unset($members);
        $params->set('url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupid . '&bulletinid=' . $bulletin->id);
        $params->set('group', $group->name);
        $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupid);
        $params->set('subject', $bulletin->title);
        $params->set('announcement', $bulletin->message);
        $params->set('message', $bulletin->message);
        $params->set('announcement_url', 'index.php?option=com_community&view=groups&task=viewbulletin&groupid=' . $group->id . '&bulletinid=' . $bulletin->id);

        CNotificationLibrary::add('groups_create_news', $my->id, $membersArray, JText::sprintf('COM_COMMUNITY_GROUPS_EMAIL_NEW_BULLETIN_SUBJECT'), '', 'groups.bulletin', $params);

        // Put notification via google gcm
        // For Android
        $message_send = array(
            'mtitle' => 'New announcement in '.$group->name,
            'mdesc'  => $bulletin->title
        );
        $payload = $this->pushNotification($message_send, $membersArray, $bulletin->id, $group->id, 'announcement');
        // Add activity logging

        $act = new stdClass();
        $act->cmd = 'group.news.create';
        $act->actor = $my->id;
        $act->target = 0;
        $act->title = $title; //JText::sprintf('COM_COMMUNITY_GROUPS_NEW_GROUP_NEWS' , '{group_url}' , $bulletin->title );
        $act->content = ( $group->approvals == 0 ) ? JString::substr(strip_tags($bulletin->message), 0, 100) : '';
        $act->app = 'groups.bulletin';
        $act->cid = $bulletin->id;
        $act->groupid = $group->id;
        $act->group_access = $group->approvals;

        $act->comment_id = CActivities::COMMENT_SELF;
        $act->comment_type = 'groups.bulletin';
        $act->like_id = CActivities::LIKE_SELF;
        $act->like_type = 'groups.bulletin';

        $params = new CParameter('');
//              $params->set( 'group_url' , 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id );
        $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewbulletin&groupid=' . $group->id . '&bulletinid=' . $bulletin->id);


        CActivityStream::add($act, $params->toString());

        $Brow = new stdClass();
        $Brow->id = $bulletin->id;
        $Brow->groupid = $bulletin->groupid;
        $Brow->created_by = $bulletin->created_by;
        $avatar = $my->_avatar;
        if($avatar == '') {
            $Brow->created_avatar = JURI::base() . 'components/com_community/assets/user-Male.png';
        } else {
            $Brow->created_avatar = $root.'/'.$avatar;
        }
        $Brow->published = $bulletin->published;
        $Brow->created_name = $bulletin->title;
        $Brow->message = $bulletin->message;
        $Brow->date = $bulletin->date;
        $Brow->params = $bulletin->params;
        $Brow->endtime = $bulletin->endtime;
        $Brow->timezone = $bulletin->timezone;
        $Brow->dateend = date('Y-m-d', $bulletin->endtime);
        $Brow->timeend = date('H:i A', $bulletin->endtime);

        //add user points
        CUserPoints::assignPoint('group.news.create');
        $response['status'] = true;
        $response['message'] = 'Announcement added.';
        $response['announcement'] = $Brow;
        $response['push_data'] = $payload['android'];
        $response['push_ios'] = $payload['ios'];
        return $response;

    }

    private function deleteAnnouncement($groupid, $announcementid) {
        $my = CFactory::getUser();
        if ($my->id == 0) {
            return $this->blockUnregister();
        }
        if (empty($announcementid) || empty($groupid)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_INVALID_ID');
            return $response;
        }
        $groupsModel = CFactory::getModel('groups');
        $bulletin = JTable::getInstance('Bulletin', 'CTable');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        if(!$group->id) {
            return array(
                'status' => false,
                'message' => 'Circle not found or has been deleted.'
            );
        }
        $creator = CFactory::getUser($group->ownerid);

        $group->ownername = $creator->getDisplayName();

        if($group->published == 0) {
            return array(
                'status' => false,
                'message' => 'Circle '.$group->name.' has been archived. Please contact '.$group->ownername.' for more information.'
            );
        }
        $fileModel = CFactory::getModel('files');

        if ($groupsModel->isAdmin($my->id, $group->id) || COwnerHelper::isCommunityAdmin()) {
            $bulletin->load($announcementid);
            if ($bulletin->delete()) {

                //add user points
                //CFactory::load( 'libraries' , 'userpoints' );
                CUserPoints::assignPoint('group.news.remove');
                CActivityStream::remove('groups.bulletin', $announcementid);

                // Remove Bulletin Files
                $fileModel->alldelete($announcementid, 'bulletin');

                $response['status'] = true;
                $response['message'] = JText::_('COM_COMMUNITY_BULLETIN_REMOVED');
                return $response;
            }
        } else {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_DELETE_WARNING');
            return $response;
        }
    }


    private function myFriends($courseid = 0, $categoryid = 0) {

        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $my = CFactory::getUser();

        $friends = CFactory::getModel('friends');

        $sorted = 'latest';
        $filter = 'all';
        $rows = $friends->getFriends($my->id, $sorted, true, $filter);
        $blockModel = CFactory::getModel('block');

        $resultRows = array();
        if($rows) {
            foreach ($rows as $row) {
                $user = CFactory::getUser($row->id);

                $profilemodel = CFactory::getModel('profile');
                $profile = $profilemodel->getViewableProfile($user->id);

                $obj = new stdClass();
                $obj->id = $row->id;
                $obj->username = $row->username;
                $obj->displayname = $user->getDisplayName();
                $avatar = $user->_avatar;
                $cover = $user->_cover;
                if($avatar == '') {
                    $obj->avatar = '';//JURI::base() . 'components/com_community/assets/user-Male-thumb.png'
                } else {
                    $obj->avatar = $root.'/'.$avatar;
                }
                if($cover == '') {
                    $obj->cover = '';//JURI::base() . 'components/com_community/assets/cover-undefined-default.png'
                } else {
                    $obj->cover = $root.'/'.$cover;
                }
                $obj->friendsCount = $user->getFriendCount();
                $obj->profileLink = CUrlHelper::userLink($row->id);
                $obj->isFriend = true;
                $obj->isBlocked = $blockModel->getBlockStatus($my->id, $user->id);
                $obj->roles = array();
                // check user role
                if(!empty($courseid)) {
                    require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');
                    $roles = JoomdleHelperContent::call_method('get_user_role', (int)$courseid, $row->username);
                    $obj->roles = json_decode($roles['roles'][0]['role']);
                }
                if(!empty($categoryid)) {
                    require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');
                    $roles = JoomdleHelperContent::call_method('get_user_category_role', (int)$categoryid, $row->username);
                    $obj->roles = (array)$roles['role'];

                }
                foreach ($profile['fields']['Basic Information'] as $basic) {
                    if($basic['name'] == 'Position') {
                        if(!empty($basic['value'])) {
                            $obj->position = $basic['value'];
                        } else {
                            $obj->position = '';
                        }
                    }
                }
                $resultRows[] = $obj;
            }
            unset($rows);

            $response['status'] = true;
            $response['message'] = 'Friend loaded';
            $response['myFriend'] = $resultRows;
        } else {
            $response['status'] = true;
            $response['message'] = 'You have no friends yet.';
            $response['myFriend'] = $resultRows;
        }
        return $response;
    }

    private function allFriend($limit, $page) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $my = CFactory::getUser();
        $searchModel = CFactory::getModel('search');
        $userModel = CFactory::getModel('user');

        $position = ($page - 1) * $limit;

        $sorted = 'latest';
        $filter = 'all';

        // part 1: already Friend
        $alreadyFriend = $searchModel->alreadyFriend($my->id, $page, $limit);
        $count_already = count($alreadyFriend['friends']);
        $count_page = $alreadyFriend['totalpage'];
        $item_limit = $limit - $count_already;
        if($count_already < $limit) {
            $page_new = $page - $count_page;
            $new_position = (($page_new - 1) * $limit) + $item_limit;
            if($new_position < 0) $new_position = 0;
            $rows = $searchModel->getPeopleApp($sorted, $filter, 0, $new_position, $item_limit, $my->id, $page, $alreadyFriend['friends']);
            if($alreadyFriend && $page_new == 0) {
                $peoples = array_merge($alreadyFriend['friends'], $rows);
            } else {
                $peoples = $rows;
            }
        }
        else {
            $peoples = $alreadyFriend['friends'];
        }
        $blockModel = CFactory::getModel('block');
        $model = CFactory::getModel('friends');

        $resultRows = array();
        if($peoples) {
            foreach ($peoples as $row) {
                $user = CFactory::getUser($row);
                $profilemodel = CFactory::getModel('profile');
                $profile = $profilemodel->getViewableProfile($user->id);
                $connection = $model->getFriendConnection($my->id, $user->id);
                $obj = new stdClass();
                $obj->id = $user->id;
                $obj->username = $user->username;
                $obj->displayname = $user->getDisplayName();
                $avatar = $user->_avatar;
                $cover = $user->_cover;
                $obj->gender = (string)$user->_gender;
                if($avatar == '') {
                    $obj->avatar = '';//JURI::base() . 'components/com_community/assets/user-Male-thumb.png'
                } else {
                    $obj->avatar = $root.'/'.$avatar;
                }
                if($cover == '') {
                    $obj->cover = '';//JURI::base() . 'components/com_community/assets/cover-undefined-default.png'
                } else {
                    $obj->cover = $root.'/'.$cover;
                }
//            $isFriend = CFriendsHelper::isConnected($row->id, $my->id);
                $isFriend = false;
                if($connection[0]->status == 1 && $connection[0]->message == ''){
                    $isFriend = true;
                }
                $isWaitingAproval = CFriendsHelper::isWaitingApproval($user->id, $my->id);

                $obj->friendsCount = $user->getFriendCount();
                $obj->profileLink = CUrlHelper::userLink($user->id);
                $obj->isFriend = $isFriend;
                $obj->isBlocked = $blockModel->getBlockStatus($my->id, $user->id);
                $obj->requestMessage = '';
                $obj->pendingRequest = false;
                if($connection && !$isFriend && $connection[0]->status != 1) {
                    $obj->pendingRequest = true;
                    $obj->requestMessage = $connection[0]->msg;
                }
                $obj->myRequest = false;
                if($my->id == $connection[0]->connect_from && !$isFriend && !$isWaitingAproval) {
                    $obj->myRequest = true;
                }
                foreach ($profile['fields']['Basic Information'] as $basic) {
                    if($basic['name'] == 'Position') {
                        if(!empty($basic['value'])) {
                            $obj->position = $basic['value'];
                        } else {
                            $obj->position = '';
                        }
                    }
                }
                $resultRows[] = $obj;
            }
            unset($rows);
            $response['status'] = true;
            $response['message'] = 'All friend loaded';
            $response['allFriend'] = $resultRows;
        } else {
            $response['status'] = false;
            $response['message'] = 'All friend not loaded';
            $response['allFriend'] = $resultRows;
        }
        return $response;
    }
    private function searchFriend($keyword, $type, $page, $limit) {

        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $modelFriend = CFactory::getModel('friends');

        $my = CFactory::getUser();
        if($type == 'all') {
            $model = CFactory::getModel('search');
            $search = array(
                'q' => $keyword,
                'search' => 'search'
            );
            $rows = $model->searchPeople($search, '', false, $page, $limit);
        } elseif($type == 'my') {
            $rows = $modelFriend->searchPeople($keyword);
        }
        $blockModel = CFactory::getModel('block');
        $profilemodel = CFactory::getModel('profile');
        $resultRows = array();
        if($rows) {
            foreach ($rows as $row) {
                $user = CFactory::getUser($row->id);
                $profile = $profilemodel->getViewableProfile($user->id);

                $connection = $modelFriend->getFriendConnection($my->id, $row->id);
                if($user->id == $my->id) {
                    continue;
                }
                $blockUser = $blockModel->getBlockStatus($row->id, $my->id);
                if($blockUser) {
                    continue;
                }
                $obj = new stdClass();
                $obj->id = $row->id;
                $obj->username = $row->username;
                $obj->displayname = $user->getDisplayName();
                $avatar = $user->_avatar;
                $cover = $user->_cover;
                $obj->gender = $user->_gender;
                if($avatar == '') {
                    $obj->avatar = '';//JURI::base() . 'components/com_community/assets/user-Male-thumb.png'
                } else {
                    $obj->avatar = $root.'/'.$avatar;
                }
                if($cover == '') {
                    $obj->cover = '';//JURI::base() . 'components/com_community/assets/cover-undefined-default.png'
                } else {
                    $obj->cover = $root.'/'.$cover;
                }
                $isFriend = false;
                if($connection[0]->status == 1 && $connection[0]->message == ''){
                    $isFriend = true;
                }
                $obj->friendsCount = $user->getFriendCount();
                $obj->profileLink = CUrlHelper::userLink($row->id);
                $obj->isFriend = $isFriend;
                $obj->isBlocked = $blockModel->getBlockStatus($my->id, $user->id);
                $obj->pendingRequest = false;
                $obj->requestMessage = '';
                $obj->myRequest = false;
                if($my->id == $connection[0]->connect_from && !$isFriend && !$isWaitingAproval) {
                    $obj->myRequest = true;
                }
                if($connection && !$isFriend && $connection[0]->status != 1) {
                    $obj->pendingRequest = true;
                    $obj->requestMessage = $connection[0]->msg;
                }
                foreach ($profile['fields']['Basic Information'] as $basic) {
                    if($basic['name'] == 'Position') {
                        if(!empty($basic['value'])) {
                            $obj->position = $basic['value'];
                        } else {
                            $obj->position = '';
                        }
                    }
                }
                $resultRows[] = $obj;
            }
            unset($rows);
            $response['status'] = true;
            $response['message'] = 'Friend search success';
            $response['searchFriend'] = $resultRows;
        } else {
            $response['status'] = false;
            $response['message'] = 'Friend search failed';
            $response['searchFriend'] = $resultRows;
        }
        return $response;
    }


    private function addFriend($friendId, $message) {

        $model = CFactory::getModel('friends');
        $user = CFactory::getUser($friendId);
        $my = CFactory::getUser();

        if ($my->id == $friendId) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_FRIENDS_CANNOT_ADD_SELF');
            return $response;
        }
        $connection = $model->getFriendConnection($my->id, $friendId);
        if($model->addFriend($user->id, $my->id, $message)) {
            $params = new CParameter('');
            $params->set('url', 'index.php?option=com_community&view=friends&task=pending');
            $params->set('msg', $message);

            CNotificationLibrary::add(
                'friends_request_connection',
                $my->id,
                $friendId,
                JText::_('COM_COMMUNITY_FRIEND_ADD_REQUEST'),
                '',
                'friends.request',
                $params
            );
            //add user points - friends.request.add removed @ 20090313
            //trigger for onFriendRequest
            $eventObject = new stdClass();
            $eventObject->profileOwnerId = $my->id;
            $eventObject->friendId = $friendId;
            $this->triggerFriendEvents('onFriendRequest', $eventObject);
            unset($eventObject);

            $response['status'] = true;
            $response['message'] = 'Requested to be your friend';

            $message_send = array(
                'mtitle' => str_replace('{actor}', $my->getDisplayName(), JText::_('COM_COMMUNITY_FRIEND_ADD_REQUEST')),
                'mdesc'  => $message
            );
            $payload = $this->pushNotification($message_send, $friendId, $friendId, 0, 'request_friend');
            $response['push_data'] = $payload['android'];
            $response['push_ios'] = $payload['ios'];
        } else {
            $response['status'] = false;
            $response['message'] = 'Friend requested send failed';
            $response['push_data'] = new stdClass();
            $response['push_ios'] = new stdClass();
        }
        return $response;
    }

    public function triggerFriendEvents($eventName, &$args, $target = null)
    {
        CError::assert($args, 'object', 'istype', __FILE__, __LINE__);

        require_once(COMMUNITY_COM_PATH . '/libraries/apps.php');
        $appsLib = CAppPlugins::getInstance();
        $appsLib->loadApplications();

        $params = array();
        $params[] = $args;

        if (!is_null($target)) {
            $params[] = $target;
        }

        $appsLib->triggerEvent($eventName, $params);
        return true;
    }
    private function approvedFriend($friendid, $type_request) {
        $model = CFactory::getModel('friends');
        $user = CFactory::getUser($friendid);
        $my = CFactory::getUser();

        if(!$user) {
            $response['status'] = false;
            $response['message'] = 'Friend doesn\'t exits.';
            return $response;
        }
        if ($my->id == $friendid) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_FRIENDS_CANNOT_ADD_SELF');
            return $response;
        }
        $connection = $model->getFriendConnection($friendid, $my->id);
        if($connection) {
            if($type_request == 'approve') {
                if($connected = $model->approveRequest($connection[0]->connection_id)) {
                    $act = new stdClass();
                    $act->cmd = 'friends.request.approve';
                    $act->actor = $connected[0];
                    $act->target = $connected[1];
                    $act->title = ''; //JText::_('COM_COMMUNITY_ACTIVITY_FRIENDS_NOW');
                    $act->content = '';
                    $act->app = 'friends.connect';
                    $act->cid = 0;

                    //add user points - give points to both parties
                    //CFactory::load( 'libraries' , 'userpoints' );
                    if(CUserPoints::assignPoint('friends.request.approve')){
                        CActivityStream::add($act);
                    };

                    $friendid = ($connected[0] == $my->id) ? $connected[1] : $connected[0];
                    $friend = CFactory::getUser($friendid);
                    $friendUrl = CRoute::_('index.php?option=com_community&view=profile&userid=' . $friendId);
                    CUserPoints::assignPoint('friends.request.approve', $friendid);

                    // need to both user's friend list
                    $model->updateFriendCount($my->id);
                    $model->updateFriendCount($friendId);

                    $params = new CParameter('');
                    $params->set('url', 'index.php?option=com_community&view=profile&userid=' . $my->id);
                    $params->set('friend', $my->getDisplayName());
                    $params->set('friend_url', 'index.php?option=com_community&view=profile&userid=' . $my->id);

                    CNotificationLibrary::add(
                        'friends_create_connection',
                        $my->id,
                        $friend->id,
                        JText::_('COM_COMMUNITY_FRIEND_REQUEST_APPROVED'),
                        '',
                        'friends.approve',
                        $params
                    );

                    //trigger for onFriendApprove
                    $eventObject = new stdClass();
                    $eventObject->profileOwnerId = $my->id;
                    $eventObject->friendId = $friendid;
                    $this->triggerFriendEvents('onFriendApprove', $eventObject);
                    // push notification
                    $message_send = array(
                        'mtitle' => 'You are now friends with '.$my->getDisplayName(),
                        'mdesc'  => 'Successfully.'
                    );
                    $payload = $this->pushNotification($message_send, $friendid, $friendid, 0, 'approve_friend');

                    unset($eventObject);
                    $response['status'] = true;
                    $response['message'] = 'Friend approve request success';
                    $response['push_data'] = $payload['android'];
                    $response['push_ios'] = $payload['ios'];
                    return $response;
                } else {
                    $response['status'] = false;
                    $response['message'] = 'Friend request approve failed';
                    return $response;
                }
            } elseif($type_request == 'reject') {
                if($model->rejectRequest($connection[0]->connection_id)) {
                    $friend = CFactory::getUser($friendid);
                    $response['status'] = true;
                    $response['message'] = 'You have rejected the friend request from '.$friend->getDisplayName();
                    //trigger for onFriendReject
                    $eventObject = new stdClass();
                    $eventObject->profileOwnerId = $my->id;
                    $eventObject->friendId = $friendid;
                    $this->triggerFriendEvents('onFriendReject', $eventObject);
                    // push notification
                    $message_send = array(
                        'mtitle' => 'You have rejected the friend request from '.$friend->getDisplayName(),
                        'mdesc'  => $my->name.' reject request of you.'
                    );
                    $payload = $this->pushNotification($message_send, $friendId, $friendId, 0, 'reject_friend');
                    $response['push_data'] = $payload['android'];
                    $response['push_ios'] = $payload['ios'];
                    unset($eventObject);
                    return $response;
                } else {
                    $response['status'] = false;
                    $response['message'] = 'Friend reject request failed';
                    return $response;
                }
            } else {
                $response['status'] = false;
                $response['message'] = 'Request type invalid. Please check request.';
                return $response;
            }
        } else {
            $response['status'] = false;
            $response['message'] = 'Friend request not avilable';
            return $response;
        }
    }

    private function removeFriend($friendid, $block) {
        $my = CFactory::getUser();
        $friend = CFactory::getUser($friendid);

        if (empty($my->id) || empty($friend->id)) {
            return false;
        }

        //CFactory::load( 'helpers' , 'friends' );
        $isFriend = $my->isFriendWith($friend->id);
        if (!$isFriend) {
            return array(
                'status' => false,
                'message' => 'Oops! Currently you are not connected with '.$friend->getDisplayName()
            );
        }

        $model = CFactory::getModel('friends');

        if ($model->deleteFriend($my->id, $friend->id)) {
            // block user
            if($block == 1) {
                $Blockmodel = CFactory::getModel('block');

                $Blockmodel->blockUser($my->id, $friend->id);
            }
            // Substract the friend count
            $model->updateFriendCount($my->id);
            $model->updateFriendCount($friend->id);


            // Update friend list of both current user and friend.
            $friend->updateFriendList(true);
            $my->updateFriendList(true);


            // Add user points
            // We deduct points to both parties
            //CFactory::load( 'libraries' , 'userpoints' );
            CUserPoints::assignPoint('friends.remove');
            CUserPoints::assignPoint('friends.remove', $friend->id);

            // Trigger for onFriendRemove
            $eventObject = new stdClass();
            $eventObject->profileOwnerId = $my->id;
            $eventObject->friendId = $friend->id;
            $this->triggerFriendEvents('onFriendRemove', $eventObject);
            unset($eventObject);
            $response['status'] = true;
            $response['message'] = JText::_('COM_COMMUNITY_FRIENDS_REMOVED');
            return $response;
        } else {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_FRIENDS_REMOVING_FRIEND_ERROR');
            return $response;
        }
    }

    public function blockUnblockFriend($friendid, $status)
    {
        if(empty($friendid) || empty($status)) {
            return array(
                'status' => false,
                'message' => 'Invalid parameters. Please try agian!'
            );
        }
        if(!in_array($status, array('block', 'unblock'))) {
            return array(
                'status' => false,
                'message' => 'Invalid parameters. Status must be block or unblock.'
            );
        }
        $my = CFactory::getUser();
        $friend = CFactory::getUser($friendid);
        if($friend->id == 0) {
            return array(
                'status' => false,
                'message' => 'Oops! Friend is not already exits. Please try again.'
            );
        }
        $model = CFactory::getModel('friends');
        $Blockmodel = CFactory::getModel('block');
        if($status == 'block') {
            $isFriend = $my->isFriendWith($friend->id);
            if(!$isFriend) {
                return array(
                    'status' => false,
                    'message' => 'Oops! Currently you are not connected with '.$friend->getDisplayName()
                );
            }
            if (!$Blockmodel->blockUser($my->id, $friend->id)) {
                return array(
                    'status' => false,
                    'message' => 'Something wrong. Please try again!'
                );
            }
            if (!$model->deleteFriend($my->id, $friend->id)) {
                return array(
                    'status' => true,
                    'message' => 'Block successfully, but can not delete friend too!'
                );
            }

            // Substract the friend count
            $model->updateFriendCount($my->id);
            $model->updateFriendCount($friend->id);

            // Add user points
            // We deduct points to both parties
            //CFactory::load( 'libraries' , 'userpoints' );
            CUserPoints::assignPoint('friends.remove');
            CUserPoints::assignPoint('friends.remove', $friend->id);

            // Trigger for onFriendRemove
            $eventObject = new stdClass();
            $eventObject->profileOwnerId = $my->id;
            $eventObject->friendId = $friend->id;
            $this->triggerFriendEvents('onFriendRemove', $eventObject);
            unset($eventObject);
            return array(
                'status' => true,
                'message' => 'Friend block successfully and remove on friend block.'
            );
        }
        if($status == 'unblock') {
            if($my->unblockUser($friend->id)) {
                return array(
                    'status' => true,
                    'message' => 'Unblock user successfully.'
                );
            } else {
                return array(
                    'status' => false,
                    'message' => 'Something wrong, Please try agian.'
                );
            }
        }
    }

    public function pushNotificationTopic($id, $wallid, $message, $reception, $app = '', $groupid, $type_comment, $ext) {
        require_once('PushNotifications.php');
        $pushNotification = new PushNotifications();
        $push = new push();
        // optional payload
        $payload = array();
        $payload['team'] = 'Vietnam';
        $payload['score'] = '5.6';
        $payload['sendtoid'] = $id;
        $payload['sendid'] = $wallid;

        $title = $message['mtitle'];
        $message_content = $message['mdesc'];

        if($type_comment == 'image' || $type_comment == 'document') {
            $media = $message_content;
            $body = '';
        } else {
            $media = 'no image';
            $body = $message_content;
        }

        $push->setTitle($title);
        $push->setMessage($body);
        $push->setType('reply_topic');
        $push->setID($id);
        $push->setGroupID($groupid);
        $push->setImage($media);
        $push->setImageType($ext);
        $push->setIcon(JPATH_ROOT.'/templates/t3_bs3_tablet_desktop_template/images/parenthexis-logo.png');
        $push->setIsBackground(FALSE);
        $push->setFlag(1);
        $push->setSendID($wallid);
        $push->setSendToID($id);
        $push->setPayload($payload);
        $response = '';

        $fcmMsg = array(
            'body' => $body,
            'title' => $title,
            'sound' => 'default',
            'vibrate' => 1,
            'icon' => JPATH_ROOT.'/templates/t3_bs3_tablet_desktop_template/images/parenthexis-logo.png',
        );
        $data = array(
            'noti_type' => 'reply_topic',
            'noti_id' => $id,
            'group_id' => $groupid,
            'sendtoid' => $id,
            'sendid' => $wallid,
            'mediaUrl' => $media,
            'mediaType' => $ext,
        );

        $receptionArr = array();
        if(!is_array($reception)) {
            $receptionArr[] = $reception;
        } else {
            $receptionArr = $reception;
        }
        $json = $push->getPush();
        foreach ($receptionArr as $recpt) {
            $user = $this->getUserDevice($recpt);
            $user_device_token = '';
            if($app == 'creator') {
                if(isset($user->device_id2)) {
                    $user_device_token = $user->device_id2;
                }
            } else {
                if(isset($user->device_id)) {
                    $user_device_token = $user->device_id;
                }
            }
            $fcmFields = array(
                'to' => $user_device_token,
                'priority' => 'high',
                'notification' => $fcmMsg,
                'data' => $data
            );
            if($user_device_token) {
//                    $response = $pushNotification->send($user_device_token, $json);
                $response = $pushNotification->sendPushNotification($fcmFields);
            }
        }
        $datas['android'] = $json;
        $datas['ios'] = $fcmFields;
        return $datas;
    }

    public function pushNotification($message, $reception, $id, $groupid, $type, $app = '') {
        require_once('PushNotifications.php');
        $pushNotification = new PushNotifications();
        $push = new push();

        // optional payload
        $payload = array();
        $payload['team'] = 'Vietnam';
        $payload['score'] = '5.6';

        $title = $message['mtitle'];
        $message_content = $message['mdesc'];

        $fcmMsg = array(
            'body' => $message_content,
            'title' => $title,
            'sound' => 'default',
            'vibrate' => 1,
            'icon' => JPATH_ROOT.'/templates/t3_bs3_tablet_desktop_template/images/parenthexis-logo.png',
        );
        $data = array(
            'noti_type' => $type,
            'noti_id' => $id,
            'group_id' => $groupid,
            'sendtoid' => 0,
            'sendid' => 0,
        );

        $push->setTitle($title);
        $push->setMessage($message_content);
        $push->setType($type);
        $push->setID($id);
        $push->setGroupID($groupid);
        $push->setImage('');
        $push->setIsBackground(FALSE);
        $push->setFlag(1);
        $push->setPayload($payload);

        $response = '';

        $receptionArr = array();
        if(!is_array($reception)) {
            $receptionArr[] = $reception;
        } else {
            $receptionArr = $reception;
        }
        $json = $push->getPush();
        if($type == 'remove_user') {
            if($app) {
                    $fcmFields = array(
                        'to' => $app,
                        'priority' => 'high',
                        'notification' => $fcmMsg,
                        'type' => $type,
                        'data' => $data
                    );
                    $response = $pushNotification->sendPushNotification($fcmFields);
            }
        } else {
        foreach ($receptionArr as $recpt) {
            $user = $this->getUserDevice($recpt);
            $user_device_token = '';
            if($app == 'creator') {
                if(isset($user->device_id2)) {
                    $user_device_token = $user->device_id2;
                    }
                } else {
                    if(isset($user->device_id)) {
                        $user_device_token = $user->device_id;
                    }
                }
                if($user_device_token) {
                    $fcmFields = array(
                        'to' => $user_device_token,
                        'priority' => 'high',
                        'notification' => $fcmMsg,
                        'data' => $data
                    );
                    $response = $pushNotification->sendPushNotification($fcmFields);
                }
            }
        }
        $data['android'] = $json;
        $data['ios'] = $fcmFields;
        return $data;
    }

    public function getUserDevice($userid) {
        $db = JFactory::getDBO();
        $query = 'SELECT device_id, device_id2 FROM #__users WHERE ' . $db->quoteName('id') . ' = ' . $db->quote($userid);
        $db->setQuery( $query );
        $return = $db->loadObject();
        return $return;
    }


    public function push_notification($device, $title, $message) {
        require_once('PushNotifications.php');
        $config = new JConfig();
        $pushNotification = new PushNotifications();
        $push = new push();

        // optional payload
        $payload = array();
        $payload['team'] = 'Vietnam';
        $payload['score'] = '5.6';

//            $title = $message['mtitle'];
//            $message_content = $message['mdesc'];

        $push->setTitle($title);
        $push->setMessage($message);
        $push->setImage('');
        $push->setIsBackground(FALSE);
        $push->setFlag(1);
        $push->setPayload($payload);

//            $json = '';
        $response = '';
        $json = $push->getPush();

        // fcm send message for ios testing
        $fcmMsg = array(
            'body' => $message,
            'title' => $title,
            'sound' => 'default',
            'vibrate' => 1,
        );
        $fcmFields = array(
            'to' => $device,
            'priority' => 'high',
            'notification' => $fcmMsg
        );


        $response = $pushNotification->sendPushNotification($fcmFields);

        return array(
            'fcm_return' => $response,
            'payload_data' => $fcmFields
        );
    }

    public function assignManage($groupid, $data) {
        $json_data = json_decode($data);
        if($json_data) {
            foreach ($json_data as $key => $value) {
                if($value->user) {
                    $userid = JUserHelper::getUserId($value->user);
                    $role = $value->roleid;
                    $status = $value->status;
                    $this->saveAssign($groupid, $userid, $role, $status);
                }
            }
            $response = array(
                'status' => true,
                'message' => 'assign success',
            );
        }
        return $response;

    }

    public function assign($groupid, $data, $username, $type) {
        /* $data: [{"frname":"<username>", "roleid": "4", "status": 0}];
         * roleid = 5: leaner, roleid = 4: Facilitator
         * status = 0: unassign, status = 1: assign;
         * $type: facilitator or learner.
         */
        $json_data = json_decode($data);
        // check group already exits
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);

        if(empty($group->id)) {
            return $response = array(
                'status' => false,
                'message' => 'Circle not found.',
            );
        }
        if(empty($username)) {
            return $response = array(
                'status' => false,
                'message' => 'User can\'t empty.',
            );
        }
        $type_arr = array('learner', 'facilitator', 'manager');
        if(!in_array($type, $type_arr)) {
            return $response = array(
                'status' => false,
                'message' => 'Type not found. You can choose type learner or facilitator.',
            );
        }
        $userid = JUserHelper::getUserId($username);
        $groupModel = CFactory::getModel('groups');
        $isAdmin = $groupModel->isAdmin($userid, $group->id);

//        $isOwner = $groupModel->isCreator($userid, $group->id);
        if((!$isAdmin)) {
            return $response = array(
                'status' => false,
                'message' => 'You don\'t have a permission to assign.',
            );
        }

        // get course of circle
        $params = json_decode($group->params);
        if($json_data) {
            // check role
            foreach ($json_data as $keys=>$values) {
                if($type == 'learner') {
                    $role_name = 'Learner';
                    if($values->rid != 5) {
                        return $response = array(
                            'status' => false,
                            'message' => 'Param roleid invalid, Please set all learner role = 5.',
                        );
                    }
                }
                if($type == 'facilitator') {
                    $role_name = 'Facilitator';
                    if($values->rid != 4) {
                        return $response = array(
                            'status' => false,
                            'message' => 'Param roleid invalid, Please set all facilitator role = 4.',
                        );
                    }
                    //check if member
                    /*$idUser = JUserHelper::getUserId($values->frname);
                    $isMember = $groupModel->isMember($idUser, $group->id);
                    if(!$isMember) {
                        return $response = array(
                            'status' => false,
                            'message' => 'User is not member of Circle.',
                        );
                    }*/
                }
                if($type == 'manager') {
                    $role_name = 'Manager';
                    if($values->rid != 1) {
                        return $response = array(
                            'status' => false,
                            'message' => 'Param roleid invalid, Please set manager role is 1.',
                        );
                    }
                    //check if member
                    /*$idUser = JUserHelper::getUserId($values->frname);
                    $isMember = $groupModel->isMember($idUser, $group->id);
                    if(!$isMember) {
                        return $response = array(
                            'status' => false,
                            'message' => 'User is not member of Circle.',
                        );
                    }*/
                }
            }
            // assign role learner to course
            if($params->course_id) {
                require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');
                $act = 1;
                // update role for manage
//                $course_role = JoomdleHelperContent::call_method('get_user_role', (int) $params->course_id, $username);
//                print_r($course_role);die;
//                $data_manger = '[{ "rid" : "4",  "frname" : '.$username.',  "status" : 1}]';
//                JoomdleHelperContent::call_method('set_user_role', $act, (int) $params->course_id, $username, $data_manger);
                $assign_learner = JoomdleHelperContent::call_method('set_user_role', $act, (int) $params->course_id, $username, $data);
            }
            $reception = array();
            foreach ($json_data as $key=>$value) {
                if($value->frname && $value->rid == 4 && $value->status == 1) {
                    $userid_recept = JUserHelper::getUserId($value->frname);
                    $reception[] = $userid_recept;
                }
            }
            if($assign_learner->status == 1) {
                foreach ($json_data as $key=>$value) {
                    if(!isset($value->frname) || !isset($value->rid) || !isset($value->status)) {
                        return $response = array(
                            'status' => false,
                            'message' => 'Params invalid, Please put param with format [{"frname": "<username>", "rid": "5", "status": "1"}].',
                        );
                    }
                    if($value->frname) {
                        $userid = JUserHelper::getUserId($value->frname);
                        $role = $value->rid;
                        $status = $value->status;
                        if($type == 'learner') {
                            $role = 0;
                        }
                        if($type == 'facilitator') {
                            $role = 1;
                        }
                        if($type == 'manager') {
                            $role = 1;
                        }

                        $this->saveAssign($groupid, $userid, $role, $status);


                    }

                }
            }
        }
        $message_send = array(
            'mtitle' => 'You have been assigned as a '.$role_name.' for the course: ' . $group->name . '',
            'mdesc' => $group->description
        );
        $payload = $this->pushNotification($message_send, $reception, $group->id, $group->id, 'assign_facilitator');
        $response['status'] = true;
        $response['message'] = 'Assign successfully.';
        $response['push_data'] = $payload['android'];
        $response['push_ios'] = $payload['ios'];
        return $response;
    }

    public function saveAssign($groupid, $userid, $role, $status) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        if($role == 1 && $status == 1) {
            $manage = 1;
            $type_role = 'manager';
        } elseif($role != 1 && $status == 1) {
            $manage = 0;
            $type_role = '';
        } else {
            $manage = 0;
            $type_role = '';
        }
        // check already role assign
        $already = $this->checkAlreadyExistRole($groupid, $userid);
        if(empty($already) && $status) {
            $query  = 'INSERT INTO '. $db->quoteName('#__community_groups_members')
                .' SET ' . $db->quoteName('groupid').' = ' . $db->Quote($groupid)
                . ', '. $db->quoteName('memberid').' = ' . $db->Quote($userid)
                . ', '. $db->quoteName('approved').' = '. $db->Quote(1)
                . ', '. $db->quoteName('permissions').' = ' . $db->Quote($manage)
                . ', '. $db->quoteName('date').' = ' . $db->Quote(time())
                . ', '. $db->quoteName('roles').' = ' . $db->Quote($type_role);
        }
        else {
            $query =  'UPDATE '. $db->quoteName('#__community_groups_members')
                .' SET ' . $db->quoteName('permissions') . '=' . $db->Quote($manage)
                .', '. $db->quoteName('roles').' = ' . $db->Quote($type_role)
                .' WHERE ' . $db->quoteName('memberid') . '=' . $db->Quote($userid)
                .' AND ' . $db->quoteName('groupid') . '=' . $db->Quote($groupid);
        }
        $db->setQuery($query);
        $db->query();
        if ($db->getErrorNum())
        {
            JError::raiseError(500, $db->stderr());
        }
    }
    function checkAlreadyExistRole($groupid, $memberid) {
        $db = JFactory::getDBO();

        $query = 'SELECT * FROM '.$db->quoteName('#__community_groups_members').''
            . ' WHERE '.$db->quoteName('groupid').' = '.$db->quote($groupid).''
            . ' AND '.$db->quoteName('memberid').' = '.$db->quote($memberid);
        $db->setQuery($query);
        $data = $db->loadRowList();
        return $data;
    }

    public function getNewsFeedHomePage($username, $limit, $page) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $userid = CUserHelper::getUserId($username);
        if ($userid) {
            $user = CFactory::getUser($userid);
            $username = $user->username;
        } else {
            $response['status'] = false;
            $response['error_message'] = 'Username not found.';
            header('Content-type: application/json; charset=UTF-8');
            header("Status", true, 400);
            echo json_encode($response);
            exit;
        }
        // Get My Friends block
        $haveFriends = ($user->_friendcount > 0) ? true : false;

        // Check update user info
        $updatedProfie = true;
        $aboutMe = $user->getInfo('FIELD_ABOUTME');
        $gender = $user->getInfo('FIELD_GENDER');
        $birthDate = $user->getInfo('FIELD_BIRTHDATE');
        $avatar = $user->_avatar;
        $cover = $user->_cover;
        $interests = $user->getInfo('FIELD_INTEREST');
//        $number = $user->getInfo('FIELD_MOBILE');
//        $address = $user->getInfo('FIELD_ADDRESS');
//        $postcode = $user->getInfo('FIELD_POSTCODE');
        if (empty($aboutMe) && empty($birthDate) && empty($gender) && empty($avatar) && empty($cover) && empty($interests) ) {
            $updatedProfie = false;
        }

        // get data
        $config = CFactory::getConfig();
        $act = new CActivityStream();
        $activities = CFactory::getModel('activities');
        $memberSince = CTimeHelper::getDate($user->registerDate);
        $friendIds = $user->getFriendIds();
        $groupIds = explode(',', $user->groups);
        $params = $user->getParams();
        $view = 'frontpage';
        if ($limit && $limit > 0) {
            $actLimit = $limit;
        } else {
            $actLimit = ($view == 'profile') ? $params->get('activityLimit', $config->get('maxactivities')) : $config->get('maxactivities');
        }
        if ($page && $page > 0) {
            $position = ($page - 1) * $actLimit;
        } else {
            $position = null;
        }

        $rows = $activities->getActivities($user->id, $friendIds, $memberSince, $actLimit, $config->get('respectactivityprivacy'), null, true, null, null, null, array(), $user->id, $position);

        $groupModel = CFactory::getModel('groups');

        $result = array();
        if ($rows) {
            $i = 0;
            foreach ($rows as $row) {
                $oRow = $row;
                $oRow->activities = array();

                $act = new stdClass();
                $act->id = $oRow->id;

                $title = $row->title;
                $app = $row->app;
                $actor = $row->actor;
                $commentTypeId = $row->comment_type . $row->comment_id;
                // all action alway show actor Object

                if ($oRow->app == 'groups.join') {
                    if ($oRow->actors) {
                        $actors = json_decode($oRow->actors);
                        $actorslist = $actors->userid;
                        $Objactors = array();
                        foreach ($actorslist as $acto) {
                            $Objact = new stdClass();
                            $Objact->actor_id = $acto->id;

                            $actor_info = CFactory::getUser($acto->id);
                            $Objact->actor_name = $actor_info->getDisplayName();
                            if ($actor_info->_avatar && $actor_info->_avatar != '') {
                                $Objact->actor_avatar = $root . '/' . $actor_info->_avatar;
                            } else {
                                $Objact->actor_avatar = '';
                            }
                            $Objactors[] = $Objact;
                            if ($acto->id == $userid || in_array($acto->id, $friendIds)) {
                                $Objactor = $Objact;
                            }
                        }
                    } else {
                        continue;
                    }
                } else {
                    $Objactor = new stdClass();
                    $Objactor->actor_id = $oRow->actor;

                    $actor_info = CFactory::getUser($oRow->actor);
                    $Objactor->actor_name = $actor_info->getDisplayName();
                    if ($actor_info->_avatar && $actor_info->_avatar != '') {
                        $Objactor->actor_avatar = $root . '/' . $actor_info->_avatar;
                    } else {
                        $Objactor->actor_avatar = '';
                    }
                }
                // end actor Obj
                //Check for event or group title if exists
                if ($row->eventid) {
                    $eventModel = CFactory::getModel('events');
                    $act->appTitle = $eventModel->getTitle($row->eventid);
                }
                if ($row->groupid) {
                    $group_name = $groupModel->getGroupName($row->groupid);
                    $iscourse = $groupModel->getIsCourseGroup($oRow->groupid);
                    if($iscourse) {
                        $check_facilitator = JoomdleHelperContent::call_method('check_facilitator', (int)$iscourse, (string)$actor_info->username);
                        $isFacilitator = (boolean) $check_facilitator->isFacilitator;
                    }
                }

                for ($j = $i; ($j < count($rows)) && ($row->getDayDiff() == $day); $j++) {
                    $row = $rows[$j];
                    // we aggregate stream that has the same content on the same day.
                    // we should not however aggregate content that does not support
                    // multiple content. How do we detect? easy, they don't have
                    // {multiple} in the title string
                    // However, if the activity is from the same user, we only want
                    // to show the laste acitivity
                    if (($row->getDayDiff() == $day) && ($row->title == $title) && ($app == $row->app) && ($cid == $row->cid) && (
                            (JString::strpos($row->title, '{/multiple}') !== FALSE) ||
                            ($row->actor == $actor)
                        )

                        // Aggregate the content only if the like/comment is the same
                        // we only perform test on comment which shoould be enough
                        && ($commentTypeId == ($row->comment_type . $row->comment_id)) && $row->app != "photos"
                    ) {
                        // @rule: If an exclusion is added, we need to fetch activities without these items.
                        // Aggregated activities should also be excluded.
                        // $row->used = true;
                        $oRow->activities[] = $row;
                    }
                }

                $app = !empty($oRow->app) ? $this->_appLink($oRow->app, $oRow->actor, $oRow->target, $oRow->title) : '';

                $oRow->title = CString::str_ireplace('{app}', $app, $oRow->title);

                $act->cid = $oRow->cid;
//                $act->title = $oRow->title;

                if ($oRow->app == 'groups.join') {
                    $act->actors = $Objactors;
                } else {
                    $act->actors = $oRow->actors;
                }
                $act->target = $oRow->target;
                if ($act->target != 0) {
                    $target_info = CFactory::getUser($oRow->target);
                    $act->target_name = $target_info->getDisplayName();
                }
                $act->access = $oRow->access;

                $timeFormat = $config->get('activitiestimeformat');
                $dayFormat = $config->get('activitiesdayformat');
                $date = CTimeHelper::getDate($oRow->created);

                $act->content = $oRow->content;

                $Objgroup = null;
                $ObjgroupUpdate = null;
                $ObjgroupDiscuss = null;
                $ObjgroupAnnount = null;
                $ObjgroupEvent = null;
                $ObjgroupEventUpdate = null;
                $ObjgroupJoin = null;
                $ObjVideoLink = null;
                $ObjPhotos = null;
                $ObjProfile = null;
                $ObjFriendConnect = null;
                $ObjProfileUploadAvatar = null;
                $ObjProfileUploadCover = null;
                $ObjDiscussionReply = null;

                if(($oRow->app == 'cover.upload' && $oRow->groupid > 0) || $oRow->app == 'events.avatar.upload' || $oRow->app == 'photo.like' || $oRow->app == 'discussion.avatar.upload') {
                    continue;
                }
                switch ($oRow->app){
                    case 'groups':          // create group
                        $Objgroup = new stdClass();
                        $act->type = 'group.create';
                        $Objgroup->groupid = (int)$oRow->groupid;
                        $Objgroup->group_name = $group_name;
                        $Objgroup->ismember = (boolean)$groupModel->isMember($userid, $oRow->groupid);
                        $Objgroup->group_type = 'social_circle';
                        $Objgroup->course = 0;
                        $Objgroup->isFacilitator = false;
                        if($iscourse) {
                            $Objgroup->group_type = 'learning_circle';
                            $Objgroup->course = $iscourse;
                            $Objgroup->isFacilitator = $isFacilitator;
                        }
//                        $check_facilitator = JoomdleHelperContent::call_method('check_facilitator', (int)$courseid, (string)$my->username);
//                    $groups[$k]->isFacilitator = (boolean) $check_facilitator->isFacilitator;
//                        $act->isfacilitator = '';
//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
//                        $circle_text = '<b>'.$group_name.'</b>';
                        $titles = CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_GROUP_CREATE'));
                        $act->title = ''.CString::str_ireplace('{circle}', $group_name, $titles);
                        break;
                    case 'groups.update':   // update group
                        $ObjgroupUpdate = new stdClass();
                        $act->type = 'group.update';
                        $ObjgroupUpdate->groupid = (int)$oRow->groupid;
                        $ObjgroupUpdate->group_name = $group_name;
                        $ObjgroupUpdate->ismember = (boolean)$groupModel->isMember($userid, $oRow->groupid);
                        $ObjgroupUpdate->group_type = 'social_circle';
                        $ObjgroupUpdate->course = 0;
                        $ObjgroupUpdate->isFacilitator = false;
                        if($iscourse) {
                            $ObjgroupUpdate->group_type = 'learning_circle';
                            $ObjgroupUpdate->course = $iscourse;
                            $ObjgroupUpdate->isFacilitator = $isFacilitator;
                        }
//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
//                        $circle_text = '<b>'.$group_name.'</b>';
                        $titles = CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_GROUP_UPDATE'));
                        $act->title = ''.CString::str_ireplace('{circle}', $group_name, $titles);
                        break;
                    case 'groups.avatar.upload':   // update group
                        $ObjgroupUpdate = new stdClass();
                        $act->type = 'group.avatar.upload';
                        $ObjgroupUpdate->groupid = (int)$oRow->groupid;
                        $ObjgroupUpdate->group_name = $group_name;
                        $ObjgroupUpdate->ismember = (boolean)$groupModel->isMember($userid, $oRow->groupid);
                        $ObjgroupUpdate->group_type = 'social_circle';
                        $ObjgroupUpdate->course = 0;
                        $ObjgroupUpdate->isFacilitator = false;
                        if($iscourse) {
                            $ObjgroupUpdate->group_type = 'learning_circle';
                            $ObjgroupUpdate->course = $iscourse;
                            $ObjgroupUpdate->isFacilitator = $isFacilitator;
                        }
//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
//                        $circle_text = '<b>'.$group_name.'</b>';
                        $titles = CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_GROUP_UPDATE_PHOTO'));
                        $act->title = ''.CString::str_ireplace('{circle}', $group_name, $titles);
                        break;
                    case 'groups.discussion':   // create discussion
                        $ObjgroupDiscuss = new stdClass();
                        $act->type = 'discussion.create';
                        $act_topic = JTable::getInstance('Discussion', 'CTable');
                        $act_topic->load($act->cid);
                        $ObjgroupDiscuss->topic_title = $act_topic->title;
                        $ObjgroupDiscuss->topic_id = $act->cid;
                        $ObjgroupDiscuss->groupid = (int)$oRow->groupid;
                        $ObjgroupDiscuss->group_name = $group_name;
                        $ObjgroupDiscuss->ismember = (boolean)$groupModel->isMember($userid, $oRow->groupid);
                        $ObjgroupDiscuss->group_type = 'social_circle';
                        $ObjgroupDiscuss->course = 0;
                        $ObjgroupDiscuss->isFacilitator = false;
                        if($iscourse) {
                            $ObjgroupDiscuss->group_type = 'learning_circle';
                            $ObjgroupDiscuss->course = $iscourse;
                            $ObjgroupDiscuss->isFacilitator = $isFacilitator;
                        }
//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
//                        $topic_text = '<b>'.$act_topic->title.'</b>';
//                        $circle_text = '<b>'.$group_name.'</b>';
                        $titles = CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_GROUP_CREATED_TOPIC'));
                        $titles = CString::str_ireplace('{topic}', $act_topic->title, $titles);
                        $act->title = ''.CString::str_ireplace('{circle}', $group_name, $titles);
                        break;
                    case 'groups.bulletin':     // create annou
                        $ObjgroupAnnount = new stdClass();
                        $act->type = 'groups.bulletin';
                        $act_annou = JTable::getInstance('Bulletin', 'CTable');
                        $act_annou->load($act->cid);
                        $ObjgroupAnnount->annou_title = $act_annou->title;
                        $ObjgroupAnnount->annou_id = $act->cid;
                        $ObjgroupAnnount->groupid = (int)$oRow->groupid;
                        $ObjgroupAnnount->group_name = $group_name;
                        $ObjgroupAnnount->group_type = 'social_circle';
                        $ObjgroupAnnount->course = 0;
                        $ObjgroupAnnount->isFacilitator = false;
                        if($iscourse) {
                            $ObjgroupAnnount->group_type = 'learning_circle';
                            $ObjgroupAnnount->course = $iscourse;
                            $ObjgroupAnnount->isFacilitator = $isFacilitator;
                        }
                        $ObjgroupAnnount->ismember = (boolean)$groupModel->isMember($userid, $oRow->groupid);
//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
//                        $annou_text = '<b>'.$act->annou_title.'</b>';
//                        $circle_text = '<b>'.$group_name.'</b>';
                        $titles = CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_GROUP_CREATED_ANNOU'));
                        $titles = CString::str_ireplace('{annou}', $act_annou->title, $titles);
                        $act->title = ''.CString::str_ireplace('{circle}', $group_name, $titles);
                        break;
                    case 'events':              // create event
                        $ObjgroupEvent = new stdClass();
                        $act->type = 'event.create';
//                        $act_event = JTable::getInstance('Event', 'CTable');
//                        $act_event->load($act->cid);
                        $ObjgroupEvent->event_title = $act->appTitle;
                        $ObjgroupEvent->event_id = $act->cid;
                        $ObjgroupEvent->groupid = (int)$oRow->groupid;
                        $ObjgroupEvent->group_name = $group_name;
                        $ObjgroupEvent->group_type = 'social_circle';
                        $ObjgroupEvent->course = 0;
                        $ObjgroupEvent->isFacilitator = false;
                        if($iscourse) {
                            $ObjgroupEvent->group_type = 'learning_circle';
                            $ObjgroupEvent->course = $iscourse;
                            $ObjgroupEvent->isFacilitator = $isFacilitator;
                        }
                        $ObjgroupEvent->ismember = (boolean)$groupModel->isMember($userid, $oRow->groupid);
//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
//                        $event_text = '<b>'.$act->appTitle.'</b>';
//                        $circle_text = '<b>'.$group_name.'</b>';
                        $titles = CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_GROUP_CREATED_EVENT'));
                        $titles = CString::str_ireplace('{event}', $act->appTitle, $titles);
                        $act->title = ''.CString::str_ireplace('{circle}', $group_name, $titles);
                        break;
                    case 'events.update':       // update event
                        $ObjgroupEventUpdate = new stdClass();
                        $act->type = 'event.update';
                        $ObjgroupEventUpdate->event_title = $act->appTitle;
                        $ObjgroupEventUpdate->event_id = $act->cid;
                        $ObjgroupEventUpdate->groupid = (int)$oRow->groupid;
                        $ObjgroupEventUpdate->group_name = $group_name;
                        $ObjgroupEventUpdate->group_type = 'social_circle';
                        $ObjgroupEventUpdate->course = 0;
                        $ObjgroupEventUpdate->isFacilitator = false;
                        if($iscourse) {
                            $ObjgroupEventUpdate->group_type = 'learning_circle';
                            $ObjgroupEventUpdate->course = $iscourse;
                            $ObjgroupEventUpdate->isFacilitator = $isFacilitator;
                        }
                        $ObjgroupEventUpdate->ismember = (boolean)$groupModel->isMember($userid, $oRow->groupid);
//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
//                        $event_text = '<b>'.$act->appTitle.'</b>';
//                        $circle_text = '<b>'.$group_name.'</b>';
                        $titles = CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_GROUP_UPDATE_EVENT'));
                        $titles = CString::str_ireplace('{event}', $act->appTitle, $titles);
                        $act->title = ''.CString::str_ireplace('{circle}', $group_name, $titles);
                        break;
                    case 'groups.join':       // Join group
                        $ObjgroupJoin = new stdClass();
                        $act->type = 'group.join';
                        $ObjgroupJoin->groupid = (int)$oRow->groupid;
                        $ObjgroupJoin->group_name = $group_name;
                        $ObjgroupJoin->ismember = (boolean)$groupModel->isMember($userid, $oRow->groupid);
                        $ObjgroupJoin->group_type = 'social_circle';
                        $ObjgroupJoin->course = 0;
                        $ObjgroupJoin->isFacilitator = false;
                        if($iscourse) {
                            $ObjgroupJoin->group_type = 'learning_circle';
                            $ObjgroupJoin->course = $iscourse;
                            $ObjgroupJoin->isFacilitator = $isFacilitator;
                        }
//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
//                        $circle_text = '<b>'.$group_name.'</b>';
                        $titles = CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_GROUP_JOIN'));
                        $act->title = ''.CString::str_ireplace('{circle}', $group_name, $titles);
                        break;
                    case 'videos.linking':       // Added video
                        $ObjVideoLink = new stdClass();
                        $act->type = 'videos.linking';
                        $act_video = JTable::getInstance('Video', 'CTable');
                        $act_video->load($act->cid);
//                        $oRow->video = $act->video;
                        $ObjVideoLink->video_url = $act_video->path;
//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
                        $act->title = ''.CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_PROFILE_ADDED_VIDEO'));
                        $ObjVideoLink->message = $oRow->title;
                        break;
                    case 'photos':       // Posted image
                        $ObjPhotos = new stdClass();
                        $act->type = 'photos';

                        $params_photo = json_decode($oRow->params);
                        // Album object
//                        $act_album = JTable::getInstance('Album', 'CTable');
//                        $act_album->load($act->cid);
                        $photo = JTable::getInstance('Photo', 'CTable');
                        $photo->load($params_photo->photoid);
                        $ObjPhotos->photo_url = $root . '/' . $photo->original;

//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
                        $act->title = ''.CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_PROFILE_ADDED_PHOTO'));
                        $ObjPhotos->message = $oRow->title;
                        break;
                    case 'videos':
                        // Album object
                        $ObjVideo = new stdClass();
                        $act->video = JTable::getInstance('Video', 'CTable');
                        $act->video->load($act->cid);
                        $oRow->video = $act->video;
                        break;
                    case 'profile':       // Added video
                        $ObjProfile = new stdClass();
                        $act->type = 'profile.status';
//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
                        $act->title = ''.CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_PROFILE_UPDATE_STATUS'));
                        $ObjProfile->message = $oRow->title;
                        break;
                    case 'friends.connect':       // Added video
                        $ObjFriendConnect = new stdClass();
                        $act->type = 'friends.connect';
//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
//                        $target_text = '<b>'.$act->target_name.'</b>';
                        $titles = CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_FRIEND_CONNECT'));
                        $act->title = ''.CString::str_ireplace('{target}', $act->target_name, $titles);
                        $ObjFriendConnect->title = ''.CString::str_ireplace('{target}', $act->target_name, $titles);
                        break;
                    case 'profile.avatar.upload':   // Update avatar
                        $ObjProfileUploadAvatar = new stdClass();
                        $act->type = 'profile.avatar.upload';
                        $param_pr = json_decode($oRow->params);
                        $ObjProfileUploadAvatar->image_url = $root. '/' .CString::str_ireplace('_stream_', '', $param_pr->attachment);
//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
                        $act->title = ''.CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_PROFILE_UPDATE_AVATAR'));
                        break;
                    case 'cover.upload':    // Update cover user/group
                        $ObjProfileUploadCover = new stdClass();
                        if ($oRow->groupid && $oRow->groupid > 0) {
                            $act->type = 'group.cover.upload';
                        } else {
                            $act->type = 'profile.cover.upload';
                        }
                        $param_pr = json_decode($oRow->params);
                        $ObjProfileUploadCover->image_url = $root. '/' .$param_pr->attachment;
//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
                        if ($oRow->groupid && $oRow->groupid > 0) {
                            $act->title = ''.CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_GROUP_UPDATE_BANNER'));
                        } else {
                            $act->title = ''.CString::str_ireplace('{actor}', $Objactor->actor_name, JText::_('COM_COMMUNITY_NEWS_PROFILE_UPDATE_BANNER'));
                        }
                        break;
                    case 'groups.discussion.reply':
                        $act->type = 'groups.discussion.reply';
                        $ObjDiscussionReply = new stdClass();
                        $act_topic = JTable::getInstance('Discussion', 'CTable');
                        $act_topic->load($act->cid);
                        $ObjDiscussionReply->topic_title = $act_topic->title;
                        $ObjDiscussionReply->topic_id = $act->cid;
                        $ObjDiscussionReply->groupid = (int)$oRow->groupid;
                        $ObjDiscussionReply->group_name = $group_name;
                        $ObjDiscussionReply->ismember = (boolean)$groupModel->isMember($userid, $oRow->groupid);
                        $ObjDiscussionReply->group_type = 'social_circle';
                        $ObjDiscussionReply->course = 0;
                        $ObjDiscussionReply->isFacilitator = false;
                        if($iscourse) {
                            $ObjDiscussionReply->group_type = 'learning_circle';
                            $ObjDiscussionReply->course = $iscourse;
                            $ObjDiscussionReply->isFacilitator = $isFacilitator;
                        }
//                        $actor_text = '<b>'.$Objactor->actor_name.'</b>';
//                        $topic_text = '<b>'.$act_topic->title.'</b>';
                        $titles = CString::str_ireplace('<a href=_QQQ_%1$s_QQQ_>%2$s</a>', $act_topic->title, JText::_('COM_COMMUNITY_GROUPS_REPLY_DISCUSSION'));
                        $act->title = $Objactor->actor_name.$titles;
                        break;
                    default :
                        $act->type = $row->app;
                        break;
                }
                $act->actor = $Objactor;
                $act->group = $Objgroup;
                $act->group_update = $ObjgroupUpdate;
                $act->discussion = $ObjgroupDiscuss;
                $act->discussion_reply = $ObjDiscussionReply;
                $act->announcement = $ObjgroupAnnount;
                $act->events = $ObjgroupEvent;
                $act->events_update = $ObjgroupEventUpdate;
                $act->group_join = $ObjgroupJoin;
                $act->video_link = $ObjVideoLink;
                $act->photos = $ObjPhotos;
                $act->profile_status = $ObjProfile;
                $act->profile_upload_avatar = $ObjProfileUploadAvatar;
                $act->profile_upload_cover = $ObjProfileUploadCover;
                $act->friend_connect = $ObjFriendConnect;
                $act->created = $oRow->created;
//                $act->createdDate = $date->Format(JText::_('DATE_FORMAT_LC2'));
//                $act->createdDateRaw = $oRow->created;
//                $act->app = $oRow->app;
//                $act->eventid = "".$oRow->eventid;
//                $act->groupid = "".$oRow->groupid;
//                $act->group_access = $oRow->group_access;
//                $act->event_access = $oRow->event_access;
                $act->location = $oRow->getLocation();
//                $act->commentCount = $oRow->getCommentCount();
//                $act->commentAllowed = $oRow->allowComment();
//                $act->commentLast = $oRow->getLastComment();
//                $act->commentsAll = "".$oRow->getCommentsAll();
//                $act->likeCount = $oRow->getLikeCount();
//                $act->likeAllowed = $oRow->allowLike();
                $act->isFriend = $user->isFriendWith($act->actor);
                $act->isMyGroup = $user->isInGroup($oRow->groupid);
                $act->isMyEvent = $user->isInEvent($oRow->eventid);
//                $act->userLiked = $oRow->userLiked($user->id);
//                $act->latitude = $oRow->latitude;
//                $act->longitude = $oRow->longitude;

//                $act->params = (!empty($oRow->params)) ? $oRow->params : '';

                // get the content
//                $act->content = $this->getActivityContent($oRow);
                //$act->title		= $this->getActivityTitle($oRow);
//                $act->title = $oRow->title;

                $act->isFeatured = isset($oRow->isFeatured) ? $oRow->isFeatured: false;

                $result[] = $act;
                $i++;
            }
        }

        $data = new StdClass;
        $data->status = true;
        $data->updatedprofile = $updatedProfie;
//        $data->courses = $cursos;
//        $data->circles = $tmpGroups;
        $data->datafeed = $result;
        $data->havefriends = $haveFriends;

        return $data;
    }
    function _appLink($name, $actor = 0, $userid = 0, $title = '') {

        if (empty($name))
            return '';

// 		if( empty($instances[$id.$actor.$userid]) )
// 		{
        $appModel = CFactory::getModel('apps');
        $url = '';

        // @todo: check if this app exist
        if (true) {
            // if no target specified, we use actor
            if ($userid == 0)
                $userid = $actor;

            if ($userid != 0 && $name != 'profile' && $name != 'news_feed' && $name != 'photos' && $name != 'friends'
            ) {

                $url = CUrlHelper::userLink($userid) . '#app-' . $name;
                if ($title == JText::_('COM_COMMUNITY_ACTIVITIES_APPLICATIONS_REMOVED')) {
                    $url = $appModel->getAppTitle($name);
                } else {
                    $url = '<a href="' . $url . '" >' . $appModel->getAppTitle($name) . '</a>';
                }
            } else {
                $url = $appModel->getAppTitle($name);
            }
        }
        return $url;
    }

    public function hideActivity ($activityId, $username) {
        $userid = CUserHelper::getUserId($username);
        if ($userid) {
            $my = CFactory::getUser($userid);
        } else {
            $response['status'] = false;
            $response['message'] = 'Username not found.';
            header('Content-type: application/json; charset=UTF-8');
            header("Status", true, 400);
            echo json_encode($response);
            exit;
        }

        $modal = CFactory::getModel('activities');
        // check exist stream
        $activity = $modal->getActivityStream($activityId);
        if (!empty($activity)) {
            //do hide function
            $modal->hide($my->id, $activityId);

            $response['status'] = true;
            $response['message'] = 'Success';
            $response['activityid'] = $activityId;
        } else {
            $response['status'] = false;
            $response['message'] = 'Not found stream.';
            $response['activityid'] = $activityId;
        }

        return $response;
    }

    public function deleteActivity ($activityId, $username) {
        $response = array();
        $userid = CUserHelper::getUserId($username);
        if ($userid) {
            $my = CFactory::getUser($userid);
        } else {
            $response['status'] = false;
            $response['message'] = 'Username not found.';
            header('Content-type: application/json; charset=UTF-8');
            header("Status", true, 400);
            echo json_encode($response);
            exit;
        }

        $activity = JTable::getInstance('Activity', 'CTable');
        $activity->load($activityId);

        // @todo: do permission checking within the ACL
        if ($my->authorise('community.delete', 'activities.' . $activityId, $activity)) {
            $activity->delete();

            if ($activity->app == 'profile') {
                $model = CFactory::getModel('activities');
                $data = $model->getAppActivities(array('app' => 'profile', 'cid' => $activity->cid, 'limit' => 1));
                $status = CFactory::getModel('status');
                $status->update($activity->cid, $data[0]->title, $activity->access);

                $user = CFactory::getUser($activity->cid);
                $today = JFactory::getDate();
                $user->set('_status', $data[0]->title);
                $user->set('_posted_on', $today->toSql());
            }

            $response = array( 'status' => true, 'message' => 'Success.', 'activityid' => $activityId );

            CUserPoints::assignPoint('wall.remove');

        } else {
            $response = array( 'status' => false, 'message' => 'You don\'t have permission.' );
        }

        return $response;
    }

    public function getCoursePublishCircle($groupid) {

        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');

        if(empty($groupid)) {
            return $response = array(
                'status' => false,
                'message' => 'Circle can not be empty.',
                'courses' => array()
            );
        }

        $group  = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);

        if($group->id == 0) {
            return $response = array(
                'status' => false,
                'message' => 'Circle not found.',
                'courses' => array()
            );
        }

        $groupModel  = CFactory::getModel('groups');
        $courseids = $groupModel->getPublishCoursesToSocial($group->id);
        if(empty($courseids)) {
            return $response = array(
                'status' => false,
                'message' => 'No course found.',
                'courses' => array()
            );
        }
        foreach ($courseids as $courseid) {
            $publishedCourses[] = JoomdleHelperContent::call_method ('get_course_info', (int)$courseid->courseid, '');
        }
        $user = CFactory::getUser();
        $username = $user->get('username');
        // Clean up the string
        foreach ($publishedCourses as $publishedCourses) {
            $publishedCourses['summary'] = strip_tags($publishedCourses['summary']);
            $db = JFactory::getDBO();

            $searchEscaped1 = $db->Quote( "%\"course_id\":" . $db->escape( $publishedCourses['remoteid'], true ) . "}%", false );
            $searchEscaped2 = $db->Quote( "%\"course_id\":" . $db->escape( $publishedCourses['remoteid'], true ) . ",%", false );
            $query = 'SELECT id' .
                ' FROM #__community_groups' .
                " WHERE params LIKE $searchEscaped1 OR params LIKE $searchEscaped2 LIMIT 1";

            $db->setQuery( $query );
            $result = $db->loadResult();
            if($result) {
                $publishedCourses['course_group'] = (int) $result;
            } else {
                $publishedCourses['course_group'] = 0;
            }
            $publishedCourses['isStudent'] = $groupModel->isCourseHas($publishedCourses['remoteid'],  'student', $username);
            $cleanedPublishedCourse[] = $publishedCourses;
        }
        return $response = array(
            'status' => true,
            'message' => 'success',
            'courses' => $cleanedPublishedCourse
        );
    }
    public function getMyLPCircles($userid, $sorted, $page, $limit) {
        require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');
        $groupsModel = CFactory::getModel('groups');
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $lpcircle = 7;
        if ($page && $page > 0) {
            $limitstart = ($page - 1) * $limit;
        } else {
            $limitstart = null;
        }
        if ($page == 0 && $limit == 0) {
            $groups = $groupsModel->getCircles($userid, 0, $limitstart, $limit, $lpcircle);
        } else {
            $groups = $groupsModel->getCircles($userid, 1, $limitstart, $limit, $lpcircle);
        }
        $eventsModel = CFactory::getModel('Events');
        foreach ($groups as $k => $r) {
            $table = JTable::getInstance('Group', 'CTable');
            $table->load($r->id);
            // get category name
            if($r->moodlecategoryid == 0) {unset($groups[$k]); continue;};
            $groups[$k]->avatar = (string) $table->getOriginalAvatar();
            $groups[$k]->lastaccess = (string) $r->lastaccess;
            if ($r->categoryname == '') {
                $category = JTable::getInstance('GroupCategory', 'CTable');
                $category->load($r->categoryid);

                $groups[$k]->categoryname = (string) $category->name;
            }
            // get group admin name
//                    $user_owner = '';
//                    if ($r->ownername == '') {
            $user_owner = CFactory::getUser($r->ownerid);

            $groups[$k]->ownername = (string) $user_owner->getDisplayName();
//                    }

            $groups[$k]->activity_count = My_Groups::groupActivities($r->id);

            // count group event
            $totalEvents = $eventsModel->getTotalGroupEvents($r->id);
            $groups[$k]->eventCount = $totalEvents;

            $groups[$k]->moodlecategoryid = (int) $r->moodlecategoryid;
        }
//        $groups_result = array_values($groups);
        $data = new stdClass();
        if ($groups) {
            $data->status = true;
            $data->message = 'Get My Circle success.';
            $data->circlecount = $groupsModel->getCirclesCount($userid, $lpcircle);
            $data->groups = $groups;
        } else {
            $data->status = false;
            $data->message = 'Get My Circle failed';
            $data->circlecount = 0;
            $data->groups = $groups;
        }
        return $data;
    }
    public function getFriendRequest($username)
    {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $userid = JUserHelper::getUserId($username);
        $my = JFactory::getUser($userid);

        $friendModel = CFactory::getModel('Friends');

        $requested = $friendModel->getPending($my->id);
        $blockModel = CFactory::getModel('block');

        $resultRows = array();
        if($requested) {
            foreach ($requested as $row) {
                $user = CFactory::getUser($row->id);

                $profilemodel = CFactory::getModel('profile');
                $profile = $profilemodel->getViewableProfile($user->id);

                $obj = new stdClass();
                $obj->id = $row->id;
                $obj->username = $row->username;
                $obj->displayname = $user->getDisplayName();
                $avatar = $user->_avatar;
                $cover = $user->_cover;
                if($avatar == '') {
                    $obj->avatar = '';//JURI::base() . 'components/com_community/assets/user-Male-thumb.png'
                } else {
                    $obj->avatar = $root.'/'.$avatar;
                }
                if($cover == '') {
                    $obj->cover = '';//JURI::base() . 'components/com_community/assets/cover-undefined-default.png'
                } else {
                    $obj->cover = $root.'/'.$cover;
                }
                $obj->friendsCount = $user->getFriendCount();
                $obj->profileLink = CUrlHelper::userLink($row->id);
                $obj->isFriend = true;
                $obj->isBlocked = $blockModel->getBlockStatus($user->id, $my->id);
                $obj->roles = array();
                // check user role
                if(!empty($courseid)) {
                    require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');
                    $roles = JoomdleHelperContent::call_method('get_user_role', (int)$courseid, $row->username);
                    $obj->roles = json_decode($roles['roles'][0]['role']);
                }
                if(!empty($categoryid)) {
                    require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');
                    $roles = JoomdleHelperContent::call_method('get_user_category_role', (int)$categoryid, $row->username);
                    $obj->roles = (array)$roles['role'];

                }
                foreach ($profile['fields']['Basic Information'] as $basic) {
                    if($basic['name'] == 'Position') {
                        if(!empty($basic['value'])) {
                            $obj->position = $basic['value'];
                        } else {
                            $obj->position = '';
                        }
                    }
                }
                $resultRows[] = $obj;
            }
        }
        $response['status'] = true;
        $response['message'] = 'Friend loaded';
        $response['data'] = $resultRows;
        return $response;
    }

    public function getAllChats($username)
    {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $data_root = $mainframe->getCfg('dataroot');

        $Dmodel = CFactory::getModel('discussions');
        $userid = JUserHelper::getUserId($username);
        $user = CFactory::getUser($userid);
        $groupModel = CFactory::getModel('groups');
        $my = CFactory::getUser();

        $discussions = $Dmodel->getAllDiscussion($user->id);
        if($discussions) {
            foreach ($discussions as $k =>$v) {
                if(!empty($v->avatar)) {
                    if(strpos($v->avatar, 'http') === false) {
                        $discussions[$k]->avatar = $root.'/'.$v->avatar;
                    } else {
                        $discussions[$k]->avatar = $v->avatar;
                    }
                }
                if(empty($v->cover)) {
                    $discussions[$k]->cover = '';
                } else {
                    $discussions[$k]->cover = $root.'/'.$v->cover;
                }
                $params = json_decode($v->params);
                $courseid = 0;
                if(isset($params->course_id) && $params->course_id > 0) {
                    if(is_object($params->course_id)) {
                        $courseid = $params->course_id->data;
                    } else {
                        $courseid = $params->course_id;
                    }
                }
//                $discussions[$k]->count = (int) $v->count;
                $discussions[$k]->isAdmin = false;
                if($user->id == $v->creator) {
                    $discussions[$k]->isAdmin = true;
                }
                $isMember = $groupModel->isMember((int)$user->id, $v->groupid);
                $discussions[$k]->isMember = false;
                if($isMember) {
                    $discussions[$k]->isMember = true;
                }
                if($courseid) {
                    $discussions[$k]->circle_type = 'course_circle';
                    $discussions[$k]->remoteid= $courseid;
                } else {
                    $discussions[$k]->circle_type = 'social_circle';
                    $discussions[$k]->remoteid = 0;
                }
                /*
                 * count notification
                 */
                $discussions[$k]->count = 0;
                $json_data = json_decode(file_get_contents($data_root . '/chats/'.$discussions[$k]->id.'/data.json', true));
                if($json_data) {
                    foreach ($json_data->data as $js) {
                        if($my->id == $js->userid) {
                            $discussions[$k]->count = $js->count;
                        }
                    }
                }

                /*
                 *  get last reply
                 */
                $lastreplytypequery = $this->getLastReply($discussions[$k]->id);

                if($lastreplytypequery) {
                    $lastreplytype = $lastreplytypequery->params;
                    if(empty($lastreplytype)) {
                        $discussions[$k]->lastreplytype = 'comment';
                        $discussions[$k]->lastmessage = (string) $lastreplytypequery->comment;
                    } else {
                        if(strpos($lastreplytype,'attached_photo_id') !== false) {
                            $discussions[$k]->lastreplytype = 'uploadphoto';
                            $photo_wall = $this->getPhotoUpload((int) json_decode($lastreplytype)->attached_photo_id);
                            $discussions[$k]->lastmessage = (string) basename("$photo_wall->original");
                        }
                        if(strpos($lastreplytype,'attached_file_id') !== false) {
                            $discussions[$k]->lastreplytype = 'uploadfile';
                            $file = $this->getFileUpload((int) json_decode($lastreplytype)->attached_file_id);
                            $discussions[$k]->lastmessage = (string) $file->name;
                        }
                        if (strpos($lastreplytype, 'website') !== false) {
                            $discussions[$k]->lastreplytype = 'comment';
                            $discussions[$k]->lastmessage = json_decode($lastreplytype)->url;
                        }
                    }
                    $discussions[$k]->lastmessageby = (int) $lastreplytypequery->post_by;
                    $lastreplyuser = CFactory::getUser((int) $lastreplytypequery->post_by);
                    if(empty($lastreplyuser->name)) {
                        $lastreplyuser = CFactory::getUser((int) $v->creator);
                    }
                    $discussions[$k]->lastmessagebyname = $lastreplyuser->name;
                    $createdby = ucfirst($lastreplyuser->name);
                    if($v->creator == $my->id) {
                        $createdby = "You";
                    }
                    $discussions[$k]->createdby = (string) 'Created by '.$createdby;
                } else {
                    $lastreplyuser = CFactory::getUser((int) $v->creator);
                    $discussions[$k]->lastmessage = '';
                    $createdby = ucfirst($lastreplyuser->name);
                    if($v->creator == $my->id) {
                        $createdby = "You";
                    }
                    $discussions[$k]->createdby = (string) 'Created by '.$createdby;
                    $discussions[$k]->lastmessageby = (int) $v->creator;
                    $discussions[$k]->lastmessagebyname = $lastreplyuser->name;
                    $discussions[$k]->lastreplytype = 'comment';
                }
            }
            return $response = array(
                'status' => true,
                'message' => 'Success',
                'data' => $discussions
            );
        } else {
            return $response = array(
                'status' => false,
                'message' => 'Failed',
                'data' => array()
            );
        }
    }

    public function getLastReply ($id)
    {
        $db = JFactory::getDbo();
        $query = 'SELECT * FROM '.$db->quoteName('#__community_wall')
            .' WHERE '.$db->quoteName('contentid').'='.$db->quote($id)
            .' ORDER BY '.$db->quoteName('date').' DESC'
            .' LIMIT 0,1';
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public function getLastReplybyID ($id)
    {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $db = JFactory::getDbo();
        $query = 'SELECT * FROM '.$db->quoteName('#__community_wall')
            .' WHERE '.$db->quoteName('id').'='.$db->quote($id);
        $db->setQuery($query);
        $result = $db->loadObject();
        $return = array();
        if($result) {
            $return['status'] = true;
            $return['message'] = 'success.';
            $response = new stdClass();
            $response->id = $result->id;
            $response->contentid = $result->contentid;
            $response->ip = $result->ip;
            $response->comment = $result->comment;
            $response->date = $result->date;
            $response->post_by = $result->post_by;
            $response->published = $result->published;
            $response->params = $result->params;
            $userRep = CFactory::getUser($result->post_by);
            $response->name = $userRep->getDisplayName();
            $avatar = $userRep->_avatar;
            if($avatar) {
                $response->avatar = $root.'/'.$avatar;
            } else {
                $response->avatar = '';
            }
            $response->type = 'comment';
            $response->photo = '';
            $response->filename = '';
            $response->filepath = '';
            if (strpos($result->params, 'attached_photo_id') !== false) {
                $response->type = 'uploadphoto';
                $photo_wall = $this->getPhotoUpload((int) json_decode($result->params)->attached_photo_id);
                $response->photo = $mainframe->getCfg('wwwrootfile') . '/' . $photo_wall->original;
            }
            if (strpos($result->params, 'attached_file_id') !== false) {
                $response->type = 'uploadfile';
                $file = $this->getFileUpload((int) json_decode($result->params)->attached_file_id);
                $response->filename = (string) $file->name;
                $response->filepath = $mainframe->getCfg('wwwrootfile').'/'.$file->filepath;
            }
            if (strpos($result->params, 'website') !== false) {
                $response->type = 'comment';
            }
            $return['content'] = $response;
        } else {
            $return['status'] = false;
            $return['message'] = 'sorry. can not find message.';
            $return['data'] = array();
        }
        return $return;
    }

    public function headerCircle($groupid, $username)
    {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        if(!$group->id) {
            return array(
                'status' => false,
                'message' => 'Circle not found or has been deleted.'
            );
        }

        // get group admin name

        $user_owner = CFactory::getUser($group->ownerid);

        $group->ownername = $user_owner->getDisplayName();
        /*
        if($group->published == 0) {
            return array(
                'status' => false,
                'message' => 'Circle '.$group->name.' has been archived. Please contact '.$group->ownername.' for more information.'
            );
        }*/
//        print_r($group); die;
        $params = $group->getParams();
        $groupModel = CFactory::getModel('groups');
        // get user current
        $my = CFactory::getUser();
        $canCreate = true;
        if($my->_permission) {
            $canCreate = false;
        }

        // Test if the current user is admin
        $isAdmin = $groupModel->isAdmin($my->id, $group->id);
        // update last access group
        $groupModel->memberLastaccessGroup($group->id, $my->id);

        // Test if the current browser is a member of the group
        $isMember = $groupModel->isMember($my->id, $group->id);
        $waitingApproval = false;
        // If I have tried to join this group, but not yet approved, display a notice
        if ($groupModel->isWaitingAuthorization($my->id, $group->id)) {
            $waitingApproval = true;
        }

        // added new field for show and hide course tab
        $isCourseShare = false;
        $courseids = $groupModel->getPublishCoursesToSocial($group->id);

        if($courseids) {
            $isCourseShare = true;
        }

        $group->description = strip_tags($group->description);
        if ($group->avatar == '') {
            $group->avatar = JURI::base() . 'components/com_community/assets/group.png';
        } else {
            $group->avatar = $root . '/' . $group->avatar;
        }
        if (empty($group->cover)) {
            $group->cover = '';
        } else {
            $group->cover = $root . '/' . $group->cover;
        }

        $group->allow_assign_facilitator = false;

        $is_course_group = $groupModel->getIsCourseGroup($group->id);
        $group->course_id = 0;
        $group->isCourseManager = false;
        $group->isCourseFacilitator = false;
        if(isset($is_course_group) && !empty($is_course_group)) {
            $group->course_id = $is_course_group;
            // check course had toggle on facilitator
            require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');
            $course_data = JoomdleHelperContent::call_method('get_course_info', (int)$group->course_id, $my->username);
            if($course_data['facilitatedcourse'] == 1) {
                $group->allow_assign_facilitator = true;
            }
            $userPermission = JoomdleHelperContent::call_method('get_user_role', (int)$group->course_id, $my->username);
            if($userPermission['status']) {
                $roles = $userPermission['roles'];
                foreach ($roles as $role) {
                    foreach (json_decode($role['role']) as $r) {
                        if($r->roleid == 1) {
                            $group->isCourseManager = true;
                        }
                        if($r->roleid == 4) {
                            $group->isCourseFacilitator = true;
                        }
                    }
                }
            }
        }
        if($group->course_id) {
            $group->circle_type = 'course_circle';
        } else {
            $group->circle_type = 'social_circle';
        }
        // parent pravicy
        $group->hasParent = false;
        $group->parentPrivacy = '';
        if($group->parentid) {
            $group->hasParent = true;
            $group->parentPrivacy = $this->checkPrivacyParent($group->parentid);
        }

        // get category name
        if ($group->categoryid == '') {
            $category = JTable::getInstance('GroupCategory', 'CTable');
            $category->load($group->categoryid);

            $group->categoryname = $category->name;
        }

        if($user_owner->_avatar == '') {
            $group->owneravatar = JURI::base() . 'components/com_community/assets/user-Male.png';
        } else {
            $group->owneravatar = $root.'/'.$user_owner->_avatar;
        }
        $group->roles = array();
        if (!empty($group->moodlecategoryid)) {
            require_once(JPATH_ROOT . '/administrator/components/com_joomdle/helpers/content.php');
            $roles = JoomdleHelperContent::call_method('get_user_category_role', (int) $group->moodlecategoryid, $my->username);
            $group->roles = (array) $roles['role'];
        }

        $bulletinModel = CFactory::getModel('bulletins');
        $bulletins = $bulletinModel->getBulletin($group->id);
        // get timezone server
        $serverTimeZone = date_default_timezone_get();
        $serverObjTime = new DateTimeZone($serverTimeZone);
        $sverTime = new DateTime('now', $serverObjTime);
        $serveroffset = $serverObjTime->getOffset($sverTime);
        $lastestBulletins = '';
        if($bulletins) {
            if (isset($bulletins['timezone'])) {
                $userTimeZone = $bulletins['timezone'];
            } else {
                $userTimeZone = date_default_timezone_get();
            }
            $enddate = date('Y-m-d H:i', $bulletins['endtime']);
            $usertimezone = new DateTimeZone($userTimeZone);
            $gmtTimezone = new DateTimeZone('GMT');
            $myDateTime = new DateTime($enddate, $gmtTimezone);
            $offset = $usertimezone->getOffset($myDateTime);

            $timeUnequal = ($serveroffset - $offset);
            $enddateUser = $bulletins['endtime'] + $timeUnequal;
            if($enddateUser > time()) {
                $lastestBulletins =  $bulletins['message'];
            }
        }
        $isSuperAdmin = COwnerHelper::isCommunityAdmin();
        $isOwner = $groupModel->isCreator($my->id, $group->id);
        if ($is_course_group) {
                $canCreateChats = (!$isSuperAdmin && !$isOwner && !$isAdmin) ? $params->get('enableCreateChats', 1) : 1;
            } else $canCreateChats = 1;

        $myGroup = array(
            'status' => true,
            'message' => 'View detail success',
            'groupDetail' => $group,
            'lastAnnouncement' => $lastestBulletins,
            'isAdmin' => $isAdmin,
            'isMember' => $isMember,
            'isCourseShare' => $isCourseShare,
            'canCreate' => $canCreate,
            'waitingApproval' => $waitingApproval,
            'canCreateChats' => $canCreateChats
        );
        return $myGroup;

    }
    public function lisTopicOfCircle($groupid, $username)
    {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $data_root = $mainframe->getCfg('dataroot');

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        if($group->id == 0) {
            return $response = array(
                'status' => false,
                'message' => 'Circle not found.',
                'topics' => array()
            );
        }
        $my = CFactory::getUser();
        $params = $group->getParams();
        $discussModel = CFactory::getModel('discussions');
        $discussions = $discussModel->getDiscussionTopics($group->id, '50', $params->get('discussordering', DISCUSSION_ORDER_BYLASTACTIVITY));
        for ($i = 0; $i < count($discussions); $i++) {
            $row = $discussions[$i];
            $avatar = CFactory::getUser($row->creator)->_avatar;
            if($avatar == '') {
                $row->avatar_creator = JURI::base() . 'components/com_community/assets/user-Male.png';
            } else {
                $row->avatar_creator = $root.'/'.$avatar;
            }
            if($row->avatar == '') {
                $row->avatar = '';
            } else {
                $row->avatar = $root.'/'.$row->avatar;
            }
            if($row->thumb == '') {
                $row->thumb = JURI::base() . 'components/com_community/assets/user-Male.png';
            } else {
                $row->thumb = $root.'/'.$row->thumb;
            }
            /*
             * count notification
             */
            $row->count = 0;
            $json_data = json_decode(file_get_contents($data_root . '/chats/' . $row->id . '/data.json', true));
            if ($json_data) {
                foreach ($json_data->data as $js) {
                    if ($my->id == $js->userid) {
                        $row->count = $js->count;
                    }
                }
            }
            $row->avatar_circle = $group->getAvatar();
            $lastreplytypequery = $this->getLastReply($row->id);

            if($lastreplytypequery) {
                $lastreplytype = $lastreplytypequery->params;
                if(empty($lastreplytype)) {
                    $row->lastreplytype = 'comment';
                    $row->lastmessage = (string) $lastreplytypequery->comment;
                } else {
                    if(strpos($lastreplytype,'attached_photo_id') !== false) {
                        $row->lastreplytype = 'uploadphoto';
                        $photo_wall = $this->getPhotoUpload((int) json_decode($lastreplytype)->attached_photo_id);
                        $row->lastmessage = (string) basename("$photo_wall->original");
                    }
                    if(strpos($lastreplytype,'attached_file_id') !== false) {
                        $row->lastreplytype = 'uploadfile';
                        $file = $this->getFileUpload((int) json_decode($lastreplytype)->attached_file_id);
                        $row->lastmessage = (string) $file->name;
                    }
                    if (strpos($lastreplytype, 'website') !== false) {
                        $row->lastreplytype = 'comment';
                        $row->lastmessage = json_decode($lastreplytype)->url;
                    }
                }
                $row->lastmessageby = (int) $lastreplytypequery->post_by;
                $lastreplyuser = CFactory::getUser((int) $lastreplytypequery->post_by);
                if(empty($lastreplyuser)) {
                    $lastreplyuser = CFactory::getUser((int) $row->creator);
                }
                $row->lastmessagebyname = $lastreplyuser->name;
            } else {
                $row->lastmessage = (string) "";
                $row->lastmessageby = (int) $row->creator;
                $lastreplyuser = CFactory::getUser((int) $row->creator);
                $createname = $lastreplyuser->name;
                if($my->id == $row->creator) {
                    $createname = "Created by You";
                }
                $row->lastmessagebyname = $createname;
                $row->lastreplytype = 'comment';
            }
        }
        $response['status'] = true;
        $response['message'] = 'List topic of cirlce';
        $response['topics'] = $discussions;
        return $response;
    }

    public function getManageMembers($groupid)
    {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $groupModel = CFactory::getModel('groups');


        $approvedMembers = $groupModel->getAdmins($groupid, 0, false);
        CError::assert($approvedMembers, 'array', 'istype', __FILE__, __LINE__);
        // retrict data and get avatar of member
        $model = CFactory::getModel('friends');
        $group = $groupModel->getGroup($groupid);

        require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');

        $my = CFactory::getUser();
        for ($i = 0; $i < count($approvedMembers); $i++) {
            $row = $approvedMembers[$i];

            $user = CFactory::getUser($row->id);
            $isFriend = CFriendsHelper::isConnected($row->id, $my->id);
            $row->isFriend = $isFriend;

            $avatar = $user->_avatar;
            if($avatar == '') {
                $row->avatar = JURI::base() . 'components/com_community/assets/user-Male.png';
            } else {
                $row->avatar = $root.'/'.$avatar;
            }
            $row->manage = $groupModel->isAdmin($row->id, $groupid);

            $isOwner = $groupModel->isCreator($row->id, $groupid);
            $row->owner = $isOwner;

            $connection = $model->getFriendConnection($my->id, $row->id);
            $row->requestMessage = '';
            $row->pendingRequest = false;
            if($connection && !$isFriend) {
                $row->pendingRequest = true;
                $row->requestMessage = $connection[0]->msg;
            }
            $row->myRequest = false;
            if($my->id == $connection[0]->connect_from && !$isFriend) {
                $row->myRequest = true;
            }
            $params = json_decode($group->params);
            if(isset($params->course_id)) {
                $course_role = JoomdleHelperContent::call_method('get_user_role', (int) $params->course_id, $user->username);

                if($course_role['roles']) {
                    foreach ($course_role['roles'] as $cr) {
                        $row->roles_course = json_decode($cr['role']);
                    }
                }
            }
            $roles = array();
            if($group->moodlecategoryid) {
                $roles = JoomdleHelperContent::call_method('get_user_category_role', (int) $group->moodlecategoryid, $user->username);
                $row->roles = (array)$roles['role'];
            } else {
                $row->roles = array();
            }
        }
        return array(
            'status' => true,
            'message' => 'Success',
            'totalManage' => count($approvedMembers),
            'manage' => $approvedMembers,
        );
    }
    private function assignNormalCircleManage($groupid, $members)
    {
        $my = CFactory::getUser();
        $group = JTable::getInstance('Group', 'CTable');
        if(empty($groupid)) {
            return array(
                'status' => false,
                'message' => 'Circle cannot be empty.'
            );
        }
        if(empty($members)) {
            return array(
                'status' => false,
                'message' => 'Please select member to assign.'
            );
        }
        $group->load($groupid);
        if($group->id == 0) {
            return array(
                'status' => false,
                'message' => 'Circle not found.'
            );
        }
        $groupModel = CFactory::getModel('groups');
        $isAdmin = $groupModel->isAdmin($my->id, $group->id);
        if(!$isAdmin) {
            return array(
                'status' => false,
                'message' => 'Your doesn\'t have permission for action.'
            );
        }
        $member_id = explode(',', $members);
        $member = JTable::getInstance('GroupMembers', 'CTable');
        $error = array();
        foreach ($member_id as $key=>$value) {
            $isMember = $groupModel->isMember((int)$value, $group->id);
            if(!$isMember) {
                $error[] = $value;
                continue;
            }
            $keys = array('groupId' => $group->id, 'memberId' => (int)$value);
            $member->load($keys);
            $member->permissions = 1;
            $member->store();
        }
        $stringErr = '';
        if(!empty($error)) {
            $text = 'user';
            $text_count = 'is';
            if(count($error) > 1) {
                $text = 'users';
                $text_count = 'are';
            }
            $stringErr = 'But some '.$text.': ';
            foreach ($error as $e => $val) {
                $userErr = CFactory::getUser((int)$val);
                $stringErr .= $userErr->name .', ';
            }
            $stringErr .= $text_count.' not member of circle.';
        }
        return $response = array(
            'status' => true,
            'message' => 'Member assigned manager successfuly.'.$stringErr
        );
    }
    public function allRequestJoinCircle($groupid, $username, $page, $limit)
    {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        if(empty($groupid)) {
            return array(
                'status' => false,
                'message' => 'Circle ID can not be empty',
                'data' => array()
            );
        }
        $userid = JUserHelper::getUserId($username);
        $user = CFactory::getUser($userid);

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        if($group->id == 0) {
            return array(
                'status' => false,
                'message' => 'Circle not found.',
                'data' => array()
            );
        }
        $groupModel = CFactory::getModel('groups');
        $isAdmin = $groupModel->isAdmin($user->id, $group->id);
        if(!$isAdmin) {
            return array(
                'status' => false,
                'message' => 'Your doesn\'t have permission for action.'
            );
        }
        $requests = $groupModel->allRequestJoin($group->id, $user->id, $page, $limit);

        if($requests) {
            $account_request = array();
            foreach ($requests as $req) {
                $user_requested = CFactory::getUser($req->memberid);
                $row = new stdClass();
                $row->request_name = $user_requested->name;
                $row->request_id = $req->memberid;

                $avatar = $user_requested->_avatar;
                if($avatar == '') {
                    $row->request_avatar = JURI::base() . 'components/com_community/assets/user-Male.png';
                } else {
                    $row->request_avatar = $root.'/'.$avatar;
                }
                $row->request_group = $req->groupid;
                $row->request_approved = $req->approved;
                $row->request_date = $req->date;
                if($req->approved == -1) {
                    $row->request = 'Request remove';
                } else {
                    $row->request = 'Request pendding';
                }
                $account_request[] = $row;
            }
            $response['status'] = true;
            $response['message'] = 'List people requested.';
            $response['data'] = $account_request;
        } else {
            $response['status'] = true;
            $response['message'] = 'Circle doesn\'t have any requested join.';
            $response['data'] = array();
        }
        return $response;
    }
    public function removeRequestJoin($groupid, $userid)
    {
        if(empty($groupid)) {
            return array(
                'status' => false,
                'message' => 'Circle can not be emty.'
            );
        }
        if(empty($userid)) {
            return array(
                'status' => false,
                'message' => 'UserID can not be empty.'
            );
        }

        $user = CFactory::getUser($userid);
        if($user->id == 0) {
            return array(
                'status' => false,
                'message' => 'User not found.'
            );
        }
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        if($group->id == 0) {
            return array(
                'status' => false,
                'message' => 'Circle not found.'
            );
        }
        $groupModel = CFactory::getModel('groups');
        $remove = $groupModel->removeRequest($group->id, $userid);
        if($remove) {
            return array(
                'status' => true,
                'message' => 'Request deleted.',
            );
        } else {
            return array(
                'status' => false,
                'message' => 'Something error when deleting request.',
            );
        }
    }
    public function getCourseCertificate($couresid, $username)
    {
        if(strcmp((JFactory::getLanguage()->getTag()),'vi-VN')==0) {
            $langcurrent = 'vn';
        }
        if(strcmp((JFactory::getLanguage()->getTag()),'en-GB')==0){
            $langcurrent = 'en';

        }
        require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');
        $certificates = JoomdleHelperContent::call_method('get_certificate', (string)$username, (int)$couresid);

        if(empty($certificates['faultCode'])) {
            $reuturn = new stdClass();
            foreach ($certificates as $k => $v) {
                if($v['downloadFiles'] != '') {
                    $reuturn->courseid = $couresid;
                    $reuturn->course_name = $v['coursename'];
                    $reuturn->url = JURI::base() . $langcurrent . '/certificate/' . $couresid . '.html';
                    $reuturn->url_image = JFactory::getApplication()->getCfg('wwwrootfile').'/images/certificates/'.md5($username . $couresid) . '.jpg?_=' . time();
                    $reuturn->downloadFiles = $v['downloadFiles'];
                }
            }
            return array(
                'status' => true,
                'message' => 'success',
                'data' => $reuturn,
            );
        } else {
            return array(
                'status' => false,
                'message' => 'Certificate not found',
                'data' => array(),
            );
        }

    }

    private function getCircleCoursePublished($courseid)
    {
        $db = JFactory::getDbo();
        $query = 'SELECT a.groupid, b.name FROM '.$db->quoteName('#__course_shareto_group').' AS a'
            . ' JOIN '.$db->quoteName('#__community_groups').' AS b ON a.groupid = b.id'
            . ' WHERE '.$db->quoteName('courseid').'='.$db->quote($courseid);
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
    }

    public function circleTreeData($parentid) {

        if(empty($parentid)) {
            return array(
                'status' => false,
                'message' => 'parentid can not be empty.'
            );
        }
        if(!is_numeric($parentid)) {
            return array(
                'status' => false,
                'message' => 'parentid must numeric.'
            );
        }

        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $user = CFactory::getUser();
        $groupModel = CFactory::getModel('groups');

        $parentCircle = (array) $groupModel->getGrandFatherCircle($parentid, $user->id);
        $currentCircle = (array) $parentCircle['currentCircle'];
        $grandCircle = (array) $parentCircle['parentCircle'];

//        $default_avatar = JURI::base() . 'components/com_community/assets/group.png';
        
        if(!empty($grandCircle)) {
            if(empty($grandCircle['origimage'])) {
                if(!empty($grandCircle['avatar'])) {
                $grandCircle['avatar'] =  $root.'/'.$grandCircle['avatar'];
            } else {
                    $grandCircle['avatar'] = '';
                }
            } else {
                $grandCircle['avatar'] =  $root.'/images/avatar/group/'.$grandCircle['origimage'];
            }
        }

        if(empty($currentCircle['origimage'])) {
            $currentCircle['avatar'] =  $root.'/'.$currentCircle['avatar'];
        } else {
            $currentCircle['avatar'] =  $root.'/images/avatar/group/'.$currentCircle['origimage'];
        }
        $currentCircle['haveSibling'] = false;
        if($parentCircle['haveSibling'] > 0) {
            $currentCircle['haveSibling'] = true;
        }

        $treedata = $groupModel->circleBLNTree($parentid, $user->id);
        foreach($treedata as $tree) {
            if (!empty($tree->origimage)) {
                $tree->avatar = $root . '/images/avatar/group/' . $tree->origimage;
            } else {
                $tree->avatar = (string) '';
            }
        }
        /*
         *  Current not use, maybe use after. DO NOT REMOVE!!
         *
        $data = array();

        if(!empty($treedata)) {
            foreach($treedata as $tree) {
                    if(!empty($tree->origimage)) {
                        $tree->avatar = $root .'/images/avatar/group/'.$tree->origimage;
                    } else {
                        $tree->avatar = (string)'';
                    }
                    $childs = $groupModel->circleBLNTree($tree->id, $user->id);
                    if($childs) {
                        foreach ($childs as $child) {
                            if(empty($child->avatar) && empty($child->origimage)){
                                $child->avatar = '';
                            }
                            if(!empty($child->avatar) && empty($child->origimage)) {
                                $child->avatar = $root.'/'.$child->avatar;
                            }
                            if(!empty($child->origimage)) {
                                $child->avatar = $root .'/images/avatar/group/'.$child->origimage;
                            }
                        }
                        $tree->children = $childs;
                    }
                    $data[] = $tree;
            }
            $parentCircle['childs'] = $data;
        }*/
        $currentCircle['childs'] = $treedata;
        $grandCircle['you_are_here'] = $currentCircle;
        return $grandCircle;
    }

    public function checkPrivacyParent($id)
    {
        $db = JFactory::getDbo();
        $query = 'SELECT approvals, unlisted FROM '.$db->quoteName('#__community_groups')
            . ' WHERE '.$db->quoteName('id').'='.$db->quote($id);
        $db->setQuery($query);
        $result = $db->loadObject();
        if($result) {
            if($result->approvals == 0 && $result->unlisted == 0) {
                $privacy = 'open';
            }
            elseif($result->approvals == 1 && $result->unlisted == 0) {
                $privacy = 'private';
            }
            elseif($result->approvals == 1 && $result->unlisted == 1) {
                $privacy = 'close';
            }
        }
        return $privacy;
    }

    public function archiveCircle($id, $type) {
        // valid input
        if(!$id) {
            return array(
                'status' => false,
                'message' => 'Circle ID can not be empty.'
            );
        }
        if(!in_array($type, array('archive', 'unarchive'))) {
            return array(
                'status' => false,
                'message' => 'Type is undifined'
            );
        }
        $user = CFactory::getUser();
        $model = CFactory::getModel('groups');
        $groupParent = JTable::getInstance('Group', 'CTable');
        $groupParent->load($id);

        $isOwner = $model->isCreator($user->id, $id);
        if(!$isOwner) {
            return array(
                'status' => false,
                'message' => 'You don\'nt have a permission for achive circle.'
            );
        }
        $sub = $model->checkSubgroup($id);
        if ($sub) {
            return array(
                'status' => false,
                'message' => 'You cannot archive this circle as it has sub-circles.'
            );
        }
        $courses = $model->getPublishCoursesToSocial($id);

        if($courses) {
            foreach ($courses as $course)
                $coursecircleid = $model->getLearningCircleId($course->courseid);

            // Archive all courses circle published to circle
            $groupchild = JTable::getInstance('Group', 'CTable');
            $groupchild->load($coursecircleid);
            if($type == "archive"){
                if($groupchild->published == 1)
                    $groupchild->published = 0;
                $groupchild->store();
                $r = JoomdleHelperContent::call_method('course_enable_self_enrolment', (int)$course->courseid, 'unenable', $user->username);
            } else {
                if($groupchild->published == 0)
                    $groupchild->published = 1;
                $groupchild->store();
                $r = JoomdleHelperContent::call_method('course_enable_self_enrolment', (int)$course->courseid, 'enable', $user->username);
            }
        }

        // Archive circle parent
        if($groupParent->published == 1)
        {
            $groupParent->published = '0';
            $msg = JText::_('COM_COMMUNITY_GROUPS_UNPUBLISH_SUCCESS');
        }
        else
        {
            $groupParent->published = 1;
            $msg = JText::_('COM_COMMUNITY_GROUPS_PUBLISH_SUCCESS');
        }
        $groupParent->store();
        
        // Send Push notification
        // get member of circle
        $members = $model->getMembers($groupParent->id, 0, true, false, false);
        $mems = array();
        foreach ($members as $mem) {
            $mems[] = $mem->id;
        }
        $member_notifi = array_unique($mems);
        $key_mem = array_search($user->id, $member_notifi);
        if ($key_mem !== false && isset($member_notifi[$key_mem])) {
            unset($member_notifi[$key_mem]);
        }
        $user_owner = CFactory::getUser($groupParent->ownerid);

        $ownername = $user_owner->getDisplayName();
        
        if($type == 'archive') {
            $type_ar = 'archive_circle';
            $mtitle = 'Circle '.$groupParent->name.' has been archived, please contact to '.$ownername.' for more information.';
        } else {
            $type_ar = 'unarchive_circle';
            $mtitle = 'Circle '.$groupParent->name.' has been uarchived.';
        }
        $message = array(
            'mtitle' => $mtitle,
            'mdesc' => $mtitle
        );
        $payload = $this->pushNotification($message, $member_notifi, $groupParent->id, $groupParent->id, $type_ar, '');
        $response['status'] = true;
        $response['message'] = $msg;
        $response['push_data'] = $payload['android'];
        $response['push_ios'] = $payload['ios'];
        return $response;
    }

    public function getCircleArchive($userid, $page, $limit) {
        require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');
        $groupsModel = CFactory::getModel('groups');
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');

        $socialcircle = 0;
        if ($page && $page > 0) {
            $limitstart = ($page - 1) * $limit;
        } else {
            $limitstart = null;
        }
        $my = CFactory::getUser($userid);
        $canCreate = true;
        if($my->_permission) {
            $canCreate = false;
        }
        
        $groups = $groupsModel->getCircles($userid, 1, $limitstart, $limit, $socialcircle, 'archived', false);

        $eventsModel = CFactory::getModel('Events');
        foreach ($groups as $k => $r) {
            $table = JTable::getInstance('Group', 'CTable');
            $table->load($r->id);

            $groups[$k]->id = $r->id;
            $groups[$k]->ownerid = $r->ownerid;
            $groups[$k]->name = $r->name;
            $groups[$k]->description = $r->description;
            $groups[$k]->summary = $r->summary;
            $groups[$k]->created = $r->created;

            if ($r->avatar == '') {
                $groups[$k]->avatar = '';
            } else {
                $groups[$k]->avatar = $table->getOriginalAvatar();
            }

            $groups[$k]->membercount = $r->membercount;
            $groups[$k]->discusscount = $r->discusscount;
            $groups[$k]->params = $r->params;
            $groups[$k]->lastaccess = (string) $r->lastaccess;
            $groups[$k]->parentid = $r->parentid;
            $groups[$k]->unlisted = $r->unlisted;
            $groups[$k]->approvals = $r->approvals;

            $isMember = $groupsModel->isMember($userid, $r->id);
            $groups[$k]->ismemberin = $isMember;
            // get category name
            if ($r->categoryname == '') {
                $category = JTable::getInstance('GroupCategory', 'CTable');
                $category->load($r->categoryid);

                $groups[$k]->categoryname = (string)$category->name;
            }

            $group->approvals = $r->approvals;
            // get group admin name
            if ($r->ownerid > 0) {
                $user_owner = CFactory::getUser($r->ownerid);

                $groups[$k]->ownername = $user_owner->getDisplayName();
            }

            $groups[$k]->activity_count = My_Groups::groupActivities($r->id);
            // count group event
            $totalEvents = $eventsModel->getTotalGroupEvents($r->id);
            $groups[$k]->eventCount = $totalEvents;
            $groups[$k]->isFacilitator = (boolean)false;
            $groups[$k]->group_type = 'social_circle';
            $groups[$k]->remoteid = 0;
        }
//        $groups_result = array_values($groups);
//        $groups = $this->getGroup($userid, $sorted, 0, $page, $limit);
        $data = new stdClass();
        if ($groups) {
            $data->status = true;
            $data->message = 'Archive circle is loaded.';
            $data->canCreate = $canCreate;
            $data->circlecount = $groupsModel->getCirclesCount($userid, $socialcircle,'archive');
            $data->groups = $groups;
        } else {
            $data->status = false;
            $data->message = 'Archive circle is not loaded';
            $data->canCreate = $canCreate;
            $data->circlecount = 0;
            $data->groups = array();
        }
        return $data;
    }
    public function removeCircle($id) {
        if(!$id) {
            return array(
                'status' => false,
                'message' => 'Circle ID can not be empty.'
            );
        }
        $user = CFactory::getUser();
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        $groupsModel = CFactory::getModel('groups');
        $sub = $groupsModel->checkSubgroup($id);
        if ($sub) {
            return array(
                'status' => false,
                'message' => 'You cannot remove this circle as it has sub-circles.'
            );
        }
        $courses = $groupsModel->getPublishCoursesToSocial($id);

        foreach($courses as $course){
            $response = $groupsModel->UnpublishCourseFromSocial($course->courseid, $id,true);

            if($response){
                $content = "Group successfully removed";
                if(count($groupsModel->isCourseExistFromShareGroup($course->courseid)) == 0){
                    JoomdleHelperContent::call_method('course_enable_self_enrolment', (int)$course->courseid, 'unenable', $user->username);
                }
            }
        }
        
        // Pushed to notification
        $members = $groupsModel->getMembers($id, 0, true, false, false);
        $mems = array();
        foreach ($members as $mem) {
                $mems[] = $mem->id;
        }
        $member_notifi = array_unique($mems);
        $key_mem = array_search($user->id, $member_notifi);
        if ($key_mem !== false && isset($member_notifi[$key_mem])) {
            unset($member_notifi[$key_mem]);
        }
                $message = array(
                    'mtitle' => 'The circle has been removed!',
            'mdesc' => 'The circle has been removed, please contact to administrator for more information!'
                );
        $payload = $this->pushNotification($message, $member_notifi, $id, $id, 'delete_circle', '');
        
        CommunityModelGroups::deleteGroupMembers($id);
        CommunityModelGroups::deleteGroupBulletins($id);
        CommunityModelGroups::deleteGroupDiscussions($id);
        CommunityModelGroups::deleteGroupMedia($id);
        CommunityModelGroups::deleteGroupWall($id);
        CommunityModelGroups::deleteGroupEvents($id);

        // Delete group
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($id);
        $groupData = $group;

        if ($group->delete($id)) {
            //CFactory::load( 'libraries' , 'featured' );
            $featured = new CFeatured(FEATURED_GROUPS);
            $featured->delete($id);

            $content = JText::_('COM_COMMUNITY_GROUPS_DELETED');

            //trigger for onGroupDelete
            $this->triggerGroupEvents('onAfterGroupDelete', $groupData);
            $status = true;
        } else {
            $content = JText::_('COM_COMMUNITY_GROUPS_DELETE_ERROR');
            $status = false;
        }
        $response['status'] = $status;
        $response['message'] = $content;
        $response['push_data'] = $payload['android'];
        $response['push_ios'] = $payload['ios'];
        return $response;
    }

    public function searchCourses($keyword, $page, $limit) {

        if(empty($keyword)) {
            return array(
                'status' => false,
                'mesage' => 'Please fill out Search box.'
            );
        }
        $user = CFactory::getUser();
        $username = $user->username;

        $groupModel = CFactory::getModel('groups');

        $listCoursesSearch = $groupModel->getCoursesForSearch($user->id);
        $courses = array();
        $listgroups = array();
        if ($listCoursesSearch) {
            foreach ($listCoursesSearch as $course) {
                $courses[] = $course->courseid;
                if (!in_array($course->groupid, $listgroups)) {
                    $listgroups[] = $course->groupid;
                }
            }
        }
        if ($page && $page > 0) {
            $limitstart = ($page - 1) * $limit;
        } else {
            $limitstart = 0;
        }
        $listcourses = "";
        if (count($courses) > 0) {
            $listcourses = implode(',', $courses);
        }
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        $list_search_course = JoomdleHelperContent::call_method('search_courses', $keyword, 'created', $listcourses, $username, (int)$limit, (int)$limitstart);

        $results = array();
        if($list_search_course["total_course"]) {
            foreach ($list_search_course["courses"] as $curso) {
                $list_search = new stdClass();
                $groups = $groupModel->getShareGroups($curso['remoteid']);
                $course_id = $curso['remoteid'];
                $ownerid = JUserHelper::getUserId($curso['creator']);
                $owner = CFactory::getUser($ownerid);
                $grid = $groups[0]->groupid;
                $table =  JTable::getInstance( 'Group' , 'CTable' );
                $table->load($grid);

                $isMember = $groupModel->isMember($user->id, $grid);
                $list_search->remoteid = $course_id;
                $list_search->fullname = $curso['fullname'];
                $list_search->isLearner = $curso['isLearner'];
                $list_search->enroled = $curso['enroled'] ? true : false;
                $list_search->course_img = $curso['courseimg'].'?'.(time()*1000);
                $list_search->course_url = JURI::base() . 'course/' . $course_id . '.html';
                $list_search->course_des = $curso['summary'];
                $list_search->learningoutcomes = $curso['learningoutcomes'];
                $list_search->targetaudience = $curso['targetaudience'];
                $list_search->duration = ($curso['duration']/3600) . ' hours';
                $list_search->owner_img = $owner->getAvatar();
                $list_search->owner_name = $owner->getDisplayName();
                $list_search->circleid = $grid;
                $list_search->circle_name = $table->name;
                $list_search->circle_url = CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $grid);
                $list_search->circle_img = $table->getAvatar();
                $list_search->ismembercircle = ($isMember > 0) ? true : false;
                $results[] = $list_search;
            }
        }
        if($list_search_course["total_course"] > 0) {
            return array(
                'status' => true,
                'message' => 'Courses loaded.',
                'total' => $list_search_course["total_course"],
                'data' => $results
            );
        }
        else {
            return array(
                'status' => false,
                'message' => 'No results found.',
                'total' => $list_search_course["total_course"],
                'data' => $results
            );
        }
    }

    public function loginTemplate() {
        $model = CFactory::getModel('theme');
        $writeup = $model->blnInfo->bln_writeup;
        $banners = $model->getBLNBanners(false);
        $logo = $model->getBLNLogo();
        $results = array();
        $results['logo'] = $logo;
        if($banners) {
            $results['banners'] = $banners;
        }
        if(empty($writeup)) {
            $results['writeup'] = JText::_('COM_COMMUNITY_MLP_WRITE_UP');
        } else {
//            $results['writeup'] = nl2br($writeup);
            $results['writeup'] = $writeup;
        }
        return array(
            'status' => true,
            'message' => 'Template loaded.',
            'data' => $results
        );
    }
}
