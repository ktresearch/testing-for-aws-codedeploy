<?php

/**
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Joomla Authentication plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	Authentication.joomla
 * @since 1.5
 */
class plgCommunityGroupsapi extends JPlugin {

    /**
     * This method should handle any authentication and report back to the subject
     *
     * @access	public
     * @param	array	Array holding the user credentials
     * @param	array	Array of extra options
     * @param	object	Authentication response object
     * @return	boolean
     * @since 1.5
     */
    function plgCommunityGroupsapi(&$subject, $params) {
        parent::__construct($subject, $params);
        $this->loadLanguage();
    }

    function onCommunityAPI(&$type, &$resource, &$response, &$sorting, &$action) {
        $my = CFactory::getUser();
        $jinput = JFactory::getApplication()->input;
        $response = array();
        if ($my->id != 0) {
            if ($resource == 'groups' && $type == 'api') { //begin groups
                // list all groups
                if ($action == 'allgroups') {

                    $categoryId = JRequest::getInt('categoryid', 0);
                    $limit = JRequest::getInt('limit', 0);
                    $page = JRequest::getInt('page', 0);
                    $category = JTable::getInstance('GroupCategory', 'CTable');
                    $category->load($categoryId);
                    $response = $this->getAllGroupsApi($categoryId, $sorting, $limit, $page);
                }
                // list my groups
                else if ($action == 'mygroups') {
                    $response = $this->getMyGroups($my->id, $sorting);
                }
                // list sub-circle (child groups)
                elseif($action == 'list_subcircle') {
                    $parentid = $jinput->get('parentid', 0, 'GET');
                    $response = $this->listSubCircle($parentid);
                }
                // data home page
                else if ($action == 'homepage') {
                    $username = JRequest::getCmd('username', '', 'GET');
                    $response = $this->getDataHomePage($username);
                }
                // search group with keyword or category
                else if ($action == 'search') {

                    $search = JRequest::getCmd('keyword', '', 'GET');
                    $search_type = JRequest::getCmd('search_type', '', 'GET');
                    $search_page = JRequest::getCmd('page', '', 'GET');
                    $limit = JRequest::getCmd('limit', '', 'GET');
                    
                    $response = $this->searchGroups($search, $search_type, $my->id, $search_page, $limit);
                }
                // view a group pre-joining with group id
                else if ($action == 'viewgroup_pre_joining') {
                    $groupId = JRequest::getCmd('groupid', 0, 'GET');
                    // check group public or not
                    $response = $this->viewGruopPre($groupId);
                }
                // Request join to group
                else if ($action == 'join_group') {
                    $groupId = JRequest::getCmd('groupid', 0, 'POST');
                    $response = $this->joinGroup($groupId);
                }
                // view my group
                elseif ($action == 'viewmygroup') {

                    $groupid = JRequest::getCmd('groupid', 0, 'GET');
                    $response = $this->viewMyGroup($groupid);
                }
                // Create new circle
                elseif($action == 'newcircle') {
                    $title = $jinput->get('title', '', 'POST');
                    $about = $jinput->get('about', '', 'POST');
                    $keywords = $jinput->get('keywords', '', 'POST');
                    $privacy_public = $jinput->get('privacy_public', 0, 'POST');
                    $privacy_private = $jinput->get('privacy_private', 0, 'POST');
                    $privacy_cecret = $jinput->get('privacy_private', 0, 'POST');
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $parentid = $jinput->get('parentid', 0, 'POST');
                    $categoryid = 6;
                    $response = $this->createCircle($title, $about, $keywords, $privacy_public, $privacy_private, $groupid, $categoryid, $email = '', $parentid, $privacy_cecret);
                }
                
                // List my group members
                elseif ($action == 'group_member') {

                    $groupid = JRequest::getCmd('groupid', 0, 'GET');
                    $response = $this->getMembersGroup($groupid);
                }
                // view discussion
                elseif ($action == 'view_discussion') {
                    $groupid = JRequest::getCmd('groupid', 0, 'GET');
                    $response = $this->viewGroupDiscussion($groupid);
                }
                // post reply discussion
                elseif ($action == 'post_reply_discussion') {
                    $discussion_id = $jinput->get('discussion_id', 0, 'POST');
                    $content = $jinput->get('content', '', 'POST');
                    $type_reply = $jinput->get('type_reply', '', 'POST');
                    
                    $upload = false;
                    $platform = '';
                    if($jinput->get('platform', '', 'POST')) {
                        $platform = $jinput->get('platform', '', 'POST');
                        $content = $jinput->files->get('content');
                        $upload = true;
                    }
                    if(!$content) {
                        $content = $jinput->files->get('content');
                        $upload = true;
                    }
                    $response = $this->postReplyDiscussion($discussion_id, $content, $upload, $type_reply, $platform);
                }
                // view my group update
                elseif ($action == 'mygroupupdate') {
                    $userid = JRequest::getCmd('userid', '', 'GET');
                    $response = $this->getViewGroupUpdate($userid);
                }
                // view viewbulletins
                elseif ($action == 'viewbulletins') {
                    $groupid = JRequest::getCmd('groupid', '', 'GET');
                    $response = $this->getViewBulletins($groupid);
                }
                // add discussions
                elseif ($action == 'add_discussion') {
                    $groupid = JRequest::getCmd('groupid', '', 'POST');
                    $title = JRequest::getString('title', '', 'POST');
                    $content = JRequest::getString('content', '', 'POST');
                    $notify_me = JRequest::getString('notify_me', 0, 'POST');
                    $topicid = JRequest::getString('topicid', 0, 'POST');
                    $response = $this->saveDiscussionGroup($groupid, $title, $content, $notify_me, $topicid);
                }
                // get notifications
                elseif ($action == 'notification') {
                    $response = $this->getNotification();
                }
                // count notifications
                elseif ($action == 'count_notification') {
                    $response = $this->countNotification();
                }
                // read all notification
                elseif($action == 'readAllNotification') {
                    $response = $this->readAllNotification();
                }
                // read only once notification
                elseif($action == 'readNotification') {
                    $notification_id = $jinput->get('notification', 0, 'POST');
                    $response = $this->readNotification($notification_id);
                }
                
                // leaved group
                elseif ($action == 'leavegroup') {
                    $groupid = JRequest::getCmd('groupid', 0, 'POST');
                    $response = $this->leaveGroupapi($groupid);
                }
                // view discussion
                elseif ($action == 'viewtopicdiscussion') {
                    $groupid = JRequest::getCmd('groupid', 0, 'GET');
                    $topicid = JRequest::getCmd('topicid', 0, 'GET');
                    $response = $this->viewDiscussionTopic($groupid, $topicid);
                }
                // delete topic
                elseif($action == 'deletetopic') {
                    $topicid = $jinput->get('topicid', 0, 'POST');
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $response = $this->deleteTopic($topicid, $groupid);
                }
                // add member
                elseif($action == 'addmember') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $member = $jinput->get('member', '', 'POST');
                    $response = $this->addMember($groupid, $member);
                }
                // make circle admin
                elseif($action == 'make_revert_circleadmin') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $memberid = $jinput->get('memberid', 0, 'POST');
                    $type = $jinput->get('type', '', 'POST');
                    $response = $this->makeORrevertAdminCircle($groupid, $memberid, $type);
                }
                // remove member
                elseif($action == 'remove_member') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $memberid = $jinput->get('memberid', 0, 'POST');
                    $ban = $jinput->get('ban', 0, 'POST');
                    $response = $this->removeMemberCircle($groupid, $memberid, $ban);
                }
                // ban member
                elseif($action == 'ban_unban_member') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $memberid = $jinput->get('memberid', 0, 'POST');
                    $type = $jinput->get('type', 0, 'POST');
                    $response = $this->banUnbanMember($groupid, $memberid, $type);
                }
                
                // lock topic
                elseif($action == 'locktopic') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $topicid = $jinput->get('topicid', 0, 'POST');
                    $status = $jinput->get('status', 0, 'POST');
                    $response = $this->lockTopic($groupid, $topicid, $status);
                }
                // load all friend not in circle
                elseif($action == 'loadfriend') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $response = $this->loadFriend($groupid);
                }
                // event api
                elseif($action == 'eventview') {
                    $goupid = $jinput->get('groupid', 0, 'GET');
                    $eventid = $jinput->get('eventid', 0, 'GET');
                    $response = $this->ViewEvent($groupid, $eventid);
                }
                // new event
                elseif($action == 'newevent') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $title = $jinput->get('title', '', 'POST');
                    $about_event = $jinput->get('about', '', 'POST');
                    $startdate = $jinput->get('startdate', '', 'POST');
                    $starttime = $jinput->get('starttime', '', 'POST');
                    $enddate = $jinput->get('enddate', '', 'POST');
                    $endtime = $jinput->get('endtime', '', 'POST');
                    $location = $jinput->get('location', '', 'POST');
                    $eventid = $jinput->get('eventid', 0, 'POST');
                    $response = $this->addEvent($groupid, $title, $about_event, $startdate, $starttime, $enddate, $endtime, $location, $eventid);
                }
                // delete event
                elseif($action == 'delete_event') {
                    $eventid = $jinput->get('eventid', 0, 'POST');
                    $response = $this->deleteEvent($eventid);
                }
                // upload avatar
                // 1 - event avatar; 2 - profile avatar; 3 - group avatar
                elseif($action == 'avatar_photo') {
                    $id = $jinput->get('id', 0, 'POST');
                    $photo = $jinput->files->get('photo');
                    $type = $jinput->get('type', '', 'POST');
                    $response = $this->uploadAvatar($id, $photo, $type);
                }
                // upload cover photo
                elseif($action == 'cover_photo') {
                    $id = $jinput->get('id', 0, 'POST');
                    $cover = $jinput->files->get('cover');
                    $type = $jinput->get('type', '', 'POST');
                    $response = $this->uploadCover($id, $cover, $type);
                }
                // Going event
                elseif($action == 'going_event') {
                    $id = $jinput->get('id', 0, 'POST');
                    $username = $jinput->get('username', '', 'POST');
                    $status = $jinput->get('status', 0, 'POST');
                    $response = $this->goingEvent($id, $username, $status);
                }
                // My event
                elseif($action == 'myevent') {
                    $response = $this->myEvent();
                }
                // List Friend
                elseif($action == 'myfriend') {
                    $courseid = $jinput->get('course', 0, 'GET');
                    $response = $this->myFriends($courseid);
                }
                // List all friend
                elseif($action == 'allfriend') {
                    $limit = $jinput->get('limit', 0, 'POST');
                    $page = $jinput->get('page', 0, 'POST');
                    $response = $this->allFriend($limit, $page);
                }
                // Search People
                elseif($action == 'searchfriend') {
                    $keyword = $jinput->get('keyword', '', 'POST');
                    $type_search = $jinput->get('type_search', '', 'POST');
                    $page = $jinput->get('page', '', 'POST');
                    $limit = $jinput->get('limit', '', 'POST');
                    $response = $this->searchFriend($keyword, $type_search, $page, $limit);
                }
                // add Friend
                elseif($action == 'addfriend') {
                    $userid = $jinput->get('userid', 0, 'POST');
                    $message = $jinput->get('message', '', 'POST');
                    $response = $this->addFriend($userid, $message);
                }
                // approve and reject friend
                elseif($action == 'approvedfriend') {
                    $friendid = $jinput->get('friendid', 0, 'POST');
                    $type_request = $jinput->get('type_request', '', 'POST');
                    $response = $this->approvedFriend($friendid, $type_request);
                }
                // block friend
                elseif($action == 'removefriend') {
                    $friendid = $jinput->get('friendid', 0, 'POST');
                    $block  = $jinput->get('block', 0, 'POST');
                    $response = $this->removeFriend($friendid, $block);
                }
                // Announcement
                elseif($action == 'addAnnouncement') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $title = $jinput->get('title', '', 'POST');
                    $message = $jinput->get('message', '', 'POST');
                    $enddate = $jinput->get('enddate', '', 'POST');
                    $endtime = $jinput->get('endtime', '', 'POST');
                    $announcementid = $jinput->get('announcementid', 0, 'POST');
                    $response = $this->addAnnouncement($groupid, $title, $message, $enddate, $endtime, $announcementid);
                }
                // Delete announcement
                elseif($action == 'deleteAnnouncement') {
                    $groupid = $jinput->get('groupid', 0, 'POST');
                    $announcementid = $jinput->get('announcementid', 0, 'POST');
                    $response = $this->deleteAnnouncement($groupid, $announcementid);
                }
                
                // Part 2: Learning Provider
                elseif($action == 'lp_register') {
                    $lp_name = $jinput->get('lp_name', '', 'POST');
                    $lp_keyword = $jinput->get('lp_keyword', '', 'POST');
                    $lp_organisation = $jinput->get('lp_organisation', '', 'POST');
                    $lp_yourname = $jinput->get('lp_yourname', '', 'POST');
                    $lp_description = $jinput->get('lp_description', '', 'POST');
                    $lp_email_add = $jinput->get('lp_email_add', '', 'POST');
                    $lp_home_add = $jinput->get('lp_home_add', '', 'POST');
                    $lp_postal_code = $jinput->get('lp_postal_code', '', 'POST');
                    $lp_id = $jinput->get('lp_id', 0, 'POST');
                    $response = $this->LP_register($lp_name, $lp_keyword, $lp_organisation, $lp_yourname, $lp_description, $lp_email_add, $lp_home_add, $lp_postal_code, $lp_id);
                }
                elseif($action == 'lp_list') {
                    $limit = $jinput->get('limit', 0, 'POST');
                    $page = $jinput->get('page', 0, 'POST');
                    $response = $this->LP_list($limit, $page);
                }
                elseif($action == 'lp_about') {
                    $lp_circle_id = $jinput->get('lp_circle_id', 0, 'POST');
                    $response = $this->LP_about($lp_circle_id);
                }
                // nothing action
                else {
                    $response['status'] = false;
                    $response['error_message'] = 'system can not be found action';
                }
            }
            // view profiles members
            else if ($resource == 'profile' && $type == 'api') { // begin view profile members
                if ($action == 'viewprofile') {
                    $userid = JRequest::getCmd('userid', 0, 'GET');
                    $response = $this->getProfile($userid);
                }
                // edit profile
                elseif ($action == 'update_profile') {
                    $userid = $jinput->get('userid', 0, 'POST');
                    $fullname = $jinput->get('fullname', '', 'POST');
                    $aboutme = $jinput->get('aboutme', '', 'POST');
                    $position = $jinput->get('position', '', 'POST');
                    $interests = $jinput->get('interests', '', 'POST');
                    $response = $this->updateProfile($userid,$fullname, $aboutme, $position, $interests);
                }
                // edit account
                elseif($action == 'update_account') {
                    $userid = $jinput->get('userid', 0, 'POST');
                    $fullname = $jinput->get('fullname', '', 'POST');
                    $email = $jinput->get('email', '', 'POST');
                    $old_pwd = $jinput->get('old_password', '', 'POST');
                    $new_pwd = $jinput->get('new_password', '', 'POST');
                    $renew_pwd = $jinput->get('renew_password', '', 'POST');
                    $number = $jinput->get('number', '', 'POST');
                    $address = $jinput->get('address', '', 'POST');
                    $postcode = $jinput->get('postcode', '', 'POST');
                    $response = $this->updateAccount($userid, $fullname, $email, $old_pwd, $new_pwd, $renew_pwd, $number, $address, $postcode);
                }
                elseif($action == 'listPreferences') {
                    $userid = $jinput->get('userid', 0, 'POST');
                    $response = $this->listPreferences($userid);
                }
                // update preferences
                elseif($action == 'preferences') {
                    $pref_addme_friend = $jinput->get('pref_addme_friend', 0, 'POST');
                    $pref_accept_myinvite_friend = $jinput->get('pref_accept_myinvite_friend', 0, 'POST');
                    $pref_invite_join_circle = $jinput->get('pref_invite_join_circle', 0, 'POST');
                    $pref_add_comment_topic = $jinput->get('pref_add_comment_topic', 0, 'POST');
                    $pref_approve_me_member_circle = $jinput->get('pref_approve_me_member_circle', 0, 'POST');
                    $response = $this->updatePreferences($pref_addme_friend, $pref_accept_myinvite_friend, $pref_invite_join_circle, $pref_add_comment_topic, $pref_approve_me_member_circle);
                }
                
            }
            // new post wall
            else if ($resource == 'wall' && $type == 'api') {
                if ($action == 'newpostwall') {
                    $user_id = JRequest::getCmd('userid', 0, 'POST');
                    $content = JRequest::getString('message', '', 'POST');
                    $response = $this->postWall($user_id, $content);
                }
            } else {
                $response['status'] = false;
                $response['error_message'] = 'system can not be found action. Please check Url';
            }
        } else {
            if($action == 'allgroups_nologin') {
                $limit = JRequest::getInt('limit', 0);
                $page = JRequest::getInt('page', 0);
                $category = JTable::getInstance('GroupCategory', 'CTable');
                $category->load($categoryId);
                $response = $this->getAllGroupsApi($categoryId, $sorting, $limit, $page);
            }// view a group pre-joining with group id
            elseif ($action == 'viewgroup_pre_joining') {
                $groupId = JRequest::getCmd('groupid', 0, 'GET');
                    // check group public or not
                $response = $this->viewGruopPre($groupId);
            } 
            else {
                $response['status'] = false;
                $response['error_message'] = 'please login first';
            }
        }

        if ($action == 'userid') {
            $username = $_GET['username'];//JRequest::getCmd('username', '', 'GET');
            $response = $this->getUserid($username);
        }
        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($response);
        exit();
    }

    public function getAllGroupsApi($category, $sorting, $limit, $page) {
        require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');
        
        $model = CFactory::getModel('groups');
        
        // Get group in category and it's children.
        $categories = $model->getAllCategories();
        $categoryIds = CCategoryHelper::getCategoryChilds($categories, $category);
        if ((int) $category > 0) {
            $categoryIds[] = (int) $category;
        }
        $eventsModel = CFactory::getModel('Events');
        // Get pagination object
        $data->pagination = $model->getPagination();
        // It is safe to pass 0 as the category id as the model itself checks for this value.
        $groups = $model->getAllGroupsMobile($categoryIds, $sorting, null, $limit, false, $page);
        if ($groups) {
            foreach ($groups as $k => $r) {
                // get category name
                    if ($r->categoryname == '') {
                        $category = JTable::getInstance('GroupCategory', 'CTable');
                        $category->load($r->categoryid);

                        $groups[$k]->categoryname = $category->name;
                    }
                    // get group admin name
                    $user_owner = '';
                    if ($r->ownername == '') {
                        $user_owner = CFactory::getUser($r->ownerid);

                        $groups[$k]->ownername = $user_owner->getDisplayName();
                    }
                    
                    $groups[$k]->activity_count = My_Groups::groupActivities($r->id);
                   // count group event
                   $totalEvents = $eventsModel->getTotalGroupEvents($r->id);
                   $groups[$k]->eventCount = $totalEvents;
                
            }
            $data = new StdClass;
            $data->status = true;
            $data->message = 'Get All Circle success';
            $data->groups = $groups;
        } else {
            $data = new StdClass;
            $data->status = false;
            $data->message = 'Get All Circle failed';
            $data->groups = $groups;
        }


        return $data;
    }

    public function getMyGroups($userid, $sorted) {
        $groups = $this->getGroup($userid, $sorted);
        $data = new stdClass();
        if ($groups) {
           $data->status = true;
           $data->message = 'Get My Circle success.';
           $data->groups = $groups;
        } else {
            $data->status = false;
            $data->message = 'Get My Circle failed';
            $data->groups = $groups;
        }
        return $data;
    }
    
    private function getGroup($userid, $sorted, $parentid = 0) {
        require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');
        $groupsModel = CFactory::getModel('groups');
        $groups = $groupsModel->getGroupsMobile($userid, $sorted, true, null, $parentid);
        
        $eventsModel = CFactory::getModel('Events');
         foreach ($groups as $k => $r) {
             $params = json_decode($r->params);
                // get category name
                if ($r->categoryname == '') {
                    $category = JTable::getInstance('GroupCategory', 'CTable');
                    $category->load($r->categoryid);
                    
                    $groups[$k]->categoryname = $category->name;
                }
                // get group admin name
                if ($r->ownername == '') {
                    $user_owner = CFactory::getUser($r->ownerid);

                    $groups[$k]->ownername = $user_owner->getDisplayName();
                }
                
                $groups[$k]->activity_count = My_Groups::groupActivities($r->id);
                // count group event
                $totalEvents = $eventsModel->getTotalGroupEvents($r->id);
                $groups[$k]->eventCount = $totalEvents;
                if(isset($params->course_id)) {
                    $groups[$k]->group_type = 'course_circle';
                    $groups[$k]->remoteid = $params->course_id;
                } else {
                    $groups[$k]->group_type = 'social_circle';
                    $groups[$k]->remoteid = 0;
                }
            }
       return $groups;
    }
    public function listSubCircle($parentid) {
        $my = JFactory::getUser();
        $return = $this->getGroup($my->id, '', $parentid);
        $response->status = true;
        $response->message = 'List subcircle';
        $response->groups = $return;
        return $response;
    }

    public function searchGroups($search, $search_type, $userid, $page, $limit) {
        require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');
        $model = CFactory::getModel('groups');
        $data = new stdClass();
        if ((!empty($search))) {
            //            JRequest::checkToken('get') or jexit(JText::_('COM_COMMUNITY_INVALID_TOKEN'));

            $appsLib = CAppPlugins::getInstance();
            $saveSuccess = $appsLib->triggerEvent('onFormSave', array('jsform-groups-search'));
            $eventsModel = CFactory::getModel('Events');
            if (empty($saveSuccess) || !in_array(false, $saveSuccess)) {
                $posted = true;
                if($search_type == 'all') {
                    $groups = $model->getAllGroupsMobile(null, null, $search, $limit, false, $page);
                } 
                if($search_type == 'my'){
                    $groups = $model->getGroupsMobile($userid, null, true, $search);
                }
                if ($groups) {
                    foreach ($groups as $k => $r) {
                        // get category name
                        if ($r->categoryname == '') {
                            $category = JTable::getInstance('GroupCategory', 'CTable');
                            $category->load($r->categoryid);

                            $groups[$k]->categoryname = $category->name;
                        }
                        // get group admin name
                        $user_owner = '';
                        if ($r->ownername == '') {
                            $user_owner = CFactory::getUser($r->ownerid);

                            $groups[$k]->ownername = $user_owner->getDisplayName();
                        }
                        
                        $groups[$k]->activity_count = My_Groups::groupActivities($r->id);
                       // count group event
                        $totalEvents = $eventsModel->getTotalGroupEvents($r->id);
                        $groups[$k]->eventCount = $totalEvents;

                    }
                    $data->status = true;
                    $data->message = 'Search Circle success';
                    $data->groups = $groups;
                } else {
                    $data->status = false;
                    $data->message = 'Search Circle failed';
                    $data->groups = $groups;
                }
            }
        } else {
            $data->status = false;
            $data->message = 'Keyword empty';
            $data->groups = $groups;
        }
        return $data;
    }

    public function viewGruopPre($groupid) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        $bulletinModel = CFactory::getModel('bulletins');
        $bulletins = $bulletinModel->getBulletins($group->id);
        $group->announcementcount = count($bulletins);
        $group->description = strip_tags($group->description);
        //check if group is public or private
        if(($group->unlisted == 0 && $group->approvals == 0)) { // || ($group->unlisted == 0 && $group->approvals == 1)
            return $this->viewMyGroup($group->id);
        } else {
        
            if ($group->avatar == '') {
                $group->avatar = JURI::base() . 'components/com_community/assets/group.png';
            } else {
                $group->avatar = $root . '/' . $group->avatar;
            }
            // get category name
            if ($group->categoryname == '') {
                $category = JTable::getInstance('GroupCategory', 'CTable');
                $category->load($group->categoryid);

                $group->categoryname = $category->name;
            }
            // get group admin name
            if ($group->ownername == '') {
                $user_owner = CFactory::getUser($group->ownerid);

                $group->ownername = $user_owner->getDisplayName();
            }
            return array (
                'status' => true,
                'message' => 'View Circle Pre-joining success ',
                'groupDetail' =>$group, 
            );
        }
    }

    public function viewMyGroup($groupid) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
//        print_r($group); 
        $params = $group->getParams();
        $groupModel = CFactory::getModel('groups');
        // get user current
        $my = CFactory::getUser();
        // get members list
        $members = $this->getMembersGroup($group->id);

        // Test if the current user is admin
        $isAdmin = $groupModel->isAdmin($my->id, $group->id);
        // update last access group
        $groupModel->memberLastaccessGroup($group->id, $my->id);
        
        // Test if the current browser is a member of the group
        $isMember = $groupModel->isMember($my->id, $group->id);
        $waitingApproval = false;
        // If I have tried to join this group, but not yet approved, display a notice
        if ($groupModel->isWaitingAuthorization($my->id, $group->id)) {
            $waitingApproval = true;
        }
        
        $group->description = strip_tags($group->description);
        if ($group->avatar == '') {
            $group->avatar = JURI::base() . 'components/com_community/assets/group.png';
        } else {
            $group->avatar = $root . '/' . $group->avatar;
        }
        if ($group->cover == '') {
            $group->cover = JURI::base() . 'components/com_community/assets/cover-group-default.png';
        } else {
            $group->cover = $root . '/' . $group->cover;
        }
        
        $is_course_group = $groupModel->getIsCourseGroup($group->id);
        $group->course_id = 0;
        if(isset($is_course_group)) {
            $group->course_id = $is_course_group;
        }
        // get category name
        if ($group->categoryname == '') {
            $category = JTable::getInstance('GroupCategory', 'CTable');
            $category->load($group->categoryid);

            $group->categoryname = $category->name;
        }
        // get group admin name
        if ($group->ownername == '') {
            $user_owner = CFactory::getUser($group->ownerid);

            $group->ownername = $user_owner->getDisplayName();
        }
        // Get like
        $likes = new CLike();
        $totalLikes = $likes->getLikeCount('groups', $group->id);

        // get discussion
        $discussModel = CFactory::getModel('discussions');
        $discussions = $discussModel->getDiscussionTopics($group->id, '10', $params->get('discussordering', DISCUSSION_ORDER_BYLASTACTIVITY));
        for ($i = 0; $i < count($discussions); $i++) {
            $row = $discussions[$i];
            $avatar = CFactory::getUser($row->creator)->_avatar;
            if($avatar == '') {
                 $row->avatar_creator = JURI::base() . 'components/com_community/assets/user-Male-thumb.png';
            } else {
                $row->avatar_creator = $root.'/'.$avatar;
            }
        }
        // get Bulletins
        $bulletinModel = CFactory::getModel('bulletins');
        $bulletins = $bulletinModel->getBulletins($group->id);

        // list album for group display
//        $albums = $this->getAlbums($group->id, $params);

        // list videos
//        $videos = $this->getListVideoGroup($group->id, $params);

        //events
        $events = $this->getEventsGroup($group->id, $params);

        $myGroup = array(
            'status' => true,
            'message' => 'View detail success',
            'groupDetail' => $group,
            'members' => $members,
            'isAdmin' => $isAdmin,
            'isMember' => $isMember,
            'waitingApproval' => $waitingApproval,
            'totalLike' => $totalLikes,
            'discussionstopic' => $discussions,
            'announcementlist' => $bulletins,
            'announcementcount' => count($bulletins),
//            'albums' => $albums,
//            'videos' => $videos,
            'events' => $events,
        );
        return $myGroup;
    }

    public function joinGroup($groupId) {
        $objResponse = array();


        // Add assertion to the group id since it must be specified in the post request
//                CError::assert($groupId, '', '!empty', __FILE__, __LINE__);
        // Get the current user's object
        $my = CFactory::getUser();

        // Load necessary tables
        $groupModel = CFactory::getModel('groups');
        if ($groupModel->isMember($my->id, $groupId)) {
            $objResponse['status'] = false;
            $objResponse['pending'] = false;
            $objResponse['message'] = JText::_('COM_COMMUNITY_GROUPS_ALREADY_MEMBER');
        } else {
            $member = $this->_saveMemberApi($groupId);

            if ($member->approved) {
                $objResponse['status'] = true;
                $objResponse['pending'] = false;
                $objResponse['message'] = JText::_('COM_COMMUNITY_GROUPS_JOIN_SUCCESS');
            } else {
                $objResponse['status'] = true;
                $objResponse['pending'] = true;
                $objResponse['message'] = JText::_('COM_COMMUNITY_GROUPS_APPROVAL_NEED');
            }
        }

        return $objResponse;
    }

    private function _saveMemberApi($groupId) {

        $group = JTable::getInstance('Group', 'CTable');
        $member = JTable::getInstance('GroupMembers', 'CTable');

        $group->load($groupId);
        $my = CFactory::getUser();

        // Set the properties for the members table
        $member->groupid = $group->id;
        $member->memberid = $my->id;

        // @rule: If approvals is required, set the approved status accordingly.
        $member->approved = ( $group->approvals == COMMUNITY_PRIVATE_GROUP ) ? '0' : 1;

        // @rule: Special users should be able to join the group regardless if it requires approval or not
        $member->approved = COwnerHelper::isCommunityAdmin() ? 1 : $member->approved;

        // @rule: Invited users should be able to join the group immediately.
        $groupInvite = JTable::getInstance('GroupInvite', 'CTable');
        $keys = array('groupid' => $groupId, 'userid' => $my->id);
        if ($groupInvite->load($keys)) {
            $member->approved = 1;
        }

        //@todo: need to set the privileges
        $member->permissions = '0';

        $member->store();
        $owner = CFactory::getUser($group->ownerid);

        //trigger for onGroupJoin
//            $this->triggerGroupEvents('onGroupJoin', $group, $my->id);
        // Update user group list
        $my->updateGroupList();

        // Test if member is approved, then we add logging to the activities.
        if ($member->approved) {

            CGroups::joinApproved($groupId, $my->id);
        }
        return $member;
    }

    private function getListVideoGroup($groupId, $params) {

        $result = array();
        $videoModel = CFactory::getModel('videos');
        $tmpVideos = $videoModel->getGroupVideos($groupId, '', $params->get('grouprecentvideos', GROUP_VIDEO_RECENT_LIMIT));
        $videos = array();

        if ($tmpVideos) {
            foreach ($tmpVideos as $videoEntry) {
                $video = JTable::getInstance('Video', 'CTable');
                $video->bind($videoEntry);
                $videos[] = $video;
            }
        }

        $totalVideos = $videoModel->total ? $videoModel->total : 0;
        $result['total'] = $totalVideos;
        $result['data'] = $videos;
        return $result;
    }

    private function getAlbums($groupid, $params) {
        $result_album = array();
        $photosModel = CFactory::getModel('photos');

        $albums = $photosModel->getGroupAlbums($groupid, true, false, $params->get('grouprecentphotos', GROUP_PHOTO_RECENT_LIMIT));

        $db = JFactory::getDBO();
        $where = 'WHERE a.' . $db->quoteName('groupid') . ' = ' . $db->quote($groupid);

        $totalAlbums = $photosModel->getAlbumCount($where);

        $result_album['total'] = $totalAlbums;
        $result_album['data'] = $albums;
        return $result_album;
    }

    private function getEventsGroup($groupid, $params) {
        
        $mainframe = JFactory::getApplication();
        
        $root = $mainframe->getCfg('wwwrootfile');
        
        $eventsModel = CFactory::getModel('Events');
        $tmpEvents = $eventsModel->getGroupEvents($groupid, $params->get('grouprecentevents', GROUP_EVENT_RECENT_LIMIT));
        $totalEvents = $eventsModel->getTotalGroupEvents($groupid);
        
        $events = array();
        foreach ($tmpEvents as $event) {
            $table = JTable::getInstance('Event', 'CTable');
            $table->bind($event);
            $events[] = $table;
        }
        $my = CFactory::getUser();
        $eventMembers = JTable::getInstance('EventMembers', 'CTable');
        $response = array();
        for($i = 0; $i < count($events); $i++ ) {
            $users = CFactory::getUser($events[$i]->creator);
            $row = new stdClass();
            $row->id = $events[$i]->id;
            $row->groupid = $events[$i]->contentid;
            $row->title = $events[$i]->title;
            $row->description = $events[$i]->description;
            $row->location = $events[$i]->location;
            $row->unlisted = $events[$i]->unlisted;
            $row->admin_id = $events[$i]->creator;
            $row->admin_name = $users->getDisplayName();
            $avatar = $users->_avatar;
            if($avatar == '') {
                $row->admin_avatar = JURI::base() . 'components/com_community/assets/user-Male-thumb.png';
            } else {
                $row->admin_avatar = $root.'/'.$avatar;
            }
            $row->startdate = $events[$i]->startdate;
            $row->enddate = $events[$i]->enddate;
            $row->permission = $events[$i]->permission;
            $keys = array('eventId' => $events[$i]->id, 'memberId' => $my->id);
            $eventMembers->load($keys);
            $row->status = $eventMembers->status;
            if($row->avatar != NULL) {
                $row->avatar = $root.'/'.$row->avatar;
            } else {
                $row->avatar = "";
            }
            if($row->cover != NULL) {
                $row->cover = $root.'/'.$row->cover;
            } else {
                $row->cover = "";
            }
            $row->created = $events[$i]->created;
            $response[] = $row;
        }
        return $eventsGroup = array(
            'events' => $response,
            'totalEvent' => $totalEvents,
        );
    }

    private function getMembersGroup($groupid) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $groupModel = CFactory::getModel('groups');
        $limit = 12;
        $approvedMembers = $groupModel->getMembers($groupid, $limit, true, false, true);
        CError::assert($approvedMembers, 'array', 'istype', __FILE__, __LINE__);
        // retrict data and get avatar of member
        $model = CFactory::getModel('friends');
        $group = $groupModel->getGroup($groupid);
        
        require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');
        
        $my = CFactory::getUser();
        for ($i = 0; $i < count($approvedMembers); $i++) {
            $row = $approvedMembers[$i];
            
            $user = CFactory::getUser($row->id);
            $isFriend = CFriendsHelper::isConnected($row->id, $my->id);
            $row->isFriend = $isFriend;
            
            $avatar = $user->_avatar;
            if($avatar == '') {
                $row->avatar = JURI::base() . 'components/com_community/assets/user-Male-thumb.png';
            } else {
                $row->avatar = $root.'/'.$avatar;
            }
            $row->manage = $groupModel->isAdmin($row->id, $groupid);
            
            $connection = $model->getFriendConnection($my->id, $row->id);
            $row->requestMessage = '';
            $row->pendingRequest = false;
            if($connection && !$isFriend) {
                $row->pendingRequest = true;
                $row->requestMessage = $connection[0]->msg;
            }
            $row->myRequest = false;
            if($my->id == $connection[0]->connect_from && !$isFriend) {
                $row->myRequest = true;
            }
            if($group->moodlecategoryid) {
                $roles = JoomdleHelperContent::call_method('get_user_category_role', (int) $group->moodlecategoryid, $user->username);
                $row->roles =  $roles['role'];
            } else {
                $row->roles = array();
            }
        }
        return array(
            'status' => true,
            'message' => 'Success',
            'totalMembers' => count($approvedMembers),
            'members' => $approvedMembers,
        );
    }

    private function getAllmemberGruop($groupid) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $groupModel = CFactory::getModel('groups');
        $members = $groupModel->getAllMember($groupid);
        for ($i = 0; $i < count($members); $i++) {
            $row = $members[$i];
            $avatar = CFactory::getUser($row->id)->_avatar;
            if($avatar == '') {
                 $row->avatar = JURI::base() . 'components/com_community/assets/user-Male-thumb.png';
            } else {
                $row->avatar = $root.'/'.$avatar;
            }
        }
        return $members;
    }

    private function getProfile($userid) {
//            jimport('joomla.utilities.arrayhelper');
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        
        $data = new stdClass();
        $model = CFactory::getModel('profile');
        $my = CFactory::getUser();
        // Test if userid is 0, check if the user is viewing its own profile.
        if ($userid == 0 && $my->id != 0) {
            $userid = $my->id;

            // We need to set the 'userid' var so that other code that uses
            // JRequest::getVar will work properly
            JRequest::setVar('userid', $userid);
        }
        $user = CFactory::getUser($userid);
        if (!isset($user->id) || empty($user->id) || empty($user->username)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_USER_NOT_FOUND');
            return $response;
        }
       
        if($userid != $my->id) {
            $friendModel = CFactory::getModel('friends');
            $connection = $friendModel->getFriendConnection($my->id, $user->id);
            $isFriend = CFriendsHelper::isConnected($user->id, $my->id);
        }
//        print_r($connection); die;
        $params = $user->getParams();
        $config = CFactory::getConfig();

        CFactory::setActiveProfile($userid);

        $id = $user->id;

        $result = array();

        $data->user = $user;
        $data->apps = array();

        $groupmodel = CFactory::getModel('groups');
//            $data->groups = $groupmodel->getGroupsCount($user->id);
        $data->course = "";

        $wall = CFactory::getModel('wall');
        
//        $walls = $wall->getPostUserslist($user->id);

        $profile = $model->getViewableProfile($user->id);

        $tmpGroups = $this->getGroup($user->id, null, $limit);
        
        require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');
        if ($userid) { 
            $certificates = JoomdleHelperContent::call_method("my_certificates", $user->username,'normal');
        } else {
            $certificates = JoomdleHelperContent::call_method("my_certificates", $my->username,'normal');
        }
        
        if(empty($user->_avatar)) {
            $avatar = "";
        } else {
            $avatar = $root.'/'.$user->_avatar;
        }
        if(empty($user->_cover)) {
            $cover = "";
        } else {
            $cover = $root.'/'.$user->_cover;
        }
        $result['id'] = $profile['id'];
        $result['name'] = $profile['name'];
        $result['username'] = $user->username;
//        $result['password'] = $user->password;
        if(!empty($user->usertype)) {
            $result['usertype'] = $user->usertype;
        } else {
            $result['usertype'] = '';
        }
        $result['alias'] = $user->_alias;
        $result['email'] = $profile['email'];
        $result['avatar'] = $avatar;
        $result['cover'] = $cover;
        $result['status'] = $user->getStatus();
        $result['posted_on'] = $user->_posted_on;
        $result['defaultCover'] = (empty($user->_cover)) ? true : false;
        $result['coverPostion'] = $params->get('coverPosition', 0);
        foreach ($profile['fields']['Other Information'] as $interest) {
            if(!empty($interest['value'])) {
                $result['interest'] = $interest['value'];
            } else {
                $result['interest'] = '';
            }
        }
        $result['isFriend'] = $isFriend;
        $result['requestMessage'] = '';
        $result['pendingRequest'] = false;
        if ($connection && !$isFriend) {
            $result['pendingRequest'] = true;
            $result['requestMessage'] = $connection[0]->msg;
        }
        $result['myRequest'] = false;
        if ($my->id == $connection[0]->connect_from && !$isFriend) {
            $result['myRequest'] = true;
        }
        $result['groupscount'] = $groupmodel->getGroupsCount($profile['id']);
        $result['groups'] = $tmpGroups;
//        $result['courses'] = '';
//        $result['coursescount'] = '';
//        $eventmodel = CFactory::getModel('events');
//        $result['events'] = $eventmodel->getEventsCount($profile['id']);

        $result['friendscount'] = $user->_friendcount;
        $result['friends'] = $user->_friends;

        $videoModel = CFactory::getModel('Videos');
        $result['videos'] = $videoModel->getVideosCount($profile['id']);

        $photosModel = CFactory::getModel('photos');
        $result['photos'] = $photosModel->getPhotosCount($profile['id']);
        
        foreach ($profile['fields']['Basic Information'] as $basic) {
            $infomation = new stdClass();
            $infomation->name = $basic['name'];
            if(!empty($basic['value'])) {
                $infomation->value = $basic['value'];
            } else {
                $infomation->value = '';
            }
            $result['basic_infomation'][] = $infomation;
        }
        foreach ($profile['fields']['Contact Information'] as $contact) {
            $contatct_info = new stdClass();
            $contatct_info->name = $contact['name'];
            if(!empty($contact['value'])) {
                $contatct_info->value = $contact['value'];
            } else {
                $contatct_info->value = '';
            }
            $result['contact_infomation'][] = $contatct_info;
        }
        if($certificates) {
            foreach ($certificates as $cer) {
                $education = new stdClass();
                $education->coursename = $cer['coursename'];
                $education->coursid = $cer['courseid'];
                $education->certificateFile = $cer['downloadFiles'];
                $education->certificateUrl = $cer['url'];
                $education->certificateThumb = JFactory::getApplication()->getCfg('wwwrootfile').'/images/certificates/'. md5($user->username.$cer['courseid']).'.jpg?_='.time();
                $education->certificateid = $cer['certificateid'];
                $result['education'][] = $education;
            }
        } else {
            $result['education'][] = new stdClass();
        }
        /* is featured */
//        $modelFeatured = CFactory::getModel('Featured');
//        $result['featured'] = $modelFeatured->isExists(FEATURED_USERS, $profile['id']);

        // Assign videoId
//            $result['profilevideo'] = $user->videoid;
//            $video = JTable::getInstance('Video', 'CTable');
//            $video->load($profile['profilevideo']);
//            $result['profilevideoTitle'] = $video->getTitle();
//        $result['posts_count'] = count($walls);
//        $result['posts_wall'] = $walls;
        $response = new stdClass();
        $response->status = true;
        $response->message = 'Profile loaded.';
        $response->profile = $result;
        return $response;
    }

    private function updateProfile($userid,$fullname, $aboutme, $position, $interests) {
        
        if(empty($userid)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Invalid user.';
            return $reponse;
        }
        
        $my = CFactory::getUser();
        if($my->id != $userid) {
            $reponse['status'] = false;
            $reponse['message'] = 'You can not edit another user.';
            return $reponse;
        }
        if(empty($fullname)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Name can not be blank.';
            return $reponse;
        }
        if(empty($aboutme)) {
            $reponse['status'] = false;
            $reponse['message'] = 'About me can not be blank.';
            return $reponse;
        }
        $model = CFactory::getModel('profile');
        
        // save Full name
        $user_u = CFactory::getUser($my->id);
        $user_u->name = $fullname;
        
        $user_u->save();
        
        $post = array();
        $post[4] = $aboutme;
        $post[17] = $interests;
        $post[19] = $position;
//        $post[8] = JRequest::getString('address', '', 'POST');
//        $post[9] = JRequest::getString('state', '', 'POST');
//        $post[10] = JRequest::getString('city', '', 'POST');
//        $post[11] = JRequest::getString('country', '', 'POST');
//        $post[12] = JRequest::getString('website', '', 'POST');
//        $post[14] = JRequest::getString('collect', '', 'POST');
//        $post[15] = JRequest::getString('graduation_year', '', 'POST');

        $values = array();
//            $profiles = $model->getEditableProfile($my->id, $my->getProfileType());
//            foreach ($profiles['fields'] as $group => $fields) {

        foreach ($post as $key => $data) {
            $fieldValue = new stdClass();
            // Get value from posted data and map it to the field.
            // Here we need to prepend the 'field' before the id because in the form, the 'field' is prepended to the id.
            // Grab raw, unfiltered data
            // Retrieve the privacy data for this particular field.
            $fieldValue->access = 0;
            $fieldValue->value = $data;

            if (get_magic_quotes_gpc()) {
                $fieldValue->value = stripslashes($fieldValue->value);
            }

            $values[$key] = $fieldValue;
        }
        $valuesCode = array();


        foreach ($values as $key => $val) {
            $fieldCode = $model->getFieldCode($key);

            if ($fieldCode) {
                // For backward compatibility, we can't pass in an object. We need it to behave
                // like 1.8.x where we only pass values.
                $valuesCode[$fieldCode] = $val->value;
            }
        }
        $saveSuccess = false;
        $saveSuccessMessage = 'Error update profile';

        $appsLib = CAppPlugins::getInstance();
        $appsLib->loadApplications();

        // Trigger before onBeforeUserProfileUpdate
        $args = array();
        $args[] = $my->id;
        $args[] = $valuesCode;
        $result = $appsLib->triggerEvent('onBeforeProfileUpdate', $args);
        // make sure none of the $result is false
        if (!$result || (!in_array(false, $result) )) {
            $saveSuccess = true;
            $saveSuccessMessage = 'Your settings have been saved';
            $model->saveProfile($my->id, $values);
        }
        $response['status'] = $saveSuccess;
        $response['message'] = $saveSuccessMessage;
        return $response;
    }
    
    private function updateAccount($userid, $fullname, $email, $old_pwd, $new_pwd, $renew_pwd, $number, $address, $postcode) {
        $mainframe = JFactory::getApplication();
        
        if(empty($userid)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Invalid user.';
            return $reponse;
        }
        $model = CFactory::getModel('profile');
        $my = CFactory::getUser();
        if($my->id != $userid) {
            $reponse['status'] = false;
            $reponse['message'] = 'You can not edit another user.';
            return $reponse;
        }
        if(empty($email)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Email can not empty.';
            return $reponse;
        }
        if(empty($fullname)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Fullname can not empty.';
            return $reponse;
        }
        $post[6] = $number;
        $post[8] = $address;
        $post[18] = $postcode;
        
        $values = array();

        // If update password
        $changePassword = false;
        if (JString::strlen($old_pwd) || JString::strlen($new_pwd) || JString::strlen($renew_pwd)) {
            $credentials['username'] = $my->username;
            $credentials['password'] = $old_pwd;

            $options = array ('skip_joomdlehooks' => '1');
            $checkpassword = $mainframe->checkUser($credentials, $options);
            if ($checkpassword->status === JAuthentication::STATUS_FAILURE) {
                $reponse['status'] = false;
                $msg = JText::_('OLD_PASSWORD_INVALID');
                $reponse['message'] = $msg;
                return $reponse;
            } else if ($new_pwd != $renew_pwd) {
                $reponse['status'] = false;
                $msg = JText::_('PASSWORDS_DO_NOT_MATCH');
                $reponse['message'] = $msg;
                return $reponse;
            } else {
                $changePassword = true;

                //Jooomla 3.2.0 fix. TO be remove in future
                if (version_compare(JVERSION, '3.2.0', '>=')) {
                    $salt = JUserHelper::genRandomPassword(32);
                    $crypt = JUserHelper::getCryptedPassword($new_pwd, $salt);
                    $password = $crypt . ':' . $salt;
                } else {
                    // Don't re-encrypt the password
                    // JUser bind has encrypted the password
                    if(class_exists(JUserHelper) && method_exists(JUserHelper,'hashpassword')){
                        $password = JUserHelper::hashPassword($new_pwd);
                    }else{
                        $password = $new_pwd;
                    }
                }
            }
        }
        // save Full name
        $user_u = CFactory::getUser($my->id);
        $user_u->name = $fullname;
        $user_u->email = $email;
        
        if($changePassword){
            $user_u->set('password', $password);
        }
        /* Save for CUser */
        $user_u->save();
        
        foreach ($post as $key => $data) {
            $fieldValue = new stdClass();
            // Get value from posted data and map it to the field.
            // Here we need to prepend the 'field' before the id because in the form, the 'field' is prepended to the id.
            // Grab raw, unfiltered data
            // Retrieve the privacy data for this particular field.
            $fieldValue->access = 0;
            $fieldValue->value = $data;

            if (get_magic_quotes_gpc()) {
                $fieldValue->value = stripslashes($fieldValue->value);
            }

            $values[$key] = $fieldValue;
        }
        $valuesCode = array();
        
        foreach ($values as $key => $val) {
            $fieldCode = $model->getFieldCode($key);

            if ($fieldCode) {
                // For backward compatibility, we can't pass in an object. We need it to behave
                // like 1.8.x where we only pass values.
                $valuesCode[$fieldCode] = $val->value;
            }
        }
        $saveSuccess = false;
        $saveSuccessMessage = 'Error update profile';

        $appsLib = CAppPlugins::getInstance();
        $appsLib->loadApplications();

        // Trigger before onBeforeUserProfileUpdate
        $args = array();
        $args[] = $my->id;
        $args[] = $valuesCode;
        $result = $appsLib->triggerEvent('onBeforeProfileUpdate', $args);
        // make sure none of the $result is false
        if (!$result || (!in_array(false, $result) )) {
            $saveSuccess = true;
            $saveSuccessMessage = 'Your settings have been saved';
            $model->saveProfile($my->id, $values);
        }
        $response['status'] = $saveSuccess;
        $response['message'] = $saveSuccessMessage;
        return $response;
    }
    
    private function updatePreferences($pref_addme_friend, $pref_accept_myinvite_friend, $pref_invite_join_circle, $pref_add_comment_topic, $pref_approve_me_member_circle) {
        $my = CFactory::getUser();
        if ($my->id == 0) {
            $response['status'] = false;
            $response['message'] = 'User blocked.';
            return $response;
        }
        $params = $my->getParams();
        $params->set('activityLimit', 20);
        $params->set('profileLikes', 1);
        $params->set('showOnlineStatus', 1);
        $params->set('etype_friends_request_connection', $pref_addme_friend);
        $params->set('etype_friends_create_connection', $pref_accept_myinvite_friend);
        $params->set('etype_groups_invite', $pref_invite_join_circle);
        $params->set('etype_groups_member_approved', $pref_approve_me_member_circle);
        $params->set('etype_groups_discussion_reply', $pref_add_comment_topic);
        if($my->save('params')) {
            $response['status'] = true;
            $response['message'] = JText::_('COM_COMMUNITY_PREFERENCES_SETTINGS_SAVED');
        } else {
            $response['status'] = false;
            $response['message'] = 'Preferences save failed.';
        }
        return $response;
    }
    
    private function listPreferences($userid) {
        $my = CFactory::getUser();
        if($my->id != $userid) {
            $response['status'] = false;
            $response['message'] = 'User blocked.';
            return $response;
        }
        $params = $my->_cparams;
        $data = new stdClass();
        foreach ($params as $key => $val) {
            if($key == 'etype_friends_request_connection') {
                $data->pref_addme_friend = $val;
            }
            if($key == 'etype_friends_create_connection') {
                $data->pref_accept_myinvite_friend = $val;
            }
            if($key == 'etype_groups_invite') {
                $data->pref_invite_join_circle = $val;
            }
            if($key == 'etype_groups_member_approved') {
                $data->pref_approve_me_member_circle = $val;
            }
            if($key == 'etype_groups_discussion_reply') {
                $data->pref_add_comment_topic = $val;
            }
        }
        $response['status'] = true;
        $response['message'] = 'Preferences loaded.';
        $response['preferences'] = $data;
        return $response;
    }

    private function viewGroupDiscussion($id) {
        $document = JFactory::getDocument();

        $my = CFactory::getUser();
        $model = CFactory::getModel('discussions');

        // Load the group
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($id);
        $params = $group->getParams();

        //check if group is valid
        if ($group->id == 0) {
            echo JText::_('COM_COMMUNITY_GROUPS_ID_NOITEM');
            return;
        }


        //display notice if the user is not a member of the group
        if ($group->approvals == 1 && !($group->isMember($my->id) ) && !COwnerHelper::isCommunityAdmin()) {
//                    $this->noAccess(JText::_('COM_COMMUNITY_GROUPS_PRIVATE_NOTICE'));
            return;
        }
        $discussions = $model->getDiscussionTopics($group->id, 0, $params->get('discussordering', DISCUSSION_ORDER_BYLASTACTIVITY));

        for ($i = 0; $i < count($discussions); $i++) {
            $row = $discussions[$i];
            $row->message = strip_tags($discussions[$i]->message);
            $row->user = CFactory::getUser($row->creator);
            $row->lastreplyuser = CFactory::getUser($row->lastmessageby);

            if ($row->lastreplyuser->block) {
                $row->title = $row->lastmessage = JText::_('COM_COMMUNITY_CENSORED');
            }
//                                print_r($row->message);
        }
        return $discussions;
    }

    private function postReplyDiscussion($uniqueId, $message, $upload, $type_reply, $platform) {

        $response = array();
        $mainframe = JFactory::getApplication();
        $data_root = $mainframe->getCfg('dataroot');
        $my = CFactory::getUser();

        // Load models
        $group = JTable::getInstance('Group', 'CTable');
        $discussionModel = CFactory::getModel('Discussions');
        $discussion = JTable::getInstance('Discussion', 'CTable');
        //$message		= strip_tags( $message );
        $discussion->load($uniqueId);
        $group->load($discussion->groupid);

        $config = CFactory::getConfig();
//        $response['content'] = $_FILES;
//        $response['image'] = basename($_FILES['content']['name']);
//        $response['image_type'] = $_FILES['content']['type'];
//        $response['image_tmp'] = $_FILES['content']['tmp_name'];
//        $response['image_size'] = $_FILES['content']['size'];
//        $response['image_ext'] = pathinfo($_FILES['content']['name'], PATHINFO_EXTENSION);
//        return $response;
        if($upload) {
            $ext = pathinfo($_FILES['content']['name'], PATHINFO_EXTENSION);
            if(empty($ext)) {
                $response['status'] = false;
                $response['error_message'] = 'Sorry. Please chose image or document.';
                return $response;
            }
            $extension = array('jpg', 'png', 'gif', 'jpeg', 'JPG', 'bmp', 'PNG');
            $extension_document = array('doc', 'docx', 'xls', 'xlsx', 'odt', 'pdf', 'ppt');
            if(in_array($ext, $extension) && ($type_reply == 'document' || $type_reply == 'comment' || $type_reply == 'ios')) {
                $response['status'] = false;
                $response['error_message'] = 'Sorry. Please check type reply.';
                return $response;
            } else if(in_array($ext, $extension_document) && ($type_reply == 'image' || $type_reply == 'comment' || $type_reply == 'ios')) {
                $response['status'] = false;
                $response['error_message'] = 'Sorry. Please check type reply.';
                return $response;
            }
        } else {
            if($type_reply == 'image' || $type_reply == 'document' || $type_reply == 'ios') {
                $response['status'] = false;
                $response['error_message'] = 'Sorry. Please check type reply.';
                return $response;
            }
        }
        // upload file
        if($upload) {
            
            // upload image
            if($type_reply == 'image') {
                if($_FILES['content']['type'] == "application/octet-stream" && ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG')) {
                    if($ext == 'jpeg') {
                        $image_type = 'image/jpeg';
                    }
                    if($ext == 'jpg') {
                        $image_type = 'image/jpg';
                    }
                    if($ext == 'JPG') {
                        $image_type = 'image/JPG';
                    }
                }
                elseif($_FILES['content']['type'] == "application/octet-stream" && ($ext == 'png' || $ext == 'PNG')) {
                    $image_type = 'image/png';
                    
                } else {
                    $image_type = $_FILES['content']['type'];
                }
                $uploadFolder = $data_root . '/' . $config->getString('imagefolder') . '/comments/';

                if (!JFolder::exists($uploadFolder)) {
                    JFolder::create($uploadFolder);
                }

                $originalPath = $uploadFolder . 'original_' . md5($my->id . '_comment' . time()) . CImageHelper::getExtension(
                        $image_type
                    );
                $fullImagePath = $uploadFolder . md5($my->id . '_comment' . time()) . CImageHelper::getExtension($image_type);
                $thumbPath = $uploadFolder . 'thumb_' . md5($my->id . '_comment' . time()) . CImageHelper::getExtension(
                        $image_type
                    );
                //Minimum height/width checking for Avatar uploads
                list($currentWidth, $currentHeight) = getimagesize($_FILES['content']['tmp_name']);
                if ($currentWidth < COMMUNITY_AVATAR_PROFILE_WIDTH || $currentHeight < COMMUNITY_AVATAR_PROFILE_HEIGHT) {
                    $this->_showUploadError(
                        true,
                        JText::sprintf(
                            'COM_COMMUNITY_ERROR_MINIMUM_AVATAR_DIMENSION',
                            COMMUNITY_AVATAR_PROFILE_WIDTH,
                            COMMUNITY_AVATAR_PROFILE_HEIGHT
                        )
                    );
                    return;
                }
                $newWidth = 0;
                $newHeight = 0;
                if ($currentWidth >= $currentHeight) {
                    if ($this->testResize(
                        $currentWidth,
                        $currentHeight,
                        COMMUNITY_AVATAR_RESERVE_WIDTH,
                        0,
                        COMMUNITY_AVATAR_PROFILE_WIDTH,
                        COMMUNITY_AVATAR_RESERVE_WIDTH
                    )
                    ) {
                        $newWidth = COMMUNITY_AVATAR_RESERVE_WIDTH;
                        $newHeight = 0;
                    } else {
                        $newWidth = 0;
                        $newHeight = COMMUNITY_AVATAR_RESERVE_HEIGHT;
                    }
                } else {
                    if ($this->testResize(
                        $currentWidth,
                        $currentHeight,
                        0,
                        COMMUNITY_AVATAR_RESERVE_HEIGHT,
                        COMMUNITY_AVATAR_PROFILE_HEIGHT,
                        COMMUNITY_AVATAR_RESERVE_HEIGHT
                    )
                    ) {
                        $newWidth = 0;
                        $newHeight = COMMUNITY_AVATAR_RESERVE_HEIGHT;
                    } else {
                        $newWidth = COMMUNITY_AVATAR_RESERVE_WIDTH;
                        $newHeight = 0;
                    }
                }
                // Generate full image
                if (!CImageHelper::resizeProportional($_FILES['content']['tmp_name'], $fullImagePath, $image_type, $newWidth, $newHeight)) {
                    $msg['error'] = JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $_FILES['content']['tmp_name']);
                    echo json_encode($msg);
                    exit;
                }

                CPhotos::generateThumbnail($_FILES['content']['tmp_name'], $thumbPath, $image_type);

                if (!JFile::copy($_FILES['content']['tmp_name'], $originalPath)) {
                    exit;
                }

                $now = new JDate();

                //lets set the photo data into the table
                $photo = JTable::getInstance('Photo', 'CTable');
                $photo->albumid = '-1'; // -1 for albumless
        //            $photo->image = str_replace(JPATH_ROOT . '/', '', $fullImagePath);
                $photo->image = str_replace($data_root . '/', '', $fullImagePath);
                $photo->caption = $_FILES['content']['name'];
                $photo->filesize = $_FILES['content']['size'];
                $photo->creator = $my->id;
        //            $photo->original = str_replace(JPATH_ROOT . '/', '', $originalPath);
                $photo->original = str_replace($data_root . '/', '', $originalPath);
                $photo->created = $now->toSql();
                $photo->status = 'temp';
        //            $photo->thumbnail = str_replace(JPATH_ROOT . '/', '', $thumbPath);
                $photo->thumbnail = str_replace($data_root . '/', '', $thumbPath);


                //save to table success, return the info needed
                if ($photo->store()) {
                    $info = array(
                        'thumb_url' => $photo->getThumbURI(),
                        'photo_id' => $photo->id
                    );
                } else {
                    $info = array('error' => 1);
                }
                $photo_id = $photo->id;
            } 
            else {
                $now = new JDate();
                $ext = pathinfo($_FILES['content']['name']);
                
                $table = JTable::getInstance('File', 'CTable');

                $file = new stdClass();
                $file->creator = $my->id;
                $file->filesize = sprintf("%u", $_FILES['content']['size']);
                // $file->name = JString::substr($_file['name'], 0, JString::strlen($_file['name']) - (JString::strlen($ext['extension']) + 1));
                $file->name = $_FILES['content']['name'];
                $file->created = $now->toSql();
                $file->type = CFileHelper::getExtensionIcon(CFileHelper::getFileExtension($_FILES['content']['name']));
                $fileName = JApplication::getHash($_FILES['content']['name'] . time()) . JString::substr($_FILES['content']['name'], JString::strlen($_FILES['content']['name']) - (JString::strlen($ext['extension']) + 1));
                
                $file->discussionid = $discussion->id;
                $file->groupid = $discussion->groupid;
                $file->filepath = 'images/files' . '/discussion/' . $file->discussionid . '/' . $fileName;
                JFile::copy($_FILES['content']['tmp_name'], $mainframe->getCfg('dataroot') . '/images/files' . '/discussion/' . $discussion->id . '/' . $fileName);
                
                $table->bind($file);

                $table->store();
                $file_id = $table->id;
            }
        }
        // Save the wall content
        $wall = CWallLibrary::saveWall($discussion->id, $message, 'discussions', $my, ($my->id == $discussion->creator), 'groups,discussion', '', 0, $photo_id, $file_id);
        
        $date = JFactory::getDate();

        $discussion->lastreplied = $date->toSql();
        $discussion->store();

        // @rule: only add the activities of the wall if the group is not private.
        //if( $group->approvals == COMMUNITY_PUBLIC_GROUP ) {
        // Build the URL
        $discussURL = CUrl::build('groups', 'viewdiscussion', array('groupid' => $discussion->groupid, 'topicid' => $discussion->id), true);

        $act = new stdClass();
        $act->cmd = 'group.discussion.reply';
        $act->actor = $my->id;
        $act->target = 0;
        $act->title = '';
        $act->content = $message;
        $act->app = 'groups.discussion.reply';
        $act->cid = $discussion->id;
        $act->groupid = $group->id;
        $act->group_access = $group->approvals;

        $act->like_id = $wall->id;
        $act->like_type = 'groups.discussion.reply';

        $params = new CParameter('');
        $params->set('action', 'group.discussion.reply');
        $params->set('wallid', $wall->id);
        $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
        $params->set('group_name', $group->name);
        $params->set('discuss_url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $discussion->groupid . '&topicid=' . $discussion->id);

        // Add activity log
        CActivityStream::add($act, $params->toString());


        // Get repliers for this discussion and notify the discussion creator too
        $users = $discussionModel->getRepliers($discussion->id, $group->id);
        $users[] = $discussion->creator;

        // Make sure that each person gets only 1 email
        $users = array_unique($users);

        // The person who post this, should not be getting notification email
        $key = array_search($my->id, $users);

        if ($key !== false && isset($users[$key])) {
            unset($users[$key]);
        }

        // Add notification
        $params = new CParameter('');
        $params->set('url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $discussion->groupid . '&topicid=' . $discussion->id);
        $params->set('message', $message);
        $params->set('title', $discussion->title);
        $params->set('discussion', $discussion->title);
        $params->set('discussion_url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $discussion->groupid . '&topicid=' . $discussion->id);
        CNotificationLibrary::add('groups_discussion_reply', $my->id, $users, JText::_('COM_COMMUNITY_GROUP_NEW_DISCUSSION_REPLY_SUBJECT'), '', 'groups.discussion.reply', $params);

        //add user points
        //CFactory::load( 'libraries' , 'userpoints' );
        CUserPoints::assignPoint('group.discussion.reply');

        $config = CFactory::getConfig();
        $order = $config->get('group_discuss_order');
        $order = ($order == 'DESC') ? 'prepend' : 'append';
        
        $wall_model = CFactory::getModel('wall');
        $walls_content = $wall_model->get($wall->id);
        
        $walls_content = (array) $walls_content;
        $user_reply = CFactory::getUser($walls_content->post_by);

        $walls_content['avatar'] = $user_reply->getAvatar();
        $params_wall = $walls_content['params'];
        $type = 'comment';
        if (strpos($walls_content['params'], 'attached_photo_id') !== false) {
            $type = 'uploadphoto';
            $photo_wall = $this->getPhotoUpload((int) json_decode($params_wall)->attached_photo_id);
            $walls_content['photo'] = $mainframe->getCfg('wwwrootfile') . '/' . $photo_wall->image;
            $walls_content['thumbnail'] = $mainframe->getCfg('wwwrootfile') . '/' . $photo_wall->thumbnail;
        }
        if (strpos($walls_content['params'], 'website') !== false) {
            $type = 'uploadlink';
        }
        if (strpos($walls_content['params'], 'attached_file_id') !== false) {
            $type = 'uploadfile';
            $file_wall = $this->getFileUpload((int) json_decode($params_wall)->attached_file_id);
            $walls_content['filename'] = $file_wall->name;
            $walls_content['filepath'] = $mainframe->getCfg('wwwrootfile') . '/' . $file_wall->filepath;
        }
        $walls_content['type'] = $type;

        $response['status'] = true;
        $response['message'] = 'success';
        $response['content'] = $walls_content;
        return $response;
    }

    private function getViewGroupUpdate($userId) {
        $groupModel = CFactory::getModel('groups');
        $my = CFactory::getUser($userId);
        $document = JFactory::getDocument();
        //get the groups of current user
        $userGroupArr = $groupModel->getGroupIds($my->id);

        $groupInfoArr = array(); //to store all the groups info that belongs to current user

        foreach ($userGroupArr as $userGrp) {
            $table = JTable::getInstance('Group', 'CTable');
            $table->load($userGrp);


            $groupInfoArr[] = array('thumb' => $table->getThumbAvatar());
        }

        $latestParticipatedDiscussion = $groupModel->getGroupDiscussionLastActive($userId);

        return $latestParticipatedDiscussion;
    }

    private function getViewBulletins($id) {
        $document = JFactory::getDocument();

        $my = CFactory::getUser();

        // Load the group
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($id);
        if ($group->id == 0) {
            echo JText::_('COM_COMMUNITY_GROUPS_ID_NOITEM');
            return;
        }
        if ($group->approvals == 1 && !($group->isMember($my->id) ) && !COwnerHelper::isCommunityAdmin()) {
//                $this->noAccess(JText::_('COM_COMMUNITY_GROUPS_PRIVATE_NOTICE'));
            return;
        }
        $model = CFactory::getModel('bulletins');
        $bulletins = $model->getBulletins($group->id);
        // Get the creator of the bulletins
        for ($i = 0; $i < count($bulletins); $i++) {
            $row = $bulletins[$i];

            $row->creator = CFactory::getUser($row->created_by);
            $row->message = strip_tags($bulletins[$i]->message);
        }

        // Only trigger the bulletins if there is really a need to.
        if (!empty($bulletins) && isset($bulletins)) {
            $appsLib = CAppPlugins::getInstance();
            $appsLib->loadApplications();

            // Format the bulletins
            // the bulletins need to be an array or reference to work around
            // PHP 5.3 pass by value
            $args = array();
            foreach ($bulletins as &$b) {
                $args[] = $b;
            }
            $appsLib->triggerEvent('onBulletinDisplay', $args);
        }
        return $bulletins;
    }

    private function saveDiscussionGroup($groupid, $title, $content, $notify_me, $topicid) {
        
        $mainframe = JFactory::getApplication();
        $groupsModel = CFactory::getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        $inputFilter = CFactory::getInputFilter(true);

        $my = CFactory::getUser();
        $config = CFactory::getConfig();
        $isBanned = $group->isBanned($my->id);

        $discussion = JTable::getInstance('Discussion', 'CTable');
        if ($my->id == 0) {
            return $this->blockUnregister();
        }
        $isNew = !$topicid ? true : false;
        if ($isNew) {
            $discussion->creator = $my->id;
        }
        $now = new JDate();
        $discussion->creator = $my->id;
        $discussion->groupid = $groupid;
        $discussion->title = strip_tags($title);
        $discussion->message = $inputFilter->clean($content);
        $discussion->created = $now->toSql();
        $discussion->lock = 0;
        $discussion->lastreplied = $now->toSql();
        if(!$isNew) {
            $discussion->id = $topicid;
        }

        $appsLib = CAppPlugins::getInstance();
        $saveSuccess = $appsLib->triggerEvent('onFormSave', array('jsform-groups-discussionform'));
        $validated = true;
        
        $data = new stdClass();
        if (empty($saveSuccess) || !in_array(false, $saveSuccess)) {
            $config = CFactory::getConfig();

            // @rule: Spam checks
            if ($config->get('antispam_akismet_discussions')) {
                //CFactory::load( 'libraries' , 'spamfilter' );

                $filter = CSpamFilter::getFilter();
                $filter->setAuthor($my->getDisplayName());
                $filter->setMessage($discussion->title . ' ' . $discussion->message);
                $filter->setEmail($my->email);
                $filter->setURL(CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id));
                $filter->setType('message');
                $filter->setIP($_SERVER['REMOTE_ADDR']);

                if ($filter->isSpam()) {
                    $validated = false;
                    $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_DISCUSSIONS_MARKED_SPAM'), 'error');
                }
            }

            if (empty($discussion->title)) {
                $validated = false;
                $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_TITLE_EMPTY'), 'error');
            }

            if (empty($discussion->message)) {
                $validated = false;
                $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_BODY_EMPTY'), 'error');
            }

            if ($validated) {
                //CFactory::load( 'models' , 'discussions' );

                $params = new CParameter('');

                $params->set('filepermission-member', JRequest::getInt('filepermission-member', 0));
                $params->set('notify-creator', $notify_me);

                $discussion->params = $params->toString();

                $discussion->store();

                if ($isNew) {
                    $group = JTable::getInstance('Group', 'CTable');
                    $group->load($groupid);

                    // Add logging.
                    $url = CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupid);


                    $act = new stdClass();
                    $act->cmd = 'group.discussion.create';
                    $act->actor = $my->id;
                    $act->target = 0;
                    $act->title = ''; //JText::sprintf('COM_COMMUNITY_GROUPS_NEW_GROUP_DISCUSSION' , '{group_url}' , $group->name );
                    $act->content = $discussion->message;
                    $act->app = 'groups.discussion';
                    $act->cid = $discussion->id;
                    $act->groupid = $group->id;
                    $act->group_access = $group->approvals;

                    $act->like_id = CActivities::LIKE_SELF;
                    $act->comment_id = CActivities::COMMENT_SELF;
                    $act->like_type = 'groups.discussion';
                    $act->comment_type = 'groups.discussion';

                    $params = new CParameter('');
                    $params->set('action', 'group.discussion.create');
                    $params->set('topic_id', $discussion->id);
                    $params->set('topic', $discussion->title);
                    $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                    $params->set('topic_url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $group->id . '&topicid=' . $discussion->id);

                    CActivityStream::add($act, $params->toString());

                    //@rule: Add notification for group members whenever a new discussion created.
                    $config = CFactory::getConfig();

                    if ($config->get('groupdiscussnotification') == 1) {
                        $model = $this->getModel('groups');
                        $members = $model->getMembers($groupid, null);
                        $admins = $model->getAdmins($groupid, null);

                        $membersArray = array();

                        foreach ($members as $row) {
                            $membersArray[] = $row->id;
                        }

                        foreach ($admins as $row) {
                            $membersArray[] = $row->id;
                        }
                        unset($members);
                        unset($admins);

                        // Add notification
                        //CFactory::load( 'libraries' , 'notification' );

                        $params = new CParameter('');
                        $params->set('url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $group->id . '&topicid=' . $discussion->id);
                        $params->set('group', $group->name);
                        $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                        $params->set('discussion', $discussion->title);
                        $params->set('discussion_url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $group->id . '&topicid=' . $discussion->id);
                        $params->set('user', $my->getDisplayName());
                        $params->set('subject', $discussion->title);
                        $params->set('message', $discussion->message);

                        CNotificationLibrary::addMultiple('groups_create_discussion', $discussion->creator, $membersArray, JText::sprintf('COM_COMMUNITY_NEW_DISCUSSION_NOTIFICATION_EMAIL_SUBJECT', $group->name), '', 'groups.discussion', $params);
                    }
                }

                //add user points
                //CFactory::load( 'libraries' , 'userpoints' );
                CUserPoints::assignPoint('group.discussion.create');
            }
            $data->status = true;
            $data->message = 'Success';
            $data->topic = $discussion;
        } else {
            $data->status = false;
            $data->message = 'Failed';
            $data->topic = $discussion;
        }
        return $data;
    }

    private function getNotification() {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $my = CFactory::getUser();
        $modelNotification = CFactory::getModel('notification');
        $notifications = $modelNotification->getNotification($my->id, '0', 0);
        $response = array();
        if($notifications) {
            $response['status'] = true;
            $response['message'] = 'Notification loaded.';
            for ($i = 0; $i < count($notifications); $i++) {
                $row = $notifications[$i];
                $row->content_id = 0;
                $row->groupid = 0;
                if(isset($row->params)) {
                    $string = json_decode($row->params);
                    if($row->cmd_type == 'notif_groups_member_join' || $row->cmd_type == 'notif_groups_invite') {
                        $group_url = explode("=",$string->group_url);
                        $row->content_id = end($group_url);
                        $row->groupid = end($group_url);
                        $row->requestMessage = '';
                        $row->isFriend = false;
                        $row->reject = false;
                    }
                    if($row->cmd_type == 'notif_events_invite' || $row->cmd_type == 'notif_groups_create_event') {
                        $event_url = explode("=", $string->event_url);
                        $row->content_id = end($event_url);
                        $group_url = explode("=", $string->group_url);
                        $row->groupid = end($group_url);
                        if($row->cmd_type == 'notif_events_invite') {
                            $event = JTable::getInstance('Event', 'CTable');
                            $event->load((int)$row->content_id);
                            $row->groupid = $event->contentid;
                        }
                        $row->requestMessage = '';
                        $row->isFriend = false;
                        $row->reject = false;
                        
                    }
                    if($row->cmd_type == 'notif_friends_request_connection') {
                        $actor_url = explode("=", $string->actor_url);
                        $row->content_id = end($actor_url);
                        $model = CFactory::getModel('friends');
                        $connection = $model->getFriendConnection($my->id, $row->content_id);
                        $row->requestMessage = (string) $connection[0]->msg;
                        if($connection[0]->status == 1) {
                            $row->isFriend = true;
                            $row->reject = false;
                        } else {
                            $row->reject = false;
                        }
                        $row->isFriend = false;
                        if(!$connection) {
                            $row->reject = true;
                        }
                    }
                    if($row->cmd_type == 'notif_groups_create_news') {
                        $announcement_url = explode("=", $string->announcement_url);
                        $row->content_id = end($announcement_url);
                        $group_url = explode("=", $string->group_url);
                        $row->groupid = end($group_url);
                        $row->requestMessage = '';
                        $row->isFriend = false;
                        $row->reject = false;
                        
                    }
                    if($row->cmd_type == 'notif_groups_discussion_reply' || $row->cmd_type == 'notif_groups_discussion_newfile') {
                        $discussion_url = explode("=", $string->discussion_url);
                        $row->content_id = end($discussion_url);
                        $discussion = JTable::getInstance('Discussion', 'CTable');
                        $discussion->load((int)($row->content_id));
                        $row->groupid = $discussion->groupid;
                        $row->requestMessage = '';
                        $row->isFriend = false;
                        $row->reject = false;
                        
                    }
                }
                $row->username = CFactory::getUser($row->actor)->username;
                if(CFactory::getUser($row->actor)->_avatar) {
                    $avatar = $root.'/'.CFactory::getUser($row->actor)->_avatar;
                } else {
                    $avatar = JURI::base() . '/'.'components/com_community/assets/default.jpg';
                }
                $row->avatar = $avatar;
                $row->content = CContentHelper::injectTags($row->content, $row->params, false);
            }
            $response['notification'] = $notifications;
        } else {
            $response['status'] = false;
            $response['message'] = 'Notification not loaded.';
            $response['notification'] = $notifications;
        }
        return $response;
    }

    private function countNotification() {
        $my = CFactory::getUser();
        $modelNotification = CFactory::getModel('notification');
        $notifications = $modelNotification->getNotification($my->id, '0', 0);
        if (count($notifications) > 0) {
            $count->count_notification = count($notifications);
        } else {
            $count->count_notification = 0;
        }

        return $count;
    }
    
    private function readAllNotification() {
        $my = CFactory::getUser();
        if($my->id == 0) {
            $response['status'] = false;
            $response['message'] = 'User not login';
            return $response;
        }
        $modelNotification = CFactory::getModel('notification');
        $notifications = $modelNotification->getNotification($my->id);
        foreach ($notifications as $notifi) {
            $modelNotification->readNotification($notifi->id, $my->id);
        }
        $response['status'] = true;
        $response['message'] = 'Notification updated.';
        return $response;
    }
    
    private function readNotification($notification) {
        if(empty($notification)) {
            $response['status'] = false;
            $response['message'] = 'Notification can not be empty';
            return $response;
        }
        $my = CFactory::getUser();
        if($my->id == 0) {
            $response['status'] = false;
            $response['message'] = 'User not login';
            return $response;
        }
        $modelNotification = CFactory::getModel('notification');
        $modelNotification->updateStatus($notification);
        
        $response['status'] = true;
        $response['message'] = 'Notification updated';
        return $response;
    }

    private function getUserid($username) {
        jimport('joomla.user.helper');
        $user_id = JUserHelper::getUserId($username);

        $model = CFactory::getModel('profile');
        CFactory::setActiveProfile($user_id);
        $user = CFactory::getUser($user_id);
        $profile = $model->getViewableProfile($user_id);
        $groupmodel = CFactory::getModel('groups');
        $notifModel = CFactory::getModel('notification');
        $userParams   = $user->getParams();

//        $wall = CFactory::getModel('wall');
//        $walls = $wall->getPostListUser($user_id);

        $result = array();
        $result['user_community_id'] = $user_id;
        $result['username'] = $username;
        $result['fullname'] = $user->name;
        $result['profileimageurl'] = $user->getAvatar();
        $result['profilecoverurl'] = $user->getCover();
        $result['status'] = $user->getStatus();
        $result['interest'] = $profile['fields']['Other Information'][0]['value'];
        $result['alias'] = $user->_alias;
        
        $result['notifycount'] = $notifModel->getNotificationCount($user_id,'0',$userParams->get('lastnotificationlist',''));
        
        $result['groupscount'] = $groupmodel->getGroupsCount($profile['id']);
        $result['groups'] = $user->_groups;
        $result['birthday'] = $profile['fields']['Basic Information'][1]['value'];
//        $result['posts_count'] = count($walls);

        return $result;
    }

    private function leaveGroupapi($groupId) {
        $objResponse = array();
        $model = CFactory::getModel('groups');
        $my = CFactory::getUser(); 

        if (!$my->authorise('community.leave', 'groups.' . $groupId)) {
            $errorMsg = $my->authoriseErrorMsg();
            if ($errorMsg == 'blockUnregister') {
                return $this->blockUnregister();
            }
        }

        $group = JTable::getInstance('Group', 'CTable');
        $aa = $group->load($groupId);

        $data = new stdClass();
        $data->groupid = $groupId;
        $data->memberid = $my->id;

        $model->removeMember($data);


        $my->updateGroupList();

        // STore the group and update the data
        $group->updateStats();
        $group->store();

        //delete invitation
        $invitation = JTable::getInstance('Invitation', 'CTable');
        $invitation->deleteInvitation($groupId, $my->id, 'groups,inviteUsers');
        $objResponse['status'] = true;
        $objResponse['message'] = 'You have successfully left the group.';
        return $objResponse;
    }

    private function viewDiscussionTopic($groupId, $topicId) {
        $mainframe = JFactory::getApplication();
        $groupModel = CFactory::getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $discussion = JTable::getInstance('Discussion', 'CTable');
        $my = CFactory::getUser(); 
        
        $model = CFactory::getModel('wall');
        $isOwner = $groupModel->isCreator($my->id,$groupId);
        $isAdmin = $groupModel->isAdmin($my->id, $groupId);
        $group->load($groupId);
        $discussion->load($topicId);
        $discussion_detail = array();

        $appType = 'discussions';
        $order = 'DESC';
        $walls = $model->getPost($appType, $discussion->id, $mainframe->getCfg('list_limit'), 0, $order);
        $result = new stdClass();
        $result->id = $discussion->id;
        $result->groupid = $discussion->groupid;
        $result->creator = $discussion->creator;
        $result->isowner = $isOwner;
        $result->ismanager = $isAdmin;
        $result->created = $discussion->created;
        $result->title = $discussion->title;
        $result->message = strip_tags($discussion->message);
        $result->lock = $discussion->lock;
        $result->params = $discussion->params;
        $result->parentid = $discussion->parentid;
        $result->lastreplied = $discussion->lastreplied;
        $discussion_detail['status'] = true;
        $discussion_detail['message'] = 'Success';
        $discussion_detail['discussion'] = $result;
//            $discussion_detail['reply'] = $walls;
        // update by luyenvt Sep 22, 2014 - Imformation of Creator User 
        $user_creator = CFactory::getUser($discussion->creator);
        $result->creator_username = $user_creator->getDisplayName();
        $result->creator_avatar = $user_creator->getAvatar();

        if ($walls) {
            foreach ($walls as $k => $r) {
                // avatar get image default
                if ($r->avatar == '') {
                    $user_reply = CFactory::getUser($r->post_by);

                    $walls[$k]->avatar = $user_reply->getAvatar();
                }
                // if upload file or photo: get file or photo
                
                $params = $walls[$k]->params;
                $type = 'comment';
                if (strpos($walls[$k]->params,'attached_photo_id') !== false) {
                    $type = 'uploadphoto';
                    $photo = $this->getPhotoUpload((int) json_decode($params)->attached_photo_id);
                    $walls[$k]->photo = $mainframe->getCfg('wwwrootfile').'/'.$photo->image;
                    $walls[$k]->thumbnail = $mainframe->getCfg('wwwrootfile').'/'.$photo->thumbnail;
                }
                if(strpos($walls[$k]->params,'website') !== false) {
                    $type = 'uploadlink';
                }
                if(strpos($walls[$k]->params,'attached_file_id') !== false) {
                    $type = 'uploadfile';
                    $file = $this->getFileUpload((int) json_decode($params)->attached_file_id);
                    $walls[$k]->filename = $file->name;
                    $walls[$k]->filepath = $mainframe->getCfg('wwwrootfile').'/'.$file->filepath;
                }
                $walls[$k]->type = $type;
            }
        }
        $discussion_detail['reply'] = $walls;
        return $discussion_detail;
    }

    private function postWall($uniqueId, $message) {
        
        require_once(JPATH_SITE.'/components/com_community/helpers/videos.php');
        $response = array();

        $my = CFactory::getUser($uniqueId);
        $user = CFactory::getUser($uniqueId);
        $config = CFactory::getConfig();

        JPlugin::loadLanguage('plg_community_walls', JPATH_ADMINISTRATOR);

        // Load libraries
        //CFactory::load( 'models' , 'photos' );
        //CFactory::load( 'libraries' , 'wall' );
        //CFactory::load( 'helpers' , 'url' );
        //CFactory::load( 'libraries', 'activities' );	

        $message = JString::trim($message);
        $message = strip_tags($message);
        
	$status		= CFactory::getModel('status');
        if (empty($message)) {
            $response['status'] = false;
            $response['error_message'] = 'Please add a message.';
        } else {
            
            // @rule: Spam checks
            if ($config->get('antispam_akismet_walls')) {
                //CFactory::load( 'libraries' , 'spamfilter' );

                $filter = CSpamFilter::getFilter();
                $filter->setAuthor($my->getDisplayName());
                $filter->setMessage($message);
                $filter->setEmail($my->email);
                $filter->setURL(CRoute::_('index.php?option=com_community&view=profile&userid=' . $user->id));
                $filter->setType('message');
                $filter->setIP($_SERVER['REMOTE_ADDR']);

                if ($filter->isSpam()) {
                    $response['status'] = false;
                    $response['error_message'] = 'Sorry. Message is marked a spam';
                    return $response;
                }
            }
            
            $status->update($my->id, $message);
            
            $wall = CWallLibrary::saveWall($uniqueId, $message, 'user', $my, ( $my->id == $user->id), 'profile,profile');
           
            //CFactory::load( 'libraries' , 'activities' );
            //CFactory::load('helpers','videos');
            $matches = CGetVideoLinkMatches($message);
            $activityParams = '';
            
            // We only want the first result of the video to be in the activity
            if ($matches) {
                $videoLink = $matches[0];

                //CFactory::load('libraries', 'videos');
                $videoLib = new CVideoLibrary();

                $provider = $videoLib->getProvider($videoLink);

                $activityParams .= 'videolink=' . $videoLink . "\r\n";
                if ($provider->isValid()) {
                    $activityParams .= 'url=' . $provider->getThumbnail();
                }
            }
            $act = new stdClass();
            $act->cmd = 'profile.wall.create';
            $act->actor = $my->id;
            $act->target = $uniqueId;
            $act->title = $message;
            $act->content = '';
            $act->app = 'profile';
            $act->access = 10;
            $act->cid = $uniqueId;

            // Allow comments on all these
            $act->comment_id = CActivities::COMMENT_SELF;
            $act->like_id = CActivities::COMMENT_SELF;
            $act->comment_type = 'profile.status';
            $act->like_type = 'profile.status';

            CActivityStream::add($act, $activityParams);

            // @rule: Send notification to the profile user.
            if ($my->id != $user->id) {
                //CFactory::load( 'libraries' , 'notification' );

                $params = new CParameter('');
                $params->set('url', 'index.php?option=com_community&view=profile&userid=' . $user->id);
                $params->set('message', $message);

                CNotificationLibrary::add('profile_submit_wall', $my->id, $user->id, JText::sprintf('PLG_WALLS_NOTIFY_EMAIL_SUBJECT', $my->getDisplayName()), '', 'profile.wall', $params);
            }
            //add user points
            //CFactory::load( 'libraries' , 'userpoints' );		
            CUserPoints::assignPoint('profile.wall.create');

            $response['status'] = true;
            $response['insert'] = $message;
        }

        return $response;
    }

    public function getDataHomePage($username) {
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        
//        if (!$username || $username == '') {
//            $user = CFactory::getUser();
//            $username = $user->username;
//        } else {
//            $userid = CUserHelper::getUserId($username);
            $user = CFactory::getUser();
            if ($user->id) {
                $username = $user->username;
            } else {
                $response['status'] = false;
                $response['error_message'] = 'Username not found.';
                header('Content-type: application/json; charset=UTF-8');
                header("Status", true, 400);
                echo json_encode($response);
                exit;
            }
//        }
        
        // Get My learning block
        $enrollable_only = null;
        $order = 'fullname ASC';
        $time = time();
        $lasttime = time() - 60 * 60 * 24 * 7;
        $swhere = ' AND lastaccess BETWEEN '.$lasttime.' AND '.$time.' ';
        $cursos = JoomdleHelperContent::getCourseList( (int) $enrollable_only,  $order, 0, $username, $where = '', $swhere = '');
        
        // Get My Circle block
        $model              = CFactory::getModel('groups');
        $limit = 5;

        //1 = my groups
        if (!$user->id) {
            //since this is my group only and if there is no userid provided, it should be empty
            $tmpGroups = array();
        } else {
           // $model->setState('limit', $limit); // limit the results
            $tmpGroups = $this->getGroup($user->id, null, 0);
//            $tmpGroups = $model->getGroups($user->id, null, $limit);

        }
        
        // Get My Friends block
        $haveFriends = ($user->_friendcount > 0) ? true : false;
        
        // Check update user info
        $updatedProfie = true;
        $aboutMe = $user->getInfo('FIELD_ABOUTME');
        $gender = $user->getInfo('FIELD_GENDER');
        $birthDate = $user->getInfo('FIELD_BIRTHDATE');
        $avatar = $user->_avatar;
        $cover = $user->_cover;
        $interests = $user->getInfo('FIELD_INTEREST');
        $position = $user->getInfo('FIELD_POSITION');
//        $number = $user->getInfo('FIELD_MOBILE');
//        $address = $user->getInfo('FIELD_ADDRESS');
//        $postcode = $user->getInfo('FIELD_POSTCODE');
        if (empty($aboutMe) || empty($birthDate) || empty($gender) || empty($avatar) || empty($cover) || empty($interests) || empty($position)) {
            $updatedProfie = false;
        }

        $data = new StdClass;
        $data->status = true;
        $data->message = 'Get data success';
        $data->updatedprofile = $updatedProfie;
        $data->courses = $cursos;
        $data->circles = $tmpGroups;
        $data->havefriends = $haveFriends;

        return $data;
    }
    private function getPhotoUpload($id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query
                ->select($db->quoteName(array('id', 'creator', 'image', 'thumbnail')))
                ->from($db->quoteName('#__community_photos'))
                ->where($db->quoteName('id')."=".$db->quote($id));

        $db->setQuery($query);
        $rows = $db->loadObject();
        return $rows;
    }
    private function getFileUpload($id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query
                ->select($db->quoteName(array('id', 'name', 'filepath')))
                ->from($db->quoteName('#__community_files'))
                ->where($db->quoteName('id')."=".$db->quote($id));

        $db->setQuery($query);
        $rows = $db->loadObject();
        return $rows;
    }
    private function createCircle($title, $about, $keywords, $privacy_public, $privacy_private, $groupid, $categoryid, $email, $parentid, $privacy_cecret = 0) {
        $response = array();
        $my = CFactory::getUser();
        $group = JTable::getInstance('Group', 'CTable');
        $model = CFactory::getModel('groups');
        $config = CFactory::getConfig();
        // create new circle
        $isNew = true;
        $message_status = 'created';
        // update circle
        if($groupid <> 0) {
            $group->load($groupid);
            $isNew = false;
            $message_status = 'updated';
            // check circle is found
            if (empty($group->id)) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_GROUPS_NOT_FOUND_ERROR');
                $response['groupid'] = $group->id;
                return $response;
            }
            $is_course_group = $model->getIsCourseGroup($group->id);
        }
        
        if($isNew) {
            if ($model->groupExist($title)) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_GROUPS_NAME_TAKEN_ERROR');
                $response['groupid'] = 0;
                return $response;
            }
            if (empty($title)) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_GROUPS_EMPTY_NAME_ERROR');
                $response['groupid'] = 0;
                return $response;
             }
            if (empty($about)) {
               $response['status'] = false;
               $response['message'] = JText::_('COM_COMMUNITY_GROUPS_DESCRIPTION_EMPTY_ERROR');
               $response['groupid'] = 0;
               return $response;
            }
        } else {
           if(empty($title)) {
               $response['status'] = false;
               $response['message'] = JText::_('COM_COMMUNITY_GROUPS_EMPTY_NAME_ERROR');
               $response['groupid'] = $group->id;
               return $response;
           } 
            if (empty($about)) {
               $response['status'] = false;
               $response['message'] = JText::_('COM_COMMUNITY_GROUPS_DESCRIPTION_EMPTY_ERROR');
               $response['groupid'] = 0;
               return $response;
            }
        }
        
        $params = new CParameter('');
        $params->set('discussordering', 0);
        $params->set('photopermission', GROUP_PHOTO_PERMISSION_ADMINS);
        $params->set('videopermission', GROUP_VIDEO_PERMISSION_ADMINS);
        $params->set('eventpermission', 2);
        $params->set('grouprecentphotos', GROUP_PHOTO_RECENT_LIMIT);
        $params->set('grouprecentvideos', GROUP_VIDEO_RECENT_LIMIT);
        $params->set('grouprecentevents', GROUP_EVENT_RECENT_LIMIT);
        $params->set('newmembernotification', 1);
        $params->set('joinrequestnotification', 1);
        $params->set('wallnotification', 1);
        $params->set('groupdiscussionfilesharing', 1);
        $params->set('groupannouncementfilesharing', 1);

        $now = new JDate();

        // Bind the post with the table first
        $group->name = $title;
        $group->description = $about;
        $group->categoryid = $categoryid;
        $group->summary = $about;
        $group->email = $email;
        $group->keyword = $keywords;
        $group->subgroup = 0;
        $group->website = '';
        $group->ownerid = $my->id;
        $group->created = $now->toSql();
        if($privacy_private && $privacy_cecret) {
            $group->unlisted = 1;
        } else {
            $group->unlisted = 0;
        }
        $group->unlisted = $privacy_private;
        $group->approvals = $privacy_public;

        $group->params = $params->toString();

        // @rule: check if moderation is turned on.
        $group->published = ( $config->get('moderategroupcreation') ) ? 0 : 1;
        if(isset($parentid)) {
            $group->parentid = $parentid;
        } else {
            $group->parentid = 0;
        }
        // we here save the group 1st. else the group->id will be missing and causing the member connection and activities broken.
        if(!$isNew) {
           $group->id = $group->id;
           $group->updateStats();
        }
        $group->store();

        if($isNew) {
            // Since this is storing groups, we also need to store the creator / admin
            // into the groups members table
            $member = JTable::getInstance('GroupMembers', 'CTable');
            $member->groupid = $group->id;
            $member->memberid = $group->ownerid;

            // Creator should always be 1 as approved as they are the creator.
            $member->approved = 1;

            // @todo: Setup required permissions in the future
            $member->permissions = '1';
            $member->store();
            // @rule: Only add into activity once a group is created and it is published.
            if ($group->published && !$group->unlisted) {

                $act = new stdClass();
                $act->cmd = 'group.create';
                $act->actor = $my->id;
                $act->target = 0;
                //$act->title       = JText::sprintf('COM_COMMUNITY_GROUPS_NEW_GROUP' , '{group_url}' , $group->name );
                $act->title = JText::sprintf('COM_COMMUNITY_GROUPS_NEW_GROUP_CATEGORY', '{group_url}', $group->name, '{category_url}', $group->getCategoryName());
                $act->content = ( $group->approvals == 0) ? $group->description : '';
                $act->app = 'groups';
                $act->cid = $group->id;
                $act->groupid = $group->id;
                $act->group_access = $group->approvals;

                // Allow comments
                $act->comment_type = 'groups.create';
                $act->like_type = 'groups.create';
                $act->comment_id = CActivities::COMMENT_SELF;
                $act->like_id = CActivities::LIKE_SELF;

                // Store the group now.
                $group->updateStats();
                $group->store();

                $params = new CParameter('');
                $params->set('action', 'group.create');
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                //Chris: removed categoryid
                $params->set('category_url', 'index.php?option=com_community&view=groups&task=display&categoryid=' . $group->categoryid);

                // Add activity logging
                CActivityStream::add($act, $params->toString());
            }
        } else {
            
            $params = new CParameter('');
            $params->set('action', 'group.update'); 
            if($is_course_group) {
                $params->set('course_id', $is_course_group);
            }
             //add user points
                if(CUserPoints::assignPoint('group.updated')){
                    $act = new stdClass();
                    $act->cmd = 'group.update';
                    $act->actor = $my->id;
                    $act->target = 0;
                    $act->title = ''; //JText::sprintf('COM_COMMUNITY_GROUPS_GROUP_UPDATED' , '{group_url}' , $group->name );
                    $act->content = '';
                    $act->app = 'groups.update';
                    $act->cid = $group->id;
                    $act->groupid = $group->id;
                    $act->group_access = $group->approvals;

                    // Add activity logging. Delete old ones
                    CActivityStream::remove($act->app, $act->cid);
                    CActivityStream::add($act, $params->toString());
                }
        }
        $response['status'] = true;
        $response['message'] = 'circle '.$message_status.' success';
        $response['groupid'] = $group->id;
        return $response;
    }
    
    private function LP_register($lp_name, $lp_keyword, $lp_origisition, $lp_yourname, $lp_desc, $lp_email, $lp_home_add, $lp_postal_code, $groupid) {
        $my = CFactory::getUser();
        $db= JFactory::getDBO();
        $categoryid = 7;
        $response = array();
        
        $lp_circle = $this->createCircle($lp_name, $lp_desc, $lp_keyword, 0, 0, $groupid, $categoryid, $lp_email);
        // create category course
        if($lp_circle['status'] == true) {
            // Part 1: Create category of course and assign manage into category
            if($groupid == 0) {
                require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
                $categories = array();
                $categories['name'] = $lp_name;
                $categories['parent'] = 0;
                $categories['idnumber'] = '';
                $categories['description'] = $lp_desc;
                $categories['descriptionformat'] = 1;
                $categories['username'] = $my->username;
                $cirlce_cate[] = $categories;
                $category_course = JoomdleHelperContent::call_method('create_categories', $cirlce_cate);
                if($category_course['status'] == true) {
                    $catid = $category_course['categories'][0]['id'];
                    $query =    'UPDATE '.$db->quoteName('#__community_groups')
                                .'SET '.$db->quoteName('moodlecategoryid').'='.$db->Quote($catid)
                                .'WHERE '.$db->quoteName('id').'='.$db->Quote($lp_circle['groupid']);

                    $db->setQuery($query);
                    $db->query();
                }
                // Part 2: Create category Hikashop
                $querySelect = 'SELECT category_id FROM #__hikashop_category WHERE category_name="Learning Provider" and category_parent_id=1';
                $db->setQuery($querySelect);
                $lp_categories = $db->loadObjectList();
                if(!empty($lp_categories)){
                    $category_namekey='product_'.time().'_'.rand();

                    $queryFindRight = 'SELECT MAX(category_right) as category_right FROM #__hikashop_category WHERE category_parent_id='.$lp_categories[0]->category_id;
                    $db->setQuery($queryFindRight);
                    $categoryRight = $db->loadObjectList();

                    if(!empty($categoryRight)){
                        $query ='INSERT IGNORE INTO #__hikashop_category (category_type, category_namekey, category_name,category_left,category_right,category_parent_id,category_created,category_modified,category_published,learningprovidercircleid) VALUES (\'product\',\''.$category_namekey.'\', \''.$lp_name.'\','.((int)$categoryRight[0]->category_right+1).','.((int)$categoryRight[0]->category_right+2).',\''.$lp_categories[0]->category_id.'\','.time().','.time().',1,'.(int)$lp_circle['groupid'].')';
                        $db->setQuery($query);
                        $db->query();
                    }

                }
                $response['status'] = true;
                $response['message'] = 'Learning Provider resgiter successful.';
            } else {
                $response['status'] = true;
                $response['message'] = 'Learning Provider update successful.';
            }
        } else {
            $response['status'] = false;
            $response['message'] = 'Learning Provider resgiter failed.';
        }
        return $response;
    }
    
    private function LP_list($limit, $page) {
        require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');
        $my = JFactory::getUser();
        $lp_category = 7; // hard code
        $sorting = '';
        $model = CFactory::getModel('groups');
        $eventsModel = CFactory::getModel('Events');
        $groups = $model->getAllGroupsMobile($lp_category,$sorting, null, $limit, false, $page);
        if($groups) {
        foreach ($groups as $k => $r) {
                // get category name
                    if ($r->categoryname == '') {
                        $category = JTable::getInstance('GroupCategory', 'CTable');
                        $category->load($r->categoryid);

                        $groups[$k]->categoryname = $category->name;
                    }
                    // get group admin name
                    $user_owner = '';
                    if ($r->ownername == '') {
                        $user_owner = CFactory::getUser($r->ownerid);

                        $groups[$k]->ownername = $user_owner->getDisplayName();
                    }
                    
                    $groups[$k]->activity_count = My_Groups::groupActivities($r->id);
                    
                   // count group event
                   $totalEvents = $eventsModel->getTotalGroupEvents($r->id);
                   $groups[$k]->eventCount = $totalEvents;
                
         }
         $data = new StdClass;
         $data->status = true;
         $data->message = 'success';
         $data->groups = $groups;
        } else {
         $data = new StdClass;
         $data->status = true;
         $data->message = 'Empty course';
         $data->groups = $groups;
        }
        return $data;
    }
    private function LP_about($lp_circle_id) {
        $my = CFactory::getUser();
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($lp_circle_id);
        
        $groupModel = CFactory::getModel('groups');
        // get members list
        $members = $this->getMembersGroup($group->id);
        
        // Test if the current user is admin
        $isAdmin = $groupModel->isAdmin($my->id, $group->id);
        
        $group->description = strip_tags($group->description);
        if ($group->avatar == '') {
            $group->avatar = JURI::base() . 'components/com_community/assets/group.png';
        } else {
            $group->avatar = $root . '/' . $group->avatar;
        }
        if ($group->cover == '') {
            $group->cover = JURI::base() . 'components/com_community/assets/cover-group-default.png';
        } else {
            $group->cover = $root . '/' . $group->cover;
        }
        if ($group->ownername == '') {
            $user_owner = CFactory::getUser($group->ownerid);

            $group->ownername = $user_owner->getDisplayName();
        }
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        $user_role = JoomdleHelperContent::call_method('get_user_category_role', (int) $group->moodlecategoryid, $my->username);
        
        if($user_role) {
             if($user_role['role']['roleid'] == 0) {
                 $rolename = 'leaner';
                 $roleid = 5;
             } elseif($user_role['role']['roleid'] == 1) {
                 $rolename = 'manager';
                 $roleid = $user_role['role']['roleid'];
             } elseif($user_role['role']['roleid'] == 2) {
                 $rolename = 'coursecreator';
                 $roleid = $user_role['role']['roleid'];
             } elseif($user_role['role']['roleid'] == 3) {
                 $rolename = 'contentcreator';
                 $roleid = $user_role['role']['roleid'];
             } elseif($user_role['role']['roleid'] == 4) {
                 $rolename = 'facilitator';
                 $roleid = $user_role['role']['roleid'];
             }
        }
        $return->name = $group->name;
        $return->description = $group->description;
        $return->avatar = $group->avatar;
        $return->cover = $group->cover;
        $return->keyword = $group->keyword;
        $return->isAdmin = $isAdmin;
        $return->ownername = $group->ownername;
        $return->permission_role = $rolename;
        $return->permission_id = $roleid;
        
        $response->status = true;
        $response->message = 'success.';
        $response->about = $return;
        $response->member = $members['members'];
        return $response;
        
    }

    private function deleteTopic($topicid, $groupid) {
        $my = CFactory::getUser();
        if ($my->id == 0) {
           return $this->blockUnregister();
        }
        $mainframe = JFactory::getApplication();

        $config = CFactory::getConfig();

        $data_root = $mainframe->getCfg('dataroot');
        $folder = $data_root . '/' . $config->getString('imagefolder') . '/comments/';
        $groupsModel = CFactory::getModel('groups');
        $wallModel = CFactory::getModel('wall');
        $activityModel = CFactory::getModel('activities');
        $fileModel = CFactory::getModel('files');
        $discussion = JTable::getInstance('Discussion', 'CTable');
        $discussion->load($topicid);

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        $isGroupAdmin = $groupsModel->isAdmin($my->id, $group->id);

        if ($my->id == $discussion->creator || $isGroupAdmin) {

            if ($discussion->delete()) {
                // Remove the replies to this discussion as well since we no longer need them
                $wallModel->deleteAllChildPosts($topicid, 'discussions');
                // Remove from activity stream
                CActivityStream::remove('groups.discussion', $topicid);
                // Remove Discussion Files
                $fileModel->alldelete($topicid, 'discussion');
                // Assuming all files are deleted, remove the folder if exists
                if (JFolder::exists(JPATH_ROOT . $topicid)) {
                    JFolder::delete($folder . $topicid);
                }
                $response['status'] = true;
                $response['message'] = 'Topic deleted';
            }
        } else {
            $response['status'] = false;
            $response['message'] = 'Delete topic failed';
        }
        
        return $response;
    }
    private function addMember($groupid, $memberid) {
        $group          = JTable::getInstance( 'Group' , 'CTable' );
        $groupModel = CFactory::getModel('groups');
        $member         = JTable::getInstance( 'GroupMembers' , 'CTable' );
        $my = CFactory::getUser();
        if ($my->id == 0) {
           return $this->blockUnregister();
        }
        if (!$groupid) {
            $response['status'] = false;
            $response['message'] = 'No group seleted';
        }
        if(empty($memberid)) {
            $response['status'] = false;
            $response['message'] = 'No member seleted';
        }
        $member_id = explode(',', $memberid);
        foreach ($member_id as $key=>$value) {
            
            $group->load( $groupid );
            $member->groupid        = $group->id;
            $member->memberid       = $value;

            $member->permissions    = 0;
            $member->approved       = '1';
            $member->date           = time();

            if($member->store()) {
                $group->updateStats();
                $group->store();
                $response['status'] = true;
                $response['message'] = 'Member added success';
            }else {
                $response['status'] = false;
                $response['message'] = 'Member not added';
            }
        }
        return $response;
    }
    private function lockTopic($groupid, $topicid, $status) {
        $mainframe = JFactory::getApplication();

        $my = CFactory::getUser();
        if ($my->id == 0) {
            return $this->blockUnregister();
        }
        if (empty($topicid) || empty($groupid)) {
            echo JText::_('COM_COMMUNITY_INVALID_ID');
            return;
        }
        $groupsModel = CFactory::getModel('groups');
        $wallModel = CFactory::getModel('wall');
        $discussion = JTable::getInstance('Discussion', 'CTable');
        $discussion->load($topicid);

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        $isGroupAdmin = $groupsModel->isAdmin($my->id, $group->id);
        if ($my->id == $discussion->creator || $isGroupAdmin || COwnerHelper::isCommunityAdmin()) {
            $lockStatus = $status;
            $confirmMsg = $lockStatus ? JText::_('COM_COMMUNITY_DISCUSSION_LOCKED') : JText::_('COM_COMMUNITY_DISCUSSION_UNLOCKED');

            if ($discussion->lock($topicid, $lockStatus)) {
                $response['status'] = true;
                $response['message'] = $confirmMsg;
            }
        } else {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_NOT_ALLOWED_TO_LOCK_GROUP_TOPIC');
        }
        return $response;
    }
    
    private function loadFriend($groupid) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        
        $my = CFactory::getUser();
        if (empty($groupid) || empty($groupid)) {
            echo JText::_('COM_COMMUNITY_INVALID_ID');
            return;
        }
        $groupsModel = CFactory::getModel('Groups');

        $frendlist = $groupsModel->getInviteFriendsList($my->id, $groupid);
        $listFriend = array();
        $data = new stdClass();
        if($frendlist) {
            for ($i = 0; $i < count($frendlist); $i++) {
                $row = $frendlist[$i];
                $friends = new stdClass();
                $friends->id = $row;
                $user = CFactory::getUser($row);
                $avatar = $user->_avatar;
                if($avatar == '') {
                     $friends->avatar = JURI::base() . 'components/com_community/assets/user-Male-thumb.png';
                } else {
                    $friends->avatar = $root.'/'.$avatar;
                }

                $friends->name = $user->getDisplayName();
                $listFriend[] = $friends;
            }
            $data->status = true;
            $data->message = 'Friend load success';
            $data->friend = $listFriend;
        } else {
           $data->status = false;
           $data->message = 'Friend load failed';
           $data->friend = $listFriend; 
        }
        return $data;
    }
    // circle admin
    private function makeORrevertAdminCircle($groupid, $memberid, $type) {
        $my = CFactory::getUser();
        
        if(empty($groupid)) {
            $response['status'] = false;
            $response['message'] = 'Circle is not available.';
            return $response;
        }
        if(empty($memberid)) {
            $response['status'] = false;
            $response['message'] = 'Member is not available.';
            return $response;
        }
        $model = CFactory::getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        $type_arr = array('makeAdmin', 'revertAdmin');
        if(!in_array($type, $type_arr) || empty($type)) {
            $response['status'] = false;
            $response['message'] = 'Request doesn\'t exits.';
            return $response;
        }
        if($type == 'makeAdmin') {
            $doAdd = true;
            $message = JText::_('COM_COMMUNITY_GROUPS_NEW_ADMIN_MESSAGE');
        } else if($type == 'revertAdmin') {
            $doAdd = false;
            $message = JText::_('COM_COMMUNITY_GROUPS_NEW_USER_MESSAGE');
        }
        if (!$my->authorise('community.edit', 'groups.admin.' . $groupid, $group)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_PERMISSION_DENIED_WARNING');
            return $response;
        } else {
            $member = JTable::getInstance('GroupMembers', 'CTable');

            $keys = array('groupId' => $group->id, 'memberId' => $memberid);
            $member->load($keys);
            $member->permissions = $doAdd ? 1 : 0;

            if($member->store()) {
                $response['status'] = true;
                $response['message'] = $message;
                return $response;
            } else {
                $response['status'] = false;
                $response['message'] = 'Circle Make Amin failed.';
                return $response;
            }
        }
        
    }
    
    //remove member
    private function removeMemberCircle($groupid, $memberid, $ban) {
        if(empty($groupid)) {
            $response['status'] = false;
            $response['message'] = 'Circle can not be empty.';
            return $response;
        }
        if(empty($memberid)) {
            $response['status'] = false;
            $response['message'] = 'Member can not be empty.';
            return $response;
        }
        $model = CFactory::getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        $ban_arr = array('0', '1');
        if(!in_array($ban, $ban_arr)) {
            $response['status'] = false;
            $response['message'] = 'Request remove with ban doesn\'t exits.';
            return $response;
        }
        $my = CFactory::getUser();
        if(!$ban) {
            if (!$my->authorise('community.remove', 'groups.member.' . $memberid, $group)) {
                $errorMsg = $my->authoriseErrorMsg();
                if ($errorMsg == 'blockUnregister') {
                    $response['status'] = false;
                    $response['message'] = $errorMsg;
                    return $response;
                } else {
                    $response['status'] = false;
                    $response['message'] = $errorMsg;
                    return $response;
                }
            } else {
                $groupMember = JTable::getInstance('GroupMembers', 'CTable');
                $keys = array('groupId' => $groupid, 'memberId' => $memberid);
                $groupMember->load($keys);

                $data = new stdClass();

                $data->groupid = $groupid;
                $data->memberid = $memberid;

                $model->removeMember($data);
                $user = CFactory::getUser($memberid);
                $user->updateGroupList(true);
                $this->triggerGroupEvents('onGroupLeave', $group, $memberid);

                //add user points
                CUserPoints::assignPoint('group.member.remove', $memberid);

                //delete invitation
                $invitation = JTable::getInstance('Invitation', 'CTable');
                $invitation->deleteInvitation($groupid, $memberid, 'groups,inviteUsers');


            }

            // Store the group and update the data
            $group->updateStats();
            $group->store();
            $response['status'] = true;
            $response['message'] = JText::_('COM_COMMUNITY_GROUPS_MEMBERS_DELETE_SUCCESS');
            return $response;
        } else {
            return $this->banUnbanMember($groupid, $memberid, 'ban');
        }
    }
    // ban and unban member
    private function banUnbanMember($groupid, $memberid, $type) {
        if(empty($groupid)) {
            $response['status'] = false;
            $response['message'] = 'Circle can not be empty.';
            return $response;
        }
        if(empty($memberid)) {
            $response['status'] = false;
            $response['message'] = 'Member can not be empty.';
            return $response;
        }
        $type_arr = array('ban', 'unban');
        if(!in_array($type, $type_arr) || empty($type)) {
            $response['status'] = false;
            $response['message'] = 'Request doesn\'t exits.';
            return $response;
        }
        if($type == 'ban') {
            $doBan = true;
        } elseif($type == 'unban') {
            $doBan = false;
        }
        $my = CFactory::getUser();

        $groupModel = CFactory::getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        if (!$my->authorise('community.update', 'groups.member.ban.' . $groupid, $group)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_PERMISSION_DENIED_WARNING');
            return $response;
        } else {
            $member = JTable::getInstance('GroupMembers', 'CTable');
            $keys = array('groupId' => $group->id, 'memberId' => $memberid);
            $member->load($keys);

            $member->permissions = ($doBan) ? COMMUNITY_GROUP_BANNED : COMMUNITY_GROUP_MEMBER;

            $member->store();

            $group->updateStats();

            $group->store();

            if ($doBan) { //if user is banned, display the appropriate response and color code
                //trigger for onGroupBanned
                $this->triggerGroupEvents('onGroupBanned', $group, $memberid);
                $response['status'] = true;
                $response['message'] = JText::_('COM_COMMUNITY_GROUPS_MEMBER_BEEN_BANNED');
                return $response;
            } else {
                //trigger for onGroupUnbanned
                $this->triggerGroupEvents('onGroupUnbanned', $group, $memberid);
                $response['status'] = true;
                $response['message'] = JText::_('COM_COMMUNITY_GROUPS_MEMBER_BEEN_UNBANNED');
                return $response;
            }
        }
    }
    
    public function triggerGroupEvents($eventName, &$args, $target = null) {
        CError::assert($args, 'object', 'istype', __FILE__, __LINE__);

        require_once( COMMUNITY_COM_PATH . '/libraries/apps.php' );
        $appsLib = CAppPlugins::getInstance();
        $appsLib->loadApplications();

        $params = array();
        $params[] = $args;

        if (!is_null($target))
            $params[] = $target;

        $appsLib->triggerEvent($eventName, $params);
        return true;
    }
    
    private function ViewEvent($groupid, $eventid) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        $my = CFactory::getUser();
        if (empty($eventid)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_EVENTS_NOT_AVAILABLE_ERROR');
            return $response;
        }
        $event = JTable::getInstance('Event', 'CTable');
        if($event->load($eventid)) {
        if($event->unlisted && !$event->isMember($my->id) && !$event->getUserStatus($my->id) == 0){
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_EVENTS_UNLISTED_ERROR');
            return $response;
        }
        if (!$event->isPublished()) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_EVENTS_UNDER_MODERATION');
            return $response;
        }
        
        $eventMembers = JTable::getInstance('EventMembers', 'CTable');
        $keys = array('eventId' => $event->id, 'memberId' => $my->id);
        $eventMembers->load($keys);
        
        $eventDetail = new stdClass();
        $eventDetail->id = $event->id;
        $eventDetail->title = $event->title;
        $eventDetail->description = $event->description;
        $eventDetail->location = $event->location;
        $eventDetail->unlisted = $event->unlisted;
        $eventDetail->admin_id = $event->creator;
        $user = CFactory::getUser($event->creator);
        $eventDetail->admin_name = $user->getDisplayName();
        $avatar = $user->_avatar;
        if($avatar == '') {
            $eventDetail->admin_avatar = JURI::base() . 'components/com_community/assets/user-Male-thumb.png';
        } else {
            $eventDetail->admin_avatar = $root.'/'.$avatar;
        }
        $eventDetail->startdate = $event->startdate;
        $eventDetail->enddate = $event->enddate;
        $eventDetail->permission = $event->permission;
        $eventDetail->status = $eventMembers->status;
        if($event->avatar == NULL) {
            $eventDetail->avatar = "";
        } else {
            $eventDetail->avatar = $root . '/'.$event->avatar;
        }
        if($event->cover == NULL) {
            $eventDetail->cover = "";
        } else {
            $eventDetail->cover = $root . '/'.$event->cover;
        }
        $eventDetail->created = $event->created;
        
        // begin get attending
        $type = 1;
        $guestsIds = $event->getMembers($type, 0, false, $approval);
        
        $userids = array();
        foreach ($guestsIds as $uid) {
            $userids[] = $uid->id;
        }
        CFactory::loadUsers($userids);
        $attending = array();
        $data = new stdClass();
        
            for ($i = 0; $i < count($guestsIds); $i++) {
                $user_attend = CFactory::getUser($guestsIds[$i]->id);
                $guests = new stdClass();
                $guests->userid = $user_attend->id;
                $guests->name = $user_attend->getDisplayName();
                $avatar_attend = $user_attend->_avatar;
                if($avatar_attend == '') {
                    $guests->avatar = JURI::base() . 'components/com_community/assets/user-Male-thumb.png';
                } else {
                    $guests->avatar = $root.'/'.$avatar_attend;
                }
                $guests->isMe = ($my->id == $guestsIds[$i]->id) ? true : false;
                $guests->isAdmin = $event->isAdmin($guestsIds[$i]->id);
                $guests->statusType = $guestsIds[$i]->statusCode;
                $attending[] = $guests;
            }
            $eventDetail->attending = $attending;
            $data->status = true;
            $data->message = 'Event Detail loaded';
            $data->event = $eventDetail;
        } else {
           $data->status = false;
           $data->message = 'Event Detail not loaded';
           $data->event = $eventDetail; 
        }
        return $data;
    }
    private function addEvent($groupid, $title, $about, $startdate, $starttime, $enddate, $endtime, $location, $eventid) {
        $my = JFactory::getUser();
        $config = CFactory::getConfig();
        $model = CFactory::getModel('events');
        $event = JTable::getInstance('Event', 'CTable');
        $isbanned = false;
        $isNew = ($eventid == 0) ? true : false;
        if ($groupid) {
            $group = JTable::getInstance('Group', 'CTable');
            $group->load($groupid);
            // Check if the current user is banned from this group
            $isbanned = $group->isBanned($my->id);
        }
        if ($isbanned) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_ACCESS_FORBIDDEN');
            $response['eventid'] = 0;
            return $response;
        }
        if (empty($title)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_EVENTS_TITLE_ERROR');
            $response['eventid'] = 0;
            return $response;
        }
        //check for user daily limit first, then check for the total limit
        if (CFactory::getConfig()->get("limit_events_perday") <= CFactory::getModel("events")->getTotalToday($my->id)) {
            $eventLimit = CFactory::getConfig()->get("limit_events_perday");
            $response['status'] = false;
            $response['message'] = JText::sprintf('COM_COMMUNITY_EVENTS_DAILY_LIMIT', $eventLimit);
            $response['eventid'] = 0;
            return $response;
        } else {
            if (CLimitsHelper::exceededEventCreation($my->id)) {
                $eventLimit = $config->get('eventcreatelimit');
                $response['status'] = false;
                $response['message'] = JText::sprintf('COM_COMMUNITY_EVENTS_LIMIT', $eventLimit);
                $response['eventid'] = 0;
                return $response;
            }
        }
        if (empty($startdate)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_EVENTS_STARTDATE_ERROR');
            $response['eventid'] = 0;
            return $response;
        }
        if (empty($enddate)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_EVENTS_ENDDATE_ERROR');
            $response['eventid'] = 0;
            return $response;
        }
        if((time()-(60*60*24)) > strtotime($startdate) && $isNew) {
            $response['status'] = false;
            $response['message'] = 'Start date invalid';
            $response['eventid'] = 0;
            return $response;
        }
        if((time()-(60*60*24)) > strtotime($enddate)) {
            $response['status'] = false;
            $response['message'] = 'End date invalid';
            $response['eventid'] = 0;
            return $response;
        }
        if(strtotime($startdate) > strtotime($enddate)) {
            $response['status'] = false;
            $response['message'] = 'End date must bigger start date';
            $response['eventid'] = 0;
            return $response;
        }
        if (empty($location)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_EVENTS_LOCATION_ERR0R');
            $response['eventid'] = 0;
            return $response;
        }
        if((strtotime($startdate) == strtotime($enddate)) && (strtotime($starttime) == strtotime($endtime))) {
            $endtime = strtotime($endtime)+ 7200;
        }
        $params = new CParameter('');
        $params->set('eventrecentphotos', 6);
        $params->set('eventrecentvideos', 6);
        $params->set('photopermission', -1);
        $params->set('videopermission', -1);
        
        
                
        $event->title = $title;
        $event->description = $about;
        $event->startdate = $startdate . ' ' . date("G:i", strtotime($starttime)) . ':00';
        $event->enddate = $enddate . ' ' . date("G:i", $endtime) . ':00';
        $event->location = $location;
        $event->params = $params->toString();
        $event->creator = $my->id;
        $event->catid = 1;
        $event->published = 1;
        $event->created = JFactory::getDate()->toSql();
        $handler = CEventHelper::getHandler($event);
        $event->contentid = $handler->getContentId();
        $event->type = $handler->getType();
        $event->offset = 0;
        $status = 'create';
        if(!$isNew) {
            $event->id = $eventid;
            $status = 'update';
        }
        if($event->store()) {
        
            if($isNew && !$event->isRecurring()) {
                $members = $this->getMembersGroup($group->id);
                // add member to event
                $member = JTable::getInstance('EventMembers', 'CTable');
                if($members['members']) {
                    foreach ($members['members'] as $mem) {
                    $member->eventid = $event->id;
                    $member->memberid = $mem->id;
                    $member->created = JFactory::getDate()->toSql();

                    // Creator should always be 1 as approved as they are the creator.
                    $member->status = COMMUNITY_EVENT_STATUS_ATTEND;

                    // @todo: Setup required permissions in the future
                    if($mem->id == $event->creator) {
                        $member->permission = '1';
                    } else {
                       $member->permission = '3'; 
                    }

                    $member->store();
                    }
                }
                
                $event->updateGuestStats();
                $event->store();
            }
            if($isNew) {
                $action_str = 'events.create';
                CUserPoints::assignPoint($action_str);
                CEvents::addGroupNotification($event);
            }
            $response['status'] = true;
            $response['message'] = 'Event '.$status.' success';
            $response['eventid'] = $event->id;
            return $response;
        } else {
            $response['status'] = false;
            $response['message'] = 'Event create failed';
            $response['eventid'] = 0;
            return $response;
        }
    }
    // Begin delete event
    private function deleteEvent($eventid) {
        
        $model = CFactory::getModel('events');
        $event = JTable::getInstance('Event', 'CTable');
        if(empty($eventid)) {
           $response['status'] = false;
           $response['message'] = 'Sorry, we cannot find the event.';
           return $response;
        }
        if(!$event->load($eventid)) {
            $response['status'] = false;
            $response['message'] = 'Sorry, we cannot find the specified event.';
            return $response;
        }
        $action = '';
        // delete all member in event
        $event->deleteAllMembers($action);
        
        $eventData = $event;
        // delete event
        $deleted = $event->delete();
        if($deleted) {
            $featured = new CFeatured(FEATURED_EVENTS);
            
           // Delete avatar
           $this->deleteEventAvatar($eventData);
           // Delete featuer
           $featured->delete($eventid);
           
           $response['status'] = true;
           $response['message'] = 'Event Deleted';
        } else {
            $response['status'] = false;
            $response['message'] = 'Sorry, we cannot find the specified event.';
        }
        return $response;
    }
    public function deleteEventAvatar($eventData, $series = false)
    {
        jimport('joomla.filesystem.file');

        $model = CFactory::getModel('events');

        $avatarInused = $model->isImageInUsed($eventData->avatar, 'avatar', $eventData->id, $series);
        if ($eventData->avatar != "components/com_community/assets/eventAvatar.png" && !empty($eventData->avatar) && !$avatarInused) {
            $path = explode('/', $eventData->avatar);

            $file = JPATH_ROOT . '/' . $path[0] . '/' . $path[1] . '/' . $path[2] . '/' . $path[3];
            if (JFile::exists($file)) {
                JFile::delete($file);
            }
        }

        $thumbInused = $model->isImageInUsed($eventData->thumb, 'thumb', $eventData->id, $series);
        if ($eventData->thumb != "components/com_community/assets/event_thumb.png" && !empty($eventData->avatar) && !$thumbInused) {
            //$path = explode('/', $eventData->avatar);
            //$file = JPATH_ROOT .'/'. $path[0] .'/'. $path[1] .'/'. $path[2] .'/'. $path[3];
            $file = JPATH_ROOT . '/' . CString::str_ireplace('/', '/', $eventData->thumb);
            if (JFile::exists($file)) {
                JFile::delete($file);
            }
        }
    }
    
    private function uploadAvatar($id, $photo, $type) {
        $params = new JRegistry();
        $mainframe = JFactory::getApplication();
        $document = JFactory::getDocument();
        // Check type upload
        $cTable = JTable::getInstance(ucfirst($type), 'CTable');
        $cTable->load($id);
        if($type == "event") {
            $model = CFactory::getModel('events');
            $handler = CEventHelper::getHandler($cTable);
        }
        $data_root = $mainframe->getCfg('dataroot');
        
        if(empty($id)) {
            $response['status'] = false;
            $response['message'] = 'Sorry. '.$type.' can not be empty.';
            return $response;
        }
        if(empty($photo)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_NO_POST_DATA');
            return $response;
        }
        
        if(empty($type)) {
            $response['status'] = false;
            $response['message'] = 'Type can not be empty.';
            return $response;
        }
        if($type == 'event') {
            if (!$handler->manageable()) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_ACCESS_FORBIDDEN');
                return $response;
            }
        }
        /*if (!CImageHelper::isValidType($_FILES['photo']['type'])) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_IMAGE_FILE_NOT_SUPPORTED');
            $response['file_type'] = $_FILES['photo']['type'];
            return $response;
        }*/
        if($type=="profile"){
            $my = CFactory::getUser($id);
        }else{
            $my = CFactory::getUser();
        }
        $config = CFactory::getConfig();
        
        $uploadLimit = (double)$config->get('maxuploadsize');
        $uploadLimit = ($uploadLimit * 1024 * 1024);
        if (filesize($_FILES['photo']['tmp_name']) > $uploadLimit && $uploadLimit != 0) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_VIDEOS_IMAGE_FILE_SIZE_EXCEEDED_MB');
            return $response;
        }
        $ext = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION);
        if ($_FILES['photo']['type'] == "application/octet-stream" && ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG')) {
            if ($ext == 'jpeg') {
                $image_type = 'image/jpeg';
            }
            if ($ext == 'jpg') {
                $image_type = 'image/jpg';
            }
            if ($ext == 'JPG') {
                $image_type = 'image/JPG';
            }
        } elseif ($_FILES['photo']['type'] == "application/octet-stream" && ($ext == 'png' || $ext == 'PNG')) {
            $image_type = 'image/png';
        } else {
            $image_type = $_FILES['photo']['type'];
        }

        CImageHelper::autoRotate($_FILES['photo']['tmp_name']);
        // @todo: configurable width?
        $imageMaxWidth = 1024;

        // Get a hash for the file name.
        $fileName = JApplication::getHash($_FILES['photo']['tmp_name'] . time());
        $hashFileName = JString::substr($fileName, 0, 24);
        $avatarFolder = ($type != 'profile' && $type != '') ? $type . '/' : '';
        
        // @todo: configurable path for avatar storage?
        $storage = $data_root . '/' . $config->getString('imagefolder') . '/avatar/'.$avatarFolder;
        $storageImage = $storage . '/'. $hashFileName . CImageHelper::getExtension($image_type);
        
        $image = $config->getString(
                        'imagefolder'
                ) . '/avatar/'.$avatarFolder . $hashFileName . CImageHelper::getExtension($image_type);

        $storageThumbnail = $storage . '/thumb_' . $hashFileName . CImageHelper::getExtension(
                        $_FILES['photo']['type']
        );
        $thumbnail = $config->getString(
                        'imagefolder'
                ) . '/avatar/'. $avatarFolder . 'thumb_' . $hashFileName . CImageHelper::getExtension(
                        $_FILES['photo']['type']
        );
        $storageReserve = $storage . '/' . $type . '-' . $hashFileName . CImageHelper::getExtension($image_type);
        $imageAttachment = $config->getString(
                    'imagefolder'
                ) . '/avatar/' . $hashFileName . '_stream_' . CImageHelper::getExtension($image_type);

        //Minimum height/width checking for Avatar uploads
        list($currentWidth, $currentHeight) = getimagesize($_FILES['photo']['tmp_name']);
        if ($currentWidth < COMMUNITY_AVATAR_PROFILE_WIDTH || $currentHeight < COMMUNITY_AVATAR_PROFILE_HEIGHT) {
                $response['status'] = false;
                $response['message'] = JText::sprintf(
                        'COM_COMMUNITY_ERROR_MINIMUM_AVATAR_DIMENSION',
                        COMMUNITY_AVATAR_PROFILE_WIDTH,
                        COMMUNITY_AVATAR_PROFILE_HEIGHT
                    );
                return $response;
        }
        
        /**
         * Generate square avatar
         */
        if (!CImageHelper::createThumb(
                        $_FILES['photo']['tmp_name'], $storageImage, $image_type, COMMUNITY_AVATAR_PROFILE_WIDTH, COMMUNITY_AVATAR_PROFILE_HEIGHT
                )
        ) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE');
            return $response;
        }

        // Generate thumbnail
        if (!CImageHelper::createThumb($_FILES['photo']['tmp_name'], $storageThumbnail, $image_type, COMMUNITY_SMALL_AVATAR_WIDTH, COMMUNITY_SMALL_AVATAR_WIDTH)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE');
            return $response;
        }
        /**
         * Generate large image use for avatar thumb cropping
         * It must be larget than profile avatar size because we'll use it for profile avatar recrop also
         */
        $newWidth = 0;
        $newHeight = 0;
        if ($currentWidth >= $currentHeight) {
            if ($this->testResize(
                            $currentWidth, $currentHeight, COMMUNITY_AVATAR_RESERVE_WIDTH, 0, COMMUNITY_AVATAR_PROFILE_WIDTH, COMMUNITY_AVATAR_RESERVE_WIDTH
                    )
            ) {
                $newWidth = COMMUNITY_AVATAR_RESERVE_WIDTH;
                $newHeight = 0;
            } else {
                $newWidth = 0;
                $newHeight = COMMUNITY_AVATAR_RESERVE_HEIGHT;
            }
        } else {
            if ($this->testResize(
                            $currentWidth, $currentHeight, 0, COMMUNITY_AVATAR_RESERVE_HEIGHT, COMMUNITY_AVATAR_PROFILE_HEIGHT, COMMUNITY_AVATAR_RESERVE_HEIGHT
                    )
            ) {
                $newWidth = 0;
                $newHeight = COMMUNITY_AVATAR_RESERVE_HEIGHT;
            } else {
                $newWidth = COMMUNITY_AVATAR_RESERVE_WIDTH;
                $newHeight = 0;
            }
        }
        
        if (!CImageHelper::resizeProportional(
                        $_FILES['photo']['tmp_name'], $storageReserve, $image_type, $newWidth, $newHeight
                )
        ) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE');
            return $response;
        }

        $album = JTable::getInstance('Album', 'CTable');

            //create the avatar default album if it does not exists
        if (!$albumId = $album->isAvatarAlbumExists($id, $type)) {
                $albumId = $album->addAvatarAlbum($id, $type);
        }
        // Autorotate avatar based on EXIF orientation value
        if ($_FILES['photo']['type'] == 'image/jpeg') {
            $orientation = CImageHelper::getOrientation($_FILES['photo']['tmp_name']);
            CImageHelper::autoRotate($storageImage, $orientation);
            CImageHelper::autoRotate($storageThumbnail, $orientation);
        }
        
        // Update the event with the new image
        $cTable->setImage($image, 'avatar');
        $cTable->setImage($thumbnail, 'thumb');
        
        $now = new JDate();
        $photo = JTable::getInstance('Photo', 'CTable');

        $photo->albumid = $albumId;
//            $photo->image = str_replace(JPATH_ROOT . '/', '', $fullImagePath);
        $photo->image = str_replace($mainframe->getCfg('dataroot') . '/', '', $storageImage);
        $photo->caption = $_FILES['photo']['name'];
        $photo->filesize = $_FILES['photo']['size'];
        $photo->creator = $my->id;
        $photo->created = $now->toSql();
        $photo->published = 1;
        $photo->thumbnail = str_replace($mainframe->getCfg('dataroot') . '/', '', $storageThumbnail);
        $photo->original = str_replace($mainframe->getCfg('dataroot') . '/', '', $storageImage);
        
        if ($photo->store()) {
                $album->load($albumId);
                $album->photoid = $photo->id;
                $album->setParam('thumbnail', $photo->thumbnail);
                $album->store();
        }
        if ($type == 'profile') {
            $profileType = $my->getProfileType();
                $multiprofile = JTable::getInstance('MultiProfile', 'CTable');
                $multiprofile->load($profileType);

                $useWatermark = $profileType != COMMUNITY_DEFAULT_PROFILE && $config->get(
                    'profile_multiprofile'
                ) && !empty($multiprofile->watermark) ? true : false;
                if ($useWatermark && $multiprofile->watermark) {
                    JFile::copy(
                        $storageImage,
                        $mainframe->getCfg('dataroot') . '/images/watermarks/original' . '/' . md5(
                            $my->id . '_avatar'
                        ) . CImageHelper::getExtension($image_type)
                    );
                    JFile::copy(
                        $storageThumbnail,
                        $mainframe->getCfg('dataroot') . '/images/watermarks/original' . '/' . md5(
                            $my->id . '_thumb'
                        ) . CImageHelper::getExtension($image_type)
                    );
                    $watermarkPath = $mainframe->getCfg('dataroot') . '/' . CString::str_ireplace('/', '/', $multiprofile->watermark);
                    list($watermarkWidth, $watermarkHeight) = getimagesize($watermarkPath);
                    list($avatarWidth, $avatarHeight) = getimagesize($storageImage);
                    list($thumbWidth, $thumbHeight) = getimagesize($storageThumbnail);

                    $watermarkImage = $storageImage;
                    $watermarkThumbnail = $storageThumbnail;

                    // Avatar Properties
                    $avatarPosition = CImageHelper::getPositions(
                        $multiprofile->watermark_location,
                        $avatarWidth,
                        $avatarHeight,
                        $watermarkWidth,
                        $watermarkHeight
                    );

                    // The original image file will be removed from the system once it generates a new watermark image.
                    CImageHelper::addWatermark(
                        $storageImage,
                        $watermarkImage,
                        $image_type,
                        $watermarkPath,
                        $avatarPosition->x,
                        $avatarPosition->y
                    );

                    //Thumbnail Properties
                    $thumbPosition = CImageHelper::getPositions(
                        $multiprofile->watermark_location,
                        $thumbWidth,
                        $thumbHeight,
                        $watermarkWidth,
                        $watermarkHeight
                    );

                    // The original thumbnail file will be removed from the system once it generates a new watermark image.
                    CImageHelper::addWatermark(
                        $storageThumbnail,
                        $watermarkThumbnail,
                        $image_type,
                        $watermarkPath,
                        $thumbPosition->x,
                        $thumbPosition->y
                    );

                    $my->set('_watermark_hash', $multiprofile->watermark_hash);
                }

                // We need to make a copy of current avatar and set it as stream 'attachement'
                // which will only gets deleted once teh stream is deleted

                $my->_cparams->set('avatar_photo_id', $photo->id); //we also set the id of the avatar photo

                $my->save();

                JFile::copy($image, $imageAttachment);
                $params->set('attachment', $imageAttachment);
        }
        if (empty($saveAction)) {
                $cTable->setImage($image, 'avatar');
                $cTable->setImage($thumbnail, 'thumb');
        } else {
                // This is for event recurring save option ( current / future event )
                $cTable->setImage($image, 'avatar', $saveAction);
                $cTable->setImage($thumbnail, 'thumb', $saveAction);
       }
        if($type == 'event') {
            
                if ($handler->isPublic()) {
                    $actor = $my->id;
                    $target = 0;
                    $content = '<img class="event-thumb" src="' . JURI::root(true) . '/' . $image . '" style="border: 1px solid #eee;margin-right: 3px;" />';
                    $cid = $cTable->id;
                    $app = 'events';
                    $act = $handler->getActivity('events.avatar.upload', $actor, $target, $content, $cid, $app);
                    $act->eventid = $cTable->id;

                    $params->set(
                            'event_url', $handler->getFormattedLink(
                                    'index.php?option=com_community&view=events&task=viewevent&eventid=' . $cTable->id, false, true, false
                            )
                    );


                    CActivityStream::add($act, $params->toString());
                }
                
                //add user points
                CUserPoints::assignPoint('event.avatar.upload');
        }
        $generateStream = true;
        switch ($type) {
            case 'profile':

                    /**
                     * Generate activity stream
                     * @todo Should we use CApiActivities::add
                     */
                    // do not have to generate a stream if the user is not the user itself (eg admin change user avatar)
                    if(CUserPoints::assignPoint('profile.avatar.upload') && $my->id == CFactory::getUser()->id){
                        $act = new stdClass();
                        $act->cmd = 'profile.avatar.upload';
                        $act->actor = $my->id;
                        $act->target = 0;
                        $act->title = '';
                        $act->content = '';
                        $act->access = $my->_cparams->get("privacyPhotoView", 0);
                        $act->app = 'profile.avatar.upload'; /* Profile app */
                        $act->cid = (isset($photo->id) && $photo->id) ? $photo->id : 0 ;
                        $act->verb = 'upload'; /* We uploaded new avatar - NOT change avatar */
                        $act->params = $params;
                        $params->set('photo_id', $photo->id);
                        $params->set('album_id', $photo->albumid);
                        $act->comment_id = CActivities::COMMENT_SELF;
                        $act->comment_type = 'profile.avatar.upload';
                        $act->like_id = CActivities::LIKE_SELF;
                        $act->like_type = 'profile.avatar.upload';
                    }
                    else{
                        $generateStream = false;
                    }
                    break;
            case 'event':
                //CUserPoints::assignPoint('events.avatar.upload'); @disabled since 4.0
                    /**
                     * Generate activity stream
                     * @todo Should we use CApiActivities::add
                     */
                    $act = new stdClass();
                    $act->cmd = 'events.avatar.upload';
                    $act->actor = $my->id;
                    $act->target = 0;
                    $act->title = '';
                    $act->content = '';
                    $act->app = 'events.avatar.upload'; /* Events app */
                    $act->cid = $id;
                    $act->eventid = $id;
                    $act->verb = 'update'; /* We do update */

                    $act->comment_id = CActivities::COMMENT_SELF;
                    $act->comment_type = 'events.avatar.upload';
                    $act->like_id = CActivities::LIKE_SELF;
                    $act->like_type = 'events.avatar.upload'; 
                break;
                case 'group':
                    /**
                     * Generate activity stream
                     * @todo Should we use CApiActivities::add
                     */
                    if(CUserPoints::assignPoint('group.avatar.upload')){
                        $act = new stdClass();
                        $act->cmd = 'groups.avatar.upload';
                        $act->actor = $my->id;
                        $act->target = 0;
                        $act->title = '';
                        $act->content = '';
                        $act->app = 'groups.avatar.upload'; /* Groups app */
                        $act->cid = $id;
                        $act->groupid = $id;
                        $act->verb = 'update'; /* We do update */
                        $params->set('photo_id', $photo->id);
                        $params->set('album_id', $photo->albumid);
                        
                        $act->comment_id = CActivities::COMMENT_SELF;
                        $act->comment_type = 'groups.avatar.upload';
                        $act->like_id = CActivities::LIKE_SELF;
                        $act->like_type = 'groups.avatar.upload';
                        $generateStream = true;
                    }else{
                        $generateStream = false;
                    }

                    break;
        }
        //we only generate stream if the uploader is the user himself, not admin or anyone else
        if (((isset($act) && $my->id == $id) || $type != 'profile') && $generateStream) {
            // $return = CApiActivities::add($act);

            /**
             * use internal Stream instead use for 3rd part API
             */
            $return = CActivityStream::add($act, $params->toString());

            //add the reference to the activity so that we can do something when someone update the avatar
            if ($type == 'profile') {
                // overwrite the params because some of the param might be updated through $my object above
                $cTableParams = $my->_cparams;
            } else {
                $cTableParams = new JRegistry($cTable->params);
            }

            $cTableParams->set('avatar_activity_id', $return->id);

            $cTable->params = $cTableParams->toString();
            $cTable->store();
        }
        if( $type != 'profile') {
            CActivityStream::add($act, $params->toString());
        }
        $response['status'] = true;
        $response['message'] = 'Upload success avatar';
        $response['path'] = $mainframe->getCfg('wwwrootfile') . '/' . str_replace($data_root . '/', '', $image);
        return $response;
    }
    
    // Begin upload cover photo
    private function uploadCover($id, $cover, $type) {
            $mainframe = JFactory::getApplication();
            $data_root = $mainframe->getCfg('dataroot');
            $config = CFactory::getConfig();
            $my = CFactory::getUser();
            $now = new JDate();
            
            
            if (!CImageHelper::checkImageSize(filesize($_FILES['cover']['tmp_name']))) {
                
                $response['status'] = false;
                $response['message'] = JText::sprintf('COM_COMMUNITY_VIDEOS_IMAGE_FILE_SIZE_EXCEEDED_MB',CFactory::getConfig()->get('maxuploadsize'));
                return $response;
            }
            $ext = pathinfo($_FILES['cover']['name'], PATHINFO_EXTENSION);
            if ($_FILES['cover']['type'] == "application/octet-stream" && ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG')) {
                if ($ext == 'jpeg') {
                    $image_type = 'image/jpeg';
                }
                if ($ext == 'jpg') {
                    $image_type = 'image/jpg';
                }
                if ($ext == 'JPG') {
                    $image_type = 'image/JPG';
                }
            } elseif ($_FILES['cover']['type'] == "application/octet-stream" && ($ext == 'png' || $ext == 'PNG')) {
                $image_type = 'image/png';
            } else {
                $image_type = $_FILES['cover']['type'];
            }
            //check if file is allwoed
            /*if (!CImageHelper::isValidType($_FILES['cover']['type'])) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_IMAGE_FILE_NOT_SUPPORTED');
                return $response;
            }*/
            CImageHelper::autoRotate($_FILES['cover']['tmp_name']);

            $album = JTable::getInstance('Album', 'CTable');

            if (!$albumId = $album->isCoverExist($type, $id)) {
                $albumId = $album->addCoverAlbum($type, $id);
            }

            $imgMaxWidht = 1140;

            // Get a hash for the file name.
            $fileName = JApplication::getHash($_FILES['cover']['tmp_name'] . time());
            $hashFileName = JString::substr($fileName, 0, 24);
            
            if (!JFolder::exists(
                $data_root . '/' . $config->getString('imagefolder') . '/cover/' . $type . '/' . $id . '/'
            )
            ) {
                JFolder::create(
                    $data_root . '/' . $config->getString('imagefolder') . '/cover/' . $type . '/' . $id . '/'
                );
            }
            $dest = $data_root . '/' . $config->getString('imagefolder') . '/cover/' . $type . '/' . $id . '/' . md5(
                    $type . '_cover' . time()
                ) . CImageHelper::getExtension($image_type);
            
            $thumbPath = $data_root . '/' . $config->getString(
                    'imagefolder'
                ) . '/cover/' . $type . '/' . $id . '/thumb_' . md5(
                    $type . '_cover' . time()
                ) . CImageHelper::getExtension($image_type);
            $cover  = $data_root . '/' . $config->getString(
                    'imagefolder'
                ) . '/cover/' . $type . '/' . $id . '/' . md5(
                    $type . '_cover' . time()
                ) . CImageHelper::getExtension($image_type);
            
            if (!CImageHelper::resizeProportional($_FILES['cover']['tmp_name'], $dest, $image_type, $imgMaxWidht)) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE');
                return $response;
            }
            
            CPhotos::generateThumbnail($_FILES['cover']['tmp_name'], $thumbPath, '');

            $cTable = JTable::getInstance(ucfirst($type), 'CTable');
            $cTable->load($id);
            
            if ($cTable->setCover(str_replace($data_root . '/', '', $dest))) {
                $photo = JTable::getInstance('Photo', 'CTable');

                $photo->albumid = $albumId;
//                $photo->image = str_replace(JPATH_ROOT . '/', '', $dest);
                $photo->image = str_replace($data_root . '/', '', $dest);
                $photo->caption = $_FILES['cover']['name'];
                $photo->filesize = $_FILES['cover']['size'];
                $photo->creator = $my->id;
                $photo->created = $now->toSql();
                $photo->published = 1;
//                $photo->thumbnail = str_replace(JPATH_ROOT . '/', '', $thumbPath);
                $photo->thumbnail = str_replace($data_root . '/', '', $thumbPath);

                if ($photo->store()) {
                    $album->load($albumId);
                    $album->photoid = $photo->id;
                    $album->store();
                }

                $response['status'] = true;
//                $msg['path'] = JURI::root() . str_replace(JPATH_ROOT . '/', '', $dest);
                $response['message'] = 'Upload success cover';
                $response['path'] = $mainframe->getCfg('wwwrootfile') . '/' . str_replace($data_root . '/', '', $dest);

                // Generate activity stream.
                $act = new stdClass();
                $act->cmd = 'cover.upload';
                $act->actor = $my->id;
                $act->target = 0;
                $act->title = '';
                $act->content = '';
                $act->access = ($type == 'profile') ? $my->_cparams->get("privacyPhotoView") : 0;
                $act->app = 'cover.upload';
                $act->cid = $photo->id;
                $act->comment_id = CActivities::COMMENT_SELF;
                $act->comment_type = 'cover.upload';
                $act->groupid = ($type == 'group') ? $id : 0;
                $act->eventid = ($type == 'event') ? $id : 0;
                $act->group_access = ($type == 'group') ? $cTable->approvals : 0;
                $act->event_access = ($type == 'event') ? $cTable->permission : 0;
                $act->like_id = CActivities::LIKE_SELF;;
                $act->like_type = 'cover.upload';

                $params = new JRegistry();
//                $params->set('attachment', str_replace(JPATH_ROOT . '/', '', $dest));
                $params->set('attachment', str_replace($data_root . '/', '', $dest));
                $params->set('type', $type);
                $params->set('album_id', $albumId);
                $params->set('photo_id', $photo->id);

                //assign points based on types.
                switch($type){
                    case 'group':
                        $addStream = CUserPoints::assignPoint('group.cover.upload');
                        break;
                    case 'event':
                        $addStream =  CUserPoints::assignPoint('event.cover.upload');
                        break;
                    default:
                        $addStream = CUserPoints::assignPoint('profile.cover.upload');
                }

                if ($type == 'event') {
                    $event = JTable::getInstance('Event', 'CTable');
                    $event->load($id);

                    $group = JTable::getInstance('Group', 'CTable');
                    $group->load($event->contentid);

                    if ($group->approvals == 1) {
                        $addStream = false;
                    }
                }

                if ($addStream) {
                    // Add activity logging
                    if( $type != 'profile' || ($type=='profile' && $id == $my->id) ) {
                        CActivityStream::add($act, $params->toString());
                    }
                }
                return $response;
            }
    }
    public function testResize($orgW, $orgH, $newW, $newH, $minVal, $maxVal)
        {
            $newValue = 0;
            if ($newH == 0) {
                /* New height value */
                $newValue = round(($newW * $orgH) / $orgW);
            } elseif ($newW == 0) {
                /* New width value */
                $newValue = round(($newH * $orgW) / $orgH);
            } else {
                return false;
            }
            return ($newValue >= $minVal && $newValue <= $maxVal) ? true : false;
        }
    private function goingEvent($id, $username, $status) {
        if(empty($id)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Event ID can not blank';
            return $reponse;
        }
        if(empty($username)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Username can not blank';
            return $reponse;
        }
        $status_array = array('1', '2', '3');
        if(!in_array($status, $status_array)) {
            $reponse['status'] = false;
            $reponse['message'] = 'Status Invalid';
            return $reponse;
        }
        $event = JTable::getInstance('Event', 'CTable');
        $event->load($id);
        
        $my = CFactory::getUser();
        $invitedCount = 0;
        
        $user_id = JUserHelper::getUserId($username);
        
        $date = JFactory::getDate();
        $eventMember = JTable::getInstance('EventMembers', 'CTable');
        $eventMember->eventid = $event->id;
        $eventMember->memberid = $user_id;
        $eventMember->status = $status;
        $eventMember->invited_by = $my->id;
        $eventMember->permission = 3;
        $eventMember->created = $date->toSql();

        if($eventMember->store()) {
            $invitedCount++;

            //now update the invited count in event
            $event->invitedcount = $event->invitedcount + $invitedCount;
            $event->store();
            $reponse['status'] = true;
            $reponse['message'] = 'Going event success';
        } else {
            $reponse['status'] = false;
            $reponse['message'] = 'Going event failed';
        }
        return $reponse;
    }
    private function myEvent() {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        
        $model = CFactory::getModel('events');
        $my = CFactory::getUser();
        $sorted = 'startdate';
        $events = $model->getEvents(null, $my->id, $sorted);
        
        $eventMembers = JTable::getInstance('EventMembers', 'CTable');
        
        $response = new stdClass();
        if($events) {
        foreach ($events as $event) {
            $keys = array('eventId' => $event->id, 'memberId' => $my->id);
            $eventMembers->load($keys);
            $data = array();
            $data['id'] = $event->id;
            $data['title'] = $event->title;
            $data['group'] = $event->contentid;
            $data['description'] = $event->description;
            $data['location'] = $event->location;
            $data['startdate'] = $event->startdate;
            $data['enddate'] = $event->enddate;
            $data['created'] = $event->created;
            $data['status'] = $eventMembers->status;
            $user_attend = CFactory::getUser($event->creator);
            $data['owner'] = $user_attend->getDisplayName();
            $datas[] = $data;
        }
            $response->status = true;
            $response->message = 'Event loaded';
            $response->events = $datas;
        } else {
            $response->status = false;
            $response->message = 'Event not loaded';
            $response->events = $datas;
        }
        return $response;
    }
    private function addAnnouncement($groupid, $title, $message, $enddate, $endtime, $id) {
        
        $my = CFactory::getUser();
        
        // check input
        if(empty($groupid)) {
            $response['status'] = false;
            $response['message'] = 'Circle does not empty.';
            return $response;
        }
        if(empty($title)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_GROUPS_BULLETIN_EMPTY');
            return $response;
        }
        if(empty($message)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_GROUPS_BULLETIN_BODY_EMPTY');
            return $response;
        }
        if(empty($enddate)) {
            $response['status'] = false;
            $response['message'] = 'End date can not blank.';
            return $response;
        }
        if(empty($endtime)) {
            $response['status'] = false;
            $response['message'] = 'End time can not blank.';
            return $response;
        }
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        $params = new CParameter('');
        
        $bulletin = JTable::getInstance('Bulletin', 'CTable');
        if($id) {
            $bulletin->load($id);
            $bulletin->params = $params->toString();
            if (!empty($bulletin->endtime)) {
                    $enddate = date('d-m-Y', $bulletin->endtime);
                    $bulletin->endtime = strtotime($enddate.' '.$endtime);
            } 
        }
        $bulletin->title = $title;
        $bulletin->message = $message;
        
        $bulletin->groupid = $groupid;
        $bulletin->date = gmdate('Y-m-d H:i:s');
        $bulletin->created_by = $my->id;
        $bulletin->endtime = strtotime($enddate).' '.$endtime;
        $bulletin->store();
        
        // Send notification to all user
        $model = CFactory::getModel('groups');
        $memberCount = $model->getMembersCount($groupid);
        $members = $model->getMembers($groupid, $memberCount, true, false, SHOW_GROUP_ADMIN);

        $membersArray = array();

        foreach ($members as $row) {
            $membersArray[] = $row->id;
        }
        unset($members);
        $params->set('url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupid);
        $params->set('group', $group->name);
        $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupid);
        $params->set('subject', $bulletin->title);
        $params->set('announcement', $bulletin->title);
        $params->set('announcement_url', 'index.php?option=com_community&view=groups&task=viewbulletin&groupid=' . $group->id . '&bulletinid=' . $bulletin->id);

        CNotificationLibrary::add('groups_create_news', $my->id, $membersArray, JText::sprintf('COM_COMMUNITY_GROUPS_EMAIL_NEW_BULLETIN_SUBJECT'), '', 'groups.bulletin', $params);
        
        // Put notification via google gcm
        // For Android
        require_once('PushNotifications.php');
	$regId = 'dkxoe9Bz0No:APA91bGlAiKGhq2ApvK6qwiO-JyxuekN6pjKD81H1fCVxJnaqNqT5gPhLGrbDNsRq4gYmUELKlGV2VpKm1DJ3yaRbDO7vuDeVXwKASQXMl3n79S_UYb6bDwfAtYgho6rHSzf2DWX_Kbu';
        $message_send = array(
            'mtitle' => JText::_('COM_COMMUNITY_GROUPS_EMAIL_NEW_BULLETIN_SUBJECT'),
            'mdesc'  => $bulletin->title
        );
        PushNotifications::android($message_send, $regId);
        // Add activity logging

        $act = new stdClass();
        $act->cmd = 'group.news.create';
        $act->actor = $my->id;
        $act->target = 0;
        $act->title = ''; //JText::sprintf('COM_COMMUNITY_GROUPS_NEW_GROUP_NEWS' , '{group_url}' , $bulletin->title );
        $act->content = ( $group->approvals == 0 ) ? JString::substr(strip_tags($bulletin->message), 0, 100) : '';
        $act->app = 'groups.bulletin';
        $act->cid = $bulletin->id;
        $act->groupid = $group->id;
        $act->group_access = $group->approvals;

        $act->comment_id = CActivities::COMMENT_SELF;
        $act->comment_type = 'groups.bulletin';
        $act->like_id = CActivities::LIKE_SELF;
        $act->like_type = 'groups.bulletin';

        $params = new CParameter('');
//              $params->set( 'group_url' , 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id );
        $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewbulletin&groupid=' . $group->id . '&bulletinid=' . $bulletin->id);


        CActivityStream::add($act, $params->toString());

        //add user points
        CUserPoints::assignPoint('group.news.create');
        $response['status'] = true;
        $response['message'] = 'Announcement added.';
        return $response;
                
    }
    
    private function deleteAnnouncement($groupid, $announcementid) {
        $my = CFactory::getUser();
        if ($my->id == 0) {
            return $this->blockUnregister();
        }
        if (empty($announcementid) || empty($groupid)) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_INVALID_ID');
            return $response;
        }
        $groupsModel = CFactory::getModel('groups');
        $bulletin = JTable::getInstance('Bulletin', 'CTable');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        
        $fileModel = CFactory::getModel('files');
        
        if ($groupsModel->isAdmin($my->id, $group->id) || COwnerHelper::isCommunityAdmin()) {
            $bulletin->load($announcementid);
            if ($bulletin->delete()) {

                //add user points
                //CFactory::load( 'libraries' , 'userpoints' );
                CUserPoints::assignPoint('group.news.remove');
                CActivityStream::remove('groups.bulletin', $announcementid);

                // Remove Bulletin Files
                $fileModel->alldelete($announcementid, 'bulletin');
                
                $response['status'] = true;
                $response['message'] = JText::_('COM_COMMUNITY_BULLETIN_REMOVED');
                return $response;
            }
        } else {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_DELETE_WARNING');
            return $response;
        }
    }


    private function myFriends($courseid = null) {
        
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        
        $my = CFactory::getUser();
        
        $friends = CFactory::getModel('friends');
        
        $sorted = 'latest';
        $filter = 'all';
        $rows = $friends->getFriends($my->id, $sorted, true, $filter);
        $blockModel = CFactory::getModel('block');
        
        $resultRows = array();
        if($rows) {
        foreach ($rows as $row) {
            $user = CFactory::getUser($row->id);
            
            $obj = new stdClass();
            //display online friends only.
            if($onlineFriendsOnly && !$user->isOnline()){
                continue;
            }
            $obj->id = $row->id;
            $obj->username = $row->username;
            $obj->displayname = $user->getDisplayName();
            $avatar = $user->_avatar;
            $cover = $user->_cover;
            if($avatar == '') {
                 $obj->avatar = '';//JURI::base() . 'components/com_community/assets/user-Male-thumb.png'
            } else {
                $obj->avatar = $root.'/'.$avatar;
            }
            if($cover == '') {
                $obj->cover = '';//JURI::base() . 'components/com_community/assets/cover-undefined-default.png'
            } else {
                $obj->cover = $root.'/'.$cover;
            }
            $obj->friendsCount = $user->getFriendCount();
            $obj->profileLink = CUrlHelper::userLink($row->id);
            $obj->isFriend = true;
            $obj->isBlocked = $blockModel->getBlockStatus($user->id, $my->id);
            $obj->roles = array();
            // check user role 
            if(!empty($courseid)) {
                require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');
                $roles = JoomdleHelperContent::call_method('get_user_role', (int)$courseid, $row->username);
                $obj->roles = $roles['roles'][0]['role'];
            }

            $resultRows[] = $obj;
        }
        unset($rows);
        
            $response['status'] = true;
            $response['message'] = 'Friend loaded';
            $response['myFriend'] = $resultRows;
        } else {
            $response['status'] = true;
            $response['message'] = 'You have no friends yet.';
            $response['myFriend'] = $resultRows;
        }
       return $response;
    }
    
    private function allFriend($limit, $page) {
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        
        $my = CFactory::getUser();
        $searchModel = CFactory::getModel('search');
        $userModel = CFactory::getModel('user');
        
        $position = ($page - 1) * $limit;
        
        $sorted = 'latest';
        $filter = 'all';
        $rows = $searchModel->getPeople($sorted, $filter, 0, $position,$limit, $my->id);
        $blockModel = CFactory::getModel('block');
        $model = CFactory::getModel('friends');
        
        $resultRows = array();
        if($rows) {
        foreach ($rows as $row) {
            $user = CFactory::getUser($row->id);
            $connection = $model->getFriendConnection($my->id, $row->id);
            $obj = new stdClass();
            //display online friends only.
            if($onlineFriendsOnly && !$user->isOnline()){
                continue;
            }
            $obj->id = $row->id;
            $obj->username = $row->username;
            $obj->displayname = $user->getDisplayName();
            $avatar = $user->_avatar;
            $cover = $user->_cover;
            $obj->gender = $user->_gender;
            if($avatar == '') {
                 $obj->avatar = '';//JURI::base() . 'components/com_community/assets/user-Male-thumb.png'
            } else {
                $obj->avatar = $root.'/'.$avatar;
            }
            if($cover == '') {
                $obj->cover = '';//JURI::base() . 'components/com_community/assets/cover-undefined-default.png'
            } else {
                $obj->cover = $root.'/'.$cover;
            }
            $isFriend = CFriendsHelper::isConnected($row->id, $my->id);
            
//            if($isFriend == true) {
//                continue;
//            }
            $Friend = false;
            if($connection && $connection[0]->status == 1 && $connection[0]->msg == '') {
                $Friend = true;
            }
            $obj->friendsCount = $user->getFriendCount();
            $obj->profileLink = CUrlHelper::userLink($row->id);
            $obj->isFriend = $Friend;
            $obj->isBlocked = $blockModel->getBlockStatus($user->id, $my->id);
            $obj->requestMessage = '';
            $obj->pendingRequest = false;
            if($connection && !$isFriend && $connection[0]->status != 1) {
                $obj->pendingRequest = true;
                $obj->requestMessage = $connection[0]->msg;
            }
            $obj->myRequest = false;
            if($my->id == $connection[0]->connect_from && !$isFriend) {
                $obj->myRequest = true;
            }

            $resultRows[] = $obj;
        }
            unset($rows);
            $response['status'] = true;
            $response['message'] = 'All friend loaded';
            $response['allFriend'] = $resultRows;
        } else {
            $response['status'] = false;
            $response['message'] = 'All friend not loaded';
            $response['allFriend'] = $resultRows;
        }
        return $response;
    }
    private function searchFriend($keyword, $type, $page, $limit) {
        
            $mainframe = JFactory::getApplication();
            $root = $mainframe->getCfg('wwwrootfile');
            
            $modelFriend = CFactory::getModel('friends');
            
            $my = CFactory::getUser();
            if($type == 'all') {
                $model = CFactory::getModel('search');
                $search = array(
                    'q' => $keyword,
                    'search' => 'search'
                );
                $rows = $model->searchPeople($search, '', false, $page, $limit);
            } elseif($type == 'my') {
                $rows = $modelFriend->searchPeople($keyword);
            }
            $blockModel = CFactory::getModel('block');
            $resultRows = array();
            if($rows) {
            foreach ($rows as $row) {
                $user = CFactory::getUser($row->id);
                
                $connection = $modelFriend->getFriendConnection($my->id, $row->id);
                
                $obj = new stdClass();
                //display online friends only.
                if($onlineFriendsOnly && !$user->isOnline()){
                    continue;
                }
                $obj->id = $row->id;
                $obj->username = $row->username;
                $obj->displayname = $user->getDisplayName();
                $avatar = $user->_avatar;
                $cover = $user->_cover;
                if($avatar == '') {
                     $obj->avatar = '';//JURI::base() . 'components/com_community/assets/user-Male-thumb.png'
                } else {
                    $obj->avatar = $root.'/'.$avatar;
                }
                if($cover == '') {
                    $obj->cover = '';//JURI::base() . 'components/com_community/assets/cover-undefined-default.png'
                } else {
                    $obj->cover = $root.'/'.$cover;
                }
                $isFriend = CFriendsHelper::isConnected($row->id, $my->id);
                
                $obj->friendsCount = $user->getFriendCount();
                $obj->profileLink = CUrlHelper::userLink($row->id);
                $obj->isFriend = $isFriend;
                $obj->isBlocked = $blockModel->getBlockStatus($user->id, $my->id);
                $obj->pendingRequest = false;
                $obj->requestMessage = '';
                $obj->myRequest = false;
                if($my->id == $connection[0]->connect_from && !$isFriend) {
                    $obj->myRequest = true;
                }
                if($connection && !$isFriend) {
                    $obj->pendingRequest = true;
                    $obj->requestMessage = $connection[0]->msg;
                }
                $resultRows[] = $obj;
         }
            unset($rows);
            $response['status'] = true;
            $response['message'] = 'Friend search success';
            $response['searchFriend'] = $resultRows;
         } else {
            $response['status'] = false;
            $response['message'] = 'Friend search failed';
            $response['searchFriend'] = $resultRows;   
         }
        return $response;
    }
    
    
    private function addFriend($friendId, $message) {
        
        $model = CFactory::getModel('friends');
        $user = CFactory::getUser($friendId);
        $my = CFactory::getUser();
        
        if ($my->id == $friendId) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_FRIENDS_CANNOT_ADD_SELF');
            return $response;
        }
        $connection = $model->getFriendConnection($my->id, $friendId);
        if($model->addFriend($user->id, $my->id, $message)) {
            $params = new CParameter('');
            $params->set('url', 'index.php?option=com_community&view=friends&task=pending');
            $params->set('msg', $message);

            CNotificationLibrary::add(
                'friends_request_connection',
                $my->id,
                $friendId,
                JText::_('COM_COMMUNITY_FRIEND_ADD_REQUEST'),
                '',
                'friends.request',
                $params
            );
            //add user points - friends.request.add removed @ 20090313
            //trigger for onFriendRequest
            $eventObject = new stdClass();
            $eventObject->profileOwnerId = $my->id;
            $eventObject->friendId = $friendId;
            $this->triggerFriendEvents('onFriendRequest', $eventObject);
            unset($eventObject);
            
            $response['status'] = true;
            $response['message'] = 'Requested to be your friend';
            
        } else {
            $response['status'] = false;
            $response['message'] = 'Friend requested send failed';
        }
        return $response;
    }
    
    public function triggerFriendEvents($eventName, &$args, $target = null)
    {
        CError::assert($args, 'object', 'istype', __FILE__, __LINE__);

        require_once(COMMUNITY_COM_PATH . '/libraries/apps.php');
        $appsLib = CAppPlugins::getInstance();
        $appsLib->loadApplications();

        $params = array();
        $params[] = $args;

        if (!is_null($target)) {
            $params[] = $target;
        }

        $appsLib->triggerEvent($eventName, $params);
        return true;
    }
    private function approvedFriend($friendid, $type_request) {
        $model = CFactory::getModel('friends');
        $user = CFactory::getUser($friendid);
        $my = CFactory::getUser();
        
        if(!$user) {
            $response['status'] = false;
            $response['message'] = 'Friend doesn\'t exits.';
            return $response;
        }
        if ($my->id == $friendid) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_FRIENDS_CANNOT_ADD_SELF');
            return $response;
        }
        $connection = $model->getFriendConnection($my->id, $friendid);
        if($connection) {
            if($type_request == 'approve') { 
                if($connected = $model->approveRequest($connection[0]->connection_id)) {
                    $act = new stdClass();
                    $act->cmd = 'friends.request.approve';
                    $act->actor = $connected[0];
                    $act->target = $connected[1];
                    $act->title = ''; //JText::_('COM_COMMUNITY_ACTIVITY_FRIENDS_NOW');
                    $act->content = '';
                    $act->app = 'friends.connect';
                    $act->cid = 0;

                    //add user points - give points to both parties
                    //CFactory::load( 'libraries' , 'userpoints' );
                    if(CUserPoints::assignPoint('friends.request.approve')){
                        CActivityStream::add($act);
                    };

                    $friendid = ($connected[0] == $my->id) ? $connected[1] : $connected[0];
                    $friend = CFactory::getUser($friendid);
                    $friendUrl = CRoute::_('index.php?option=com_community&view=profile&userid=' . $friendId);
                    CUserPoints::assignPoint('friends.request.approve', $friendid);

                    // need to both user's friend list
                    $model->updateFriendCount($my->id);
                    $model->updateFriendCount($friendId);

                    $params = new CParameter('');
                    $params->set('url', 'index.php?option=com_community&view=profile&userid=' . $my->id);
                    $params->set('friend', $my->getDisplayName());
                    $params->set('friend_url', 'index.php?option=com_community&view=profile&userid=' . $my->id);

                    CNotificationLibrary::add(
                        'friends_create_connection',
                        $my->id,
                        $friend->id,
                        JText::_('COM_COMMUNITY_FRIEND_REQUEST_APPROVED'),
                        '',
                        'friends.approve',
                        $params
                    );

                    //trigger for onFriendApprove
                    $eventObject = new stdClass();
                    $eventObject->profileOwnerId = $my->id;
                    $eventObject->friendId = $friendid;
                    $this->triggerFriendEvents('onFriendApprove', $eventObject);
                    unset($eventObject);
                    $response['status'] = true;
                    $response['message'] = 'Friend approve request success';
                    return $response;
                } else {
                    $response['status'] = false;
                    $response['message'] = 'Friend request approve failed';
                    return $response;
                }
            } elseif($type_request == 'reject') {
                if($model->rejectRequest($connection[0]->connection_id)) {
                    $response['status'] = true;
                    $response['message'] = JText::_('COM_COMMUNITY_FRIEND_REQUEST_DECLINED');
                    //trigger for onFriendReject
                    $eventObject = new stdClass();
                    $eventObject->profileOwnerId = $my->id;
                    $eventObject->friendId = $friendid;
                    $this->triggerFriendEvents('onFriendReject', $eventObject);
                    unset($eventObject);
                    return $response;
                } else {
                    $response['status'] = false;
                    $response['message'] = 'Friend reject request failed';
                    return $response;
                }
            } else {
                $response['status'] = false;
                $response['message'] = 'Request type invalid. Please check request.';
                return $response;
            }
        } else {
            $response['status'] = false;
            $response['message'] = 'Friend request not avilable';
            return $response;
        }
    }
    
    private function removeFriend($friendid, $block) {
        $my = CFactory::getUser();
        $friend = CFactory::getUser($friendid);
        
        if (empty($my->id) || empty($friend->id)) {
            return false;
        }

        //CFactory::load( 'helpers' , 'friends' );
        $isFriend = $my->isFriendWith($friend->id);
        if (!$isFriend) {
            return true;
        }

        $model = CFactory::getModel('friends');
        
        if ($model->deleteFriend($my->id, $friend->id)) {
            // block user
            if($block == 1) {
                $Blockmodel = CFactory::getModel('block');

                $Blockmodel->blockUser($my->id, $friend->id);
            }
        if ($model->deleteFriend($my->id, $friend->id)) {
            
            // Substract the friend count
            $model->updateFriendCount($my->id);
            $model->updateFriendCount($friend->id);


            // Update friend list of both current user and friend.
            $friend->updateFriendList(true);
            $my->updateFriendList(true);


            // Add user points
            // We deduct points to both parties
            //CFactory::load( 'libraries' , 'userpoints' );
            CUserPoints::assignPoint('friends.remove');
            CUserPoints::assignPoint('friends.remove', $friend->id);

            // Trigger for onFriendRemove
            $eventObject = new stdClass();
            $eventObject->profileOwnerId = $my->id;
            $eventObject->friendId = $friend->id;
            $this->triggerFriendEvents('onFriendRemove', $eventObject);
            unset($eventObject);
            $response['status'] = true;
            $response['message'] = JText::_('COM_COMMUNITY_FRIENDS_REMOVED');
            return $response;
        } else {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_FRIENDS_REMOVING_FRIEND_ERROR');
            return $response;
        }
    }
    
}
