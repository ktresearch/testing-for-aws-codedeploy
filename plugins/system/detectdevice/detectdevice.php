<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

/**
 * Detect Device Plugin
 */
class plgSystemDetectdevice extends JPlugin
{
    public function onAfterInitialise()
    {
        include_once( dirname(__FILE__) . '/lib/Mobile_Detect.php' );

        $jinput = JFactory::getApplication()->input;
        $type = $jinput->get('type', '', 'STRING');
        if (!$type)
            $type = isset($_COOKIE['app']) ? $_COOKIE['app'] : '';

        if ( class_exists( 'Mobile_Detect' ) )  {
            $detect = new Mobile_Detect();
            $layout = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'mobile') : 'desktop');
            if ( $detect->is('Bot') || $detect->is('MobileBot') ) $layout = 'bot';

            // store user agent layout in session variable.
            $session = JFactory::getSession();
            $session->set('device', $layout);
        } else {
            // class not present, default to desktop
            $session = JFactory::getSession();
            $session->set('device', 'desktop');
        }

        $detect_layout = $session->get('device');
        if($detect_layout == "tablet" || $detect_layout == "desktop") {
            $tpl = 't3_bs3_tablet_desktop_template';
            $app = JFactory::getApplication();
            if ($app->isAdmin()) {
                return;
            }
            $app->setTemplate($tpl);
        }

        if ($type == 'appview') {
            $tpl = 't3_bs3_appview_template';
            $app = &JFactory::getApplication();
            setcookie('app', 'appview');
            if ($app->isAdmin()) {
                return;
            }
            $app->setTemplate($tpl);
        }
    }
}