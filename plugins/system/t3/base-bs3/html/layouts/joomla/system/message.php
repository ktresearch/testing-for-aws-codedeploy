<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

$msgList = $displayData['msgList'];
$array_check = ['Circle updated','A notification has been sent to the users','Announcement added','Announcement updated successfully.','Announcement removed successfully.','Discussion added','Discussion Updated','Discussion removed successfully.','You have successfully left the circle'];
$check = $msgList['message'][0];
$array_msg = ['Failed sending email.','Sorry, this circle is under moderation by the administrator.'];
$array_reset = ['Completing reset password failed: Invalid field: Confirm Password','Reset password failed: Invalid email address','Your password reset confirmation failed because the verification code was invalid. User not found.',
    'Completing reset password failed: The passwords you entered do not match. Please enter your desired password in the password field and confirm your entry by entering it in the confirm password field.'];
if (in_array($check, $array_check) || (strpos($check, 'created!') !== false && strpos($check, 'New event') !== false)) {

}else{
    ?>
    <div id="system-message-container">
        <?php if (is_array($msgList) && !empty($msgList)) : ?>
            <?php if($msgList['warning'][0] == 'Username and password do not match or you do not have an account yet.'){ ?>
                <input type="hidden" id="check_value" value="1"/>
            <?php }else{?>
                <div id="system-message">
                    <?php foreach ($msgList as $type => $msgs) : ?>
                        <?php if (!empty($msgs)) : ?>
                            <div class="hienPopup">
                                <?php foreach ($msgs as $msg) : 
                                      $checkno = false;
                                      if(in_array($msg, $array_msg)) $checkno = true;
                                      if(in_array($msg, $array_reset)){ ?>
                                         <input type="hidden" id="check_value" value="1"/>
                                      <?php }?>
                                    <p><?php echo $msg; ?></p>
                                <?php endforeach; ?>
                                <button class = "closepopup"data-dismiss="alert">CLOSE</button>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            <?php }?>
        <?php endif; ?>
    </div>
<?php  } ?>
<script>
    if(<?php echo isset($checkno) && $checkno ? 1 : 0 ?>)
        jQuery('body').addClass('overlay2');
    
    jQuery('.closepopup').click(function(){
        jQuery('.hienPopup').hide();
        jQuery('body').removeClass('overlay2');
    });
    check = jQuery('#check_value').val();
    if(check == 1){
        jQuery('.hienPopup').hide();
    }
</script>