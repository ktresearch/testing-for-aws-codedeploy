<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');

$token =  $_GET['token'];
?>
<div class="signup-back">
    <a href="javascript:void(0);" onclick="goBack();"> <img src="<?php echo JURI::root();?>images/backbutton.png"></a>
</div>
<div class="reset-confirm-page reset-confirm<?php echo $this->pageclass_sfx?>">

    <h4><?php echo JText::_('COM_USERS_VERIFICATION_IS_REQUIRED'); ?></h4>

    <form action="<?php echo JRoute::_('index.php?option=com_users&task=reset.confirm'); ?>" method="post" class="form-validate">
        <?php foreach ($this->form->getFieldsets() as $fieldset): ?>
            <p class="reset-message"><?php echo JText::_($fieldset->label); ?></p>		
            <fieldset>
                <?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field): ?>
                    <!--				<dt>--><?php //echo $field->label; ?><!--</dt>-->
                    <div class="form-group">
                        <?php echo $field->input; ?>
                    </div>
                <?php endforeach; ?>
            </fieldset>
        <?php endforeach; ?>
        <div class="form-group">
            <div class="form-actions">
                <button type="submit" class="validate btsave"><?php echo JText::_('JSUBMIT'); ?></button>
                <?php echo JHtml::_('form.token'); ?>
            </div>
        </div>
    </form>
</div>
<div class="div-footer-menu">
    <ul>
        <li><a href="<?php echo JURI::root(true); ?>/about"><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/terms"><?php echo JText::_('COM_COMMUNITY_TERMS'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/privacy-policy"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/support"><?php echo JText::_('COM_COMMUNITY_SUPPORT'); ?></a></li>
        <!-- <li><a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/business"><?php echo JText::_('COM_COMMUNITY_BUSINESS'); ?></a></li> -->
    </ul>
</div>
<div class="divCopyright">
    <p>© <?php echo Date("Y", time()).' '.JFactory::getApplication()->get('sitename');?> </p>
</div>
<script type="text/javascript">
        var check = jQuery('#check_value').val();
        if(check == 1) {
            jQuery('#jform_email').parents('.form-group').addClass('hasError');
            jQuery('#jform_token').parents('.form-group').addClass('hasError');
           
        }
        token = '<?php echo $token; ?>';
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
           return regex.test(email);
        }
         
         function validateInput(){
        var vali =true;
        if(jQuery('#jform_email').val().trim() == ''){
             jQuery('#jform_email').parents('.form-group').addClass('hasError');
             vali = false;
        }
        else{
            jQuery('#jform_email').parents('.form-group').removeClass('hasError');
        }
        if(jQuery('#jform_token').val() == ''){
             jQuery('#jform_token').parents('.form-group').addClass('hasError');
             vali = false;
        }
        else{
            jQuery('#jform_token').parents('.form-group').removeClass('hasError');
        }
        return vali;
    }
    jQuery('input').on('keyup', function(){
        jQuery('.form-group').removeClass('hasError');
    })

    jQuery('.btsave').on('click', function(event){
        event.stopPropagation();
        event.preventDefault();
        em = jQuery('#jform_email').val();        
        if(!isEmail(em) && validateInput())
                jQuery('#jform_email').parents('.form-group').addClass('hasError');
            else  if(jQuery('#jform_token').val() != token  && validateInput())
            jQuery('#jform_token').parents('.form-group').addClass('hasError');
        else if(validateInput())
            jQuery('form').submit();
    
    });
    function goBack() {
        // window.history.back();
        var nav = window.navigator;
        if( this.phonegapNavigationEnabled &&
            nav &&
            nav.app &&
            nav.app.backHistory ){
            nav.app.backHistory();
        } else {
            window.history.back();
        }
    }
</script>
