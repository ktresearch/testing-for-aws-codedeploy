<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
if(version_compare(JVERSION, '3.0', 'lt')){
    JHtml::_('behavior.tooltip');
}
JHtml::_('behavior.formvalidation');
?>
<div class="signup-back">
    <a href="javascript:void(0);" onclick="goBack();"> <img src="<?php echo JURI::root();?>images/backbutton.png"></a>
</div>
<div class="reset reset-page <?php echo $this->pageclass_sfx?>">
    <div class="page-header">
        <h4><?php echo $this->escape($this->params->get('page_heading')); ?></h4>
    </div>

    <form id="user-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request'); ?>" method="post" class="form-validate form-horizontal">

        <?php foreach ($this->form->getFieldsets() as $fieldset): ?>
            <p class="reset-message"><?php echo JText::_($fieldset->label); ?></p>

            <fieldset>
                <?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field): ?>
                    <div class="form-group">
                        <!--					<div class="col-sm-3 control-label">-->
                        <!--                        --><?php ////echo $field->label; ?>
                        <!--                    </div>-->
                        <!--					<div class="col-sm-9">-->
                        <?php echo $field->input; ?>
                        <!--                    </div>-->
                    </div>
                <?php endforeach; ?>
            </fieldset>
        <?php endforeach; ?>

        <div class="form-group">
            <div>
                <button type="submit" class="btn btn-primary validate btsave"><?php echo JText::_('JSUBMIT'); ?></button>
                <?php echo JHtml::_('form.token'); ?>
            </div>
        </div>
    </form>
</div>
<div class="div-footer-menu">
    <ul>
        <li><a href="<?php echo JURI::root(true); ?>/about"><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/terms"><?php echo JText::_('COM_COMMUNITY_TERMS'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/privacy-policy"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/support"><?php echo JText::_('COM_COMMUNITY_SUPPORT'); ?></a></li>
        <!-- <li><a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/business"><?php echo JText::_('COM_COMMUNITY_BUSINESS'); ?></a></li> -->
    </ul>
</div>
<div class="divCopyright">
    <p>© <?php echo Date("Y", time()).' '.JFactory::getApplication()->get('sitename');?> </p>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
    var check = jQuery('#check_value').val();
    if(check == 1) jQuery('.validate-username').parents('.form-group').addClass('hasError');
    
    function isEmail(email) {
         var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    jQuery('.validate-username').on('keyup', function(){
        jQuery('.form-group').removeClass('hasError');
    });

    jQuery('.btsave').on('click', function(event){
        event.stopPropagation();
        event.preventDefault();
        em = jQuery('.validate-username').val();
        if(jQuery('.validate-username').val().trim() != '' && isEmail(em) )
            jQuery('form').submit();
        else jQuery('.validate-username').parents('.form-group').addClass('hasError');
    
    });
 });
    function goBack() {
        // window.history.back();
        var nav = window.navigator;
        if( this.phonegapNavigationEnabled &&
            nav &&
            nav.app &&
            nav.app.backHistory ){
            nav.app.backHistory();
        } else {
            window.history.back();
        }
    }
</script>