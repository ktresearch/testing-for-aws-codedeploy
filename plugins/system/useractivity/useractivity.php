<?php 
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );
jimport( 'joomla.application.component.controller' );


class plgSystemUseractivity extends JPlugin {
    
    protected $db;
    private $_redirect_to = '';
    private $_redirect_id = 0;
     
    
    public function onAfterRoute() {
        
        $app=JFactory::getApplication();
        if($app->isAdmin())
            return true;
        
        $jinput = JFactory::getApplication()->input;
        $type = $jinput->get('type', '', 'STRING');
        
        if($type == 'appview' || $_SESSION['type'] == 'appview') {
            return;
        }
        
        $currentUrl = JFactory::getURI()->toString();
        $this->getRedirectPage();
        if(is_null($_SERVER['HTTP_REFERER']) && !$type) {
            if(!empty($this->_redirect_to)) {
                if($currentUrl != $this->_redirect_to) {
                    $app->redirect(JRoute::_($this->_redirect_to,false)); 
                }
            }
        }
       
    }
    
    public function onAfterRender($url, $userid) {
        
        $app = JFactory::getApplication();
	if($app->isAdmin())
            return true;
        
        $view = JRequest::getCmd('view', '');
        $task = JRequest::getCmd('task', '');
        $option = JRequest::getCmd('option', '');
        $courseid = JRequest::getVar('id');
        $groupid = JRequest::getVar('groupid');
        $type = JRequest::getCmd('type', '');
        if($type == 'appview' || $_SESSION['type'] == 'appview')
            return;
        // get current user login
        $users = JFactory::getUser();
        // get current URL
        $currentUrl = JFactory::getURI()->toString();
        // get base url
        $baseUrl = JUri::base();
        // set moduleid
        $moduleid = $groupid;
        if($courseid) {
            $moduleid = $courseid;
        }
        
        // get data of user
        $rows = $this->userActivities($users->id);
        
        // only save data when user access course and group
        if($users->id == 0) {
            return false;
        }
        if($currentUrl == $baseUrl) {
            return false;
        }
        if($option == "com_community" || $option == "com_joomdle" && $type != 'appview') {
            if($rows->component == $option && $rows->moduleid == $moduleid) {
                $query = $this->db->getQuery(true)
                            ->update($this->db->quoteName('#__user_log'))
                            ->set($this->db->quoteName('datetime') . ' = ' . $this->db->quote(date('Y-m-d H:i:s')))
                            ->set($this->db->quoteName('accessagain') . ' = ' . $this->db->quote(date('Y-m-d H:i:s')))
                            ->where($this->db->quoteName('id') . ' = ' . $this->db->quote($rows->id));
                    try
                    {
                            $this->db->setQuery($query)->execute();
                    }
                    catch (RuntimeException $e)
                    {
                            return false;
                    }
                    if(($option == 'com_community') && $moduleid != 0) {
                       $this->setGroupLastaccess($moduleid);
                    }
            } else {
                    $columns = array('userid', 'activities', 'component', 'moduleid', 'redirect', 'datetime', 'accessagain');
                    $values = array($this->db->quote($users->id), $this->db->quote($currentUrl), $this->db->quote($option), $this->db->quote($moduleid), $this->db->quote($currentUrl), $this->db->quote(date('Y-m-d H:i:s')), $this->db->quote(date('Y-m-d H:i:s')));
                    // Prepare the insert query.
                    $query = $this->db->getQuery(true);
                    $query
                     ->insert($this->db->quoteName('#__user_log'))
                     ->columns($this->db->quoteName($columns))
                     ->values(implode(',', $values));

                    // Set the query using our newly populated query object and execute it.
                    $this->db->setQuery($query);
                    if($this->db->execute()) {
                        $this->db->insertid();
                    }
                    if($option == 'com_community' && $moduleid != 0) {
                        $this->setGroupLastaccess($moduleid);
                    }
            }
        }
        
    }
    public function onUserLogout () {
        $users = JFactory::getUser();
        $query = $this->db->getQuery(true)
				->delete($this->db->quoteName('#__user_log'))
                                ->where($this->db->quoteName('userid') . ' = ' . (int) $users->id);
        
        try {
            $this->db->setQuery($query)->execute();
        } catch (RuntimeException $e) {
            return false;
        }
        
	return true;
    }
    /*
     * get user Activities
     */
    public function userActivities ($user_id) {
        
         // get data of user
        $query = $this->db->getQuery(true);
        $query
                ->select($this->db->quoteName(array('id', 'userid', 'activities', 'component', 'moduleid', 'redirect', 'datetime')))
                ->from($this->db->quoteName('#__user_log'))
                ->where($this->db->quoteName('userid')."=".$this->db->quote($user_id))
                ->order('datetime DESC');

        $this->db->setQuery($query, 0, 1);
        $rows = $this->db->loadObject();
        return $rows;
    }
    /*
     * get user id
     */
    public function get_user($user) {
        $user_id = 0;
        if(empty($user['id'])){
		if(!empty($user['username'])){
                    jimport('joomla.user.helper');
                    $instance = new JUser();
                    if($id = intval(JUserHelper::getUserId($user['username'])))  {
                        $instance->load($id);
                    }
                    if ($instance->get('block') == 0) {
                        $user_id=$instance->id;
                    }
		}
	} else {
            $user_id = $user['id'];
	}
       return $user_id;
    }
    public function setURLRedirect($id) {
        $query = $this->db->getQuery(true)
			->update($this->db->quoteName('#__user_log'))
			->set($this->db->quoteName('redirect') . ' = ' . $this->db->quote(NULL))
			->where($this->db->quoteName('id') . ' = ' . $this->db->quote($id));
		try
		{
			$this->db->setQuery($query)->execute();
		}
		catch (RuntimeException $e)
		{
			return false;
		}
    }

    public function getRedirectPage() {
        $users = JFactory::getUser();
        
        $query = $this->db->getQuery(true);
        $query
                ->select($this->db->quoteName(array('id', 'userid', 'activities', 'component', 'moduleid', 'redirect', 'datetime', 'accessagain')))
                ->from($this->db->quoteName('#__user_log'))
                ->where($this->db->quoteName('userid')."=".$this->db->quote($users->id) ." AND ".$this->db->quoteName('redirect')." IS NOT NULL")
                ->order('datetime DESC');

        $this->db->setQuery($query, 0, 1);
        $rows = $this->db->loadObject();
        
		
        $this->_redirect_to = $rows->redirect;
        $this->_redirect_id = $rows->id;
    }
    public function setGroupLastaccess($id) {
         $query = $this->db->getQuery(true);
         $query
                ->update($this->db->quoteName('#__community_groups'))
                ->set($this->db->quoteName('lastaccess') . ' = ' . $this->db->quote(date('Y-m-d H:i:s')))
                ->where($this->db->quoteName('id') . ' = ' . $this->db->quote($id));
        try {
            $this->db->setQuery($query)->execute();
        } catch (RuntimeException $e) {
            return false;
        }
    }
}