
CREATE TABLE IF NOT EXISTS `#__user_log` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) NOT NULL,
  `activities` varchar(255) NOT NULL DEFAULT '',
  `component` varchar(150) NOT NULL,
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `redirect` varchar(255) NOT NULL DEFAULT '',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `accessagain` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;