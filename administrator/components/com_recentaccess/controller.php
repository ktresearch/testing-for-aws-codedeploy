<?php 

defined('_JEXEC') or die;

class RecentaccessController extends JControllerLegacy {
    
    public function display($cachable = false, $urlparams = false)
	{
              
		JRequest::setVar('view', 'recentaccess'); // force it to be the search view
                
               
		return parent::display($cachable, $urlparams);
	}
    public function clearRecent() {
        $users = JFactory::getUser();
        $app=JFactory::getApplication();
        
        $db = JFactory::getDBO();
        $query = $db->getQuery(true)
				->delete($db->quoteName('#__user_log'));
        
        try {
            $db->setQuery($query)->execute();
        } catch (RuntimeException $e) {
            return false;
        }
        $app->redirect('index.php');
   }
}