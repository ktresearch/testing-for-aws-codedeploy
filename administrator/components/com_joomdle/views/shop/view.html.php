<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomla
 * @subpackage Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries
jimport( 'joomla.application.component.view');
require_once( JPATH_COMPONENT.'/helpers/shop.php' );
require_once( JPATH_COMPONENT.'/helpers/groups.php' );

class JoomdleViewShop extends JViewLegacy {
    function display($tpl = null) {

		$params = JComponentHelper::getParams( 'com_joomdle' );

        $this->sidebar = JHtmlSidebar::render();

		if ($params->get( 'shop_integration' ) == 'no')
		{
			JToolbarHelper::title(JText::_('COM_JOOMDLE_VIEW_SHOP_TITLE'), 'shop');
			$this->message = JText::_('COM_JOOMDLE_SHOP_INTEGRATION_NOT_ENABLED');
			$tpl = "disabled";
			parent::display($tpl);
			return;
		}

        $this->addToolbar();

        $db           = JFactory::getDBO();
        $query = 'SELECT product_id, product_code, price_value  ' .
            ' FROM #__hikashop_product a LEFT JOIN #__hikashop_price b ON a.product_id = b.price_product_id' ;

        $db->setQuery($query);
        $products = $db->loadObjectList();

        $bundleArr = array();
        $progArr = array();
        $courseArr = array();
        foreach($products as $k => $v ) {
            if (!is_null($v->price_value)) {
                if (strpos($v->product_code, 'bundle') !== false) {
                    $bundleArr[end(explode('_', $v->product_code))] = end(explode('_', $v->product_code));
                } else if (strpos($v->product_code, 'prog') !== false) {
                    $progArr[end(explode('_', $v->product_code))] = end(explode('_', $v->product_code));
                } else {
                    $courseArr[$v->product_code] = $v->product_code;
                }
            }
            $products[$v->product_code] = $v;
            unset($products[$k]);
        }

		$this->bundles = JoomdleHelperShop::get_bundles ();
        foreach ($this->bundles as $v) {
            if ($v->published  == 1 && in_array($v->id, $bundleArr) ) { $v->hasPrice = 1; } else {$v->hasPrice = 0;}
            if ($v->published  == 1) {
                $n = 'bundle_'.$v->id;
                $v->product_id = $products[$n]->product_id;
            }
        }

		$this->courses = JoomdleHelperShop::getShopCourses ();

        foreach ($this->courses as $v) {
            if ($v->published  == 1 && in_array($v->id, $courseArr) ) { $v->hasPrice = 1; } else {$v->hasPrice = 0;}
            if ($v->published  == 1) {
                $n = $v->id;
                $v->product_id = $products[$n]->product_id;
            }
        }

        $programs = JoomdleHelperContent::call_method ('list_progs', 0);
        $this->programs = array();
        foreach ($programs as $k=> $v) {
            $obj = new stdClass();
            $obj->id = $v['id'];
            $obj->fullname = $v['fullname'];
            $obj->published = JoomdleHelperShop::is_course_on_sell ('prog_'.$v['id']);
            if ($obj->published  == 1 && in_array($obj->id, $progArr)) { $obj->hasPrice = 1; } else {$obj->hasPrice = 0;}
            if ($obj->published  == 1) {
                $n = 'prog_'.$obj->id;
                $obj->product_id = $products[$n]->product_id;
            }

            $this->programs[] = $obj;
        }

//        JoomdleHelperShop::enrol_program(JFactory::getUser()->username, 'prog_50');
        parent::display($tpl);
    }

    protected function addToolbar()
    {
        JToolbarHelper::title(JText::_('COM_JOOMDLE_VIEW_SHOP_TITLE'), 'shop');

		JToolBarHelper::addNew('bundle.add', 'COM_JOOMDLE_NEW_BUNDLE');
		JToolbarHelper::publish('shop.publish', 'JTOOLBAR_PUBLISH', true);
		JToolbarHelper::unpublish('shop.unpublish', 'JTOOLBAR_UNPUBLISH', true);

		JToolBarHelper::custom( 'shop.reload', 'restore', 'restore', 'COM_JOOMDLE_RELOAD_FROM_MOODLE', true, false );
		JToolBarHelper::trash('shop.delete_courses_from_shop');


        JHtmlSidebar::setAction('index.php?option=com_joomdle&view=shop');
    }

}
?>
