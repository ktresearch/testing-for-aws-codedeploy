<?php
/**
 * @version     
 * @package     Joomdle
 * @subpackage  Content
 * @copyright   Copyright (C) Antonio Duran Terres
 * @license     GNU/GPL, see LICENSE.php
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.user.helper');

//require_once (JPATH_ADMINISTRATOR . '/components/com_joomdle/help/coursegroups.php');


class JoomdleHelperSocialgroups
{
	static function add_group ($name, $description, $course_id, $username, $lpusername = '', $pic_url = '', $pic_name = '', $enableCreateChats = 1)
	{
		JPluginHelper::importPlugin( 'joomdlesocialgroups' );
		$dispatcher = JDispatcher::getInstance();
		$dispatcher->trigger('onAddSocialGroup', array ($name, $description, $course_id, $username, $lpusername, $pic_url, $pic_name, $enableCreateChats));
	}

        static function copy_group ($old_course_id, $new_course_id) 
        {
                JPluginHelper::importPlugin( 'joomdlesocialgroups' );
		$dispatcher = JDispatcher::getInstance();
		$dispatcher->trigger('onCopySocialGroup', array ($old_course_id, $new_course_id));
        }

	static function update_group ($name, $description, $course_id, $pic_url = '', $pic_name = '', $enableCreateChats = 1)
	{
		JPluginHelper::importPlugin( 'joomdlesocialgroups' );
		$dispatcher = JDispatcher::getInstance();
		$dispatcher->trigger('onUpdateSocialGroup', array ($name, $description, $course_id, $pic_url, $pic_name, $enableCreateChats));
	}

	static function delete_group ($course_id, $course_name)
	{
		JPluginHelper::importPlugin( 'joomdlesocialgroups' );
		$dispatcher = JDispatcher::getInstance();
		$dispatcher->trigger('onDeleteSocialGroup', array ($course_id, $course_name));
	}

	static function add_group_member ($username, $permissions, $course_id)
	{
		JPluginHelper::importPlugin( 'joomdlesocialgroups' );
		$dispatcher = JDispatcher::getInstance();
		$dispatcher->trigger('onAddSocialGroupMember', array ($username, $permissions, $course_id));
	}

	static function remove_group_member ($username, $course_id)
	{
		JPluginHelper::importPlugin( 'joomdlesocialgroups' );
		$dispatcher = JDispatcher::getInstance();
		$dispatcher->trigger('onRemoveSocialGroupMember', array ($username, $course_id));
	}

        static function add_BLN_member ($username)
	{
		JPluginHelper::importPlugin( 'joomdlesocialgroups' );
		$dispatcher = JDispatcher::getInstance();
		$dispatcher->trigger('onAddBLNMember', array ($username));
        }

}

?>
