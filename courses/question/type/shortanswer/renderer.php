<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Short answer question renderer class.
 *
 * @package    qtype
 * @subpackage shortanswer
 * @copyright  2009 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();


/**
 * Generates the output for short answer questions.
 *
 * @copyright  2009 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_shortanswer_renderer extends qtype_renderer {
    public function formulation_and_controls(question_attempt $qa,
            question_display_options $options) {

        $question = $qa->get_question();
        $currentanswer = $qa->get_last_qt_var('answer');

        $inputname = $qa->get_qt_field_name('answer');
        $inputattributes = array(
            'type' => 'text',
            'name' => $inputname,
//            'value' => $currentanswer,
            'id' => $inputname,
            'size' => 80,
            'placeholder' => get_string('typeyouranswerhere', 'qtype_shortanswer'),
        );

        if ($options->readonly) {
            $inputattributes['readonly'] = 'readonly';
        }

        $feedbackimg = '';
        if ($options->correctness) {
            $answer = $question->get_matching_answer(array('answer' => $currentanswer));
            if ($answer) {
                $fraction = $answer->fraction;
            } else {
                $fraction = 0;
            }
            $inputattributes['class'] = $this->feedback_class($fraction);
            $feedbackimg = $this->feedback_image($fraction);
        }

        $questiontext = $question->format_questiontext($qa);
        $placeholder = false;
        if (preg_match('/_____+/', $questiontext, $matches)) {
            $placeholder = $matches[0];
            $inputattributes['size'] = round(strlen($placeholder) * 1.1);
        }
//        $input = html_writer::empty_tag('input', $inputattributes) . $feedbackimg;
        $input = html_writer::start_tag('textarea', $inputattributes);
        $input .= $currentanswer;
        $input .= html_writer::end_tag('textarea') . $feedbackimg;

        if ($placeholder) {
            $inputinplace = html_writer::tag('label', get_string('answer'),
                    array('for' => $inputattributes['id'], 'class' => 'accesshide'));
            $inputinplace .= $input;
            $questiontext = substr_replace($questiontext, $inputinplace,
                    strpos($questiontext, $placeholder), strlen($placeholder));
        }

        $result = html_writer::tag('div', $questiontext, array('class' => 'qtext'));

        if (!$placeholder) {
            $result .= html_writer::start_tag('div', array('class' => 'ablock'));
            $result .= html_writer::tag('label', get_string('answer', 'qtype_shortanswer',
                    html_writer::tag('span', $input, array('class' => 'answer'))),
                    array('for' => $inputattributes['id']));
            $result .= html_writer::end_tag('div');
        }

        $ans = '';
        $fb = '';
        foreach ($question->answers as $v) {
            if ((float)$v->fraction > 0) {
                $ans = $v->answer;
                $fb = $v->feedback;
            }
        }
        $answer = $question->get_matching_answer($question->get_correct_response());
        if (!$answer) {
            $correctAnswerText = '';
        } else
        $correctAnswerText = get_string('correctanswer', 'qtype_truefalse').': '.$question->clean_response($answer->answer);
	$correctAnswerText = str_replace("'", "\'", $correctAnswerText);
        if ($fb) {
            $result .= html_writer::start_tag('div', array('class' => 'quesFeedback'));
            $result .= html_writer::tag('p', get_string('feedback', 'qtype_truefalse'), array('class'=>'labelFeedback'));
//            $result .= html_writer::tag('p', $this->correct_response($qa));
            $result .= html_writer::tag('p', $fb);
//            $result .= html_writer::tag('p', $this->specific_feedback($qa));

            $result .= html_writer::end_tag('div'); // Close <div class="quesFeedback">
        }

        $result .= html_writer::script("jQuery(document).ready(function() {
            jQuery('.buttonSubmit.atype1, .buttonSubmit.atype2, .buttonSubmit.atype').click(function() {
                if (jQuery('.shortanswer .answer textarea').val() == '') {
                    alert('Please fill up your answer first.');
                } else {
                    if (jQuery(this).hasClass('atype1') || jQuery(this).hasClass('atype')) {
                    jQuery('#page-mod-quiz-attempt .quesFeedback').fadeIn();
                    jQuery(this).fadeOut();
                    jQuery('#page-mod-quiz-attempt .answer').append('<p class=\"correctAnswerText\">$correctAnswerText</p>');
                    jQuery('#iframe-quiz', window.parent.document).css('min-height', jQuery('#page-mod-quiz-attempt').height() );
                    } else if (jQuery(this).hasClass('atype2')) {
                        jQuery('#page-mod-quiz-attempt input[name=\"next\"]').click();
                }
                }
            });
        }); ");

	$correctAnswerText = str_replace("\'", "'", $correctAnswerText);

        if ($qa->get_state() == question_state::$invalid) {
            $result .= html_writer::nonempty_tag('div',
                    $question->get_validation_error(array('answer' => $currentanswer)),
                    array('class' => 'validationerror'));
        }

        return $result;
    }

    public function specific_feedback(question_attempt $qa) {
        $question = $qa->get_question();

        $answer = $question->get_matching_answer(array('answer' => $qa->get_last_qt_var('answer')));
        if (!$answer || !$answer->feedback) {
            return '';
        }

        return $question->format_text($answer->feedback, $answer->feedbackformat,
                $qa, 'question', 'answerfeedback', $answer->id);
    }

    public function correct_response(question_attempt $qa) {
        $question = $qa->get_question();

        $answer = $question->get_matching_answer($question->get_correct_response());
        if (!$answer) {
            return '';
        }

        return get_string('correctansweris', 'qtype_shortanswer',
                s($question->clean_response($answer->answer)));
    }
}
