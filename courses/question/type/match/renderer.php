<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Matching question renderer class.
 *
 * @package   qtypematch
 * @copyright 2009 The Open University
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();


/**
 * Generates the output for matching questions.
 *
 * @copyright 2009 The Open University
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_match_renderer extends qtype_with_combined_feedback_renderer {

    public function formulation_and_controls(question_attempt $qa,
                                             question_display_options $options) {

        $question = $qa->get_question();
        $stemorder = $question->get_stem_order();
        $response = $qa->get_last_qt_data();
        // update turn off random question
        asort($stemorder);
        $choices = $this->format_choices($question);

        $result = '';
        $result .= html_writer::tag('div', $question->format_questiontext($qa),
            array('class' => 'qtext'));

        $result .= html_writer::start_tag('div', array('class' => 'ablock'));
        $result .= html_writer::start_tag('table', array('class' => 'answer'));
        $result .= html_writer::start_tag('tbody');

        $parity = 0;
        $i = 1;
        $chars = ['0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        foreach ($stemorder as $key => $stemid) {

            $result .= html_writer::start_tag('tr');
            $result .= html_writer::start_tag('td');
            $result .= html_writer::tag('p', get_string('statement', 'qtype_match', $i), array('class'=>'statement statement'.$i));
            $result .= html_writer::tag('p', $chars[$i].'. '.$question->format_text(
                    $question->stems[$stemid], $question->stemformat[$stemid],
                    $qa, 'qtype_match', 'subquestion', $stemid), array('class'=>'statementContent statementContent'.$i));
            $result .= html_writer::end_tag('td');
            $result .= html_writer::end_tag('tr');

            $result .= html_writer::start_tag('tr', array('class' => 'tr'.($i-1).' r' . $parity));
            $fieldname = 'sub' . $key;

            $result .= html_writer::tag('td', $question->format_text(
                $question->stems[$stemid], $question->stemformat[$stemid],
                $qa, 'qtype_match', 'subquestion', $stemid),
                array('class' => 'qtxt'.($i-1).' text'));

            $classes = 'control';
            $feedbackimage = '';

            if (array_key_exists($fieldname, $response)) {
                $selected = $response[$fieldname];
            } else {
                $selected = 0;
            }

            $fraction = (int) ($selected && $selected == $question->get_right_choice_for($stemid));

            if ($options->correctness && $selected) {
                $classes .= ' ' . $this->feedback_class($fraction);
                $feedbackimage = $this->feedback_image($fraction);
            }

            $lis = '';
            foreach ($choices as $k => $v) {
                $lis .= html_writer::tag('li', $v, ['data-value'=>$k, 'class'=>'liSelect']);
            }
            $val = $selected ? $choices[$selected] : get_string('option', 'qtype_match');
            $fakeSelect = html_writer::tag('div',
                html_writer::tag('div', $val, ['class'=>'divSelect']).
                html_writer::tag('ul', $lis, ['class'=>'ulSelect', 'data-name'=>$qa->get_qt_field_name('sub' . $key), 'data-rightans'=>$question->get_right_choice_for($stemid)])
                , ['class'=>'fakeSelect']) ;

            $result .= html_writer::tag('td',
                html_writer::label(get_string('answertext', 'qtype_match'),
                    'menu' . $qa->get_qt_field_name('sub' . $key), false,
                    array('class' => 'accesshide')) .
                html_writer::select($choices, $qa->get_qt_field_name('sub' . $key), $selected,
                    get_string('option', 'qtype_match'), array('disabled' => $options->readonly)) .
                $fakeSelect.
                ' ' . $feedbackimage, array('class' => $classes));
            $result .= html_writer::end_tag('tr');
            $parity = 1 - $parity;
            $i++;
        }
        $result .= html_writer::end_tag('tbody');
        $result .= html_writer::end_tag('table');

        $result .= html_writer::end_tag('div'); // Closes <div class="ablock">.

//        $result .= html_writer::start_tag('div', array('class'=>'correctAnswerText'));
//
//        $result .= html_writer::tag('p', $this->correct_response($qa));
////        $result .= html_writer::tag('p', $this->specific_feedback($qa));
//
//        $result .= html_writer::end_tag('div'); // Close <div class="quesFeedback">

        if ($question->generalfeedback) {
            $result .= html_writer::start_tag('div', array('class'=>'quesFeedback'));

            $result .= html_writer::tag('p', get_string('feedback', 'qtype_truefalse'), array('class'=>'labelFeedback'));

            $result .= html_writer::tag('p', $question->generalfeedback);

//        $result .= html_writer::tag('p', $this->correct_response($qa));

//        $result .= html_writer::tag('p', $this->specific_feedback($qa));

            $result .= html_writer::end_tag('div'); // Close <div class="quesFeedback">
        }

        $result .= html_writer::script("jQuery(document).ready(function() {
            jQuery('.fakeSelect').click(function() {
                jQuery(this).find('.divSelect').toggleClass('clicked');
                jQuery(this).find('.ulSelect').toggle();
            });
            jQuery('.liSelect').click(function() {
                jQuery(this).parent('.ulSelect').prev('.divSelect').html( jQuery(this).html() );
                jQuery('select[name=\"'+ jQuery(this).parent('.ulSelect').attr('data-name') +'\"]').val(jQuery(this).attr('data-value'));
            });
            jQuery('.buttonSubmit.atype1, .buttonSubmit.atype2, .buttonSubmit.atype').click(function() {
                var valid = true;
                jQuery('.match table.answer .control select').each(function() {
                    if (jQuery(this).val() == '') valid = false;
                });
                if (!valid) {
                    alert('Please match the correct option to each statement given box.');
                } else {
                    if (jQuery(this).hasClass('atype1') || jQuery(this).hasClass('atype')) {
                        jQuery('#page-mod-quiz-attempt .quesFeedback').fadeIn();
                        jQuery(this).fadeOut();
                        jQuery('.ulSelect').each(function() {
                            if (jQuery('select[name=\"'+ jQuery(this).attr('data-name') +'\"]').val() == jQuery(this).attr('data-rightans')) {
                                jQuery(this).before('<img style=\"width:27px;height:20px;position:absolute;top:9px;right:-37px;\" src=\"/media/joomdle/images/icon/Completed_Uncompleted/tick.png\">');
                            } else {
                                jQuery(this).before('<img style=\"width:20px;height:20px;position:absolute;top:9px;right:-30px;\" src=\"/media/joomdle/images/icon/Completed_Uncompleted/cross.png\">');
                                jQuery(this).after('<p class=\"correctAnswerText\">".get_string('correctanswer', 'qtype_truefalse').': '."'+ jQuery(this).find('li[data-value='+jQuery(this).attr('data-rightans')+']').html() +'</p>');
                            }
                        });
                        jQuery('#iframe-quiz', window.parent.document).css('min-height', jQuery('#page-mod-quiz-attempt').height() );
                    } else if (jQuery(this).hasClass('atype2')) {
                        jQuery('#page-mod-quiz-attempt input[name=\"next\"]').click();
                    }
                }
            });
        }); ");

        if ($qa->get_state() == question_state::$invalid) {
            $result .= html_writer::nonempty_tag('div',
                $question->get_validation_error($response),
                array('class' => 'validationerror'));
        }

        return $result;
    }

    public function specific_feedback(question_attempt $qa) {
        return $this->combined_feedback($qa);
    }

    protected function format_choices($question) {
        $choices = array();
        foreach ($question->get_choice_order() as $key => $choiceid) {
            $choices[$key] = format_string($question->choices[$choiceid], true,
                array('context' => $question->contextid));
        }
        return $choices;
    }

    public function correct_response(question_attempt $qa) {
        $question = $qa->get_question();
        $stemorder = $question->get_stem_order();

        $choices = $this->format_choices($question);
        $right = array();
        foreach ($stemorder as $key => $stemid) {
            $right[] = $question->format_text($question->stems[$stemid],
                    $question->stemformat[$stemid], $qa,
                    'qtype_match', 'subquestion', $stemid) . ' – ' .
                $choices[$question->get_right_choice_for($stemid)];
        }

        if (!empty($right)) {
            return get_string('correctansweris', 'qtype_match', implode(', ', $right));
        }
    }
} 
