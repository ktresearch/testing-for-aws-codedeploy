<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A4_non_embedded certificate type
 *
 * @package    mod
 * @subpackage certificate
 * @copyright  Mark Nelson <markn@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); // It must be included from view.php
}

if (!isset($user) || empty($user)) {
    $user = $USER;
}
if (isset($certrecord->timearchived)) {
    // Use archived values
    $timecompleted = certificate_get_date_completed_formatted($certificate, $certrecord->timecompleted, $user);
    $grade = $certrecord->grade;
    $outcome = $certrecord->outcome;
    $code = $certrecord->code;
} else {
    $timecompleted = certificate_get_date($certificate, $certrecord, $course);
    $grade = certificate_get_grade($certificate, $course);
    $outcome = certificate_get_outcome($certificate, $course);
    $code = certificate_get_code($certificate, $certrecord);
}
$strmgr = get_string_manager();

//var_dump($CFG->jdataroot . '/certificatetemp/pix/borders/');
//var_dump(dirname($CFG->dirroot).'/templates/t3_bs3_tablet_desktop_template/fonts/freeserif/FreeSerif.ttf');
//var_dump($CFG->dataroot.'/joomla/images/certificates/');
//die;
$pdf = new PDF($certificate->orientation, 'mm', 'A4', true, 'UTF-8', false);

$pdf->SetTitle($certificate->name);
$pdf->SetProtection(array('modify'));
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetAutoPageBreak(false, 0);
$pdf->AddPage();

// Define variables
// Landscape
if ($certificate->orientation == 'L') {
    $x = 10;
    $y = 130;
    $sealx = 230;
    $sealy = 150;
    $sigx = 47;
    $sigy = 155;
    $custx = 47;
    $custy = 155;
    $wmarkx = 40;
    $wmarky = 31;
    $wmarkw = 212;
    $wmarkh = 148;
    $brdrx = 0;
    $brdry = 0;
    $brdrw = 297;
    $brdrh = 210;
    $codey = 175;
} else {
    // Portrait
    $x = 10;
    $y = 140;
    $sealx = 150;
    $sealy = 220;
    $sigx = 30;
    $sigy = 230;
    $custx = 30;
    $custy = 230;
    $wmarkx = 26;
    $wmarky = 58;
    $wmarkw = 158;
    $wmarkh = 170;
    $brdrx = 0;
    $brdry = 0;
    $brdrw = 210;
    $brdrh = 297;
    $codey = 250;
}

// Add images and lines
certificate_print_image($pdf, $certificate, CERT_IMAGE_BORDER, $brdrx, $brdry, $brdrw, $brdrh);
certificate_draw_frame($pdf, $certificate);
// Set alpha to semi-transparency
$pdf->SetAlpha(0.2);
certificate_print_image($pdf, $certificate, CERT_IMAGE_WATERMARK, $wmarkx, $wmarky, $wmarkw, $wmarkh);
$pdf->SetAlpha(1);
certificate_print_image($pdf, $certificate, CERT_IMAGE_SEAL, $sealx, $sealy, '', '');
certificate_print_image($pdf, $certificate, CERT_IMAGE_SIGNATURE, $sigx, $sigy, '', '');

// Add images and lines
certificate_print_image($pdf, $certificate, CERT_IMAGE_BORDER, $brdrx, $brdry, $brdrw, $brdrh);
certificate_draw_frame($pdf, $certificate);
// Set alpha to semi-transparency
$pdf->SetAlpha(0.2);
certificate_print_image($pdf, $certificate, CERT_IMAGE_WATERMARK, $wmarkx, $wmarky, $wmarkw, $wmarkh);
$pdf->SetAlpha(1);
certificate_print_image($pdf, $certificate, CERT_IMAGE_SEAL, $sealx, $sealy, '', '');
certificate_print_image($pdf, $certificate, CERT_IMAGE_SIGNATURE, $sigx, $sigy, '', '');

// Add text
$pdf->SetTextColor(0, 0, 0);
$pdf->SetFont('Times');

certificate_print_text($pdf, $x+1, $y-29, 'C', CERT_FONT_SANS, '', 20, date("Y"));
certificate_print_text($pdf, $x, $y+5, 'C', CERT_FONT_SANS, '', 20, fullname($user));
//certificate_print_text($pdf, $x, $y + 19, 'C', CERT_FONT_SANS, '', 20, $course->fullname);
certificate_print_text($pdf, $x, $y + 63, 'C', CERT_FONT_SANS, '', 20,  $timecompleted);

certificate_print_text($pdf, $x, $y + 120, 'C', CERT_FONT_SERIF, '', 10, $grade);
certificate_print_text($pdf, $x, $y + 130, 'C', CERT_FONT_SERIF, '', 10, $outcome);
$lines = explode('|', wordwrap($course->fullname, 40, '|'));
$i = $y;
foreach ($lines as $line) {
    certificate_print_text($pdf, $x, $i + 35, 'C', CERT_FONT_SANS, '', 20, $line);
    $i += 9;
}
//certificate_print_text($pdf, $x, $y + 92, 'C', CERT_FONT_SANS, '', 14, $timecompleted);
//certificate_print_text($pdf, $x, $y + 102, 'C', CERT_FONT_SERIF, '', 10, $grade);
//certificate_print_text($pdf, $x, $y + 112, 'C', CERT_FONT_SERIF, '', 10, $outcome);
if ($certificate->printhours) {
    certificate_print_text($pdf, $x, $y + 122, 'C', CERT_FONT_SERIF, '', 10, $strmgr->get_string('credithours', 'certificate', null, $user->lang) . ': ' . $certificate->printhours);
}
certificate_print_text($pdf, $x, $codey, 'C', CERT_FONT_SERIF, '', 10, $code);
$i = 0;
if ($certificate->printteacher) {
    $context = context_module::instance($cm->id);
    if ($teachers = get_users_by_capability($context, 'mod/certificate:printteacher', '', $sort = 'u.lastname ASC', '', '', '', '', false)) {
        foreach ($teachers as $teacher) {
            $i++;
            certificate_print_text($pdf, $sigx, $sigy + ($i * 4), 'L', CERT_FONT_SERIF, '', 12, fullname($teacher));
        }
    }
}

certificate_print_text($pdf, $custx, $custy, 'L', null, null, null, $certificate->customtext);


////////////////////////////////////////////////
if (!empty($action)) {

    if ($certificate->orientation == 'L') {
//        $im = imagecreate(1170, 827);
//        $im = imagecreate(297, 210);
        $im = imagecreate(1188, 840);
    } else
//        $im = imagecreate(827, 1170);
//        $im = imagecreate(210, 297);
        $im = imagecreate(840, 1188);

    if ($certificate->borderstyle === '0') {
        imagecolorallocate($im, 255, 255, 255);
    } else {
        $colortext = imagecolorallocate($im, 0, 0, 0);
        $src_border = $CFG->jdataroot . '/certificatetemp/pix/borders/'.$course->id .'/' . $certificate->borderstyle;
//$certificate->printwmark = 'Crest.png';
//$src_watermark = $CFG->dirroot.'/mod/certificate/pix/watermarks/'.$certificate->printwmark;
        if (exif_imagetype($src_border) == 2) {
            $srcim = imagecreatefromjpeg($src_border);
        } else if (exif_imagetype($src_border) == 3) {
            $srcim = imagecreatefrompng($src_border);
        }

        imagecopyresized($im, $srcim, 0, 0, 0, 0, imagesx($im), imagesy($im), imagesx($srcim), imagesy($srcim));
    }

    // if ($certificate->orientation == 'P') {
//    $srcim = imagerotate($srcim, 90 , 0);
    // }

    $text1 = $strmgr->get_string('title', 'certificate', null, $user->lang);

    $text2 = $strmgr->get_string('certify', 'certificate', null, $user->lang);
    $text3 = fullname($user);
    $text4 = $strmgr->get_string('statement', 'certificate', null, $user->lang);
    $text5 = $course->fullname;
    $text6 = $timecompleted;
    $text7 = $grade;
    $text8 = $outcome;
    if ($certificate->printhours) {
        $text9 = $strmgr->get_string('credithours', 'certificate', null, $user->lang) . ': ' . $certificate->printhours;
    }
    $text10 =  $code;


//certificate_print_text($pdf, $custx, $custy, 'L', null, null, null, $certificate->customtext);
    $y = $y +10;
    $z = 2;

    $font_path =  dirname($CFG->dirroot).'/templates/t3_bs3_tablet_desktop_template/fonts/open-sans/OpenSans-Regular.ttf';
    $font_path2 =  dirname($CFG->dirroot).'/templates/t3_bs3_tablet_desktop_template/fonts/freesans/FreeSans.ttf';
    $font_path3 =  dirname($CFG->dirroot).'/templates/t3_bs3_tablet_desktop_template/fonts/freeserif/FreeSerif.ttf';
    $angle = 0;
    $font_size = 30;
    $image_width = imagesx($im);
    $image_height = imagesy($im);
    if ($certificate->borderstyle != 0) {
        $rate = imagesx($im) / imagesx($srcim);
    } else {
//        $rate = imagesx($im) / 300;
        $rate = 1;
    }
    function getCenterX($font_size, $rate, $font_path, $text, $image_width) {
        $text_box = imagettfbbox($font_size * $rate ,0,$font_path,$text);
        $text_width = $text_box[2]-$text_box[0];
//    $text_height = $text_box[7]-$text_box[1];
        $x = ($image_width/2) - ($text_width/2);
        //$y = ($image_height/2) - ($text_height/2);
        return $x;
    }

    $colortext = imagecolorallocate($im, 0, 0, 120);
    $x = getCenterX(30 + $z, $rate, $font_path2, $text1, $image_width  );
//    imagettftext($im, 33 * $rate , $angle, $x, ($image_height / 230) * $y , $colortext, $font_path, $text1);
    imagettftext($im, 30 + $z , $angle, $x, $y * 4 , $colortext, $font_path2, $text1);

    $colortext = imagecolorallocate($im, 0, 0, 0);
    $x = getCenterX( 20 + $z, $rate, $font_path3, $text2, $image_width  );
//    imagettftext($im, 20 * $rate , $angle, $x, ($image_height / 260) * ($y + 20) , $colortext, $font_path, $text2);
    imagettftext($im, 20 + $z  , $angle, $x, 4 * ($y + 16) , $colortext, $font_path3, $text2);

    $x = getCenterX(30 + $z, $rate, $font_path2, $text3, $image_width  );
    imagettftext($im, 30 + $z , $angle, $x, 4 * ($y + 36) , $colortext, $font_path2, $text3);

    $x = getCenterX(20 + $z, $rate, $font_path2, $text4, $image_width  );
    imagettftext($im, 20 + $z , $angle, $x, 4 * ($y + 55) , $colortext, $font_path2, $text4);

   // $x = getCenterX(20 + $z, $rate, $font_path2, $text5, $image_width  );
    // $lines = explode('|', wordwrap($text5, 60, '|'));
    $j = $y;
    foreach ($lines as $line) {
        $x = getCenterX(20 + $z, $rate, $font_path2, $line, $image_width  );
        imagettftext($im, 20 + $z, $angle, $x, 4 * ($j + 72), $colortext, $font_path2, $line);
        $j += 10;
    }

    $x = getCenterX(14 + $z, $rate, $font_path2, $text6, $image_width  );
    imagettftext($im, 14 + $z , $angle, $x, 4 * ($y + 92) , $colortext, $font_path2, $text6);

    $x = getCenterX(10 + $z, $rate, $font_path3, $text7, $image_width  );
    imagettftext($im, 10 + $z, $angle, $x, 4 * ($y + 102) , $colortext, $font_path3, $text7);

    $x = getCenterX(10 + $z, $rate, $font_path3, $text8, $image_width  );
    imagettftext($im, 10 + $z , $angle, $x, 4 * ($y + 112) , $colortext, $font_path3, $text8);

    if ($certificate->printhours) {
        $x = getCenterX(10 + $z, $rate, $font_path3, $text9, $image_width);
        imagettftext($im, 10 + $z , $angle, $x, 4 * ($y + 122), $colortext, $font_path3, $text9);
    }

    $x = getCenterX(10 + $z, $rate, $font_path3, $text10, $image_width  );
    imagettftext($im, 10 + $z , $angle, $x, 4 * $codey , $colortext, $font_path3, $text10);

    $path = $CFG->dataroot.'/joomla/images/certificates/';
    $filename = md5($user->username.$course->id).'.jpg';
    imagejpeg($im, $path.$filename);


}
?>