<?php
require_once('../../config.php');
require_once($CFG->dirroot . '/mod/assign/locallib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/mod/assign/lib.php');
require_once($CFG->dirroot . '/mod/assign/lang/en/assign.php');
$id = required_param('id', PARAM_INT);
$assign = get_assignment($id);
$tit = (array_values($assign)[0]->name);
$PAGE->set_context(get_system_context());
$PAGE->set_title('Assignment '.$tit);

$urlparams = array('id' => $id,
    'action' => optional_param('action', '', PARAM_TEXT),
    'rownum' => optional_param('rownum', 0, PARAM_INT),
    'useridlistid' => optional_param('action', 0, PARAM_INT),
    'type' => optional_param('type', '', PARAM_TEXT)
);

$url = new moodle_url('/mod/assign/view.php', $urlparams);
$urlsb_params = array('id' => $id,
    'action' => 'editsubmission',
);

$url_submission = new moodle_url('/mod/assign/view.php', $urlsb_params);
$cm = get_coursemodule_from_id('assign', $id, 0, false, MUST_EXIST);
$list_modules = array();
$info = get_array_of_activities($cm->course);
foreach ($info as $value) {
    array_push($list_modules, $value);
}
$count_list_modules = count($list_modules);
$context = context_module::instance($cm->id);
foreach ($assign as $as){
    $id_as = $as->id;
}
$submission = get_submission($id_as,$USER->id);
$grade = get_grades($id_as,$USER->id);
if(!empty($grade))
{
    $string['notgraded'] = $grade;
}
$count_comment= 0;
if(!empty($submission))
{
    foreach ($submission as $sb){
        if($sb->status == 'submitted')
        {
            $submission_status = $string['submitted'];
        }
        else
        {
            $submission_status = $string['attempted'];
        }
        $status_sb = $sb->status;
        $id_sb = $sb->id;
        $count_comment = count(get_count_comment($sb->id));
        $lastmodified = $sb->timemodified;
    }
    $text = get_onlinetext($id_as,$id_sb);
}

if(!isset($submission_status))
{
    $submission_status = $string['noattempt'];
}
$url_sb = new moodle_url('/mod/assign/view_assignment.php', array('id' => $id,
    'sec' => 'sub',
));
$url_add = new moodle_url('/mod/assign/view.php', array('id' => $id,'action' => 'editsubmission'));

$url_as = new moodle_url('/mod/assign/view_assignment.php', array('id' => $id));


?>
<!-- ///////////////////////////////////=============view========== //////////////////////////////////////////// -->
<link href="styles.css" rel="stylesheet" type="text/css" media="screen,print" />
<link href="style_assign.css" rel="stylesheet" type="text/css" media="screen,print" />
<style type="text/css">
@font-face {
    font-family: "OpenSansBold";
    src: url(/templates/t3_bs3_tablet_desktop_template/fonts/open-sans/OpenSans-Bold.ttf);
}@font-face {
    font-family: "OpenSansBoldItalic";
    src: url(/templates/t3_bs3_tablet_desktop_template/fonts/open-sans/OpenSans-BoldItalic.ttf);
}@font-face {
    font-family: "OpenSansExtraBold";
    src: url(/templates/t3_bs3_tablet_desktop_template/fonts/open-sans/OpenSans-ExtraBold.ttf);
}@font-face {
    font-family: "OpenSansExtraBoldItalic";
    src: url(/templates/t3_bs3_tablet_desktop_template/fonts/open-sans/OpenSans-ExtraBoldItalic.ttf);
}@font-face {
    font-family: "OpenSansLight";
    src: url(/templates/t3_bs3_tablet_desktop_template/fonts/open-sans/OpenSans-Light.ttf);
}@font-face {
    font-family: "OpenSansLightItalic";
    src: url(/templates/t3_bs3_tablet_desktop_template/fonts/open-sans/OpenSans-LightItalic.ttf);
}@font-face {
    font-family: "OpenSansRegular";
    src: url(/templates/t3_bs3_tablet_desktop_template/fonts/open-sans/OpenSans-Regular.ttf);
}@font-face {
    font-family: "OpenSansSemibold";
    src: url(/templates/t3_bs3_tablet_desktop_template/fonts/open-sans/OpenSans-Semibold.ttf);
}@font-face {
    font-family: "OpenSansSemiboldItalic";
    src: url(/templates/t3_bs3_tablet_desktop_template/fonts/open-sans/OpenSans-SemiboldItalic.ttf);
}
</style>
</script>
<meta charset="UTF-8">

<div class="view_assigment">
    <?php
    if(isset($_REQUEST['sec'])) {
    foreach($assign as $a ) {
        $time = $a->duedate-time()  ;
        if($time>0) {
            $time_re = get_date($time);
        } else {
            $time_re = $string['assignmentisdue'];
        }
        ?>
        <div class="title_assignment" style="text-align: center;">
            <?php echo $a->name ?>
        </div>
        <div class="content">
            <p class="sb_st"><?php echo $string['submissionstatus']; ?></p>
            <p><?php echo $string['submissionstatusheading']." : ".$submission_status ?></p>
            <p><?php echo $string['gradingstatus'].' : '.$string['notgraded']?></p>
            <p><?php echo $string['duedate'].' : '.date('d/m/Y',$a->duedate) ?></p>
            <p><?php echo $string['timeremaining'].' : '.$time_re ?></p>
            <p><?php echo $string['timemodified'].' : ';if(!empty($lastmodified )) echo  date('d/m/Y',$lastmodified); else echo date('d/m/Y',$a->timemodified) ?></p>
            <p><?php echo $string['submissioncomments'].' : '.$count_comment ?></p>
        </div>
        <?php if(empty($status_sb) || $status_sb!=='submitted') {?>
        <div class="bttn">
        <div class="submission-add">
        <a class="add" href="<?php echo $url_add ?>"><span class="glyphicon glyphicon-open"> </span> <?php if(empty($submission)) { echo '<span style="float:left;">Add Submission</span><img class="img-sub-download" src="pix/icon_sb.png">'; } else echo '<span style="float:left;">Edit Submission</span><img class="img-sub-download" src="pix/icon_sb.png"';?></a>
    <?php }
    if(!empty($status_sb) && $status_sb!=='submitted') {
        ?>
        <a class="submit"  onclick="return confirm('Are you sure you want to submit?');" href="add_sub.php?id=<?php echo $id_sb ?>&id_as=<?php echo $id?>"><?php echo $string['submitassignment']; ?></a>
        </div>
        </div>
    <?php
    }
    ?>


    <?php }
    } else {

    // =======================================file================================================
    // =======================================file================================================
    // =======================================file================================================

    function create_zip($files = array(),$destination = '',$overwrite = true) {
        if(file_exists($destination) && !$overwrite) { return false; }
        $valid_files = array();
        if(is_array($files)) {
            foreach($files as $file) {
                if(file_exists($file)) {
                    $valid_files[] = $file;
                }
            }
        }
        if(count($valid_files)) {
            $zip = new ZipArchive();
            if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                return false;
            }
            foreach($valid_files as $file) {
                $zip->addFile($file,$file);
            }
            $zip->close();
            return file_exists($destination);
        } else {
            return false;
        }
    }


    //    foreach ($assign as $value) {
    //        $string = $value->intro;
    //    }

    //    $array = explode('|_|_|', $string);
    //    $file_name_upload = str_replace('"', '', $array[1]);
    //
    //    if(strlen($file_name_upload)>3) {
    //        $dir    = $CFG->dataroot.'/joomla/files/course/'.$cm->course.'/assign/';
    //        $dir_new= $CFG->dirroot.'/mod/assign/';
    //        copy($dir.$file_name_upload, $dir_new.$file_name_upload);
    //        $files_to_zip[] = $file_name_upload;
    //        $result = create_zip($files_to_zip,'assignmentbrief.zip');
    //        foreach ($files_to_zip as $file) {
    //            unlink($file);
    //        }
    //        $url_dowload = 'href="'.new moodle_url('/mod/assign/assignmentbrief.zip').'"';
    //    } else {
    //        $url_dowload = '';
    //    }

    foreach($assign as $a ) {
    $url_dowload = '';
    if (!empty($a->params)) {
        $prs = json_decode($a->params);
        $itemid = $prs->itemid;
        $fname = $prs->fname;
        $url_dowload = $CFG->wwwroot . '/pluginfile.php/' . $context->id . '/mod_assign/brief/'.$itemid.'/'.$fname;
        if ($itemid == 0 || $fname == '' || !@file_get_contents($url_dowload,0,NULL,0,1)) {
            $url_dowload = '';
        }
    }
    // format description paragraph
        $options['noclean'] = true;
        $a->intro = format_text($a->intro, FORMAT_MOODLE, $options);
    ?>
        <div class="title_assignment" style="text-align: center;"><?php echo $a->name ?></div>
        <div style="clear:both;"></div>
        <div class="content">
            <div><img style="margin-bottom:15px;" src="<?php echo new moodle_url('/mod/assign/assignment.png');?>" class="img-submission"/></div>
            <p class="a_p pull-left" style="float:left">Introduction</p>
            <p class="a_p pull-right" style="float:right"><?php  foreach($assign as $a ) { echo "Due: ".date('d/m/Y',$a->duedate); }?></p>
            <p class="intro" id="intro" ><?php echo $a->intro; ?></p>
        </div>
        <div class="bttn">
            <?php if ($url_dowload != '') { ?>
            <a class="download btn-assign" href="<?php echo $url_dowload; ?>"><span style="float:left;">Assignment Brief</span><img width="20" height="20" class="img-sub-download" src="<?php echo new moodle_url('/mod/assign/pix/Downloadload.png');?>" ></a>
            <?php } ?>

            <!--======================Download Assignment Brief=============================-->
            <!--======================Download Assignment Brief=============================-->
            <div style="clear:both;height:50px;"></div>
            <a class="btn-assign" href="<?php echo $url_sb; ?>" class="respond"> Submit &rarr;</a>
            <div style="clear:both; margin-bottom:30px;"></div>
        </div>
    <?php } ?>
        <script type="text/javascript">
            <?php if($url_dowload=='') { ?>
            $('.download').click(function(){
                alert('Empty file.');
            });
            <?php } ?>
        </script>
    <?php } ?>
</div>