<?php

/**
 * Attendance Register view page
 *
 * @package    mod
 * @subpackage attendanceregister
 * @author Lorenzo Nicora <fad@nicus.it>
 *
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// Disable output buffering
define('NO_OUTPUT_BUFFERING', true);


require('../../config.php');
require_once("lib.php");
require_once($CFG->libdir . '/completionlib.php');

// Main parameters
$userId = optional_param('userid', 0, PARAM_INT);   // if $userId = 0 you'll see all logs
$id = optional_param('id', 0, PARAM_INT);           // Course Module ID, or
$a = optional_param('a', 0, PARAM_INT);             // register ID
$groupId = optional_param('groupid', 0, PARAM_INT);             // Group ID
// Other parameters
$inputAction = optional_param('action', '', PARAM_ALPHA);   // Available actions are defined as ATTENDANCEREGISTER_ACTION_*
// Parameter for deleting offline session
$inputSessionId = optional_param('session', null, PARAM_INT);

// =========================
// Retrieve objects
// =========================

if ($id) {
    $cm = get_coursemodule_from_id('attendanceregister', $id, 0, false, MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $register = $DB->get_record('attendanceregister', array('id' => $cm->instance), '*', MUST_EXIST);
} else {
    $register = $DB->get_record('attendanceregister', array('id' => $a), '*', MUST_EXIST);
    $cm = get_coursemodule_from_instance('attendanceregister', $register->id, $register->course, false, MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $id = $cm->id;
}

// Retrive session to delete
$sessionToDelete = null;
if ($inputSessionId) {
    $sessionToDelete = attendanceregister_get_session($inputSessionId);
}

// ================================
// Basic security checks
// ================================
// Requires login
require_course_login($course, false, $cm);

// Retrieve Context
if (!($context = context_module::instance($cm->id))) {
    print_error('badcontext');
}

// Preload User's Capabilities
$userCapabilities = new attendanceregister_user_capablities($context);

// If user is not defined AND the user has NOT the capability to view other's Register
// force $userId to User's own ID
if ( !$userId && !$userCapabilities->canViewOtherRegisters) {
    $userId = $USER->id;
}
// (beyond this point, if $userId is specified means you are working on one User's Register
//  if not you are viewing all users Sessions)


// ==================================================
// Determine Action and checks specific permissions
// ==================================================
/// These capabilities checks block the page execution if failed

// Requires capabilities to view own or others' register
if ( attendanceregister__isCurrentUser($userId) ) {
    require_capability(ATTENDANCEREGISTER_CAPABILITY_VIEW_OWN_REGISTERS, $context);
} else {
    require_capability(ATTENDANCEREGISTER_CAPABILITY_VIEW_OTHER_REGISTERS, $context);
}

// Require capability to recalculate
$doRecalculate = false;
$doScheduleRecalc = false;
if ($inputAction == ATTENDANCEREGISTER_ACTION_RECALCULATE ) {
    require_capability(ATTENDANCEREGISTER_CAPABILITY_RECALC_SESSIONS, $context);
    $doRecalculate = true;
}
if ($inputAction == ATTENDANCEREGISTER_ACTION_SCHEDULERECALC ) {
    require_capability(ATTENDANCEREGISTER_CAPABILITY_RECALC_SESSIONS, $context);
    $doScheduleRecalc = true;
}


// Printable version?
$doShowPrintableVersion = false;
if ($inputAction == ATTENDANCEREGISTER_ACTION_PRINTABLE) {
    $doShowPrintableVersion = true;
}

/// Check permissions and ownership for showing offline session form or saving them
$doShowOfflineSessionForm = false;
$doSaveOfflineSession = false;
// Only if Offline Sessions are enabled (and No printable-version action)
if ( $register->offlinesessions &&  !$doShowPrintableVersion  ) {
    // Only if User is NOT logged-in-as, or ATTENDANCEREGISTER_ALLOW_LOGINAS_OFFLINE_SESSIONS is enabled
    if ( !session_is_loggedinas() || ATTENDANCEREGISTER_ALLOW_LOGINAS_OFFLINE_SESSIONS ) {
        // If user is on his own Register and may save own Sessions
        // or is on other's Register and may save other's Sessions..
        if ( $userCapabilities->canAddThisUserOfflineSession($register, $userId) ) {
            // Do show Offline Sessions Form
            $doShowOfflineSessionForm = true;

            // If action is saving Offline Session...
            if ( $inputAction == ATTENDANCEREGISTER_ACTION_SAVE_OFFLINE_SESSION  ) {
                // Check Capabilities, to show an error if a security violation attempt occurs
                if ( attendanceregister__isCurrentUser($userId) ) {
                    require_capability(ATTENDANCEREGISTER_CAPABILITY_ADD_OWN_OFFLINE_SESSIONS, $context);
                } else {
                    require_capability(ATTENDANCEREGISTER_CAPABILITY_ADD_OTHER_OFFLINE_SESSIONS, $context);
                }

                // Do save Offline Session
                $doSaveOfflineSession = true;
            }
        }
    }
}


/// Check capabilities to delete self cert
// (in the meanwhile retrieve the record to delete)
$doDeleteOfflineSession = false;
if ($sessionToDelete) {
    // Check if logged-in-as Session Delete
    if (session_is_loggedinas() && !ATTENDANCEREGISTER_ACTION_SAVE_OFFLINE_SESSION) {
        print_error('onlyrealusercandeleteofflinesessions', 'attendanceregister');
    } else if ( attendanceregister__isCurrentUser($userId) ) {
        require_capability(ATTENDANCEREGISTER_CAPABILITY_DELETE_OWN_OFFLINE_SESSIONS, $context);
        $doDeleteOfflineSession = true;
    } else {
        require_capability(ATTENDANCEREGISTER_CAPABILITY_DELETE_OTHER_OFFLINE_SESSIONS, $context);
        $doDeleteOfflineSession = true;
    }
}

// ===========================
// Retrieve data to be shown
// ===========================

// Retrieve Course Completion info object
$completion = new completion_info($course);


// If viewing/updating one User's Register, load the user into $userToProcess
// and retireve User's Sessions or retrieve the Register's Tracked Users
// If viewing all Users load tracked user list
$userToProcess = null;
$userSessions = null;
$trackedUsers = null;
if ( $userId ) {
    $userToProcess = attendanceregister__getUser($userId);
    $userToProcessFullname = fullname($userToProcess);
    $userSessions = new attendanceregister_user_sessions($register, $userId, $userCapabilities);
} else {
    $trackedUsers = new attendanceregister_tracked_users($register, $userCapabilities);
}


// ===========================
// Pepare PAGE for rendering
// ===========================
// Setup PAGE
$url = attendanceregister_makeUrl($register, $userId, $groupId, $inputAction);
$PAGE->set_url($url->out());
$PAGE->set_context($context);
$titleStr = $course->shortname . ': ' . $register->name . ( ($userId) ? ( ': ' . $userToProcessFullname ) : ('') );
$PAGE->set_title(format_string($titleStr));

$PAGE->set_heading($course->fullname);
if ($doShowPrintableVersion) {
    $PAGE->set_pagelayout('print');
}

// Add User's Register Navigation node
if ( $userToProcess ) {
    $registerNavNode = $PAGE->navigation->find($cm->id, navigation_node::TYPE_ACTIVITY);
    $userNavNode = $registerNavNode->add( $userToProcessFullname, $url );
    $userNavNode->make_active();
}


// ==================================================
// Logs User's action and update completion-by-view
// ==================================================

attendanceregister_add_to_log($register, $cm->id, $inputAction, $userId, $groupId);

/// On View Completion [fixed with isse #52]        
// If current user is the selected user (and completion is enabled) mark module as viewed
if ( $userId == $USER->id && $completion->is_enabled($cm) ) {
    $completion->set_module_viewed($cm, $userId);
}    
        

// ==============================================
// Start Page Rendering
// ==============================================
echo $OUTPUT->header();
//$headingStr = $register->name . ( ( $userId ) ? (': ' . $userToProcessFullname ) : ('') );
//echo '<h4>Unit : '.$course->fullname . '</h4>';
//echo '<h4>Mandatory Learning Time : '.$register->name.'</h4><br/>';
//echo '<h4>'.$register->name.'</h4><br/>';
//echo '<h4>Student '.( ( $userId ) ? (': ' . $userToProcessFullname ) : ('') ).'</h4>';
echo '<div class="mod-content">';
echo $OUTPUT->heading(format_string($headingStr));


// ==============================================
// Pepare Offline Session insert form, if needed
// ==============================================
// If a userID is defined, offline sessions are enabled and the user may insert Self.certificatins...
// ...prepare the Form for Self.Cert.
// Process the form (if submitted)
// Note that the User is always the CURRENT User (no UserId param is passed by the form)
$doShowContents = true;
$mform = null;
if ($userId && $doShowOfflineSessionForm && !$doShowPrintableVersion ) {

    // Prepare Form
    $customFormData = array('register' => $register,'courses' => $userSessions->trackedCourses->courses);
    // Also pass userId only if is saving for another user
    if (!attendanceregister__isCurrentUser($userId)) {
        $customFormData['userId'] = $userId;
    }
    $mform = new mod_attendanceregister_selfcertification_edit_form(null, $customFormData);


    // Process Self.Cert Form submission
    if ($mform->is_cancelled()) {
        // Cancel
        redirect($PAGE->url);
    } else if ($doSaveOfflineSession && ($formData = $mform->get_data())) {
        // Save Session
        attendanceregister_save_offline_session($register, $formData);

        // Notification & Continue button
        echo $OUTPUT->notification(get_string('offline_session_saved', 'attendanceregister'), 'notifysuccess');
        echo $OUTPUT->continue_button(attendanceregister_makeUrl($register, $userId));
        $doShowContents = false;
    }
}

//// Process Recalculate
if ($doShowContents && ($doRecalculate||$doScheduleRecalc)) {

    //// Recalculate Session for one User
    if ($userToProcess) {
        $progressbar = new progress_bar('recalcbar', 500, true);
        attendanceregister_force_recalc_user_sessions($register, $userId, $progressbar);

        // Reload User's Sessions
        $userSessions = new attendanceregister_user_sessions($register, $userId, $userCapabilities);
    }

    //// Recalculate (or schedule recalculation) of all User's Sessions
    else {

        //// Schedule Recalculation?
        if ( $doScheduleRecalc ) {
            // Set peding recalc, if set
            if ( !$register->pendingrecalc ) {
                attendanceregister_set_pending_recalc($register, true);
            }
        }

        //// Recalculate Session for all User
        if ( $doRecalculate ) {
            // Reset peding recalc, if set
            if ( $register->pendingrecalc ) {
                attendanceregister_set_pending_recalc($register, false);
            }

            // Turn off time limit: recalculation can be slow
            set_time_limit(0);

            // Cleanup all online Sessions & Aggregates before recalculating [issue #14]
            attendanceregister_delete_all_users_online_sessions_and_aggregates($register);

            // Reload tracked Users list before Recalculating [issue #14]
            $newTrackedUsers = attendanceregister_get_tracked_users($register);

            // Iterate each user and recalculate Sessions
            foreach ($newTrackedUsers as $user) {

                // Recalculate Session for one User
                $progressbar = new progress_bar('recalcbar_' . $user->id, 500, true);
                attendanceregister_force_recalc_user_sessions($register, $user->id, $progressbar, false); // No delete needed, having done before [issue #14]
            }
            // Reload All Users Sessions
            $trackedUsers = new attendanceregister_tracked_users($register, $userCapabilities);
        }
    }

    // Notification & Continue button
    if ( $doRecalculate || $doScheduleRecalc ) {
        $notificationStr = get_string( ($doRecalculate)?'recalc_complete':'recalc_scheduled', 'attendanceregister');
        echo $OUTPUT->notification($notificationStr, 'notifysuccess');
    }
    echo $OUTPUT->continue_button(attendanceregister_makeUrl($register, $userId));
    $doShowContents = false;
}
//// Process Delete Offline Session Action
else if ($doShowContents && $doDeleteOfflineSession) {
    // Delete Offline Session
    attendanceregister_delete_offline_session($register, $sessionToDelete->userid, $sessionToDelete->id);

    // Notification & Continue button
    echo $OUTPUT->notification(get_string('offline_session_deleted', 'attendanceregister'), 'notifysuccess');
    echo $OUTPUT->continue_button(attendanceregister_makeUrl($register, $userId));
    $doShowContents = false;
}
//// Show Contents: User's Sesions (if $userID) or Tracked Users summary
else if ($doShowContents) {

    //// Show User's Sessions
    if ($userId) {

        /// Button bar

        echo $OUTPUT->container_start('attendanceregister_buttonbar btn-group');
        
        // Printable version button or Back to normal version
        $linkUrl = attendanceregister_makeUrl($register, $userId, null, ( ($doShowPrintableVersion) ? (null) : (ATTENDANCEREGISTER_ACTION_PRINTABLE)));
        echo $OUTPUT->single_button($linkUrl, (($doShowPrintableVersion) ? (get_string('back_to_normal', 'attendanceregister')) : (get_string('show_printable', 'attendanceregister'))), 'get');
        //echo $OUTPUT->single_button($CFG->wwwroot.'/mod/attendanceregister/view.php?a='.$cm->instance.'&userid='.$userId.'&action=recalc','Recalculate My Session', 'get' );//add by Zin
		// Back to Users List Button (if allowed & !printable)
        if ($userCapabilities->canViewOtherRegisters && !$doShowPrintableVersion) {
            echo $OUTPUT->single_button(attendanceregister_makeUrl($register), get_string('back_to_tracked_user_list', 'attendanceregister'), 'get');
        }
		echo '<button onClick="window.print()">Print</button>';//add by Zin
        echo $OUTPUT->container_end();  // Button Bar
        echo '<br />';

		///add by Zin//
		echo '<h4>Unit : '.$course->fullname . '</h4>';
		echo '<h4>Student '.( ( $userId ) ? (': ' . $userToProcessFullname ) : ('') ).'</h4>';
		
		if ($userId==$USER->id){
		echo '<h4>Student ID'.( ( $userId ) ? (': ' . $USER->id ) : ('') ).'</h4>';
		}

        $hours = $DB->get_record('attendanceregister',array('id'=>$cm->instance))->completiontotaldurationmins / 60;
        $hours = !empty($hours) ? number_format($hours, 2) : $hours;
        echo '<h4>Required Hours : '.$hours. ' Hours</h4>';

		//print_object($USER);//test to retrive student ID added by Zin;
        /// Offline Session Form
        // Show Offline Session Self-Certifiation Form (not in printable)
        if ($mform && $register->offlinesessions && !$doShowPrintableVersion) {
            echo "<br />";
            echo $OUTPUT->box_start('generalbox attendanceregister_offlinesessionform');
            $mform->display();
            echo $OUTPUT->box_end();
        }

//    // Show tracked Courses
//    echo '<div class="table-responsive">';
//    echo html_writer::table( $userSessions->trackedCourses->html_table()  );
//    echo '</div>';    

        // Show User's Sessions summary
        echo '<div class="table-responsive">';
        echo html_writer::table($userSessions->userAggregates->html_table());
        echo '</div>';
        
        echo '<div class="table-responsive">';       
        echo html_writer::table($userSessions->html_table());
        echo '</div>';
    }

    //// Show list of Tracked Users summary
    else {

        /// Button bar
        // Show Recalc pending warning
        if ( $register->pendingrecalc && $userCapabilities->canRecalcSessions && !$doShowPrintableVersion ) {
            echo $OUTPUT->notification( get_string('recalc_scheduled_on_next_cron', 'attendanceregister')  );
        }
        // Show cron not yet run on this instance
        else if ( !attendanceregister__didCronRanAfterInstanceCreation($cm) ) {
            echo $OUTPUT->notification( get_string('first_calc_at_next_cron_run', 'attendanceregister')  );
        }

        echo $OUTPUT->container_start('attendanceregister_buttonbar btn-group');
        
        // If current user is tracked, show view-my-sessions button [feature #28]
        if ( $userCapabilities->isTracked ) {
            $linkUrl = attendanceregister_makeUrl($register, $USER->id);
            echo $OUTPUT->single_button($linkUrl, get_string('show_my_sessions' ,'attendanceregister'), 'get' );
			echo $OUTPUT->single_button($CFG->wwwroot.'/mod/attendanceregister/view.php?a='.$cm->instance.'&action=recalc','Recalculate Sessions', 'get' );//add by Zin
        }
        
        // Printable version button or Back to normal version
        $linkUrl = attendanceregister_makeUrl($register, null, null, ( ($doShowPrintableVersion) ? (null) : (ATTENDANCEREGISTER_ACTION_PRINTABLE)));
        echo $OUTPUT->single_button($linkUrl, (($doShowPrintableVersion) ? (get_string('back_to_normal', 'attendanceregister')) : (get_string('show_printable', 'attendanceregister'))), 'get');
        echo '<button onClick="window.print()">Print</button>';//add by Zin
        echo $OUTPUT->container_end();  // Button Bar
        echo '<br />';

        // Show list of tracked courses
        echo '<div class="table-responsive">'; 
        echo html_writer::table($trackedUsers->trackedCourses->html_table());
        echo '</div>';

        $ar = unserialize($trackedUsers->trackedCourses->courses[$course->id]->modinfo) [$id] ;

        $gm = array(
            1=>'Separate groups',
            2=>'Visible groups'
        );
        echo "</br>";
//        echo ($ar->groupmode != 0 ) ? "<p>".$gm[$ar->groupmode]."</p>":'';
        if ($ar->groupmode != 0 ) {
            $groupings = $DB->get_records('groupings', array('courseid'=>$course->id));
            $options = array();
            $options[0] = get_string('all');
            if ($groupings) {
                foreach ($groupings as $grouping) {
                    $options[$grouping->id] = format_string($grouping->name);
                }
            }

            $rooturl   = $CFG->wwwroot.'/mod/attendanceregister/view.php?id='.$id;
            $groupid    = optional_param('group', 0, PARAM_INT);

            $groupingid = (is_null($ar->groupingid)) ? optional_param('grouping', 0, PARAM_INT) : $ar->groupingid;
            $groupingid = (is_null($_GET['grouping'])) ? $groupingid : optional_param('grouping', 0, PARAM_INT);
            $strgrouping         = get_string('grouping', 'group');
            $strgroup            = ($ar->groupmode != 0 ) ? $gm[$ar->groupmode]: get_string('group', 'group');

            $popupurl = new moodle_url($rooturl.'&group='.$groupid);
            $select = new single_select($popupurl, 'grouping', $options, $groupingid, array());
            $select->label = $strgrouping;
            $select->formid = 'selectgrouping';
            echo $OUTPUT->render($select);

            $members = array();
            foreach ($groupings as $grouping) {
                $members[$grouping->id] = array();
            }

            $groups = $DB->get_records('groups', array('courseid'=>$course->id), 'name');

            $options = array();
            $options[0] = get_string('all');
            foreach ($groups as $group) {
                $options[$group->id] = strip_tags(format_string($group->name));
            }
            $popupurl = new moodle_url($rooturl.'&grouping='.$groupingid);
            $select = new single_select($popupurl, 'group', $options, $groupid, array());
            $select->label = $strgroup;
            $select->formid = 'selectgroup';
            echo $OUTPUT->render($select);

            if ($groupingid != 0 || $groupid != 0) {

                $params = array('courseid'=>$course->id);
                if ($groupid) {
                    $groupwhere = "AND g.id = :groupid";
                    $params['groupid']   = $groupid;
                } else {
                    $groupwhere = "";
                }

                if ($groupingid) {
                    $groupingwhere = "AND gg.groupingid = :groupingid";
                    $params['groupingid'] = $groupingid;

                } else {
                    $groupingwhere = "";
                }

                list($sort, $sortparams) = users_order_by_sql('u');

                if ($groupingid != 0 && $groupid != 0) {
                    $fquery = "SELECT g.id AS groupid, gg.groupingid, u.id AS userid, u.firstname, u.lastname, u.idnumber, u.username
                    FROM {groups} g
                       LEFT JOIN {groupings_groups} gg ON g.id = gg.groupid
                       LEFT JOIN {groups_members} gm ON g.id = gm.groupid
                       LEFT JOIN {user} u ON gm.userid = u.id
                    WHERE g.courseid = :courseid $groupwhere
                    ORDER BY g.name, $sort";

                    $frs = $DB->get_recordset_sql($fquery, array_merge($params, $sortparams));
                    $arr = array();
                    foreach ($frs as $row) {
                        $arr[$row->userid] = $row->userid;
                    }
                    $frs->close();
                    $squery = "SELECT g.id AS groupid, gg.groupingid, u.id AS userid, u.firstname, u.lastname, u.idnumber, u.username
                    FROM {groups} g
                       LEFT JOIN {groupings_groups} gg ON g.id = gg.groupid
                       LEFT JOIN {groups_members} gm ON g.id = gm.groupid
                       LEFT JOIN {user} u ON gm.userid = u.id
                    WHERE g.courseid = :courseid $groupingwhere
                    ORDER BY g.name, $sort";

                    $srs = $DB->get_recordset_sql($squery, array_merge($params, $sortparams));

                    $ar = array();
                    foreach ($srs as $row) {
                        if (array_key_exists($row->userid, $arr)) {
                            $ar[] = $row;
                        }
                    }
                    $srs->close();
                    $rs = $ar;
                } else {
                    $sql = "SELECT g.id AS groupid, gg.groupingid, u.id AS userid, u.firstname, u.lastname, u.idnumber, u.username
                    FROM {groups} g
                       LEFT JOIN {groupings_groups} gg ON g.id = gg.groupid
                       LEFT JOIN {groups_members} gm ON g.id = gm.groupid
                       LEFT JOIN {user} u ON gm.userid = u.id
                    WHERE g.courseid = :courseid $groupwhere $groupingwhere
                    ORDER BY g.name, $sort";

                    $rs = $DB->get_recordset_sql($sql, array_merge($params, $sortparams));
                }

                $membersIDArray = array();
                foreach ($rs as $row) {
                    $user = new stdClass();
                    $user->id        = $row->userid;
                    $user->firstname = $row->firstname;
                    $user->lastname  = $row->lastname;
                    $user->username  = $row->username;
                    $user->idnumber  = $row->idnumber;
                    if (!$row->groupingid) {
                        $row->groupingid = -1;
                    }
                    if (!array_key_exists($row->groupid, $members[$row->groupingid])) {
                        $members[$row->groupingid][$row->groupid] = array();
                    }
                    if(isset($user->id)){
                        $members[$row->groupingid][$row->groupid][] = $user;
                        $membersIDArray[$user->id] = $user->id;
                    }
                }
                if ($groupingid == 0 || $groupid == 0) {
                    $rs->close();
                }
                foreach ($trackedUsers->users as $k => $v) {
                    if (!array_key_exists($v->id, $membersIDArray)) {
                        unset($trackedUsers->users[$k]);
                    }
                }
            }



        }
        echo "</br></br>";

        // Show tracked Users list
        echo '<div class="table-responsive">'; 
        echo html_writer::table($trackedUsers->html_table());
        echo '</div>';
    }
}


echo '</div>';

// Output page footer
if (!$doShowPrintableVersion) {
    echo $OUTPUT->footer();
}