<?php
/**
 * create by KV team, 19 Nov 2015
 */

require_once('config.php');
require_once('lib/filelib.php');

$path = get_file_argument();
$forcedownload = optional_param('forcedownload', 0, PARAM_BOOL);
$preview = optional_param('preview', null, PARAM_ALPHANUM);
$name = optional_param('n', null, PARAM_TEXT);

send_stored_file_joomla($path, 60 * 60 * 24 * 365, 0, $forcedownload, array('preview' => $preview, 'name' => $name));

?>