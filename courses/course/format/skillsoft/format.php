<?php 
/*
* Skillsoft course format. Display format name "Skillsoft"
* @package format_parenthesis
* @copyright 2015 Kydon vietnam
* @author Dungnv (Dave), and others.
*/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/completionlib.php');

// Horrible backwards compatible parameter aliasing..
if ($skillsoft = optional_param('skillsoft', 0, PARAM_INT)) {
    $url = $PAGE->url;
    $url->param('section', $skillsoft);
    debugging('Outdated parenthesis param passed to course/view.php', DEBUG_DEVELOPER);
    redirect($url);
}
// End backwards-compatible aliasing..
$context = context_course::instance($course->id);

if (($marker >=0) && has_capability('moodle/course:setcurrentsection', $context) && confirm_sesskey()) {
    $course->marker = $marker;
    course_set_marker($course->id, $marker);
}

// make sure all sections are created
$course = course_get_format($course)->get_course();
course_create_sections_if_missing($course, range(0, $course->numsections));

// update course sections for parenthesis format
$sections = get_all_sections($course->id);
$already_skillsoft = true;
get_all_mods($course->id, $mods, $modnames, $modnamesplural, $modnamesused);
foreach ($sections as $section) {
    if(!empty($section->sequence)) {
        $already_skillsoft = false;
    }
}
if($already_skillsoft) {
    skillsoft_add_activity($course, range(0, $course->numsections));
}

// end

//$course->realcoursedisplay = $course->coursedisplay == COURSE_DISPLAY_MULTIPAGE;
//$course->coursedisplay = COURSE_DISPLAY_MULTIPAGE;

$renderer = $PAGE->get_renderer('format_skillsoft');

$section = optional_param('section', -1, PARAM_INT);

if (isset($section) && $section >= 0) {
     $USER->display[$course->id] = $section;
     $displaysection = $section;
} 
else {
    if (isset($USER->display[$course->id])) {
        $displaysection = $USER->display[$course->id];
    } else {
        $USER->display[$course->id] = 0;
        $displaysection = 0;
    }
}
//$is_manager = is_manager_mycourse();
if (!empty($displaysection)) {
    $renderer->print_single_section_page($course, null, null, null, null, $displaysection);
} else {
    $renderer->print_multiple_section_page($course, null, null, null, null);
}

$PAGE->requires->js('/course/format/topics/format.js');
?>
