<?php 
/*
* @package format Skillsoft
* @copyright 2015	
*/


defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot.'/course/format/renderer.php');
require_once($CFG->dirroot.'/course/format/skillsoft/lib.php');

class format_skillsoft_renderer extends format_section_renderer_base {

	protected function start_section_list () {
		return html_writer::start_tag('ul', array('class' => 'skillsoft'));
	}
	 /**
     * Generate the closing container html for a list of sections
     * @return string HTML to output.
     */
    	protected function end_section_list() {
        	return html_writer::end_tag('ul');
    	}

	protected function page_title() {
        	return get_string('skillsoftoutline');
    	}
}
?>
