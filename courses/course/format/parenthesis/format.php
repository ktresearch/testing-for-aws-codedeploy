<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Parenthesis course format.  Display the whole course as "Parenthesis" made of modules.
 *
 * @package format_parenthesis
 * @copyright 2015 Kydon Vietnam
 * @author Dungnv(Dave), and others.
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/completionlib.php');

// Horrible backwards compatible parameter aliasing..
if ($parenthesis = optional_param('parenthesis', 0, PARAM_INT)) {
    $url = $PAGE->url;
    $url->param('section', $parenthesis);
    debugging('Outdated parenthesis param passed to course/view.php', DEBUG_DEVELOPER);
    redirect($url);
}
// End backwards-compatible aliasing..
$context = context_course::instance($course->id);

if (($marker >=0) && has_capability('moodle/course:setcurrentsection', $context) && confirm_sesskey()) {
    $course->marker = $marker;
    course_set_marker($course->id, $marker);
}

// make sure all sections are created
$course = course_get_format($course)->get_course();
course_create_sections_if_missing($course, range(0, $course->numsections));

// update course sections for parenthesis format
$sections = get_all_sections($course->id);
$sectionname = array();
foreach ($sections as $insection) {
    $sectionname[] = $insection->name;
}
$sectionname = array_filter($sectionname);
if(empty($sectionname)) {
    course_update_sections($course, range(0, $course->numsections));
}
// end

//$course->realcoursedisplay = $course->coursedisplay == COURSE_DISPLAY_MULTIPAGE;
//$course->coursedisplay = COURSE_DISPLAY_MULTIPAGE;

$renderer = $PAGE->get_renderer('format_parenthesis');

$section = optional_param('section', -1, PARAM_INT);

if (isset($section) && $section >= 0) {
     $USER->display[$course->id] = $section;
     $displaysection = $section;
} 
else {
    if (isset($USER->display[$course->id])) {
        $displaysection = $USER->display[$course->id];
    } else {
        $USER->display[$course->id] = 0;
        $displaysection = 0;
    }
}
$is_manager = is_manager_mycourse();
if (!$is_manager) {
    if($section==0)
    {
       $renderer->print_multiple_section_page($course, null, null, null, null);
    }  
    else
    $renderer->print_parenthesis_page($course, null, null, null, null, $displaysection);
} else {
    $renderer->print_multiple_section_page($course, null, null, null, null);
}

//$PAGE->requires->js('/course/format/topics/format.js');

