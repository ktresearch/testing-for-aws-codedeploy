<?php
// This file is part of the Parenthexis theme for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle's Joomdle bootstrap theme
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * Settings file for Parenthexis theme
 *
 * @package   Parenthexis theme
 * @copyright 2013 Fernando Acedo, http://3-bits.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    


//Body background
    $name = 'theme_parenthexis/bodybgcolor';
    $title = get_string('bodybgcolor','theme_parenthexis');
    $description = get_string('bodybgcolordesc', 'theme_parenthexis');
    $default = '#fdfdfd';
    $previewconfig = NULL;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $settings->add($setting);

//Link color
    $name = 'theme_parenthexis/linkcolor';
    $title = get_string('linkcolor','theme_parenthexis');
    $description = get_string('linkcolordesc', 'theme_parenthexis');
    $default = '#0066cc';
    $previewconfig = NULL;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $settings->add($setting);

//Link Hover color
    $name = 'theme_parenthexis/linkhovercolor';
    $title = get_string('linkhovercolor','theme_parenthexis');
    $description = get_string('linkhovercolordesc', 'theme_parenthexis');
    $default = '#00cccc';
    $previewconfig = NULL;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $settings->add($setting);

    
//Menu background color
    $name = 'theme_parenthexis/menubgcolor';
    $title = get_string('menubgcolor','theme_parenthexis');
    $description = get_string('menubgcolordesc', 'theme_parenthexis');
    $default = '#0066cc';
    $previewconfig = NULL;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $settings->add($setting);

    
    
// Custom CSS   
	$name = 'theme_parenthexis/customcss';
	$title = get_string('customcss','theme_parenthexis');
	$description = get_string('customcssdesc', 'theme_parenthexis');
	$default = '';
	$setting = new admin_setting_configtextarea($name, $title, $description, $default);
	$settings->add($setting);

    


}

