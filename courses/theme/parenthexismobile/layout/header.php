<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * Logo and header of theme
 */

?>
<!DOCTYPE>
<html>
        <!-- Include Js, css file here -->
    <head>
        <title><?php echo $PAGE->title ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->pix_url('favicon', 'theme')?>" />
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href='<?php echo $CFG->wwwroot;?>/theme/parenthexismobile/style/custom.css' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <header role="banner" class="navbar navbar-fixed-top<?php echo $html->navbarclass ?>">
            <nav role="navigation" class="navbar-inner"  id="navigation">
            <div class="container-fluid">
                <a class="brand" style="padding:0px;margin:0px;" href="<?php echo $CFG->root;?>/index.php/homepage"><img id="img-logo" src="<?php echo $CFG->root;?>/courses/theme/parenthexismobile/pix/parenthexis-logo.png" alt="" style="padding:20px 20px 20px 26px;" /></a>
                 <a class="btn btn-navbar" id="navbar-mobile" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="nav-collapse collapse" id="div-nav-mobile">
                    <div id="central-menu"></div>
                    <?php echo $OUTPUT->custom_menu(); ?>
                    <ul class="nav pull-right">
                        <li><?php echo $OUTPUT->page_heading_menu(); ?></li>

                    </ul>
                </div>
            </div>
            </nav>
        </header>
    </body>