#Joomdlebs theme#

Joomdlebs is a theme for moodle 2.5 or newer and based in the Bootstrap moodle theme created by Bas Brands.

##Theme Credits##
Fernando Acedo - 3-bits.org

Contact: http://3-bits.org/contact


##Description##
This theme is created to be used in Joomdle sites only (http://joomdle.com) Some parts like the header, footer and login link have been removed deliberately. 

*DO NOT USE IN MOODLE STAND ALONE INSTALLATIONS*


##Settings##
You can change the main colors from the backend: site background, links, links hover and menu background.


##Report a bug##
To report issues or bugs:
https://github.com/3-bits/joomdlebs/issues


##Updates and Support##
Visit our site for support and tips & tricks
http://3-bits.org/forum

Visit our GitHub for the latest update
https://github.com/3-bits/joomdlebs


##Tweaks##
If you want to modify this theme, we recommend that you first duplicate it and then rename it before making your changes. 
This will prevent your customized theme from being overwritten by future upgrades, and you will still have the original files if you make a mess.


##License##
This theme is licensed under the GNU General Public License. (see http://www.gnu.org/licenses/gpl.html )


##Changelog##

- 1.0 First release
- 1.1 Use of bootstrapbase3b as a base theme. You must install bootstrapbase3b in order to use this theme
- 1.2 Fixed the screenshot and some bugs
- 1.3 Compatible with moodle version 2.5, 2.6 and 2.7
