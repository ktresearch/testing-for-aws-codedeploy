<?php
// This file is part of the joomdleBS theme for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * English Language strings file for joomdle theme
 *
 * @package   joomdleBS theme
 * @copyright 2013 Fernando Acedo, http://3-bits.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 
 
$string['configtitle'] = 'joomdlebs';
$string['pluginname'] = 'Joomdle Bootstrap';

// Settings page

$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['purgecaches'] = 'Purge caches';
$string['notes'] =  'Joomdle Configuration page';
$string['notesdesc'] = 'Configure your theme';
$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Add your extra CSS in this box, use it to customize your theme with a little css';


$string['bodybgcolor'] = 'Body background color';
$string['linkcolor'] = 'Link color';
$string['linkhovercolor'] = 'Link hover color';
$string['menubgcolor'] = 'Menu background color';

$string['bodybgcolordesc'] = 'Body background color';
$string['linkcolordesc'] = 'Link color';
$string['linkhovercolordesc'] = 'Link hover color';
$string['menubgcolordesc'] = 'Menu background color';



// Theme info
$string['choosereadme'] = 
'<div class="clearfix"></div>
<div class="theme_screenshot">
<h2>joomdle bootstrap</h2>
<img src="joomdlebs/pix/screenshot.png" />

<h3>Theme Credits</h3><p>
<p>Fernando Acedo - 3-bits.com</p>
<p>Contact: <a href="http://3-bits.org">http://3-bits.org</a></p>
<br />

<div class="theme_description">
<h3>Description</h3>
<p>This theme is created to be used in <a href="http://joomdle.com">Joomdle<a/> sites only. Some parts like the header, footer and login link have been removed deliberately. It is based in the Bootstrap theme created for moodle by Bas Brand 

<strong>DO NOT USE IN MOODLE STAND ALONE INSTALLATIONS.</strong> 
</p>
<br />

<h3>Settings</h3>
<p>You can change the main colors from the backend: site background, links, links hover and menu background.
</p>
<br />


<h3>Report a bug</h3>
<p>To report issues or bugs:<br />
<a href="https://github.com/3-bits/joomdlebs/issues">https://github.com/3-bits/joomdlebs/issues</a>
</p>
<br />


<h3>Updates and Support</h3>
<p>Visit our site for support<br />
<a href="http://3-bits.org">http://3-bits.org</a>
</p>
<p>Visit our GitHub for latest update<br />
<a href="https://github.com/3-bits/joomdlebs">https://github.com/3-bits/joomdlebs</a>
</p>

<br />


<h3>Tweaks</h3>
<p>If you want to modify this theme, we recommend that you first duplicate it and then rename it before making your changes. 
This will prevent your customized theme from being overwritten by future upgrades, and you will still have the original files if you make a mess.</p>
<br />

<h3>License</h3>
<p>This theme is licensed under the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>.
</div></div>';

