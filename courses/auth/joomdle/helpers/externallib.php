<?php

require_once("$CFG->libdir/externallib.php");
require_once($CFG->dirroot.'/auth/joomdle/auth.php');
 
class joomdle_helpers_external extends external_api {
 
    /* user_id */
    public static function user_id_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'multilang compatible name, course unique'),
                        )
        );
    }

    public static function user_id_returns() {
        return new  external_value(PARAM_INT, 'multilang compatible name, course unique');
    }

    public static function user_id($username) { //Don't forget to set it as static
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::user_id_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->user_id ($username);

        return $id;
    }

    
    /* list_courses */
    public static function list_courses_parameters() {
        return new external_function_parameters(
                        array(
                            'enrollable_only' => new external_value(PARAM_INT, 'Return only enrollable courses'),
                            'sortby' => new external_value(PARAM_TEXT, 'Order field'),
                            'guest' => new external_value(PARAM_INT, 'Return only courses for guests'),
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'where' => new external_value(PARAM_TEXT, 'where'),
                            'swhere' => new external_value(PARAM_TEXT, 'swhere'),
                            'version' => new external_value(PARAM_TEXT, 'version', false),
                        )
        );
    }

    public static function list_courses_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                    'remoteid' => new external_value(PARAM_INT, 'course id'),
                    'cat_id' => new external_value(PARAM_INT, 'category id'),
                    'cat_name' => new external_value(PARAM_TEXT, 'cartegory name'),
                    'course_group' => new external_value(PARAM_RAW, 'group of course', false),
                    'activity_count' => new external_value(PARAM_RAW, 'group of course', false),
                    'cat_description' => new external_value(PARAM_RAW, 'category description'),
                    'sortorder' => new external_value(PARAM_TEXT, 'sortorder'),
                    'fullname' => new external_value(PARAM_TEXT, 'course name'),
                    'shortname' => new external_value(PARAM_TEXT, 'course shortname'),
                    'idnumber' => new external_value(PARAM_RAW, 'idnumber'),
                    'summary' => new external_value(PARAM_RAW, 'summary'),
                    'startdate' => new external_value(PARAM_INT, 'start date'),
                    'creator' => new external_value(PARAM_TEXT, 'creator'),
                    'count_learner' => new external_value(PARAM_INT, 'count learner', false),
                    'created' => new external_value(PARAM_INT, 'created'),
                    'isLearner' => new external_value(PARAM_BOOL, 'check user is learner', false),
                    'modified' => new external_value(PARAM_INT, 'modified'),
                    'timepublish' => new external_value(PARAM_INT, 'timepublish', false),
                    'last_access' => new external_value(PARAM_INT, 'last access', VALUE_OPTIONAL),
                    'enrol_time' => new external_value(PARAM_INT, 'enrol time', VALUE_OPTIONAL),
                    'timeend' => new external_value(PARAM_INT, 'timeend', VALUE_OPTIONAL),
                    'completion_status' => new external_value(PARAM_INT, 'completion status', VALUE_OPTIONAL),
                    'time_completed' => new external_value(PARAM_INT, 'time completed', VALUE_OPTIONAL),
                    'cost' => new external_value(PARAM_FLOAT, 'cost', VALUE_OPTIONAL),
                    'duration' => new external_value(PARAM_FLOAT, 'duration'),
                    'currency' => new external_value(PARAM_TEXT, 'currency', VALUE_OPTIONAL),
                    'self_enrolment' => new external_value(PARAM_INT, 'self enrollable', VALUE_OPTIONAL), 
                    'enroled' => new external_value(PARAM_INT, 'user enroled'),
                    'in_enrol_date' => new external_value(PARAM_BOOL, 'in enrol date', VALUE_OPTIONAL), 
                    'guest' => new external_value(PARAM_INT, 'guest access', VALUE_OPTIONAL),
                    'fileid' => new external_value(PARAM_TEXT, 'file id', VALUE_OPTIONAL),
                    'filetype' => new external_value(PARAM_TEXT, 'file type', VALUE_OPTIONAL),
                    'filepath' => new external_value(PARAM_TEXT, 'file path', VALUE_OPTIONAL),
                    'filename' => new external_value(PARAM_TEXT, 'file name', VALUE_OPTIONAL),
                    'coursetype' => new external_value(PARAM_TEXT, 'course type', VALUE_OPTIONAL),
                    'isFacilitator' => new external_value(PARAM_BOOL, 'check user is facilitator', false),
                )
            )
        );
    }

    public static function list_courses($enrollable_only, $sortby, $guest, $username, $where, $swhere, $version) { //Don't forget to set it as static
        global $CFG, $DB;
 
      $params = self::validate_parameters(self::list_courses_parameters(), array('enrollable_only'=>$enrollable_only, 'sortby' => $sortby,'guest' => $guest, 'username' => $username, 'where'=>$where, 'swhere'=>$swhere, 'version'=>$version));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->list_courses2 ($enrollable_only, $sortby, $guest, $username, $where, $swhere, $version);

        return $id;
    }

    /* list_programs */
    public static function list_progs_parameters() {
        return new external_function_parameters(
            array(
                'prog_id' => new external_value(PARAM_INT, 'program id'),
            )
        );
    }

    public static function list_progs_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'program id'),
                    'fullname' => new external_value(PARAM_TEXT, 'program name'),
                    'summary' => new external_value(PARAM_RAW, 'summary'),
                    'courses' => new external_value(PARAM_RAW, 'courses'),
                    'filepath' => new external_value(PARAM_RAW, 'filepath'),
                    'filename' => new external_value(PARAM_RAW, 'filename'),
                )
            )
        );
    }

    public static function list_progs($prog_id) { //Don't forget to set it as static
        global $CFG, $DB;

        $params = self::validate_parameters(self::list_progs_parameters(), array('prog_id'=>$prog_id));

        $auth = new  auth_plugin_joomdle ();
        $id = $auth->list_progs ($prog_id);

        return $id;
    }
    
    
    /* test */
    public static function test_parameters() {
        return new external_function_parameters(
                        array(
                            'content'=>  new external_value(PARAM_FILE, 'File'),
                            'courseid'=>  new external_value(PARAM_TEXT, 'course id'),
                        )
        );
    }

    public static function test_returns() {
         return  new external_value(PARAM_TEXT, 'test data');
    }

    public static function test($file, $courseid) { //Don't forget to set it as static
        global $CFG, $DB;
//       print_r($file); die;
        $params = self::validate_parameters(self::test_parameters(), array('content' => $file, 'courseid' => $courseid));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->test ($_FILES, $courseid);

        return $return;
    }
    
    //load question bank
    public static function load_questions_bank_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'Course id'),
                'aid' => new external_value(PARAM_INT, 'Assessment id', false),
                'detail' => new external_value(PARAM_INT, 'Detail', false)
            )
        );
    }

    public static function load_questions_bank_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'questions' => new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'Question ID'),
                    'category' => new external_value(PARAM_INT, 'Category ID'),
                    'name' => new external_value(PARAM_TEXT, 'Question Name'),
                    'questiontext' => new external_value(PARAM_RAW, 'Question text'),
                    'qtype' => new external_value(PARAM_TEXT, 'Question Type'),
                    'generalfeedback' => new external_value(PARAM_TEXT, 'generalfeedback', false),
                    'selected' => new external_value(PARAM_INT, 'Selected', false),
                    'options' => new external_value(PARAM_RAW, 'Options', false),
                )
            )
                    ),
            )
        );
    }

    public static function load_questions_bank($courseid, $aid, $detail) { //Don't forget to set it as static
        global $CFG, $DB;

        $params = self::validate_parameters(self::load_questions_bank_parameters(), array('courseid'=> $courseid, 'aid' => $aid, 'detail' => $detail));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->load_questions_bank ($courseid, $aid, $detail);

        return $return;
    }

    //delete list question
    public static function delete_questions_parameters() {
        return new external_function_parameters(
            array(
                'act' => new external_value(PARAM_INT, 'action'),
                'courseid' => new external_value(PARAM_INT, 'Course id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
                'qids' => new external_value(PARAM_TEXT, 'question id list', false),
            )
        );
    }

    public static function delete_questions_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'error status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
            )
        );
    }

    public static function delete_questions($act, $courseid, $username, $qids) { //Don't forget to set it as static
        global $CFG, $DB;

        $params = self::validate_parameters(self::delete_questions_parameters(), array('act'=>$act, 'courseid'=>$courseid, 'username'=>$username, 'qids'=>$qids));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->delete_questions($act, $courseid, $username, $qids);

        return $return;
    }

    //create question
    public static function create_question_parameters() {
        return new external_function_parameters(
            array(
                'act' => new external_value(PARAM_INT, 'action'),
                'courseid' => new external_value(PARAM_INT, 'Course id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
                'q' =>  new external_single_structure(
                    array(
                        'qid' => new external_value(PARAM_INT, 'question id', false),
                        'type' => new external_value(PARAM_TEXT, 'question type', false),
                        'name' => new external_value(PARAM_RAW, 'question name', false),
                        'questiontext' => new external_value(PARAM_RAW, 'question text', false),
                        'generalfeedback' => new external_value(PARAM_RAW, 'general feedback', false),
                        'feedbacktrue' => new external_value(PARAM_RAW, 'feedback true', false),
                        'feedbackfalse' => new external_value(PARAM_RAW, 'feedback false', false),
                        'correctanswer' => new external_value(PARAM_INT, 'correct answer', false),
                        'noanswers' => new external_value(PARAM_INT, 'number of answers', false),
                        'norightanswer' => new external_value(PARAM_INT, 'single or multiple', false),
                        'subquestions' => new external_value(PARAM_RAW, 'json subquestions', false),
                        'subanswers' => new external_value(PARAM_RAW, 'json subanswers', false),
                        'answers' => new external_value(PARAM_RAW, 'json answers', false),
                        'feedbacks' => new external_value(PARAM_RAW, 'json feedbacks', false),
                        'fraction' => new external_value(PARAM_RAW, 'json fraction', false),
                    )
                )
            )
        );
    }

    public static function create_question_returns() {
        return new external_single_structure(
            array(
                'error' => new external_value(PARAM_INT, 'error status'),
                'mes' => new external_value(PARAM_TEXT, 'message'),
                'data' => new external_value(PARAM_RAW, 'data'),
            )
        );
    }

    public static function create_question($act, $courseid, $username, $q) { //Don't forget to set it as static
        global $CFG, $DB;

        $params = self::validate_parameters(self::create_question_parameters(), array('act'=>$act, 'courseid'=>$courseid, 'username'=>$username, 'q'=>$q));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->create_question($act, $courseid, $username, $q);

        return $return;
    }
    
    public static function create_question_platform($act, $courseid, $username, $q) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::create_question_platform_parameters(), array('act'=>$act, 'courseid'=>$courseid, 'username'=>$username, 'q'=>$q));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->create_question_platform($act, $courseid, $username, $q);

        return $return;
    }
    
    public static function create_question_platform_parameters() {
        return new external_function_parameters(
            array(
                'act' => new external_value(PARAM_INT, 'action'),
                'courseid' => new external_value(PARAM_INT, 'Course id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
                'q' => new external_value(PARAM_RAW, 'data'),
                
//                'q' =>  new external_single_structure(
//                    array(
//                        'qid' => new external_value(PARAM_INT, 'question id', false),
//                        'type' => new external_value(PARAM_TEXT, 'question type', false),
//                        'name' => new external_value(PARAM_RAW, 'question name', false),
//                        'questiontext' => new external_value(PARAM_RAW, 'question text', false),
//                        'generalfeedback' => new external_value(PARAM_RAW, 'general feedback', false),
//                        'feedbacktrue' => new external_value(PARAM_RAW, 'feedback true', false),
//                        'feedbackfalse' => new external_value(PARAM_RAW, 'feedback false', false),
//                        'correctanswer' => new external_value(PARAM_INT, 'correct answer', false),
//                        'noanswers' => new external_value(PARAM_INT, 'number of answers', false),
//                        'norightanswer' => new external_value(PARAM_INT, 'single or multiple', false),
//                        'subquestions' => new external_value(PARAM_RAW, 'json subquestions', false),
//                        'subanswers' => new external_value(PARAM_RAW, 'json subanswers', false),
//                        'answers' => new external_value(PARAM_RAW, 'json answers', false),
//                        'feedbacks' => new external_value(PARAM_RAW, 'json feedbacks', false),
//                        'fraction' => new external_value(PARAM_RAW, 'json fraction', false),
//                    )
//                )
            )
        );
    }
    
    public static function create_question_platform_returns() {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL, 'status'),
                        'message' => new external_value(PARAM_TEXT, 'message'),
                        'question' => new external_single_structure(
                                    array(
                                        'name' => new external_value(PARAM_TEXT, 'question name'),
                                        'id' => new external_value(PARAM_INT, 'question id'),
                                        'qtype' => new external_value(PARAM_TEXT, 'question type'),
                                    )
                                ),
                    )
                );
    }

    //create quiz activity
    public static function create_quiz_activity_parameters() {
        return new external_function_parameters(
            array(
                'act' => new external_value(PARAM_INT, 'action'),
                'courseid' => new external_value(PARAM_INT, 'Course id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
                'q' =>  new external_single_structure(
                    array(
                        'qid' => new external_value(PARAM_INT, 'question id', false),
                        'atype' => new external_value(PARAM_TEXT, 'quiz type', false),
                        'name' => new external_value(PARAM_TEXT, 'question name', false),
                        'des' => new external_value(PARAM_TEXT, 'question text', false),
                        'attempts' => new external_value(PARAM_INT, 'attempts', false),
                        'timeopen' => new external_value(PARAM_RAW, 'time open', false),
                        'timeclose' => new external_value(PARAM_RAW, 'time close', false),
                        'timelimit' => new external_value(PARAM_INT, 'time limit', false),
                        'timezoneOffset' => new external_value(PARAM_FLOAT, 'timezoneOffset', false),
                        'questions' => new external_value(PARAM_RAW, 'questions', false),
                        'gradingscheme' => new external_value(PARAM_INT, 'Grading scheme toggle on/off', false),
                        'gradeboundaryA' => new external_value(PARAM_INT, 'Grade boundary A', false),
                        'gradeboundaryB' => new external_value(PARAM_INT, 'Grade boundary B', false),
                        'gradeboundaryC' => new external_value(PARAM_INT, 'Grade boundary C', false),
                        'gradeboundaryD' => new external_value(PARAM_INT, 'Grade boundary D', false),
                    )
                ),
                'sectionid'=> new external_value(PARAM_INT, 'section id', false),
            )
        );
    }

    public static function create_quiz_activity_returns() {
        return
            new external_single_structure(
                array(
                    'status' => new external_value(PARAM_BOOL, 'status'),
                    'message' => new external_value(PARAM_TEXT, 'message'),
                    'res' => new external_value(PARAM_RAW, 'result'),
                    'aid' => new external_value(PARAM_INT, 'scorm id', false),
                    'courseid' => new external_value(PARAM_INT, 'course id', false),
                    'name' => new external_value(PARAM_TEXT, 'scorm name', false),
                    'atype' => new external_value(PARAM_TEXT, 'scorm type', false),
                    'des' => new external_value(PARAM_RAW, 'des', false),
                    'desformat' => new external_value(PARAM_INT, 'des format', false),
                    'timeclose' => new external_value(PARAM_INT, 'time close', false),
                    'timeopen' => new external_value(PARAM_INT, 'time open', false),
                    'timelimit' => new external_value(PARAM_INT, 'time limit', false),
                    'attempts' => new external_value(PARAM_INT, 'attempts', false),
                    'questions' => new external_value(PARAM_RAW, 'questions', false),
                    'gradingscheme' => new external_value(PARAM_INT, 'Grading scheme toggle on/off', false),
                    'gradeboundaryA' => new external_value(PARAM_INT, 'Grade boundary A', false),
                    'gradeboundaryB' => new external_value(PARAM_INT, 'Grade boundary B', false),
                    'gradeboundaryC' => new external_value(PARAM_INT, 'Grade boundary C', false),
                    'gradeboundaryD' => new external_value(PARAM_INT, 'Grade boundary D', false),
                )
            );
    }

    public static function create_quiz_activity($act, $courseid, $username, $q, $sectionid) { //Don't forget to set it as static
        global $CFG, $DB;

        $params = self::validate_parameters(self::create_quiz_activity_parameters(), array('act'=>$act, 'courseid'=>$courseid, 'username'=>$username, 'q'=>$q, 'sectionid'=>$sectionid));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->create_quiz_activity($act, $courseid, $username, $q, $sectionid);

        return $return;
    }

    //update_course_section
    public static function update_course_section_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'Course id'),
                'action' => new external_value(PARAM_INT, '1 create 2 update 3 remove', false),
                'title' => new external_value(PARAM_TEXT, 'title', false),
                'des' => new external_value(PARAM_TEXT, 'des', false),
                'lo' => new external_value(PARAM_TEXT, 'learning outcomes', false),
                'sectionid' => new external_value(PARAM_INT, 'Section id', false),
            )
        );
    }

    public static function update_course_section_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'datas' => new external_single_structure(
                        array(
                            'res' => new external_value(PARAM_INT, 'result', false),
                            'numsections' => new external_value(PARAM_INT, 'num sections', false),
                            'sections' => new external_multiple_structure(
                                new external_single_structure(
                                    array (
                                        'id' => new external_value(PARAM_INT, 'section id', false),
                                        'course' => new external_value(PARAM_INT, 'Course id', false),
                                        'section' => new external_value(PARAM_INT, 'section position', false),
                                        'name' => new external_value(PARAM_RAW, 'section name', false),
                                        'summary' => new external_value(PARAM_RAW, 'summary', false),
                                        'summaryformat' => new external_value(PARAM_INT, 'Summary format', false),
                                        'sequence' => new external_value(PARAM_RAW, 'Sequence', false),
                                        'learningoutcomes' => new external_value(PARAM_RAW, 'Learning outcomes', false),
                                    )
                                ), 'Sections', false
                            ),
                        )
                    )
            )
        );
    }

    public static function update_course_section($courseid, $action, $title, $des, $lo, $sectionid) { //Don't forget to set it as static
        global $CFG, $DB;

        $params = self::validate_parameters(self::update_course_section_parameters(), array('courseid'=> $courseid, 'action' => $action, 'title' => $title, 'des' => $des, 'lo' => $lo, 'sectionid' => $sectionid ));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->update_course_section ($courseid, $action, $title, $des, $lo, $sectionid);

        return $return;
    }
    
//course progran
    
    public static function my_course_program_parameters() {
        return new external_function_parameters(
                array(
            'username' => new external_value(PARAM_TEXT, 'Username'),
            'programid' => new external_value(PARAM_INT, 'order by category'),
                )
        );
    }

    public static function my_course_program_returns() {
             return new external_multiple_structure(
            new external_single_structure(
                array(
                    'remoteid' => new external_value(PARAM_INT, 'course id'),
                    'cat_id' => new external_value(PARAM_INT, 'category id'),
                    'cat_name' => new external_value(PARAM_TEXT, 'cartegory name'),
                    'course_group' => new external_value(PARAM_RAW, 'Course group Joomsocial'),
                    'activity_count' => new external_value(PARAM_RAW, 'group of course', false),
                    'cat_description' => new external_value(PARAM_RAW, 'category description'),
                    'sortorder' => new external_value(PARAM_TEXT, 'sortorder'),
                    'fullname' => new external_value(PARAM_TEXT, 'course name'),
                    'shortname' => new external_value(PARAM_TEXT, 'course shortname'),
                    'idnumber' => new external_value(PARAM_RAW, 'idnumber'),
                    'summary' => new external_value(PARAM_RAW, 'summary'),
                    'startdate' => new external_value(PARAM_INT, 'start date'),
                    'created' => new external_value(PARAM_INT, 'created'),
                    'modified' => new external_value(PARAM_INT, 'modified'),
                    'last_access' => new external_value(PARAM_INT, 'last access', VALUE_OPTIONAL),
                    'enrol_time' => new external_value(PARAM_INT, 'enrol time', VALUE_OPTIONAL),
                    'timeend' => new external_value(PARAM_INT, 'timeend', VALUE_OPTIONAL),
                    'completion_status' => new external_value(PARAM_INT, 'completion status', VALUE_OPTIONAL),
                    'time_completed' => new external_value(PARAM_INT, 'time completed', VALUE_OPTIONAL),
                    'cost' => new external_value(PARAM_FLOAT, 'cost', VALUE_OPTIONAL),
                    'currency' => new external_value(PARAM_TEXT, 'currency', VALUE_OPTIONAL),
                    'self_enrolment' => new external_value(PARAM_INT, 'self enrollable'), 
                    'enroled' => new external_value(PARAM_INT, 'user enroled'),
                    'in_enrol_date' => new external_value(PARAM_BOOL, 'in enrol date'), 
                    'guest' => new external_value(PARAM_INT, 'guest access'),
                    'fileid' => new external_value(PARAM_TEXT, 'file id', VALUE_OPTIONAL),
                    'filetype' => new external_value(PARAM_TEXT, 'file type', VALUE_OPTIONAL),
                    'filepath' => new external_value(PARAM_TEXT, 'file path', VALUE_OPTIONAL),
                    'filename' => new external_value(PARAM_TEXT, 'file name', VALUE_OPTIONAL),
                    'coursetype' => new external_value(PARAM_TEXT, 'course type', VALUE_OPTIONAL),
                    'name_pro' => new external_value(PARAM_TEXT, 'name program', VALUE_OPTIONAL), 
                    
                )
            )
        );
    }


    public static function my_course_program($username, $programid) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::my_course_program_parameters(), array('username' => $username, 'programid' => $programid));

        $auth = new auth_plugin_joomdle ();
        $return = $auth->my_course_program($username, $programid);

        return $return;
    }
    /* my_courses */
    public static function my_courses_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'Username'),
                            'order_by_cat' => new external_value(PARAM_INT, 'order by category'),
                            'is_notmalp' => new external_value(PARAM_INT, 'user is not MA or LP', false),
                        )
        );
    }

    public static function my_courses_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'courses' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'group record id'),
                            'fullname' => new external_value(PARAM_TEXT, 'course name'),
                            'shortname' => new external_value(PARAM_TEXT, 'course shortname'),
                            'summary' => new external_value(PARAM_RAW, 'summary'),
                            'category' => new external_value(PARAM_INT, 'course category id'),
                            'cat_name' => new external_value(PARAM_TEXT, 'course category name'),
                            'cat_description' => new external_value(PARAM_RAW, 'category description',false),
                            'startdate' => new external_value(PARAM_INT, 'start date'),
                            'enrolperiod' => new external_value(PARAM_INT, 'enrolperiod'),
                            'self_enrolment' => new external_value(PARAM_INT, 'self enrolment', false),
                            'has_nonmember' => new external_value(PARAM_INT, 'has non-user'),
                            'creator' => new external_value(PARAM_RAW, 'creator'),
                            'content_creator' => new external_value(PARAM_TEXT, 'content creator of course', false),
                            'can_unenrol' => new external_value(PARAM_INT, 'user can self unenrol'),
                            'fileid' => new external_value(PARAM_TEXT, 'file id', VALUE_OPTIONAL),
                            'filetype' => new external_value(PARAM_TEXT, 'file type', VALUE_OPTIONAL),
                            'filepath' => new external_value(PARAM_TEXT, 'file path', VALUE_OPTIONAL),
                            'filename' => new external_value(PARAM_TEXT, 'file name', VALUE_OPTIONAL),
                            'duration' => new external_value(PARAM_FLOAT, 'duration'),
                            'price' => new external_value(PARAM_FLOAT, 'price'),
                            'currency' => new external_value(PARAM_TEXT, 'currency', VALUE_OPTIONAL),
                            'learning_outcomes' => new external_value(PARAM_TEXT, 'learning outcomes'),
                            'target_audience' => new external_value(PARAM_TEXT, 'target audience'),
                            'course_survey' => new external_value(PARAM_INT, 'course survey'),
                            'course_facilitated' => new external_value(PARAM_INT, 'facilitated course'),
                            'course_status' => new external_value(PARAM_TEXT, 'Course status'),
                        )
                    )
                )
            )
        );
    }

    public static function my_courses($username, $order_by_cat, $is_notmalp) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::my_courses_parameters(), array('username'=>$username, 'order_by_cat' => $order_by_cat, 'is_notmalp' => $is_notmalp));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->my_courses ($username, $order_by_cat, $is_notmalp);

        return $return;
    }

    /* my_own_courses */
    public static function my_own_courses_parameters() {
        return new external_function_parameters(
            array(
                'username' => new external_value(PARAM_TEXT, 'username'),
                'catid' => new external_value(PARAM_TEXT, 'list category id', ''),
                'limit' => new external_value(PARAM_INT, 'limit', false),
                'offset' => new external_value(PARAM_INT, 'offset', false),
                'is_malp' => new external_value(PARAM_INT, 'is Manager or LP', false),
            )
        );
    }

    public static function my_own_courses_returns() {
         return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'courses' => new external_multiple_structure(
                             new external_single_structure(
                                array(
                                    'id' => new external_value(PARAM_INT, 'course id'),
                                    'category' => new external_value(PARAM_INT, 'category id'),
                                    'cat_name' => new external_value(PARAM_TEXT, 'cartegory name'),
                                    'cat_description' => new external_value(PARAM_RAW, 'category description', false),
                                    'fullname' => new external_value(PARAM_TEXT, 'course name'),
                                    'shortname' => new external_value(PARAM_TEXT, 'course shortname'),
                                    'idnumber' => new external_value(PARAM_RAW, 'idnumber', false),
                                    'summary' => new external_value(PARAM_RAW, 'summary'),
                                    'startdate' => new external_value(PARAM_INT, 'start date'),
                                    'enrolperiod' => new external_value(PARAM_INT, 'enrolperiod'),
                                    'enrolmenttimestart' => new external_value(PARAM_INT, 'enrolmenttimestart', false),
                                    'enrolmenttimeend' => new external_value(PARAM_INT, 'enrolmenttimeend', false),
                                    'self_enrolment' => new external_value(PARAM_INT, 'self enrolment', false),
                                    'has_nonmember' => new external_value(PARAM_INT, 'has non-user'),
                                    'count_learner' => new external_value(PARAM_INT, 'count learner', false),
                                    'course_status' => new external_value(PARAM_TEXT, 'Course status'),
                                    'created' => new external_value(PARAM_INT, 'created', false),
                                    'modified' => new external_value(PARAM_INT, 'modified', false),
                                    'timepublish' => new external_value(PARAM_INT, 'timepublish', false),
                                    'creator' => new external_value(PARAM_TEXT, 'creator'),
                                    'content_creator' => new external_value(PARAM_TEXT, 'content creator of course', false),
                                    'price' => new external_value(PARAM_FLOAT, 'price'),
                                    'currency' => new external_value(PARAM_TEXT, 'currency', VALUE_OPTIONAL),
                                    'duration' => new external_value(PARAM_FLOAT, 'duration'),
                                    'learning_outcomes' => new external_value(PARAM_TEXT, 'learning outcomes'),
                                    'target_audience' => new external_value(PARAM_TEXT, 'target audience'),
                                    'course_survey' => new external_value(PARAM_INT, 'course survey'),
                                    'course_facilitated' => new external_value(PARAM_INT, 'facilitated course'),
                                    'can_unenrol' => new external_value(PARAM_INT, 'user unenroled'),
                                    'fileid' => new external_value(PARAM_TEXT, 'file id', VALUE_OPTIONAL),
                                    'filetype' => new external_value(PARAM_TEXT, 'file type', VALUE_OPTIONAL),
                                    'filepath' => new external_value(PARAM_TEXT, 'file path', VALUE_OPTIONAL),
                                    'filename' => new external_value(PARAM_TEXT, 'file name', VALUE_OPTIONAL),
                                )
                                )
                            )
                    )
                );
//        return new external_multiple_structure(
//            new external_single_structure(
//                array(
//                    'id' => new external_value(PARAM_INT, 'course id'),
//                    'category' => new external_value(PARAM_INT, 'category id'),
//                    'cat_name' => new external_value(PARAM_TEXT, 'cartegory name'),
//                    'cat_description' => new external_value(PARAM_RAW, 'category description'),
//                    'fullname' => new external_value(PARAM_TEXT, 'course name'),
//                    'shortname' => new external_value(PARAM_TEXT, 'course shortname'),
//                    'idnumber' => new external_value(PARAM_RAW, 'idnumber'),
//                    'summary' => new external_value(PARAM_RAW, 'summary'),
//                    'startdate' => new external_value(PARAM_INT, 'start date'),
//                    'enrolperiod' => new external_value(PARAM_INT, 'enrolperiod'),
//                    'has_nonmember' => new external_value(PARAM_INT, 'has non-user'),
//                    'created' => new external_value(PARAM_INT, 'created'),
//                    'modified' => new external_value(PARAM_INT, 'modified'),
//                    'creator' => new external_value(PARAM_INT, 'creator'),
//                    'price' => new external_value(PARAM_FLOAT, 'price'),
//                    'duration' => new external_value(PARAM_INT, 'duration'),
//                    'learning_outcomes' => new external_value(PARAM_TEXT, 'learning outcomes'),
//                    'target_audience' => new external_value(PARAM_TEXT, 'target audience'),
//                    'course_survey' => new external_value(PARAM_INT, 'course survey'),
//                    'course_facilitated' => new external_value(PARAM_INT, 'facilitated course'),
//                    'can_unenrol' => new external_value(PARAM_INT, 'user unenroled'),
//                    'fileid' => new external_value(PARAM_TEXT, 'file id', VALUE_OPTIONAL),
//                    'filetype' => new external_value(PARAM_TEXT, 'file type', VALUE_OPTIONAL),
//                    'filepath' => new external_value(PARAM_TEXT, 'file path', VALUE_OPTIONAL),
//                    'filename' => new external_value(PARAM_TEXT, 'file name', VALUE_OPTIONAL),
//                )
//            )
//        );
    }

    public static function my_own_courses($username, $catid, $limit, $offset, $is_malp) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::my_own_courses_parameters(), array('username' => $username, 'catid' => $catid, 'limit' => $limit, 'offset' => $offset, 'is_malp' => $is_malp));

        $auth = new  auth_plugin_joomdle ();
        $id = $auth->my_own_courses ( $username, $catid, $limit, $offset, $is_malp);

        return $id;
    }

    /* get course info platform */
    public static function get_courseInfo_platform_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_TEXT, 'course id', ''),
                'username' => new external_value(PARAM_TEXT, 'username'),
            )
        );
    }

    public static function get_courseInfo_platform_returns() {
         return new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'course id'),
                    'category' => new external_value(PARAM_INT, 'category id'),
                    'cat_name' => new external_value(PARAM_TEXT, 'cartegory name'),
                    'cat_description' => new external_value(PARAM_RAW, 'category description', false),
                    'fullname' => new external_value(PARAM_TEXT, 'course name'),
                    'shortname' => new external_value(PARAM_TEXT, 'course shortname'),
                    'idnumber' => new external_value(PARAM_RAW, 'idnumber', false),
                    'summary' => new external_value(PARAM_RAW, 'summary'),
                    'startdate' => new external_value(PARAM_INT, 'start date'),
                    'enrolperiod' => new external_value(PARAM_INT, 'enrolperiod'),
                    'enrolmenttimestart' => new external_value(PARAM_INT, 'enrolmenttimestart', false),
                    'enrolmenttimeend' => new external_value(PARAM_INT, 'enrolmenttimeend', false),
                    'self_enrolment' => new external_value(PARAM_INT, 'self enrolment', false),
                    'has_nonmember' => new external_value(PARAM_INT, 'has non-user', false),
                    'count_learner' => new external_value(PARAM_INT, 'count learner', false),
                    'course_status' => new external_value(PARAM_TEXT, 'Course status'),
                    'created' => new external_value(PARAM_INT, 'created', false),
                    'modified' => new external_value(PARAM_INT, 'modified', false),
                    'timepublish' => new external_value(PARAM_INT, 'timepublish', false),
                    'creator' => new external_value(PARAM_TEXT, 'creator'),
                    'price' => new external_value(PARAM_FLOAT, 'price'),
                    'currency' => new external_value(PARAM_TEXT, 'currency', VALUE_OPTIONAL),
                    'duration' => new external_value(PARAM_FLOAT, 'duration'),
                    'learning_outcomes' => new external_value(PARAM_TEXT, 'learning outcomes'),
                    'target_audience' => new external_value(PARAM_TEXT, 'target audience'),
                    'course_survey' => new external_value(PARAM_INT, 'course survey'),
                    'course_facilitated' => new external_value(PARAM_INT, 'facilitated course'),
                    'can_unenrol' => new external_value(PARAM_INT, 'user unenroled'),
                    'fileid' => new external_value(PARAM_TEXT, 'file id', VALUE_OPTIONAL),
                    'filetype' => new external_value(PARAM_TEXT, 'file type', VALUE_OPTIONAL),
                    'filepath' => new external_value(PARAM_TEXT, 'file path', VALUE_OPTIONAL),
                    'filename' => new external_value(PARAM_TEXT, 'file name', VALUE_OPTIONAL),
                )
        );
    }

    public static function get_courseInfo_platform($id, $username) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_courseInfo_platform_parameters(), array('id'=>$id, 'username' => $username));

        $auth = new  auth_plugin_joomdle ();
        $courseinfo = $auth->get_course_info ($id, $username);
        $record = array();
        if ($courseinfo) {
            $record['id'] = $courseinfo['remoteid'];
            $record['category'] = $courseinfo['cat_id'];
            $record['cat_name'] = $courseinfo['cat_name'];
            $record['cat_description'] = strip_tags(format_string($courseinfo['cat_description'],true));
            $record['fullname'] = $courseinfo['fullname'];
            $record['shortname'] = $courseinfo['shortname'];
            $record['idnumber'] = $courseinfo['idnumber'];
            $record['summary'] = strip_tags(format_string($courseinfo['summary'],true));
            $record['startdate'] = $courseinfo['startdate'];
            $record['self_enrolment'] = $courseinfo['self_enrolment'];
            $record['course_status'] = $courseinfo['course_status'];
            $record['timepublish'] = $courseinfo['timepublish'];
            $record['creator'] = $courseinfo['creator'];
            $record['price'] = $courseinfo['price'];
            $record['currency'] = $courseinfo['currency'];
            $record['duration'] = (float)$courseinfo['duration']/3600;
            $record['learning_outcomes'] = $courseinfo['learningoutcomes'];
            $record['target_audience'] = $courseinfo['targetaudience'];
            $record['course_survey'] = $courseinfo['coursesurvey'];
            $record['course_facilitated'] = $courseinfo['facilitatedcourse'];
            $record['fileid'] = $courseinfo['fileid'];
            $record['filetype'] = $courseinfo['filetype'];
            $record['filepath'] = $courseinfo['filepath'];
            $record['filename'] = $courseinfo['filename'];
            
            $query = "SELECT MAX(enrolperiod) AS enrolperiod
              FROM {$CFG->prefix}enrol
              WHERE status = 0 AND courseid = ".$courseinfo['remoteid'];
            $result = $DB->get_record_sql($query);
            $record['enrolperiod'] = $result->enrolperiod;
            
            $context = context_course::instance($courseinfo['remoteid']);
            $user = get_complete_user_data ('username', $username);
            if ((has_capability('enrol/manual:unenrolself', $context, $user->id)) || (has_capability('enrol/self:unenrolself', $context, $user->id)))
                $record['can_unenrol'] = 1;
            else
                $record['can_unenrol'] = 0;
        }

        return $record;
    }

    /* get_course_info */
    public static function get_course_info_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_course_info_returns() {
            return new external_single_structure(
                array(
                    'remoteid' => new external_value(PARAM_INT, 'course id'),
                    'cat_id' => new external_value(PARAM_INT, 'category id'),
                    'cat_name' => new external_value(PARAM_TEXT, 'category name'),
                    'cat_description' => new external_value(PARAM_RAW, 'category description'),
                    'visible' => new external_value(PARAM_INT, 'visible course', false),
                    'cat_parent' => new external_value(PARAM_INT, 'category parent'),
                    'sortorder' => new external_value(PARAM_TEXT, 'category name'),
                    'fullname' => new external_value(PARAM_TEXT, 'course name'),
                    'shortname' => new external_value(PARAM_TEXT, 'course name'),
                    'idnumber' => new external_value(PARAM_RAW, 'category name'),
                    'summary' => new external_value(PARAM_RAW, 'summary'),
                    'fileid' => new external_value(PARAM_TEXT, 'file id', VALUE_OPTIONAL),
                    'filetype' => new external_value(PARAM_TEXT, 'file type', VALUE_OPTIONAL),
                    'filepath' => new external_value(PARAM_TEXT, 'file path', VALUE_OPTIONAL),
                    'filename' => new external_value(PARAM_TEXT, 'file name', VALUE_OPTIONAL),
                    'startdate' => new external_value(PARAM_INT, 'start date'),
                    'timepublish' => new external_value(PARAM_INT, 'timepublish', false),
                    'numsections' => new external_value(PARAM_INT, 'number of sections'),
                    'lang' => new external_value(PARAM_RAW, 'lang'),
                    'price' => new external_value(PARAM_FLOAT, 'price', VALUE_OPTIONAL),
                    'currency' => new external_value(PARAM_TEXT, 'currency', VALUE_OPTIONAL),
                    'duration' => new external_value(PARAM_FLOAT, 'estimated duration', VALUE_OPTIONAL),
                    'enrolstartdate' => new external_value(PARAM_INT, 'enrol start date', VALUE_OPTIONAL), 
                    'enrolenddate' => new external_value(PARAM_INT, 'enrol end date', VALUE_OPTIONAL), 
                    'enrolperiod' => new external_value(PARAM_INT, 'enrol duration', VALUE_OPTIONAL),
                    'self_enrolment' => new external_value(PARAM_INT, 'self enrollable'), 
                    'enroled' => new external_value(PARAM_INT, 'user enroled'), 
                    'enrol_timeend' => new external_value(PARAM_INT, 'enrol timeend', VALUE_OPTIONAL),
                    'in_enrol_date' => new external_value(PARAM_BOOL, 'in enrol date'), 
                    'guest' => new external_value(PARAM_INT, 'guest access'), 
                    'creator' => new external_value(PARAM_TEXT, 'username of creator'),
                    'count_learner' => new external_value(PARAM_INT, 'count learner', false),
                    'learningoutcomes' => new external_value(PARAM_RAW, 'learningoutcomes', VALUE_OPTIONAL),
                    'targetaudience' => new external_value(PARAM_RAW, 'targetaudience', VALUE_OPTIONAL),
                    'coursesurvey' => new external_value(PARAM_INT, 'course survey', VALUE_OPTIONAL),
                    'facilitatedcourse' => new external_value(PARAM_INT, 'facilitated course', VALUE_OPTIONAL),
                    'certificatecourse' => new external_value(PARAM_INT, 'certificate course', VALUE_OPTIONAL),
                    'course_status' => new external_value(PARAM_TEXT, 'course status'),
                    'userRoles' => new external_value(PARAM_RAW, 'userRoles', false),
                    'course_type' => new external_value(PARAM_TEXT, 'course type: LP course or non course', false),
                )
            );
    }

    public static function get_course_info($id, $username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_info_parameters(), array('id'=>$id, 'username' => $username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_info ($id, $username);


        return $return;
    }

    /* get_course_contents */
    public static function get_course_contents_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function get_course_contents_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'section' => new external_value(PARAM_INT, 'section id'),
                        'name' => new external_value(PARAM_TEXT, 'section name'),
                        'summary' => new external_value(PARAM_RAW, 'summary'),
                    )
                )
            );
    }

    public static function get_course_contents($id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_contents_parameters(), array('id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_contents ($id);


        return $return;
    }

    /* courses_by_category */
    public static function courses_by_category_parameters() {
        return new external_function_parameters(
                        array(
                            'category' => new external_value(PARAM_INT, 'category id'),
                            'enrollable_only' => new external_value(PARAM_INT, 'Return only enrollable courses'),
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function courses_by_category_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'remoteid' => new external_value(PARAM_INT, 'course id'),
                        'cat_id' => new external_value(PARAM_INT, 'category id'),
                        'cat_name' => new external_value(PARAM_TEXT, 'category name'),
                        'cat_description' => new external_value(PARAM_RAW, 'category description'),
                        'fullname' => new external_value(PARAM_TEXT, 'course name'),
                        'summary' => new external_value(PARAM_RAW, 'course summary'),
                        'cost' => new external_value(PARAM_FLOAT, 'cost', VALUE_OPTIONAL),
                        'price' => new external_value(PARAM_FLOAT, 'price', VALUE_OPTIONAL),
                                                'duration' => new external_value(PARAM_FLOAT, 'duration'),
                        'currency' => new external_value(PARAM_TEXT, 'currency', VALUE_OPTIONAL),
						'self_enrolment' => new external_value(PARAM_INT, 'self enrollable'), 
						'enroled' => new external_value(PARAM_INT, 'user enroled'), 
						'in_enrol_date' => new external_value(PARAM_BOOL, 'in enrol date'), 
						'guest' => new external_value(PARAM_INT, 'guest access'), 
                    )
                )
            );
    }

    public static function courses_by_category($category, $enrollable_only, $username) {
        global $CFG, $DB;
 
//        $params = self::validate_parameters(self::courses_by_category_parameters(), array('id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->courses_by_category ($category, $enrollable_only, $username);


        return $return;
    }


    /* get_course_categories */
    public static function get_course_categories_parameters() {
        return new external_function_parameters(
                        array(
                            'category' => new external_value(PARAM_INT, 'category id'),
                            'username' => new external_value(PARAM_TEXT, 'username', false),
                            'isLP' => new external_value(PARAM_INT, 'is learning provider', false),
                        )
        );
    }

    public static function get_course_categories_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'category id'),
                        'name' => new external_value(PARAM_TEXT, 'category name'),
                        'parent' => new external_value(PARAM_INT, 'category parent'),
                        'description' => new external_value(PARAM_RAW, 'description'),
                    )
                )
            );
    }

    public static function get_course_categories($category, $username, $isLP) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_categories_parameters(), array('category'=>$category,'username'=>$username, 'isLP'=>$isLP));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_categories ($category, $username, $isLP);

        return $return;
    }

    /* get_course_editing_teachers */
    public static function get_course_editing_teachers_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function get_course_editing_teachers_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'firstname' => new external_value(PARAM_TEXT, 'firstname'),
                        'lastname' => new external_value(PARAM_TEXT, 'lastname'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                    )
                )
            );
    }

    public static function get_course_editing_teachers($id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_editing_teachers_parameters(), array('id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_editing_teachers ($id);


        return $return;
    }


    /* get_course_no */
    public static function get_course_no_parameters() {
        return new external_function_parameters(
                        array(
                        )
        );
    }

    public static function get_course_no_returns() {
        return new  external_value(PARAM_INT, 'number of courses');
    }

    public static function get_course_no() {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_no_parameters(), array());
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_no ();


        return $return;
    }
    public static function get_lastest_course ($limit) {
        global $CFG, $DB;
        $params = self::validate_parameters(self::get_lastest_course_parameters(), array('limit'=>$limit));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_lastest_course($limit);
        return $return;
    }
    public static function get_lastest_course_parameters() {
        return new external_function_parameters(
                        array(
                            'limit' => new external_value(PARAM_INT, 'course limit'),
                        )
        );
    }
    public static function get_lastest_course_returns () {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'group record id'),
                    'fullname' => new external_value(PARAM_TEXT, 'course name'),
                    'category' => new external_value(PARAM_INT, 'course category id'),
                    'cat_name' => new external_value(PARAM_TEXT, 'course category name'),
                    'can_unenrol' => new external_value(PARAM_INT, 'user can self unenrol'),
                    'fileid' => new external_value(PARAM_TEXT, 'file id', VALUE_OPTIONAL),
                    'filetype' => new external_value(PARAM_TEXT, 'file type', VALUE_OPTIONAL),
                    'filepath' => new external_value(PARAM_TEXT, 'file path', VALUE_OPTIONAL),
                    'filename' => new external_value(PARAM_TEXT, 'file name', VALUE_OPTIONAL),
                )
            )
        );
    }
    
    /*
     * function get progress of activities and resources
     */
    public static function get_mod_progress($id, $username) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::get_mod_progress_parameters(), array('id'=>$id, 'username'=>$username));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_mod_progress($id, $username);
        return $return;
    }
    public static function get_mod_progress_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'course id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
            )
        );
    }

    public static function get_mod_progress_returns() {
        return new external_single_structure (
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'coursename' => new external_value(PARAM_TEXT, 'course name'),
                'gradeid' => new external_value(PARAM_INT, 'grade id', false),
                'gradeitemid' => new external_value(PARAM_INT, 'grade itemid', false),
                'finalgrade' => new external_value(PARAM_FLOAT, 'finalgrade', false),
                'finalgradeletters' => new external_value(PARAM_TEXT, 'finalgradeletters', false),
                'grademax' => new external_value(PARAM_FLOAT, 'grademax', false),
                'gradefeedback' => new external_value(PARAM_TEXT, 'feedback', false),
                'gradeoptionsletters' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'value' => new external_value(PARAM_INT, 'grade letter value'),
                            'name' => new external_value(PARAM_TEXT, 'grade letter name'),
                        )
                    )
                ),
                'hidden' => new external_value(PARAM_INT, 'hidden', false),
                'timemodified' => new external_value(PARAM_INT, 'timemodified'),
                'gradeoffacitator' => new external_value(PARAM_BOOL, 'gradeoffacitator', false),
                'count_learner' => new external_value(PARAM_INT, 'Count Learner'),
                'count_completed' => new external_value(PARAM_INT, 'Count Completed'),
                'sections' =>
                    new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'sectionname' => new external_value(PARAM_RAW, 'section name'),
                                'mods' => new external_multiple_structure(
                                    new external_single_structure(
                                        array (
                                            'id' => new external_value(PARAM_INT, 'resource id'),
                                            'name' => new external_value(PARAM_RAW, 'name'),
                                            'mod' => new external_value(PARAM_RAW, 'mod'),
                                            'type' => new external_value(PARAM_RAW, 'type'),
                                            'available' => new external_value(PARAM_INT, 'available'),
                                            'mod_completion' => new external_value(PARAM_BOOL, 'mod_completion'),
                                            'mod_completion_date' => new external_value(PARAM_RAW, 'mod_completion_date'),
                                            'gradeid' => new external_value(PARAM_INT, 'grade id'),
                                            'gradeitemid' => new external_value(PARAM_INT, 'grade id'),
                                            'finalgrade' => new external_value(PARAM_FLOAT, 'finalgrade'),
                                             'hidden' => new external_value(PARAM_INT, 'hidden', false),
                                            'finalgradeletters' => new external_value(PARAM_TEXT, 'finalgradeletters', false),
                                            'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                                            'feedback' => new external_value(PARAM_TEXT, 'feedback'),
                                            'mod_lastaccess' => new external_value(PARAM_TEXT, 'mod_lastaccess'),
                                            'gradingscheme' => new external_value(PARAM_INT, 'gradingscheme', false),
                                            'attempt_quiz' => new external_value(PARAM_INT, 'attempt_quiz', false),
                                            'firstAttempt' => new external_value(PARAM_BOOL, 'firstAttempt',false),
                                            'inProgress' => new external_value(PARAM_BOOL, 'inProgress',false),
                                            'quizFinished' => new external_value(PARAM_BOOL, 'quizFinished',false),
                                            'submission_status' => new external_value(PARAM_TEXT, 'submission_status', false),
                                            'completionusegrade' => new external_value(PARAM_INT, 'mod check grade'),
                                            'totalcompleted' => new external_value(PARAM_INT, 'Total learner completed'),
                                            'lettergradeoption' => new external_multiple_structure(
                                                new external_single_structure(
                                                    array(
                                                        'value' => new external_value(PARAM_INT, 'grade letter value',false),
                                                        'name' => new external_value(PARAM_TEXT, 'grade letter letter',false),
                                                    )
                                                )
                                            ),
                                        )
                                    ), false
                                ),
                            )
                        )
                    )
            )
        );
    }

    /* get_enrollable_course_no */
    public static function get_enrollable_course_no_parameters() {
        return new external_function_parameters(
                        array(
                        )
        );
    }

    public static function get_enrollable_course_no_returns() {
        return new  external_value(PARAM_INT, 'number of courses');
    }

    public static function get_enrollable_course_no() {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_enrollable_course_no_parameters(), array());
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_enrollable_course_no ();


        return $return;
    }


    /* get_student_no */
    public static function get_student_no_parameters() {
        return new external_function_parameters(
                        array(
                        )
        );
    }

    public static function get_student_no_returns() {
        return new  external_value(PARAM_INT, 'number of students');
    }

    public static function get_student_no() {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_student_no_parameters(), array());
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_student_no ();


        return $return;
    }

    /* get_total_assignment_submissions */
    public static function get_total_assignment_submissions_parameters() {
        return new external_function_parameters(
                        array(
                        )
        );
    }

    public static function get_total_assignment_submissions_returns() {
        return new  external_value(PARAM_INT, 'number of submitted assingments');
    }

    public static function get_total_assignment_submissions() {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_total_assignment_submissions_parameters(), array());
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_total_assignment_submissions ();


        return $return;
    }


    /* get_course_student_no */
    public static function get_course_students_no_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function get_course_students_no_returns() {
        return new  external_value(PARAM_INT, 'number of submitted assingments');
    }

    public static function get_course_students_no($id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_students_no_parameters(), array('id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_students_no ($id);


        return $return;
    }

    /* get_assignment_submissions */
    public static function get_assignment_submissions_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function get_assignment_submissions_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'id'),
                        'tarea' => new external_value(PARAM_TEXT, 'task'),
                        'entregados' => new external_value(PARAM_INT, 'submitted'),
                    )
                )
            );
    }

    public static function get_assignment_submissions($id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_assignment_submissions_parameters(), array('id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_assignment_submissions ($id);


        return $return;
    }

    /* get_assignment_grades */
    public static function get_assignment_grades_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function get_assignment_grades_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'tarea' => new external_value(PARAM_TEXT, 'task'),
                        'media' => new external_value(PARAM_FLOAT, 'submitted'),
                    )
                )
            );
    }

    public static function get_assignment_grades($id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_assignment_grades_parameters(), array('id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_assignment_grades ($id);


        return $return;
    }


    /* get_upcoming_events */
    public static function get_upcoming_events_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function get_upcoming_events_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'name' => new external_value(PARAM_TEXT, 'event name'),
                        'timestart' => new external_value(PARAM_INT, 'start time'),
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                    )
                )
            );
    }

    public static function get_upcoming_events($id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_upcoming_events_parameters(), array('id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_upcoming_events ($id);


        return $return;
    }

    /* get_news_items */
    public static function get_news_items_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function get_news_items_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'discussion' => new external_value(PARAM_INT, 'discussion id'),
                        'subject' => new external_value(PARAM_TEXT, 'subject'),
                        'timemodified' => new external_value(PARAM_INT, 'timemodified'),
                    )
                )
            );
    }

    public static function get_news_items($id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_news_items_parameters(), array('id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_news_items ($id);


        return $return;
    }

    /* get_user_grades */
    public static function get_user_grades_parameters() {
        return new external_function_parameters(
                        array(
                            'user' => new external_value(PARAM_TEXT, 'username'),
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function get_user_grades_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'itemname' => new external_value(PARAM_TEXT, 'item name'),
                        'finalgrade' => new external_value(PARAM_TEXT, 'final grade'),
                    )
                )
            );
    }

    public static function get_user_grades($user, $id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_user_grades_parameters(), array('user' => $user, 'id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_user_grades ($user, $id);


        return $return;
    }


    /* get_course_grade_categories */
    public static function get_course_grade_categories_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function get_course_grade_categories_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'fullname' => new external_value(PARAM_TEXT, 'item name'),
                        'grademax' => new external_value(PARAM_TEXT, 'final grade'),
                    )
                )
            );
    }

    public static function get_course_grade_categories($id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_grade_categories_parameters(), array('id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_grade_categories ($id);


        return $return;
    }

    /* get_course_grade_categories_and_items */
    public static function get_course_grade_categories_and_items_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function get_course_grade_categories_and_items_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'fullname' => new external_value(PARAM_TEXT, 'item name'),
                        'grademax' => new external_value(PARAM_TEXT, 'final grade'),
                        'items' => new external_multiple_structure(
                                new external_single_structure(
                                    array(
                                        'name' => new external_value(PARAM_TEXT, 'item name'),
                                        'due' => new external_value(PARAM_INT, 'due date'),
                                        'has_rubrics' => new external_value(PARAM_BOOL, 'has rubrics'),
                                        'id' => new external_value(PARAM_INT, 'id'),
                                    )
                                )
                            )
                    )
                )
            );
    }

    public static function get_course_grade_categories_and_items($id) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_course_grade_categories_and_items_parameters(), array('id'=>$id));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_grade_categories_and_items ($id);


        return $return;
    }

    /* search_courses */
    public static function search_courses_parameters() {
        return new external_function_parameters(
            array(
                'text' => new external_value(PARAM_TEXT, 'text search'),
                'sortby' => new external_value(PARAM_TEXT, 'Order field'),
                'listcourse' => new external_value(PARAM_TEXT, 'Return only courses for guests'),
                'username' => new external_value(PARAM_TEXT, 'username'),
                'limit' => new external_value(PARAM_INT, 'limit'),
                'position' => new external_value(PARAM_INT, 'position', false),
            )
        );
    }

    public static function search_courses_returns() {

        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'total_course' => new external_value(PARAM_INT, 'Total course'),
                'courses' => new external_multiple_structure (
                    new external_single_structure (
                        array(
                            'remoteid' => new external_value(PARAM_INT, 'course id'),
                            'fullname' => new external_value(PARAM_TEXT, 'course name'),
                            'summary' => new external_value(PARAM_RAW, 'summary'),
                            'creator' => new external_value(PARAM_TEXT, 'user name of creator'),
                            'enroled' => new external_value(PARAM_INT, 'user enroled'),
                            'isLearner' => new external_value(PARAM_BOOL, 'Check user is learner of course'),
                            'courseimg' => new external_value(PARAM_TEXT, 'file path', VALUE_OPTIONAL),
                            'learningoutcomes' => new external_value(PARAM_TEXT, 'Learning outcome'),
                            'targetaudience' => new external_value(PARAM_TEXT, 'Target audience'),
                            'duration' => new external_value(PARAM_INT, 'Duration'),
                        )
                    )
                )
            )
        );
    }

    public static function search_courses($text, $sortby, $listcourse, $username, $limit, $position) { //Don't forget to set it as static
        global $CFG, $DB;

        $params = self::validate_parameters(self::search_courses_parameters(), array('text'=>$text, 'sortby' => $sortby,'listcourse' => $listcourse, 'username' => $username,'limit'=>$limit, 'position'=>$position));

        $auth = new  auth_plugin_joomdle ();
        $id = $auth->search_courses ($text, $sortby, $listcourse, $username, $limit, $position);

        return $id;
    }

    /* search_courses */

   public static function search_categories_parameters() {
        return new external_function_parameters(
                        array(
                            'text' => new external_value(PARAM_TEXT, 'text to search'),
                            'phrase' => new external_value(PARAM_TEXT, 'search type'),
                            'ordering' => new external_value(PARAM_TEXT, 'order'),
                            'limit' => new external_value(PARAM_TEXT, 'limit'),
                        )
        );
    }

    public static function search_categories_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'cat_id' => new external_value(PARAM_INT, 'category id'),
                        'cat_name' => new external_value(PARAM_TEXT, 'category name'),
                        'cat_description' => new external_value(PARAM_RAW, 'category description'),
                    )
                )
            );
    }
    public static function search_categories($text, $phrase, $ordering, $limit) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::search_categories_parameters(), array('text'=>$text, 'phrase'=>$phrase, 'ordering'=>$ordering, 'limit'=>$limit));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->search_categories ($text, $phrase, $ordering, $limit);


        return $return;
    }


    /* search_topics */
   public static function search_topics_parameters() {
        return new external_function_parameters(
                        array(
                            'text' => new external_value(PARAM_TEXT, 'text to search'),
                            'phrase' => new external_value(PARAM_TEXT, 'search type'),
                            'ordering' => new external_value(PARAM_TEXT, 'order'),
                            'limit' => new external_value(PARAM_TEXT, 'limit'),
                        )
        );
    }

    public static function search_topics_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'remoteid' => new external_value(PARAM_INT, 'course id'),
                        'fullname' => new external_value(PARAM_TEXT, 'course name'),
                        'course' => new external_value(PARAM_TEXT, 'course name'),
                        'section' => new external_value(PARAM_TEXT, 'course name'),
                        'summary' => new external_value(PARAM_RAW, 'summary'),
                        'cat_id' => new external_value(PARAM_INT, 'category id'),
                        'cat_name' => new external_value(PARAM_TEXT, 'category name'),
                    )
                )
            );
    }
    public static function search_topics($text, $phrase, $ordering, $limit) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::search_topics_parameters(), array('text'=>$text, 'phrase'=>$phrase, 'ordering'=>$ordering, 'limit'=>$limit));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->search_topics ($text, $phrase, $ordering, $limit);


        return $return;
    }

    /* get_my_courses_grades */
    public static function get_my_courses_grades_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_my_courses_grades_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'course id'),
                        'fullname' => new external_value(PARAM_TEXT, 'course name'),
                        'cat_id' => new external_value(PARAM_INT, 'category id'),
                        'cat_name' => new external_value(PARAM_TEXT, 'category name'),
                        'avg' => new external_value(PARAM_TEXT, 'average grade'),
                    )
                )
            );
    }

    public static function get_my_courses_grades($username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_my_courses_grades_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_my_courses_grades ($username);


        return $return;
    }

    /* check_moodle_users */
    public static function check_moodle_users_parameters() {
        return new external_function_parameters(
                        array(
                            'users' => new external_multiple_structure(
                                        new external_single_structure(
                                            array(
                                                'username' => new external_value(PARAM_TEXT, 'username'),
                                            )
                                        )
                        )
                )
        );
    }

    public static function check_moodle_users_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'admin' => new external_value(PARAM_INT, 'admin user'),
                        'auth' => new external_value(PARAM_TEXT, 'auth plugin'),
                        'm_account' => new external_value(PARAM_INT, 'moodle account'),
                    )
                )
            );
    }

    public static function check_moodle_users($users) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::check_moodle_users_parameters(), array('users'=>$users));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->check_moodle_users ($users);


        return $return;
    }

    /* get_moodle_only_users */
    public static function get_moodle_only_users_parameters() {
        return new external_function_parameters(
                        array(
                            'users' => new external_multiple_structure(
                                        new external_single_structure(
                                            array(
                                                'username' => new external_value(PARAM_TEXT, 'username'),
                                            )
                                        )
                                    ),
                            'search' => new external_value(PARAM_TEXT, 'sarch text'),
                        )
                );
    }

    public static function get_moodle_only_users_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'user id'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'email' => new external_value(PARAM_TEXT, 'email'),
                        'name' => new external_value(PARAM_TEXT, 'name'),
                        'auth' => new external_value(PARAM_TEXT, 'auth plugin'),
                        'admin' => new external_value(PARAM_INT, 'admin user'),
                    )
                )
            );
    }

    public static function get_moodle_only_users($users, $search) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_moodle_only_users_parameters(), array('users'=>$users, 'search' => $search));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_moodle_only_users ($users, $search);


        return $return;
    }

    /* get_moodle_users */
    public static function get_moodle_users_parameters() {
        return new external_function_parameters(
                        array(
                            'limitstart' => new external_value(PARAM_INT, 'limit start'),
                            'limit' => new external_value(PARAM_INT, 'limit'),
                            'order' => new external_value(PARAM_TEXT, 'order'),
                            'order_dir' => new external_value(PARAM_TEXT, 'order dir'),
                            'search' => new external_value(PARAM_TEXT, 'search text'),
                        )
                );
    }

    public static function get_moodle_users_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'user id'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'email' => new external_value(PARAM_TEXT, 'email'),
                        'name' => new external_value(PARAM_TEXT, 'name'),
                        'auth' => new external_value(PARAM_TEXT, 'auth plugin'),
                        'admin' => new external_value(PARAM_INT, 'admin user'),
                    )
                )
            );
    }

    public static function get_moodle_users($limitstart, $limit, $order, $order_dir, $search) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_moodle_users_parameters(), array('limitstart'=>$limitstart, 'limit'=>$limit, 'order'=>$order, 'order_dir'=>$order_dir,'search' => $search));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_moodle_users ($limitstart, $limit, $order, $order_dir, $search);


        return $return;
    }

    /* get_moodle_users_number */
    public static function get_moodle_users_number_parameters() {
        return new external_function_parameters(
                        array(
                            'search' => new external_value(PARAM_TEXT, 'sarch text'),
                        )
                );
    }

    public static function get_moodle_users_number_returns() {
        return new  external_value(PARAM_INT, 'user number');
    }

    public static function get_moodle_users_number($search) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_moodle_users_number_parameters(), array('search' => $search));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_moodle_users_number ($search);


        return $return;
    }

    /* user_exists */
    public static function user_exists_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'multilang compatible name, course unique'),
                        )
        );
    }

    public static function user_exists_returns() {
        return new  external_value(PARAM_INT, 'whether user exists');
    }

    public static function user_exists($username) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::user_exists_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->user_exists ($username);

        return $id;
    }
    
    public static function user_course_inprogress_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }
    public static function user_course_inprogress_returns() {
        return new  external_value(PARAM_BOOL, 'whether user exists');
    }

    public static function user_course_inprogress($username) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::user_course_inprogress_parameters(), array('username'=>$username));
        
        $auth = new auth_plugin_joomdle();
        $user_inprogress = $auth->user_course_inprogress($username);
        return $user_inprogress;
    }

    /* create_joomdle_user */
    public static function create_joomdle_user_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function create_joomdle_user_returns() {
        return new  external_value(PARAM_INT, 'user created');
    }

    public static function create_joomdle_user($username) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::create_joomdle_user_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->create_joomdle_user ($username);

        return $id;
    }

    /* create_joomdle_user_additional */
    public static function create_joomdle_user_additional_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'app' => new external_value(PARAM_TEXT, 'app'),
                        )
        );
    }

    public static function create_joomdle_user_additional_returns() {
        return new  external_value(PARAM_INT, 'user created');
    }

    public static function create_joomdle_user_additional($username, $app) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::create_joomdle_user_additional_parameters(), array('username'=>$username, 'app'=>$app));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->create_joomdle_user_additional ($username, $app);

        return $id;
    }

    /* enrol_user */
    public static function enrol_user_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'roleid' => new external_value(PARAM_INT, 'role id'),
                        )
        );
    }

    public static function enrol_user_returns() {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL, 'status'),
                        'message' => new external_value(PARAM_TEXT, 'message'),
                    )
                );
    }

    public static function enrol_user($username, $id, $roleid) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::enrol_user_parameters(), array('username'=>$username, 'id' => $id, 'roleid' => $roleid));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->enrol_user ($username, $id, $roleid);

        return $id;
    }

    /* multiple_enrol_and_addtogroup */
    public static function multiple_enrol_and_addtogroup_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'courses' => new external_value(PARAM_TEXT, 'course shortnames'),
                            'groups' => new external_value(PARAM_TEXT, 'group names'),
                            'roleid' => new external_value(PARAM_INT, 'role id'),
                        )
        );
    }

    public static function multiple_enrol_and_addtogroup_returns() {
        return new  external_value(PARAM_INT, 'user enroled');
    }

    public static function multiple_enrol_and_addtogroup($username, $courses, $groups, $roleid) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::multiple_enrol_and_addtogroup_parameters(), array('username'=>$username, 'courses' => $courses, 'groups' => $groups, 'roleid' => $roleid));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->multiple_enrol_and_addtogroup ($username, $courses, $groups, $roleid);

        return $id;
    }

    /* multiple_enrol */
    public static function multiple_enrol_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'courses' => new external_multiple_structure(
                                    new external_single_structure(
                                        array(
                                            'id' => new external_value(PARAM_INT, 'course id'),
                                        )
                                    )
                                ),
                            'roleid' => new external_value(PARAM_INT, 'role id'),
                            'progid' => new external_value(PARAM_INT, 'prog id', false),
                        )
        );
    }

    public static function multiple_enrol_returns() {
        return new  external_value(PARAM_INT, 'user enroled');
    }

    public static function multiple_enrol($username, $courses, $roleid, $progid) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::multiple_enrol_parameters(), array('username'=>$username, 'courses' => $courses, 'roleid' => $roleid, 'progid' => $progid));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->multiple_enrol ($username, $courses, $roleid, $progid);

        return $id;
    }

    /* user_details */
    public static function user_details_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function user_details_returns() {
        return  new external_single_structure(
                    array(
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'firstname' => new external_value(PARAM_TEXT, 'firstname'),
                        'lastname' => new external_value(PARAM_TEXT, 'lastname'),
                        'email' => new external_value(PARAM_TEXT, 'email'),
                        'id' => new external_value(PARAM_INT, 'id'),
                        'name' => new external_value(PARAM_TEXT, 'name', VALUE_OPTIONAL),
                        'city' => new external_value(PARAM_TEXT, 'city', VALUE_OPTIONAL),
                        'country' => new external_value(PARAM_TEXT, 'country', VALUE_OPTIONAL),
                        'lang' => new external_value(PARAM_TEXT, 'lang', VALUE_OPTIONAL),
                        'timezone' => new external_value(PARAM_TEXT, 'timezone', VALUE_OPTIONAL),
                        'phone1' => new external_value(PARAM_TEXT, 'phone1', VALUE_OPTIONAL),
                        'phone2' => new external_value(PARAM_TEXT, 'phone2', VALUE_OPTIONAL),
                        'address' => new external_value(PARAM_TEXT, 'address', VALUE_OPTIONAL),
                        'description' => new external_value(PARAM_RAW, 'description', VALUE_OPTIONAL),
                        'institution' => new external_value(PARAM_TEXT, 'institution', VALUE_OPTIONAL),
                        'url' => new external_value(PARAM_TEXT, 'url', VALUE_OPTIONAL),
                        'icq' => new external_value(PARAM_TEXT, 'icq', VALUE_OPTIONAL),
                        'skype' => new external_value(PARAM_TEXT, 'skype', VALUE_OPTIONAL),
                        'aim' => new external_value(PARAM_TEXT, 'aim', VALUE_OPTIONAL),
                        'yahoo' => new external_value(PARAM_TEXT, 'yahoo', VALUE_OPTIONAL),
                        'msn' => new external_value(PARAM_TEXT, 'msn', VALUE_OPTIONAL),
                        'idnumber' => new external_value(PARAM_TEXT, 'idnumber', VALUE_OPTIONAL),
                        'department' => new external_value(PARAM_TEXT, 'department', VALUE_OPTIONAL),
                        'picture' => new external_value(PARAM_TEXT, 'picture', VALUE_OPTIONAL),
                        'pic_url' => new external_value(PARAM_TEXT, 'pic url', VALUE_OPTIONAL),
                        'custom_fields' => new external_multiple_structure(
                                new external_single_structure(
                                    array(
                                        'id' => new external_value(PARAM_INT, 'field id'),
                                        'data' => new external_value(PARAM_RAW, 'data')
                                    )
                                )
                            )
                    )
                );
    }

    public static function user_details($username) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::user_details_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->user_details ($username);

        return $id;
    }

    /* user_details_by_id */
    public static function user_details_by_id_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'user id'),
                        )
        );
    }

    public static function user_details_by_id_returns() {
        return  new external_single_structure(
                    array(
                        'username' => new external_value(PARAM_TEXT, 'username'),
                    )
                );
    }

    public static function user_details_by_id($id) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::user_details_by_id_parameters(), array('id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->user_details_by_id ($id);

        return $id;
    }


   /* migrate_to_joomdle */
    public static function migrate_to_joomdle_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username')
                        )
        );
    }

    public static function migrate_to_joomdle_returns() {
        return new  external_value(PARAM_BOOL, 'user migrated');
    }
    public static function migrate_to_joomdle($username) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::migrate_to_joomdle_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->migrate_to_joomdle ($username);

        return $id;
    }

   /* my_events */
    public static function my_events_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'courses' =>  new external_multiple_structure(
                                            new external_single_structure(
                                                array(
                                                    'id' => new external_value(PARAM_INT, 'course id'),
                                                )
                                            )
                                        )
                        )
        );
    }

    public static function my_events_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                    'name' => new external_value(PARAM_TEXT, 'event name'),
                    'timestart' => new external_value(PARAM_INT, 'start time'),
                    'courseid' => new external_value(PARAM_INT, 'course id'),
                )
            )
        );
    }
    public static function my_events($username, $courses) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::my_events_parameters(), array('username'=>$username, 'courses' => $courses));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->my_events ($username, $courses);

        return $id;
    }

    /* delete_user */
    public static function delete_user_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function delete_user_returns() {
        return new  external_value(PARAM_BOOL, 'user deleted');
    }

    public static function delete_user($username) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::delete_user_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->delete_user ($username);

        return $id;
    }

    /* get_mentees */
    public static function get_mentees_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_mentees_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'user id'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'name' => new external_value(PARAM_TEXT, 'name'),
                    )
                )
            );
    }

    public static function get_mentees($username) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_mentees_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->get_mentees ($username);

        return $id;
    }


    /* get_roles */
    public static function get_roles_parameters() {
        return new external_function_parameters(
                        array(
                        )
        );
    }

    public static function get_roles_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'role id'),
                        'name' => new external_value(PARAM_TEXT, 'role name'),
                    )
                )
            );
    }

    public static function get_roles() { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_roles_parameters(), array());
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->get_roles ();

        return $id;
    }

    /* get_parents */
    public static function get_parents_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_parents_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'username' => new external_value(PARAM_TEXT, 'username'),
                    )
                )
            );
    }

    public static function get_parents($username) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_parents_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->get_parents ($username);

        return $id;
    }

    /* get_site_last_week_stats */
    public static function get_site_last_week_stats_parameters() {
        return new external_function_parameters(
                        array(
                        )
        );
    }

    public static function get_site_last_week_stats_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                    'stat1' => new external_value(PARAM_INT, 'reads'),
                    'stat2' => new external_value(PARAM_INT, 'writes'),
                )
            )
        );
    }

    public static function get_site_last_week_stats() {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_site_last_week_stats_parameters(), array());
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_site_last_week_stats ();

        return $return;
    }

    /* get_course_daily_stats */
    public static function get_course_daily_stats_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function get_course_daily_stats_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                    'stat1' => new external_value(PARAM_INT, 'reads'),
                    'stat2' => new external_value(PARAM_INT, 'writes'),
                    'timeend' => new external_value(PARAM_INT, 'time'),
                )
            )
        );
    }

    public static function get_course_daily_stats($id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_daily_stats_parameters(), array('id' => $id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_daily_stats ($id);

        return $return;
    }

    /* get_last_user_grades */
    public static function get_last_user_grades_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'limit' => new external_value(PARAM_INT, 'max items to return'),
                        )
        );
    }

    public static function get_last_user_grades_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'itemname' => new external_value(PARAM_TEXT, 'task name'),
                        'finalgrade' => new external_value(PARAM_TEXT, 'grade'),
                        'average' => new external_value(PARAM_TEXT, 'average'),
                    )
                )
            );
    }

    public static function get_last_user_grades($username, $limit) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_last_user_grades_parameters(), array('username'=>$username, 'limit' => $limit));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->get_last_user_grades ($username, $limit);

        return $id;
    }

    /* system_check */
    public static function system_check_parameters() {
        return new external_function_parameters(
                        array(
                        )
        );
    }

    public static function system_check_returns() {
           return new external_single_structure(
                array(
                    'joomdle_auth' => new external_value(PARAM_INT, 'joomdle plugin enabled'),
                    'mnet_auth' => new external_value(PARAM_INT, 'mnet plugin enabled'),
 //                   'enablewebservices' => new external_value(PARAM_INT, 'webservices enabled'),
                    'joomdle_configured' => new external_value(PARAM_INT, 'joomdle configured'),
                    'test_data' => new external_value(PARAM_RAW, 'test data', VALUE_OPTIONAL),
                )
        );
    }

    public static function system_check() {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::system_check_parameters(), array());
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->system_check ();

        return $return;
    }

    /* add_parent_role */
    public static function add_parent_role_parameters() {
        return new external_function_parameters(
                        array(
                            'child' => new external_value(PARAM_TEXT, 'child username'),
                            'parent' => new external_value(PARAM_TEXT, ' parent username'),
                        )
        );
    }

    public static function add_parent_role_returns() {
        return new  external_value(PARAM_TEXT, 'role added');
    }

    public static function add_parent_role($child, $parent) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::add_parent_role_parameters(), array('child'=>$child, 'parent' => $parent));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->add_parent_role ($child, $parent);

        return $id;
    }

    /* get_paypal_config */
    public static function get_paypal_config_parameters() {
        return new external_function_parameters(
                        array(
                        )
        );
    }

    public static function get_paypal_config_returns() {
        return  new external_single_structure(
                    array(
                        'paypalurl' => new external_value(PARAM_TEXT, 'paypal url'),
                        'paypalbusiness' => new external_value(PARAM_TEXT, 'paypal email'),
                    )
                );
    }

    public static function get_paypal_config() { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_paypal_config_parameters(), array());
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->get_paypal_config ();

        return $id;
    }

    /* update_session */
    public static function update_session_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function update_session_returns() {
        return new  external_value(PARAM_BOOL, 'session updated');
    }

    public static function update_session($username) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::update_session_parameters(), array('username' => $username));

        $auth = new  auth_plugin_joomdle ();
        $id = $auth->update_session ($username);

        return $id;
    }


    /* get_cat_name */
    public static function get_cat_name_parameters() {
        return new external_function_parameters(
                        array(
                            'cat_id' => new external_value(PARAM_INT, 'category id'),
                        )
        );
    }

    public static function get_cat_name_returns() {
        return new  external_value(PARAM_TEXT, 'category name');
    }

    public static function get_cat_name($cat_id) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_cat_name_parameters(), array('cat_id'=>$cat_id));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->get_cat_name ($cat_id);

        return $id;
    }

    /* courses_abc */
    public static function courses_abc_parameters() {
        return new external_function_parameters(
                        array(
                            'start_chars' => new external_value(PARAM_TEXT, 'Start chars'),
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function courses_abc_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                    'remoteid' => new external_value(PARAM_INT, 'course id'),
                    'cat_id' => new external_value(PARAM_INT, 'category id'),
                    'cat_name' => new external_value(PARAM_TEXT, 'cartegory name'),
                    'cat_description' => new external_value(PARAM_RAW, 'category description'),
                    'sortorder' => new external_value(PARAM_TEXT, 'sortorder'),
                    'fullname' => new external_value(PARAM_TEXT, 'course name'),
                    'shortname' => new external_value(PARAM_TEXT, 'course shortname'),
                    'idnumber' => new external_value(PARAM_RAW, 'idnumber'),
                    'summary' => new external_value(PARAM_RAW, 'summary'),
                    'startdate' => new external_value(PARAM_INT, 'start date'),
                    'created' => new external_value(PARAM_INT, 'created'),
                    'modified' => new external_value(PARAM_INT, 'modified'), 
                    'cost' => new external_value(PARAM_FLOAT, 'cost', VALUE_OPTIONAL),
                    'currency' => new external_value(PARAM_TEXT, 'currency', VALUE_OPTIONAL),
					'self_enrolment' => new external_value(PARAM_INT, 'self enrollable'), 
					'enroled' => new external_value(PARAM_INT, 'user enroled'), 
					'in_enrol_date' => new external_value(PARAM_BOOL, 'in enrol date'), 
                    'guest' => new external_value(PARAM_INT, 'guest access'), 
                )
            )
        );
    }

    public static function courses_abc($start_chars, $username) { 
        global $CFG, $DB;
 
      $params = self::validate_parameters(self::courses_abc_parameters(), array('start_chars' => $start_chars, 'username' => $username)); 
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->courses_abc ($start_chars, $username);

        return $id;
    }

    /* teachers_abc */
    public static function teachers_abc_parameters() {
        return new external_function_parameters(
                        array(
                            'start_chars' => new external_value(PARAM_TEXT, 'Start chars'),
                        )
        );
    }

    public static function teachers_abc_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                        'firstname' => new external_value(PARAM_TEXT, 'firstname'),
                        'lastname' => new external_value(PARAM_TEXT, 'lastname'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                )
            )
        );
    }

    public static function teachers_abc($start_chars) { 
        global $CFG, $DB;
 
      $params = self::validate_parameters(self::teachers_abc_parameters(), array('start_chars' => $start_chars)); 
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->teachers_abc ($start_chars);

        return $id;
    }

    /* teacher_courses */
    public static function teacher_courses_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'Teacher username'),
                        )
        );
    }

    public static function teacher_courses_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                        'remoteid' => new external_value(PARAM_INT, 'course id'),
                        'fullname' => new external_value(PARAM_TEXT, 'course name'),
                        'cat_id' => new external_value(PARAM_INT, 'category id'),
                        'cat_name' => new external_value(PARAM_TEXT, 'category name'),
                )
            )
        );
    }

    public static function teacher_courses($username) { 
        global $CFG, $DB;
 
      $params = self::validate_parameters(self::teacher_courses_parameters(), array('username' => $username)); 
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->teacher_courses ($username);

        return $id;
    }

    /* user_custom_fields */
    public static function user_custom_fields_parameters() {
        return new external_function_parameters(
                        array(
                        )
        );
    }

    public static function user_custom_fields_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                        'id' => new external_value(PARAM_INT, 'field id'),
                        'name' => new external_value(PARAM_TEXT, 'field name'),
                )
            )
        );
    }

    public static function user_custom_fields() { 
        global $CFG, $DB;
 
      $params = self::validate_parameters(self::user_custom_fields_parameters(), array()); 
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->user_custom_fields ();

        return $id;
    }

    /* course_enrol_methods */
    public static function course_enrol_methods_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function course_enrol_methods_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                        'id' => new external_value(PARAM_INT, 'enrol method id'),
                        'enrol' => new external_value(PARAM_TEXT, 'enrol method name'),
						'enrolstartdate' => new external_value(PARAM_INT, 'enrol start date', VALUE_OPTIONAL), 
						'enrolenddate' => new external_value(PARAM_INT, 'enrol end date', VALUE_OPTIONAL), 
                )
            )
        );
    }

    public static function course_enrol_methods($id) { 
        global $CFG, $DB;
 
      $params = self::validate_parameters(self::course_enrol_methods_parameters(), array('id' => $id)); 
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->course_enrol_methods ($id);

        return $id;
    }

    /* QUIZ FUNCTIONS */

    /* quiz_get_question */
    public static function quiz_get_question_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'question id'),
                            'newformat' => new external_value(PARAM_BOOL, 'new format result', false)
                        )
        );
    }

    public static function quiz_get_question_returns() {
         return  new external_single_structure(
                array(
                    'res' => new external_value(PARAM_RAW, 'result', false),
                    'id' => new external_value(PARAM_INT, 'question id', false),
                    'questiontext' => new external_value(PARAM_RAW, 'question text', false),
                    'generalfeedback' => new external_value(PARAM_RAW, 'question general feedback', false),
                    'qtype' => new external_value(PARAM_TEXT, 'question type', false),
                    'name' => new external_value(PARAM_RAW, 'question name', false),
                    'options' => new external_value(PARAM_RAW, 'question options', false),
                )
        );
    }

    public static function quiz_get_question($id, $newformat) { 
        global $CFG, $DB;
 
      $params = self::validate_parameters(self::quiz_get_question_parameters(), array('id' => $id, 'newformat' => $newformat)); 
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->quiz_get_question ($id, $newformat);

        return $id;
    }

    /* quiz_random_question */
    public static function quiz_get_random_question_parameters() {
        return new external_function_parameters(
                        array(
                            'cat_id' => new external_value(PARAM_INT, 'category id'),
                            'used_ids' => new external_value(PARAM_TEXT, 'used question ids'),
                        )
        );
    }

    public static function quiz_get_random_question_returns() {
         return  new external_single_structure(
                array(
                        'id' => new external_value(PARAM_INT, 'question id'),
                        'questiontext' => new external_value(PARAM_RAW, 'question text'),
                        'qtype' => new external_value(PARAM_TEXT, 'question type'),
                        'answers' =>  new external_multiple_structure(
                             new external_single_structure(
                                array(
                                        'id' => new external_value(PARAM_INT, 'answer id'),
                                        'answer' => new external_value(PARAM_RAW, 'answer text'),
                                        'fraction' => new external_value(PARAM_FLOAT, 'answer text'),
                                )
                            )
                        )
                )
        );
    }

    public static function quiz_get_random_question($cat_id, $used_ids) { 
        global $CFG, $DB;
 
      $params = self::validate_parameters(self::quiz_get_random_question_parameters(), array('cat_id' => $cat_id, 'used_ids' => $used_ids)); 
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->quiz_get_random_question ($cat_id, $used_ids);

        return $id;
    }

    /* quiz_get_correct_answer */
    public static function quiz_get_correct_answer_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'question id'),
                        )
        );
    }

    public static function quiz_get_correct_answer_returns() {
        return new  external_value(PARAM_INT, 'answer id');
    }

    public static function quiz_get_correct_answer($id) { 
        global $CFG, $DB;
 
      $params = self::validate_parameters(self::quiz_get_correct_answer_parameters(), array('id' => $id)); 
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->quiz_get_correct_answer ($id);

        return $id;
    }

    /* get_question_categories */
    public static function quiz_get_question_categories_parameters() {
        return new external_function_parameters(
                        array(
                        )
        );
    }

    public static function quiz_get_question_categories_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'category id'),
                        'name' => new external_value(PARAM_TEXT, 'category name'),
                    )
                )
            );
    }

    public static function quiz_get_question_categories() {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::quiz_get_question_categories_parameters(), array());
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->quiz_get_question_categories ();


        return $return;
    }

    /* quiz_get_answers */
    public static function quiz_get_answers_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'question id'),
                        )
        );
    }

    public static function quiz_get_answers_returns() {
         return new external_multiple_structure(
             new external_single_structure(
                array(
                        'id' => new external_value(PARAM_INT, 'answer id'),
                        'answer' => new external_value(PARAM_RAW, 'answer text'),
                )
            )
        );
    }

    public static function quiz_get_answers($id) { 
        global $CFG, $DB;
 
      $params = self::validate_parameters(self::quiz_get_answers_parameters(), array('id' => $id)); 
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->quiz_get_answers ($id);

        return $id;
    }

    /* get_course_students */
    public static function get_course_students_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'complete' => new external_value(PARAM_TEXT, 'complete',false),
                        )
        );
    }

    public static function get_course_students_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'firstname' => new external_value(PARAM_TEXT, 'firstname'),
                        'lastname' => new external_value(PARAM_TEXT, 'lastname'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'id' => new external_value(PARAM_INT, 'user id'),
                    )
                )
            );
    }

    public static function get_course_students($id,$complete) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_course_students_parameters(), array('id'=>$id, 'complete'=>$complete));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_students ($id, $complete);


        return $return;
    }

    /* my_teachers */
    public static function my_teachers_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'Username'),
                        )
        );
    }

    public static function my_teachers_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'group record id'),
                    'fullname' => new external_value(PARAM_TEXT, 'course name'),
                    'teachers' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'firstname' => new external_value(PARAM_TEXT, 'firstname'),
                                'lastname' => new external_value(PARAM_TEXT, 'lastname'),
                                'username' => new external_value(PARAM_TEXT, 'username'),
                            )
                        )
                    )
                )
            )
        );
    }

    public static function my_teachers($username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::my_teachers_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->my_teachers ($username);

        return $return;
    }

    /* my_classmates */
    public static function my_classmates_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function my_classmates_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'firstname' => new external_value(PARAM_TEXT, 'firstname'),
                        'lastname' => new external_value(PARAM_TEXT, 'lastname'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                    )
                )
            );
    }

    public static function my_classmates($username) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::my_classmates_parameters(), array('username'=>$username));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->my_classmates ($username);


        return $return;
    }

    /* multiple_suspend_enrolment */
    public static function multiple_suspend_enrolment_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'courses' => new external_multiple_structure(
                                    new external_single_structure(
                                        array(
                                            'id' => new external_value(PARAM_INT, 'course id'),
                                        )
                                    )
                                ),
                        )
        );
    }

    public static function multiple_suspend_enrolment_returns() {
        return new  external_value(PARAM_INT, 'user enroled');
    }

    public static function multiple_suspend_enrolment($username, $courses) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::multiple_suspend_enrolment_parameters(), array('username'=>$username, 'courses' => $courses));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->multiple_suspend_enrolment ($username, $courses);

        return $id;
    }

        /* suspend_enrolment */
    public static function suspend_enrolment_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function suspend_enrolment_returns() {
        return new  external_value(PARAM_INT, 'user created');
    }

    public static function suspend_enrolment($username, $id) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::suspend_enrolment_parameters(), array('username'=>$username, 'id' => $id));

        $auth = new  auth_plugin_joomdle ();
        $id = $auth->suspend_enrolment ($username, $id);

        return $id;
    }

    /* get_course_resources */
    public static function get_course_resources_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_course_resources_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'section' => new external_value(PARAM_INT, 'section id'),
                        'summary' => new external_value(PARAM_RAW, 'summary'),
                        'resources' => new external_multiple_structure(
                                        new external_single_structure(
                                            array (
                                            'id' => new external_value(PARAM_INT, 'resource id'),
                                            'name' => new external_value(PARAM_RAW, 'name'),
                                            'type' => new external_value(PARAM_RAW, 'type'),
                                            )
                                        )
                            ),
                    )
                )
            );
    }

    public static function get_course_resources($id, $username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_resources_parameters(), array('id'=>$id, 'username' => $username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_resources ($id, $username);


        return $return;
    }

    /* get_course_mods */
    public static function get_course_mods_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'mtype' => new external_value(PARAM_TEXT, 'module type', false),
                        )
        );
    }

    public static function get_course_mods_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'coursemods' => new external_multiple_structure(
                new external_single_structure(
                    array(
                        'section' => new external_value(PARAM_INT, 'section id'),
                        'name' => new external_value(PARAM_TEXT, 'section name'),
                        'summary' => new external_value(PARAM_RAW, 'summary'),
                        'mods' => new external_multiple_structure(
                                        new external_single_structure(
                                            array (
                                            'id' => new external_value(PARAM_INT, 'resource id'),
                                            'atype' => new external_value(PARAM_INT, 'assessment type', false),
                                            'name' => new external_value(PARAM_RAW, 'name'),
                                            'mod' => new external_value(PARAM_RAW, 'mod'),
                                            'type' => new external_value(PARAM_RAW, 'type'),
                                            'available' => new external_value(PARAM_INT, 'available'),
                                            'completion_info' => new external_value(PARAM_RAW, 'completion info'),
                                            'mod_completion' => new external_value(PARAM_BOOL, 'mod_completion'),
                                            'display' => new external_value(PARAM_INT, 'display'),
                                            'url' => new external_value(PARAM_RAW, 'url activity'),
                                            )
                                        )
                            ),
                    )
                )
                        )
                    )
            );
    }

    public static function get_course_mods($id, $username, $mtype) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_mods_parameters(), array('id'=>$id, 'username' => $username, 'mtype'=>$mtype));
 
        $auth = new  auth_plugin_joomdle ();
        $auth->update_last_access_course($id, $username); //save last access time
        $return = $auth->get_course_mods($id, $username, $mtype);


        return $return;
    }
    /* get_course_mods_visible */
    public static function get_course_mods_visible_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'course id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
                'visible' => new external_value(PARAM_INT, 'visible'),
                'mtype' => new external_value(PARAM_TEXT, 'module type', false),
            )
        );
    }

    public static function get_course_mods_visible_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'section' => new external_value(PARAM_INT, 'section no'),
                    'sectionid' => new external_value(PARAM_INT, 'section id'),
                    'name' => new external_value(PARAM_TEXT, 'section name'),
                    'summary' => new external_value(PARAM_RAW, 'summary'),
                    'mods' => new external_multiple_structure(
                        new external_single_structure(
                            array (
                                'id' => new external_value(PARAM_INT, 'resource id'),
                                'atype' => new external_value(PARAM_INT, 'assessment type', false),
                                'name' => new external_value(PARAM_RAW, 'name'),
                                'mod' => new external_value(PARAM_RAW, 'mod'),
                                'type' => new external_value(PARAM_RAW, 'type'),
                                'available' => new external_value(PARAM_INT, 'available'),
                                'completion_info' => new external_value(PARAM_RAW, 'completion info'),
                                'mod_completion' => new external_value(PARAM_BOOL, 'mod_completion'),
                                'display' => new external_value(PARAM_INT, 'display'),
                                'url' => new external_value(PARAM_RAW, 'url activity'),
                            )
                        )
                    ),
                )
            )
        );
    }

    public static function get_course_mods_visible($id, $username,$visible, $mtype) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_course_mods_visible_parameters(), array('id'=>$id, 'username' => $username,'visible'=>$visible, 'mtype'=>$mtype));

        $auth = new  auth_plugin_joomdle ();
        $auth->update_last_access_course($id, $username); //save last access time
        $return = $auth->get_course_mods_visible($id, $username,$visible, $mtype);


        return $return;
    }
    
    /* list_activity_add_to_course */
    public static function list_activity_add_to_course_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }
    
    public static function list_activity_add_to_course($id, $username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_mods_parameters(), array('id'=>$id, 'username' => $username));
 
        $auth = new  auth_plugin_joomdle ();
        $auth->update_last_access_course($id, $username); //save last access time
        $return = $auth->list_activity_add_to_course($id, $username);


        return $return;
    }

    public static function list_activity_add_to_course_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'coursemods' => new external_multiple_structure(
                new external_single_structure(
                    array(
                        'modtype' => new external_value(PARAM_TEXT, 'mod type'),
                        'total' => new external_value(PARAM_TEXT, 'mods total'),
                        'mods' => new external_multiple_structure(
                                        new external_single_structure(
                                            array (
                                                'id' => new external_value(PARAM_INT, 'resource id'),
                                                'atype' => new external_value(PARAM_INT, 'assessment type', false),
                                                'name' => new external_value(PARAM_RAW, 'name'),
                                                'mod' => new external_value(PARAM_RAW, 'mod'),
                                                'type' => new external_value(PARAM_RAW, 'type'),
                                                'available' => new external_value(PARAM_INT, 'available'),
                                                'completion_info' => new external_value(PARAM_RAW, 'completion info'),
                                                'mod_completion' => new external_value(PARAM_BOOL, 'mod_completion'),
                                                'display' => new external_value(PARAM_INT, 'display'),
                                                'activityAdded' => new external_value(PARAM_BOOL, 'Activity added to course outline')
                                            )
                                        )
                            ),
                    )
                )
                        )
                    )
            );
    }
    
    // Sort module
    public static function sort_module_in_section_course_parameters(){
        return new external_function_parameters(
            array(
                'listModuleSection' => new external_value(PARAM_TEXT, 'Section Num'),
                'courseid' => new external_value(PARAM_INT, 'Course ID'),
                'sectionnum' => new external_value(PARAM_INT, 'Section Num'),
                'titleSection' => new external_value(PARAM_TEXT, 'Section Title'),
            )
        );
    }
    public static function sort_module_in_section_course_returns(){
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'status'=> new external_value(PARAM_BOOL, 'status'),
                    'message'=> new external_value(PARAM_TEXT, 'message'),
                )
            )
        );
    }
    public static function sort_module_in_section_course($listModuleSection,$courseid,$sectionnum,$titleSection){
        global $CFG, $DB;

        $params = self::validate_parameters(self::sort_module_in_section_course_parameters(), array('listModuleSection'=> $listModuleSection,'courseid'=> $courseid,'sectionnum'=>$sectionnum,'titleSection'=>$titleSection));
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->sort_module_in_section_course ($listModuleSection,$courseid,$sectionnum,$titleSection);
        return $return;
    }


    /* my_certificates */
    public static function get_certificate_parameters() {
        return new external_function_parameters(
            array(
                'username' => new external_value(PARAM_TEXT, 'username'),
                'courseid' => new external_value(PARAM_INT, 'courseid'),
            )
        );
    }

    public static function get_certificate_returns() {
        return new external_multiple_structure(
            new external_single_structure(
            array(
                    'name' => new external_value(PARAM_TEXT, 'name'),
                    'id' => new external_value(PARAM_INT, 'id'),
                    'date' => new external_value(PARAM_TEXT, 'certdate'),

                    'downloadFiles' => new external_value(PARAM_RAW, 'downloadFiles'),
                    'url' => new external_value(PARAM_RAW, 'url'),
                    'userid' => new external_value(PARAM_INT, 'userid'),

                    'coursename' => new external_value(PARAM_TEXT, 'coursename'),
                    'certificateid' => new external_value(PARAM_INT, 'certificateid')
            )
            )
        );
    }

    public static function get_certificate($username, $courseid) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_certificate_parameters(), array('username'=>$username, 'courseid'=>$courseid));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_certificate ($username, $courseid);


        return $return;
    }


    // multiple edit activity and section
    public static function multiple_sort_module_in_section_course($username, $courseid, $sections) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::multiple_sort_module_in_section_course_parameters(), array('username' => $username, 'courseid'=> $courseid, 'sections' => $sections));
        $auth = new auth_plugin_joomdle();
        $return = $auth->multiple_sort_module_in_section_course($username, $courseid, $sections);
        return $return;
    }
    
    public static function multiple_sort_module_in_section_course_parameters() {
        return new external_function_parameters(
                array(
                    'username' => new external_value(PARAM_TEXT, 'username'),
                    'courseid' => new external_value(PARAM_INT, 'courseid'),
                    'sections' => new external_value(PARAM_RAW, 'data sections'),
//                    'sections' => new external_single_structure(
//                            array(
//                                'sectionname' => new external_value(PARAM_TEXT, 'section title'),
//                                'sectionnum' => new external_value(PARAM_INT, 'section title'),
//                                'mods' => new external_multiple_structure(
//                                            new external_single_structure(
//                                                        array(
//                                                            'moduleid' => new external_value(PARAM_INT, 'module id'),
//                                                            'moduletitle' => new external_value(PARAM_TEXT, 'module name'),
//                                                            'position' => new external_value(PARAM_INT, 'module postion'),
//                                                            'action' => new external_value(PARAM_TEXT, 'module status'),
//                                                        )
//                                                    )
//                                        )
//                             )
//                            )
                )
            );
    }
    public static function multiple_sort_module_in_section_course_returns() {
        return new external_single_structure(
                 array(
                     'status'=> new external_value(PARAM_BOOL, 'status'),
                     'message'=> new external_value(PARAM_TEXT, 'message'),
                 )
                );
    }

    // Edit title module update_title_module update_title_module
    public static function update_title_module_parameters(){
        return new external_function_parameters(
            array(
                'data_moduid_title' => new external_value(PARAM_RAW, 'data json module id title'),
            )
        );
    }
    public static function update_title_module_returns(){
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'status'=> new external_value(PARAM_BOOL, 'status'),
                    'message'=> new external_value(PARAM_TEXT, 'message'),
                )
            )
        );
    }
    public static function update_title_module($data_moduid_title){
        global $CFG, $DB;

        $params = self::validate_parameters(self::update_title_module_parameters(), array('data_moduid_title'=> $data_moduid_title));
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->update_title_module ($data_moduid_title);
        return $return;
    }


    /* add_course_outline_to course_content */
    public static function update_visible_module_course_parameters() {
        return new external_function_parameters(
            array(
                'module_id' => new external_value(PARAM_INT, 'module id'),
                'course_id'=> new external_value(PARAM_INT, 'course id'),
                'status' => new external_value(PARAM_INT, 'status'),
            )
        );
    }

    public static function update_visible_module_course_returns() {
        return new  external_value(PARAM_BOOL, 'visible updated');

    }

    public static function update_visible_module_course($module_id, $course_id,$status) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::update_visible_module_course_parameters(), array('module_id'=>$module_id  , 'course_id'=>$course_id, 'status'=>$status));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->update_visible_module_course($module_id, $course_id,$status);


        return $return;
    }

    // get_question_option_match
     public static function get_question_option_match_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'id_question')
            )
        );
    }
    public static function get_question_option_match_returns() {
        return new external_multiple_structure(
            new external_single_structure(
            array(
                'id' => new external_value(PARAM_INT, 'course id'),
                'questiontext' => new external_value(PARAM_TEXT, 'questiontext'),
                'answertext' => new external_value(PARAM_TEXT, 'answertext'),
            )
            )
        );
    }
    public static function get_question_option_match($id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_question_option_match_parameters(), array('id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_question_option_match ($id);


        return $return;
    }
    // get_question_option_match
    /* get_course_completion */
    public static function get_course_completion_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_course_completion_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'section' => new external_value(PARAM_INT, 'section id'),
                        'summary' => new external_value(PARAM_RAW, 'summary'),
                        'complete' => new external_value(PARAM_INT, 'complete'),
                    )
                )
            );
    }

    public static function get_course_completion($id, $username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_completion_parameters(), array('id'=>$id, 'username' => $username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_completion ($id, $username);


        return $return;
    }

    /* get_course_quizes */
    public static function get_course_quizes_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_course_quizes_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'section' => new external_value(PARAM_INT, 'section id'),
                        'summary' => new external_value(PARAM_RAW, 'summary'),
                        'quizes' => new external_multiple_structure(
                            new external_single_structure(
                                array (
                                'id' => new external_value(PARAM_INT, 'resource id'),
                                'atype' => new external_value(PARAM_INT, 'resource id', false),
                                'name' => new external_value(PARAM_RAW, 'name'),
                                'intro' => new external_value(PARAM_RAW, 'intro'),
                                'grade' => new external_value(PARAM_FLOAT, 'grade'),
                                'passed' => new external_value(PARAM_BOOL, 'passed'),
                                'timelimit' => new external_value(PARAM_INT, 'timelimit quiz'),
                                'quiz_id' => new external_value(PARAM_INT, 'id quiz'),
                                'attempt' => new external_value(PARAM_INT, 'id attempt'),
                                'timeopen' => new external_value(PARAM_INT, 'timeopen quiz'),
                                'timeclose' => new external_value(PARAM_INT, 'timeclose quiz'),
                                // ================================

                                'time' => new external_single_structure(
                                    array(
                                        'state' => new external_value(PARAM_RAW, 'status quiz',false),
                                        'timestart' => new external_value(PARAM_INT, 'time start quiz',false),
                                        'timefinish' => new external_value(PARAM_INT, 'time finish quiz',false),
                                    )
                                ),
                                // ================================
                                'questions' => new external_value(PARAM_RAW, 'questions', false),
                                'numOfAttempts' => new external_value(PARAM_INT, 'numOfAttempts', false),
                                'attempts' => new external_value(PARAM_RAW, 'attempts', false),
                                
                                )
                            )
                        )
                    )
                )
            );
    }

    public static function get_course_quizes($id, $username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_quizes_parameters(), array('id'=>$id, 'username' => $username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_quizes ($id, $username);


        return $return;
    }

    /* my_certificates */
    public static function my_certificates_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'type' => new external_value(PARAM_TEXT, 'type'),
                        )
        );
    }

    public static function my_certificates_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                    'courseid'=>new external_value(PARAM_INT, 'courseid'),
                    'name' => new external_value(PARAM_TEXT, 'name'),
                    'id' => new external_value(PARAM_INT, 'id'),
                    'date' => new external_value(PARAM_TEXT, 'certdate'),
                    
                    'downloadFiles' => new external_value(PARAM_RAW, 'downloadFiles'),
                    'url' => new external_value(PARAM_RAW, 'url'),
                    'userid' => new external_value(PARAM_INT, 'userid'), 

                    'coursename' => new external_value(PARAM_TEXT, 'coursename'),
                    'certificateid' => new external_value(PARAM_INT, 'certificateid')   

                    // Trung modified 25-06 - not use these value
                    // 'code' => new external_value(PARAM_TEXT, 'code'),        
                    // 'orientation' => new external_value(PARAM_TEXT, 'orientation'), 
                    // 'borderstyle' => new external_value(PARAM_RAW, 'borderstyle'), 
                    // 'bordercolor' => new external_value(PARAM_INT, 'bordercolor'), 
                    // 'printwmark' => new external_value(PARAM_RAW, 'printwmark'), 
                    // 'printdate' => new external_value(PARAM_INT, 'printdate'), 
                    // 'datefmt' => new external_value(PARAM_INT, 'datefmt'), 
                    // 'printnumber' => new external_value(PARAM_INT, 'printnumber'), 
                    // 'printgrade' => new external_value(PARAM_INT, 'printgrade'), 
                    // 'gradefmt' => new external_value(PARAM_INT, 'gradefmt'), 
                    // 'printoutcome' => new external_value(PARAM_TEXT, 'printoutcome'), 
                    // 'printhours' => new external_value(PARAM_TEXT, 'printhours'), 
                    // 'printteacher' => new external_value(PARAM_INT, 'printteacher'), 
                    // 'customtext' => new external_value(PARAM_TEXT, 'customtext'), 
                    // 'printsignature' => new external_value(PARAM_RAW, 'printsignature'), 
                    // 'printseal' => new external_value(PARAM_RAW, 'printseal')
                    // end

                    )
                )
            );
    }

    public static function my_certificates($username, $type) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::my_certificates_parameters(), array('username'=>$username, 'type'=>$type));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->my_certificates ($username, $type);


        return $return;
    }


    /* get_page */
    public static function get_page_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'page id'),
                        )
        );
    }

    public static function get_page_returns() {
         return new external_single_structure(
                    array(
                        'name' => new external_value(PARAM_TEXT, 'name'),
                        'content' => new external_value(PARAM_RAW, 'content'),
                    )
        );
    }

    public static function get_page($id) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_page_parameters(), array('id'=>$id));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_page ($id);


        return $return;
    }

    /* get_label */
    public static function get_label_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'label id'),
                        )
        );
    }

    public static function get_label_returns() {
         return new external_single_structure(
                    array(
                        'name' => new external_value(PARAM_TEXT, 'name'),
                        'content' => new external_value(PARAM_RAW, 'content'),
                    )
        );
    }

    public static function get_label($id) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_label_parameters(), array('id'=>$id));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_label ($id);


        return $return;
    }

    /* get_news_item */
    public static function get_news_item_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'label id'),
                        )
        );
    }

    public static function get_news_item_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'subject' => new external_value(PARAM_RAW, 'name'),
                        'message' => new external_value(PARAM_RAW, 'name'),
                    )
                )
            );
    }

    public static function get_news_item($id) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_news_item_parameters(), array('id'=>$id));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_news_item ($id);


        return $return;
    }

    /* get_my_news */
    public static function get_my_news_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_my_news_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'remoteid' => new external_value(PARAM_INT, 'course id'),
                        'fullname' => new external_value(PARAM_TEXT, 'fullname'),
                        'news' => new external_multiple_structure(
                                    new external_single_structure(
                                        array (
                                            'discussion' => new external_value(PARAM_INT, 'discussion id'),
                                            'subject' => new external_value(PARAM_TEXT, 'subject'),
                                            'timemodified' => new external_value(PARAM_INT, 'timemodified'),
                                        )
                                    )
                        ),

                    )
                )
            );
    }

    public static function get_my_news($username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_my_news_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_my_news ($username);


        return $return;
    }

    /* get_my_events */
    public static function get_my_events_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_my_events_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'fullname' => new external_value(PARAM_TEXT, 'fullname'),
                        'remoteid' => new external_value(PARAM_INT, 'course id'),
                        'events' => new external_multiple_structure(
                                    new external_single_structure(
                                        array (
                                            'name' => new external_value(PARAM_TEXT, 'event name'),
                                            'timestart' => new external_value(PARAM_INT, 'timestart'),
                                            'courseid' => new external_value(PARAM_INT, 'course id'),
                                        )
                                    )
                        ),

                    )
                )
            );
    }

    public static function get_my_events($username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_my_events_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_my_events ($username);


        return $return;
    }

    /* get_my_grades */
    public static function get_my_grades_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_my_grades_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'fullname' => new external_value(PARAM_TEXT, 'fullname'),
                        'remoteid' => new external_value(PARAM_INT, 'course id'),
                        'grades' => new external_multiple_structure(
                                    new external_single_structure(
                                        array (
                                            'itemname' => new external_value(PARAM_TEXT, 'item name'),
                                            'finalgrade' => new external_value(PARAM_TEXT, 'final grade'),
                                        )
                                    )
                        ),

                    )
                )
            );
    }

    public static function get_my_grades($username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_my_grades_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_my_grades ($username);


        return $return;
    }

    /* get_course_grades_by_category */
    public static function get_course_grades_by_category_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'course id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
            )
        );
    }

    public static function get_course_grades_by_category_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'fullname' => new external_value(PARAM_TEXT, 'item name'),
                    'gradeitemid' => new external_value(PARAM_INT, 'grade item id', false),
                    'gradeid' => new external_value(PARAM_INT, 'item id'),
                    'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                    'gradefeedback' => new external_value(PARAM_TEXT, 'gradefeedback'),
                    'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                    'finalgradeletters' => new external_value(PARAM_TEXT, 'final grade letter'),
                    'hidden' => new external_value(PARAM_INT, 'hidden'),
                    'timemodified' => new external_value(PARAM_INT, 'timemodified'),
                    'gradeoffacitator' => new external_value(PARAM_BOOL, 'gradeoffacitator', false),
                    'items' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'name' => new external_value(PARAM_TEXT, 'item name'),
                                'due' => new external_value(PARAM_INT, 'due date'),
                                'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                                'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                                'feedback' => new external_value(PARAM_RAW, 'feedback'),
                            )
                        )
                    ),
                    'gradeoptionsletters' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'value' => new external_value(PARAM_INT, 'grade letter value'),
                                'name' => new external_value(PARAM_TEXT, 'grade letter name'),
                            )
                        )
                    )
                )
            )
        );
    }

    public static function get_course_grades_by_category($id, $username) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_course_grades_by_category_parameters(), array('id'=>$id, 'username' => $username));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_grades_by_category ($id, $username);


        return $return;
    }

    /* get_my_grades_by_category */
    public static function get_my_grades_by_category_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_my_grades_by_category_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'fullname' => new external_value(PARAM_TEXT, 'fullname'),
                        'remoteid' => new external_value(PARAM_INT, 'course id'),
                        'grades' => new external_multiple_structure(
                                        new external_single_structure(
                                array(
                                    'fullname' => new external_value(PARAM_TEXT, 'item name'),
                                    'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                                    'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                                    'items' => new external_multiple_structure(
                                            new external_single_structure(
                                                array(
                                                    'name' => new external_value(PARAM_TEXT, 'item name'),
                                                    'due' => new external_value(PARAM_INT, 'due date'),
                                                    'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                                                    'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                                                    'feedback' => new external_value(PARAM_RAW, 'feedback'),
                                                )
                                            )
                                        )
                                )
                            )
                        )
                    )
                )
            );
    }

    public static function get_my_grades_by_category($username) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_my_grades_by_category_parameters(), array( 'username' => $username));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_my_grades_by_category ($username);


        return $return;
    }

    /* get_cohorts */
    public static function get_cohorts_parameters() {
        return new external_function_parameters(
                        array(
                        )
        );
    }

    public static function get_cohorts_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'cohort id'),
                        'name' => new external_value(PARAM_TEXT, 'name'),
                    )
                )
            );
    }

    public static function get_cohorts() {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_cohorts_parameters(), array());
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_cohorts ();


        return $return;
    }

    /* add_cohort_member */
    public static function add_cohort_member_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'cohort_id' => new external_value(PARAM_INT, 'cohort id'),
                        )
        );
    }

    public static function add_cohort_member_returns() {
        return new  external_value(PARAM_INT, 'user added');
    }

    public static function add_cohort_member($username, $cohort_id) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::add_cohort_member_parameters(), array('username'=>$username, 'cohort_id' => $cohort_id));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->add_cohort_member ($username, $cohort_id);

        return $id;
    }

    /* get_rubrics */
    public static function get_rubrics_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'id'),
                        )
        );
    }

    public static function get_rubrics_returns() {
         return 
                new external_single_structure(
                        array(
                            'assign_name' => new external_value(PARAM_TEXT, 'definition name'),
                            'definitions' =>
                    new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'definition' => new external_value(PARAM_TEXT, 'definition name'),
                            'criteria' => new external_multiple_structure(
                                            new external_single_structure(
                                    array(
                                        'description' => new external_value(PARAM_TEXT, 'criterion description'),
                                        'levels' => new external_multiple_structure(
                                                new external_single_structure(
                                                    array(
                                                        'definition' => new external_value(PARAM_RAW, 'level definition'),
                                                        'score' => new external_value(PARAM_FLOAT, 'grademax'),
                                                    )
                                                )
                                            )
                                    )
                                )
                            )
                        )
                    )
                )
                )
                );
    }

    public static function get_rubrics($id) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_rubrics_parameters(), array( 'id' => $id));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_rubrics ($id);


        return $return;
    }

    /* get_grade_user_report */
    public static function get_grade_user_report_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_grade_user_report_returns() {
         return 
                new external_single_structure(
                        array(
                            'config' =>
                                new external_single_structure(
                                    array(
                                            'showlettergrade' => new external_value(PARAM_INT, 'showlettergrade'),
                                        )
                                    ),
                            'data' =>
                                 new external_multiple_structure(
                                    new external_single_structure(
                                        array(
                                            'fullname' => new external_value(PARAM_TEXT, 'item name'),
                                            'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                                            'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                                            'letter' => new external_value(PARAM_TEXT, 'grade letter'),
                                            'items' => new external_multiple_structure(
                                                    new external_single_structure(
                                                        array(
                                                            'name' => new external_value(PARAM_TEXT, 'item name'),
                                                            'due' => new external_value(PARAM_INT, 'due date'),
                                                            'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                                                            'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                                                            'feedback' => new external_value(PARAM_RAW, 'feedback'),
                                                            'letter' => new external_value(PARAM_TEXT, 'grade letter'),
                                                        )
                                                    )
                                                )
                                        )
                                    )
                                )
                        )
                    );
    }

    public static function get_grade_user_report($id, $username) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_grade_user_report_parameters(), array('id'=>$id, 'username' => $username));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_grade_user_report ($id, $username);


        return $return;
    }


    /* get_my_grade_user_report */
    public static function get_my_grade_user_report_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_my_grade_user_report_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'fullname' => new external_value(PARAM_TEXT, 'fullname'),
                        'remoteid' => new external_value(PARAM_INT, 'course id'),
						'grades' => 
                            new external_single_structure(
                            array(
                                'config' =>
                                    new external_single_structure(
                                        array(
                                                'showlettergrade' => new external_value(PARAM_INT, 'showlettergrade'),
                                            )
                                        ),
                                'data' =>
                                     new external_multiple_structure(
                                        new external_single_structure(
                                            array(
                                                'fullname' => new external_value(PARAM_TEXT, 'item name'),
                                                'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                                                'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                                                'letter' => new external_value(PARAM_TEXT, 'grade letter'),
                                                'items' => new external_multiple_structure(
                                                        new external_single_structure(
                                                            array(
                                                                'name' => new external_value(PARAM_TEXT, 'item name'),
                                                                'due' => new external_value(PARAM_INT, 'due date'),
                                                                'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                                                                'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                                                                'feedback' => new external_value(PARAM_RAW, 'feedback'),
                                                                'letter' => new external_value(PARAM_TEXT, 'grade letter'),
                                                            )
                                                        )
                                                    )
                                            )
                                        )
                                    )
                            )
                        )
                    )
                )
            );
    }

    public static function get_my_grade_user_report($username) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_my_grade_user_report_parameters(), array('username' => $username));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_my_grade_user_report ($username);


        return $return;
    }


    /* teacher_get_course_grades */
    public static function teacher_get_course_grades_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'course id'),
                'search' => new external_value(PARAM_TEXT, 'search text'),
                'limitfrom' => new external_value(PARAM_TEXT, 'limit from'),
                'limitnum' => new external_value(PARAM_TEXT, 'limit number'),
                'completed' => new external_value(PARAM_TEXT, 'completed',false),
            )
        );
    }

    public static function teacher_get_course_grades_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'firstname' => new external_value(PARAM_TEXT, 'firstname'),
                    'lastname' => new external_value(PARAM_TEXT, 'lastname'),
                    'username' => new external_value(PARAM_TEXT, 'username'),
                    'group' => new external_value(PARAM_TEXT, 'group name'),
                    'id' => new external_value(PARAM_INT, 'user id'),
                    'overallgrade' => new external_value(PARAM_FLOAT, 'overallgrade', false),
                    'overallgradeletter' => new external_value(PARAM_TEXT, 'overallgradeletter', false),
                    'grades' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'fullname' => new external_value(PARAM_TEXT, 'item name'),
                                'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                                'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                                'finalgradeletters' => new external_value(PARAM_TEXT, 'final grade letter'),
                                'hidden' => new external_value(PARAM_INT, 'hidden'),
                                'gradeoffacitator' => new external_value(PARAM_BOOL, 'gradeoffacitator', false),
                                'items' => new external_multiple_structure(
                                    new external_single_structure(
                                        array(
                                            'name' => new external_value(PARAM_TEXT, 'item name'),
                                            'due' => new external_value(PARAM_INT, 'due date'),
                                            'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                                            'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                                            'feedback' => new external_value(PARAM_RAW, 'feedback'),
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );
    }

    public static function teacher_get_course_grades($id, $search, $limitfrom, $limitnum,$completed) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::teacher_get_course_grades_parameters(), array('id'=>$id, 'search' => $search, 'limitfrom' => $limitfrom, 'limitnum' => $limitnum,'completed'=>$completed));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->teacher_get_course_grades ($id, $search, $limitfrom, $limitnum,$completed);
        return $return;
    }

    /* get_group_members */
    public static function get_group_members_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'group id'),
                            'search' => new external_value(PARAM_TEXT, 'search'),
                        )
        );
    }

    public static function get_group_members_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'record id'),
                    'firstname' => new external_value(PARAM_TEXT, 'first name'),
                    'lastname' => new external_value(PARAM_TEXT, 'last name'),
                    'username' => new external_value(PARAM_TEXT, 'username'),
                )
            )
        );
    }

    public static function get_group_members($id, $search) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_group_members_parameters(), array('id'=>$id, 'search' => $search));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_group_members ($id, $search);

        return $return;
    }


    /* get_course_groups */
    public static function get_course_groups_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function get_course_groups_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'group record id'),
                    'name' => new external_value(PARAM_TEXT, 'group name'),
                    'description' => new external_value(PARAM_RAW, 'description'),
                )
            )
        );
    }

    public static function get_course_groups($id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_groups_parameters(), array('id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_groups ($id);

        return $return;
    }

    /* teacher_get_group_grades */
    public static function teacher_get_group_grades_parameters() {
        return new external_function_parameters(
                        array(
                            'course_id' => new external_value(PARAM_INT, 'course id'),
                            'group_id' => new external_value(PARAM_INT, 'group id'),
                            'search' => new external_value(PARAM_TEXT, 'search'),
                        )
        );
    }

    public static function teacher_get_group_grades_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'firstname' => new external_value(PARAM_TEXT, 'firstname'),
                        'lastname' => new external_value(PARAM_TEXT, 'lastname'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'group' => new external_value(PARAM_TEXT, 'group name'),
                        'id' => new external_value(PARAM_INT, 'user id'),
                        'grades' => new external_multiple_structure(
                                        new external_single_structure(
                                array(
                                    'fullname' => new external_value(PARAM_TEXT, 'item name'),
                                    'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                                    'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                                    'items' => new external_multiple_structure(
                                            new external_single_structure(
                                                array(
                                                    'name' => new external_value(PARAM_TEXT, 'item name'),
                                                    'due' => new external_value(PARAM_INT, 'due date'),
                                                    'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                                                    'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                                                    'feedback' => new external_value(PARAM_RAW, 'feedback'),
                                                )
                                            )
                                        )
                                )
                            )
                        )
                    )
                )
            );
    }

    public static function teacher_get_group_grades($course_id, $group_id, $search) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::teacher_get_group_grades_parameters(), array('course_id'=>$course_id, 'group_id' => $group_id, 'search' => $search));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->teacher_get_group_grades ($course_id, $group_id, $search);


        return $return;
    }


    /* create_course */
    public static function create_course_parameters() {
        return new external_function_parameters(
                        array(
                            'course' =>     new external_single_structure(
                                            array(
                                                'fullname' => new external_value(PARAM_TEXT, 'fullname'),
                                                'shortname' => new external_value(PARAM_TEXT, 'shortname'),
                                                'summary' => new external_value(PARAM_RAW, 'summary'),
                                                'course_lang' => new external_value(PARAM_TEXT, 'lang'),
                                                'startdate' => new external_value(PARAM_INT, 'startdate'),
                                                'timemodified' => new external_value(PARAM_INT, 'timemodified'),
                                                'duration' => new external_value(PARAM_FLOAT, 'duration'),
                                                'price' => new external_value(PARAM_FLOAT, 'price'),
                                                'idnumber' => new external_value(PARAM_TEXT, 'idnumber'),
                                                'category' => new external_value(PARAM_INT, 'cat id'),
                                                'imgName' => new external_value(PARAM_TEXT, 'image name', false),
                                                'tmpfile' => new external_value(PARAM_TEXT, 'tmp file name', false),
                                                'username' => new external_value(PARAM_TEXT, 'username'),
                                                'learningoutcomes' => new external_value(PARAM_TEXT, 'learning outcomes', false),
                                                'targetaudience' => new external_value(PARAM_TEXT, 'target audience', false),
                                                'coursesurvey' => new external_value(PARAM_INT, 'course survey', false),
                                                'facilitatedcourse' => new external_value(PARAM_INT, 'facilitated course', false),
                                                'certificatecourse' => new external_value(PARAM_INT, 'certificate course', false),
                                                'validfor' => new external_value(PARAM_INT, 'valid for', false),
                                            )
                                        )
                        )
        );
    }

    public static function create_course_returns() {
        return new external_single_structure(
            array(
                'error' => new external_value(PARAM_INT, 'error status'),
                'mes' => new external_value(PARAM_TEXT, 'message'),
                'data' => new external_value(PARAM_INT, 'course id'),
            )
        );
    }

    public static function create_course($course) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::create_course_parameters(), array('course'=>$course));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->create_course ($course);


        return $return;
    }
    
    /*
     * duplicate_course
     * 
     */
    public static function duplicate_course_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'Course id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
            )
        );
    }
    /*
     * duplicate_course
     * 
     * @param json $json_param
     * @return array courses (id and shortname only)
     * @since Create by Luyen Vu 11 Apr 2017
     */
    public static function duplicate_course($courseid, $username) {
        global $CFG, $DB;
        
        $params = self::validate_parameters(self::duplicate_course_parameters(), array('courseid' => $courseid, 'username' => $username));
        
        $auth = new auth_plugin_joomdle();
        $return = $auth->duplicate_course($courseid, $username);
        
        return $return;
    }
    
    public static function duplicate_course_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'course' => new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
//                            'category' => new external_value(PARAM_INT, 'category id'),
//                            'cat_name' => new external_value(PARAM_TEXT, 'cartegory name'),
//                            'cat_description' => new external_value(PARAM_RAW, 'category description', false),
//                            'fullname' => new external_value(PARAM_TEXT, 'course name'),
                            'shortname' => new external_value(PARAM_TEXT, 'course shortname'),
//                            'idnumber' => new external_value(PARAM_RAW, 'idnumber', false),
//                            'summary' => new external_value(PARAM_RAW, 'summary'),
//                            'startdate' => new external_value(PARAM_INT, 'start date'),
//                            'enrolperiod' => new external_value(PARAM_INT, 'enrolperiod'),
//                            'enrolmenttimestart' => new external_value(PARAM_INT, 'enrolmenttimestart', false),
//                            'enrolmenttimeend' => new external_value(PARAM_INT, 'enrolmenttimeend', false),
//                            'self_enrolment' => new external_value(PARAM_INT, 'self enrolment', false),
//                            'has_nonmember' => new external_value(PARAM_INT, 'has non-user'),
//                            'course_status' => new external_value(PARAM_TEXT, 'Course status'),
//                            'created' => new external_value(PARAM_INT, 'created', false),
//                            'modified' => new external_value(PARAM_INT, 'modified', false),
//                            'creator' => new external_value(PARAM_INT, 'creator'),
//                            'price' => new external_value(PARAM_FLOAT, 'price'),
//                            'currency' => new external_value(PARAM_TEXT, 'currency', VALUE_OPTIONAL),
//                            'duration' => new external_value(PARAM_FLOAT, 'duration'),
//                            'learning_outcomes' => new external_value(PARAM_TEXT, 'learning outcomes'),
//                            'target_audience' => new external_value(PARAM_TEXT, 'target audience'),
//                            'course_survey' => new external_value(PARAM_INT, 'course survey'),
//                            'course_facilitated' => new external_value(PARAM_INT, 'facilitated course'),
//                            'can_unenrol' => new external_value(PARAM_INT, 'user unenroled'),
//                            'fileid' => new external_value(PARAM_TEXT, 'file id', VALUE_OPTIONAL),
//                            'filetype' => new external_value(PARAM_TEXT, 'file type', VALUE_OPTIONAL),
//                            'filepath' => new external_value(PARAM_TEXT, 'file path', VALUE_OPTIONAL),
//                            'filename' => new external_value(PARAM_TEXT, 'file name', VALUE_OPTIONAL),
                        )
                )
            )
        );
    }
    
    public static function get_token_parameters() {
        return new external_function_parameters(
            array(
                'username' => new external_value(PARAM_TEXT, 'username'),
            )
        );
    }
    
    public static function get_token($username) {
        global $CFG, $DB;
        
        $params = self::validate_parameters(self::get_token_parameters(), array('username' => $username));
        
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_token($username);
        
        return $return;
    }
    
    public static function get_token_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'token' => new external_value(PARAM_TEXT, 'course shortname'),
            )
        );
    }
    
    /*
     * Added API create categories and assign user into categories
     * added by KV Team
     */
    public static function create_categories($categories) {
        global $CFG, $DB;
        $params = self::validate_parameters(self::create_categories_parameters(), array('categories'=>$categories));
        $auth = new auth_plugin_joomdle();
        $return = $auth->create_categories($categories);
        return $return;
    }
    public static function create_categories_parameters() {
        return new external_function_parameters(
            array(
                'categories' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'name' => new external_value(PARAM_TEXT, 'new category name'),
                                'parent' => new external_value(PARAM_INT,
                                        'the parent category id inside which the new category will be created
                                         - set to 0 for a root category',
                                        VALUE_DEFAULT, 0),
                                'idnumber' => new external_value(PARAM_RAW,
                                        'the new category idnumber'),
                                'description' => new external_value(PARAM_RAW,
                                        'the new category description'),
                                'descriptionformat' => new external_format_value('description', VALUE_DEFAULT),
                                'username' => new external_value(PARAM_TEXT,
                                        'username of user register learning provider'),
                        )
                    )
                )
            )
        );
    }
    
    public static function create_categories_returns() {
        return new external_single_structure(
        array(
            'status' => new external_value(PARAM_BOOL, 'status'),
            'categories' => new external_multiple_structure(
                    new external_single_structure(
                    array(
                    'id' => new external_value(PARAM_INT, 'new category id'),
                    'name' => new external_value(PARAM_TEXT, 'new category name'),
                )
            )
        )
                                            ));
    }


    /* update_course */
    public static function update_course_parameters() {
        return new external_function_parameters(
                        array(
                            'course' =>     new external_single_structure(
                                            array(
                                                'id' => new external_value(PARAM_INT, 'id'),
                                                'fullname' => new external_value(PARAM_TEXT, 'fullname'),
                                                'shortname' => new external_value(PARAM_TEXT, 'shortname'),
                                                'summary' => new external_value(PARAM_RAW, 'summary'),
                                                'course_lang' => new external_value(PARAM_TEXT, 'lang'),
                                                'startdate' => new external_value(PARAM_INT, 'startdate'),
                                                'timemodified' => new external_value(PARAM_INT, 'timemodified'),
                                                'idnumber' => new external_value(PARAM_TEXT, 'idnumber'),
                                                'category' => new external_value(PARAM_INT, 'cat id'),
                                                'duration' => new external_value(PARAM_FLOAT, 'duration'),
                                                'price' => new external_value(PARAM_FLOAT, 'price'),
                                                'tmpfile' => new external_value(PARAM_TEXT, 'tmp file name', false),
                                                'imgName' => new external_value(PARAM_TEXT, 'temp img url', false),
                                                'username' => new external_value(PARAM_TEXT, 'temp img url'),
                                                'learningoutcomes' => new external_value(PARAM_TEXT, 'learning outcomes', false),
                                                'targetaudience' => new external_value(PARAM_TEXT, 'target audience', false),
                                                'coursesurvey' => new external_value(PARAM_INT, 'course survey', false),
                                                'facilitatedcourse' => new external_value(PARAM_INT, 'facilitated course', false),
                                                'certificatecourse' => new external_value(PARAM_INT, 'certificate course', false),
                                                'validfor' => new external_value(PARAM_INT, 'valid for', false),
                                            )
                                        )
                        )
        );
    }

    public static function update_course_returns() {
        return new external_single_structure(
            array(
                'error' => new external_value(PARAM_INT, 'error status'),
                'mes' => new external_value(PARAM_TEXT, 'message'),
                'data' => new external_value(PARAM_INT, 'course id'),
            )
        );
    }

    public static function update_course($course) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::update_course_parameters(), array('course'=>$course));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->update_course ($course);


        return $return;
    }

    /* remove_course */
    public static function remove_course_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
            )
        );
    }

    public static function remove_course_returns() {
        return
            new external_single_structure(
                array(
                    'res' => new external_value(PARAM_INT, 'result'),
                )
            );
    }

    public static function remove_course($courseid, $username) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::remove_course_parameters(), array('courseid'=>$courseid, 'username'=>$username));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->remove_course ($courseid, $username);

        return $return;
    }

    /* create_scorm_activity */
    public static function create_scorm_activity_parameters() {
        return new external_function_parameters(
            array(
                'scorm' =>  new external_single_structure(
                    array(
                        'act' => new external_value(PARAM_INT, 'act'),
                        'courseid' => new external_value(PARAM_INT, 'id'),
                        'mid' => new external_value(PARAM_INT, 'module id', false),
                        'sname' => new external_value(PARAM_TEXT, 'scorm name', false),
                        'section' => new external_value(PARAM_INT, 'section', false),
                        'des' => new external_value(PARAM_RAW, 'summary', false),
                        'fname' => new external_value(PARAM_TEXT, 'file name', false),
                        'tmpfile' => new external_value(PARAM_TEXT, 'tmp file name', false),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                    )
                ),
                'sectionid'=> new external_value(PARAM_INT, 'section id', false),
            )
        );
    }

    public static function create_scorm_activity_returns() {
        return
            new external_single_structure(
                array(
                    'error' => new external_value(PARAM_INT, 'result'),
                    'sid' => new external_value(PARAM_INT, 'scorm id', false),
                    'courseid' => new external_value(PARAM_INT, 'course id', false),
                    'sname' => new external_value(PARAM_TEXT, 'scorm name', false),
                    'scormtype' => new external_value(PARAM_TEXT, 'scorm type', false),
                    'fname' => new external_value(PARAM_TEXT, 'file name', false),
                    'des' => new external_value(PARAM_RAW, 'des', false),
                    'desformat' => new external_value(PARAM_INT, 'des format', false),

                    'mes' => new external_value(PARAM_TEXT, 'message', false),
                    'data' => new external_value(PARAM_RAW, 'data', false),
                )
        );
    }

    public static function create_scorm_activity($scorm, $sectionid) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::create_scorm_activity_parameters(), array('scorm'=>$scorm, 'sectionid'=>$sectionid));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->create_scorm_activity ($scorm, $sectionid);

        return $return;
    }
    
    public static function create_scorm_platform($username, $courseid, $act, $scorm) {
        global $CFG, $DB;
        $params = self::validate_parameters(self::create_scorm_platform_parameters(), array('username'=>$username, 'courseid'=>$courseid, 'act'=>$act, 'scorm'=>$scorm));
        $auth = new auth_plugin_joomdle();
        $return = $auth->create_scorm_platform($username, $courseid, $act, $scorm);
        return $return;
    }
    
    public static function create_scorm_platform_parameters() {
        return new external_function_parameters(
                    array(
                        'username' => new external_value(PARAM_TEXT,'username'),
                        'courseid' => new external_value(PARAM_INT,'course id'),
                        'act' => new external_value(PARAM_INT,'action: 1- add, 2- Edit, 3- delete'),
                        'scorm' => new external_value(PARAM_RAW, 'scorm content'),
                    )
                );
    }
    
    public static function create_scorm_platform_returns() {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL, 'status'),
                        'message' => new external_value(PARAM_TEXT, 'message'),
                        'sid' => new external_value(PARAM_INT, 'scorm id'),
                    )
                );
    }
    
    //create assignment activity
    public static function create_assignment_activity_parameters() {
        return new external_function_parameters(
            array(
                'act' => new external_value(PARAM_INT, 'action'),
                'courseid' => new external_value(PARAM_INT, 'Course id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
                'p' =>  new external_single_structure(
                    array(
                        'aid' => new external_value(PARAM_INT, 'question id', false),
                        'name' => new external_value(PARAM_RAW, 'question name', false),
                        'des' => new external_value(PARAM_RAW, 'question text', false),
                        'fname' => new external_value(PARAM_RAW, 'file name', false),
                        'itemid' => new external_value(PARAM_INT, 'itemid', false),
                        'duedate' => new external_value(PARAM_RAW, 'duedate', false),
                        'timezoneOffset' => new external_value(PARAM_FLOAT, 'timezoneOffset', false),
                    )
                ),
                'sectionid'=> new external_value(PARAM_INT, 'section id', false),
            )
        );
    }

    public static function create_assignment_activity_returns() {
        return
            new external_single_structure(
                array(
                    'error' => new external_value(PARAM_RAW, 'error status'),
                    'aid' => new external_value(PARAM_INT, 'scorm id', false),
                    'courseid' => new external_value(PARAM_INT, 'course id', false),
                    'name' => new external_value(PARAM_TEXT, 'scorm name', false),
                    'des' => new external_value(PARAM_RAW, 'des', false),
                    'desformat' => new external_value(PARAM_INT, 'des format', false),
                    'duedate' => new external_value(PARAM_INT, 'duedate', false),
                    'params' => new external_value(PARAM_RAW, 'params', false),

                    'mes' => new external_value(PARAM_TEXT, 'message', false),
                    'data' => new external_value(PARAM_RAW, 'data', false),
                )
            );
    }

    public static function create_assignment_activity($act, $courseid, $username, $p, $sectionid) { //Don't forget to set it as static
        global $CFG, $DB;

        $params = self::validate_parameters(self::create_assignment_activity_parameters(), array('act'=>$act, 'courseid'=>$courseid, 'username'=>$username, 'p'=>$p, 'sectionid'=>$sectionid));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->create_assignment_activity($act, $courseid, $username, $p, $sectionid);

        return $return;
    }

    //student submission assignment activity
    public static function student_submission_assign_parameters() {
        return new external_function_parameters(
            array(
                'course_id' => new external_value(PARAM_INT, 'Course id'),
                'assign_id' => new external_value(PARAM_INT, 'Assign id'),
                'username' => new external_value(PARAM_TEXT,'username'),
                'file' =>  new external_single_structure(
                    array(
                        'fname' => new external_value(PARAM_RAW, 'file name', false),
                        'itemid' => new external_value(PARAM_INT, 'itemid', false),
                        'duedate' => new external_value(PARAM_RAW, 'duedate', false),
                        'timezoneOffset' => new external_value(PARAM_FLOAT, 'timezoneOffset', false),
                    )
                ),
            )
        );
    }

    public static function student_submission_assign_returns() {
        return
            new external_single_structure(
                array(
                    'message' => new external_value(PARAM_RAW, 'message action'),
                )
            );
    }

    public static function student_submission_assign($course_id, $assign_id, $username, $file) { //Don't forget to set it as static
        global $CFG, $DB;

        $params = self::validate_parameters(self::student_submission_assign_parameters(), array('course_id'=>$course_id,'assign_id'=>$assign_id,'username'=>$username, 'file'=>$file));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->student_submission_assign($course_id, $assign_id, $username, $file);

        return $return;
    }

    //create page activity
    public static function create_page_activity_parameters() {
        return new external_function_parameters(
            array(
                'act' => new external_value(PARAM_INT, 'action'),
                'courseid' => new external_value(PARAM_INT, 'Course id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
                'p' =>  new external_single_structure(
                    array(
                        'pid' => new external_value(PARAM_INT, 'question id', false),
                        'content' => new external_value(PARAM_RAW, 'page content', false),
                        'contentArr' => new external_value(PARAM_RAW, 'json page content', false),
                        'name' => new external_value(PARAM_RAW, 'question name', false),
                        'des' => new external_value(PARAM_RAW, 'question text', false),
                        'fname' => new external_value(PARAM_RAW, 'file name', false),
                        'itemid' => new external_value(PARAM_INT, 'itemid', false),
                    )
                ),
                'sectionid'=> new external_value(PARAM_INT, 'section id', false),
            )
        );
    }

    public static function create_page_activity_returns() {
        return
            new external_single_structure(
                array(
                    'error' => new external_value(PARAM_RAW, 'error status'),
                    'pid' => new external_value(PARAM_INT, 'scorm id', false),
                    'courseid' => new external_value(PARAM_INT, 'course id', false),
                    'name' => new external_value(PARAM_TEXT, 'scorm name', false),
                    'des' => new external_value(PARAM_RAW, 'des', false),
                    'desformat' => new external_value(PARAM_INT, 'des format', false),
                    'content' => new external_value(PARAM_RAW, 'content', false),
                    'contentformat' => new external_value(PARAM_INT, 'content format', false),
                    'params' => new external_value(PARAM_RAW, 'params', false),

                    'mes' => new external_value(PARAM_TEXT, 'message', false),
                    'data' => new external_value(PARAM_RAW, 'data', false),
                )
            );
    }

    public static function create_page_activity($act, $courseid, $username, $p, $sectionid) { //Don't forget to set it as static
        global $CFG, $DB;

        $params = self::validate_parameters(self::create_page_activity_parameters(), array('act'=>$act, 'courseid'=>$courseid, 'username'=>$username, 'p'=>$p, 'sectionid'=>$sectionid));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->create_page_activity($act, $courseid, $username, $p, $sectionid);

        return $return;
    }
    
    // save question questionnaire save_question_questionnaire
    public static function create_and_add_question_for_feedback($data_feedback, $data_question,$courseid,$username) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::create_and_add_question_for_feedback_parameters(), array('data_feedback'=> $data_feedback,'data_question'=> $data_question, 'courseid'=>$courseid,'username'=>$username));
        $auth = new auth_plugin_joomdle();
        $return = $auth->create_and_add_question_for_feedback($data_feedback, $data_question,$courseid,$username);
        return $return;
    }
    public static function create_and_add_question_for_feedback_parameters() {
        return new external_function_parameters(
            array(
                'data_feedback' => new external_value(PARAM_RAW, 'data title and description feedback'),
                'data_question' => new external_value(PARAM_RAW, 'data question of feedback'),
                'courseid' => new external_value(PARAM_INT, 'course id'),
                'username'=> new external_value(PARAM_TEXT, 'user name'),
            )
        );
    }
    public static function create_and_add_question_for_feedback_returns() {
        return new external_single_structure(
            array(
                'status'=> new external_value(PARAM_INT, 'error'),
                'message'=> new external_value(PARAM_TEXT, 'message'),
            )
        );
    }
    /* Create activity */
    
    public static function create_activity_feedback($activity) {
        //$username, $courseid, $module, $title, $description, $content
        global $DB, $CFG;
        $params = self::validate_parameters(self::create_activity_feedback_parameters(), array('activity'=>$activity));
        $auth = new auth_plugin_joomdle();
        $return = $auth->create_activity_feedback($activity);
        return $return;
    }
    
    public static function create_activity_feedback_parameters() {
        return new external_function_parameters(
                    array(
                        'activity' => new external_single_structure(
                                array(
                                    'username' => new external_value(PARAM_TEXT, 'username'),
                                    'courseid' => new external_value(PARAM_INT, 'course id'),
                                    'module'   => new external_value(PARAM_TEXT, 'module name'),
                                    'title'   => new external_value(PARAM_TEXT, 'title module'),
                                    'description'   => new external_value(PARAM_RAW, 'description'),
                                    'content'   => new external_value(PARAM_RAW, 'content'),
                                )
                                )
                    )
                );
    }
    public static function create_activity_feedback_returns() {
        return new external_single_structure(
                  array(
                      'status' => new external_value(PARAM_BOOL, 'status'),
                      'message' => new external_value(PARAM_TEXT, 'message'),
                      'feedbackid' => new external_value(PARAM_INT, 'id'),
                  )  
                );
    }
    /* change owner course*/
     public static function change_owner_course_parameters() {
        return new external_function_parameters(
                    array(
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'ownerblnname' => new external_value(PARAM_TEXT, 'ownerblnname'),
                    )
                );
    }
    public static function change_owner_course_returns() {
        return new external_single_structure(
                array(
                     'status' => new external_value(PARAM_BOOL, 'status'),
                     'message' => new external_value(PARAM_TEXT, 'message'),
                )
            );
    }
    
    public static function change_owner_course($username,$ownerblnname) {
        
        global $DB;
        $params = self::validate_parameters(self::change_owner_course_parameters(), array('username'=>$username, 'ownerblnname'=>$ownerblnname));
        $auth = new auth_plugin_joomdle();
        $return = $auth->change_owner_course($username, $ownerblnname);
        return $return;
    }
    /* add_user_role */
    public static function add_user_role_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'roleid' => new external_value(PARAM_INT, 'role id'),
                        )
        );
    }

    public static function add_user_role_returns() {
        return new  external_value(PARAM_INT, 'user created');
    }

    public static function add_user_role($username, $id, $roleid) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::add_user_role_parameters(), array('username'=>$username, 'id' => $id, 'roleid' => $roleid));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->add_user_role ($username, $id, $roleid);

        return $id;
    }

    /* get_course_parents */
    public static function get_course_parents_parameters() {
        return new external_function_parameters(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function get_course_parents_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'firstname' => new external_value(PARAM_TEXT, 'firstname'),
                        'lastname' => new external_value(PARAM_TEXT, 'lastname'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                    )
                )
            );
    }

    public static function get_course_parents($id) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_course_parents_parameters(), array('id'=>$id));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_parents ($id);


        return $return;
    }

    /* get_all_parents */
    public static function get_all_parents_parameters() {
        return new external_function_parameters(
                        array(
                        )
        );
    }

    public static function get_all_parents_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'firstname' => new external_value(PARAM_TEXT, 'firstname'),
                        'lastname' => new external_value(PARAM_TEXT, 'lastname'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                    )
                )
            );
    }

    public static function get_all_parents() {
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_all_parents_parameters(), array());

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_all_parents ();


        return $return;
    }


    /* remove_cohort_member */
    public static function remove_cohort_member_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'cohort_id' => new external_value(PARAM_INT, 'cohort id'),
                        )
        );
    }

    public static function remove_cohort_member_returns() {
        return new  external_value(PARAM_INT, 'user added');
    }

    public static function remove_cohort_member($username, $cohort_id) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::remove_cohort_member_parameters(), array('username'=>$username, 'cohort_id' => $cohort_id));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->remove_cohort_member ($username, $cohort_id);

        return $id;
    }

    /* multiple_add_cohort_member */
    public static function multiple_add_cohort_member_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'cohorts' => new external_multiple_structure(
                                    new external_single_structure(
                                        array(
                                            'id' => new external_value(PARAM_INT, 'cohort id'),
                                        )
                                    )
                                ),
                        )
        );
    }

    public static function multiple_add_cohort_member_returns() {
        return new  external_value(PARAM_INT, 'user enroled');
    }

    public static function multiple_add_cohort_member($username, $cohorts) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::multiple_add_cohort_member_parameters(), array('username'=>$username, 'cohorts' => $cohorts));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->multiple_add_cohort_member ($username, $cohorts);

        return $id;
    }

    /* multiple_remove_cohort_member */
    public static function multiple_remove_cohort_member_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'cohorts' => new external_multiple_structure(
                                    new external_single_structure(
                                        array(
                                            'id' => new external_value(PARAM_INT, 'cohort id'),
                                        )
                                    )
                                ),
                        )
        );
    }

    public static function multiple_remove_cohort_member_returns() {
        return new  external_value(PARAM_INT, 'user enroled');
    }

    public static function multiple_remove_cohort_member($username, $cohorts) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::multiple_remove_cohort_member_parameters(), array('username'=>$username, 'cohorts' => $cohorts));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->multiple_remove_cohort_member ($username, $cohorts);

        return $id;
    }

    /* get_courses_and_groups */
    public static function get_courses_and_groups_parameters() {
        return new external_function_parameters(
                        array(
                        )
        );
    }

    public static function get_courses_and_groups_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                    'remoteid' => new external_value(PARAM_INT, 'course id'),
                    'fullname' => new external_value(PARAM_TEXT, 'course name'),
                    'groups' => new external_multiple_structure(
                                    new external_single_structure(
                                        array(
                                            'id' => new external_value(PARAM_INT, 'group record id'),
                                            'name' => new external_value(PARAM_TEXT, 'group name'),
                                            'description' => new external_value(PARAM_RAW, 'description'),
                                        )
                                    )
                                )
                    )
            )
        );
    }

    public static function get_courses_and_groups() {
        global $CFG, $DB;
 
      $params = self::validate_parameters(self::get_courses_and_groups_parameters(), array()); 
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->get_courses_and_groups ();

        return $id;
    }

    /* multiple_enrol_to_course_and_group */
    public static function multiple_enrol_to_course_and_group_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'courses' => new external_multiple_structure(
                                    new external_single_structure(
                                        array(
                                            'id' => new external_value(PARAM_INT, 'course id'),
                                            'group_id' => new external_value(PARAM_INT, 'group id'),
                                        )
                                    )
                                ),
                            'roleid' => new external_value(PARAM_INT, 'role id'),
                        )
        );
    }

    public static function multiple_enrol_to_course_and_group_returns() {
        return new  external_value(PARAM_INT, 'user enroled');
    }

    public static function multiple_enrol_to_course_and_group($username, $courses, $roleid) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::multiple_enrol_to_course_and_group_parameters(), array('username'=>$username, 'courses' => $courses, 'roleid' => $roleid));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->multiple_enrol_to_course_and_group ($username, $courses, $roleid);

        return $id;
    }

    /* multiple_remove_from_group */
    public static function multiple_remove_from_group_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'courses' => new external_multiple_structure(
                                    new external_single_structure(
                                        array(
                                            'id' => new external_value(PARAM_INT, 'course id'),
                                            'group_id' => new external_value(PARAM_INT, 'group id'),
                                        )
                                    )
                                ),
                        )
        );
    }

    public static function multiple_remove_from_group_returns() {
        return new  external_value(PARAM_INT, 'user enroled');
    }

    public static function multiple_remove_from_group($username, $courses) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::multiple_remove_from_group_parameters(), array('username'=>$username, 'courses' => $courses));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->multiple_remove_from_group ($username, $courses);

        return $id;
    }

    /* my_all_courses */
    public static function my_all_courses_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'Username'),
                        )
        );
    }

    public static function my_all_courses_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'group record id'),
                    'fullname' => new external_value(PARAM_TEXT, 'course name'),
                    'category' => new external_value(PARAM_INT, 'course category id'),
                )
            )
        );
    }

    public static function my_all_courses($username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::my_all_courses_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->my_all_courses ($username);

        return $return;
    }

    /* multiple_unenrol_user */
    public static function multiple_unenrol_user_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'courses' => new external_multiple_structure(
                                    new external_single_structure(
                                        array(
                                            'id' => new external_value(PARAM_INT, 'course id'),
                                        )
                                    )
                                ),
                            'progid'  =>  new external_value(PARAM_INT, 'program id', false),
                        )
        );
    }

    public static function multiple_unenrol_user_returns() {
        return new  external_value(PARAM_INT, 'user enroled');
    }

    public static function multiple_unenrol_user($username, $courses, $progid) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::multiple_unenrol_user_parameters(), array('username'=>$username, 'courses' => $courses, 'progid' => $progid));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->multiple_unenrol_user ($username, $courses, $progid);

        return $id;
    }

        /* suspend_enrolment */
    public static function unenrol_user_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
    }

    public static function unenrol_user_returns() {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL, 'status'),
                        'message' => new external_value(PARAM_TEXT, 'message'),
                    )
                );
    }

    public static function unenrol_user($username, $id) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::unenrol_user_parameters(), array('username'=>$username, 'id' => $id));

        $auth = new  auth_plugin_joomdle ();
        $id = $auth->unenrol_user ($username, $id);

        return $id;
    }


    /* get_children_grades */
    public static function get_children_grades_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_children_grades_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'name' => new external_value(PARAM_TEXT, 'name'),
                        'grades' =>
                                 new external_multiple_structure(
                                    new external_single_structure(
                                        array(
                                            'fullname' => new external_value(PARAM_TEXT, 'fullname'),
                                            'remoteid' => new external_value(PARAM_INT, 'course id'),
                                            'grades' => new external_multiple_structure(
                                                        new external_single_structure(
                                                            array (
                                                                'itemname' => new external_value(PARAM_TEXT, 'item name'),
                                                                'finalgrade' => new external_value(PARAM_TEXT, 'final grade'),
                                                            )
                                                        )
                                            ),

                                        )
                                    )
                                )
                    )
                )
            );
    }

    public static function get_children_grades($username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_children_grades_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_children_grades ($username);

        return $return;
    }


    /* get_children_grade_user_report */
    public static function get_children_grade_user_report_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function get_children_grade_user_report_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'name' => new external_value(PARAM_TEXT, 'name'),
                        'grades' =>
                                  new external_multiple_structure(
                                    new external_single_structure(
                                        array(
                                            'fullname' => new external_value(PARAM_TEXT, 'fullname'),
                                            'remoteid' => new external_value(PARAM_INT, 'course id'),
											'grades' => 
                                                new external_single_structure(
                                                array(
                                                    'config' =>
                                                        new external_single_structure(
                                                            array(
                                                                    'showlettergrade' => new external_value(PARAM_INT, 'showlettergrade'),
                                                                )
                                                            ),
                                                    'data' =>
                                                         new external_multiple_structure(
                                                            new external_single_structure(
                                                                array(
                                                                    'fullname' => new external_value(PARAM_TEXT, 'item name'),
                                                                    'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                                                                    'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                                                                    'letter' => new external_value(PARAM_TEXT, 'grade letter'),
                                                                    'items' => new external_multiple_structure(
                                                                            new external_single_structure(
                                                                                array(
                                                                                    'name' => new external_value(PARAM_TEXT, 'item name'),
                                                                                    'due' => new external_value(PARAM_INT, 'due date'),
                                                                                    'grademax' => new external_value(PARAM_FLOAT, 'grademax'),
                                                                                    'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                                                                                    'feedback' => new external_value(PARAM_RAW, 'feedback'),
                                                                                    'letter' => new external_value(PARAM_TEXT, 'grade letter'),
                                                                                )
                                                                            )
                                                                        )
                                                                )
                                                            )
                                                        )
                                                )
                                            )
                                        )
                                    )
                                )

                    )
                )
            );
    }

    public static function get_children_grade_user_report($username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_children_grade_user_report_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_children_grade_user_report ($username);

        return $return;
    }

  /* enrol_user_with_start_date */
    public static function enrol_user_with_start_date_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'roleid' => new external_value(PARAM_INT, 'role id'),
                            'start_date' => new external_value(PARAM_INT, 'start_date'),
                        )
        );
    }

    public static function enrol_user_with_start_date_returns() {
        return new  external_value(PARAM_INT, 'user created');
    }

    public static function enrol_user_with_start_date($username, $id, $roleid, $start_date) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::enrol_user_with_start_date_parameters(), array('username'=>$username, 'id' => $id, 'roleid' => $roleid, 'start_date' => $start_date));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->enrol_user ($username, $id, $roleid, $start_date);

        return $id;
    }

    /* remove_user_role */
    public static function remove_user_role_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'roleid' => new external_value(PARAM_INT, 'role id'),
                            'action' => new external_value(PARAM_INT, 'action',false),
                        )
        );
    }

    public static function remove_user_role_returns() {
        return new  external_value(PARAM_INT, 'user created');
    }

    public static function remove_user_role($username, $id, $roleid,$action) { 
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::remove_user_role_parameters(), array('username'=>$username, 'id' => $id, 'roleid' => $roleid,'action' =>$action));
 
        $auth = new  auth_plugin_joomdle ();
		$id = $auth->remove_user_role ($username, $id, $roleid,$action);

        return $id;
    }

    /* get_themes */
    public static function get_themes_parameters() {
        return new external_function_parameters(
                        array(
                        )
        );
    }

    public static function get_themes_returns() {
         return new external_multiple_structure(
                new external_single_structure(
                    array(
                        'name' => new external_value(PARAM_TEXT, 'name'),
                    )
                )
            );
    }

    public static function get_themes() {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_themes_parameters(), array());
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_themes ();


        return $return;
    }

    /* list_courses_scorm */
    public static function list_courses_scorm_parameters() {
        return new external_function_parameters(
                        array(
                            'enrollable_only' => new external_value(PARAM_INT, 'Return only enrollable courses'),
                            'sortby' => new external_value(PARAM_TEXT, 'Order field'),
                            'guest' => new external_value(PARAM_INT, 'Return only courses for guests'),
                            'username' => new external_value(PARAM_TEXT, 'username'),
                        )
        );
    }

    public static function list_courses_scorm_returns() {
         return new external_multiple_structure(
            new external_single_structure(
                array(
                    'remoteid' => new external_value(PARAM_INT, 'course id'),
                    'cat_id' => new external_value(PARAM_INT, 'category id'),
                    'cat_name' => new external_value(PARAM_TEXT, 'cartegory name'),
                    'cat_description' => new external_value(PARAM_RAW, 'category description'),
                    'sortorder' => new external_value(PARAM_TEXT, 'sortorder'),
                    'fullname' => new external_value(PARAM_TEXT, 'course name'),
                    'shortname' => new external_value(PARAM_TEXT, 'course shortname'),
                    'idnumber' => new external_value(PARAM_RAW, 'idnumber'),
                    'summary' => new external_value(PARAM_RAW, 'summary'),
                    'startdate' => new external_value(PARAM_INT, 'start date'),
                    'created' => new external_value(PARAM_INT, 'created'),
                    'modified' => new external_value(PARAM_INT, 'modified'), 
                    'cost' => new external_value(PARAM_FLOAT, 'cost', VALUE_OPTIONAL),
                    'currency' => new external_value(PARAM_TEXT, 'currency', VALUE_OPTIONAL),
                    'self_enrolment' => new external_value(PARAM_INT, 'self enrollable'), 
                    'enroled' => new external_value(PARAM_INT, 'user enroled'), 
                    'in_enrol_date' => new external_value(PARAM_BOOL, 'in enrol date'), 
                    'guest' => new external_value(PARAM_INT, 'guest access'), 
                    'scorm_data' =>
                            new external_single_structure(
                                array(
                                    'start_time' => new external_value(PARAM_INT, 'start time'),
                                    'total_time' => new external_value(PARAM_TEXT, 'total time'),
                                    'lesson_status' => new external_value(PARAM_TEXT, 'lesson status'),
                                    'score' => new external_value(PARAM_FLOAT, 'raw score'),
                                )
                            )
                )
            )
        );
    }

    public static function list_courses_scorm($enrollable_only, $sortby, $guest, $username) { //Don't forget to set it as static
        global $CFG, $DB;
 
      $params = self::validate_parameters(self::list_courses_scorm_parameters(), array('enrollable_only'=>$enrollable_only, 'sortby' => $sortby,'guest' => $guest, 'username' => $username)); 
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->list_courses_scorm ($enrollable_only, $sortby, $guest, $username);

        return $id;
    }

    /* create_moodle_only_user */
    public static function create_moodle_only_user_parameters() {
        return new external_function_parameters(
                        array(
                            'user_data' =>     new external_single_structure(
                                            array(
                                                'username' => new external_value(PARAM_TEXT, 'username'),
                                                'firstname' => new external_value(PARAM_TEXT, 'fistname'),
                                                'lastname' => new external_value(PARAM_TEXT, 'lastname'),
                                                'email' => new external_value(PARAM_RAW, 'email'),
                                                'password' => new external_value(PARAM_TEXT, 'password'),
                                                'city' => new external_value(PARAM_TEXT, 'city'),
                                                'country' => new external_value(PARAM_TEXT, 'country'),
                                            )
                                        )
                        )
        );
    }

    public static function create_moodle_only_user_returns() {
                return new  external_value(PARAM_INT, 'user id');
    }

    public static function create_moodle_only_user($user_data) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::create_moodle_only_user_parameters(), array('user_data'=>$user_data));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->create_moodle_only_user ($user_data);


        return $return;
    }

  /* enrol_user_with_start_and_end_date */
    public static function enrol_user_with_start_and_end_date_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'roleid' => new external_value(PARAM_INT, 'role id'),
                            'start_date' => new external_value(PARAM_INT, 'start_date'),
                            'end_date' => new external_value(PARAM_INT, 'end_date'),
                        )
        );
    }

    public static function enrol_user_with_start_and_end_date_returns() {
        return new  external_value(PARAM_INT, 'user created');
    }

    public static function enrol_user_with_start_and_end_date($username, $id, $roleid, $start_date, $end_date) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::enrol_user_with_start_and_end_date_parameters(), array('username'=>$username, 'id' => $id, 'roleid' => $roleid, 'start_date' => $start_date, 'end_date' => $end_date));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->enrol_user ($username, $id, $roleid, $start_date, $end_date);

        return $id;
    }

    /* update_course_enrolments_dates */
    public static function update_course_enrolments_dates_parameters() {
        return new external_function_parameters(
                        array(
                            'course_id' => new external_value(PARAM_INT, 'course id'),
                            'start_date' => new external_value(PARAM_INT, 'start_date'),
                            'end_date' => new external_value(PARAM_INT, 'end_date'),
                        )
        );
    }

    public static function update_course_enrolments_dates_returns() {
        return new  external_value(PARAM_INT, 'user created');
    }

    public static function update_course_enrolments_dates($course_id, $start_date, $end_date) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::update_course_enrolments_dates_parameters(), array('course_id' => $course_id, 'start_date' => $start_date, 'end_date' => $end_date));
 
        $auth = new  auth_plugin_joomdle ();
        $id = $auth->update_course_enrolments_dates ($course_id, $start_date, $end_date);

        return $id;
    }

    /* get_course_validity */
    public static function get_course_validity_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'id' => new external_value(PARAM_INT, 'course id'),
                        )
        );
}

    public static function get_course_validity_returns() {
        return new  external_value(PARAM_TEXT, 'Course validity');
    }

    public static function get_course_validity($username, $id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_validity_parameters(), array('username' => $username, 'id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_validity($username, $id);

        return $return;
    }

    //get_user_role
    public static function get_user_role_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'course id'),
                'username' => new external_value(PARAM_TEXT, 'username', false),
            )
        );
    }
    public static function get_user_role_returns () {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'roles' => new external_multiple_structure(
                             new external_single_structure(
                                array(
                                    'username' => new external_value(PARAM_TEXT, 'username'),
                                    'role' => new external_value(PARAM_RAW, 'json role data'),
                                    'hasPermission' => new external_value(PARAM_INT, '1 yes 0 no'),
                                )
                            )
                        )
                    )
                );
    }
    public static function get_user_role($id, $username) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::get_user_role_parameters(), array('id'=> $id, 'username'=>$username));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_user_role($id, $username);
        return $return;
    }
    
    //get_user_category_role
    public static function get_user_category_role_parameters() {
        return new external_function_parameters(
                    array(
                        'categoryid' => new external_value(PARAM_INT, 'categoryid'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'all' => new external_value(PARAM_INT, 'get roles of all members or not', false),
                    )
                );
    }
    public static function get_user_category_role_returns() {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL, 'status'),
                        'message' => new external_value(PARAM_TEXT, 'message'),
                        'name' => new external_value(PARAM_TEXT, 'category name', false),
                        'role' => new external_multiple_structure(
                                new external_single_structure(
                                    array(
                                        'roleid' => new external_value(PARAM_INT, 'roleid'),
                                        'rolename' => new external_value(PARAM_TEXT, 'role name'),
                                    )
                                    )
                                ),
                        'roles' => new external_value(PARAM_RAW, 'roles', false),
                    )
                );
    }
    public static function get_user_category_role($categoryid, $username, $all) {
        global $DB;
        $params = self::validate_parameters(self::get_user_category_role_parameters(), array('categoryid'=>$categoryid, 'username' => $username, 'all' => $all));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_user_category_role($categoryid, $username, $all);
        return $return;
    }

    //set User Role
    public static function set_user_role_parameters() {
        return new external_function_parameters(
            array(
                'act' => new external_value(PARAM_INT, 'action'),
                'courseid' => new external_value(PARAM_INT, 'course id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
                'data' => new external_value(PARAM_RAW, 'data'),
            )
        );
    }
    public static function set_user_role_returns () {
        return
            new external_single_structure(
                array(
                    'status' => new external_value(PARAM_BOOL, 'status'),
                    'message' => new external_value(PARAM_TEXT, 'message'),
                )
        );
    }
    public static function set_user_role($act, $courseid, $username, $data) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::set_user_role_parameters(), array('act'=>$act, 'courseid'=> $courseid, 'username'=>$username, 'data'=>$data));
        $auth = new auth_plugin_joomdle();
        $return = $auth->set_user_role($act, $courseid, $username, $data);
        return $return;
    }
    
    // save grade for user
    public static function save_grade_user_parameters() {
        return new external_function_parameters(
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'grade_id' => new external_value(PARAM_INT, 'grade id'),
                            'gradeitem_id' => new external_value(PARAM_INT, 'grade item id'),
                            'grade_value' => new external_value(PARAM_FLOAT, 'grade value'),
                            'feedback' => new external_value(PARAM_TEXT, 'feedback'),
                        )
        );
    }
    public static function save_grade_user_returns() {
        return new  external_value(PARAM_BOOL, 'grade save');
    }

    public static function save_grade_user($username, $grade_id, $gradeitem_id, $grade_value, $feedback) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::save_grade_user_parameters(), array('username'=> $username, 'grade_id'=>$grade_id, 'gradeitem_id' => $gradeitem_id, 'grade_value' => $grade_value, 'feedback'=>$feedback));
        $auth = new auth_plugin_joomdle();
        $return = $auth->save_grade_user($username, $grade_id, $gradeitem_id, $grade_value, $feedback);
        return $return;
    }

    // save all activity status and feedback
    public static function save_completion_activity_parameters() {
        return new external_function_parameters(
            array(
                    'username' => new external_value(PARAM_TEXT, 'username'),
                    'id' => new external_value(PARAM_INT, 'id'),
                    'status' => new external_value(PARAM_TEXT, 'status'),
                    'feedback' => new external_value(PARAM_TEXT, 'feedback'),
            )
       );
    }
    public static function save_completion_activity_returns() {
        return new  external_value(PARAM_BOOL, 'activity save');
    }
    
    public static function save_completion_activity($username, $act_id, $status, $feedback) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::save_completion_activity_parameters(), array('username'=> $username, 'id'=>$act_id, 'status' => $status, 'feedback'=>$feedback));
        $auth = new auth_plugin_joomdle();
        $return = $auth->save_completion_activity($username, $act_id, $status, $feedback);
        return $return;
    }

    //get_mod_detail
    public static function get_mod_detail_parameters() {
        return new external_function_parameters(
                array(
                    'id'=> new external_value(PARAM_INT, 'id'),
                    'type' => new external_value(PARAM_TEXT, 'type')
                )
        );
    }
    
    public static function get_mod_detail_returns() {
        return new external_multiple_structure (
         new external_single_structure(
            array(
                'id' => new external_value(PARAM_INT, 'activity id'),
                'name' => new external_value(PARAM_TEXT, 'activity name'),
                'topicname' => new external_value(PARAM_TEXT, 'topic name'),
                'hasgrade' => new external_value(PARAM_BOOL, 'Check mod has grade'),
                'student' => new external_multiple_structure (
                    new external_single_structure (
                        array(
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'completed' => new external_value(PARAM_BOOL, 'Check user completed mod'),
                            'timecompleted' => new external_value(PARAM_INT, 'timecompleted assign'),
                            'grademax' => new external_value(PARAM_FLOAT, 'grademax', false),
                            'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                            'finalgradeletter' => new external_value(PARAM_TEXT, 'final grade letter'),
                            'gradeid' => new external_value(PARAM_INT, 'grade id'),
                            'gradeitemid' => new external_value(PARAM_INT, 'grade item id'),
                            'feedback' => new external_value(PARAM_TEXT, 'Feedback mod', false),
                            'hidden' => new external_value(PARAM_INT, 'hidden', false),
                            ), '', false
                        ), 'student', false
                        ),
                'lettergradeoption' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'value' => new external_value(PARAM_INT, 'grade letter value'),
                                'name' => new external_value(PARAM_TEXT, 'grade letter letter'),
                            )
                        )
                    ),
                )
            )
        );
    }
    
    public static function get_mod_detail($id, $type) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::get_mod_detail_parameters(), array('id'=>$id, 'type'=>$type));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_mod_detail($id, $type);
        return $return;
    }

    //grade_detail
    public static function grade_detail_parameters() {
        return new external_function_parameters(
                array(
                    'id'=> new external_value(PARAM_INT, 'id'),
                    'gradeitem'=> new external_value(PARAM_INT, 'gradeitem'),
                    'username'=> new external_value(PARAM_TEXT, 'username')
                )
        );
    }
    
    public static function grade_detail_returns() {
        return    new external_single_structure (
                            array(
                                'id' => new external_value(PARAM_INT, 'grade id'),
                                'gradeitemid' => new external_value(PARAM_INT, 'grade item id'),
                                'course' => new external_value(PARAM_INT, 'course'),
                                'activity_moduleid' => new external_value(PARAM_INT, 'activity id'),
                                'activity_name' => new external_value(PARAM_TEXT,' Activity name'),
                                'mod' => new external_value(PARAM_TEXT,'module name'),
                                'grade_cheme_setting' => new external_value(PARAM_BOOL,'Grade scheme setting'),
                                'finalgrade' => new external_value(PARAM_FLOAT, 'final grade'),
                                'grademax' => new external_value(PARAM_FLOAT, 'grademax', false),
                                'feedback' => new external_value(PARAM_TEXT,' grade feedback'),
                                'finalgradeletter'  => new external_value(PARAM_TEXT, 'grade letter letter'),
                                'lettergradeoption' => new external_multiple_structure(
                                        new external_single_structure(
                                            array(
                                                'value' => new external_value(PARAM_INT, 'grade letter value'),
                                                'name' => new external_value(PARAM_TEXT, 'grade letter letter'),
                                            )
                                        )
                                 ),
                                'activity_completion' => new external_single_structure(
                                        array(
                                            'mod_completion' => new external_value(PARAM_BOOL, 'activity completion'),
                                            'mod_completion_date' => new external_value(PARAM_RAW, 'activity completion'),
                                            'mod_lastaccess' => new external_value(PARAM_RAW, 'activity lastaccess')
                                        )
                               )
                            )
                );
    }

    public static function grade_detail($grade_id, $gradeitem, $username) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::grade_detail_parameters(), array('id'=>$grade_id, 'gradeitem'=>$gradeitem, 'username'=>$username));
        $auth = new auth_plugin_joomdle();
        $return = $auth->grade_detail($grade_id, $gradeitem, $username);
        return $return;
    }

//get question feedback
    public static function get_feedback_question_parameters(){
        return new external_function_parameters(
            array(
                'survey_id' => new external_value(PARAM_INT, 'Survey ID')
            )
        );
    }
    public static function get_feedback_question_returns(){
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'Question ID'),
                    'survey_id' => new external_value(PARAM_INT, 'Survey ID'),
                    'type_id' => new external_value(PARAM_INT, 'Type ID'),
                    'name' => new external_value(PARAM_TEXT, 'Question Name'),
                    'content' => new external_value(PARAM_TEXT, 'Question Content'),
                    'deleted' => new external_value(PARAM_TEXT, 'deleted', false),
                    'params' => new external_value(PARAM_RAW, 'params', false),
                )
            )
        );
    }
    public static function get_feedback_question($survey_id){
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_feedback_question_parameters(), array('survey_id'=> $survey_id));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_feedback_question ($survey_id);
        return $return;
    }

// save question questionnaire save_question_questionnaire
    public static function save_question_questionnaire_parameters() {
        return new external_function_parameters(
            array(
                'name' => new external_value(PARAM_TEXT, 'name'),
                'length' => new external_value(PARAM_INT, 'length'),
                'survey_id' => new external_value(PARAM_INT, 'survey id'),
                'type_id' => new external_value(PARAM_INT, 'type id'),
                'position' => new external_value(PARAM_INT, 'position'),
                'content' => new external_value(PARAM_TEXT, 'content'),
            )
        );
    }

    public static function save_question_questionnaire_returns() {
        return new  external_value(PARAM_BOOL, 'question save');
    }

    public static function save_question_questionnaire($name, $length, $survey, $type, $position, $content) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::save_question_questionnaire_parameters(), array('name'=> $name, 'length'=>$length, 'survey_id' => $survey, 'type_id' => $type, 'position'=>$position, 'content'=>$content));
        $auth = new auth_plugin_joomdle();
        $return = $auth->save_question_questionnaire($name, $length, $survey, $type, $position, $content);
        return $return;
    }

    // get_info_questionnaire
     public static function get_info_questionnaire_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
            )
        );
    }
    public static function get_info_questionnaire_returns() {
        return new external_single_structure(
            array(
                'module_id' => new external_value(PARAM_INT, 'module ID'),
                'questionnaire_id' => new external_value(PARAM_INT, 'Questionnaire ID'),
                'courseid' => new external_value(PARAM_INT, 'Course ID'),
                'name' => new external_value(PARAM_RAW, 'Name Questionnaire'),
                'intro' => new external_value(PARAM_RAW, 'Intro Questionnaire'),
                'surveyid' => new external_value(PARAM_INT, 'Survey ID'),
                'questions' => new external_multiple_structure(
                                new external_single_structure(
                                    array (
                                    'id' => new external_value(PARAM_INT, 'resource id'),
                                    'type_ques' => new external_value(PARAM_TEXT, 'type_ques'),
                                    'type_id' => new external_value(PARAM_INT, 'type_id', false),
                                    'name' => new external_value(PARAM_RAW, 'name'),
                                    'content' => new external_value(PARAM_RAW, 'content'),
                                    'required' => new external_value(PARAM_RAW, 'required'),
                                    'position' => new external_value(PARAM_INT, 'position'),
                                    'length' => new external_value(PARAM_INT, 'length'),
                                    'deleted' => new external_value(PARAM_TEXT, 'deleted', false),
                                    'params' => new external_value(PARAM_RAW, 'params', false),
                                    )
                                )
                )
            )
        );
    }

    public static function get_info_questionnaire($id,$username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_info_questionnaire_parameters(), array('id'=>$id, 'username' => $username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_info_questionnaire($id,$username);


        return $return;
    }

    // get_info_feedback
     public static function get_info_feedback_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'id'),
            )
        );
    }
    public static function get_info_feedback_returns() {
        return new external_single_structure(
            array(
                'rank' =>   new external_multiple_structure(
                                new external_single_structure(
                                    array (
                                        'ques_id' => new external_value(PARAM_INT, 'id question'),
                                        'user' => new external_value(PARAM_RAW, 'user'),
                                        'ques_text' => new external_value(PARAM_RAW, 'text question'),
                                        'ques_response' => new external_value(PARAM_INT, 'rank question'),
                                    )
                                )
                            ),
                'text' =>   new external_multiple_structure(
                                new external_single_structure(
                                    array (
                                        'ques_id' => new external_value(PARAM_INT, 'id question'),
                                        'user' => new external_value(PARAM_RAW, 'user'),
                                        'ques_text' => new external_value(PARAM_RAW, 'text question'),
                                        'ques_response' => new external_value(PARAM_RAW, 'response question'),
                                    )
                                )
                            )
            )
        );
    }

    public static function get_info_feedback($id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_info_feedback_parameters(), array('id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_info_feedback($id);


        return $return;
    }

    // get_info_assignment
     public static function get_info_assignment_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'id')
            )
        );
    }
    public static function get_info_assignment_returns() {
        return new external_single_structure(
            array(
                'id' => new external_value(PARAM_INT, 'module instance id'),
                'course' => new external_value(PARAM_INT, 'course id'),
                'name' => new external_value(PARAM_TEXT, 'assign name'),
                'intro' => new external_value(PARAM_RAW, 'intro name'),
                'duedate' => new external_value(PARAM_INT, 'due date assign'),
                'publishdate' => new external_value(PARAM_INT, 'publishdate assign'),
                'timemodified' => new external_value(PARAM_INT, 'timemodified assign'),
            )
        );
    }
   
    public static function get_info_assignment($id) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_info_assignment_parameters(), array('id'=>$id));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_info_assignment($id);


        return $return;
    }

    // delete_module
     public static function delete_module_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'id'),
                'course' => new external_value(PARAM_INT, 'course')
            )
        );
    }
    public static function delete_module_returns() {
        return new  external_value(PARAM_TEXT, 'delete module');
    }
   
    public static function delete_module($id,$course) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::delete_module_parameters(), array('id'=>$id,'course'=>$course));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->delete_module($id,$course);


        return $return;
    }

    // post quiz end
     public static function post_end_quiz_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
            )
        );
    }

    public static function post_end_quiz_returns() {
        return new  external_value(PARAM_TEXT, 'End quiz');
    }
   
    public static function post_end_quiz($id,$username) {
        global $CFG, $DB;

        $params = self::validate_parameters(self::post_end_quiz_parameters(), array('id'=>$id, 'username' => $username));

        $auth = new  auth_plugin_joomdle ();
        $return = $auth->post_end_quiz($id,$username);


        return $return;
    }

    //get_questionnaire_id
    public static function get_questionnaire_id_parameters(){
        return new external_function_parameters(
            array(
                'course' => new external_value(PARAM_INT, 'Course ID')
            )
        );
    }
    public static function get_questionnaire_id_returns(){
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'Questionnaire ID'),
                    'module' => new external_value(PARAM_INT, 'Module ID'),
                    'instance' => new external_value(PARAM_INT, 'Instance Questionnaire'),
                    'section' => new external_value(PARAM_INT, 'Section ID')
                )
            )
        );
    }
    public static function get_questionnaire_id($course_id){
        global $CFG, $DB;

        $params = self::validate_parameters(self::get_questionnaire_id_parameters(), array('course'=> $course_id));
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_questionnaire_id ($course_id);
        return $return;
    }
    
    //get_submission_file
    public static function get_submission_file_parameters() {
        return new external_function_parameters(
                array(
                    'id'=> new external_value(PARAM_INT, 'id'),
                    'username'=> new external_value(PARAM_TEXT, 'username',false),
                )
           );
    }
    public static function get_submission_file_returns() {
        return new external_multiple_structure ( 
            new external_single_structure(
                array(
                    'filename' => new external_value(PARAM_TEXT, 'file name'),
                    'filetype' => new external_value(PARAM_RAW, 'file type'),
                    'filepath' => new external_value(PARAM_RAW, 'file path'),
                )
            )
          );
    }

    public static function get_submission_file($id,$username) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::get_submission_file_parameters(), array('id'=>$id,'username'=>$username));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_submission_file($id,$username);
        return $return;
    }

    //update_multi_activity_module
    public static function update_multi_activity_module_parameters() {
        return new external_function_parameters(
            array(
                'activity' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'module_id' => new external_value(PARAM_INT, 'module id', false),
                            'status' => new external_value(PARAM_INT, 'status', false),

                        )
                    )
                ),
                'sectionid' => new external_value(PARAM_INT, 'section id', false),
                'courseid' => new external_value(PARAM_INT, 'course id', false)
            )
        );
    }
    public static function update_multi_activity_module_returns() {
        return new external_single_structure(
            array(
                'status'=> new external_value(PARAM_BOOL, 'status'),
                'message'=> new external_value(PARAM_TEXT, 'message'),
            )
        );
    }
    
    public static function update_multi_activity_module($acivity,$sectionId,$courseid) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::update_multi_activity_module_parameters(), array('activity'=>$acivity,'sectionid'=>$sectionId,'courseid'=>$courseid));
        $auth = new auth_plugin_joomdle();
        $return = $auth->update_multi_activity_module($acivity,$sectionId,$courseid);
        return $return;
    }
    
    //assign_multi_user_role
    public static function assign_multi_user_role_parameters() {
        return new external_function_parameters(
                array(
                    'courseid' => new external_value(PARAM_INT, 'course id'),
                    'users'=> new external_multiple_structure(
                            new external_single_structure(
                                    array(
                                        'username'=> new external_value(PARAM_TEXT, 'username'),
                                        'role'=> new external_value(PARAM_INT, 'role'),
                                    )
                                    )
                            )
                )
                );
    }
    
    public static function assign_multi_user_role_returns() {
        return new external_single_structure(
                 array(
                     'status'=> new external_value(PARAM_BOOL, 'status'),
                     'message'=> new external_value(PARAM_TEXT, 'message'),
                 )
                );
    }
    
    public static function assign_multi_user_role($courseid, $users) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::assign_multi_user_role_parameters(), array('courseid' =>$courseid ,'users'=>$users));
        $auth = new auth_plugin_joomdle();
        $return = $auth->assign_multi_user_role($courseid, $users);
        return $return;
    }
    
    //get_course_outline_content
    public static function get_course_outline_content_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'course id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
                'visible' => new external_value(PARAM_INT, 'visible'),
            )
        );
    }
    public static function get_course_outline_content_returns() {
        return new external_single_structure (
                  array(
                      'status' => new external_value(PARAM_BOOL, 'status'),
                      'course' => new external_single_structure(
                              array (
                                      'courseinfo' => new external_single_structure(
                                              array(
                                                  'id'=> new external_value(PARAM_INT, 'course id'),
                                                  'fullname'=> new external_value(PARAM_TEXT, 'course name'),
                                                  'shortname' => new external_value(PARAM_TEXT, 'short name'),
                                                  'summary' => new external_value(PARAM_TEXT, 'description'),
                                                  'time_end' => new external_value(PARAM_TEXT, 'time end'),
                                                  'image' => new external_value(PARAM_RAW, 'image'),
                                                  'course_survey' => new external_value(PARAM_INT, 'course survey'),
                                                  'course_facilitated' => new external_value(PARAM_INT, 'facilitated course'),
                                                  'course_status' => new external_value(PARAM_TEXT, 'course status'),
                                              )
                                    ),
                                  'topic' => new external_multiple_structure(
                                          new external_single_structure(
                                            array(
                                              'section' => new external_value(PARAM_INT, 'section no'),
                                              'sectionid' => new external_value(PARAM_INT, 'section id'),
                                              'name' => new external_value(PARAM_TEXT, 'section name'),
                                              'summary' => new external_value(PARAM_RAW, 'summary'),
                                              'mods' => new external_multiple_structure(
                                                  new external_single_structure(
                                                      array (
                                                          'id' => new external_value(PARAM_INT, 'resource id'),
                                                          'atype' => new external_value(PARAM_INT, 'assessment type', false),
                                                          'name' => new external_value(PARAM_RAW, 'name'),
                                                          'mod' => new external_value(PARAM_RAW, 'mod'),
                                                          'type' => new external_value(PARAM_RAW, 'type'),
                                                          'available' => new external_value(PARAM_INT, 'available'),
                                                          'completion_info' => new external_value(PARAM_RAW, 'completion info'),
                                                          'mod_completion' => new external_value(PARAM_BOOL, 'mod_completion'),
                                                          'display' => new external_value(PARAM_INT, 'display'),
                                                          'url' => new external_value(PARAM_RAW, 'url activity'),
                                                      )
                                                  )
                                              ),
                                          )
                                       )
                                  )
                              )
                              
                      )  
                  )
                );
    }
    
    public static function get_course_outline_content($id, $username, $visiable) {
        global $BD, $CFG;
        $params = self::validate_parameters(self::get_course_outline_content_parameters(), array('id'=>$id, 'username'=>$username, 'visible'=>$visiable));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_course_outline_content($id, $username, $visiable);
        return $return;

    }
    
    //get_quiz_feedback
    public static function get_quiz_feedback_parameters() {
        return new external_function_parameters(
                    array(
                        'username'=> new external_value(PARAM_TEXT, 'username'),
                    )
                );
    }
    
    public static function get_quiz_feedback_returns() {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL, 'status'),
                        'message' => new external_value(PARAM_TEXT, 'message'),
                        'questions' => new external_multiple_structure(
                                    new external_single_structure(
                                        array(
                                            'id' => new external_value(PARAM_INT, 'Question ID'),
                                            'survey_id' => new external_value(PARAM_INT, 'Survey ID'),
                                            'type_id' => new external_value(PARAM_INT, 'Type ID'),
                                            'name' => new external_value(PARAM_TEXT, 'Question Name'),
                                            'content' => new external_value(PARAM_TEXT, 'Question Content'),
                                            'position' => new external_value(PARAM_INT, 'Position'),
                                        )
                                    )
                                )
                    )
                );
    }
    
    public static function get_quiz_feedback($username) {
        global $DB, $CFG;
        $param = self::validate_parameters(self::get_quiz_feedback_parameters(), array('username'=>$username));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_quiz_feedback($username);
        return $return;
    }

    //add_questionarie_to_feedback
    public static function add_questionarie_to_feedback_parameters() {
        return new external_function_parameters(
                            array(
                                'username' => new external_value(PARAM_TEXT, 'username'),
                                'feedbackid' => new external_value(PARAM_INT, 'feedback id'),
                                'lenght' => new external_value(PARAM_INT, 'lenght'),
                                'questions' => new external_multiple_structure(
                                        new external_single_structure(
                                                array(
                                                        'name' => new external_value(PARAM_TEXT, 'name'),
                                                        'type_id' => new external_value(PARAM_INT, 'type id'),
                                                        'position' => new external_value(PARAM_INT, 'position'),
                                                        'content' => new external_value(PARAM_TEXT, 'content'),
                                                    )
                                                )
                                        )
                                
                            )
                );
        
    }
    
    public static function add_questionarie_to_feedback_returns() {
        return new external_single_structure(
                 array(
                     'status'=> new external_value(PARAM_BOOL, 'status'),
                     'message'=> new external_value(PARAM_TEXT, 'message'),
                 )
                );
    }

    public static function add_questionarie_to_feedback($username, $feedbackid, $lenght, $question) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::add_questionarie_to_feedback_parameters(), array('username'=>$username, 'feedbackid'=>$feedbackid, 'lenght'=>$lenght, 'questions'=>$question));
        $auth = new auth_plugin_joomdle();
        $return = $auth->add_question_to_feedback($username, $feedbackid, $lenght, $question);
        return $return;
    }
    
    // add activity content
    public static function create_activity_content_parameters() {
        return new external_function_parameters(
                    array(
                        'activity' => new external_single_structure(
                                array(
                                    'username' => new external_value(PARAM_TEXT, 'username'),
                                    'courseid' => new external_value(PARAM_INT, 'course id'),
                                    'act' => new external_value(PARAM_INT, 'action'),
                                    'module'   => new external_value(PARAM_TEXT, 'module name'),
                                    'title'   => new external_value(PARAM_TEXT, 'title module'),
                                    'contentid'   => new external_value(PARAM_INT, 'content id'),
                                    'content'   => new external_multiple_structure(
                                                new external_single_structure(
                                                            array(
                                                                'data' => new external_value(PARAM_RAW, 'content data'),
                                                                'type' => new external_value(PARAM_TEXT, 'content type'),
                                                                'position' => new external_value(PARAM_INT, 'content position'),
                                                                'caption' => new external_value(PARAM_TEXT, 'caption video', false),
                                                            )
                                                        )
                                            ),
                                )
                            )
                    )
                );
    }
    public static function create_activity_content_returns() {
        return new external_single_structure(
                  array(
                      'status' => new external_value(PARAM_BOOL, 'status'),
                      'message' => new external_value(PARAM_TEXT, 'message'),
                      'contentid' => new external_value(PARAM_INT, 'id'),
                  )  
                );
    }
    
    public static function create_activity_content($activity) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::create_activity_content_parameters(), array('activity'=>$activity));
        $auth = new auth_plugin_joomdle();
        $return = $auth->create_activity_content($activity);
        return $return;
    }

    // get activity content detail
     public static function get_activity_content_parameters() {
            return new external_function_parameters(
                array(
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'aid' => new external_value(PARAM_INT, 'activity id', false),
                    )
            );
    }
    
    public static function get_activity_content_returns() {
        return new external_single_structure(
                array(
                    'status' => new external_value(PARAM_BOOL, 'status'),
                    'message' => new external_value(PARAM_TEXT, 'message'),
                    'data' => new external_single_structure(
                            array(
                                'sid' => new external_value(PARAM_INT, 'section id'),
                                'courseid' => new external_value(PARAM_INT, 'course id'),
                                'name' => new external_value(PARAM_TEXT, 'section name'),
                                'des' => new external_value(PARAM_RAW, 'Describe'),
                                'desformat' => new external_value(PARAM_INT, 'Describe format'),
                                'content' => new external_multiple_structure(
                                    new external_single_structure(
                                            array(
                                                'type' => new external_value(PARAM_TEXT, 'content type', false),
                                                'data' => new external_value(PARAM_RAW, 'Content', false),
                                                'position' => new external_value(PARAM_INT, 'Position', false),
                                                'itemid' => new external_value(PARAM_INT, 'Item id', false),
                                                'fname' => new external_value(PARAM_TEXT, 'File name', false),
                                                'fpath' => new external_value(PARAM_RAW, 'File path', false),
                                                'caption' => new external_value(PARAM_RAW, 'Caption video', false),
                                            )
                                    )
                                ),
                                'contentformat' => new external_value(PARAM_RAW, 'Content format'),
                            ), '', false
                        )
                    )
            );
    }
    
    public static function get_activity_content($courseid, $username, $aid) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::get_activity_content_parameters(), array('courseid' => $courseid, 'username' => $username, 'aid' => $aid));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_activity_content($courseid, $username, $aid);
        return $return;
    }

    // get quiz activity detail
     public static function get_quiz_activity_detail_parameters() {
            return new external_function_parameters(
                array(
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'aid' => new external_value(PARAM_INT, 'activity id'),
                    )
            );
    }
    
    public static function get_quiz_activity_detail_returns() {
        return new external_single_structure(
                array(
                    'status' => new external_value(PARAM_BOOL, 'status'),
                    'message' => new external_value(PARAM_TEXT, 'message'),
                    'data' => new external_single_structure(
                            array(
                                'sid' => new external_value(PARAM_INT, 'section id'),
                                'courseid' => new external_value(PARAM_INT, 'course id'),
                                'name' => new external_value(PARAM_TEXT, 'section name'),
                                'atype' => new external_value(PARAM_INT, 'atype'),
                                'des' => new external_value(PARAM_RAW, 'Describe'),
                                'desformat' => new external_value(PARAM_INT, 'Describe format'),
                                'timeopen' => new external_value(PARAM_TEXT, 'Time open'),
                                'timeclose' => new external_value(PARAM_TEXT, 'Time close'),
                                'timelimit' => new external_value(PARAM_TEXT, 'Time limit'),
                                'attempts' => new external_value(PARAM_TEXT, 'attempts'),
                                'questions' => new external_value(PARAM_TEXT, 'List questions id'),
                            ), '', false
                        )
                    )
            );
    }
    
    public static function get_quiz_activity_detail($courseid, $username, $aid) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::get_quiz_activity_detail_parameters(), array('courseid' => $courseid, 'username' => $username, 'aid' => $aid));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_quiz_activity_detail($courseid, $username, $aid);
        return $return;
    }

    // get assignment activity detail
     public static function get_assignment_activity_detail_parameters() {
            return new external_function_parameters(
                array(
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'aid' => new external_value(PARAM_INT, 'activity id'),
                    )
            );
    }

    public static function get_assignment_activity_detail_returns() {
        return new external_single_structure(
                array(
                    'status' => new external_value(PARAM_BOOL, 'status'),
                    'message' => new external_value(PARAM_TEXT, 'message'),
                'assign_information' => new external_value(PARAM_RAW, 'json information assignment'),
                'submission_status' => new external_value(PARAM_RAW, 'submission_status'),
                'grade_status' => new external_value(PARAM_RAW, 'grade_status'),
                'link_download_brief' => new external_value(PARAM_RAW, 'link_download_brief'),
                'topic_name' => new external_value(PARAM_RAW, 'topic name'),
                        )
            );
    }

    public static function get_assignment_activity_detail($courseid, $username, $aid) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::get_assignment_activity_detail_parameters(), array('courseid' => $courseid, 'username' => $username, 'aid' => $aid));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_assignment_activity_detail($courseid, $username, $aid);
        return $return;
    }


    // get scorm activity detail
     public static function get_scorm_activity_detail_parameters() {
            return new external_function_parameters(
                array(
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'aid' => new external_value(PARAM_INT, 'activity id'),
                    )
            );
    }
    
    public static function get_scorm_activity_detail_returns() {
        return new external_single_structure(
                array(
                    'status' => new external_value(PARAM_BOOL, 'status'),
                    'message' => new external_value(PARAM_TEXT, 'message'),
                    'data' => new external_single_structure(
                            array(
                                'sid' => new external_value(PARAM_INT, 'section id'),
                                'courseid' => new external_value(PARAM_INT, 'course id'),
                                'sname' => new external_value(PARAM_TEXT, 'section name'),
                                'des' => new external_value(PARAM_RAW, 'Describe'),
                                'desformat' => new external_value(PARAM_INT, 'Describe format'),
                                'scormtype' => new external_value(PARAM_TEXT, 'Scorm type'),
                                'filename' => new external_value(PARAM_TEXT, 'File name'),
                            ), '', false
                        )
                    )
            );
    }
    
    public static function get_scorm_activity_detail($courseid, $username, $aid) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::get_scorm_activity_detail_parameters(), array('courseid' => $courseid, 'username' => $username, 'aid' => $aid));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_scorm_activity_detail($courseid, $username, $aid);
        return $return;
    }

    // delete a activity
     public static function delete_course_content_parameters() {
            return new external_function_parameters(
                array(
                        'aid' => new external_value(PARAM_INT, 'activity id'),
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'type' => new external_value(PARAM_TEXT, 'activity type'),
                    )
            );
    }
    
    public static function delete_course_content_returns() {
        return new external_single_structure(
                array(
                    'status' => new external_value(PARAM_BOOL, 'status'),
                    'message' => new external_value(PARAM_TEXT, 'message'),
                    'aid' => new external_value(PARAM_INT, 'activity id'),
                    )
            );
    }
    
    public static function delete_course_content($aid, $courseid, $username, $type) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::delete_course_content_parameters(), array('aid' => $aid, 'courseid' => $courseid, 'username' => $username, 'type' => $type));
        $auth = new auth_plugin_joomdle();
        $return = $auth->delete_course_content($aid, $courseid, $username, $type);
        return $return;
    }
    
    // List activity
    public static function list_activity_parameters() {
        return new external_function_parameters(
                    array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'username' => new external_value(PARAM_TEXT, 'username'),
                            'mtype' => new external_value(PARAM_TEXT, 'module type', false),
                        )
                );
    }
    
    public static function list_activity_returns() {
        return new external_single_structure(
                array(
                    'status' => new external_value(PARAM_BOOL, 'status'),
                    'activity' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'section' => new external_value(PARAM_INT, 'section id'),
                                'name' => new external_value(PARAM_TEXT, 'section name'),
                                'summary' => new external_value(PARAM_RAW, 'summary'),
                                'mods' => new external_multiple_structure(
                                                new external_single_structure(
                                                    array (
                                                    'id' => new external_value(PARAM_INT, 'resource id'),
                                                    'atype' => new external_value(PARAM_INT, 'assessment type', false),
                                                    'name' => new external_value(PARAM_RAW, 'name'),
                                                    'mod' => new external_value(PARAM_RAW, 'mod'),
                                                    'type' => new external_value(PARAM_RAW, 'type'),
                                                    'available' => new external_value(PARAM_INT, 'available'),
                                                    'completion_info' => new external_value(PARAM_RAW, 'completion info'),
                                                    'mod_completion' => new external_value(PARAM_BOOL, 'mod_completion'),
                                                    'display' => new external_value(PARAM_INT, 'display'),
                                                    'url' => new external_value(PARAM_RAW, 'url activity'),
                                                    )
                                                )
                                    ),
                            )
                        )
                     )
                    )
            );
    }
    
    public static function list_activity($id, $username, $type) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::list_activity_parameters(), array('id'=>$id, 'username'=>$username, 'mtype'=> $type));
        $auth = new auth_plugin_joomdle();
        $return  = $auth->list_activity($id, $username, $type);
        return $return;
    }

    /*
     * Check: Is user Facilitator of Course?
     */
    public static function check_facilitator_parameters () {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'course id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
            )
        );
    }
    
    public static function check_facilitator_returns () {
        return new external_single_structure(
            array(
                'isFacilitator' => new external_value(PARAM_BOOL, 'check user is facilitator', false),
            )
        );
    }

    public static function check_facilitator ($courseid, $username) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::check_facilitator_parameters(), array('courseid'=>$courseid, 'username'=>$username));
        $auth = new auth_plugin_joomdle();
        $check = $auth->checkFacilitator($courseid, $username);

        $return = array('isFacilitator' => $check);
        return $return;
    }
    
    // get course of learning provider
    public static function learning_provider_courses_parameters() {
        return new external_function_parameters(
                    array(
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'categoryid' => new external_value(PARAM_TEXT, 'list category id'),
                    )
                );
    }
    
    public static function learning_provider_courses_returns() {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL, 'status'),
                        'message' => new external_value(PARAM_TEXT, 'message'),
                        'courses' => new external_multiple_structure(
                                     new external_single_structure(
                                                array(
                                                    'id' => new external_value(PARAM_INT, 'course id'),
                                                    'category' => new external_value(PARAM_INT, 'category id'),
                                                    'fullname' => new external_value(PARAM_TEXT, 'course full name'),
                                                    'summary' => new external_value(PARAM_RAW, 'course summary'),
                                                    'startdate' => new external_value(PARAM_INT, 'start date'),
                                                    'enrolperiod' => new external_value(PARAM_INT, 'enrolperiod'),
                                                    'creator' => new external_value(PARAM_TEXT, 'creator'),
                                                    'count_learner' => new external_value(PARAM_INT, 'count learner', false),
                                                    'created' => new external_value(PARAM_INT, 'created'),
                                                    'modified' => new external_value(PARAM_INT, 'modified'),
                                                    'timepublish' => new external_value(PARAM_INT, 'timepublish', false),
                                                    'timeend' => new external_value(PARAM_RAW, 'timeend', VALUE_OPTIONAL),
                                                    'cost' => new external_value(PARAM_FLOAT, 'cost', VALUE_OPTIONAL),
                            'price' => new external_value(PARAM_FLOAT, 'price', VALUE_OPTIONAL),
                                                    'currency' => new external_value(PARAM_TEXT, 'currency', VALUE_OPTIONAL),
                                                    'duration' => new external_value(PARAM_FLOAT, 'duration'),
//                                                    'courseimage' => new external_value(PARAM_RAW, 'course image'), 
                                                    'filepath' => new external_value(PARAM_TEXT, 'file path', VALUE_OPTIONAL),
                                                    'filename' => new external_value(PARAM_TEXT, 'file name', VALUE_OPTIONAL),
                            'is_mycourse' => new external_value(PARAM_BOOL, 'Is my course'),
                            'pending_approve' => new external_value(PARAM_BOOL, 'course is pendding approved'),
                            'approved' => new external_value(PARAM_BOOL, 'course is approved'),
                                                    'unapproved' => new external_value(PARAM_BOOL, 'course is unapproved'),
                                                    'published' => new external_value(PARAM_BOOL, 'course is Publish'),
                                                    'unpublished' => new external_value(PARAM_BOOL, 'course is UnPublish'),
                                                    'course_survey' => new external_value(PARAM_INT, 'course survey'),
                                                    'course_facilitated' => new external_value(PARAM_INT, 'facilitated course'),
                                                    'course_status' => new external_value(PARAM_TEXT, 'Course status'),
                                                    'isFacilitator' => new external_value(PARAM_BOOL, 'check user is facilitator', false),
                                                    'assignedUsers' => new external_multiple_structure(
                                                            new external_single_structure(
                                                               array(
                                                                   'username' => new external_value(PARAM_TEXT, 'username'),
                                                                   'role' => new external_value(PARAM_RAW, 'json role data'),
                                                                   'hasPermission' => new external_value(PARAM_INT, '1 yes 0 no'),
                                                               )
                                                            )
                                                    )
                                                )
                                            )
                                )
                    )
                );
    }
    
    public static function learning_provider_courses($username, $categoryid) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::learning_provider_courses_parameters(), array('username'=>$username, 'categoryid' => $categoryid));
        $auth = new auth_plugin_joomdle();
        $return = $auth->learning_provider_courses($username, $categoryid);
        return $return;
    }
    
    // send course to LP approve
    public static function send_course_for_approve_parameters() {
        return new external_function_parameters(
                    array(
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'categoryid' => new external_value(PARAM_INT, 'category id'),
                    )
                );
    }
    public static function  send_course_for_approve_returns() {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL, 'status'),
                        'message' => new external_value(PARAM_TEXT, 'message'),
                    )
                );
    }
    
    public static function send_course_for_approve($courseid, $username, $categoryid) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::send_course_for_approve_parameters(), array('courseid'=>$courseid, 'username'=>$username, 'categoryid'=>$categoryid));
        $auth = new auth_plugin_joomdle();
        $return = $auth->send_course_for_approve($courseid, $username, $categoryid);
        return $return;
    }
    
    // approve or unapprove course
    public static function approve_or_unapprove_course_parameters() {
        return new external_function_parameters(
                    array(
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                        'categoryid' => new external_value(PARAM_INT, 'category id'),
                        'action' => new external_value(PARAM_TEXT, 'action: approve or unapprove'),
                    )
                );
    }
    
    public static function approve_or_unapprove_course_returns() {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL, 'status'),
                        'message' => new external_value(PARAM_TEXT, 'message'),
                    )
                );
    }
    
    public static function approve_or_unapprove_course($username, $couresid, $categoryid, $action) {
        global $DB;
        $params = self::validate_parameters(self::approve_or_unapprove_course_parameters(), array('username'=>$username, 'courseid'=>$couresid, 'categoryid'=>$categoryid, 'action' => $action));
        $auth = new auth_plugin_joomdle();
        $return = $auth->approve_or_unapprove_course($username, $couresid, $categoryid, $action);
        return $return;
    }
    
    // publish and unpublish course
    public static function publish_unpublish_course_parameters() {
        return new external_function_parameters(
                array(
                    'categoryid' => new external_value(PARAM_INT, 'category id'),
                    'courseid' => new external_value(PARAM_INT, 'course id'),
                    'action' => new external_value(PARAM_TEXT, 'action'),
                    'username' => new external_value(PARAM_TEXT, 'username'),
                    'hikashopcatids' => new external_value(PARAM_TEXT, 'hikashop categories', false),
                )
                );
    }
    
    public static function publish_unpublish_course_returns() {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL, 'status'),
                        'message' => new external_value(PARAM_TEXT, 'message'),
                    )
                );
    }
    
    public static function publish_unpublish_course($categoryid, $courseid, $action, $username, $hikashopcatids) {
        global $DB;
        $params = self::validate_parameters(self::publish_unpublish_course_parameters(), array('categoryid'=>$categoryid, 'courseid'=>$courseid, 'action'=>$action, 'username'=>$username, 'hikashopcatids'=>$hikashopcatids));
        $auth = new auth_plugin_joomdle();
        $return = $auth->publish_unpublish_course($categoryid, $courseid, $action, $username, $hikashopcatids);
        return $return;
    }
    
    // remove course platform
    public static function remove_course_platform_parameters() {
        return new external_function_parameters(
                    array(
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'categoryid' => new external_value(PARAM_INT, 'category id'),
                    )
                );
    }
    
    public static function remove_course_platform_returns() {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL, 'status'),
                        'message' => new external_value(PARAM_TEXT, 'message'),
                    )
                );
    }
    
    public static function remove_course_platform($courseid, $username, $categoryid) {
        global $DB;
        $params = self::validate_parameters(self::remove_course_platform_parameters(), array('courseid' => $courseid, 'username' => $username, 'categoryid' => $categoryid));
        $auth = new auth_plugin_joomdle();
        $return = $auth->remove_course_platform($courseid, $username, $categoryid);
        return $return;
    }
    
    //create_assignment_platform
    public static function create_assignment_platform_parameters() {
        return new external_function_parameters(
                    array(
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'act' => new external_value(PARAM_INT, 'action'),
                        'assign' => new external_value(PARAM_RAW, 'assignment data'),
                    )
                );
    }

    public static function create_assignment_platform_returns () {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL,'status'),
                        'message' => new external_value(PARAM_TEXT,'message'),
                        'aid' => new external_value(PARAM_INT,'assignment id'),
                    )
                );
    }
    
    public static function create_assignment_platform($courseid, $username, $act, $assign) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::create_assignment_platform_parameters(), array('courseid'=>$courseid, 'username'=> $username, 'act'=>$act, 'assign'=>$assign));
        $auth = new auth_plugin_joomdle();
        $returns = $auth->create_assignment_platform($courseid, $username, $act, $assign);
        return $returns;
    }
    
    // get my category
    public static function get_my_categories_parameters() {
            return new external_function_parameters(
                array(
                    'username' => new external_value(PARAM_TEXT, 'username'),
                )
            );
    }
    
    public static function get_my_categories_returns () {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'categories' => new external_multiple_structure(
                             new external_single_structure(
                                array(
                                        'catid' => new external_value(PARAM_INT, 'category id'),
                                        'name' => new external_value(PARAM_TEXT, 'new category name'),
                                        'username' => new external_value(PARAM_TEXT,
                                                'username of user register learning provider'),
                                        'role' => new external_value(PARAM_TEXT, 'role of user in this category: LP, CMA, non-cat'),
                                    )
                                )
                            )
                    )
                );
    }
    
    public static function get_my_categories ($username) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::get_my_categories_parameters(), array('username'=>$username));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_my_categories($username);
        return $return;
    }
    
    // get LP of category
    public static function get_lp_of_category_parameters() {
            return new external_function_parameters(
                array(
                    'categoryid' => new external_value(PARAM_INT, 'category id'),
                )
            );
    }
    
    public static function get_lp_of_category_returns () {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'username' => new external_value(PARAM_TEXT, 'username of user register learning provider'),
            )
        );
    }
    
    public static function get_lp_of_category ($categoryid) {
        global $DB, $CFG;
        $params = self::validate_parameters(self::get_lp_of_category_parameters(), array('categoryid'=>$categoryid));
        $auth = new auth_plugin_joomdle();
        $return = $auth->getLPOfCategory($categoryid);
        return $return;
    }

    //course_enable_self_enrolment
        public static  function course_enable_self_enrolment_parameters () {
        return new external_function_parameters(
            array(
//                    'categoryid' => new external_value(PARAM_INT, 'category id'),
                    'courseid' => new external_value(PARAM_INT, 'course id'),
                    'action' => new external_value(PARAM_TEXT, 'action'),
                    'username' => new external_value(PARAM_TEXT, 'username'),
                    'circleids' => new external_value(PARAM_TEXT, 'list circle id', false)
                )
            );
    }
    
    public static  function course_enable_self_enrolment_returns () {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
            )
        );
    }
    
    public static  function course_enable_self_enrolment ($courseid, $action, $username, $circleids) {
        global $DB;
        $params = self::validate_parameters(self::course_enable_self_enrolment_parameters(), array('courseid'=>$courseid, 'action'=>$action, 'username'=>$username, 'circleids'=>$circleids));
        $auth = new auth_plugin_joomdle();
        $return = $auth->enable_self_enrolment($courseid, $action, $username, $circleids);
        return $return;
    }

    //get_certificate_template_default
    public static function get_certificate_template_default_parameters() {
        return new external_function_parameters(
                    array(
                        'courseid' => new external_value(PARAM_INT, 'courseid'),
                    )
                );
    }
    public static function get_certificate_template_default_returns() {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL, 'status'),
                        'message' => new external_value(PARAM_TEXT, 'message'),
                        'template' => new external_single_structure(
                                 array(
                                     'cer_id' => new external_value(PARAM_INT, 'certificate id'),
                                     'cer_name' => new external_value(PARAM_TEXT, 'certificate name'),
                                 )
                                ),
                    )
                );
    }
    public static function get_certificate_template_default($courseid) {
        global $DB;
        $params = self::validate_parameters(self::get_certificate_template_default_parameters(), array('courseid' => $courseid));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_certificate_template_default($courseid);
        return $return;
    }
    
    //set_default_certificate_template
    public static function set_default_certificate_template_parameters() {
        return new external_function_parameters(
                    array(
                        'cer_id' => new external_value(PARAM_INT, 'certificate id'),
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                        'temp_name' => new external_value(PARAM_RAW, 'certificate name'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                    )
                );
    }
    
    public static function set_default_certificate_template_returns() {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL, 'status'),
                        'message' => new external_value(PARAM_TEXT, 'message'),
                    )
                );
    }
    
    public static function set_default_certificate_template($certificate_id, $courseid, $temp_name, $username) {
        global $DB;
        $params = self::validate_parameters(self::set_default_certificate_template_parameters(), array('cer_id' => $certificate_id, 'courseid'=>$courseid, 'temp_name'=> $temp_name, 'username' => $username));
        $auth = new auth_plugin_joomdle();
        $return  = $auth->set_default_template_certificate($certificate_id, $courseid, $temp_name, $username);
        return $return;
    }

    //set_category_role
    public static function set_category_role_parameters() {
        return new external_function_parameters(
                    array(
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'categoryid' => new external_value(PARAM_INT, 'category id'),
                        'data' => new external_value(PARAM_RAW, 'data post'),
                    )
                );
    }
    public static function set_category_role_returns() {
        return new external_single_structure(
                 array(
                     'status' => new external_value(PARAM_BOOL, 'status'),
                     'message' => new external_value(PARAM_TEXT, 'message'),
                 )
                );
    }
    
    public static function set_category_role($username, $categoryid, $data) {
        global $DB;
        $params = self::validate_parameters(self::set_category_role_parameters(), array('username'=>$username, 'categoryid'=>$categoryid, 'data' => $data));
        $auth = new auth_plugin_joomdle();
        $return = $auth->set_user_category_role($username, $categoryid, $data);
        return $return;
    }

    //get_feedback_detail
    public static function get_feedback_detail_parameters() {
        return new external_function_parameters(
                    array(
                        'id' => new external_value(PARAM_INT, 'feedbackid')
                    )
                );
    }

    public static function get_feedback_detail_returns() {
        return new external_single_structure(
                array(
                    'status' => new external_value(PARAM_BOOL, 'status'),
                    'message' => new external_value(PARAM_TEXT, 'message'),
                    'feedback' => new external_single_structure(
                            array(
                                'feedbackid' => new external_value(PARAM_INT, 'feedback id'),
                                'feedbackname' => new external_value(PARAM_TEXT, 'feedback name'),
                                'feedbackintro' => new external_value(PARAM_TEXT, 'feedback intro'),
                                'rate' => new external_value(PARAM_INT, 'likert scale'),
                                'feedback_question' => 
                                    new external_multiple_structure (
                                        new external_single_structure(
                                            array(
                                                'id' => new external_value(PARAM_INT, 'question id'),
                                                'survey_id' => new external_value(PARAM_INT, 'survey id'),
                                                'type_id' => new external_value(PARAM_INT, 'type id'),
                                                'name' => new external_value(PARAM_TEXT, 'question name'),
                                                'content' => new external_value(PARAM_TEXT, 'question content'),
                                                'position' => new external_value(PARAM_INT, 'question position'),
                                                'length' => new external_value(PARAM_INT, 'likert scale'),
                                                'deleted' => new external_value(PARAM_TEXT, 'deleted', false),
                                                'params' => new external_value(PARAM_RAW, 'question params'),
                                            )
                                        )
                                      )
                            )
                            )
                )
                );
    }
    public static function get_feedback_detail($feedbackid) {
        global $DB;
        $params = self::validate_parameters(self::get_feedback_detail_parameters(), array('id' => $feedbackid));
        $auth = new auth_plugin_joomdle();
        $return = $auth->feedback_detail($feedbackid);
        return $return;
    }
    
    //edit_detail_feedback
    public static function edit_detail_feedback_parameters() {
        return new external_function_parameters(
                array(
                        'username' => new external_value(PARAM_TEXT, 'username'),
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                        'data_feedback' => new external_value(PARAM_RAW, 'feedback data'),
                        'data_question' => new external_value(PARAM_RAW, 'feedback data'),
                    )
                );
    }
    
    public static function edit_detail_feedback_returns() {
        return new external_single_structure(
                 array(
                     'status' => new external_value(PARAM_BOOL, 'status'),
                     'message' => new external_value(PARAM_TEXT, 'message'),
                 )
                );
    }

    public static function edit_detail_feedback($username, $courseid, $data_feedback, $data_question) {
        global $DB;
        $params = self::validate_parameters(self::edit_detail_feedback_parameters(), array('username' => $username, 'courseid'=>$courseid, 'data_feedback' => $data_feedback, 'data_question'=>$data_question));
        $auth = new auth_plugin_joomdle();
        $return = $auth->edit_detail_feedback($username, $courseid, $data_feedback, $data_question);
        return $return;
    }

    /* get_manage_page_data */
    public static function get_manage_page_data_parameters() {
        return new external_function_parameters(
            array(
                'username' => new external_value(PARAM_TEXT, 'Username')
            )
        );
    }

    public static function get_manage_page_data_returns() {
        return new external_single_structure(
            array(
                'mycategory' => new external_value(PARAM_RAW, 'mycategory'),
                'cursos' => new external_value(PARAM_RAW, 'cursos'),
                'courses_app' => new external_value(PARAM_RAW, 'courses_app'),
                'macourses' => new external_value(PARAM_RAW, 'macourses'),
                'courses' => new external_value(PARAM_RAW, 'courses', false),
            )
        );
    }

    public static function get_manage_page_data($username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_manage_page_data_parameters(), array('username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_manage_page_data ($username);

        return $return;
    }

    /* get_course_page_data */
    public static function get_course_page_data_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'course id'),
                'username' => new external_value(PARAM_TEXT, 'Username')
            )
        );
    }

    public static function get_course_page_data_returns() {
        return new external_single_structure(
            array(
                'userRoles' => new external_value(PARAM_RAW, 'userRoles'),
                'course_info' => new external_value(PARAM_RAW, 'course_info'),
                'mods' => new external_value(PARAM_RAW, 'mods'),
                'progressmods' => new external_value(PARAM_RAW, 'progress mods'),
                'topics' => new external_value(PARAM_RAW, 'topics'),
                'modsForCreator' => new external_value(PARAM_RAW, 'modsForCreator', false),
                'questions' => new external_value(PARAM_RAW, 'questions', false),
                'checkCompleteFeedback' => new external_value(PARAM_BOOL, 'complete FeedBack', false),
            )
        );
    }

    public static function get_course_page_data($id, $username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_page_data_parameters(), array('id'=>$id, 'username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_page_data ($id, $username);

        return $return;
    }

    /* get_course_content_page_data */
    public static function get_course_content_page_data_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'course id'),
                'username' => new external_value(PARAM_TEXT, 'Username')
            )
        );
    }

    public static function get_course_content_page_data_returns() {
        return new external_single_structure(
            array(
                'userRoles' => new external_value(PARAM_RAW, 'userRoles'),
                'course_info' => new external_value(PARAM_RAW, 'course_info'),
                'coursemods' => new external_value(PARAM_RAW, 'coursemods'),
                'mods_not_visible' => new external_value(PARAM_RAW, 'mods_not_visible'),
                'questions' => new external_value(PARAM_RAW, 'questions'),
            )
        );
    }

    public static function get_course_content_page_data($id, $username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_course_content_page_data_parameters(), array('id'=>$id, 'username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_course_content_page_data ($id, $username);

        return $return;
    }

    /* change_course_into_lpcourse */
    public static function change_course_into_lpcourse_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'courseid'),
                'categoryid' => new external_value(PARAM_RAW, 'categoryid array'),
                'username' => new external_value(PARAM_TEXT, 'Username')
            )
        );
    }

    public static function change_course_into_lpcourse_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_INT, 'error'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'data' => new external_value(PARAM_RAW, 'data'),
            )
        );
    }

    public static function change_course_into_lpcourse($courseid, $categoryid, $username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::change_course_into_lpcourse_parameters(), array('courseid'=>$courseid, 'categoryid'=>$categoryid, 'username'=>$username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->change_course_into_lpcourse ($courseid, $categoryid, $username);

        return $return;
    }

    /* get_quiz_attempt_info */
    public static function get_quiz_attempt_info_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'attempt id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
            )
        );
    }

    public static function get_quiz_attempt_info_returns() {
        return new external_value(PARAM_RAW, 'json data');
    }

    public static function get_quiz_attempt_info($id, $username) {
        global $CFG, $DB;
 
        $params = self::validate_parameters(self::get_quiz_attempt_info_parameters(), array('id'=>$id, 'username' => $username));
 
        $auth = new  auth_plugin_joomdle ();
        $return = $auth->get_quiz_attempt_info ($id, $username);


        return $return;
    }
    
    public static function get_course_header($id, $username)
    {
        global $CFG;
        $params = self::validate_parameters(self::get_course_header_parameters(), array('id'=>$id, 'username'=>$username));
        $auth = new auth_plugin_joomdle();
        $return = $auth->get_course_header($id, $username);
        return $return;
    }
    
    public static function get_course_header_parameters()
    {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'course id'),
                'username' => new external_value(PARAM_TEXT, 'username'),
            )
        );
    }
    
    public static function get_course_header_returns()
    {
        return new external_single_structure(
                    array(
                       'status' => new external_value(PARAM_BOOL, 'status'), 
                       'message' => new external_value(PARAM_TEXT, 'message'), 
                       'data' => new external_single_structure(
                                    array(
                                        'coursename' => new external_value(PARAM_TEXT, 'course name'),
                                        'courseimage' => new external_value(PARAM_RAW, 'course image'),
                                        'course_group' => new external_value(PARAM_INT, 'course circle'),
                                        'isFacilitator' => new external_value(PARAM_BOOL, 'is Facilitator course'),
                                        'isOwner' => new external_value(PARAM_BOOL, 'is Owner course'),
                                        'courseprogress' => new external_value(PARAM_INT, 'course progress')
                                    )
                                )
                    )
                );
    }
    
    public static function get_course_progressBar($id, $username)
    {
        global $DB;
        $params = self::validate_parameters(self::get_course_progressBar_parameters(), array('id' => $id, 'username' => $username));
        $auth = new auth_plugin_joomdle();
        $returns = $auth->get_course_progressBar($id, $username);
        return $returns;
    }
    
    public static function get_course_progressBar_parameters()
    {
        return new external_function_parameters(
                    array(
                        'id' => new external_value(PARAM_INT, 'course id'),
                        'username' => new external_value(PARAM_TEXT, 'username'),
                    )
                );
    }
    
    public static function get_course_progressBar_returns()
    {
        return new external_single_structure(
                    array(
                        'status' => new external_value(PARAM_BOOL, 'status'),
                        'message' => new external_value(PARAM_TEXT, 'message'),
                        'data' => new external_single_structure(
                                  array(
                                      'progress' => new external_value(PARAM_INT, 'Progress of course'),
                                  )  
                                ),
                    )
                );
    }
    
    public static function get_course_report_parameters() {
        return new external_function_parameters(
                    array(
                        'courseid' => new external_value(PARAM_INT, 'Course id'),
                    )
                );
    }
    
    public static function get_course_report($courseid) {
        global $DB;
        $params = self::validate_parameters(self::get_course_report_parameters(), array('courseid' => $courseid));
        $auth = new auth_plugin_joomdle();
        $returns = $auth->get_course_report($courseid);
        return $returns;
    }
    
    public static function get_course_report_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'data' => new external_single_structure(
                    array(
                        'courseinfo' => 
                            new external_single_structure(
                                array(
                                    'remoteid' => new external_value(PARAM_TEXT, 'course id'),
                                    'sortorder' => new external_value(PARAM_TEXT, 'sortorder'),
                                    'fullname' => new external_value(PARAM_TEXT, 'course name'),
                                    'shortname' => new external_value(PARAM_TEXT, 'course shortname'),
                                    'summary' => new external_value(PARAM_RAW, 'summary'),
                                    'startdate' => new external_value(PARAM_TEXT, 'start date'),
                                    'creator' => new external_value(PARAM_TEXT, 'creator'),
                                    'created' => new external_value(PARAM_TEXT, 'created'),
                                    'modified' => new external_value(PARAM_TEXT, 'modified'),
                                    'timepublish' => new external_value(PARAM_INT, 'timepublish', false),
                                    'course_img' => new external_value(PARAM_TEXT, 'file path', VALUE_OPTIONAL),
                                    'coursetype' => new external_value(PARAM_TEXT, 'course type', VALUE_OPTIONAL),
                                    'count_learner' => new external_value(PARAM_INT, 'Count Learner'),
                                    'count_completed' => new external_value(PARAM_INT, 'Count Completed'),
                                    'course_manager' => new external_value(PARAM_TEXT, 'List manager of course', false),
                                    'course_facilitator' => new external_value(PARAM_TEXT, 'List facilitator of course', false),
                                    'average_time_spent' => new external_value(PARAM_FLOAT, 'Average time spent on the course', false),
                                )
                            
                        ),
                        'membersinfo' => new external_multiple_structure (
                            new external_single_structure(
                                array(
                                    'username' => new external_value(PARAM_TEXT, 'username of member'),
                                    'enrol_time' => new external_value(PARAM_TEXT, 'Start date'),
                                    'last_access' => new external_value(PARAM_INT, 'last access', VALUE_OPTIONAL),
                                    'time_completed' => new external_value(PARAM_INT, 'time completed', VALUE_OPTIONAL),
                                    'time_spent' => new external_value(PARAM_FLOAT, 'Total time spent'),
                                    'overall_grade' => new external_value(PARAM_TEXT, 'Overall grade'),
                                )
                            )
                        ),
                        'feedbackinfo' => new external_multiple_structure (
                            new external_single_structure(
                                array(
                                    'username' => new external_value(PARAM_TEXT, 'Username of member'),
                                    'rank_arr' => new external_multiple_structure (
                                        new external_single_structure(
                                            array(
                                                'name' => new external_value(PARAM_TEXT, 'Name question'),
                                                'feedback' => new external_value(PARAM_RAW, 'Rank and Response'),
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );
    }
    
    public static function get_courses_bln_parameters() {
        return new external_function_parameters(
                    array(
                        'blncategoryid' => new external_value(PARAM_INT, 'BLN Moodle Category id'),
                        'scourse' => new external_value(PARAM_TEXT, 'text search course', flase),
                        'limitstart' => new external_value(PARAM_INT, 'limit start', false),
                        'limit' => new external_value(PARAM_INT, 'limit', false),
                    )
                );
    }
    
    public static function get_courses_bln($blncategoryid, $scourse, $limitstart, $limit) {
        global $DB;
        $params = self::validate_parameters(self::get_courses_bln_parameters(), array('blncategoryid' => $blncategoryid, 'scourse' => $scourse, 'limitstart' => $limitstart, 'limit' => $limit));
        $auth = new auth_plugin_joomdle();
        $returns = $auth->get_courses_bln($blncategoryid, $scourse, $limitstart, $limit);
        return $returns;
    }
    
    public static function get_courses_bln_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'totalcourses' => new external_value(PARAM_INT, 'Total courses'),
                'data' => new external_multiple_structure (
                        new external_single_structure(
                            array(
                                'remoteid' => new external_value(PARAM_TEXT, 'course id'),
                                'sortorder' => new external_value(PARAM_TEXT, 'sortorder'),
                                'fullname' => new external_value(PARAM_TEXT, 'course name'),
                                'shortname' => new external_value(PARAM_TEXT, 'course shortname'),
                                'summary' => new external_value(PARAM_RAW, 'summary'),
                                'startdate' => new external_value(PARAM_TEXT, 'start date'),
                                'creator' => new external_value(PARAM_TEXT, 'creator'),
                                'created' => new external_value(PARAM_TEXT, 'created'),
                                'modified' => new external_value(PARAM_TEXT, 'modified'),
                                'timepublish' => new external_value(PARAM_INT, 'timepublish', false),
                                'course_img' => new external_value(PARAM_TEXT, 'file path', VALUE_OPTIONAL),
                                'coursetype' => new external_value(PARAM_TEXT, 'course type', VALUE_OPTIONAL),
                                'count_learner' => new external_value(PARAM_INT, 'Count Learner'),
                                'count_completed' => new external_value(PARAM_INT, 'Count Completed'),
                                'course_manager' => new external_value(PARAM_TEXT, 'List manager of course', false),
                                'course_facilitator' => new external_value(PARAM_TEXT, 'List facilitator of course', false),
                            )
                        )
                )
            )
        );
    }
    
    public static function get_user_report_parameters() {
        return new external_function_parameters(
                    array(
                        'username' => new external_value(PARAM_TEXT, 'Username of user'),
                    )
                );
    }
    
    public static function get_user_report($username) {
        global $DB;
        $params = self::validate_parameters(self::get_user_report_parameters(), array('username' => $username));
        $auth = new auth_plugin_joomdle();
        $returns = $auth->get_user_report($username);
        return $returns;
    }
    
    public static function get_user_report_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'data' => new external_single_structure(
                        array(
                            'courses_created' => new external_multiple_structure (
                                new external_single_structure(
                                    array(
                                        'remoteid' => new external_value(PARAM_TEXT, 'course id'),
                                        'fullname' => new external_value(PARAM_TEXT, 'course name'),
                                        'shortname' => new external_value(PARAM_TEXT, 'course shortname'),
                                        'creator' => new external_value(PARAM_TEXT, 'creator'),
                                        'timepublish' => new external_value(PARAM_INT, 'timepublish', false),
                                    )
                                )
                            ),
                            'courses_completed' => new external_multiple_structure (
                                new external_single_structure(
                                    array(
                                        'remoteid' => new external_value(PARAM_TEXT, 'course id'),
                                        'fullname' => new external_value(PARAM_TEXT, 'course name'),
                                        'shortname' => new external_value(PARAM_TEXT, 'course shortname'),
                                        'creator' => new external_value(PARAM_TEXT, 'creator'),
                                        'timepublish' => new external_value(PARAM_INT, 'timepublish', false),
                                        'overall_grade' => new external_value(PARAM_TEXT, 'Overall grade', false),
                                    )
                                )
                            ),
                            'no_course_enrolled' => new external_value(PARAM_INT, 'No. of course enrolled', false),
                        )
                    )
            )
        );
    }
    
    public static function get_courses_byids_parameters() {
        return new external_function_parameters(
                    array(
                        'courseids' => new external_value(PARAM_TEXT, 'List course id'),
                    )
                );
    }
    
    public static function get_courses_byids($courseids) {
        global $DB;
        $params = self::validate_parameters(self::get_courses_byids_parameters(), array('courseids' => $courseids));
        $auth = new auth_plugin_joomdle();
        $returns = $auth->getCoursesByIds($courseids);
        return $returns;
    }
    
    public static function get_courses_byids_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status'),
                'message' => new external_value(PARAM_TEXT, 'message'),
                'data' => new external_multiple_structure (
                        new external_single_structure(
                        array(
                            'remoteid' => new external_value(PARAM_TEXT, 'course id'),
                            'fullname' => new external_value(PARAM_TEXT, 'course name'),
                        )
                                
                    )
                )
            )
        );
    }
}
?>
