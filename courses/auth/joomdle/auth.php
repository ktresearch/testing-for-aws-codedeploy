<?php

/**
 * @author Antonio Duran
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package moodle multiauth
 *
 * Authentication Plugin: Joomdle
 *
 * Checks against Joomla web services provided my Joomdle
 *
 * 2009-10-25  File created.
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir.'/authlib.php');
require_once($CFG->dirroot.'/auth/manual/auth.php');
//require_once($CFG->dirroot.'/search/lib.php');
require_once($CFG->dirroot.'/calendar/lib.php');
require_once($CFG->dirroot.'/mod/forum/lib.php');
require_once($CFG->dirroot.'/lib/datalib.php');
require_once($CFG->dirroot.'/lib/gdlib.php');
require_once($CFG->dirroot.'/lib/grade/grade_grade.php');
require_once($CFG->dirroot.'/lib/grade/grade_item.php');
require_once($CFG->dirroot.'/lib/gradelib.php');
require_once $CFG->dirroot.'/grade/lib.php';
require_once("$CFG->dirroot/enrol/locallib.php");
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/group/lib.php');
require_once($CFG->dirroot.'/lib/grouplib.php');
require_once($CFG->dirroot.'/cohort/lib.php');
require_once $CFG->dirroot.'/grade/report/user/lib.php';



/**
 * Joomdle authentication plugin.
 */
class auth_plugin_joomdle extends auth_plugin_manual {

    /**
     * Constructor.
     */
    function auth_plugin_joomdle() {
        $this->authtype = 'joomdle';
        $this->config = get_config('auth/joomdle');
    }

    function can_signup() {
        return true;
    }

    function user_signup($user, $notify=true) {
        global $CFG, $DB;
        require_once($CFG->dirroot.'/user/profile/lib.php');


        $password_clear = $user->password;
        $user->password = hash_internal_user_password($user->password);

        if (! ($user->id = $DB->insert_record('user', $user)) ) {
            print_error('auth_emailnoinsert','auth');
        }

        /// Save any custom profile field information
        profile_save_data($user);

        $conditions = array ('id' => $user->id);
        $user = $DB->get_record('user', $conditions);

        /* Create user in Joomla */
        $userinfo['username'] = $user->username;
        $userinfo['password'] = $password_clear;
        $userinfo['password2'] = $password_clear;
        $userinfo['name'] = $user->firstname. " " . $user->lastname;
        $userinfo['email'] = $user->email;
        $userinfo['block'] = 1;

        $this->call_method ("createUser", $userinfo);

        events_trigger('user_created', $user);

        if (! send_confirmation_email($user)) {
            print_error('auth_emailnoemail','auth');
        }

        if ($notify) {
            global $CFG;
            $emailconfirm = get_string('emailconfirm');
            $navlinks = array();
            $navlinks[] = array('name' => $emailconfirm, 'link' => null, 'type' => 'misc');
            $navigation = build_navigation($navlinks);

            print_header($emailconfirm, $emailconfirm, $navigation);
            notice(get_string('emailconfirmsent', '', $user->email), "$CFG->wwwroot/index.php");
        } else {
            return true;
        }
    }

    function can_confirm() {
        return true;
    }

    function user_confirm($username, $confirmsecret = null) {
        global $DB;

        $user = get_complete_user_data('username', $username);

        if (!empty($user)) {
            if ($user->confirmed) {
                return AUTH_CONFIRM_ALREADY;

            } else if ($user->auth != 'joomdle') {
                return AUTH_CONFIRM_ERROR;

            } else if ($user->secret == stripslashes($confirmsecret)) {   // They have provided the secret key to get in
                $conditions = array ('id' => $user->id);
                if (!$DB->set_field("user", "confirmed", 1, $conditions)) {
                    return AUTH_CONFIRM_FAIL;
                }
                if (!$DB->set_field("user", "firstaccess", time(), $conditions)) {
                    return AUTH_CONFIRM_FAIL;
                }

                /* Enable de user in Joomla */
                $this->call_method ("activateUser", $username);

                return AUTH_CONFIRM_OK;
            }
        } else {
            return AUTH_CONFIRM_ERROR;
        }
    }


    /**
     * Returns true if the username and password work and false if they are
     * wrong or don't exist.
     *
     * @param string $username The username (with system magic quotes)
     * @param string $password The password (with system magic quotes)
     *
     * @return bool Authentication success or failure.
     */
    function user_login($username, $password) {

        if (!$password)
            return false;

        $user = get_complete_user_data ('username', $username);

        if (!$user)
            return false;

        $logged = $this->call_method ("login", $username, $password);

        return $logged;
    }

    /**
     * Returns true if this authentication plugin is 'internal'.
     *
     * @return bool
     */
    function is_internal() {
        return true;
    }


    function can_change_password() {
        return true;
    }

    function user_update_password ($user, $password) {
        $return =  $this->call_method ("changePassword", $user->username, $password);

        return true;
        //  return $return;
    }

    /**
     * Prints a form for configuring this authentication plugin.
     *
     * This function is called from admin/auth.php, and outputs a full page with
     * a form for configuring this plugin.
     *
     * @param array $page An object containing all the data for this page.
     */
    function config_form($config, $err, $user_fields) {
        include "config.html";
    }

    function process_config($config) {
        global $DB;

        if (!isset($config->joomla_url)) {
            $config->joomla_url = '';
        }
        set_config('joomla_url', $config->joomla_url, 'auth/joomdle');

        if (!isset($config->connection_method)) {
            $config->connection_method = 'fgc';
        }
        set_config('connection_method', $config->connection_method, 'auth/joomdle');

        if (!isset($config->joomla_auth_token)) {
            $config->joomla_auth_token = '';
        }
        set_config('joomla_auth_token', $config->joomla_auth_token, 'auth/joomdle');

        if (!isset($config->sync_to_joomla)) {
            $config->sync_to_joomla = 0;
        }
        set_config('sync_to_joomla', $config->sync_to_joomla, 'auth/joomdle');

        if (!isset($config->joomla_lang)) {
            $config->joomla_lang = '';
        }
        set_config('joomla_lang', $config->joomla_lang, 'auth/joomdle');

        if (!isset($config->joomla_sef)) {
            $config->joomla_sef = '';
        }
        set_config('joomla_sef', $config->joomla_sef, 'auth/joomdle');

        if (!isset($config->redirectless_sso)) {
            $config->redirectless_sso = '';
        }
        set_config('redirectless_sso', $config->redirectless_sso, 'auth/joomdle');

        if (!isset($config->logout_redirect_to_joomla)) {
            $config->logout_redirect_to_joomla = '';
        }
        set_config('logout_redirect_to_joomla', $config->logout_redirect_to_joomla, 'auth/joomdle');

        if (!isset($config->jomsocial_activities)) {
            $config->jomsocial_activities = 0;
        }
        set_config('jomsocial_activities', $config->jomsocial_activities, 'auth/joomdle');

        if (!isset($config->jomsocial_groups)) {
            $config->jomsocial_groups = 0;
        }
        set_config('jomsocial_groups', $config->jomsocial_groups, 'auth/joomdle');

        if (!isset($config->jomsocial_groups_delete)) {
            $config->jomsocial_groups_delete = 0;
        }
        set_config('jomsocial_groups_delete', $config->jomsocial_groups_delete, 'auth/joomdle');

        if (!isset($config->auto_sell)) {
            $config->auto_sell = 0;
        }
        set_config('auto_sell', $config->auto_sell, 'auth/joomdle');

        if (!isset($config->enrol_parents)) {
            $config->enrol_parents = 0;
        }
        set_config('enrol_parents', $config->enrol_parents, 'auth/joomdle');

        if (!isset($config->parent_role_id)) {
            $config->parent_role_id = '';
        }
        set_config('parent_role_id', $config->parent_role_id, 'auth/joomdle');

        if (!isset($config->give_points)) {
            $config->give_points = 0;
        }
        set_config('give_points', $config->give_points, 'auth/joomdle');

        if (!isset($config->auto_mailing_lists)) {
            $config->auto_mailing_lists = 0;
        }
        set_config('auto_mailing_lists', $config->auto_mailing_lists, 'auth/joomdle');

        if (!isset($config->joomla_user_groups)) {
            $config->joomla_user_groups = 0;
        }
        set_config('joomla_user_groups', $config->joomla_user_groups, 'auth/joomdle');

        if (!isset($config->use_kunena_forums)) {
            $config->use_kunena_forums = 0;
        }
        set_config('use_kunena_forums', $config->use_kunena_forums, 'auth/joomdle');


        $handler = new stdClass();
        if (($config->jomsocial_activities) || ($config->jomsocial_groups) || ($config->enrol_parents) || ($config->auto_sell) || ($config->auto_mailing_lists) || ($config->joomla_user_groups)
            || ($config->use_kunena_forums))
        {
            /* Insert the event handlers */
            $conditions = array ('eventname' => 'course_created', 'component' => 'joomdle');
            if (!$DB->record_exists ('events_handlers', $conditions))
            {
                $event = 'course_created';
                $handler->eventname = $event;
                $handler->component = 'joomdle';
                $handler->handlerfile = '/auth/joomdle/auth.php';
                $handler->handlerfunction = serialize ('joomdle_'.$event);
                $handler->schedule = 'instant';
                $handler->status = 0;

                $DB->insert_record ('events_handlers', $handler);
            }
            $conditions = array ('eventname' => 'course_deleted', 'component' => 'joomdle');
            if (!$DB->record_exists ('events_handlers', $conditions))
            {
                $event = 'course_deleted';
                $handler->eventname = $event;
                $handler->component = 'joomdle';
                $handler->handlerfile = '/auth/joomdle/auth.php';
                $handler->handlerfunction = serialize ('joomdle_'.$event);
                $handler->schedule = 'instant';
                $handler->status = 0;

                $DB->insert_record ('events_handlers', $handler);
            }
            $conditions = array ('eventname' => 'role_assigned', 'component' => 'joomdle');
            if (!$DB->record_exists ('events_handlers', $conditions))
            {
                $event = 'role_assigned';
                $handler->eventname = $event;
                $handler->component = 'joomdle';
                $handler->handlerfile = '/auth/joomdle/auth.php';
                $handler->handlerfunction = serialize ('joomdle_'.$event);
                $handler->schedule = 'instant';
                $handler->status = 0;

                $DB->insert_record ('events_handlers', $handler);
            }
            $conditions = array ('eventname' => 'role_unassigned', 'component' => 'joomdle');
            if (!$DB->record_exists ('events_handlers', $conditions))
            {
                $event = 'role_unassigned';
                $handler->eventname = $event;
                $handler->component = 'joomdle';
                $handler->handlerfile = '/auth/joomdle/auth.php';
                $handler->handlerfunction = serialize ('joomdle_'.$event);
                $handler->schedule = 'instant';
                $handler->status = 0;

                $DB->insert_record ('events_handlers', $handler);
            }
            $conditions = array ('eventname' => 'course_updated', 'component' => 'joomdle');
            if (!$DB->record_exists ('events_handlers', $conditions))
            {
                $event = 'course_updated';
                $handler->eventname = $event;
                $handler->component = 'joomdle';
                $handler->handlerfile = '/auth/joomdle/auth.php';
                $handler->handlerfunction = serialize ('joomdle_'.$event);
                $handler->schedule = 'instant';
                $handler->status = 0;

                $DB->insert_record ('events_handlers', $handler);
            }

            $conditions = array ('eventname' => 'quiz_attempt_submitted', 'component' => 'joomdle');
            if (!$DB->record_exists ('events_handlers', $conditions))
            {
                $event = 'quiz_attempt_submitted';
                $handler->eventname = $event;
                $handler->component = 'joomdle';
                $handler->handlerfile = '/auth/joomdle/auth.php';
                $handler->handlerfunction = serialize ('joomdle_'.$event);
                $handler->schedule = 'instant';
                $handler->status = 0;

                $DB->insert_record ('events_handlers', $handler);
            }

            $conditions = array ('eventname' => 'mod_created', 'component' => 'joomdle');
            if (!$DB->record_exists ('events_handlers', $conditions))
            {
                $event = 'mod_created';
                $handler->eventname = $event;
                $handler->component = 'joomdle';
                $handler->handlerfile = '/auth/joomdle/auth.php';
                $handler->handlerfunction = serialize ('joomdle_'.$event);
                $handler->schedule = 'instant';
                $handler->status = 0;

                $DB->insert_record ('events_handlers', $handler);
            }

            $conditions = array ('eventname' => 'mod_deleted', 'component' => 'joomdle');
            if (!$DB->record_exists ('events_handlers', $conditions))
            {
                $event = 'mod_deleted';
                $handler->eventname = $event;
                $handler->component = 'joomdle';
                $handler->handlerfile = '/auth/joomdle/auth.php';
                $handler->handlerfunction = serialize ('joomdle_'.$event);
                $handler->schedule = 'instant';
                $handler->status = 0;

                $DB->insert_record ('events_handlers', $handler);
            }

            $conditions = array ('eventname' => 'mod_updated', 'component' => 'joomdle');
            if (!$DB->record_exists ('events_handlers', $conditions))
            {
                $event = 'mod_updated';
                $handler->eventname = $event;
                $handler->component = 'joomdle';
                $handler->handlerfile = '/auth/joomdle/auth.php';
                $handler->handlerfunction = serialize ('joomdle_'.$event);
                $handler->schedule = 'instant';
                $handler->status = 0;

                $DB->insert_record ('events_handlers', $handler);
            }

            $conditions = array ('eventname' => 'course_completed', 'component' => 'joomdle');
            if (!$DB->record_exists ('events_handlers', $conditions))
            {
                $event = 'course_completed';
                $handler->eventname = $event;
                $handler->component = 'joomdle';
                $handler->handlerfile = '/auth/joomdle/auth.php';
                $handler->handlerfunction = serialize ('joomdle_'.$event);
                $handler->schedule = 'instant';
                $handler->status = 0;

                $DB->insert_record ('events_handlers', $handler);
            }
        }
        else
        {
            /* Delete the event handlers */
            $conditions = array ('eventname' => 'course_created', 'component' => 'joomdle');
            $DB->delete_records ('events_handlers', $conditions);
            $conditions = array ('eventname' => 'course_deleted', 'component' => 'joomdle');
            $DB->delete_records ('events_handlers', $conditions);
            $conditions = array ('eventname' => 'role_assigned', 'component' => 'joomdle');
            $DB->delete_records ('events_handlers', $conditions);
            $conditions = array ('eventname' => 'role_unassigned', 'component' => 'joomdle');
            $DB->delete_records ('events_handlers', $conditions);
            $conditions = array ('eventname' => 'course_updated', 'component' => 'joomdle');
            $DB->delete_records ('events_handlers', $conditions);
            $conditions = array ('eventname' => 'course_completed', 'component' => 'joomdle');
            $DB->delete_records ('events_handlers', $conditions);
        }

        if ($config->sync_to_joomla)
        {
            /* Insert the event handlers */
            $conditions = array ('eventname' => 'user_created', 'component' => 'joomdle');
            if (!$DB->record_exists ('events_handlers', $conditions))
            {
                $event = 'user_created';
                $handler->eventname = $event;
                $handler->component = 'joomdle';
                $handler->handlerfile = '/auth/joomdle/auth.php';
                $handler->handlerfunction = serialize ('joomdle_'.$event);
                $handler->schedule = 'instant';
                $handler->status = 0;

                $DB->insert_record ('events_handlers', $handler);
            }
            $conditions = array ('eventname' => 'user_updated', 'component' => 'joomdle');
            if (!$DB->record_exists ('events_handlers', $conditions))
            {
                $event = 'user_updated';
                $handler->eventname = $event;
                $handler->handlermodule = 'auth/joomdle';
                $handler->component = 'joomdle';
                $handler->handlerfunction = serialize ('joomdle_'.$event);
                $handler->schedule = 'instant';
                $handler->status = 0;

                $DB->insert_record ('events_handlers', $handler);
            }
            $conditions = array ('eventname' => 'user_deleted', 'component' => 'joomdle');
            if (!$DB->record_exists ('events_handlers', $conditions))
            {
                $event = 'user_deleted';
                $handler->eventname = $event;
                $handler->handlermodule = 'auth/joomdle';
                $handler->component = 'joomdle';
                $handler->handlerfunction = serialize ('joomdle_'.$event);
                $handler->schedule = 'instant';
                $handler->status = 0;

                $DB->insert_record ('events_handlers', $handler);
            }
        }
        else
        {
            /* Delete the event handlers */
            $conditions = array ('eventname' => 'user_created', 'component' => 'joomdle');
            $DB->delete_records ('events_handlers', $conditions);
            $conditions = array ('eventname' => 'user_updated', 'component' => 'joomdle');
            $DB->delete_records ('events_handlers', $conditions);
            $conditions = array ('eventname' => 'user_deleted', 'component' => 'joomdle');
            $DB->delete_records ('events_handlers', $conditions);
        }

        return true;
    }

    function _get_xmlrpc_url () {
        $joomla_lang = get_config('auth/joomdle', 'joomla_lang');
        $joomla_sef = get_config('auth/joomdle', 'joomla_sef');
        $joomla_auth_token = get_config('auth/joomdle', 'joomla_auth_token');

        if ($joomla_lang == '')
            $joomla_xmlrpc_server_url = get_config ('auth/joomdle', 'joomla_url').'/index.php?option=com_joomdle&task=ws.server&format=xmlrpc';
        else
            if ($joomla_sef)
                $joomla_xmlrpc_server_url = get_config ('auth/joomdle', 'joomla_url').'/index.php/'.$joomla_lang.'/?option=com_joomdle&task=ws.server&format=xmlrpc';
            else
                $joomla_xmlrpc_server_url = get_config ('auth/joomdle', 'joomla_url').'/index.php?lang='.$joomla_lang.'&option=com_joomdle&task=ws.server&format=xmlrpc';

        // Add auth token
        $joomla_xmlrpc_server_url .= "&token=" . $joomla_auth_token;

        return $joomla_xmlrpc_server_url;
    }


    function call_method ($method, $params = '', $params2 = '', $params3 = '' , $params4 = '', $params5 = '') {
        $connection_method = get_config('auth/joomdle', 'connection_method');

        if ($connection_method == 'fgc')
            $response = $this->call_method_fgc ($method, $params, $params2, $params3, $params4, $params5);
        else
            $response = $this->call_method_curl ($method, $params, $params2, $params3, $params4, $params5);

        return $response;
    }

    function call_method_fgc ($method, $params = '', $params2 = '', $params3 = '' , $params4 = '', $params5 = '') {
        $joomla_xmlrpc_url = $this->_get_xmlrpc_url ();

        $options = array ();

        if ($params == '')
            $request = xmlrpc_encode_request("joomdle.".$method, array (), $options);
        else if ($params2 == '')
            $request = xmlrpc_encode_request("joomdle.".$method, array ($params), $options);
        else if ($params3 == '')
            $request = xmlrpc_encode_request("joomdle.".$method, array ($params, $params2), $options);
        else if ($params4 == '')
            $request = xmlrpc_encode_request("joomdle.".$method, array ($params, $params2, $params3), $options);
        else if ($params5 == '')
            $request = xmlrpc_encode_request("joomdle.".$method, array ($params, $params2, $params3, $params4), $options);
        else
            $request = xmlrpc_encode_request("joomdle.".$method, array ($params, $params2, $params3, $params4, $params5), $options);

        $context = stream_context_create(array('http' => array(
            'method' => "POST",
            'header' => "Content-Type: text/xml ",
            'content' => $request
        )));
        $response = file_get_contents($joomla_xmlrpc_url, false, $context);

        $data = xmlrpc_decode($response);

        if (is_array ($data))
            if (xmlrpc_is_fault ($data))
            {
                return  "XML-RPC Error (".$data['faultCode']."): ".$data['faultString'];
            }

        return $data;
    }

    function call_method_curl ($method, $params = '', $params2 = '', $params3 = '' , $params4 = '', $params5 = '') {
        global $CFG;

        $joomla_xmlrpc_url = $this->_get_xmlrpc_url ();


        $options = array ();

        if ($params == '')
            $request = xmlrpc_encode_request("joomdle.".$method, array (), $options);
        else if ($params2 == '')
            $request = xmlrpc_encode_request("joomdle.".$method, array ($params), $options);
        else if ($params3 == '')
            $request = xmlrpc_encode_request("joomdle.".$method, array ($params, $params2), $options);
        else if ($params4 == '')
            $request = xmlrpc_encode_request("joomdle.".$method, array ($params, $params2, $params3), $options);
        else if ($params5 == '')
            $request = xmlrpc_encode_request("joomdle.".$method, array ($params, $params2, $params3, $params4), $options);
        else
            $request = xmlrpc_encode_request("joomdle.".$method, array ($params, $params2, $params3, $params4, $params5), $options);

        $headers = array();
        array_push($headers,"Content-Type: text/xml");
        array_push($headers,"Content-Length: ".strlen($request));
        array_push($headers,"\r\n");

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $joomla_xmlrpc_url); # URL to post to
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 ); # return into a variable
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers ); # custom headers, see above
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $request );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' ); # This POST is special, and uses its specified Content-type

        // use proxy if one is configured
        if (!empty($CFG->proxyhost)) {
            if (empty($CFG->proxyport)) {
                curl_setopt($ch, CURLOPT_PROXY, $CFG->proxyhost);
            } else {

                curl_setopt($ch, CURLOPT_PROXY, $CFG->proxyhost.':'.$CFG->proxyport);
            }
            curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, false);
        }

        $response = curl_exec( $ch ); # run!
        curl_close($ch);

        $data = xmlrpc_decode($response);

        if (is_array ($response))
            if (xmlrpc_is_fault ($response))
            {
                return  "XML-RPC Error (".$response['faultCode']."): ".$response['faultString'];
            }

        return $data;

    }

    function get_file ($file) {
        $connection_method = get_config('auth/joomdle', 'connection_method');

        if ($connection_method == 'fgc')
            $response = file_get_contents ($file, FALSE, NULL);
        else
            $response = $this->get_file_curl ($file);

        return $response;
    }

    function get_file_curl ($file) {
        global $CFG;

        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $file);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // use proxy if one is configured
        if (!empty($CFG->proxyhost)) {
            if (empty($CFG->proxyport)) {
                curl_setopt($ch, CURLOPT_PROXY, $CFG->proxyhost);
            } else {
                curl_setopt($ch, CURLOPT_PROXY, $CFG->proxyhost.':'.$CFG->proxyport);
            }
            curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, false);
        }

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }

    function test ($file, $courseid) {
//        return "Moodle web services are working!";
        global $USER, $CFG;
        
        $tmp_file = $CFG->dataroot . '/temp/' . 'tmp_file';
        $name = basename($_FILES['content']['name']);
        if(move_uploaded_file($_FILES['content']['tmp_name'], "$tmp_file/$name")) {
            echo 'success'; 
        } else {
            echo 'upload failed';
        }

        
        $draftitemid = file_get_submitted_draft_itemid('introeditor');
        file_prepare_draft_area($draftitemid, null, null, null, null);
        $courseid = 1577;
        $context = context_course::instance($courseid);
        $fs = get_file_storage();

        $record = new stdClass();
        $record->filearea = 'draft';
        $record->component = 'user';
        $record->filepath = '/';
        $record->itemid = $draftitemid;
        $record->license = 'allrightsreserved';
        $record->author = $USER->username;
        $record->contextid = $context->id;
        $record->userid = $USER->id;
        $record->filename = $file['name'];

        $sourcefield = $record->filename;
        $record->source = repository_upload::build_source_field($sourcefield);
        //            $fs->delete_area_files($context->id);
        $stored_file = $fs->create_file_from_pathname($record, $tmp_file);
    }
    
    function load_questions_bank($courseid, $aid = null, $detail = 0) {
        global $CFG, $DB;
        $page = 0;
        $perpage = 20;
        $course = $this->get_course_info($courseid);
        $catid = $course['cat_id'];

        $contexts = array();
//        $contexts[] = context_system::instance();
//        $contexts[] = context_coursecat::instance($catid);
        $contexts[] = context_course::instance($courseid);
        require_once($CFG->libdir . '/questionlib.php');

        $pcontexts = array();
        foreach ($contexts as $context) {
            $pcontexts[] = $context->id;
        }
        $contextslist = join($pcontexts, ', ');

        $categories = get_categories_for_contexts($contextslist);
        $categoriesArr = array();

        if (empty($categories)) {
            $category = new stdClass();
            $category->contextid = context_course::instance($courseid)->id;
            $category->name = 'Default for '.$course['fullname'];
            $category->info = 'The default category for questions shared in context '.$course['fullname'];
            $category->parent = 0;
            $category->sortorder = 999;
            $category->stamp = make_unique_id_code();
            $questionCat = $DB->insert_record('question_categories', $category);
            $categoriesArr[] = $questionCat;
        } else {
            foreach ($categories as $k => $v) {
                $categoriesArr[] = $v->id;
            }
        }

        $select = (empty($categoriesArr)) ? 'parent = 0 AND hidden = 0' : 'parent = 0 AND hidden = 0 AND category IN ('.implode($categoriesArr, ',').') ';
        $sort = 'timecreated DESC';
        $q = $DB->get_records_select('question', $select, array(), $sort, 'id, category, name, questiontext, qtype, generalfeedback', $page, $perpage );

        $result = array();
         $questions = array();
         if ($aid) {
             $mod = $DB->get_record_sql("SELECT cm.instance, m.name as modname
                                   FROM {modules} m, {course_modules} cm
                                  WHERE cm.course = ? AND cm.module = m.id AND m.visible = 1 AND cm.id = ?",
                 array($courseid, $aid));
             $listq = $DB->get_record_select('quiz', 'id = '.$mod->instance, null, 'questions');
             $questions = explode(',', $listq->questions);
         }

        foreach ($q as $k => $v) {
            if (in_array($v->id, $questions)) {
                $q[$k]->selected = 1;
            } else $q[$k]->selected = 0;

            if ($detail) {
                $question = $DB->get_record('question', array('id' => $v->id));
                get_question_options($question, true);
                // if ($newformat) {
                //     if ($question->options->answers) {
                //         $answers = array();
                //         foreach ($question->options->answers as $an) {
                //             $answers[] = $an;
                //         }
                //         $question->options->answers = $answers;
                //     }
                    
                //     if ($question->options->subquestions) {
                //         $subquestions = array();
                //         foreach ($question->options->subquestions as $subq) {
                //             $subquestions[] = $subq;
                //         }
                //         $question->options->subquestions = $subquestions;
                //     }
                // }
                $q[$k]->options = json_encode($question->options);
        }
        }
        if ($q && !empty($q)) {
            $result['status'] = 1;
            $result['message'] = 'Success';
            $result['questions'] = $q;
        } else {
            $result['status'] = 0;
            $result['message'] = 'Not found';
            $result['questions'] = array();
    }

        return $result;
    }

    function delete_questions ($act, $courseid, $username, $qids) {
        global $CFG, $DB;
       
        $user = get_complete_user_data ('username', $username);
        $context = context_course::instance($courseid);
        $categorycontext = context::instance_by_id($context->id);
        $addpermission = has_capability('moodle/question:add', $categorycontext, $user);
        $course = $this->get_course_info($courseid, $username);
        $qids_arr = explode(',', $qids);

        if ($addpermission) {
            require_once($CFG->libdir . '/questionlib.php');
            if ($act == 3) {
                if (count($qids_arr) > 0) {
                    foreach ($qids_arr as $qid) {
                        if ($qid) {
                            $questionid = (int)$qid;
        //                    $questionBank = $this->load_questions_bank($courseid);
                            $question = $this->load_questions_bank($courseid);
                            if ($question['status']) {
                                $questionBank = $question['questions'];
                            } else {
                                $questionBank = array();
                            }
                            if (!array_key_exists($questionid, $questionBank)) {
                                $res['status'] = false;
                                $res['message'] = 'Question not exist';
                                return $res;
                            }
                            question_require_capability_on($questionid, 'edit', $username);
                            if (questions_in_use(array($questionid))) {
                                $DB->set_field('question', 'hidden', 1, array('id' => $questionid));
                            } else {
                                question_delete_question($questionid, $username);
                            }
                        }
                    }
                }
                $res['status'] = true;
                $res['message'] = 'remove success';
                return $res;
            }
        } else {
            $res['status'] = false;
            $res['message'] = 'no permission';
            return $res;
        }
    }

    function create_question($act, $courseid, $username, $q) {
        global $CFG, $DB;
        
        $user = get_complete_user_data ('username', $username);
        $context = context_course::instance($courseid);
        $categorycontext = context::instance_by_id($context->id);
        $addpermission = has_capability('moodle/question:add', $categorycontext, $user);
        $course = $this->get_course_info($courseid, $username);

        if ($addpermission) {
            require_once($CFG->libdir . '/questionlib.php');
            if ($act == 3) {
                if ($q['qid']) {
                    $questionid = $q['qid'];
                    $questionid = (int)$questionid;
//                    $questionBank = $this->load_questions_bank($courseid);
                    $question = $this->load_questions_bank($courseid);
                    if ($question['status']) {
                        $questionBank = $question['questions'];
                    } else {
                        $questionBank = array();
                    }
                    if (!array_key_exists($questionid, $questionBank)) {
                        $res['error'] = 1;
                        $res['mes'] = 'notexist';
                        $res['data'] = 0;
                        return $res;
                    }
                    question_require_capability_on($questionid, 'edit', $username);
                    if (questions_in_use(array($questionid))) {
                        $DB->set_field('question', 'hidden', 1, array('id' => $questionid));
                    } else {
                        question_delete_question($questionid, $username);
                    }
                }
                $res['error'] = 0;
                $res['mes'] = 'removesuccess';
                $res['data'] = 0;
                return $res;
            } else {
                $qcategories = get_categories_for_contexts($context->id);
                
                if (empty($qcategories)) {
                    $category = new stdClass();
                    $category->contextid = $context->id;
                    $category->name = 'Default for '.$course['fullname'];
                    $category->info = 'The default category for questions shared in context '.$course['fullname'];
                    $category->parent = 0;
                    $category->sortorder = 999;
                    $category->stamp = make_unique_id_code();
                    $questionCat = $DB->insert_record('question_categories', $category);

                } else {
                    foreach ($qcategories as $k => $v) {
                        if ($v->parent == 0) {
                            $questionCat = $v->id;
                            break;
                        }
                    }
                }

                $question = new stdClass();
                if ($q['qid']) $question->id = $q['qid'];
                $question->category = $questionCat;
                $question->qtype = $q['type'];
                $question->createdby = $user->id;
                $question->formoptions = (object) array(
                    'canEdit'=>true,
                    'canMove'=>true,
                    "cansaveasnew"=>true,
                    "repeatelements"=>true,
                    "movecontext"=>true,
                    "mustbeusable"=>true
                );
                $form = new stdClass();
                $form->category = $questionCat;
                $form->name = utf8_decode($q['name']);
                $form->questiontext = array(
                    'text' => utf8_decode($q['questiontext']),
                    'format' => 2
                );

                switch ($q['type']) {
                    case 'truefalse':
                        $form->generalfeedback = array(
                            'text' => utf8_decode($q['generalfeedback']),
                            'format' => 2
                        );
                        $form->feedbacktrue = array(
                            'text' => $q['feedbacktrue'],
                            'format' => 2
                        );
                        $form->feedbackfalse = array(
                            'text' => $q['feedbackfalse'],
                            'format' => 2
                        );
                        $form->defaultmark = (float)1;
                        $form->penalty = (float)1;
                        $form->correctanswer = ($q['correctanswer']) ? 1 : 0; //1 true - 0 false
                        break;
                    case 'match':
                        $form->generalfeedback = array(
                            'text' => utf8_decode($q['generalfeedback']),
                            'format' => 2
                        );
                        $form->defaultmark = (float)1;
                        $form->penalty = (float)0;

                        $form->shuffleanswers = 1;
                        $form->noanswers = $q['noanswers'];
                        $form->subquestions = array();
                        $subquestions = json_decode($q['subquestions']);
                        foreach ($subquestions as $k=>$v) {
                            $form->subquestions[$k] = array(
                                'text' => $v,
                                'format' => 2
                            );
                        }
                        $form->subanswers = array();
                        $subanswers = json_decode($q['subanswers']);
                        foreach ($subanswers as $k=>$v) {
                            $form->subanswers[$k] = $v;
                        }
                        $form->correctfeedback = array(
                            'text' => 'Your answer is correct.',
                            'format' => 2
                        );
                        $form->partiallycorrectfeedback = array(
                            'text' => 'Your answer is partially correct.',
                            'format' => 2
                        );
                        $form->incorrectfeedback = array(
                            'text' => 'Your answer is incorrect.',
                            'format' => 2
                        );

                        break;
                    case 'multichoice':
                        $form->defaultmark = (float)1;
                        $form->generalfeedback = array(
                            'text' => utf8_decode($q['generalfeedback']),
                            'format' => 2
                        );
                        $form->single = ($q['norightanswer'] == 1) ? 1 : 0; // 1 : single - 0 : multiple
                        $form->penalty = (float)0;
                        $form->answernumbering = 'abc';
                        $form->shuffleanswers = 1;
                        $form->noanswers = $q['noanswers'];
                        $form->answer = array();
                        $answers = json_decode($q['answers']);
                        foreach ($answers as $k=>$v) {
                            $form->answer[$k] = array(
                                'text' => $v,
                                'format' => 2
                            );
                        }
                        $form->feedback = array();
                        $feedbacks = json_decode($q['feedbacks']);
                        foreach ($feedbacks as $k=>$v) {
                            $form->feedback[$k] = array(
                                'text' => $v,
                                'format' => 2
                            );
                        }
                        switch($q['norightanswer']) {
                            case '1':
                                $fraction = 1.0;
                                break;
                            case '2':
                                $fraction = 0.5;
                                break;
                            case '3':
                                $fraction = 0.3333333;
                                break;
                            case '4':
                                $fraction = 0.25;
                                break;
                            case '5':
                                $fraction = 0.2;
                                break;
                            default:
                                $fraction = round( 1 / $q['norightanswer'], 7);
                                break;
                        }

                        $form->fraction = array();
                        $fractions = json_decode($q['fraction']);
                        foreach ($fractions as $k=>$v) {
                            $form->fraction[$k] = ($v) ? $fraction : (float)0 ;
                        }
                        $form->correctfeedback = array(
                            'text' => 'Your answer is correct.',
                            'format' => 2
                        );
                        $form->partiallycorrectfeedback = array(
                            'text' => 'Your answer is partially correct.',
                            'format' => 2
                        );
                        $form->shownumcorrect = 1;
                        $form->incorrectfeedback = array(
                            'text' => 'Your answer is incorrect.',
                            'format' => 2
                        );
                        break;
                    case 'shortanswer':
                        $form->defaultmark = (float)0;
                        $form->generalfeedback = array(
                            'text' => utf8_decode($q['generalfeedback']),
                            'format' => 2
                        );
                        $form->usecase = 0;
                        $form->noanswers = 1;
                        $form->penalty = (float)0;

                        $form->answer = array();
                        $answers = json_decode($q['answers']);
                        foreach ($answers as $k=>$v) {
                            $form->answer[$k] = $v;
                        }
                        $form->feedback = array();
                        $feedbacks = json_decode($q['feedbacks']);
                        foreach ($feedbacks as $k=>$v) {
                            $form->feedback[$k] = array(
                                'text' => $v,
                                'format' => 2
                            );
                        }
                        $form->fraction[0] = 1.0;
//                    foreach ($q['fraction'] as $k=>$v) {
//                        $form->fraction[$k] = ($v['fraction']) ? $q['norightanswer'] : (float)0.0 ;
//                    }
                        break;
                    default:
                        break;
                }

                require_once($CFG->dirroot.'/question/type/questiontypebase.php');
                $qtypeobj = question_bank::get_qtype($question->qtype);
                $r = $qtypeobj->save_question($question, $form, $username);
                get_question_options($r, true);

                $res['error'] = 0;
                $res['mes'] = 'success';
                $res['data'] = json_encode($r);
                return $res;
            }
        } else {
            $res['error'] = 1;
            $res['mes'] = 'nopermission';
            $res['data'] = 0;
            return $res;
        }
    }
    
    function create_question_platform($act, $courseid, $username, $q) {
        global $DB, $CFG;
        
        $res_r = array();
        $res_r['name'] = null;
        $res_r['id'] = null;
        $res_r['qtype'] = Null;
        
        $act_required = array(1,2,3);
        if(!in_array($act, $act_required)) {
            return $response = array(
                'status' => false,
                'message' => 'System can not be found the action.',
                'question' => $res_r,
            );
        }
        $data = json_decode($q);
        $data = (array) $data;       
        if(empty($data['name'])) {
            return $response = array(
                'status' => false,
                'message' => 'Question title can not blank.',
                'question' => $res_r,
            );
        }
        if(empty($data['type'])) {
            return $response = array(
                'status' => false,
                'message' => 'Question type can not blank.',
                'question' => $res_r,
            );
        }
        $data['name'] = $data['name'];
        $result = $this->create_question($act, $courseid, $username, $data);
        if($act == 1) {
            $action = "added.";
        } else if($act == 2) {
            $action = "updated.";
        } else {
            $action = "deleted.";
        }
        
        if(!$result['error']) {
            $res = json_decode($result['data']);
            $res_r['name'] = $res->name;
            $res_r['id'] = $res->id;
            $res_r['qtype'] = $res->qtype;
            return $response = array(
                'status' => true,
                'message' => 'Question '.$action,
                'question' => $res_r,
            );
        } else {
            return $response = array(
               'status' => false,             
               'message' => 'Something error for '.$action,             
               'question' => $res_r,             
            );
        }
    }

    function create_quiz_activity($act, $courseid, $username, $q, $sectionid) {
        global $CFG, $DB, $USER;
        if($act != 0) {
            // Update time modifi of course
            $course = new object();
            $course->id = $courseid;
            $course->timemodified = time();
            $DB->update_record('course', $course);
        }

        switch ($act) {
            case 0:
                if (isset($q['qid']) && ($q['qid'] != 0 )) {
                    require_once($CFG->dirroot.'/lib/datalib.php');
                    require_once($CFG->dirroot.'/course/modlib.php');

                    if (!array_key_exists($q['qid'], get_course_mods($courseid))) {
                        $return = array(
                            'status' => false,
                            'message' => 'Assessment not found.',
                            'res' => 0
                        );
                        return $return;
                    }
                    $cm = get_coursemodule_from_id('', $q['qid'], 0, false, MUST_EXIST);
                    $module = $DB->get_record('modules', array('id'=>$cm->module), '*', MUST_EXIST);
                    $data = $DB->get_record($module->name, array('id'=>$cm->instance), '*', MUST_EXIST);

                    $quizgrades = $DB->get_records('quiz_grade_letters', array('moduleid' => $q['qid']));
                    if ($quizgrades) {
                        foreach ($quizgrades as $quizgrade) {
                            if ($quizgrade->letter == 'A') {
                                $gradeA = $quizgrade->lowerboundary;
                            }
                            if ($quizgrade->letter == 'B') {
                                $gradeB = $quizgrade->lowerboundary;
                            }
                            if ($quizgrade->letter == 'C') {
                                $gradeC = $quizgrade->lowerboundary;
                            }
                            if ($quizgrade->letter == 'D') {
                                $gradeD = $quizgrade->lowerboundary;
                            }
                        }
                    }
//                    // format description paragraph
//                    $options['noclean'] = true;
//                    $context = context_course::instance($courseid);
//                    $data->intro = file_rewrite_pluginfile_urls($data->intro, 'pluginfile.php', $context->id, 'course', 'summary', NULL);
//                    $data->intro = str_replace('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $data->intro);
//                    $data->intro = format_text($data->intro, FORMAT_MOODLE, $options);
                    
                    $return = array(
                        'status' => true,
                        'message' => 'success',
                        'res' => 1,
                        'aid' => $data->id,
                        'courseid' => $data->course,
                        'name' => $data->name,
                        'atype' => $data->atype,
                        'des' => $data->intro,
                        'desformat' => $data->introformat,
                        'timeopen' => $data->timeopen,
                        'timeclose' => $data->timeclose,
                        'timelimit' => $data->timelimit,
                        'attempts' => $data->attempts,
                        'questions' => $data->questions,
                        'gradingscheme' => ($data->reviewmarks == 0) ? $data->reviewmarks : 1,
                        'gradeboundaryA' => ($gradeA ? (int)$gradeA : 0),
                        'gradeboundaryB' => ($gradeB ? (int)$gradeB : 0),
                        'gradeboundaryC' => ($gradeC ? (int)$gradeC : 0),
                        'gradeboundaryD' => ($gradeD ? (int)$gradeD : 0),
                    );
                    return $return;
                } else {
                    $params = array();
                    $params['courseid'] = $courseid;
                    $params['modulename'] = 'quiz';

                    $r = $DB->get_records_sql("SELECT cm.*, m.name, md.name as modname
                                   FROM {course_modules} cm, {modules} md, {".$params['modulename']."} m
                                  WHERE cm.course = :courseid AND
                                        cm.instance = m.id AND
                                        md.name = :modulename AND
                                        md.id = cm.module ORDER BY m.id DESC", $params);
                    $return = array(
                        'res' => json_encode($r)
                    );
                    return $return;
                }
                break;
            case 1:
            case 2:
                $user = get_complete_user_data('username', $username);
                $context = context_course::instance($courseid);
                if (!has_capability('moodle/course:manageactivities', $context, $user->id)) {
                    $return = array(
                        'status' => false,
                        'message' => 'You don\'t have permission for action.',
                        'res' => 0
                    );
                    return $return;
                }

                if (!has_capability('mod/quiz:addinstance', $context, $user->id)) {
                    $return = array(
                        'status' => false,
                        'message' => 'You don\'t have permission for action.',
                        'res' => 0
                    );
                    return $return;
                }

                $json = '{"name":"quiz 1","introeditor":{"text":"adasd","format":"1","itemid":"487893131"},
                "atype":"3","timeopen":0,"timeclose":0,"timelimit":0,"overduehandling":"autoabandon",
                "graceperiod":0,"gradecat":"1162","grade":10,"questions":"0","attempts":"1","grademethod":"1",
                "shufflequestions":"0","questionsperpage":"1","navmethod":"free","shuffleanswers":"1",
                "preferredbehaviour":"deferredfeedback","attemptonlast":"0","attemptimmediately":"1",
                "correctnessimmediately":"1","specificfeedbackimmediately":"1",
                "generalfeedbackimmediately":"1","rightanswerimmediately":"1","overallfeedbackimmediately":"1",
                "attemptopen":"1","correctnessopen":"1","specificfeedbackopen":"1",
                "generalfeedbackopen":"1","rightansweropen":"1","overallfeedbackopen":"1","attemptclosed":"1",
                "correctnessclosed":"1","specificfeedbackclosed":"1","generalfeedbackclosed":"1",
                "rightanswerclosed":"1","overallfeedbackclosed":"1","showuserpicture":"0","decimalpoints":"2",
                "questiondecimalpoints":"-1","showblocks":"0","quizpassword":"","subnet":"","delay1":0,"delay2":0,
                "browsersecurity":"-","boundary_repeats":4,"feedbacktext":[{"text":"","format":"1","itemid":"116331097"},
                {"text":"","format":"1","itemid":"167723689"},{"text":"","format":"1","itemid":"760677138"},
                {"text":"","format":"1","itemid":"252403804"},{"text":"","format":"1","itemid":"650004225"}],
                "feedbackboundaries":["","","",""],"visible":"1","cmidnumber":"","groupmode":"0","groupingid":"0",
                "availablefrom":0,"availableuntil":0,"conditiongraderepeats":1,"conditiongradegroup":[{"conditiongradeitemid":"0",
                "conditiongrademin":"","conditiongrademax":""}],"conditionfieldrepeats":1,"conditionfieldgroup":[{"conditionfield":"0",
                "conditionfieldoperator":"contains","conditionfieldvalue":""}],"conditioncompletionrepeats":1,
                "conditioncompletiongroup":[{"conditionsourcecmid":"0","conditionrequiredcompletion":"1"}],"showavailability":"1",
                "completionunlocked":1,"completion":"2","completionview":"1","completionusegrade":"1","completionexpected":0,
                "course":1481,"coursemodule":0,"section":0,"module":20,"modulename":"quiz","instance":0,"add":"quiz","update":0,
                "return":0,"sr":0,"submitbutton2":"Save and return to course"}';

                $fromform = json_decode($json);
                $fromform->name = utf8_decode($q['name']);
                $fromform->introeditor = (array)$fromform->introeditor;
                $fromform->introeditor['text'] = utf8_decode($q['des']);
                $fromform->introeditor['format'] = 2; //plain text
                $fromform->conditiongradegroup[0] = (array)$fromform->conditiongradegroup[0];
                $fromform->conditionfieldgroup[0] = (array)$fromform->conditionfieldgroup[0];
                $fromform->conditioncompletiongroup[0] = (array)$fromform->conditioncompletiongroup[0];
                foreach ($fromform->feedbacktext as $k=>$v) {
                    $fromform->feedbacktext[$k] = (array)$v;
                }
                $fromform->atype = $q['atype'];
                $fromform->questions = $q['questions'];

                if ($fromform->atype == 1) {
                    $fromform->attempts = 1;
                } else if ($fromform->atype == 2) {
                    $fromform->attempts = $q['attempts'];
                } else if ($fromform->atype == 3) {
                    $fromform->attempts = 1;
//                    $timeopen = explode('/', $q['timeopen']);
//                    $fromform->timeopen = make_timestamp( $timeopen[2], $timeopen[1], $timeopen[0], 0, 0, 0 );
                    $timeopen = str_replace('/', '-', $q['timeopen']);
                    $fromform->timeopen = strtotime($timeopen) + ($q['timezoneOffset']*60);
                    $fromform->timeopen_raw = date('Y-m-d H:i:s', $fromform->timeopen);

//                    $timeclose = explode('/', $q['timeclose']);
//                    $fromform->timeclose = make_timestamp( $timeclose[2], $timeclose[1], $timeclose[0], 0, 0, 0 );
                    $timeclose = str_replace('/', '-', $q['timeclose']);
                    $fromform->timeclose = strtotime($timeclose);
                    if ($fromform->timeopen == $fromform->timeclose) {
                        $fromform->timeclose = $fromform->timeclose + (24*60*60 - 1);
                    }
                    $fromform->timeclose_raw = date('Y-m-d H:i:s', $fromform->timeclose);

                    $fromform->timelimit =  $q['timelimit'] * 60;
                }
                if ($q['gradingscheme'] == 0) { 
                    $fromform->completionusegrade = 0;
                } else {
                    $fromform->marksclosed = 1;
                    $fromform->marksimmediately = 1;
                    $fromform->marksopen = 1;
                }

                $fromform->course = $courseid;

                if ($act == 1) {
                    $fromform->add = 'quiz';
                } else if ($act == 2) {
                    $fromform->update = $q['qid'];
                    $fromform->coursemodule = $q['qid'];
                }

                require_once($CFG->dirroot.'/course/modlib.php');
                require_once($CFG->dirroot.'/mod/quiz/lib.php');
                require_once($CFG->dirroot.'/mod/quiz/editlib.php');
                require_once($CFG->libdir . '/questionlib.php');

                $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
                if (!empty($fromform->update)) {
                    $cm = get_coursemodule_from_id(null, $fromform->update, $course->id, false, MUST_EXIST);
                    $module = $DB->get_record('modules', array('id'=>$cm->module), '*', MUST_EXIST);
                    $quiz = $DB->get_record($module->name, array('id'=>$cm->instance), '*', MUST_EXIST);
                    $quiz->instance = $quiz->id;
                    $quiz->cmid = $cm->id;

                    $questions = explode(',', $quiz->questions);
                    $newquestions = explode(',', $q['questions']);

                    foreach ($newquestions as $k=>$v) {
                        if ($v == 0) continue;
                        if (!in_array($v, $questions)) {
//                            quiz_require_question_use($v, $user->username);
                            if ($DB->record_exists('question', array('id' => $v))) {
//                                quiz_require_question_use($v, $user->username);
                                $question = $DB->get_record('question', array('id' => $v), '*', MUST_EXIST);

                                if (!question_has_capability_on($question, 'use', null, $user->username)) {
                                    $hasCapability = false;
                                }
                                else $hasCapability = true;
                                if ($hasCapability) quiz_add_quiz_question($v, $quiz);
                            }
                        }
                    }

                    foreach ($questions as $k=>$v) {
                        if ($v == 0) continue;
                        if (!in_array($v, $newquestions)) {
                            if ($DB->record_exists('question', array('id' => $v))) {
//                                quiz_require_question_use($v, $user->username);
                                $question = $DB->get_record('question', array('id' => $v), '*', MUST_EXIST);

                                if (!question_has_capability_on($question, 'use', null, $user->username)) {
                                    $hasCapability = false;
                                }
                                else $hasCapability = true;
                                if ($hasCapability) quiz_remove_question($quiz, $v);
                            }
                        }
                    }

                    quiz_delete_previews($quiz);
                    quiz_update_sumgrades($quiz);

                    list($cm, $fromform) = update_moduleinfo($cm, $fromform, $course, null, $user->username);
                } else if (!empty($fromform->add)) {
                    $add = add_moduleinfo($fromform, $course, null, $user->username);
                }

                if($sectionid!=0 && $courseid!=0) {
                    // move module from section 0 to section
                    $id = $add->coursemodule;
                    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
                    $cm = get_coursemodule_from_id(null, $id, $course->id, false, MUST_EXIST);
                    $modcontext = context_module::instance($cm->id);

                    if (!$section_temp = $DB->get_record('course_sections', array('course' => $course->id, 'id' => $sectionid))) {
                        throw new moodle_exception('AJAX commands.php: Bad section ID ' . $sectionid);
                    }
                    $beforemod = 0;
                    moveto_module($cm, $section_temp, $beforemod);
                }

                // Add grading scheme
                if ($q['gradingscheme'] && $q['gradingscheme'] == 1 && $q['atype'] == 2) {
                    if (!empty($fromform->update)) {
                        $mdid = $q['qid'];
                    } else {
                        $mdid = $add->coursemodule;
                    }
                    $old_ids = array();
                    if ($records = $DB->get_records('quiz_grade_letters', array('moduleid' => $mdid), 'lowerboundary ASC', 'id')) {
                        $old_ids = array_keys($records);
                    }
                    if ($q['gradeboundaryA']) {
                        $record = new stdClass();
                        $record->letter        = 'A';
                        $record->lowerboundary = $q['gradeboundaryA'];
                        $record->moduleid     = $mdid;

                        if ($old_id = array_pop($old_ids)) {
                            $record->id = $old_id;
                            $DB->update_record('quiz_grade_letters', $record);
                        } else {
                            $DB->insert_record('quiz_grade_letters', $record);
                        }
                    }
                    if ($q['gradeboundaryB']) {
                        $record = new stdClass();
                        $record->letter        = 'B';
                        $record->lowerboundary = $q['gradeboundaryB'];
                        $record->moduleid     = $mdid;

                        if ($old_id = array_pop($old_ids)) {
                            $record->id = $old_id;
                            $DB->update_record('quiz_grade_letters', $record);
                        } else {
                            $DB->insert_record('quiz_grade_letters', $record);
                        }
                    }
                    if ($q['gradeboundaryC']) {
                        $record = new stdClass();
                        $record->letter        = 'C';
                        $record->lowerboundary = $q['gradeboundaryC'];
                        $record->moduleid     = $mdid;

                        if ($old_id = array_pop($old_ids)) {
                            $record->id = $old_id;
                            $DB->update_record('quiz_grade_letters', $record);
                        } else {
                            $DB->insert_record('quiz_grade_letters', $record);
                        }
                    }
//                    if (!empty($q['gradeboundaryD'])) {
                        $record = new stdClass();
                        $record->letter        = 'D';
                        $record->lowerboundary = $q['gradeboundaryD'];
                        $record->moduleid     = $mdid;

                        if ($old_id = array_pop($old_ids)) {
                            $record->id = $old_id;
                            $DB->update_record('quiz_grade_letters', $record);
                        } else {
                            $DB->insert_record('quiz_grade_letters', $record);
                        }
//                    }
                } else if ($q['gradingscheme'] == 0) {
                    if (!empty($fromform->update)) {
                        $mdid = $q['qid'];
                    } else {
                        $mdid = $add->coursemodule;
                }
                    $DB->delete_records('quiz_grade_letters', array('moduleid' => $mdid));
                }

                $this->check_invisible_activities_and_rebuild_course_cache($courseid);

                $return = array(
                    'status' => true,
                    'message' => 'Success.',
                    'res' => 1,
                    'aid' => $add->id,
                    'courseid' => $courseid,
                );
                return $return;
                break;
            case 3:
                if ($username) {
                    $user = get_complete_user_data('username', $username);
                    $context = context_course::instance($courseid);
                    $cm = get_coursemodule_from_id('', $q['qid'], 0, true, MUST_EXIST);
                    $modcontext = context_module::instance($cm->id);

                    if (!has_capability('moodle/course:manageactivities', $context, $user->id) || !has_capability('moodle/course:manageactivities', $modcontext, $user->id)) {
                        $return = array(
                            'status' => false,
                            'message' => 'Failed.',
                            'res' => 0
                        );
                        return $return;
                    }
                    course_delete_module($cm->id);
                    $return = array(
                        'status' => true,
                        'message' => 'Success.',
                        'res' => 1
                    );
                    return $return;
                }
                break;
            default:
                break;
        }
    }

    function create_page_activity($act, $courseid, $username, $p, $sectionid) {
        global $CFG, $DB, $USER;
        if($act != 0){
            // Update time modifi of course
            $course = new object();
            $course->id     =   $courseid;
            $course->timemodified  = time();
            $DB->update_record('course', $course);
        }

        switch ($act) {
            case 0:
                if (isset($p['pid']) && ($p['pid'] != 0 )) {
                    require_once($CFG->dirroot.'/lib/datalib.php');
                    require_once($CFG->dirroot.'/course/modlib.php');
                    if (!array_key_exists($p['pid'], get_course_mods($courseid))) {
                        $return = array(
                            'error' => 1,
                            'mes' => 'noexistmodule'
                        );
                        return $return;
                    }
                    $cm = get_coursemodule_from_id('', $p['pid'], 0, false, MUST_EXIST);
                    $module = $DB->get_record('modules', array('id'=>$cm->module), '*', MUST_EXIST);
                    $data = $DB->get_record($module->name, array('id'=>$cm->instance), '*', MUST_EXIST);

                    // format description paragraph
//                    $options['noclean'] = true;
//                    $context = context_course::instance($courseid);
//                    $data->intro = file_rewrite_pluginfile_urls($data->intro, 'pluginfile.php', $context->id, 'course', 'summary', NULL);
//                    $data->intro = str_replace('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $data->intro);
//                    $data->intro = format_text($data->intro, FORMAT_MOODLE, $options);

                    $return = array(
                        'error' => 0,
                        'pid' => $data->id,
                        'courseid' => $data->course,
                        'name' => $data->name,
                        'des' => $data->intro,
                        'desformat' => $data->introformat,
                        'content' => $data->content,
                        'contentformat' => $data->contentformat,
                        'params' => $data->params
                    );
                    return $return;
                } else {
                    $params = array();
                    $params['courseid'] = $courseid;
                    $params['modulename'] = 'quiz';


                    $r = $DB->get_records_sql("SELECT cm.*, m.name, md.name as modname
                                   FROM {course_modules} cm, {modules} md, {".$params['modulename']."} m
                                  WHERE cm.course = :courseid AND
                                        cm.instance = m.id AND
                                        md.name = :modulename AND
                                        md.id = cm.module ORDER BY m.id DESC", $params);
                    $return = array(
                        'error' => 0,
                        'mes' => 'success',
                        'data' => json_encode($r)
                    );
                    return $return;
                }
                break;
            case 1:
            case 2:
                $user = get_complete_user_data('username', $username);
                $context = context_course::instance($courseid);
                if (!has_capability('moodle/course:manageactivities', $context, $user->id)) {
                    $return = array(
                        'error' => 1,
                        'mes' => 'manageactivitiespermission'
                    );
                    return $return;
                }

                if (!has_capability('mod/page:addinstance', $context, $user->id)) {
                    $return = array(
                        'error' => 1,
                        'mes' => 'pageaddinstancepermission'
                    );
                    return $return;
                }

                $json = '{"name":"new page","introeditor":{"text":"asdasdasd","format":"1","itemid":"720670314"},"page":{"text":"<p>aloso<\/p>","format":"1","itemid":"862933383"},"display":"5","popupwidth":"620","popupheight":"450","printheading":"1","printintro":"0","visible":"1","cmidnumber":"","groupingid":"0","availablefrom":0,"availableuntil":0,"conditiongraderepeats":1,"conditiongradegroup":[{"conditiongradeitemid":"0","conditiongrademin":"","conditiongrademax":""}],"conditionfieldrepeats":1,"conditionfieldgroup":[{"conditionfield":"0","conditionfieldoperator":"contains","conditionfieldvalue":""}],"conditioncompletionrepeats":1,"conditioncompletiongroup":[{"conditionsourcecmid":"0","conditionrequiredcompletion":"1"}],"showavailability":"1","completionunlocked":1,"completion":"2","completionview":"1","completionexpected":0,"course":1481,"coursemodule":0,"section":0,"module":19,"modulename":"page","instance":0,"add":"page","update":0,"return":0,"sr":0,"submitbutton2":"Save and return to course","revision":1}';

                $fromform = json_decode($json);

                $fromform->name = utf8_decode($p['name']);
                $fromform->introeditor = (array)$fromform->introeditor;
                $fromform->introeditor['text'] = utf8_decode($p['name']);
                $fromform->introeditor['format'] = 2; //plain text
                $fromform->page = (array)$fromform->page;
                $fromform->page['text'] = json_decode(($p['content']));
                $pr = (json_decode($p['contentArr']));
                $fromform->params = json_encode($pr);
                $fromform->page['format'] = 1;
                $fromform->conditiongradegroup[0] = (array)$fromform->conditiongradegroup[0];
                $fromform->conditionfieldgroup[0] = (array)$fromform->conditionfieldgroup[0];
                $fromform->conditioncompletiongroup[0] = (array)$fromform->conditioncompletiongroup[0];
                $fromform->course = $courseid;

                $fromform->username = $username;

                if ($act == 1) {
                    $fromform->add = 'page';
                } else if ($act == 2) {
                    $fromform->update = $p['pid'];
                    $fromform->coursemodule = $p['pid'];
                }

                require_once($CFG->dirroot.'/course/modlib.php');
                require_once($CFG->dirroot.'/mod/scorm/lib.php');

                $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
                if (!empty($fromform->update)) {
                    $cm = get_coursemodule_from_id('', $fromform->update, 0, false, MUST_EXIST);
                    list($cm, $fromform) = update_moduleinfo($cm, $fromform, $course, null, $user->username);
                } else if (!empty($fromform->add)) {
                    $add = add_moduleinfo($fromform, $course, null, $user->username);
                }

                if($sectionid!=0 && $courseid!=0) {
                    // move module from section 0 to section
                    $id = $add->coursemodule;
                    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
                    $cm = get_coursemodule_from_id(null, $id, $course->id, false, MUST_EXIST);
                    $modcontext = context_module::instance($cm->id);

                    if (!$section_temp = $DB->get_record('course_sections', array('course' => $course->id, 'id' => $sectionid))) {
                        throw new moodle_exception('AJAX commands.php: Bad section ID ' . $sectionid);
                    }
                    $beforemod = 0;
                    moveto_module($cm, $section_temp, $beforemod);
                }

                $this->check_invisible_activities_and_rebuild_course_cache($courseid);

                $return = array(
                    'error' => 0,
                    'mes' => 'success'
                );
                return $return;
                break;
            case 3:
                if ($username) {
                    $user = get_complete_user_data('username', $username);
                    $context = context_course::instance($courseid);
                    $cm = get_coursemodule_from_id('', $p['pid'], 0, true, MUST_EXIST);
                    $modcontext = context_module::instance($cm->id);

                    if (!has_capability('moodle/course:manageactivities', $context, $user->id) || !has_capability('moodle/course:manageactivities', $modcontext, $user->id)) {
                        $return = array(
                            'error' => 1,
                            'mes' => 'manageactivitiespermission'
                        );
                        return $return;
                    }
                    course_delete_module($cm->id);
                    $fs = get_file_storage();
                    $fs->delete_area_files($modcontext->id, 'mod_page', 'content');

                    $return = array(
                        'error' => 0,
                        'mes' => 'success'
                    );
                    return $return;
                }
                break;
            case 4:
                if ($p['fname']) {
                    $tmp_file = $CFG->dataroot . '/temp/content/'.$courseid.'/'.$p['itemid'] . '/'.$p['fname'];

                    if ($username) {
                        $user = get_complete_user_data('username', $username);
                        $usercontext = context_user::instance($user->id);
                        $context = context_course::instance($courseid);
                        if (!has_capability('moodle/course:manageactivities', $context, $user->id)) {
                            $return = array(
                                'error' => 1,
                                'mes' => 'manageactivitiespermission'
                            );
                            return $return;
                        }

                        if (!has_capability('mod/page:addinstance', $context, $user->id)) {
                            $return = array(
                                'error' => 1,
                                'mes' => 'pageaddinstancepermission'
                            );
                            return $return;
                        }
                    }

                    $packagefile = file_get_submitted_draft_itemid('packagefile');
//                    file_prepare_draft_area($packagefile, $context->id, 'mod_page', 'package', 0);
                    file_prepare_draft_area($packagefile, $usercontext->id, 'user', 'draft', 0);

                    $draftitemid = file_get_submitted_draft_itemid('introeditor');
                    file_prepare_draft_area($draftitemid, null, null, null, null);

                    $fs = get_file_storage();

                    $record = new stdClass();
                    $record->component = 'mod_page';
                    $record->filearea = 'content';
                    $record->filepath = '/';
                    $record->itemid = $draftitemid;

                    $record->license = 'allrightsreserved';
                    $record->author = $user->username;

                    $record->contextid = $usercontext->id;
                    $record->userid = $user->id;
                    $record->filename = $p['fname'];

                    $sourcefield = $record->filename;
                    $record->source = repository_upload::build_source_field($sourcefield);

                    if (!$fs->file_exists($usercontext->id, 'mod_page', 'content', $draftitemid, '/',  $record->filename)) {
                        $stored_file = $fs->create_file_from_pathname($record, $tmp_file);
                        $filename = $stored_file->get_filename();
//                        $url = $CFG->wwwroot . '/pluginfile.php/' . $usercontext->id . '/mod_page/content/'.$draftitemid.'/';
                        $url = '/courses/pluginfile.php/' . $usercontext->id . '/mod_page/content/'.$draftitemid.'/';
                    } else {
                        $stored_file = $fs->get_file($usercontext->id, 'mod_page', 'content', $draftitemid, '/', $record->filename);
                        $draftitemid = $stored_file->get_itemid();
//                        $url = $CFG->wwwroot . '/pluginfile.php/' . $usercontext->id . '/mod_page/content/'.$draftitemid.'/';
                        $url = '/courses/pluginfile.php/' . $usercontext->id . '/mod_page/content/'.$draftitemid.'/';
                        $filename = $record->filename;

                    }

                    $fileData = array(
                        'filepath' => $url,
                        'filename' => $filename,
                        'itemid' => $draftitemid
                    );

                    $return = array(
                        'error' => 0,
                        'mes' => 'success',
                        'data' => json_encode($fileData)
                    );
                    return $return;
                }
                break;
            case 5:
                if ($username) {
                    $user = get_complete_user_data('username', $username);
                    $usercontext = context_user::instance($user->id);
                    $context = context_course::instance($courseid);
                    if (!has_capability('moodle/course:manageactivities', $context, $user->id)) {
                        $return = array(
                            'error' => 1,
                            'mes' => 'manageactivitiespermission'
                        );
                        return $return;
                    }

                    if (!has_capability('mod/page:addinstance', $context, $user->id)) {
                        $return = array(
                            'error' => 1,
                            'mes' => 'pageaddinstancepermission'
                        );
                        return $return;
                    }
                }

                $fs = get_file_storage();

                if ($p['fname'] != 'all') {
                    if ($fs->file_exists($usercontext->id, 'user', 'draft', $p['itemid'], '/', $p['fname'])) {
                        $fs->delete_area_files($usercontext->id, 'user', 'draft', $p['itemid']);
                    }
                } else {
                    $fs->delete_area_files($usercontext->id, 'user', 'draft');
                }
                $return = array(
                    'error' => 0,
                    'mes' => 'success'
                );
                return $return;

                break;
            default:
                break;
        }
    }

    function create_assignment_activity($act, $courseid, $username, $p, $sectionid) {
        global $CFG, $DB, $USER;

        if($act != 0) {
        // Update time modifi of course
        $course = new object();
        $course->id     =   $courseid;
        $course->timemodified  = time();
        $DB->update_record('course', $course);
        }

        switch ($act) {
            case 0:
                if (isset($p['aid']) && ($p['aid'] != 0 )) {
                    require_once($CFG->dirroot.'/lib/datalib.php');
                    require_once($CFG->dirroot.'/course/modlib.php');
                    if (!$DB->record_exists("course_modules", array("id"=>$p['aid'] )) || !array_key_exists($p['aid'], get_course_mods($courseid)) ) {
                        $return = array(
                            'error' => 1,
                            'mes' => 'noexistmodule'
                        );
                        return $return;
                    }

                    $cm = get_coursemodule_from_id('', $p['aid'], 0, false, MUST_EXIST);
                    $module = $DB->get_record('modules', array('id'=>$cm->module), '*', MUST_EXIST);
                    $data = $DB->get_record($module->name, array('id'=>$cm->instance), '*', MUST_EXIST);

//                    // format description paragraph
//                    $options['noclean'] = true;
//                    $context = context_course::instance($courseid);
//                    $data->intro = file_rewrite_pluginfile_urls($data->intro, 'pluginfile.php', $context->id, 'course', 'summary', NULL);
//                    $data->intro = str_replace('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $data->intro);
//                    $data->intro = format_text($data->intro, FORMAT_MOODLE, $options);

                    $return = array(
                        'error' => 0,
                        'aid' => $data->id,
                        'courseid' => $data->course,
                        'name' => $data->name,
                        'des' => $data->intro,
                        'desformat' => $data->introformat,
                        'duedate' => $data->duedate,
//                        'timezoneOffset' => $data->duedate,
                        'params' => $data->params
                    );
                    return $return;
                } else {
                    $params = array();
                    $params['courseid'] = $courseid;
                    $params['modulename'] = 'assign';


                    $r = $DB->get_records_sql("SELECT cm.*, m.name, md.name as modname
                                   FROM {course_modules} cm, {modules} md, {".$params['modulename']."} m
                                  WHERE cm.course = :courseid AND
                                        cm.instance = m.id AND
                                        md.name = :modulename AND
                                        md.id = cm.module ORDER BY m.id DESC", $params);
                    $return = array(
                        'error' => 0,
                        'mes' => 'success',
                        'data' => json_encode($r)
                    );
                    return $return;
                }
                break;
            case 1:
            case 2:
                $user = get_complete_user_data('username', $username);
                $context = context_course::instance($courseid);
                if (!has_capability('moodle/course:manageactivities', $context, $user->id)) {
                    $return = array(
                        'error' => 1,
                        'mes' => 'manageactivitiespermission'
                    );
                    return $return;
                }

                if (!has_capability('mod/assign:addinstance', $context, $user->id)) {
                    $return = array(
                        'error' => 1,
                        'mes' => 'assignaddinstancepermission'
                    );
                    return $return;
                }

                $json = '{"name":"Assignment test 22","introeditor":{"text":"assignmetn descasd","format":"1","itemid":"165995950"},"mform_isexpanded_id_availability":1,"allowsubmissionsfromdate_raw":"2016-12-29 16:00:00","allowsubmissionsfromdate":1482998400,"duedate_raw":"2017-01-05 16:00:00","duedate":1483603200,"cutoffdate":0,"alwaysshowdescription":"1","assignsubmission_onlinetext_enabled":"1","assignsubmission_file_enabled":"1","assignsubmission_file_maxfiles":"1","assignsubmission_file_maxsizebytes":"0","assignfeedback_comments_enabled":"1","mform_isexpanded_id_submissiontypes":1,"submissiondrafts":"0","requiresubmissionstatement":"0","attemptreopenmethod":"none","maxattempts":"-1","teamsubmission":"0","requireallteammemberssubmit":"0","teamsubmissiongroupingid":"0","sendnotifications":"1","sendlatenotifications":"1","grade":"100","advancedgradingmethod_submissions":"","gradecat":"1182","blindmarking":"0","visible":"1","cmidnumber":"","groupmode":"0","groupingid":"0","availablefrom":0,"availableuntil":0,"conditiongraderepeats":1,"conditiongradegroup":[{"conditiongradeitemid":"0","conditiongrademin":"","conditiongrademax":""}],"conditionfieldrepeats":1,"conditionfieldgroup":[{"conditionfield":"0","conditionfieldoperator":"contains","conditionfieldvalue":""}],"conditioncompletionrepeats":1,"conditioncompletiongroup":[{"conditionsourcecmid":"0","conditionrequiredcompletion":"1"}],"showavailability":"1","completionunlocked":1,"completion":"2","completionview":"1","completionusegrade":"1","completionsubmit":"1","completionexpected":0,"course":1589,"coursemodule":0,"section":0,"module":1,"modulename":"assign","instance":0,"add":"assign","update":0,"return":0,"sr":0,"submitbutton":"Save and display"}';

                $fromform = json_decode($json);

                $fromform->name = utf8_decode($p['name']);;
                $fromform->introeditor = (array)$fromform->introeditor;
                $fromform->introeditor['text'] = utf8_decode($p['des']);
                $fromform->introeditor['format'] = 2; //plain text

                $allowsubmissionsfromdate = make_timestamp(  date('Y', time()) , date('m', time()) , date('d', time()), 0, 0, 0  );
                $fromform->allowsubmissionsfromdate_raw = date('Y-m-d H:i:s', $allowsubmissionsfromdate);
                $fromform->allowsubmissionsfromdate = $allowsubmissionsfromdate;

                $duedate = str_replace('/', '-', $p['duedate']);
                $fromform->duedate = strtotime($duedate) + ($p['timezoneOffset']*60);
                $fromform->duedate_raw = date('Y-m-d H:i:s', $fromform->duedate);

                $fromform->visible = 1;

                $fromform->conditiongradegroup[0] = (array)$fromform->conditiongradegroup[0];
                $fromform->conditionfieldgroup[0] = (array)$fromform->conditionfieldgroup[0];
                $fromform->conditioncompletiongroup[0] = (array)$fromform->conditioncompletiongroup[0];
                $fromform->course = $courseid;

                $fromform->username = $username;
                $fromform->itemid = $p['itemid'];
                $fromform->fname = utf8_decode($p['fname']);

                if ($act == 1) {
                    $fromform->add = 'assign';
                } else if ($act == 2) {
                    $fromform->update = $p['aid'];
                    $fromform->coursemodule = $p['aid'];
                }

                require_once($CFG->dirroot.'/course/modlib.php');
                require_once($CFG->dirroot.'/mod/assign/lib.php');

                $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
                if (!empty($fromform->update)) {
                    $cm = get_coursemodule_from_id('', $fromform->update, 0, false, MUST_EXIST);
                    list($cm, $fromform) = update_moduleinfo($cm, $fromform, $course, null, $user->username);
                } else if (!empty($fromform->add)) {
                    $add = add_moduleinfo($fromform, $course, null, $user->username);
                }
                if($sectionid!=0 && $courseid!=0) {
                    // move module from section 0 to section
                    $id = $add->coursemodule;
                    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
                    $cm = get_coursemodule_from_id(null, $id, $course->id, false, MUST_EXIST);
                    $modcontext = context_module::instance($cm->id);

                    if (!$section_temp = $DB->get_record('course_sections', array('course' => $course->id, 'id' => $sectionid))) {
                        throw new moodle_exception('AJAX commands.php: Bad section ID ' . $sectionid);
                    }
                    $beforemod = 0;
                    moveto_module($cm, $section_temp, $beforemod);
                }

				$this->check_invisible_activities_and_rebuild_course_cache($courseid);

                $return = array(
                    'error' => 0,
                    'mes' => 'success'
                );
                return $return;
                break;
            case 3:
                if ($username) {
                    $user = get_complete_user_data('username', $username);
                    $context = context_course::instance($courseid);
                    $cm = get_coursemodule_from_id('', $p['aid'], 0, true, MUST_EXIST);
                    $modcontext = context_module::instance($cm->id);

                    if (!has_capability('moodle/course:manageactivities', $context, $user->id) || !has_capability('moodle/course:manageactivities', $modcontext, $user->id)) {
                        $return = array(
                            'error' => 1,
                            'mes' => 'manageactivitiespermission'
                        );
                        return $return;
                    }

                    course_delete_module($cm->id);
                    $fs = get_file_storage();
                    $fs->delete_area_files($modcontext->id, 'mod_assign', 'brief');

                    $return = array(
                        'error' => 0,
                        'mes' => 'success'
                    );
                    return $return;
                }
                break;
            case 4:
                if ($p['fname']) {
                    $tmp_file = $CFG->dataroot . '/temp/' . 'tmp_file';

                    if ($username) {
                        $user = get_complete_user_data('username', $username);
                        $usercontext = context_user::instance($user->id);
                        $context = context_course::instance($courseid);
                        if (!has_capability('moodle/course:manageactivities', $context, $user->id)) {
                            $return = array(
                                'error' => 1,
                                'mes' => 'manageactivitiespermission'
                            );
                            return $return;
                        }

                        if (!has_capability('mod/assign:addinstance', $context, $user->id)) {
                            $return = array(
                                'error' => 1,
                                'mes' => 'assignaddinstancepermission'
                            );
                            return $return;
                        }
                    }

                    $packagefile = file_get_submitted_draft_itemid('packagefile');
//                    file_prepare_draft_area($packagefile, $context->id, 'mod_page', 'package', 0);
                    file_prepare_draft_area($packagefile, $usercontext->id, 'user', 'draft', 0);

                    $draftitemid = file_get_submitted_draft_itemid('introeditor');
                    file_prepare_draft_area($draftitemid, null, null, null, null);

                    $fs = get_file_storage();

                    $record = new stdClass();
                    $record->component = 'user';
                    $record->filearea = 'draft';
                    $record->filepath = '/';
                    $record->itemid = $draftitemid;

                    $record->license = 'allrightsreserved';
                    $record->author = $user->username;

                    $record->contextid = $usercontext->id;
                    $record->userid = $user->id;
                    $record->filename = utf8_decode($p['fname']);

                    $sourcefield = $record->filename;
                    $record->source = repository_upload::build_source_field($sourcefield);

                    if (!$fs->file_exists($usercontext->id, 'user', 'draft', $draftitemid, '/',  $record->filename)) {
                        $stored_file = $fs->create_file_from_pathname($record, $tmp_file);
                        $filename = $stored_file->get_filename();
                        $url = $CFG->wwwroot . '/pluginfile.php/' . $usercontext->id . '/user/draft/'.$draftitemid.'/';
                    } else {
                        $stored_file = $fs->get_file($usercontext->id, 'user', 'draft', $draftitemid, '/', $record->filename);
                        $draftitemid = $stored_file->get_itemid();
                        $url = $CFG->wwwroot . '/pluginfile.php/' . $usercontext->id . '/user/draft/'.$draftitemid.'/';
                        $filename = $record->filename;

                    }

                    $fileData = array(
                        'filepath' => $url,
                        'filename' => $filename,
                        'itemid' => $draftitemid
                    );

                    $return = array(
                        'error' => 0,
                        'mes' => 'success',
                        'data' => json_encode($fileData)
                    );
                    return $return;
                }
                break;
            case 5:
                if ($username) {
                    $user = get_complete_user_data('username', $username);
                    $usercontext = context_user::instance($user->id);
                    $context = context_course::instance($courseid);
                    if (!has_capability('moodle/course:manageactivities', $context, $user->id)) {
                        $return = array(
                            'error' => 1,
                            'mes' => 'manageactivitiespermission'
                        );
                        return $return;
                    }

                    if (!has_capability('mod/page:addinstance', $context, $user->id)) {
                        $return = array(
                            'error' => 1,
                            'mes' => 'pageaddinstancepermission'
                        );
                        return $return;
                    }
                }

                $fs = get_file_storage();

                if ($p['fname'] != 'all') {
                    if ($fs->file_exists($usercontext->id, 'user', 'draft', $p['itemid'], '/', $p['fname'])) {
                        $fs->delete_area_files($usercontext->id, 'user', 'draft', $p['itemid']);
                    }
                } else {
                    $fs->delete_area_files($usercontext->id, 'user', 'draft');
                }
                $return = array(
                    'error' => 0,
                    'mes' => 'success'
                );
                return $return;

                break;
            default:
                break;
        }
    }

    function student_submission_assign($course_id, $assign_id, $username, $file){
        global $CFG, $DB, $USER;
        require_once($CFG->dirroot.'/mod/assign/locallib.php');

        require_once($CFG->dirroot.'/course/modlib.php');
        require_once($CFG->dirroot.'/mod/assign/lib.php');
        require_once($CFG->dirroot.'/mod/assign/submission/file/locallib.php');

        if ($file['fname']) {
            $tmp_file = $CFG->dataroot . '/temp/' . 'tmp_file';
            // $tmp_file = /var/www/html/sitedata_parentheXisv2/temp/tmp_file
            if ($username) {

                $user = get_complete_user_data('username', $username);
                $usercontext = context_user::instance($user->id);
                $context = context_course::instance($course_id);

            }
            $activity = $DB->get_record('course_modules', array('id'=>$assign_id));

            $packagefile = file_get_submitted_draft_itemid('packagefile');
//                    file_prepare_draft_area($packagefile, $context->id, 'mod_page', 'package', 0);
            file_prepare_draft_area($packagefile, $usercontext->id, 'user', 'draft', 0);

            $draftitemid = file_get_submitted_draft_itemid('introeditor');
            file_prepare_draft_area($draftitemid, null, null, null, null);

            $fs = get_file_storage();

            $record = new stdClass();
            $record->component = 'user';
            $record->filearea = 'draft';
            $record->filepath = '/';
            $record->itemid = $draftitemid;

            $record->license = 'allrightsreserved';
            $record->author = $user->username;

            $record->contextid = $usercontext->id;
            $record->userid = $user->id;
            $record->filename = utf8_decode($file['fname']);

            $sourcefield = $record->filename;
            $record->source = repository_upload::build_source_field($sourcefield);

            if (!$fs->file_exists($usercontext->id, 'user', 'draft', $draftitemid, '/',  $record->filename)) {
                $stored_file = $fs->create_file_from_pathname($record, $tmp_file);
                $filename = $stored_file->get_filename();
                $url = $CFG->wwwroot . '/pluginfile.php/' . $usercontext->id . '/user/draft/'.$draftitemid.'/';
            } else {
                $stored_file = $fs->get_file($usercontext->id, 'user', 'draft', $draftitemid, '/', $record->filename);
                $draftitemid = $stored_file->get_itemid();
                $url = $CFG->wwwroot . '/pluginfile.php/' . $usercontext->id . '/user/draft/'.$draftitemid.'/';
                $filename = $record->filename;

            }

            // Create assign_submission

            $newcontext = context_module::instance($assign_id);

            $assign_submission = new stdClass();
            $assign_submission->assignment = $activity->instance;
            $assign_submission->userid = $user->id;
            $assign_submission->status = 'submitted';
            $assign_submission->timecreated = time();
            $assign_submission->timemodified = time();
            $assign_submission_id = $DB->insert_record('assign_submission', $assign_submission);


            // Create assignsubmission_file
            $assignsubmission_file = new stdClass();
            $assignsubmission_file->assignment = $activity->instance;
            $assignsubmission_file->submission = $assign_submission_id;
            $assignsubmission_file->numfiles = 1;

            $assignsubmission_file_id = $DB->insert_record('assignsubmission_file', $assignsubmission_file);

            // Create assignsubmission_onlinetext
            $assignsubmission_onlinetext = new stdClass();
            $assignsubmission_onlinetext->assignment = $activity->instance;
            $assignsubmission_onlinetext->submission = $assign_submission_id;
            $assignsubmission_onlinetext->onlineformat = 0;

            $assignsubmission_onlinetext_id = $DB->insert_record('assignsubmission_onlinetext', $assignsubmission_onlinetext);

            $file_record = array('contextid'=>$newcontext->id, 'component'=>'assignsubmission_file', 'filearea'=>'submission_files', 'itemid'=>$assign_submission_id,
                'filepath'=>'/', 'filename'=>$record->filename, 'userid'=>$user->id);

            $fs->create_file_from_pathname($file_record, $tmp_file);

            $return = array(
                'message' => "true"
            );

            return $return;
        }
    }

    function remove_course ($courseid, $username) {
        global $CFG, $DB, $USER;

        if (!$DB->record_exists("course", array("id"=>$courseid))) {
            return ['res' => 0 ];
        }

            $user = get_complete_user_data('username', $username);

        $course = $DB->get_record("course", array("id"=>$courseid));
        if (!can_delete_course($courseid, $user)) {
            $return = ['res' => 0 ];
        } else {
        delete_course($course, false, $username);
        fix_course_sortorder();

            $return = ['res' => 1 ];
        }

        return $return;
    }

    function system_check () {
        $system['joomdle_auth'] = is_enabled_auth('joomdle');
        //  $system['mnet_auth'] = is_enabled_auth('mnet');
        $system['mnet_auth'] = 1; // Left this way so we can have the same system check code for 19 and 20

        /* Web services and XMLRPC enabled */
        //  $system['enablewebservices'] =  get_config (NULL, 'enablewebservices');

        $joomla_url = get_config ('auth/joomdle', 'joomla_url');
        if ($joomla_url == '')
        {
            $system['joomdle_configured'] = 0;
        }
        else
        {
            $system['joomdle_configured'] = 1;
            $data = $this->call_method ("test");
            if (is_array ($data))
            {
                //j1.5 devuelve un error xmlrpc con esta info
                $system['test_data'] = $data['faultString'];
            }
            else
                $system['test_data'] = $data;

        }
        return $system;
    }

    function get_paypal_config () {
        global $CFG;

        $paypal_config = array ();
        $paypal_config['paypalurl'] = empty($CFG->usepaypalsandbox) ? 'https://www.paypal.com/cgi-bin/webscr' : 'https://www.sandbox.paypal.com/cgi-bin/webscr';

        $plugin = enrol_get_plugin('paypal');
        $paypal_config['paypalbusiness'] = $plugin->get_config('paypalbusiness');

        return $paypal_config;
    }
    function my_course_program($username,$programid) {
        global $CFG, $DB;
        $username = utf8_decode($username);
        $username = strtolower($username);

        if ($username)
        {
            $user = get_complete_user_data ('username', $username);
            $courses = enrol_get_users_courses ($user->id, true);
            $program_name = get_program_name($programid);
            foreach($program_name as $name)
            {
                $name_pro = $name->fullname;
            }
            $my_courses = array ();
            foreach ($courses as $course)
            {
                $my_courses[] = $course->id;
            }
            if (!empty($my_courses)) {
                $where .= ' AND co.id IN ('.implode($my_courses, ',').')  AND las.userid = '.$user->id;
            }
            $sql_pro = " select courseid
                FROM mdl_prog AS p 
                INNER JOIN mdl_prog_assignment AS pu ON p.id = pu.programid 
                INNER JOIN mdl_course_categories AS cat ON p.category= cat.id 
                INNER JOIN mdl_prog_courseset AS css ON css.programid= p.id 
                INNER JOIN mdl_prog_courseset_course AS cc ON cc.coursesetid= css.id
                WHERE  pu.assignmenttypeid =$user->id and p.id=$programid";

        }

        $query =
            "SELECT
            co.id          AS remoteid,
            ca.id          AS cat_id,
            co.sortorder,
            co.fullname,
            co.shortname,
            co.idnumber,
            co.summary,
            co.startdate,
            co.timecreated as created,
            co.timemodified as modified,
            ca.name        AS cat_name,
            ca.description AS cat_description,
            cc.status AS completion_status,
            cc.timecompleted AS time_completed,
                        las.lastaccess, 
                        las.timeend 
            FROM
            {$CFG->prefix}course_categories ca
            JOIN
            {$CFG->prefix}course co ON
            ca.id = co.category
                        LEFT JOIN 
                        (SELECT ue.lastaccess, ue.timestart, ue.timeend, e.courseid, e.status, ue.userid FROM {$CFG->prefix}enrol e LEFT JOIN {$CFG->prefix}user_enrolments ue ON e.id = ue.enrolid WHERE e.status = 0) as las"
            . " ON las.courseid = co.id   
            LEFT JOIN
            {$CFG->prefix}course_completions cc ON
            cc.course = co.id
            WHERE
            co.visible = '1' and co.id in ($sql_pro)  
            $where
            ORDER BY
            las.lastaccess DESC, cc.status desc, timestart desc
            ";

        $records = $DB->get_records_sql($query);

        $i = 0;
        $now = time();
        $options['noclean'] = true;
        $cursos = array ();
        $fs = get_file_storage();

        foreach ($records as $curso)
        {

            $enrol_methods = enrol_get_instances($curso->remoteid, true);
            $c['name_pro'] = $name_pro;
            $c = get_object_vars ($curso);
            $c['coursetype'] = '';
            $c['self_enrolment'] = 0;
            $c['guest'] = 0;
            // get course group from jomsocial for App
            // added by Dungnv 24 Sept 2016
            $group_course = $this->call_method('getGroupbyCourseID', (int) $curso->remoteid, (string) $username);
            $c['course_group'] = 0;
            if($group_course) {
               $c['course_group'] = (int) $group_course[0];
                $c['activity_count'] = (int) $group_course[1];
            }
            $in = true;
            foreach ($enrol_methods as $instance)
            {
                if (($instance->enrol == 'paypal') || ($instance->enrol == 'joomdle'))
                {
                    $enrol = $instance->enrol;
                    $query = "SELECT cost, currency
                                FROM {$CFG->prefix}enrol
                                where courseid = ? and enrol = ?";
                    $params = array ($curso->remoteid, $enrol);
                    $record =  $DB->get_record_sql($query, $params);
                    $c['cost'] = (float) $record->cost;
                    $c['currency'] = $record->currency;
                }

                // Self-enrolment
                if ($instance->enrol == 'self')
                    $c['self_enrolment'] = 1;
                else
                    $c['self_enrolment'] = 0;


                // Guest access
                if ($instance->enrol == 'guest')
                    $c['guest'] = 1;

                if (($instance->enrolstartdate) && ($instance->enrolenddate))
                {
                    $in = false;
                    if (($instance->enrolstartdate <= $now) && ($instance->enrolenddate >= $now))
                        $in = true;
                }
                else if ($instance->enrolstartdate)
                {
                    $in = false;
                    if (($instance->enrolstartdate <= $now))
                        $in = true;
                }
                else if ($instance->enrolenddate)
                {
                    $in = false;
                    if ($instance->enrolenddate >= $now)
                        $in = true;
                }
            }

            // Check if only guest courses are wanted
            if (($guest) && (!$course_info['guest']))
                continue;

            // Skip not self-enrolable courses if param says so
            if (($available) && (!$c['self_enrolment']))
                continue;


            $c['in_enrol_date'] = $in;

            $c['enroled'] = 0;
            if ($username)
            {
                if (in_array ($curso->remoteid, $my_courses))
                    $c['enroled'] = 1;
            }


            $c['fullname'] = format_string($c['fullname']);
            $c['cat_name'] = format_string($name_pro);

            if ($username) {
                $c['completion_status'] = $curso->completion_status;
                $c['timeend'] = $curso->timeend;
                $query_com = "SELECT timecompleted FROM {$CFG->prefix}course_completions where userid = $user->id and course = $curso->remoteid";
                $result_com = $DB->get_records_sql($query_com);
                if($result_com)
                {
                    foreach ($result_com as $res) {
                        $c['time_completed'] = $res->timecompleted;
                    }
                }
                else
                {
                    $c['time_completed'] = 0;
                }

                $query = "SELECT ue.lastaccess, ue.timestart FROM {$CFG->prefix}enrol e LEFT JOIN {$CFG->prefix}user_enrolments ue ON e.id = ue.enrolid WHERE e.courseid = $curso->remoteid AND ue.userid = $user->id AND e.status = 0";

                $result = $DB->get_records_sql($query);
                if ($result) {
                    $lastAccessArr = array();
                    $timeStartArr = array();
                    foreach ($result as $res) {
                        $lastAccessArr[] = $res->lastaccess;
                        $timeStartArr[] = $res->timestart;
                    }
                    $c['last_access'] = max($lastAccessArr);
                    $c['enrol_time'] = min($timeStartArr);
                } else {
                    if ($swhere != ' ') continue;
                    $c['last_access'] = 0;
                    $c['enrol_time'] = 0;
                }
            }
            $context = context_course::instance($curso->remoteid);
//          $c['summary'] = file_rewrite_pluginfile_urls ($c['summary'], 'pluginfile.php', $context->id, 'course', 'summary', NULL);
//          $c['summary'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $c['summary']);
//          $c['summary'] = format_text($c['summary'], FORMAT_MOODLE, $options);
            $c['summary'] = strip_tags(format_string($c['summary'],true));          
            $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', 0);
            if (count($files) > 0) {
                foreach ($files as $file) {
                    $c['fileid'] = $context->id;
                    $c['filetype'] = $file->get_mimetype();

                    // if (preg_match('/banner/i', $file->get_filename())) {
                    $c['filepath'] = $CFG->wwwroot . '/pluginfile.php/' . $context->id . '/course/overviewfiles/';
                    $c['filename'] = $file->get_filename();
                    // } else {
                    //     $record['filepath'] = $CFG->wwwroot . '/theme/clean/pix/';
                    //     $record['filename'] = 'nocourseimg.jpg';
                    // }
                }
            } else {
                $c['filepath'] = $CFG->wwwroot . '/theme/parenthexis/pix/';
                $c['filename'] = 'nocourseimg.jpg';
            }
            $context = context_coursecat::instance($curso->cat_id);
            $c['cat_description'] = file_rewrite_pluginfile_urls ($c['cat_description'], 'pluginfile.php', $context->id, 'coursecat', 'description', NULL);
            $c['cat_description'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $c['cat_description']);
            $c['cat_description'] = format_text($c['cat_description'], FORMAT_MOODLE, $options);

            $cursos[$i] = $c;

            $i++;


        }
        return ($cursos);
    }

    function my_courses ($username, $order_by_cat = 0, $is_notmalp = 0) {
        global $DB, $CFG;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $user = get_complete_user_data ('username', $username);

        if (!$user)
            return array ();

        if ($order_by_cat)
            $c = enrol_get_users_courses ($user->id, true, NULL, 'category, sortorder ASC');
        else
            $c = enrol_get_users_courses ($user->id, true, 'id,fullname,shortname,category,creator,startdate,duration,price,currency,learningoutcomes,targetaudience,coursesurvey,facilitatedcourse,summary');

        $results = array();
        $courses = array ();
        $i = 0;
        $fs = get_file_storage();
        
        if ($c && count($c) > 0) {
        foreach ($c as $course)
        {
            $record = array ();
            // Check course has user assign who is content creator/facilitator
            $record['has_nonmember'] = 0;
            if ($is_notmalp) {
                if ($course->visible == 1) continue;
                $query1 =   "SELECT * 
                            FROM {$CFG->prefix}course_request_approve
                            WHERE (status = 0 OR status = 1) AND courseid = $course->id";
                $coursesendapp = $DB->get_record_sql($query1);
                
//                $coursesendapp = $DB->get_record('course_request_approve', array('courseid' => $course->id, 'status' => -1));
                if ($coursesendapp && count($coursesendapp) > 0) continue;
//                $userroles = $this->get_user_role($course->id, $username);
                $userRole = $this->get_user_role($course->id);
                if ($userRole['status']) {
                    $userroles = $userRole['roles'];
                } else {
                    $userroles = array();
                }
                $check = true;
                $count_learner = 0;
                $record['content_creator'] = '';
                if (!empty($userroles)) {
                    foreach ($userroles as $userrole) {
                        foreach (json_decode($userrole['role']) as $role) {
                            if($role->roleid == 5) {
                                $count_learner++;
                            }
                            if ($role->roleid == 3) {
                                $record['content_creator'] = $userrole['username'];
                            $check = false;
                            continue;
                        }
                    }
                }
                }
                if ($check == true) continue;
                $record['has_nonmember'] = 1;
            }
            $tempUserArr = array();
            if (!array_key_exists($course->creator, $tempUserArr)) {
                $user_creator = $DB->get_record('user', array('id'=>$course->creator), 'id, username, firstname, lastname');
                $tempUserArr[$course->creator] = $user_creator->firstname.' '.$user_creator->lastname;
            }
            
            $record['id'] = $course->id;
            $record['fullname'] = $course->fullname;
            $record['shortname'] = $course->shortname;
            $record['category'] = $course->category;
            $record['cat_name'] = $this->get_cat_name ($course->category);
//            $record['creator'] = $course->creator;
            $record['count_learner'] = $count_learner;
            $record['creator'] = $tempUserArr[$course->creator];
            $record['startdate'] = $course->startdate;
            $record['duration'] = $course->duration/3600;
            $record['price'] = $course->price;
            $record['currency'] = $course->currency;
            $record['learning_outcomes'] = $course->learningoutcomes;
            $record['target_audience'] = $course->targetaudience;
            $record['course_survey'] = $course->coursesurvey;
            $record['course_facilitated'] = $course->facilitatedcourse;
            $record['summary'] = $course->summary;
            $record['summary'] = strip_tags(format_string($course->summary,true));
//            if (textlib::strlen($record['summary']) > 68) {
//                $record['summary'] = textlib::substr($record['summary'], 0, 68).'...';
//            }
            
            $query = "SELECT MAX(enrolperiod) AS enrolperiod
              FROM {$CFG->prefix}enrol
              WHERE status = 0 AND courseid = $course->id";
            $result = $DB->get_record_sql($query);
            $record['enrolperiod'] = $result->enrolperiod;
            
            $enrol_methods = enrol_get_instances($course->id, true);
            foreach ($enrol_methods as $instance) {
                // Self-enrolment
                if ($instance->enrol == 'self') {
                    $record['self_enrolment'] = 1;
                    break;
                }
                else
                    $record['self_enrolment'] = 0;
            }

            // Check if user can self-unenrol
            $context = context_course::instance($course->id);
            $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', 0);
            if (count($files) > 0) {
                foreach ($files as $file) {
                    $record['fileid'] = $context->id;
                    $record['filetype'] = $file->get_mimetype();

                    // if (preg_match('/banner/i', $file->get_filename())) {
                    $record['filepath'] = $CFG->wwwroot . '/pluginfile.php/' . $context->id . '/course/overviewfiles/';
                    $record['filename'] = $file->get_filename();
                    // } else {
                    //     $record['filepath'] = $CFG->wwwroot . '/theme/clean/pix/';
                    //     $record['filename'] = 'nocourseimg.jpg';
                    // }
                }
            } else {
                $record['filepath'] = $CFG->wwwroot . '/theme/parenthexis/pix/';
                $record['filename'] = 'nocourseimg.jpg';
            }
            if ((has_capability('enrol/manual:unenrolself', $context, $user->id)) || (has_capability('enrol/self:unenrolself', $context, $user->id)))
                $record['can_unenrol'] = 1;
            else
                $record['can_unenrol'] = 0;
            
            $record['course_status'] = '';
            $course_status = $this->get_course_status($course->id);  
            if($course_status) {
                $record['course_status'] = $course_status;
            }

            $courses[$i] = $record;
            $i++;
        }
            $results['status'] = 1;
            $results['message'] = 'Success.';
            $results['courses'] = $courses;
        } else {
            $results['status'] = 0;
            $results['message'] = 'Not found.';
            $results['courses'] = array();
    }
        return $results;
    }

    function my_own_courses ($username, $catid = '', $limit = 0, $offset = 0, $is_malp = 0) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $user = get_complete_user_data ('username', $username);

        if (!$user)
            return array ();
        if (!$limit) $limit = 0;
        if (!$offset) $offset = 0;

        $limitText = ($limit) ? "LIMIT $limit OFFSET $offset" : '';

        // $where = "WHERE co.visible = '1' AND co.creator = $user->id";
        $where = '';
        $where_arr = array();
        
        if ($is_malp) {
            $where_arr[] = "(co.visible = 0 OR co.visibleold = 0)";
        } else {
            $where_arr[] = "co.creator = $user->id";  //LP's Courses may be not created by LP user
        }
        if ($catid != '') {
//            $catids = explode(',', $catid);
//            if (count($catids) > 1) {
                $where_arr[] = "ca.id IN ($catid)";
//            } else if ($catids == 1) {
//                $where_arr[] = "ca.id = $catid";
//            }
            }
        if (!empty($where_arr)) {
            $where = 'WHERE '.implode(' AND ', $where_arr);
        }

        $query =
            "SELECT
            co.id          AS remoteid,
            ca.id          AS cat_id,
            co.fullname,
            co.shortname,
            co.idnumber,
            co.summary,
            co.startdate,
            co.timecreated AS created,
            co.timemodified AS modified,
            co.timepublish,
            co.visible,
            co.creator,
            co.price,
            co.duration,
            co.learningoutcomes,
            co.targetaudience,
            co.coursesurvey,
            co.facilitatedcourse,
            ca.name        AS cat_name,
            ca.description AS cat_description
            FROM
                {$CFG->prefix}course_categories ca
            JOIN
                {$CFG->prefix}course co ON ca.id = co.category
            $where
            ORDER BY modified DESC
            $limitText
            ";

        $records = $DB->get_records_sql($query);
        $data = array();
        $courses = array ();
        $cur_coursesid = array();
        $i = 0;
        $fs = get_file_storage();
        $tempUserArr = array();
        if ($records) {
            foreach ($records as $course) {
                if ($is_malp) {
                    $sql = "SELECT * FROM {$CFG->prefix}course_request_approve "
                    . "WHERE courseid = " . $course->remoteid ." AND status in (0, 1, 2)";
                    $check_pending = $DB->get_records_sql($sql);
                    if ($check_pending && count($check_pending) > 0) continue;
                }
                if (!array_key_exists($course->creator, $tempUserArr)) {
                    $tempUserArr[$course->creator] = $DB->get_record('user', array('id'=>$course->creator), 'id, username')->username;
                }
                $record = array ();
                $record['id'] = $course->remoteid;
                $record['fullname'] = $course->fullname;
                $record['shortname'] = $course->shortname;
                $record['idnumber'] = $course->idnumber;
                $record['summary'] = $course->summary;
                $record['summary'] = strip_tags(format_string($course->summary,true));
    //            if (textlib::strlen($record['summary']) > 68) {
    //                $record['summary'] = textlib::substr($record['summary'], 0, 68).'...';
    //            }
                $record['startdate'] = $course->startdate;
                $record['created'] = $course->created;
                $record['modified'] = $course->modified;
                $record['timepublish'] = $course->timepublish;
//                if ($course->visible &&  $course->timepublish == 0) $record['timepublish'] = 1483203600; 
                $record['category'] = $course->cat_id;
    //            $record['cat_name'] = $this->get_cat_name ($course->category);
                $record['cat_name'] = $course->cat_name;
                $record['cat_description'] = $course->cat_description;
                $record['creator'] = $tempUserArr[$course->creator];
                $record['price'] = $course->price;
                $record['currency'] = $course->currency;
                $record['duration'] = (float)$course->duration/3600;
                $record['learning_outcomes'] = $course->learningoutcomes;
                $record['target_audience'] = $course->targetaudience;
                $record['course_survey'] = $course->coursesurvey;
                $record['course_facilitated'] = $course->facilitatedcourse;

                $query = "SELECT MAX(enrolperiod) AS enrolperiod
                  FROM {$CFG->prefix}enrol
                  WHERE status = 0 AND courseid = $course->remoteid";
                $result = $DB->get_record_sql($query);
                $record['enrolperiod'] = $result->enrolperiod;

                $query2 = "SELECT MAX(timestart) as timestart, MAX( timeend) as timeend
                  FROM {$CFG->prefix}user_enrolments ue JOIN {$CFG->prefix}enrol e on e.id = ue.enrolid
                  WHERE ue.status = 0 AND e.courseid = $course->remoteid AND ue.userid = $user->id";
                $res = $DB->get_record_sql($query2);

                $record['enrolmenttimestart'] = $res->timestart;
                $record['enrolmenttimeend'] = $res->timeend;
                
                $enrol_methods = enrol_get_instances($course->remoteid, true);
                foreach ($enrol_methods as $instance) {
                    // Self-enrolment
                    if ($instance->enrol == 'self') {
                        $record['self_enrolment'] = 1;
                        break;
                    }
                    else
                        $record['self_enrolment'] = 0;
                }
                
                // Check course has user assign who is content creator/facilitator
                $record['has_nonmember'] = 0;
                $record['content_creator'] = '';
                $countlearner = 0;
//                $userroles = $this->get_user_role($course->remoteid);
                $userRole = $this->get_user_role($course->remoteid);
                if ($userRole['status']) {
                    $userroles = $userRole['roles'];
                } else {
                    $userroles = array();
                }
                if (!empty($userroles)) {
                    foreach ($userroles as $userrole) {
                        foreach (json_decode($userrole['role']) as $role) {
                            if ($role->roleid == 3) {
                            $record['has_nonmember'] = 1;
                                $record['content_creator'] = $userrole['username'];
                        }
                            if ($role->roleid == 5) {
                                $countlearner++;
                    }
                }
                }
                }
                $record['count_learner'] = $countlearner;
                
                $record['course_status'] = '';
                $course_status = $this->get_course_status($course->remoteid);  
                if($course_status) {
                    $record['course_status'] = $course_status;
                }

                // Check if user can self-unenrol
                $context = context_course::instance($course->remoteid);
                $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', 0);
                if (count($files) > 0) {
                    foreach ($files as $file) {
                        $record['fileid'] = $context->id;
                        $record['filetype'] = $file->get_mimetype();

                        // if (preg_match('/banner/i', $file->get_filename())) {
                        $record['filepath'] = $CFG->wwwroot . '/pluginfile.php/' . $context->id . '/course/overviewfiles/';
                        $record['filename'] = $file->get_filename();
                        // } else {
                        //     $record['filepath'] = $CFG->wwwroot . '/theme/clean/pix/';
                        //     $record['filename'] = 'nocourseimg.jpg';
                        // }
                    }
                } else {
                    $record['filetype'] = "image/jpeg";
                    $record['filepath'] = $CFG->wwwroot . '/theme/parenthexis/pix/';
                    $record['filename'] = 'nocourseimg.jpg';
                }
                if ((has_capability('enrol/manual:unenrolself', $context, $user->id)) || (has_capability('enrol/self:unenrolself', $context, $user->id)))
                    $record['can_unenrol'] = 1;
                else
                    $record['can_unenrol'] = 0;

                $courses[$i] = $record;
                $cur_coursesid[] = $record['id'];
                $i++;
            }
        }
        
            // list course is Content creator
            if ($is_malp) {
                $courses_cc = $this->my_courses($username, 0, 1);
                if ($courses_cc['status']) {
                    foreach ($courses_cc['courses'] as $course_cc) {
                        if (!in_array($course_cc['id'], $cur_coursesid)) {
                        $courses[$i] = $course_cc;
                        }
                        $i++;
                    }
                }
            }
            if (!empty($courses)) {
                $data['status'] = 1;
                $data['message'] = 'Success';
                $data['courses'] = $courses;
            } else {
                $data['status'] = 0;
                $data['message'] = 'Not found.';
                $data['courses'] = $courses;
            }
//        } else {
//            $data['status'] = 0;
//            $data['message'] = 'Not found.';
//            $data['courses'] = $courses;
//        }
        
        return $data;
    }


    function get_lastest_course($limit) {
        global $CFG, $DB;
        $records = $DB->get_records('course', null, 'id DESC', '*', 0, $limit);
        $courses = array ();
        $i = 0;
        $fs = get_file_storage();
        foreach ($records as $course)
        {
            $record = array ();
            $record['id'] = $course->id;
            $record['fullname'] = $course->fullname;
            $record['category'] = $course->category;
            $record['cat_name'] = $this->get_cat_name ($course->category);

            // Check if user can self-unenrol
            $context = context_course::instance($course->id);
            $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', 0);
            if (count($files) > 0) {
                foreach ($files as $file) {
                    $record['fileid'] = $context->id;
                    $record['filetype'] = $file->get_mimetype();

                    // if (preg_match('/banner/i', $file->get_filename())) {
                    $record['filepath'] = $CFG->wwwroot . '/pluginfile.php/' . $context->id . '/course/overviewfiles/';
                    $record['filename'] = $file->get_filename();
                    // } else {
                    //     $record['filepath'] = $CFG->wwwroot . '/theme/clean/pix/';
                    //     $record['filename'] = 'nocourseimg.jpg';
                    // }
                }
            } else {
                $record['filepath'] = $CFG->wwwroot . '/theme/parenthexis/pix/';
                $record['filename'] = 'nocourseimg.jpg';
            }
            if ((has_capability('enrol/manual:unenrolself', $context, $user->id)) || (has_capability('enrol/self:unenrolself', $context, $user->id)))
                $record['can_unenrol'] = 1;
            else
                $record['can_unenrol'] = 0;

            $courses[$i] = $record;
            $i++;
        }
        return $courses;
    }

    // Returns all courses in which the user has a role assigned
    function my_all_courses ($username) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);

        $query = " SELECT distinct c.id as remoteid, c.fullname, ca.name as cat_name, ca.id as cat_id, ra.roleid
                    FROM {$CFG->prefix}course as c, {$CFG->prefix}role_assignments AS ra, {$CFG->prefix}user AS u, {$CFG->prefix}context AS ct,  {$CFG->prefix}course_categories ca
                    WHERE c.id = ct.instanceid  AND ra.userid = u.id AND ct.id = ra.contextid AND ca.id = c.category and u.username= ?";

        // Get user student courses
        $user = get_complete_user_data ('username', $username);
        $c = enrol_get_users_courses ($user->id, true); // get only non supended enrolments
        $my_courses = array ();
        foreach ($c as $course)
        {
            $my_courses[] = $course->id;
        }


        $params = array ($username);
        $records =  $DB->get_records_sql($query, $params);
        $data = array ();
        $i = 0;
        foreach ($records as $p)
        {
            if ($p->roleid == 5) // If student, check that enrolment is not suspended
            {
                if (!in_array ($p->remoteid, $my_courses))
                    continue;
            }

            $e['id'] = $p->remoteid;
            $e['fullname'] = $p->fullname;
            $e['fullname'] = format_string($e['fullname']);
            $e['category'] = $p->cat_id;

            $data[$i] = $e;
            $i++;
        }
        return $data;
    }


    function my_teachers ($username) {
        global $CFG;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $user = get_complete_user_data ('username', $username);
        $c = enrol_get_users_courses ($user->id);

        $courses = array ();
        $i = 0;
        foreach ($c as $course)
        {
            $record = array ();
            $record['id'] = $course->id;
            $record['fullname'] = $course->fullname;

            $context = context_course::instance($course->id);
            /* 3 indica profesores editores (table mdl_role) */
            $profs = get_role_users(3 , $context);
            $data = array ();
            foreach ($profs as $p)
            {
                $e['firstname'] = $p->firstname;
                $e['lastname'] = $p->lastname;
                $e['username'] = $p->username;

                $data[] = $e;
            }
            $record['teachers'] = $data;

            $courses[$i] = $record;
            $i++;
        }
        return $courses;
    }

    function my_classmates ($username) {
        global $CFG;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $user = get_complete_user_data ('username', $username);
        $c = enrol_get_users_courses ($user->id);

        $courses = array ();
        $i = 0;
        $data = array ();
        foreach ($c as $course)
        {
            $mates = $this->get_course_students ($course->id);

            foreach ($mates as $p)
            {
                $e['firstname'] = $p['firstname'];
                $e['lastname'] = $p['lastname'];
                $e['username'] = $p['username'];

                $data[$e['username']] = $e;
            }
        }
        return $data;
    }
    /**
     * Returns course list
     *
     * @param int $available If true, return only self enrollable courses
     */
    function list_courses ($available = 0, $sortby = 'created', $guest = 0, $username = '', $where = '', $swhere = '', $version = 'app') {
        global $CFG, $DB;

        $cc_on = '';

        if ($swhere == 0) $swhere = '';

        if ($username) {
            $user = get_complete_user_data ('username', $username);
            $courses = enrol_get_users_courses ($user->id, true);

            $my_courses = array ();
            foreach ($courses as $course)
            {
                $my_courses[] = $course->id;
            }
            if (!empty($my_courses)) {
                $where .= ' AND co.id IN ('.implode($my_courses, ',').') AND co.creator <> '.$user->id.' AND las.userid = '.$user->id;
            } else {
                return array();
            }
            $cc_on = ' AND cc.userid = '.$user->id;

            $programid = get_programid($user->id);
            $course_id = array();
            $course_com = array();
            $course_las = array();
            $course_start = array();
            foreach ($programid as $id) {
                $course_id[] =  get_date_program($user->id,$id->id);
                $course_com[] = get_date_program_com($user->id,$id->id);
                $course_las[] = get_date_program_las($user->id,$id->id);
                $course_start[] = get_date_program_start($user->id,$id->id);
            }
        }

        $query =
            "SELECT
            co.id          AS remoteid,
            ca.id          AS cat_id,
            co.sortorder,
            co.fullname,
            co.shortname,
            co.idnumber,
            co.summary,
            co.startdate,
            co.timecreated as created,
            co.timemodified as modified,
            co.timepublish as timepublish,
            ca.name        AS cat_name,
            ca.description AS cat_description,
            cc.status AS completion_status,
            cc.timecompleted AS time_completed,
                        las.lastaccess ,
                        las.timestart,
                        las.timeend,
                        las.enrol AS enrol_type 
            FROM
                            {$CFG->prefix}course_categories ca
            JOIN
                            {$CFG->prefix}course co ON ca.id = co.category
                        LEFT JOIN 
                            (SELECT ue.lastaccess, ue.timestart, ue.timeend, e.courseid, e.enrol, ue.userid FROM {$CFG->prefix}enrol e LEFT JOIN {$CFG->prefix}user_enrolments ue ON e.id = ue.enrolid WHERE e.status = 0) as las
                        ON las.courseid = co.id
            LEFT JOIN
                            {$CFG->prefix}course_completions cc ON
            cc.course = co.id $cc_on
            WHERE
            co.visible = '1'
            $where
            ORDER BY
            cc.timecompleted DESC, las.lastaccess DESC,las.timestart DESC,  cc.status desc, $sortby
            ";

        $records = $DB->get_records_sql($query);

        $now = time();
        $options['noclean'] = true;
        $cursos = array ();
        $fs = get_file_storage();

        $pro_arr = array();
        foreach ($records as $curso) {
            if ($username) {
                $query = "SELECT e.id, ue.lastaccess, ue.timestart, ue.timeend, e.enrol FROM {$CFG->prefix}enrol e LEFT JOIN {$CFG->prefix}user_enrolments ue ON e.id = ue.enrolid WHERE e.courseid = $curso->remoteid AND ue.userid = $user->id";
                $result = $DB->get_records_sql($query);
            }
            $enrol_type = array();
            if ($result) {
                $timeend = 1;
                foreach( $result as $rs) {
                    $timestart = $rs->timestart;

                    if ($timeend != 0) {
                        if ($rs->timeend == 0) $timeend = 0; else
                            if ($timeend < $rs->timeend) $timeend = $rs->timeend;
                    }

                    $enrol_type[] = $rs->enrol;
                }
            } else {
                $timeend = $curso->timeend;
            }

            if ($username) $courses =  get_courseid_program($user->id, $curso->remoteid);
            if (empty($courses) || (count($enrol_type) > 1) ) {
            // Khong thuoc prog nao
                $enrol_methods = enrol_get_instances($curso->remoteid, true);

                $c = get_object_vars ($curso);
                $c['coursetype'] = 'single_course';
                $c['self_enrolment'] = 0;
                $c['guest'] = 0;
                // get course group from jomsocial for app
                // added by Dungnv 24 Sept 2016
                if (is_null($version)) {
                    $group_course = $this->call_method('getGroupbyCourseID', (int) $curso->remoteid, (string) $username);
                $c['course_group'] = 0;
                    if ($group_course) {
                    $c['course_group'] = (int) $group_course[0];
                }
                }

                $in = true;
                foreach ($enrol_methods as $instance) {
                    if (($instance->enrol == 'paypal') || ($instance->enrol == 'joomdle')) {
                        $enrol = $instance->enrol;
                        $query = "SELECT cost, currency
                                FROM {$CFG->prefix}enrol
                                where courseid = ? and enrol = ?";
                        $params = array ($curso->remoteid, $enrol);
                        $record =  $DB->get_record_sql($query, $params);
                        $c['cost'] = (float) $record->cost;
                        $c['currency'] = $record->currency;
                    }

                    // Self-enrolment
                    if ($instance->enrol == 'self')
                        $c['self_enrolment'] = 1;
                    else
                        $c['self_enrolment'] = 0;


                    // Guest access
                    if ($instance->enrol == 'guest')
                        $c['guest'] = 1;

                    if (($instance->enrolstartdate) && ($instance->enrolenddate))
                    {
                        $in = false;
                        if (($instance->enrolstartdate <= $now) && ($instance->enrolenddate >= $now))
                            $in = true;
                    }
                    else if ($instance->enrolstartdate)
                    {
                        $in = false;
                        if (($instance->enrolstartdate <= $now))
                            $in = true;
                    }
                    else if ($instance->enrolenddate)
                    {
                        $in = false;
                        if ($instance->enrolenddate >= $now)
                            $in = true;
                    }
                }

                // Check if only guest courses are wanted
                if ($guest)
                    continue;

                // Skip not self-enrolable courses if param says so
                if (($available) && (!$c['self_enrolment']))
                    continue;

                $c['in_enrol_date'] = $in;

                $c['enroled'] = 0;
                if ($username) {
                    if (in_array ($curso->remoteid, $my_courses))
                        $c['enroled'] = 1;
                }

                $c['fullname'] = format_string($c['fullname']);
                $c['cat_name'] = format_string($c['cat_name']);
                
                if ($username) {
                    $c['completion_status'] = (int) $curso->completion_status;
                    $c['timeend'] = $timeend;
                    $query_com = "SELECT timecompleted FROM {$CFG->prefix}course_completions where userid = $user->id and course = $curso->remoteid";
                    $result_com = $DB->get_records_sql($query_com);
                    if($result_com)
                    {
                        foreach ($result_com as $res) {
                            $c['time_completed'] = (int) $res->timecompleted;
                        }
                    }
                    else
                    {
                        $c['time_completed'] = 0;
                    }
                    $query = "SELECT ue.lastaccess, ue.timestart FROM {$CFG->prefix}enrol e LEFT JOIN {$CFG->prefix}user_enrolments ue ON e.id = ue.enrolid WHERE e.courseid = $curso->remoteid AND ue.userid = $user->id AND e.status = 0 $swhere";

                    $result = $DB->get_records_sql($query);
                    if ($result) {
                        $lastAccessArr = array();
                        $timeStartArr = array();
                        foreach ($result as $res) {
                            $lastAccessArr[] = $res->lastaccess ? $res->lastaccess : 0;
                            $timeStartArr[] = $res->timestart;
                        }
                        $c['last_access'] = max($lastAccessArr);
                        $c['enrol_time'] = min($timeStartArr);
                    } else {
                        if ($swhere != ' ') continue;
                        $c['last_access'] = 0;
                        $c['enrol_time'] = 0;
                    }
                    
                    // Check facilitator
                    $c['isFacilitator'] = $this->checkFacilitator($curso->remoteid, $username);
                }
                $context = context_course::instance($curso->remoteid);

                $c['summary'] = strip_tags(format_string($c['summary'],true));
                if (textlib::strlen($c['summary']) > 68) {
                    $c['summary'] = textlib::substr($c['summary'], 0, 68).'...';
                }
                $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', 0);
                if (count($files) > 0) {
                    foreach ($files as $file) {
                        $c['fileid'] = $context->id;
                        $c['filetype'] = $file->get_mimetype();
                        $c['filepath'] = $CFG->wwwroot . '/pluginfile.php/' . $context->id . '/course/overviewfiles/';
                        $c['filename'] = $file->get_filename();
                    }
                } else {
                    $c['filepath'] = $CFG->wwwroot . '/theme/parenthexis/pix/';
                    $c['filename'] = 'nocourseimg.jpg';
                }
                $context = context_coursecat::instance($curso->cat_id);
                $c['cat_description'] = file_rewrite_pluginfile_urls ($c['cat_description'], 'pluginfile.php', $context->id, 'coursecat', 'description', NULL);
                $c['cat_description'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $c['cat_description']);
                $c['cat_description'] = format_text($c['cat_description'], FORMAT_MOODLE, $options);

                $cursos[] = $c;
            }
            if ( in_array($curso->remoteid, $course_id) && !empty($courses)  ) {
                if (in_array($curso->remoteid,$course_com)) {
                    $time_com = (int) $curso->time_completed;
                    $time_las = 0;
                } elseif(in_array($curso->remoteid, $course_las)) {
                    $time_com = 0;
                    $time_las = $curso->lastaccess;
                }

            if ($username) {
                    $query = "SELECT ue.lastaccess, ue.timestart FROM {$CFG->prefix}enrol e LEFT JOIN {$CFG->prefix}user_enrolments ue ON e.id = ue.enrolid WHERE e.courseid = $curso->remoteid AND ue.userid = $user->id AND e.status = 0 $swhere";

                    $result = $DB->get_records_sql($query);
                    if ($result) {
                        $lastAccessArr = array();
                        $timeStartArr = array();
                        foreach ($result as $res) {
                            $lastAccessArr[] = $res->lastaccess;
                            $timeStartArr[] = $res->timestart;
                        }
                        $c['enrol_time'] = min($timeStartArr);
                    } else {
                        $c['enrol_time'] = 0;
                    }
                }

                $course_program =  get_program($user->id,$curso->remoteid);
                if ($course_program) {
                    foreach ($course_program  as $pro)
                    {
                        if (!in_array($pro->id, $pro_arr)) {
                            if (is_null($version)) {
                                $group_course = $this->call_method('getGroupbyCourseID', 0, (string) $username);
                            $c['course_group'] = 0;
                            if($group_course) {
                                $c['course_group'] = (int) $group_course[0];
                            }
                            }

                            $c['last_access'] =  (int) $time_las;
                            $c['coursetype']='program';
                            $c['isFacilitator'] = false;
                            $c['self_enrolment'] = 0;
                            $c['remoteid'] = 0;
                            $c['guest'] = 0;
                            $c['cat_id'] = $pro->id;
                            $c['cat_name'] = '';
                            $c['cat_description']='';
                            $c['sortorder'] = 0;
                            $c['fullname'] = $pro->fullname;
                            $c['shortname'] = '';
                            $c['idnumber']='';
                            $c['summary'] = strip_tags(format_string($pro->summary,true));
                            if (textlib::strlen($c['summary']) > 68) {
                                $c['summary'] = textlib::substr($c['summary'], 0, 150).'...';
                            }
                            $c['startdate']=0;
                            $c['modified']=0;
                            $c['created']=0;
                            $c['completion_status'] = 0;
                            $c['time_completed'] = $time_com;
                            $c['cost'] =0;
                            $c['currency']='';
                            $c['self_enrolment']=0;
                            $c['enroled']=0;
                            $c['in_enrol_date']=0;
                            $c['guest']=0;
                            $c['fileid']='';
                            $c['filetype']='';
                            $c['filepath']=0;
                            $c['filename']=0;

                            $query =
                                "SELECT completiontime, completionevent
                                            FROM
                                            {$CFG->prefix}prog_assignment
                                            WHERE
                                            assignmenttypeid = $user->id and programid = $pro->id
                                            ";

                            $res = $DB->get_records_sql($query);
                            $max = 0;
                            foreach ($res as $key => $value) {
                                if ($value->completiontime != -1) {
                                    if ($value->completionevent) {
                                        $max = max($max, time() + ($value->completiontime));
                                    } else {
                                        $max = max($max, $value->completiontime);
                                    }
                                }
                            }

                            $c['timeend'] = $max;

                            $program_context = context_program::instance($pro->id);
                            $files = $fs->get_area_files($program_context->id, 'totara_program', 'overviewfiles', 0);
                            if (count($files) > 0) {
                                foreach ($files as $file) {
                                    $c['filename'] = $file->get_filename();

                                    $c['filepath'] =  file_encode_url("$CFG->wwwroot/pluginfile.php",
                                        '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                                        $file->get_filearea(). $file->get_filepath().$file->get_itemid().$file->get_filepath().$file->get_filename(), !$isimage);
                                }
                            } else {
                                $c['filepath'] = $CFG->wwwroot . '/theme/parenthexis/pix/';
                                $c['filename'] = 'nocourseimg.jpg';
                            }
                            $cursos[] = $c;

                            $pro_arr[] = $pro->id;
                        }
                    }
                }

            }
        }

        return ($cursos);
    }
        
    function list_courses2 ($available = 0, $sortby = 'created', $guest = 0, $username = '', $where = '', $swhere = '', $version = 'app') {
        global $CFG, $DB;
        if ($swhere == 0) $swhere = '';
        if($where == 0) $where = '';
        if (!$username) {
            $query =
                "SELECT
            co.id AS remoteid,
            co.sortorder,
            co.fullname,
            co.shortname,
            co.idnumber,
            co.summary,
            co.startdate,
            co.timecreated as created,
            co.timemodified as modified,
            co.timepublish as timepublish,
            ca.id AS cat_id,
            ca.name AS cat_name,
            ca.description AS cat_description
            FROM
                {$CFG->prefix}enrol e
            LEFT JOIN
                {$CFG->prefix}course co ON e.courseid = co.id
            LEFT JOIN
                {$CFG->prefix}course_categories ca  ON ca.id = co.category
            WHERE
            co.visible = '1' AND e.status = 0
            $where
            ORDER BY
            $sortby
            ";
            $cursos = $DB->get_records_sql($query);
        } else {
            $user = get_complete_user_data ('username', $username);
            $courses = enrol_get_users_courses ($user->id, true);

            $my_courses = array ();
            foreach ($courses as $course) {
                $my_courses[] = $course->id;
            }
            if (!empty($my_courses)) {
                $where .= ' AND co.id IN ('.implode($my_courses, ',').') AND co.creator <> '.$user->id.' AND ue.userid = '.$user->id;
            } else {
                return array();
            }
            $programid = get_programid($user->id);
            $course_id = array();
            $course_com = array();
            $course_las = array();
            $course_start = array();
            foreach ($programid as $id) {
                $course_id[] =  get_date_program($user->id,$id->id);
                $course_com[] = get_date_program_com($user->id,$id->id);
                $course_las[] = get_date_program_las($user->id,$id->id);
                $course_start[] = get_date_program_start($user->id,$id->id);
            }
            $query =
                "SELECT
                ue.id AS ue_id,
                ue.lastaccess,
                ue.timestart,
                ue.timeend,
                e.id AS enrol_id,
                co.id AS remoteid,
                co.sortorder,
                co.fullname,
                co.shortname,
                co.idnumber,
                co.summary,
                co.startdate,
                co.creator,
                co.duration,
                co.timecreated as created,
                co.timemodified as modified,
                co.timepublish,
                co.visible,
                ca.id AS cat_id,
                ca.name AS cat_name,
                ca.description AS cat_description,
                cc.status AS completion_status,
                cc.timecompleted AS time_completed
                FROM
                    {$CFG->prefix}user_enrolments ue
                LEFT JOIN
                    {$CFG->prefix}enrol e ON e.id = ue.enrolid
                LEFT JOIN
                    {$CFG->prefix}course co ON e.courseid = co.id
                LEFT JOIN
                    {$CFG->prefix}course_categories ca  ON ca.id = co.category
                LEFT JOIN
                    {$CFG->prefix}course_completions cc ON cc.course = co.id AND cc.userid = $user->id
                WHERE
                co.visible = '1' AND e.status = 0
                $where
                ORDER BY
                cc.timecompleted DESC, ue.lastaccess DESC, ue.timestart DESC, cc.status desc, $sortby
                ";
    
            $records = $DB->get_records_sql($query);
            $now = time();
            $fs = get_file_storage();
            $options['noclean'] = true;
            $cursos = array ();
            $pro_arr = array();
    
            foreach ($records as $curso) {
                $courses =  get_courseid_program($user->id, $curso->remoteid);
                $curso->self_enrolment = 0;
                $curso->guest = 0;
                // Khong thuoc prog nao
                if (empty($courses)  ) {
                    if (array_key_exists($curso->remoteid, $cursos)) continue;
                    $enrol_methods = enrol_get_instances($curso->remoteid, true);
                    $c = get_object_vars ($curso);
                    $c['coursetype'] = 'single_course';
                    if (is_null($version)) {
                        $group_course = $this->call_method('getGroupbyCourseID', (int) $curso->remoteid, (string) $username);       
                        $c['course_group'] = 0;
                        if ($group_course) {
                            $c['course_group'] = (int) $group_course[0];
                            $c['activity_count'] = (int) $group_course[1];
                        }
                    }
                    $in_enrol_date = true;
                    $c['cost'] = 0;
                    foreach ($enrol_methods as $instance) {
                        if (($instance->enrol == 'paypal') || ($instance->enrol == 'joomdle')) {
                            $enrol = $instance->enrol;
                            $query = "SELECT cost, currency
                                    FROM {$CFG->prefix}enrol
                                    where courseid = ? and enrol = ?";
                            $params = array ($curso->remoteid, $enrol);
                            $record =  $DB->get_record_sql($query, $params);
                            if ($c['cost'] < $record->cost) $c['cost'] = (float) $record->cost;
                            $c['currency'] = $record->currency;
                        }
                        // Self-enrolment
                        if ($instance->enrol == 'self') $c['self_enrolment'] = 1;
    
                        // Guest access
                        if ($instance->enrol == 'guest') $c['guest'] = 1;
    
                        if (($instance->enrolstartdate) && ($instance->enrolenddate)) {
                            $in_enrol_date = false;
                            if (($instance->enrolstartdate <= $now) && ($instance->enrolenddate >= $now))
                                $in_enrol_date = true;
                        } else if ($instance->enrolstartdate) {
                            $in_enrol_date = false;
                            if (($instance->enrolstartdate <= $now))
                                $in_enrol_date = true;
                        } else if ($instance->enrolenddate) {
                            $in_enrol_date = false;
                            if ($instance->enrolenddate >= $now)
                                $in_enrol_date = true;
                        }
                    }
                    // Check if only guest courses are wanted
                    if ($guest) continue;
                    // Skip not self-enrolable courses if param says so
                    if (($available) && (!$c['self_enrolment'])) continue;
    
                    $c['in_enrol_date'] = $in_enrol_date;
                    $c['enroled'] = 0;
                    if (in_array ($curso->remoteid, $my_courses)) $c['enroled'] = 1;
    
                    $c['fullname'] = format_string($c['fullname']);
                    $c['cat_name'] = format_string($c['cat_name']);
                    $c['completion_status'] = (int)$curso->completion_status;
                    $c['timepublish'] = (int) $curso->timepublish;
    
                    $query_com = "SELECT timecompleted FROM {$CFG->prefix}course_completions WHERE userid = $user->id AND course = $curso->remoteid";
                    $result_com = $DB->get_records_sql($query_com);
    
                    $c['time_completed'] = 0;
                    if($result_com) {
                        foreach ($result_com as $res) {
                            if ($c['time_completed'] < $res->timecompleted) $c['time_completed'] = (int) $res->timecompleted;
                        }
                    }
                    $query = "SELECT ue.lastaccess, ue.timestart FROM {$CFG->prefix}enrol e LEFT JOIN {$CFG->prefix}user_enrolments ue ON e.id = ue.enrolid WHERE e.courseid = $curso->remoteid AND ue.userid = $user->id AND e.status = 0 $swhere";
                    $result = $DB->get_records_sql($query);
                    if ($result) {
                        $lastAccessArr = array();
                        $timeStartArr = array();
                        foreach ($result as $res) {
                            $lastAccessArr[] = $res->lastaccess ? $res->lastaccess : 0;
                            $timeStartArr[] = $res->timestart;
                        }
                        $c['last_access'] = max($lastAccessArr);
                        $c['enrol_time'] = min($timeStartArr);
                    } else {
                        if ($swhere != ' ') continue;
                        $c['last_access'] = 0;
                        $c['enrol_time'] = 0;
                    }
    
                    $tempUserArr = array();
                    if (!array_key_exists($c['creator'], $tempUserArr)) {
                        $user_creator = $DB->get_record('user', array('id'=>$c['creator']), 'id, username, firstname, lastname');
                        $tempUserArr[$c['creator']] = $user_creator->firstname .' '.$user_creator->lastname;
                    }
                    $c['creator'] = $tempUserArr[$c['creator']];
                    $c['isLearner'] = false;
                    $countlearner = 0;
                    $userRole = $this->get_user_role($curso->remoteid, $username);
                    
                    if ($userRole['status']) {
                        $userroles = $userRole['roles'];
                    } else {
                        $userroles = array();
                    }
                    $isLearner = true;
                    if (!empty($userroles)) {
                        foreach ($userroles as $userrole) {
                            foreach (json_decode($userrole['role']) as $role) {
                                // roleid = 5 (student)
                                // roleid = 4 (teacher)
                                if ($role->roleid == 5) {
                                    $countlearner++;
                                    $c['isLearner'] = true;
                                }
                                if(in_array($role->roleid, array('4', '5')) !== false) {
                                    $isLearner = false;
                            }
                        }
                    }
                    }
                    $c['count_learner'] = $countlearner;
                    $c['duration'] = (float)($curso->duration/3600);
                    // Check facilitator
                    $c['isFacilitator'] = $this->checkFacilitator($curso->remoteid, $username);
                    $context = context_course::instance($curso->remoteid);
                    $c['summary'] = strip_tags(format_string($c['summary'],true));

                    $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', 0);
                    if (count($files) > 0) {
                        foreach ($files as $file) {
                            $c['fileid'] = $context->id;
                            $c['filetype'] = $file->get_mimetype();
                            $c['filepath'] = $CFG->wwwroot . '/pluginfile.php/' . $context->id . '/course/overviewfiles/';
                            $c['filename'] = $file->get_filename();
                        }
                    } else {
                        $c['filepath'] = $CFG->wwwroot . '/theme/parenthexis/pix/';
                        $c['filename'] = 'nocourseimg.jpg';
                    }
                    $context = context_coursecat::instance($curso->cat_id);
                    $c['cat_description'] = file_rewrite_pluginfile_urls ($c['cat_description'], 'pluginfile.php', $context->id, 'coursecat', 'description', NULL);
                    $c['cat_description'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $c['cat_description']);
                    $c['cat_description'] = format_text($c['cat_description'], FORMAT_MOODLE, $options);
    
                    if($isLearner) {
                        continue;
                    }
                    $cursos[] = $c;
    
                } else if ( in_array($curso->remoteid, $course_id) && !empty($courses)  ) {
                    if (in_array($curso->remoteid, $course_com)) {
                        $time_com = (int) $curso->time_completed;
                        $time_las = 0;
                    } elseif(in_array($curso->remoteid, $course_las)) {
                        $time_com = 0;
                        $time_las = $curso->lastaccess;
                    }
    
                    $query = "SELECT ue.lastaccess, ue.timestart FROM {$CFG->prefix}enrol e
                        LEFT JOIN {$CFG->prefix}user_enrolments ue ON e.id = ue.enrolid
                        WHERE e.courseid = $curso->remoteid AND ue.userid = $user->id AND e.status = 0 $swhere";
    
                    $result = $DB->get_records_sql($query);
                    if ($result) {
                        $lastAccessArr = array();
                        $timeStartArr = array();
                        foreach ($result as $res) {
                            $lastAccessArr[] = $res->lastaccess;
                            $timeStartArr[] = $res->timestart;
                        }
                        $c['enrol_time'] = min($timeStartArr);
                    } else {
                        $c['enrol_time'] = 0;
                    }
    
                    $course_program =  get_program($user->id, $curso->remoteid);
                    if ($course_program) {
                        foreach ($course_program  as $pro) {
                            if (!in_array($pro->id, $pro_arr)) {
                                if (is_null($version)) {
                                    $group_course = $this->call_method('getGroupbyCourseID', 0, (string) $username);
                                    $c['course_group'] = 0;
                                    if($group_course) {
                                        $c['course_group'] = (int) $group_course[0];
                                        $c['activity_count'] = (int) $group_course[1];
                                    }
                                }
                                $c['last_access'] = (int) $time_las;
                                $c['coursetype'] = 'program';
                                $c['isFacilitator'] = false;
                                $c['remoteid'] = 0;
                                $c['cat_id'] = $pro->id;
                                $c['cat_name'] = '';
                                $c['cat_description']='';
                                $c['sortorder'] = 0;
                                $c['fullname'] = $pro->fullname;
                                $c['shortname'] = '';
                                $c['idnumber']='';
                                $c['summary'] = strip_tags(format_string($pro->summary,true));
                                $c['startdate'] = 0;
                                $c['modified'] = 0;
                                $c['created'] = 0;
                                $c['completion_status'] = 0;
                                $c['time_completed'] = $time_com;
                                $c['cost'] = 0;
                                $c['duration'] = (float)($curso->duration/3600);
                                $c['currency'] = '';
                                $c['enroled'] = 0;
                                $c['in_enrol_date'] = 0;
                                $c['fileid']='';
                                $c['filetype']='';
                                $c['filepath']=0;
                                $c['filename']=0;
                                $c['timepublish'] = (int) $pro->timecreated;
    
                                $tempUserArr = array();
                                if (!array_key_exists($pro->usermodified, $tempUserArr)) {
                                    $user_creator = $DB->get_record('user', array('id'=>$pro->usermodified), 'id, username, firstname, lastname');
                                    $tempUserArr[$c['creator']] = $user_creator->firstname.' '.$user_creator->lastname;
                                }
                                $c['creator'] = $tempUserArr[$c['creator']];
                                
                                $countlearner = 0;
                                $userRole = $this->get_user_role($curso->remoteid);
                                if ($userRole['status']) {
                                    $userroles = $userRole['roles'];
                                } else {
                                    $userroles = array();
                                }
                                if (!empty($userroles)) {
                                    foreach ($userroles as $userrole) {
                                        foreach (json_decode($userrole['role']) as $role) {
                                            if ($role->roleid == 5) {
                                                $countlearner++;
                                                $c['isLearner'] = true;
                                            }
                                        }
                                    }
                                }
                                $c['count_learner'] = $countlearner;

                                $query =
                                    "SELECT completiontime, completionevent
                                        FROM {$CFG->prefix}prog_assignment
                                        WHERE assignmenttypeid = $user->id and programid = $pro->id
                                        ";
                                $res = $DB->get_records_sql($query);
                                $max = 0;
                                $c['isFacilitator'] = $this->checkFacilitator($curso->remoteid, $username);
                                foreach ($res as $key => $value) {
                                    if ($value->completiontime != -1) {
                                        if ($value->completionevent) {
                                            $max = max($max, time() + ($value->completiontime));
                                        } else {
                                            $max = max($max, $value->completiontime);
                                        }
                                    }
                                }
    
                                $c['timeend'] = $max;
                                $program_context = context_program::instance($pro->id);
                                $files = $fs->get_area_files($program_context->id, 'totara_program', 'overviewfiles', 0);
                                if (count($files) > 0) {
                                    foreach ($files as $file) {
                                        $c['filename'] = $file->get_filename();
    
                                        $c['filepath'] =  file_encode_url("$CFG->wwwroot/pluginfile.php",
                                            '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                                            $file->get_filearea(). $file->get_filepath().$file->get_itemid().$file->get_filepath().$file->get_filename(), !$isimage);
                                    }
                                } else {
                                    $c['filepath'] = $CFG->wwwroot . '/theme/parenthexis/pix/';
                                    $c['filename'] = 'nocourseimg.jpg';
                                }
                                $cursos[] = $c;
                                $pro_arr[] = $pro->id;
                            }
                        }
                    }
    
                }
            }
        }
        return ($cursos);
    }
    
    function get_courses_bln ($blncategoryid, $scourse, $limitstart, $limit) {
        global $DB, $CFG;
        
        $categories = $DB->get_fieldset_select('course_categories', 'id', 'parent = :category OR id = :categoryid ORDER BY sortorder ASC', array('category' => $blncategoryid, 'categoryid' => $blncategoryid));
        
        if ($categories) {
            $text = utf8_decode($scourse);
            $str = preg_replace('/\s+/', '', $text);
            $text_search_have_space = '\'%' . $text . '%\'';
            $text_search_dont_have_space = '\'%' . $str . '%\'';
            $wheres2 = array();
            $wheres2[] = 'fullname LIKE ' . $text_search_have_space;
            $wheres2[] = 'summary LIKE ' . $text_search_have_space;
            $wheres2[] = 'learningoutcomes LIKE ' . $text_search_have_space;
            $wheres2[] = 'targetaudience LIKE ' . $text_search_have_space;
            if($text_search_dont_have_space != $text_search_have_space) {
                $wheres2[] = 'fullname LIKE ' . $text_search_dont_have_space;
                $wheres2[] = 'summary LIKE ' . $text_search_dont_have_space;
                $wheres2[] = 'learningoutcomes LIKE ' . $text_search_dont_have_space;
                $wheres2[] = 'targetaudience LIKE ' . $text_search_have_space;
            }
            $wheres = implode(' OR ', $wheres2);
            
            $cats = implode(',', $categories);
            $query =
                "SELECT
                id as remoteid,
                sortorder,
                fullname,
                shortname,
                idnumber,
                summary,
                startdate,
                creator,
                timecreated as created,
                timemodified as modified,
                timepublish
                FROM
                    {$CFG->prefix}course 
                WHERE
                visible = '1'
                AND category IN ($cats)
                AND ($wheres)
                ORDER BY
                fullname
                LIMIT $limitstart, $limit 
                ";
    
            $records = $DB->get_records_sql($query);
            
            $query2 =
                "SELECT
                COUNT(*) as count
                FROM
                    {$CFG->prefix}course 
                WHERE
                visible = '1'
                AND category IN ($cats)
                AND ($wheres)
                ORDER BY
                id
                ";

            $count = $DB->get_record_sql($query2);

            $totalcourse = $count->count;            
            
            $cursos = array ();
            $response = array();
            $fs = get_file_storage();
            foreach ($records as $curso) {
                $c = array();
                $c['remoteid'] = $curso->remoteid;
                $c['sortorder'] = $curso->sortorder;
                $c['shortname'] = $curso->shortname;
                $c['startdate'] = $curso->startdate;
                $c['created'] = $curso->created;
                $c['modified'] = $curso->modified;
                $c['coursetype'] = 'single_course';
                
                
                // Check if only guest courses are wanted

                $c['fullname'] = format_string($curso->fullname);
                $c['timepublish'] = (int) $curso->timepublish;

                $user_creator = $DB->get_record('user', array('id'=>$curso->creator), 'id, username, firstname, lastname');
                $c['creator'] = $user_creator->username;
                
                $managers = array();
                $facilitators = array();
                $learnerarr = array();
                $countlearner = 0;
                $userRole = $this->get_user_role($curso->remoteid);

                if ($userRole['status']) {
                    $userroles = $userRole['roles'];
                } else {
                    $userroles = array();
                }
                if (!empty($userroles)) {
                    foreach ($userroles as $userrole) {
                        foreach (json_decode($userrole['role']) as $role) {
                            // roleid = 5 (student)
                            // roleid = 4 (teacher)
                            // roleid = 1 (manager)
                            if ($role->roleid == 5) {
                                $countlearner++;
                                $user = get_complete_user_data ('username', $userrole['username']);
                                $learnerarr[] = $user->id;
                            }
                            if ($role->roleid == 1) {
                                $managers[] = $userrole['username'];
                            }
                            if ($role->roleid == 4) {
                                $facilitators[] = $userrole['username'];
                            }
                        }
                    }
                }
                $c['count_learner'] = $countlearner;
                
                if (count($learnerarr) > 1) {
                    $learnerid = implode(',', $learnerarr);
                    $wh = ' AND userid IN ('.$learnerid.')';
                } else if (count($learnerarr) == 1) {
                    $wh = ' AND userid = '.$learnerarr[0];
                } else {
                    $wh = '';
                }
                $query_com = "SELECT userid FROM {$CFG->prefix}course_completions WHERE course = $curso->remoteid AND status = 50 $wh";
                $result_com = $DB->get_records_sql($query_com);
                
                $c['count_completed'] = count($result_com);
                
                $c['course_manager'] = implode(', ', $managers);
                $c['course_facilitator'] = implode(', ', $facilitators);

                $context = context_course::instance($curso->remoteid);
                $c['summary'] = strip_tags(format_string($curso->summary,true));

                $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', 0);
                if (count($files) > 0) {
                    foreach ($files as $file) {
//                        $c['fileid'] = $context->id;
//                        $c['filetype'] = $file->get_mimetype();
                        $filepath = $CFG->wwwroot . '/pluginfile.php/' . $context->id . '/course/overviewfiles/';
                        $filename = $file->get_filename();
                    }
                } else {
                    $filepath = $CFG->wwwroot . '/theme/parenthexis/pix/';
                    $filename = 'nocourseimg.jpg';
                }
                $c['course_img'] = $filepath.$filename;

                $cursos[] = $c;

            }
            $response['status'] = true;
            $response['message'] = 'Success.';
            $response['totalcourses'] = $totalcourse;
            $response['data'] = $cursos;            
            return $response;
        } else {
            $response['status'] = false;
            $response['message'] = 'Not found category';
            $response['totalcourses'] = 0;
            $response['data'] = array();
            return ($response);
        }
    }
    
    function get_course_report ($courseid) {
        global $DB, $CFG;

            $query =
                "SELECT
                id as remoteid,
                sortorder,
                fullname,
                shortname,
                idnumber,
                summary,
                startdate,
                creator,
                timecreated as created,
                timemodified as modified,
                timepublish
                FROM
                    {$CFG->prefix}course 
                WHERE
                visible = '1'
                AND id = $courseid
                ORDER BY
                fullname
                ";
    
            $records = $DB->get_records_sql($query);
            
            $cursos = array ();
            $response = array();
            $fs = get_file_storage();
        if ($records) {
            foreach ($records as $curso) {
                $curso->self_enrolment = 0;
                $curso->guest = 0;
                
                // Khong thuoc prog nao
                if (array_key_exists($curso->remoteid, $cursos)) continue;
                $c = array();
                $c['remoteid'] = $curso->remoteid;
                $c['sortorder'] = $curso->sortorder;
                $c['shortname'] = $curso->shortname;
                $c['startdate'] = $curso->startdate;
                $c['created'] = $curso->created;
                $c['modified'] = $curso->modified;
                $c['coursetype'] = 'single_course';
                
                
                // Check if only guest courses are wanted
                if ($guest) continue;

                $c['fullname'] = format_string($curso->fullname);
                $c['timepublish'] = (int) $curso->timepublish;

                $user_creator = $DB->get_record('user', array('id'=>$curso->creator), 'id, username, firstname, lastname');
                $c['creator'] = $user_creator->username;
                
                $query_feed = "SELECT sid FROM {$CFG->prefix}questionnaire WHERE course = $curso->remoteid ORDER BY id DESC";
                $result_feed = $DB->get_record_sql($query_feed);                                
                $surid = $result_feed->sid; 
                
                $managers = array();
                $facilitators = array();
                $learneridarr = array();
                $members_info = array();
                $feedbackinfo = array();
                $total_dedication = 0;
                $countlearner = 0;
                $userRole = $this->get_user_role($curso->remoteid);

                if ($userRole['status']) {
                    $userroles = $userRole['roles'];
                } else {
                    $userroles = array();
                }
                if (!empty($userroles)) {
                    foreach ($userroles as $userrole) {
                        foreach (json_decode($userrole['role']) as $role) {
                            // roleid = 5 (student)
                            // roleid = 4 (teacher)
                            // roleid = 1 (manager)
                            if ($role->roleid == 5) {
                                $countlearner++;
                                $user = get_complete_user_data ('username', $userrole['username']);
                                $learneridarr[] = $user->id;
                                
                                // get member info 
                                $m = array();
                                $query_com = "SELECT timecompleted FROM {$CFG->prefix}course_completions WHERE userid = $user->id AND course = $curso->remoteid";
                                $resultcom = $DB->get_records_sql($query_com);

                                $m['username'] = $userrole['username'];
                                $m['time_completed'] = 0;
                                if($resultcom) {
                                    foreach ($resultcom as $res) {
                                        if ($m['time_completed'] < $res->timecompleted) $m['time_completed'] = (int) $res->timecompleted;
                                    }
                                }
                                $query = "SELECT ue.lastaccess, ue.timestart FROM {$CFG->prefix}enrol e LEFT JOIN {$CFG->prefix}user_enrolments ue ON e.id = ue.enrolid WHERE e.courseid = $curso->remoteid AND ue.userid = $user->id AND e.status = 0 $swhere";
                                $result = $DB->get_records_sql($query);
                                if ($result) {
                                    $lastAccessArr = array();
                                    $timeStartArr = array();
                                    foreach ($result as $res) {
                                        $lastAccessArr[] = $res->lastaccess ? $res->lastaccess : 0;
                                        $timeStartArr[] = $res->timestart;
                                    }
                                    $m['last_access'] = max($lastAccessArr);
                                    $m['enrol_time'] = min($timeStartArr);
                                } else {
                                    if ($swhere != ' ') continue;
                                    $m['last_access'] = 0;
                                    $m['enrol_time'] = 0;
                                }
                                
                                $params = array(
                                    'courseid' => $curso->remoteid,
                                    'userid' => $user->id,
                                    'mintime' =>  $curso->startdate,
                                    'maxtime' => time()
                                );
                                $limit = 60 * 60;
                                $where = 'course = :courseid AND userid = :userid AND time >= :mintime AND time <= :maxtime';
                                $logs = $DB->get_records_select('log', $where, $params, 'time ASC', 'id,time');                              
                                if ($logs) {
                                    $previouslog = array_shift($logs);
                                    $previouslogtime = $previouslog->time;
                                    $sessionstart = $previouslog->time;
                                    $dedication = 0;
                                    $daysconnected[date('Y-m-d', $previouslog->time)] = 1;

                                    foreach ($logs as $log) {
                                        if (($log->time - $previouslogtime) > $limit) {
                                            $dedication += $previouslogtime - $sessionstart;
                                            $sessionstart = $log->time;
                                        }
                                        $previouslogtime = $log->time;
                                        $daysconnected[date('Y-m-d', $log->time)] = 1;
                                    }
                                    $dedication += $previouslogtime - $sessionstart;
                                } else {
                                    $dedication = 0;
                                } 
                                $total_dedication += $dedication;
                                
                                $m['time_spent'] = $dedication;
                                
                                // get grade item object: updated 18 Jul 2016 by KV team
                                $query3 =
                                    "SELECT id
                                    from  {$CFG->prefix}grade_items
                                    where courseid = '$curso->remoteid'
                                    AND itemtype = 'course'
                                        ";
                                $cat_item = $DB->get_record_sql($query3);
                                $grade_item = grade_item::fetch(array('id'=>$cat_item->id, 'courseid'=>$curso->remoteid));
                                $gradedisplaytype = 3;

                                $query4 = "SELECT g.finalgrade, g.rawgrademax, g.feedback, g.id, g.hidden, g.userid, g.usermodified
                                  FROM {$CFG->prefix}grade_grades g
                                 WHERE g.itemid = ?
                                   AND g.userid =  ?";
                                $params2 = array ($cat_item->id, $user->id);

                                $grade = $DB->get_record_sql($query4, $params2);

                                // convert grade final number to letters: updated 18 Jul 2016 by KV team
                                $grade_letter = grade_format_gradevalue($grade->finalgrade, $grade_item, true, $gradedisplaytype, null);
                                $m['overall_grade'] = $grade_letter;
                                
                                $members_info[] = $m;
                                
                                // ------ Get feedback --------
                                $feedback = array();
                                $query_response = "SELECT id FROM {$CFG->prefix}questionnaire_response WHERE survey_id = $surid AND username = $user->id";
                                $result_response = $DB->get_record_sql($query_response);   
                                
                                if ($result_response) {
                                    $query_feedback = "SELECT a.name, b.rank
                                                        FROM mdl_questionnaire_question AS a 
                                                        LEFT JOIN mdl_questionnaire_response_rank AS b ON a.id = b.question_id 
                                                        WHERE b.response_id = $result_response->id AND a.deleted = 'n'
                                                        ORDER BY a.position";
                                    $result_feedback = $DB->get_records_sql($query_feedback);    

                                    $query_text = "SELECT a.name, b.response
                                                        FROM mdl_questionnaire_question AS a 
                                                        LEFT JOIN mdl_questionnaire_response_text AS b ON a.id = b.question_id 
                                                        WHERE b.response_id = $result_response->id AND a.deleted = 'n' 
                                                        ORDER BY a.position";                                
                                    $result_text = $DB->get_records_sql($query_text);
                                    $feedbacks = array();
                                    if ($result_feedback) {
                                        foreach ($result_feedback as $re) {
                                            $feedbacks[] = array('name' => $re->name, 'feedback' => $re->rank);
                                        }
                                    }
                                    if ($result_text) {
                                        foreach ($result_text as $re) {
                                            $feedbacks[] = array('name' => $re->name, 'feedback' => $re->response);
                                        }
                                    }
                                    $feedback['username'] = $userrole['username'];
                                    $feedback['rank_arr'] = $feedbacks;
                                    $feedbackinfo[] = $feedback;
                                }
                            }
                            if ($role->roleid == 1) {
                                $managers[] = $userrole['username'];
                            }
                            if ($role->roleid == 4) {
                                $facilitators[] = $userrole['username'];
                            }                        
                        }
                        
                    }
                }
                $c['average_time_spent'] = ($total_dedication / $countlearner) ? ($total_dedication / $countlearner) : 0;
                $c['count_learner'] = $countlearner;
                
                if (count($learneridarr) > 1) {
                    $learnerid = implode(',', $learneridarr);
                    $wh = ' AND userid IN ('.$learnerid.')';
                } else if (count($learneridarr) == 1) {
                    $wh = ' AND userid = '.$learneridarr[0];
                } else {
                    $wh = '';
                }
                $query_com = "SELECT userid FROM {$CFG->prefix}course_completions WHERE course = $curso->remoteid AND status = 50 $wh";
                $result_com = $DB->get_records_sql($query_com);
                
                $c['count_completed'] = count($result_com);
                
                $c['course_manager'] = implode(', ', $managers);
                $c['course_facilitator'] = implode(', ', $facilitators);

                $context = context_course::instance($curso->remoteid);
                $c['summary'] = strip_tags(format_string($curso->summary,true));

                $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', 0);
                if (count($files) > 0) {
                    foreach ($files as $file) {
//                        $c['fileid'] = $context->id;
//                        $c['filetype'] = $file->get_mimetype();
                        $filepath = $CFG->wwwroot . '/pluginfile.php/' . $context->id . '/course/overviewfiles/';
                        $filename = $file->get_filename();
                    }
                } else {
                    $filepath = $CFG->wwwroot . '/theme/parenthexis/pix/';
                    $filename = 'nocourseimg.jpg';
                }
                $c['course_img'] = $filepath.$filename;

//                $cursos[] = $c;
            } 
            
            $datas = array();
            $datas['courseinfo'] =  $c;
            $datas['membersinfo'] =  $members_info;
            $datas['feedbackinfo'] =  $feedbackinfo;
            
            $response['status'] = true;
            $response['message'] = 'Success.';
            $response['data'] = $datas;            
            return $response;
        } else {
            $response['status'] = false;
            $response['message'] = 'Not found category';
            $response['data'] = array();
            return ($response);
        }
    }
            
    function get_user_report ($username) {
        global $DB, $CFG;

        $user = get_complete_user_data ('username', $username);
        $courses = enrol_get_users_courses ($user->id, true, 'creator,timepublish');
        
        $data = array ();
        $courses_created = array();
        $courses_enrolled = array();
        $courses_completed = array();
        foreach ($courses as $course) {
            if ($course->creator == $user->id) {
                $cc = array();
                $cc['remoteid'] = $course->id;
                $cc['fullname'] = $course->fullname;
                $cc['shortname'] = $course->shortname;
                $user_creator = $DB->get_record('user', array('id'=>$course->creator), 'username');
                $cc['creator'] = $user_creator->username;
                $cc['timepublish'] = $course->timepublish;
                
                $courses_created[] = $cc;
            } else {
                $courses_enrolled[] = $course;
            }
        }
        
        if (count($courses_enrolled) > 0) {
            foreach ($courses_enrolled as $curso) {
                $ce = array();
                
                $query_com = "SELECT timecompleted FROM {$CFG->prefix}course_completions WHERE userid = $user->id AND status = 50 AND course = $curso->id";
                $resultcom = $DB->get_records_sql($query_com);
                if ($resultcom) {
                    $query3 =
                        "SELECT id
                        from  {$CFG->prefix}grade_items
                        where courseid = '$curso->id'
                        AND itemtype = 'course'
                            ";
                    $cat_item = $DB->get_record_sql($query3);
                    $grade_item = grade_item::fetch(array('id'=>$cat_item->id, 'courseid'=>$curso->id));
                    $gradedisplaytype = 3;

                    $query4 = "SELECT g.finalgrade, g.rawgrademax, g.feedback, g.id, g.hidden, g.userid, g.usermodified
                      FROM {$CFG->prefix}grade_grades g
                     WHERE g.itemid = ?
                       AND g.userid =  ?";
                    $params2 = array ($cat_item->id, $user->id);

                    $grade = $DB->get_record_sql($query4, $params2);

                    // convert grade final number to letters: updated 18 Jul 2016 by KV team
                    $grade_letter = grade_format_gradevalue($grade->finalgrade, $grade_item, true, $gradedisplaytype, null);
                    $curso->overallgrade = $grade_letter;
                                
                    $ce['remoteid'] = $curso->id;
                    $ce['fullname'] = $curso->fullname;
                    $ce['shortname'] = $curso->shortname;
                    $user_creator = $DB->get_record('user', array('id'=>$curso->creator), 'username');
                    $ce['creator'] = $user_creator->username;
                    $ce['timepublish'] = $curso->timepublish;
                    $ce['overall_grade'] = $grade_letter;
                    
                    $courses_completed[] = $ce;
                }
            }
        }
        $data['courses_created'] = $courses_created;
        $data['courses_completed'] = $courses_completed;
        $data['no_course_enrolled'] = count($courses_enrolled);

        $response['status'] = true;
        $response['message'] = 'Success.';
        $response['data'] = $data;            
        return $response;
    }
            
    function getCoursesByIds ($ids) {
        global $DB, $CFG;
        
        $query =
                "SELECT
                id as remoteid,
                fullname,
                shortname,
                idnumber,
                summary,
                creator,
                timepublish
                FROM
                    {$CFG->prefix}course 
                WHERE
                visible = '1'
                AND id IN ($ids)
                ORDER BY
                fullname
                ";
    
        $records = $DB->get_records_sql($query);
        $data = array();
        if ($records) {
            foreach ($records as $curso) {
                $course = array();
                $course['remoteid'] = $curso->remoteid;
                $course['fullname'] = $curso->fullname;
                
                $data[] = $course;
            }
            
            $response['status'] = true;
            $response['message'] = 'Success.';
            $response['data'] = $data;            
            return $response;
        } else {
            $response['status'] = false;
            $response['message'] = 'Not found courses.';
            $response['data'] = array();
            return ($response);
        }
    }

    function checkFacilitator ($courseid, $username, $app = '') {
        // Check facilitator
        $isFacilitator = false;
        $userroles = $this->get_user_role($courseid, $username);
        if ($userroles['status']) {
            $roles = json_decode($userroles['roles'][0]['role']);
            if (count($roles) > 0) {
                foreach ($roles as $role) {
                    // change for app: given two roles for app
                    if($app) {
                        if(($role->roleid == 4 || $role->sortname == 'teacher') || ($role->roleid == 1 || $role->sortname == 'manager')) {
                            $isFacilitator = true;
                        }
                    }
                    else if ($role->roleid == 4 || $role->sortname == 'teacher') {
                        $isFacilitator = true;
                    }
                }
            }
        }
        
        return $isFacilitator;
    }
    
    function learning_provider_courses($username, $categoryids) {
        global $DB, $CFG;
        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $user = get_complete_user_data ('username', $username);
        $catid_arr = explode(',', $categoryids);

        // part 1: get course of category
        $results = array();
        if (!empty($catid_arr)) {
            foreach ($catid_arr as $categoryid) {
                $courses = $this->courses_by_category($categoryid, 0, $username);
                
                $fs = get_file_storage();
                if ($courses) {
                    foreach ($courses as $cour) {
                        $data = array();
                        $data['id'] = $cour['remoteid'];
                        $data['category'] = $categoryid;
                        $data['fullname'] = $cour['fullname'];
                        $data['summary'] = strip_tags($cour['summary']);
                        $data['is_mycourse'] = false;
                        $data['startdate'] = $cour['startdate'];
                        $data['created'] = $cour['timecreated'];
                        $data['modified'] = $cour['timemodified'];
                        $data['timepublish'] = $cour['timepublish'];
                        if($cour['creator'] == $user->id) {
                            $data['is_mycourse'] = true;
                        }
                        $tempUserArr = array();
                        if (!array_key_exists($cour['creator'], $tempUserArr)) {
                            $user_creator = $DB->get_record('user', array('id'=>$cour['creator']), 'id, username, firstname, lastname');
                            $tempUserArr[$cour['creator']] = $user_creator->firstname.' '.$user_creator->lastname;
                        }
                        $data['creator'] = $tempUserArr[$cour['creator']];
                        $data['duration'] = (float)($cour['duration']/3600);
                        $data['course_survey'] = $cour['coursesurvey'];
                        $data['course_facilitated'] = $cour['facilitatedcourse'];
                        // course request
                        $data['pending_approve'] = false;
                        $data['approved'] = false;
                        $data['unapproved'] = false;
                        $data['published'] = false;
                        $data['unpublished'] = false;
                        $course_request = $DB->get_records('course_request_approve', array('courseid' => $cour['remoteid']), 'id');
                        if ($course_request && count($course_request) > 0) {
                            $cr = end($course_request);
                            if($cr->status == 0) {
                                $data['pending_approve'] = true;
                            } else if($cr->status == 1) {
                                $data['approved'] = true;
                            } else if ($cr->status == -1) {
                                $data['unapproved'] = true;
                            } else if ($cr->status == 2) {
                                $data['published'] = true;
                            } else if($cr->status == -2) {
                                $data['unpublished'] = true;
                            }
                            if (($count = count($course_request)) > 1) {
                                $i = 1;
                                foreach ($course_request as $courser) {
                                    if ($i < $count) 
                                        $DB->delete_records('course_request_approve', array('id' => $courser->id));
                                    $i++;
                        }
                            }
                        }
                        
                        $query = "SELECT MAX(enrolperiod) AS enrolperiod
                        FROM {$CFG->prefix}enrol
                        WHERE status = 0 AND courseid = ".$cour['remoteid'];
                        $result = $DB->get_record_sql($query);
                        $data['enrolperiod'] = $result->enrolperiod;
            
                        $context = context_course::instance($cour['remoteid']);
                        $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', 0);
                        if (count($files) > 0) {
                            foreach ($files as $file) {
                                $data['filepath'] = $CFG->wwwroot . '/pluginfile.php/' . $context->id . '/course/overviewfiles/';
                                $data['filename'] = $file->get_filename();
                            }
                        } else {
                            $data['filepath'] = $CFG->wwwroot . '/theme/parenthexis/pix/';
                            $data['filename'] = 'nocourseimg.jpg';
                        }
                         /* Get course cost if any */
                        $instances = enrol_get_instances($cour['remoteid'], true);

                        $params = array ($cour['remoteid']);
                        $query =
                            "SELECT count(*)
                            FROM
                            {$CFG->prefix}course_sections
                            WHERE
                            course = ? and section != 0 and visible=1
                            ";

                        $course_info['numsections'] = $DB->count_records_sql($query, $params);

                        $creator = get_complete_user_data ('id', $cour['creator']);
                        $course_info['creator'] = $creator->username;

                        $course_info['self_enrolment'] = 0;
                        $course_info['guest'] = 0;
                        $in = true;
                        $now = time ();
                        foreach ($instances as $instance)
                        {
                            if (($instance->enrol == 'paypal') || ($instance->enrol == 'joomdle') || ($instance->enrol == 'manual') || ($instance->enrol == 'totara_program'))
                            {
                                $enrol = $instance->enrol;
                                $query = "SELECT cost, currency
                                            FROM {$CFG->prefix}enrol
                                            where courseid = ? and enrol = ?";
                                $params = array ($cour['remoteid'], $enrol);
                                $record =  $DB->get_record_sql($query, $params);
                                $course_info['cost'] = (float) $record->cost;
                                $course_info['currency'] = $record->currency;
                            }

                            /* Get enrolment dates. We get the last one, as good/bad as any other XXX */
                            if ($instance->enrolstartdate)
                                $course_info['enrolstartdate'] = $instance->enrolstartdate;
                            if ($instance->enrolenddate)
                                $course_info['enrolenddate'] = $instance->enrolenddate;

                            if ($instance->enrolperiod)
                                $course_info['enrolperiod'] = $instance->enrolperiod;

                            // Self-enrolment
                            if ($instance->enrol == 'self')
                                $course_info['self_enrolment'] = 1;

                            // Guest access
                            if ($instance->enrol == 'guest')
                                $course_info['guest'] = 1;

                            if (($instance->enrolstartdate) && ($instance->enrolenddate))
                            {
                                $in = false;
                                if (($instance->enrolstartdate <= $now) && ($instance->enrolenddate >= $now))
                                    $in = true;
                            }
                            else if ($instance->enrolstartdate)
                            {
                                $in = false;
                                if (($instance->enrolstartdate <= $now))
                                    $in = true;
                            }
                            else if ($instance->enrolenddate)
                            {
                                $in = false;
                                if ($instance->enrolenddate >= $now)
                                    $in = true;
                            }
                        }
                        $data['timeend'] = date('d/m/Y' , ($cour['startdate']+$course_info['enrolperiod']) );
                        /*
                        $data['cost'] = $course_info['cost'];
                        $data['currency'] = $course_info['currency'];
                        */
                        $data['cost'] = $cour['price'];
                        $data['price'] = $cour['price'];
                        $data['currency'] = $cour['currency'];

                        $data['course_status'] = '';
                        $course_status = $this->get_course_status($cour['remoteid']);  
                        if ($course_status) {
                            $data['course_status'] = $course_status;
                        }
                        
                        // Check facilitator
                        $data['isFacilitator'] = $this->checkFacilitator($cour['remoteid'], $username);
                        
                        $userRole = $this->get_user_role($cour['remoteid']);
                        if ($userRole['status']) {
                            $userroles = $userRole['roles'];
                        } else {
                            $userroles = array();
                        }
                        $data['assignedUsers'] = $userroles;
                        $countlearner = 0;
                        if (!empty($userroles)) {
                            foreach ($userroles as $userrole) {
                                foreach (json_decode($userrole['role']) as $role) {
                                    if ($role->roleid == 5) {
                                        $countlearner++;
                                    }
                                }
                            }
                        }
                        $data['count_learner'] = $countlearner;
                        
                        $results[] = $data;
                     }
                }
            }
        }
        // list course is Content creator
        if ($categoryids == '' || !$categoryids) {
            $courses_cc = $this->my_courses($username);
            if ($courses_cc['status']) {
                foreach ($courses_cc['courses'] as $course_cc) {
                    $course_request2 = $DB->get_records('course_request_approve', array('courseid' => $course_cc['id']), 'id');
                    if($course_request2 && count($course_request2) > 0) {
                        $cr2 = end($course_request2);
                        if($cr2->status == 0) {
                            $course_cc['pending_approve'] = true;
                            $course_cc['created'] = $course_cc['timecreated'];
                            $course_cc['modified'] = $course_cc['timemodified'];
                            $course_cc['is_mycourse'] = false;
                            $course_cc['approved'] = false;
                            $course_cc['unapproved'] = false;
                            $course_cc['published'] = false;
                            $course_cc['unpublished'] = false;
                            $userRole = $this->get_user_role($course_cc['id']);
                            if ($userRole['status']) {
                                $userroles = $userRole['roles'];
                            } else {
                                $userroles = array();
                            }
                            $course_cc['assignedUsers'] = $userroles;
                            $results[] = $course_cc;
                        } 
                    }
                }
            }
        }
        $response['status'] = true;
        $response['message'] = 'Success.';
        $response['courses'] = $results;
        return $response;
    }
    
    function send_course_for_approve($courseid, $username, $categoryid) {
        global $DB;
        
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        
        $user = get_complete_user_data ('username', $username);
        
        // check permission and get user manage of category
        $catcontext = context_coursecat::instance($categoryid, IGNORE_MISSING);
        // get manage role of category
        $rolemanages = $DB->get_records('role_assignments', array('contextid' => $catcontext->id));
                
        $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
        if($rolemanages) {
            foreach ($rolemanages as $rol) {
                $role_arr[] = $rol->userid;
            }
       }
       $role_str = implode(',', $role_arr);
       
        $hasPermission = $this->get_user_role($courseid, $username);
        if(!$hasPermission['roles'][0]['hasPermission']) {
            return $response = array(
                    'status' => false,
                    'message' => 'You don\'t have a permission this course.',
            );
        }
        if(!empty($course)) {
            // check data already send request
            $request_send = $DB->get_record('course_request_approve', array('courseid'=>$course->id));
            
            $data = new stdClass();
            if(!empty($request_send)) {
                $data->id = $request_send->id;
                $data->timesendrequest = time();
                $data->requestor = $user->id;
                $data->status = 0;
                $DB->update_record('course_request_approve', $data);
            } else {
                $data->courseid = $course->id;
                $data->status = 0;
                $data->requestor = $user->id;
                $data->requestto = $role_str;
                $data->timesendrequest = time();
                $DB->insert_record('course_request_approve', $data);
            }
            
            // send notification for LP: not yet send notification
            $userlp = $this->get_lp_of_category($categoryid);
            $this->call_method('sendForApprovalNotif', $course->id, $course->shortname, $username, $userlp->username);
            
            $response = array(
                'status' => true,
                'message' => 'Request send.',
            );
            return $response;
        } else {
            $response = array(
                'status' => false,
                'message' => 'Course is not found.',
            );
            return $response;
        }
    }
    
    // Approve or Unapprove course
    function approve_or_unapprove_course($username, $courseid, $categoryid, $action) {
        global $DB;
        
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        
        $user = get_complete_user_data ('username', $username);
        
        $catcontext = context_coursecat::instance($categoryid, IGNORE_MISSING);
        
        $rolemanages = $DB->get_records('role_assignments', array('contextid' => $catcontext->id));
       
       if($rolemanages) {
            foreach ($rolemanages as $rol) {
                $role_arr[] = $rol->userid;
            }
       }
       if(!in_array($user->id, $role_arr)) {
           return $response = array(
                'status' => false,
                'message' => 'You don\'t have a permission.',
           );
       }
       $action_arr = array('approve', 'unapprove');
       if(!in_array($action, $action_arr)) {
           return $response = array(
                'status' => false,
                'message' => 'Can not find a action, system only accepted action: approve or unapprove',
           );
       }
       $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
       if(empty($course)) {
           return $response = array(
               'status' => false,
               'message' => 'No course found.',
           );
       }
       $request_send = $DB->get_record('course_request_approve', array('courseid'=>$course->id));
       if(empty($request_send)) {
           return $response = array(
               'status' => false,
               'message' => 'No course pending approved.',
           );
       }
       if($action == 'approve') {
           $status = 1;
//           $visible = 1;
       } elseif($action == 'unapprove') {
           $status = -1;
//           $visible = 0;
       }
       $data = new stdClass();
       $data->id = $request_send->id;
       $data->status = $status;
       $data->approved_by = $user->id;
       $data->timeapproved = time();
       $DB->update_record('course_request_approve', $data);
       
        // unenrol content creator
        if($action == 'approve') {
            $context = context_course::instance($courseid, MUST_EXIST);
            $courseusers = $this->get_user_role($courseid);
            if ($courseusers['status']) {
                if (!empty($courseusers['roles'])) {
                    foreach ($courseusers['roles'] as $userenroled) {
                        $u = get_complete_user_data ('username', $userenroled['username']);
                        $userroles = json_decode($userenroled['role']);
                        if (!empty($userroles)) {
                            if (count($userroles) == 1 && $userroles[0]->roleid == 3) {
                                $this->unenrol_user($userenroled['username'], $courseid);
                            } else {
                                foreach ($userroles as $role) {
                                    if ($role->roleid == 3) {
                                        role_unassign($role->roleid, $u->id, $context->id);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
       
       cache_helper::purge_by_event('changesincourse');
       add_to_log($course->id, "course", ($visible ? 'show' : 'hide'), "edit.php?id=$course->id", $course->id);

       $usersend = get_complete_user_data('id', $request_send->requestor); 
       $this->call_method('courseApprovalNotif', $course->id, $course->shortname, $username, $usersend->username, $action);
        
       return $response = array(
            'status' => true,
            'message' => 'Course '.$action.'d.',
       );
    }
    
    function publish_unpublish_course($categoryid, $courseid, $action, $username, $hikashopcatids = '') {
        global $DB, $CFG;
        
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);
        
        $course = $DB->get_record('course', array('id' => $courseid, 'category' => $categoryid), '*', MUST_EXIST);
        if(empty($course)) {
            return $response = array(
                'status' => false,
                'message' => 'No coures found.',
            );
        }
        $catcontext = context_coursecat::instance($categoryid, IGNORE_MISSING);
        $rolemanages = $DB->get_records('role_assignments', array('contextid' => $catcontext->id));
        
       if($rolemanages) {
            foreach ($rolemanages as $rol) {
                $role_arr[] = $rol->userid;
            }
       }
       if($user->id != $course->creator && !in_array($user->id, $role_arr)) {
           return $response = array(
               'status' => false,
               'message' => 'You don\'t have a permission to publish and unpublish course.',
           );
       }
       $action_arr = array('publish', 'unpublish');
       if(!in_array($action, $action_arr)) {
           return $response = array(
               'status' => false,
               'message' => 'System can\'t found action.',
           );
       }
       if($action == 'publish') {
           $visible = 1;
           $text = 'published';
           $status = 2;
       } elseif($action == 'unpublish') {
           $visible = 0;
           $text = 'unpublished';
           $status = -2;
       }
       $this->call_method('publishCourse', $course->id, $categoryid, $hikashopcatids, $action);
       
       // Update course_request_approve
       $courserequest = $DB->get_record('course_request_approve', array('courseid' => $course->id), '*', MUST_EXIST);
       $data = new stdClass();
       $data->id = $courserequest->id;
       $data->status = $status;
       $DB->update_record('course_request_approve', $data);
       
       // Update show and hide course
       $params = array('id' => $course->id, 'visible' => $visible, 'visibleold' => $visible, 'timemodified' => time());

       if ($course->timepublish == 0) $params['timepublish'] = time();
       $DB->update_record('course', $params);
       
       return $response = array(
            'status' => true,
            'message' => 'Course '. $text,
       );
    }
    
    function enable_self_enrolment ($courseid, $action, $username, $circleid = '') {
        global $DB, $CFG;
        
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);
        
        $course = $DB->get_record('course', array('id' => $courseid), 'id, category, creator, timemodified, timepublish', MUST_EXIST);

        if(empty($course)) {
            return $response = array(
                'status' => false,
                'message' => 'No coures found.',
            );
        }
        $catcontext = context_coursecat::instance($course->category, IGNORE_MISSING);
        $rolemanages = $DB->get_records('role_assignments', array('contextid' => $catcontext->id));
        
        if($rolemanages) {
            foreach ($rolemanages as $rol) {
                $role_arr[] = $rol->userid;
            }
        }
        if($user->id != $course->creator && !in_array($user->id, $role_arr)) {
           return $response = array(
               'status' => false,
               'message' => 'You don\'t have a permission to publish and unpublish course.',
           );
        }

        if($action == 'enable') {
           $visible = 1;
           $text = 'enabled-selfenrolment';
           // Update show and hide course
           $params = array('id' => $course->id, 'timemodified' => time());
           if ($course->timepublish == 0) $params['timepublish'] = time();
           $DB->update_record('course', $params);
        } else if ($action == 'unenable') {
           $visible = 0;
           $text = 'unenabled-selfenrolment';
        }
        //show and hide course
        $params = array('id' => $course->id, 'visible' => $visible, 'visibleold' => $visible, 'timemodified' => time());
        $DB->update_record('course', $params);
       
        // enable self-enrolment
        if (!$DB->record_exists('enrol', array('courseid'=>$course->id, 'enrol'=>'self', 'roleid'=>5))) {
            $e = new stdClass();
            $e->enrol = "self";
            $e->status = 1;
            $e->roleid = 5; //learner's default role
            $e->courseid = $course->id;
            $e->timecreated = time();
            $e->timemodified = time();
            $DB->insert_record('enrol', $e);
        }
        $instance = $DB->get_record('enrol', array('courseid'=>$course->id, 'enrol'=>'self', 'roleid'=>5), '*');
        $plugins  = enrol_get_plugins(false);
        $plugin = $plugins[$instance->enrol];

        if($action == 'enable') {
        if ($instance->status != ENROL_INSTANCE_ENABLED) {
            $plugin->update_status($instance, ENROL_INSTANCE_ENABLED);
        }
        } else if ($action == 'unenable') {
            if ($instance->status != ENROL_INSTANCE_DISABLED) {
                $plugin->update_status($instance, ENROL_INSTANCE_DISABLED);
            }
        }
       
        if ($circleid && $circleid != '') {
            $this->call_method('shareCourseToCircle', $action, $course->id, $circleid);
        }
       
        return $response = array(
            'status' => true,
            'message' => 'Course '. $text,
        );
    }
    function change_course_into_lpcourse($courseid, $categoryid, $username) {
        global $DB;

        $user = get_complete_user_data('username', $username);
        $coursecontext = context_course::instance($courseid);

        if (!has_capability('moodle/course:update', $coursecontext, $user->id)) {
            return array('status'=>0, 'message'=>'nopermission', 'data'=>0);
        }
        $categoryid = end(json_decode($categoryid, true)); //re-check after

        // check if name course exist in category
        $course_info = $this->get_course_info($courseid);
        $course_info['shortname']  = utf8_decode ($course_info['shortname']);
        if ($DB->record_exists('course', array('shortname' => $course_info['shortname'],'category' => $categoryid))) {
            return array('status'=>0, 'message'=>'There is an existing course with the same name. Please rename your course.', 'data'=>0);
        }
        if ($DB->record_exists("course_categories", array("id"=>$categoryid))) {
            $category = $DB->get_record("course_categories", array("id"=>$categoryid), 'id, parent, userowner');

            if ($category->parent == 0 && $category->userowner == $user->id) { //Is LP Category and
                $course = $DB->get_record('course', array('id'=>$courseid), 'id, category');

                if (!$destcategory = $DB->get_record('course_categories', array('id' => $categoryid))) {
                    return array('status'=>0, 'message'=>'cannotfindcategory', 'data'=>0);
                }

                $id = $course->category;
                $courses = array();
                array_push($courses, $courseid);
                // Check this course's category.
                if ($movingcourse = $DB->get_record('course', array('id' => $courseid))) {
                    if ($id && $movingcourse->category != $id ) {
                        return array('status'=>0, 'message'=>'coursedoesnotbelongtocategory', 'data'=>0);
                    }
                } else {
                    return array('status'=>0, 'message'=>'cannotfindcourse', 'data'=>0);
                }
                move_courses($courses, $categoryid);
            } else return array('status'=>0, 'message'=>'commonCategoryOrNotOwner', 'data'=>0);
        } else return array('status'=>0, 'message'=>'categoryNotExist', 'data'=>0);
        return array(
            'status' => 1,
            'message' => 'ok',
            'data' => $categoryid
        );
    }
    function remove_course_platform($courseid, $username, $categoryid) {
        global $DB;
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);
        
        $catcontext = context_coursecat::instance($categoryid, IGNORE_MISSING);
        
        $roleassignments = get_user_roles_with_special($catcontext, $user->id);
        $rolenames = role_get_names($catcontext);
        $systemcontext = context_system::instance();
        $manage = false;
        foreach ($roleassignments as $ra) {
            $racontext = context::instance_by_id($ra->contextid);
            $rolename = $rolenames[$ra->roleid]->localname;
            if (has_capability('moodle/role:manage', $systemcontext)) {
                $manage = true;
                $roleusers = get_role_users($ra->roleid, $racontext, false, 'u.id, u.firstname, u.lastname');
                break;
            }
       }
       if($roleusers) {
            foreach ($roleusers as $rol) {
                $role_arr[] = $rol->id;
            }
       }
       $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
       if($user->id != $course->creator && !in_array($user->id, $role_arr)) {
           return $response = array(
               'status' => false,
               'message' => 'You don\'t have a permission to delete this course.',
           );
       }
       delete_course($course, false, $username);
       fix_course_sortorder();
       
       return $response = array(
            'status' => true,
            'message' => 'Course deleted.',
       );
    }
            
    function list_progs($prog_id = 0) {
        global $DB, $CFG;
        $where = '';
        if ($prog_id != 0 ) {
            $where = ' AND id = '.$prog_id;
        }
        $query = "SELECT id, fullname, summary
            FROM {$CFG->prefix}prog
            WHERE visible = 1 $where";
        $records = $DB->get_records_sql($query);
        foreach ($records as $k => $v) {
            $query2 = "SELECT courseid FROM {$CFG->prefix}prog_courseset a
                INNER JOIN {$CFG->prefix}prog_courseset_course b ON a.id = b.coursesetid
                WHERE a.programid = $v->id";
            $records2 = $DB->get_records_sql($query2);
            $courseArr = array();
            foreach($records2 as $val) {
                $courseArr[] = $val->courseid;
            }
            $v->courses = implode(',',$courseArr);

            $program_context = context_program::instance($v->id);
            $fs = get_file_storage();
            $files = $fs->get_area_files($program_context->id, 'totara_program', 'overviewfiles', 0);
            if (count($files) > 0) {
                foreach ($files as $file) {
                    $v->filename = $file->get_filename();
                    $v->filepath =  "$CFG->wwwroot/pluginfile.php".
                        '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                        $file->get_filearea(). $file->get_filepath().$file->get_itemid().'/';
                }
            } else {
                $v->filepath = $CFG->wwwroot . '/theme/parenthexis/pix/';
                $v->filename = 'nocourseimg.jpg';
            }
        }

        return $records;
    }
    
    /*
     *  Get course inprogress of user when user login to Hub
     *  @param $username
     *  @return bool
     */
    function user_course_inprogress($username) {
        global $DB, $CFG;
        // get userid by username
        if ($username)
        {
            $user = get_complete_user_data ('username', $username);
            $query = "SELECT e.id, ue.lastaccess, e.enrol FROM {$CFG->prefix}enrol e LEFT JOIN {$CFG->prefix}user_enrolments ue ON e.id = ue.enrolid WHERE ue.userid = $user->id";
            $result = $DB->get_records_sql($query);
            
            $inprogress = false;
            foreach ($result as $res) {
                if($res->lastaccess > 0) {
                    $inprogress = true;
                }
            }
        }
        return $inprogress;
    }

    /**
     * Returns course list based on start chars
     *
     * @param start_chars: return courses that starts with these chars
     */
    function courses_abc ($start_chars, $username) {
        global $CFG, $DB;

        $where = '(';
        $array = str_split ($start_chars);
        foreach ($array as $c)
        {
            $conds[] =  " (fullname like '$c%')";
        }
        $where .= implode (' OR ', $conds);
        $where .= ')';

        $query =
            "SELECT
            co.id          AS remoteid,
            ca.id          AS cat_id,
            co.sortorder,
            co.fullname,
            co.shortname,
            co.idnumber,
            co.summary,
            co.startdate,
            co.timecreated as created,
            co.timemodified as modified,
            ca.name        AS cat_name,
            ca.description AS cat_description
            FROM
            {$CFG->prefix}course_categories ca
            JOIN
            {$CFG->prefix}course co ON
            ca.id = co.category
            WHERE
            co.visible = '1'  AND 
            $where
            ORDER BY
            fullname
            ";
        $records = $DB->get_records_sql($query);

        if ($username)
        {
            $user = get_complete_user_data ('username', $username);
            $c = enrol_get_users_courses ($user->id, true);

            $my_courses = array ();
            foreach ($c as $course)
            {
                $my_courses[] = $course->id;
            }
        }

        $i = 0;
        $now = time();
        $options['noclean'] = true;
        $cursos = array ();
        foreach ($records as $curso)
        {
            $c = get_object_vars ($curso);

            $c['self_enrolment'] = 0;
            $c['guest'] = 0;
            $in = true;
            $enrol_methods = enrol_get_instances($curso->remoteid, true);
            foreach ($enrol_methods as $instance)
            {
                if (($instance->enrol == 'paypal') || ($instance->enrol == 'joomdle'))
                {
                    $enrol = $instance->enrol;
                    $query = "SELECT cost, currency
                                FROM {$CFG->prefix}enrol
                                where courseid = ? and enrol = ?";
                    $params = array ($curso->remoteid, $enrol);
                    $record =  $DB->get_record_sql($query, $params);
                    $c['cost'] = (float) $record->cost;
                    $c['currency'] = $record->currency;
                }

                // Self-enrolment
                if ($instance->enrol == 'self')
                    $c['self_enrolment'] = 1;

                // Guest access
                if ($instance->enrol == 'guest')
                    $c['guest'] = 1;

                if (($instance->enrolstartdate) && ($instance->enrolenddate))
                {
                    $in = false;
                    if (($instance->enrolstartdate <= $now) && ($instance->enrolenddate >= $now))
                        $in = true;
                }
                else if ($instance->enrolstartdate)
                {
                    $in = false;
                    if (($instance->enrolstartdate <= $now))
                        $in = true;
                }
                else if ($instance->enrolenddate)
                {
                    $in = false;
                    if ($instance->enrolenddate >= $now)
                        $in = true;
                }
            }
            $c['in_enrol_date'] = $in;

            $c['enroled'] = 0;
            if ($username)
            {
                if (in_array ($curso->remoteid, $my_courses))
                    $c['enroled'] = 1;
            }


            $c['fullname'] = format_string($c['fullname']);
            $c['cat_name'] = format_string($c['cat_name']);

            $context = context_course::instance($curso->remoteid);
            $c['summary'] = file_rewrite_pluginfile_urls ($c['summary'], 'pluginfile.php', $context->id, 'course', 'summary', NULL);
            $c['summary'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $c['summary']);
            $c['summary'] = format_text($c['summary'], FORMAT_MOODLE, $options);

            $cursos[] = $c;
        }

        return ($cursos);
    }
    /**
     * Returns course category list
     *
     * @param int $cat Parent category
     */
    function get_course_categories ($cat = 0, $username = null, $isLP = 0) {
        global $CFG, $DB;

        $cat = addslashes ($cat);
        $all = false;
        if ($cat == -1) $all = true;

        if ($username) {
            $user = get_complete_user_data('username', $username);
            if ($isLP) $cat = 0 ; else $cat = 67;
            $query =
                "SELECT id, name, description, parent
            FROM
            {$CFG->prefix}course_categories
            WHERE
            visible = '1' AND
            parent = ? AND userowner = $user->id
            ORDER BY
            sortorder ASC
            ";
        } else
        if ($all) $query =
            "SELECT id, name, description, parent
            FROM
            {$CFG->prefix}course_categories
            WHERE
            visible = '1'
            ORDER BY
            sortorder ASC
            ";
        else
            $query =
            "SELECT id, name, description, parent
            FROM
            {$CFG->prefix}course_categories
            WHERE
            visible = '1' AND
            parent = ?
            ORDER BY
            sortorder ASC
            ";

        $params = array ($cat);
        $records =  $DB->get_records_sql($query, $params);

        $options['noclean'] = true;
        $cats = array ();
        foreach ($records as $cat)
        {
            $c = get_object_vars ($cat);
            $c['name'] = format_string($c['name']);
            $c['parent'] = format_string($c['parent']);
            $context = context_coursecat::instance($cat->id);
            $c['description'] = file_rewrite_pluginfile_urls ($c['description'], 'pluginfile.php', $context->id, 'coursecat', 'description', NULL);
            $c['description'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $c['description']);
            $c['description'] = format_text($c['description'], FORMAT_MOODLE, $options);
            $cats[] = $c;
        }

        return ($cats);
    }

    /**
     * Returns courses from a specific category
     *
     * @param string $category Category ID
     * @param int $available If true, return only enrollable courses
     */
    function courses_by_category ($category, $available = 0, $username = '') {
        global $CFG, $DB;

        $where = '';
        if ($available)
            $where = " AND co.enrollable = '1'";

        $query =
            "SELECT
            co.id          AS remoteid,
            ca.id          AS cat_id,
            ca.name        AS cat_name,
            ca.description AS cat_description,
            co.sortorder,
            co.fullname,
            co.summary,
            co.startdate,
            co.timecreated,
            co.timemodified,
            co.timepublish,
            co.visible,
            co.price,
            co.currency,
            co.creator, co.coursesurvey, co.facilitatedcourse, co.duration 
            FROM
            {$CFG->prefix}course_categories ca
            JOIN
            {$CFG->prefix}course co ON
            ca.id = co.category
            WHERE 
            ca.id = ?
            $where
            ORDER BY
            sortorder ASC
            ";

        $params = array ($category);
        $records = $DB->get_records_sql($query,$params);

        if ($username)
        {
            $user = get_complete_user_data ('username', $username);
            $c = enrol_get_users_courses ($user->id, true);

            $my_courses = array ();
            foreach ($c as $course)
            {
                $my_courses[] = $course->id;
            }
        }

        $i = 0;
        $now = time();
        $options['noclean'] = true;
        $cursos = array ();
        foreach ($records as $curso)
        {
            $c = get_object_vars ($curso);
//            if ($c['visible'] && $c['timepublish'] == 0) $c['timepublish'] = 1483203600; 
            // Course cost
            $enrol_methods = enrol_get_instances($curso->remoteid, true);
            $c['self_enrolment'] = 0;
            $c['guest'] = 0;
            $in = true;
            foreach ($enrol_methods as $instance)
            {
                /*
                if (($instance->enrol == 'paypal') || ($instance->enrol == 'joomdle'))
                {
                    $enrol = $instance->enrol;
                    $query = "SELECT cost, currency
                                FROM {$CFG->prefix}enrol
                                where courseid = ? and enrol = ?";
                    $params = array ($curso->remoteid, $enrol);
                    $record =  $DB->get_record_sql($query, $params);
                    $c['cost'] = (float) $record->cost;
                    $c['currency'] = $record->currency;
                }
                */

                // Self-enrolment
                if ($instance->enrol == 'self')
                    $c['self_enrolment'] = 1;

                // Guest access
                if ($instance->enrol == 'guest')
                    $c['guest'] = 1;

                if (($instance->enrolstartdate) && ($instance->enrolenddate))
                {
                    $in = false;
                    if (($instance->enrolstartdate <= $now) && ($instance->enrolenddate >= $now))
                        $in = true;
                }
                else if ($instance->enrolstartdate)
                {
                    $in = false;
                    if (($instance->enrolstartdate <= $now))
                        $in = true;
                }
                else if ($instance->enrolenddate)
                {
                    $in = false;
                    if ($instance->enrolenddate >= $now)
                        $in = true;
                }
            }
            $c['cost'] = $c['price'];
            $c['in_enrol_date'] = $in;

            $c['enroled'] = 0;
            if ($username)
            {
                if (in_array ($curso->remoteid, $my_courses))
                    $c['enroled'] = 1;
            }

            $c['fullname'] = format_string($c['fullname']);
            $c['cat_name'] = format_string($c['cat_name']);
            $context = context_coursecat::instance($c['cat_id']);
            $c['cat_description'] = file_rewrite_pluginfile_urls ($c['cat_description'], 'pluginfile.php', $context->id, 'coursecat', 'description', NULL);
            $c['cat_description'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $c['cat_description']);
            $c['cat_description'] = format_text($c['cat_description'], FORMAT_MOODLE, $options);

            $context = context_course::instance($curso->remoteid);
            $c['summary'] = file_rewrite_pluginfile_urls ($c['summary'], 'pluginfile.php', $context->id, 'course', 'summary', NULL);
            $c['summary'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $c['summary']);
            $c['summary'] = format_text($c['summary'], FORMAT_MOODLE, $options);
            $c['coursesurvey'] = $curso->coursesurvey;
            $c['facilitatedcourse'] = $curso->facilitatedcourse;
            $c['duration'] = $curso->duration;

            $cursos[] = $c;
        }


        return ($cursos);
    }


    /**
     * Returns detailed info aboout a course
     *
     * @param int $id Course identifier
     */
    function get_course_info ($id, $username = '') {
        global $CFG, $DB;

        if (!$DB->record_exists("course", array("id"=>$id))) {
            $return = array("remoteid"=> 0,"cat_id"=>0,"cat_name"=>"","cat_description"=>"","sortorder"=> "","fullname"=>"","shortname"=>"","idnumber"=>"","summary"=>"","filepath"=>"http=>//localhost/px/courses/theme/parenthexis/pix/","filename"=> "nocourseimg.jpg","startdate"=> 0,"numsections"=> 0,"lang"=>"","cost"=>0,"currency"=>COURSE_CURRENCY,"enrolperiod"=>0,"self_enrolment"=>0,"enroled"=>0,"in_enrol_date"=>true,"guest"=>0,"creator"=>"0","learningoutcomes"=>null,"targetaudience"=>null, "course_status" => ""
            );
            return $return;
        }
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $this->check_old_course($id, $username);

        $query =
            "SELECT
            co.id          AS remoteid,
            ca.id          AS cat_id,
            ca.name        AS cat_name,
            ca.description AS cat_description,
            ca.parent AS cat_parent,
            co.sortorder,
            co.fullname,
            co.shortname,
            co.idnumber,
            co.summary,
            co.startdate,
            co.timepublish,
            co.lang,
            co.creator,
            co.duration,
            co.price,
            co.currency,
            co.learningoutcomes,
            co.targetaudience,
            co.coursesurvey,
            co.facilitatedcourse,
            co.certificatecourse,
            co.visible
            FROM
            {$CFG->prefix}course_categories ca
            JOIN
            {$CFG->prefix}course co ON
            ca.id = co.category
            WHERE
            co.id = ?
            ORDER BY
            sortorder ASC";

        $params = array ($id);
        $record =  $DB->get_record_sql($query, $params);

        $fs = get_file_storage();
        $options['noclean'] = true;

        $course_info =  get_object_vars ($record);

        $course_info['fullname'] = format_string($course_info['fullname']);
        $course_info['cat_name'] = format_string($course_info['cat_name']);
        $context = context_coursecat::instance($course_info['cat_id']);
        /* Check fielt visible course */
        $course_info['visible'] = (int)$record->visible;
        $course_info['cat_description'] = file_rewrite_pluginfile_urls ($course_info['cat_description'], 'pluginfile.php', $context->id, 'coursecat', 'description', NULL);
        $course_info['cat_description'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $course_info['cat_description']);
        $course_info['cat_description'] = format_text($course_info['cat_description'], FORMAT_MOODLE, $options);
        $context = context_course::instance($record->remoteid);
//        $course_info['summary'] = nl2br($course_info['summary']);
//        $course_info['summary'] = file_rewrite_pluginfile_urls ($course_info['summary'], 'pluginfile.php', $context->id, 'course', 'summary', NULL);
//        $course_info['summary'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $course_info['summary']);
//        $course_info['summary'] = format_text($course_info['summary'], FORMAT_MOODLE, $options);

        $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', 0);
        if (count($files) > 0) {
            foreach ($files as $file) {
                $fname = $file->get_filename();
                if ($fname == '.') continue;
                $course_info['filename'] = $fname;
                $course_info['fileid'] = $context->id;
                $course_info['filetype'] = $file->get_mimetype();

                $course_info['filepath'] = $CFG->wwwroot . '/pluginfile.php/' . $context->id . '/course/overviewfiles/';
            }
        } else {
            $course_info['filepath'] = $CFG->wwwroot . '/theme/parenthexis/pix/';
            $course_info['filename'] = 'nocourseimg.jpg';
        }
        /* Get course cost if any */
        $instances = enrol_get_instances($id, true);

        $params = array ($id);
        $query =
            "SELECT count(*)
            FROM
            {$CFG->prefix}course_sections
            WHERE
            course = ? and section != 0 and visible=1
            ";

        $course_info['numsections'] = $DB->count_records_sql($query, $params);
        if($record->learningoutcomes) {
        $course_info['learningoutcomes'] = $record->learningoutcomes;
        } else {
            $course_info['learningoutcomes'] = '';
        }

//        $course_info['learningoutcomes'] = file_rewrite_pluginfile_urls ($course_info['learningoutcomes'], 'pluginfile.php', $context->id, 'course', 'learningoutcomes', NULL);
//        $course_info['learningoutcomes'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $course_info['learningoutcomes']);
//        $course_info['learningoutcomes'] = format_text($course_info['learningoutcomes'], FORMAT_MOODLE, $options);

        if($record->targetaudience) {
        $course_info['targetaudience'] = $record->targetaudience;
        } else {
            $course_info['targetaudience'] = '';
        }
        $course_info['coursesurvey'] = $record->coursesurvey;
        $course_info['facilitatedcourse'] = $record->facilitatedcourse;

        if($record->duration) {
            $course_info['duration'] = (float)($record->duration/3600);
        } else {
            $course_info['duration'] = 0;
        }
        $course_info['price'] = (float) $record->price;
        if($record->currency) {
        $course_info['currency'] = $record->currency;
        } else {
            $course_info['currency'] = '';
        }

        $creator = get_complete_user_data ('id', $record->creator);
        $course_info['creator'] = $creator->username;

        $hascertificate = false;
        $act_certificate = $this->get_course_mods($record->remoteid, $creator->username, 'certificate');
        if ($act_certificate['status'] && count($act_certificate['coursemods'])> 0) {
            foreach ($act_certificate['coursemods'] as $mod) {
                $resources = $mod['mods'];
                foreach ($resources as $res) {
                    if ($res['mod'] == 'certificate') {
                        $hascertificate = true;
                    }
                }
            }
        }
        if ($hascertificate == true) {
            $course_info['certificatecourse'] = 1;
        } else {
            $course_info['certificatecourse'] = 0;
        }

        $course_info['self_enrolment'] = 0;
        $course_info['guest'] = 0;
        $in = true;
        $now = time ();
        foreach ($instances as $instance) {
            /* Get enrolment dates. We get the last one, as good/bad as any other XXX */
            if ($instance->enrolstartdate)
                $course_info['enrolstartdate'] = $instance->enrolstartdate;
            if ($instance->enrolenddate)
                $course_info['enrolenddate'] = $instance->enrolenddate;

            if ($instance->enrolperiod)
                $course_info['enrolperiod'] = $instance->enrolperiod;

            // Self-enrolment
            if ($instance->enrol == 'self')
                $course_info['self_enrolment'] = 1;

            // Guest access
            if ($instance->enrol == 'guest')
                $course_info['guest'] = 1;

            if (($instance->enrolstartdate) && ($instance->enrolenddate)) {
                $in = false;
                if (($instance->enrolstartdate <= $now) && ($instance->enrolenddate >= $now))
                    $in = true;
            } else if ($instance->enrolstartdate) {
                $in = false;
                if (($instance->enrolstartdate <= $now))
                    $in = true;
            } else if ($instance->enrolenddate) {
                $in = false;
                if ($instance->enrolenddate >= $now)
                    $in = true;
            }
        }
        $course_info['in_enrol_date'] = $in;
        $course_info['enroled'] = 0;
        if ($username) {
            $user = get_complete_user_data ('username', $username);
            $courses = enrol_get_users_courses ($user->id, true);
            $my_courses = array ();
            foreach ($courses as $course) {
                $my_courses[] = $course->id;
            }
            if (in_array ($id, $my_courses)) {
                $course_info['enroled'] = 1;
                $query =
                    "SELECT ue.timeend
                FROM
                {$CFG->prefix}user_enrolments ue INNER JOIN {$CFG->prefix}enrol e ON ue.enrolid = e.id
                WHERE
                ue.userid = $user->id and e.courseid = $id and e.status = 0
                ";
                $res = $DB->get_records_sql($query);
                $max = 1;
                foreach ($res as $key => $value) {
                    if ($max != 0) {
                        if ($value->timeend == 0) $max = 0; else
                    $max = max($max, $value->timeend);
                }
                }
                $course_info['enrol_timeend'] = $max;
                $course_info['userRoles'] = json_encode($this->get_user_role($id, $username));
            }
        }
        $countlearner = 0;
        $userRole = $this->get_user_role($record->remoteid);
        if ($userRole['status']) {
            $userroles = $userRole['roles'];
        } else {
            $userroles = array();
        }
        if (!empty($userroles)) {
            foreach ($userroles as $userrole) {
                foreach (json_decode($userrole['role']) as $role) {
                    if ($role->roleid == 5) {
                        $countlearner++;
                    }
                }
            }
        }
        $course_info['count_learner'] = $countlearner;
        $course_info['course_status'] = '';
        $course_status = $this->get_course_status($id);
        if($course_status) {
            $course_info['course_status'] = $course_status;
        }
        $coursetype = $this->check_course_type($id);
        if ($coursetype) {
            $course_info['course_type'] = $coursetype['role'];
        } else {
            $course_info['course_type'] = false;
        }
        return $course_info;
    }
    
    function get_course_status($id) {
        global $DB, $CFG;
        $course_request = $DB->get_record('course_request_approve', array('courseid'=>$id));
        if($course_request && $course_request->status == 0) {
           $status = 'pending_approval';
        } else if($course_request && $course_request->status == 1) {
           $status = 'approved';
        } else if($course_request && $course_request->status == -1) {
           $status = 'unapproved';
        } else if($course_request && $course_request->status == 2) {
           $status = 'published';
        } else if($course_request && $course_request->status == -2) {
           $status = 'unpublished';
        } else { 
            $enrol_methods = enrol_get_instances($id, true);
            foreach ($enrol_methods as $instance) {
                // Self-enrolment
                if ($instance->enrol == 'self') {
                    $status = 'published';
                    break;
        }
            }
        }
        return $status;
    }
    
    /*
     *  get course header for PX scrubbing
     *  @updated by KV
     */
    
    function get_course_header($id, $username)
    {
        global $DB, $CFG;
        if (!$DB->record_exists("course", array("id"=>$id))) {
            $return = array();
            return $return;
        }
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        if ($username)
            $user = get_complete_user_data ('username', $username);
        
        $query =
            "SELECT
            co.id          AS remoteid,
            co.fullname,
            co.creator
            FROM
            {$CFG->prefix}course_categories ca
            JOIN
            {$CFG->prefix}course co ON
            ca.id = co.category
            WHERE
            co.id = ?";

        $params = array ($id);
        $record =  $DB->get_record_sql($query, $params);
        
        $course_info =  get_object_vars ($record);
        

        $return = array();
        $return['coursename'] = $record->fullname;
        
        // get course group from jomsocial for App
        // added by Dungnv 24 Sept 2016
        $group_course = $this->call_method('getGroupbyCourseID', (int) $record->remoteid, (string) $username);
        $return['course_group'] = 0;
        if ($group_course) {
            $return['course_group'] = (int) $group_course[0];
            $return['activity_count'] = (int) $group_course[1];
        }
        
        $context = context_course::instance($record->remoteid);
        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', 0);
        if (count($files) > 0) {
            foreach ($files as $file) {
                $fname = $file->get_filename();
                if ($fname == '.') continue;
                $course_info['filename'] = $fname;
                $course_info['fileid'] = $context->id;
                $course_info['filetype'] = $file->get_mimetype();

                $course_info['filepath'] = $CFG->wwwroot . '/pluginfile.php/' . $context->id . '/course/overviewfiles/';
            }
        } else {
            $course_info['filepath'] = $CFG->wwwroot . '/theme/parenthexis/pix/';
            $course_info['filename'] = 'nocourseimg.jpg';
        }
        $return['courseimage'] = $course_info['filepath'] . $course_info['filename'];
        
        $modinfo = get_fast_modinfo($record->remoteid);
        $sections = $modinfo->get_section_info_all();
        
        $mods = get_fast_modinfo($record->remoteid)->get_cms();

        $return['isOwner'] = false;
        if ($username) {
            $return['isFacilitator'] = $this->checkFacilitator($record->remoteid, $username, 'app');
            if($user->id == $record->creator) {
                $return['isOwner'] = true;
            }
        }

        $count_compled = 0;
        $count_activity = 0;
        if ($sections) {
        foreach ($sections as $section)
        {
            if (!$section->visible && $section->section == 0)
                continue;

            $sectionmods = explode(",", $section->sequence);
            foreach ($sectionmods as $modnumber) {
                if (empty($mods[$modnumber])) {
                    continue;
                }
                $mod = $mods[$modnumber];
                
                if ($mod->modname == 'quiz') {
                    $quiz = $DB->get_record('quiz', array('id' => $mod->instance ));
                    $resource['atype'] = (int)$quiz->atype;
                }
                if (($mod->modname != 'forum') && ($mod->modname != 'certificate') && ($mod->modname != 'feedback') && ($mod->modname != 'questionnaire')) {
                
                if ($username)
                {
                    /*
                     * get activity completion
                     */
                    $mod_completion = $DB->get_record('course_modules_completion', array('coursemoduleid' => $mod->id, 'userid'=>$user->id, 'completionstate'=>'1'));
                    if($mod_completion) {
                        $count_compled++;
                    }
                }
                $count_activity++;
               }
              }
            }
        }
        $num_progress_bar = intval(($count_compled / $count_activity) * 100);
        $return['courseprogress'] = $num_progress_bar;
        $response = array();
        $response['status'] = true;
        $response['message'] = 'success';
        $response['data'] = $return;
        return $response;
    }
    
    function get_course_progressBar($id, $username)
    {
        global $DB, $CFG;
        if (!$DB->record_exists("course", array("id"=>$id))) {
            $return = array();
            return $return;
        }
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        if ($username)
            $user = get_complete_user_data ('username', $username);
        
        
        $modinfo = get_fast_modinfo($id);
        $sections = $modinfo->get_section_info_all();
        
        $mods = get_fast_modinfo($id)->get_cms();
        $count_compled = 0;
        $count_activity = 0;
        if ($sections) {
        foreach ($sections as $section)
        {
            if (!$section->visible && $section->section == 0)
                continue;

            $sectionmods = explode(",", $section->sequence);
            foreach ($sectionmods as $modnumber) {
                if (empty($mods[$modnumber])) {
                    continue;
                }
                $mod = $mods[$modnumber];
                
                if ($mod->modname == 'quiz') {
                    $quiz = $DB->get_record('quiz', array('id' => $mod->instance ));
                    $resource['atype'] = (int)$quiz->atype;
                }
                if (($mod->modname != 'forum') && ($mod->modname != 'certificate') && ($mod->modname != 'feedback') && ($mod->modname != 'questionnaire')) {
                
                if ($username)
                {
                    /*
                     * get activity completion
                     */
                    $mod_completion = $DB->get_record('course_modules_completion', array('coursemoduleid' => $mod->id, 'userid'=>$user->id, 'completionstate'=>'1'));
                    if($mod_completion) {
                        $count_compled++;
                    }
                }
                $count_activity++;
               }
              }
            }
        }
        $num_progress_bar = intval(($count_compled / $count_activity) * 100);
        $return['progress'] = $num_progress_bar;
        
        return $response = array(
                'status' => true,
                'message' => 'success',
                'data' => $return
        );
    }
    
    /**
     * Returns course topics
     *
     * @param int $id Course identifier
     */
    function get_course_contents ($id) {
        global $CFG, $DB;

        $query =
            "SELECT
            cs.id,
            cs.section,
            cs.name,
            cs.summary
            FROM
            {$CFG->prefix}course_sections cs
            WHERE
            cs.course = ?
            and cs.visible = 1
            ORDER by cs.section;
            ";

        $params = array ($id);
        $records =  $DB->get_records_sql($query, $params);

        $context = context_course::instance($id);

        $options['noclean'] = true;
        $data = array ();
        foreach ($records as $r)
        {
            $e['section'] = $r->section;
            $e['section'] = format_string($e['section']);
            $e['name'] = $r->name;
            $e['summary'] = $r->summary;
            $e['summary'] = file_rewrite_pluginfile_urls ($r->summary, 'pluginfile.php', $context->id, 'course', 'section', $r->id);
            $e['summary'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $e['summary']);
            $e['summary'] = format_text($e['summary'], FORMAT_MOODLE, $options);

            $data[] = $e;
        }

        return $data;
    }

    /**
     * Returns editing teachers
     *
     * @param int $id Course identifier
     */
    function get_course_editing_teachers ($id) {
        global $CFG;

        $id = addslashes ($id);
        $context = context_course::instance($id);
        /* 3 indica profesores editores (table mdl_role) */
        $profs = get_role_users(3 , $context);

        $data = array ();
        $i = 0;
        foreach ($profs as $p)
        {
            $e['firstname'] = $p->firstname;
            $e['lastname'] = $p->lastname;
            $e['username'] = $p->username;

            $data[$i] = $e;
            $i++;
        }

        return $data;
    }


    function teachers_abc ($start_chars) {
        global $CFG, $DB;

        $where = '(';
        $array = str_split ($start_chars);
        foreach ($array as $c)
        {
            $conds[] =  " (u.lastname like '$c%')";
        }
        $where .= implode (' OR ', $conds);
        $where .= ')';

        $query = "SELECT distinct (u.id), u.username, u.firstname, u.lastname
                 FROM {$CFG->prefix}course as c, {$CFG->prefix}role_assignments AS ra, {$CFG->prefix}user AS u, {$CFG->prefix}context AS ct
                 WHERE c.id = ct.instanceid AND ra.roleid =3 AND ra.userid = u.id AND ct.id = ra.contextid 
                 AND c.visible=1 and u.suspended=0 AND $where";

        $query .= " ORDER BY lastname, firstname";

        $records =  $DB->get_records_sql($query);
        $data = array ();
        foreach ($records as $p)
        {
            $e['firstname'] = $p->firstname;
            $e['lastname'] = $p->lastname;
            $e['username'] = $p->username;

            $data[] = $e;
        }

        return $data;
    }

    function teacher_courses ($username) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $query = " SELECT distinct c.id as remoteid, c.fullname, ca.name as cat_name, ca.id as cat_id
                    FROM {$CFG->prefix}course as c, {$CFG->prefix}role_assignments AS ra, {$CFG->prefix}user AS u, {$CFG->prefix}context AS ct,  {$CFG->prefix}course_categories ca
                    WHERE c.id = ct.instanceid AND ra.roleid =3 AND ra.userid = u.id AND ct.id = ra.contextid AND ca.id = c.category and u.username= ?";

        $params = array ($username);
        $records =  $DB->get_records_sql($query, $params);
        $data = array ();
        $i = 0;
        foreach ($records as $p)
        {
            $e['remoteid'] = $p->remoteid;
            $e['fullname'] = $p->fullname;
            $e['fullname'] = format_string($e['fullname']);
            $e['cat_id'] = $p->cat_id;
            $e['cat_name'] = $p->cat_name;
            $e['cat_name'] = format_string($e['cat_name']);

            $data[$i] = $e;
            $i++;
        }
        return $data;
    }


    /**
     * Returns number of visible courses
     *
     */
    function get_course_no () {
        global $CFG, $DB;

        $query =
            "SELECT count(*)
            FROM
            {$CFG->prefix}course_categories ca
            JOIN
            {$CFG->prefix}course co ON
            ca.id = co.category
            WHERE
            co.visible = '1'
            ";

        return $DB->count_records_sql($query);
    }

    /**
     * Returns number of visible and enrollable courses
    XXX NOT WORKING IN 2.0 now
     *
     */
    function get_enrollable_course_no () {
        global $CFG, $DB;

        $query =
            "SELECT count(*)
            FROM
            {$CFG->prefix}course_categories ca
            JOIN
            {$CFG->prefix}course co ON
            ca.id = co.category
            WHERE
            co.visible = '1' 
            ";

        return $DB->count_records_sql($query);
    }

    /**
     * Returns student number
     *
     */
    function get_student_no () {
        global $CFG, $DB;

        $query =
            "select count(distinct (userid)) from  {$CFG->prefix}role_assignments where roleid=5;
            ";

        return $DB->count_records_sql($query);

    }

    /**
     * Returns number of submitted assignments in each task of the course
     *
     * @param int $id Course identifier
     *
     */
    function get_assignment_submissions ($id) {
        global $CFG, $DB;

        /* Obtenemos todas las tareas del curso */
        $query =
            "select id,name from  {$CFG->prefix}assignment where course= ?;
                ";
        /* Para cada una, obtenemos el numero de trabajos entregados */


        $params = array ($id);
        $tareas = $DB->get_records_sql($query, $params);

        $i = 0;
        $rdo = array ();
        foreach ($tareas as $tarea)
        {
            $ass_id = $tarea->id;
            $query =
                "select count(*) from  {$CFG->prefix}assignment_submissions where assignment= ?;
                        ";
            $params = array ($ass_id);
            $n = $DB->count_records_sql($query, $params);
            $rdo[$i]['id'] = $tarea->id;
            $rdo[$i]['tarea'] = $tarea->name;
            $rdo[$i]['entregados'] = $n;
            $i++;
        }

        return $rdo;
    }

    /**
     * Returns total number of submitted assignments
     *
     */
    function get_total_assignment_submissions () {
        global $CFG, $DB;

        $query =
            "select count(*) from  {$CFG->prefix}assignment_submissions;
                ";

        $n = $DB->count_records_sql($query);

        return $n;
    }

    function get_assignment_grades ($id) {
        global $CFG, $DB;

        $SQL = "SELECT g.itemid, gi.itemname as iname,SUM(g.finalgrade) AS sum
              FROM {$CFG->prefix}grade_items gi
               JOIN {$CFG->prefix}grade_grades g      ON g.itemid = gi.id
             WHERE gi.courseid = ?
               AND g.finalgrade IS NOT NULL
              GROUP BY g.itemid";
        $sum_array = array();
        $params = array ($id);
        if ($sums = $DB->get_records_sql($SQL, $params))
        {
            foreach ($sums as $itemid => $csum) {
                $sql = " select count(*) from {$CFG->prefix}grade_grades where itemid= ?;";
                $params = array ($itemid);
                $n = $DB->count_records_sql($sql, $params);
                $nota['tarea'] = $csum->iname;
                $nota['media'] = $csum->sum / $n;

                $sum_array[] = $nota;
            }
        }

        return $sum_array;
    }

    /**
     * Returns average grade for a task
     *
     * @param int $id Course identifier
     */
    function get_average_grade ($itemid) {
        global $CFG, $DB;

        $id = addslashes ($itemid);
        $avg = 0;
        $SQL = "SELECT g.itemid, gi.itemname as iname,SUM(g.finalgrade) AS sum
                  FROM {$CFG->prefix}grade_items gi
                   JOIN {$CFG->prefix}grade_grades g      ON g.itemid = gi.id
                 WHERE gi.id = ?
                   AND g.finalgrade IS NOT NULL
              GROUP BY g.itemid";
        $sum_array = array();
        $params = array ($itemid);
        if ($sums = $DB->get_record_sql($SQL, $params)) {
            $sql = " select count(*) from {$CFG->prefix}grade_grades where itemid=?;";
            $n = $DB->count_records_sql($sql, $params);
            $avg = $sums->sum / $n;
        }

        return $avg;
    }

    /**
     * Returns stats about student grades
     * FIXME doc return type
     *
    //XXX creo que no la usamos
     * @param int $id Course identifier
     */
    function get_assignments_grades ($id) {
        global $CFG, $DB;

        /* Obtenemos todas las tareas del curso */
        $query =
            "select id,name from  {$CFG->prefix}assignment where course=?;
                ";
        /* Para cada una, obtenemos la nota media */
        $params = array ($id);
        $tareas = $DB->get_records_sql($query, $params);

        $i = 0;
        foreach ($tareas as $tarea)
        {
            $ass_id = $tarea->id;
            $query =
                "select itemid,avg(finalgrade) as media
                        from  {$CFG->prefix}grade_grades 
                        where itemid= ? and
                        finalgrade is not NULL
                        GROUP BY itemid;
                        ";
            $params = array ($ass_id);
            $n = $DB->get_records_sql($query, $params);
            $rdo[$i]['tarea'] = $tarea->name;
            foreach ($n as $nn)
                $rdo[$i]['media'] = $nn;
            $i++;
        }

        return $rdo;
    }

    /**
     * Returns grades of a student for each task in a course
     *
     * @param string $user Username
     * @param int $cid Course identifier
     */
    function get_user_grades ($username,$cid) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);
        $uid = $user->id;


        $SQL = "SELECT g.itemid, g.finalgrade,gi.courseid,gi.itemname,gi.id, g.timemodified
                      FROM {$CFG->prefix}grade_items gi
                           JOIN {$CFG->prefix}grade_grades g      ON g.itemid = gi.id
                           JOIN {$CFG->prefix}user u              ON u.id = g.userid
                           JOIN {$CFG->prefix}role_assignments ra ON ra.userid = u.id
                     WHERE g.finalgrade IS NOT NULL
               AND u.id =  ?
               AND gi.courseid = ?
                  GROUP BY g.itemid, g.finalgrade, gi.courseid, gi.itemname,gi.id, g.timemodified";

        $sum_array = array();
        $params = array ($uid, $cid);
        if ($sums = $DB->get_records_sql($SQL, $params))
        {
            $i = 0;
            $rdo = array ();
            foreach ($sums as $sum)
            {
                if (! $grade_grade = grade_grade::fetch(array('itemid'=>$sum->id,'userid'=>$uid))) {
                    $grade_grade = new grade_grade();
                    $grade_grade->userid = $this->user->id;
                    $grade_grade->itemid = $grade_object->id;
                }

                $grade_item = $grade_grade->load_grade_item();

                $sums2[$i] = $sum;
                $scale = $grade_item->load_scale();
                $formatted_grade = grade_format_gradevalue($sums2[$i]->finalgrade, $grade_item, true, GRADE_DISPLAY_TYPE_REAL);

                $sums2[$i]->finalgrade = $formatted_grade;

                $rdo[$i]['itemname'] = $sum->itemname;
                $rdo[$i]['finalgrade'] = $formatted_grade;

                $i++;
            }
            return $rdo;
        }

        return array();
    }


    function get_course_grades_by_category ($id, $username) {
        global $CFG, $DB;

        // require libraries grade: updated 18 Jul 2016 by KV team
        require_once $CFG->dirroot.'/grade/lib.php';
        require_once $CFG->libdir.'/gradelib.php';

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);

        if (!$user)
            return array();

        // Get course total
        $query =
            "SELECT id
            from  {$CFG->prefix}grade_items
            where courseid = '$id'
            AND itemtype = 'course'
                ";
        $cat_item = $DB->get_record_sql($query);

        // get grade item object: updated 18 Jul 2016 by KV team
        $grade_item = grade_item::fetch(array('id'=>$cat_item->id, 'courseid'=>$id));
        $gradedisplaytype = 3;

        $query = "SELECT g.finalgrade, g.rawgrademax, g.feedback, g.id, g.hidden, g.userid, g.usermodified,g.timemodified
          FROM {$CFG->prefix}grade_grades g
         WHERE g.itemid = ?
           AND g.userid =  ?";
        $params = array ($cat_item->id, $user->id);

        $grade = $DB->get_record_sql($query, $params);

        // convert grade final number to letters: updated 18 Jul 2016 by KV team
        $grade_letter = grade_format_gradevalue($grade->finalgrade, $grade_item, true, $gradedisplaytype, null);
        // get grade lettes
        $letters = grade_get_letters(null);
        $total['fullname'] = '';
        // get grade item id and grade id
        $total['userid'] = (int) $grade->userid;
        $total['usermodified'] = (int) $grade->usermodified;
        $total['gradeitemid'] = (int) $cat_item->id;
        $total['gradeid'] = (int) $grade->id;
        $total['finalgrade'] = (float) $grade->finalgrade;
        $total['finalgradeletters'] = $grade_letter;
        $total['grademax'] = (float) $grade->rawgrademax;
        $total['gradefeedback'] = $grade->feedback;
        $total['hidden'] = (int)$grade->hidden;
        $total['timemodified'] = (int)$grade->timemodified;
        if($total['usermodified'] != null){
            $total['gradeoffacitator'] = true;
        } else {
            $total['gradeoffacitator'] = false;
        }
        $total['items'] = array();

        // Get items of main grade category
        $query =
            "SELECT gi.id, gi.itemname, gi.grademax, gi.itemmodule, gi.iteminstance
                FROM  {$CFG->prefix}grade_categories gc JOIN {$CFG->prefix}grade_items gi ON gc.id = gi.categoryid
                WHERE gc.courseid = '$id' AND gc.depth = 1 AND gc.hidden = 0
                    ";

        $items = $DB->get_records_sql($query);
        $category_items = array ();
        foreach ($items as $item) {
            $category_item['name'] = $item->itemname;
            $category_item['grademax'] = (float)$item->grademax;

            switch ($item->itemmodule) {
                case 'quiz':
                    $conditions = array ('id' => $item->iteminstance);
                    $quiz = $DB->get_record('quiz',$conditions);
                    $category_item['due'] = $quiz->timeclose;
                    break;
                case 'assignment':
                    $conditions = array ('id' => $item->iteminstance);
                    $assignment = $DB->get_record('assignment',$conditions);
                    $category_item['due'] = $assignment->timedue;
                    break;
                default:
                    $category_item['due'] = 0;
                    break;
            }

            $query = "SELECT g.finalgrade, g.feedback
                  FROM {$CFG->prefix}grade_grades g
                 WHERE g.itemid = ?
                   AND g.userid =  ?";
            $params = array ($item->id, $user->id);

            $grade = $DB->get_record_sql($query, $params);

            if ($grade) {
                $category_item['finalgrade'] = (float) $grade->finalgrade;
                $category_item['feedback'] = $grade->feedback;
            } else {
                $category_item['finalgrade'] = (float) 0;
                $category_item['feedback'] = '';;
            }

            $category_items[] = $category_item;
        }
        if (!empty($category_items)) $total['items'] = $category_items;

        $letter_option = array();
        foreach ($letters as $key => $value) {
            $grade_letter = array();
            $grade_letter['value'] = $key;
            $grade_letter['name'] = $value;
            $letter_option[] = $grade_letter;
        }

        $total['gradeoptionsletters'] = $letter_option;

        $data = array ();
        $data[] = $total;

        //get grade sub-categories
        $query =
            "SELECT
            {$CFG->prefix}grade_categories.id AS grade_cat_id,
            {$CFG->prefix}grade_categories.fullname AS grade_cat_fullname,
            {$CFG->prefix}grade_items.id AS grade_item_id,
            {$CFG->prefix}grade_items.grademax AS grade_item_grademax
            FROM {$CFG->prefix}grade_categories, {$CFG->prefix}grade_items
            WHERE {$CFG->prefix}grade_categories.id = {$CFG->prefix}grade_items.iteminstance
            AND {$CFG->prefix}grade_items.courseid = '$id'
            AND {$CFG->prefix}grade_items.itemtype = 'category';
                ";

        $cats = $DB->get_records_sql($query);

        foreach ($cats as $r) {
            $e['fullname'] = $r->grade_cat_fullname;
            $e['grademax'] = (float) $r->grade_item_grademax;
            $e['gradeitemid'] = (int) $r->grade_item_id;

            $cat_id = $r->grade_cat_id;

            // Get category grade total
            $query =
                "SELECT id
                FROM  {$CFG->prefix}grade_items
                WHERE iteminstance = '$cat_id'
                    ";

            $cat_item = $DB->get_record_sql($query);

            $query = "SELECT g.finalgrade
              FROM {$CFG->prefix}grade_grades g
             WHERE g.itemid = ?
               AND g.userid =  ?";
            $params = array ($cat_item->id, $user->id);

            $grade = $DB->get_record_sql($query, $params);
            $e['finalgrade'] = (float) $grade->finalgrade;
            $e['gradeid'] = (int) $grade->id;
            $e['gradefeedback'] = $grade->feedback;


            $grade_item = grade_item::fetch(array('id'=>$cat_item->id, 'courseid'=>$id));
            $grade_letter = grade_format_gradevalue($grade->finalgrade, $grade_item, true, $gradedisplaytype, null);
            $e['finalgradeletters'] = $grade_letter;

            // Get items
            $query =
                "SELECT *
                FROM  {$CFG->prefix}grade_items
                WHERE categoryid = '$cat_id'
                    ";

            $items = $DB->get_records_sql($query);
            $category_items = array ();

            foreach ($items as $item) {
                $category_item['name'] = $item->itemname;
                $category_item['grademax'] = (float)$item->grademax;

                switch ($item->itemmodule) {
                    case 'quiz':
                        $conditions = array ('id' => $item->iteminstance);
                        $quiz = $DB->get_record('quiz',$conditions);
                        $category_item['due'] = $quiz->timeclose;
                        break;
                    case 'assignment':
                        $conditions = array ('id' => $item->iteminstance);
                        $assignment = $DB->get_record('assignment',$conditions);
                        $category_item['due'] = $assignment->timedue;
                        break;
                    default:
                        $category_item['due'] = 0;
                        break;
                }

                $query = "SELECT g.finalgrade, g.feedback
                  FROM {$CFG->prefix}grade_grades g
                 WHERE g.itemid = ?
                   AND g.userid =  ?";
                $params = array ($item->id, $user->id);
                $grade = $DB->get_record_sql($query, $params);

                if ($grade) {
                    $category_item['finalgrade'] = (float) $grade->finalgrade;
                    $category_item['feedback'] = $grade->feedback;
                } else {
                    $category_item['finalgrade'] = (float) 0;
                    $category_item['feedback'] = '';;
                }

                $category_items[] = $category_item;
            }

            $e['items'] = $category_items;
            $e['gradeoptionsletters'] = $letter_option;

            $data[] = $e;
        }

        // don't return total if there is nothing else
        /*if (count ($data) == 1)
            return array ();*/

        return $data;
    }

    // save grade for only user
    function save_grade_user($username, $grade_id, $gradeitem_id, $grade_value, $feedback) {
        global $DB, $CFG, $USER;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);
        $gradeitem = $DB->get_record('grade_items', array('id'=>$gradeitem_id));
        if ($gradeitem_id) {
            if($grade_id > 0) {
                $grades = $DB->get_record('grade_grades', array('id'=>$grade_id));
                $quiz_grades = $DB->get_record('quiz_grades', array('quiz'=>$gradeitem->iteminstance, 'userid'=>$user->id));
                if ($grades && $grade_value != (-1)) {
                    $grades->id = $grade_id; //active
                    $grades->userid = $user->id;
                    $grades->usermodified = $USER->id;
                    $grades->overridden = time();
                    if ($grades->rawgrademax == '100.000') $grades->finalgrade = $grade_value;
                    else if ($grades->rawgrademax == '10.000') $grades->finalgrade = $grade_value/10;
                    $grades->feedback = utf8_decode ($feedback);
                    $grades->timemodified = time();
                    $grades->hidden = 0;
                    $DB->update_record('grade_grades', $grades);
                    if($quiz_grades){
                        $quiz_grades->id = $quiz_grades->id ;
                        $quiz_grades->grade = $grade_value/10;
                        $quiz_grades->timemodified = time();
                        $DB->update_record('quiz_grades', $quiz_grades);
                    }
                    return true;
                } elseif($grades && $grade_value == (-1)){
                    $grades->id = $grade_id; //active
                    $grades->userid = $user->id;
                    $grades->usermodified = $USER->id;
                    $grades->overridden = time();
                    $grades->feedback = utf8_decode ($feedback);
                    $grades->timemodified = time();
                    $grades->hidden = 1;
                    $DB->update_record('grade_grades', $grades);
                    return true;
                }
            } else {
                $data = new stdClass();
                if($grade_value != (-1)){
                    $data->itemid = $gradeitem_id;
                    $data->userid = (int)$user->id;
                    $data->rawgrademax = '100.000';
                    $data->usermodified = $USER->id;
                    $grades->hidden = 0;
                    $data->finalgrade = $grade_value;
                    $data->feedback = utf8_decode ($feedback);
                    $data->overridden = time();
                    $data->timecreated = time();
                    $data->timemodified = time();
                } else {
                    $data->itemid = $gradeitem_id;
                    $data->userid = (int)$user->id;
                    $data->rawgrademax = '100.000';
                    $data->usermodified = $USER->id;
                    $data->hidden = 1;
                    $data->feedback = utf8_decode ($feedback);
                    $data->overridden = time();
                    $data->timecreated = time();
                    $data->timemodified = time();
                }
                $DB->insert_record('grade_grades', $data);
                if(!$quiz_grades){
                    $dt = new stdClass();
                    $dt->quiz = $gradeitem->iteminstance;
                    $dt->userid = (int) $user->id;
                    $dt->grade = $grade_value/10;
                    $dt->timemodified = time();
                    $DB->insert_record('quiz_grades', $dt);
                }
                return true;
            }
        } else {
            return false;
        }
    }
    

    // save activity status and feedback
    function save_completion_activity($username, $act_id, $status, $feedback) {
        global $DB, $CFG, $USER;
        
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);
        $value = 0;
        if($status == 'completed') {
            $value = 1;
        } else if ($status == 'in-progress') {
            $value = 0;
        }
        //check activity module completion
        if($status != 'not yet started') {
            $mod_compled = $DB->get_record('course_modules_completion', array('coursemoduleid' => $act_id, 'userid'=>$user->id));
            if($mod_compled) {
                $compl = new stdClass();
                $compl->id = $mod_compled->id; //active
                $compl->userid = $user->id;
                $compl->completionstate = $value;
                $compl->timemodified = time();
                $compl->timecompleted = time();
                $DB->update_record('course_modules_completion', $compl);
            } else {
                $compl = new stdClass();
                $compl->coursemoduleid = $act_id;
                $compl->userid = $user->id;
                $compl->completionstate = $value;
                $compl->viewed = 0;
                $compl->timemodified = time();
                $compl->timecompleted = time();
                $DB->insert_record('course_modules_completion', $compl);
            }
        }
        
        // Save activity completed
        if ($value == 1) {
            $course_module_info = $DB->get_record('course_modules',array('id' => $act_id));
            $module_info = $DB->get_record('modules',array('id'=> $course_module_info->module));
            $course_compl_crit = $DB->get_record('course_completion_criteria', array('moduleinstance'=>$act_id));
            if ($course_compl_crit) {
                $act_completed = $DB->get_record('course_completion_crit_compl', array('userid'=>$user->id, 'course'=>$course_compl_crit->course, 'criteriaid'=>$course_compl_crit->id));
                if ($act_completed) {
                    $actcompl = new stdClass();
                    $actcompl->id = $act_completed->id; //active
                    $actcompl->timecompleted = time();
                    $DB->update_record('course_completion_crit_compl', $actcompl);
                } else {
                    $actcompl = new stdClass();
                    $actcompl->userid = $user->id;
                    $actcompl->course = $course_compl_crit->course;
                    $actcompl->criteriaid = $course_compl_crit->id;
                    $actcompl->timecompleted = time();
                    $DB->insert_record('course_completion_crit_compl', $actcompl);
                }
            } else {                
                $addto_course_completion_criteria = new stdClass();
                $addto_course_completion_criteria->course = $course_module_info->course;
                $addto_course_completion_criteria->criteriatype = 4;
                $addto_course_completion_criteria->module = $module_info->name;
                $addto_course_completion_criteria->moduleinstance = $course_module_info->id;
                $id = $DB->insert_record('course_completion_criteria', $addto_course_completion_criteria);
                
                $actcompl = new stdClass();
                $actcompl->userid = $user->id;
                $actcompl->course = $course_module_info->course;
                $actcompl->criteriaid = $id;
                $actcompl->timecompleted = time();
                $DB->insert_record('course_completion_crit_compl', $actcompl);
            }
//            $course_module_info = $DB->get_record('course_modules',array('id' => $act_id));
//            $module_info = $DB->get_record('modules',array('id'=> $course_module_info->module));
//        
//            $cm = get_coursemodule_from_id($module_info->name, $act_id, 0, false, MUST_EXIST);
//            $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
//            $completion=new completion_info($course);
//            $completion->set_module_completed($cm, $user->id);
            // Complete course completion.
            $test = $this->course_completed($course_compl_crit->course, $user->id); 

            //save activity quiz
                    

            }

        //save feed back for each other module
        /*$course_module = $DB->get_record('course_module', array('id'=>$act_id));
        switch ($act_type) {
            case 'checklist':
                $data = new stdClass();
                $data->itemid = $course_module->instance;
                $data->userid = $user->id;
                $data->commentby = $user->id;
                $data->text = $feedback;
                $DB->insert_record('checklist_comment', $data);
                break;
        }*/
        return true;
    }
    
    function course_completed ($courseid, $userid) {
        global $DB;
        
        // Grab all criteria and their associated criteria completions
        $sql = '
            SELECT DISTINCT
                c.id AS course,
                cr.id AS criteriaid,
                cr.criteriatype AS criteriatype
            FROM
                {course_completion_criteria} cr
            INNER JOIN
                {course} c
             ON cr.course = c.id
            WHERE
                c.enablecompletion = 1
            AND cr.course = :courseid
            ORDER BY
                course
        ';

        $rs = $DB->get_recordset_sql($sql, array('courseid' => $courseid));
        
        // Grab all criteria and their associated criteria completions
        $sql2 = '
            SELECT DISTINCT
                cc.course AS course,
                cc.userid AS userid,
                cc.criteriaid AS criteriaid,
                cc.timecompleted AS timecompleted
            FROM
                {course_completion_crit_compl} cc
            WHERE
                cc.course = :courseid
            AND cc.userid = :userid
            ORDER BY
                course
        ';

        $cc = $DB->get_recordset_sql($sql2, array('courseid' => $courseid, 'userid' => $userid));
    
        $crit_compl_arr = array();
        $cc_arr = array();
        if ($cc) {
            foreach ($cc as $course_crit_comp) {
                $crit_compl_arr[] = $course_crit_comp->criteriaid;
                $cc_arr[$course_crit_comp->criteriaid] = $course_crit_comp;
            }
        }
    
        $completions = array();

        // Grab records for current user/course
        foreach ($rs as $record) {
            // If we are still grabbing the same users completions
            if (in_array($record->criteriaid, $crit_compl_arr)) {
                $record->userid = $cc_arr[$record->criteriaid]->userid;
                $record->timecompleted = $cc_arr[$record->criteriaid]->timecompleted;
                $completions[$record->criteriaid] = $record;
            } else {
                $record->userid = null;
                $record->timecompleted = null;
                $completions[$record->criteriaid] = $record;
            }
        }
            
        // Aggregate
        if (!empty($completions)) {
            if (!$DB->get_record('course_completions', array('userid'=>$userid, 'course'=>$courseid), '*')) {
                // Get earliest current enrolment start date
                $sql3 = "SELECT ue.*
                        FROM {user_enrolments} ue
                        JOIN {enrol} e ON (e.id = ue.enrolid AND e.courseid = :courseid)
                        JOIN {user} u ON u.id = ue.userid
                        WHERE ue.userid = :userid AND ue.status = :active
                        AND e.status = :enabled AND u.deleted = 0";
                $params3 = array(
                    'enabled'  => ENROL_INSTANCE_ENABLED,
                    'active'   => ENROL_USER_ACTIVE,
                    'userid'   => $userid,
                    'courseid' => $courseid
                );

                if ($enrolments = $DB->get_records_sql($sql3, $params3)) {
                    $now = time();
                    foreach ($enrolments as $e) {
                        if (!$e->timestart || $e->timestart > $now) {
                            continue;
                        }

                        if ($e->timeend && $e->timeend < $now) {
                            continue;
                        }

                        if (!$timeenrolled || $timenrolled > $e->timestart) {
                            $timeenrolled = $e->timestart;
                        }
                    }
                }

                if (!$timeenrolled) {
                    $timeenrolled = 0;
                }
                // add course completion
                $actcompl = new stdClass();
                $actcompl->userid = $userid;
                $actcompl->course = $courseid;
                $actcompl->timeenrolled = $timeenrolled;
                $DB->insert_record('course_completions', $actcompl);
            } 
            
            // Get course info object
            $info = new completion_info((object)array('id' => $courseid));

            // Setup aggregation
            $overall = $info->get_aggregation_method();
            $activity = $info->get_aggregation_method(COMPLETION_CRITERIA_TYPE_ACTIVITY);
            $prerequisite = $info->get_aggregation_method(COMPLETION_CRITERIA_TYPE_COURSE);
            $role = $info->get_aggregation_method(COMPLETION_CRITERIA_TYPE_ROLE);

            $overall_status = null;
            $activity_status = null;
            $prerequisite_status = null;
            $role_status = null;
            
            // Get latest timecompleted
            $timecompleted = null;

            // Check each of the criteria
            foreach ($completions as $params) {
                $timecompleted = max($timecompleted, $params->timecompleted);

                $completion = new completion_criteria_completion((array)$params, false);

                // Handle aggregation special cases
                if ($params->criteriatype == COMPLETION_CRITERIA_TYPE_ACTIVITY) {
                    completion_status_aggregate($activity, $completion->is_complete(), $activity_status);
                } else if ($params->criteriatype == COMPLETION_CRITERIA_TYPE_COURSE) {
                    completion_status_aggregate($prerequisite, $completion->is_complete(), $prerequisite_status);
                } else if ($params->criteriatype == COMPLETION_CRITERIA_TYPE_ROLE) {
                    completion_status_aggregate($role, $completion->is_complete(), $role_status);
                } else {
                    completion_status_aggregate($overall, $completion->is_complete(), $overall_status);
                }
            }

            // Include role criteria aggregation in overall aggregation
            if ($role_status !== null) {
                completion_status_aggregate($overall, $role_status, $overall_status);
            }

            // Include activity criteria aggregation in overall aggregation
            if ($activity_status !== null) {
                completion_status_aggregate($overall, $activity_status, $overall_status);
            }

            // Include prerequisite criteria aggregation in overall aggregation
            if ($prerequisite_status !== null) {
                completion_status_aggregate($overall, $prerequisite_status, $overall_status);
            }

            // If aggregation status is true, mark course complete for user
            if ($overall_status) {
//                if (debugging()) {
//                    mtrace('Marking complete');
//                }

//                if (!$timecompleted) $timecompleted = time();
                
                $ccompletion = new completion_completion(array('course' => $params->course, 'userid' => $params->userid));
                $ccompletion->mark_complete($timecompleted);
            }
            
    }
    }
    
    function get_mod_detail($id, $type) {
        global $DB, $CFG;
        
        if (!$cm = get_coursemodule_from_id($type, $id)) {
            return '';
        }
        $data = array();
        $activities = $DB->get_record($type, array('id'=>$cm->instance), '*', MUST_EXIST);
        $detail['id'] = $activities->id;
        $detail['name'] = $activities->name;
        
        // get topic name
        $sections = $DB->get_record_sql("SELECT * FROM {$CFG->prefix}course_sections WHERE sequence LIKE '%$id%'");
        $detail['topicname'] = $sections->name;

        $detail['hasgrade'] = is_null($cm->completiongradeitemnumber) ? false : true;
            
        $gradeitem = $DB->get_record('grade_items', array('iteminstance' => $cm->instance, 'courseid'=>$cm->course, 'itemmodule'=>$type));
        $letter_option = array();

        if ($gradeitem->itemmodule == 'quiz') {
            $quiz_detail = $DB->get_record('quiz', array('id'=>$cm->instance));
            $grade_scheme = false;
            if($quiz_detail->reviewmarks != 0) {
                $grade_scheme = true;
                // get grade letter scheme setting
                $grade_letters_scheme = $DB->get_records('quiz_grade_letters', array('moduleid'=>$id));
                if($grade_letters_scheme) {
                    foreach ($grade_letters_scheme as $letter) {
                        $letter_scheme = array();
                        $letter_scheme['value'] = (int) $letter->lowerboundary;
                        $letter_scheme['name'] = $letter->letter;
                        $letter_option[] = $letter_scheme;
                    }
                }
            } 
        } 
        if (empty($letter_option)) {
            $letters = grade_get_letters(null);
            foreach ($letters as $key=>$value) {
                $grade_letter = array();
                $grade_letter['value'] = $key;
                $grade_letter['name'] = $value;
                $letter_option[] = $grade_letter;
            }
        }
        $detail['lettergradeoption'] = $letter_option;
            
        // get eroment of course
        $students = $this->get_course_students($cm->course);
        $studentdetail = [];

        foreach ($students as $student) {
            // Check user role
            $checkLearner = false;
            $user = get_complete_user_data ('id', $student['id']);
            $userRole = $this->get_user_role($cm->course, $user->username);           

            if ($userRole['status']) {
                $userroles = $userRole['roles'];
            } else {
                $userroles = array();
            }
            if (!empty($userroles)) {
                foreach ($userroles as $userrole) {
                    $countrole = count(json_decode($userrole['role']));
                    foreach (json_decode($userrole['role']) as $role) {
                        if ($role->roleid == 5 && $countrole == 1) { // 1 (manager) - 4 (teacher) - 5 (student)
                            $checkLearner = true;
                        }                   
                    }

                }
            }
            if ($checkLearner == false) continue;
                    
            $act_student = array();
            // Check user completed
            $query_com = "SELECT * FROM {$CFG->prefix}course_modules_completion WHERE coursemoduleid = $id AND completionstate = 1 AND userid = ".$student['id'];
            $result_com = $DB->get_records_sql($query_com);
            if (count($result_com) > 0) {
                $act_student['completed'] = true;
                foreach ($result_com as $re) {
                    if ($re->timecompleted) {
                        $act_student['timecompleted'] = $re->timecompleted;
                    } else if ($re->timemodified) {
                        $act_student['timecompleted'] = $re->timemodified;
                    }
                }
            } else {
                if ($type == 'assign') {
                    $assignSubmission = "SELECT * FROM {$CFG->prefix}assign_submission WHERE assignment = $activities->id AND userid = " . $student['id'];
                    $assignSubmissionResult = $DB->get_record_sql($assignSubmission);
                    
                    if ($assignSubmissionResult && $assignSubmissionResult->status == 'submitted') 
                        $act_student['completed'] = true;
                    else 
                        $act_student['completed'] = false;
                } else 
                    $act_student['completed'] = false;
                
                $act_student['timecompleted'] = 0;
            }
            $act_student['username'] = $student['username'];

            // get grade item
            if ($gradeitem) {
                // get grade item object: updated 18 Jul 2016 by KV team
                $grade_item = grade_item::fetch(array('id'=>$gradeitem->id, 'courseid'=>$cm->course));
                $gradedisplaytype = $grade_item->get_displaytype();
                // get final grade
                $grade_grade = $DB->get_record('grade_grades', array('itemid'=>$gradeitem->id, 'userid'=>$student['id']));
                
                // convert grade final number to letters: updated 18 Jul 2016 by KV team
                $grade_letter = grade_format_gradevalue($grade_grade->finalgrade, $grade_item, true, $gradedisplaytype, null);

                $act_student['finalgrade'] = $grade_grade->finalgrade;
                $act_student['grademax'] = $grade_grade->rawgrademax;
                $act_student['finalgradeletter'] = $grade_letter;
                $act_student['gradeid'] = (int) $grade_grade->id;
                $act_student['gradeitemid'] = (int) $gradeitem->id;
                $act_student['feedback'] = $grade_grade->feedback;
                $act_student['hidden'] = $grade_grade->hidden;

                // Assessment( check grade and not grade)
                if ($gradeitem->itemmodule == 'quiz') {
                    if ($act_student['grademax'] == 100)
                        $finalgrade =  $act_student['finalgrade'];
                    else if ($act_student['grademax'] == 10)
                        $finalgrade = $act_student['finalgrade']*10;

                    $quiz_detail = $DB->get_record('quiz', array('id'=>$gradeitem->iteminstance));

                    $grade_scheme = false;
                    $act_student['finalgradeletter'] =  null;
                    // if quiz(assessment turn on grading scheme)
                    if ($quiz_detail->reviewmarks != 0) {
                        $grade_scheme = true;
                        // get grade letter scheme setting
                        $grade_letters_scheme = $DB->get_records('quiz_grade_letters', array('moduleid'=>$id));
                        if ($grade_letters_scheme) {
                            foreach ($grade_letters_scheme as $letter) {
                                if ($finalgrade >= (int) $letter->lowerboundary){
                                    $act_student['finalgradeletter'] =  $letter->letter;
                                    break;
                                }
                                else{
                                    $act_student['finalgradeletter'] =  null;
                                }
                            }
                        }
                    }
                }

                // Scorm and Page( not check grade)
                if ($gradeitem->itemmodule == 'scorm' || $gradeitem->itemmodule == 'page') {
                    $act_student['finalgradeletter'] =  null;
                }

//                $detail['student'] = $act_student;
            } else {
                $act_student['finalgrade'] = null;
                $act_student['grademax'] = null;
                $act_student['finalgradeletter'] = null;
                $act_student['gradeid'] = null;
                $act_student['gradeitemid'] = null;
                $act_student['feedback'] = null;
            }

            $studentdetail[] = $act_student;
        }

        $detail['student'] = $studentdetail;
	    $data[] = $detail;

        return $data;
    }
    
    function grade_detail($id, $gradeitem_id, $username) {
        global $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);
        
        if($id > 0) {
            $grade = $DB->get_record('grade_grades', array('id'=>$id, 'userid'=>$user->id));
        }
        $gradeitem = $DB->get_record('grade_items', array('id' => $gradeitem_id));
        
        $grade_item = grade_item::fetch(array('id'=>$gradeitem->id, 'courseid'=>$gradeitem->courseid));
        $gradedisplaytype = $grade_item->get_displaytype();
        $grade_final_letter = grade_format_gradevalue($grade->finalgrade, $grade_item, true, $gradedisplaytype, null);

        $course_module = $DB->get_record('modules', array('name' => $gradeitem->itemmodule));
        $activity_module = $DB->get_record('course_modules', array('instance' => $gradeitem->iteminstance, 'course'=>$gradeitem->courseid, 'module'=>$course_module->id));
        
        $sql = "SELECT cm.id, COUNT('x') AS numviews, MAX(time) AS lasttime
                          FROM {course_modules} cm
                               JOIN {modules} m ON m.id = cm.module
                               JOIN {log} l     ON l.cmid = cm.id
                         WHERE cm.course = ? AND l.action LIKE 'view%' AND m.visible = 1 AND l.userid = $user->id
                      GROUP BY cm.id";
        $views = $DB->get_records_sql($sql, array($gradeitem->courseid));
        
        $cm = get_coursemodule_from_id(false, $activity_module->id);
        
        $resource['mod_lastaccess'] = '';
        if(isset($views[$cm->id]->lasttime)) {
            /* $timeago = format_time(time() - $views[$cm->id]->lasttime);
            $resource['mod_lastaccess'] = userdate($views[$cm->id]->lasttime)." ($timeago)"; */
            $resource['mod_lastaccess'] = date('d/m/Y',$views[$cm->id]->lasttime);
        }
        $mod_completion = $DB->get_record('course_modules_completion', array('coursemoduleid' => $activity_module->id, 'userid' => $user->id, 'completionstate' => '1'));
        $resource['mod_completion'] = false;
        $resource['mod_completion_date'] = '';
        if ($mod_completion) {
            $resource['mod_completion'] = true;
            $resource['mod_completion_date'] = $mod_completion->timemodified;
        }
        // letter grade option
        // if activity is quiz
        // check quiz have toggle grade for Quiz.
        // Ok, Let's go
        $grade_scheme = true;
        $letter_option = array();
        if($gradeitem->itemmodule == 'quiz') {
            $quiz_detail = $DB->get_record('quiz', array('id'=>$activity_module->instance));
            $grade_scheme = false;
            if($quiz_detail->reviewmarks != 0) {
                $grade_scheme = true;
                // get grade letter scheme setting
                $grade_letters_scheme = $DB->get_records('quiz_grade_letters', array('moduleid'=>$activity_module->id));
                if($grade_letters_scheme) {
                    foreach ($grade_letters_scheme as $letter) {
                        $letter_scheme = array();
                        $letter_scheme['value'] = (int) $letter->lowerboundary;
                        $letter_scheme['name'] = $letter->letter;
                        $letter_option[] = $letter_scheme;
                    }
                }
            } 
        }
        // end
        else {
            $letters = grade_get_letters(null);
            foreach ($letters as $key=>$value) {
                $grade_letter = array();
                $grade_letter['value'] = $key;
                $grade_letter['name'] = $value;
                $letter_option[] = $grade_letter;
            }
        }
        
        $data['id'] = (int) $grade->id;
        $data['gradeitemid'] = $gradeitem->id;
        $data['course'] = $gradeitem->courseid;
        $data['activity_moduleid'] = $cm->id;
        $data['activity_name'] = $gradeitem->itemname;
        $data['mod'] = $gradeitem->itemmodule;
        $data['grade_cheme_setting'] = $grade_scheme;
        $data['grademax'] = $grade->rawgrademax;
        $data['finalgrade'] = $grade->finalgrade;
        $data['finalgradeletter'] = $grade_final_letter;
        $data['feedback'] = $grade->feedback;
        $data['lettergradeoption'] = $letter_option;
        $data['activity_completion'] = $resource;
        return $data;
    }
    
    function get_submission_file($id,$username=false) {
        global $DB, $CFG;
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);
        $activity = $DB->get_record('course_modules', array('id'=>$id));
//        var_dump($activity);die;
        $context = context_module::instance($id);
        // get submission submitted
        if($user)
            $submissions = $DB->get_records('assign_submission', array('assignment'=>$activity->instance,'userid'=> $user->id, 'status'=>'submitted'));
        else
            $submissions = $DB->get_records('assign_submission', array('assignment'=>$activity->instance, 'status'=>'submitted'));
        $return = array();
//        if($submissions) {
//            $fs = get_file_storage();
//            $component = 'assignsubmission_file';
//            $filearea = 'submission_files';
//
//            $filees = Array();
//            foreach ($submissions as $value) {
//                $files = $fs->get_area_files($context->id, $component, $filearea, $value->id);
//                $filees = $filees + $files;
//            }
//            $preview = null;
//            $forcedownload = 1;
//            foreach ($filees as $file) {
//                if($file->get_mimetype()) {
//                $data['filename'] = $file->get_filename();
//                $data['filetype'] = $file->get_mimetype();
//                $data['filepath'] = $CFG->wwwroot . '/pluginfile.php/' . $context->id . '/'.$component.'/'.$filearea.'/'.$file->get_itemid().'/'.$file->get_filename();
//                $return[] = $data;
//                }
//            }
//        }
        if($submissions) {
            $fs = get_file_storage();
            $component = 'assignsubmission_file';
            $filearea = 'submission_files';

            $filees = Array();
            foreach ($submissions as $value) {
                $files = $fs->get_area_files($context->id, $component, $filearea, $value->id);
                $filees = $filees + $files;
            }
            $preview = null;
            $forcedownload = 1;
            foreach ($filees as $file) {
                if($file->get_mimetype()) {
                $data['filename'] = $file->get_filename();
                $data['filetype'] = $file->get_mimetype();
                $filehash = $file->get_contenthash();
                $filepath = $file->get_filedir() . '/' . substr($filehash, 0, 2) . '/' . substr($filehash, 2, 2) . '/' . $filehash;
                $data['filepath'] = $filepath;//$CFG->wwwroot . '/pluginfile.php/' . $context->id . '/'.$component.'/'.$filearea.'/'.$file->get_itemid().'/'.$file->get_filename();
                $return[] = $data;
                }
            }
        }
        return $return;
        
    }
            
    function get_my_grades ($username) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $user = get_complete_user_data ('username', $username);


        if (!$user)
            return array();

        $courses = enrol_get_users_courses ($user->id, true);

        $news = array ();
        foreach ($courses as $c)
        {
            $course_news['remoteid'] = $c->id;
            $course_news['fullname'] = $c->fullname;
            $course_news['fullname'] = format_string ($course_news['fullname']);
            $course_news['grades'] = $this->get_user_grades ($username, $c->id);

            $news[] = $course_news;
        }

        return $news;
    }

    function get_my_grades_by_category ($username) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $user = get_complete_user_data ('username', $username);

        if (!$user)
            return array();

        $courses = enrol_get_users_courses ($user->id, true);

        $news = array ();
        foreach ($courses as $c)
        {
            $course_news['remoteid'] = $c->id;
            $course_news['fullname'] = $c->fullname;
            $course_news['fullname'] = format_string ($course_news['fullname']);
            $course_news['grades'] = $this->get_course_grades_by_category ($c->id, $username);

            $news[] = $course_news;
        }

        return $news;
    }

    function get_my_grade_user_report ($username) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $user = get_complete_user_data ('username', $username);

        if (!$user)
            return array();

        $courses = enrol_get_users_courses ($user->id, true);

        $news = array ();
        foreach ($courses as $c)
        {
            $course_news['remoteid'] = $c->id;
            $course_news['fullname'] = $c->fullname;
            $course_news['fullname'] = format_string ($course_news['fullname']);
            $course_news['grades'] = $this->get_grade_user_report ($c->id, $username);


            $news[] = $course_news;
        }

        return $news;
    }

    function get_grade_user_report ($id, $username) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);

        if (!$user)
            return array();

        $gpr = new grade_plugin_return(array('type'=>'report', 'plugin'=>'user', 'courseid'=>$id, 'userid'=>$user->id));
        $context = context_course::instance($id);
        $report = new grade_report_user($id, $gpr, $context, $user->id);

        // Get course total
        $query =
            "select id 
            from  {$CFG->prefix}grade_items 
            where courseid = '$id' 
            AND itemtype='course'
                ";
        $cat_item = $DB->get_record_sql($query);

        $query = "SELECT g.finalgrade,g.rawgrademax
          FROM {$CFG->prefix}grade_grades g
         WHERE g.itemid = ?
           AND g.userid =  ?";
        $params = array ($cat_item->id, $user->id);

        $grade = $DB->get_record_sql($query, $params);

        $total['fullname'] = '';
        $total['finalgrade'] = (float) $grade->finalgrade;
        $total['grademax'] = (float) $grade->rawgrademax;
        $total['items'] = array();
        $total['letter'] = '';

        if (! $grade_grade = grade_grade::fetch(array('itemid'=>$cat_item->id,'userid'=>$user->id))) {
            $grade_grade = new grade_grade();
            $grade_grade->userid = $user->id;
            $grade_grade->itemid = $cat_item->id;
        }
        $grade_grade->load_grade_item();

        $total['letter'] = grade_format_gradevalue($grade->finalgrade, $grade_grade->grade_item, true, GRADE_DISPLAY_TYPE_LETTER);

        $data = array ();
        $data[] = $total;

        $query =
            "select {$CFG->prefix}grade_categories.fullname, {$CFG->prefix}grade_items.id, {$CFG->prefix}grade_items.grademax, {$CFG->prefix}grade_categories.id
            from {$CFG->prefix}grade_categories, {$CFG->prefix}grade_items 
            where {$CFG->prefix}grade_categories.id = {$CFG->prefix}grade_items.iteminstance 
            and {$CFG->prefix}grade_items.courseid='$id' and itemtype='category';
                ";
        $cats = $DB->get_records_sql($query);

        if (count ($cats) == 0) {
            // No cats, get items  in "main"
            $query =
                "select {$CFG->prefix}grade_categories.fullname, {$CFG->prefix}grade_items.id, {$CFG->prefix}grade_items.grademax, {$CFG->prefix}grade_categories.id
                from {$CFG->prefix}grade_categories, {$CFG->prefix}grade_items 
                where {$CFG->prefix}grade_categories.id = {$CFG->prefix}grade_items.iteminstance 
                and {$CFG->prefix}grade_items.courseid='$id' and itemtype='course';
                    ";
            $cats = $DB->get_records_sql($query);
        }

        foreach ($cats as $r) {
            if ($r->fullname != '?')
                $e['fullname'] = $r->fullname;
            else $e['fullname'] = '';
            $e['grademax'] = (float) $r->grademax;

            $cat_id = $r->id;

            // Get category grade total
            $query =
                "select id 
                from  {$CFG->prefix}grade_items 
                where iteminstance = '$cat_id' 
                    ";
            $cat_item = $DB->get_record_sql($query);

            $query = "SELECT g.finalgrade
              FROM {$CFG->prefix}grade_grades g
             WHERE g.itemid = ?
               AND g.userid =  ?";
            $params = array ($cat_item->id, $user->id);

            $grade = $DB->get_record_sql($query, $params);
            if ($grade)
                $e['finalgrade'] = (float) $grade->finalgrade;
            else $e['finalgrade'] = (float) 0;

            if (! $grade_grade = grade_grade::fetch(array('itemid'=>$cat_item->id,'userid'=>$user->id))) {
                $grade_grade = new grade_grade();
                $grade_grade->userid = $user->id;
                $grade_grade->itemid = $cat_item->id;
            }
            $grade_grade->load_grade_item();

            $e['letter'] = grade_format_gradevalue($grade->finalgrade, $grade_grade->grade_item, true, GRADE_DISPLAY_TYPE_LETTER);

            // Get items

            $query =
                "select * 
                from  {$CFG->prefix}grade_items 
                where categoryId = '$cat_id' 
                    ";

            $items = $DB->get_records_sql($query);
            $category_items = array ();

            if (count ($items) == 0)
                continue;

            foreach ($items as $item) {
                $category_item['name'] = $item->itemname;
                $category_item['grademax'] = $item->grademax;

                switch ($item->itemmodule)
                {
                    case 'quiz':
                        $conditions = array ('id' => $item->iteminstance);
                        $quiz = $DB->get_record('quiz',$conditions);
                        $category_item['due'] = $quiz->timeclose;
                        break;
                    case 'assignment':
                        $conditions = array ('id' => $item->iteminstance);
                        $assignment = $DB->get_record('assignment',$conditions);
                        $category_item['due'] = $assignment->timedue;
                        break;
                    default:
                        $category_item['due'] = 0;
                        break;
                }

                $query = "SELECT g.finalgrade, g.feedback
                  FROM {$CFG->prefix}grade_grades g
                 WHERE g.itemid = ?
                   AND g.userid =  ?";
                $params = array ($item->id, $user->id);

                $grade = $DB->get_record_sql($query, $params);

                if (! $grade_grade = grade_grade::fetch(array('itemid'=>$item->id,'userid'=>$user->id))) {
                    $grade_grade = new grade_grade();
                    $grade_grade->userid = $user->id;
                    $grade_grade->itemid = $item->id;
                }

                $grade_grade->load_grade_item();

                if ($grade)
                {
                    $category_item['finalgrade'] = (float) $grade->finalgrade;
                    $category_item['feedback'] = $grade->feedback;
                    if ($report->showlettergrade)
                        $category_item['letter'] = grade_format_gradevalue($grade->finalgrade, $grade_grade->grade_item, true, GRADE_DISPLAY_TYPE_LETTER);
                    else
                        $category_item['letter'] = '';
                }
                else
                {
                    $category_item['finalgrade'] = (float) 0;
                    $category_item['feedback'] = '';
                    $category_item['letter'] = '';
                }

                $category_items[] = $category_item;
            }

            $e['items'] = $category_items;

            $data[] = $e;
        }

        // don't return total if there is nothing else
        if (count ($data) == 1)
            $rdo['data'] = array();
        else
            $rdo['data'] = $data;

        $rdo['config']['showlettergrade'] = (int) $report->showlettergrade;

        return $rdo;
    }


     function teacher_get_course_grades ($id, $search, $limitfrom = '', $limitnum = '',$completed ='') {
        global $CFG, $DB;
        if ($search)
            $students = $this->get_course_students  ($id,'', $search);
        else if($completed)
             $students = $this->get_course_students  ($id, $completed, '', $limitfrom, $limitnum);
        else 
            $students = $this->get_course_students  ($id,'', '', $limitfrom, $limitnum);
        $course_grades = array ();
        $letters = grade_get_letters(null);

        foreach ($students as $student) {
            $grades = $this->get_course_grades_by_category ($id, $student['username']);
            if($grades[0]['usermodified'] != null){
                $grades[0]['gradeoffacitator'] = true;
            } else {
                $grades[0]['gradeoffacitator'] = false;
            }
            $student['grades'] = $grades;
            $gradereport = $this->get_grade_user_report($id, $student['username']);
            $maxFinalGrade = array();

            if (!empty($grades )) {
                foreach ($grades as $v) {
                    if (isset($v['items']) && !empty($v['items'])) {
                        foreach ($v['items'] as $val) {
                            if ($val['grademax'] == 100) $maxFinalGrade[] = $val['finalgrade'];
                            else if ($val['grademax'] == 10) $maxFinalGrade[] = $val['finalgrade']*10;
                        }
                    }
                }
            }

//            if (!empty($gradereport['data'] )) {
//                foreach ($gradereport['data'] as $v) {
//                    if (isset($v['items']) && !empty($v['items'])) {
//                        foreach ($v['items'] as $val) {
//                            if ($val['grademax'] == 100) $maxFinalGrade[] = $val['finalgrade'];
//                            else if ($val['grademax'] == 10) $maxFinalGrade[] = $val['finalgrade']*10;
//                        }
//                    }
//                }
//            }
            if (empty($maxFinalGrade)) $maxFinalGrade[] = 0;

            $student['overallgrade'] = array_sum($maxFinalGrade)/count($maxFinalGrade);
            $l = array();
            foreach ($letters as $k => $v) {
                if (array_sum($maxFinalGrade)/count($maxFinalGrade) < $k) continue;
                $l[] = $k;
            }
            if (empty($l)) $l[] = 0;
            $student['overallgradeletter'] = $letters[max($l)];

            $user_groups = groups_get_user_groups ($id, $student['id']);
            if (!count ($user_groups[0]))
                $student['group'] = '';
            else {
                $group_id = $user_groups[0][0];
                $student['group'] = groups_get_group_name ($group_id);
            }

            $course_grades[] = $student;
        }


        return $course_grades;
    }
    function teacher_get_group_grades ($course_id, $group_id, $search) {
        $students = $this->get_group_members  ($group_id, $search);

        $course_grades = array ();
        foreach ($students as $student)
        {
            $grades = $this->get_course_grades_by_category ($course_id, $student['username']);
            $student['grades'] = $grades;

            $student['group'] = groups_get_group_name ($group_id);

            $course_grades[] = $student;
        }

        return $course_grades;
    }


    function get_children_grades ($username) {
        $children = $this->get_mentees ($username);

        $cg = array ();
        foreach ($children as $child)
        {
            $data = array ();
            $data['username'] = $child['username'];
            $data['name'] = $child['name'];

            $grades = $this->get_my_grades ($child['username']);
            $data['grades'] = $grades;

            $cg[] = $data;
        }

        return $cg;
    }

    function get_children_grade_user_report ($username) {
        $children = $this->get_mentees ($username);

        $cg = array ();
        foreach ($children as $child)
        {
            $data = array ();
            $data['username'] = $child['username'];
            $data['name'] = $child['name'];

            $grades = $this->get_my_grade_user_report ($child['username']);
            $data['grades'] = $grades;

            $cg[] = $data;
        }

        return $cg;
    }

    /**
     * Returns latest grades
     *
     * @param string $user Username
     * @param int $cid Course identifier
     */
    function get_last_user_grades ($username, $limit) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);
        $uid = $user->id;

        if (!$limit)
            $limit = 1000;


        $SQL = "SELECT distinct(g.itemid), g.finalgrade,gi.courseid,gi.itemname,gi.id, g.timemodified as tm
                      FROM {$CFG->prefix}grade_items gi
                           JOIN {$CFG->prefix}grade_grades g      ON g.itemid = gi.id
                           JOIN {$CFG->prefix}user u              ON u.id = g.userid
                           JOIN {$CFG->prefix}role_assignments ra ON ra.userid = u.id
                     WHERE g.finalgrade IS NOT NULL 
                and gi.itemname IS NOT NULL
               AND u.id = ?
                    ORDER BY tm
            LIMIT $limit";


        $sum_array = array();
        $params = array ($uid);
        if ($sums = $DB->get_records_sql($SQL, $params)) {

            $i = 0;
            foreach ($sums as $sum)
            {
                if (! $grade_grade = grade_grade::fetch(array('itemid'=>$sum->id,'userid'=>$uid))) {
                    $grade_grade = new grade_grade();
                    $grade_grade->userid = $this->user->id;
                    $grade_grade->itemid = $grade_object->id;
                }

                $grade_item = $grade_grade->load_grade_item();

                $scale = $grade_item->load_scale();
                $formatted_grade = grade_format_gradevalue($sum->finalgrade, $grade_item, true, GRADE_DISPLAY_TYPE_REAL);

                $t['itemname']= $sum->itemname;
                $t['finalgrade']= $formatted_grade;
                $t['average']= $this->get_average_grade ($grade_grade->itemid);
                $tareas[] = $t;
                $i++;
            }

            return $tareas;
        }

        return array();
    }
    /**
     * Retursn number of enrolled students in a course
     *
     * @param int $id Course identifier
     */
    function get_course_students_no($id) {
        global $CFG;

        $context = context_course::instance($id);
        /* 5 indica estudiantes (table mdl_role) */
        $alumnos = get_role_users(5 , $context);

        return count($alumnos);
    }

    function get_course_students($id, $completed = '', $search = '', $limitfrom = '', $limitnum = '') {
        global $CFG,$DB;

        $context = context_course::instance($id);
        /* 5 indica estudiantes (table mdl_role) */
        $sort = ' firstname ASC, lastname ASC ';
        $alumnos = get_role_users(5 , $context, false, '', $sort, true, '', $limitfrom, $limitnum);
        // check role manage and facilitator
        $teachers = get_role_users(4 , $context, false, '', $sort, true, '', $limitfrom, $limitnum);
        $manages = get_role_users(1 , $context, false, '', $sort, true, '', $limitfrom, $limitnum);

        $query = "SELECT userid FROM {$CFG->prefix}course_completions WHERE status = 50 AND course = $id";
        $complete = $DB->get_records_sql($query);


        $coursecompleted = array();
        foreach($complete as $comp){
            $coursecompleted[] = $comp->userid;
        }
//        return ($coursecompleted);

        $idteaches = array ();
        foreach ($teachers as $alumno){
            $idteaches[] = $alumno->id;
        }
        $idmanages = array ();
        foreach ($manages as $alumno){
            $idmanages[] = $alumno->id;
        }

        $students = array ();

        foreach ($alumnos as $alumno)
        {
            if ($search)
            {
                if ( (stripos ($alumno->username, $search) === false)
                    && ( stripos ($alumno->firstname, $search) === false)
                    && (stripos ($alumno->lastname, $search) === false)
                    && (stripos ($alumno->idnumber, $search) === false)
                )
                    continue;
            }
            if($completed == "complete"){

                if(in_array($alumno->id,$coursecompleted)){
                    $a['username'] = $alumno->username;
                    $a['firstname'] = $alumno->firstname;
                    $a['lastname'] = $alumno->lastname;
                    $a['id'] = $alumno->id;
                    if(in_array($a['id'],$idteaches) || in_array($a['id'],$idmanages)) continue;
                    $students[] = $a;
                }
            }
            else if($completed == "notcomplete"){

                if(!in_array($alumno->id,$coursecompleted)){
                    $a['username'] = $alumno->username;
                    $a['firstname'] = $alumno->firstname;
                    $a['lastname'] = $alumno->lastname;
                    $a['id'] = $alumno->id;
                    if(in_array($a['id'],$idteaches) || in_array($a['id'],$idmanages)) continue;
                    $students[] = $a;
                }
            }
            else{
                $a['username'] = $alumno->username;
                $a['firstname'] = $alumno->firstname;
                $a['lastname'] = $alumno->lastname;
                $a['id'] = $alumno->id;
                if(in_array($a['id'],$idteaches) || in_array($a['id'],$idmanages)) continue;
                $students[] = $a;
            }
//            $students[] = $a;

        }

        return ($students);
    }
    
    /**
     * Returns upcoming events for a course
     *
     * @param int $id Course identifier
     */
    function get_upcoming_events ($id, $username = '') {
        global $CFG;

        $id = addslashes ($id);
        $courseshown = $id;
        $filtercourse    = array($courseshown => $id);
        $groupeventsfrom = array($courseshown => 1);

        $true = true;
        if ($CFG->version >= 2011070100)
        {
            list($courses, $group, $user) = calendar_set_filters($filtercourse, $true);
            $courses = array ($id => $id);

            if ($username != '')
            {
                // Show only events for groups where user is a member
                $groups = groups_get_all_groups($id);
                $gs = array ();
                foreach ($groups as $group)
                {
                    $found = false;
                    // Check is user is a member of the group
                    $members = $this->get_group_members ($group->id);
                    foreach ($members as $member)
                    {
                        if ($member['username'] == $username)
                        {
                            $found = true;
                            break;
                        }
                    }
                    if ($found)
                        $gs[$group->id] = $group->id;
                }
            }

            $events = calendar_get_upcoming($courses, $gs, true,
                CALENDAR_DEFAULT_UPCOMING_LOOKAHEAD,
                CALENDAR_DEFAULT_UPCOMING_MAXEVENTS);
        }
        else
        {
            calendar_set_filters($courses, $group, $user, $filtercourse, $groupeventsfrom, true);
            $events = calendar_get_upcoming($courses, $group, $user,
                CALENDAR_UPCOMING_DAYS,
                CALENDAR_UPCOMING_MAXEVENTS);
        }

        $data = array ();
        foreach ($events as $r)
        {
            $e['name'] = $r->name;
            $e['timestart'] = $r->timestart;
            $e['courseid'] = $r->courseid;

            $data[$i] = $e;
            $i++;
        }

        return $data;
    }
    /**
     * Returns last news for a course
     *
     * @param int $id Course identifier
     */
    function get_news_items ($id) {
        global $CFG, $DB;

        $conditions = array ('id' => $id);
        $COURSE = $DB->get_record('course', $conditions);

        if (!$forum = forum_get_course_forum($COURSE->id, 'news')) {
            return array ();
        }

        $modinfo = get_fast_modinfo($COURSE);
        if (empty($modinfo->instances['forum'][$forum->id])) {
            return array ();
        }
        $cm = $modinfo->instances['forum'][$forum->id];


        /// Get all the recent discussions we're allowed to see

        if (! $discussions = forum_get_discussions($cm, 'p.modified DESC', false, -1, $COURSE->newsitems) )
        {
            return array ();
        }

        $data = array ();
        foreach ($discussions as $r)
        {
            $e['discussion'] = $r->discussion;
            $e['subject'] = $r->subject;
            $e['timemodified'] = $r->timemodified;

            $data[$i] = $e;
            $i++;
        }

        return $data;
    }

    function get_my_news ($username) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $user = get_complete_user_data ('username', $username);

        if (!$user)
            return array();

        $courses = enrol_get_users_courses ($user->id, true);

        $news = array ();
        foreach ($courses as $c)
        {
            $course_news['remoteid'] = $c->id;
            $course_news['fullname'] = $c->fullname;
            $course_news['news'] = $this->get_news_items ($c->id);

            $news[] = $course_news;
        }

        return $news;
    }



    /**
     * Returns daily stats for a course
     *
     * @param int $id Course identifier
     */
    function get_course_daily_stats ($id) {
        global $CFG, $DB;
        $id = addslashes ($id);

        $query =
            "select * from {$CFG->prefix}stats_daily 
        where courseid='$id'
        and roleid='5'
        and stattype='activity';
                ";

        $stats = $DB->get_records_sql($query);

        $data = array ();
        if ($stats)
        {
            foreach ($stats as $r)
            {
                $e['stat1'] = $r->stat1;
                $e['stat2'] = $r->stat2;
                $e['timeend'] = $r->timeend;

                $data[$i] = $e;
                $i++;
            }
        }
        else
        {
            $e['stat1'] = 0;
            $e['stat2'] = 0;
            $e['timeend'] = 0;

            $data[] = $e;
        }

        return $data;
    }

    /**
     * Return access daily stats
     *
     */
    function get_site_last_week_stats () {
        global $CFG, $DB;

        $query =
            "select * from {$CFG->prefix}stats_weekly
        where stattype='logins' 
        order by timeend DESC LIMIT 1;
                ";

        $stats = $DB->get_records_sql($query);

        $data = array ();
        if ($stats)
        {
            foreach ($stats as $r)
            {
                $e['stat1'] = $r->stat1;
                $e['stat2'] = $r->stat2;

                $data[$i] = $e;
                $i++;
            }
        }
        else
        {
            $e['stat1'] = 0;
            $e['stat2'] = 0;

            $data[] = $e;
        }

        return $data;
    }

    /**
     * Returns grading system for a course
     *
     * @param int $id Course identifier
     */
    function get_course_grade_categories ($id) {
        global $CFG, $DB;
        $query =
            "select {$CFG->prefix}grade_categories.fullname, {$CFG->prefix}grade_items.grademin, {$CFG->prefix}grade_items.grademax, {$CFG->prefix}grade_categories.id
            from {$CFG->prefix}grade_categories, {$CFG->prefix}grade_items 
            where {$CFG->prefix}grade_categories.id = {$CFG->prefix}grade_items.iteminstance 
            and {$CFG->prefix}grade_items.courseid='$id' and itemtype='category';
                ";

        $cats = $DB->get_records_sql($query);

        $data = array ();
        foreach ($cats as $r)
        {
            $e['fullname'] = $r->fullname;
            $e['grademax'] = $r->grademax;
            $e['id'] = $r->id;

            $data[] = $e;
        }

        return $data;
    }

    function get_course_grade_categories_and_items ($id) {
        global $CFG, $DB;
        $query =
            "select {$CFG->prefix}grade_categories.fullname, {$CFG->prefix}grade_items.id, {$CFG->prefix}grade_items.grademin, {$CFG->prefix}grade_items.grademax, {$CFG->prefix}grade_categories.id
            from {$CFG->prefix}grade_categories, {$CFG->prefix}grade_items 
            where {$CFG->prefix}grade_categories.id = {$CFG->prefix}grade_items.iteminstance 
            and {$CFG->prefix}grade_items.courseid='$id' and itemtype='category';
                ";

        $cats = $DB->get_records_sql($query);

        $data = array ();
        foreach ($cats as $r)
        {
            $e['fullname'] = $r->fullname;
            $e['grademax'] = $r->grademax;

            $cat_id = $r->id;
            $query =
                "select * 
                from  {$CFG->prefix}grade_items 
                where categoryId = '$cat_id' 
                    ";

            $items = $DB->get_records_sql($query);
            $category_items = array ();
            foreach ($items as $item)
            {
                $category_item['name'] = $item->itemname;

                $category_item['id'] = $item->id;
                $rubrics = $this->get_rubrics ($item->id);
                if (count ($rubrics['definitions']))
                    $category_item['has_rubrics'] = true;
                else
                    $category_item['has_rubrics'] = false;


                switch ($item->itemmodule)
                {
                    case '':
                        if ($item->itemtype == 'manual')
                            $category_item['due'] = $item->locktime;
                        break;
                    case 'quiz':
                        $conditions = array ('id' => $item->iteminstance);
                        $quiz = $DB->get_record('quiz',$conditions);
                        $category_item['due'] = $quiz->timeclose;
                        break;
                    case 'assignment':
                        $conditions = array ('id' => $item->iteminstance);
                        $assignment = $DB->get_record('assignment',$conditions);
                        $category_item['due'] = $assignment->timedue;
                        break;
                    case 'assign':
                        $conditions = array ('id' => $item->iteminstance);
                        $assignment = $DB->get_record('assign',$conditions);
                        $category_item['due'] = $assignment->duedate;
                        break;
                    case 'scorm':
                        $conditions = array ('id' => $item->iteminstance);
                        $assignment = $DB->get_record('scorm',$conditions);
                        $category_item['due'] = $assignment->timeclose;
                        break;
                    case 'forum':
                        $conditions = array ('id' => $item->iteminstance);
                        $assignment = $DB->get_record('forum',$conditions);
                        $category_item['due'] = $assignment->assesstimefinish;
                        break;
                    case 'lesson':
                        $conditions = array ('id' => $item->iteminstance);
                        $assignment = $DB->get_record('lesson',$conditions);
                        $category_item['due'] = $assignment->deadline;
                        break;
                    case 'turnitintool':
                        $conditions = array ('id' => $item->iteminstance);
                        $assignment = $DB->get_record('turnitintool',$conditions);
                        $category_item['due'] = $assignment->defaultdtdue;
                        break;
                    case 'bigbluebuttonbn':
                        $conditions = array ('id' => $item->iteminstance);
                        $assignment = $DB->get_record('bigbluebuttonbn',$conditions);
                        $category_item['due'] = $assignment->timedue;
                        break;
                    default:
                        $category_item['due'] = 0;
                        break;
                }

                $category_items[] = $category_item;
            }

            $e['items'] = $category_items;

            $data[] = $e;
        }

        return $data;
    }

    function get_rubrics ($grade_item_id) {
        global $CFG, $DB;

        $conditions = array ('id' => $grade_item_id);
        $grade_item = $DB->get_record('grade_items',$conditions);

        $assign_data['assign_name'] = $grade_item->itemname;
        $assign_data['definitions'] = array();

//        $conditions = array ('id' => $grade_item->idnumber);
//        $cm = $DB->get_record('course_modules',$conditions);

        $conditions = array ('name' => $grade_item->itemmodule);
        $module = $DB->get_record('modules',$conditions);
        if (!$module)
            return $assign_data;

        $conditions = array ('course' => $grade_item->courseid, 'module' => $module->id, 'instance' => $grade_item->iteminstance);
        $cm = $DB->get_record('course_modules',$conditions);

        if (!$cm)
            return $assign_data;

        $context = context_module::instance($cm->id);

        $conditions = array ('contextid' => $context->id);
        $area = $DB->get_record('grading_areas',$conditions);

        if (!$area)
            return $assign_data;

        $conditions = array ('areaid' => $area->id);
        $definitions = $DB->get_records('grading_definitions',$conditions);

        $data = array ();
        foreach ($definitions as $definition)
        {
            $d['definition'] = $definition->name;
            $conditions = array ('definitionid' => $definition->id);
            $criteria = $DB->get_records('gradingform_rubric_criteria',$conditions);

            $d['criteria'] = array ();
            foreach ($criteria as $c)
            {
                $data_criteria['description'] = $c->description;

                $conditions = array ('criterionid' => $c->id);
                $levels = $DB->get_records('gradingform_rubric_levels',$conditions);

                $data_levels = array ();
                foreach ($levels as $level)
                {
                    $dl['definition'] = $level->definition;
                    $dl['score'] = $level->score;

                    $data_levels[] = $dl;
                }
                $data_criteria['levels'] = $data_levels;


                $d['criteria'][] = $data_criteria;
            }

            $data[]  = $d;
        }

        $assign_data['definitions'] = $data;

        return $assign_data;
    }



    function user_exists ($username) {
        global $CFG, $DB;
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);
        if ($user)
            return 1;
        return 0;
    }

    function create_joomdle_user_additional ($username, $app) {
        global $CFG, $DB;

        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);
        if (!$user)
            return 0;

        return $this->create_joomdle_user ($username, $app);
    }

    /**
     * Creates a new Joomdle user
     * XXX Also used to update user profile if the user already exists
     *
     * @param string $username Joomla username
     */
    function create_joomdle_user ($username, $app = '') {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        /* Creamos el nuevo usuario de Moodle si no está creado */
        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);
        if (!$user)
            $user = create_user_record($username, "", "joomdle");

        /* Obtenemos la información del usuario en Joomla */
        $juser_info = $this->call_method ("getUserInfo", $username, $app);

        if (array_key_exists ('email', $juser_info))
            $email = $juser_info['email'];
        else  $email = '';
        if (array_key_exists ('firstname', $juser_info))
            $firstname = $juser_info['firstname'];
        else  $firstname = '';
        if (array_key_exists ('lastname', $juser_info) && $juser_info['lastname'] != '')
            $lastname = $juser_info['lastname'];
        else  $lastname = $firstname;
        if (array_key_exists ('city', $juser_info))
            $city = $juser_info['city'];
        else  $city = 'Singapore';
        if (array_key_exists ('country', $juser_info))
            $country = $juser_info['country'];
        else  $country = 'SG';
        if (array_key_exists ('lang', $juser_info))
            $lang = $juser_info['lang'];
        else  $lang = '';
        if (array_key_exists ('timezone', $juser_info))
            $timezone = $juser_info['timezone'];
        else  $timezone = '';
        if (array_key_exists ('phone1', $juser_info))
            $phone1 = $juser_info['phone1'];
        else  $phone1 = '';
        if (array_key_exists ('phone2', $juser_info))
            $phone2 = $juser_info['phone2'];
        else  $phone2 = '';
        if (array_key_exists ('address', $juser_info))
            $address = $juser_info['address'];
        else  $address = '';
        if (array_key_exists ('description', $juser_info))
            $description = $juser_info['description'];
        else  $description = '';
        if (array_key_exists ('institution', $juser_info))
            $institution = $juser_info['institution'];
        else  $institution = '';
        if (array_key_exists ('url', $juser_info))
            $url = $juser_info['url'];
        else  $url = '';

        if (array_key_exists ('icq', $juser_info))
            $icq = $juser_info['icq'];
        else  $icq = '';
        if (array_key_exists ('skype', $juser_info))
            $skype = $juser_info['skype'];
        else  $skype = '';
        if (array_key_exists ('aim', $juser_info))
            $aim = $juser_info['aim'];
        else  $aim = '';
        if (array_key_exists ('yahoo', $juser_info))
            $yahoo = $juser_info['yahoo'];
        else  $yahoo = '';
        if (array_key_exists ('msn', $juser_info))
            $msn = $juser_info['msn'];
        else  $msn = '';
        if (array_key_exists ('idnumber', $juser_info))
            $idnumber = $juser_info['idnumber'];
        else  $idnumber = '';
        if (array_key_exists ('department', $juser_info))
            $department = $juser_info['department'];
        else  $department = '';

        //XXX Maybe this can be optimized for a single DB call...$bool = update_record('user', addslashes_recursive($localuser)); en ment/aut.php
        if (!xmlrpc_is_fault($juser_info)) {

            /* Actualizamos la informacion del usuario recien creado con los datos de Joomla */
            $conditions = array ('id' => $user->id);
            if ($firstname)
                $DB->set_field('user', 'firstname', $firstname, $conditions);
            if ($lastname)
                $DB->set_field('user', 'lastname', $lastname, $conditions);
            if ($email)
                $DB->set_field('user', 'email', $email, $conditions);

            /* Set first access as now */
            $DB->set_field('user', 'firstaccess', time (), $conditions);
            /* Optional data in Joomla, only fill if has a value */
            if ($city)
                $DB->set_field('user', 'city', ($city), $conditions);
            if ($country)
                $DB->set_field('user', 'country', substr ($country, 0, 2), $conditions);
            //  $DB->set_field('user', 'country', $country, $conditions);
            if ($lang)
                $DB->set_field('user', 'lang', $lang, $conditions);
            if ($timezone)
                $DB->set_field('user', 'timezone', $timezone, $conditions);
            if ($phone1)
                $DB->set_field('user', 'phone1', ($phone1), $conditions);
            if ($phone2)
                $DB->set_field('user', 'phone2', ($phone2), $conditions);
            if ($address)
                $DB->set_field('user', 'address', ($address), $conditions);
            if ($description)
                $DB->set_field('user', 'description', ($description), $conditions);
            if ($institution)
                $DB->set_field('user', 'institution', ($institution), $conditions);
            if ($url)
                $DB->set_field('user', 'url', $url, $conditions);
            if ($icq)
                $DB->set_field('user', 'icq', $icq, $conditions);
            if ($skype)
                $DB->set_field('user', 'skype', $skype, $conditions);
            if ($aim)
                $DB->set_field('user', 'aim', $aim, $conditions);
            if ($yahoo)
                $DB->set_field('user', 'yahoo', $yahoo, $conditions);
            if ($msn)
                $DB->set_field('user', 'msn', $msn, $conditions);
            if ($idnumber)
                $DB->set_field('user', 'idnumber', $idnumber, $conditions);
            if ($department)
                $DB->set_field('user', 'department', $department, $conditions);
        }

        /* Get user pic */
        if ((array_key_exists ('pic_url', $juser_info)) && ($juser_info['pic_url']))
        {
            if ($juser_info['pic_url'] != 'none')
            {
                $joomla_url = get_config ('auth/joomdle', 'joomla_url');
                $pic_url = $joomla_url.'/'.$juser_info['pic_url'];
                //$pic = @file_get_contents ($pic_url, false, NULL);
                $pic = $this->get_file ($pic_url);
                if ($pic)
                {
                    //$pic = file_get_contents ($pic_url, false, NULL);
                    $pic = $this->get_file_curl ($pic_url);
                    $tmp_file = $CFG->dataroot.'/temp/'.'tmp_pic';
                    file_put_contents ($tmp_file, $pic);

                    $context = context_user::instance($user->id);
                    process_new_icon($context, 'user', 'icon', 0, $tmp_file);

                    $conditions = array ('id' => $user->id);
                    $DB->set_field('user', 'picture', 1, $conditions);
                }
            }
        }


        /* Custom fields */
        if ($fields = $DB->get_records('user_info_field')) {
            foreach ($fields as $field)
            {
                if ((array_key_exists ('cf_'.$field->id, $juser_info)) && ($juser_info['cf_'.$field->id]))
                {
                    $data = new stdClass();
                    $data->fieldid = $field->id;
                    $data->data = $juser_info['cf_'.$field->id];
                    $data->userid = $user->id;
                    /* update custom field */
                    if ($dataid = $DB->get_field('user_info_data', 'id', array('userid'=>$user->id, 'fieldid'=>$data->fieldid)))
                    {
                        $data->id = $dataid;
                        $DB->update_record('user_info_data', $data);
                    } else {
                        $DB->insert_record('user_info_data', $data);
                    }

                }
            }
        }
        $this->create_user_category($user->username);

        return 1;
    }
    
    function create_user_category($username) {
        global $DB;
        
        $user = get_complete_user_data ('username', $username);
        
        // get default category main of BLN
        $moodleCategoryId = $this->call_method('getBLNMoodleCategoryId', $username);      
        if (count($moodleCategoryId) <= 0) {
            $result['status'] = 0;
            $result['message'] = 'Category not found.';
            $result['roles'] = array();
            return $result;
        }
        $categories_default = $moodleCategoryId[0];
//        // get default category main of PX
//        if(!$categories_default_is = $DB->record_exists('course_categories', array('name' => 'Miscellaneous'))) {
//            $result['status'] = 0;
//            $result['message'] = 'Category not found.';
//            $result['roles'] = array();
//            return $result;
//        }
//        $categories_default = $DB->get_record('course_categories', array('name' => 'Miscellaneous'));
        
        
        $category = $DB->get_record('course_categories', array('id'=>(int)$categories_default), '*', MUST_EXIST);
        if($DB->record_exists('course_categories', array('name' => $user->username))) {
            return;
        }
        $cat_data['name'] = $user->username;
        $cat_data['parent'] = $category->id;
        $cat_data['idnumber'] = '';
        $cat_data['description'] = "Normal category for ".$user->username;
        $cat_data['descriptionformat'] = 1;
        $cat_data['userowner'] = $user->id;
        
        $subcategory = coursecat::create($cat_data);
        $catcontext = context_coursecat::instance($subcategory->id);
        // assign default role course creator in category for all user
        $role = 2;
        role_assign($role, $user->id, $catcontext);
        return;
    }
    
    function create_moodle_only_user ($user_data) {
        global $CFG, $DB;

        $username = $user_data['username'];
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        /* Creamos el nuevo usuario de Moodle si no está creado */
        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);
        if (!$user)
            $user = create_user_record($username, "", "manual");

        if (array_key_exists ('password', $user_data))
            $password = utf8_decode ($user_data['password']);
        else  $password = '';
        if (array_key_exists ('email', $user_data))
            $email = utf8_decode ($user_data['email']);
        else  $email = '';
        if (array_key_exists ('firstname', $user_data))
            $firstname = utf8_decode ($user_data['firstname']);
        else  $firstname = '';
        if (array_key_exists ('lastname', $user_data))
            $lastname = utf8_decode ($user_data['lastname']);
        else  $lastname = '';
        if (array_key_exists ('city', $user_data))
            $city = utf8_decode ($user_data['city']);
        else  $city = '';
        if (array_key_exists ('country', $user_data))
            $country = utf8_decode ($user_data['country']);
        else  $country = '';

        $conditions = array ('id' => $user->id);
        if ($firstname)
            $DB->set_field('user', 'firstname', $firstname, $conditions);
        if ($lastname)
            $DB->set_field('user', 'lastname', $lastname, $conditions);
        if ($email)
            $DB->set_field('user', 'email', $email, $conditions);
        if ($city)
            $DB->set_field('user', 'city', ($city), $conditions);
        if ($country)
            $DB->set_field('user', 'country', $country, $conditions);

        update_internal_user_password($user, $password);

    }

    function search_courses($text, $sortby = 'created', $listcourse = 0, $username = '', $limit, $position = 0)
    {
        global $CFG, $DB;
        $text = utf8_decode($text);
        $str = preg_replace('/\s+/', '', $text);
        $text_search_have_space = '\'%' . $text . '%\'';
        $text_search_dont_have_space = '\'%' . $str . '%\'';
        $wheres2 = array();
        $wheres2[] = 'co.fullname LIKE ' . $text_search_have_space;
        $wheres2[] = 'co.summary LIKE ' . $text_search_have_space;
        $wheres2[] = 'co.learningoutcomes LIKE ' . $text_search_have_space;
        $wheres2[] = 'co.targetaudience LIKE ' . $text_search_have_space;
        if($text_search_dont_have_space != $text_search_have_space) {
            $wheres2[] = 'co.fullname LIKE ' . $text_search_dont_have_space;
            $wheres2[] = 'co.summary LIKE ' . $text_search_dont_have_space;
            $wheres2[] = 'co.learningoutcomes LIKE ' . $text_search_dont_have_space;
            $wheres2[] = 'co.targetaudience LIKE ' . $text_search_have_space;
        }
        $wheres = implode(' OR ', $wheres2);
        $data = array();
        $fs = get_file_storage();
        
        $user = get_complete_user_data('username', $username);
        $courses = enrol_get_users_courses($user->id, true);

        $my_courses = array();
        foreach ($courses as $course) {
            $my_courses[] = $course->id;
        }

        if ($position == 0)
            $position = 0;
        $query =
            "SELECT
            co.id          AS remoteid,
            co.fullname,
            co.summary,
            co.learningoutcomes,
            co.targetaudience,
            co.duration,
            co.creator
            FROM
                            {$CFG->prefix}course co
            WHERE         
                co.id IN ($listcourse) AND ( $wheres )"
            . " LIMIT  $position, $limit"; 

        $records = $DB->get_records_sql($query);
        foreach ($records as $record) {
            $c = get_object_vars($record);
            $c['fullname'] = format_string($c['fullname']);
            $c['summary'] = strip_tags(format_string($c['summary'], true));
            if (textlib::strlen($c['summary']) > 68) {
                $c['summary'] = textlib::substr($c['summary'], 0, 68) . '...';
            }
            $c['completion_status'] = $c['completion_status'];
            $c['time_completed'] = $c['time_completed'];
            $c['last_access'] = $c['last_access'];
            $c['timestart'] = $c['timestart'];
            $c['timeend'] = $c['timeend'];
            $c['enrol_type'] = (string) $c['enrol_type'];
                        
            $enrol_methods = enrol_get_instances($c['remoteid'], true);
            $in = true;
            foreach ($enrol_methods as $instance)
            {

                    // Self-enrolment
                    if ($instance->enrol == 'self')
                        $c['self_enrolment'] = 1;
                    else
                        $c['self_enrolment'] = 0;
            }
        //                // Enroll
            $c['isLearner'] = false;
            $countlearner = 0;
            $userRole = $this->get_user_role($record->remoteid, $username);

            if ($userRole['status']) {
                $userroles = $userRole['roles'];
            } else {
                $userroles = array();
            }
            $isLearner = true;
            if (!empty($userroles)) {
                foreach ($userroles as $userrole) {
                    foreach (json_decode($userrole['role']) as $role) {
                        // roleid = 5 (student)
                        // roleid = 4 (teacher)
                        if ($role->roleid == 5) {
                            $countlearner++;
                            $c['isLearner'] = true;
                        }
                        if(in_array($role->roleid, array('4', '5')) !== false) {
                            $isLearner = false;
                        }
                    }
                }
            }
            $tempUserArr = array();
            if (!array_key_exists($c['creator'], $tempUserArr)) {
                $user_creator = $DB->get_record('user', array('id'=>$c['creator']), 'id, username, firstname, lastname');
                $tempUserArr[$c['creator']] = $user_creator->username;
            }
            $c['creator'] = $tempUserArr[$c['creator']];
            $c['enroled'] = 0;
            if ($username) {
                if (in_array($c['remoteid'], $my_courses))
                    $c['enroled'] = 1;
            }
            $context_course = context_course::instance($c['remoteid']);
            $files = $fs->get_area_files($context_course->id, 'course', 'overviewfiles', 0);
            if (count($files) > 0) {
                foreach ($files as $file) {
                    $c['courseimg'] = $CFG->wwwroot . '/pluginfile.php/' . $context_course->id . '/course/overviewfiles/'.$file->get_filename();
                }
            } else {
                $c['courseimg'] = $CFG->wwwroot . '/theme/parenthexis/pix/nocourseimg.jpg';
            }

            $c['coursetype'] = 'single_course';
            if ($username) {
                $c['isFacilitator'] = $this->checkFacilitator($c['remoteid'], $username);
            }

            $data[] = $c;
        }
            
        $query2 =
            "SELECT
            COUNT(*) as total
            FROM
                {$CFG->prefix}course co
            WHERE         
                co.id IN ($listcourse) AND ( $wheres )";
        $totalcourse = $DB->get_record_sql($query2);       
        
        $response = array();
        $response['status']  = true;
        $response['total_course'] = $totalcourse->total;
        $response['courses']  = $data;
        return $response;
    }
    function search_categories ($text, $phrase, $ordering, $limit) {
        global $CFG, $DB;

        $wheres = array();
        switch ($phrase)
        {
            case 'exact':
                $text           = '\'%'.$text.'%\'';
                $wheres2        = array();
                $wheres2[]      = 'ca.name LIKE '.$text;
                $wheres2[]      = 'ca.description LIKE '.$text;
                $where          = '(' . implode( ') OR (', $wheres2 ) . ')';
                break;

            case 'all':
            case 'any':
            default:
                $words = explode( ' ', $text );
                $wheres = array();
                foreach ($words as $word) {
                    $word           = '\'%'.$word.'%\'';
                    $wheres2        = array();
                    $wheres2[]      = 'ca.name LIKE '.$word;
                    $wheres2[]      = 'ca.description LIKE '.$word;
                    $wheres[]       = implode( ' OR ', $wheres2 );
                }
                $where = '(' . implode( ($phrase == 'all' ? ') AND (' : ') OR ('), $wheres ) . ')';
                break;
        }

        switch ( $ordering )
        {
            case 'alpha':
            case 'category':
                $order = 'ca.name ASC';
                break;
            case 'newest':
            case 'oldest':
            case 'popular':
            default:
                $order = 'ca.name DESC';
        }

        $query =
            "SELECT
        ca.id          AS cat_id,
        ca.name        AS cat_name,
        ca.description AS cat_description
        FROM
        {$CFG->prefix}course_categories ca
        WHERE
        $where
        ORDER BY
        $order
        LIMIT $limit
        ";

        $results =  $DB->get_records_sql($query);

        $options['noclean'] = true;
        $data = array ();
        foreach ($results as $r)
        {
            $c = get_object_vars ($r);
            $c['cat_name'] = format_string($c['cat_name']);
            $c['cat_description'] = format_text($c['cat_description'], FORMAT_MOODLE, $options);
            $data[] = $c;
        }

        return $data;

    }

    function search_topics ($text, $phrase, $ordering, $limit = 50) {
        global $CFG, $DB;

        $wheres = array();
        switch ($phrase) {
            case 'exact':
                $text           = '\'%'.$text.'%\'';
                $where      = 'cs.summary LIKE '.$text;
                break;

            case 'all':
            case 'any':
            default:
                $words = explode( ' ', $text );
                $wheres = array();
                foreach ($words as $word)
                {
                    $word           = '\'%'.$word.'%\'';
                    $wheres[]      = 'cs.summary LIKE '.$word;
                }
                $where = '(' . implode( ($phrase == 'all' ? ') AND (' : ') OR ('), $wheres ) . ')';
                break;
        }
        $where .= " and cs.visible = 1";

        switch ( $ordering ) {
            case 'alpha':
                $order = 'cs.summary ASC';
                break;
            case 'category':
                $order = 'co.id ASC';
                break;
            case 'newest':
                $order = 'co.id ASC, cs.section DESC';
                break;
            case 'oldest':
                $order = 'co.id ASC, cs.section ASC';
                break;
            case 'popular':
            default:
                $order = 'cs.summary DESC';
        }

        /* REMEMBER: For get_records_sql First field in query must be UNIQUE!!!!! */
        $query =
            "SELECT cs.id,
        co.id          AS remoteid,
        co.fullname,
        cs.course,
        cs.section,
        cs.summary,
        ca.id as cat_id,
        ca.name as cat_name
        FROM
        {$CFG->prefix}course_sections cs 
    JOIN {$CFG->prefix}course co  ON
        co.id = cs.course 
    LEFT JOIN {$CFG->prefix}course_categories ca  ON
        ca.id = co.category
        WHERE
        $where
        ORDER BY
        $order
        LIMIT $limit
        ";

        $results =  $DB->get_records_sql($query);

        $options['noclean'] = true;
        $data = array ();
        foreach ($results as $r)
        {
            $c = get_object_vars ($r);
            $c['fullname'] = format_string($c['fullname']);
            $c['summary'] = format_text($c['summary'], FORMAT_MOODLE, $options);
            $c['cat_name'] = format_string($c['cat_name']);
            $data[] = $c;
        }

        return $data;

    }

// $courses: course idnumbers separated by commas
// $groups: group names separated by commas
    function multiple_enrol_and_addtogroup ($username, $courses, $groups, $roleid = 5) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);

        $c = explode (',', $courses);
        $g = explode (',', $groups);
        foreach ($c as $idnumber)
        {
            //   $conditions = array ('idnumber' => $idnumber);
            //   $course = $DB->get_record ('course', $conditions);

            $query = "SELECT idnumber, id
                        FROM {$CFG->prefix}course
                        WHERE idnumber LIKE '%$idnumber%'";

            $records = $DB->get_records_sql($query);

            $group_name = array_shift ($g);

            foreach ($records as $r)
            {
                $codes = explode (',', $r->idnumber);

                foreach ($codes as $code)
                {
                    $code = trim ($code);
                    if ($code == $idnumber)
                    {
                        $conditions = array ('id' => $r->id);
                        $course = $DB->get_record ('course', $conditions);


                        if (!$course)
                            continue;

                        $this->enrol_user_change_role ($username, $course->id, $roleid);

                        // Group

                        // If user already in a group, do nothing
                        $user_groups = groups_get_user_groups ($course->id, $user->id);
                        if (count ($user_groups[0]) > 0)  // Already in a group
                            continue;

                        // add first char from course code
                        $char = substr ($code, 0, 1);
                        $modified_group_name = $char.$group_name;
                        $conditions = array ('name' => $modified_group_name, 'courseid' => $course->id);
                        $group = $DB->get_record ('groups', $conditions);

                        if (!$group)
                        {
                            // Create group if it does not exist

                            $data->courseid = $course->id;
                            $data->name = $modified_group_name;

                            groups_create_group ($data);
                        }

                        $conditions = array ('name' => $modified_group_name, 'courseid' => $course->id);
                        $group = $DB->get_record ('groups', $conditions);

                        groups_add_member ($group->id, $user->id);
                    }
                }
            }
        }
    }

    function multiple_enrol_to_course_and_group ($username, $courses, $roleid = 5) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);

        foreach ($courses as $course)
        {
            $conditions = array ('id' => $course['id']);
            $course_db = $DB->get_record ('course', $conditions);

            if (!$course_db)
                continue;

            $this->enrol_user ($username, $course_db->id, $roleid);

            // Group
            groups_add_member ($course['group_id'], $user->id);
        }
    }



    function enrol_user_change_role ($username, $course_id, $roleid = 5) {
        global $CFG, $DB, $PAGE;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        /* Create the user before if it is not created yet */
        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);
        if (!$user)
            $this->create_joomdle_user ($username);

        $user = $DB->get_record('user',$conditions);
        $conditions = array ('id' => $course_id);
        $course = $DB->get_record('course', $conditions);

        if (!$course)
            return 0;

        // First, check if user is already enroled but suspended, so we just need to enable it

        $conditions = array ('courseid' => $course_id, 'enrol' => 'manual');
        $enrol = $DB->get_record('enrol', $conditions);

        if (!$enrol)
            return 0;

        $conditions = array ('username' => $username);
        $user = $DB->get_record('user', $conditions);

        if (!$user)
            return 0;

        $conditions = array ('enrolid' => $enrol->id, 'userid' => $user->id);
        $ue = $DB->get_record('user_enrolments', $conditions);

        // Update role info
        if ($ue)
        {
            $conditions = array ('contextid' => $context->id, 'userid' => $user->id);
            $ra = $DB->get_record('role_assignments', $conditions);

            if (!$ra)
                return 1;
            //  XXX Comprobar si ya esta en profe

            $ra->roleid = $roleid;
            $DB->update_record('role_assignments', $ra);
            return 1;
        }



        if ($CFG->version >= 2011061700)
            $manager = new course_enrolment_manager($PAGE, $course);
        else
            $manager = new course_enrolment_manager($course);

        $instances = $manager->get_enrolment_instances();
        $plugins = $manager->get_enrolment_plugins();
        $enrolid = 1; //manual

        $today = time();
        $today = make_timestamp(date('Y', $today), date('m', $today), date('d', $today), 0, 0, 0);
        $timestart = $today;
        $timeend = 0;


        $found = false;
        foreach ($instances as $instance)
        {
            if ($instance->enrol == 'manual')
            {
                $found = true;
                break;
            }
        }

        if (!$found)
            return 0;

        $plugin = $plugins['manual'];

        if ( $instance->enrolperiod)
            $timeend   = $timestart + $instance->enrolperiod;

        $plugin->enrol_user($instance, $user->id, $roleid, $timestart, $timeend);

        return 1;
    }


    function get_course_groups ($id) {
        $groups = groups_get_all_groups($id);

        $rdo = array ();

        foreach ($groups as $group)
        {
            $g['id'] = $group->id;
            $g['name'] = $group->name;
            $g['description'] = $group->description;

            $rdo[] = $g;
        }

        return $rdo;
    }

    function get_group_members ($group_id, $search = '') {
        $users = groups_get_members ($group_id);

        $rdo = array ();
        foreach ($users as $u)
        {
            if ($search)
            {
                if ( (stripos ($u->username, $search) === false)
                    && ( stripos ($u->firstname, $search) === false)
                    && (stripos ($u->lastname, $search) === false)
                    && (stripos ($u->idnumber, $search) === false)
                )
                    continue;
            }

            $member['id'] = $u->id;
            $member['firstname'] = $u->firstname;
            $member['lastname'] = $u->lastname;
            $member['username'] = $u->username;

            $rdo[] = $member;
        }

        return $rdo;
    }

    function get_courses_and_groups () {
        $courses = $this->list_courses ();

        $c = array ();
        foreach ($courses as $course)
        {
            $course_data['remoteid'] = $course['remoteid'];
            $course_data['fullname'] = $course['fullname'];

            $course_data['groups'] = $this->get_course_groups ($course['remoteid']);

            $c[] = $course_data;
        }

        return $c;
    }


    /*
// $courses: course shortnames separated by commas
function enrol_user_multiple_by_shortname ($username, $courses, $roleid = 5)
{
        global $CFG, $DB;

        $c = explode (',', $courses);
        foreach ($c as $shortname)
        {
            $conditions = array ('shortname' => $shortname);
            $course = $DB->get_record ('course', $conditions);

            $this->enrol_user ($username, $course->id, $roleid);
        }

        return 0;
}
*/

    function multiple_enrol ($username, $courses, $roleid = 5, $progid = 0) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);

        if (!$user)
            $this->create_joomdle_user ($username);
        $user = $DB->get_record('user',$conditions);

        if (!$user)
            return;

        if ($progid) {
            require_once($CFG->libdir.'/adminlib.php');
            require_once($CFG->dirroot.'/totara/program/lib.php');
            require_once($CFG->dirroot.'/totara/certification/lib.php');
            require_once($CFG->dirroot . '/totara/core/js/lib/setup.php');
            $categories = prog_assignment_category::get_categories();
            $program = new program($progid);
            $data = new stdClass();
            $data->id = $progid;
            $oldUsers = array();
            foreach ($program->get_program_learners() as $k => $v) {
                $oldUsers[$v] = "1";
            }
            if (!array_key_exists($user->id, $oldUsers)) $oldUsers[$user->id] = "1";
            $data->item = array($roleid => $oldUsers);
            foreach ($categories as $category) {
                $category->update_assignments($data, false);
            }
            
            $program->update_learner_assignments();
        } else {
        foreach ($courses as $c)
        {
            $conditions = array ('id' => $c['id']);
            $course = $DB->get_record ('course', $conditions);
            
            if (!$course)
                continue;

            $this->enrol_user ($username, $course->id, $roleid);
        }
        }

        return 0;
    }

    function enrol_user ($username, $course_id, $roleid = 5, $timestart = 0, $timeend = 0, $cur_username = null) {
        global $CFG, $DB, $PAGE, $USER;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        /* Create the user before if it is not created yet */
        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);
        if (!$user)
            $this->create_joomdle_user ($username);

        $user = $DB->get_record('user',$conditions);
        $conditionsc = array ('id' => $course_id);
        $course = $DB->get_record('course', $conditionsc);

        if (!$course)
            return array(
                'status' => false,
                'message' => 'No course found.',
            );

        // Get enrol start and end dates of manual enrolment plugin
        if ($CFG->version >= 2011061700)
            $manager = new course_enrolment_manager($PAGE, $course);
        else
            $manager = new course_enrolment_manager($course);

        $instances = $manager->get_enrolment_instances();
        $plugins = $manager->get_enrolment_plugins();
        $enrolid = 1; //manual

        if (!$timestart)
        {
            // Set NOW as enrol start if not one defined
            $today = time();
            $today = make_timestamp(date('Y', $today), date('m', $today), date('d', $today), date ('H', $today), date ('i', $today), date ('s', $today));
            $timestart = $today;
        }
        //  $timeend = 0;

        $found = false;
        foreach ($instances as $instance)
        {
            if ($instance->enrol == 'manual')
            {
                $found = true;
                break;
            }
        }

        if (!$found)
            return array(
                'status' => false,
                'message' => 'No enrol plugin found.',
            );

        $plugin = $plugins['manual'];

        if ( $instance->enrolperiod != 0)
            $timeend   = $timestart + $instance->enrolperiod;

        // First, check if user is already enroled but suspended, so we just need to enable it

        $conditions = array ('courseid' => $course_id, 'enrol' => 'manual');
        $enrol = $DB->get_record('enrol', $conditions);

        if (!$enrol)
            return array(
                'status' => false,
                'message' => 'No manual enrol method found.',
            );

        $conditions = array ('username' => $username);
        $user = $DB->get_record('user', $conditions);

        if (!$user)
            return array(
                'status' => false,
                'message' => 'No user found.',
            );

        $conditions = array ('enrolid' => $enrol->id, 'userid' => $user->id);
        $ue = $DB->get_record('user_enrolments', $conditions);

        if ($ue)
        {
            // User already enroled
            // Can be suspended, or maybe enrol time passed
            // Just activate enrolment and set new dates
            $ue->status = 0; //active
            $ue->timestart = $timestart;
            $ue->timeend = $timeend;
            $ue->timemodified = $timestart;
            $DB->update_record('user_enrolments', $ue);
//            return array(
//                'status' => true,
//                'message' => 'Enroled updated.',
//            );
        }

        $plugin->enrol_user($instance, $user->id, $roleid, $timestart, $timeend);
        
        // send email 
        if (!$cur_username) {
            $cur_username = $USER->username;
            if (!$cur_username) {
                $cur_username = 'admin';
            }
        }
        
        if ($roleid == 4 || $roleid == 3) {     // send email to facilitator or content creator
            $rolename = '';
            if ($roleid == 3) { $rolename = 'Content Creator'; }
            if ($roleid == 4) { $rolename = 'Facilitator'; }

            $this->call_method('courseNotifications', $course_id, $course->shortname, $cur_username, $username, $rolename);
        }
        if ($roleid == 5) {     // send email to course creator or LP when user is subscribed to the course.
            // check course 
            $checkcourse = $this->check_course_type($course_id);
            if ($checkcourse) {
                $userto = $user = $DB->get_record('user', array('id' => $checkcourse['managerid']));
                $this->call_method('courseSubscribeNotif', $course_id, $course->shortname, $username, $userto->username, $checkcourse['role']);
            }
        }

        return array(
                'status' => true,
                'message' => 'Enroled success.',
            );
    }


// Assigns role to user, to appear in "other users" course section
    function add_user_role ($username, $course_id, $role_id) {
        global $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $params = array ('username' => $username);
        $user = $DB->get_record('user',$params);
        if (!$user)
            return;

        $params = array ('id' => $course_id);
        $course = $DB->get_record('course', $params);

        $context = context_course::instance($course->id);

        if (!$context)
            return;

        if (!role_assign($role_id, $user->id, $context->id)) {
            return;
        }
    }

    function remove_user_role ($username, $course_id, $role_id,$action = false) {
        global $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $params = array ('username' => $username);
        $user = $DB->get_record('user',$params);
        if (!$user)
            return;

        $params = array ('id' => $course_id);
        $course = $DB->get_record('course', $params);

        $context = context_course::instance($course->id);
        
        $params = array('roleid'=> $role_id,'contextid'=> $context->id,'userid'=> $user->id);
        
        if (!$context)
            return;
         
        if ($action) {
           $unassign = $DB->delete_records('role_assignments', $params);
            return;
        }
        else if (!role_unassign($role_id, $user->id, $context->id)) {
            return;
        }  
    }

    function update_course_enrolments_dates ($course_id,  $timestart, $timeend) {
        global $CFG, $DB;

        $context = context_course::instance($course_id);
        /* 5 indica estudiantes (table mdl_role) */
        $students = get_role_users(5 , $context);

        $conditions = array ('courseid' => $course_id, 'enrol' => 'manual');
        $enrol = $DB->get_record('enrol', $conditions);

        if (!$enrol)
            return 0;

        foreach ($students as $user)
        {
            $conditions = array ('enrolid' => $enrol->id, 'userid' => $user->id);
            $ue = $DB->get_record('user_enrolments', $conditions);

            if ($ue)
            {
                // User already enroled
                // Can be suspended, or maybe enrol time passed
                // Just activate enrolment and set new dates
                $ue->status = 0; //active
                $ue->timestart = $timestart;
                $ue->timeend = $timeend;
                $ue->timemodified = $timestart;
                $DB->update_record('user_enrolments', $ue);
                return 1;
            }
        }
    }

    function get_cat_name ($cat_id) {
        global $CFG, $DB;

        $cat_id = addslashes ($cat_id);

        $query = "SELECT name
        FROM  {$CFG->prefix}course_categories
        WHERE id = '$cat_id';";

        $rdo = $DB->get_records_sql($query);
        $row = (reset ($rdo));
        return format_string ($row->name);
    }
    
    function get_my_categories ($username) {
        global $DB, $CFG;

        $username = utf8_decode($username);
        $username = strtolower($username);
        $user = get_complete_user_data('username', $username);
        
        $query3 = "SELECT id "
                    . "FROM {$CFG->prefix}course_categories "
                    . "WHERE name = '" . $username . "'";
        $noncat = $DB->get_record_sql($query3);
        if ($noncat && count($noncat) > 0) {
            $noncat->userowner = $user->id; 
            $DB->update_record('course_categories', $noncat);
        } else {
        $query2 = "SELECT id "
                . "FROM {$CFG->prefix}course_categories "
                . "WHERE userowner = " . $user->id;

        $checkownercat = $DB->get_records_sql($query2);

        if (count($checkownercat) <= 0) {            
            $this->create_user_category($username);
        } else {
            foreach ($checkownercat as $cat) {
                $cat_context = context_coursecat::instance($cat->id, IGNORE_MISSING);
                $rolemanages = $DB->get_records('role_assignments', array('contextid' => $cat_context->id, 'userid' => $user->id, 'roleid' => 1));
                if (!empty($rolemanages) && count($rolemanages) > 0) {
                    $this->create_user_category($username);
                }
            }
        }
        }
            
        $query1 = "SELECT id, name, userowner "
                . "FROM {$CFG->prefix}course_categories";
                
        $allcat = $DB->get_records_sql($query1);
        $response = array();
        $categories = array();
        
        if (!empty($allcat)) {
            foreach ($allcat as $cat) {
                $category = array();
                $catcontext = context_coursecat::instance($cat->id, IGNORE_MISSING);
                
                if ($cat->userowner == $user->id) {
                    // check user has LP category
                    $rolemanages = $DB->get_records('role_assignments', array('contextid' => $catcontext->id, 'userid' => $user->id, 'roleid' => 1));
                    if (!empty($rolemanages) && count($rolemanages) > 0) {
                        $category['catid'] = $cat->id;
                        $category['name'] = $cat->name;
                        $category['username'] = $username;
                        $category['role'] = 'LP';
                    } else {
                        $category['catid'] = $cat->id;
                        $category['name'] = $cat->name;
                        $category['username'] = $username;
                        $category['role'] = 'non-cat';
                    }
                } else {
                    // check user has Manager category
                    $rolemanages = $DB->get_records('role_assignments', array('contextid' => $catcontext->id, 'userid' => $user->id, 'roleid' => 1));
                    if (!empty($rolemanages) && count($rolemanages) > 0) {
                        $category['catid'] = $cat->id;
                        $category['name'] = $cat->name;
                        $category['username'] = $username;
                        $category['role'] = 'CMA';
                    }
                }
                if (!empty($category)) {
                    $categories[] = $category;
                }
            }
            $response['status'] = true;
            $response['message'] = 'Success.';
            $response['categories'] = $categories;
            return $response;
        } else {
            $response['status'] = false;
            $response['message'] = 'No category found.';
            $response['categories'] = array();
            return $response;
        }        
    }

    function getLPOfCategory ($categoryid) {
        $result = $this->get_lp_of_category($categoryid);
        
        if ($result) {
            $response['status'] = true;
            $response['message'] = 'Success.';
            $response['username'] = $result->username;
            return $response;
        } else {
            $response['status'] = false;
            $response['message'] = 'No LP found.';
            $response['username'] = null;
            return $response;
        }
    }
    function get_lp_of_category ($categoryid) {
        global $DB;
        
        $cat = $DB->get_record('course_categories', array('id' => $categoryid));
        if ($cat) {
            $catcontext = context_coursecat::instance($cat->id, IGNORE_MISSING);
            $rolemanages = $DB->get_records('role_assignments', array('contextid' => $catcontext->id, 'userid' => $cat->userowner, 'roleid' => 1));
            
            if (!empty($rolemanages) && count($rolemanages) > 0) {
                $user = get_complete_user_data ('id', $cat->userowner);
                return $user;
            } else return null;
        } else {
            return null;
        }
    }

    function check_course_type ($courseid) {
        global $CFG, $DB;
        
        $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
        if ($course) {
            $cat = $DB->get_record('course_categories', array('id' => $course->category));
            $catcontext = context_coursecat::instance($cat->id, IGNORE_MISSING);
            
            if ($cat->userowner) {
                $rolemanages = $DB->get_records('role_assignments', array('contextid' => $catcontext->id, 'userid' => $cat->userowner, 'roleid' => 1));

                if (!empty($rolemanages) && count($rolemanages) > 0) {
                    $result['role'] = 'course_lp';
                    $result['managerid'] = $cat->userowner;
                } else {
                    $result['role'] = 'course_non';
                    $result['managerid'] = $course->creator;
                }
            } else {
                $result['role'] = 'course_non';
                $result['managerid'] = $course->creator;
            }
            
            return $result;
        } else {
            return false;
        }
    }

    function get_my_courses_grades ($username) {
        $i = 0;
        $rdo = array ();
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);
        $cursos = enrol_get_users_courses ($user->id);
        foreach ($cursos as $curso)
        {
            $tareas = $this->get_user_grades ($username, $curso->id);
            $sum = 0;
            $n = count ($tareas);
            $rdo[$i]['id'] = $curso->id;
            $rdo[$i]['fullname'] = $curso->fullname;
            $rdo[$i]['cat_id'] = $curso->category;
            $rdo[$i]['cat_name'] = $this->get_cat_name ($curso->category);
            if ($n)
            {
                foreach ($tareas as  $tarea)
                    $sum += $tarea['finalgrade'];
                $rdo[$i]['avg'] = $sum/$n;
            }
            else $rdo[$i]['avg'] = 0;
            $i++;
        }
        return $rdo;
    }

    function get_moodle_users ($limitstart, $limit, $order, $order_dir, $search ) {
        global $CFG, $DB;

        /* Don't show admins and guests */
        $admins = get_admins();
        foreach ($admins as $admin)
        {
            $a[] = $admin->id;
        }
        $a[] = 1; //Guest user
        $userlist = "'".implode("','", $a)."'";

        if ($limit)
            $limit_c = " LIMIT $limitstart, $limit";

        // kludge for ordering by name
        if ($order == 'name')
            $order = 'firstname, lastname';

        if ($order != "")
            $order_c = "  ORDER BY $order $order_dir";
        else $order_c = "";

        if ($search)
        {
            /*
        $users = $DB->get_records_sql("SELECT id, username, email,  firstname, lastname ,auth
                        FROM {$CFG->prefix}user
                        WHERE deleted = 0
                        AND((username like '%$search%') OR (email like '%$search%') OR (firstname like '%$search%') OR (lastname like '%$search%'))
                        $order_c
                        $limit_c");
*/

            $params = array();
            $likeu = $DB->sql_like('username', '?', false);
            $params[] = "%$search%";
            $likee = $DB->sql_like('email', '?', false);
            $params[] = "%$search%";
            $likef = $DB->sql_like('firstname', '?', false);
            $params[] = "%$search%";
            $likel = $DB->sql_like('lastname', '?', false);
            $params[] = "%$search%";

            $users = $DB->get_records_sql("SELECT id, username, email,  firstname, lastname ,auth
                        FROM {$CFG->prefix}user
                        WHERE deleted = 0
                       AND(({$likeu}) OR ({$likee}) OR ({$likef}) OR ({$likel})) 
                        $order_c
                        $limit_c", $params);
        }
        else
            $users = $DB->get_records_sql("SELECT id, username, email,  firstname, lastname ,auth
                FROM {$CFG->prefix}user
                WHERE deleted = 0
                $order_c
                $limit_c");

        $i = 0;
        $u = array ();
        foreach ($users as $user)
        {
            $u[$i] = get_object_vars ($user);
            if (in_array ($user->id, $a))
                $u[$i]['admin'] = '1';
            else $u[$i]['admin'] = '0';

            $u[$i]['name'] = $user->firstname . ' ' . $user->lastname;

            $i++;
        }
        return $u;
    }

    function get_moodle_users_number ($search = "") {
        global $CFG, $DB;

        $search = addslashes ($search);
        /* Don't show admins and guets */
        $admins = get_admins();
        foreach ($admins as $admin)
        {
            $a[] = $admin->id;
        }
        $a[] = 1; //Guest user
        $userlist = "'".implode("','", $a)."'";

        if ($search)
        {
            $params = array();
            $likeu = $DB->sql_like('username', '?', false);
            $params[] = "%$search%";
            $likee = $DB->sql_like('email', '?', false);
            $params[] = "%$search%";
            $likef = $DB->sql_like('firstname', '?', false);
            $params[] = "%$search%";
            $likel = $DB->sql_like('lastname', '?', false);
            $params[] = "%$search%";

            $users = $DB->count_records_sql("SELECT count(id) as n
                        FROM {$CFG->prefix}user
                        WHERE deleted = 0
                        AND id not in ($userlist)
                       AND(({$likeu}) OR ({$likee}) OR ({$likef}) OR ({$likel}))", $params);

        }
        else
        {
            $users = $DB->count_records_sql("SELECT count(id) as n
                FROM {$CFG->prefix}user
                WHERE deleted = 0
                AND id not in ($userlist)");
        }

        return $users;
    }

    function check_moodle_users ($users) {
        global $CFG, $DB;

        $admins = get_admins();
        foreach ($admins as $admin)
        {
            $a[] = $admin->id;
        }
        $a[] = 1; //Guest user
        $i = 0;
        foreach ($users as $user)
        {

            $username = utf8_decode ($user['username']);
            $username = strtolower ($username);
            $conditions = array ('username' => $username);

            $user = $DB->get_record('user', $conditions);
            if ($user)
            {
                $users[$i]['m_account'] = 1;
                $users[$i]['auth'] = $user->auth;
                if (in_array ($user->id, $a))
                    $users[$i]['admin'] = 1;
                else
                    $users[$i]['admin'] = 0;
            }
            else
            {
                $users[$i]['m_account'] = 0;
                $users[$i]['admin'] = 0;
                $users[$i]['auth'] = '';
            }
            $i++;
        }

        return $users;
    }

    function get_moodle_only_users ($users, $search) {
        global $CFG, $DB;

        /* Don't show admins and guets */
        $admins = get_admins();
        foreach ($admins as $admin)
        {
            $a[] = $admin->id;
        }
        $a[] = 1; //Guest user
        $adminlist = "'".implode("','", $a)."'";

        $usernames = array ();
        foreach ($users as $user)
        {
            $username = utf8_decode ($user['username']);
            $username = strtolower ($username);
            $usernames[] = $username;
        }

        $userlist = "'".implode("','", $usernames)."'";
        $users = array();
        if ($search)
        {
            $params = array();
            $likeu = $DB->sql_like('username', '?', false);
            $params[] = "%$search%";
            $likee = $DB->sql_like('email', '?', false);
            $params[] = "%$search%";
            $likef = $DB->sql_like('firstname', '?', false);
            $params[] = "%$search%";
            $likel = $DB->sql_like('lastname', '?', false);
            $params[] = "%$search%";

            /*
        $users = $DB->count_records_sql("SELECT count(id) as n
                        FROM {$CFG->prefix}user
                        WHERE deleted = 0
                        AND id not in ($userlist)
                       AND(({$likeu}) OR ({$likee}) OR ({$likef}) OR ({$likel}))", $params);
*/

            $users = $DB->get_records_sql("SELECT id, username, email,  firstname, lastname, auth
                        FROM {$CFG->prefix}user
                        WHERE deleted = 0 
                        AND auth != 'webservice'
                        AND (username not in ($userlist))
                       AND(({$likeu}) OR ({$likee}) OR ({$likef}) OR ({$likel}))", $params);
        }
        else
            $users = $DB->get_records_sql("SELECT id, username, email, firstname, lastname, auth
                FROM {$CFG->prefix}user
                WHERE deleted = 0
                AND auth != 'webservice'
                AND (username not in ($userlist))");


        $n = count ($users);
        $i = 0;
        $u = array ();
        foreach ($users as $user)
        {
            $u[$i] = get_object_vars ($user);
            if (in_array ($user->id, $a))
                $u[$i]['admin'] = '1';
            else $u[$i]['admin'] = '0';

            $u[$i]['name'] = $user->firstname . ' ' . $user->lastname;

            $i++;
        }
        return $u;

    }

    function delete_user ($username) {
        global $DB, $CFG;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $conditions = array("username" => $username);
        $user = $DB->get_record("user", $conditions);

        if ($user)
        {
            delete_user ($user);
            $DB->delete_records("user",array('username'=>$username));

            // Delete category moodle user
            $usercategory = $DB->get_record("course_categories", array('userowner'=>$user->id, 'depth'=>2));           
            if (count($usercategory) > 0) {
                $coursesids = $DB->get_fieldset_select('course', 'id', 'category = :category ORDER BY sortorder ASC', array('category' => $usercategory->id));
                $newparentid = $usercategory->parent;
                if ($coursesids) {
                    if (!move_courses($coursesids, $newparentid)) {
                        return false;
                    }
                }
                $DB->delete_records("course_categories",array('userowner'=>$user->id, 'depth'=>2));
            }
            return 1;
        }
        return 0;
    }

    function user_id ($username) {
        global $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $conditions = array("username" => $username);
        $user = $DB->get_record("user", $conditions);

        if (!$user)
            return 0;

        return $user->id;
    }

    function user_details ($username) {
        global $DB, $CFG;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $conditions = array("username" => $username);
        $user = $DB->get_record("user", $conditions);

        $u['username'] = $user->username;
        $u['firstname'] = $user->firstname;
        $u['lastname'] = $user->lastname;
        $u['email'] = $user->email;
        $u['id'] = $user->id;
        $u['name'] = $user->firstname. " " . $user->lastname;
        $u['city'] = $user->city;
        $u['country'] = $user->country;
        $u['lang'] = $user->lang;
        $u['timezone'] = $user->timezone;
        $u['phone1'] = $user->phone1;
        $u['phone2'] = $user->phone2;
        $u['address'] = $user->address;
        $u['description'] = $user->description;
        $u['institution'] = $user->institution;
        $u['url'] = $user->url;
        $u['icq'] = $user->icq;
        $u['skype'] = $user->skype;
        $u['aim'] = $user->aim;
        $u['yahoo'] = $user->yahoo;
        $u['msn'] = $user->msn;
        $u['idnumber'] = $user->idnumber;
        $u['department'] = $user->department;
        $u['picture'] = $user->picture;

        $id = $user->id;
        $usercontext = context_user::instance($id);
        $context_id = $usercontext->id;

        if ($user->picture)
            $u['pic_url'] = $CFG->wwwroot."/pluginfile.php/$context_id/user/icon/f1";

        /* Custom fields */
        $query = "SELECT f.id, d.data 
                FROM {$CFG->prefix}user_info_field as f, {$CFG->prefix}user_info_data d 
                WHERE f.id=d.fieldid and userid = ?";

        $params = array ($id);
        $records =  $DB->get_records_sql($query, $params);

        $i = 0;
        $u['custom_fields'] = array ();
        foreach ($records as $field)
        {
            $u['custom_fields'][$i]['id'] = $field->id;
            $u['custom_fields'][$i]['data'] = $field->data;
            $i++;
        }

        return $u;
    }

    function user_custom_fields () {
        global $DB, $CFG;

        $query = "SELECT id, name
                FROM {$CFG->prefix}user_info_field";

        $records =  $DB->get_records_sql($query);
        $i = 0;
        $custom_fields = array ();
        foreach ($records as $field)
        {
            $custom_fields[$i]['id'] = $field->id;
            $custom_fields[$i]['name'] = $field->name;
            $i++;
        }

        return $custom_fields;
    }

    function user_details_by_id ($id) {
        global $DB;

        $conditions = array("id" => $id);
        $user = $DB->get_record("user", $conditions);

        $u['username'] = $user->username;

        return $u;
    }

    function update_session ($username) {
        global $DB, $CFG;

        $conditions = array("username" => $username);
        $user = $DB->get_record("user", $conditions);

        $params = array ($user->id);
        $sql = "SELECT sid FROM {$CFG->prefix}sessions " .
            " WHERE userid = ? " .
            " ORDER BY timemodified DESC LIMIT 1";

        $session = $DB->get_records_sql ($sql, $params);

        if (!$session)
            return false;

        $session_obj = array_shift ($session);

        $conditions = array ('sid' => $session_obj->sid);
        $session = $DB->get_record ('sessions', $conditions);

        if (!$session)
            return;

        $session->timemodified = time ();
        $DB->update_record ('sessions', $session);

        return true;
    }

    function migrate_to_joomdle ($username) {
        global $DB;
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $conditions = array ('username' =>  $username);
        $DB->set_field('user', 'auth', 'joomdle', $conditions);

        return true;
    }

    // Get user events
    function my_events ($username, $cursosid) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $whereclause = '';
        if ($username == 'admin')
        {
            $whereclause .= ' (groupid = 0 AND courseid = 1) ';
        }
        else
        {
            $user = get_complete_user_data ('username', $username);

            if (!$user)
                return array ();

            $g = array ();
            $i = 0;
            $w = array ();
            foreach ($cursosid as $course)
            {
                $course_id = $course['id'];
                $cursos_ids[] = $course_id;
                $groups = groups_get_user_groups ($course_id, $user->id);

                if (!count($groups[0]))
                    continue;

                foreach ($groups[0] as $group)
                    $w[] = " or (courseid = $course_id and groupid = $group)";
            }

            $whereclause = ' (userid = '.$user->id.' AND courseid = 0 AND groupid = 0)';
            $whereclause .= ' OR  (groupid = 0 AND courseid IN ('.implode(',', $cursos_ids).')) ';

            foreach ($w as $cond)
                $whereclause .= $cond;
        }
        $whereclause .= ' AND visible = 1';
        $events = $DB->get_records_select('event', $whereclause);

        $data = array ();
        foreach ($events as $event)
        {
            $e['name'] = $event->name;
            $e['timestart'] = $event->timestart;
            $e['courseid'] = $event->courseid;

            $data[] = $e;
        }

        return $data;
    }

    function get_my_events ($username) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $user = get_complete_user_data ('username', $username);


        if (!$user)
            return array();

        $courses = enrol_get_users_courses ($user->id, true);

        $news = array ();
        foreach ($courses as $c)
        {
            $course_news['remoteid'] = $c->id;
            $course_news['fullname'] = $c->fullname;
            $course_news['events'] = $this->get_upcoming_events ($c->id, $username);

            $news[] = $course_news;
        }

        return $news;
    }


    function add_parent_role ($child, $parent) {
        $child = utf8_decode ($child);
        $child = strtolower ($child);
        $parent = utf8_decode ($parent);
        $parent = strtolower ($parent);

        $parent_user = get_complete_user_data ('username', $parent);
        if (!$parent_user)
            return false;

        $child_user = get_complete_user_data ('username', $child);
        if (!$child_user)
            return false;

        $parent_role_id = get_config('auth/joomdle', 'parent_role_id');
        if (!$parent_role_id)
            return false;


        $context = context_user::instance($child_user->id);

        role_assign($parent_role_id, $parent_user->id, $context->id ); //, $timestart, 0, $hidden);

        return true;
    }
    
    function assign_multi_user_role($courseid, $data) {
        global $DB, $CFG, $PAGE;
        
        $response = array();
        $conditions = array ('id' => $courseid);
        $course = $DB->get_record('course', $conditions);
        
         // Get enrol start and end dates of manual enrolment plugin
        if ($CFG->version >= 2011061700)
            $manager = new course_enrolment_manager($PAGE, $course);
        else
            $manager = new course_enrolment_manager($course);

        $instances = $manager->get_enrolment_instances();
        $plugins = $manager->get_enrolment_plugins();
        $enrolid = 1; //manual
        
        if (!$timestart)
        {
            // Set NOW as enrol start if not one defined
            $today = time();
            $today = make_timestamp(date('Y', $today), date('m', $today), date('d', $today), date ('H', $today), date ('i', $today), date ('s', $today));
            $timestart = $today;
        }
        $found = false;
        foreach ($instances as $instance)
        {
            if ($instance->enrol == 'manual')
            {
                $found = true;
                break;
            }
        }

        if (!$found) {
            $response['status'] = false;
            $response['message'] = 'Enrolment plugin is not found.';
            return $response;
        }

        $plugin = $plugins['manual'];

        if ( $instance->enrolperiod != 0)
            $timeend   = $timestart + $instance->enrolperiod;

        // First, check if user is already enroled but suspended, so we just need to enable it

        $conditions = array ('courseid' => $courseid, 'enrol' => 'manual');
        $enrol = $DB->get_record('enrol', $conditions);

        if (!$enrol) {
            $response['status'] = false;
            $response['message'] = 'Plugin not found or not enabled.';
            return $response;
        }
        foreach($data as $use) { 
            $role_accepted = array(2, 3);
            if(!in_array($use['role'], $role_accepted)) {
                $response['status'] = false;
                $response['message'] = 'Role not accepted, Please select course creator (roleid = 2)  or editing trainer (roleid = 3).';
                return $response;
            }
            $conditions = array ('username' => $use['username']);
            $user = $DB->get_record('user', $conditions);

            if (!$user) {
                    $response['status'] = false;
                    $response['message'] = 'User not found.';
                    return $response;
            }

            $conditions = array ('enrolid' => $enrol->id, 'userid' => $user->id);
            $ue = $DB->get_record('user_enrolments', $conditions);

            if ($ue)
            {
                // User already enroled
                // Can be suspended, or maybe enrol time passed
                // Just activate enrolment and set new dates
                $ue->status = 0; //active
                $ue->timestart = $timestart;
                $ue->timeend = $timeend;
                $ue->timemodified = $timestart;
                $DB->update_record('user_enrolments', $ue);
                $response['status'] = true;
                $response['message'] = 'Role updated.';
            }

            $plugin->enrol_user($instance, $user->id, $use['role'], $timestart, $timeend);
        }
        $response['status'] = true;
        $response['message'] = 'Role added.';
        return $response;
    }

    function get_mentees ($username) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);

        if (!$user)
            return array ();

        $usercontexts = $DB->get_records_sql("SELECT c.instanceid, c.instanceid, u.firstname, u.lastname
                                         FROM {$CFG->prefix}role_assignments ra,
                                              {$CFG->prefix}context c,
                                              {$CFG->prefix}user u
                                         WHERE ra.userid = $user->id
                                         AND   ra.contextid = c.id
                                         AND   c.instanceid = u.id
                                         AND   c.contextlevel = ".CONTEXT_USER);
        if (!$usercontexts)
            return array ();

        $i = 0;
        $users = array ();
        foreach ($usercontexts as $usercontext) {
            $users[$i]['id'] = $usercontext->instanceid;
            $child_user = get_complete_user_data ('id', $usercontext->instanceid);
            $users[$i]['username'] = $child_user->username;
            $users[$i]['name'] = $child_user->firstname. " " . $child_user->lastname;
            $i++;
        }

        return $users;
    }

    function get_roles () {
        global $CFG, $DB ,$PAGE;

        $roles = $DB->get_records_sql("SELECT id, name, shortname
                                         FROM {$CFG->prefix}role");

        $data = array ();
        foreach ($roles as $role)
        {
            // Only return roles assignables in course context
            $contextlevels = get_role_contextlevels($role->id);
            if (!in_array (CONTEXT_COURSE, $contextlevels))
                continue;

            $r['id'] = $role->id;
            $r['name'] = $role->name;
            $r['name'] = get_string($role->shortname);

            $data[] = $r;
        }

        return $data;
    }

    function get_parents ($username) {
        global $CFG, $DB;
        $parent_role_id = get_config('auth/joomdle', 'parent_role_id');

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);
        /* Get mentors for the student */
        $usercontext = context_user::instance($user->id);
        $usercontextid = $usercontext->id;

        $query =
            "SELECT r.userid,u.username
            FROM
            {$CFG->prefix}role_assignments r, {$CFG->prefix}user u
            WHERE
            r.roleid = '$parent_role_id' and r.contextid = '$usercontextid'
            and r.userid  = u.id
            ";

        $mentors =  $DB->get_records_sql($query);

        $data = array ();
        foreach ($mentors as $mentor)
        {
            //  $r['userid'] = $mentor->userid;
            $r['username'] = $mentor->username;

            $data[] = $r;
        }

        return $data;
    }

    function get_all_parents () {
        global $DB, $CFG;

        $parent_role_id = get_config('auth/joomdle', 'parent_role_id');

        $params = array ('roleid' => $parent_role_id);
        $query = "SELECT distinct (userid) FROM {$CFG->prefix}role_assignments" .
            " WHERE roleid = ?";
        $parents =  $DB->get_records_sql($query, $params);

        $p = array ();
        foreach ($parents as $parent)
        {
            $conditions = array ('id' => $parent->userid);
            $parent_user = $DB->get_record('user',$conditions);

            $parent_data['username'] = $parent_user->username;
            $parent_data['firstname'] = $parent_user->firstname;
            $parent_data['lastname'] = $parent_user->lastname;
            $p[] = $parent_data;
        }

        return $p;
    }

    function get_course_parents ($id) {
        global $DB, $CFG;

        $p = array ();
        $students = $this->get_course_students ($id);
        foreach ($students as $student)
        {
            $context = context_user::instance($student['id']);

            $conditions = array ('contextid' => $context->id);
            $parents = $DB->get_records('role_assignments',$conditions);

            foreach ($parents as $parent)
            {
                $conditions = array ('id' => $parent->userid);
                $parent_user = $DB->get_record('user',$conditions);

                // Check it is not already included
                $added = 0;
                $parents_copy = $p;
                foreach ($parents_copy as $pc)
                {
                    if ($pc['username'] == $parent_user->username)
                    {
                        $added = true;
                        break;
                    }
                }

                if ($added)
                    continue;

                $parent_data['username'] = $parent_user->username;
                $parent_data['firstname'] = $parent_user->firstname;
                $parent_data['lastname'] = $parent_user->lastname;
                $p[] = $parent_data;
            }
        }
        return $p;
    }




    function course_enrol_methods ($course_id) {
        $instances = enrol_get_instances($course_id, true);

        $i = 0;
        foreach ($instances as $method)
        {
            $m[$i]['id'] = $method->id;
            $m[$i]['enrol'] = $method->enrol;
            $m[$i]['enrolstartdate'] = $method->enrolstartdate;
            $m[$i]['enrolenddate'] = $method->enrolenddate;
            $i++;
        }

        return $m;
    }


    function quiz_get_question ($id, $newformat = false) {
        global $CFG, $DB;

        $query = "SELECT id, questiontext, qtype, name, generalfeedback
                FROM {$CFG->prefix}question
                WHERE id = ?";
        $params = array ($id);
        $record =  $DB->get_record_sql($query, $params);

        require_once($CFG->libdir . '/questionlib.php');
        if ($record) {
            $question = $DB->get_record('question', array('id' => $id));
            get_question_options($question, true);
            if ($newformat) {
                if ($question->options->answers) {
                    $answers = array();
                    foreach ($question->options->answers as $an) {
                        $answers[] = $an;
                    }
                    $question->options->answers = $answers;
                }
                
                if ($question->options->subquestions) {
                    $subquestions = array();
                    foreach ($question->options->subquestions as $subq) {
                        $subquestions[] = $subq;
                    }
                    $question->options->subquestions = $subquestions;
                }
            }
            $record->options = json_encode($question->options);

            $r = get_object_vars ($record);
        } else {
            $r['res'] = 1;
        }

        return $r;
    }

    function question_rewrite_question_urls($text, $file, $contextid, $component,
                                            $filearea,  $ids, $itemid, $options=null) {
        global $CFG;

        $options = (array)$options;
        if (!isset($options['forcehttps'])) {
            $options['forcehttps'] = false;
        }

        if (!$CFG->slasharguments) {
            $file = $file . '?file=';
        }

        $baseurl = "$CFG->wwwroot/$file/$contextid/$component/$filearea/";

        if (!empty($ids)) {
            $baseurl .= (implode('/', $ids) . '/');
        }

        if ($itemid !== null) {
            $baseurl .= "$itemid/";
        }

        if ($options['forcehttps']) {
            $baseurl = str_replace('http://', 'https://', $baseurl);
        }

        return str_replace('@@PLUGINFILE@@/', $baseurl, $text);
    }

    public function make_html_inline($html) {
        $html = preg_replace('~\s*<p>\s*~', '', $html);
        $html = preg_replace('~\s*</p>\s*~', '<br />', $html);
        $html = preg_replace('~<br />$~', '', $html);
        return $html;
    }
    // get_info_feedback
    function get_info_feedback($id){
        global $CFG, $DB;
        $data['rank']=array();
        $data['text']=array();
        $q1 = "SELECT *
                FROM mdl_questionnaire_response
                WHERE survey_id =".$id;
        $qk1 = $DB->get_records_sql($q1);
        if(count($qk1)>0){
            foreach ($qk1 as $value) {
                $user_id = "SELECT *
                FROM mdl_user
                WHERE id =".$value->username;
                $user = $DB->get_records_sql($user_id);
                $t['user'] = $user[$value->username]->username;
                $tt['user'] = $user[$value->username]->username;
                $q2 = "SELECT *
                FROM mdl_questionnaire_response_rank
                WHERE response_id =".$value->id;
                $qk2 = $DB->get_records_sql($q2);
                foreach ($qk2 as $key) {
                    $t['ques_id']= $key->question_id;
                    $q4 = "SELECT *
                    FROM mdl_questionnaire_question
                    WHERE id =".$key->question_id;
                    $qk4 = $DB->get_records_sql($q4);
                    $t['ques_text']= $qk4[$key->question_id]->content;
                    $t['ques_response']= $key->rank;
                    $data['rank'][] = $t;
                }
                $q3 = "SELECT *
                FROM mdl_questionnaire_response_text
                WHERE response_id =".$value->id;
                $qk3 = $DB->get_records_sql($q3);
                foreach ($qk3 as $toan) {
                    $tt['ques_id']= $toan->question_id;
                    $q5 = "SELECT *
                    FROM mdl_questionnaire_question
                    WHERE id =".$toan->question_id;
                    $qk5 = $DB->get_records_sql($q5);
                    $tt['ques_text']= $qk5[$toan->question_id]->content;
                    $tt['ques_response']= $toan->response;
                    $data['text'][] = $tt;
                }
            }
        }    
        // print_r($data);die;   
        return $data;
    }
    
// feedback detail, using for edit feedback app
    function feedback_detail($feedbackid) {
        global $DB;
        $cm = get_coursemodule_from_id('', $feedbackid, 0, false, MUST_EXIST);
        // check record exits
        if(!$DB->record_exists('questionnaire', array('id'=>$cm->instance))) {
            return $response = array(
                'status' => false,
                'message' => 'Feedback not found.',
            );
        }
        // get feedback by feedback id
        $feedback = $DB->get_record('questionnaire', array('id' => $cm->instance));
        
        $data = array();
        $data['feedbackid'] = $feedback->id;
        $data['feedbackname'] = $feedback->name;
        $data['feedbackintro'] = $feedback->intro;
        // get question of feedback
        $feedback_question = $this->get_feedback_question($feedback->sid);
        $data['rate'] = end($feedback_question)->length;
        $data['feedback_question'] = $feedback_question;
        
        $response['status'] = true;
        $response['message'] = 'success.';
        $response['feedback'] = $data;
        return $response;
    }
    
    // edit feedback
    function edit_detail_feedback($username, $courseid, $data_feedback, $data_question) {
        global $DB;
        $feeback = json_decode($data_feedback);
        $question = json_decode($data_question);
        if(empty($feeback)) {
            return $response = array(
                'status' => false,
                'message' => 'Feedback data not empty',
            );
        }
        if(empty($feeback->id)) {
            return $response = array(
                'status' => false,
                'message' => 'Feedback id not empty',
            );
        }
        if(empty($question)) {
            return $response = array(
                'status' => false,
                'message' => 'Question not empty',
            );
        }
        $this->create_and_add_question_for_feedback($data_feedback, $data_question, $courseid, $username);
        return $response = array(
            'status' => true,
            'message' => 'Feedback updated.',
        );
        
    }
    // get_info_feedback
    // function  get_info_assignment
    function get_info_assignment($id){
        global $CFG, $DB;
        $query = "SELECT *
                FROM mdl_course_modules
                WHERE id =".$id;
        $ass = $DB->get_records_sql($query);
        $instance = $ass[$id]->instance;
        $s = "SELECT *
                FROM  mdl_assign 
                WHERE id =".$instance;
        $assignments = $DB->get_records_sql($s);
        // print_r($assignments);die;
        $data = array ();
        $data['id'] = $assignments[$instance]->id;
        $data['course'] = $assignments[$instance]->course;
        $data['name'] = $assignments[$instance]->name;
        $data['intro'] = $assignments[$instance]->intro;
        $data['duedate'] = $assignments[$instance]->duedate;
        $data['publishdate'] = $assignments[$instance]->allowsubmissionsfromdate;
        $data['timemodified'] = $assignments[$instance]->timemodified;
        return $data;
    }
    // function  delete_module
    function delete_module($id,$course){
        global $CFG, $DB;
        $query = "SELECT * FROM mdl_course_modules WHERE id=".$id." AND course=".$course;
        $kq = $DB->get_record_sql($query);
        $module = ['id'=>$id];
        $assign = ['id'=>$kq->instance];
        $DB->delete_records('course_modules',$module);
        $DB->delete_records('assign',$assign);
        $q2 = "SELECT * FROM mdl_course_sections WHERE course=".$course." AND section=0";
        $kq2 = $DB->get_record_sql($q2);
        $arr_module = $kq2->sequence;
        $arr_module = explode(',', $arr_module);
        for($i = 0; $i < count($arr_module); $i++ ){
            if($arr_module[$i] == $id ){
                array_splice($arr_module, $i,1);
            }
        }
        $n = count($arr_module);
        if($n>0){
            for($j = 0; $j < count($arr_module); $j++ ){
                if($j < $n-1){
                    $a .= $arr_module[$j].',';
                }
                else if($j == $n-1 ){
                    $a .= $arr_module[$j];
                }
            }
        }
        $kq2->sequence = $a;
        $DB->update_record('course_sections', $kq2);
        return true;
    }
    
    // get_info_questionnaire
    function get_info_questionnaire($id, $username = ''){
        global $CFG, $DB;
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);
    
        $query = "SELECT *
                FROM mdl_course_modules
                WHERE id =".$id;
        $questionnarie = $DB->get_record_sql($query);
        $data = array();
        $data['module_id'] = $questionnarie->id;
        $data['questionnaire_id'] = $questionnarie->instance;
        $data['courseid'] = $questionnarie->course;

        $q1 = "SELECT *
                FROM mdl_questionnaire
                WHERE id =".$data['questionnaire_id'];
        $qk1 = $DB->get_record_sql($q1);
        $data['name'] = $qk1->name;
        $data['intro'] = $qk1->intro;
        $data['surveyid'] = $qk1->sid;

        $q2 = "SELECT *
                FROM mdl_questionnaire_question
                WHERE survey_id =".$data['surveyid'];
        $qk2 = $DB->get_records_sql($q2);

        $q3 = "SELECT *
                FROM mdl_questionnaire_question_type ";
        $qk3 = $DB->get_records_sql($q3);

        if (count($qk2) > 0) {
            foreach ($qk2 as $value) {
                $tt['id'] = $value->id;
                $tt['type_id'] = $value->type_id;
                $tt['type_ques'] = $value->type_id;
                foreach ($qk3 as $type) {
                    if ($tt['type_id'] == $type->typeid) {
                        $tt['type_ques'] = $type->type;
                    }
                }
                $tt['name'] = $value->name;
                $tt['content'] = $value->content;
                $tt['required'] = $value->required;
                $tt['position'] = $value->position;
                $tt['deleted'] = $value->deleted;
                $tt['params'] = $value->params;

                if ($tt['type_ques'] == 'Rate (scale 1..5)') {
                $tt['length'] = $value->length;
                } else if ($tt['type_ques'] == 'Text Box') {
                    $tt['length'] = $value->precise;
                }
                $data['questions'][] = $tt;
            }
        } else {
            $data['questions'] = [];
        }

        return $data;
    }
    // get_info_questionnaire

     // function end quiz
    function post_end_quiz($id,$username = ''){
        global $CFG, $DB;
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);
        $query = "SELECT *
                FROM mdl_quiz_attempts
                WHERE id =".$id." AND userid = ".$user->id;
        $attempt = $DB->get_record_sql($query);
        $attempt->id = $id; 
        $attempt->userid = $user->id; 
        $attempt->state = 'finished';
        $attempt->timefinish = time();
        $attempt->timemodified = time();
        $DB->update_record('quiz_attempts', $attempt);
        return true;
    }

    function get_question_option_match($id){
        global $CFG, $DB;
        $query = "SELECT id, questiontext, answertext
                FROM mdl_qtype_match_subquestions
                WHERE questionid =".$id ;
        $records =  $DB->get_records_sql($query);
        $q = array();  
        foreach ($records as $record ) {
            $r = get_object_vars ($record);
            $q[] = $r;
        }    
        return $q;
    }

    function quiz_get_answers ($id) {
        global $CFG, $DB;

        $query = "SELECT id, answer, fraction
                FROM {$CFG->prefix}question_answers
                WHERE question = ?";
        $params = array ($id);
        $records =  $DB->get_records_sql($query, $params);

        $options['noclean'] = true;
        $answers = array ();
        foreach ($records as $record)
        {
            $r = get_object_vars ($record);

            $r['fraction'] = (float) $r['fraction'];
            $r['answer'] = format_text($r['answer'], FORMAT_HTML, $options);
            $r['answer'] = $this->question_rewrite_question_urls($r['answer'], 'pluginfile.php', 1, 'question', 'answer', array(), $r['id']);
            $r['answer'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $r['answer']);
            $r['answer'] = $this->make_html_inline ($r['answer']);


            $answers[] = $r;
        }

        return $answers;
    }

    function quiz_get_random_question ($cat_id, $used_ids) {
        global $CFG, $DB;

        $query = "SELECT id,questiontext, qtype
                FROM {$CFG->prefix}question
                WHERE category = ? AND qtype = 'multichoice'";
        if ($used_ids)
            $query .= " AND id not in ($used_ids)";
        $query .= " ORDER BY RAND() LIMIT 1";

        $params = array ($cat_id);
        $record =  $DB->get_record_sql($query, $params);

        if (!$record)
            return NULL;

        $r = get_object_vars ($record);
        $r['questiontext'] = $this->question_rewrite_question_urls($r['questiontext'], 'pluginfile.php', 1, 'question', 'questiontext', array(), $r['id']);
        $r['questiontext'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $r['questiontext']);

        $r['answers'] = $this->quiz_get_answers ($r['id']);

        return $r;
    }

    function quiz_get_question_categories () {
        global $CFG, $DB;

        $cat = addslashes ($cat);
        $query =
            "SELECT id, name
            FROM 
            {$CFG->prefix}question_categories
            ORDER BY
            sortorder ASC
            ";

        $params = array ($cat);
        $records =  $DB->get_records_sql($query, $params);

        $cats = array ();
        foreach ($records as $cat)
        {
            $c = get_object_vars ($cat);
            $cats[] = $c;
        }


        return ($cats);
    }



    function quiz_get_correct_answer ($id) {
        global $CFG, $DB;

        $query = "SELECT id
                FROM {$CFG->prefix}question_answers
                WHERE question = ? and fraction = 1";
        $params = array ($id);
        $record =  $DB->get_record_sql($query, $params);

        return $record->id;
    }

    function multiple_suspend_enrolment ($username, $courses) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);

        if (!$user)
            return;

        foreach ($courses as $c)
        {
            $conditions = array ('id' => $c['id']);
            $course = $DB->get_record ('course', $conditions);

            if (!$course)
                continue;

            $this->suspend_enrolment ($username, $course->id);
        }

        return 0;
    }

    function suspend_enrolment ($username, $course_id) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $conditions = array ('courseid' => $course_id, 'enrol' => 'manual');
        $enrol = $DB->get_record('enrol', $conditions);

        if (!$enrol)
            return;

        $conditions = array ('username' => $username);
        $user = $DB->get_record('user', $conditions);

        if (!$user)
            return;

        $conditions = array ('enrolid' => $enrol->id, 'userid' => $user->id);
        $ue = $DB->get_record('user_enrolments', $conditions);

        if (!$ue)
            return;

        $ue->status = 1; //suspended
        $DB->update_record('user_enrolments', $ue);

        return;
    }


    function multiple_unenrol_user ($username, $courses, $progid = 0) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);

        if (!$user)
            return;

        if ($progid) {
            require_once($CFG->libdir.'/adminlib.php');
            require_once($CFG->dirroot.'/totara/program/lib.php');
            require_once($CFG->dirroot.'/totara/certification/lib.php');
            require_once($CFG->dirroot . '/totara/core/js/lib/setup.php');
            
            $program = new program($progid);
            $program->unassign_learners(array((int)$user->id));
            
        } else {
        foreach ($courses as $c)
        {
            $conditions = array ('id' => $c['id']);
            $course = $DB->get_record ('course', $conditions);
            
            if (!$course)
                continue;

            $this->unenrol_user ($username, $course->id);
        }
        }

        return 0;
    }

    // Unenrol user totally
    function unenrol_user ($username, $course_id) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $conditions = array ('courseid' => $course_id, 'enrol' => 'manual');
        $enrol = $DB->get_record('enrol', $conditions);

        if (!$enrol)
            return array(
                'status' => false,
                'message' => 'No enroled method found.',
            );

        $conditions = array ('username' => $username);
        $user = $DB->get_record('user', $conditions);

        if (!$user)
            return array(
                'status' => false,
                'message' => 'No user found.',
            );

        $conditions = array ('enrolid' => $enrol->id, 'userid' => $user->id);
        $ue = $DB->get_record('user_enrolments', $conditions);

        if (!$ue)
            return array(
                'status' => false,
                'message' => 'No user enroled found.',
            );

        $instance = $DB->get_record('enrol', array('id'=>$ue->enrolid), '*', MUST_EXIST);

        $plugin = enrol_get_plugin($instance->enrol);

        $plugin->unenrol_user($instance, $ue->userid);
        return array(
                'status' => true,
                'message' => 'Unenroled success.',
       );
    }

    function multiple_remove_from_group ($username, $courses) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);

        if (!$user)
            return;

        foreach ($courses as $c)
        {
            $conditions = array ('id' => $c['id']);
            $course = $DB->get_record ('course', $conditions);

            if (!$course)
                continue;

            // Group
            groups_remove_member ($c['group_id'], $user->id);
        }

        return 0;
    }

    function get_course_completion ($id, $username) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $query =
            "SELECT
            cs.id,
            cs.section,
            cs.summary
            FROM
            {$CFG->prefix}course_sections cs
            WHERE
            cs.course = ?
            and cs.visible = 1
            ";

        $params = array ($id);
        $records =  $DB->get_records_sql($query, $params);

        $context = context_course::instance($id);

        $user = get_complete_user_data ('username', $username);

        $data = array ();
        foreach ($records as $r)
        {
            $e['section'] = $r->section;
            $e['summary'] = $r->summary;

            // Check all modules in section
            $query =
                "SELECT
                cm.id
                FROM
                {$CFG->prefix}course_modules cm
                WHERE
                cm.section = ?
                and cm.visible = 1
                and completion != 0
                ";

            $params = array ($r->id);
            $records_cm =  $DB->get_records_sql($query, $params);

            $n_cm = count ($records_cm);
            $n = 0;
            foreach ($records_cm as $module)
            {
                $query = "SELECT count(*) 
                    FROM  {$CFG->prefix}course_modules_completion 
                    WHERE coursemoduleid = ? and userid = ?";

                $params = array ($module->id, $user->id);
                $n +=  $DB->count_records_sql($query, $params);
            }
            if ($n == 0)
                $complete = 0;
            else if ($n < $n_cm)
                $complete = 1;
            else $complete = 2;
            $e['complete'] = $complete;

            $data[$i] = $e;
            $i++;
        }

        return $data;
    }

    function get_course_resources ($id, $username = '') {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        if ($username)
            $user = get_complete_user_data ('username', $username);

        $modinfo = get_fast_modinfo($id);
        $sections = $modinfo->get_section_info_all();

        $mods = get_fast_modinfo($id)->get_cms();
        $modnames = get_module_types_names();
        $modnamesplural = get_module_types_names(true);
        $modnamesused = get_fast_modinfo($id)->get_used_module_names();

        foreach ($sections as $section)
        {
            $sectionmods = explode(",", $section->sequence);
            foreach ($sectionmods as $modnumber) {
                if (empty($mods[$modnumber])) {
                    continue;
                }
                $mod = $mods[$modnumber];

                if ( $mod->modname == 'resource')
                {
                    if ($username)
                    {
                        $cm = get_coursemodule_from_id(false, $mod->id);
                        if (!coursemodule_visible_for_user ($cm, $user->id))
                            continue;
                    }

                    $e[$section->section]['section'] = $section->section;
                    $e[$section->section]['summary'] = $section->summary;
                    $resource['id'] = $mod->id;
                    $resource['name'] = $mod->name;
                    $resource['type'] = substr ($mod->icon, 2);

                    $e[$section->section]['resources'][] = $resource;
                }
            }
        }
        return $e;
    }

    function get_course_quizes ($id, $username = '') {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $query =
            "SELECT
            cs.id,
            cs.section,
            cs.summary
            FROM
            {$CFG->prefix}course_sections cs
            WHERE
            cs.course = ? ";

        $params = array ($id);
        $records =  $DB->get_records_sql($query, $params);
        $context = context_course::instance($id);

        if ($username)
            $user = get_complete_user_data ('username', $username);
        $i = 0;
        $data = array ();
        foreach ($records as $r)
        {
            $e['section'] = $r->section;
            $e['summary'] = $r->summary;

            $query =
                "SELECT cm.id, q.name, q.atype, q.attempts, q.id as quiz_id, q.questions, q.timelimit, q.intro
                FROM
                {$CFG->prefix}course_modules cm, {$CFG->prefix}quiz q, {$CFG->prefix}modules m
                WHERE
                cm.instance = q.id and cm.module = m.id and m.name = 'quiz'
                and cm.course = ?
                and cm.section = ? ";

            $params = array ($id, $r->id);
            $records_cm =  $DB->get_records_sql($query, $params);

            $resources = array ();

            foreach ($records_cm as $r_cm)
            {
                $resource['grade'] = (float) 0;
                $resource['passed'] = false;
                if ($username)
                {
                    $cm = get_coursemodule_from_id(false, $r_cm->id);
                    if (!coursemodule_visible_for_user ($cm, $user->id))
                        continue;

                    $grades = grade_get_grades ($id, 'mod', 'quiz', $r_cm->quiz_id, $user->id);
                    $grade = array_shift ($grades->items[0]->grades);

                    $resource['grade'] = (float) $grade->grade;
                    if ($grade->grade == $grades->items[0]->grademax)
                        $resource['passed'] = true;
                    else
                        $resource['passed'] = false;
                }


                $resource['id'] = $r_cm->id;
                $resource['name'] = $r_cm->name;
                 $options['noclean'] = true;
                $resource['intro'] = format_text($r_cm->intro, FORMAT_MOODLE, $options);
                $resource['timelimit'] = $r_cm->timelimit;
                $resource['quiz_id'] = $r_cm->quiz_id;
                $resource['questions'] = $r_cm->questions;
                $resource['atype'] = $r_cm->atype;
                $resource['numOfAttempts'] = (int)$r_cm->attempts;
                //===================================================
                $s = "SELECT timeopen, timeclose FROM mdl_quiz  WHERE id = ".$r_cm->quiz_id;
                $time_close_open = $DB->get_records_sql($s);
                if(count($time_close_open) > 0){
                    foreach ($time_close_open as $key ) {
                        $resource['timeopen'] = $key->timeopen;
                        $resource['timeclose'] = $key->timeclose;
                    }
                }else{
                    $resource['timeopen'] = 0;
                    $resource['timeclose'] = 0;
                }
                // =======================================================
                $resource['attempts'] = array();
                $q = "SELECT id, state, timestart, timefinish FROM mdl_quiz_attempts  WHERE quiz = ".$r_cm->quiz_id." AND userid = ".$user->id;
                $record_quiz_time = $DB->get_records_sql($q);
                if ($record_quiz_time != null) {
                    foreach ($record_quiz_time as $key ) {
                        $resource['attempt'] = $key->id;
                        $resource['time']['state'] = $key->state;
                        $resource['time']['timestart'] = $key->timestart;
                        $resource['time']['timefinish'] = $key->timefinish;
                        $attempt = new stdClass();
                        $attempt->id = $key->id;
                        $attempt->state = $key->state;
                        $attempt->timestart = $key->timestart;
                        $attempt->timefinish = $key->timefinish;
                        $resource['attempts'][] = $attempt;
                    }
                } else {
                    $resource['attempt'] = 0;
                    $resource['time']['state'] = '';
                    $resource['time']['timestart'] = 0;
                    $resource['time']['timefinish'] = 0;
                }
                $resource['attempts'] = json_encode($resource['attempts']);
                // =======================================================
                $resources[] = $resource;
            }

            $e['quizes'] = $resources;
            $data[$i] = $e;
            $i++;
        }

        return $data;
    }

    function get_quiz_attempt_info($attemptid, $username) {
        global $CFG, $DB;
        require_once($CFG->dirroot . '/mod/quiz/locallib.php');
        require_once($CFG->dirroot . '/mod/quiz/report/reportlib.php');
        $attemptobj = quiz_attempt::create($attemptid);
        $attempt = $attemptobj->get_attempt();
        $quiz = $attemptobj->get_quiz();

        $return = array();
        $return['attempt'] = $attempt;
        $return['quiz'] = $quiz;
        $questions = explode(',', $quiz->questions);
        $return['questions'] = array();
        foreach ($questions as $value) {
            if ($value == 0) continue;
            $question = $this->quiz_get_question($value);
            $question['correct_answers'] = $this->quiz_get_correct_answer($value);
            $return['questions'][] = $question;
        }
        $return['pages'] = array();
        $slots = $attemptobj->get_slots();
        foreach ($slots as $slot) {
            $qa = $attemptobj->get_question_attempt($slot);
            $question = $qa->get_question();
            $arr = array();
            $arr['page'] = $attemptobj->get_question_page($slot);
            $arr['mark'] = $qa->get_mark();
            $arr['maxMark'] = $qa->get_max_mark();
            $arr['fraction'] = $qa->get_fraction();
            $arr['finalMark'] = $attemptobj->get_question_mark($slot);
            $arr['qtype'] = $question->qtype->name();

            $arr['correctResponse'] = $question->get_correct_response();

            switch($question->qtype->name()) {
                case 'truefalse':
                    $response = $qa->get_last_qt_var('answer', '');
//                    $arr['questionAttempt'] = ($response) ? 'True' : 'False';
                    $arr['questionAttempt'] = $response;
                    break;
                case 'multichoice':
                    $response = $question->get_response($qa);

//                    foreach ($question->get_order($qa) as $value => $ansid) {
//                        $ans = $question->answers[$ansid];
//                        $isselected = $question->is_choice_selected($response, $value);
//                        if ($isselected) {
//                            $ansText = $question->make_html_inline($question->format_text(
//                                $ans->answer, $ans->answerformat,
//                                $qa, 'question', 'answer', $ansid));
//                        }
//                    }
//                    $arr['questionAttempt'] = $ansText;
                    $arr['choices'] = $question->get_order($qa);
                    $arr['questionAttempt'] = $response;
                    break;
                case 'match':
                    $response = $qa->get_last_qt_data();
                    $arr['questionAttempt'] = $response;
//                    $stemorder = $question->get_stem_order();
//
//                    $choices = array();
//                    foreach ($question->get_choice_order() as $key => $choiceid) {
//                        $choices[$key] = $question->choices[$choiceid];
//                    }
                    $arr['choices'] = $question->get_choice_order();
//                    $arr['questionAttempt'] = [];
//                    foreach ($stemorder as $key => $stemid) {
//                        $fieldname = 'sub' . $key;
//                        if (array_key_exists($fieldname, $response)) {
//                            $selected = $response[$fieldname];
//                        } else {
//                            $selected = 0;
//                        }
//                        $arr['questionAttempt'][$key] = $choices[$selected];
//                    }
                    break;
                case 'shortanswer':
                    $response = $qa->get_last_qt_var('answer');
                    $arr['questionAttempt'] = $response;
//                    $arr['questionAttempt'] = $qa->get_last_qt_var('answer');
                    break;
            }

            $return['pages'][] = $arr;
        }

        $return['sumMarks'] = $attemptobj->get_sum_marks();

        return json_encode($return);
    }
    
    function update_last_access_course ($id, $username) {
        global $DB;
        $username = utf8_decode ($username);
        $username = strtolower ($username);

        if ($username)
            $user = get_complete_user_data ('username', $username);

        $conditions = array ('courseid' => $id, 'status' => 0);
        $enrol = $DB->get_records('enrol', $conditions);

        if ($enrol) {
            foreach ($enrol as $val) {
                $conditions = array ('enrolid' => $val->id, 'userid' => $user->id);
                $ue = $DB->get_record('user_enrolments', $conditions);

                if ($ue) {
                    $ue->lastaccess = time();
                    $DB->update_record('user_enrolments', $ue);
                }
            }
        }
    }
    function get_course_mods ($id, $username = '', $mtype = '') {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        if ($username)
            $user = get_complete_user_data ('username', $username);

        $modinfo = get_fast_modinfo($id);
        $sections = $modinfo->get_section_info_all();


        // $forum = forum_get_course_forum($id, 'news');
        // $news_forum_id = $forum->id;

        $mods = get_fast_modinfo($id)->get_cms();
        $modnames = get_module_types_names();
        $modnamesplural = get_module_types_names(true);
        $modnamesused = get_fast_modinfo($id)->get_used_module_names();


        $context = context_course::instance($id);
        
        $e = array ();
        $result = array();
        if ($sections) {
        foreach ($sections as $section)
        {
            if (!$section->visible && $section->section != 0)
                continue;

            $e[$section->section]['section'] = $section->section;
            $e[$section->section]['name'] = $section->name;
            $e[$section->section]['summary'] = file_rewrite_pluginfile_urls ($section->summary, 'pluginfile.php', $context->id, 'course', 'section', $section->id);
            $e[$section->section]['summary'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $e[$section->section]['summary']);
            $e[$section->section]['mods'] = array();

            $sectionmods = explode(",", $section->sequence);
            foreach ($sectionmods as $modnumber) {
                if (empty($mods[$modnumber])) {
                    continue;
                }
                $mod = $mods[$modnumber];
                if ($mtype) if ($mod->modname != $mtype) continue;

                if ($mod->modname == 'quiz') {
                    $quiz = $DB->get_record('quiz', array('id' => $mod->instance ));
                    $resource['atype'] = (int)$quiz->atype;
                }
                
                $resource['completion_info'] = '';
                $resource['mod_completion'] = false;
                if ($username)
                {
                    /*
                     * get activity completion
                     */
                    $mod_completion = $DB->get_record('course_modules_completion', array('coursemoduleid' => $mod->id, 'userid'=>$user->id, 'completionstate'=>'1'));
                    if($mod_completion) {
                        $resource['mod_completion'] = true;
                    }
                    $cm = get_coursemodule_from_id(false, $mod->id);
                    if (!coursemodule_visible_for_user ($cm, $user->id))
                    {
                        if (!$mod->showavailability) // Mod not visible, and no completion info to show
                            continue;

                        $resource['available'] = 0;
                        $ci = new condition_info($mod);
                        $resource['completion_info'] = $ci->get_full_information ();
                    }
                    else
                        $resource['available'] = 1;
                }
                else
                    $resource['available'] = 1;

                // $e[$section->section]['section'] = $section->section;
                // $e[$section->section]['name'] = $section->name;
                // $e[$section->section]['summary'] = file_rewrite_pluginfile_urls ($section->summary, 'pluginfile.php', $context->id, 'course', 'section', $section->id);
                // $e[$section->section]['summary'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $e[$section->section]['summary']);
                $resource['id'] = $mod->id;
                $resource['name'] = $mod->name;
                $resource['mod'] = $mod->modname;

                // Format for mod->icon is: f/type-24
                $type = substr ($mod->icon, 2);
                $parts = explode ('-', $type );
                $type = $parts[0];
                $resource['type'] = $type;


                //In forum, type is unused, so we use it for forum type: news/general
                // if ($mod->modname == 'forum')
                // {
                //     $cm = get_coursemodule_from_id('forum', $mod->id);
                //     if ($cm->instance == $news_forum_id)
                //         $resource['type'] = 'news';
                // }

                /*
                if ($mod->modname == 'resource')
                {
                    // Get display options for resource
                    $params = array ($mod->instance);
                    $query = "SELECT display from  {$CFG->prefix}resource where id = ?";
                    $record = $DB->get_record_sql ($query, $params);
                    
                    $resource['display'] = $record->display;
                }
                else $resource['display'] = 0;
*/
                $resource['display'] = $this->get_display ($mod->modname, $mod->instance);

                // link activity
                switch ($mod->modname)
                {
                    case "questionnaire" :
                        $thisurl = $CFG->root.'/index.php?option=com_joomdle&view=feedbackview&module_id='.$mod->id.'&course_id='.$id;
                        break;
                    case "quiz" :
                        $thisurl = $CFG->root.'/index.php?option=com_joomdle&view=quiz&id='.$mod->id.'&course_id='.$id;
                        break;
                    case "page" :
                    case "assignment" :
                    case "folder" :
                    case "messages" :
                    case "assign" :
                    case "scorm" :
                    case "skillsoft" :
                        $thisurl = $CFG->root.'/index.php?option=com_joomdle&view=wrapper&mtype='.$mod->modname.'&course_id='.$id.'&id='.$mod->id;
                        break;
                    default:
                        if ($mod->modname)
                        {
                            $path = '/mod/'.$mod->modname.'/view.php?id=';
                            $thisurl = $CFG->wwwroot.$path.$id;
                            break;
                        }
                        else
                        {
                            $path = '/?a=1';
                            $thisurl = $CFG->wwwroot.$path;
                        }
                        break;
                }
                $resource['url'] = urlencode($thisurl);

                $e[$section->section]['mods'][] = $resource;
            }
        }
            $result['status'] = 1;
            $result['message'] = 'Success.';
            $result['coursemods'] = $e;
        } else {
            $result['status'] = 0;
            $result['message'] = 'Not found.';
            $result['coursemods'] = array();
    }
        return $result;
    }
    
    function list_activity_add_to_course ($id, $username = '') {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        if ($username)
            $user = get_complete_user_data ('username', $username);

        $modinfo = get_fast_modinfo($id);
        $sections = $modinfo->get_section_info_all();


        // $forum = forum_get_course_forum($id, 'news');
        // $news_forum_id = $forum->id;

        $mods = get_fast_modinfo($id)->get_cms();
        $modnames = get_module_types_names();
        $modnamesplural = get_module_types_names(true);
        $modnamesused = get_fast_modinfo($id)->get_used_module_names();

        $array_module_id_visibled = array();
        $query =
            "SELECT
                cm.id
                FROM
                {$CFG->prefix}course_modules cm
                WHERE
            cm.course = ?
            and cm.visible = ?
            ";
        $params = array ($id,1);

        $module_id_visible =  $DB->get_records_sql($query, $params);
        if ($module_id_visible) {
            foreach ($module_id_visible as $key ) {
                $array_module_id_visibled[] = $key->id;
            }
        }

        $context = context_course::instance($id);
        
        $e = array ();
        $result = array();
        $page = array(); $ipage = 0;
        $quizs = array(); $iquiz = 0;
        $assign = array(); $iassign = 0;
        $scorm = array(); $iscorm = 0;
        
        if ($sections) { 
            foreach ($sections as $section)
            {
                if (!$section->visible)
                    continue;

                $sectionmods = explode(",", $section->sequence);
                foreach ($sectionmods as $modnumber) {
                    if (empty($mods[$modnumber])) {
                        continue;
                    }
                    $mod = $mods[$modnumber];

                    if ($mod->modname == 'quiz') {
                        $quiz = $DB->get_record('quiz', array('id' => $mod->instance ));
                        $resource['atype'] = (int)$quiz->atype;
                    }

                    $resource['completion_info'] = '';
                    $resource['mod_completion'] = false;
                    if ($username)
                    {
                        /*
                         * get activity completion
                         */
                        $mod_completion = $DB->get_record('course_modules_completion', array('coursemoduleid' => $mod->id, 'userid'=>$user->id, 'completionstate'=>'1'));
                        if($mod_completion) {
                            $resource['mod_completion'] = true;
                        }
                        $cm = get_coursemodule_from_id(false, $mod->id);
                        if (!coursemodule_visible_for_user ($cm, $user->id))
                        {
                            if (!$mod->showavailability) // Mod not visible, and no completion info to show
                                continue;

                            $resource['available'] = 0;
                            $ci = new condition_info($mod);
                            $resource['completion_info'] = $ci->get_full_information ();
                        }
                        else
                            $resource['available'] = 1;
                    }
                    else
                        $resource['available'] = 1;

                    $resource['id'] = $mod->id;
                    $resource['name'] = $mod->name;
                    $resource['mod'] = $mod->modname;

                    // Format for mod->icon is: f/type-24
                    $type = substr ($mod->icon, 2);
                    $parts = explode ('-', $type );
                    $type = $parts[0];
                    $resource['type'] = $type;

                    $resource['display'] = $this->get_display ($mod->modname, $mod->instance);
                    
                    $resource['activityAdded'] = false;
                    if(in_array($modnumber,$array_module_id_visibled)== true) {
                        $resource['activityAdded'] = true;
                    }

                    if ($mod->modname == 'page') {
                        $ipage++;
                        $page[] = $resource;
                    }
                    if ($mod->modname == 'quiz') {
                        $iquiz++;
                        $quizs[] = $resource;
                    }
                    if ($mod->modname == 'assign') {
                        $iassign++;
                        $assign[] = $resource;
                    }
                    if ($mod->modname == 'scorm') {
                        $iscorm++;
                        $scorm[] = $resource;
                    }
//                    $e[$section->section]['mods'][] = $resource;
                }
            }
            $e[0]['modtype'] = 'content';
            $e[0]['total'] = $ipage;
            $e[0]['mods'] = $page;
            
            $e[1]['modtype'] = 'scorm';
            $e[1]['total'] = $iscorm;
            $e[1]['mods'] = $scorm;
            
            $e[2]['modtype'] = 'assessment';
            $e[2]['total'] = $iquiz;
            $e[2]['mods'] = $quizs;
            
            $e[3]['modtype'] = 'assign';
            $e[3]['total'] = $iassign;
            $e[3]['mods'] = $assign;
            
            $result['status'] = 1;
            $result['message'] = 'Success.';
            $result['coursemods'] = $e;
        } else {
            $result['status'] = 0;
            $result['message'] = 'Not found.';
            $result['coursemods'] = array();
        }
        return $result;
    }
    
    function list_activity($id, $username, $type) {
        global $DB, $CFG;
        $response = array();
        $activity = $this->get_course_mods($id, '', $type);
        if ($activity['status']) {
            $arr = array();
            $result = array();
            $result[0] = $activity['coursemods'][0];
            foreach ($activity['coursemods'] as $section ) {
                if (!empty($section['mods'])) {
                    foreach ($section['mods'] as $mod) {
                        $arr[$mod['id']] = $mod;
                    }
                }
            }
            krsort($arr);
            $result[0]['mods'] = $arr;
            
            
        $response['status'] = true;
            $response['activity'] = $result;//$activity['coursemods'];
        } else {
            $response['status'] = false;
            $response['activity'] = $activity['coursemods'];
        }
        return $response;
    }
    //  get module at tab course content visible = 0, tab course outline visible = 1
    function get_course_mods_visible($id, $username = '', $visible, $mtype = '') {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $visible = (int)$visible;
        if ($username) {
            $user = get_complete_user_data ('username', $username);
        }

        $courseformatoptions = course_get_format($id)->get_format_options();
//        $modinfo = get_fast_modinfo($id);
//        $sections = $modinfo->get_section_info_all();
        $sections = $DB->get_records('course_sections', array('course'=>$id));

        // $forum = forum_get_course_forum($id, 'news');
        // $news_forum_id = $forum->id;

        $mods = get_fast_modinfo($id)->get_cms();
        $query =
            "SELECT
                cm.id
                FROM
                {$CFG->prefix}course_modules cm
                WHERE
            cm.course = ?
            and cm.visible = ?
            ";
        $params = array ($id,$visible);

        $module_id_visible =  $DB->get_records_sql($query, $params);
        foreach ($module_id_visible as $key ) {
            $array_module_id_visible[] = $key->id;
        }

        $context = context_course::instance($id);
        
        $e = array ();
        
        foreach ($sections as $section)
        {
            if ((!$section->visible && $section->section != 0) || $section->section > $courseformatoptions['numsections'])
                continue;

            $e[$section->section]['section'] = $section->section;
            $e[$section->section]['visible'] = $section->visible;
            $e[$section->section]['sectionid'] = $section->id;
            $e[$section->section]['name'] = $section->name ? $section->name : 'Topic Title Here';
            $e[$section->section]['summary'] = file_rewrite_pluginfile_urls ($section->summary, 'pluginfile.php', $context->id, 'course', 'section', $section->id);
            $e[$section->section]['summary'] = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $e[$section->section]['summary']);
            $e[$section->section]['mods'] = array();

            $sectionmods = explode(",", $section->sequence);

            foreach ($sectionmods as $modnumber) {
                if(in_array($modnumber,$array_module_id_visible)== true) {

                    if (empty($mods[$modnumber])) {
                        continue;
                    }
                    $mod = $mods[$modnumber];
                    if ($mod->modname == 'forum') {
                        continue;
                    }
                    if ($mtype) if ($mod->modname != $mtype) continue;

                    if ($mod->modname == 'quiz') {
                        $quiz = $DB->get_record('quiz', array('id' => $mod->instance));
                        $resource['atype'] = (int)$quiz->atype;
                    }

                    $resource['completion_info'] = '';
                    $resource['mod_completion'] = false;
                    if ($username) {
                        /*
                         * get activity completion
                         */
                        // if ($mod->modname == 'quiz') {
                        //      $mod_completion = $DB->get_record('quiz_attempts', array('quiz' => $mod->instance, 'userid' => $user->id, 'state' => 'finished'));
                        // }
                        // else{
                            $mod_completion = $DB->get_record('course_modules_completion', array('coursemoduleid' => $mod->id, 'userid' => $user->id, 'completionstate' => '1' ));
                        //}
                        if ($mod_completion) {
                            $resource['mod_completion'] = true;
                        }
                        if ($mod->modname == 'assign') {
                            $assign = $this->get_assignment_activity_detail($id, $username, $mod->id);
                            if ($assign['submission_status'] == 'Submitted') $resource['mod_completion'] = true;
                        }
                        $cm = get_coursemodule_from_id(false, $mod->id);
                        if (!coursemodule_visible_for_user($cm, $user->id)) {
                            if (!$mod->showavailability) // Mod not visible, and no completion info to show
                                continue;

                            $resource['available'] = 0;
                            $ci = new condition_info($mod);
                            $resource['completion_info'] = $ci->get_full_information();
                        } else
                            $resource['available'] = 1;
                    } else
                        $resource['available'] = 1;

                    $resource['id'] = $mod->id;
                    $resource['name'] = $mod->name;
                    $resource['mod'] = $mod->modname;

                    // Format for mod->icon is: f/type-24
                    $type = substr($mod->icon, 2);
                    $parts = explode('-', $type);
                    $type = $parts[0];
                    $resource['type'] = $type;
                    $resource['display'] = $this->get_display($mod->modname, $mod->instance);

                    // link activity
                    switch ($mod->modname)
                    {
                        case "questionnaire" :
                            $thisurl = $CFG->root.'/index.php?option=com_joomdle&view=feedbackview&module_id='.$mod->id.'&course_id='.$id;
                            break;
                        case "quiz" :
                            $thisurl = $CFG->root.'/index.php?option=com_joomdle&view=quiz&id='.$mod->id.'&course_id='.$id;
                            break;
                        case "page" :
                        case "assignment" :
                        case "folder" :
                        case "messages" :
                        case "assign" :
                        case "scorm" :
                        case "skillsoft" :
                            $thisurl = $CFG->root.'/index.php?option=com_joomdle&view=wrapper&mtype='.$mod->modname.'&course_id='.$id.'&id='.$mod->id;
                            break;
                        default:
                            if ($mod->modname)
                            {
                                $path = '/mod/'.$mod->modname.'/view.php?id=';
                                $thisurl = $CFG->wwwroot.$path.$id;
                                break;
                            }
                            else
                            {
                                $path = '/?a=1';
                                $thisurl = $CFG->wwwroot.$path;
                            }
                            break;
                    }
                    $resource['url'] = urlencode($thisurl);

                    $e[$section->section]['mods'][] = $resource;
                }
            }
        }
        return $e;
    }
    
    function check_old_course($id, $username = null) {
        $mods = $this->get_course_mods($id, $username, 'questionnaire')['coursemods'];
        $t = 0;
        if (is_array($mods)) {
            foreach ($mods as $tema) {
                $resources = $tema['mods'];
                foreach ($resources as $id => $res) {
                    if ($res['mod'] == 'questionnaire') {
                        $t++;
                        $id_ques = $res['id'];
                        break;
                    }
                }
            }
        }
        if ($t == 0) { 
            if ($username) {
                $data_feedback = '{"title":"Add your content title here","description":"Tell us what you think about this course!","rate":"5"}';
                $data_question = '[{"name":"Question 1","type_id":8,"position":1,"content":"Course content is relevant to my learning objectives.","deleted":"n"},{"name":"Question 2","type_id":8,"position":2,"content":"Course content helps to improve my skills.","deleted":"n"},{"name":"Question 3","type_id":8,"position":3,"content":"Course content is well organised and easy to understand.","deleted":"n"},{"name":"Question 4","type_id":8,"position":4,"content":"I learnt something new from this course.","deleted":"n"},{"name":"Question 5","type_id":8,"position":5,"content":"I intend to apply what I have learnt immediately.","deleted":"n"},{"name":"Question 6","type_id":8,"position":6,"content":"I am confident I can apply the skills learnt in this course for my job or personal improvement.","deleted":"n"},{"name":"Question 7","type_id":8,"position":7,"content":"I will recommend a friend to take this course.","deleted":"n"},{"name":"Question 8","type_id":8,"position":8,"content":"I am interested to take another course provided by this Learning Provider.","deleted":"n"},{"name":"Question 9","type_id":2,"position":9,"content":"Please give us suggestions on how to improve this course:","deleted":"n"},{"name":"Question 10","type_id":2,"position":10,"content":"What other related topic would you like us to address in this course?","deleted":"n"}]';

                $this->create_and_add_question_for_feedback($data_feedback, $data_question, $id, $username);                
            }
           
        }
    }
    
    function get_course_outline_content($id, $username, $visiable) {
        global $DB, $CFG;
        $course = $this->get_course_info($id, $username);
        $response = array();
        $response['status'] = true;
        $courseinfo['id'] = $course['remoteid'];
        $courseinfo['fullname'] = $course['fullname'];
        $courseinfo['shortname'] = $course['shortname'];
        $courseinfo['shortname'] = $course['shortname'];
        $courseinfo['summary'] = strip_tags($course['summary']);
        $courseinfo['time_end'] = date('d/m/Y' , ($course['startdate']+$course['enrolperiod']) );
        $courseinfo['image'] = (string)$course['filepath'].$course['filename'];
        $courseinfo['course_survey'] = $course['coursesurvey'];
        $courseinfo['course_facilitated'] = $course['facilitatedcourse'];
        $courseinfo['course_status'] = '';
        $course_status = $this->get_course_status($course['remoteid']);  
        if($course_status) {
            $courseinfo['course_status'] = $course_status;
        }
        
        $response['course']['courseinfo'] = $courseinfo;
        
        $section = $this->get_course_mods_visible($id, $username, $visiable);
        $response['course']['topic'] = $section;
//        print_r($response); die;
        return $response;
        
    }

    // change visible for module
    function update_visible_module_course($module_id, $course_id, $status){
        global $DB, $CFG;
        if($status==1) {
            $data = new stdClass();
            $data->id = $module_id;
            $data->visible = 1;
            $DB->update_record('course_modules', $data);
        }
        if($status==0) {
            $data = new stdClass();
            $data->id = $module_id;
            $data->visible = 0;
            $DB->update_record('course_modules', $data);
        }

    }
    // change multi visable or hide for activity module

    function update_multi_activity_module($activity, $sectionId = 0, $courseid = 0) {
        global $DB, $CFG;
        if(!$DB->record_exists("course", array('id'=>$courseid))) {
            return $response = array(
                'status' => false,
                'message' => 'No course found.',
            );
        }
        if($DB->record_exists("course_request_approve", array('courseid'=>$courseid))) {
            $course_request = $DB->get_record('course_request_approve', array('courseid' => (int)$courseid));
            if($course_request->status == 0) {
                return $response = array(
                    'status' => false,
                    'message' => 'Course is pending approval. You can not update this course',
                );
            }
        }
        $array_module_id_visibled = array();
        $query =
            "SELECT
                cm.id
                FROM
                {$CFG->prefix}course_modules cm
                INNER JOIN {$CFG->prefix}course_sections cs ON cm.section = cs.id
                WHERE
            cm.course = ?
            and cm.visible = ?
            and cm.section != ?
            ";
            $params = array($courseid, 1, $sectionId);

        $module_id_visible =  $DB->get_records_sql($query, $params);
        if ($module_id_visible) {
                foreach ($module_id_visible as $key) {
                $array_module_id_visibled[] = $key->id;
            }
        }
        
        foreach ($activity as $act) {
            // Check module checked
            if (in_array($act['module_id'], $array_module_id_visibled)) {
                continue;
//                return $response = array(
//                    'status' => false,
//                    'message' => 'Module '.$act['module_id'].' has been added in other topic.',
//                );
            }
            
            $data = new stdClass();
            $data->id = $act['module_id'];
            $data->visible = $act['status'];
            $DB->update_record('course_modules', $data);
            $check_table_course_completion_criteria = $DB->get_record('course_completion_criteria', array('moduleinstance' => $data->id));
            if($sectionId!=0 && $courseid!=0) {
                // move module from section 0 to section
                $beforeid = 0;
                $id = $data->id;
                $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
                $cm = get_coursemodule_from_id(null, $id, $course->id, false, MUST_EXIST);
                $modcontext = context_module::instance($cm->id);

                if (!$section_temp = $DB->get_record('course_sections', array('course' => $course->id, 'id' => $sectionId))) {
                    throw new moodle_exception('AJAX commands.php: Bad section ID ' . $sectionId);
                }

                if ($beforeid > 0) {
                    $beforemod = get_coursemodule_from_id('', $beforeid, $course->id);
                    $beforemod = $DB->get_record('course_modules', array('id' => $beforeid));
                } else {
                    $beforemod = NULL;
                }

                moveto_module($cm, $section_temp, $beforemod);
            }

            // status ==1 (check option completion for activity)
            if ($check_table_course_completion_criteria == false && $data->visible == 1) {
                $module_info = $DB->get_record('course_modules', array('id' => $data->id));
                $module_info_name = $DB->get_record('modules', array('id' => $module_info->module));
                $addto_course_completion_criteria = new stdClass();
                $addto_course_completion_criteria->course = $module_info->course;
                $addto_course_completion_criteria->criteriatype = 4;
                $addto_course_completion_criteria->module = $module_info_name->name;
                $addto_course_completion_criteria->moduleinstance = $data->id;
                $DB->insert_record('course_completion_criteria', $addto_course_completion_criteria);

                // Handle overall aggregation.
                $aggdata = array(
                    'course'        => $module_info->course,
                    'criteriatype'  => null
                );
                $aggregation = new completion_aggregation($aggdata);
                $aggregation->setMethod(1);
                $aggregation->save();

                // Handle activity aggregation.

                $aggdata['criteriatype'] = COMPLETION_CRITERIA_TYPE_ACTIVITY;
                $aggregation = new completion_aggregation($aggdata);
                $aggregation->setMethod(1);
                $aggregation->save();

                // Handle course aggregation.

                $aggdata['criteriatype'] = COMPLETION_CRITERIA_TYPE_COURSE;
                $aggregation = new completion_aggregation($aggdata);
                $aggregation->setMethod(1);
                $aggregation->save();

                // Handle role aggregation.

                $aggdata['criteriatype'] = COMPLETION_CRITERIA_TYPE_ROLE;
                $aggregation = new completion_aggregation($aggdata);
                $aggregation->setMethod(1);
                $aggregation->save();

        }

            // status ==0 (not check option completion for activity)
            if ($check_table_course_completion_criteria == true && $data->visible == 0) {
                $deleteto_course_completion_criteria = new stdClass();
                $deleteto_course_completion_criteria->id = $check_table_course_completion_criteria->id;
                $DB->delete_records('course_completion_criteria', array('id' => $deleteto_course_completion_criteria->id));
            }
        }
        $response['status'] = true;
        $response['message'] = 'module updated';
        return $response;
    }
    
    // create feedback and add question for feedback
    function create_and_add_question_for_feedback($data_feedback, $data_question, $courseid, $username) {
        global $DB, $CFG;
        
        // create feedback at section 0
        $sectionnum = 0;
        $feedback = json_decode($data_feedback);
        
        $questions = (array) json_decode($data_question);
        $user = get_complete_user_data('username', $username);
        $context = context_course::instance($courseid);

        // Update time modifi of course
        $course = new object();
        $course->id     =   $courseid;
        $course->timemodified  = time();
        $DB->update_record('course', $course);

        if (!has_capability('moodle/course:manageactivities', $context, $user->id)) {
            $return = array(
                'status' => 0,
                'message' => 'manageactivitiespermission'
            );
            return $return;
        }

        if (!has_capability('mod/questionnaire:addinstance', $context, $user->id)) {
            $return = array(
                'status' => 0,
                'message' => 'questionnaireaddinstancepermission'
            );
            return $return;
        }
        // get data from form create questionnaire

        $json = '{"name":"hung vu",
        "introeditor":{"text":"hung vu","format":"1","itemid":"642086451"},
        "opendate_raw":"2016-12-01 17:55:00",
        "opendate":1480586100,
        "closedate_raw":"2016-12-01 17:55:00",
        "closedate":1480586100,
        "qtype":"0",
        "cannotchangerespondenttype":0,
        "respondenttype":"fullname",
        "resp_view":"1",
        "resume":"0",
        "navigate":"0",
        "autonum":"3",
        "grade":"0",
        "create":"new-0",
        "visible":"1",
        "cmidnumber":"",
        "groupmode":"0",
        "groupingid":"0",
        "availablefrom":0,
        "availableuntil":0,
        "conditiongraderepeats":1,
        "conditiongradegroup":[{"conditiongradeitemid":"0","conditiongrademin":"","conditiongrademax":""}],
        "conditionfieldrepeats":1,
        "conditionfieldgroup":[{"conditionfield":"0","conditionfieldoperator":"contains","conditionfieldvalue":""}],
        "conditioncompletionrepeats":1,
        "conditioncompletiongroup":[{"conditionsourcecmid":"0","conditionrequiredcompletion":"1"}],
        "showavailability":"1",
        "completionunlocked":1,
        "completion":"0",
        "completionsubmit":"0",
        "completionexpected":0,
        "course":1428,
        "coursemodule":0,
        "section":0,
        "module":33,
        "modulename":"questionnaire",
        "instance":0,
        "add":"questionnaire",
        "update":0,
        "return":0,
        "sr":0,
        "submitbutton2":"Save and return to course"}';

        $fromform = json_decode($json);
        $fromform->name = utf8_decode($feedback->title);
        $fromform->opendate = time();
        $fromform->closedate = time();
        $fromform->introeditor = (array)$fromform->introeditor;
        $fromform->introeditor['text'] = utf8_decode($feedback->description);
        $fromform->introeditor['format'] = 2; //plain text
        $fromform->conditiongradegroup[0] = (array)$fromform->conditiongradegroup[0];
        $fromform->conditionfieldgroup[0] = (array)$fromform->conditionfieldgroup[0];
        $fromform->conditioncompletiongroup[0] = (array)$fromform->conditioncompletiongroup[0];
        $fromform->course = $courseid;
        // action add
        $fromform->add = 'questionnaire';

        if (empty($feedback->mid) && !isset($feedback->id)) {
            $fromform->add = 1;
        }
        if($feedback->id) {
            $feeback_instance = get_coursemodule_from_instance('questionnaire', (int)$feedback->id, $courseid);
            $fromform->update = $feeback_instance->id;
            $fromform->coursemodule = $feeback_instance->id;
        }
        else {
            $fromform->update = $feedback->mid;
            $fromform->coursemodule = $feedback->mid;
        }

        require_once($CFG->dirroot . '/course/modlib.php');
        require_once($CFG->dirroot . '/mod/scorm/lib.php');

        $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);

        if (!empty($fromform->update)) {
            $cm = get_coursemodule_from_id('', $fromform->update, 0, false, MUST_EXIST);
            list($cm, $fromform) = update_moduleinfo($cm, $fromform, $course, null, $user->username);

            $questionnaire_id = $DB->get_record('course_modules', array('id' => $fromform->coursemodule), 'instance', MUST_EXIST);
            $survey_id = $DB->get_record('questionnaire', array('id' => $questionnaire_id->instance), 'sid', MUST_EXIST);
            
            $destinationparams = array();
            foreach ($questions as $question) {
                if ($DB->record_exists('questionnaire_question', array('survey_id' => $survey_id->sid, 'position'=>$question->position), '*', MUST_EXIST)) {
                    $fiel_table_question_questionnaire = $DB->get_record('questionnaire_question', array('survey_id' => $survey_id->sid, 'position'=>$question->position), '*', MUST_EXIST);

                    $fiel_table_question_questionnaire->name = $question->name;
                    $fiel_table_question_questionnaire->length = $feedback->rate;
                    $fiel_table_question_questionnaire->survey_id = $survey_id->sid;
                    $fiel_table_question_questionnaire->type_id = $question->type_id;
                    $fiel_table_question_questionnaire->position = $question->position;
                    $fiel_table_question_questionnaire->content = $question->content;
                    $fiel_table_question_questionnaire->deleted = $question->deleted ? $question->deleted : 'n';
                    if ($question->type_id == 8) {
                        if (empty($destinationparams)) {
                            $sourceparams = json_decode($question->params, true);
                            foreach ($sourceparams as $key => $value) {
                                $destinationparams[$key] = $value;
                        }
                        }

                        $fiel_table_question_questionnaire->params = json_encode($destinationparams);
                    }

                    if ($question->type_id == 2) {
                        $fiel_table_question_questionnaire->precise = 1000;
                    }
                    $id_question_questionnaire = $DB->update_record('questionnaire_question', $fiel_table_question_questionnaire);

                    $fiel_table_questionnaire_quest_choice = $DB->get_record('questionnaire_quest_choice', array('question_id' => $fiel_table_question_questionnaire->id), '*', MUST_EXIST);
                    $fiel_table_questionnaire_quest_choice->content = $question->content;
                    $id_questionnaire_quest_choice = $DB->update_record('questionnaire_quest_choice', $fiel_table_questionnaire_quest_choice);
                    
                }
            }
        } else if (!empty($fromform->add)) {
            $add = add_moduleinfo($fromform, $course, null, $user->username);
        $sort = 'id DESC';
        $select = 'course = ' . $courseid;

            $questionnaire_id = $DB->get_record('course_modules', array('id' => $add->coursemodule), 'instance', MUST_EXIST);
            $survey_id = $DB->get_record('questionnaire', array('id' => $questionnaire_id->instance), 'sid', MUST_EXIST);

            $destinationparams = array();
        foreach ($questions as $question) {
            $fiel_table_question_questionnaire = new stdClass();
            $fiel_table_question_questionnaire->name = $question->name;
            $fiel_table_question_questionnaire->length = $feedback->rate;
                $fiel_table_question_questionnaire->survey_id = $survey_id->sid;
                $fiel_table_question_questionnaire->type_id = $question->type_id;
            $fiel_table_question_questionnaire->position = $question->position;
            $fiel_table_question_questionnaire->content = $question->content;
                $fiel_table_question_questionnaire->deleted = $question->deleted ? $question->deleted : 'n';

                if ($question->type_id == 8) {
                    if (empty($destinationparams)) {
                        $sourceparams = json_decode($question->params, true);
                        foreach ($sourceparams as $key => $value) {
                            $destinationparams[$key] = $value;
                    }
                    }
                    $fiel_table_question_questionnaire->params = json_encode($destinationparams);
                }

                if ($question->type_id == 2) {
                $fiel_table_question_questionnaire->precise = 1000;
            }
            $id_question_questionnaire = $DB->insert_record('questionnaire_question', $fiel_table_question_questionnaire);
            $fiel_table_questionnaire_quest_choice = new stdClass();
            $fiel_table_questionnaire_quest_choice->question_id = $id_question_questionnaire;
            $fiel_table_questionnaire_quest_choice->content = $question->content;
            $id_questionnaire_quest_choice = $DB->insert_record('questionnaire_quest_choice', $fiel_table_questionnaire_quest_choice);
        }
        }

        $response = array();
        $response['status'] = 1;
        $response['message'] = 'Question added';
        return $response;
    }
    
    // sort module in section
    function sort_module_in_section_course($listModuleSection,$courseid,$sectionnum,$titleSection){
        global $DB, $CFG;
        $class      = 'resourse';
        $field      = 'move';
        $sectionid = $sectionnum;
        $section = $DB->get_record('course_sections', array('course' => $courseid, 'section' => $sectionid ), '*', MUST_EXIST);
        $this->update_course_section($courseid,2,$titleSection,null,null, $section->id);
        if($listModuleSection != NULL) {
        $moduleInSection = array_map('intval', explode(',', $listModuleSection));
        $t = count($moduleInSection);
        $response = array();
            for ($i = $t - 1; $i >= 0; $i--) {
                $beforeid = 0;
            $id = $moduleInSection[$i];
                if ($i < $t - 1) {
                $beforeid = $moduleInSection[$i + 1];
            }

            $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
            $cm = get_coursemodule_from_id(null, $id, $course->id, false, MUST_EXIST);
            $modcontext = context_module::instance($cm->id);
//            require_capability('moodle/course:manageactivities', $modcontext);
                if (!$section = $DB->get_record('course_sections', array('course' => $course->id, 'section' => $sectionid))) {
                    throw new moodle_exception('AJAX commands.php: Bad section ID ' . $sectionid);
            }

                if ($beforeid > 0) {
                $beforemod = get_coursemodule_from_id('', $beforeid, $course->id);
                    $beforemod = $DB->get_record('course_modules', array('id' => $beforeid));
            } else {
                $beforemod = NULL;
            }

            moveto_module($cm, $section, $beforemod);
        }
        }
        $response['status'] = true;
        $response['message'] = 'module updated';
        return array($response);
    }
    
    function multiple_sort_module_in_section_course($username, $courseid, $sections) {
        global $DB, $CFG;
        
        $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);

        // Update time modifi of course
        $course_info = new object();
        $course_info->id     =   $courseid;
        $course_info->timemodified  = time();
        $DB->update_record('course', $course_info);

        $datas = json_decode($sections);
        $moduleid = array();
        
        foreach($datas as $value) {
            if (!$section = $DB->get_record('course_sections', array('course'=>$course->id, 'id'=>$value->sectionid))) {
//                throw new moodle_exception('AJAX commands.php: Bad section ID '.$sections['sectionnum']);
                $response['status'] = false;
                $response['message'] = 'AJAX commands.php: Bad section ID '.$sections['sectionnum'];

                return $response;
        }
            
            // update module
            if ($value->mods && count($value->mods) > 0) {
            foreach ($value->mods as $mod) {
                $moduleid[] = $mod->moduleid;
                $position[] = $mod->position;
                $cm = get_coursemodule_from_id(null, $mod->moduleid, $course->id, false, MUST_EXIST);
                // update title
                if($cm->name != $mod->moduletitle && $mod->action != 'delete') {
                    $mtype = $DB->get_record('modules', array('id' => $cm->module), 'name', MUST_EXIST);
                    $data = new stdClass();
                    $data->id = $cm->instance;
                    $data->name = $mod->moduletitle;
                    $DB->update_record($mtype->name, $data);
                }

                // arrangement activity
                $query = "SELECT section FROM {$CFG->prefix}course_sections WHERE course =  $course->id AND sequence LIKE '%$mod->moduleid%'";
                $sectionold =  $DB->get_records_sql($query);
                $beforeid = $sectionold[0]->section;
                $cm = get_coursemodule_from_id(null, $mod->moduleid, $course->id, false);
                if ($cm) {
                    $beforemod = get_coursemodule_from_id('', $beforeid, $course->id);
                    $beforemod = $DB->get_record('course_modules', array('id'=>$beforeid));
                    moveto_module($cm, $section, $beforemod);
                }

                //delete module activity
                if($mod->action == 'delete') {

                        // HIDDENT MODULE AND TURN OFF CHECK IN COMPLETE SETTING COURSE

//                        $data = new stdClass();
//                        $data->id = $mod->moduleid;
//                        $data->visible = 0;
//                        $DB->update_record('course_modules', $data);
//                        $check_table_course_completion_criteria = $DB->get_record('course_completion_criteria', array('moduleinstance' => $data->id));
//                        if ($check_table_course_completion_criteria == true ) {
//                            $deleteto_course_completion_criteria = new stdClass();
//                            $deleteto_course_completion_criteria->id = $check_table_course_completion_criteria->id;
//                            $DB->delete_records('course_completion_criteria', array('id' => $deleteto_course_completion_criteria->id));
//                        }

                        // DELETE MODULE AND DELETE MODULE IN COMPLETE SETTING COURSE
                        if ($username) {
                            $user = get_complete_user_data('username', $username);
                            $context = context_course::instance($courseid);
                            $cm = get_coursemodule_from_id('', $mod->moduleid, 0, true, MUST_EXIST);
                            $modcontext = context_module::instance($cm->id);

                            if (!has_capability('moodle/course:manageactivities', $context, $user->id) || !has_capability('moodle/course:manageactivities', $modcontext, $user->id)) {
                                $return = array(
                                    'error' => 1,
                                    'mes' => 'manageactivitiespermission'
                                );
                                return $return;
                }
                            course_delete_module($cm->id);
        }
            }
            }
            }

            // Update section
            if ($value->action == 'sort') {      // update section
                $sectionup = $DB->get_record('course_sections', array('course' => $course->id, 'id' => $value->sectionid ), '*', MUST_EXIST);

                if ($sectionup->section == 0 && $value->sectionnum != $sectionup->section) {
                    $response['status'] = false;
                    $response['message'] = 'We can\'t move section position 0.';

                    return $response;
        }
                
               $sectionup->name = $value->sectionname;
               // $sectionup->section = $value->sectionnum;
                $DB->update_record('course_sections', $sectionup);
                if ((int)$sectionup->section != 0 && (int)$value->sectionnum != 0 && (int)$value->sectionnum != (int)$sectionup->section) {
                    move_section_to($course, $sectionup->section, ((int)$value->sectionnum));
                }
            }
            if ($value->action == 'delete') {           // delete section
                $courseformatoptions = course_get_format($course)->get_format_options();
                if (isset($courseformatoptions['numsections'])) {
                    $sectiond = $DB->get_record('course_sections', array('course' => $course->id, 'id' => $value->sectionid), '*', MUST_EXIST);
                    $i = $sectiond->section + 1;
                    for ($i; $i <= $courseformatoptions['numsections']; $i++) {
                        $sec = $DB->get_record('course_sections', array('course' => $course->id, 'section' => $i), '*', MUST_EXIST);
                        move_section_to($course, $sec->section, ((int)$sec->section - 1));
                    }
                    $sectiond->section = $courseformatoptions['numsections'];
                    $DB->delete_records('course_sections', array('course' => $course->id, 'id' => $sectiond->id));

                    $courseformatoptions['numsections']--;
                    if ($courseformatoptions['numsections'] >= 0) {
                        course_get_format($course)->update_course_format_options(
                            array('numsections' => $courseformatoptions['numsections']));
                    }
                }
            }
//            if ($value->action == 'delete') {           // delete section
//                $courseformatoptions = course_get_format($course)->get_format_options();
//                if (isset($courseformatoptions['numsections'])) {
//                    $sectiond = $DB->get_record('course_sections', array('course' => $course->id, 'id' => $value->sectionid), '*', MUST_EXIST);
//
//                    // check activity in section
//                    $check_mod = $DB->get_record('course_modules', array('course' => $course->id, 'section' => $value->sectionid, 'visible' => 1), '*');
//                    if ($sectiond->section == 0) {
//                        $response['status'] = false;
//                        $response['message'] = '"'.$value->sectionname.'" is the default topic from your course, you cannot delete it.';
//
//                        return $response;
//                    } else if (isset($sectiond->sequence) && $sectiond->sequence != '' && $check_mod && count($check_mod)>0) {
//                        $response['status'] = false;
//                        $response['message'] = 'The topic cannot be deleted because there are activities inside.';
//
//                        return $response;
//                    } else if ($sectiond->section != $courseformatoptions['numsections']) {
//                        $select = 'course = ' . $sectiond->course . ' AND section = ' . $value->sectionnum;
//                        $moduleInSection1 = $DB->get_records_select('course_modules', $select, array(), '', 'id,instance,section');
//                        $certificate_id_course = $DB->get_record('course_modules', array('course' => $course->id, 'module' => 5), 'id,section');
//                        foreach ($moduleInSection1 as $a => $id) {
//                            $beforeid = (int)$certificate_id_course->id;
//                            $id = $a;
//
//                            $cm = get_coursemodule_from_id(null, $id, $course->id, false, MUST_EXIST);
//                            $modcontext = context_module::instance($cm->id);
//
//                            $section0 = 0;
//                            if (!$section_temp = $DB->get_record('course_sections', array('course' => $course->id, 'section' => $section0))) {
//                                throw new moodle_exception('AJAX commands.php: Bad section ID ' . $section0);
//                            }
//
//                            if ($beforeid > 0) {
//                                $beforemod = get_coursemodule_from_id('', $beforeid, $course->id);
//                                $beforemod = $DB->get_record('course_modules', array('id' => $beforeid));
//                            } else {
//                                $beforemod = NULL;
//                            }
//
//                            moveto_module($cm, $section_temp, $beforemod);
//                        }
//                        $i = $sectiond->section + 1;
//                        for ($i; $i <= $courseformatoptions['numsections']; $i++) {
//                            $sec = $DB->get_record('course_sections', array('course' => $course->id, 'section' => $i), '*', MUST_EXIST);
//                            move_section_to($course, $sec->section, ((int)$sec->section - 1));
//        }
//                        $sectiond->section = $courseformatoptions['numsections'];
//                        $DB->delete_records('course_sections', array('course' => $course->id, 'id' => $sectiond->id));
//                    }
//                    $courseformatoptions['numsections']--;
//                    if ($courseformatoptions['numsections'] >= 0) {
//                        course_get_format($course)->update_course_format_options(
//                            array('numsections' => $courseformatoptions['numsections']));
//                    }
//                }
//            }
        }
        $this->check_invisible_activities_and_rebuild_course_cache($courseid);

        $response['status'] = true;
        $response['message'] = 'Activity updated';
        return $response;
    }
    
    function update_title_module($data_moduid_title){
        global $DB, $CFG;
        $moduleIdAndTitle = json_decode($data_moduid_title);
        foreach ($moduleIdAndTitle as $mod){
            $moduleid = $mod->id;
            $moduletitle = $mod->title;
            $module = $DB->get_record('course_modules', array('id' => $moduleid), '*', MUST_EXIST);
            $mtype_id = $module->module;
            $mtype = $DB->get_record('modules', array('id' => $mtype_id), 'name', MUST_EXIST);
            $data = new stdClass();
            $data->id = $module->instance;
            $data->name = $moduletitle;
            $update_title_forum = $DB->update_record($mtype->name, $data);
            }
        $response = array();
        $response['status'] = true;
        $response['message'] = 'module updated';

        return array($response);

    }
    
    // get certificate course
    function get_certificate($username,$courseid){
        global $CFG, $DB, $OUTPUT;

        $template_default = $this->get_certificate_template_default($courseid);
        $tmp_default_id = $template_default['template']['cer_id'];

        require_once("$CFG->libdir/pdflib.php");
        require_once("../../config.php");
        require_once("$CFG->dirroot/mod/certificate/deprecatedlib.php");
        require_once("$CFG->dirroot/mod/certificate/lib.php");

        // If course have many certificate, get certificate max
        $certificate_data = $DB->get_records('certificate',array('course'=>$courseid));
        $certificate_default_id = array();
        foreach ($certificate_data as $certificate_default){
            $certificate_default_id[] = (int)$certificate_default->id;
        }
        $certificate_id_max = max($certificate_default_id);
        $certificate1 = $certificate_data[$certificate_id_max];

        $course = $DB->get_record('course',array('id'=>$courseid));
        $cm = get_coursemodule_from_instance("certificate", (int)$certificate1->id);
        $USER = $DB->get_record('user',array('username'=>$username));
        $context = context_module::instance($cm->id);
        $pdf = new PDF($certificate1->orientation, 'mm', 'A4', true, 'UTF-8', false);
        $certrecord = certificate_get_issue($course, $USER, $certificate1, $cm);
        $certname = rtrim($certificate1->name, '.');
        $filename = clean_filename(str_replace(array('&amp;', '&'), get_string('ampersand', 'totara_core'),
                format_string(strip_tags($course->shortname . '_' . $certname), true))) . '.pdf';
        if ($certificate1->savecert == 1) {
            // PDF contents are now in $file_contents as a string
            $file_contents = $pdf->Output('', 'S');
            certificate_save_pdf($file_contents, $certrecord->id, $filename, $context->id);
        }
        if ($certificate1->delivery == 0) {
//            $pdf->Output($filename, 'I'); // open in browser
        } elseif ($certificate1->delivery == 1) {
//            $pdf->Output($filename, 'D'); // force downloacd when create
        } elseif ($certificate1->delivery == 2) {
            certificate_email_student($course, $certificate1, $certrecord, $context);
//            $pdf->Output($filename, 'I'); // open in browser
//            $pdf->Output('', 'S'); // send
        }

        $username = utf8_decode($username);
        $username = strtolower($username);

        $user = get_complete_user_data('username', $username);
        $user_id = $user->id;

        $cursos = enrol_get_users_courses($user->id, true);

        if (!count($cursos))
            return array();

        $c_ids = array();
        foreach ($cursos as $curso) {
            $c_ids[] = $curso->id;
    }
        $ids_str = implode(',', $c_ids);

    /*
          // Code for old version of certificate module
          $certs = $DB->get_records_sql("SELECT  c.name, c.id, ci.certdate
          FROM {$CFG->prefix}certificate c
          LEFT JOIN {$CFG->prefix}certificate_issues ci ON c.id = ci.certificateid
          WHERE ci.userid = $user_id
          AND c.course  in ($ids_str)
          ORDER BY ci.certdate DESC");
         */
        $sql_pr = "SELECT  cc.courseid as categoryname
                FROM mdl_prog AS p
                INNER JOIN mdl_prog_assignment AS pu ON p.id = pu.programid 
                INNER JOIN mdl_course_categories AS cat ON p.category= cat.id 
                INNER JOIN mdl_prog_courseset AS css ON css.programid= p.id 
                INNER JOIN mdl_prog_courseset_course AS cc ON cc.coursesetid= css.id
                WHERE  pu.assignmenttypeid =$user_id";
        $sql = "select course from {$CFG->prefix}course_completions where userid=$user_id";
        $certs = $DB->get_records_sql("SELECT  *
                FROM {$CFG->prefix}certificate c
                LEFT JOIN {$CFG->prefix}certificate_issues ci ON c.id = ci.certificateid
                WHERE ci.userid = $user_id
                AND (c.course = ( $courseid) OR c.course in ($sql_pr))
                                ORDER BY ci.timecreated DESC");

        // $certs = $DB->get_records_sql("SELECT  *
        //         FROM {$CFG->prefix}certificate c
        //         LEFT JOIN {$CFG->prefix}certificate_issues ci ON c.id = ci.certificateid
        //         WHERE ci.userid = $user_id
        //         AND c.course in ($sql)
        //         AND (c.course in ( $ids_str) OR c.course in ($sql_pr))
        //                         ORDER BY ci.timecreated DESC");

        $c = array();

        foreach ($certs as $cert) {
            $coursemodule = get_coursemodule_from_instance("certificate", $cert->certificateid);
            $certificate['id'] = $coursemodule->id;
            $certificate['name'] = $cert->name;
            $certificate['date'] = $cert->timecreated;

            $certificate['userid'] = $cert->userid;
            $certificate['courseid'] = $cert->course;

            $course = $this->get_course_info($cert->course , $username);
            $certificate['coursename'] = $course['fullname'];
            $certificate['certificateid'] = $cert->certificateid;


//            $userrecord = $DB->get_record('user', array('id' => $cert->userid));
            //  $certificate['fullname'] = $user->firstname.' '.$user->lastname;

            //$certificate['avatar'] = $OUTPUT->user_picture($user, array('courseid'=>$coursemodule->id, 'size'=>'20', 'class'=>'profilepicture'));

            //    User Files
            $context = context_module::instance($cm->id);
//            var_dump($context);die;

            $output = '';

//            $certrecord = $DB->get_record('certificate_issues', array('userid' => $cert->userid));
            $certrecord = $DB->get_records('certificate_issues', array('userid' => $cert->userid, 'certificateid'=> $cert->certificateid));

            $fs = get_file_storage();
            $browser = get_file_browser();
            $component = 'mod_certificate';
            $filearea = 'issue';

            $filees = Array();
            foreach ($certrecord as $value) {
                $files = $fs->get_area_files($context->id, $component, $filearea, $value->id);
                $filees = $filees + $files;
            }

            if (!empty($filees)) {
                foreach ($filees as $file) {
                    $filesize = $file->get_filesize();
                    if ($filesize == 0) {
                        continue;
                    }
                    $filename = $file->get_filename();
                    $mimetype = $file->get_mimetype();

                    $itemid = $file->get_itemid();
                    //                $link = file_encode_url($CFG->wwwroot.'/pluginfile.php', '/'.$context->id.'/mod_certificate/issue/'.$certrecord->id.'/'.$filename);
                    $link = file_encode_url($CFG->wwwroot.'/pluginfile.php', '/'.$context->id.'/mod_certificate/issue/'.$itemid.'/'.$filename);
                    $link_url = file_encode_url($CFG->wwwroot.'/mod/certificate/view.php?id='.$cm->id.'&action=get');
                    // $output = '<img src="'.$OUTPUT->pix_url(file_mimetype_icon($file->get_mimetype())).'" height="16" width="16" alt="'.$file->get_mimetype().'" />&nbsp;'.
                    //     '<a href="'.$link.'" >'.s($filename).'</a>';

                }

                // $output .= '<br />';
                // $output = '<div class="files">'.$output.'</div>';

                $certificate['downloadFiles'] =  $link;
                $certificate['url'] = $link_url;
            } else {
                $certificate['downloadFiles'] =  '';
                $certificate['url'] = '';
            }
//            $course = $DB->get_record('course', array('id'=> $cm->course));
//            $grade = certificate_get_grade($cert, $course, $cert->userid);


            $c[$cert->certificateid] = $certificate;
        }
        return $c;

    }

    /*
     *get certificate template default
     */
    function get_certificate_template_default($courseid) {
        global $DB, $CFG;
        $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
        $cer_template = $DB->get_record('certificate', array('course' => $course->id));
        $response = array();
        $data = array();
        if($cer_template) {
            $data['cer_id'] = $cer_template->id;
            $data['cer_name'] = $cer_template->borderstyle;
            $response['status'] = true;
            $response['message'] = 'success.';
            $response['template'] = $data;
            return $response;
        } else {
            $response['status'] = false;
            $response['message'] = 'failed.';
            $response['template'] = $data;
            return $response;
        }
    }
    
    function set_default_template_certificate($cer_id, $courseid, $temp_name, $username) {
        global $DB, $CFG;
        
        $username = utf8_decode ($username);
        $username = strtolower ($username); 
        if ($username)
            $user = get_complete_user_data ('username', $username);
        
        // check permission
//        $roles = $this->get_user_role($courseid, $user->username);
        $userRoles = $this->get_user_role($courseid, $user->username);
        if ($userRoles['status']) {
            $roles = $userRoles['roles'];
        } else {
            $roles = array();
        }
        $permission = false;
        foreach ($roles as $role) {
            if($role['hasPermission']) {
                $permission = true;
            }
        }
        if(!$permission) {
            $response['status'] = false;
            $response['message'] = 'You don\'t have a permission for action.';
            return $response;
        }
        $cer_template = $DB->get_record('certificate', array('id' => $cer_id));
        if(!$cer_template) {
            $response['status'] = false;
            $response['message'] = 'Certficate can not be found.';
            return $response;
        }

        // Update time modifi of course
        $course = new object();
        $course->id     =   $courseid;
        $course->timemodified  = time();
        $DB->update_record('course', $course);
        
        $data = new stdClass();
        $data->id = $cer_template->id;
        $data->course = $cer_template->course;
        $data->borderstyle = $temp_name;
//        $data->borderstyle = 'parenthesis_certificate_default.png';
        $DB->update_record('certificate', $data);
        
        $response['status'] = true;
        $response['message'] = 'Certficate template have been set.';
        return $response;
    }

    function my_certificates ($username, $type = 'normal') {
        switch ($type)
        {
            case "normal":
                return $this->my_certificates_normal ($username);
                break;
            case "simple":
                return $this->my_certificates_simple ($username);
                break;
        }
    }

    function my_certificates_normal ($username) {

        global $CFG, $DB, $OUTPUT;


        $username = utf8_decode($username);
        $username = strtolower($username);

        $user = get_complete_user_data('username', $username);
        $user_id = $user->id;


        $cursos = enrol_get_users_courses($user->id, true);

        if (!count($cursos))
            return array();

        $c_ids = array();
        foreach ($cursos as $curso) {
            $c_ids[] = $curso->id;
        }
        $ids_str = implode(',', $c_ids);

    /*
          // Code for old version of certificate module
          $certs = $DB->get_records_sql("SELECT  c.name, c.id, ci.certdate
          FROM {$CFG->prefix}certificate c
          LEFT JOIN {$CFG->prefix}certificate_issues ci ON c.id = ci.certificateid
          WHERE ci.userid = $user_id
          AND c.course  in ($ids_str)
          ORDER BY ci.certdate DESC");
         */
        $sql_pr = "SELECT  cc.courseid as categoryname
                FROM mdl_prog AS p
                INNER JOIN mdl_prog_assignment AS pu ON p.id = pu.programid 
                INNER JOIN mdl_course_categories AS cat ON p.category= cat.id 
                INNER JOIN mdl_prog_courseset AS css ON css.programid= p.id 
                INNER JOIN mdl_prog_courseset_course AS cc ON cc.coursesetid= css.id
                WHERE  pu.assignmenttypeid =$user_id";
        $sql = "select course from {$CFG->prefix}course_completions where userid=$user_id and status = 50";

        $certs = $DB->get_records_sql("SELECT  *
                FROM {$CFG->prefix}certificate c
                LEFT JOIN {$CFG->prefix}certificate_issues ci ON c.id = ci.certificateid
                WHERE ci.userid = $user_id
                AND (c.course in ( $ids_str) OR c.course in ($sql_pr))
                                ORDER BY ci.timecreated DESC");
        
        // $certs = $DB->get_records_sql("SELECT  *
        //         FROM {$CFG->prefix}certificate c
        //         LEFT JOIN {$CFG->prefix}certificate_issues ci ON c.id = ci.certificateid
        //         WHERE ci.userid = $user_id
        //         AND c.course in ($sql)  
        //         AND (c.course in ( $ids_str) OR c.course in ($sql_pr))
        //                         ORDER BY ci.timecreated DESC");

        $c = array();

        foreach ($certs as $cert) {
            $coursemodule = get_coursemodule_from_instance("certificate", $cert->certificateid);
            $certificate['id'] = $coursemodule->id;
            $certificate['name'] = $cert->name;
            $certificate['date'] = $cert->timecreated;

            $certificate['userid'] = $cert->userid;
            $certificate['courseid'] = $cert->course;

            $course = $this->get_course_info($cert->course , $username);
            $certificate['coursename'] = $course['fullname'];
            $certificate['certificateid'] = $cert->certificateid;

            // Trung modified 25-06 - not use these value
            // $certificate['orientation'] = $cert->orientation;
            // $certificate['borderstyle'] = $cert->borderstyle;
            // $certificate['bordercolor'] = $cert->bordercolor;
            // $certificate['printwmark'] = $cert->printwmark;
            // $certificate['printdate'] = $cert->printdate;
            // $certificate['datefmt'] = $cert->datefmt;
            // $certificate['printnumber'] = $cert->printnumber;
            // $certificate['printgrade'] = $cert->printgrade;
            // $certificate['gradefmt'] = $cert->gradefmt;
            // $certificate['printoutcome'] = $cert->printoutcome;
            // $certificate['printhours'] = $cert->printhours;
            // $certificate['printteacher'] = $cert->printteacher;
            // $certificate['customtext'] = $cert->customtext;
            // $certificate['printsignature'] = $cert->printsignature;
            // $certificate['printseal'] = $cert->printseal;
            // $certificate['code'] = $cert->code;
            // end

//            $userrecord = $DB->get_record('user', array('id' => $cert->userid));
            //  $certificate['fullname'] = $user->firstname.' '.$user->lastname;

            //$certificate['avatar'] = $OUTPUT->user_picture($user, array('courseid'=>$coursemodule->id, 'size'=>'20', 'class'=>'profilepicture'));

            //    User Files
            $cm = get_coursemodule_from_id('certificate', $coursemodule->id);

            $context = context_module::instance($cm->id);

            $output = '';

//            $certrecord = $DB->get_record('certificate_issues', array('userid' => $cert->userid));
            $certrecord = $DB->get_records('certificate_issues', array('userid' => $cert->userid, 'certificateid'=> $cert->certificateid));

            $fs = get_file_storage();
            $browser = get_file_browser();
            $component = 'mod_certificate';
            $filearea = 'issue';

            $filees = Array();
            foreach ($certrecord as $value) {
                $files = $fs->get_area_files($context->id, $component, $filearea, $value->id);
                $filees = $filees + $files;
            }

            if (!empty($filees)) {
                foreach ($filees as $file) {
                    $filesize = $file->get_filesize();
                    if ($filesize == 0) {
                        continue;
                    }
                    $filename = $file->get_filename();
                    $mimetype = $file->get_mimetype();

                    $itemid = $file->get_itemid();
                    //                $link = file_encode_url($CFG->wwwroot.'/pluginfile.php', '/'.$context->id.'/mod_certificate/issue/'.$certrecord->id.'/'.$filename);
                    $link = file_encode_url($CFG->wwwroot.'/pluginfile.php', '/'.$context->id.'/mod_certificate/issue/'.$itemid.'/'.$filename);
                    $link_url = file_encode_url($CFG->wwwroot.'/mod/certificate/view.php?id='.$cm->id.'&action=get');
                    // $output = '<img src="'.$OUTPUT->pix_url(file_mimetype_icon($file->get_mimetype())).'" height="16" width="16" alt="'.$file->get_mimetype().'" />&nbsp;'.
                    //     '<a href="'.$link.'" >'.s($filename).'</a>';

                }

                // $output .= '<br />';
                // $output = '<div class="files">'.$output.'</div>';

                $certificate['downloadFiles'] =  $link;
                $certificate['url'] = $link_url;
            } else {
                $certificate['downloadFiles'] =  '';
                $certificate['url'] = '';
            }
//            $course = $DB->get_record('course', array('id'=> $cm->course));
//            $grade = certificate_get_grade($cert, $course, $cert->userid);


            $c[$cert->certificateid] = $certificate;
        }
        return $c;
    }

    function my_certificates_simple ($username) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $user = get_complete_user_data ('username', $username);
        $user_id = $user->id;


        $cursos = enrol_get_users_courses ($user->id, true);

        if (!count ($cursos))
            return array ();

        $c_ids = array ();
        foreach ($cursos as $curso)
        {
            $c_ids[] = $curso->id;
    }
        $ids_str = implode (',', $c_ids);

        $certs = $DB->get_records_sql("SELECT  c.name, c.id, ci.timecreated as certdate
                FROM {$CFG->prefix}simplecertificate c
                LEFT JOIN {$CFG->prefix}simplecertificate_issues ci ON c.id = ci.certificateid
                WHERE ci.userid = $user_id
                AND c.course  in ($ids_str)
                ORDER BY ci.timecreated DESC");

        $c = array ();
        foreach ($certs as $cert)
        {
            $coursemodule = get_coursemodule_from_instance ("simplecertificate", $cert->id);
            $certificate['id'] =  $coursemodule->id;
            $certificate['name']  = $cert->name;
            $certificate['date']  = $cert->certdate;

            $c[] = $certificate;
        }

        return $c;
    }


    /*
         * Get progress of activity and resource
         */
    function get_mod_progress($id, $username) {
        global $DB, $CFG;
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        require_once($CFG->dirroot.'/course/modlib.php');
        require_once($CFG->dirroot.'/mod/assign/lib.php');
        if ($username)
            $user = get_complete_user_data ('username', $username);

        /*
                 *  get course view
                 */
        $e = array ();
        $sql = "SELECT cm.id, COUNT('x') AS numviews, MAX(time) AS lasttime
                          FROM {course_modules} cm
                               JOIN {modules} m ON m.id = cm.module
                               JOIN {log} l     ON l.cmid = cm.id
                         WHERE cm.course = ? AND l.action LIKE 'view%' AND m.visible = 1 AND l.userid = $user->id
                      GROUP BY cm.id";
        $views = $DB->get_records_sql($sql, array($id));

        $modinfo = get_fast_modinfo($id);
        $sections = $modinfo->get_section_info_all();
        $courseformatoptions = course_get_format($id)->get_format_options();

        $mods = get_fast_modinfo($id)->get_cms();

        $coursename = $modinfo->get_course()->fullname;

        $e['status'] = true;
        $e['message'] = 'success.';
        $e['coursename'] = $coursename;

        // Update by Luyen Vu - get learner
        $learneridarr = array();
        $countlearner = 0;
        $userRole = $this->get_user_role($id);

        if ($userRole['status']) {
            $userroles = $userRole['roles'];
        } else {
            $userroles = array();
        }
        if (!empty($userroles)) {
            foreach ($userroles as $userrole) {
                $countrole = count(json_decode($userrole['role']));
                foreach (json_decode($userrole['role']) as $role) {
                    // roleid = 5 (student)
                    // roleid = 4 (teacher)
                    // roleid = 1 (manager)
                    if ($role->roleid == 5 && $countrole == 1) {
                        $countlearner++;
                        $user_l = get_complete_user_data ('username', $userrole['username']);
                        $learneridarr[] = $user_l->id;
                    }
                }

            }
        }
        $e['count_learner'] = $countlearner;

        if (count($learneridarr) > 1) {
            $learnerid = implode(',', $learneridarr);
            $wh = ' AND userid IN ('.$learnerid.')';
        } else if (count($learneridarr) == 1) {
            $wh = ' AND userid = '.$learneridarr[0];
        } else {
            $wh = '';
        }
        if ($wh != '') {
            $query_com = "SELECT userid FROM {$CFG->prefix}course_completions WHERE course = $id AND status = 50 $wh";
            $result_com = $DB->get_records_sql($query_com);

            $e['count_completed'] = count($result_com);
        } else {
            $e['count_completed'] = 0;
        }
         $dataquizs = $this->get_course_quizes($id,$username);
        $grades = $this->get_course_grades_by_category ($id, $username);
        if (!empty($grades)) {
            $e['gradeid']  = (int)$grades[0]['gradeid'];
            $e['gradeitemid'] = (int)$grades[0]['gradeitemid'];
            $e['grademax'] = (float)$grades[0]['grademax'];
            $e['gradefeedback'] = (string)$grades[0]['gradefeedback'];
            $e['finalgrade'] = (float)$grades[0]['finalgrade'];
            $e['finalgradeletters'] = (string)$grades[0]['finalgradeletters'];
            $e['hidden'] = (int)$grades[0]['hidden'];
            $e['timemodified'] = (int)$grades[0]['timemodified'];
            if($grades[0]['usermodified'] != null){
                $e['gradeoffacitator'] = true;
            } else {
                $e['gradeoffacitator'] = false;
            }
             $letter_option = array();
             
            $e['gradeoptionsletters'] = $grades[0]['gradeoptionsletters'];
           
        }
        $e['sections'] = array();

        foreach ($sections as $section)
        {

            if ((!$section->visible && $section->section != 0) || $section->section > $courseformatoptions['numsections'])
                continue;

            $sectionmods = explode(",", $section->sequence);
            $e['sections'][$section->section]['sectionname'] = $section->name ? $section->name : 'Topic Title Here';
            $e['sections'][$section->section]['mods'] = array();
            $imods = 0;
            foreach ($sectionmods as $modnumber) {
                if (empty($mods[$modnumber])) {
                    continue;
                }
                $mod = $mods[$modnumber];
                $cm = get_coursemodule_from_id(false, $mod->id);
                if ($mod->modname == 'forum' || $mod->modname == 'certificate' || $mod->modname == 'questionnaire') {
                    continue;
                }
                $imods++;
                // update display activity not grade

//                if (is_null($cm->completiongradeitemnumber)) {
//                    continue;
//                }
                $resource = array();
                if ($mod->visible == 1) {
                    $resource['completionusegrade'] = is_null($cm->completiongradeitemnumber) ? 0 : 1;
                    $resource['mod_completion'] = false;
                    $resource['mod_completion_date'] = '';
                    if ($username) {
                        /*
                         * get activity completion
                         */
                        //    if ($mod->modname == 'quiz') {
                        //      $mod_completion = $DB->get_record('quiz_attempts', array('quiz' => $mod->instance, 'userid' => $user->id, 'state' => 'finished'));
                        // }
                        // else{
                        $mod_completion = $DB->get_record('course_modules_completion', array('coursemoduleid' => $mod->id, 'userid' => $user->id, 'completionstate' => '1' ));
                        // }
                        if ($mod_completion) {
                            $resource['mod_completion'] = true;
                            $resource['mod_completion_date'] = date('d/m/Y', $mod_completion->timemodified);
                        }

                        if ($mod->modname == 'assign') {
                            $assign = $DB->get_record('assign_submission', ['assignment' => $mod->instance, 'userid' => $user->id]);
                            if ($assign && $assign->status == 'submitted') $resource['mod_completion'] = true;
                        }

                        if (!coursemodule_visible_for_user($cm, $user->id)) {
                            if (!$mod->showavailability) // Mod not visible, and no completion info to show
                                continue;

                            $resource['available'] = 0;
                        } else
                            $resource['available'] = 1;
                    } else
                        $resource['available'] = 1;

                    // Check learner completed
                    if ($wh != '') {
                        $result2 = $DB->get_records_sql("SELECT userid FROM {$CFG->prefix}course_modules_completion WHERE coursemoduleid = $mod->id AND completionstate = 1 $wh");

                        $resource['totalcompleted'] = count($result2);

                        if ($mod->modname == 'assign') {
                            $assigns = $DB->get_records('assign_submission', ['assignment' => $mod->instance]);

                            if (!empty($assigns)) {
                                foreach ($assigns as $key => $value) {
                                    if (!$DB->record_exists('course_modules_completion', array('coursemoduleid' =>
                                        $mod->id, 'userid' => $value->userid))) {
                                        $resource['totalcompleted']++;
                                    }
                                }
                            }
                        }
                    } else {
                        $resource['totalcompleted'] = 0;
                    }

                    $resource['id'] = $mod->id;
                    $resource['name'] = $mod->name;
                    $resource['mod'] = $mod->modname;

                    // Format for mod->icon is: f/type-24
                    $type = substr($mod->icon, 2);
                    $parts = explode('-', $type);
                    $type = $parts[0];
                    $resource['type'] = $type;
                    $resource['mod_lastaccess'] = '';
                    if (isset($views[$cm->id]->lasttime)) {
                        /*$timeago = format_time(time() - $views[$cm->id]->lasttime);
                            $resource['mod_lastaccess'] = userdate($views[$cm->id]->lasttime)." ($timeago)";*/
                        $resource['mod_lastaccess'] = date('d/m/Y', $views[$cm->id]->lasttime);
                    }
                    //In forum, type is unused, so we use it for forum type: news/general
                    /*if ($mod->modname == 'forum')
                    {
                        $cm = get_coursemodule_from_id('forum', $mod->id);
                        if ($cm->instance == $news_forum_id)
                            $resource['type'] = 'news';
                    }*/
                    // get grade of activity

                    // Get course total

                    $id = (int)$id;
                    $cat_item = $DB->get_record('grade_items', array('courseid' => $id, 'itemmodule' => $mod->modname, 'iteminstance' => $cm->instance));
                    $query1 = "SELECT g.finalgrade,g.rawgrademax, g.feedback, g.id,g.hidden
                          FROM {$CFG->prefix}grade_grades g
                         WHERE g.itemid = ?
                           AND g.userid =  ?";
                    $params = array($cat_item->id, $user->id);
                    $grade = $DB->get_record_sql($query1, $params);

                    if ($cat_item->id) {
                        $grade_item = grade_item::fetch(array('id'=>$cat_item->id, 'courseid'=>$id));
                        $gradedisplaytype = $grade_item->get_displaytype();
                        $grade_letter = grade_format_gradevalue($grade->finalgrade, $grade_item, true, $gradedisplaytype, null);
                        $resource['finalgradeletters'] = $grade_letter;

                    }
                    $resource['gradeid'] = (int)$grade->id;
                    $resource['gradeitemid'] = (int)$cat_item->id;
                    $resource['finalgrade'] = (float)$grade->finalgrade;
                     $resource['hidden'] = (int)$grade->hidden;
                    $resource['grademax'] = (float)$grade->rawgrademax;
                    $resource['feedback'] = (string)$grade->feedback;
                    $letter_option = array();
                    if($mod->modname == 'assign'){
                        $cm = get_coursemodule_from_id('', $mod->id, 0, false, MUST_EXIST);
                        $module = $DB->get_record('modules', array('id'=>$cm->module), '*', MUST_EXIST);

                        // Get information assignment(*)
                        $data = $DB->get_record($module->name, array('id'=>$cm->instance), '*', MUST_EXIST);
                        $submission = get_submission($data->id,$user->id);
                        if (!empty($submission)) {
                            foreach ($submission as $sb) {
                                if ($sb->status == 'submitted') {
                                    $submission_status = 'Submitted';
                                } else {
                                    $submission_status = 'Not Yet Submitted';
                                }
                            }
                        } else {
                            $submission_status = 'Not Yet Submitted';
                        }
                        $resource['submission_status'] = $submission_status;
                    }
                    if ($mod->modname == 'quiz') {
                        
                        foreach ($dataquizs as $key => $value) {
                            foreach ($value['quizes'] as $k => $v) {
                                if ($v['id'] != $mod->id) continue; else $dataquiz = $v;
                            }
                        }
                        $intro_quiz = $dataquiz['intro'];
                        $resource['attempt_quiz']  = $dataquiz['attempt'];
                        $numOfAttempts = $dataquiz['numOfAttempts'];
                        $attempts = json_decode($dataquiz['attempts'], true);
                        $quizFinished = true;
                        $firstAttempt = false;
                        $inProgress = false;
                        $reAttempt = false;
                        if (!empty($attempts)) {
                            if (count($attempts) == 1) $firstAttempt = true;
                            foreach ($attempts as $key => $value) {
                                if ($value['state'] != "finished") {
                                    $quizFinished = false;
                                    $inProgress = true;
                                }
                            }
                            if (count($attempts) != $numOfAttempts) {
                                $quizFinished = false;
                                if (!$inProgress) $reAttempt = true;
                            }
                        } else {
                            $quizFinished = false;
                            $firstAttempt = true;
                        }
                        $resource['firstAttempt'] = $firstAttempt;
                        $resource['inProgress'] = $inProgress;
                        $resource['quizFinished'] = $quizFinished;
                        
                        $quizgrades = $DB->get_records('quiz_grade_letters', array('moduleid' => $mod->id));
                        $quiz = $DB->get_record('quiz_grades', array('quiz' => $mod->instance,'userid' => $user->id));
                        $resource['finalgrade'] = (float)$quiz->grade;

                        if ($quizgrades) {
                            $resource['gradingscheme'] = 1;
                            foreach ($quizgrades as $letter) {
                                $letter_scheme = array();
                                $letter_scheme['value'] = (int) $letter->lowerboundary;
                                $letter_scheme['name'] = $letter->letter;
                                $letter_option[] = $letter_scheme;
                            }
                        } else
                            $resource['gradingscheme'] = 0;
                    }

                    $resource['lettergradeoption'] = $letter_option ;
                    $e['sections'][$section->section]['mods'][] = $resource;
                }
            }
            if ($section->section == 0 && $imods == 0) {
                unset($e['sections'][$section->section]);
            }
        }
        return $e;
    }
    
     /*
     *  KV team: new function get user role
     */
    
    function get_user_role($id, $username = null) {
        global $DB, $CFG, $PAGE;
        
        if(!$DB->record_exists('course', array('id' => $id))) {
            $result['status'] = 0;
            $result['message'] = 'Course not found.';
            $result['roles'] = array();
            return $result;
        }
        $users = array();
        if ($username) {
            $username = utf8_decode ($username);
            $username = strtolower ($username);
            $user = get_complete_user_data ('username', $username);
            $users[$user->id] = $user->id;
        } else {
            $course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);
            $manager = new course_enrolment_manager($PAGE, $course);
            $enroledUsers = $manager->get_users('firstname', 'asc', 0, 99999);

            foreach ($enroledUsers as $v) {
                $users[$v->id] = $v->id;
            }
            
        }

        $context = context_course::instance($id);
        $res = array();
        $result = array();

        if ($users) {
            foreach ($users as $u) {
                $roles = get_user_roles($context, $u, false);
                $roles_user = array();
                $hasPermission = false;
                foreach ($roles as $role) {
                        $user_role['roleid'] = $role->roleid;
                        if(in_array($role->roleid, array(1, 2, 3))) $hasPermission = true;
                        $user_role['sortname'] = $role->shortname;
                        $roles_user[] = $user_role;
                    }

                $user = $DB->get_record('user', array('id'=>$u), 'username, id', MUST_EXIST);
                $ob = array();
                $ob['username'] = $user->username;

                if ($hasPermission)
                    $ob['hasPermission'] = 1;
                else {
                    $creator = $DB->get_record('course', array('id'=>$id), 'creator', MUST_EXIST);
                    if ($creator->creator == $user->id) {
                        $ob['hasPermission'] = 1;
                        if (array_key_exists($user->username, get_enrolled_users($context, '' , 0, 'username'))) {
                            role_assign(2, $user->id, $context);
                        } else {
                            $this->enrol_user($user->username, $id, 2); //2 - Course Creator
                        }
                        $roles_user[] = array('roleid' => 2, 'sortname' => 'coursecreator');
                    } else $ob['hasPermission'] = 0;
                }

                $ob['role'] = json_encode($roles_user);

                $res[] = $ob;
            }
            $result['status'] = 1;
            $result['message'] = 'Success.';
            $result['roles'] = $res;
        } else {
            $result['status'] = 0;
            $result['message'] = 'Username not exist.';
            $result['roles'] = array();
        }

        return $result;
    }
    
    function get_user_category_role($categoryid, $username, $all = 0) {
        global $DB, $CFG;
        
        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $user = get_complete_user_data ('username', $username);
        
        // check permission and get user manage of category
        $catcontext = context_coursecat::instance($categoryid, IGNORE_MISSING);
        $category = $DB->get_record('course_categories', array('id'=>$categoryid), '*', MUST_EXIST);
        // get manage role of category
        $roles = $DB->get_records('role_assignments', array('contextid' => $catcontext->id));
        $role_assign = array();
        $roles_assign = array();
        
        if($roles) {
            foreach ($roles as $role) {
                $rol = array();
                $rolename = $DB->get_record('role', array('id'=>$role->roleid));
                $rol['roleid'] = $rolename->id;
                $rol['rolename'] = $rolename->shortname;
                if ($all) {
                    if ($role->userid == $user->id) $role_assign[] = $rol; 
                    else {
                        $rol['username'] = $DB->get_record('user', array('id' => $role->userid), 'username')->username;
                        $roles_assign[] = $rol; 
                    }
                } else {
                    if ($role->userid == $user->id) $role_assign[] = $rol; 
                } 
            }
        } else {
            $role_assign['roleid'] = 0;
            $role_assign['rolename'] = '';
        }
        $response['status'] = true;
        $response['message'] = 'success';
        $response['role'] = $role_assign;
        $response['roles'] = json_encode($roles_assign);
        $response['name'] = $category->name;
        return $response;
    }
    
    function set_user_category_role($username, $categoryid, $data) {
        global $DB, $CFG;
        if (empty($categoryid)) {
            return $response = array(
                'status' => false,
                'message' => 'Category can not be empty.',
            );
        }
        if (empty($username)) {
            return $response = array(
                'status' => false,
                'message' => 'Username can not be empty.',
            );
        }
        $username = utf8_decode($username);
        $username = strtolower($username);
        $user = get_complete_user_data('username', $username);

        // check permission and get user manage of category
        $catcontext = context_coursecat::instance($categoryid, IGNORE_MISSING);
        $rolemanages = $DB->get_records('role_assignments', array('contextid' => $catcontext->id));

        if ($rolemanages) {
            foreach ($rolemanages as $rol) {
                $role_arr[] = $rol->userid;
            }
        }
        if (!in_array($user->id, $role_arr)) {
            return $response = array(
                'status' => false,
                'message' => 'You don\'t have a permission.',
            );
        }
        $json_data = json_decode($data);

        if ($json_data) {
            foreach ($json_data as $key => $value) {
                if ($value->user) {
                    $uname = $value->user;
                    $u = get_complete_user_data('username', $uname);
                    if (!$u) {
                        $response = array(
                            'status' => false,
                            'message' => 'notexisteduser'
                        );
                        return $response;
                    }
                    if (empty($value->roleid)) {
                        continue;
                    }
                    $roleid = $value->roleid;

                    if (isset($value->status)) {
                        if ($value->status) {
                            if (isset($value->changeOwner) && $value->changeOwner) {
                                $category = $DB->get_record("course_categories", array("id" => $categoryid), 'id, parent, userowner');
                                $category->userowner =  $u->username;
                                $DB->update_record('course_categories', $category);
                            }
                            role_assign($roleid, $u->id, $catcontext->id);
                        } else {
                            role_unassign($roleid, $u->id, $catcontext->id);
                        }
                    } else
                        role_assign($roleid, $u->id, $catcontext->id);
                }
            }
        }
        return $reponse = array(
            'status' => true,
            'message' => 'success',
        );
    }

    function set_user_role($act, $courseid, $username, $data) {
        global $DB, $CFG, $PAGE;
        $username = utf8_decode ($username);
        $username = strtolower ($username);

        if ($username) {
            $user = get_complete_user_data ('username', $username);
            $userid = $user->id;
        } else {
            $user = null;
            $userid = null;
        }

        $context = context_course::instance($courseid, MUST_EXIST);
        if (!has_capability('moodle/course:enrolreview', $context, $userid)) {
            $return = array(
                'status' => 0,
                'message' => 'nopermission'
            );
            return $return;
        }

        $data = json_decode($data);
        $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
        $manager = new course_enrolment_manager($PAGE, $course);

                foreach ($data as $v) {
                    $uname = $v->frname;
                    $u = get_complete_user_data('username', $uname);
                    if (!$u) {
                        $return = array(
                    'status' => 0,
                    'message' => 'notexisteduser'
                        );
                        return $return;
                    }
                    $roleid = $v->rid;
            $status = $v->status;

                    $conditions = array ('courseid' => $courseid, 'status' => 0);
                    $enrol = $DB->get_records('enrol', $conditions);

                $enrolidArr = array();
                    if (!$enrol) {
                        $return = array(
                            'status' => 0,
                            'message' => 'disabledallmethod'
                        );
                        return $return;
                    } else {    
                    foreach ($enrol as $e) {
                        $enrolidArr[] = $e->id;
                    }
                }
            if ($status != 0) {
//                        $ue = $DB->get_records_select('user_enrolments', 'userid = '.$u->id.' AND enrolid IN ('.implode($enrolidArr, ',').') ');
//                        if (!$ue) 
                            $this->enrol_user($uname, $courseid, $roleid, 0, 0, $username);

//                    if (!array_key_exists($roleid, $manager->get_assignable_roles(false, $user))) {
//                        $return = array(
//                            'error' => 1,
//                            'mes' => 'invalidrole'
//                        );
//                        return $return;
//                    }
//                    if (!has_capability('moodle/role:assign', $manager->get_context(), $user) || !$manager->assign_role_to_user($roleid, $u->id, $user)) {
//                        $return = array(
//                            'error' => 1,
//                            'mes' => 'assignnotpermitted'
//                        );
//                        return $return;
//                    }
                    } else if ($status == 0) {
                        // throw an exception if user is not able to unassign the role in this context
//                        $roles = get_assignable_roles($context, ROLENAME_SHORT);
//                        if (!array_key_exists($unassignment['roleid'], $roles)) {
//                            throw new invalid_parameter_exception('Can not unassign roleid='.$unassignment['roleid'].' in contextid='.$unassignment['contextid']);
//                        }
                          role_unassign($roleid, $u->id, $context->id);
//                if (!has_capability('moodle/role:assign', $manager->get_context(), $user) || !$manager->unassign_role_from_user($u->id, $roleid, $user)) {
//                        $return = array(
//                            'status' => 0,
//                            'message' => 'unassignnotpermitted'
//                        );
//                        return $return;
//                }
//                        $ue = $DB->get_records_select('user_enrolments', 'userid = '.$u->id.' AND enrolid IN ('.implode($enrolidArr, ',').') ');
//                        if (!$ue) 
//                            $this->unenrol_user($uname, $courseid, $roleid);
                    }
                }

        $return = array(
            'status' => 1,
            'message' => 'Success.'
        );
        return $return;
    }

    function get_display ($modname, $instance) {
        global $CFG, $DB;

        switch ( $modname )
        {
            case 'resource':
                // Get display options for resource
                $params = array ($instance);
                $query = "SELECT display from  {$CFG->prefix}resource where id = ?";
                $record = $DB->get_record_sql ($query, $params);

                $display = $record->display;
                break;
            case 'url':
                // Get display options for url
                $params = array ($instance);
                $query = "SELECT display from  {$CFG->prefix}url where id = ?";
                $record = $DB->get_record_sql ($query, $params);

                $display = $record->display;
                break;
            default:
                $display = 0;
                break;
        }

        return $display;
    }



    function get_page ($id) {
        global $DB;

        if (!$cm = get_coursemodule_from_id('page', $id)) {
            return '';
        }
        $page = $DB->get_record('page', array('id'=>$cm->instance), '*', MUST_EXIST);

        $options['noclean'] = true;
        $mypage['name'] = $page->name;
        $context = context_module::instance($cm->id);
        $mypage['content'] = file_rewrite_pluginfile_urls ($page->content, 'pluginfile.php', $context->id, 'mod_page', 'content', $page->revision);
        $mypage['content'] = format_text($mypage['content'], FORMAT_MOODLE, $options);

        return $mypage;
    }

    function get_label ($id) {
        global $DB;

        if (!$cm = get_coursemodule_from_id('label', $id)) {
            return '';
        }
        $label = $DB->get_record('label', array('id'=>$cm->instance), '*', MUST_EXIST);

        $options['noclean'] = true;
        $mylabel['name'] = $label->name;
        $context = context_module::instance($cm->id);
        $mylabel['content'] = $label->intro;
        $mylabel['content'] = format_text($mylabel['content'], FORMAT_MOODLE, $options);
        $mylabel['content'] = file_rewrite_pluginfile_urls ($mylabel['content'], 'pluginfile.php', $context->id, 'mod_label', 'intro');

        return $mylabel;
    }

    function get_news_item ($id) {
        global $CFG, $DB;

        $posts = forum_get_all_discussion_posts ($id, 'created');

        $item_posts = array ();
        foreach ($posts as $post)
        {
            $p['subject'] = $post->subject;
            $p['message'] = $post->message;

            $item_posts[] = $p;
        }

        return $item_posts;
    }

    function add_cohort_member ($username, $cohort_id) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);

        if (!$user)
            return 0;

        $conditions = array ('userid' => $user->id, 'cohortid' => $cohort_id);;
        $member = $DB->get_record('cohort_members',$conditions);

        if ($member)
            return 0;


        cohort_add_member ($cohort_id, $user->id);

        return 1;
    }

    function remove_cohort_member ($username, $cohort_id) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);
        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);

        if (!$user)
            return 0;

        $conditions = array ('userid' => $user->id, 'cohortid' => $cohort_id);;
        $member = $DB->get_record('cohort_members',$conditions);

        if (!$member)
            return 0;

        cohort_remove_member ($cohort_id, $user->id);

        return 1;
    }

    function multiple_add_cohort_member ($username, $cohorts) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);

        if (!$user)
            return;

        foreach ($cohorts as $cohort)
        {
            $this->add_cohort_member ($username, $cohort['id']);
        }

        return 0;
    }


    function multiple_remove_cohort_member ($username, $cohorts) {
        global $CFG, $DB;

        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $conditions = array ('username' => $username);
        $user = $DB->get_record('user',$conditions);

        if (!$user)
            return;

        foreach ($cohorts as $cohort)
        {
            $this->remove_cohort_member ($username, $cohort['id']);
        }

        return 0;
    }


    function get_cohorts () {
        global $CFG, $DB;

        $query = "SELECT id, name
          FROM {$CFG->prefix}cohort";

        $cohorts = $DB->get_records_sql($query);

        $rdo = array ();
        foreach ($cohorts as $cohort)
        {
            $c['id'] = $cohort->id;
            $c['name'] = $cohort->name;

            $rdo[] = $c;
        }

        return $rdo;
    }

    function get_themes () {
        $availablethemes = core_component::get_plugin_list('theme');

        $themes = array ();
        foreach ($availablethemes as $name => $path)
        {
            $theme = array ();
            $theme['name'] = $name;
            $themes[] = $theme;
        }

        return $themes;
    }


    function create_course ($course_ext, $skip_fix_course_sortorder=0, $skip_create_activity = 0){
        global $CFG, $DB, $USER;

        if ($course_ext['username']) {
            $user = get_complete_user_data('username', $course_ext['username']);
            if ($course_ext['category'])
                $categoryid = $course_ext['category'];
            else $categoryid = 1;
            $category = $DB->get_record('course_categories', array('id'=>$categoryid), '*', MUST_EXIST);
            $catcontext = context_coursecat::instance($category->id);
            if (!has_capability('moodle/course:create', $catcontext, $user->id)) {
                return array('error'=>1, 'mes'=>'nopermission', 'data'=>0);
            }
        }
        $shortname_check_exists =  utf8_decode ( $course_ext['shortname']);
        if ($DB->record_exists('course', array('shortname' => $shortname_check_exists,'creator' => $user->id,'category' => $categoryid))) {
            $oldcourse = $DB->get_record('course', array('shortname' => $shortname_check_exists));
            return array('error'=>1, 'mes'=>'shortnametaken', 'data'=>0);
        }

        // set defaults
        $course = new object();
        $course->student  = get_string('defaultcoursestudent');
        $course->students = get_string('defaultcoursestudents');
        $course->teacher  = get_string('defaultcourseteacher');
        $course->teachers = get_string('defaultcourseteachers');
        $course->format = 'topics';

        // override with required ext data
        $course->fullname  = preg_replace("/\"/", "&quot;", utf8_decode ($course_ext['fullname']));
        $course->shortname  = preg_replace("/\"/", "&quot;", utf8_decode ($course_ext['shortname']));
        $course->summary  = htmlentities(utf8_decode ( $course_ext['summary']));
        $course->lang     = utf8_decode ( $course_ext['course_lang'] );
        $course->startdate     =   $course_ext['startdate'] ;
        $course->idnumber  = $course_ext['idnumber'];

        if ($course_ext['category']) $course->category = $categoryid;

        $course->timecreated = time();
        $course->timemodified = time();
        $course->visible     = 0;
        $course->enrollable     = 0;
        $course->creator = $user->id;

        $course->duration = $course_ext['duration'] * 3600;
        $course->price = $course_ext['price'];
        $course->currency = COURSE_CURRENCY;//'usd';

        $course->enablecompletion = 1;
        
        $course->learningoutcomes = preg_replace("/\"/", "&quot;", utf8_decode ($course_ext['learningoutcomes']));
        $course->targetaudience = preg_replace("/\"/", "&quot;", utf8_decode ($course_ext['targetaudience']));
        $course->coursesurvey = ($course_ext['coursesurvey']) ? 1 : 0;
        $course->facilitatedcourse = ($course_ext['facilitatedcourse']) ? 1 : 0;
        $course->certificatecourse = ($course_ext['certificatecourse']) ? 1 : 0;

        // store it and log
        if ($newcourseid = $DB->insert_record("course", $course)) {  // Set up new course
            $section = new object();
            $section->course = $newcourseid;   // Create a default section.
            $section->section = 0;
            $section->visible = 0; // section default invisible
            $section->id = $DB->insert_record("course_sections", $section);
            add_to_log($newcourseid, "course", "new", "view.php?id=$newcourseid", "auth/joomdle auto-creation");
        } else {
            return array('error'=>1, 'mes'=>'cantinsertrecord', 'data'=>0);
        }

        // Add manual enrol method
        $enrol = enrol_get_plugin('manual');
        $courserec = $DB->get_record('course', array('id' => $newcourseid));
        $newitemid = $enrol->add_instance($courserec);
        $context = context_course::instance($newcourseid, MUST_EXIST);

        if ($course_ext['tmpfile']) {
            $tmp_file = $CFG->dataroot . '/temp/' . $course_ext['tmpfile'];
            $context = context_course::instance($newcourseid);
            $fs = get_file_storage();

            $record = new stdClass();
            $record->filearea = 'overviewfiles';
            $record->component = 'course';
            $record->filepath = '/';
            $record->itemid = 0;
            $record->license = 'allrightsreserved';
            $record->author = $USER->username;
            $record->contextid = $context->id;
            $record->userid = $USER->id;
            $record->filename = $course_ext['imgName'];
            $record->source = $course_ext['imgName'];

            $stored_file = $fs->create_file_from_pathname($record, $tmp_file);
        }

        $c = course_get_format($newcourseid)->get_course();

        if ((!isset($skip_create_activity) || !$skip_create_activity) && $course_ext['certificatecourse'] == 1) {
            $json = '{"name":"Certificate default",
            "introeditor":{"text":"intro","format":"1","itemid":"14062716"},
            "emailteachers":"0","emailothers":"","delivery":"0",
            "savecert":"1",
            "requiredtime":0,
            "printdate":"2",
            "datefmt":"3",
            "printnumber":"0",
            "printgrade":"0",
            "gradefmt":"1",
            "printoutcome":"0",
            "printhours":"",
            "printteacher":"0",
            "customtext":"",
            "certificatetype":"A4_non_embedded",
            "orientation":"P",
            "borderstyle":"parenthesis_certificate_default.png",
            "bordercolor":"0",
            "printwmark":"0",
            "printsignature":"0",
            "printseal":"0",
            "visible":"1",
            "cmidnumber":"",
            "groupmode":"0",
            "groupingid":"0",
            "availablefrom":0,
            "availableuntil":0,
            "conditiongraderepeats":1,
            "conditiongradegroup":[{"conditiongradeitemid":"0","conditiongrademin":"","conditiongrademax":""}],
            "conditionfieldrepeats":1,
            "conditionfieldgroup":[{"conditionfield":"0","conditionfieldoperator":"contains","conditionfieldvalue":""}],
            "conditioncompletionrepeats":1,
            "conditioncompletiongroup":[{"conditionsourcecmid":"0","conditionrequiredcompletion":"1"}],
            "showavailability":"1",
            "completionunlocked":1,
            "completion":"2",
            "completionview":"1",
            "completionexpected":0,
            "coursemodule":0,
            "section":0,
            "module":5,
            "modulename":"certificate",
            "instance":0,
            "add":"certificate",
            "update":0,
            "return":0,
            "sr":0,
            "submitbutton2":"Save and return to course"}';
            $fromform = json_decode($json);
            $fromform->introeditor = (array)$fromform->introeditor;
            $fromform->conditiongradegroup[0] = (array)$fromform->conditiongradegroup[0];
            $fromform->conditionfieldgroup[0] = (array)$fromform->conditionfieldgroup[0];
            $fromform->conditioncompletiongroup[0] = (array)$fromform->conditioncompletiongroup[0];
            $fromform->course = $newcourseid;
            $fromform->add = 'certificate';


            require_once($CFG->dirroot . '/course/modlib.php');
            require_once($CFG->dirroot . '/mod/scorm/lib.php');

            $course = $DB->get_record('course', array('id' => $newcourseid), '*', MUST_EXIST);
            if (!empty($fromform->update)) {
                $cm = get_coursemodule_from_id('', $fromform->update, 0, false, MUST_EXIST);
                list($cm, $fromform) = update_moduleinfo($cm, $fromform, $course, null, $user->username);
            } else if (!empty($fromform->add)) {
                $add = add_moduleinfo($fromform, $course, null, $user->username);
            }
        }
        // Create a default section.
        course_create_sections_if_missing($c, 0);

        fix_course_sortorder();
        // purge appropriate caches in case fix_course_sortorder() did not change anything
        cache_helper::purge_by_event('changesincourse');

        // new context created - better mark it as dirty
        mark_context_dirty($context->path);

        // Save any custom role names.
        save_local_role_names($c->id, (array)$course);

        enrol_course_updated(true, $c, $courserec);

        $instances = $DB->get_records('enrol', array('courseid'=>$newcourseid));
        if ($instances && count($instances) > 0) {
            foreach ($instances as $instance) {
                if (is_number($course_ext['validfor']) && $course_ext['validfor'] > 0) {$instance->enrolperiod = $course_ext['validfor'] * 30 * 24 * 3600;}
                $DB->update_record('enrol', $instance);
            }
        }

        $user_lp = $this->get_lp_of_category($categoryid);
        if ($user_lp) {
            $this->enrol_user($user->username, $newcourseid, 1); //1 - Manager
            $this->enrol_user($user_lp->username, $newcourseid, 2); //2 - Course Creator
        } else {
            $this->enrol_user($user->username, $newcourseid, 2); //2 - Course Creator
        }

        add_to_log(SITEID, 'course', 'new', 'view.php?id='.$courserec->id, $courserec->fullname.' (ID '.$courserec->id.')');

//        events_trigger('course_created', $courserec);
        blocks_add_default_course_blocks($courserec);
        if (!isset($skip_create_activity) || !$skip_create_activity) {
            $this->check_old_course($newcourseid, $user->username);
        }

        return array('error'=>0, 'mes'=>'success', 'data'=>$newcourseid);
    }

    function duplicate_course ($courseid, $username) {
        global $CFG, $DB, $USER;
        
        $course_cur = $DB->get_record('course', array('id' => $courseid));
        
        $user = get_complete_user_data('username', $username);
        $catcontext = context_coursecat::instance($course_cur->category);
        if (!has_capability('moodle/course:create', $catcontext, $user->id)) {
            return array('status'=>false, 'message'=>'No permission', 'course'=>array('id' => null, 'shortname' => null));
        }
        
        if (count($course_cur) > 0) {
            $course = array();
            if ($DB->record_exists('course', array('shortname' => $course_cur->shortname))) {
                $query = "SELECT shortname FROM {course} WHERE shortname LIKE ?";
                $list = $DB->get_records_sql($query, array( '%'.$course_cur->shortname ));
                $max = count($list);

                while ($DB->record_exists('course', array('shortname' => '(Copy'.$max.') '.$course_cur->shortname))) {
                    $max++;
                }

                $course['fullname'] = '(Copy'.$max.') '.$course_cur->fullname;
                $course['shortname'] = '(Copy'.$max.') '.$course_cur->shortname;
            } else {
                $course['fullname'] = '(Copy) '.$course_cur->fullname;
                $course['shortname'] = '(Copy) '.$course_cur->shortname;
            }
            if (strlen($course['fullname']) > 254 || strlen($course['shortname']) > 254) return array('status'=>false, 'message'=>'Maximum number of characters in course name field is 254.', 'course'=>array('id' => null, 'shortname' => null));
            $course['category'] = $course_cur->category;
            $course['course_lang'] = '';
            $course['summary'] = $course_cur->summary;
            $course['startdate'] = time();
            $course['idnumber'] = '';
            $course['price'] = $course_cur->price;
            $course['duration'] = $course_cur->duration;
            $course['username'] = $username;
            $course['imgName'] = '';
            $course['learningoutcomes']  = $course_cur->learningoutcomes;
            $course['targetaudience'] = $course_cur->targetaudience;
            $course['coursesurvey'] = $course_cur->coursesurvey;
            $course['facilitatedcourse'] = $course_cur->facilitatedcourse;
            
                require_once($CFG->dirroot . '/course/externallib.php');
                $class = core_course_external::duplicate_course($courseid, $course['fullname'], $course['shortname'], $course['category'], 0, array(), $course['username']);
                
                if (isset($class['id']) && $class['id'] > 0) {
                    $coursenewids = array('status'=>true, 'message'=>'Copy success.', 'course'=>array("id" => $class['id'], 'shortname' => $class['shortname']));
                } else {
                    $coursenewids = array('status'=>false, 'message'=>$class->message, 'course'=>array("id" => null, 'shortname' => null));
                }
            
                return $coursenewids;
            } else {
            return array('status'=>false, 'message'=>'Course ID deleted.', 'course'=>array('id' => null, 'shortname' => null));
        }
    }

    function get_token($username) {
        global $CFG, $DB, $USER;
        
        $user = get_complete_user_data('username', $username);
        
        $webservice = new webservice(); //load the webservice library
        $webservice->generate_user_ws_tokens($user->id); //generate all token that need to be generated
        $tokens = $webservice->get_user_ws_tokens($user->id);
        foreach ($tokens as $token) {
            $r = $token->token;
        }
        
        // if some valid tokens exist then use the most recent
        if ($r) {
            return array('status'=>true, 'message'=>'', 'token'=>$r);
        } else {
            $serviceshortname = 'moodle_mobile_app';
            if ( ($serviceshortname == MOODLE_OFFICIAL_MOBILE_SERVICE and has_capability('moodle/webservice:createmobiletoken', get_system_context()))
                    //Note: automatically token generation is not available to admin (they must create a token manually)
                    or (!is_siteadmin($user) && has_capability('moodle/webservice:createtoken', get_system_context()))) {
                // if service doesn't exist, dml will throw exception
                $service_record = $DB->get_record('external_services', array('shortname'=>$serviceshortname, 'enabled'=>1), '*', MUST_EXIST);
                // create a new token
                $token = new stdClass;
                $token->token = md5(uniqid(rand(), 1));
                $token->userid = $user->id;
                $token->tokentype = EXTERNAL_TOKEN_PERMANENT;
                $token->contextid = context_system::instance()->id;
                $token->creatorid = $user->id;
                $token->timecreated = time();
                $token->externalserviceid = $service_record->id;
                $tokenid = $DB->insert_record('external_tokens', $token);
                add_to_log(SITEID, 'webservice', 'automatically create user token', '' , 'User ID: ' . $user->id);
                $token->id = $tokenid;
            return array('status'=>false, 'message'=>'', 'token'=>$r);
            } else {
                return array('status'=>false, 'message'=>'', 'token'=>null);
        }
    }
    }
    function change_owner_course($username,$ownerblnname){

        global $CFG, $DB;
        
        if (empty($username)) {
            return $response = array(
                'status' => false,
                'message' => 'Username can not be empty.',
            );
        }
        $username = utf8_decode($username);
        $username = strtolower($username);
        $user = get_complete_user_data('username', $username);
        $blnname = utf8_decode($ownerblnname);
        $blnname = strtolower($ownerblnname);
        $userbln = get_complete_user_data('username', $blnname);
        
        $query = "SELECT id FROM {course} WHERE creator = ?";
        $ids = $DB->get_records_sql($query, array($user->id));
      
        $query = "SELECT id "
                    . "FROM {course_categories} "
                    . "WHERE name = '" . $blnname . "'";
        $categorybln = $DB->get_record_sql($query);  
        foreach($ids as $course){
            
            $courses = new object();
            $courses->id     =  $course->id;
            $courses->creator  = $userbln->id;
            $courses->category  = $categorybln->id;
            $courses->timemodified = time();
            $DB->update_record('course', $courses);
            
            $ob = new stdClass();
            $ob->frname = $blnname;
            $ob->rid = 2; // roleid for manager
            $ob->status = 1;
            $datamanager[]=$ob;
             $datamanager = json_encode($datamanager);
            
           $reponse =  $this->set_user_role(1,$course->id,$username,$datamanager);
           $this->unenrol_user ($username, $course->id);

        }
        
        return $response = array(
                'status' => true,
                'message' => 'successfully',
        );
    }
    function update_course ($course_ext) {
        global $CFG, $DB, $USER;

        if ($course_ext['username']) {
            $user = get_complete_user_data('username', $course_ext['username']);
            if ($course_ext['category'])
                $categoryid = $course_ext['category'];
            else $categoryid = 1;
            $coursecontext = context_course::instance($course_ext['id']);
            if (!has_capability('moodle/course:update', $coursecontext, $user->id)) {
                return array('error'=>1, 'mes'=>'nopermission', 'data'=>0);
            }
        }
        $course_info = $this->get_course_info((int)$course_ext['id']);
        $shortname_check_exists =  utf8_decode ( $course_ext['shortname']);
        if (($course_info['shortname'] != $shortname_check_exists) && ($DB->record_exists('course', array('shortname' => $shortname_check_exists,'creator' => $user->id, 'category' => $categoryid)))) {
//            $oldcourse = $DB->get_record('course', array('shortname' => $shortname_check_exists)); // Not in use
            return array('error'=>1, 'mes'=>'shortnametaken', 'data'=>0);
        }

        // check if course already pending approve
        $course_request = $DB->get_record('course_request_approve', array('courseid' => (int)$course_ext['id']));
        if($course_request) {
            if($course_request->status == 0 || $course_request->status == 1) {
                return array('error'=>1, 'mes'=>'alreadypendingapproval', 'data'=>0);
            }
        }

        $courseold = $DB->get_record('course', array('id' => (int)$course_ext['id']), '*', MUST_EXIST);

        $course = new object();
        $course->id     =   $course_ext['id'];
        $course->fullname  = utf8_decode ($course_ext['fullname']);
        $course->shortname  = utf8_decode ( $course_ext['shortname']);
        $course->summary  = utf8_decode ( $course_ext['summary'] );
        $course->lang     = utf8_decode ( $course_ext['course_lang'] );
        $course->learningoutcomes = utf8_decode ( $course_ext['learningoutcomes'] );
        $course->targetaudience = utf8_decode ( $course_ext['targetaudience'] );
        $course->startdate     =   $course_ext['startdate'] ;
        $course->timemodified     =   $course_ext['timemodified'] ;
        $course->category     =   $categoryid ;

        $course->duration = $course_ext['duration'] * 3600;
        $course->price = $course_ext['price'];
        $course->currency = COURSE_CURRENCY;//'usd';

        $course->coursesurvey = ($course_ext['coursesurvey']) ? 1 : 0;
        $course->facilitatedcourse = ($course_ext['facilitatedcourse']) ? 1 : 0;
        $course->certificatecourse = ($course_ext['certificatecourse']) ? 1 : 0;

        if ($course_ext['idnumber']) // Don't touch idnumber if not set, in case it is used by an external application
            $course->idnumber  = $course_ext['idnumber'];

        $DB->update_record('course', $course);

        $act_certificate = $this->get_course_mods($courseold->id, $course_ext['username'], 'certificate');
        if ($courseold && $course->certificatecourse == 1) {
            $hascertificate = false;
            if ($act_certificate['status'] && count($act_certificate['coursemods'])> 0) {
                foreach ($act_certificate['coursemods'] as $mod) {
                    $resources = $mod['mods'];
                    foreach ($resources as $res) {
                        if ($res['mod'] == 'certificate') {
                            $hascertificate = true;
                        }
                    }
                }
            }

            if (!$hascertificate) {
                $json = '{"name":"Certificate default",
                "introeditor":{"text":"intro","format":"1","itemid":"14062716"},
                "emailteachers":"0","emailothers":"","delivery":"0",
                "savecert":"1",
                "requiredtime":0,
                "printdate":"2",
                "datefmt":"3",
                "printnumber":"0",
                "printgrade":"0",
                "gradefmt":"1",
                "printoutcome":"0",
                "printhours":"",
                "printteacher":"0",
                "customtext":"",
                "certificatetype":"A4_non_embedded",
                "orientation":"P",
                "borderstyle":"parenthesis_certificate_default.png",
                "bordercolor":"0",
                "printwmark":"0",
                "printsignature":"0",
                "printseal":"0",
                "visible":"1",
                "cmidnumber":"",
                "groupmode":"0",
                "groupingid":"0",
                "availablefrom":0,
                "availableuntil":0,
                "conditiongraderepeats":1,
                "conditiongradegroup":[{"conditiongradeitemid":"0","conditiongrademin":"","conditiongrademax":""}],
                "conditionfieldrepeats":1,
                "conditionfieldgroup":[{"conditionfield":"0","conditionfieldoperator":"contains","conditionfieldvalue":""}],
                "conditioncompletionrepeats":1,
                "conditioncompletiongroup":[{"conditionsourcecmid":"0","conditionrequiredcompletion":"1"}],
                "showavailability":"1",
                "completionunlocked":1,
                "completion":"2",
                "completionview":"1",
                "completionexpected":0,
                "coursemodule":0,
                "section":0,
                "module":5,
                "modulename":"certificate",
                "instance":0,
                "add":"certificate",
                "update":0,
                "return":0,
                "sr":0,
                "submitbutton2":"Save and return to course"}';
                $fromform = json_decode($json);
                $fromform->introeditor = (array)$fromform->introeditor;
                $fromform->conditiongradegroup[0] = (array)$fromform->conditiongradegroup[0];
                $fromform->conditionfieldgroup[0] = (array)$fromform->conditionfieldgroup[0];
                $fromform->conditioncompletiongroup[0] = (array)$fromform->conditioncompletiongroup[0];
                $fromform->course = $course_ext['id'];
                $fromform->add = 'certificate';


                require_once($CFG->dirroot . '/course/modlib.php');
                require_once($CFG->dirroot . '/mod/scorm/lib.php');

                if (!empty($fromform->update)) {
                    $cm = get_coursemodule_from_id('', $fromform->update, 0, false, MUST_EXIST);
                    list($cm, $fromform) = update_moduleinfo($cm, $fromform, $course, null, $user->username);
                } else if (!empty($fromform->add)) {
                    $add = add_moduleinfo($fromform, $course, null, $user->username);
                }
            }
        } else if ($courseold && $course->certificatecourse == 0) {

            if ($act_certificate['status'] && count($act_certificate['coursemods'])> 0) {
                foreach ($act_certificate['coursemods'] as $mod) {
                    $resources = $mod['mods'];
                    foreach ($resources as $res) {
                        if ($res['mod'] == 'certificate') {
                            $cm = get_coursemodule_from_id('certificate', $res['id'], 0, true, MUST_EXIST);
                            course_delete_module($cm->id);
                        }
                    }
                }
            }
        }

        if ($course_ext['tmpfile']) {
            $tmp_file = $CFG->dataroot . '/temp/' . $course_ext['tmpfile'];
            $context = context_course::instance($course->id);
            $fs = get_file_storage();

            $record = new stdClass();
            $record->filearea = 'overviewfiles';
            $record->component = 'course';
            $record->filepath = '/';
            $record->itemid = 0;
            $record->license = 'allrightsreserved';
            $record->author = $USER->username;
            $record->contextid = $context->id;
            $record->userid = $USER->id;
            $record->filename = $course_ext['imgName'];
            $record->source = $course_ext['imgName'];

            $fs->delete_area_files($context->id, 'course', 'overviewfiles');
            $stored_file = $fs->create_file_from_pathname($record, $tmp_file);
        }

        $courserec = $DB->get_record('course', array('id' => $course->id));

        $conditions = array ('courseid' => $courserec->id, 'status' => 0);
        $enrol = $DB->get_records('enrol', $conditions);

        $enrolidArr = array();
        if (!$enrol) {
            return array('error'=>1, 'mes'=>'disabledallenrolmethod', 'data'=>0);
        } else {
            foreach ($enrol as $e) {
                $enrolidArr[] = $e->id;
            }
        }

        $ue = $DB->get_records_select('user_enrolments', 'userid = '.$user->id.' AND enrolid IN ('.implode($enrolidArr, ',').') ');
        if (!$ue) $this->enrol_user($user->username, $courserec->id, 2); //2 - Course Creator

        $c = course_get_format($courserec->id)->get_course();
        enrol_course_updated(true, $c, $courserec);

        $instances = $DB->get_records('enrol', array('courseid'=>$courserec->id));
        if ($instances && count($instances) > 0) {
            foreach ($instances as $instance) {
                if (is_number($course_ext['validfor']) && $course_ext['validfor'] > 0) {$instance->enrolperiod = $course_ext['validfor'] * 30 * 24 * 3600;}
                $DB->update_record('enrol', $instance);
            }
        }
        $this->check_old_course($courserec->id, $user->username);

        return array('error'=>0, 'mes'=>'success', 'data'=>$courserec->id);
    }

    function create_scorm_activity($s, $sectionid) {
        global $CFG, $DB, $USER;

        // Update time modifi of course
        $course = new object();
        $course->id     =  $s['courseid'];
        $course->timemodified  = time();
        $DB->update_record('course', $course);

        switch ($s['act']) {
            case 0:
                if (isset($s['mid'])) {
                    require_once($CFG->dirroot.'/lib/datalib.php');
                    require_once($CFG->dirroot.'/course/modlib.php');
                    if (!array_key_exists($s['mid'], get_course_mods($s['courseid']))) {
                        $return = array(
                            'error' => 1,
                            'mes' => 'noexistmodule'
                        );
                        return $return;
                    }
                    $cm = get_coursemodule_from_id('', $s['mid'], 0, false, MUST_EXIST);
                    $module = $DB->get_record('modules', array('id'=>$cm->module), '*', MUST_EXIST);
                    $data = $DB->get_record($module->name, array('id'=>$cm->instance), '*', MUST_EXIST);

                    $return = array(
                        'error' => 0,
                        'mes' => 'success',
                        'sid' => $data->id,
                        'courseid' => $data->course,
                        'sname' => $data->name,
                        'scormtype' => $data->scormtype,
                        'fname' => $data->reference,
                        'des' => $data->intro,
                        'desformat' => $data->introformat
                    );
                    return $return;
                }
                break;
            case 1:
            case 2:
                if ($s['tmpfile']) {
                    $tmp_file = $CFG->dataroot . '/temp/' . $s['tmpfile'];
                    
                    // check scorm pakage invalid
                    $packer = get_file_packer('application/zip');
                    $filelist = $packer->list_files($tmp_file);
                    $manifestpresent = false;
                    $aiccfound       = false;
                    if (!is_array($filelist)) {
                        $return = array(
                                'error' => 1,
                                'mes' => 'Incorrect file package - not an archive'
                        );
                        return $return;
                    } else {
                        foreach ($filelist as $info) {
                            if ($info->pathname == 'imsmanifest.xml') {
                                $manifestpresent = true;
                            }
                            if (preg_match('/\.cst$/', $info->pathname)) {
                                $aiccfound = true;
                            }
                        }
                        if (!$manifestpresent and !$aiccfound) {
    //                        $errors['packagefile'] = 'Incorrect file package - missing imsmanifest.xml or AICC structure'; //TODO: localise
                            $return = array(
                                    'error' => 1,
                                    'mes' => 'Incorrect file package - missing imsmanifest.xml or AICC structure'
                            );
                            return $return;
                        }
                    }
                    // end check scorm pakage
                    if ($s['username']) {
                        $user = get_complete_user_data('username', $s['username']);
                        $context = context_course::instance($s['courseid']);
                        if (!has_capability('moodle/course:manageactivities', $context, $user->id)) {
                            $return = array(
                                'error' => 1,
                                'mes' => 'manageactivitiespermission'
                            );
                            return $return;
                        }

                        if (!has_capability('mod/scorm:addinstance', $context, $user->id)) {
                            $return = array(
                                'error' => 1,
                                'mes' => 'scormaddinstancepermission'
                            );
                            return $return;
                        }
                    }

                    $packagefile = file_get_submitted_draft_itemid('packagefile');
                    file_prepare_draft_area($packagefile, $context->id, 'mod_scorm', 'package', 0);

                    $draftitemid = file_get_submitted_draft_itemid('introeditor');
                    file_prepare_draft_area($draftitemid, null, null, null, null);

                    $fs = get_file_storage();

                    $record = new stdClass();
                    $record->filearea = 'draft';
                    $record->component = 'user';
                    $record->filepath = '/';
                    $record->itemid = $draftitemid;
                    $record->license = 'allrightsreserved';
                    $record->author = $user->username;
                    $record->contextid = $context->id;
                    $record->userid = $user->id;
                    $record->filename = $s['fname'];

                    $sourcefield = $record->filename;
                    $record->source = repository_upload::build_source_field($sourcefield);
                    
                    $stored_file = $fs->create_file_from_pathname($record, $tmp_file);
                }

                $json = '{"name":"scorm","introeditor":{"text":"scorm","format":"1","itemid":"450732168"},
                "mform_isexpanded_id_packagehdr":1,
                "scormtype":"local","updatefreq":"0","packagefile":"888987952","popup":"1",
                "width":100,"height":500,"skipview":"0","hidebrowse":"0","displaycoursestructure":"0",
                "hidetoc":"3","hidenav":"0","displayattemptstatus":"1",
                "timeopen":0,"timeclose":0,"grademethod":"1",
                "maxgrade":"100","maxattempt":"0","whatgrade":"0","forcenewattempt":"0","lastattemptlock":"0","forcecompleted":"0",
                "auto":"0","datadir":"","pkgtype":"","launch":"","redirect":"no","redirecturl":"..\/mod\/scorm\/view.php?id=","visible":"1",
                "cmidnumber":"", "groupingid":"0","availablefrom":0,"availableuntil":0,"conditiongraderepeats":1,
                    "conditiongradegroup":[{"conditiongradeitemid":"0","conditiongrademin":"","conditiongrademax":""}],
                "conditionfieldrepeats":1,
                "conditionfieldgroup":[{"conditionfield":"0","conditionfieldoperator":"contains","conditionfieldvalue":""}],
                "conditioncompletionrepeats":1,
                "conditioncompletiongroup":[{"conditionsourcecmid":"0","conditionrequiredcompletion":"1"}],
                "showavailability":"1","completionunlocked":1,"completion":"2","completionview":"1",
                "completionscorerequired":null,
                "completionscoredisabled":"1",
                "completionexpected":0,
                "course":2126,"coursemodule":0,"section":1,"module":23,
                "modulename":"scorm","instance":0,
                "add":"scorm","update":0,"return":0,"sr":0,
                "submitbutton2":"Save and return to course",
                "completionstatusrequired":null}';
                $fromform = json_decode($json);
                $fromform->introeditor = (array)$fromform->introeditor;
                $fromform->introeditor['text'] = utf8_decode($s['des']);
                $fromform->introeditor['format'] = 2; //plain text
                $fromform->introeditor['itemid'] = $draftitemid;
                $fromform->packagefile = $packagefile;
                $fromform->conditiongradegroup[0] = (array)$fromform->conditiongradegroup[0];
                $fromform->conditionfieldgroup[0] = (array)$fromform->conditionfieldgroup[0];
                $fromform->conditioncompletiongroup[0] = (array)$fromform->conditioncompletiongroup[0];
                $fromform->course = $s['courseid'];
                $fromform->name = utf8_decode($s['sname']);
                $fromform->completionexpected = 0;
                $fromform->completion = 2;
                $fromform->completionview = 1;
                $fromform->completionusegrade = 0;

                if (isset($s['section']) && is_numeric($s['section']) && $s['section'] >= 0) $fromform->section = $s['section'];
                $fname = '';
                $tmpfile = '';
                if ($s['fname']) {
                    $fname = $s['fname'];
                    $tmpfile = $s['tmpfile'];
                    //            $fromform->itemid = $draftitemid;
                }
                $fromform->fname = $fname;
                $fromform->tmpfile = $tmpfile;
                if ($s['act'] == 1) {
                    $fromform->add = 1;
                } else if ($s['act'] == 2) {
                    $fromform->update = $s['mid'];
                    $fromform->coursemodule = $s['mid'];
                }

                require_once($CFG->dirroot.'/course/modlib.php');
                require_once($CFG->dirroot.'/mod/scorm/lib.php');

                $course = $DB->get_record('course', array('id'=>$s['courseid']), '*', MUST_EXIST);
                if (!empty($fromform->update)) {
                    $cm = get_coursemodule_from_id('', $fromform->update, 0, false, MUST_EXIST);
                    list($cm, $fromform) = update_moduleinfo($cm, $fromform, $course, null, $user->username);
                } else if (!empty($fromform->add)) {
                    $add = add_moduleinfo($fromform, $course, null, $user->username);
                }

                if($sectionid!=0 && $s['courseid']!=0) {
                    // move module from section 0 to section
                    $id = $add->coursemodule;
                    $course = $DB->get_record('course', array('id' => $s['courseid']), '*', MUST_EXIST);
                    $cm = get_coursemodule_from_id(null, $id, $course->id, false, MUST_EXIST);
                    $modcontext = context_module::instance($cm->id);

                    if (!$section_temp = $DB->get_record('course_sections', array('course' => $course->id, 'id' => $sectionid))) {
                        throw new moodle_exception('AJAX commands.php: Bad section ID ' . $sectionid);
                    }
                    $beforemod = 0;
                    moveto_module($cm, $section_temp, $beforemod);
                }

                $this->check_invisible_activities_and_rebuild_course_cache($course->id);

                $return = array(
                    'error' => 0,
                    'mes' => 'success'
                );
                return $return;
                break;
            case 3:
                if ($s['username']) {
                    $user = get_complete_user_data('username', $s['username']);
                    $context = context_course::instance($s['courseid']);
                    $cm = get_coursemodule_from_id('', $s['mid'], 0, true, MUST_EXIST);
                    $modcontext = context_module::instance($cm->id);

                    if (!has_capability('moodle/course:manageactivities', $context, $user->id) || !has_capability('moodle/course:manageactivities', $modcontext, $user->id)) {
                        $return = array(
                            'error' => 1,
                            'mes' => 'manageactivitiespermission'
                        );
                        return $return;
                    }
                    course_delete_module($cm->id);
                    $return = array(
                        'error' => 0,
                        'mes' => 'success'
                    );
                    return $return;
                }
                break;
            default:
                break;
        }
    }
    
    function create_scorm_platform($username, $courseid, $act, $scorm) {
        global $DB, $CFG;
        $s = (array) json_decode($scorm);
        if(empty($s['scorm'])) {
            return $return = array(
                'status' => false,
                'message' => 'Scorm can not be empty.',
                'sid'=> 0
            );
        }
        $act_required = array(1,2,3);
        if(!in_array($act, $act_required)) {
            return $return = array(
                'status' => false,
                'message' => 'System can not be found this action.',
                'sid' => 0,
            );
        }
        switch ($act) {
            case 0:
                if (isset($s['sid'])) {
                    require_once($CFG->dirroot.'/lib/datalib.php');
                    require_once($CFG->dirroot.'/course/modlib.php');
                    if (!array_key_exists($s['sid'], get_course_mods($courseid))) {
                        $return = array(
                            'status' => false,
                            'message' => 'Scorm is not found.',
                        );
                        return $return;
                    }
                    $cm = get_coursemodule_from_id('', $s['sid'], 0, false, MUST_EXIST);
                    $module = $DB->get_record('modules', array('id'=>$cm->module), '*', MUST_EXIST);
                    $data = $DB->get_record($module->name, array('id'=>$cm->instance), '*', MUST_EXIST);

                    $return = array(
                        'status' => true,
                        'sid' => $data->id,
                        'courseid' => $data->course,
                        'sname' => $data->name,
                        'scormtype' => $data->scormtype,
                        'fname' => $data->reference,
                        'des' => $data->intro,
                        'desformat' => $data->introformat
                    );
                    return $return;
                }
                break;
            case 1:
            case 2:
                if ($s['scorm']) {
                    /*$file_scorm = $this->upload_scorm_package($s['scorm']);
                    if($file_scorm['status'] == 0) {
                        return $response = array(
                            'status' => false,
                            'message' => 'Scorm package is invalid.',
                            'sid' => 0,
                        );
                    }*/
//                    $tmp_file = $CFG->dataroot . '/temp/' . 'tmp_file';
                    $fscorm = explode('_scormtmpfile', $s['scorm']);
                    $filename = $fscorm[0];
                    $filenamepath = 'scormtmpfile'.$fscorm[1];
                    $tmp_file = $CFG->dataroot . '/temp/' . $filenamepath;
                    // check scorm pakage invalid
                    $packer = get_file_packer('application/zip');
                    $filelist = $packer->list_files($tmp_file);
                    $manifestpresent = false;
                    $aiccfound       = false;
                    if (!is_array($filelist)) {
                        $return = array(
                                'status' => false,
                                'message' => 'Incorrect file package - not an archive',
                                'sid' => 0
                        );
                        return $return;
                    } else {
                        foreach ($filelist as $info) {
                            if ($info->pathname == 'imsmanifest.xml') {
                                $manifestpresent = true;
                            }
                            if (preg_match('/\.cst$/', $info->pathname)) {
                                $aiccfound = true;
                            }
                        }
                        if (!$manifestpresent and !$aiccfound) {
                            $return = array(
                                    'status' => false,
                                    'message' => 'Incorrect file package - missing imsmanifest.xml or AICC structure',
                                    'sid' => 0
                            );
                            return $return;
                        }
                    }
                    if ($username) {
                        $user = get_complete_user_data('username', $username);
                        $context = context_course::instance($courseid);
                        if (!has_capability('moodle/course:manageactivities', $context, $user->id)) {
                            $return = array(
                                'status' => false,
                                'message' => 'manage activities permission',
                                'sid' => 0
                            );
                            return $return;
                        }

                        if (!has_capability('mod/scorm:addinstance', $context, $user->id)) {
                            $return = array(
                                'status' => false,
                                'message' => 'scorm addinstance permission',
                                'sid' => 0
                            );
                            return $return;
                        }
                    }

                    $packagefile = file_get_submitted_draft_itemid('packagefile');
                    file_prepare_draft_area($packagefile, $context->id, 'mod_scorm', 'package', 0);

                    $draftitemid = file_get_submitted_draft_itemid('introeditor');
                    file_prepare_draft_area($draftitemid, null, null, null, null);

                    $fs = get_file_storage();
                    $record = new stdClass();
                    $record->filearea = 'draft';
                    $record->component = 'user';
                    $record->filepath = '/';
                    $record->itemid = $draftitemid;
                    $record->license = 'allrightsreserved';
                    $record->author = $user->username;
                    $record->contextid = $context->id;
                    $record->userid = $user->id;
                    $record->filename = $filename;
//                    $record->filename = $s['scorm'];

                    $sourcefield = $record->filename;
                    $record->source = repository_upload::build_source_field($sourcefield);
                    $stored_file = $fs->create_file_from_pathname($record, $tmp_file); 
                }

                $json = '{"name":"scorm","introeditor":{"text":"scorm","format":"1","itemid":"450732168"},
                "mform_isexpanded_id_packagehdr":1,
                "scormtype":"local","updatefreq":"0","packagefile":"888987952","popup":"1",
                "width":100,"height":500,"skipview":"0","hidebrowse":"0","displaycoursestructure":"0",
                "hidetoc":"3","hidenav":"0","displayattemptstatus":"1",
                "timeopen":0,"timeclose":0,"grademethod":"1",
                "maxgrade":"100","maxattempt":"0","whatgrade":"0","forcenewattempt":"0","lastattemptlock":"0","forcecompleted":"0",
                "auto":"0","datadir":"","pkgtype":"","launch":"","redirect":"no","redirecturl":"..\/mod\/scorm\/view.php?id=","visible":"1",
                "cmidnumber":"", "groupingid":"0","availablefrom":0,"availableuntil":0,"conditiongraderepeats":1,
                    "conditiongradegroup":[{"conditiongradeitemid":"0","conditiongrademin":"","conditiongrademax":""}],
                "conditionfieldrepeats":1,
                "conditionfieldgroup":[{"conditionfield":"0","conditionfieldoperator":"contains","conditionfieldvalue":""}],
                "conditioncompletionrepeats":1,
                "conditioncompletiongroup":[{"conditionsourcecmid":"0","conditionrequiredcompletion":"1"}],
                "showavailability":"1","completionunlocked":1,"completion":"2","completionview":"1",
                "completionscorerequired":null,
                "completionscoredisabled":"1",
                "completionexpected":0,
                "course":2126,"coursemodule":0,"section":1,"module":23,
                "modulename":"scorm","instance":0,
                "add":"scorm","update":0,"return":0,"sr":0,
                "submitbutton2":"Save and return to course",
                "completionstatusrequired":null}';
                $fromform = json_decode($json);
                $fromform->introeditor = (array)$fromform->introeditor;
                $fromform->introeditor['text'] = utf8_decode($s['des']);
                $fromform->introeditor['format'] = 2; //plain text
                $fromform->introeditor['itemid'] = $draftitemid;
                $fromform->packagefile = $packagefile;
                $fromform->conditiongradegroup[0] = (array)$fromform->conditiongradegroup[0];
                $fromform->conditionfieldgroup[0] = (array)$fromform->conditionfieldgroup[0];
                $fromform->conditioncompletiongroup[0] = (array)$fromform->conditioncompletiongroup[0];
                $fromform->course = $s['courseid'];
                $fromform->name = utf8_decode($s['sname']);
                $fromform->completionexpected = 0;
                $fromform->completion = 2;
                $fromform->completionview = 1;
                $fromform->completionusegrade = 0;

                if (isset($s['section']) && is_numeric($s['section']) && $s['section'] >= 0) $fromform->section = $s['section'];
                if ($filename) {
                    $fromform->fname = $filename;
                    $fromform->tmpfile = $filenamepath;
                    //            $fromform->itemid = $draftitemid;
                }
                if ($act == 1) {
                    $fromform->add = 1;
                } else if ($act == 2) {
                    $fromform->update = $s['sid'];
                    $fromform->coursemodule = $s['sid'];
                }

                require_once($CFG->dirroot.'/course/modlib.php');
                require_once($CFG->dirroot.'/mod/scorm/lib.php');
                
                $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
                if (!empty($fromform->update)) {
                    $cm = get_coursemodule_from_id('', $fromform->update, 0, false, MUST_EXIST);
                    list($cm, $fromform) = update_moduleinfo($cm, $fromform, $course, null, $user->username);
                } else if (!empty($fromform->add)) {
                    $add = add_moduleinfo($fromform, $course, null, $user->username);
                }
                unlink($CFG->dataroot.'/temp/'.$filenamepath);
                $return = array(
                    'status' => true,
                    'message' => 'Scorm created.',
                    'sid' => $add->coursemodule,
                );
                return $return;
                break;
            case 3:
                if ($s['username']) {
                    $user = get_complete_user_data('username', $s['username']);
                    $context = context_course::instance($courseid);
                    $cm = get_coursemodule_from_id('', $s['sid'], 0, true, MUST_EXIST);
                    $modcontext = context_module::instance($cm->id);

                    if (!has_capability('moodle/course:manageactivities', $context, $user->id) || !has_capability('moodle/course:manageactivities', $modcontext, $user->id)) {
                        $return = array(
                            'status' => false,
                            'message' => 'You can\'t permission for action.',
                            'sid' => $s['sid'],
                        );
                        return $return;
                    }
                    course_delete_module($cm->id);
                    $return = array(
                        'status' => true,
                        'message' => 'Scorm deleted.',
                        'sid' => 0,
                    );
                    return $return;
                }
                break;
            default:
                break;
        }
    }
    
    function upload_scorm_package($package) {
        global $CFG, $USER;
        
        $sc_pack = base64_decode($package);
        $f = finfo_open();

        $mime_type = finfo_buffer($f, $sc_pack, FILEINFO_MIME_TYPE);
        
        if($mime_type != 'application/zip') {
            return $result = array(
                'res' => false,
                'filename' => ''
            );
        }
        
        if($mime_type == 'application/zip') {
                $filename = uniqid(time()).'.zip';
        }
        $tmp_dir = $CFG->dataroot . '/temp/' . 'tmp_file';
        $upload_tmp = file_put_contents($tmp_dir, $sc_pack);
        if($upload_tmp) {
            return $result = array(
                'status' => true,
                'filename' => $filename
            );
        }
    }
    
    // create assignment
    function create_assignment_platform($courseid, $username, $act, $assign) {
        global $DB, $CFG;
        if(empty($courseid) || $courseid == 0) {
            return $response = array(
                'status' => false,
                'message' => 'Course can not be blank.',
                'aid' => 0,
            );
        }
        $act_required = array(1,2,3);
        if(!in_array($act, $act_required)) {
            return $response = array(
                'status' => false,
                'message' => 'System can not be found action.',
                'aid' => 0,
            );
        }
        $data_assign = (array) json_decode($assign);
//        print_r($data_assign); die;
        if(empty($data_assign['aname'])) {
            return $response = array(
                'status' => false,
                'message' => 'Assignment title can not be blank.',
                'aid' => 0,
            );
        }
        if(empty($data_assign['aintro'])) {
            return $response = array(
                'status' => false,
                'message' => 'Assignment introduction can not be blank.',
                'aid' => 0,
            );
        }
        if(empty($data_assign['aduedate'])) {
            return $response = array(
                'status' => false,
                'message' => 'Assignment due date can not be blank.',
                'aid' => 0,
            );
        } 
        if(!empty($data_assign['aduedate'])) {
            $duedate = strtotime(str_replace('/', '-', $data_assign['aduedate']));
            if($duedate < (time()-(60*60*24))) {
                return $response = array(
                    'status' => false,
                    'message' => 'Assignment due date invalid.',
                    'aid' => 0,
                );
            }
        }
//        if(empty($data_assign['abrief'])) {
//            return $response = array(
//                    'status' => false,
//                    'message' => 'Assignment Brief can not be blank.',
//                    'aid' => 0,
//                );
//        }
        
        switch ($act) {
            case 1:
            case 2: 
                
                    /*$brief = $this->upload_assignment_brief($data_assign['abrief'], $courseid, $data_assign['afilename']);
                    if($brief['status'] == 0) {
                        return $response = array(
                            'status' => false,
                            'message' => 'Brief file is invalid.',
                            'sid' => 0,
                        );
                    }*/
                    $user = get_complete_user_data('username', $username);
                    $context = context_course::instance($courseid);
                    if (!has_capability('moodle/course:manageactivities', $context, $user->id)) {
                        $return = array(
                            'status' => false,
                            'message' => 'You don\'t have permission for action.',
                            'aid' => 0
                        );
                        return $return;
                    }

                    if (!has_capability('mod/assign:addinstance', $context, $user->id)) {
                        $return = array(
                            'status' => false,
                            'message' => 'You don\'t have permission for assignment.',
                            'aid' => 0
                        );
                        return $return;
                    }

                    $json = '{"name":"assignment","introeditor":{"text":"test","format":"1","itemid":"701459577"},
                    "mform_isexpanded_id_availability":"1","allowsubmissionsfromdate_raw":"2016-12-11 14:05:00","allowsubmissionsfromdate":"1481436300","duedate_raw":"2016-12-18 14:05:00","duedate":"1482041100",
                    "cutoffdate":"0","alwaysshowdescription":"1","assignsubmission_onlinetext_enabled":"1","assignsubmission_file_enabled":"1","assignsubmission_file_maxfiles":"1","assignsubmission_file_maxsizebytes":"0",
                    "assignfeedback_comments_enabled":"1","mform_isexpanded_id_submissiontypes":"1","submissiondrafts":"1","requiresubmissionstatement":"1",
                    "attemptreopenmethod":"untilpass","maxattempts":"-1","teamsubmission":"0",
                    "requireallteammemberssubmit":"0","teamsubmissiongroupingid":"0","sendnotifications":"1",
                    "sendlatenotifications":"1","grade":"100","advancedgradingmethod_submissions":"",
                    "gradecat":"1210","blindmarking":"0","visible":"0","cmidnumber":"",
                    "groupmode":"0","groupingid":"0","availablefrom":"0","availableuntil":"0",
                    "conditiongraderepeats":"1","conditiongradegroup":[{"conditiongradeitemid":"0","conditiongrademin":"","conditiongrademax":""}],"conditionfieldrepeats":"1",
                    "conditionfieldgroup":[{"conditionfield":"0","conditionfieldoperator":"contains","conditionfieldvalue":""}],"conditioncompletionrepeats":"1",
                    "conditioncompletiongroup":[{"conditionsourcecmid":"0", "conditionrequiredcompletion":"1"}],"showavailability":"1","completionunlocked":"1","completion":"2",
                    "completionview":"1","completionusegrade":"1","completionsubmit":"1","completionexpected":"0","course":"1544","coursemodule":"0","section":"0","module":"1", "modulename":"assign","instance":"0","add":"assign","update":"0","return":"0","sr":"0","submitbutton2":"Save and return to course"}';

                    $fromform = json_decode($json);
                    $fromform->name = $data_assign['aname'];
                    $fromform->introeditor = (array)$fromform->introeditor;
                    $fromform->introeditor['text'] = $data_assign['aintro'];
                    $fromform->introeditor['format'] = 2; //plain text
                    $fromform->conditiongradegroup[0] = (array)$fromform->conditiongradegroup[0];
                    $fromform->conditionfieldgroup[0] = (array)$fromform->conditionfieldgroup[0];
                    $fromform->conditioncompletiongroup[0] = (array)$fromform->conditioncompletiongroup[0];


                    $fromform->course = $courseid;
                    $fromform->duedate_raw = date('Y-m-d H:i:s',  strtotime(str_replace('/', '-', $data_assign['aduedate'])));
                    $fromform->duedate = strtotime(str_replace('/', '-', $data_assign['aduedate']));
                    $fromform->fname = utf8_decode($data_assign['abrief']);
                    if ($act == 1) {
                        $fromform->add = 'assign';
                    } else if ($act == 2) {
                        $fromform->update = $data_assign['aid'];
                        $fromform->coursemodule = $data_assign['aid'];
                    }

                    require_once($CFG->dirroot.'/course/modlib.php');

                    $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
                    if (!empty($fromform->update)) {
                        $cm = get_coursemodule_from_id('', $fromform->update, 0, false, MUST_EXIST);
                        list($cm, $fromform) = update_moduleinfo($cm, $fromform, $course, null, $user->username);
                        $assignid = $fromform->coursemodule;
                    } else if (!empty($fromform->add)) {
                        $add = add_moduleinfo($fromform, $course, null, $user->username);
                        $assignid = $add->coursemodule;
                    }
                    
                    if($data_assign['abrief']) {
                        // move upload file
                        $dir = $CFG->jdataroot;
                        // check folder exits
                        if (!is_dir($dir . '/files')) {
                            mkdir($dir . '/files', 0755);
                        }
                        if (!is_dir($dir . '/files/course')) {
                            mkdir($dir . '/files/course', 0755);
                        }
                        if (!is_dir($dir . '/files/course/' . $courseid)) {
                            mkdir($dir . '/files/course/' . $courseid, 0755);
                        }
                        if (!is_dir($dir . '/files/course/' . $courseid.'/assign')) {
                            mkdir($dir . '/files/course/' . $courseid.'/assign', 0755);
                        }

                        $name_file_assignment = $data_assign['abrief'];
                        $name_file_assignment = str_replace(' ', '_', $name_file_assignment);
                        $name_file_assignment = str_replace('-', '', $name_file_assignment);
                        $name_file_assignment = str_replace('(', '', $name_file_assignment);
                        $name_file_assignment = str_replace(')', '', $name_file_assignment);
                        $new_name = preg_replace('/\.(?=.*\.)/', '', $name_file_assignment);
                        $date = date_timestamp_get(date_create());
                        $date_time =  date('h_i_s_d_m_Y',$date);

                        $tmp_file = $CFG->dataroot . '/temp/' . $data_assign['abrief'];
                        copy($tmp_file, $dir . '/files/course/' . $courseid.'/assign/'.$date_time.'_'.$new_name);
                        unlink($tmp_file);
                    }
                    $return = array(
                        'status' => true,
                        'message' => 'assignment created.',
                        'aid' => $assignid,
                    );
                    return $return;
                
                
            break;
            case 3:
                if ($username) {
                    $user = get_complete_user_data('username', $username);
                    $context = context_course::instance($courseid);
                    $cm = get_coursemodule_from_id('', $data_assign['aid'], 0, true, MUST_EXIST);
                    $modcontext = context_module::instance($cm->id);

                    if (!has_capability('moodle/course:manageactivities', $context, $user->id) || !has_capability('moodle/course:manageactivities', $modcontext, $user->id)) {
                        $return = array(
                            'status' => false,
                            'message' => 'You can\'t permission for action.',
                            'aid' => $data_assign['sid'],
                        );
                        return $return;
                    }
                    course_delete_module($cm->id);
                    $return = array(
                        'status' => true,
                        'message' => 'assignment deleted.',
                        'aid' => 0,
                    );
                    return $return;
                }
                break;
            default:
                break;
        }
    }
    
    function upload_assignment_brief($filedata, $courseid, $filename) {
        global $CFG;
        
        $sc_pack = base64_decode($filedata);
        $f = finfo_open();

        $mime_type = finfo_buffer($f, $sc_pack, FILEINFO_MIME_TYPE);
        
        if($mime_type != 'application/msword') {
            return $result = array(
                'res' => false,
                'filename' => ''
            );
        }
        
        $date = date_timestamp_get(date_create());
        $date_time =  date('h_i_s_d_m_Y',$date);
        
        if($mime_type == 'application/msword') {
                $filename = $date_time.'.docx';
        }
        $dir = $CFG->jdataroot;
    // check folder exits
        if (!is_dir($dir . '/files')) {
            mkdir($dir . '/files', 0755);
        }
        if (!is_dir($dir . '/files/course')) {
            mkdir($dir . '/files/course', 0755);
        }
        if (!is_dir($dir . '/files/course/' . $courseid)) {
            mkdir($dir . '/files/course/' . $courseid, 0755);
        }
        if (!is_dir($dir . '/files/course/' . $courseid.'/assign')) {
            mkdir($dir . '/files/course/' . $courseid.'/assign', 0755);
        }
        $upload_tmp = file_put_contents($dir . '/files/course/' . $courseid.'/assign/'.$filename, $sc_pack);
        if($upload_tmp) {
            return $result = array(
                'status' => true,
                'filename' => $filename
            );
        }
    }
    
    function create_activity_feedback($activity) {
        global $DB, $CFG;
        require_once($CFG->dirroot.'/course/modlib.php');
        
        $section = 0;
        $add = $activity['module'];
        $course = $DB->get_record('course', array('id'=>$activity['courseid']), '*', MUST_EXIST);
        $user = get_complete_user_data('username', $activity['username']);
        
        $module_info = $DB->get_record('modules', array('name' => $activity['module']));
        $cm = null;
        $mform = null;
        
        $data = new stdClass();
        $data->section          = $section;  // The section number itself - relative!!! (section column in course_sections)
        $data->course           = $course->id;
        $data->module           = $module_info->id;
        $data->modulename       = $module_info->name;
        $data->groupmode        = $course->groupmode;
        $data->groupingid       = $course->defaultgroupingid;
        $data->groupmembersonly = 0;
        $data->id               = '';
        $data->instance         = '';
        $data->coursemodule     = '';
        $data->availablefrom     = 0;
        $data->availableuntil     = 0;
        $data->showavailability     = 1;
        $data->add              = $add;
        $data->return           = 0; //must be false if this is an add, go back to course view on cancel
        $data->sr               = 0;
        $data->name               = $activity['title'];
        $data->completionexpected  = 0;
        $data->introeditor['format']               = 1;
        $data->introeditor['text']               = $activity['description'];
        $data->visible          = 1;
        $data->create  = 'new-0';
        $data->resp_view  = 1;
        $data->owner  = $course->id;
        
        $added = add_moduleinfo($data, $course, $mform, $user->username);
        $response = array();
        if($added) {
            $response['status'] = true;
            $response['message'] = $module_info->name.' added.';
            $response['feedbackid'] = $added->sid;
        } else {
            $response['status'] = false;
            $response['message'] = 'Save failed.';
            $response['feedbackid'] = 0;
        }
        return $response;
        
    }
    
    //add content activity
    function create_activity_content($activity) {
        global $DB, $CFG;
        require_once($CFG->dirroot.'/course/modlib.php');
        
        $act = $activity['act'];
        
        if ($act != 1 && $act != 2 & $act !=3) {
            $return = array(
                'status' => false,
                'message' => 'act is must 1 or 2 or 3.',
                'contentid' => 0
            );
            return $return;
        }
        
        switch ($act) {
            case 1:
            case 2:
        $section = 0;
        $add = $activity['module'];
        $course = $DB->get_record('course', array('id'=>$activity['courseid']), '*', MUST_EXIST);
        $user = get_complete_user_data('username', $activity['username']);

        $module_info = $DB->get_record('modules', array('name' => $activity['module']));
        $cm = null;
        $mform = null;
        $response = array();
        // case add
        if($activity['contentid'] == 0) {
            $data = new stdClass();
            $data->section          = $section;  // The section number itself - relative!!! (section column in course_sections)
            $data->course           = $course->id;
            $data->module           = $module_info->id;
            $data->modulename       = $module_info->name;
            $data->groupmode        = $course->groupmode;
            $data->groupingid       = $course->defaultgroupingid;
            $data->groupmembersonly = 0;
            $data->id               = '';
            $data->instance         = '';
            $data->coursemodule     = '';
            $data->availablefrom     = 0;
            $data->availableuntil     = 0;
            $data->showavailability     = 1;
            $data->add              = $add;
            $data->return           = 0; //must be false if this is an add, go back to course view on cancel
            $data->sr               = 0;
            // set default value
            if(empty($activity['title'])) {
                $response['status'] = false;
                $response['message'] = 'Title can not be empty.';
                $response['contentid'] = 0;
            }
            $data->name               = $activity['title'];

            $data->completionexpected  = 0;
            $data->introeditor['format']               = 1;

            $data->introeditor['text']         = $activity['title'];


            $data->visible          = 0;
            $mform = 1;
            $data->page['format']               = 1;
            $data->display               = 5;
            $data->popupwidth               = 620;
            $data->popupheight               = 450;
            $data->printheading               = 1;
            $data->printintro               = 0;
                // processing with other data
            $data->page['text'] = '';
                    $i = 0;
                    $contentArr = array();
                    $itemid = 0; $fname = '';
                    // sort array by position
                    usort($activity['content'], function($a, $b) {
                        return $a['position'] - $b['position'];
                    });
            foreach ($activity['content'] as $content) {
                        if($content['type'] == 'text' || $content['type'] == 'embedVideo') {
                            $skipdata = array("\n", "\r\n", "%0A", "\r");
                            $cdata = '<p>'.str_replace($skipdata, "<br/>", stripcslashes($content['data'])).'</p>';
                            $data->page['text'] .= $cdata;
                            $contentdata = $cdata;
                } else if($content['type'] == 'image') {
                    $image = upload_data_content_page($content['data'], $course->id, $content['position'], 'image');

                            $data->page['text'] .= $image['data']; 
                            $contentdata = $image['data'];
                            $fpath = $image['fpath'];
                            $datac = explode('/', $content['data']);
                            $fname = end($datac);
                        } else if($content['type'] == 'video') {
                            $r = $this->create_page_activity( 4, $course->id, $activity['username'], array('fname'=>$content['data'], 'itemid'=>$content['position']), 0);
//                            $video = upload_data_content_page($content['data'], $course->id, $content['position'], 'video');
                            
                            if (!$r['error']) {
                                $fileData = json_decode($r['data']);
                                $urlvideo = $fileData->filepath . $fileData->filename;
                                $itemid = $fileData->itemid;
                                $fname = $fileData->filename;
                            } else {
                                $response['status'] = false;
                                $response['message'] = 'Cannot save the content, could you try it again?';
                                $response['contentid'] = 0;
                                return $response;
                }
                            $video = '<video webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" oncontextmenu="return false;" frameborder="0" height="281" width="500" controls="controls"> '
                                    . '<source src="' . $urlvideo . '" type="video/mp4"></video>';
                            
                            $data->page['text'] .= $video;
                            $contentdata = $video;
                            $fpath = $urlvideo ? $urlvideo : '';
                        } else if ($content['type'] == 'linkVideo') {
                            $linkvideo = '<a href="'.$content['data'].'" target="_blank">'.$content['data'].'</a>';
                            $data->page['text'] .= $linkvideo;
                            $fpath = $content['data'];
                            $contentdata = $linkvideo;
                            $fname = null;
                        } else if ($content['type'] == 'attach') {
                            $f = $this->create_page_activity( 4, $course->id, $activity['username'], array('fname'=>$content['data'], 'itemid'=>$content['position']), 0);
//                            var_dump(filetype($content['data'])); die();
                            if (!$f['error']) {
                                $fileData = json_decode($f['data']);
                                $urlfile = $fileData->filepath . $fileData->filename;
                                $itemid = $fileData->itemid;
                                $fname = $fileData->filename;
                            } else {
                                $response['status'] = false;
                                $response['message'] = 'Cannot save the content, could you try it again?';
                                $response['contentid'] = 0;
                                return $response;
                            }
                            $dotfile = end(explode('.', $fileData->filename));
                            switch ($dotfile) {
                                case "csv":
                                    $iconUrl = $CFG->root.'/images/icons/attach_csv30x30.png';
                                    break;
                                case "doc":
                                case "docx":
                                    $iconUrl = $CFG->root.'/images/icons/attach_doc30x30.png';
                                    break;
                                case "pdf":
                                    $iconUrl = $CFG->root.'/images/icons/attach_pdf30x30.png';
                                    break;
                                case "ppt":
                                    $iconUrl = $CFG->root.'/images/icons/attach_ppt30x30.png';
                                    break;
                                case "pps":
                                    $iconUrl = $CFG->root.'/images/icons/attach_pps30x30.png';
                                    break;
                                case "rtf":
                                    $iconUrl = $CFG->root.'/images/icons/attach_rtf30x30.png';
                                    break;
                                case "svg":
                                    $iconUrl = $CFG->root.'/images/icons/attach_svg30x30.png';
                                    break;
                                case "txt":
                                    $iconUrl = $CFG->root.'/images/icons/attach_txt30x30.png';
                                    break;
                                case "xls":
                                case "xlsx":
                                    $iconUrl = $CFG->root.'/images/icons/attach_xls30x30.png';
                                    break;
                                default:
                                    $iconUrl = $CFG->root.'/media/joomdle/images/icon/courseoutline/CourseContent_Inactive.png';
                                    break;
                            }
                            $attch = '<div class="uploadedFile">'
                                    . '<a href="' . $urlfile . '"><img src="' . $iconUrl . '">' . $fname . '</a></div>';
                            
                            $data->page['text'] .= $attch;
                            $contentdata = $attch;
                            $fpath = $urlfile ? $urlfile : '';
                        } 
                        
                        $contentpar = array();
                        $contentpar['no'] = $i;
                        $contentpar['type'] = $content['type'];
                        $contentpar['content'] = $contentdata;
                        if ($content['type'] == 'image' || $content['type'] == 'video' || $content['type'] == 'linkVideo' || $content['type'] == 'attach'){
                            $contentpar['itemid'] = $itemid;
                            $contentpar['fname'] = $fname ? $fname : '';
                            $contentpar['fpath'] = $fpath ? $fpath : '';
                        }
                        
                        $contentArr[] = $contentpar;
                        
                        if (isset($content['caption']) && $content['caption'] != '') {
                            $skipdata = array("\n", "\r\n", "%0A", "\r");
                            $caption = '<p>'.str_replace($skipdata, "<br/>", stripcslashes($content['caption'])).'</p>';
                            $data->page['text'] .= $caption;
                            $contentdata = $caption;
                            
                            $contentpa = array();
                            $contentpa['no'] = $i;
                            $contentpa['type'] = 'caption';
                            $contentpa['content'] = $contentdata;
                            $contentArr[] = $contentpa;
                        }
                        $i++;
            }
                    $data->params = json_encode($contentArr);
            $status = add_moduleinfo($data, $course, $mform, $user->username);
            $id = $status->id;
        } else {
                    $cm = get_coursemodule_from_id($module_info->name, $activity['contentid'], 0, false, MUST_EXIST);
        //            $cm = get_coursemodule_from_instance($module_info->name, $activity['contentid'], 0, false, MUST_EXIST);
            $course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);

            $data->coursemodule       = $cm->id;
            $data->section            = $section;  // The section number itself - relative!!! (section column in course_sections)
            $data->visible            = $cm->visible; //??  $cw->visible ? $cm->visible : 0; // section hiding overrides
            $data->cmidnumber         = $cm->idnumber;          // The cm IDnumber
            $data->groupingid         = $cm->groupingid;
            $data->groupmembersonly   = $cm->groupmembersonly;
            $data->course             = $course->id;
            $data->module             = $cm->module;
            $data->modulename         = $cm->modname;
            $data->instance           = $cm->instance;
            $data->return             = 0;
            $data->sr                 = 0;
            $data->update             = $cm->id;
            $data->completion         = $cm->completion;
            $data->completionview     = $cm->completionview;
            $data->completionexpected = $cm->completionexpected;
            $data->completionusegrade = is_null($cm->completiongradeitemnumber) ? 0 : 1;
            $data->showdescription    = $cm->showdescription;
            $data->availablefrom = 0;
            $data->availableuntil = 0;
            $data->showavailability = 1;

            // get current content data
            $content = $DB->get_record('page', array('id'=>$activity['contentid']), 'content');

            if(!empty($activity['title'])) {
                $data->name               = $activity['title'];
            }
            $data->completionexpected  = 0;
            $data->introeditor['format']               = 1;
            if(!empty($activity['description'])) {
                $data->introeditor['text']               = $activity['description'];
            }

            $data->visible          = 0;
            $mform = 1;
            $data->page['format']      = 1;
            $data->display               = 5;
            $data->popupwidth               = 620;
            $data->popupheight               = 450;
            $data->printheading               = 1;
            $data->printintro               = 0;
                    $i = 0;
                    $contentArr = array();
                    $itemid = 0; $fname = '';
            foreach ($activity['content'] as $content) {
                        if($content['type'] == 'text' || $content['type'] == 'embedVideo') {
                            $skipdata = array("\n", "\r\n", "%0A", "\r");
                            $cdata = '<p>'.str_replace($skipdata, "<br/>", stripcslashes($content['data'])).'</p>';
                            $data->page['text'] .= $cdata;
                            $contentdata = $cdata;
                        } else if($content['type'] == 'image') {
                            $image = upload_data_content_page($content['data'], $course->id, $content['position'], 'image');

                            $data->page['text'] .= $image['data']; 
                            $contentdata = $image['data'];
                            $fpath = $image['fpath'];
                            $datac = explode('/', $content['data']);
                            $fname = end($datac);
                        } else if($content['type'] == 'video') {
                            $r = $this->create_page_activity( 4, $course->id, $activity['username'], array('fname'=>$content['data'], 'itemid'=>$content['position']), 0);
//                            $video = upload_data_content_page($content['data'], $course->id, $content['position'], 'video');
                            
                            if (!$r['error']) {
                                $fileData = json_decode($r['data']);
                                $urlvideo = $fileData->filepath . $fileData->filename;
                                $itemid = $fileData->itemid;
                                $fname = $fileData->filename;
                            } else {
                                $response['status'] = false;
                                $response['message'] = 'Cannot save the content, could you try it again?';
                                $response['contentid'] = 0;
                                return $response;
                }
                            $video = '<video webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" oncontextmenu="return false;" frameborder="0" height="281" width="500" controls="controls"> '
                                    . '<source src="' . $urlvideo . '" type="video/mp4"></video>';
                            
                            $data->page['text'] .= $video;
                            $contentdata = $video;
                            $fpath = $urlvideo ? $urlvideo : '';
                        } else if ($content['type'] == 'linkVideo') {
                            $linkvideo = '<a href="'.$content['data'].'" target="_blank">'.$content['data'].'</a>';
                            $data->page['text'] .= $linkvideo;
                            $fpath = $content['data'];
                            $contentdata = $linkvideo;
                            $fname = null;
                        } else if ($content['type'] == 'attach') {
                            $f = $this->create_page_activity( 4, $course->id, $activity['username'], array('fname'=>$content['data'], 'itemid'=>$content['position']), 0);
//                            var_dump(filetype($content['data'])); die();
                            if (!$f['error']) {
                                $fileData = json_decode($f['data']);
                                $urlfile = $fileData->filepath . $fileData->filename;
                                $itemid = $fileData->itemid;
                                $fname = $fileData->filename;
                            } else {
                                $response['status'] = false;
                                $response['message'] = 'Cannot save the content, could you try it again?';
                                $response['contentid'] = 0;
                                return $response;
                }
                            $dotfile = end(explode('.', $fileData->filename));
                            switch ($dotfile) {
                                case "csv":
                                    $iconUrl = $CFG->root.'/images/icons/attach_csv30x30.png';
                                    break;
                                case "doc":
                                case "docx":
                                    $iconUrl = $CFG->root.'/images/icons/attach_doc30x30.png';
                                    break;
                                case "pdf":
                                    $iconUrl = $CFG->root.'/images/icons/attach_pdf30x30.png';
                                    break;
                                case "ppt":
                                    $iconUrl = $CFG->root.'/images/icons/attach_ppt30x30.png';
                                    break;
                                case "pps":
                                    $iconUrl = $CFG->root.'/images/icons/attach_pps30x30.png';
                                    break;
                                case "rtf":
                                    $iconUrl = $CFG->root.'/images/icons/attach_rtf30x30.png';
                                    break;
                                case "svg":
                                    $iconUrl = $CFG->root.'/images/icons/attach_svg30x30.png';
                                    break;
                                case "txt":
                                    $iconUrl = $CFG->root.'/images/icons/attach_txt30x30.png';
                                    break;
                                case "xls":
                                case "xlsx":
                                    $iconUrl = $CFG->root.'/images/icons/attach_xls30x30.png';
                                    break;
                                default:
                                    $iconUrl = $CFG->root.'/media/joomdle/images/icon/courseoutline/CourseContent_Inactive.png';
                                    break;
                            }
                            $attch = '<div class="uploadedFile">'
                                    . '<a href="' . $urlfile . '"><img src="' . $iconUrl . '">' . $fname . '</a></div>';
                            
                            $data->page['text'] .= $attch;
                            $contentdata = $attch;
                            $fpath = $urlfile ? $urlfile : '';
                        } 
                        
                        $contentpar = array();
                        $contentpar['no'] = $i;
                        $contentpar['type'] = $content['type'];
                        $contentpar['content'] = $contentdata;
                        if ($content['type'] == 'image' || $content['type'] == 'video' || $content['type'] == 'linkVideo' || $content['type'] == 'attach'){
                            $contentpar['itemid'] = $itemid;
                            $contentpar['fname'] = $fname ? $fname: '';
                            $contentpar['fpath'] = $fpath ? $fpath : '';
                        }
                        
                        $contentArr[] = $contentpar;
                        
                        if (isset($content['caption']) && $content['caption'] != '') {
                            $skipdata = array("\n", "\r\n", "%0A", "\r");
                            $caption = '<p>'.str_replace($skipdata, "<br/>", stripcslashes($content['caption'])).'</p>';
                            $data->page['text'] .= $caption;
                            $contentdata = $caption;
                            
                            $contentpa = array();
                            $contentpa['no'] = $i;
                            $contentpa['type'] = 'caption';
                            $contentpa['content'] = $contentdata;
                            $contentArr[] = $contentpa;
                        }
                        $i++;
            }
                    $data->params = json_encode($contentArr);
            $status = update_moduleinfo($cm, $data, $course, $mform);
            $id = $status[0]->instance;
        }
        if($status) {
            $response['status'] = true;
            $response['message'] = $module_info->name.' saved.';
            $response['contentid'] = $id;
        } else {
            $response['status'] = false;
            $response['message'] = 'Save failed.';
            $response['contentid'] = 0;
        }
        return $response;
                break;
            case 3: // delete 
                if($activity['contentid'] == 0) {
                    $return = array(
                        'status' => false,
                        'message' => 'Contentid do not empty.',
                        'contentid' => 0
                    );
                    return $return;
                } else {
                    $username = $activity['username'];
                    $courseid = $activity['courseid'];
                    if ($username) {
                        $user = get_complete_user_data('username', $username);
                        $context = context_course::instance($courseid);
                        $cm = get_coursemodule_from_id('', $activity['contentid'], 0, true, MUST_EXIST);
                        $modcontext = context_module::instance($cm->id);

                        if (!has_capability('moodle/course:manageactivities', $context, $user->id) || !has_capability('moodle/course:manageactivities', $modcontext, $user->id)) {
                            $return = array(
                                'status' => false,
                                'message' => 'manageactivitiespermission',
                                'contentid' => 0
                            );
                            return $return;
    }
                        course_delete_module($cm->id);
                        $fs = get_file_storage();
                        $fs->delete_area_files($modcontext->id, 'mod_page', 'content');

                        $return = array(
                            'status' => true,
                            'message' => 'Success.',
                            'contentid' => $activity['contentid']
                        );
                        return $return;
                    } else {
                        $return = array(
                            'status' => false,
                            'message' => 'Username do not empty.',
                            'contentid' => 0
                        );
                        return $return;
                    }
                }
                break;
            default:
                break;
        }
    }
    
    function get_activity_content ($courseid, $username, $aid) {
        global $CFG, $DB, $USER;

        if (isset($aid) && ($aid != 0 )) {
            require_once($CFG->dirroot.'/lib/datalib.php');
            require_once($CFG->dirroot.'/course/modlib.php');
            if (!array_key_exists($aid, get_course_mods($courseid))) {
                $return = array(
                    'status' => false,
                    'message' => 'No exist module.'
                );
                return $return;
            }
            $cm = get_coursemodule_from_id('', $aid, 0, false, MUST_EXIST);
            $module = $DB->get_record('modules', array('id'=>$cm->module), '*', MUST_EXIST);
            $data = $DB->get_record($module->name, array('id'=>$cm->instance), '*', MUST_EXIST);
            
            $content = array();
            $content['sid'] = $data->id;
            $content['courseid'] = $data->course;
            $content['name'] = $data->name;
            $content['des'] = $data->intro;
            $content['desformat'] = $data->introformat;
//            $content['contentf'] = $data->content;
            $content['contentformat'] = $data->contentformat;
//            $content['params'] = $data->params;
            
            $contents = array();
            $datas = json_decode($data->params);           
            $i = 1; $typebefor = '';
            if ($datas) {
                $countdata = count($datas);
                foreach ($datas as $k => $v) {
                    $htmldisable = array("<p>", "</p>");
                    $v->content = str_replace($htmldisable, "", $v->content);
                    if ($v->type == 'caption') {
                        $cont['caption'] = $v->content;
                        $contents[] = $cont;
                    } else {
                        if ($typebefor == 'video' || $typebefor == 'embedVideo') {$contents[] = $cont;}
                    $cont = array();
                    $cont['type'] = $v->type;
                    $cont['data'] = $v->content;
                    $cont['position'] = $i;

                        if ($v->type == 'image' || $v->type == 'video'|| $v->type == 'attach' || $v->type == 'linkVideo') {
                        if (!isset($v->itemid)) $v->itemid = null;
                        if (!isset($v->fname)) $v->fname = null;
                        if (!isset($v->fpath)) $v->fpath = null;
                        $cont['itemid'] = (int)$v->itemid;
                        $cont['fname'] = $v->fname;
                        $cont['fpath'] = $v->fpath;
                    } 
                        if ($v->type !== 'video' && $v->type !== 'embedVideo') {
                    $contents[] = $cont;
                        } else if ($k == ($countdata-1) && ($v->type == 'video' || $v->type == 'embedVideo')) {
                            $contents[] = $cont;
                        }

                    $i++;
                }
                    $typebefor = $v->type;
                }
            } else { 
                $cont = array();
                $cont['type'] = 'text';
                $cont['position'] = $i;
                $cont['data'] = $data->content;
                $cont['itemid'] = null;
                $cont['fname'] = '';
                $contents[] = $cont;                
            }
                    
            $content['content'] = $contents;
            
            $return = array(
                'status' => true,
                'message' => 'Success.',
                'data' => $content
            );
            return $return;
        } else {
            
            $return = array(
                'status' => false,
                'message' => 'aid is not null',
//                'data' => $content
            );
            return $return;
        }
    }

    function get_quiz_activity_detail ($courseid, $username, $aid) {
        global $CFG, $DB, $USER;

        if (isset($aid) && ($aid != 0 )) {
            require_once($CFG->dirroot.'/lib/datalib.php');
            require_once($CFG->dirroot.'/course/modlib.php');
            if (!array_key_exists($aid, get_course_mods($courseid))) {
            $return = array(
                    'status' => false,
                    'message' => 'No exist module.'
                );
                return $return;
            }
            $cm = get_coursemodule_from_id('', $aid, 0, false, MUST_EXIST);
            $module = $DB->get_record('modules', array('id'=>$cm->module), '*', MUST_EXIST);
            $data = $DB->get_record($module->name, array('id'=>$cm->instance), '*', MUST_EXIST);

            $quiz = array(
                'sid' => $data->id,
                'courseid' => $data->course,
                'name' => $data->name,
                'atype' => $data->atype,
                'des' => $data->intro,
                'desformat' => $data->introformat,
                'timeopen' => $data->timeopen,
                'timeclose' => $data->timeclose,
                'timelimit' => $data->timelimit,
                'attempts' => $data->attempts,
                'questions' => $data->questions
            );
            $return = array(
                'status' => true,
                'message' => 'Success.',
                'data' => $quiz
            );
            return $return;
        } else {
            $return = array(
                'status' => false,
                'message' => 'aid is not null',
//                'data' => $content
            );
            return $return;
        }
    }
    
    function get_assignment_activity_detail ($courseid, $username, $aid) {
        global $CFG, $DB, $USER;
        if (isset($aid) && ($aid != 0 )) {
            require_once($CFG->dirroot.'/course/modlib.php');
            require_once($CFG->dirroot.'/mod/assign/lib.php');
            if (!$DB->record_exists("course_modules", array("id"=>$aid )) || !array_key_exists($aid, get_course_mods($courseid)) ) {
                $return = array(
                    'status' => false,
                    'message' => 'No exist module.'
                );
                return $return;
            }

            $cm = get_coursemodule_from_id('', $aid, 0, false, MUST_EXIST);
            $module = $DB->get_record('modules', array('id'=>$cm->module), '*', MUST_EXIST);

            // Get information assignment(*)
            $data = $DB->get_record($module->name, array('id'=>$cm->instance), '*', MUST_EXIST);

            $course_modules_info = $DB->get_record('course_modules', array('id'=>$aid), 'section', MUST_EXIST);
            $course_section_info = $DB->get_record('course_sections', array('id'=>$course_modules_info->section), 'name', MUST_EXIST);

            if($course_section_info->name == ''){
                $course_section_info->name = 'Topic title here';
            }

            // get link download brief(**)
            $context = context_module::instance($aid);
            $link_dowload_brief = '';
            if (!empty($data->params)) {
                $prs = json_decode($data->params);
                $itemid = $prs->itemid;
                $fname = $prs->fname;
                $link_dowload_brief = $CFG->wwwroot . '/pluginfile.php/' . $context->id . '/mod_assign/brief/' . $itemid . '/' . $fname;
                if ($itemid == 0 || $fname == '' || !@file_get_contents($link_dowload_brief, 0, NULL, 0, 1)) {
                    $link_dowload_brief = '';
                }
            }

            // Check status grade and status submission(***)
            $user = get_complete_user_data('username', $username);
            $submission = get_submission($data->id,$user->id);
            $grade1 = $this->get_mod_progress($courseid, $username);
            $check_grade = false;
            foreach ($grade1['sections'] as $section){
                foreach ($section['mods'] as $mod){
                    if($aid == $mod['id']){
                        if($mod['finalgradeletters'] != 'Ungraded' && $mod['gradeid'] > 0){
                            $check_grade = true;
                        }
                    }
                }
            }
            if (!empty($submission)) {
                foreach ($submission as $sb) {
                    if ($sb->status == 'submitted') {
                        $submission_status = 'Submitted';
                        if ($check_grade) {
                            $grade_status = "Completed";
                        } else {
                            $grade_status = "In Progress";
                        }
                    } else {
                        $submission_status = 'Not Yet Submitted';
                        $grade_status = "Not Yet Graded";
                    }
                }
            } else {
                $submission_status = 'Not Yet Submitted';
                if ($check_grade) {
                    $grade_status = "Completed";
                }else{
                    $grade_status = "Not Yet Graded";
                }
            }
            $options['noclean'] = true;
            $data->intro = format_text($data->intro, FORMAT_MOODLE, $options);
            $data_json = json_encode($data);
            $return = array(
                'status' => true,
                'message' => 'Success.',
                'assign_information' => $data_json,
                'topic_name' => $course_section_info->name,
                'submission_status'=> $submission_status,
                'grade_status'=> $grade_status,
                'link_download_brief'=> $link_dowload_brief
            );
            return $return;
        } else {
            $return = array(
                'status' => false,
                'message' => 'aid is not null',
                'data' => ''
            );
            return $return;
        }
    }
    
    function get_scorm_activity_detail ($courseid, $username, $aid) {
        global $CFG, $DB, $USER;
        
        if (isset($aid) && ($aid != 0 )) {
            require_once($CFG->dirroot.'/lib/datalib.php');
            require_once($CFG->dirroot.'/course/modlib.php');
            if (!array_key_exists($aid, get_course_mods($courseid))) {
                $return = array(
                    'status' => false,
                    'message' => 'No exist module.'
                );
                return $return;
            }

            $cm = get_coursemodule_from_id('', $aid, 0, false, MUST_EXIST);
            $module = $DB->get_record('modules', array('id'=>$cm->module), '*', MUST_EXIST);
            $data = $DB->get_record($module->name, array('id'=>$cm->instance), '*', MUST_EXIST);

            $scorm = array(
                'sid' => $data->id,
                'courseid' => $data->course,
                'sname' => $data->name,
                'des' => $data->intro,
                'desformat' => $data->introformat,
                'scormtype' => $data->scormtype,
                'filename' => $data->reference
            );
            $return = array(
                'status' => true,
                'message' => 'Success.',
                'data' => $scorm
            );
            return $return;
        } else {
            $return = array(
                'status' => false,
                'message' => 'aid is not null',
//                'data' => $content
            );
            return $return;
        }
    }
    
    function delete_course_content ($aid, $courseid, $username, $type) {
        global $DB, $CFG;
        
        if ($aid == 0 || empty($aid)) {
            $return = array(
                'status' => false,
                'message' => 'aid can not be blank.',
                'aid' => 0
            );
            return $return;
        }
        if ($courseid == 0 || empty($courseid)) {
            $return = array(
                'status' => false,
                'message' => 'courseid can not be blank.',
                'aid' => 0
            );
            return $return;
        }
        if ($username == '' || empty($username)) {
            $return = array(
                'status' => false,
                'message' => 'username can not be blank.',
                'aid' => 0
            );
            return $return;
        }
        if ($type == '' || empty($type)) {
            $return = array(
                'status' => false,
                'message' => 'type can not be blank.',
                'aid' => 0
            );
            return $return;
        }
        if ($type != 'content' && $type != 'assessment' && $type != 'assignment' && $type != 'scorm' && $type != 'question') {
            $return = array(
                'status' => false,
                'message' => 'type must is content|assessment|assignment|scorm|question.',
                'aid' => 0
            );
            return $return;
        }
        
        switch ($type) {
            case 'content':
                $user = get_complete_user_data('username', $username);
                $context = context_course::instance($courseid);
                $cm = get_coursemodule_from_id('', $aid, 0, true, MUST_EXIST);
                $modcontext = context_module::instance($cm->id);

                if (!has_capability('moodle/course:manageactivities', $context, $user->id) || !has_capability('moodle/course:manageactivities', $modcontext, $user->id)) {
                    $return = array(
                        'status' => false,
                        'message' => 'You can\'t permission for action.',
                        'aid' => $aid
                    );
                    return $return;
                }
                course_delete_module($cm->id);
                $fs = get_file_storage();
                $fs->delete_area_files($modcontext->id, 'mod_page', 'content');

                $return = array(
                    'status' => true,
                    'message' => 'Content deleted.',
                    'aid' => $aid
                );
                return $return;
                break;
            case 'assessment':
            case 'assignment':
            case 'scorm':
                if ($username) {
                    $user = get_complete_user_data('username', $username);
                    $context = context_course::instance($courseid);
                    $cm = get_coursemodule_from_id('', $aid, 0, true, MUST_EXIST);
                    $modcontext = context_module::instance($cm->id);

                    if (!has_capability('moodle/course:manageactivities', $context, $user->id) || !has_capability('moodle/course:manageactivities', $modcontext, $user->id)) {
                        $return = array(
                            'status' => false,
                            'message' => 'You can\'t permission for action.',
                            'aid' => $aid,
                        );
                        return $return;
                    }
                    course_delete_module($cm->id);
                    $return = array(
                        'status' => true,
                        'message' => 'Activity deleted.',
                        'aid' => $aid,
                    );
                    return $return;
                }
                break;
            case 'question':
                $user = get_complete_user_data ('username', $username);
                $context = context_course::instance($courseid);
                $categorycontext = context::instance_by_id($context->id);
                $addpermission = has_capability('moodle/question:add', $categorycontext, $user);
                if ($addpermission) {
                    $questionid = (int)$aid;
    //                    $questionBank = $this->load_questions_bank($courseid);
                    $question = $this->load_questions_bank($courseid);
                    if ($question['status']) {
                        $questionBank = $question['questions'];
                    } else {
                        $questionBank = array();
                    }
                    if (!array_key_exists($questionid, $questionBank)) {
                        $return = array(
                            'status' => false,
                            'message' => 'No exist.',
                            'aid' => $aid,
                        );
                        return $return;
                    }
                    question_require_capability_on($questionid, 'edit', $username);
                    if (questions_in_use(array($questionid))) {
                        $DB->set_field('question', 'hidden', 1, array('id' => $questionid));
                    } else {
                        question_delete_question($questionid, $username);
                    }

                    $return = array(
                            'status' => true,
                            'message' => 'Question deleted.',
                            'aid' => $aid,
                        );
                    return $return;
                } else {
                    $return = array(
                            'status' => false,
                            'message' => 'No permission.',
                            'aid' => $aid,
                        );
                    return $return;
                }
                break;
            default:
                break;
        }
    }

    function create_categories($categories) {
        global $CFG, $DB;
        require_once($CFG->libdir . "/coursecatlib.php");
        $transaction = $DB->start_delegated_transaction();
        
        $createdcategories = array();
        foreach ($categories as $category) {
            if ($category['parent']) {
                if (!$DB->record_exists('course_categories', array('id' => $category['parent']))) {
                    throw new moodle_exception('unknowcategory');
                }
                $context = context_coursecat::instance($category['parent']);
            } else {
                $context = context_system::instance();
            }
            $user = get_complete_user_data ('username', $category['username']);
            $category['userowner'] = $user->id;
            $newcategory = coursecat::create($category);
            $context = context_coursecat::instance($newcategory->id);
            $roleid = 1; // hard code for manage role
            role_assign($roleid, $user->id, $context->id);

            $createdcategories[] = array('id' => $newcategory->id, 'name' => $newcategory->name);
        }
        
        $transaction->allow_commit();
        $response = array();
        $response['status']  = true;
        $response['categories']  = $createdcategories;
        return $response;
    }

    function logoutpage_hook() {
        global $redirect, $USER;

        if ($USER->auth != 'joomdle')
            return;

        $remember_cookie = $this->call_method ("logout", $USER->username);
        $r = $remember_cookie;

        setcookie($r, false,  time() - 3600, '/');

        $logout_redirect_to_joomla = get_config('auth/joomdle', 'logout_redirect_to_joomla');

        if ($logout_redirect_to_joomla)
            $redirect = get_config ('auth/joomdle', 'joomla_url').'/components/com_joomdle/views/wrapper/getout.php';
    }

    function get_scorm_item_track_data ($id, $username, $item) {
        global $CFG, $DB;
        $user = get_complete_user_data ('username', $username);

        $query =
            "SELECT
            value
            FROM
            {$CFG->prefix}scorm_scoes_track
            WHERE
            userid = ?
            and scormid = ?
            and element = ?
            ";

        $params = array ($user->id, $id, $item);
        $record =  $DB->get_record_sql($query, $params);

        $data = $record->value;

        return $data;
    }

    function get_scorm_track_data ($id, $username) {
        $data = array ();
        $data['start_time'] = $this->get_scorm_item_track_data ($id, $username, 'x.start.time');
        $data['total_time'] = $this->get_scorm_item_track_data ($id, $username, 'cmi.core.total_time');
        $data['lesson_status'] = $this->get_scorm_item_track_data ($id, $username, 'cmi.core.lesson_status');
        $data['score'] = $this->get_scorm_item_track_data ($id, $username, 'cmi.core.score.raw');

        return $data;
    }

    function get_scorm_data ($course_id, $username) {
        $sections = $this->get_course_mods ($course_id, $username);

        if ($sections['status']) {
            foreach ($sections['coursemods'] as $section)
        {
            foreach ($section['mods'] as $mod)
            {
                if ($mod['mod'] == 'scorm')
                {
                    // Scorm object found, we return its info, as we assume only one scorm object per course
                    $cm = get_coursemodule_from_id('scorm', $mod['id']);
                    $scorm_track = $this->get_scorm_track_data ($cm->instance, $username);

                    return ($scorm_track);
                }
                else
                    continue;
            }
        }
    }
    }

    function list_courses_scorm ($available = 0, $sortby = 'created', $guest = 0, $username = '') {
        $courses = $this->list_courses ($available, $sortby, $guest, $username);

        $scorm_courses = array ();
        foreach ($courses as $c)
        {
            $c['scorm_data'] = $this->get_scorm_data ($c['remoteid'], 'pepe');

            if (!$c['scorm_data'])
                continue; // Skip non scorm courses

            $scorm_courses[] = $c;
        }

        return $scorm_courses;

    }

    /* Logs the user in both Joomla and Moodle once auth is passed */
    function user_authenticated_hook (&$user, $username, $password) {
        global $redirect, $USER, $SESSION;


        if ($user->auth != 'joomdle')
            return;

        /* Login from password change, don't log in to Joomla */
        if ( (array_key_exists ('password', $_POST))  && (array_key_exists ('newpassword1', $_POST)) && (array_key_exists ('newpassword2', $_POST)) )
            return;

        complete_user_login ($user);

        $redirectless_sso = get_config('auth/joomdle', 'redirectless_sso');

        if ($redirectless_sso)
        {
            // redirect-less login
            $this->log_into_joomla ($username, $password);
            return;
        }

        // Normal login
        $login_data = base64_encode ($username.':'.$password);

        $redirect_url = get_config ('auth/joomdle', 'joomla_url').'/index.php?option=com_joomdle&view=joomdle&task=login&data='.$login_data.'&wantsurl='. urlencode ($SESSION->wantsurl);

        redirect($redirect_url);
    }

    /* Logs the user into Joomla using cURL to set the cookies */
    function log_into_joomla ($username, $password) {
        global $CFG;

        $cookie_path = "/";

        $username = str_replace (' ', '%20', $username);
        $login_data = base64_encode ($username.':'.$password);
        $url = get_config ('auth/joomdle', 'joomla_url').'/index.php?option=com_joomdle&view=joomdle&task=login&data='.$login_data;

        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        $file =  $CFG->tempdir . "/" . random_string(20);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $file);
        curl_setopt($ch, CURLOPT_HEADER, 1);

        // Accept certificate
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $output = curl_exec($ch);
        curl_close($ch);


        if (!file_exists($file))
            die('The temporary file isn\'t there for CURL!');

        $f = fopen ($file, 'ro');

        if (!$f)
            die('The temporary file for CURL could not be opened!');

        while (!feof ($f))
        {
            $line = fgets ($f);
            // if (($line == '\n') || ($line[0] == '#'))
            if (($line == '\n') || ( strncmp ($line, '# ', 2) == 0))
                continue;
            $parts = explode ("\t", $line);
            if (array_key_exists (5, $parts))
            {
                $name = $parts[5];
                $value = trim ($parts[6]);
                setcookie ($name, $value, 60*60*24*365, $cookie_path);
            }
        }
        unlink ($file);
    }

    function update_joomla_sessions () {
        global $CFG, $DB;
        $cutoff = time() - 300;

        $query =
            "SELECT username
            FROM
            {$CFG->prefix}user
            WHERE
            auth = 'joomdle' and
            lastaccess > '$cutoff'
            ";

        $query = "SELECT username FROM {$CFG->prefix}user WHERE auth = 'joomdle' and lastaccess > '$cutoff';"; ///XXX PREFIX
        $records = $DB->get_records_sql($query);
        $usernames = array();
        foreach ($records as $record)
            $usernames[] = $record->username;

        $updates = $this->call_method ("updateSessions", $usernames);
    }

    function cron() {
        $this->update_joomla_sessions();
    }
    
    function is_site_admin($userid = false) {
        global $CFG, $USER;

        // Because this script is called many times (150+ for course page) with
        // the same parameters, it is worth doing minor optimisations. This static
        // cache stores the value for a single userid, saving about 2ms from course
        // page load time without using significant memory. As the static cache
        // also includes the value it depends on, this cannot break unit tests.

        if ($userid) {
            $siteadmins = explode(',', $CFG->siteadmins);
            $knownresult = in_array($userid, $siteadmins);
        } else {
            $siteadmins = explode(',', $CFG->siteadmins);
            $knownresult = in_array($USER->id, $siteadmins);
        }
        
        return $knownresult;
    }
     function sync_session($username){
        global $CFG;
        require_once($CFG->dirroot."/../configuration.php");
//        $sync = $this->call_method("syncMoodleSession",$username);
        $config = new JConfig();
        $conn = mysql_connect($CFG->dbhost,$CFG->dbuser,$CFG->dbpass);
        mysql_select_db($config->db,$conn);
        $query = "SELECT * FROM joc_session WHERE username = '".$username."'";
        $results = mysql_query($query,$conn);
        $max = 0;
        $id = 0;
        $value='';
        while($result =  mysql_fetch_array($results)){
            if($result['time'] > $max){
                $max = $result['time'];
                $id = $result['session_id'];
                $data = $result['data'];
            }
        }

        $string='"session.timer.now";i:';
        $position = 0;
        $pos = -1;
        while (($pos = strpos($data, $string, $pos+1)) !== false) {
            $position = $pos;
            break;
        }
        $start=$position+22;
        $end=10;
        $time=time();
        $data=str_replace(substr($data,$start,$end),$time,$data);
        $query = "UPDATE joc_session SET time = '".$time."',data = '".addslashes($data)."' WHERE session_id = '".$id."'";

        mysql_query($query,$conn);
        mysql_close($conn);
//        return $sync;
    }

    function sync_lifetime($time,$oldtime){
        global $CFG, $DB;
        $config = new StdClass();
        $config->id='';
        $config->userid='2';
        $config->timemodified = time();
        $config->plugin = NULL;
        $config->name = 'sessiontimeout';
        $config->value = $time * 60;
        $config->oldvalue = $oldtime * 60;
        $value = $time * 60;
        $sql = "UPDATE {$CFG->prefix}config SET value = '".$value."' WHERE name = 'sessiontimeout'";
        if(!$DB->insert_record('config_log',$config)||!$DB->execute($sql)){
            return false;
        }

        purge_all_caches();

        return true;
    }

    function get_feedback_question ($survey_id) {
        global $CFG, $DB;

        $sort = 'id ASC';
        $select =  'survey_id ='. (int)$survey_id;
        $q = $DB->get_records_select('questionnaire_question', $select, array(), $sort, 'id,survey_id,type_id,name,content,position, deleted, params, length');
        return $q;
    }

    function get_questionnaire_id($course_id){
        global $CFG, $DB;
        $idsection = $DB->get_record('course_sections', array('course'=>$course_id, 'section'=>0));
        $module_questionnaire_info = $DB->get_record('modules',array('name' =>'questionnaire'));
//        add_activity_for_section($course_id, $idsection->id, 'Unit information', 0, $module_page_info->name, $module_page_info->id);        $idsection = $DB->get_record('course_sections', array('course'=>$course_id, 'section'=>0));
        $sort = 'id DESC';
        $select =  'course = '.$course_id.' AND module = '.$module_questionnaire_info->id.' AND section = '.$idsection->id;
        $id_module_questionnaire = $DB->get_records_select('course_modules', $select, array(), $sort, 'id,module,instance,section');
        return $id_module_questionnaire;
    }
    
    // function for mobile
    function get_quiz_feedback($username) {
        global $DB, $CFG, $SESSION;
        $username = utf8_decode($username);
        $username = strtolower($username);
        
        $response = array();
        if((!isloggedin() or isguestuser()) && !empty($SESSION->has_timed_out) && !empty($CFG->dbsessions)) {
            $response['status'] = false;
            $response['message'] = 'User not login';
            $response['questions'] = array();
            return $response;
        }
        $question = $this->get_feedback_question(0);
        $response['status'] = true;
        $response['message'] = 'success';
        $response['questions'] = $question;
        return $response;
    }

    function save_question_questionnaire($name, $length, $survey, $type, $position, $content) {
        global $DB, $CFG, $USER;

        $name = utf8_decode ($name);
        $name = strtolower ($name);
        if($name) {
            $fiel_table_question_questionnaire = new stdClass();
            $fiel_table_question_questionnaire->name    = $name;
            $fiel_table_question_questionnaire->length  = $length;
            $fiel_table_question_questionnaire->survey_id  = $survey;
            $fiel_table_question_questionnaire->type_id    = $type;
            $fiel_table_question_questionnaire->position= $position;
            $fiel_table_question_questionnaire->content = $content;
            if($type==2){
                $fiel_table_question_questionnaire->precise = 1000;
            }
            $id_question_questionnaire = $DB->insert_record('questionnaire_question', $fiel_table_question_questionnaire);

            $fiel_table_questionnaire_quest_choice = new stdClass();
            $fiel_table_questionnaire_quest_choice->question_id = $id_question_questionnaire;
            $fiel_table_questionnaire_quest_choice->content = $content;
            $id_questionnaire_quest_choice = $DB->insert_record('questionnaire_quest_choice', $fiel_table_questionnaire_quest_choice);

            return true;
        }
        else
            return false;

    }
    function add_question_to_feedback($username, $feedbackid, $lenght, $question) {
        global $DB, $CFG;
        foreach ($question as $ques) {
            $this->save_question_questionnaire($ques['name'], $lenght, $feedbackid, $ques['type_id'], $ques['position'], $ques['content']);
        }
        $response = array();
        $response['status'] = true;
        $response['message'] = 'Question added';
        return $response;
    }

    function update_course_section($courseid, $action = 0, $title = null, $des = null, $lo = null, $sectionid = null) {
        global $CFG, $DB;
        if(!$DB->record_exists("course", array('id'=>$courseid))) {
            return $response = array(
                'status' => false,
                'message' => 'No course found.',
                'datas' => array()
            );
        }
        if($DB->record_exists("course_request_approve", array('courseid'=>$courseid))) {
            $course_request = $DB->get_record('course_request_approve', array('courseid' => (int)$courseid));
            if($course_request->status == 0 || $course_request->status == 1) {
                return $response = array(
                    'status' => false,
                    'message' => 'Course is pending approval. You can not update this course',
                    'datas' => array()
                );
            }
        }
        $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
        $courseformatoptions = course_get_format($course)->get_format_options();


        $title = utf8_decode($title);
        $des = utf8_decode($des);
        $lo = utf8_decode($lo);

        if ($action == 0) {         // list sections
            $sections = $DB->get_records_select('course_sections', 'course = :course', array('course' => $courseid ), null, 'id, course, section, name, summary, summaryformat, sequence, learningoutcomes');
            $res = array(
                'status' => 1,
                'message' => 'Success',
                'datas' => array(
                    'res'=>1,
                    'numsections' => $courseformatoptions['numsections'],
                    'sections' => $sections
                )
            );
            return $res;
        } else if (isset($courseformatoptions['numsections'])) {
            if ($action == 1) {     // create section
                $courseformatoptions['numsections']++;

//                $section = $DB->get_record_select('course_sections', 'course = ? AND section = ?', array($courseid, (int)$courseformatoptions['numsections']));
                $section = $DB->record_exists_select('course_sections', 'course = ? AND section = ?', array($courseid, (int)$courseformatoptions['numsections']));

                if (!$section) {
                    $section = new stdClass();
                    $section->course = $courseid;
                    $section->section = $courseformatoptions['numsections'];
                    $section->name = $title;
                    $section->summary = $des;
                    $section->summaryformat = 2; //plain text
                    $section->learningoutcomes = $lo;
                    $DB->insert_record('course_sections', $section);
                } else {
                    $section = $DB->get_record('course_sections', array('course' => $courseid, 'section' => ((int)$courseformatoptions['numsections']) ), '*', MUST_EXIST);
                    $section->name = $title;
                    $section->summary = $des;
                    $section->summaryformat = 2; //plain text
                    $section->learningoutcomes = $lo;
                    $DB->update_record('course_sections', $section);
                }

            } else if ($action == 2) {      // update section
                $section = $DB->get_record('course_sections', array('course' => $courseid, 'id' => $sectionid ), '*', MUST_EXIST);

                $section->name = $title;
                $section->summary = $des;
                $section->summaryformat = 2; //plain text
                $section->learningoutcomes = $lo;
                $DB->update_record('course_sections', $section);
            } if ($action == 3) {           // delete section
                $section = $DB->get_record('course_sections', array('course' => $courseid, 'id' => $sectionid), '*', MUST_EXIST);
                $select = 'course = ' . $section->course . ' AND section = ' . $sectionid;
                $moduleInSection1 = $DB->get_records_select('course_modules', $select, array(), '', 'id,instance,section');
                $certificate_id_course = $DB->get_record('course_modules', array('course' => $courseid, 'module' => 5), 'id,section');
                foreach ($moduleInSection1 as $a => $id) {
                    $beforeid = (int)$certificate_id_course->id;
                    $id = $a;

                    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
                    $cm = get_coursemodule_from_id(null, $id, $course->id, false, MUST_EXIST);
                    $modcontext = context_module::instance($cm->id);

                    $section0 = 0;
                    if (!$section_temp = $DB->get_record('course_sections', array('course' => $course->id, 'section' => $section0))) {
                        throw new moodle_exception('AJAX commands.php: Bad section ID ' . $section0);
                    }

                    if ($beforeid > 0) {
                        $beforemod = get_coursemodule_from_id('', $beforeid, $course->id);
                        $beforemod = $DB->get_record('course_modules', array('id' => $beforeid));
                    } else {
                        $beforemod = NULL;
                    }

                    moveto_module($cm, $section_temp, $beforemod);
                }

                if ($section->section != $courseformatoptions['numsections']) {
                    $i = $section->section + 1;
                    for ($i; $i <= $courseformatoptions['numsections']; $i++) {
                        $sec = $DB->get_record('course_sections', array('course' => $courseid, 'section' => $i), '*', MUST_EXIST);
                        move_section_to($course, $sec->section, ((int)$sec->section - 1));
                    }
                    $section->section = $courseformatoptions['numsections'];
                    $DB->delete_records('course_sections', array('course' => $courseid, 'id' => $section->id));
                }
                $courseformatoptions['numsections']--;
            }

            if ($courseformatoptions['numsections'] >= 0) {
                course_get_format($course)->update_course_format_options(
                    array('numsections' => $courseformatoptions['numsections']));
            }
        }

        if($action != 3) {
            $s = $DB->get_record('course_sections', array('course' => $courseid, 'section' => ((int)$section->section)), 'id, course, section, name, summary, summaryformat, sequence, learningoutcomes', MUST_EXIST);
        }
        else{
            $s = $DB->get_record('course_sections', array('course' => $courseid, 'section' => ($courseformatoptions['numsections'])), 'id, course, section, name, summary, summaryformat, sequence, learningoutcomes', MUST_EXIST);
        }

        $this->check_invisible_activities_and_rebuild_course_cache($courseid);
        $res = array(
                    'status' => 1,
                    'message' => 'Success',
            'datas' => array(
                'res'=>1,
                'numsections' => $courseformatoptions['numsections'],
                'sections' => array($s)
            )
            );
        return $res;
    }

    function get_manage_page_data($username) {

        $mycategory = $this->get_my_categories($username);
        if ($mycategory['status']) {
            foreach ($mycategory['categories'] as $mycat) {
                if ($mycat['role'] == 'LP') {
                    $lpcat_arr[] = $mycat['catid'];
                    $malpcat_arr[] = $mycat['catid'];
                } else if ($mycat['role'] == 'CMA') {
                    $macat_arr[] = $mycat['catid'];
                    $malpcat_arr[] = $mycat['catid'];
                } else if ($mycat['role'] == 'non-cat') {
                    $noncat_arr[] = $mycat['catid'];
                }
            }
        }

        if (!empty($noncat_arr)) $noncat = implode(',', $noncat_arr); else $noncat = '';
        if (!empty($malpcat_arr)) $malpcat = implode(',', $malpcat_arr); else $malpcat = '';

        $cursos = ($noncat) ? $this->my_own_courses($username, $noncat) : array();

        $courses_app = ($malpcat) ? $this->learning_provider_courses($username, $malpcat) : array();

        $macourses = ($malpcat) ? $this->my_own_courses($username, $malpcat, 0, 0, 1) : array();

        $courses = $this->my_courses($username, 0, 1);

        $result = array(
            'mycategory' => json_encode($mycategory),
            'cursos' => json_encode($cursos),
            'courses_app' => json_encode($courses_app),
            'macourses' => json_encode($macourses),
            'courses' => json_encode($courses)
        );
        return $result;
    }

    function get_course_page_data($id, $username) {
        global $DB;

        // $course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);       performance my learning
        $userRoles = $this->get_user_role( $id, $username );

        $course_info = $this->get_course_info($id, $username);
        $check_table_course_completion_criteria = $DB->get_record('course_completion_criteria', array('course' => $course_info['remoteid'], 'module' => 'questionnaire'));
        if ($check_table_course_completion_criteria == true ) {
            $deleteto_course_completion_criteria = new stdClass();
            $deleteto_course_completion_criteria->id = $check_table_course_completion_criteria->id;
            $DB->delete_records('course_completion_criteria', array('id' => $deleteto_course_completion_criteria->id));
        }

        $this->update_last_access_course($id, $username); //save last access time

        if ($course_info['enroled']) $mods = $this->get_course_mods_visible( $id, $username, 1);
        else $mods = $this->get_course_mods_visible( $id, '', 1);

        $a = json_decode($userRoles["roles"][0]['role'], true);
        $user_role = $a[0]['sortname'];

        $modsForCreator = array();
        $questions = array();
        if ($user_role != 'student' && $user_role != 'teacher') {
            $questions = $this->load_questions_bank($id, null, 1);
            if (is_array($mods)) {
                foreach ($mods as $tema) {
                    $resources = $tema['mods'];
                    if (is_array($resources)) {
                        foreach ($resources as $key => $resource) {

                            switch ($resource['mod']) {
                                case 'page':
                                    $page = $this->create_page_activity(0, $id, $username, array('pid' => (int)$resource['id']), 0);

                                    $modsForCreator[] = array('id'=>(int)$resource['id'], 'type'=>$resource['mod'], 'data'=>$page);
                                    break;
                                case "quiz":
                                    $quiz = $this->create_quiz_activity(0, $id, $username, array('qid' => (int)$resource['id']), 0);

                                    // format description paragraph
                                    $options['noclean'] = true;
                                    $context = context_course::instance($id);
                                    $quiz['des'] = file_rewrite_pluginfile_urls($quiz['des'], 'pluginfile.php', $context->id, 'course', 'summary', NULL);
                                    $quiz['des'] = str_replace('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $quiz['des']);
                                    $quiz['des'] = format_text($quiz['des'], FORMAT_MOODLE, $options);

                                    $modsForCreator[] = array('id'=>(int)$resource['id'], 'type'=>$resource['mod'], 'data'=>$quiz);
                                    break;
                                case 'assign':
                                    $assignment = $this->create_assignment_activity(0, $id, $username, array('aid' => (int)$resource['id']), 0);

                                    // format description paragraph
                                    $options['noclean'] = true;
                                    $context = context_course::instance($id);
                                    $assignment['des'] = file_rewrite_pluginfile_urls($assignment['des'], 'pluginfile.php', $context->id, 'course', 'summary', NULL);
                                    $assignment['des'] = str_replace('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $assignment['des']);
                                    $assignment['des'] = format_text($assignment['des'], FORMAT_MOODLE, $options);

                                    $modsForCreator[] = array('id'=>(int)$resource['id'], 'type'=>$resource['mod'], 'data'=>$assignment);
                                    break;
                                case 'questionnaire':
                                    $questionnaire = $this->get_info_questionnaire( (int)$resource['id'], $username);
                                    $modsForCreator[] = array('id'=>(int)$resource['id'], 'type'=>$resource['mod'], 'data'=>$questionnaire);
                                    break;
                                case 'scorm':
                                    $modsForCreator[] = array('id'=>(int)$resource['id'], 'type'=>$resource['mod'], 'name'=>$resource['name']);
                                    break;
                                default:
                                    # code...
                                    break;
                            }
                        }
                    }
                }
            }
        }

        $completeFeedback = false;
        // student check complete course feedback
        if($user_role == 'student') {
            $user = get_complete_user_data('username', $username);
            $questionnaire_info_course = $this->get_questionnaire_id($id);
            foreach ($questionnaire_info_course as $questionnaire_info) {
                $questionnaire_info_id = $questionnaire_info->id;
            }

            $query1 = "SELECT *
                FROM mdl_course_modules
                WHERE id =" . $questionnaire_info_id;
            $questionnarie = $DB->get_record_sql($query1);
            $data1 = array();
            $data1['module_id'] = $questionnarie->id;
            $data1['questionnaire_id'] = $questionnarie->instance;
            $data1['courseid'] = $questionnarie->course;


            $q1 = "SELECT *
                FROM mdl_questionnaire
                WHERE id =" . $data1['questionnaire_id'];
            $qk1 = $DB->get_record_sql($q1);
            $data1['name'] = $qk1->name;
            $data1['intro'] = $qk1->intro;
            $data1['surveyid'] = $qk1->sid;

            $checkCompleteFeedback = $DB->get_record('questionnaire_response', array('survey_id' => $data1['surveyid'], 'username' => $user->id));
            if ($checkCompleteFeedback) {
                $completeFeedback = true;
            }
        }

        $topics = $this->update_course_section($id, 0);
        $progressmods = $this->get_mod_progress($id, $username);

        /* Check old course(fix visible activity + check course complete setting) */
        $this->check_invisible_activities_and_rebuild_course_cache($id);

        $result = array(
            'userRoles' => json_encode($userRoles),
            'course_info' => json_encode($course_info),
            'mods' => json_encode($mods),
            'progressmods' => json_encode($progressmods),
            'topics' => json_encode($topics),
            'modsForCreator' => json_encode($modsForCreator),
            'questions' => json_encode($questions),
            'checkCompleteFeedback' => $completeFeedback
        );
        return $result;
    }

    function check_invisible_activities_and_rebuild_course_cache($courseid) {
        $invisibleMods = $this->get_course_mods_visible( $courseid, '', 0);

        if (is_array($invisibleMods)) {
            $moduleIdStatus1 = array();

            foreach ($invisibleMods as $tema) {
                foreach ($tema['mods'] as $id => $resource) {
                    $moduleIdStatus1[] = ['module_id' => $resource['id'], 'status' => 1];
                }
            }

            if (!empty($moduleIdStatus1)) $this->update_multi_activity_module ($moduleIdStatus1, 0, $courseid);
        }
    }

    function get_course_content_page_data($id, $username) {

        $userRoles = $this->get_user_role( $id, $username );

        $course_info = $this->get_course_info($id, $username);
        
        $coursemods = $this->get_course_mods($id);

        $mods_not_visible = $this->get_course_mods_visible( $id, '', 0);

        $questions = $this->load_questions_bank($id);

        $result = array(
            'userRoles' => json_encode($userRoles),
            'course_info' => json_encode($course_info),
            'coursemods' => json_encode($coursemods),
            'mods_not_visible' => json_encode($mods_not_visible),
            'questions' => json_encode($questions)
        );
        return $result;
    }

} //class



// Events handlers

function joomdle_user_updated ($user)
{
    global $CFG, $DB;

    if ($user->auth != 'joomdle')
        return true;

    $auth_joomdle = new auth_plugin_joomdle ();

    /* Update user info in Joomla */
    $userinfo['username'] = $user->username;
    //      $userinfo['password'] = $password_clear;
    //      $userinfo['password2'] = $password_clear;
    $userinfo['name'] = $user->firstname. " " . $user->lastname;
    $userinfo['email'] = $user->email;
    $userinfo['firstname'] =  $user->firstname;
    $userinfo['lastname'] = $user->lastname;
    $userinfo['city'] = $user->city;
    $userinfo['country'] = $user->country;
    $userinfo['lang'] = $user->lang;
    $userinfo['timezone'] = $user->timezone;
    $userinfo['phone1'] = $user->phone1;
    $userinfo['phone2'] = $user->phone2;
    $userinfo['address'] = $user->address;
    $userinfo['description'] = $user->description;
    $userinfo['institution'] = $user->institution;
    $userinfo['url'] = $user->url;
    $userinfo['icq'] = $user->icq;
    $userinfo['skype'] = $user->skype;
    $userinfo['aim'] = $user->aim;
    $userinfo['yahoo'] = $user->yahoo;
    $userinfo['msn'] = $user->msn;
    $userinfo['idnumber'] = $user->idnumber;
    $userinfo['department'] = $user->department;
    $userinfo['picture'] = $user->picture;

    $id = $user->id;
    $usercontext = context_user::instance($id);
    $context_id = $usercontext->id;

    if ($user->picture)
        $userinfo['pic_url'] = $CFG->wwwroot."/pluginfile.php/$context_id/user/icon/f1";

    $userinfo['block'] = 0;

    /* Custom fields */
    $query = "SELECT f.id, d.data 
                            FROM {$CFG->prefix}user_info_field as f, {$CFG->prefix}user_info_data d 
                            WHERE f.id=d.fieldid and userid = ?";

    $params = array ($id);
    $records =  $DB->get_records_sql($query, $params);

    $i = 0;
    $userinfo['custom_fields'] = array ();
    foreach ($records as $field) {
        $userinfo['custom_fields'][$i]['id'] = $field->id;
        $userinfo['custom_fields'][$i]['data'] = $field->data;
        $i++;
    }

    $auth_joomdle->call_method ("updateUser", $userinfo);

    return true;
}


/* Creates a new Joomla user */
function joomdle_user_created ($user)
{
    global $CFG, $DB;

    if ($user->auth != 'joomdle')
        return true;

    $auth_joomdle = new auth_plugin_joomdle ();

    $password_clear = '';
    if (array_key_exists ('password', $_POST))
        $password_clear =  $_POST['password']; //Self registration
    if ((array_key_exists ('newpassword', $_POST)) && (!$password_clear))
        $password_clear =  $_POST['newpassword']; //admin form

    /* Create user in Joomla */
    $userinfo['username'] = $user->username;
    $userinfo['password'] = $password_clear;
    $userinfo['password2'] = $password_clear;
    $userinfo['name'] = $user->firstname. " " . $user->lastname;
    $userinfo['email'] = $user->email;
    $userinfo['firstname'] = $user->firstname;
    $userinfo['lastname'] = $user->lastname;
    $userinfo['city'] = $user->city;
    $userinfo['country'] = $user->country;
    $userinfo['lang'] = $user->lang;
    $userinfo['timezone'] = $user->timezone;
    $userinfo['phone1'] = $user->phone1;
    $userinfo['phone2'] = $user->phone2;
    $userinfo['address'] = $user->address;
    $userinfo['description'] = $user->description;
    $userinfo['institution'] = $user->institution;
    $userinfo['url'] = $user->url;
    $userinfo['icq'] = $user->icq;
    $userinfo['skype'] = $user->skype;
    $userinfo['aim'] = $user->aim;
    $userinfo['yahoo'] = $user->yahoo;
    $userinfo['msn'] = $user->msn;
    $userinfo['idnumber'] = $user->idnumber;
    $userinfo['department'] = $user->department;
    $userinfo['picture'] = $user->picture;

    $id = $user->id;
    $usercontext = context_user::instance($id);
    $context_id = $usercontext->id;

    if ($user->pic_url && $user->pic_url != '') {
        $userinfo['pic_url'] = $user->pic_url;
    } else if ($user->picture && (!$user->pic_url || $user->pic_url == '')) {
        $userinfo['pic_url'] = $CFG->wwwroot."/pluginfile.php/$context_id/user/icon/f1";
    }

    $userinfo['block'] = 0;

    /* Custom fields */
    $query = "SELECT f.id, d.data 
                    FROM {$CFG->prefix}user_info_field as f, {$CFG->prefix}user_info_data d 
                    WHERE f.id=d.fieldid and userid = ?";

    $params = array ($id);
    $records =  $DB->get_records_sql($query, $params);

    $i = 0;
    $userinfo['custom_fields'] = array ();
    foreach ($records as $field) {
        $userinfo['custom_fields'][$i]['id'] = $field->id;
        $userinfo['custom_fields'][$i]['data'] = $field->data;
        $i++;
    }

    $auth_joomdle->call_method ("createUser", $userinfo);

    return true;
}

function joomdle_user_deleted ($user)
{
    global $CFG, $DB;

    if ($user->auth != 'joomdle')
        return true;

    $auth_joomdle = new auth_plugin_joomdle ();

    $auth_joomdle->call_method ("deleteUser", $user->username);
    return true;
}

function joomdle_course_created ($course)
{
    global $DB, $USER, $CFG;

    $activities = get_config('auth/joomdle', 'jomsocial_activities');
    $groups = get_config('auth/joomdle', 'jomsocial_groups');
    $autosell = get_config('auth/joomdle', 'auto_sell');
    $joomla_user_groups = get_config('auth/joomdle', 'joomla_user_groups');
    $use_kunena_forums = get_config('auth/joomdle', 'use_kunena_forums');

    $auth_joomdle = new auth_plugin_joomdle ();

    /* kludge for the call_method fn to work */
    if (!$course->summary)
        $course->summary = ' ';

    $conditions = array ('id' => $course->category);
    $cat = $DB->get_record('course_categories',$conditions);

    $context = context_course::instance($course->id);
    $course->summary = file_rewrite_pluginfile_urls ($course->summary, 'pluginfile.php', $context->id, 'course', 'summary', NULL);
    $course->summary = str_replace ('pluginfile.php', '/auth/joomdle/pluginfile_joomdle.php', $course->summary);
    if ($activities)
        $auth_joomdle->call_method ('addActivityCourse', (int) $course->id, $course->fullname,  $course->summary, (int) $course->category, $cat->name);
    if ($groups)
        if(isset($course->creator)) {
            $userconditions = array("id" => $course->creator);
            $user = $DB->get_record("user", $userconditions);
            $username = $user->username;
        } else {
            $username =  $USER->username;
        }
        $tmp_dir = $CFG->dataroot . '/temp/' . 'tmp_pic';
        $pic_name = isset($course->filename) ? $course->filename : '';

        $auth_joomdle->call_method ('addSocialGroup', $course->fullname,  $course->summary,  (int) $course->id, $username, $tmp_dir, $pic_name);
    //      $auth_joomdle->call_method ('addJSGroup', $course->fullname,  get_string('auth_joomla_group_for_course', 'auth_joomdle') . ' ' .$course->fullname,  (int) $course->id, "x");

    if ($autosell) {
        $cid = array ($course->id);
        $auth_joomdle->call_method ("sellCourse", array ((int) $course->id));
    }

    if ($joomla_user_groups) {
        $auth_joomdle->call_method ('addUserGroups', (int) $course->id, $course->fullname);
    }

    if ($use_kunena_forums) {
        // Create section
        // $auth_joomdle->call_method ('addForum', (int) $course->id, (int) -2, $course->fullname);
        // Create news forum
        //      $auth_joomdle->call_method ('addForum', (int) $course->id, (int) -1, get_string('namenews', 'forum'));
    }

    return true;
}

function joomdle_course_deleted ($course)
{
    $groups_delete = get_config('auth/joomdle', 'jomsocial_groups_delete');
    $autosell = get_config('auth/joomdle', 'auto_sell');
    $use_kunena_forums = get_config('auth/joomdle', 'use_kunena_forums');
    $joomla_user_groups = get_config('auth/joomdle', 'joomla_user_groups');

    $auth_joomdle = new auth_plugin_joomdle ();

    if ($groups_delete)
        $auth_joomdle->call_method ('deleteSocialGroup', $course->id);

    if ($autosell) {
        $cid = array ($course->id);
        $auth_joomdle->call_method ("deleteCourseShop", array ((int) $course->id));
    }

    if ($joomla_user_groups)
        $auth_joomdle->call_method ("removeUserGroups", (int) $course->id);

    if ($use_kunena_forums)
        $auth_joomdle->call_method ("removeCourseForums", (int) $course->id);

    return true;
}

function joomdle_course_updated ($course)
{
    $groups = get_config('auth/joomdle', 'jomsocial_groups');
    $autosell = get_config('auth/joomdle', 'auto_sell');

    $auth_joomdle = new auth_plugin_joomdle ();

    if ($groups)
        $auth_joomdle->call_method ('updateSocialGroup', $course->fullname,  get_string('auth_joomla_group_for_course', 'auth_joomdle') . ' ' .$course->fullname,  (int) $course->id);
    //  $auth_joomdle->call_method ('updateJSGroup', $course->fullname,  get_string('auth_joomla_group_for_course', 'auth_joomdle') . ' ' .$course->fullname,  (int) $course->id, "x");

    if ($autosell) {
        $cid = array ($course->id);
        $auth_joomdle->call_method ("updateCourseShop", array ((int) $course->id));
    }

    return true;

}

function joomdle_role_assigned ($role)
{
    global $CFG, $DB;

    $activities = get_config('auth/joomdle', 'jomsocial_activities');
    $groups = get_config('auth/joomdle', 'jomsocial_groups');
    $enrol_parents = get_config('auth/joomdle', 'enrol_parents');
    $parent_role_id = get_config('auth/joomdle', 'parent_role_id');
    $points = get_config('auth/joomdle', 'give_points');
    $auto_mailing_lists = get_config('auth/joomdle', 'auto_mailing_lists');
    $use_kunena_forums = get_config('auth/joomdle', 'use_kunena_forums');
    $joomla_user_groups = get_config('auth/joomdle', 'joomla_user_groups');

    $auth_joomdle = new auth_plugin_joomdle ();

    $context = context::instance_by_id ($role->contextid);
    /* If a course enrolment, publish */
    if ($context->contextlevel == CONTEXT_COURSE) {
        $courseid = $context->instanceid;
        $conditions = array ('id' => $courseid);
        $course = $DB->get_record('course', $conditions);
        $conditions = array ('id' => $course->category);
        $cat = $DB->get_record('course_categories',$conditions);
        $conditions = array ('id' => $role->userid);
        $user = $DB->get_record('user', $conditions);

        // get user role
        $context_course = context_course::instance($course->id);
        $roles = get_user_roles($context_course, $user->id, false);
        $roles_arr = array();
        foreach ($roles as $r) {
            // store manager, course creator, editing trainer and teacher role into array
            if($r->roleid == 1 || $r->roleid == 2 || $r->roleid == 3 || $r->roleid == 4) {
                $roles_arr[] = $r->roleid;
            }
        }
        // Jomsocial activity
        if ($activities)
        {
            //$this->call_method ('addActivityCourseEnrolment', $user->username, (int) $courseid, $course->fullname, (int) $course->category, $cat->name);
            $auth_joomdle->call_method ('addActivityCourseEnrolment', $user->username, (int) $courseid, $course->fullname, (int) $course->category, $cat->name);
        }

        // Join Jomsocial group
        if ($groups)
        {
            /* Join teachers as group admins, and students as regular members */
            if (!empty($roles_arr) || in_array($role->roleid, $roles_arr)) //XXX not hardcoded value?
                //              $auth_joomdle->call_method ('addJSGroupMember', $course->fullname, $user->username, 1, (int) $courseid);
                $auth_joomdle->call_method ('addSocialGroupMember', $user->username, 1, (int) $courseid);
            else
                $auth_joomdle->call_method ('addSocialGroupMember', $user->username, -1, (int) $courseid);
            //  $auth_joomdle->call_method ('addJSGroupMember', $course->fullname, $user->username, -1, (int) $courseid);
        }

        // Enrol parents
        if (($enrol_parents) && ($parent_role_id))
        {
            if ($role->roleid == 5) //XXX not hardcoded value?
            {
                /* Get mentors for the student */
                $usercontext = context_user::instance($role->userid);
                $usercontextid = $usercontext->id;

                $query =
                    "SELECT userid
                        FROM
                        {$CFG->prefix}role_assignments
                        WHERE
                        roleid = ? and contextid = ?
                        ";

                $params = array ($parent_role_id, $usercontextid);
                $mentors =  $DB->get_records_sql($query, $params);
                foreach ($mentors as $mentor)
                {
                    /* Enrol as parent into course*/
                    //role_assign($parent_role_id, $mentor->userid, $context->id );
                    $conditions = array ('id' => $mentor->userid);
                    $parent_user = $DB->get_record('user', $conditions);

                    $auth_joomdle->enrol_user ($parent_user->username, $courseid, $parent_role_id);
                }
            }
        }

        if ($points)
            $auth_joomdle->call_method ('addPoints', 'joomdle.enrol', $user->username,   (int) $courseid, $course->fullname);

        if ($auto_mailing_lists)
        {
            $type = '';
            if ($role->roleid == 3)
                $type = 'course_teachers';
            else  if ($role->roleid == 5)
            {
                $type = 'course_students';

                /* Get mentors for the student */
                $usercontext = context_user::instance($role->userid);
                $usercontextid = $usercontext->id;

                $query =
                    "SELECT userid
                        FROM
                        {$CFG->prefix}role_assignments
                        WHERE
                        roleid = ? and contextid = ?
                        ";

                $params = array ($parent_role_id, $usercontextid);
                $mentors =  $DB->get_records_sql($query, $params);
                foreach ($mentors as $mentor)
                {
                    $conditions = array ('id' => $mentor->userid);
                    $parent_user = $DB->get_record('user', $conditions);

                    $auth_joomdle->call_method ('addMailingSub',  $parent_user->username,   (int) $courseid, 'course_parents');
                }

            }

            if ($type)
                $auth_joomdle->call_method ('addMailingSub',  $user->username,   (int) $courseid, $type);
        }

        if ($joomla_user_groups)
        {
            $type = '';
            if ($role->roleid == 3)
                $type = 'teachers';
            else  if ($role->roleid == 5)
                $type = 'students';

            if ($type)
                $auth_joomdle->call_method ('addGroupMember',  (int) $courseid, $user->username, $type);
        }

        if ($use_kunena_forums)
        {
            // if ($role->roleid == 3)
                // $auth_joomdle->call_method ('addForumsModerator',  (int) $courseid, $user->username);
        }
    }

    return true;
}

function joomdle_role_unassigned ($role)
{
    global $DB, $CFG;

    $groups = get_config('auth/joomdle', 'jomsocial_groups');
    $auto_mailing_lists = get_config('auth/joomdle', 'auto_mailing_lists');
    $use_kunena_forums = get_config('auth/joomdle', 'use_kunena_forums');
    $joomla_user_groups = get_config('auth/joomdle', 'joomla_user_groups');
    $parent_role_id = get_config('auth/joomdle', 'parent_role_id');

    $auth_joomdle = new auth_plugin_joomdle ();

    $context = context::instance_by_id ($role->contextid);
    /* If a course unenrolment, remove from group */
    if ($context->contextlevel == CONTEXT_COURSE) {
        $courseid = $context->instanceid;
        $conditions = array ('id' => $courseid);
        $course = $DB->get_record('course', $conditions);
        $conditions = array ('id' => $course->category);
        $cat = $DB->get_record('course_categories', $conditions);
        $conditions = array ('id' => $role->userid);
        $user = $DB->get_record('user', $conditions);

        if ($groups)
            $auth_joomdle->call_method ('removeSocialGroupMember', $user->username, $courseid);
        //  $auth_joomdle->call_method ('removeJSGroupMember', $course->fullname, $user->username);

        if ($auto_mailing_lists)
        {
            if ($role->roleid == 3)
                $type = 'course_teachers';
            else  if ($role->roleid == 5)
            {
                $type = 'course_students';

                /* Get mentors for the student */
                $usercontext = context_user::instance($role->userid);
                $usercontextid = $usercontext->id;

                $query =
                    "SELECT userid
                        FROM
                        {$CFG->prefix}role_assignments
                        WHERE
                        roleid = ? and contextid = ?
                        ";

                $params = array ($parent_role_id, $usercontextid);
                $mentors =  $DB->get_records_sql($query, $params);
                foreach ($mentors as $mentor)
                {
                    $conditions = array ('id' => $mentor->userid);
                    $parent_user = $DB->get_record('user', $conditions);

                    $auth_joomdle->call_method ('removeMailingSub',  $parent_user->username,   (int) $courseid, 'course_parents');
                }

            }

            $auth_joomdle->call_method ('removeMailingSub',  $user->username,   (int) $courseid, $type);
        }

        if ($joomla_user_groups)
        {
            $type = '';
            if ($role->roleid == 3)
                $type = 'teachers';
            else  if ($role->roleid == 5)
                $type = 'students';

            if ($type)
                $auth_joomdle->call_method ('removeGroupMember',  (int) $courseid, $user->username, $type);
        }

        if ($use_kunena_forums)
        {
            if ($role->roleid == 3)
                $auth_joomdle->call_method ('removeForumsModerator',  (int) $courseid, $user->username);
        }
    }

    return true;
}

function joomdle_quiz_attempt_submitted ($event)
{
    global $DB , $CFG;

    $activities = get_config('auth/joomdle', 'jomsocial_activities');
    $points = get_config('auth/joomdle', 'give_points');

    $auth_joomdle = new auth_plugin_joomdle ();

    $course  = $DB->get_record('course', array('id' => $event->courseid));
    $quiz    = $DB->get_record('quiz', array('id' => $event->quizid));
    $user    = $DB->get_record('user', array('id' => $event->submitterid));

    if ($activities)
        $auth_joomdle->call_method ('addActivityQuizAttempt', $user->username, (int) $event->courseid, $course->fullname,  $quiz->name);

    if ($points)
        $auth_joomdle->call_method ('addPoints', 'joomdle.quiz_attempt', $user->username,   (int) $event->courseid, $course->fullname);

    return true;
}

function joomdle_mod_created ($event)
{
    $use_kunena_forums = get_config('auth/joomdle', 'use_kunena_forums');

    $auth_joomdle = new auth_plugin_joomdle ();

    if ($use_kunena_forums) {
        if ($event->modulename == 'forum')
        {
            // $auth_joomdle->call_method ('addForum', (int) $event->courseid, $event->cmid, $event->name);
        }
    }

    return true;
}

function joomdle_mod_deleted ($event)
{
    $use_kunena_forums = get_config('auth/joomdle', 'use_kunena_forums');

    $auth_joomdle = new auth_plugin_joomdle ();

    if ($use_kunena_forums) {
        if ($event->modulename == 'forum')
        {
            $auth_joomdle->call_method ("removeForum", (int) $event->courseid, $event->cmid);
        }
    }

    return true;
}

function joomdle_mod_updated ($event)
{
    $use_kunena_forums = get_config('auth/joomdle', 'use_kunena_forums');

    $auth_joomdle = new auth_plugin_joomdle ();

    if ($use_kunena_forums) {
        if ($event->modulename == 'forum')
        {
            $auth_joomdle->call_method ("updateForum", (int) $event->courseid, $event->cmid, $event->name);
        }
    }

    return true;
}

function joomdle_course_completed ($event)
{
    global $DB , $CFG;

    $activities = get_config('auth/joomdle', 'jomsocial_activities');
    $points = get_config('auth/joomdle', 'give_points');

    $auth_joomdle = new auth_plugin_joomdle ();

    $course  = $DB->get_record('course', array('id' => $event->course));
    $user    = $DB->get_record('user', array('id' => $event->userid));

    if ($activities)
        $auth_joomdle->call_method ('addActivityCourseCompleted', $user->username, (int) $event->course, $course->fullname);

    if ($points)
        $auth_joomdle->call_method ('addPoints', 'joomdle.course_completed', $user->username,   (int) $event->courseid, $course->fullname);

    return true;
}
function joomdle_update_product ( $courseid) {
    $auth_joomdle = new auth_plugin_joomdle ();

    $res = $auth_joomdle->call_method ("updateHikashopProduct", $courseid );
    return $res;
}


?>
