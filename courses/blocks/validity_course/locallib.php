<?php
/**
 *
 * @package    block
 * @subpackage profile
 * @copyright  2011 Mohamed Alsharaf
 * @author     Mohamed Alsharaf <mohamed.alsharaf@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

include_once $CFG->libdir . '/form/text.php';

/**
 * Helper class for the block
 *
 * @copyright  2011 Mohamed Alsharaf
 * @author     Mohamed Alsharaf <mohamed.alsharaf@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class validity_course_helper
{
    private $name = 'validity_course';

    /**
     * Return string from the block language
     * 
     * @param string $name
     * @param object $a
     */
    public function get_string($name, $a = null)
    {
        return get_string($name, 'block_' . $this->name, $a);
    }

    /**
     * Return block unique name
     * 
     * @return string
     */
    public function get_name()
    {
        return $this->name;
    }

    /**
     * Return the directory path of the block
     * 
     * @global object $CFG
     * @return string
     */
    public function get_dirpath()
    {
        global $CFG;
        return $CFG->dirroot . '/blocks/' . $this->name . '/';
    }

    /**
     * Get list of site roles names
     *
     * @param context $context
     * @param array $exclude
     * @return array
     */
    public function get_roles($context, $exclude = array())
    {
        $return = array();

        $roles = get_all_roles();
        $rolenames = role_fix_names($roles, $context, ROLENAME_ORIGINAL);
        foreach ($rolenames as $role) {
            if (!in_array($role->id, $exclude)) {
                $return[$role->id] = $role->localname;
            }
        }

        return $return;
    }
    public function get_course_user_enrol($userid, $courseid) {
        global $DB;
        $query = "SELECT er.* "
                . "FROM mdl_user_enrolments as er "
                . "JOIN mdl_enrol as e ON er.enrolid = e.id "
                . "JOIN mdl_course as c ON e.courseid = c.id "
                . "WHERE c.id = ? AND er.userid = ?";
        $params = array($courseid, $userid);
        $record = $DB->get_record_sql($query, $params);
        return $record;
    }
	public function get_prog_user_enrol($userid,$courseid){
        global $DB;
         $query = "SELECT c.* "
                . "FROM mdl_prog_courseset_course as er "
                . "JOIN mdl_prog_courseset as e ON er.coursesetid = e.id "
                . "JOIN mdl_prog_completion as c ON e.programid = c.programid "
                . "WHERE er.courseid = ? AND c.userid = ? AND status=0";
        $params = array($courseid, $userid);
         $record = $DB->get_record_sql($query, $params);
        return $record;
    }
    public function get_user_enrol($userid,$courseid) {
        global $DB;
        $query = "SELECT er.*, e.enrol "
            . "FROM mdl_user_enrolments as er "
            . "JOIN mdl_enrol as e ON er.enrolid = e.id "
            . "JOIN mdl_course as c ON e.courseid = c.id "
            . "WHERE c.id = ? AND er.userid = ? AND e.status = 0";
        $params = array($courseid, $userid);
        $records = $DB->get_records_sql($query, $params);

        return $records;
    }
}
