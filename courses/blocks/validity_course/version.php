<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version = 2012083002;
$plugin->requires = 2012062500;
$plugin->maturity = MATURITY_STABLE;
$plugin->release = '1.0';

