<?php

/**
 *
 * @package    block
 * @subpackage profile
 * @copyright  2011 Mohamed Alsharaf
 * @author     Mohamed Alsharaf <mohamed.alsharaf@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * Displays user(s) profile information.
 *
 * @copyright  2011 Mohamed Alsharaf
 * @author     Mohamed Alsharaf <mohamed.alsharaf@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_validity_course extends block_base
{

    protected $helper;

    public function init()
    {
        global $CFG;

        include_once realpath(dirname(__FILE__)) . '/locallib.php';
        $this->helper = new validity_course_helper;
        $this->title = $this->helper->get_string('pluginname');
    }


    /**
     * allow more than one instance of the block on a page
     *
     * @return boolean
     */
    public function instance_allow_multiple()
    {
        return true;
    }
    /**
     * instance specialisations (must have instance allow config true)
     *
     * @return void
     */
    public function specialization()
    {
        global $CFG;
        $this->title = html_writer::tag('h2', 'Course Validity');
        
    }
    
    /**
     * The settings block cannot be hidden by default as it is integral to
     * the navigation of Moodle.
     *
     * @return false
     */
    function  instance_can_be_hidden() {
        return false;
    }
     /**
     * Set the applicable formats for this block to all
     * @return array
     */
    function applicable_formats() {
        return array('all' => true);
    }
     /**
     * Allow the user to configure a block instance
     * @return bool Returns true
     */
    function instance_allow_config() {
        return true;
    }
    /**
     * Display the content of a block
     *
     * @global moodle_database $DB
     * @return object
     */
   	public function get_content()
    {
        global $USER, $COURSE;
        if ($this->content !== NULL) {
            return $this->content;
        }


        $this->content = new stdClass;
        $this->content->text = '<div class="course-validity">';

        if (isset($this->config->message) != '') {
            $this->content->text .= '<div class="desc">' . format_text($this->config->message, FORMAT_MOODLE) . '</div>';
        }
        // get deadline assignment of section
        $this->content->text .= '<div class="course-validity-content">';
        
        $user = get_complete_user_data('username', $USER->username);
//        $course_enrol = validity_course_helper::get_course_user_enrol($user->id, $COURSE->id);
//        $progra_enrol=  validity_course_helper::get_prog_user_enrol($user->id, $COURSE->id);

//        if (allow_guest_access($COURSE->id) ) {
//                $validuntil = 'Unlimited';
//        } else if(($progra_enrol->timedue) && ($progra_enrol->timedue > 0) ) {
//                $validuntil = date('d M Y', $progra_enrol->timedue);
//        } else if ($course_enrol->timeend) {
//                $validuntil = date('d M Y', $course_enrol->timeend);
//
//        } else {
//                $validuntil = 'Unlimited';
//        }
        $enrol=  validity_course_helper::get_user_enrol($user->id, $COURSE->id);
        if (!empty($enrol)) {
            foreach ($enrol as $k => $v) {
                if ($v->enrol == "manual") {
                    $manual_enrol_timeend = $v->timeend;
                } else if ($v->enrol == "totara_program") {
                    $program_enrol_timeend = $v->timeend;
                } else {
                    $enrol_timeend = $v->timeend;
                }
            }
        }
        if (allow_guest_access($COURSE->id) ) {
            $validuntil = 'Unlimited';
        } else if (!empty($program_enrol_timeend)) {
            $validuntil = ($program_enrol_timeend != 0) ? date('d M Y', $program_enrol_timeend) : 'Unlimited';
        } else if (!empty($manual_enrol_timeend)) {
            $validuntil = ($manual_enrol_timeend != 0) ? date('d M Y', $manual_enrol_timeend) : 'Unlimited';
        } else if (!empty($enrol_timeend)) {
            $validuntil = ($enrol_timeend != 0) ? date('d M Y', $enrol_timeend) : 'Unlimited';
        } else {
            $validuntil = 'Unlimited';
        }

        $this->content->text .= '<span class="valid-until">Valid until '.$validuntil.'</span>';
        $this->content->text .= '</div>';
        $this->content->text .= '</div>';
        $this->content->footer = '';

        return $this->content;
    }



    /**
     * allow instances to have their own configuration
     *
     * @return boolean
     */
//    public function instance_allow_config()
//    {
//        return true;
//    }



}
?>




