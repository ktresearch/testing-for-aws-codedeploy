<?php

/**
 *
 * @package    block
 * @subpackage profile
 * @copyright  2011 Mohamed Alsharaf
 * @author     Mohamed Alsharaf <mohamed.alsharaf@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * Displays user(s) profile information.
 *
 * @copyright  2011 Mohamed Alsharaf
 * @author     Mohamed Alsharaf <mohamed.alsharaf@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_unit extends block_base
{

    protected $helper;

    public function init()
    {
        $this->title = get_string('config_default_title', 'block_unit');
    }

    /**
     * Display the content of a block
     *
     * @global moodle_database $DB
     * @return object
     * 
     */
    public function get_content()
    {
        if ($this->content !== NULL) {
            return $this->content;
        }


        $this->content = new stdClass;
        $this->content->text = '<div class="unitblock">';

        
        $this->content->text .= '<div class="desc">This block not using. Please remove it!</div>';
        
        
        $this->content->text .= '</div>';
        $this->content->footer = '';

        return $this->content;
    }
    
    public function instance_config_save($data, $nolongerused = false)
    {

        return parent::instance_config_save($data, $nolongerused);
    }

    /**
     * allow more than one instance of the block on a page
     *
     * @return boolean
     */
    public function instance_allow_multiple()
    {
        return true;
    }

    /**
     * allow instances to have their own configuration
     *
     * @return boolean
     */
    public function instance_allow_config()
    {
        return true;
    }

    /**
     * instance specialisations (must have instance allow config true)
     *
     * @return void
     */
    public function specialization()
    {
        
            $this->title = 'My Progress';
    }

    /**
     * disable the displays of the instance configuration form (config_instance.html)
     *
     * @return boolean
     */
    public function instance_config_print()
    {
        return false;
    }

    /**
     * locations where block can be displayed
     *
     * @return array
     */
    public function applicable_formats()
    {
        return array('all'=> false,'course-view' => true, 'mod-*' => true);
    }

    /**
     * post install configurations
     *
     */
    public function after_install()
    {
        
    }

    /**
     * post delete configurations
     *
     */
    public function before_delete()
    {
        
    }

}
