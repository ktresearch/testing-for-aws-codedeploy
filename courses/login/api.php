<?php
/*
 * Login API app
 * 
 * @author LSS
 * @package API
 * 
 */


require('../config.php');

$response = array();
$user = false;
if (!isset($_POST['action'])) {
    header('Content-type: application/json;charset=UTF-8');
    header("Status", true, 400);
    echo json_encode(array("error"=>"Action is missing."));
    exit();
} else if ($_POST['action'] == '') {
    header('Content-type: application/json;charset=UTF-8');
    header("Status", true, 400);
    echo json_encode(array("error"=>"Action is blank."));
    exit();
} else if ($_POST['action'] == 'login') {
    apilogin($_POST['username'], $_POST['password']);
} else if ($_POST['action'] == 'logout') {
    apilogout($_POST['username'], $_POST['password']);
} else if ($_POST['action'] == 'login_social') {
    $appToken = $_POST['appToken'];
    $appID = $_POST['appID'];
    $appType = $_POST['appType'];
    
    if (!isset($appToken)) {
        header('Content-type: application/json;charset=UTF-8');
        header("Status", true, 400);
        echo json_encode(array("error"=>"Token is not set."));
        exit();
    } 
    else if ($appID == '') {
        header('Content-type: application/json;charset=UTF-8');
        header("Status", true, 400);
        echo json_encode(array("error"=>"AppID is not set."));
        exit();
    } elseif($appType == '') {
        header('Content-type: application/json;charset=UTF-8');
        header("Status", true, 400);
        echo json_encode(array("error"=>"AppType is not set."));
        exit();
    }
    else {
        loginSocial($appToken, $appID, $appType);
    }
} 
// signup api
else if($_POST['action'] == 'signup') {
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $repassword = $_POST['repassword'];
//    $gender = $_POST['gender'];
//    $birthday = $_POST['birthday'];
//    $aboutme = $_POST['aboutme'];
//    $position = $_POST['position'];
//    $address = $_POST['address'];
    signup($firstname, $lastname, $email, $password, $repassword);
    
} else if($_POST['action'] == 'logout_social') {
    logoutSocial($_POST['appID']);
}
else {
    header('Content-type: application/json;charset=UTF-8');
    header("Status", true, 400);
    echo json_encode(array("error"=>"Action is false."));
    exit();
}

/*
 * api signup
 */

function signup($firstname, $lastname, $email, $password, $repassword) {
    global $CFG, $USER, $SESSION;
    require_once("$CFG->dirroot/auth/joomdle/helpers/externallib.php");
    require_once("$CFG->dirroot/user/externallib.php");
    // check data
    $profile_pic = '';
    if(empty($firstname) || empty($email) || empty($password)) {
        $response['status'] = false;
        $response['message'] = 'Please enter required field';
        header('Content-type: application/json;charset=UTF-8');
        header("Status", true, 200);
        echo json_encode($response);
        exit();
    }
    if($password != $repassword) {
        $response['status'] = false;
        $response['message'] = 'Password does not match.';
        header('Content-type: application/json;charset=UTF-8');
        header("Status", true, 200);
        echo json_encode($response);
        exit();
    } else {
        $check_user = joomdle_helpers_external::user_exists($email);
        if($check_user) {
            $response['status'] = false;
            $response['message'] = 'Username already exits.';
            header('Content-type: application/json;charset=UTF-8');
            header("Status", true, 200);
            echo json_encode($response);
            exit;
        } else {
            $user = array();
                $user[] = 'users[0][username]='.$email; 
                $user[] = 'users[0][password]='.$password;
                $user[] = 'users[0][firstname]='.$firstname;
                $user[] = 'users[0][lastname]='.$lastname;
                $user[] = 'users[0][email]='.$email;
                $user[] = 'users[0][auth]=joomdle';
                $user[] = 'users[0][lang]=en';
                $user[] = 'users[0][country]=SG';
                $user[] = 'users[0][city]=Singapore';
                $user[] = 'users[0][mailformat]=1';
                $user[] = 'users[0][cron]=cronjob';
                $user[] = 'users[0][avatar]='.$profile_pic;
                
                $postdata = implode('&', $user);                
                if ($postdata) {

                    // [TODO] remove hardcode webservice keys * important *

                    $token = '83c95aeb4fc2f47ace618bb6ba7e106c';
//                    $token = '0d0649a09777aac062043bcd4eea9988'; // localhost
//                    $user_ids = core_user_external::create_users($users);
                    $url = $CFG->wwwroot.'/webservice/rest/server.php?wsfunction=core_user_create_users&moodlewsrestformat=json&wstoken='.$token;
                    try {
                    $ch = curl_init();
                    // set url
                    curl_setopt($ch, CURLOPT_URL, $url);

                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_VERBOSE,0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
                    // Accept certificate
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                    $user_ids = json_decode(curl_exec($ch));
//                    print_r($user_ids); die;
                    curl_close($ch);
                    } catch (moodle_exception $e) {
                        $user_ids = array();
                    }
                }
                if ($user_ids && count($user_ids) > 0) {
                    header('Content-type: application/json;charset=UTF-8');
                    header("Status", true, 200);
                    $response['status'] = true;
                    $response['message'] = 'User created.';
                    echo json_encode($response);
                    exit();
                } else {
                    header('Content-type: application/json;charset=UTF-8');
                    header("Status", true, 200);
                    echo json_encode(array("error"=>"Something went wrong. Please try again later."));
                    exit();
                }
        }
    }
    
    
}

/**
 * apilogin
 * @param type $username
 * @param type $password
 */
function apilogin($username, $password) {
    global $CFG, $USER, $SESSION;
    
    if(!isset($username) || !isset($password)) {
        $response['status'] = false;
        $response['error_message'] = 'Not enough params';

        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($response);
        die;
    }
    if(!isset($username)) {
        $response['status'] = false;
        $response['error_message'] = 'User is blank';

        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($response);
        die;
    } if($password == '') {
        $response['status'] = false;
        $response['error_message'] = 'Password is blank';

        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($response);
        die;
    } 
    else {
        $username = str_replace (' ', '%20', $username);
        
            // Login to Joomla
            $cookie_path = "/";

            $login_data = base64_encode ($username.':'.$password);
            $url = get_config ('auth/joomdle', 'joomla_url').'/index.php?option=com_joomdle&view=joomdle&task=login&data='.$login_data;

            $ch = curl_init();
            // set url
            curl_setopt($ch, CURLOPT_URL, $url);

            $file =  $CFG->tempdir . "/" .random_string(20);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_COOKIEJAR, $file);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $file);
            // Accept certificate
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $output = curl_exec($ch);
            curl_close($ch);

            if($output) {
                $test = true;
            } else {
                $test = false;
            }

            if (!file_exists($file))
                die('The temporary file isn\'t there for CURL!');

            $f = fopen ($file, 'r');
            if (!$f)
                die('The temporary file for CURL could not be opened!');
            $session_user = '';
            $session_id = 0;
            while (!feof ($f))
            {
                    $line = fgets ($f);
                   // if (($line == '\n') || ($line[0] == '#'))
                    if (($line == '\n') || ( strncmp ($line, '# ', 2) == 0))
                            continue;
                    $parts = explode ("\t", $line);

                    if (array_key_exists (5, $parts))
                    {
                            $name = $parts[5];
                            $value = trim ($parts[6]);
                            setcookie ($name, $value, 0, $cookie_path);
                            $session_user = $name;
                            $session_id = $value;
                    }
            }
            if($session_user == 'activeProfile') {
                // Login to moodle module
                $user = authenticate_user_login_api($username, $password);

                if ($user) {

                    // language setup
                    if (isguestuser($user)) {
                        // no predefined language for guests - use existing session or default site lang
                        unset($user->lang);

                    } else if (!empty($user->lang)) {
                        // unset previous session language - use user preference instead
                        unset($SESSION->lang);
                    }
                    complete_user_login ($user);
                    if (empty($user->confirmed)) { 
                        $response['status'] = false;
                        $response['message_error'] = get_string("mustconfirm");
                        header('Content-type: application/json; charset=UTF-8');
                        echo json_encode($response);
                        die;
                    }

                    /// Let's get them all set up.
                    add_to_log(SITEID, 'user', 'login', "view.php?id=$USER->id&course=".SITEID,
                               $user->id, 0, $user->id);

                    // if multiple logins not permitted, clear out any existing sessions for this user
                    if (!empty($CFG->preventmultiplelogins)) {
                        session_kill_user($user->id);
                    }

                    // sets the username cookie
                    if (!empty($CFG->nolastloggedin)) {
                        // do not store last logged in user in cookie
                        // auth plugins can temporarily override this from loginpage_hook()
                        // do not save $CFG->nolastloggedin in database!

                    } else if (empty($CFG->rememberusername) or ($CFG->rememberusername == 2 and empty($username))) {
                        // no permanent cookies, delete old one if exists
                        set_moodle_cookie('');

                    } else {
                        set_moodle_cookie($USER->username);
                    }
                    
                    $url_user = get_config ('auth/joomdle', 'joomla_url').'/index.php?option=com_community&view=groups&type=api&resource=groups&action=userid&username='.$username;
                    $ch_user = curl_init();
                    // Disable SSL verification
                    curl_setopt($ch_user, CURLOPT_SSL_VERIFYPEER, false);
                    // Will return the response, if false it print the response
                    curl_setopt($ch_user, CURLOPT_RETURNTRANSFER, true);
                    // Set the url
                    curl_setopt($ch_user, CURLOPT_URL,$url_user);
                    // Execute
                    $result=curl_exec($ch_user);
                    // Closing
                    curl_close($ch_user);
                    $data_user = (json_decode($result));

                    $auth = new auth_plugin_joomdle();
                    $user_inprogress = $auth->user_course_inprogress($username);

                    $response['status'] = true;
                    $response['user'] = $data_user;
                    $response['course_inprogress'] = $user_inprogress;
                    header('Content-type: application/json; charset=UTF-8');
                    echo json_encode($response);
                } else {
                    $response['status'] = false;
                    $response['error_message'] = 'Invalid username or password';
                    header('Content-type: application/json; charset=UTF-8');
                    echo json_encode($response);
                    exit;
                }
            } else {
                $response['status'] = false;
                $response['error_message'] = 'Invalid username or password';
                header('Content-type: application/json; charset=UTF-8');
                echo json_encode($response);
                exit;
            }
            unlink ($file);
            // End login joomla
    }
}
function loginSocial($apptoken, $appid, $apptype) {
    // The first check $appToken valid
    global $CFG;
    //request by curl an access token and refresh token
    require_once($CFG->libdir . '/filelib.php');
    $curl = new curl();
    
    $params = array();
    switch ($apptype) {
        case 'FB':
            $params['access_token'] = $apptoken;
            $postreturnvalues = $curl->get('https://graph.facebook.com/me', $params);
            $facebookuser = json_decode($postreturnvalues);
            if(empty($facebookuser)) {
                $response['status'] = false;
                $response['error_message'] = $postreturnvalues;
                header('Content-type: application/json; charset=UTF-8');
                echo json_encode($response);
                exit;
            }
            $useremail = $facebookuser->email;
            $fullname = $facebookuser->first_name .' '.$facebookuser->last_name;
//            $url="https://graph.facebook.com/me?fields=name,email,picture&access_token=".$apptoken;
            break;
        case 'GG':
            $params['access_token'] = $apptoken;
            $params['alt'] = 'json';
            $postreturnvalues = $curl->get('https://www.googleapis.com/plus/v1/people/me', $params);
            $postreturnvalues = json_decode($postreturnvalues);
            if($postreturnvalues->error) {
                $response['status'] = false;
                $response['error_message'] = 'Invalid Credentials.';
                header('Content-type: application/json; charset=UTF-8');
                echo json_encode($response);
                exit;
            }
            foreach($postreturnvalues->emails as $googleemail) {
                    if($googleemail->type == "account") {
                        $useremail = $googleemail->value;
                    }
            }
            $useremail = $postreturnvalues->emails[0]->value;
            $id = $postreturnvalues->id;
            $fullname = 'google_account';
            if(isset($postreturnvalues->name)) {
                $fullname = $postreturnvalues->name->familyName .' '. $postreturnvalues->name->givenName;
            }
//            $url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?acces_token='.$apptoken;
            break;
        case 'IN':
            $params['format'] = 'json';
            $params['oauth2_access_token'] = $apptoken;
            $postreturnvalues = $curl->get('https://api.linkedin.com/v1/people/~:(first-name,last-name,email-address,location:(name,country:(code)))', $params);
            $linkedinuser = json_decode($postreturnvalues);
            if($linkedinuser->status == 401) {
                $response['status'] = false;
                $response['error_message'] = 'Unable to verify access token';
                header('Content-type: application/json; charset=UTF-8');
                echo json_encode($response);
                exit;
            }
            $useremail = $linkedinuser->emailAddress;
            $id = $linkedinuser->id;
            $fullname = $linkedinuser->firstName. ' '. $linkedinuser->lastName;
            break;
        default:
            $response['status'] = false;
            $response['error_message'] = 'Invalid Social Plugin';
            header('Content-type: application/json; charset=UTF-8');
            echo json_encode($response);
            exit; 
            break;
    }
    
    /*
    $ch = curl_init();
    $request_headers = array();
    $request_headers[] = 'Content-Type: application/json';
    $request_headers[] = 'Connection: Keep-Alive';
    $request_headers[] = 'x-li-format: json';
    
    curl_setopt($ch, CURLOPT_URL,$url );
    curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_HTTPAUTH,CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
    $result = curl_exec($ch);
    curl_close($ch);
    $social_profile = json_decode( $result, true);
    */
    if( isset($postreturnvalues['error'] ) )
    {
        $response['status'] = false;
        $response['error_message'] = 'Invalid token';
        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($response);
        exit;
    }
    elseif( isset($id) && $id != $appid )//Check if this auth token has the same ID sent.
    {
        $response['status'] = false;
        $response['error_message'] = 'Invalid ID Sent';
        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($response);
        exit; 
    } else {
        // implement create user and login
        apilogin_with_social($appid, $appid, $fullname, $useremail, '');
    }
}
function apilogin_with_social($username, $password, $fullname, $email, $profile_pic = '') {
    
    global $CFG, $USER, $SESSION;
    require_once("$CFG->dirroot/auth/joomdle/helpers/externallib.php");
    require_once("$CFG->dirroot/user/externallib.php");
    
    if(!isset($username) || !isset($password) || !isset($fullname)) {
        $response['status'] = false;
        $response['error_message'] = 'Not enough params';

        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($response);
        die;
    }
    if(!isset($username)) {
        $response['status'] = false;
        $response['error_message'] = 'User is blank';

        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($response);
        die;
    } else if($password == '') {
        $response['status'] = false;
        $response['error_message'] = 'Password is blank';

        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($response);
        die;
    } else if ($fullname == '') {
        $response['status'] = false;
        $response['error_message'] = 'Fullname is blank';

        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($response);
        die;
    } else {
        // Check user
        $check_user = joomdle_helpers_external::user_exists($username);
        
        if (!$check_user) {      // User don't exists 
            // Create new user
            if (!empty($fullname)) {
                if(empty($email)) {
                    $email = $fullname.'user@parenthexis.asia';
                }
                $users = array();
                $user[] = 'users[0][username]='.$username; 
                $user[] = 'users[0][password]='.$password;
                $user[] = 'users[0][firstname]='.$fullname;
                $user[] = 'users[0][lastname]='.$fullname;
                $user[] = 'users[0][email]='.$email;
                $user[] = 'users[0][auth]=joomdle';
                $user[] = 'users[0][lang]=en';
                $user[] = 'users[0][country]=SG';
                $user[] = 'users[0][city]=Singapore';
                $user[] = 'users[0][mailformat]=1';
                $user[] = 'users[0][cron]=cronjob';
                $user[] = 'users[0][avatar]='.$profile_pic;
                
                $postdata = implode('&', $user);                
                if ($postdata) {
                    $token = '83c95aeb4fc2f47ace618bb6ba7e106c';
//                    $token = '0d0649a09777aac062043bcd4eea9988'; // localhost
//                    $user_ids = core_user_external::create_users($users);
                    $url = $CFG->wwwroot.'/webservice/rest/server.php?wsfunction=core_user_create_users&moodlewsrestformat=json&wstoken='.$token;
                    try {
                    $ch = curl_init();
                    // set url
                    curl_setopt($ch, CURLOPT_URL, $url);

                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_VERBOSE,0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
                    // Accept certificate
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                    $user_ids = json_decode(curl_exec($ch));
//                    print_r($user_ids); die;
                    curl_close($ch);
                    } catch (moodle_exception $e) {
                        $user_ids = array();
                    }
                }
                if ($user_ids && count($user_ids) > 0) {
                    if (isset($user_ids[0]->id) && $user_ids[0]->id > 0) {       
                        apilogin($username, $password);
                    } else {
                        header('Content-type: application/json;charset=UTF-8');
                        header("Status", true, 500);
                        echo json_encode(array("error"=>"Something went wrong. Please try again later."));
                        exit();
                    }
                } else {
                    header('Content-type: application/json;charset=UTF-8');
                    header("Status", true, 500);
                    echo json_encode(array("error"=>"Something went wrong. Please try again later."));
                    exit();
                }
            } else {
                $response['status'] = false;
                $response['error_message'] = 'Profile info is blank';

                header('Content-type: application/json; charset=UTF-8');
                echo json_encode($response);
                die;
            }
        } else {
            apilogin($username, $password);
        }
    }
}

function apilogout($username, $password) {
    global $CFG, $USER;

    if(!isset($username) || !isset($password)) {
        $response['status'] = false;
        $response['error_message'] = 'Not enough params';

        header('Content-type: application/json; charset=UTF-8');
        header("Status", true, 400);
        echo json_encode($response);
        die;
    }
    if(!isset($username) || $username == '') {
        $response['status'] = false;
        $response['error_message'] = 'Username is blank';

        header('Content-type: application/json; charset=UTF-8');
        header("Status", true, 400);
        echo json_encode($response);
        die;
    }  
    else {
        $username = str_replace (' ', '%20', $username);
        
        $user = authenticate_user_login_api($username, $password);
        $USER = $user;
//        var_dump($user); exit();
    
//        $sesskey = $data->auth_key;

        // can be overridden by auth plugins
        $redirect = $CFG->wwwroot.'/';

        if (!isloggedin()) {
            // no confirmation, user has already logged out
            require_logout();
//            redirect($redirect);

        } 
//        else if (!confirm_sesskey($sesskey)) {
//            header('Content-type: application/json;charset=UTF-8');
//            header("Status", true, 401);
//            echo json_encode(array("error"=>"Session key not found."));
//            exit();
//        }

        $authsequence = get_enabled_auth_plugins(); // auths, in sequence
        foreach($authsequence as $authname) {
            $authplugin = get_auth_plugin($authname);
            $authplugin->logoutpage_hook();
        }

        require_logout();
        session_gc(); 
        session_kill_user($user->id);

        header('Content-type: application/json;charset=UTF-8');
        echo json_encode(array("status"=>true));
        exit();
    }
}

function logoutSocial($appID) {
    global $CFG, $USER;

    if(!isset($appID) || $appID == '') {
        $response['status'] = false;
        $response['error_message'] = 'App ID can not be empty.';

        header('Content-type: application/json; charset=UTF-8');
        header("Status", true, 400);
        echo json_encode($response);
        die;
    } else {
        $user = authenticate_user_login_api($appID, $appID);
        $USER = $user;

        if (!isloggedin()) {
            // no confirmation, user has already logged out
            require_logout();

        } 

        $authsequence = get_enabled_auth_plugins(); // auths, in sequence
        foreach($authsequence as $authname) {
            $authplugin = get_auth_plugin($authname);
            $authplugin->logoutpage_hook();
        }

        require_logout();
        session_gc(); 
        session_kill_user($user->id);

        header('Content-type: application/json;charset=UTF-8');
        echo json_encode(array("status"=>true));
        exit();
    }
}
 

?>
