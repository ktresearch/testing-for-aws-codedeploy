<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require('../config.php');
$username = $_POST['username'];
$password = $_POST['password'];
$type = $_POST['type'];

if ($type == '') {
    $type = $_GET['type'];
}

$user = authenticate_user_login_api($username, $password);

if ($user) {
    if(!isset($_SESSION)){
        session_start();
    }
    if ($type == 'appview') {
        $_SESSION['device_view_type'] = 'appview';
        $url = urldecode($_POST['url']) . '&type=appview';
        // language setup
        if (isguestuser($user)) {
            // no predefined language for guests - use existing session or default site lang
            unset($user->lang);
        } else if (!empty($user->lang)) {
            // unset previous session language - use user preference instead
            unset($SESSION->lang);
        }
        complete_user_login($user);

        if (empty($user->confirmed)) {
            $response['status'] = false;
            $response['message_error'] = get_string("mustconfirm");
            header('Content-type: application/json; charset=UTF-8');
            echo json_encode($response);
            die;
        }

        /// Let's get them all set up.
        add_to_log(SITEID, 'user', 'login', "view.php?id=$USER->id&course=" . SITEID, $user->id, 0, $user->id);

        // if multiple logins not permitted, clear out any existing sessions for this user
        if (!empty($CFG->preventmultiplelogins)) {
            session_kill_user($user->id);
        }

        // sets the username cookie
        if (!empty($CFG->nolastloggedin)) {
            // do not store last logged in user in cookie
            // auth plugins can temporarily override this from loginpage_hook()
            // do not save $CFG->nolastloggedin in database!
        } else if (empty($CFG->rememberusername) or ( $CFG->rememberusername == 2 and empty($username))) {
            // no permanent cookies, delete old one if exists
            set_moodle_cookie('');
        } else {
            set_moodle_cookie($USER->username);
        }
        
        if ( base64_encode(base64_decode($url, true)) === $url){
            $wantsurl = $url;
        } else {
            $wantsurl = base64_encode($url);
        }
        $login_data = base64_encode($username . ':' . $password);
        $redirect_url = get_config ('auth/joomdle', 'joomla_url').'/index.php?option=com_joomdle&view=joomdle&task=login&data='.$login_data.'&wantsurl='. $wantsurl.'&type=appview';
        redirect($redirect_url);
    } else {
        $url = $_POST['url'];
        redirect($url);
    }
}
?>
