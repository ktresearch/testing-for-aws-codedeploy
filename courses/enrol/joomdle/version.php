<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version  = 2010073102;
$plugin->requires = 2010090501;   // Requires this Moodle version
$plugin->maturity = MATURITY_STABLE;
$plugin->release = "1.0";

