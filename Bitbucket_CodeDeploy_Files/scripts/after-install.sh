#!/bin/bash

if [ "$DEPLOYMENT_GROUP_NAME" == "BLNTest_DO_NOT_USE" ]
then

if [ ! -f /data/web/www/blnpatchlogfiles/prodserver/"blnKydon-$(date +"%d-%m-%Y").txt" ]; then
    touch /data/web/www/blnpatchlogfiles/prodserver/"blnKydon-$(date +"%d-%m-%Y").txt"
	echo "BLNKydon Server" >> /data/web/www/blnpatchlogfiles/prodserver/"blnKydon-$(date +"%d-%m-%Y").txt"
fi
	
	sed -i '$a\\'$(date +%d-%m-%Y,%H:%M:%S) /data/web/www/blnpatchlogfiles/prodserver/"blnKydon-$(date +"%d-%m-%Y").txt"
	rsync --dry-run -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-prod-blnkydon.txt' /data/web/www/blnbackup/ /data/web/www/blnkydon/ >> /data/web/www/blnpatchlogfiles/prodserver/"blnKydon-$(date +"%d-%m-%Y").txt"
	sed -i '$aEND\n' /data/web/www/blnpatchlogfiles/prodserver/"blnKydon-$(date +"%d-%m-%Y").txt"
	
	rsync -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-prod-blnkydon.txt' /data/web/www/blnbackup/ /data/web/www/blnkydon/ 

fi


if [ "$DEPLOYMENT_GROUP_NAME" == "blndevelopment" ]
then

if [ ! -f /data/web/www/blnpatchlogfiles/devserver/"blndev-$(date +"%d-%m-%Y").txt" ]; then
    touch /data/web/www/blnpatchlogfiles/devserver/"blndev-$(date +"%d-%m-%Y").txt"
	echo "BLNTest Server" >> /data/web/www/blnpatchlogfiles/devserver/"blndev-$(date +"%d-%m-%Y").txt"
fi

	sed -i '$a\\'$(date +%d-%m-%Y,%H:%M:%S) /data/web/www/blnpatchlogfiles/devserver/"blndev-$(date +"%d-%m-%Y").txt"
	rsync --dry-run -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-dev-blntest.txt' /data/web/www/blnbackup/ /data/web/www/blntest/ >> /data/web/www/blnpatchlogfiles/devserver/"blndev-$(date +"%d-%m-%Y").txt"
	sed -i '$aEND\n' /data/web/www/blnpatchlogfiles/devserver/"blndev-$(date +"%d-%m-%Y").txt"
	rsync -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-dev-blntest.txt' /data/web/www/blnbackup/ /data/web/www/blntest/
fi

if [ "$DEPLOYMENT_GROUP_NAME" == "blnstaging" ]
then

if [ ! -f /data/web/www/blnpatchlogfiles/stagingserver/"blnstaging-$(date +"%d-%m-%Y").txt" ]; then
    touch /data/web/www/blnpatchlogfiles/stagingserver/"blnstaging-$(date +"%d-%m-%Y").txt"
	echo "BLNSIT Server" >> /data/web/www/blnpatchlogfiles/stagingserver/"blnstaging-$(date +"%d-%m-%Y").txt"
fi

	sed -i '$a\\'$(date +%d-%m-%Y,%H:%M:%S) /data/web/www/blnpatchlogfiles/stagingserver/"blnstaging-$(date +"%d-%m-%Y").txt"
	rsync --dry-run -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-staging-blnsit.txt' /data/web/www/blnbackup/ /data/web/www/blnsit/ >> /data/web/www/blnpatchlogfiles/stagingserver/"blnstaging-$(date +"%d-%m-%Y").txt"
	sed -i '$aEND\n' /data/web/www/blnpatchlogfiles/stagingserver/"blnstaging-$(date +"%d-%m-%Y").txt"
	rsync -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-staging-blnsit.txt' /data/web/www/blnbackup/ /data/web/www/blnsit/
fi

if [ "$DEPLOYMENT_GROUP_NAME" == "blnUAT" ]
then

if [ ! -f /data/web/www/blnpatchlogfiles/prodserver/"blnKydon-$(date +"%d-%m-%Y").txt" ]; then
    touch /data/web/www/blnpatchlogfiles/prodserver/"blnKydon-$(date +"%d-%m-%Y").txt"
	echo "BLNKydon Server" >> /data/web/www/blnpatchlogfiles/prodserver/"blnKydon-$(date +"%d-%m-%Y").txt"
fi
	
	sed -i '$a\\'$(date +%d-%m-%Y,%H:%M:%S) /data/web/www/blnpatchlogfiles/prodserver/"blnKydon-$(date +"%d-%m-%Y").txt"
	rsync --dry-run -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-prod-blnkydon.txt' /data/web/www/blnbackup/ /data/web/www/blnkydon/ >> /data/web/www/blnpatchlogfiles/prodserver/"blnKydon-$(date +"%d-%m-%Y").txt"
	sed -i '$aEND\n' /data/web/www/blnpatchlogfiles/prodserver/"blnKydon-$(date +"%d-%m-%Y").txt"
	
	rsync -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-prod-blnkydon.txt' /data/web/www/blnbackup/ /data/web/www/blnkydon/ 

fi

# if [ "$DEPLOYMENT_GROUP_NAME" == "blnproduction" ]
# then

# if [ ! -f /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt" ]; then
    # touch /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
# fi

	# echo "BLNBlank Server" >> /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# sed -i '$a\\'$(date +%d-%m-%Y,%H:%M:%S) /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# rsync --dry-run -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-prod-blnblank.txt' /data/web/www/blnbackup/ /data/web/www/blnblank/ >> /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# sed -i '$aEND\n' /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	
	# rsync -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-prod-blnblank.txt' /data/web/www/blnbackup/ /data/web/www/blnblank/
	
	
	# echo "BLNEpm Server" >> /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# sed -i '$a\\'$(date +%d-%m-%Y,%H:%M:%S) /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# rsync --dry-run -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-prod-blnepm.txt' /data/web/www/blnbackup/ /data/web/www/blnepm/ >> /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# sed -i '$aEND\n' /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	
	# rsync -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-prod-blnepm.txt' /data/web/www/blnbackup/ /data/web/www/blnepm/
	
	
	# echo "BLNSandbox Server" >> /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# sed -i '$a\\'$(date +%d-%m-%Y,%H:%M:%S) /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# rsync --dry-run -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-prod-blnsandbox.txt' /data/web/www/blnbackup/ /data/web/www/blnsandbox/ >> /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# sed -i '$aEND\n' /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	
	# rsync -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-prod-blnsandbox.txt' /data/web/www/blnbackup/ /data/web/www/blnsandbox/

	
	# echo "BLNSsg Server" >> /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# sed -i '$a\\'$(date +%d-%m-%Y,%H:%M:%S) /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# rsync --dry-run -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-prod-blnssg.txt' /data/web/www/blnbackup/ /data/web/www/blnssg/ >> /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# sed -i '$aEND\n' /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	
	# rsync -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-prod-blnssg.txt' /data/web/www/blnbackup/ /data/web/www/blnssg/

	
	# echo "BLNScala Server" >> /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# sed -i '$a\\'$(date +%d-%m-%Y,%H:%M:%S) /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# rsync --dry-run -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-prod-scala.txt' /data/web/www/blnbackup/ /data/web/www/scala/ >> /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	# sed -i '$aEND\n' /data/web/www/blnpatchlogfiles/prodserver/"blnprod-$(date +"%d-%m-%Y").txt"
	
	# rsync -clvrz -o -g --exclude=configuration.php --exclude-from '/data/web/www/blnbackup/exclude-list/exclude-list-prod-scala.txt' /data/web/www/blnbackup/ /data/web/www/scala/

# fi

echo "The AfterInstall deployment lifecycle event successfully completed." > after-install.txt