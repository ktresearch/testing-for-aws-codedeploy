<?php
/**
* @package RSForm! Pro
* @copyright (C) 2007-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/

defined('_JEXEC') or die('Restricted access');
?>
<?php if ($this->params->get('show_page_heading', 0)) { ?>
	<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
<?php } ?>
<?php echo RSFormProHelper::displayForm($this->formId); ?>
<script>
    jQuery( document ).ready(function() {
        jQuery(".termText a").attr({"href": "/components/com_community/assets/source/document/Parenthexis Partner Agreement May2018.pdf", "target" : "_blank"})
    });
</script>
