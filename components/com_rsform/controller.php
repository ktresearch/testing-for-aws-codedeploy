<?php
/**
* @package RSForm! Pro
* @copyright (C) 2007-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/


defined('_JEXEC') or die('Restricted access');

//require_once(JPATH_SITE.DS.'components'.DS.'com_joomdle'.DS.'helpers'.DS.'content.php');
//require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/content.php');

defined('JPATH_BASE') or die();
require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');


class RSFormController extends JControllerLegacy
{	
	public function captcha() {
		require_once JPATH_SITE.'/components/com_rsform/helpers/captcha.php';
		
		$componentId 	= JFactory::getApplication()->input->getInt('componentId');
		$captcha 		= new RSFormProCaptcha($componentId);

		JFactory::getSession()->set('com_rsform.captcha.'.$componentId, $captcha->getCaptcha());
		JFactory::getApplication()->close();
	}
	
	public function plugin() {
		JFactory::getApplication()->triggerEvent('rsfp_f_onSwitchTasks');
	}
	
	/* deprecated */
	public function showForm() {}
	
	public function submissionsViewFile() {
		$db 	= JFactory::getDbo();
		$secret = JFactory::getConfig()->get('secret');
		$hash 	= JFactory::getApplication()->input->getCmd('hash');
		
		// Load language file
		JFactory::getLanguage()->load('com_rsform', JPATH_ADMINISTRATOR);
		
		if (strlen($hash) != 32) {
			JError::raiseError(500, JText::_('RSFP_VIEW_FILE_NOT_FOUND'));
		}
		
		$db->setQuery("SELECT * FROM #__rsform_submission_values WHERE MD5(CONCAT(SubmissionId,'".$db->escape($secret)."',FieldName)) = '".$hash."'");
		if ($result = $db->loadObject()) {
			// Check if it's an upload field
			$db->setQuery("SELECT c.ComponentTypeId FROM #__rsform_properties p LEFT JOIN #__rsform_components c ON (p.ComponentId=c.ComponentId) WHERE p.PropertyName='NAME' AND p.PropertyValue='".$db->escape($result->FieldName)."'");
			$type = $db->loadResult();
			if ($type != 9) {
				JError::raiseError(500, JText::_('RSFP_VIEW_FILE_NOT_UPLOAD'));
			}
			
			if (file_exists($result->FieldValue)) {
				RSFormProHelper::readFile($result->FieldValue);
			}
		} else {
			JError::raiseError(500, JText::_('RSFP_VIEW_FILE_NOT_FOUND'));
		}
	}
	
	public function ajaxValidate()
	{
		$db = JFactory::getDbo();
		$form = JRequest::getVar('form');
		$formId = (int) @$form['formId'];
		
		$db->setQuery("SELECT ComponentId, ComponentTypeId FROM #__rsform_components WHERE `FormId`='".$formId."' AND `Published`='1' ORDER BY `Order`");
		$components = $db->loadObjectList();
		
		$page = JRequest::getInt('page');
		if ($page)
		{
			$current_page = 1;
			foreach ($components as $i => $component)
			{
				if ($current_page != $page)
					unset($components[$i]);
				if ($component->ComponentTypeId == 41)
					$current_page++;
			}
		}
		
		$removeUploads   = array();
		$formComponents  = array();
		foreach ($components as $component)
		{
			$formComponents[] = $component->ComponentId;
			if ($component->ComponentTypeId == 9)
				$removeUploads[] = $component->ComponentId;
		}
		
		echo implode(',', $formComponents);
		
		echo "\n";
		
		$invalid = RSFormProHelper::validateForm($formId);
		
		//Trigger Event - onBeforeFormValidation
		$mainframe = JFactory::getApplication();
		$post = JRequest::get('post', JREQUEST_ALLOWRAW);
                
		$mainframe->triggerEvent('rsfp_f_onBeforeFormValidation', array(array('invalid'=>&$invalid, 'formId' => $formId, 'post' => &$post)));
		
		if (count($invalid))
		{
			foreach ($invalid as $i => $componentId)
				if (in_array($componentId, $removeUploads))
					unset($invalid[$i]);
                        if($componentId == 29 && $invalid[$i] == 'Name already eixts')
                            echo implode(',', $invalid);
                        else
			$invalidComponents = array_intersect($formComponents, $invalid);
			
			echo implode(',', $invalidComponents);
		}
		
		if (isset($invalidComponents))
		{
			echo "\n";
			
			$pages = RSFormProHelper::componentExists($formId, 41);
			$pages = count($pages);
			
			if ($pages && !$page)
			{
				$first = reset($invalidComponents);
				$current_page = 1;
				foreach ($components as $i => $component)
				{
					if ($component->ComponentId == $first)
						break;
					if ($component->ComponentTypeId == 41)
						$current_page++;
				}
				echo $current_page;
				
				echo "\n";
				
				echo $pages;
			}
		}
		
		jexit();
	}

	function getAdminEmailFrom(){
		$lpId = 5;
		$db = JFactory::getDbo();
		$db->setQuery("SELECT AdminEmailFrom FROM `#__rsform_forms` WHERE `FormId` = '".(int) $lpId."'");
			$AdminEmail = $db->loadObjectList();
			$db->execute();
			$AdminEmailFrom = $db->escape($AdminEmail[0]->AdminEmailFrom);
			return $AdminEmailFrom;
	}


	function emailsuccess($recipient, $circleid){
		//TODO: need to pickup the config admin email
		$from = $this->getAdminEmailFrom();
		$fromname = 'Parenthexis';
		$subject = JText::_('RSFP_EMAIL_SUBJECT_SUCCESS');
		$text = 'Your learning provider application has been approved! ' . '<a href="' . JURI::base() . 'index.php?option=com_community&view=groups&task=viewlpabout&groupid=' . $circleid .'">'. 'Click here to view your cicle' . '</a>';
		RSFormProHelper::sendMail($from, $fromname, $recipient, $subject, $text, 'html', null, null, '', '');
	}
	
	function emailreject($recipient){
		//TODO: need to pickup the config admin email
		$from = $this->getAdminEmailFrom();
		$fromname = 'Parenthexis';
		$subject = JText::_('RSFP_SUBMISSION_REJECTED_CIRCLE');
		$text = 'Your learning provider application has been rejected!';
		RSFormProHelper::sendMail($from, $fromname, $recipient, $subject, $text, 'html', null, null, '', '');
	}

	function isduplicate(){
		$db 	= JFactory::getDbo();
		$app	= JFactory::getApplication();
		$hash 	= $app->input->getCmd('hash');
		$db->setQuery("SELECT `LearningProvider` FROM `#__rsform_submissions` WHERE MD5(CONCAT(`SubmissionId`,`FormId`,`DateSubmitted`)) = '".$db->escape($hash)."' ");
		$confirmation = $db->loadResult();
		return $confirmation;
	} 

	
	public function confirm() {

		require_once(JPATH_SITE.'/components/com_hikashop/helpers/lpapi.php');

		$db 	= JFactory::getDbo();
		$app	= JFactory::getApplication();
		$hash 	= $app->input->getCmd('hash');

		if (strlen($hash) == 32) {
			$db->setQuery("SELECT `SubmissionId` FROM `#__rsform_submissions` WHERE MD5(CONCAT(`SubmissionId`,`FormId`,`DateSubmitted`)) = '".$db->escape($hash)."' ");

			if ($SubmissionId = $db->loadResult()) {
				// Check if group is already created to avoid duplicates
				if(!$this->isduplicate()){
					$db->setQuery("UPDATE `#__rsform_submissions` SET `confirmed` = 1, `LearningProvider` = 1 WHERE `SubmissionId` = '".(int) $SubmissionId."'");
					$db->execute();

					// Get submissions
					$db->setQuery("SELECT * FROM `#__rsform_submissions` WHERE `SubmissionId` = '".(int) $SubmissionId."'");
					$submissions = $db->loadObjectList();
					$db->execute();
					$username = $submissions[0]->Username;
					
					// Get submissions values
					$db->setQuery("SELECT * FROM `#__rsform_submission_values` WHERE `SubmissionId` = '".(int) $SubmissionId."' ORDER BY `FieldName`");
					$submissionvalues = $db->loadObjectList();
					$db->execute();

					// Insert into Circle as Learning Provider
					$circlecategoryid = 7;
					$usersubmitterid = $submissions[0]->UserId;
					$circlename = $db->escape($submissionvalues[6]->FieldValue);
					$circledescription = $db->escape($submissionvalues[0]->FieldValue);
					$emailaddress = $db->escape($submissionvalues[3]->FieldValue);
					$keywords = $db->escape($submissionvalues[5]->FieldValue);
					$organisation = $db->escape($submissionvalues[7]->FieldValue);
					$billingaddress = $db->escape($submissionvalues[1]->FieldValue);
					$postalcode = $db->escape($submissionvalues[8]->FieldValue);
                                        $date = date("Y-m-d H:i:s");

					// TOCHECK: KV_API_HERE - Create Categories and assign user manage of categories
					
					$categories = array(array("name" => (string)$circlename, "parent" => (int)0, "idnumber" => "", "description" => $circledescription, "descriptionformat" => (int)1, "username" => $username));
					$response = JoomdleHelperContent::call_method ('create_categories', $categories);
				
					$moodlecategoryid = $response['categories'][0]['id'];

					// Add new Learning Provider group

					$db->setQuery("INSERT INTO `#__community_groups` (`published`, `ownerid`, `categoryid`,`name`,`description`,`email`,`keyword`,`organisation`,`billingaddress`,`postalcode`,`approvals`,`unlisted`,`created`, `moodlecategoryid`) VALUES (1, $usersubmitterid, $circlecategoryid, '$circlename', '$circledescription','$emailaddress','$keywords','$organisation','$billingaddress','$postalcode', 0, 0,'$date', $moodlecategoryid)");
					$db->execute();

					$circleid = $db->insertid();

					// TODO: Insert the ID to group members
					
					$db->setQuery("INSERT INTO `#__community_groups_members` (`groupid`, `memberid`, `approved`, `permissions`) VALUES ($circleid, $usersubmitterid, 1,1)");
					$db->execute();	

					
					// TODO: NYI Insert to Hikashop  	
					$responseHika = LpApiController::createcategory($circlename,$circleid);

					
					
					// TODO:

					// Get the circle url Send Email to user about the approval
					
					$this->emailsuccess($emailaddress, $circleid);
					
					$app->triggerEvent('rsfp_f_onSubmissionConfirmation', array(array('SubmissionId' => $SubmissionId, 'hash' => $hash)));
					
					JError::raiseNotice(200, JText::_('RSFP_SUBMISSION_CONFIRMED_CIRCLE'));
				}else{
					JError::raiseWarning(500, JText::_('RSFP_SUBMISSION_ALREADY_APPROVED'));
				}

			}
		} else {
			JError::raiseWarning(500, JText::_('RSFP_SUBMISSION_CONFIRMED_ERROR'));
		}
	}

	public function reject(){

		$db 	= JFactory::getDbo();
		$app	= JFactory::getApplication();
		$hash 	= $app->input->getCmd('hash');

		if (strlen($hash) == 32) {
			$db->setQuery("SELECT `SubmissionId` FROM `#__rsform_submissions` WHERE MD5(CONCAT(`SubmissionId`,`FormId`,`DateSubmitted`)) = '".$db->escape($hash)."' ");
			if ($SubmissionId = $db->loadResult()) {
				$db->setQuery("UPDATE `#__rsform_submissions` SET `confirmed` = 0 WHERE `SubmissionId` = '".(int) $SubmissionId."'");
				$db->execute();
			}

			// Get submissions
			$db->setQuery("SELECT * FROM `#__rsform_submissions` WHERE `SubmissionId` = '".(int) $SubmissionId."'");
			$submissions = $db->loadObjectList();
			$db->execute();

			// Get submissions values
			$db->setQuery("SELECT * FROM `#__rsform_submission_values` WHERE `SubmissionId` = '".(int) $SubmissionId."' ORDER BY `FieldName`");
			$submissionvalues = $db->loadObjectList();
			$db->execute();

			$emailaddress = $db->escape($submissionvalues[1]->FieldValue);

			//TODO: Send rejection email
			$this->emailreject($emailaddress);

			JError::raiseNotice(200, JText::_('RSFP_SUBMISSION_REJECTED_CIRCLE'));
		}else{
			JError::raiseWarning(500, JText::_('RSFP_SUBMISSION_CONFIRMED_ERROR'));
		}	
	}
}