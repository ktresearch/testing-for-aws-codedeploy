<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class CGroupHelper
{
	static public function getMediaPermission( $groupId )
	{
		// load COwnerHelper::isCommunityAdmin()
		//CFactory::load( 'helpers' , 'owner' );
		$my	= CFactory::getUser();

		$isSuperAdmin		= COwnerHelper::isCommunityAdmin();
		$isAdmin			= false;
		$isMember			= false;
		$waitingApproval	= false;

		// Load the group table.
		$groupModel	= CFactory::getModel( 'groups' );
		$group		= JTable::getInstance( 'Group' , 'CTable' );
		$group->load( $groupId );
		$params		= $group->getParams();

		if(!$isSuperAdmin)
		{
			$isAdmin	= $groupModel->isAdmin( $my->id , $group->id );
			$isMember	= $group->isMember( $my->id );

			//check if awaiting group's approval
			if( $groupModel->isWaitingAuthorization( $my->id , $group->id ) )
			{
				$waitingApproval	= true;
			}
		}

		$permission = new stdClass();
		$permission->isMember 			= $isMember;
		$permission->waitingApproval 	= $waitingApproval;
		$permission->isAdmin 			= $isAdmin;
		$permission->isSuperAdmin 		= $isSuperAdmin;
		$permission->params 			= $params;
		$permission->privateGroup		= $group->approvals;

		return $permission;
	}

	static public function allowViewMedia( $groupId )
	{
		if(empty($groupId))
		{
			return false;
		}

		//get permission
		$permission = CGroupHelper::getMediaPermission($groupId);

		if($permission->privateGroup)
		{
			if($permission->isSuperAdmin || ($permission->isMember && !$permission->waitingApproval) )
			{
				$allowViewVideos = true;
			}
			else
			{
				$allowViewVideos = false;
			}
		}
		else
		{
			$allowViewVideos = true;
		}

		return $allowViewVideos;
	}

	static public function allowManageVideo( $groupId )
	{
		$allowManageVideos = false;

		//get permission
		$permission = CGroupHelper::getMediaPermission($groupId);

		$videopermission	= $permission->params->get('videopermission' , GROUP_VIDEO_PERMISSION_ADMINS );

		//checking for backward compatibility
                if($videopermission == GROUP_VIDEO_PERMISSION_ALL)
                {
                    $videopermission = GROUP_VIDEO_PERMISSION_MEMBERS;
                }

		if($videopermission == GROUP_VIDEO_PERMISSION_DISABLE)
		{
			$allowManageVideos = false;
		}
		else if( ($videopermission == GROUP_VIDEO_PERMISSION_MEMBERS && $permission->isMember && !$permission->waitingApproval) || $permission->isAdmin || $permission->isSuperAdmin )
		{
			$allowManageVideos = true;
		}

		return $allowManageVideos;
	}

	static public function allowManagePhoto($groupId)
	{
		$allowManagePhotos = false;

		//get permission
		$permission = CGroupHelper::getMediaPermission($groupId);

		$photopermission	= $permission->params->get('photopermission' , GROUP_PHOTO_PERMISSION_ADMINS );

                //checking for backward compatibility
                if($photopermission == GROUP_PHOTO_PERMISSION_ALL)
                {
                    $photopermission = GROUP_PHOTO_PERMISSION_MEMBERS;
                }

		if($photopermission == GROUP_PHOTO_PERMISSION_DISABLE)
		{
			$allowManagePhotos = false;
		}

		else if( ($photopermission == GROUP_PHOTO_PERMISSION_MEMBERS && $permission->isMember && !$permission->waitingApproval) || $permission->isAdmin || $permission->isSuperAdmin )
		{
			$allowManagePhotos = true;
		}

		return $allowManagePhotos;
	}
	static public function allowManageEvent( $userId , $groupId , $eventId )
	{
		//CFactory::load( 'helpers' , 'owner' );
		$user		= CFactory::getUser( $userId );
		$group		= JTable::getInstance( 'Group' , 'CTable' );
		$event		= JTable::getInstance( 'Event' , 'CTable' );

		$event->load( $eventId );
		$group->load( $groupId );

		if( COwnerHelper::isCommunityAdmin() || $group->isAdmin( $user->id ) || $event->isCreator( $user->id ) )
		{
			return true;
		}
		return false;
	}

	static public function allowCreateEvent( $userId , $groupId )
	{
		//CFactory::load( 'helpers' , 'owner' );
		$user		= CFactory::getUser( $userId );
		$group		= JTable::getInstance( 'Group' , 'CTable' );
		$group->load( $groupId );

		$params		= $group->getParams();

        if (COwnerHelper::isCommunityAdmin()) {
            return true;
        }

		if( $group->isAdmin( $user->id ) && ( $params->get('eventpermission') == GROUP_EVENT_PERMISSION_ALL || $params->get('eventpermission') == GROUP_EVENT_PERMISSION_ADMINS ) )
		{
			return true;
		}

		if( $group->isMember( $user->id ) && $params->get('eventpermission') == GROUP_EVENT_PERMISSION_ALL )
		{
			return true;
		}

		return false;
	}

	static public function allowPhotoWall($groupid)
	{
		$permission = CGroupHelper::getMediaPermission($groupid);

		if( $permission->isMember || $permission->isAdmin || $permission->isSuperAdmin )
		{
			return true;
		}
		return false;
	}

	static public function getGroupType($approvals, $unlisted){
		//TODO (Chris) Add text to language file

		if($approvals == 0 && $unlisted == 0){
            $grouptype = 'Public Circle';
       	}elseif ($approvals == 1 && $unlisted == 0){
            $grouptype = 'Private Circle';
        }else{
            $grouptype = 'Secret Circle';
       	}

       	return $grouptype;
	}

	static public function cropTheImage($courseimage, $groupid){
		
		$mainframe = JFactory::getApplication();
		$config = CFactory::getConfig();
		$groupsModel = CFactory::getModel('Groups');  

		$imageMaxWidth = 160;
		$imageMaxHeight = 160;

        // Get a hash for the file name.
        $fileName = JApplication::getHash($courseimage . time());
        $hashFileName = JString::substr($fileName, 0, 24);
        $imageDetails = getimagesize($courseimage);
        $mimetype = $imageDetails['mime'];
        $data_root = $mainframe->getCfg('dataroot');

        // @todo: configurable path for avatar storage?
        $storage = $data_root . '/' . $config->getString('imagefolder') . '/avatar/group';
        $storageImage = $storage . '/' . $hashFileName . CImageHelper::getExtension($mimetype);
        $storageThumbnail = $storage . '/thumb_' . $hashFileName . CImageHelper::getExtension($mimetype);
        $image = $config->getString('imagefolder') . '/avatar/group/' . $hashFileName . CImageHelper::getExtension($mimetype);
        $thumbnail = $config->getString('imagefolder') . '/avatar/group/' . 'thumb_' . $hashFileName . CImageHelper::getExtension($mimetype);

        // Generate Avatar
         if (!CImageHelper::createThumb($courseimage, $storageImage, $mimetype, $imageMaxWidth, $imageMaxHeight)) {
            $mainframe->enqueueMessage(JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $storageThumbnail), 'error');
        }

        // Generate thumbnail
        if (!CImageHelper::createThumb($courseimage, $storageThumbnail,$mimetype)) {
            $mainframe->enqueueMessage(JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $storageThumbnail), 'error');
        }

        // Update the group with the new image
        $groupsModel->setImage($groupid, $image, 'avatar');
        $groupsModel->setImage($groupid, $thumbnail, 'thumb');
        
	}

	static public function getCheckCourseDetails($groupId, $course_group){
		// check group avatar and thumbnail
        $groupModel = CFactory::getModel('Groups');   
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);
        require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');
		if($course_group) {
        $course_info = JoomdleHelperContent::call_method('get_course_info', (int)$course_group, '');
        $course_name = $course_info['fullname'];
        $course_image_avatar = $course_info['filepath'] . $course_info['filename'];
        $course_description = $course_info['summary'];
	        
	    //Check if the image is updated    
        if($group->origimage == '' || ($group->origimage != $course_info['filename'])){
        	
	        /*
	        if ($course_info['filename'] == 'nocourseimg.jpg') {
	        	//dont generate
	        }
	        */
	       	
	        if(@getimagesize($course_image_avatar)) CGroupHelper::cropTheImage($course_image_avatar, $groupId);
	       	
	        $groupModel->setImage($groupId, $course_info['filename'], 'origimage');
        }
		}
        //Check if the description is updated
        if(trim(strip_tags($course_description)) != trim($group->description)){
        	$data->id = $groupId;
	        $data->description = strip_tags($course_description);
	        $groupModel->updateGroup($data);
        }

        //Check if the circle name is updated
        if(trim(strip_tags($course_name)) != trim($group->name)){
        	$data->id = $groupId;
	        $data->name = strip_tags($course_name);
	        $groupModel->updateGroup($data);
        }

	}

}

/**
 * Deprecated since 1.8
 * Use CGroupHelper::getMediaPermission instead.
 */
function _cGetGroupMediaPermission($groupId)
{
	return CGroupHelper::getMediaPermission( $groupId );
}

/**
 * Deprecated since 1.8
 * Use CGroupHelper::allowViewMedia instead.
 */
function cAllowViewMedia($groupId)
{
	return CGroupHelper::allowViewMedia( $groupId );
}

/**
 * Deprecated since 1.8
 * Use CGroupHelper::allowManageVideo instead.
 */
function cAllowManageVideo($groupId)
{
	return CGroupHelper::allowManageVideo( $groupId );
}

/**
 * Deprecated since 1.8
 * Use CGroupHelper::allowManagePhoto instead.
 */
function cAllowManagePhoto($groupId)
{
	return CGroupHelper::allowManagePhoto( $groupId );
}

/**
 * Deprecated since 1.8
 * Use CGroupHelper::allowPhotoWall instead.
 */
function cAllowPhotoWall($groupId)
{
	return CGroupHelper::allowPhotoWall( $groupId );
}