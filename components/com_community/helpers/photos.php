<?php

/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die('Restricted access');

class CPhotosHelper {

    /**
     * Get photo ID of stream ID
     * @param int $streamID Stream id of photo (cover, avatar,...)
     * @return mixed Null when failed.
     */
    public static function getPhotoOfStream($streamID) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        /* Get stream params */
        $query->select($db->quoteName('params'))
                ->from($db->quoteName('#__community_activities'))
                ->where($db->quoteName('id') . '=' . $db->quote($streamID));
        $db->setQuery($query);
        $params = $db->loadResult();
        /* Params is valid */
        if ($params !== null) {
            /* Decode JSON */
            $params = json_decode($params);
            /* Get photo ID */
            $query->clear()->select($db->quoteName('id'))
                    ->from($db->quoteName('#__community_photos'))
                    ->where($db->quoteName('image') . '=' . $db->quote($params->attachment));
            $db->setQuery($query);
            return $db->loadResult();
        }
        return null;
    }

    /**
     * Checks if the photo watermark is enabled and the watermark exists
     */
    public static function photoWatermarkEnabled(){
        $config = CFactory::getConfig();
        $mainframe = JFactory::getApplication();
//        if($config->get('photo_watermark') && file_exists(JPATH_ROOT .'/'. COMMUNITY_WATERMARKS_PATH .'/'.WATERMARK_DEFAULT_NAME.'.png')){
        if($config->get('photo_watermark') && file_exists($mainframe->getCfg('dataroot') .'/'. COMMUNITY_WATERMARKS_PATH .'/'.WATERMARK_DEFAULT_NAME.'.png')){
            return true;
        }
        return false;
    }

    /**
     *
     * @param type $type
     * @param type $id
     * @param type $sourceX
     * @param type $sourceY
     * @param type $width
     * @param type $height
     */
    public static function updateAvatar($type, $id, $sourceX, $sourceY, $width, $height) {
        $filter = JFilterInput::getInstance();

        /* Filter input values */
        $type    = $filter->clean($type, 'string');
        $id      = $filter->clean($id, 'integer');
        $sourceX = $filter->clean($sourceX, 'float');
        $sourceY = $filter->clean($sourceY, 'float');
        $width   = $filter->clean($width, 'float');
        $height  = $filter->clean($height, 'float');

        $cTable = JTable::getInstance(ucfirst($type), 'CTable');
        $cTable->load($id);

        $cTable->storage = 'file';
        $cTable->store();

//        $srcPath = JPATH_ROOT . '/' . $cTable->avatar;
//        $destPath = JPATH_ROOT . '/' . $cTable->thumb;
        $mainframe = JFactory::getApplication();
        $data_root = $mainframe->getCfg('dataroot');
        $srcPath = $data_root . '/' . $cTable->avatar;
        $destPath = $data_root  . '/' . $cTable->thumb;

        /* */
        $config = CFactory::getConfig();
        $avatarFolder = ($type != 'profile' && $type != '') ? $type . '/' : '';
        /* Get original image */
//        $originalPath = JPATH_ROOT . '/' . $config->getString('imagefolder') . '/avatar' . '/' . $avatarFolder . '/' . $type . '-' . JFile::getName($cTable->avatar);
        $originalPath = $data_root . '/' . $config->getString('imagefolder') . '/avatar' . '/' . $avatarFolder . '/' . $type . '-' . JFile::getName($cTable->avatar);

        /**
         * If original image does not exists than we use source image
         * @todo we should get from facebook original avatar file
         */
        if (!JFile::exists($originalPath)) {
            $originalPath = $srcPath;
        }

        $srcPath = str_replace('/', '/', $srcPath);
        $destPath = str_replace('/', '/', $destPath);

        $info = getimagesize($srcPath);
        $destType = $info['mime'];

        $destWidth = COMMUNITY_SMALL_AVATAR_WIDTH;
        $destHeight = COMMUNITY_SMALL_AVATAR_WIDTH;

        /* thumb size */
        $currentWidth = $width;
        $currentHeight = $height;

        /* avatar size */
        $imageMaxWidth = 160;
        $imageMaxHeight = 160;

        /**
         * @todo Should we generate new filename and update into database ?
         */
        /* do avatar resize */
        CImageHelper::resize($originalPath, $srcPath, $destType, $imageMaxWidth, $imageMaxHeight, $sourceX, $sourceY, $currentWidth, $currentHeight);
        /* do thumb resize */
        CImageHelper::resize($originalPath, $destPath, $destType, $destWidth, $destHeight, $sourceX, $sourceY, $currentWidth, $currentHeight);

        /**
         * Now we do check and process watermark
         */
        /* Check multiprofile to reapply watermark for thumbnail */
        $my = CFactory::getUser();
        $profileType = $my->getProfileType();
        $multiprofile = JTable::getInstance('MultiProfile', 'CTable');
        $multiprofile->load($profileType);
        $useWatermark = $profileType != COMMUNITY_DEFAULT_PROFILE && $config->get('profile_multiprofile') && !empty($multiprofile->watermark) ? true : false;

        if ($useWatermark && $type == 'profile') {
//            $watermarkPath = JPATH_ROOT . '/' . CString::str_ireplace('/', '/', $multiprofile->watermark);
            $watermarkPath = $data_root . '/' . CString::str_ireplace('/', '/', $multiprofile->watermark);
            list($watermarkWidth, $watermarkHeight) = getimagesize($watermarkPath);
            list($thumbWidth, $thumbHeight) = getimagesize($destPath);
            list($avatarWidth, $avatarHeight) = getimagesize($srcPath);

            // Avatar Properties
            $avatarPosition = CImageHelper::getPositions($multiprofile->watermark_location, $avatarWidth, $avatarHeight, $watermarkWidth, $watermarkHeight);
            // The original image file will be removed from the system once it generates a new watermark image.
            CImageHelper::addWatermark($srcPath, $srcPath, $destType, $watermarkPath, $avatarPosition->x, $avatarPosition->y);

            $thumbPosition = CImageHelper::getPositions($multiprofile->watermark_location, $thumbWidth, $thumbHeight, $watermarkWidth, $watermarkHeight);
            /* addWatermark into thumbnail */
            CImageHelper::addWatermark($destPath, $destPath, $destType, $watermarkPath, $thumbPosition->x, $thumbPosition->y);
        }

        // we need to update the activity stream of group if applicable, so the cropped image will be updated as well
        if($type == 'group'){
            $groupParams = new JRegistry($cTable->params);
            $actId = $groupParams->get('avatar_activity_id');

            if($actId){
                $act = JTable::getInstance('Activity','CTable');
                $act->load($actId);
                $actParams = new JRegistry($act->params);
                $actParams->set('avatar_cropped_thumb',$cTable->avatar);
                $act->params = $actParams->toString();
                $act->store();
            }
        }

        $connectModel = CFactory::getModel('connect');

        // For facebook user, we need to add the watermark back on
        if ($connectModel->isAssociated($my->id) && $config->get('fbwatermark') && $type == 'profile') {
            list( $watermarkWidth, $watermarkHeight ) = getimagesize(FACEBOOK_FAVICON);
            CImageHelper::addWatermark($destPath, $destPath, $destType, FACEBOOK_FAVICON, ( $destWidth - $watermarkWidth), ( $destHeight - $watermarkHeight));
        }
    }

    public static function APIphotoPreview($userId)
    {
        $jinput = JFactory::getApplication()->input;
        $my = CFactory::getUser($userId);
        $config = CFactory::getConfig();
        $groupId = $jinput->get('groupid', 0, 'INT');
        $eventId = $jinput->get('eventid', 0, 'INT');
        $type = PHOTOS_USER_TYPE;
        $albumId = $my->id;

        $mainframe = JFactory::getApplication();
        $data_root = $mainframe->getCfg('dataroot');
        
        $response = array();

        if (CLimitsLibrary::exceedDaily('photos')) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_PHOTOS_LIMIT_REACHED');
            return $response;
        }

        // We can't use blockUnregister here because practically, the CFactory::getUser() will return 0
        if ($my->id == 0) {
            $response['status'] = false;
            $response['message'] = JText::_('COM_COMMUNITY_PROFILE_NEVER_LOGGED_IN');
            return $response;
        }

        // Get default album or create one
        $model = CFactory::getModel('photos');

        $album = $model->getDefaultAlbum($albumId, $type);
        $newAlbum = false;

        if (empty($album)) {
            $album = JTable::getInstance('Album', 'CTable');
            $album->load();

            $newAlbum = true;
            $now = new JDate();

            $album->creator = $my->id;
            $album->created = $now->toSql();
            $album->type = PHOTOS_PROFILE_TYPE;
            $album->default = '1';
            $album->groupid = $groupId;
            $album->eventid = $eventId;

            switch ($type) {
                case PHOTOS_USER_TYPE:
                    $album->name = JText::sprintf('COM_COMMUNITY_DEFAULT_ALBUM_CAPTION', $my->getDisplayName());
                    break;
                case PHOTOS_EVENT_TYPE:
                    $event = JTable::getInstance('Event', 'CTable');
                    $event->load($eventId);
                    $album->name = JText::sprintf('COM_COMMUNITY_EVENT_DEFAULT_ALBUM_NAME', $event->title);
                    break;
                case PHOTOS_GROUP_TYPE:
                    $group = JTable::getInstance('Group', 'CTable');
                    $group->load($groupId);
                    $album->name = JText::sprintf('COM_COMMUNITY_GROUP_DEFAULT_ALBUM_NAME', $group->name);
                    break;
            }

            $storage = $data_root . '/' . $config->getString('photofolder');
            $albumPath = $storage . '/photos' . '/' . $my->id . '/' . $albumId;
//                $albumPath = CString::str_ireplace(JPATH_ROOT . '/', '', $albumPath);
            $albumPath = CString::str_ireplace($data_root . '/', '', $albumPath);
            $albumPath = CString::str_ireplace('\\', '/', $albumPath);
            $album->path = $albumPath;

            $album->store();

            if ($type == PHOTOS_GROUP_TYPE) {

                $group = JTable::getInstance('Group', 'CTable');
                $group->load($groupId);

                $modelGroup = $this->getModel('groups');
                $groupMembers = array();
                $groupMembers = $modelGroup->getMembersId($album->groupid, true);

                $params = new CParameter('');
                $params->set('albumName', $album->name);
                $params->set('group', $group->name);
                $params->set(
                    'group_url',
                    'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id
                );
                $params->set('album', $album->name);
                $params->set(
                    'album_url',
                    'index.php?option=com_community&view=photos&task=album&albumid=' . $album->id . '&groupid=' . $group->id
                );
                $params->set(
                    'url',
                    'index.php?option=com_community&view=photos&task=album&albumid=' . $album->id . '&groupid=' . $group->id
                );
                CNotificationLibrary::add(
                    'groups_create_album',
                    $my->id,
                    $groupMembers,
                    JText::sprintf('COM_COMMUNITY_GROUP_NEW_ALBUM_NOTIFICATION'),
                    '',
                    'groups.album',
                    $params
                );

            }
        } else {
            $albumId = $album->id;
            $album = JTable::getInstance('Album', 'CTable');

            $album->load($albumId);
        }

        $photos = JRequest::get('files'); //$jinput->files->get('filedata');

        foreach ($photos as $image) {
            // @todo: foreach here is redundant since we exit on the first loop
            $storage = $data_root . '/' . $config->getString('photofolder');
            $albumPath = (empty($album->path)) ? '' : $album->id . '/';
            $originalPath = $storage.'/'.JPath::clean(
                            $config->get('originalphotopath')
                        ) . '/' . $my->id . '/' . $albumPath;
            $test = $storage.'/'.JPath::clean($config->get('originalphotopath'));
            if (!is_dir($test)) {
                mkdir($test, 0777);
            }
            if (!is_dir($storage.'/'.JPath::clean($config->get('originalphotopath')).'/'.$my->id)) {
                    mkdir($storage.'/'.JPath::clean($config->get('originalphotopath')).'/'.$my->id, 0777);
            }
            if (!is_dir($originalPath)) {
                    mkdir($originalPath, 0777);
            }
            if (!JFolder::exists($originalPath)) {

                if (!JFolder::create($originalPath, (int)octdec($config->get('folderpermissionsphoto')))) {
                    $response['status'] = false;
                    $response['message'] = JText::_('COM_COMMUNITY_VIDEOS_CREATING_USERS_PHOTO_FOLDER_ERROR');
                    return $response;
                }
//                JFile::copy(JPATH_ROOT . '/components/com_community/index.html', $originalPath . '/index.html');
                JFile::copy($data_root . '/components/com_community/index.html', $originalPath . '/index.html');
            } 
            $info = getimagesize($image['tmp_name']);
            $fileName = JApplication::getHash($image['tmp_name'] . time());
            $hashFilename = JString::substr($fileName, 0, 24);
            $imgType = image_type_to_mime_type($info[2]);

            $thumbPath = $storage . '/photos' . '/' . $my->id;
            $thumbPath = $thumbPath . '/' . $albumPath . 'thumb_' . $hashFilename . CImageHelper::getExtension(
                    $image['type']
                );
            if (!is_dir($storage.'/photos')) {
                    mkdir($storage.'/photos', 0777);
            }
            if (!is_dir($storage.'/photos' . '/' . $my->id)) {
                    mkdir($storage.'/photos' . '/' . $my->id, 0777);
            }
            if (!is_dir( $storage.'/photos' . '/' . $my->id . '/' . $albumPath)) {
                    mkdir( $storage.'/photos' . '/' . $my->id . '/' . $albumPath, 0777);
            }
            CPhotos::generateThumbnail($image['tmp_name'], $thumbPath, $imgType);

            // Original photo need to be kept to make sure that, the gallery works
            $useAlbumId = (empty($album->path)) ? 0 : $album->id;
            $originalFile = $originalPath . $hashFilename . CImageHelper::getExtension($imgType);

            $filePath = $storage . '/photos' . '/' . $my->id;
            $filePath = $filePath . '/' . $albumPath . $hashFilename . CImageHelper::getExtension(
                    $image['type']
                );
            if (!JFile::copy($image['tmp_name'], $filePath . $hashFilename . CImageHelper::getExtension($imgType))) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_VIDEOS_CREATING_USERS_PHOTO_FOLDER_ERROR');
                return $response;
            }
            if (!JFile::copy($image['tmp_name'], $originalFile)) {
                $response['status'] = false;
                $response['message'] = JText::_('COM_COMMUNITY_VIDEOS_CREATING_USERS_PHOTO_FOLDER_ERROR');
                return $response;
            }
            
            // Load the tables
            $photoTable = JTable::getInstance('Photo', 'CTable');
            $photoTable->original = CString::str_ireplace($data_root . '/', '', $originalFile);

            // In joomla 1.6, CString::str_ireplace is not replacing the path properly. Need to do a check here
            if ($photoTable->original == $originalFile) {
//                $photoTable->original = str_ireplace(JPATH_ROOT . '/', '', $originalFile);
                $photoTable->original = str_ireplace($data_root . '/', '', $originalFile);
            }

            // Set photos properties
            $now = new JDate();

            $photoTable->albumid = $album->id;
            $photoTable->caption = $image['name'];
            $photoTable->creator = $my->id;
            $photoTable->created = $now->toSql();

            // Remove the filename extension from the caption
            if (JString::strlen($photoTable->caption) > 4) {
                $photoTable->caption = JString::substr(
                    $photoTable->caption,
                    0,
                    JString::strlen($photoTable->caption) - 4
                );
            }

            // @todo: configurable options?
            // Permission should follow album permission
            $photoTable->published = '1';
            $photoTable->permissions = $album->permissions;
            $photoTable->status = 'temp';

            $storedPath = $filePath . '/' . $albumPath . $hashFilename . CImageHelper::getExtension($image['type']);

            $photoTable->image = CString::str_ireplace($data_root . '/', '', $storedPath);
            $photoTable->thumbnail = CString::str_ireplace($data_root . '/', '', $thumbPath);

            // In joomla 1.6, CString::str_ireplace is not replacing the path properly. Need to do a check here
            if ($photoTable->image == $storedPath) {
                $photoTable->image = str_ireplace($data_root . '/', '', $storedPath);
            }
            if ($photoTable->thumbnail == $thumbPath) {
                $photoTable->thumbnail = str_ireplace($data_root . '/', '', $thumbPath);
            }

            // Photo filesize, use sprintf to prevent return of unexpected results for large file.
            $photoTable->filesize = sprintf("%u", filesize($originalPath));

            // @rule: Set the proper ordering for the next photo upload.
            $photoTable->setOrdering();

            // Store the object
            $photoTable->store();

            if ($newAlbum) {
                $album->photoid = $photoTable->id;
                $album->store();
            }

            // We need to see if we need to rotate this image, from EXIF orientation data
            // Only for jpeg image.
//                    if ($config->get('photos_auto_rotate') && $imgType == 'image/jpeg') {
//                        $this->rotatePhotoAPI($image['file'], $photoTable, $storedPath, $thumbPath);
//                    }

            $tmpl = new CTemplate();
            $tmpl->set('photo', $photoTable);
            $tmpl->set('filename', $image['file']['name']);
            $html = $tmpl->fetch('status.photo.item');

            $photo = new stdClass();
            $photo->id = $photoTable->id;
            $photo->thumbnail = $photoTable->thumbnail;
            $photo->html = rawurlencode($html);

//    echo json_encode($photo);
        }
//        exit;
        $response['status'] = true;
        $response['message'] = 'Success';
        $response['data'] = $photo;
        return $response;
    }
    
}
