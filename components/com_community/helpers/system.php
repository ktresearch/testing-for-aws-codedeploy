<?php
    /**
     * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
     * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
     * @author iJoomla.com <webmaster@ijoomla.com>
     * @url https://www.jomsocial.com/license-agreement
     * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
     * More info at https://www.jomsocial.com/license-agreement
     */

    defined('_JEXEC') or die('Restricted access');

    class CSystemHelper
    {
        /**
         * @param $componentName
         * @return bool
         */
        public static function isComponentExists($componentName){
            $db = JFactory::getDbo();
            $db->setQuery("SELECT extension_id FROM #__extensions WHERE element =".$db->quote($componentName)." AND type=".$db->quote('component'));

            return $db->loadResult() ? true : false ;
        }

        /**
         * Check if the two factor authentication by Joomla! is enabled
         */
        public static function tfaEnabled(){

            //tfa is not supported before 3.2
            if(version_compare(JVERSION,'3.2','<')){
                return false;
            }

            //check for two way authentication
            require_once JPATH_ADMINISTRATOR . '/components/com_users/helpers/users.php';
            $tfa = UsersHelper::getTwoFactorMethods();

            if(isset($tfa) && is_array($tfa) && count($tfa) > 1){
                return true;
            }

            return false;
        }
        /**
         * List of the available views
         * @return array
         */
        public static function communityViewExists($viewname){
            $view = array(
                'apps',
                'connect',
                'developer',
                'events',
                'friends',
                'frontpage',
                'groups',
                'inbox',
                'memberlist',
                'multiprofile',
                'oauth',
                'photos',
                'profile',
                'register',
                'search',
                'videos'
            );

            return in_array($viewname, $view);
        }
        
        public static function ApiStreamAdd($username, $message, $attachment)
        {
            $streamHTML = '';
            // $attachment pending filter

            $cache = CFactory::getFastCache();
            $cache->clean(array('activities'));

            if ($username) {
                jimport('joomla.user.helper');
                $user_id = JUserHelper::getUserId($username);
                $my = CFactory::getUser($user_id);
            } else {
                $my = CFactory::getUser();
            }
            $userparams = $my->getParams();

            $response = array();
            
            if (!COwnerHelper::isRegisteredUser()) {
                $response['status'] = false;
                $response['message'] = 'User not found.';
                return $response;
            }

            //@rule: In case someone bypasses the status in the html, we enforce the character limit.
            $config = CFactory::getConfig();
            if (JString::strlen($message) > $config->get('statusmaxchar')) {
                $message = JHTML::_('string.truncate', $message, $config->get('statusmaxchar'));
            }

            $message = JString::trim($message);
    //        $objResponse = new JAXResponse();
            $rawMessage = $message;

            // @rule: Autolink hyperlinks
            // @rule: Autolink to users profile when message contains @username
            // $message     = CUserHelper::replaceAliasURL($message); // the processing is done on display side
            $emailMessage = CUserHelper::replaceAliasURL($rawMessage, true);

            // @rule: Spam checks
            if ($config->get('antispam_akismet_status')) {
                $filter = CSpamFilter::getFilter();
                $filter->setAuthor($my->getDisplayName());
                $filter->setMessage($message);
                $filter->setEmail($my->email);
                $filter->setURL(CRoute::_('index.php?option=com_community&view=profile&userid=' . $my->id));
                $filter->setType('message');
                $filter->setIP($_SERVER['REMOTE_ADDR']);

                if ($filter->isSpam()) {
                    $response['status'] = false;
                    $response['message'] = JText::_('COM_COMMUNITY_STATUS_MARKED_SPAM');
                    return $response;
                }
            }
//return $attachment;
//            $attachment = json_decode($attachment, true);
            
            switch ($attachment['type']) {
                case 'message':
                    //if (!empty($message)) {
                    switch ($attachment['element']) {

                        case 'profile':
                            //only update user status if share messgage is on his profile
                            if (COwnerHelper::isMine($my->id, $attachment['target'])) {

                                //save the message
                                $status = CFactory::getModel('status');
                                /* If no privacy in attachment than we apply default: Public */
                                if (!isset($attachment['privacy']))
                                    $attachment['privacy'] = COMMUNITY_STATUS_PRIVACY_PUBLIC;
                                $status->update($my->id, $rawMessage, $attachment['privacy']);

                                //set user status for current session.
                                $today = JFactory::getDate();
                                $message2 = (empty($message)) ? ' ' : $message;
                                $my->set('_status', $rawMessage);
                                $my->set('_posted_on', $today->toSql());

                                // Order of replacement
                                $order = array("\r\n", "\n", "\r");
                                $replace = '<br />';

                                // Processes \r\n's first so they aren't converted twice.
                                $messageDisplay = str_replace($order, $replace, $message);
                                $messageDisplay = CKses::kses($messageDisplay, CKses::allowed());

                                //update user status
    //                            $objResponse->addScriptCall("joms.jQuery('#profile-status span#profile-status-message').html('" . addslashes($messageDisplay) . "');");
                            }

                            //if actor posted something to target, the privacy should be under target's profile privacy settings
                            if (!COwnerHelper::isMine($my->id, $attachment['target']) && $attachment['target'] != '') {
                                $attachment['privacy'] = CFactory::getUser($attachment['target'])->getParams()->get('privacyProfileView');
                            }

                            //push to activity stream
                            $act = new stdClass();
                            $act->cmd = 'profile.status.update';
                            $act->actor = $my->id;
                            $act->target = $attachment['target'];
                            $act->title = $message;
                            $act->content = '';
                            $act->app = $attachment['element'];
                            $act->cid = $my->id;
                            $act->access = $attachment['privacy'];
                            $act->comment_id = CActivities::COMMENT_SELF;
                            $act->comment_type = 'profile.status';
                            $act->like_id = CActivities::LIKE_SELF;
                            $act->like_type = 'profile.status';

                            $activityParams = new CParameter('');

                            /* Save cords if exists */
                            if (isset($attachment['location'])) {
                                /* Save geo name */
                                $act->location = $attachment['location'][0];
                                $act->latitude = $attachment['location'][1];
                                $act->longitude = $attachment['location'][2];
                            };

                            $headMeta = new CParameter('');

                            if (isset($attachment['fetch'])) {
                                $headMeta->set('title', $attachment['fetch'][2]);
                                $headMeta->set('description', $attachment['fetch'][3]);
                                $headMeta->set('image', $attachment['fetch'][1]);
                                $headMeta->set('link', $attachment['fetch'][0]);

                                //do checking if this is a video link
                                $video = JTable::getInstance('Video', 'CTable');
                                $isValidVideo = @$video->init($attachment['fetch'][0]);
                                if ($isValidVideo) {
                                    $headMeta->set('type', 'video');
                                    $headMeta->set('video_provider', $video->type);
                                    $headMeta->set('video_id', $video->getVideoId());
                                    $headMeta->set('height', $video->getHeight());
                                    $headMeta->set('width', $video->getWidth());
                                }

                                $activityParams->set('headMetas', $headMeta->toString());
                            }
                            //Store mood in paramm
                            if (isset($attachment['mood']) && $attachment['mood'] != 'Mood') {
                                $activityParams->set('mood', $attachment['mood']);
                            }
                            $act->params = $activityParams->toString();

                            //CActivityStream::add($act);
                            //check if the user points is enabled
                            if(CUserPoints::assignPoint('profile.status.update')){
                                /* Let use our new CApiStream */
                                $activityData = CApiActivities::add($act);
                                CTags::add($activityData);

                                $recipient = CFactory::getUser($attachment['target']);
                                $params = new CParameter('');
                                $params->set('actorName', $my->getDisplayName());
                                $params->set('recipientName', $recipient->getDisplayName());
                                $params->set('url', CUrlHelper::userLink($act->target, false));
                                $params->set('message', $message);
                                $params->set('stream', JText::_('COM_COMMUNITY_SINGULAR_STREAM'));
                                $params->set('stream_url',CRoute::_('index.php?option=com_community&view=profile&userid='.$activityData->actor.'&actid='.$activityData->id));

                                CNotificationLibrary::add('profile_status_update', $my->id, $attachment['target'], JText::sprintf('COM_COMMUNITY_FRIEND_WALL_POST', $my->getDisplayName()), '', 'wall.post', $params);

                                //email and add notification if user are tagged
                                CUserHelper::parseTaggedUserNotification($message, $my, $activityData, array('type' => 'post-comment'));
                            }

                            $response['message'] = 'Success';
                            $response['insert'] = $message;
                            break;
                        // Message posted from Group page
                        case 'groups':
                            //
                            $groupLib = new CGroups();
                            $group = JTable::getInstance('Group', 'CTable');
                            $group->load($attachment['target']);

                            // Permission check, only site admin and those who has
                            // mark their attendance can post message
                            if (!COwnerHelper::isCommunityAdmin() && !$group->isMember($my->id) && $config->get('lockgroupwalls')) {
                                $response['status'] = false;
                                $response['message'] = 'Permission denied';
                                return $response;
                            }

                            $act = new stdClass();
                            $act->cmd = 'groups.wall';
                            $act->actor = $my->id;
                            $act->target = 0;

                            $act->title = $message;
                            $act->content = '';
                            $act->app = 'groups.wall';
                            $act->cid = $attachment['target'];
                            $act->groupid = $group->id;
                            $act->group_access = $group->approvals;
                            $act->eventid = 0;
                            $act->access = 0;
                            $act->comment_id = CActivities::COMMENT_SELF;
                            $act->comment_type = 'groups.wall';
                            $act->like_id = CActivities::LIKE_SELF;
                            $act->like_type = 'groups.wall';

                            $activityParams = new CParameter('');

                            /* Save cords if exists */
                            if (isset($attachment['location'])) {
                                /* Save geo name */
                                $act->location = $attachment['location'][0];
                                $act->latitude = $attachment['location'][1];
                                $act->longitude = $attachment['location'][2];
                            };

                            $headMeta = new CParameter('');

                            if (isset($attachment['fetch'])) {
                                $headMeta->set('title', $attachment['fetch'][2]);
                                $headMeta->set('description', $attachment['fetch'][3]);
                                $headMeta->set('image', $attachment['fetch'][1]);
                                $headMeta->set('link', $attachment['fetch'][0]);

                                //do checking if this is a video link
                                $video = JTable::getInstance('Video', 'CTable');
                                $isValidVideo = @$video->init($attachment['fetch'][0]);
                                if ($isValidVideo) {
                                    $headMeta->set('type', 'video');
                                    $headMeta->set('video_provider', $video->type);
                                    $headMeta->set('video_id', $video->getVideoId());
                                    $headMeta->set('height', $video->getHeight());
                                    $headMeta->set('width', $video->getWidth());
                                }

                                $activityParams->set('headMetas', $headMeta->toString());
                            }

                            //Store mood in paramm
                            if (isset($attachment['mood']) && $attachment['mood'] != 'Mood') {
                                $activityParams->set('mood', $attachment['mood']);
                            }

                            $act->params = $activityParams->toString();

                            $activityData = CApiActivities::add($act);

                            CTags::add($activityData);
                            CUserPoints::assignPoint('group.wall.create');

                            $recipient = CFactory::getUser($attachment['target']);
                            $params = new CParameter('');
                            $params->set('message', $emailMessage);
                            $params->set('group', $group->name);
                            $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                            $params->set('url', CRoute::getExternalURL('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id, false));

                            //Get group member emails
                            $model = CFactory::getModel('Groups');
                            $members = $model->getMembers($attachment['target'], null, true, false, true);

                            $membersArray = array();
                            if (!is_null($members)) {
                                foreach ($members as $row) {
                                    if ($my->id != $row->id) {
                                        $membersArray[] = $row->id;
                                    }
                                }
                            }
                            $groupParams = new CParameter($group->params);

                            if($groupParams->get('wallnotification')) {
                                CNotificationLibrary::add('groups_wall_create', $my->id, $membersArray, JText::sprintf('COM_COMMUNITY_NEW_WALL_POST_NOTIFICATION_EMAIL_SUBJECT', $my->getDisplayName(), $group->name), '', 'groups.post', $params);
                            }

                            //@since 4.1 when a there is a new post in group, dump the data into group stats
                            $statsModel = CFactory::getModel('stats');
                            $statsModel->addGroupStats($group->id, 'post');

                            // Add custom stream
                            // Reload the stream with new stream data
                            $streamHTML = $groupLib->getStreamHTML($group, array('showLatestActivityOnTop'=>true));

                            break;

                        // Message posted from Event page
                        case 'events' :

                            $eventLib = new CEvents();
                            $event = JTable::getInstance('Event', 'CTable');
                            $event->load($attachment['target']);

                            // Permission check, only site admin and those who has
                            // mark their attendance can post message
                            if ((!COwnerHelper::isCommunityAdmin() && !$event->isMember($my->id) && $config->get('lockeventwalls'))) {
                                $response['status'] = false;
                                $response['message'] = 'Permission denied';
                                return $response;
                            }

                            // If this is a group event, set the group object
                            $groupid = ($event->type == 'group') ? $event->contentid : 0;
                            //
                            $groupLib = new CGroups();
                            $group = JTable::getInstance('Group', 'CTable');
                            $group->load($groupid);

                            $act = new stdClass();
                            $act->cmd = 'events.wall';
                            $act->actor = $my->id;
                            $act->target = 0;
                            $act->title = $message;
                            $act->content = '';
                            $act->app = 'events.wall';
                            $act->cid = $attachment['target'];
                            $act->groupid = ($event->type == 'group') ? $event->contentid : 0;
                            $act->group_access = $group->approvals;
                            $act->eventid = $event->id;
                            $act->event_access = $event->permission;
                            $act->access = 0;
                            $act->comment_id = CActivities::COMMENT_SELF;
                            $act->comment_type = 'events.wall';
                            $act->like_id = CActivities::LIKE_SELF;
                            $act->like_type = 'events.wall';

                            $activityParams = new CParameter('');

                            /* Save cords if exists */
                            if (isset($attachment['location'])) {
                                /* Save geo name */
                                $act->location = $attachment['location'][0];
                                $act->latitude = $attachment['location'][1];
                                $act->longitude = $attachment['location'][2];
                            };

                            $headMeta = new CParameter('');

                            if (isset($attachment['fetch'])) {
                                $headMeta->set('title', $attachment['fetch'][2]);
                                $headMeta->set('description', $attachment['fetch'][3]);
                                $headMeta->set('image', $attachment['fetch'][1]);
                                $headMeta->set('link', $attachment['fetch'][0]);

                                //do checking if this is a video link
                                $video = JTable::getInstance('Video', 'CTable');
                                $isValidVideo = @$video->init($attachment['fetch'][0]);
                                if ($isValidVideo) {
                                    $headMeta->set('type', 'video');
                                    $headMeta->set('video_provider', $video->type);
                                    $headMeta->set('video_id', $video->getVideoId());
                                    $headMeta->set('height', $video->getHeight());
                                    $headMeta->set('width', $video->getWidth());
                                }

                                $activityParams->set('headMetas', $headMeta->toString());
                            }

                            //Store mood in paramm
                            if (isset($attachment['mood']) && $attachment['mood'] != 'Mood') {
                                $activityParams->set('mood', $attachment['mood']);
                            }

                            $act->params = $activityParams->toString();

                            $activityData = CApiActivities::add($act);
                            CTags::add($activityData);

                            // add points
                            CUserPoints::assignPoint('event.wall.create');

                            $params = new CParameter('');
                            $params->set('message', $emailMessage);
                            $params->set('event', $event->title);
                            $params->set('event_url', 'index.php?option=com_community&view=events&task=viewevent&eventid=' . $event->id);
                            $params->set('url', CRoute::getExternalURL('index.php?option=com_community&view=events&task=viewevent&eventid=' . $event->id, false));

                            //Get event member emails
                            $members = $event->getMembers(COMMUNITY_EVENT_STATUS_ATTEND, 12, CC_RANDOMIZE);

                            $membersArray = array();
                            if (!is_null($members)) {
                                foreach ($members as $row) {
                                    if ($my->id != $row->id) {
                                        $membersArray[] = $row->id;
                                    }
                                }
                            }

                            CNotificationLibrary::add('events_wall_create', $my->id, $membersArray, JText::sprintf('COM_COMMUNITY_NEW_WALL_POST_NOTIFICATION_EMAIL_SUBJECT_EVENTS', $my->getDisplayName(), $event->title), '', 'events.post', $params);

                            //@since 4.1 when a there is a new post in event, dump the data into event stats
                            $statsModel = CFactory::getModel('stats');
                            $statsModel->addEventStats($event->id, 'post');

                            // Reload the stream with new stream data
                            $streamHTML = $eventLib->getStreamHTML($event, array('showLatestActivityOnTop'=>true));
                            break;
                    }

    //                $objResponse->addScriptCall('__callback', '');
                    // /}

                    break;

                case 'photo':
                    switch ($attachment['element']) {

                        case 'profile': 
                            $photoIds[] = $attachment['id'];
                            //use User Preference for Privacy
                            //$privacy = $userparams->get('privacyPhotoView'); //$privacy = $attachment['privacy'];

                            $photo = JTable::getInstance('Photo', 'CTable');


                            if (!isset($photoIds[0]) || $photoIds[0] <= 0) {
                                //$objResponse->addScriptCall('__callback', JText::sprintf('COM_COMMUNITY_PHOTO_UPLOADED_SUCCESSFULLY', $photo->caption));
    //                            exit;
                                $response['status'] = false;
                                $response['message'] = 'Error';
                                return $response;
                            }

                            //always get album id from the photo itself, do not let it assign by params from user post data
                            $photoModel = CFactory::getModel('photos');
                            $photo = $photoModel->getPhoto($photoIds[0]);
                            /* OK ! If album_id is not provided than we use album id from photo ( it should be default album id ) */
                            $albumid = (isset($attachment['album_id'])) ? $attachment['album_id'] : $photo->albumid;

                            $album = JTable::getInstance('Album', 'CTable');
                            $album->load($albumid);

                            $privacy = $album->permissions;

                            //limit checking
    //                        $photoModel = CFactory::getModel( 'photos' );
    //                        $config       = CFactory::getConfig();
    //                        $total        = $photoModel->getTotalToday( $my->id );
    //                        $max      = $config->getInt( 'limit_photo_perday' );
    //                        $remainingUploadCount = $max - $total;
                            $params = array();
                            foreach ($photoIds as $key => $photoId) {
                                if (CLimitsLibrary::exceedDaily('photos')) {
                                    unset($photoIds[$key]);
                                    continue;
                                }
                                $photo->load($photoId);
                                $photo->permissions = $privacy;
                                $photo->published = 1;
                                $photo->status = 'ready';
                                $photo->albumid = $albumid; /* We must update this photo into correct album id */
                                $photo->store();
                                $params[] = clone($photo);
                            }

                            if ($config->get('autoalbumcover') && !$album->photoid) {
                                $album->photoid = $photoIds[0];
                                $album->store();
                            }

                            // Break if no photo added, which is likely because of daily limit.
                            if ( count($photoIds) < 1 ) {
                                $response['status'] = false;
                                $response['message'] = JText::_('COM_COMMUNITY_PHOTO_UPLOAD_LIMIT_EXCEEDED');
                                return $response;
                            }

                            // Trigger onPhotoCreate
                            //
                            $apps = CAppPlugins::getInstance();
                            $apps->loadApplications();
                            $apps->triggerEvent('onPhotoCreate', array($params));

                            $act = new stdClass();
                            $act->cmd = 'photo.upload';
                            $act->actor = $my->id;
                            $act->access = $privacy; //$attachment['privacy'];
                            $act->target = ($attachment['target'] == $my->id) ? 0 : $attachment['target'];
                            $act->title = $message;
                            $act->content = ''; // Generated automatically by stream. No need to add anything
                            $act->app = 'photos';
                            $act->cid = $albumid;
                            $act->location = $album->location;

                            /* Comment and like for individual photo upload is linked
                             * to the photos itsel
                             */
                            $act->comment_id = $photo->id;
                            $act->comment_type = 'photos';
                            $act->like_id = $photo->id;
                            $act->like_type = 'photo';

                            $albumUrl = 'index.php?option=com_community&view=photos&task=album&albumid=' . $album->id . '&userid=' . $my->id;
                            $albumUrl = CRoute::_($albumUrl);

                            $photoUrl = 'index.php?option=com_community&view=photos&task=photo&albumid=' . $album->id . '&userid=' . $photo->creator . '&photoid=' . $photo->id;
                            $photoUrl = CRoute::_($photoUrl);

                            $params = new CParameter('');
                            $params->set('multiUrl', $albumUrl);
                            $params->set('photoid', $photo->id);
                            $params->set('action', 'upload');
                            $params->set('stream', '1');
                            $params->set('photo_url', $photoUrl);
                            $params->set('style', COMMUNITY_STREAM_STYLE);
                            $params->set('photosId', implode(',', $photoIds));

                            if (count($photoIds > 1)) {
                                $params->set('count', count($photoIds));
                                $params->set('batchcount', count($photoIds));
                            }

                            //Store mood in param
                            if (isset($attachment['mood']) && $attachment['mood'] != 'Mood') {
                                $params->set('mood', $attachment['mood']);
                            }

                            // Add activity logging
                            // CActivityStream::remove($act->app, $act->cid);
                            $activityData = CActivityStream::add($act, $params->toString());

                            // Add user points
                            CUserPoints::assignPoint('photo.upload');

                            //add a notification to the target user if someone posted photos on target's profile
                            if($my->id != $attachment['target']){
                                $recipient = CFactory::getUser($attachment['target']);
                                $params = new CParameter('');
                                $params->set('actorName', $my->getDisplayName());
                                $params->set('recipientName', $recipient->getDisplayName());
                                $params->set('url', CUrlHelper::userLink($act->target, false));
                                $params->set('message', $message);
                                $params->set('stream', JText::_('COM_COMMUNITY_SINGULAR_STREAM'));
                                $params->set('stream_url',CRoute::_('index.php?option=com_community&view=profile&userid='.$activityData->actor.'&actid='.$activityData->id));

                                CNotificationLibrary::add('profile_status_update', $my->id, $attachment['target'], JText::sprintf('COM_COMMUNITY_NOTIFICATION_STREAM_PHOTO_POST', count($photoIds)), '', 'wall.post', $params);
                            }

                            //email and add notification if user are tagged
                            CUserHelper::parseTaggedUserNotification($message, $my, $activityData, array('type' => 'post-comment'));

    //                        $objResponse->addScriptCall('__callback', JText::sprintf('COM_COMMUNITY_PHOTO_UPLOADED_SUCCESSFULLY', $photo->caption));
                            $response['message'] = JText::sprintf('COM_COMMUNITY_PHOTO_UPLOADED_SUCCESSFULLY', $photo->caption);
                            break;
                        case 'events':
                            $event = JTable::getInstance('Event', 'CTable');
                            $event->load($attachment['target']);

                            $privacy = 0;
                            //if this is a group event, we need to follow the group privacy
                            if($event->type == 'group' && $event->contentid){
                                $group = JTable::getInstance('Group', 'CTable');
                                $group->load($$event->contentid);
                                $privacy = $group->approvals ? PRIVACY_GROUP_PRIVATE_ITEM : 0;

                            }

                            $photoIds = $attachment['id'];
                            $photo = JTable::getInstance('Photo', 'CTable');
                            $photo->load($photoIds[0]);

                            $albumid = (isset($attachment['album_id'])) ? $attachment['album_id'] : $photo->albumid;
                            $album = JTable::getInstance('Album', 'CTable');
                            $album->load($albumid);

                            $params = array();
                            foreach ($photoIds as $photoId) {
                                $photo->load($photoId);

                                $photo->caption = $message;
                                $photo->permissions = $privacy;
                                $photo->published = 1;
                                $photo->status = 'ready';
                                $photo->albumid = $albumid;
                                $photo->store();
                                $params[] = clone($photo);
                            }

                            // Trigger onPhotoCreate
                            //
                            $apps = CAppPlugins::getInstance();
                            $apps->loadApplications();
                            $apps->triggerEvent('onPhotoCreate', array($params));

                            $act = new stdClass();
                            $act->cmd = 'photo.upload';
                            $act->actor = $my->id;
                            $act->access = $privacy;
                            $act->target = ($attachment['target'] == $my->id) ? 0 : $attachment['target'];
                            $act->title = $message; //JText::sprintf('COM_COMMUNITY_ACTIVITIES_UPLOAD_PHOTO' , '{photo_url}', $album->name );
                            $act->content = ''; // Generated automatically by stream. No need to add anything
                            $act->app = 'photos';
                            $act->cid = $album->id;
                            $act->location = $album->location;

                            $act->eventid = $event->id;
                            $act->group_access = $privacy; // just in case this event belongs to a group
                            //$act->access      = $attachment['privacy'];

                            /* Comment and like for individual photo upload is linked
                             * to the photos itsel
                             */
                            $act->comment_id = $photo->id;
                            $act->comment_type = 'photos';
                            $act->like_id = $photo->id;
                            $act->like_type = 'photo';

                            $albumUrl = 'index.php?option=com_community&view=photos&task=album&albumid=' . $album->id . '&userid=' . $my->id;
                            $albumUrl = CRoute::_($albumUrl);

                            $photoUrl = 'index.php?option=com_community&view=photos&task=photo&albumid=' . $album->id . '&userid=' . $photo->creator . '&photoid=' . $photo->id;
                            $photoUrl = CRoute::_($photoUrl);

                            $params = new CParameter('');
                            $params->set('multiUrl', $albumUrl);
                            $params->set('photoid', $photo->id);
                            $params->set('action', 'upload');
                            $params->set('stream', '1'); // this photo uploaded from status stream
                            $params->set('photo_url', $photoUrl);
                            $params->set('style', COMMUNITY_STREAM_STYLE); // set stream style
                            $params->set('photosId', implode(',', $photoIds));
                            // Add activity logging
                            if (count($photoIds > 1)) {
                                $params->set('count', count($photoIds));
                                $params->set('batchcount', count($photoIds));
                            }
                            //Store mood in paramm
                            if (isset($attachment['mood']) && $attachment['mood'] != 'Mood') {
                                $params->set('mood', $attachment['mood']);
                            }
                            // CActivityStream::remove($act->app, $act->cid);
                            $activityData = CActivityStream::add($act, $params->toString());

                            // Add user points
                            CUserPoints::assignPoint('photo.upload');

                            // Reload the stream with new stream data
                            $eventLib = new CEvents();
                            $event = JTable::getInstance('Event', 'CTable');
                            $event->load($attachment['target']);
                            $streamHTML = $eventLib->getStreamHTML($event, array('showLatestActivityOnTop'=>true));

    //                        $objResponse->addScriptCall('__callback', JText::sprintf('COM_COMMUNITY_PHOTO_UPLOADED_SUCCESSFULLY', $photo->caption));
                            $response['message'] = JText::sprintf('COM_COMMUNITY_PHOTO_UPLOADED_SUCCESSFULLY', $photo->caption);
                            break;
                        case 'groups':
                            //
                            $groupLib = new CGroups();
                            $group = JTable::getInstance('Group', 'CTable');
                            $group->load($attachment['target']);

                            $photoIds = $attachment['id'];
                            $privacy = $group->approvals ? PRIVACY_GROUP_PRIVATE_ITEM : 0;

                            $photo = JTable::getInstance('Photo', 'CTable');
                            $photo->load($photoIds[0]);

                            $albumid = (isset($attachment['album_id'])) ? $attachment['album_id'] : $photo->albumid;

                            $album = JTable::getInstance('Album', 'CTable');
                            $album->load($albumid);

                            $params = array();
                            foreach ($photoIds as $photoId) {
                                $photo->load($photoId);

                                $photo->caption = $message;
                                $photo->permissions = $privacy;
                                $photo->published = 1;
                                $photo->status = 'ready';
                                $photo->albumid = $albumid;
                                $photo->store();
                                $params[] = clone($photo);
                            }
                            // Trigger onPhotoCreate
                            //
                            $apps = CAppPlugins::getInstance();
                            $apps->loadApplications();
                            $apps->triggerEvent('onPhotoCreate', array($params));

                            $act = new stdClass();
                            $act->cmd = 'photo.upload';
                            $act->actor = $my->id;
                            $act->access = $privacy;
                            $act->target = ($attachment['target'] == $my->id) ? 0 : $attachment['target'];
                            $act->title = $message; //JText::sprintf('COM_COMMUNITY_ACTIVITIES_UPLOAD_PHOTO' , '{photo_url}', $album->name );
                            $act->content = ''; // Generated automatically by stream. No need to add anything
                            $act->app = 'photos';
                            $act->cid = $album->id;
                            $act->location = $album->location;

                            $act->groupid = $group->id;
                            $act->group_access = $group->approvals;
                            $act->eventid = 0;
                            //$act->access      = $attachment['privacy'];

                            /* Comment and like for individual photo upload is linked
                             * to the photos itsel
                             */
                            $act->comment_id = $photo->id;
                            $act->comment_type = 'photos';
                            $act->like_id = $photo->id;
                            $act->like_type = 'photo';

                            $albumUrl = 'index.php?option=com_community&view=photos&task=album&albumid=' . $album->id . '&userid=' . $my->id;
                            $albumUrl = CRoute::_($albumUrl);

                            $photoUrl = 'index.php?option=com_community&view=photos&task=photo&albumid=' . $album->id . '&userid=' . $photo->creator . '&photoid=' . $photo->id;
                            $photoUrl = CRoute::_($photoUrl);

                            $params = new CParameter('');
                            $params->set('multiUrl', $albumUrl);
                            $params->set('photoid', $photo->id);
                            $params->set('action', 'upload');
                            $params->set('stream', '1'); // this photo uploaded from status stream
                            $params->set('photo_url', $photoUrl);
                            $params->set('style', COMMUNITY_STREAM_STYLE); // set stream style
                            $params->set('photosId', implode(',', $photoIds));
                            // Add activity logging
                            if (count($photoIds > 1)) {
                                $params->set('count', count($photoIds));
                                $params->set('batchcount', count($photoIds));
                            }
                            //Store mood in paramm
                            if (isset($attachment['mood']) && $attachment['mood'] != 'Mood') {
                                $params->set('mood', $attachment['mood']);
                            }
                            // CActivityStream::remove($act->app, $act->cid);
                            $activityData = CActivityStream::add($act, $params->toString());

                            //add notifcation to all the members
                            $params = new CParameter('');
                            $params->set('message', $emailMessage);
                            $params->set('group', $group->name);
                            $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                            $params->set('url', CRoute::getExternalURL('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id, false));
                            //Get group member emails
                            $model = CFactory::getModel('Groups');
                            $members = $model->getMembers($attachment['target'], null, true, false, true);

                            $membersArray = array();
                            if (!is_null($members)) {
                                foreach ($members as $row) {
                                    if ($my->id != $row->id) {
                                        $membersArray[] = $row->id;
                                    }
                                }
                            }
                            $groupParams = new CParameter($group->params);

                            if($groupParams->get('wallnotification')) {
                                CNotificationLibrary::add('groups_wall_video', $my->id, $membersArray, JText::sprintf('COM_COMMUNITY_NEW_WALL_POST_NOTIFICATION_EMAIL_SUBJECT', $my->getDisplayName(), $group->name), '', 'groups.post', $params);
                            }


                            // Add user points
                            CUserPoints::assignPoint('photo.upload');

                            // Reload the stream with new stream data
                            $streamHTML = $groupLib->getStreamHTML($group, array('showLatestActivityOnTop'=>true));

    //                        $objResponse->addScriptCall('__callback', JText::sprintf('COM_COMMUNITY_PHOTO_UPLOADED_SUCCESSFULLY', $photo->caption));
                            $response['message'] = JText::sprintf('COM_COMMUNITY_PHOTO_UPLOADED_SUCCESSFULLY', $photo->caption);
                            break;
                            dafault:
                            return;
                    }

                    break;

                case 'video':
                    switch ($attachment['element']) {
                        case 'profile':
                            // attachment id
                            $fetch = $attachment['fetch'];
                            $cid = $attachment['id']; //$fetch[0];
                            $privacy = isset($attachment['privacy']) ? $attachment['privacy'] : COMMUNITY_STATUS_PRIVACY_PUBLIC;

                            $video = JTable::getInstance('Video', 'CTable');
                            $video->load($cid);
                            $video->set('creator_type', VIDEO_USER_TYPE);
                            $video->set('status', 'ready');
                            $video->set('permissions', $privacy);
//                            $video->set('title', $fetch[3]);
//                            $video->set('description', $fetch[4]);
//                            $video->set('category_id', $fetch[5]);
                            /* Save cords if exists */
                            if (isset($attachment['location'])) {
                                $video->set('location', $attachment['location'][0]);
                                $video->set('latitude', $attachment['location'][1]);
                                $video->set('longitude', $attachment['location'][2]);
                            };

                            // Add activity logging
                            $url = $video->getViewUri(false);

                            $act = new stdClass();
                            $act->cmd = 'videos.linking';
                            $act->actor = $my->id;
                            $act->target = ($attachment['target'] == $my->id) ? 0 : $attachment['target'];
                            $act->access = $privacy;

                            //filter empty message
                            $act->title = $message;
                            $act->app = 'videos.linking';
                            $act->content = '';
                            $act->cid = $video->id;
                            $act->location = $video->location;

                            /* Save cords if exists */
                            if (isset($attachment['location'])) {
                                /* Save geo name */
                                $act->location = $attachment['location'][0];
                                $act->latitude = $attachment['location'][1];
                                $act->longitude = $attachment['location'][2];
                            };

                            $act->comment_id = $video->id;
                            $act->comment_type = 'videos.linking';

                            $act->like_id = $video->id;
                            $act->like_type = 'videos.linking';

                            $params = new CParameter('');
                            $params->set('video_url', $url);
                            $params->set('style', COMMUNITY_STREAM_STYLE); // set stream style
                            //Store mood in paramm
                            if (isset($attachment['mood']) && $attachment['mood'] != 'Mood') {
                                $params->set('mood', $attachment['mood']);
                            }

                            //
                            $activityData = CActivityStream::add($act, $params->toString());

                            //this video must be public because it's posted on someone else's profile
                            if($my->id != $attachment['target']){
                                $video->set('permissions', COMMUNITY_STATUS_PRIVACY_PUBLIC);
                                $params = new CParameter();
                                $params->set('activity_id', $activityData->id); // activity id is used to remove the activity if someone deleted this video
                                $params->set('target_id', $attachment['target']);
                                $video->params = $params->toString();

                                //also send a notification to the user
                                $recipient = CFactory::getUser($attachment['target']);
                                $params = new CParameter('');
                                $params->set('actorName', $my->getDisplayName());
                                $params->set('recipientName', $recipient->getDisplayName());
                                $params->set('url', CUrlHelper::userLink($act->target, false));
                                $params->set('message', $message);
                                $params->set('stream', JText::_('COM_COMMUNITY_SINGULAR_STREAM'));
                                $params->set('stream_url',CRoute::_('index.php?option=com_community&view=profile&userid='.$activityData->actor.'&actid='.$activityData->id));

                                CNotificationLibrary::add('profile_status_update', $my->id, $attachment['target'], JText::_('COM_COMMUNITY_NOTIFICATION_STREAM_VIDEO_POST'), '', 'wall.post', $params);
                            }

                            $video->store();

                            // @rule: Add point when user adds a new video link
                            //
                            CUserPoints::assignPoint('video.add', $video->creator);

                            //email and add notification if user are tagged
                            CUserHelper::parseTaggedUserNotification($message, $my, $activityData, array('type' => 'post-comment'));

                            // Trigger for onVideoCreate
                            //
                            $apps = CAppPlugins::getInstance();
                            $apps->loadApplications();
                            $params = array();
                            $params[] = $video;
                            $apps->triggerEvent('onVideoCreate', $params);

//                            $this->cacheClean(array(COMMUNITY_CACHE_TAG_VIDEOS, COMMUNITY_CACHE_TAG_FRONTPAGE, COMMUNITY_CACHE_TAG_FEATURED, COMMUNITY_CACHE_TAG_VIDEOS_CAT, COMMUNITY_CACHE_TAG_ACTIVITIES));

    //                        $objResponse->addScriptCall('__callback', JText::sprintf('COM_COMMUNITY_VIDEOS_UPLOAD_SUCCESS', $video->title));
                            $response['message'] = JText::sprintf('COM_COMMUNITY_VIDEOS_UPLOAD_SUCCESS', $video->title);
                            break;

                        case 'groups':
                            // attachment id
                            $fetch = $attachment['fetch'];
                            $cid = $fetch[0];
                            $privacy = 0; //$attachment['privacy'];

                            $video = JTable::getInstance('Video', 'CTable');
                            $video->load($cid);
                            $video->set('status', 'ready');
                            $video->set('groupid', $attachment['target']);
                            $video->set('permissions', $privacy);
                            $video->set('creator_type', VIDEO_GROUP_TYPE);
                            $video->set('title', $fetch[3]);
                            $video->set('description', $fetch[4]);
                            $video->set('category_id', $fetch[5]);

                            /* Save cords if exists */
                            if (isset($attachment['location'])) {
                                $video->set('location', $attachment['location'][0]);
                                $video->set('latitude', $attachment['location'][1]);
                                $video->set('longitude', $attachment['location'][2]);
                            };

                            $video->store();

                            //
                            $groupLib = new CGroups();
                            $group = JTable::getInstance('Group', 'CTable');
                            $group->load($attachment['target']);

                            // Add activity logging
                            $url = $video->getViewUri(false);

                            $act = new stdClass();
                            $act->cmd = 'videos.linking';
                            $act->actor = $my->id;
                            $act->target = ($attachment['target'] == $my->id) ? 0 : $attachment['target'];
                            $act->access = $privacy;

                            //filter empty message
                            $act->title = $message;
                            $act->app = 'videos';
                            $act->content = '';
                            $act->cid = $video->id;
                            $act->groupid = $video->groupid;
                            $act->group_access = $group->approvals;
                            $act->location = $video->location;

                            /* Save cords if exists */
                            if (isset($attachment['location'])) {
                                /* Save geo name */
                                $act->location = $attachment['location'][0];
                                $act->latitude = $attachment['location'][1];
                                $act->longitude = $attachment['location'][2];
                            };

                            $act->comment_id = $video->id;
                            $act->comment_type = 'videos';

                            $act->like_id = $video->id;
                            $act->like_type = 'videos';

                            $params = new CParameter('');
                            $params->set('video_url', $url);
                            $params->set('style', COMMUNITY_STREAM_STYLE); // set stream style
                            //Store mood in paramm
                            if (isset($attachment['mood']) && $attachment['mood'] != 'Mood') {
                                $params->set('mood', $attachment['mood']);
                            }

                            $activityData = CActivityStream::add($act, $params->toString());

                            // @rule: Add point when user adds a new video link
                            CUserPoints::assignPoint('video.add', $video->creator);

                            //add notifcation to all the members
                            $params = new CParameter('');
                            $params->set('message', $emailMessage);
                            $params->set('group', $group->name);
                            $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                            $params->set('url', CRoute::getExternalURL('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id, false));
                            //Get group member emails
                            $model = CFactory::getModel('Groups');
                            $members = $model->getMembers($attachment['target'], null, true, false, true);

                            $membersArray = array();
                            if (!is_null($members)) {
                                foreach ($members as $row) {
                                    if ($my->id != $row->id) {
                                        $membersArray[] = $row->id;
                                    }
                                }
                            }
                            $groupParams = new CParameter($group->params);

                            if($groupParams->get('wallnotification')) {
                                CNotificationLibrary::add('groups_wall_video', $my->id, $membersArray, JText::sprintf('COM_COMMUNITY_NEW_WALL_POST_NOTIFICATION_EMAIL_SUBJECT', $my->getDisplayName(), $group->name), '', 'groups.post', $params);
                            }

                            // Trigger for onVideoCreate
                            $apps = CAppPlugins::getInstance();
                            $apps->loadApplications();
                            $params = array();
                            $params[] = $video;
                            $apps->triggerEvent('onVideoCreate', $params);

//                            $this->cacheClean(array(COMMUNITY_CACHE_TAG_VIDEOS, COMMUNITY_CACHE_TAG_FRONTPAGE, COMMUNITY_CACHE_TAG_FEATURED, COMMUNITY_CACHE_TAG_VIDEOS_CAT, COMMUNITY_CACHE_TAG_ACTIVITIES));

    //                        $objResponse->addScriptCall('__callback', JText::sprintf('COM_COMMUNITY_VIDEOS_UPLOAD_SUCCESS', $video->title));
                            $response['message'] = JText::sprintf('COM_COMMUNITY_VIDEOS_UPLOAD_SUCCESS', $video->title);
                            // Reload the stream with new stream data
                            $streamHTML = $groupLib->getStreamHTML($group, array('showLatestActivityOnTop'=>true));

                            break;
                        case 'events':
                            //event videos
                            $fetch = $attachment['fetch'];
                            $cid = $fetch[0];
                            $privacy = 0; //$attachment['privacy'];

                            $video = JTable::getInstance('Video', 'CTable');
                            $video->load($cid);
                            $video->set('status', 'ready');
                            $video->set('eventid', $attachment['target']);
                            $video->set('permissions', $privacy);
                            $video->set('creator_type', VIDEO_EVENT_TYPE);
                            $video->set('title', $fetch[3]);
                            $video->set('description', $fetch[4]);
                            $video->set('category_id', $fetch[5]);

                            /* Save cords if exists */
                            if (isset($attachment['location'])) {
                                $video->set('location', $attachment['location'][0]);
                                $video->set('latitude', $attachment['location'][1]);
                                $video->set('longitude', $attachment['location'][2]);
                            };

                            $video->store();

                            //
                            $eventLib = new CEvents();
                            $event = JTable::getInstance('Event', 'CTable');
                            $event->load($attachment['target']);

                            $group = new stdClass();
                            if($event->type == 'group' && $event->contentid){
                                // check if this a group event, and follow the permission
                                $group = JTable::getInstance('Group', 'CTable');
                                $group->load($event->contentid);
                            }

                            // Add activity logging
                            $url = $video->getViewUri(false);

                            $act = new stdClass();
                            $act->cmd = 'videos.linking';
                            $act->actor = $my->id;
                            $act->target = ($attachment['target'] == $my->id) ? 0 : $attachment['target'];
                            $act->access = $privacy;

                            //filter empty message
                            $act->title = $message;
                            $act->app = 'videos';
                            $act->content = '';
                            $act->cid = $video->id;
                            $act->groupid = 0;
                            $act->group_access = isset($group->approvals) ? $group->approvals : 0; // if this is a group event
                            $act->location = $video->location;

                            /* Save cords if exists */
                            if (isset($attachment['location'])) {
                                /* Save geo name */
                                $act->location = $attachment['location'][0];
                                $act->latitude = $attachment['location'][1];
                                $act->longitude = $attachment['location'][2];
                            };

                            $act->eventid = $event->id;

                            $act->comment_id = $video->id;
                            $act->comment_type = 'videos';

                            $act->like_id = $video->id;
                            $act->like_type = 'videos';

                            $params = new CParameter('');
                            $params->set('video_url', $url);
                            $params->set('style', COMMUNITY_STREAM_STYLE); // set stream style
                            //Store mood in paramm
                            if (isset($attachment['mood']) && $attachment['mood'] != 'Mood') {
                                $params->set('mood', $attachment['mood']);
                            }

                            $activityData = CActivityStream::add($act, $params->toString());

                            // @rule: Add point when user adds a new video link
                            CUserPoints::assignPoint('video.add', $video->creator);

                            // Trigger for onVideoCreate
                            $apps = CAppPlugins::getInstance();
                            $apps->loadApplications();
                            $params = array();
                            $params[] = $video;
                            $apps->triggerEvent('onVideoCreate', $params);

//                            $this->cacheClean(array(COMMUNITY_CACHE_TAG_VIDEOS, COMMUNITY_CACHE_TAG_FRONTPAGE, COMMUNITY_CACHE_TAG_FEATURED, COMMUNITY_CACHE_TAG_VIDEOS_CAT, COMMUNITY_CACHE_TAG_ACTIVITIES));

    //                        $objResponse->addScriptCall('__callback', JText::sprintf('COM_COMMUNITY_VIDEOS_UPLOAD_SUCCESS', $video->title));
                            $response['message'] = JText::sprintf('COM_COMMUNITY_VIDEOS_UPLOAD_SUCCESS', $video->title);
                            // Reload the stream with new stream data
                            $streamHTML = $eventLib->getStreamHTML($event, array('showLatestActivityOnTop'=>true));
                            break;
                        default:
                            return;
                    }

                    break;

                case 'link':
                    break;
            }

            //no matter what kind of message it is, always filter the hashtag if there's any
            if(!empty($act->title)&& isset($activityData->id) && $activityData->id){
                //use model to check if this has a tag in it and insert into the table if possible
                $hashtags = CContentHelper::getHashTags($act->title);
                if(count($hashtags)){
                    //$hashTag
                    $hashtagModel = CFactory::getModel('hashtags');

                    foreach($hashtags as $tag){
                        $hashtagModel->addActivityHashtag($tag, $activityData->id);
                    }
                }
            }

            // Frontpage filter
            if ($streamFilter != false) {
                $streamFilter = json_decode($streamFilter);
                $filter = $streamFilter->filter;
                $value = $streamFilter->value;
                $extra = false;

                // Append added data to the list.
                if (isset($activityData) && $activityData->id) {
                    $model = CFactory::getModel('Activities');
                    $extra = $model->getActivity($activityData->id);
                }

                switch ($filter) {
                    case 'privacy':
                        if ($value == 'me-and-friends' && $my->id != 0) {
                            $streamHTML = CActivities::getActivitiesByFilter('active-user-and-friends', $my->id, 'frontpage', true, array(), $extra);
                        } else {
                            $streamHTML = CActivities::getActivitiesByFilter('all', $my->id, 'frontpage', true, array(), $extra);
                        }
                        break;

                    case 'apps':
                        $streamHTML = CActivities::getActivitiesByFilter('all', $my->id, 'frontpage', true, array('apps' => array($value)), $extra);
                        break;

                    case 'hashtag';
                        $streamHTML = CActivities::getActivitiesByFilter('all', $my->id, 'frontpage', true, array($filter => $value), $extra);
                        break;

                    default:
                        $defaultFilter = $config->get('frontpageactivitydefault');
                        if ($defaultFilter == 'friends' && $my->id != 0) {
                            $streamHTML = CActivities::getActivitiesByFilter('active-user-and-friends', $my->id, 'frontpage', true, array(), $extra);
                        } else {
                            $streamHTML = CActivities::getActivitiesByFilter('all', $my->id, 'frontpage', true, array(), $extra);
                        }
                        break;
                }
            }

            if (!isset($attachment['filter'])) {
                $attachment['filter'] = '';
                $filter = $config->get('frontpageactivitydefault');
                $filter = explode(':', $filter);

                $attachment['filter'] = (isset($filter[1])) ? $filter[1] : $filter[0];
            }

            if (empty($streamHTML)) {
                if (!isset($attachment['target']))
                    $attachment['target'] = '';
                if (!isset($attachment['element']))
                    $attachment['element'] = '';
                $streamHTML = CActivities::getActivitiesByFilter($attachment['filter'], $attachment['target'], $attachment['element'], true, array('show_featured'=>true,'showLatestActivityOnTop'=>true));
            }

    //        $objResponse->addAssign('activity-stream-container', 'innerHTML', $streamHTML);
            $response['status'] = true;
            $response['data'] = $activityData;

            // Log user engagement
            CEngagement::log($attachment['type'] . '.share', $my->id);

            return $response;
        }
    }