<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die('Restricted access');

// Include interface definition
//CFactory::load( 'models' , 'tags' );

class CTableDiscussion extends JTable
	implements CTaggable_Item
{

	var $id			= null;
	var $groupid	= null;
	var $creator 	= null;
	var $created 	= null;
	var $title 		= null;
	var $message 	= null;
	var $lock	 	= null;
	var $params		= null;

	/**
	 * Constructor
	 */
	public function __construct( &$db )
	{
		parent::__construct( '#__community_groups_discuss', 'id', $db );
	}

	public function check()
	{
		// Filter the discussion
		$config = CFactory::getConfig();
		//$clean = ('none' != $config->get('htmleditor'));

		$safeHtmlFilter	= CFactory::getInputFilter();
		$this->title	= $safeHtmlFilter->clean($this->title);

		$safeHtmlFilter	= CFactory::getInputFilter($config->getBool('allowhtml'));
                $this->message 	= $safeHtmlFilter->clean($this->message);

		return true;
	}

	public function store($updateNulls = false)
	{
		if (!$this->check()) {
			return false;
		}

		$result = parent::store();

		if($result)
		{
			$this->_updateGroupStats();
		}
		return $result;
	}

	/**
	 * Delete the discussion
	 * @param  [type] $oid [description]
	 * @return [type]
	 */
	public function delete($oid=null)
	{
		// Delete the stream related to the discussion replies
		CActivities::remove('groups.discussion.reply', $this->id);

		$result = parent::delete($oid);

		if($result)
		{
			$this->_updateGroupStats();
		}


		return $result;
	}

	private function _updateGroupStats()
	{
		//CFactory::load( 'models' , 'groups' );
		$group	= JTable::getInstance( 'Group' , 'CTable' );
		$group->load( $this->groupid );
		$group->updateStats();
		$group->store();
	}

	public function lock( $id=null, $lockStatus=false )
	{
		$db		= JFactory::getDBO();

		$obj		= new stdClass();
		$obj->id	= $id;
		$obj->lock	= $lockStatus;

		return $db->updateObject('#__community_groups_discuss',$obj,'id',false);

	}

	/**
	 * Return the title of the object
	 */
	public function tagGetTitle()
	{
		return $this->title;
	}

	/**
	 * Return the HTML summary of the object
	 */
    public function tagGetHtml()
	{
		return '';
	}

	/**
	 * Return the internal link of the object
	 *
	 */
	public function tagGetLink()
	{
		return $this->getViewURI();
	}

	/**
	 * Return true if the user is allow to modify the tag
	 *
	 */
	public function tagAllow($userid)
	{
		// @todo: neec to check with group admin
		return true;
	}

	public function getParams()
	{
		$params	= new CParameter( $this->params );

		return $params;
	}

	public function getAvatar() {
        // Get the avatar path. Some maintance/cleaning work: We no longer store
        // the default avatar in db. If the default avatar is found, we reset it
        // to empty. In next release, we'll rewrite this portion accordingly.
        // We allow the default avatar to be template specific.

        $discussionsModel = CFactory::getModel('Discussion');
        
        $avatar = CUrlHelper::avatarURI($this->avatar, 'discussion_avatar.png');

        return $avatar;
    }

      /**
     * Get large avatar use for cropping
     * @return string
     */
    public function getLargeAvatar() {
        $config = CFactory::getConfig();
        $mainframe = JFactory::getApplication();

        /* Some profile type avatar are stored directly to the avatar with this format avatar_[id]
         * So, if we have this kind of format, we will take this as priority
         * Used by JSPT
         */
        
        if(count(explode('_',$this->avatar)) > 1){
            $largeAvatar = $this->avatar;
        }else{
            $largeAvatar = $config->getString('imagefolder') . '/avatar/discussion/discussion-' . JFile::getName($this->avatar);
        }

       
        if (JFile::exists($mainframe->getCfg('dataroot') . '/' . $largeAvatar)) {
            return $mainframe->getCfg('wwwrootfile') . '/' . $largeAvatar;
        } else {
            return $this->getAvatar();
        }
    }

    public function removeAvatar() {
        if (JFile::exists($this->avatar) && !JString::stristr($this->avatar, 'avatar_')) {
            JFile::delete($this->avatar);
        }

        if (JFile::exists($this->thumb) && !JString::stristr($this->thumb, 'avatar_')) {
            JFile::delete($this->thumb);
        }

        $this->avatar = '';
        $this->thumb = '';
        $this->store();
    }


    public function setImage($path, $type = 'thumb') {
        CError::assert($path, '', '!empty', __FILE__, __LINE__);

        $db = $this->getDBO();

        // Fix the back quotes
        $path = JString::str_ireplace('\\', '/', $path);
        $type = JString::strtolower($type);

        // Test if the record exists.
        $oldFile = $this->$type;

        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }

        if ($oldFile) {
            // File exists, try to remove old files first.
            $oldFile = JString::str_ireplace('/', '/', $oldFile);

            // If old file is default_thumb or default, we should not remove it.
            if (!JString::stristr($oldFile, 'user.png') && !JString::stristr($oldFile, 'user_thumb.png') && !JString::stristr($oldFile, 'avatar_')) {
                jimport('joomla.filesystem.file');
                JFile::delete($oldFile);
            }
        }
        $this->$type = $path;
        $this->store();

        // Trigger profile avatar update event.
        if ($type == 'avatar') {
            $appsLib = CAppPlugins::getInstance();
            $appsLib->loadApplications();
            $args = array();
            $args[] = $this->userid;   // userid
            $args[] = $oldFile; // old path
            $args[] = $path;  // new path

            $appsLib->triggerEvent('onProfileAvatarUpdate', $args);
        }
    }

}