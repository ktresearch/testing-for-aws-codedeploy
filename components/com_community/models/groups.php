<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */

defined('_JEXEC') or die('Restricted access');

require_once ( JPATH_ROOT .'/components/com_community/models/models.php');

class CommunityModelGroups extends JCCModel
    implements CLimitsInterface, CNotificationsInterface
{
    /**
     * Configuration data
     *
     * @var object	JPagination object
     **/
    var $_pagination	= '';

    /**f
     * Configuration data
     *
     * @var object	JPagination object
     **/
    var $total			= '';

    /**
     * member count data
     *
     * @var int
     **/
    var $membersCount	= array();


    /** variable for pagination limits */

    var $paginationlimit = 15;

    /**
     * Constructor
     */
    public function CommunityModelGroups()
    {
        parent::JCCModel();

        $mainframe	= JFactory::    getApplication();
        $jinput 	= $mainframe->input;
        $config = CFactory::getConfig();

        // Get pagination request variables
        $limit		= ($config->get('pagination') == 0) ? 5 : $config->get('pagination');
        $limitstart = $jinput->request->get('limitstart', 0); //JRequest::getVar('limitstart', 0, 'REQUEST');

        if(empty($limitstart))
        {
            $limitstart = $jinput->get('limitstart', 0, 'uint');
        }

        // In case limit has been changed, adjust it
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);
    }

    /**
     * Method to get a pagination object for the events
     *
     * @access public
     * @return integer
     */
    public function getPagination()
    {
        return $this->_pagination;
    }

    /**
     * Deprecated since 1.8, use $groupd->updateStats()->store();
     */
    public function substractMembersCount( $groupId )
    {
        $this->addWallCount($groupId);
    }

    /**
     * Deprecated since 1.8, use $groupd->updateStats()->store();
     */
    public function addDiscussCount( $groupId )
    {
        $this->addWallCount($groupId);
    }

    /**
     * Deprecated since 1.8, use $groupd->updateStats()->store();
     */
    public function substractDiscussCount( $groupId )
    {
        $this->addWallCount($groupId);
    }

    /**
     * Retrieves the most active group throughout the site.
     * @param   none
     *
     * @return  CTableGroup The most active group table object.
     **/
    public function getMostActiveGroup()
    {
        $db		= $this->getDBO();

        $query	= 'SELECT '.$db->quoteName('cid').' FROM '.$db->quoteName('#__community_activities')
            . ' WHERE '.$db->quoteName('app').' LIKE ' . $db->Quote( 'groups%' ).''
            . ' GROUP BY '.$db->quoteName('cid')
            . ' ORDER BY COUNT(1) DESC '
            . ' LIMIT 1';
        $db->setQuery( $query );

        $id		= $db->loadResult();

        $group	= JTable::getInstance( 'Group' , 'CTable' );
        $group->load( $id );

        // return a null object if there is no group yet
        if( $id )
            return $group;
        else
            return null;
    }

    public function getGroupInvites( $userId , $sorting = null )
    {
        $db			= $this->getDBO();
        $extraSQL	= ' AND a.userid=' . $db->Quote($userId);
        $orderBy	= '';
        $limit			= $this->getState('limit');
        $limitstart 	= $this->getState('limitstart');
        $total			= 0;


        switch($sorting)
        {

            case 'mostmembers':
                // Get the groups that this user is assigned to
                $query		= 'SELECT a.'.$db->quoteName('groupid').' FROM ' . $db->quoteName('#__community_groups_invite') . ' AS a '
                    . ' LEFT JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b '
                    . ' ON a.'.$db->quoteName('groupid').'=b.'.$db->quoteName('groupid')
                    . ' WHERE b.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
                    . $extraSQL;

                $db->setQuery( $query );
                $groupsid		= $db->loadColumn();

                if($db->getErrorNum())
                {
                    JError::raiseError( 500, $db->stderr());
                }

                if( $groupsid )
                {
                    $groupsid		= implode( ',' , $groupsid );

                    $query			= 'SELECT a.* '
                        . ' FROM ' . $db->quoteName('#__community_groups_invite') . ' AS a '
                        . ' INNER JOIN '.$db->quoteName('#__community_groups').' AS b '
                        . ' ON a.'.$db->quoteName('groupid').'=b.'.$db->quoteName('id')
                        . ' WHERE a.'.$db->quoteName('groupid').' IN (' . $groupsid . ') '
                        . ' ORDER BY b.'.$db->quoteName('membercount').' DESC '
                        . ' LIMIT ' . $limitstart . ',' . $limit;
                }
                break;
            case 'mostdiscussed':
                if( empty($orderBy) )
                    $orderBy	= ' ORDER BY b.'.$db->quoteName('discusscount').' DESC ';
            case 'mostwall':
                if( empty($orderBy) )
                    $orderBy	= ' ORDER BY b.'.$db->quoteName('wallcount').' DESC ';
            case 'alphabetical':
                if( empty($orderBy) )
                    $orderBy	= 'ORDER BY b.'.$db->quoteName('name').' ASC ';
            case 'mostactive':
                //@todo: Add sql queries for most active group

            default:
                if( empty($orderBy) )
                    $orderBy	= ' ORDER BY b.'.$db->quoteName('created').' DESC ';

                $query	= 'SELECT distinct a.* FROM '
                    . $db->quoteName('#__community_groups_invite') . ' AS a '
                    . ' INNER JOIN ' . $db->quoteName( '#__community_groups' ) . ' AS b ON a.'.$db->quoteName('groupid').'=b.'.$db->quoteName('id')
                    . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS c ON a.'.$db->quoteName('groupid').'=c.'.$db->quoteName('groupid')
                    . ' AND c.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
                    . ' AND b.'.$db->quoteName('published').'=' . $db->Quote( '1' ) . ' '
                    . $extraSQL
                    . $orderBy
                    . 'LIMIT ' . $limitstart . ',' . $limit;
                break;
        }
        $db->setQuery( $query );
        $result	= $db->loadObjectList();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        $query	= 'SELECT COUNT(distinct b.'.$db->quoteName('id').') FROM ' . $db->quoteName('#__community_groups_invite') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName( '#__community_groups' ) . ' AS b '
            . ' ON a.'.$db->quoteName('groupid').'=b.'.$db->quoteName('id')
            . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS c '
            . ' ON a.'.$db->quoteName('groupid').'=c.'.$db->quoteName('groupid')
            . ' WHERE b.'.$db->quoteName('published').'=' . $db->Quote( '1' )
            . ' AND c.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
            . $extraSQL;

        $db->setQuery( $query );
        $total	= $db->loadResult();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        if( empty($this->_pagination) )
        {
            jimport('joomla.html.pagination');

            $this->_pagination	= new JPagination( $total , $limitstart , $limit );
        }

        return $result;
    }

    /**
     * Return an array of ids the user belong to
     * @param type $userid
     * @return type
     *
     */
    public function getGroupIds($userId)
    {
        $db		= $this->getDBO();
        $query		= 'SELECT DISTINCT a.'.$db->quoteName('id').' FROM ' . $db->quoteName('#__community_groups') . ' AS a '
            . ' LEFT JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b '
            . ' ON a.'.$db->quoteName('id').'=b.'.$db->quoteName('groupid')
            . ' WHERE b.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
            . ' AND b.memberid=' . $db->Quote($userId);

        $db->setQuery( $query );
        $groupsid		= $db->loadColumn();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        return $groupsid;

    }

    /**
     * Get all the featured groups
     * @return Array of featured group ids
     */
    public function getFeaturedGroups(){
        $db		= $this->getDBO();

        $query	= 'SELECT cid FROM '. $db->quoteName('#__community_featured')
            . ' WHERE '. $db->quoteName('type').'=' . $db->Quote( 'groups' );
        $db->setQuery($query);
        $results = $db->loadColumn();
        return $results;
    }

    /**
     * Returns an object of groups which the user has registered.
     *
     * @access	public
     * @param	string 	User's id.
     * @returns array  An objects of custom fields.
     * @todo: re-order with most active group stays on top
     */
    public function getGroups( $userId = null , $sorting = null , $useLimit = true, $limitstart = null, $limit = null )
    {
        $db		= $this->getDBO();

        $extraSQL	= '';


        if( !is_null($userId) )
        {
            $extraSQL	= ' AND b.memberid=' . $db->Quote($userId);
        }

        //special case for sorting by featured
        if($sorting == 'featured'){
            $featuredGroups = $this->getFeaturedGroups();
            if(count($featuredGroups) > 0 ){
                $featuredGroups = implode(',', $featuredGroups);
                $extraSQL .= ' AND a.id IN ('.$featuredGroups.') ';
            }
        }

        $orderBy	= '';

        $limitSQL = '';
        $total		= 0;
        $limit		= ($limit) ? $limit : $this->getState('limit');
        $limitstart = ($limitstart) ? $limitstart : $this->getState('limitstart');
        if($useLimit){
            $limitSQL	= ' LIMIT ' . $limitstart . ',' . $limit ;
        }

        switch($sorting)
        {
            case 'mostmembers':
                // Get the groups that this user is assigned to
                $query		= 'SELECT a.'.$db->quoteName('id').' FROM ' . $db->quoteName('#__community_groups') . ' AS a '
                    . ' LEFT JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b '
                    . ' ON a.'.$db->quoteName('id').'=b.'.$db->quoteName('groupid')
                    . ' WHERE b.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
                    . $extraSQL;

                $db->setQuery( $query );
                $groupsid		= $db->loadColumn();

                if($db->getErrorNum())
                {
                    JError::raiseError( 500, $db->stderr());
                }

                if( $groupsid )
                {
                    $groupsid		= implode( ',' , $groupsid );

                    $query			= 'SELECT a.* '
                        . 'FROM ' . $db->quoteName('#__community_groups') . ' AS a '
                        . ' WHERE a.'.$db->quoteName('published').'=' . $db->Quote( '1' )
                        . ' AND a.'.$db->quoteName('id').' IN (' . $groupsid . ') '
                        . ' ORDER BY a.'.$db->quoteName('membercount').' DESC '
                        . $limitSQL;
                }
                break;
            case 'mostactive':
                $query	= 'SELECT *, (a.' . $db->quoteName('discusscount') . ' + a.' . $db->quoteName('wallcount') . ' ) AS count FROM ' . $db->quoteName('#__community_groups') . ' as a'
                    . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b ON a.' . $db->quoteName('id') . '= b.' . $db->quoteName('groupid')
                    . ' WHERE a.' . $db->quoteName('published') . ' = ' . $db->Quote('1')
                    . $extraSQL
                    . ' GROUP BY a.' . $db->quoteName('id')
                    . ' ORDER BY count DESC '
                    . $limitSQL;
                break;
            case 'mostdiscussed':
                if( empty($orderBy) )
                    $orderBy	= ' ORDER BY a.'.$db->quoteName('discusscount').' DESC ';
            case 'mostwall':
                if( empty($orderBy) )
                    $orderBy	= ' ORDER BY a.'.$db->quoteName('wallcount').' DESC ';
            case 'alphabetical':
                if( empty($orderBy) )
                    $orderBy	= 'ORDER BY a.'.$db->quoteName('name').' ASC ';
            default:
                if( empty($orderBy) )
                    $orderBy	= ' ORDER BY a.created DESC ';

                $query	= 'SELECT a.* FROM '
                    . $db->quoteName('#__community_groups') . ' AS a '
                    . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b '
                    . ' ON a.'.$db->quoteName('id').'=b.'.$db->quoteName('groupid')
                    . ' AND b.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
                    . ' AND a.'.$db->quoteName('published').'=' . $db->Quote( '1' ) . '
						'
                    . $extraSQL
                    . ' where a.'.$db->quoteName('categoryid').'!=' . $db->Quote( '6' ).'OR SUBSTRING_INDEX( SUBSTRING_INDEX( a.'.$db->quoteName('params').',  \'}\', 1 ) ,  \'course_id":\', -1 ) IN (select courseid from '
                    . $db->quoteName('#__course_shareto_group') .' WHERE ' . $db->quoteName('status_unpulished') . ' = '. $db->quote(0) . ') or SUBSTRING_INDEX( SUBSTRING_INDEX( a.'.$db->quoteName('params').',  \'}\', 1 ) ,  \'course_id":\', -1 ) IN (select product_code from '
                    . $db->quoteName('#__hikashop_product') . ' WHERE '.$db->quoteName('product_published').' = '.$db->quote(1).')'
                    . $orderBy
                    . $limitSQL;
                break;
        }

        $db->setQuery( $query );

        $result	= $db->loadObjectList();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        $query	= 'SELECT COUNT(*) FROM ' . $db->quoteName('#__community_groups') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b '
            . ' on a.'.$db->quoteName('id').'=b.'.$db->quoteName('groupid')
            . ' AND a.'.$db->quoteName('published').'=' . $db->Quote( '1' ) . ' '
            . ' AND b.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
            . $extraSQL
            . ' where a.'.$db->quoteName('categoryid').'!=' . $db->Quote( '6' ).'OR SUBSTRING_INDEX( SUBSTRING_INDEX( a.'.$db->quoteName('params').',  \'}\', 1 ) ,  \'course_id":\', -1 ) IN (select courseid from '
            . $db->quoteName('#__course_shareto_group') .' WHERE ' . $db->quoteName('status_unpulished') . ' = '. $db->quote(0) . ') or SUBSTRING_INDEX( SUBSTRING_INDEX( a.'.$db->quoteName('params').',  \'}\', 1 ) ,  \'course_id":\', -1 ) IN (select product_code from '
            . $db->quoteName('#__hikashop_product') . ' WHERE '.$db->quoteName('product_published').' = '.$db->quote(1).')';

        $db->setQuery( $query );
        $total	= $db->loadResult();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        if( empty($this->_pagination) )
        {
            jimport('joomla.html.pagination');

            $this->_pagination	= new JPagination( $total , $limitstart , $limit );
        }

        return $result;
    }

    /**
     * Returns an object of groups which the user has registered.
     *
     * @access	public
     * @param	string 	User's id.
     * @returns array  An objects of custom fields.
     * @todo: re-order with most active group stays on top
     */
    /***
    public function getCircles_( $userId = null, $withlimit, $limitstart, $limit, $categoryid)
    {

    $db		= $this->getDBO();
    $extraSQL	= '';
    $orderBy	= '';
    $limitSQL = '';
    $total		= 0;

    if ($categoryid == 6){
    $extraSQL	= 'AND ' . $db->quoteName('coursepublish'). '=' . $db->Quote( '1' ) ;
    }

    if($withlimit){
    $limitSQL	= ' LIMIT ' . $limitstart . ',' . $limit ;
    }

    $orderBy	= ' ORDER BY created DESC ';

    $query	= 'SELECT * FROM '
    . $db->quoteName('#__community_groups')
    . ' WHERE ' . $db->quoteName('published').'=' . $db->Quote( '1' ) . ' '
    . $extraSQL
    . $orderBy
    . $limitSQL;

    $db->setQuery( $query );

    $result	= $db->loadObjectList();

    if($db->getErrorNum())
    {
    JError::raiseError( 500, $db->stderr());
    }

    if( empty($this->_pagination) )
    {
    jimport('joomla.html.pagination');

    $this->_pagination	= new JPagination( $total , $limitstart , $limit );
    }

    return $result;
    }

     ***/

    // list circl course

    public function getCirclescourse( $userId = null, $withlimit, $limitstart, $limit, $categoryid, $listOwner = false)
    {

        $db		= $this->getDBO();

        $extraSQL	= '';


        if( !is_null($userId) )
        {
            if ($listOwner && $listOwner != '') {
                if ($listOwner == 'owner') {
                    $extraSQL	= ' AND b.memberid=' . $db->Quote($userId) . ' AND a.ownerid=' . $db->Quote($userId) . ' AND a.categoryid=' . $db->Quote($categoryid);
                } else if ($listOwner == 'member') {
                    $extraSQL	= ' AND b.memberid=' . $db->Quote($userId) . ' AND a.ownerid != ' . $db->Quote($userId) . ' AND a.categoryid=' . $db->Quote($categoryid);
                }
            } else {
                $extraSQL	= ' AND b.memberid=' . $db->Quote($userId) . ' AND a.categoryid=' . $db->Quote($categoryid);
            }
            //Temporarily remove the filter for learning circles
            /*
            if ($categoryid == 6){
                $extraSQL	= $extraSQL . ' AND a.coursepublish' . '=' . $db->Quote( '1' ) ;
        }
            */
        }

        $orderBy	= '';

        $limitSQL = '';
        $total		= 0;
        if($withlimit){
            $limitSQL	= ' LIMIT ' . $limitstart . ',' . $limit ;
        }

        $orderBy	= ' ORDER BY a.created DESC ';

        $query	= 'SELECT a.* FROM '
            . $db->quoteName('#__community_groups') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b '
            . ' ON a.'.$db->quoteName('id').'=b.'.$db->quoteName('groupid')
            . ' AND b.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
            . ' AND a.'.$db->quoteName('published').'=' . $db->Quote( '1' ) . $extraSQL. ' 
				where SUBSTRING_INDEX( SUBSTRING_INDEX( a.'.$db->quoteName('params').',  \'}\', 1 ) ,  \'course_id":\', -1 ) IN (select courseid from '
            . $db->quoteName('#__course_shareto_group') . ' WHERE ' . $db->quoteName('status_unpulished') . ' = '. $db->quote(0) . ') or SUBSTRING_INDEX( SUBSTRING_INDEX( a.'.$db->quoteName('params').',  \'}\', 1 ) ,  \'course_id":\', -1 ) IN (select product_code from '
            . $db->quoteName('#__hikashop_product') . ' WHERE '.$db->quoteName('product_published').' = '.$db->quote(1).')'

            . $orderBy
            . $limitSQL;

        $db->setQuery( $query );
        $result	= $db->loadObjectList();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        if( empty($this->_pagination) )
        {
            jimport('joomla.html.pagination');

            $this->_pagination	= new JPagination( $total , $limitstart , $limit );
        }

        return $result;

    }
    public function countgetCirclescourse ( $userId, $circleid, $listOwner = false, $checkLP = false )
    {

        $extraSQL	= '';
        // guest obviously has no group

        if($userId == 0)
        {
            return 0;
        }

        $db		= $this->getDBO();

        //Temporarily remove the filter for learning circles
        /*
        if ($circleid == 6){
            $extraSQL = ' AND b.'.$db->quoteName('coursepublish'). '=' . $db->Quote( '1' ) ;
        }
        */

        if ( $listOwner && $listOwner != '') {
            if ($listOwner == 'owner') {
                $extraSQL	= 'AND b.ownerid=' . $db->Quote($userId) . ' ';
            } else {
                $extraSQL	= 'AND a.memberid=' . $db->Quote($userId) . ' AND b.ownerid != ' . $db->Quote($userId) . ' ';
            }
        } else {
            $extraSQL	= 'AND '. $db->quoteName( 'memberid' ) . '=' . $db->Quote( $userId ) . ' ';
        }

        if ($checkLP) {
            $extraSQL	.= ' AND b.moodlecategoryid > 0 AND b.ownerid = '. $db->Quote( $userId ).' ';
        }

        $query	= 'SELECT COUNT(*) FROM '
            . $db->quoteName('#__community_groups') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b '
            . ' ON a.'.$db->quoteName('id').'=b.'.$db->quoteName('groupid')
            . ' AND b.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
            .' AND a.'.$db->quoteName('categoryid').'=' . $db->Quote( '6' )
            . ' AND a.'.$db->quoteName('published').'=' . $db->Quote( '1' ) . $extraSQL. ' 
				where SUBSTRING_INDEX( SUBSTRING_INDEX( a.'.$db->quoteName('params').',  \'}\', 1 ) ,  \'course_id":\', -1 ) IN (select courseid from '
            . $db->quoteName('#__course_shareto_group') . ' WHERE ' . $db->quoteName('status_unpulished') . ' = '. $db->quote(0) . ') or SUBSTRING_INDEX( SUBSTRING_INDEX( a.'.$db->quoteName('params').',  \'}\', 1 ) ,  \'course_id":\', -1 ) IN (select product_code from '
            . $db->quoteName('#__hikashop_product') .' WHERE '.$db->quoteName('product_published').' = '.$db->quote(1).')';


        $db->setQuery( $query );
        $count	= $db->loadResult();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        return $count;

    }
    //

	public function getCircles( $userId = null, $withlimit, $limitstart, $limit, $categoryid, $listOwner = false,$report=false)
    {

        $db		= $this->getDBO();

        $extraSQL	= '';


        if( !is_null($userId) )
        {
            if ($listOwner && $listOwner != '') {
                if ($listOwner == 'owner' || $listOwner == 'archived') {
                    $extraSQL	= ' AND b.memberid=' . $db->Quote($userId) . ' AND a.ownerid=' . $db->Quote($userId) . ' AND a.categoryid=' . $db->Quote($categoryid);
                } else if ($listOwner == 'member') {
                    $extraSQL	= ' AND b.memberid=' . $db->Quote($userId) . ' AND a.ownerid != ' . $db->Quote($userId) . ' AND a.categoryid=' . $db->Quote($categoryid);
                }

            } else {
                $extraSQL	= ' AND b.memberid=' . $db->Quote($userId) . ' AND a.categoryid=' . $db->Quote($categoryid);
            }
            if(!$report){
                if($listOwner == 'archived'){
                    $published = ' AND a.published = 0' ;
                }
                else  $published = ' AND a.published = 1' ;
            }
            //Temporarily remove the filter for learning circles
            /*
            if ($categoryid == 6){
                $extraSQL	= $extraSQL . ' AND a.coursepublish' . '=' . $db->Quote( '1' ) ;
        }
            */
        }

        $orderBy	= '';

        $limitSQL = '';
        $total		= 0;
        if($withlimit){
            $limitSQL	= ' LIMIT ' . $limitstart . ',' . $limit ;
        }

        $orderBy	= ' ORDER BY a.created DESC ';

        $query	= 'SELECT a.* FROM '
            . $db->quoteName('#__community_groups') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b '
            . ' ON a.'.$db->quoteName('id').'=b.'.$db->quoteName('groupid')
            . ' AND b.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
            .$published. ' '
            . $extraSQL
            . $orderBy
            . $limitSQL;

        $db->setQuery( $query );
        $result	= $db->loadObjectList();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        if( empty($this->_pagination) )
        {
            jimport('joomla.html.pagination');

            $this->_pagination	= new JPagination( $total , $limitstart , $limit );
        }

        return $result;
    }


    public function getAllCircles($withlimit, $limitstart, $limit, $categoryid)
    {
        $db		= $this->getDBO();

        $limitSQL = '';

        if($withlimit){
            $limitSQL	= ' LIMIT ' . $limitstart . ',' . $limit ;
        }

        $query	= 'SELECT * FROM '
            . $db->quoteName('#__community_groups')
            . ' WHERE ' . $db->quoteName('published').'=' . $db->Quote( '1' )
            . ' AND ' . $db->quoteName('unlisted').'=' . $db->Quote( '0' )
            . ' AND ' . $db->quoteName('categoryid').'=' . $db->Quote($categoryid)
            . ' ORDER BY created DESC '
            . $limitSQL;

        $db->setQuery( $query );

        $result	= $db->loadObjectList();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        return $result;
    }


    /**
     * Return the number of cicles count for specific user
     **/
    public function getCirclesCount( $userId, $circleid, $listOwner = false,$isSubCircle = false,$report = false )
    {
        $extraSQL	= '';
        // guest obviously has no group

        if($userId == 0)
        {
            return 0;
        }

        $db		= $this->getDBO();

        //Temporarily remove the filter for learning circles
        /*
        if ($circleid == 6){
            $extraSQL = ' AND b.'.$db->quoteName('coursepublish'). '=' . $db->Quote( '1' ) ;
        }
        */

        if ( $listOwner && $listOwner != '') {
            if ($listOwner == 'owner' || $listOwner == 'archive') {
                $extraSQL	= 'AND b.ownerid=' . $db->Quote($userId) . ' ';
            } else {
                $extraSQL	= 'AND a.memberid=' . $db->Quote($userId) . ' AND b.ownerid != ' . $db->Quote($userId) . ' ';
            }
        } else {
            $extraSQL	= 'AND '. $db->quoteName( 'memberid' ) . '=' . $db->Quote( $userId ) . ' ';
        }
        if ($isSubCircle) {
            $extraSQL .= 'AND (b.isBLN = '.$db->Quote(0).' OR b.isBLN IS NULL) ';
        }
        
        if(!$report) {
            if($listOwner == 'archive'){
                $published = 'AND b.'.$db->quoteName( 'published' ). '= 0' ;
            }
            else  $published = 'AND b.'.$db->quoteName( 'published' ). '= 1' ;
        }
        
        $checkLP = false;
        if ($checkLP) {
            $extraSQL	.= ' AND b.moodlecategoryid > 0 AND b.ownerid = '. $db->Quote( $userId ).' ';
        }

        $query	= 'SELECT COUNT(*) FROM '. $db->quoteName( '#__community_groups_members' ) . ' AS a '
            . 'INNER JOIN ' . $db->quoteName('#__community_groups') . ' AS b '
            . 'WHERE a.'.$db->quoteName('groupid').'= b.'.$db->quoteName('id'). ' '
            . 'AND '. $db->quoteName( 'memberid' ) . '=' . $db->Quote( $userId ) . ' '
            . 'AND '. $db->quoteName( 'approved' ) . '=' . $db->Quote( '1' ) . ' '
            . $published . ' '
            . 'AND b.'.$db->quoteName( 'categoryid' ). '='. $db->Quote( $circleid ) . ' '
            . $extraSQL;

        $db->setQuery( $query );
        $count	= $db->loadResult();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        return $count;
    }

    /**
     * Return the number of cicles count for specific user
     **/
    public function getAllCount($circleid)
    {

        $db		= $this->getDBO();

        $query	= 'SELECT count(*) FROM ' . $db->quoteName('#__community_groups')
            . ' WHERE ' . $db->quoteName('published').'=' . $db->Quote('1')
            . ' AND ' . $db->quoteName('unlisted').'=' . $db->Quote('0')
            . ' AND ' . $db->quoteName('categoryid').'=' . $db->Quote($circleid);

        $db->setQuery( $query );
        $count	= $db->loadResult();

        return $count;
    }

    /**
     * Return the number of groups count for specific user
     **/
    public function getGroupsCount( $userId )
    {
        // guest obviously has no group
        if($userId == 0)
        {
            return 0;
        }

        $db		= $this->getDBO();

        $query	= 'SELECT COUNT(*) FROM '
            . $db->quoteName('#__community_groups') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b '
            . ' ON a.'.$db->quoteName('id').'= b.'.$db->quoteName('groupid')
            . ' AND b.'. $db->quoteName( 'memberid' ) . '=' . $db->Quote( $userId )
            . ' AND b.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
            . ' AND a.'.$db->quoteName('published').'=' . $db->Quote( '1' ) ;

        $db->setQuery( $query );
        $count	= $db->loadResult();

        return $count;
    }

    public function getTotalToday( $userId )
    {
        $date	= JFactory::getDate();
        $db		= JFactory::getDBO();

        $query	= 'SELECT COUNT(*) FROM ' . $db->quoteName( '#__community_groups' ) . ' AS a '
            . ' WHERE a.'.$db->quoteName('ownerid').'=' . $db->Quote( $userId )
            . ' AND TO_DAYS(' . $db->Quote( $date->toSql( true ) ) . ') - TO_DAYS( DATE_ADD( a.'.$db->quoteName('created').' , INTERVAL ' . $date->getOffset() . ' HOUR ) ) = '.$db->Quote(0);
        $db->setQuery( $query );

        $count		= $db->loadResult();

        return $count;
    }
    /**
     * Return the number of groups cretion count for specific user
     **/
    public function getGroupsCreationCount( $userId )
    {
        // guest obviously has no group
        if($userId == 0)
            return 0;

        $db		= $this->getDBO();

        $query	= 'SELECT COUNT(*) FROM '
            . $db->quoteName( '#__community_groups' ) . ' '
            . 'WHERE ' . $db->quoteName( 'ownerid' ) . '=' . $db->Quote( $userId );
        $db->setQuery( $query );

        $count	= $db->loadResult();

        return $count;
    }

    /**
     * Returns the count of the members of a specific group
     *
     * @access	public
     * @param	string 	Group's id.
     * @return	int	Count of members
     */
    public function getMembersCount( $id, $loadAdmin = false )
    {
        $db	= $this->getDBO();

        if( !isset($this->membersCount[$id] ) )
        {
            $query	= 'SELECT COUNT(*) FROM ' . $db->quoteName('#__community_groups_members') . ' AS a '
                . ' INNER JOIN ' . $db->quoteName('#__users') . ' AS b '
                . ' ON b.'.$db->quoteName('id').'=a.'.$db->quoteName('memberid')
                . ' WHERE a.'.$db->quoteName('groupid').'=' . $db->Quote( $id ) . ' '
                . ' AND b.'.$db->quoteName('block').'=' . $db->Quote( '0' ) . ' '
                . ' AND a.'.$db->quoteName('permissions').' !=' . $db->quote( COMMUNITY_GROUP_BANNED ) . ' '
                . ' AND a.'.$db->quoteName('memberid').' != 0 '
                . ' AND a.' . $db->quoteName( 'approved' ) . '=' . $db->Quote( '1' );

            if( !$loadAdmin ) {
                $query	.= ' AND a.'.$db->quoteName('permissions').'=' . $db->Quote( '0' );
            }

            $db->setQuery( $query );
            $this->membersCount[$id]	= $db->loadResult();

            if($db->getErrorNum())
            {
                JError::raiseError( 500, $db->stderr());
            }
        }
        return $this->membersCount[$id];
    }

    /**
     * Return the count of the user's friend of a specific group
     */
    public function getFriendsCount( $userid, $groupid )
    {
        $db	= $this->getDBO();

        $query	=   'SELECT COUNT(DISTINCT(a.'.$db->quoteName('connect_to').')) AS id  FROM ' . $db->quoteName('#__community_connection') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName( '#__users' ) . ' AS b '
            . ' INNER JOIN ' . $db->quoteName( '#__community_groups_members' ) . ' AS c '
            . ' ON a.'.$db->quoteName('connect_from').'=' . $db->Quote( $userid )
            . ' AND a.'.$db->quoteName('connect_to').'=b.'.$db->quoteName('id')
            . ' AND c.'.$db->quoteName('groupid').'=' . $db->Quote( $groupid )
            . ' AND a.'.$db->quoteName('connect_to').'=c.'.$db->quoteName('memberid')
            . ' AND a.'.$db->quoteName('status').'=' . $db->Quote( '1' )
            . ' AND c.'.$db->quoteName('approved').'=' . $db->Quote( '1' );

        $db->setQuery( $query );

        $total = $db->loadResult();

        return $total;
    }

    public function getInviteFriendsList($userid, $groupid){
        $db	= $this->getDBO();

        $query	=   'SELECT DISTINCT(a.'.$db->quoteName('connect_to').') AS id  FROM ' . $db->quoteName('#__community_connection') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName( '#__users' ) . ' AS b '
            . ' ON a.'.$db->quoteName('connect_from').'=' . $db->Quote( $userid )
            . ' AND a.'.$db->quoteName('connect_to').'=b.'.$db->quoteName('id')
            . ' AND a.'.$db->quoteName('status').'=' . $db->Quote( '1' )
            . ' AND b.'.$db->quoteName('block').'=' .$db->Quote('0')
            . ' WHERE NOT EXISTS ( SELECT d.'.$db->quoteName('blocked_userid') . ' as id'
            . ' FROM '.$db->quoteName('#__community_blocklist') . ' AS d  '
            . ' WHERE d.'.$db->quoteName('userid').' = '.$db->Quote($userid)
            . ' AND d.'.$db->quoteName('blocked_userid').' = a.'.$db->quoteName('connect_to').')'
            . ' AND NOT EXISTS (SELECT e.'.$db->quoteName('memberid') . ' as id'
            . ' FROM '.$db->quoteName('#__community_groups_members') . ' AS e  '
            . ' WHERE e.'.$db->quoteName('groupid').' = '.$db->Quote($groupid)
            . ' AND e.'.$db->quoteName('memberid').' = a.'.$db->quoteName('connect_to')
            .')' ;

        $db->setQuery( $query );

        $friends = $db->loadColumn();

        return $friends;
    }


    public function getInviteListByName($namePrefix ,$userid, $cid, $limitstart = 0, $limit = 8){
        $db	= $this->getDBO();

        $andName = '';
        $config = CFactory::getConfig();
        $nameField = $config->getString('displayname');
        if(!empty($namePrefix)){
            $andName	= ' AND b.' . $db->quoteName( $nameField ) . ' LIKE ' . $db->Quote( '%'.$namePrefix.'%' ) ;
        }
        $query	=   'SELECT DISTINCT(a.'.$db->quoteName('connect_to').') AS id  FROM ' . $db->quoteName('#__community_connection') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName( '#__users' ) . ' AS b '
            . ' ON a.'.$db->quoteName('connect_from').'=' . $db->Quote( $userid )
            . ' AND a.'.$db->quoteName('connect_to').'=b.'.$db->quoteName('id')
            . ' AND a.'.$db->quoteName('status').'=' . $db->Quote( '1' )
            . ' AND b.'.$db->quoteName('block').'=' .$db->Quote('0')
            . ' WHERE NOT EXISTS ( SELECT d.'.$db->quoteName('blocked_userid') . ' as id'
            . ' FROM '.$db->quoteName('#__community_blocklist') . ' AS d  '
            . ' WHERE d.'.$db->quoteName('userid').' = '.$db->Quote($userid)
            . ' AND d.'.$db->quoteName('blocked_userid').' = a.'.$db->quoteName('connect_to').')'
            . ' AND NOT EXISTS (SELECT e.'.$db->quoteName('memberid') . ' as id'
            . ' FROM '.$db->quoteName('#__community_groups_members') . ' AS e  '
            . ' WHERE e.'.$db->quoteName('groupid').' = '.$db->Quote($cid)
            . ' AND e.'.$db->quoteName('memberid').' = a.'.$db->quoteName('connect_to')
            .')'
            . $andName
            . ' ORDER BY b.' . $db->quoteName($nameField)
            . ' LIMIT ' . $limitstart.','.$limit
        ;
        $db->setQuery( $query );
        $friends = $db->loadColumn();

        //calculate total
        $query	=   'SELECT COUNT(DISTINCT(a.'.$db->quoteName('connect_to').'))  FROM ' . $db->quoteName('#__community_connection') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName( '#__users' ) . ' AS b '
            . ' ON a.'.$db->quoteName('connect_from').'=' . $db->Quote( $userid )
            . ' AND a.'.$db->quoteName('connect_to').'=b.'.$db->quoteName('id')
            . ' AND a.'.$db->quoteName('status').'=' . $db->Quote( '1' )
            . ' AND b.'.$db->quoteName('block').'=' .$db->Quote('0')
            . ' WHERE NOT EXISTS ( SELECT d.'.$db->quoteName('blocked_userid') . ' as id'
            . ' FROM '.$db->quoteName('#__community_blocklist') . ' AS d  '
            . ' WHERE d.'.$db->quoteName('userid').' = '.$db->Quote($userid)
            . ' AND d.'.$db->quoteName('blocked_userid').' = a.'.$db->quoteName('connect_to').')'
            . ' AND NOT EXISTS (SELECT e.'.$db->quoteName('memberid') . ' as id'
            . ' FROM '.$db->quoteName('#__community_groups_members') . ' AS e  '
            . ' WHERE e.'.$db->quoteName('groupid').' = '.$db->Quote($cid)
            . ' AND e.'.$db->quoteName('memberid').' = a.'.$db->quoteName('connect_to')
            .')'
            . $andName;

        $db->setQuery( $query );
        $this->total	=  $db->loadResult();

        return $friends;
    }

    /**
     * Return an object of group's invitors
     */
    public function getInvitors( $userid, $groupid )
    {
        if($userid == 0)
        {
            return false;
        }

        $db	=  $this->getDBO();

        $query	=   'SELECT DISTINCT(' . $db->quoteName( 'creator' ) . ') FROM ' . $db->quoteName('#__community_groups_invite') . ' '
            . 'WHERE ' . $db->quoteName( 'groupid' ) . '=' . $db->Quote( $groupid ) . ' '
            . 'AND ' . $db->quoteName( 'userid' ) . '=' . $db->Quote( $userid );

        $db->setQuery( $query );

        $results  =	$db->loadObjectList();

        // bind to table
        $data = array();
        foreach($results AS $row) {
            $invitor = JTable::getInstance('GroupInvite', 'CTable');
            $invitor->bind($row);
            $data[] = $invitor;
        }

        return $data;
    }


    /**
     * (Chris)
     * Return an object for Subgroups
     */

    public function getSubgroups($groupid){
        CError::assert( $groupid , '', '!empty', __FILE__ , __LINE__ );
        $db		= $this->getDBO();

        $query  = 'SELECT id, name, description, thumb, avatar, membercount, discusscount, ownerid FROM ' . $db->quoteName('#__community_groups')  . ' WHERE ' . $db->quoteName('parentid') . ' = ' . $groupid;

        $db->setQuery( $query );
        $result	= $db->loadObjectList();
        $this->total = count($result);
        return $result;
    }


    /**
     * (Chris)
     * Return an object for Grouping
     */

    public function getMainGroups(){

        $db		= $this->getDBO();

        $query  = 'SELECT id, name, description, thumb, avatar FROM ' . $db->quoteName('#__community_groups')  . ' WHERE ' . $db->quoteName('parentid') . ' = 0';

        $db->setQuery( $query );
        $result	= $db->loadObjectList();
        $this->total = count($result);
        return $result;
    }


    /**
     * Returns All the groups
     *
     * @access	public
     * @param	string 	Category id
     * @param	string	The sort type
     * @param	string	Search value
     * @return	Array	An array of group objects
     */
    public function getAllGroups( $categoryId = null , $sorting = null , $search = null , $limit = null , $skipDefaultAvatar = false , $hidePrivateGroup = false, $pagination = true, $nolimit = false, $position=NULL, $listgroups=false)
    {
        $db		= $this->getDBO();

        $extraSQL	= '';
        $pextra		= '';

        if( is_null( $limit ) )
        {
            $limit		= $this->getState('limit');
        }
        $limit	= ($limit < 0) ? 0 : $limit;

        if($pagination){
            $limitstart = $this->getState('limitstart');
        }else{
            $limitstart = 0;
        }

        $user = CFactory::getUser();
        $userId = (int) $user->id;

        //special case for sorting by featured
        if($sorting == 'featured'){
            $featuredGroups = $this->getFeaturedGroups();
            if(count($featuredGroups) > 0 ){
                $featuredGroups = implode(',', $featuredGroups);
                $extraSQL .= ' AND a.id IN ('.$featuredGroups.') ';
            }
        }


        // Test if search is parsed
        if( !is_null( $search ) )
        {

            //(Chris) Added SQL statment for keyword
            //$extraSQL	.= ' AND a.'.$db->quoteName('name').' LIKE ' . $db->Quote( '%' . $search . '%' ) . ' ';
            $extraSQL_cr = "";
            $user = CFactory::getUser();
            $userId = (int) $user->id;
            if(isset($_REQUEST['name']))
            {
                $sql = 'SELECT  ' . $db->quoteName('groupid').' FROM ' . $db->quoteName('#__community_groups_members') . ' WHERE `memberid`='.$userId.'';
                $extraSQL_cr	.= 'WHERE id in ('.$sql.') AND ( a.'.$db->quoteName('name').' LIKE ' . $db->Quote( '%' . $search . '%' ) . ' OR a.'.$db->quoteName('keyword').' LIKE ' . $db->Quote( '%' . $search . '%' ) . ' )';

            }
            else
            {
                $extraSQL	.= ' AND ( a.'.$db->quoteName('name').' LIKE ' . $db->Quote( '%' . $search . '%' ) . ' OR a.'.$db->quoteName('keyword').' LIKE ' . $db->Quote( '%' . $search . '%' ) . ' )';
            }
            //if unregistered user hide the private group for the search result
            if($userId == 0){
                $extraSQL	.= ' AND a.'.$db->quoteName('approvals').' != ' . $db->Quote('1') . ' ';
            }

            // Luyen Vu added
            $extraSQL       .= ' AND ((a.' . $db->quoteName('unlisted') . ' != ' . $db->Quote('1') . ' AND a.'.$db->quoteName('approvals').' != ' . $db->Quote('1') . ') '
                . ' AND a.' . $db->quoteName('categoryid') . ' != ' . $db->Quote('6') . ') ';
            if ($listgroups) {
                $extraSQL .= ' OR ((a.' . $db->quoteName('unlisted') . ' = ' . $db->Quote('0') . ' AND a.'.$db->quoteName('approvals').' = ' . $db->Quote('1') . ') AND a.' . $db->quoteName('parentid') . ' IN ('.$listgroups.'))';
            }

        }

        if( $skipDefaultAvatar )
        {
            $extraSQL	.= ' AND ( a.'.$db->quoteName('thumb').' != ' . $db->Quote( DEFAULT_GROUP_THUMB ) . ' AND a.'.$db->quoteName('avatar').' != ' . $db->Quote( DEFAULT_GROUP_AVATAR ) . ' )';
        }

        if ( $hidePrivateGroup )
        {
            $extraSQL	.= ' AND a.'.$db->quoteName('approvals').' != ' . $db->Quote('1') . ' ';
        }

        // (Chris) Exclude the subgroup
        $extraSQL .= ' AND a.'.$db->quoteName('subgroup').' = ' . $db->Quote('0') . ' ';

        $order	='';
        switch ( $sorting )
        {
            case 'alphabetical':
                $order		= ' ORDER BY a.'.$db->quoteName('name').' ASC ';
                break;
            case 'mostdiscussed':
                $order	= ' ORDER BY '.$db->quoteName('discusscount').' DESC ';
                break;
            case 'mostwall':
                $order	= ' ORDER BY '.$db->quoteName('wallcount').' DESC ';
                break;
            case 'mostmembers':
                $order	= ' ORDER BY '.$db->quoteName('membercount').' DESC ';
                break;
            case 'hits' :
                $order	= ' ORDER BY '.$db->quoteName('hits').' DESC ';
                break;
            case 'featuredtop' :
                $order	= ' ORDER BY '.$db->quoteName('featured').' ASC ';
                break;
            default:
                $order	= 'ORDER BY a.'.$db->quoteName('created').' DESC ';
                break;
// 			case 'mostactive':
// 				$order	= ' ORDER BY count DESC';
// 				break;
        }

        if( !is_null($categoryId) && $categoryId != 0 )
        {
            if (is_array($categoryId)) {
                if (count($categoryId) > 0) {
                    $categoryIds = implode(',', $categoryId);
                    $extraSQL .= ' AND a.' . $db->quoteName('categoryid'). ' IN(' . $categoryIds . ')';
                }
            } else {
                $extraSQL .= ' AND a.'.$db->quoteName('categoryid').'=' . $db->Quote($categoryId) . ' ';
            }
        }




        /*
        // Super slow query
        $query = 'SELECT a.*,'
                . 'COUNT(DISTINCT(b.memberid)) AS membercount,'
                . 'COUNT(DISTINCT(c.id)) AS discussioncount,'
                . 'COUNT(DISTINCT(d.id)) AS wallcount '
                . 'FROM ' . $db->quoteName( '#__community_groups' ) . ' AS a '
                . 'INNER JOIN ' . $db->quoteName( '#__community_groups_members' ) . ' AS b '
                . 'ON a.id=b.groupid '
                . $extraSQL
                . 'AND b.approved=' . $db->Quote( '1' ) . ' '
                . 'AND a.published=' . $db->Quote( '1' ) . ' '
                . 'LEFT JOIN ' . $db->quoteName( '#__community_groups_discuss' ) . ' AS c '
                . 'ON a.id=c.groupid '
                . 'AND c.parentid=' . $db->Quote( '0' ) . ' '
                . 'LEFT JOIN ' . $db->quoteName( '#__community_wall') . ' AS d '
                . 'ON a.id=d.contentid '
                . 'AND d.type=' . $db->Quote( 'groups' ) . ' '
                . 'AND d.published=' . $db->Quote( '1' ) . ' '
                . 'GROUP BY a.id '
                . $order
                . ' LIMIT ' . $limitstart . ',' . $limit;

        $db->setQuery( $query );
        $result	= $db->loadObjectList();
        */

        $user = CFactory::getUser();
        $userId = (int) $user->id;
        unset($user);



        if($userId > 0) {

            if(!COwnerHelper::isCommunityAdmin()) {
                $extraSQL.= '

            AND('
                    . 'a.' . $db->quoteName('unlisted') . ' = ' . $db->Quote('0')
                    .' OR('
                    .'a.' . $db->quoteName('unlisted') . ' = ' . $db->Quote('1')
                    .' AND'

                    .'(SELECT COUNT(' . $db->quoteName('groupid').') FROM ' . $db->quoteName('#__community_groups_members') . ' as b WHERE b.'.$db->quoteName('memberid').'='. $db->quote($userId) .' and b.'.$db->quoteName('groupid').'=a.'.$db->quoteName('id').') > 0
                 )

                 )';
            }
        } else {
            $extraSQL .= ' AND a.' . $db->quoteName('unlisted') . ' = ' . $db->Quote('0');
        }


        $extraSQL .= ' AND a.'.$db->quoteName('subgroup').' = ' . $db->Quote('0') . ' ';
        $extraSQL       .= ' AND ((a.' . $db->quoteName('unlisted') . ' != ' . $db->Quote('1') . ' AND a.'.$db->quoteName('approvals').' != ' . $db->Quote('1') . ') '
            . ' OR a.' . $db->quoteName('categoryid') . ' != ' . $db->Quote('6') . ') ';

        if ($sorting == 'mostactive')
        {
            $query	= 'SELECT *, (a.' . $db->quoteName('discusscount') . ' + a.' . $db->quoteName('wallcount') . ' ) AS count FROM ' . $db->quoteName('#__community_groups') . ' as a'
                . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b ON a.' . $db->quoteName('id') . '= b.' . $db->quoteName('groupid')
                . ' WHERE a.' . $db->quoteName('published') . ' = ' . $db->Quote('1')
                . $extraSQL
                . ' GROUP BY a.' . $db->quoteName('id')
                . ' ORDER BY '.$db->quoteName('count').' DESC ';
        }
        else
        {
            if(!isset($_REQUEST['name']))
            {
                $user = CFactory::getUser();
                $query = 'SELECT '
                    .' a.*'
                    .' FROM '.$db->quoteName('#__community_groups').' as a '
                    . ' WHERE (a.'.$db->quoteName('published').'='.$db->Quote('1') .'  '
                    .'AND a.'.$db->quoteName('unlisted').'!='.$db->Quote('1') .'  '
                    .'OR a.'.$db->quoteName('id').'IN (SELECT '.$db->quoteName('groupid').' FROM  '.$db->quoteName('#__community_groups_members').' WHERE '.$db->quoteName('memberid').'= '.$db->quote($user->id).' AND  '.$db->quoteName('approved').' = '.$db->quote('1').') )'
                    . $extraSQL. ''
                    . 'OR (a.'.$db->quoteName('published').'='.$db->Quote('1').' AND a.'.$db->quoteName('ownerid').'='.$db->Quote($userId).')'.' '
                    . $order;
                
            }
            else
            {
                $query = 'SELECT '
                    .' a.*'
                    .' FROM '.$db->quoteName('#__community_groups').' as a '
                    . $extraSQL_cr
                    . $order;

            }
        }

        //Chris: list all the groups and stick the feature group on top

        if($sorting == 'featuredtop'){
            $featuredGroups = $this->getFeaturedGroups();
            if(count($featuredGroups) > 0 ){
                $featuredGroups = implode(',', $featuredGroups);
            }

            $query = 'SELECT '
                .' a.* , IF(a.id IN ('.$featuredGroups.'), 1, 0) as featured '
                .' FROM '.$db->quoteName('#__community_groups').' as a '
                . ' WHERE a.'.$db->quoteName('published').'='.$db->Quote('1') .'  '
                . $extraSQL
                . ' ORDER BY '.$db->quoteName('featured').' DESC ';
        }
        if($position!==NULL)
        {
            $query.="limit  $position , $limit";
        }
        $db->setQuery( $query );
        $rows	= $db->loadObjectList();

        if(!empty($rows)){
            //count members, some might be blocked, so we want to deduct from the total we currently have
            foreach($rows as $k => $r){
                $query = "SELECT COUNT(*)
						  FROM #__community_groups_members a
						  JOIN #__users b ON a.memberid=b.id
						  WHERE `approved`='1' AND b.block=0 AND groupid=".$db->Quote($r->id);
                $db->setQuery( $query );
                $rows[$k]->membercount = $db->loadResult();
            }
        }


// 		if(!empty($rows)){
// 			for($i = 0; $i < count($rows); $i++){
//
// 				// Count no of members
// 				$query = "SELECT COUNT(*) FROM #__community_groups_members WHERE `approved`='1' "
// 					. " AND groupid=".$db->Quote($rows[$i]->id);
// 				$db->setQuery( $query );
// 				$rows[$i]->membercount = $db->loadResult();
//
// 				// Count wall post
// 				$query = "SELECT COUNT(*) FROM #__community_wall WHERE "
// 					. " `contentid`=".$db->Quote($rows[$i]->id)
// 					. " AND type=".$db->Quote('groups')
// 					. " AND published=".$db->Quote('1');
//
// 				$db->setQuery( $query );
// 				$rows[$i]->wallcount = $db->loadResult();
//
// 				// Count discussion
// 				$query = "SELECT groupid FROM #__community_groups_discuss "
// 					. " WHERE groupid=".$db->Quote($rows[$i]->id)
// 					. " AND parentid=" . $db->Quote( '0' );
// 				$db->setQuery( $query );
// 				$rows[$i]->discussioncount = $db->loadResult();
// 			}
// 		}

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        $query	= 'SELECT COUNT(*) FROM '.$db->quoteName('#__community_groups').' AS a '
            . 'WHERE a.'.$db->quoteName('published').'=' . $db->Quote( '1' )
            . $extraSQL;

        $db->setQuery( $query );
        $this->total	=  $db->loadResult();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        if( empty($this->_pagination) )
        {
            jimport('joomla.html.pagination');

            $this->_pagination	= new JPagination( $this->total , $limitstart , $limit);
        }

        return $rows;
    }

    /**
     * Returns an object of group
     *
     * @access	public
     * @param	string 	Group Id
     * @returns object  An object of the specific group
     */
    public function & getGroup( $id )
    {
        $db		= $this->getDBO();

        $query	= 'SELECT a.*, b.'.$db->quoteName('name').' AS ownername , c.'.$db->quoteName('name').' AS category FROM '
            . $db->quoteName('#__community_groups') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__users') . ' AS b '
            . ' INNER JOIN ' . $db->quoteName('#__community_groups_category') . ' AS c '
            . ' WHERE a.'.$db->quoteName('id').'=' . $db->Quote( $id ) . ' '
            . ' AND a.'.$db->quoteName('ownerid').'=b.'.$db->quoteName('id')
            . ' AND a.'.$db->quoteName('categoryid').'=c.'.$db->quoteName('id');

        $db->setQuery( $query );
        $result	= $db->loadObject();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        return $result;
    }

    /**
     * Loads the categories
     *
     * @access	public
     * @returns Array  An array of categories object
     */
    public function getCategories( $catId = COMMUNITY_ALL_CATEGORIES )
    {
        $db	=  $this->getDBO();

        $where	=   '';

        if( $catId !== COMMUNITY_ALL_CATEGORIES && ($catId != 0 || !is_null($catId )))
        {
            if( $catId === COMMUNITY_NO_PARENT )
            {
                $where	=   'WHERE a.'.$db->quoteName('parent').'=' . $db->Quote( COMMUNITY_NO_PARENT ) . ' ';
            }
            else
            {
                $where	=   'WHERE a.'.$db->quoteName('parent').'=' . $db->Quote( $catId ) . ' ';
            }
        }

        $query	=   'SELECT a.*, COUNT(b.'.$db->quoteName('id').') AS count '
            . ' FROM ' . $db->quoteName('#__community_groups_category') . ' AS a '
            . ' LEFT JOIN ' . $db->quoteName( '#__community_groups' ) . ' AS b '
            . ' ON a.'.$db->quoteName('id').'=b.'.$db->quoteName('categoryid')
            . ' AND b.'.$db->quoteName('published').'=' . $db->Quote( '1' ) . ' '
            . $where
            . ' GROUP BY a.'.$db->quoteName('id').' ORDER BY a.'.$db->quoteName('name').' ASC';

        $db->setQuery( $query );
        $result	= $db->loadObjectList();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        return $result;
    }

    /**
     * Return all category.
     *
     * @access  public
     * @returns Array  An array of categories object
     * @since   Jomsocial 2.6
     **/
    public function getAllCategories()
    {
        $db     = $this->getDBO();

        $query  = 'SELECT *
					FROM ' . $db->quoteName('#__community_groups_category');

        $db->setQuery( $query );
        $result = $db->loadObjectList();

        // bind to table
        $data = array();
        foreach($result AS $row) {
            $groupCat = JTable::getInstance('GroupCategory', 'CTable');
            $groupCat->bind($row);
            $data[] = $groupCat;
        }

        return $data;
    }

    /**
     * Returns the category's group count
     *
     * @access  public
     * @returns Array  An array of categories object
     * @since   Jomsocial 2.4
     **/
    function getCategoriesCount()
    {
        $db	=  $this->getDBO();

        $query = "SELECT c.id, c.parent, c.name, count(g.id) AS total, c.description
				  FROM " . $db->quoteName('#__community_groups_category') . " AS c
				  LEFT JOIN " . $db->quoteName('#__community_groups'). " AS g ON g.categoryid = c.id
							AND g." . $db->quoteName('published') . "=" . $db->Quote( '1' ) . "
				  GROUP BY c.id
				  ORDER BY c.name";

        $db->setQuery( $query );
        $result	= $db->loadObjectList('id');

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        return $result;
    }
    /**
     * Returns the category name of the specific category
     *
     * @access public
     * @param	string Category Id
     * @returns string	Category name
     **/
    public function getCategoryName( $categoryId )
    {
        // Chris
        // CError::assert($categoryId, '', '!empty', __FILE__ , __LINE__ );
        $db		= $this->getDBO();

        $query	= 'SELECT ' . $db->quoteName('name') . ' '
            . 'FROM ' . $db->quoteName('#__community_groups_category') . ' '
            . 'WHERE ' . $db->quoteName('id') . '=' . $db->Quote( $categoryId );
        $db->setQuery( $query );

        $result	= $db->loadResult();

        if($db->getErrorNum()) {
            JError::raiseError( 500, $db->stderr());
        }
        CError::assert( $result , '', '!empty', __FILE__ , __LINE__ );
        return $result;
    }

    /**
     * Returns the members list for the specific groups
     *
     * @access public
     * @param	string Category Id
     * @returns string	Category name
     **/
    public function getAdmins( $groupid , $limit = 0 , $randomize = false )
    {
        CError::assert( $groupid , '', '!empty', __FILE__ , __LINE__ );

        $db		= $this->getDBO();

        $limit		= ($limit === 0) ? $this->getState('limit') : $limit;
        $limitstart = $this->getState('limitstart');

        $query	= 'SELECT a.'.$db->quoteName('memberid').' AS id, a.'.$db->quoteName('approved').' , b.'.$db->quoteName('name').' as name FROM '
            . $db->quoteName('#__community_groups_members') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__users') . ' AS b '
            . ' WHERE b.'.$db->quoteName('id').'=a.'.$db->quoteName('memberid')
            . ' AND a.'.$db->quoteName('groupid').'=' . $db->Quote( $groupid )
            . ' AND a.'.$db->quoteName('permissions').'=' . $db->Quote( '1' );

        if($randomize)
        {
            $query	.= ' ORDER BY RAND() ';
        }

        if( !is_null($limit) )
        {
            $query	.= ' LIMIT ' . $limitstart . ',' . $limit;
        }
        $db->setQuery( $query );
        $result	= $db->loadObjectList();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        $query	= 'SELECT COUNT(*) FROM '
            . $db->quoteName('#__community_groups_members') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__users') . ' AS b '
            . ' WHERE b.'.$db->quoteName('id').'=a.'.$db->quoteName('memberid')
            . ' AND a.'.$db->quoteName('groupid').'=' . $db->Quote( $groupid )
            . ' AND a.'.$db->quoteName('permissions').'=' . $db->Quote( '1' );

        $db->setQuery( $query );
        $total	= $db->loadResult();
        $this->total	= $total;

        if($db->getErrorNum()) {
            JError::raiseError( 500, $db->stderr());
        }

        if( empty($this->_pagination) )
        {
            jimport('joomla.html.pagination');

            $this->_pagination	= new JPagination( $total , $limitstart , $limit);
        }

        return $result;
    }




    /**
     * Returns the members list for the specific groups
     *
     * @access public
     * @param	string Category Id
     * @returns string	Category name
     **/
    public function getAllMember($groupid){
        CError::assert( $groupid , '', '!empty', __FILE__ , __LINE__ );
        $db		= $this->getDBO();

        $query	= 'SELECT a.'.$db->quoteName('memberid').' AS id, a.'.$db->quoteName('approved').' , b.'.$db->quoteName('name').' as name , a.'. $db->quoteName('permissions') .' as permission FROM '
            . $db->quoteName('#__community_groups_members') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__users') . ' AS b '
            . ' WHERE b.'.$db->quoteName('id').'=a.'.$db->quoteName('memberid')
            . ' AND a.'.$db->quoteName('groupid').'=' . $db->Quote( $groupid )
            . ' AND b.'.$db->quoteName('block').'=' . $db->Quote( '0' ) . ' '
            . ' AND a.'.$db->quoteName('permissions').' !=' . $db->quote( COMMUNITY_GROUP_BANNED );
        $db->setQuery( $query );
        $result	= $db->loadObjectList();
        $this->total = count($result);
        return $result;
    }

    /**
     * @param $groupid
     * @param int $limit
     * @param bool $onlyApproved
     * @param bool $randomize
     * @param bool $loadAdmin
     * @param bool $ignoreLimit
     * @return mixed
     */
    public function getMembers( $groupid , $limit = 0 , $onlyApproved = true , $randomize = false , $loadAdmin = false, $ignoreLimit = false,$position=NULL )
    {
        CError::assert( $groupid , '', '!empty', __FILE__ , __LINE__ );

        $db		= $this->getDBO();
        $config	= CFactory::getConfig();
        $limit		= ($limit === 0) ? $this->getState('limit') : $limit;
        $limitstart = $this->getState('limitstart');

        $query	= 'SELECT a.'.$db->quoteName('memberid').' AS id, a.'.$db->quoteName('roles').' AS roles, a.'.$db->quoteName('date').' AS date, a.'.$db->quoteName('approved').' , b.'.$db->quoteName($config->get( 'displayname')).' as name FROM'
            . $db->quoteName('#__community_groups_members') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__users') . ' AS b '
            . ' WHERE b.'.$db->quoteName('id').'=a.'.$db->quoteName('memberid')
            . ' AND a.'.$db->quoteName('groupid').'=' . $db->Quote( $groupid )
            . ' AND b.'.$db->quoteName('block').'=' . $db->Quote( '0' ) . ' '
            . ' AND a.'.$db->quoteName('permissions').' !=' . $db->quote( COMMUNITY_GROUP_BANNED );

        if( $onlyApproved )
        {
            $query	.= ' AND a.'.$db->quoteName('approved').'=' . $db->Quote( '1' );
        }
        else
        {
            $query	.= ' AND a.'.$db->quoteName('approved').'=' . $db->Quote( '0' );
        }

        if( !$loadAdmin )
        {
            $query	.= ' AND a.'.$db->quoteName('permissions').'=' . $db->Quote( '0' );
        }

        if( $randomize )
        {
            $query	.= ' ORDER BY RAND() ';
        }
        else
        {

            $query	.= ' ORDER BY b.`' . $config->get( 'displayname') . '`';
        }

        if($position!==NULL)
        {
            $query.="limit  $position , $limit";
        }

        $db->setQuery( $query );
        $result	= $db->loadObjectList();

        if($db->getErrorNum()) {
            JError::raiseError( 500, $db->stderr());
        }

        $query = explode('FROM', $query);
        $query = explode('LIMIT', $query[1]);
        $query = "SELECT COUNT(*) FROM".$query[0];

        $db->setQuery( $query );

        $total		= $db->loadResult();


        if($db->getErrorNum()) {
            JError::raiseError( 500, $db->stderr());
        }

        if( empty($this->_pagination))
        {
            jimport('joomla.html.pagination');

            $this->_pagination	= new JPagination( $total , $limitstart , $limit);
        }

        return $result;
    }


    /**
     * @param $groupid
     * @param int $limit
     * @param bool $onlyApproved
     * @param bool $randomize
     * @param bool $loadAdmin
     * @param bool $ignoreLimit
     * @return mixed
     */
    //Chris: Added this for Listing for Administer group or circle

    public function getListMembers( $groupid , $limit = 0 , $loadAdmin = false, $ignoreLimit = false )
    {
        CError::assert( $groupid , '', '!empty', __FILE__ , __LINE__ );

        $db		= $this->getDBO();
        $config	= CFactory::getConfig();
        $limit		= ($limit === 0) ? $this->getState('limit') : $limit;
        $limitstart = $this->getState('limitstart');

        $query	= 'SELECT a.'.$db->quoteName('memberid').' AS id, a.'.$db->quoteName('approved').' , b.'.$db->quoteName($config->get( 'displayname')).' as name, a.permissions FROM'
            . $db->quoteName('#__community_groups_members') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__users') . ' AS b '
            . ' WHERE b.'.$db->quoteName('id').'=a.'.$db->quoteName('memberid')
            . ' AND a.'.$db->quoteName('groupid').'=' . $db->Quote( $groupid )
            . ' AND b.'.$db->quoteName('block').'=' . $db->Quote( '0' ) . ' '
            . ' AND a.'.$db->quoteName('permissions').' !=' . $db->quote( COMMUNITY_GROUP_BANNED );
        /*
        if( $onlyApproved )
        {
            $query	.= ' AND a.'.$db->quoteName('approved').'=' . $db->Quote( '1' );
        }
        else
        {
            $query	.= ' AND a.'.$db->quoteName('approved').'=' . $db->Quote( '0' );
        }
        */


        if( !$loadAdmin )
        {
            $query	.= ' AND a.'.$db->quoteName('permissions').'=' . $db->Quote( '0' );
        }

        $query	.= ' ORDER BY a.' . $db->quoteName('approved') . ', a.'.$db->quoteName('permissions') . 'DESC, ' . ' b.'.$db->quoteName($config->get( 'displayname')) . ' ';

        if( $limit && !$ignoreLimit )
        {
            $query	.= ' LIMIT ' . $limitstart . ',' . $limit;
        }

        $db->setQuery( $query );
        $result	= $db->loadObjectList();

        if($db->getErrorNum()) {
            JError::raiseError( 500, $db->stderr());
        }

        $query = explode('FROM', $query);
        $query = explode('LIMIT', $query[1]);
        $query = "SELECT COUNT(*) FROM".$query[0];

        $db->setQuery( $query );

        $total		= $db->loadResult();


        if($db->getErrorNum()) {
            JError::raiseError( 500, $db->stderr());
        }

        if( empty($this->_pagination))
        {
            jimport('joomla.html.pagination');

            $this->_pagination	= new JPagination( $total , $limitstart , $limit);
        }

        return $result;
    }

    /**
     * Check if the given group name exist.
     * if id is specified, only search for those NOT within $id
     */
    public function groupExist($name, $id=0) {
        $db		= $this->getDBO();

        $strSQL	= 'SELECT COUNT(*) FROM '.$db->quoteName('#__community_groups')
            . ' WHERE '.$db->quoteName('name').'=' . $db->Quote( $name )
            . ' AND '.$db->quoteName('id').'!='. $db->Quote( $id ) ;


        $db->setQuery( $strSQL );
        $result	= $db->loadResult();

        if($db->getErrorNum()) {
            JError::raiseError( 500, $db->stderr());
        }

        return $result;
    }

    /**
     * Check if the given group name exist same owner
     * if id is specified, only search for those NOT within $id
     */
    public function groupExistUserCategory($name,$userid,$categoryid) {
        $db		= $this->getDBO();
        $strSQL	= 'SELECT COUNT(*) FROM '.$db->quoteName('#__community_groups')
            . ' WHERE '.$db->quoteName('name').'=' . $db->Quote( $name )
            . ' AND '.$db->quoteName('ownerid').'='. $db->Quote( $userid )
            . ' AND '.$db->quoteName('categoryid').'='. $db->Quote( $categoryid );

        $db->setQuery( $strSQL );
        $result	= $db->loadResult();

        if($db->getErrorNum()) {
            JError::raiseError( 500, $db->stderr());
        }

        return $result;
    }

    public function getMembersId( $groupid , $onlyApproved = false )
    {
        $db		= $this->getDBO();

        $query	= 'SELECT a.'.$db->quoteName('memberid').' AS id FROM '
            . $db->quoteName('#__community_groups_members') . ' AS a '
            . 'JOIN ' . $db->quoteName('#__users') . ' AS b ON a.' . $db->quoteName('memberid') . '=b.' . $db->quoteName('id')
            . 'WHERE a.'.$db->quoteName('groupid').'=' . $db->Quote( $groupid );

        if( $onlyApproved ){
            $query	.= ' AND ' . $db->quoteName( 'approved' ) . '=' . $db->Quote( '1' );
            $query	.= ' AND b.' . $db->quoteName('block') . '=0 ';
            $query	.= 'AND permissions!=' . $db->Quote(COMMUNITY_GROUP_BANNED);
        }

        $db->setQuery( $query );
        $result	= $db->loadColumn();

        return $result;
    }

    public function updateGroup($data)
    {
        $db		= $this->getDBO();

        if($data->id == 0)
        {
            // New record, insert it.
            $db->insertObject( '#__community_groups' , $data );

            if($db->getErrorNum()) {
                JError::raiseError( 500, $db->stderr());
            }
            $data->id				= $db->insertid();

            // Insert an object for this user in the #__community_groups_members as well
            $members				= new stdClass();
            $members->groupid		= $data->id;
            $members->memberid		= $data->ownerid;


            // Creator should always be 1 as approved as they are the creator.
            $members->approved		= 1;
            $members->permissions	= 'admin';

            $db->insertObject( '#__community_groups_members' , $members );

            if($db->getErrorNum()) {
                JError::raiseError( 500, $db->stderr());
            }
        }
        else
        {
            // Old record, update it.
            $db->updateObject( '#__community_groups' , $data , 'id');
        }
        return $data->id;
    }

    /**
     *	Set the avatar for for specific group
     *
     * @param	appType		Application type. ( users , groups )
     * @param	path		The relative path to the avatars.
     * @param	type		The type of Image, thumb or avatar.
     *
     **/
    public function setImage(  $id , $path , $type = 'thumb' )
    {
        CError::assert( $id , '' , '!empty' , __FILE__ , __LINE__ );
        CError::assert( $path , '' , '!empty' , __FILE__ , __LINE__ );

        $db			= $this->getDBO();

        // Fix the back quotes
        $path		= CString::str_ireplace( '\\' , '/' , $path );
        $type		= JString::strtolower( $type );

        // Test if the record exists.
        $query		= 'SELECT ' . $db->quoteName( $type ) . ' FROM ' . $db->quoteName( '#__community_groups' )
            . 'WHERE ' . $db->quoteName( 'id' ) . '=' . $db->Quote( $id );

        $db->setQuery( $query );
        $oldFile	= $db->loadResult();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        if( !$oldFile )
        {
            $query	= 'UPDATE ' . $db->quoteName( '#__community_groups' ) . ' '
                . 'SET ' . $db->quoteName( $type ) . '=' . $db->Quote( $path ) . ' '
                . 'WHERE ' . $db->quoteName( 'id' ) . '=' . $db->Quote( $id );
            $db->setQuery( $query );
            $db->query( $query );

            if($db->getErrorNum())
            {
                JError::raiseError( 500, $db->stderr());
            }
        }
        else
        {

            $query	= 'UPDATE ' . $db->quoteName( '#__community_groups' ) . ' '
                . 'SET ' . $db->quoteName( $type ) . '=' . $db->Quote( $path ) . ' '
                . 'WHERE ' . $db->quoteName( 'id' ) . '=' . $db->Quote( $id );
            $db->setQuery( $query );
            $db->query( $query );

            if($db->getErrorNum())
            {
                JError::raiseError( 500, $db->stderr());
            }

            // File exists, try to remove old files first.
            $oldFile	= CString::str_ireplace( '/' , '/' , $oldFile );

            // If old file is default_thumb or default, we should not remove it.
            // Need proper way to test it
            if(!JString::stristr( $oldFile , 'group.jpg' ) && !JString::stristr( $oldFile , 'group_thumb.jpg' ) &&
                !JString::stristr( $oldFile , 'default.jpg' ) && !JString::stristr( $oldFile , 'default_thumb.jpg' ) )
            {
                jimport( 'joomla.filesystem.file' );
                JFile::delete($oldFile);
            }
        }

        return $this;
    }

    public function removeMember( $data )
    {
        $db	= $this->getDBO();

        $strSQL	= 'DELETE FROM ' . $db->quoteName('#__community_groups_members') . ' '
            . 'WHERE ' . $db->quoteName('groupid') . '=' . $db->Quote( $data->groupid ) . ' '
            . 'AND ' . $db->quoteName('memberid') . '=' . $db->Quote( $data->memberid );

        $db->setQuery( $strSQL );
        $db->query();

        if($db->getErrorNum()) {
            JError::raiseError( 500, $db->stderr());
        }
    }

    /**
     * Check if the user is a group creator
     */
    public function isCreator( $userId , $groupId )
    {
        // guest is not a member of any group
        if($userId == 0)
            return false;

        $db		= $this->getDBO();

        $query	= 'SELECT COUNT(*) FROM ' . $db->quoteName( '#__community_groups' ) . ' '
            . 'WHERE ' . $db->quoteName( 'id' ) . '=' . $db->Quote( $groupId ) . ' '
            . 'AND ' . $db->quoteName( 'ownerid' ) . '=' . $db->Quote( $userId );
        $db->setQuery( $query );

        $isCreator	= ( $db->loadResult() >= 1 ) ? true : false;
        return $isCreator;
    }

    /**
     * Check if the user is invited in the group
     */
    public function isInvited($userid, $groupid)
    {
        if($userid == 0)
        {
            return false;
        }

        $db	=  $this->getDBO();

        $query	=   'SELECT * FROM ' . $db->quoteName('#__community_groups_invite') . ' '
            . 'WHERE ' . $db->quoteName( 'groupid' ) . '=' . $db->Quote( $groupid ) . ' '
            . 'AND ' . $db->quoteName( 'userid' ) . '=' . $db->Quote( $userid );

        $db->setQuery( $query );

        $isInvited	= ( $db->loadResult() >= 1 ) ? true : false;

        return $isInvited;
    }

    /**
     * Check if the user is a group admin
     */
    public function isAdmin($userid, $groupid)
    {
        if($userid == 0)
            return false;
        $jparams = JComponentHelper::getParams('com_joomdle');
        $user = CFactory::getUser($userid);
        $db		= $this->getDBO();
        $courseid = $this->getIsCourseGroup($groupid);
        $namerole = true ;
        if($courseid){
            if ($jparams->get('use_new_performance_method'))
                $roles = JHelperLGT::getUserRole(['id' => $courseid, 'username' => $user->username]);
            else
                $roles = JoomdleHelperContent::call_method ('get_user_role', (int)$courseid, $user->username);

            foreach ($roles['roles'] as $role) {
                $user_roles = $role['role'];
                $decode_user_roles = json_decode($user_roles);
                foreach ($decode_user_roles as $dur) {
                    if($dur->roleid == 5) {
                        $isAdmin = false;

                    }
                    else  if($dur->roleid == 4) {
                        $isAdmin = false;

                    }

                    else if($dur->roleid == 2 || $dur->roleid == 1){
                        $namerole = false;
                        $isAdmin = true;
                        return $isAdmin;

                    }
                }
            }
        }
        else{
            $query	= 'SELECT COUNT(*) FROM ' . $db->quoteName('#__community_groups_members') . ' '
                . ' WHERE ' . $db->quoteName('groupid') . '=' . $db->Quote($groupid) . ' '
                . ' AND ' . $db->quoteName('memberid') . '=' . $db->Quote($userid)
                . ' AND '.$db->quoteName('permissions').'=' . $db->Quote( '1' );

            $db->setQuery( $query );
            $isAdmin	= ( $db->loadResult() >= 1 ) ? true : false;

            if($db->getErrorNum())
            {
                JError::raiseError( 500, $db->stderr());
            }
            //@remove: in newer version we need to skip this test as we were using 'admin'
            // as the permission for the creator
            if( !$isAdmin )
            {
                $query	= 'SELECT COUNT(*) FROM ' . $db->quoteName( '#__community_groups' ) . ' '
                    . 'WHERE ' . $db->quoteName( 'id' ) . '=' . $db->Quote( $groupid ) . ' '
                    . 'AND ' . $db->quoteName( 'ownerid' ) . '=' . $db->Quote( $userid );
                $db->setQuery( $query );

                $isAdmin	= ( $db->loadResult() >= 1 ) ? true : false;

                if($db->getErrorNum())
                {
                    JError::raiseError( 500, $db->stderr());
                }

                // If user is admin, update necessary records
                if( $isAdmin )
                {
                    $members	= JTable::getInstance( 'GroupMembers' , 'CTable' );
                    $keys = array('memberId'=>$userid , 'groupId'=>$groupid );
                    $members->load( $keys);
                    $members->permissions	= '1';
                    $members->store();
                }
            }
        }
        return $isAdmin;
    }

    /**
     * Check if the given user is a member of the group
     * @param	string	userid
     * @param	string	groupid
     */
    public function isMember($userid, $groupid) {

        // guest is not a member of any group
        if($userid == 0)
            return false;

        $db	= $this->getDBO();
        $strSQL	= 'SELECT COUNT(*) FROM ' . $db->quoteName('#__community_groups_members')
            . ' WHERE ' . $db->quoteName('groupid') 	. '=' . $db->Quote($groupid)
            . ' AND ' 	. $db->quoteName('memberid') 	. '=' . $db->Quote($userid)
            . ' AND ' 	. $db->quoteName( 'approved' ) 	. '=' . $db->Quote( '1' )
            . ' AND '   . $db->quoteName('permissions') . '!=' . $db->Quote(COMMUNITY_GROUP_BANNED);


        $db->setQuery( $strSQL );
        $count	= $db->loadResult();
        return $count;
    }

    /**
     * See if the given user is waiting authorization for the group
     * @param	string	userid
     * @param	string	groupid
     */
    public function isWaitingAuthorization($userid, $groupid) {
        // guest is not a member of any group
        if($userid == 0)
            return false;

        $db	= $this->getDBO();
        $strSQL	= 'SELECT COUNT(*) FROM `#__community_groups_members` '
            . 'WHERE ' . $db->quoteName('groupid') . '=' . $db->Quote($groupid) . ' '
            . 'AND ' . $db->quoteName('memberid') . '=' . $db->Quote($userid)
            . 'AND ' . $db->quoteName('approved') . '=' . $db->Quote(0);

        $db->setQuery( $strSQL );
        $count	= $db->loadResult();
        return $count;
    }

    public function allRequestJoin($groupid, $userid, $page, $limit) {
        if($userid == 0) {
            return false;
        }
        if($page !== null) {
            $position = ($page - 1) * $limit;
            $query = 'LIMIT  '.$position.','.$limit.'';
        }
        $db	= $this->getDBO();
        $strSQL	= 'SELECT * FROM `#__community_groups_members` '
            . 'WHERE ' . $db->quoteName('groupid') . '=' . $db->Quote($groupid) . ' '
            . 'AND ' . $db->quoteName('approved') . '!=' . $db->Quote(1).' '
            . 'ORDER BY date DESC '
            . $query;

        $db->setQuery( $strSQL );
        $result	= $db->loadObjectList();
        return $result;
    }

    /**
     * Gets the groups property if it requires an approval or not.
     *
     * param	string	id The id of the group.
     *
     * return	boolean	True if it requires approval and False otherwise
     **/
    public function needsApproval( $id )
    {
        $db		= $this->getDBO();
        $query	= 'SELECT ' . $db->quoteName( 'approvals' ) . ' FROM '
            . $db->quoteName( '#__community_groups' ) . ' WHERE '
            . $db->quoteName( 'id' ) . '=' . $db->Quote( $id );

        $db->setQuery( $query );
        $result	= $db->loadResult();

        return ( $result == '1' );
    }

    /**
     * Sets the member data in the group members table
     *
     * param	Object	An object that contains the fields value
     *
     **/
    public function approveMember( $groupid , $memberid )
    {
        $db		= $this->getDBO();
        $time = time();

        $query	= 'UPDATE ' . $db->quoteName( '#__community_groups_members' ) . ' SET '
            . $db->quoteName( 'approved' ) . '=' . $db->Quote( '1' ) . ', '
            . $db->quoteName( 'date' ) . '=' . $db->Quote( $time ) . ' '
            . 'WHERE ' . $db->quoteName( 'groupid' ) . '=' . $db->Quote( $groupid ) . ' '
            . 'AND ' . $db->quoteName( 'memberid' ) . '=' . $db->Quote( $memberid );

        $db->setQuery( $query );
        $db->query();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }
    }

    /**
     * Sets the member data in the group members table
     *
     * param	Object	An object that contains the fields value
     *
     **/
    public function memberLastaccessGroup( $groupid , $memberid )
    {
        $db		= $this->getDBO();
        $time   = date('Y-m-d H:i:s',time());

        $query	= 'UPDATE ' . $db->quoteName( '#__community_groups_members' ) . ' SET '
            . $db->quoteName( 'lastaccess' ) . '=' . $db->Quote( $time ) . ' '
            . 'WHERE ' . $db->quoteName( 'groupid' ) . '=' . $db->Quote( $groupid ) . ' '
            . 'AND ' . $db->quoteName( 'memberid' ) . '=' . $db->Quote( $memberid );

        $db->setQuery( $query );
        $db->query();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }
    }

    //read all notification of user
    public function readNotificationGroup($memberid )
    {
        $db		= $this->getDBO();
        $time   = date('Y-m-d H:i:s',time());

        $query	= 'UPDATE ' . $db->quoteName( '#__community_groups_members' ) . ' SET '
            . $db->quoteName( 'lastaccess' ) . '=' . $db->Quote( $time ) . ' '
            . 'WHERE '. $db->quoteName( 'memberid' ) . '=' . $db->Quote( $memberid );

        $db->setQuery( $query );
        $db->query();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }
    }

    /**
     * Delete group's bulletin
     *
     * param	string	id The id of the group.
     *
     **/
    static public function deleteGroupBulletins($gid)
    {
        $db = JFactory::getDBO();

        $sql = "DELETE

				FROM
						".$db->quoteName("#__community_groups_bulletins")."
				WHERE
						".$db->quoteName("groupid")." = ".$db->quote($gid);

        $db->setQuery($sql);
        $db->Query();
        if($db->getErrorNum()){
            JError::raiseError( 500, $db->stderr());
        }

        return true;
    }
    /**
     * Delete group's events
     *
     * param	string	id The id of the group.
     *
     **/
    static public function deleteGroupEvents($gid)
    {
        $db = JFactory::getDBO();

        $sql = "DELETE

				FROM
						".$db->quoteName("#__community_events")."
				WHERE
						".$db->quoteName("contentid")." = ".$db->quote($gid);

        $db->setQuery($sql);
        $db->Query();
        if($db->getErrorNum()){
            JError::raiseError( 500, $db->stderr());
        }

        return true;
    }

    /**
     * Delete group's member
     *
     * param	string	id The id of the group.
     *
     **/
    static public function deleteGroupMembers($gid)
    {
        $db = JFactory::getDBO();

        //before removing from this table, remove from the user group column first
        $sql = "SELECT ".$db->quoteName('memberid')." FROM ".$db->quoteName("#__community_groups_members")." WHERE ".$db->quoteName("groupid")."=".$db->quote($gid);
        $db->setQuery($sql);
        $results = $db->loadColumn();

        foreach($results as $result){
            $user = CFactory::getUser($result);
            $groups = explode(',',$user->_groups);
            $filteredGroup = array_diff( $groups, array($gid) );
            $groups = implode(',', $filteredGroup);
            $user->_groups = $groups;
            $user->save();
        }

        $sql = "DELETE

				FROM
						".$db->quoteName("#__community_groups_members")."
				WHERE
						".$db->quoteName("groupid")."=".$db->quote($gid);
        $db->setQuery($sql);
        $db->Query();
        if($db->getErrorNum()){
            JError::raiseError( 500, $db->stderr());
        }

        return true;
    }

    /**
     * Delete group's wall
     *
     * param	string	id The id of the group.
     *
     **/
    static public function deleteGroupWall($gid)
    {
        $db = JFactory::getDBO();

        $sql = "DELETE

				FROM
						".$db->quoteName("#__community_wall")."
				WHERE
						".$db->quoteName("contentid")." = ".$db->quote($gid)." AND
						".$db->quoteName("type")." = ".$db->quote('groups');
        $db->setQuery($sql);
        $db->Query();
        if($db->getErrorNum()){
            JError::raiseError( 500, $db->stderr());
        }

        //Remove Group info from activity stream
        $sql = "Delete FROM " .$db->quoteName("#__community_activities"). "
                        WHERE ". $db->quoteName("groupid") . " = ".$db->Quote($gid);

        $db->setQuery($sql);
        $db->Query();
        if($db->getErrorNum()){
            JError::raiseError( 500, $db->stderr());
        }

        return true;
    }

    /**
     * Delete group's discussion
     *
     * param	string	id The id of the group.
     *
     **/
    static public function deleteGroupDiscussions($gid)
    {
        $db = JFactory::getDBO();

        $sql = "SELECT
						".$db->quoteName("id")."
				FROM
						".$db->quoteName("#__community_groups_discuss")."
				WHERE
						".$db->quoteName("groupid")." = ".$gid;
        $db->setQuery($sql);
        $row = $db->loadobjectList();
        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        if(!empty($row))
        {
            $ids_array = array();
            foreach($row as $tempid)
            {
                array_push($ids_array, $tempid->id);
            }
            $ids = implode(',', $ids_array);
        }

        $sql = "DELETE

				FROM
						".$db->quoteName("#__community_groups_discuss")."
				WHERE
						".$db->quoteName("groupid")." = ".$gid;
        $db->setQuery($sql);
        $db->Query();
        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        if(!empty($ids))
        {
            $sql = "DELETE

					FROM
							".$db->quoteName("#__community_wall")."
					WHERE
							".$db->quoteName("contentid")." IN (".$ids.") AND
							".$db->quoteName("type")." = ".$db->quote('discussions');
            $db->setQuery($sql);
            $db->Query();
            if($db->getErrorNum())
            {
                JError::raiseError( 500, $db->stderr());
            }
        }

        return true;
    }

    /**
     * Delete group's media
     *
     * param	string	id The id of the group.
     *
     **/
    static public function deleteGroupMedia($gid)
    {
        $db 			= JFactory::getDBO();
        $photosModel	= CFactory::getModel( 'photos' );
        $videoModel		= CFactory::getModel( 'videos' );
        $fileModel		= CFactory::getModel( 'files' );

        // group's photos removal.
        $albums			= $photosModel->getGroupAlbums($gid , false, false, 0);
        foreach ($albums as $item)
        {
            $photos			= $photosModel->getAllPhotos($item->id, PHOTOS_GROUP_TYPE);

            foreach ($photos as $row)
            {
                $photo	= JTable::getInstance( 'Photo' , 'CTable' );
                $photo->load($row->id);
                $photo->delete();
            }

            //now we delete group photo album folder
            $album	= JTable::getInstance( 'Album' , 'CTable' );
            $album->load($item->id);
            $album->delete();
        }

        //group's videos


        $featuredVideo	= new CFeatured(FEATURED_VIDEOS);
        $videos			= $videoModel->getGroupVideos($gid);

        foreach($videos as $vitem)
        {
            if (!$vitem) continue;

            $video		= JTable::getInstance( 'Video' , 'CTable' );
            $videaId	= (int) $vitem->id;

            $video->load($videaId);

            if($video->delete())
            {
                // Delete all videos related data
                $videoModel->deleteVideoWalls($videaId);
                $videoModel->deleteVideoActivities($videaId);

                //remove featured video
                $featuredVideo->delete($videaId);

                //remove the physical file
                $storage = CStorage::getStorage($video->storage);
                if ($storage->exists($video->thumb))
                {
                    $storage->delete($video->thumb);
                }

                if ($storage->exists($video->path))
                {
                    $storage->delete($video->path);
                }
            }

        }

        $fileModel->alldelete($gid,'group');

        return true;
    }

    /* @since 2.6
     * group category id - int
     * list of group ids - string, separate by comma
     * limit - int
     */
    public function getGroupLatestDiscussion($category = 0, $groupids = '', $limit = '')
    {
        $db 		= JFactory::getDBO();
        $config		= CFactory::getConfig();
        $mainframe	= JFactory::getApplication();

        // Get pagination request variables
        $limit  = (empty($limit)) ? $mainframe->getCfg('list_limit') : $limit;

        // Filter category
        $idswhere = '';
        if( !empty($groupids))
        {
            $idswhere = ' AND b.`id` IN (' . $groupids . ')';
        }

        $query	 = 'SELECT a.'.$db->quoteName('id').', a.'.$db->quoteName('groupid').', a.'.$db->quoteName('creator').', a.'.$db->quoteName('title').',a.'.$db->quoteName('message').', a.'.$db->quoteName('lastreplied');
        $query	.= ' FROM '.$db->quoteName('#__community_groups_discuss').' AS a ';
        $query	.= '	JOIN (';
        $query	.= '	SELECT b.'.$db->quoteName('id');
        $query	.= '	FROM '.$db->quoteName('#__community_groups').' AS b';
        $query	.= '	WHERE ';
        $query	.= '		b.'.$db->quoteName('published').' = 1';
        $query	.= '		AND ';
        $query	.= '		b.'.$db->quoteName('approvals').' = 0';
        $query	.= '		'.$idswhere;
        $query	.= '	) AS c ON c.'.$db->quoteName('id').' = a.'.$db->quoteName('groupid');

        $query  .= ' order by a.'. $db->quoteName('lastreplied'). ' desc';
        if(! empty($limit))
        {
            $query  .= ' LIMIT '. $limit;
        }

        $db->setQuery($query);
        $result = $db->loadObjectList();

        return $result;
    }

    /**
     * @since 2.6
     * @param user id
     * @return array of the latest group discussion the user has participated
     */
    public function getGroupDiscussionLastActive( $userid ){
        $db 		= JFactory::getDBO();

        //first we get the discussion that the user has participated into
        $query = "SELECT contentid FROM ".$db->quoteName('#__community_wall')." WHERE type='discussions' AND post_by=".$db->Quote($userid);
        $db->setQuery( $query );

        $discussionIds = $db->loadAssocList();
        $discussionIds = JArrayHelper::getColumn($discussionIds,'contentid');

        if(count($discussionIds) > 0){
            $discussionIds = implode(',',$discussionIds);

            //set the limit if configurable
            $query = "SELECT g.id AS discussionid, w.post_by, w.comment, w.date, g.groupid, g.title, gr.name AS group_name, w.params
						FROM ".$db->quoteName('#__community_wall')." w, ".$db->quoteName('#__community_groups_discuss')."
						g, ".$db->quoteName('#__community_groups')." gr WHERE w.contentid = g.id AND w.type='discussions'
						AND gr.id = g.groupid AND w.contentid IN (".$discussionIds.") ORDER BY w.id DESC LIMIT 9";
            $db->setQuery( $query );

            $discussionUpdates = $db->loadAssocList();

            //generate extra info for latest discussion updates
            foreach($discussionUpdates as &$disc){
                //grouplink
                $disc['group_link'] =  CRoute::_( 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $disc['groupid'] );
                $disc['discussion_link'] = CRoute::_('index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $disc['groupid'] . '&topicid=' . $disc['discussionid']);
                $date	= CTimeHelper::getDate($disc['date']);

                $disc['created_interval'] = CTimeHelper::timeLapse($date);

                $user = CFactory::getUser($disc['post_by']);
                $disc['created_by'] = $user->getDisplayName();

                $table	=  JTable::getInstance( 'Group' , 'CTable' );
                $table->load($disc['groupid']);
                $disc['group_avatar'] = $table->getThumbAvatar();
            }
            return $discussionUpdates;
        }

        return null;
    }

    /**
     * @since 2.6
     * @param user id
     * @return array of the latest group announcement the user is in
     */
    public function getGroupAnnouncementUpdate( $userid, $limit = 3 ){
        $userGroups = $this->getGroupIds( $userid );
        $albumsDetails = array();

        if($limit > 0){
            $extraSQL = ' LIMIT '.$limit;
        }

        if(count($userGroups) > 0){
            $db	= $this->getDBO();

            $groups_id = implode(',',$userGroups);
            $query	= 'SELECT b.*, g.name AS group_name FROM ' . $db->quoteName('#__community_groups_bulletins')
                . ' AS b, '.$db->quoteName('#__community_groups').' AS g WHERE b.groupid IN (' . $groups_id .' ) AND g.id=b.groupid ORDER BY b.id DESC '.$extraSQL;

            $db->setQuery( $query );

            $announcements = $db->loadAssocList();

            foreach($announcements as &$announcement){
                //grouplink
                $announcement['group_link'] =  CRoute::_( 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $announcement['groupid'] );
                $announcement['announcement_link'] = CRoute::_('index.php?option=com_community&view=groups&task=viewbulletin&groupid=' . $announcement['groupid'] . '&bulletinid=' . $announcement['id']);
                $date	= CTimeHelper::getDate($announcement['date']);

                $announcement['created_interval'] = CTimeHelper::timeLapse($date);
                $announcement['user_avatar'] = CFactory::getUser($announcement['created_by'])->getThumbAvatar();
                $announcement['user_name'] = CFactory::getUser($announcement['created_by'])->getName();
            }
        }

        return $announcements;
    }

    /*
     * @since 2.6
     * To get all the album from the group that the user participated into (only album with new photo updates)
     **/
    public function getGroupLatestAlbumUpdate( $userId, $limit = 3 ){
        $userGroups = $this->getGroupIds( $userId );
        $albumsDetails = array();

        if($limit > 0){
            $extraSQL = ' LIMIT '.$limit;
        }

        if(count($userGroups) > 0){
            $groups_id = implode(',',$userGroups);
            $db	= $this->getDBO();
            $query	= 'SELECT * FROM (SELECT a.id as photo_id, a.creator as creator_id, b.name as album_name,b.id as album_id, b.groupid FROM ' . $db->quoteName('#__community_photos') . ' AS a '
                . ', ' . $db->quoteName('#__community_photos_albums') . ' AS b '
                . ' WHERE b.groupid IN (' . $groups_id .' ) AND a.albumid = b.id ORDER BY a.id DESC) as tmp GROUP BY album_id'.$extraSQL;

            $db->setQuery( $query );

            $albumsDetails = $db->loadAssocList();

            //merge in the user posted name
            foreach($albumsDetails as &$album){
                $user = CFactory::getUser($album['creator_id']);
                $album['creator_name'] = $user->getDisplayName();

                $table	=  JTable::getInstance( 'Group' , 'CTable' );
                $table->load($album['groupid']);
                $album['group_avatar'] = $table->getThumbAvatar();
                $album['group_name'] = $table->name;

                $table	=  JTable::getInstance( 'Album' , 'CTable' );
                $table->load($album['album_id']);
                $album['album_thumb'] = $table->getCoverThumbURI();
            }
        }

        return $albumsDetails;
    }

    /*
     * @since 2.6
     * To get all events update from the group that the user participated
     **/
    public function getGroupUpcomingEvents($userId, $limit = 3){
        $userGroups = $this->getGroupIds( $userId );
        $eventsDetails = array();

        $extraSQL = '';
        if($limit > 0){
            $extraSQL = ' LIMIT '.$limit;
        }

        if(count($userGroups) > 0){
            $groups_id = implode(',',$userGroups);
            $db	= $this->getDBO();

            $now		=   CTimeHelper::getDate();

            $query = "SELECT * FROM ". $db->quoteName('#__community_events') ." WHERE ".$db->quoteName('contentid')." IN (" . $groups_id ." ) AND
					((parent = 0 AND  (".$db->quoteName('repeat')." IS NULL OR  ".$db->quoteName('repeat')." =  ''))
					OR (parent > 0 AND  ".$db->quoteName('repeat')." IS NOT NULL)) AND ".$db->quoteName('published')." = 1 AND
					".$db->quoteName('type')." =  'group' AND ". $db->quoteName('enddate')." >= " . $db->Quote( $now->toSql() )
                ." ORDER by startdate ASC ".$extraSQL;
            $db->setQuery( $query );

            $result = $db->loadObjectList();
            if(!empty($result))
            {
                foreach($result as $row)
                {
                    $event = JTable::getInstance('Event', 'CTable');
                    $event->bind($row);
                    $eventsDetails[] = $event;
                }
            }
        }
        return $eventsDetails;
    }

    /*
     * @since 2.6
     * To get all videos update from the group that the user participated
     **/
    public function getGroupVideosUpdate( $userId, $limit = 3 ){
        $userGroups = $this->getGroupIds( $userId );
        $videoDetails = array();

        if($limit > 0){
            $extraSQL = ' LIMIT '.$limit;
        }

        if(count($userGroups) > 0){
            $groups_id = implode(',',$userGroups);
            $db	= $this->getDBO();
            $query	= "SELECT * FROM " . $db->quoteName('#__community_videos')
                . " WHERE ".$db->quoteName('creator_type')."='group' AND "
                . $db->quoteName('groupid')." IN (" . $groups_id .") ORDER BY created DESC ".$extraSQL;
            $db->setQuery( $query );

            $videoDetails = $db->loadObjectList();
            $videos = array();

            if ($videoDetails)
            {
                foreach($videoDetails as $videoEntry)
                {
                    $video	= JTable::getInstance('Video','CTable');
                    $video->bind( $videoEntry );
                    $videos[]	= $video;
                }
            }
        }

        return $videos;
    }

    /**
     * Return the name of the group id
     */
    public function getGroupName( $groupid )
    {
        $session = JFactory::getSession();
        $data = $session->get('groups_name_'.$groupid);
        if($data)
        {
            return $data;
        }
        $db	= $this->getDBO();

        $query	=   'SELECT ' . $db->quoteName('name').' FROM ' . $db->quoteName('#__community_groups')
            . " WHERE " . $db->quoteName("id") . "=" . $db->Quote($groupid);

        $db->setQuery( $query );

        $name = $db->loadResult();

        $session->set('groups_name_'.$groupid, $name);
        return $name;
    }


    /**
     * @deprecated Since 2.0
     */
    public function getThumbAvatar($id, $thumb)
    {

        $thumb	= CUrlHelper::avatarURI($thumb, 'group_thumb.png');

        return $thumb;
    }


    public function getBannedMembers( $groupid, $limit=0, $randomize=false )
    {
        CError::assert( $groupid , '', '!empty', __FILE__ , __LINE__ );

        $db	    =	$this->getDBO();

        $limit	    =	($limit === 0) ? $this->getState('limit') : $limit;
        $limitstart =	$this->getState('limitstart');

        $query	    =	'SELECT a.'.$db->quoteName('memberid').' AS id, a.'.$db->quoteName('approved').' , b.'.$db->quoteName('name').' as name '
            . ' FROM '. $db->quoteName('#__community_groups_members') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__users') . ' AS b '
            . ' WHERE b.'.$db->quoteName('id').'=a.'.$db->quoteName('memberid')
            . ' AND a.'.$db->quoteName('groupid').'=' . $db->Quote( $groupid )
            . ' AND a.'.$db->quoteName('permissions').'=' . $db->Quote( COMMUNITY_GROUP_BANNED );

        if( $randomize )
        {
            $query	.=  ' ORDER BY RAND() ';
        }

        if( !is_null($limit) )
        {
            $query	.=  ' LIMIT ' . $limitstart . ',' . $limit;
        }

        $db->setQuery( $query );

        $result	    =   $db->loadObjectList();

        if( $db->getErrorNum() )
        {
            JError::raiseError( 500, $db->stderr() );
        }

        $query	    =	'SELECT COUNT(*) FROM '
            . $db->quoteName('#__community_groups_members') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__users') . ' AS b '
            . ' WHERE b.'.$db->quoteName('id').'=a.'.$db->quoteName('memberid')
            . ' AND a.'.$db->quoteName('groupid').'=' . $db->Quote( $groupid ) . ' '
            . ' AND a.'.$db->quoteName('permissions').'=' . $db->Quote( COMMUNITY_GROUP_BANNED );

        $db->setQuery( $query );
        $total		=   $db->loadResult();
        $this->total	=   $total;

        if( $db->getErrorNum() ) {
            JError::raiseError( 500, $db->stderr() );
        }

        if( empty($this->_pagination) )
        {
            jimport( 'joomla.html.pagination' );
            $this->_pagination  =	new JPagination( $total, $limitstart, $limit );
        }

        return $result;
    }

    public function getGroupsSearchTotal()
    {
        return $this->total;
    }

    static public function getGroupChildId($gid){

        $db = JFactory::getDBO();
        //CFactory::load( 'libraries' , 'activities' );
        $sql = "SELECT
						".$db->quoteName("id")."
				FROM
						".$db->quoteName("#__community_groups_discuss")."
				WHERE
						".$db->quoteName("groupid")." = ".$db->Quote($gid);
        $db->setQuery($sql);
        $row = $db->loadobjectList();
        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        $sql = "SELECT
						".$db->quoteName("id")."
				FROM
						".$db->quoteName("#__community_groups_bulletins")."
				WHERE
						".$db->quoteName("groupid")." = ".$db->Quote($gid);
        $db->setQuery($sql);
        $bulletin = $db->loadobjectList();
        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }
        $sql = "SELECT
						".$db->quoteName("id")."
				FROM
						".$db->quoteName("#__community_wall")."
				WHERE
						".$db->quoteName("contentid")." = ".$db->Quote($gid);
        $db->setQuery($sql);
        $wall = $db->loadobjectList();
        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }
        $row = array_merge($row, array_merge($bulletin,$wall));

        if(!empty($row))
        {
            $ids_array = array();
            foreach($row as $tempid)
            {
                array_push($ids_array, $tempid->id);
            }
            $ids = implode(',', $ids_array);
            $ids .= ','.$gid;
            //Remove All groupActivity stream
            CActivityStream::removeGroup($ids);
        }
    }
    public function countPending($userId)
    {

        $db = $this->getDBO();

        $query	= 'SELECT COUNT(*) FROM '
            . $db->quoteName('#__community_groups_invite') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName( '#__community_groups' ) . ' AS b ON a.'.$db->quoteName('groupid').'=b.'.$db->quoteName('id')
            . ' AND a.' .$db->quoteName('userid'). '=' . $db->Quote($userId);

        $db->setQuery($query);

        if ($db->getErrorNum())
        {
            JError::raiseError(500, $db->stderr());
        }

        return $db->loadResult();
    }

    public function getTotalNotifications( $user )
    {
        if($user->_cparams->get('notif_groups_invite'))
        {
            $privateGroupRequestCount=0;

            if($user->_cparams->get('notif_groups_member_join'))
            {
                $allGroups      =   $this->getAdminGroups( $user->id , COMMUNITY_PRIVATE_GROUP);

                foreach($allGroups as $groups)
                {
                    $member     =    $this->getMembers( $groups->id , 0, false );

                    if(!empty($member))
                    {
                        $privateGroupRequestCount += count($member);
                    }
                }
            }

            return (int) $this->countPending( $user->id ) + $privateGroupRequestCount;
        }

        return 0;
    }

    public function getAdminGroups( $userId, $privacy = NULL )
    {
        $extraSQL = NULL;
        $db		= $this->getDBO();

        if( $privacy == COMMUNITY_PRIVATE_GROUP )
        {
            $extraSQL = ' AND a.'.$db->quoteName('approvals').'=' . $db->Quote( '1' );
        }

        if( $privacy == COMMUNITY_PUBLIC_GROUP )
        {
            $extraSQL = ' AND a.'.$db->quoteName('approvals').'=' . $db->Quote( '0' );
        }
        $query	=   'SELECT a.* FROM '
            . $db->quoteName('#__community_groups') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b '
            . ' ON a.'.$db->quoteName('id').'=b.'.$db->quoteName('groupid')
            . ' AND b.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
            . ' AND b.'.$db->quoteName('permissions').'=' . $db->Quote( '1' )
            . ' AND a.'.$db->quoteName('published').'=' . $db->Quote( '1' )
            . ' AND b.'.$db->quoteName('memberid').'=' . $db->Quote($userId)
            . $extraSQL;

        $db->setQuery( $query );
        $result	= $db->loadObjectList();

        // bind to table
        $data = array();
        foreach($result as $row){
            $groupAdmin	= JTable::getInstance( 'Group' , 'CTable' );
            $groupAdmin->bind( $row );
            $data[] = $groupAdmin;
        }

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        return $data;
    }

    public function getIsCourseGroup($groupid) {
        $db           = JFactory::getDBO();
        $query = 'SELECT *' .
            ' FROM #__community_groups' .
            " WHERE id = " . $db->Quote($groupid);
        $db->setQuery($query);
        $course_group = $db->loadObject();

        //Chris: temporarily because of error
        if (!is_null($course_group)) {
            $group_param = json_decode($course_group->params);
            return (int) $group_param->course_id;
        } else {
            return null;
        }
    }

    public function getMemberRole($groupid, $memberid){
        $roles = '';
        $db = JFactory::getDBO();
        $query = 'SELECT *' .
            ' FROM #__community_groups_members' .
            ' WHERE groupid = ' . $db->Quote($groupid) . ' AND ' . 'memberid = ' . $db->Quote($memberid);
        $db->setQuery($query);
        $member = $db->loadObject();
        if($member) $roles = $member->roles;
        return $roles;
    }

    public function addSubscribeMemberToCourse($courseid,$groupid,$memberid,$act){
        $db = JFactory::getDbo();

        // Data subscrible group and member
        $query_data = $query = 'SELECT *' .
            ' FROM ' . $db->quoteName('#__community_groups_members') .
            'WHERE '. $db->quoteName('groupid') .'='. $db->Quote($groupid) .' AND '. $db->quoteName('memberid') .'='. $db->Quote($memberid) ;
        $db->setQuery($query_data);
        $data = $db->loadObject();
        $data->course_subscribe;

        // Subscribe course
        if ((int)$act == 1) {
            if(!$data){
                // ADD MEMBER
                $members = new stdClass();
                $members->groupid = $groupid;
                $members->memberid = $memberid;
                $members->approved = 1;
                $members->permissions = 0;

                $db->insertObject('#__community_groups_members', $members);
            }
            if ($data->course_subcribe == NULL) { // no course subscribe member

                $query = 'UPDATE '. $db->quoteName('#__community_groups_members') .
                    'SET '.$db->quoteName('course_subscribe').'='. $db->Quote($courseid) .
                    'WHERE '. $db->quoteName('groupid') .'='. $db->Quote($groupid) .' AND '. $db->quoteName('memberid') .'='. $db->Quote($memberid) ;
                $db->setQuery($query);
                $db->query();
            }else{
                $arrCourseId = array_map('intval', explode(',', $data->course_subscribe));
                array_push($arrCourseId, (int)$courseid);
                $felt_course_subcribe = implode(",",$arrCourseId);
                $query = 'UPDATE '. $db->quoteName('#__community_groups_members') .
                    'SET '.$db->quoteName('course_subscribe').'='. $db->Quote($felt_course_subcribe) .
                    'WHERE '. $db->quoteName('groupid') .'='. $db->Quote($groupid) .' AND '. $db->quoteName('memberid') .'='. $db->Quote($memberid) ;
                $db->setQuery($query);
                $db->query();
            }

            // UnSubscribe course
        } else {
            
             //remove member
             if($data){
                $datamem = new stdClass();

                $datamem->groupid = $groupid;
                $datamem->memberid = $memberid;
                
                $this->removeMember($datamem);
             }
            if ($data->course_subcribe != NULL) {
                $arrCourseId = array_map('intval', explode(',', $data->course_subscribe));
                $arrCourseIdNew = array();

                for ($i = 0; $i < count($arrCourseId); $i++) {
                    if ($arrCourseId[$i] != (int)$courseid) {
                        array_push($arrCourseIdNew, $arrCourseId[$i]);
                    }
                }
                $felt_course_unsubcribe = implode(",", $arrCourseIdNew);

                $query = 'UPDATE ' . $db->quoteName('#__community_groups_members') .
                    'SET ' . $db->quoteName('course_subscribe') . '=' . $db->Quote($felt_course_unsubcribe) .
                    'WHERE ' . $db->quoteName('groupid') . '=' . $db->Quote($groupid) . ' AND ' . $db->quoteName('memberid') . '=' . $db->Quote($memberid);
                $db->setQuery($query);
                $db->query();
            }
        }


        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }
        return true;
    }


    public function getPublishCoursesToSocial($groupid)
    {
        $db = JFactory::getDBO();
        $query = 'SELECT *' .
            ' FROM #__course_shareto_group' .
           " WHERE groupid = " . $db->Quote($groupid) . ' AND '.$db->quoteName( 'status_unpulished' ).' = 0';
        $db->setQuery($query);

        $result = $db->loadObjectList();
        return $result;
    }

    public function UnpublishCourseFromSocial($courseid, $groupid)
    {
        $db = $this->getDBO();

        // Check if course public with one circle ( unpublic course at circle => unpublic course at manage)
        $query = 'SELECT *' .
            ' FROM #__course_shareto_group' .
            " WHERE courseid = " . $db->Quote($courseid);
        $db->setQuery($query);
        $result = $db->loadObjectList();

        $count_group_public_course = count($result);
        $check_courseid_shared_groupid = false;


        for ($i = 0; $i < $count_group_public_course; $i++) {
            if ($result[$i]->groupid == $groupid) {
                $check_courseid_shared_groupid = true;
            }
        }

// Course share to one circle

        if ($count_group_public_course == 1 && $check_courseid_shared_groupid == true) {
            $my = CFactory::getUser();
            $username = $my->username;
            require_once(JPATH_SITE . '/components/com_joomdle/helpers/content.php');
            $r = JoomdleHelperContent::call_method('course_enable_self_enrolment', (int)$courseid, 'unenable', $username);
//            $strSQL = 'DELETE FROM ' . $db->quoteName('#__course_shareto_group') . ' '
//                . 'WHERE ' . $db->quoteName('groupid') . '=' . $db->Quote($groupid) . ' '
//                . 'AND ' . $db->quoteName('courseid') . '=' . $db->Quote($courseid);
//
//            $db->setQuery($strSQL);
//            $db->query();

            if ($groupid) {
                $sql = "UPDATE " . $db->quoteName("#__course_shareto_group") ." SET "
                    . $db->quoteName( 'status_unpulished' ) . '=' . $db->Quote( '1' )
                    . " WHERE " . $db->quoteName("courseid") . " = " . $db->quote($courseid)
                    . " AND " . $db->quoteName("groupid") . " = " . $db->quote($groupid);
            } else {
                $sql = "UPDATE " . $db->quoteName("#__course_shareto_group") . " SET "
                    . $db->quoteName( 'status_unpulished' ) . '=' . $db->Quote( '1' )
                    . " WHERE " . $db->quoteName("courseid") . " = " . $db->quote($courseid);
            }

            $db->setQuery($sql);
            $db->Query();
            if($db->getErrorNum()){
                JError::raiseError( 500, $db->stderr());
            }

            // unenroll course with all member
            $query_member_of_group = 'SELECT *' .
                ' FROM #__community_groups_members' .
                " WHERE groupid =" . $db->Quote($groupid);
            $db->setQuery($query_member_of_group);
            $result_member_of_group = $db->loadObjectList();
            $count_member_of_group = count($result_member_of_group);

            for ($j = 0; $j < $count_member_of_group; $j++) {
                $user = CFactory::getUser($result_member_of_group[$j]->memberid);
                if ($user->username != $username) {
                    $response = JoomdleHelperContent::call_method('unenrol_user', $user->username, (int)$courseid);
                }
            }

            if ($db->getErrorNum()) {
                JError::raiseError(500, $db->stderr());
            }

        }

// Course share to many circle(2 circle up to)
        if ($count_group_public_course >= 2) {
            $strSQL = 'DELETE FROM ' . $db->quoteName('#__course_shareto_group') . ' '
                . 'WHERE ' . $db->quoteName('groupid') . '=' . $db->Quote($groupid) . ' '
                . 'AND ' . $db->quoteName('courseid') . '=' . $db->Quote($courseid);

            $db->setQuery($strSQL);
            $db->query();

            if ($db->getErrorNum()) {
                JError::raiseError(500, $db->stderr());
            }
        }


        return true;
    }

    public function isCourseExistFromShareGroup($courseid){
        $db           = JFactory::getDBO();
        $query = 'SELECT *' .
            ' FROM #__course_shareto_group' .
            " WHERE courseid = " . $db->Quote($courseid);
        $db->setQuery($query);
        $result	= $db->loadObjectList();
        return $result;
    }

    public function checkProductInHikashop($productid){
        $db = JFactory::getDBO();
        $query = 'SELECT *' .
            ' FROM #__hikashop_product' .
            ' WHERE product_code = ' . $db->Quote($productid) . ' '
            . 'AND ' . $db->quoteName('product_published') . '=' . $db->Quote(1);
        $db->setQuery($query);
        $product = $db->loadObject();
        return $product;
    }



    /*
     * Begin return data for app
     */
    public function getGroupsMobile( $userId = null , $sorting = null , $useLimit = true, $search = null, $parentid = 0)
    {
        $db		= $this->getDBO();

        $extraSQL	= '';

        if( !is_null($userId) )
        {
            $extraSQL	= ' AND b.memberid=' . $db->Quote($userId);
        }
        if( !is_null($search) )
        {
            $extraSQL	.= ' AND a.'.$db->quoteName('name').' LIKE ' . $db->Quote( '%' . $search . '%' ) . ' ';
        }

        if($parentid) {
            $extraSQL .= 'AND a.'.$db->quoteName('parentid').' = '.$db->Quote($parentid);

        } else {
            $extraSQL .= 'AND a.'.$db->quoteName('parentid').' = '.$db->Quote(0);
        }

        $orderBy	= '';

        $limitSQL = '';
        $total		= 0;
        $limit		= $this->getState('limit');
        $limitstart = $this->getState('limitstart');
        if($useLimit){
            $limitSQL	= ' LIMIT ' . $limitstart . ',' . $limit ;
        }

        switch($sorting)
        {
            case 'mostmembers':
                // Get the groups that this user is assigned to
                $query		= 'SELECT a.'.$db->quoteName('id').' FROM ' . $db->quoteName('#__community_groups') . ' AS a '
                    . ' LEFT JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b '
                    . ' ON a.'.$db->quoteName('id').'=b.'.$db->quoteName('groupid')
                    . ' WHERE b.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
                    . $extraSQL;

                $db->setQuery( $query );
                $groupsid		= $db->loadColumn();

                if($db->getErrorNum())
                {
                    JError::raiseError( 500, $db->stderr());
                }

                if( $groupsid )
                {
                    $groupsid		= implode( ',' , $groupsid );

                    $query			= 'SELECT a.*'
                        . 'FROM ' . $db->quoteName('#__community_groups') . ' AS a '
                        . ' WHERE a.'.$db->quoteName('published').'=' . $db->Quote( '1' )
                        . ' AND a.'.$db->quoteName('id').' IN (' . $groupsid . ') '
                        . ' ORDER BY a.'.$db->quoteName('membercount').' DESC '
                        . $limitSQL;
                }
                break;
            case 'mostactive':
                $query	= 'SELECT *, (a.' . $db->quoteName('discusscount') . ' + a.' . $db->quoteName('wallcount') . ' ) AS count FROM ' . $db->quoteName('#__community_groups') . ' as a'
                    . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b ON a.' . $db->quoteName('id') . '= b.' . $db->quoteName('groupid')
                    . ' WHERE a.' . $db->quoteName('published') . ' = ' . $db->Quote('1')
                    . $extraSQL
                    . ' GROUP BY a.' . $db->quoteName('id')
                    . ' ORDER BY count DESC '
                    . $limitSQL;
                break;
            case 'mostdiscussed':
                if( empty($orderBy) )
                    $orderBy	= ' ORDER BY a.'.$db->quoteName('discusscount').' DESC ';
            case 'mostwall':
                if( empty($orderBy) )
                    $orderBy	= ' ORDER BY a.'.$db->quoteName('wallcount').' DESC ';
            case 'alphabetical':
                if( empty($orderBy) )
                    $orderBy	= 'ORDER BY a.'.$db->quoteName('name').' ASC ';
            default:
                if( empty($orderBy) )
                    $orderBy	= ' ORDER BY b.lastaccess DESC ';

                $query	= 'SELECT a.id, a.ownerid, a.name, a.description, a.summary,a.created, a.avatar, a.membercount, a.discusscount, a.params, b.lastaccess, a.parentid, a.unlisted, a.approvals FROM '
                    . $db->quoteName('#__community_groups') . ' AS a '
                    . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b '
                    . ' ON a.'.$db->quoteName('id').'=b.'.$db->quoteName('groupid')
                    . ' AND b.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
                    . ' AND a.'.$db->quoteName('published').'=' . $db->Quote( '1' ) . ' '
                    . $extraSQL
                    . $orderBy
                    . $limitSQL;
                break;
        }

//                print_r($query);die;
        $db->setQuery( $query );

        $result	= $db->loadObjectList();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        $query	= 'SELECT COUNT(*) FROM ' . $db->quoteName('#__community_groups') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b '
            . ' WHERE a.'.$db->quoteName('id').'=b.'.$db->quoteName('groupid')
            . ' AND a.'.$db->quoteName('published').'=' . $db->Quote( '1' ) . ' '
            . ' AND b.'.$db->quoteName('approved').'=' . $db->Quote( '1' )
            . $extraSQL;

        $db->setQuery( $query );
        $total	= $db->loadResult();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        if( empty($this->_pagination) )
        {
            jimport('joomla.html.pagination');

            $this->_pagination	= new JPagination( $total , $limitstart , $limit );
        }
        $my = CFactory::getUser();

        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        if(!empty($result)){
            foreach($result as $k => $r){
                $isMember = $this->isMember($my->id, $r->id);
                $result[$k]->ismemberin = $isMember;
                // remove tag html in description
                $result[$k]->description =  strip_tags($r->description);
                // avatar get image default
                if ($r->avatar == '') {
                    $result[$k]->avatar = JURI::base() . 'components/com_community/assets/group.png';
                } else {
                    $result[$k]->avatar = $root . '/' . $r->avatar;
                }
            }
        }
        return $result;
    }

    public function getAllGroupsMobile( $categoryId = null , $sorting = null , $search = null , $limit = null , $skipDefaultAvatar = false , $hidePrivateGroup = false, $pagination = true, $nolimit = false, $position=NULL, $listgroups=false )
    {
        $db		= $this->getDBO();

        $extraSQL	= '';
        $pextra		= '';

        if( is_null( $limit ) )
        {
            $limit		= $this->getState('limit');
        }
        $limit	= ($limit < 0) ? 0 : $limit;
        $limitstart = $this->getState('limitstart');
        // Test if search is parsed
        if( !is_null( $search ) )
        {
//                    $words = mysql_real_escape_string( $search );
            $words = explode( ' ', $search );
            $wheres2 = array();
            $wheres = array();
            foreach ($words as $word)
            {
                $text_s           = '\'%'.$word.'%\'';
                //                  $word           = '\'%'.$DB->search_escape_string( $word).'%\'';
                $wheres2[]      = 'a.name LIKE '.$text_s;
//                        $wheres2[]      = 'a.description LIKE '.$text_s;
//                        $wheres2[]      = 'a.summary LIKE '.$text_s;
                $wheres2[]      = 'a.keyword LIKE '.$text_s;
                $wheres[]       = implode( ' OR ', $wheres2 );
            }
//                    $words = stripslashes( $search );
            $extraSQL	.= ' AND ('.$wheres[0] .') ';

        }

        if( $skipDefaultAvatar )
        {
            $extraSQL	.= ' AND ( a.'.$db->quoteName('thumb').' != ' . $db->Quote( DEFAULT_GROUP_THUMB ) . ' AND a.'.$db->quoteName('avatar').' != ' . $db->Quote( DEFAULT_GROUP_AVATAR ) . ' )';
        }
        if(!is_array($categoryId) && !empty($categoryId)) {
            $extraSQL .= ' AND a. '.$db->QuoteName('categoryid').' IN ('. $db->Quote($categoryId).' )';
        }
        /*
         * remove for search BLN
         * updated 05 May 2018
         */
        // $extraSQL .= ' AND a.'.$db->quoteName('params').' NOT LIKE '.$db->Quote('%\"course_id\":%').' ';
        $extraSQL .= ' AND a.'.$db->quoteName('parentid').' <> '.$db->quote(0).' ';
        $order	='';
        switch ( $sorting )
        {
            case 'alphabetical':
                $order		= ' ORDER BY a.'.$db->quoteName('name').' ASC ';
                break;
            case 'mostdiscussed':
                $order	= ' ORDER BY '.$db->quoteName('discusscount').' DESC ';
                break;
            case 'mostwall':
                $order	= ' ORDER BY '.$db->quoteName('wallcount').' DESC ';
                break;
            case 'mostmembers':
                $order	= ' ORDER BY '.$db->quoteName('membercount').' DESC ';
                break;
            default:
                $order	= 'ORDER BY a.'.$db->quoteName('created').' DESC ';
                break;
// 			case 'mostactive':
// 				$order	= ' ORDER BY count DESC';
// 				break;
        }

        // not use for get group by category id
        /*
if( !is_null($categoryId) && $categoryId != 0 )
{
            if (is_array($categoryId)) {
                if (count($categoryId) > 0) {
                    $categoryIds = implode(',', $categoryId);
                    $extraSQL .= ' AND a.' . $db->quoteName('categoryid'). ' IN(' . $categoryIds . ')';
                }
            } else {
                $extraSQL .= ' AND a.'.$db->quoteName('categoryid').'=' . $db->Quote($categoryId) . ' ';
            }
}*/


        if ($sorting == 'mostactive')
        {
            $query = ' SELECT *, ' .$db->quoteName('cid').', COUNT('.$db->quoteName('cid').') AS '.$db->quoteName('count')
                . ' FROM '.$db->quoteName('#__community_activities').' AS qx'
                . ' INNER JOIN	'.$db->quoteName('#__community_groups').' AS a ON qx.'.$db->quoteName('cid').' = a.'.$db->quoteName('id')
                . ' WHERE qx.'.$db->quoteName('app').' = '.$db->quote('groups')
                . ' AND a.'.$db->quoteName('published').' = '.$db->quote('1')
                . ' AND qx.'.$db->quoteName('archived').' = '.$db->quote('0')
                . ' AND qx.'.$db->quoteName('cid').' != '.$db->quote('0')
                . $extraSQL
                . ' GROUP BY qx.'.$db->quoteName('cid')
                . ' ORDER BY '.$db->quoteName('count').' DESC'
                . ' LIMIT '.$limitstart .' , '.$limit;
        }
        else
        {
            $query = 'SELECT a.id, a.ownerid, a.name, a.description, a.summary,a.created, a.avatar, a.membercount, a.discusscount, a.lastaccess, a.moodlecategoryid, a.params, a.published, a.unlisted, a.approvals, a.categoryid FROM '.$db->quoteName('#__community_groups').' as a WHERE a.'.$db->quoteName('published').' = '.$db->Quote('1') .' AND ((a.'.$db->quoteName('approvals').' = '.$db->Quote(0).' AND a.'.$db->quoteName('unlisted').' = '.$db->Quote(0).') OR ('.$db->quoteName('approvals').'='.$db->Quote(1).' AND '.$db->quoteName('unlisted').' = '.$db->Quote(0).')) '
                . $extraSQL
                . $order;
        }
            $query.="LIMIT  $position , $limit";

        $db->setQuery( $query );
        $rows	= $db->loadObjectList();
        $my = CFactory::getUser();
        $mainframe = JFactory::getApplication();
        $root = $mainframe->getCfg('wwwrootfile');
        if(!empty($rows)){
            //count members, some might be blocked, so we want to deduct from the total we currently have
            foreach($rows as $k => $r){
                $query = "SELECT COUNT(*)
						  FROM #__community_groups_members a
						  JOIN #__users b ON a.memberid=b.id
						  WHERE `approved`='1' AND b.block=0 AND groupid=".$db->Quote($r->id);
                $db->setQuery( $query );
                $rows[$k]->membercount = $db->loadResult();

                $isMember = $this->isMember($my->id, $r->id);
                $rows[$k]->ismemberin = $isMember;
                // remove tag html in description
                $rows[$k]->description =  strip_tags($r->description);
                // avatar get image default
                if($r->avatar == '') {

                    $rows[$k]->avatar = JURI::base().'components/com_community/assets/group.png';
                } else {
                    $rows[$k]->avatar = $root.'/'.$r->avatar;

                }
            }
        }

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        $query	= 'SELECT COUNT(*) FROM '.$db->quoteName('#__community_groups').' AS a '
            . 'WHERE a.'.$db->quoteName('published').'=' . $db->Quote( '1' )
            . $extraSQL;

        $db->setQuery( $query );
        $this->total	=  $db->loadResult();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        return $rows;
    }

    // get own groups
    public function getOwnGroups( $userId )
    {
        // guest obviously has no group
        if($userId == 0)
        {
            return 0;
        }

        $db		= $this->getDBO();
        $extraSQL	= ' AND b.memberid=' . $db->Quote($userId) . ' AND a.ownerid=' . $db->Quote($userId) . ' AND a.categoryid=' . $db->Quote(0);
        $extraSQL .= ' AND a.'.$db->quoteName('params').' NOT LIKE '.$db->Quote('%\"course_id\":%').' ';

        $query	= 'SELECT * FROM '. $db->quoteName('#__community_groups').' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b'
            . ' ON a.'.$db->quoteName('id').'=b.'.$db->quoteName('groupid')
            . 'WHERE a.'.$db->quoteName( 'ownerid' ) . '=' . $db->Quote( $userId ) . ' '
            . 'AND a.'.$db->quoteName( 'published' ). '='. $db->Quote( '1' )
            . $extraSQL;

        $db->setQuery( $query );
        $result	= $db->loadObjectList();

        return $result;
    }

    // get groups share
    public function getShareGroups( $courseid, $groupid = false )
    {
        // guest obviously has no course
        if($courseid == 0)
        {
            return 0;
        }

        $db		= $this->getDBO();

        $where = 'WHERE '.$db->quoteName( 'courseid' ) . '=' . $db->Quote( $courseid );
        if ($groupid) {
            $where .= "  AND ".$db->quoteName( 'groupid' ) . '=' . $db->Quote( $groupid );
        }
        $query	= 'SELECT * FROM '. $db->quoteName('#__course_shareto_group').' as g INNER JOIN '.$db->quoteName('#__community_groups') .' as gr ON g.groupid = gr.id '
            . $where;

        $db->setQuery( $query );
        $result	= $db->loadObjectList();

        return $result;
    }

    // get name groups share
    public function getShareNameGroups($courseid, $groupid = false)
    {
        // guest obviously has no course
        if ($courseid == 0) {
            return 0;
        }

        $db = $this->getDBO();
        $query = 'SELECT * FROM ' . $db->quoteName('#__course_shareto_group') . ' as a INNER JOIN ' . $db->quoteName('#__community_groups') . ' as b ON a.courseid= ' . $db->Quote($courseid) . ' AND a.groupid = b.id';
        $db->setQuery($query);
        $result = $db->loadObjectList();

        return $result;
    }

    // get share course to group again
    public function getShareGroupsAgain($courseid, $groupid = false)
    {
        $db = JFactory::getDBO();

//        $query	= 'UPDATE ' . $db->quoteName( '#__community_groups_members' ) . ' SET '
//            . $db->quoteName( 'approved' ) . '=' . $db->Quote( '-1' ) . ', '
//            . $db->quoteName( 'date' ) . '=' . $db->Quote( $time ) . ' '
//            . 'WHERE ' . $db->quoteName( 'groupid' ) . '=' . $db->Quote( $groupid ) . ' '
//            . 'AND ' . $db->quoteName( 'memberid' ) . '=' . $db->Quote( $userid );


        if ($groupid) {
            $sql = "UPDATE " . $db->quoteName("#__course_shareto_group") . " SET "
                . $db->quoteName('status_unpulished') . '=' . $db->Quote('0')
                . " WHERE " . $db->quoteName("courseid") . " = " . $db->quote($courseid)
                . " AND " . $db->quoteName("groupid") . " = " . $db->quote($groupid);
        } else {
            $sql = "UPDATE " . $db->quoteName("#__course_shareto_group") . " SET "
                . $db->quoteName('status_unpulished') . '=' . $db->Quote('0')
                . " WHERE " . $db->quoteName("courseid") . " = " . $db->quote($courseid);
        }

        $db->setQuery($sql);
        $db->Query();
        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }

        return true;
    }

    // insert course share to group
    public function courseShareToGroup( $courseid, $groupid )
    {
        if($courseid == 0 || $groupid == 0)
        {
            return 0;
        }

        $db		= $this->getDBO();
        // update when do task archive circle
        $coursecircleid = $this->getLearningCircleId($courseid);
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($coursecircleid);

        if($group->published == 0){
            $group->published = 1;
        }
        $group->store();
        $time = time();

        $query	= 'INSERT INTO ' . $db->quoteName( '#__course_shareto_group' )
            . ' (' . $db->quoteName( 'courseid' ) . ', ' . $db->quoteName( 'groupid' ) . ', '. $db->quoteName( 'date' )
            . ') VALUES (' . $db->Quote( $courseid ) . ',' . $db->Quote( $groupid ) . ',' . $db->Quote( $time ) . ' )';

        $db->setQuery( $query );
        $db->query();

        // Enroll member to course if member is subscribed before.
        $query_data_group_member = $query = 'SELECT *' .
            ' FROM ' . $db->quoteName('#__community_groups_members') .
            'WHERE '. $db->quoteName('groupid') .'='. $db->Quote($groupid);
        $db->setQuery($query_data_group_member);
        $members_subscribed_course_before = $db->loadObjectList();

        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');

        foreach($members_subscribed_course_before as $member_subscribed_course_before){
            if($member_subscribed_course_before->course_subscribe != NULL){
                $arrCourseId = array_map('intval', explode(',', $member_subscribed_course_before->course_subscribe));
                for($i=0;$i<count($arrCourseId);$i++){
                    if($arrCourseId[$i] != 0){
                        $learner = 5;
                        $user = CFactory::getUser($member_subscribed_course_before->memberid);
                        $response = JoomdleHelperContent::call_method('enrol_user', $user->username, (int)$arrCourseId[$i], (int)$learner);
                    }
                }

            }
        }
        // Enroll member to course if member is subscribed before.

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }
    }

    // delete course share to group
    public function deleteCourseShareToGroup($courseid, $groupid = false)
    {
        $db = JFactory::getDBO();

//        if ($groupid) {
//            $sql = "DELETE FROM " . $db->quoteName("#__course_shareto_group")
//                . " WHERE " . $db->quoteName("courseid") . " = " . $db->quote($courseid)
//                . " AND " . $db->quoteName("groupid") . " = " . $db->quote($groupid);
//        } else {
//            $sql = "DELETE FROM " . $db->quoteName("#__course_shareto_group")
//                . " WHERE " . $db->quoteName("courseid") . " = " . $db->quote($courseid);
//        }

        if ($groupid) {
            $sql = "UPDATE " . $db->quoteName("#__course_shareto_group") ." SET "
                . $db->quoteName( 'status_unpulished' ) . '=' . $db->Quote( '1' )
                . " WHERE " . $db->quoteName("courseid") . " = " . $db->quote($courseid)
                . " AND " . $db->quoteName("groupid") . " = " . $db->quote($groupid);
        } else {
            $sql = "UPDATE " . $db->quoteName("#__course_shareto_group") . " SET "
                . $db->quoteName( 'status_unpulished' ) . '=' . $db->Quote( '1' )
                . " WHERE " . $db->quoteName("courseid") . " = " . $db->quote($courseid);
        }

        $db->setQuery($sql);
        $db->Query();
        if($db->getErrorNum()){
            JError::raiseError( 500, $db->stderr());
        }

        return true;
    }

    public function isCourseHas($courseid,  $varrole, $username = ''){

        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');

        if($username != ''){
            $role = JoomdleHelperContent::call_method ('get_user_role', $courseid, $username);
        }else{
            $role = JoomdleHelperContent::call_method ('get_user_role', $courseid);
        }

        $userroles = array();

        foreach ($role['roles'] as $value) {
            $user_roles = $value['role'];
            $decode_user_roles = json_decode($user_roles);

            foreach ($decode_user_roles as $dur) {
                array_push($userroles, $dur->sortname);
            }
        }

        if(in_array($varrole, $userroles)) {
            return true;
        }else{
            return false;
        }


    }

    public function isSocialCourse($courseid){
        $db		= $this->getDBO();

        $query	= 'SELECT b.categoryid FROM ' . $db->quoteName('#__course_shareto_group') . ' AS a '
            . ' INNER JOIN ' . $db->quoteName('#__community_groups') . ' AS b '
            . ' WHERE a.'.$db->quoteName('groupid').'= b.'.$db->quoteName('id')
            . ' AND a.'.$db->quoteName('courseid').'=' . $courseid;

        $db->setQuery( $query );
        $db->query();
        $result	= $db->loadObjectList();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        $response = false;
        if(!empty($result) && $result[0]->categoryid != 7){
            $response = true;
        }

        return $response;
    }
    public function getLPGroupId($moodlecategoryid) {
        $db		= $this->getDBO();
        $query		= 'SELECT a.'.$db->quoteName('id').' FROM ' . $db->quoteName('#__community_groups') . ' AS a '
            . ' WHERE a.'.$db->quoteName('moodlecategoryid').'=' . $db->Quote( $moodlecategoryid );

        $db->setQuery( $query );
        $groupid = $db->loadRow();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        return $groupid[0];
    }

    public function getCircleCategory($circleid){
        $db		= $this->getDBO();

        $query	= 'SELECT categoryid FROM '. $db->quoteName('#__community_groups')
            . 'WHERE '.$db->quoteName( 'id' ) . '=' . $db->Quote($circleid);

        $db->setQuery( $query );
        $db->query();
        $result	= $db->loadObjectList();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        return $result;
    }

    public function getGlobalUserGroupID($title){
        $db		= $this->getDBO();

        $query	= 'SELECT id FROM '. $db->quoteName('#__usergroups')
            . 'WHERE '.$db->quoteName( 'title' ) . '=' . $db->Quote($title);

        $db->setQuery( $query );
        $db->query();
        $result	= $db->loadObjectList();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        return $result;
    }

    public function getUserStatus($userid) {
        $db = $this->getDBO();

        $query = 'SELECT ' . $db->quoteName('status') . ' FROM ' . $db->quoteName('#__community_users')
            . ' WHERE ' . $db->quoteName('userid') . ' = ' . $db->Quote($userid);

        $db->setQuery($query);

        $result = $db->loadObjectList();

        return $result;
    }

    public function getLearningCircleId($courseid) {
        $db		= $this->getDBO();
        $query		= 'SELECT '.$db->quoteName('id').' FROM ' . $db->quoteName('#__community_groups')
            . ' WHERE '.$db->quoteName('params').' LIKE ' . $db->Quote( '%'.$courseid.'%' ) ;

        $db->setQuery( $query );
        $groupids = $db->loadObjectList();

        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }

        $learningCircleId = 0;
        foreach ($groupids as $groupid) {
            $group = $this->getGroup($groupid->id);
            $params = json_decode($group->params, true);
            if ($params['course_id'] == $courseid) {
                $learningCircleId = $group->id;
                break;
            }
        }

        return $learningCircleId;
    }

    public function getLearningCircle($courseid) {
        $learningCircle = $this->getGroup($this->getLearningCircleId($courseid));

        return $learningCircle;
    }

    public function removeRequest($groupid, $userid) {
        $db = $this->getDBO();
        $time = time();

        $query	= 'UPDATE ' . $db->quoteName( '#__community_groups_members' ) . ' SET '
            . $db->quoteName( 'approved' ) . '=' . $db->Quote( '-1' ) . ', '
            . $db->quoteName( 'date' ) . '=' . $db->Quote( $time ) . ' '
            . 'WHERE ' . $db->quoteName( 'groupid' ) . '=' . $db->Quote( $groupid ) . ' '
            . 'AND ' . $db->quoteName( 'memberid' ) . '=' . $db->Quote( $userid );

        $db->setQuery( $query );
        $db->query();
        if($db->getErrorNum())
        {
            JError::raiseError( 500, $db->stderr());
        }
        return true;
    }

    public function checkBLNCircle($userid) {
        $db = $this->getDBO();
        $query = 'SELECT g.*, m.permissions FROM '.$db->quoteName('#__community_groups').' AS g'
            .' JOIN '.$db->quoteName('#__community_groups_members').' AS m ON m.groupid = g.id'
            .' WHERE m.'.$db->quoteName('memberid').'='.$db->quote($userid).' AND g.' .$db->quoteName('isBLN').' = 1';
        $db->setQuery( $query );
        $result = $db->loadObject();
        return $result;
    }

    public function getBLNCircle() {
        $db = $this->getDBO();
        $query = 'SELECT * FROM '.$db->quoteName('#__community_groups')
            . ' WHERE '.$db->quoteName('isBLN').'= 1';
        $db->setQuery( $query );
        $result = $db->loadObject();

        return $result;
    }

    public function circleBLNTree($parentid, $userid) {
        $db = $this->getDBO();
        $query = 'SELECT DISTINCT g.id, g.name, g.description, g.origimage, g.thumb, g.parentid, g.avatar, g.ownerid FROM ' . $db->quoteName('#__community_groups') . ' AS g'
            . ' LEFT JOIN ' . $db->quoteName('#__community_groups_members') . ' AS m ON m.groupid = g.id'
            . ' WHERE g.' . $db->quoteName('parentid') . '=' . $db->quote($parentid)
            . ' AND g.' . $db->quoteName('published') . '=' . $db->quote(1)
            . ' AND (g.' . $db->quoteName('approvals') . '=' . $db->Quote('0')
            . ' OR ('
            . ' m.' . $db->quoteName('approved') . '=' . $db->Quote('1')
//            . ' AND m.' . $db->quoteName('memberid') . '=' . $db->Quote($userid)
            . ' AND m.' . $db->quoteName('permissions') . '!=' . $db->Quote(COMMUNITY_GROUP_BANNED)
            . ') '
            . ')';
        $and = ' AND ((m.memberid = ' . $db->Quote($userid).' AND g.approvals = 1 AND g.unlisted = 1) || (g.approvals = 1 AND g.unlisted = 0) || (g.approvals = 0 AND g.unlisted = 0))';
        $db->setQuery($query . $and);
        $list = $db->loadObjectList();
        return $list;

    }

    public function addUserToCircle($userid, $circleid) {
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($circleid);

        if ($group->id) {
            $member = JTable::getInstance('GroupMembers', 'CTable');
            $gMember = CFactory::getUser($userid);

            $member->groupid = $group->id;
            $member->memberid = $gMember->id;
            $member->approved = 1;
            $member->permissions = '0';
            $member->store();
            $group->updateStats();
            $group->store();

            $gMember->updateGroupList(true);

            return true;
        } else
            return false;
    }

    public function getGrandFatherCircle($parentid, $userid)
    {
        $db = $this->getDBO();
        $query = 'SELECT g.id, g.name, g.description, g.origimage, g.thumb, g.parentid, g.avatar, g.ownerid, g.approvals, g.unlisted, g.published FROM ' . $db->quoteName('#__community_groups') . ' AS g'
            . ' LEFT JOIN ' . $db->quoteName('#__community_groups_members') . ' AS m ON m.groupid = g.id'
            . ' WHERE g.' . $db->quoteName('id') . '=' . $db->quote($parentid)
//            . ' AND g.' . $db->quoteName('published') . '=' . $db->quote(1)
            . ' AND (g.' . $db->quoteName('approvals') . '=' . $db->Quote('0')
            . ' OR ('
            . ' m.' . $db->quoteName('approved') . '=' . $db->Quote('1')
            . ' AND m.' . $db->quoteName('memberid') . '=' . $db->Quote($userid)
            . ' AND m.' . $db->quoteName('permissions') . '!=' . $db->Quote(COMMUNITY_GROUP_BANNED)
            . ') '
            . ')';
        $db->setQuery($query);
        $cfather = $db->loadObject();
        if($cfather) {
            if($cfather->parentid != 0 && $cfather->id = $parentid) {
                $query = 'SELECT g.id, g.name, g.description, g.origimage, g.thumb, g.parentid, g.avatar, g.ownerid, g.approvals, g.unlisted, g.published FROM ' . $db->quoteName('#__community_groups') . ' AS g'
                    . ' LEFT JOIN ' . $db->quoteName('#__community_groups_members') . ' AS m ON m.groupid = g.id'
                    . ' WHERE g.' . $db->quoteName('id') . '=' . $db->quote($cfather->parentid)
//                    . ' AND g.' . $db->quoteName('published') . '=' . $db->quote(1)
                    . ' AND (g.' . $db->quoteName('approvals') . '=' . $db->Quote('0')
                    . ' OR ('
                    . ' m.' . $db->quoteName('approved') . '=' . $db->Quote('1')
                    . ' AND m.' . $db->quoteName('memberid') . '=' . $db->Quote($userid)
                    . ' AND m.' . $db->quoteName('permissions') . '!=' . $db->Quote(COMMUNITY_GROUP_BANNED)
                    . ') '
                    . ')';
                $db->setQuery($query);
                $grand = $db->loadObject();
            } else {
                $grand = new stdClass();
            }
        }
        $query_count = 'SELECT COUNT(*) FROM '.$db->quoteName('#__community_groups').' AS c'
            .' WHERE c.parentid = '.$db->Quote($parentid);
        $db->setQuery($query_count);
        $count = $db->loadResult();

        return array(
            'currentCircle' => $cfather,
            'parentCircle' => $grand,
            'haveSibling' => $count
        );
    }
    public function changePrivacyToClose($parentId, $path){
        $db = $this->getDBO();
        if($parentId){
            $query	= 'UPDATE ' . $db->quoteName( '#__community_groups' ) . ' '
                . 'SET ' . $db->quoteName( 'approvals' ) . '=' . $db->Quote(1) . ' ,'
                . $db->quoteName( 'unlisted' ) . '=' . $db->Quote(1)
                . 'WHERE ' . $db->quoteName( 'path' ) . ' like ' . $db->Quote( '%' . $path . '/%' );
            $db->setQuery( $query );
            $db->query( $query );
            return true;
        }
        else return false;
    }

    public function buildPath($parentid, $groupid) {
        $db = $this->getDBO();
        $my = CFactory::getUser();
        $parent = $this->getGrandFatherCircle($parentid, $my->id);
        if($parentid == 0) {
            $path = '/'.$groupid;
        }else {
            if(!empty($parent['parentCircle']->id)) {
                $path = '/'.$parent['parentCircle']->id.'/'.$parentid.'/'.$groupid;
            } else {
                $path = '/'.$parentid.'/'.$groupid;
            }
        }
        $query	= 'UPDATE ' . $db->quoteName( '#__community_groups' ) . ' '
            . 'SET ' . $db->quoteName( 'path' ) . '=' . $db->Quote($path)
            . 'WHERE ' . $db->quoteName( 'id' ) . ' = ' . $db->Quote( $groupid );
        $db->setQuery( $query );
        $db->query( $query );
        return true;
    }

    public function getAllCircleBLN($blnid, $limitstart = false, $limit = false, $scircle = '',$publish = false) {
        $db = $this->getDBO();
        
        $extraSQL = '';
        if ($scircle != '') {
            $text_search = '\'%' . $scircle . '%\'';
            $wheres2 = array();
            $wheres2[] = ' a.name LIKE ' . $text_search;
            $wheres2[] = ' a.keyword LIKE ' . $text_search;
            
            $wheres = implode(' OR ', $wheres2);
            $extraSQL .= ' AND ('.$wheres.')';
        }
        $sqllimit = '';
        if ($limit) {
            $sqllimit = " LIMIT $limitstart, $limit ";
        }
        if(!$publish) $qpublish = ' AND a.' . $db->quoteName('published') . '=' . $db->quote(1);
        $query = 'SELECT a.*, ROUND ((LENGTH (a.' . $db->quoteName('path') . ') - LENGTH ( REPLACE (a.'.$db->quoteName('path').', "/", "") )) / LENGTH("/")) AS level'
                . ' FROM ' . $db->quoteName('#__community_groups') . ' a '
                . ' INNER JOIN ' . $db->quoteName('#__users') . ' b ON a.' . $db->quoteName('ownerid') . ' = b.' . $db->quoteName('id')
                . ' WHERE (a.' . $db->quoteName('parentid') . '>' . $db->quote(0)
                . ' OR a.' . $db->quoteName('isBLN') . '=' . $db->quote('1') . ')'
                . ' AND a.path LIKE ' . $db->Quote( '%/' . $blnid . '%' )
                . $qpublish
                . $extraSQL 
                . ' ORDER BY a.' . $db->quoteName('path')
                . $sqllimit;        
        $db->setQuery($query);
        $list = $db->loadObjectList();
        return $list;
    }

    public function getMembersBLN($blnid) {
        $db = $this->getDBO();
        $query = 'SELECT * FROM ' . $db->quoteName('#__community_groups_members') . ' AS a'
            . ' INNER JOIN ' . $db->quoteName('#__users') . ' AS b '
            . ' WHERE b.'.$db->quoteName('id').'=a.'.$db->quoteName('memberid')
            . ' AND a.'.$db->quoteName('groupid').'=' . $db->Quote( $blnid )
            . ' AND a.'.$db->quoteName('permissions').' !=' . $db->quote( COMMUNITY_GROUP_BANNED ) . ' '
            . ' AND a.'.$db->quoteName('memberid').' != 0 '
            . ' AND a.'.$db->quoteName('approved').' = '.$db->Quote(1)
            . ' AND b.'.$db->quoteName('block').'=' . $db->Quote( '0' ) . ' ';
        $db->setQuery($query);
        $list = $db->loadObjectList();
        return $list;
    }

    public function getBLNGenerationCount($blnid) {
        $db = $this->getDBO();
        $query = 'SELECT id, ROUND ((LENGTH (' . $db->quoteName('path') . ') - LENGTH ( REPLACE ('.$db->quoteName('path').', "/", "") )) / LENGTH("/")) AS level'
            . ' FROM ' . $db->quoteName('#__community_groups')
            . ' WHERE path LIKE ' . $db->Quote( '%/' . $blnid . '%' )
            . ' ORDER BY level DESC';
        $db->setQuery($query);
        $list = $db->loadObjectList();
        return $list;
    }

    public function getChildrenBLN ($parentid) {
        $db = $this->getDBO();
        $query = 'SELECT * FROM ' . $db->quoteName('#__community_groups')
            . ' WHERE (' . $db->quoteName('parentid') . '=' . $db->quote($parentid)
            . ' AND ' . $db->quoteName('published') . '=' . $db->quote(1)
            . ' ORDER BY ' . $db->quoteName('name');
        $db->setQuery($query);
        $list = $db->loadObjectList();
        return $list;
    }

    public function getGenerationBLN ($gen) {
        $db = $this->getDBO();
        $query = 'SELECT *, ROUND ((LENGTH (' . $db->quoteName('path') . ') - LENGTH ( REPLACE ('.$db->quoteName('path').', "/", "") )) / LENGTH("/")) AS level'
            . ' FROM ' . $db->quoteName('#__community_groups')
            . ' WHERE (' . $db->quoteName('parentid') . '>' . $db->quote(0)
            . ' OR ' . $db->quoteName('isBLN') . '=' . $db->quote('1') . ')'
            . ' AND ' . $db->quoteName('published') . '=' . $db->quote(1)
            . ' AND ROUND ((LENGTH (' . $db->quoteName('path') . ') - LENGTH ( REPLACE (' . $db->quoteName('path') . ', "/", "") )) / LENGTH("/")) <= ' . $db->quote($gen+1)
            . ' AND ROUND ((LENGTH (' . $db->quoteName('path') . ') - LENGTH ( REPLACE (' . $db->quoteName('path') . ', "/", "") )) / LENGTH("/")) >= ' . $db->quote($gen)
            . ' ORDER BY ' . $db->quoteName('path');
        $db->setQuery($query);
        $list = $db->loadObjectList();
        return $list;
    }

    public function checkSubgroup($id){
        $db = $this->getDBO();
        $query = 'SELECT COUNT(*) FROM '
            . $db->quoteName('#__community_groups')
            . ' WHERE ' . $db->quoteName('parentid') . ' = ' . $db->Quote($id) ;
        $db->setQuery($query);
        $sub = $db->loadResult();
        return $sub;
    }

    // Search in Open circle and private & Closed of user
    public function getCoursesForSearch ($userid) {
        $db = $this->getDbo();
        $query = 'SELECT id'
            . ' FROM ' . $db->quoteName('#__community_groups')
            . ' WHERE ' . $db->quoteName('approvals') . '=' . $db->quote(0)
            . ' AND ' . $db->quoteName('categoryid') . '=' . $db->quote(0)
            . ' AND (' . $db->quoteName('published') . '=' . $db->quote(1) .' OR ('
            .  $db->quoteName('published') . '=' . $db->quote(0).'AND '. $db->quoteName('ownerid') . '=' . $db->quote($userid).'))';
        $db->setQuery($query);
        $circlesopen = $db->loadObjectList();

        $query2 = 'SELECT a.id'
            . ' FROM ' . $db->quoteName('#__community_groups') . ' AS a'
            . ' INNER JOIN ' . $db->quoteName('#__community_groups_members') . ' AS b'
            . ' ON a.' . $db->quoteName('id') . '=b.' . $db->quoteName('groupid')
            . ' AND (' . $db->quoteName('published') . '=' . $db->quote(1) .' OR ('
            .  $db->quoteName('published') . '=' . $db->quote(0).'AND '. $db->quoteName('ownerid') . '=' . $db->quote($userid).'))'
            . ' AND a.' . $db->quoteName('approvals') . '=' . $db->quote(1)
            . ' AND b.' . $db->quoteName('memberid') . '=' . $db->quote($userid)
            . ' AND b.' . $db->quoteName('approved') . '=' . $db->quote(1);
        $db->setQuery($query2);
        $circlespc = $db->loadObjectList();

        $circlesSearch = array();

        if ($circlesopen) {
            foreach ($circlesopen as $circle) {
                $circlesSearch[] = $circle->id;
            }
        }
        if ($circlespc) {
            foreach ($circlespc as $circle) {
                $circlesSearch[] = $circle->id;
            }
        }

        $query3 = 'SELECT *' .
            ' FROM #__course_shareto_group' .
            ' WHERE groupid IN (' . implode(',', $circlesSearch) . ')';
        $db->setQuery($query3);
        $result = $db->loadObjectList();

        return $result;
    }
      public function getMembersCirclePublished($groupids, $limit, $position) {
        $db = $this->getDBO();
        $query	= 'SELECT a.'.$db->quoteName('memberid').' AS id, a.'.$db->quoteName('roles').' AS roles, a.'.$db->quoteName('date').' AS date, a.'.$db->quoteName('approved').' , b.'.$db->quoteName('username').' as name,g.name as groupname, g.id as groupid FROM'
				. $db->quoteName('#__community_groups_members') . ' AS a '
                                . ' JOIN '.$db->quoteName('#__community_groups') . ' AS g ON a.groupid = g.id'
				. ' INNER JOIN ' . $db->quoteName('#__users') . ' AS b '
				. ' WHERE b.'.$db->quoteName('id').'=a.'.$db->quoteName('memberid')
				. ' AND a.'.$db->quoteName('groupid').'IN (' . $db->Quote( $groupids ).')'
				. ' AND b.'.$db->quoteName('block').'=' . $db->Quote( '0' ) . ' '
				. ' AND a.'.$db->quoteName('permissions').' !=' . $db->quote( COMMUNITY_GROUP_BANNED )
                                . ' LIMIT '.$position.','.$limit;
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
}
}
