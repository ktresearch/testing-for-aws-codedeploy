<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die('Restricted access');

require_once ( JPATH_ROOT .'/components/com_community/models/models.php');

class CommunityModelDiscussions extends JCCModel
{
	/**
	 * Configuration data
	 *
	 * @var object	JPagination object
	 **/
	var $_pagination	= '';

	/**
	 * Configuration data
	 *
	 * @var object	JPagination object
	 **/
	var $total			= '';

	/**
	 * Constructor
	 */
	public function CommunityModelDiscussions()
	{
		parent::JCCModel();

		$mainframe	= JFactory::getApplication();
		$jinput 	= $mainframe->input;

		// Get pagination request variables
 	 	$limit		= ($mainframe->getCfg('list_limit') == 0) ? 5 : $mainframe->getCfg('list_limit');
	    $limitstart = $jinput->request->get('limitstart', 0, 'INT'); //JRequest::getVar('limitstart', 0, 'REQUEST');

	    if(empty($limitstart))
 	 	{
 	 		$limitstart = $jinput->get('limitstart', 0, 'uint');
 	 	}

		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	}

	/**
	 * Method to get a pagination object for the events
	 *
	 * @access public
	 * @return integer
	 */
	public function getPagination()
	{
		return $this->_pagination;
	}

	/**
	 * Get list of discussion topics
	 *
	 * @param	$id	The group id
	 * @param	$limit Limit
	 **/
	public function getDiscussionTopics( $groupId , $limit = 0 , $order = '' )
	{
		$db			= $this->getDBO();
		$limit		= ($limit == 0) ? $this->getState( 'limit' ) : $limit;
		$limitstart	= $this->getState( 'limitstart' );

		$query	= 'SELECT COUNT(*) FROM ' . $db->quoteName('#__community_groups_discuss') . ' '
				. 'WHERE ' . $db->quoteName( 'groupid' ) . '=' . $db->Quote( $groupId )
				. 'AND ' . $db->quoteName('parentid') .'=' . $db->Quote( '0' );

		$db->setQuery( $query );
		$total	= $db->loadResult();
		$this->total	= $total;

		if($db->getErrorNum())
		{
			JError::raiseError( 500, $db->stderr());
		}

		if( empty($this->_pagination) )
		{
			jimport('joomla.html.pagination');

			$this->_pagination	= new JPagination( $total , $limitstart , $limit);
		}

		$orderByQuery	= '';
		switch( $order )
		{
            case 1:
                $orderByQuery = 'ORDER BY a.' . $db->quoteName('id') .' DESC ';
                break;
			default:
				$orderByQuery = 'ORDER BY a.' . $db->quoteName('lastreplied') .' DESC ';
				break;
		}

		$query		= 'SELECT a.*, COUNT( b.' . $db->quoteName('id').' ) AS count, b.' . $db->quoteName('comment') .' AS lastmessage , b.' . $db->quoteName('post_by') .' AS lastmessageby '
					. ' FROM ' . $db->quoteName( '#__community_groups_discuss' ) . ' AS a '
					. ' LEFT JOIN ' . $db->quoteName( '#__community_wall' ) . ' AS b ON b.' . $db->quoteName('contentid') .'=a.' . $db->quoteName('id')
					. ' AND b.' . $db->quoteName('date') .'=( SELECT max( date ) FROM ' . $db->quoteName('#__community_wall').' WHERE ' . $db->quoteName('contentid').'=a.' . $db->quoteName('id').' ) '
					. ' AND b.' . $db->quoteName('type').'=' . $db->Quote( 'discussions' )
					. ' LEFT JOIN ' . $db->quoteName( '#__community_wall' ) . ' AS c ON c.' . $db->quoteName('contentid').'=a.' . $db->quoteName('id')
					. ' AND c.' . $db->quoteName('type').'=' . $db->Quote( 'discussions')
					. ' WHERE a.' . $db->quoteName('groupid').'=' . $db->Quote( $groupId )
					. ' AND a.' . $db->quoteName('parentid').'=' . $db->Quote( '0' )
					. ' GROUP BY a.' . $db->quoteName('id')
					. $orderByQuery
					. 'LIMIT ' . $limitstart . ',' . $limit;

		$db->setQuery( $query );
		$result	= $db->loadObjectList();

		if($db->getErrorNum())
		{
			JError::raiseError( 500, $db->stderr());
		}

		return $result;
	}


	/**
	 * Search for discussion topics
	 *
	 * @param	$id	The group id
	 * @param	$limit Limit
	 **/
	public function searchDiscussionTopics( $groupId , $limit = 0 , $order = '', $keyword )
	{
		$db			= $this->getDBO();
		$limit		= ($limit == 0) ? $this->getState( 'limit' ) : $limit;
		$limitstart	= $this->getState( 'limitstart' );

		

		$query	= 'SELECT COUNT(*) FROM ' . $db->quoteName('#__community_groups_discuss') . ' '
				. 'WHERE ' . $db->quoteName( 'groupid' ) . '=' . $db->Quote( $groupId ) . ' '
				. 'AND ' . $db->quoteName('parentid') .'=' . $db->Quote( '0' ) . ' '
				. 'AND ' . $db->quoteName('title') .' like ' . $db->Quote( '%'.$keyword.'%' );

		$db->setQuery( $query );
		$total	= $db->loadResult();
		$this->total	= $total;

		if($db->getErrorNum())
		{
			JError::raiseError( 500, $db->stderr());
		}

		if( empty($this->_pagination) )
		{
			jimport('joomla.html.pagination');

			$this->_pagination	= new JPagination( $total , $limitstart , $limit);
		}

		$orderByQuery	= '';
		switch( $order )
		{
            case 1:
                $orderByQuery = 'ORDER BY a.' . $db->quoteName('id') .' DESC ';
                break;
			default:
				$orderByQuery = 'ORDER BY a.' . $db->quoteName('lastreplied') .' DESC ';
				break;
		}

		$query		= 'SELECT a.*, COUNT( b.' . $db->quoteName('id').' ) AS count, b.' . $db->quoteName('comment') .' AS lastmessage , b.' . $db->quoteName('post_by') .' AS lastmessageby '
					. ' FROM ' . $db->quoteName( '#__community_groups_discuss' ) . ' AS a '
					. ' LEFT JOIN ' . $db->quoteName( '#__community_wall' ) . ' AS b ON b.' . $db->quoteName('contentid') .'=a.' . $db->quoteName('id')
					. ' AND b.' . $db->quoteName('date') .'=( SELECT max( date ) FROM ' . $db->quoteName('#__community_wall').' WHERE ' . $db->quoteName('contentid').'=a.' . $db->quoteName('id').' ) '
					. ' AND b.' . $db->quoteName('type').'=' . $db->Quote( 'discussions' )
					. ' LEFT JOIN ' . $db->quoteName( '#__community_wall' ) . ' AS c ON c.' . $db->quoteName('contentid').'=a.' . $db->quoteName('id')
					. ' AND c.' . $db->quoteName('type').'=' . $db->Quote( 'discussions')
					. ' WHERE a.' . $db->quoteName('groupid').'=' . $db->Quote( $groupId )
					. ' AND a.' . $db->quoteName('parentid').'=' . $db->Quote( '0' )
					. ' AND a.' . $db->quoteName('title').' like ' . $db->Quote( '%'.$keyword.'%' )
					. ' GROUP BY a.' . $db->quoteName('id')
					. $orderByQuery
					. 'LIMIT ' . $limitstart . ',' . $limit;



		$db->setQuery( $query );
		$result	= $db->loadObjectList();

		if($db->getErrorNum())
		{
			JError::raiseError( 500, $db->stderr());
		}

		return $result;
	}

	/**
	 * Method to get the last replier information from specific discussion
	 *
	 * @params $discussionId	The specific discussion row id
	 **/
	public function getLastReplier( $discussionId )
	{
		$db		= $this->getDBO();

		$query	= 'SELECT * FROM ' . $db->quoteName( '#__community_wall' ) . ' '
				. 'WHERE ' . $db->quoteName( 'contentid' ) . '=' . $db->Quote( $discussionId ) . ' '
				. 'AND ' . $db->quoteName( 'type' ) . '=' . $db->Quote( 'discussions' )
				. 'ORDER BY ' . $db->quoteName('date').' DESC LIMIT 1';
		$db->setQuery( $query );
		$result	= $db->loadObject();

		if($db->getErrorNum())
		{
			JError::raiseError( 500, $db->stderr());
		}

		return $result;
	}

	public function getRepliers( $discussionId , $groupId )
	{
		$db		= JFactory::getDBO();
		$query	= 'SELECT DISTINCT(a.' . $db->quoteName('post_by').') FROM ' . $db->quoteName( '#__community_wall' ) . ' AS a '
				. ' INNER JOIN ' . $db->quoteName('#__community_groups_members').' AS b '
				. ' ON b.' . $db->quoteName('groupid').'=' . $db->Quote( $groupId )
				. ' WHERE a.' . $db->quoteName( 'contentid' ) . '=' . $db->Quote( $discussionId )
				. ' AND a.' . $db->quoteName( 'type' ) . '=' . $db->Quote( 'discussions' )
				. ' AND a.' . $db->quoteName('post_by').'=b.' . $db->quoteName('memberid');

		$db->setQuery( $query );
		return $db->loadColumn();
	}

	/**
	 * Return a list of discussion replies.
	 *
	 * @param	int		$topicId	The replies for specific topic id.
	 * @return	Array	An array of database objects.
	 **/
	public function getReplies( $topicId )
	{
		$db		= JFactory::getDBO();

		$query	= 'SELECT a.* , b.' . $db->quoteName('name').' FROM ' . $db->quoteName('#__community_wall').' AS a '
				. ' INNER JOIN ' . $db->quoteName('#__users').' AS b '
				. ' WHERE b.' . $db->quoteName('id').'=a.' . $db->quoteName('post_by')
				. ' AND a.' . $db->quoteName('type').'=' . $db->Quote( 'discussions' )
				. ' AND a.' . $db->quoteName('contentid').'=' . $db->Quote( $topicId )
				. ' ORDER BY a.' . $db->quoteName('date').' DESC ';

		$db->setQuery( $query );

		if($db->getErrorNum())
		{
			JError::raiseError(500, $db->stderr());
		}

		$result	= $db->loadObjectList();

		return $result;
	}

	/*
	 * @since 2.4
	 * @param keywords : array of keyword to be searched to match the title
	 * @param exclude : exclude the id of the discussion
	 */
	public function getRelatedDiscussion( $keywords = array(''), $exclude = array(''))
	{
		$db	= $this->getDBO();
		$topicid = JRequest::getInt('topicid',0);
        $matchQuery = '';
		$excludeQuery = '';

		if(!empty($keywords))
		{
			foreach($keywords as $words)
			{
				$matchQuery .= '  a.' . $db->quoteName('title').' LIKE '.$db->Quote( '%'.$words.'%' ). ' OR ';
			}
			$matchQuery = ' AND ('.$matchQuery.' 0 ) ';
		}

		if(!empty($exclude))
		{
			foreach($exclude as $id)
			{
				$excludeQuery .= '  a.' . $db->quoteName('id').' <> '.$db->Quote( $id ). ' AND ';
			}
			$excludeQuery = ' AND ('.$excludeQuery.' 1 ) ';
		}

		$query		= 'SELECT a.*,d.name as group_name, COUNT( b.' . $db->quoteName('id').' ) AS count, b.' . $db->quoteName('comment') .' AS lastmessage '
					. ' FROM ' . $db->quoteName( '#__community_groups_discuss' ) . ' AS a '
					. ' LEFT JOIN ' . $db->quoteName( '#__community_wall' ) . ' AS b ON b.' . $db->quoteName('contentid') .'=a.' . $db->quoteName('id')
					. ' AND b.' . $db->quoteName('date') .'=( SELECT max( date ) FROM ' . $db->quoteName('#__community_wall').' WHERE ' . $db->quoteName('contentid').'=a.' . $db->quoteName('id').' ) '
					. ' AND b.' . $db->quoteName('type').'=' . $db->Quote( 'discussions' )
					. ' LEFT JOIN ' . $db->quoteName( '#__community_wall' ) . ' AS c ON c.' . $db->quoteName('contentid').'=a.' . $db->quoteName('id')
					. ' AND c.' . $db->quoteName('type').'=' . $db->Quote( 'discussions')
					. ' LEFT JOIN ' . $db->quoteName( '#__community_groups' ) . ' AS d ON a.' . $db->quoteName('groupid').'=d.' . $db->quoteName('id')
					. ' WHERE a.' . $db->quoteName('lock').'=' . $db->Quote( '0' )
					. ' AND d.' . $db->quoteName('approvals').'=' . $db->Quote( '0' )
					. ' AND a.' . $db->quoteName('parentid').'=' . $db->Quote( '0' )
                    . ' AND a.' . $db->quoteName('id').'!=' . $db->Quote($topicid)
					. ' AND d.' . $db->quoteName('published') . '=' .$db->Quote('1')
					. $matchQuery
					. $excludeQuery
					. ' GROUP BY a.' . $db->quoteName('id');

		$db->setQuery( $query );

		if($db->getErrorNum())
		{
			JError::raiseError(500, $db->stderr());
		}

		$result	= $db->loadObjectList();

		return $result;
	}

	public function getSiteDiscussionCount()
	{
		$db	= $this->getDBO();

		$sql = 'SELECT COUNT(*) FROM '. $db->quoteName('#__community_groups_discuss');

		$db->setQuery($sql);

		$result = $db->loadResult();

		return $result;
	}
        
        public function getAllDiscussion($userid, $limit = 0, $limitstart = 0, $order = '' )
        {
            $db	= $this->getDBO();
            
            $orderByQuery	= '';
            switch( $order )
            {
                case 1:
                    $orderByQuery = ' ORDER BY d.' . $db->quoteName('id') .' DESC ';
                    break;
                default:
                    $orderByQuery = ' ORDER BY d.' . $db->quoteName('lastreplied') .' DESC ';
                    break;
            }
            
            $limitQuery = '';
            if ($limit && $limit > 0) {
                $limitQuery = 'LIMIT ' . $limitstart . ',' . $limit;
            }
            
            $query = 'SELECT d.id, d.title,d.groupid,d.creator,d.created,d.message,d.lastreplied,d.avatar AS cover,g.avatar,g.params FROM '.$db->quoteName('#__community_groups_discuss').' AS d'
                    .' LEFT JOIN '.$db->quoteName('#__community_groups').' AS g ON g.'.$db->quoteName('id').'=d.'.$db->quoteName('groupid')
                    .' LEFT JOIN '.$db->quoteName('#__community_groups_members').' AS m ON m.'.$db->quoteName('groupid').'=d.'.$db->quoteName('groupid')
                    .' WHERE m.'.$db->quoteName('memberid').'='.$db->quote($userid)
                    .' AND m.'.$db->quoteName('approved').'='.$db->quote('1')
                    .' AND d.'.$db->quoteName('lock').'='.$db->quote('0')
                    .' AND g.'.$db->quoteName('published').'='.$db->quote('1')
                    .' AND (g.'.$db->quoteName('categoryid').'!=' . $db->Quote( '6' ).' OR SUBSTRING_INDEX( SUBSTRING_INDEX( g.'.$db->quoteName('params').',  \'}\', 1 ) ,  \'course_id":\', -1 ) IN (SELECT courseid FROM '
                    . $db->quoteName('#__course_shareto_group') . ') OR SUBSTRING_INDEX( SUBSTRING_INDEX( g.'.$db->quoteName('params').',  \'}\', 1 ) ,  \'course_id":\', -1 ) IN (SELECT product_code FROM '
                    . $db->quoteName('#__hikashop_product') . ' WHERE '.$db->quoteName('product_published').' = '.$db->Quote('1').'))'
                    . $orderByQuery
                    . $limitQuery;
            $db->setQuery( $query );

            $result = $db->loadObjectList();
            if($db->getErrorNum())
            {
                    return false;
            }
            return $result;
        }
        
        /**
	 * Get list of discussion topics
	 *
	 * @param	$userId	The user id
	 * @param	$limit Limit
	 **/
	public function getMyDiscussionTopics( $userId , $limit = 0 , $order = '' )
	{
		$db		= $this->getDBO();
		$limit		= ($limit == 0) ? $this->getState( 'limit' ) : $limit;
		$limitstart	= $this->getState( 'limitstart' );

		$query	= 'SELECT COUNT(*) FROM ' . $db->quoteName('#__community_groups_discuss') . ' '
				. 'WHERE ' . $db->quoteName( 'creator' ) . '=' . $db->Quote( $userId )
				. 'AND ' . $db->quoteName('parentid') .'=' . $db->Quote( '0' );

		$db->setQuery( $query );
		$total	= $db->loadResult();
		$this->total	= $total;

		if($db->getErrorNum())
		{
			JError::raiseError( 500, $db->stderr());
}

		if( empty($this->_pagination) )
		{
			jimport('joomla.html.pagination');

			$this->_pagination	= new JPagination( $total , $limitstart , $limit);
		}

		$orderByQuery	= '';
		switch( $order )
		{
                    case 1:
                        $orderByQuery = 'ORDER BY a.' . $db->quoteName('id') .' DESC ';
                        break;
                    default:
                        $orderByQuery = 'ORDER BY a.' . $db->quoteName('lastreplied') .' DESC ';
                        break;
		}

		$query		= 'SELECT a.*, COUNT( b.' . $db->quoteName('id').' ) AS count, b.' . $db->quoteName('comment') .' AS lastmessage , b.' . $db->quoteName('post_by') .' AS lastmessageby '
					. ' FROM ' . $db->quoteName( '#__community_groups_discuss' ) . ' AS a '
					. ' LEFT JOIN ' . $db->quoteName( '#__community_wall' ) . ' AS b ON b.' . $db->quoteName('contentid') .'=a.' . $db->quoteName('id')
					. ' AND b.' . $db->quoteName('date') .'=( SELECT max( date ) FROM ' . $db->quoteName('#__community_wall').' WHERE ' . $db->quoteName('contentid').'=a.' . $db->quoteName('id').' ) '
					. ' AND b.' . $db->quoteName('type').'=' . $db->Quote( 'discussions' )
					. ' LEFT JOIN ' . $db->quoteName( '#__community_wall' ) . ' AS c ON c.' . $db->quoteName('contentid').'=a.' . $db->quoteName('id')
					. ' AND c.' . $db->quoteName('type').'=' . $db->Quote( 'discussions')
					. ' WHERE a.' . $db->quoteName('creator').'=' . $db->Quote( $userId )
					. ' AND a.' . $db->quoteName('parentid').'=' . $db->Quote( '0' )
					. ' GROUP BY a.' . $db->quoteName('id')
					. $orderByQuery
					. 'LIMIT ' . $limitstart . ',' . $limit;

		$db->setQuery( $query );
		$result	= $db->loadObjectList();

		if($db->getErrorNum())
		{
			JError::raiseError( 500, $db->stderr());
		}

		return $result;
	}
        
        /**
         * Sets lastaccess discussion in the group members table __community_discuss_access
         */
        public function memberLastaccessDiscuss( $chatid , $memberid )
	{
            if ($chatid > 0 && $memberid > 0) {
		$db		= $this->getDBO();
//                $time   = date('Y-m-d H:i:s',time());
                $dateObject = CTimeHelper::getDate();
                $time = $dateObject->Format('Y-m-d H:i:s');
                
                $query_check = 'SELECT * FROM '
				. $db->quoteName( '#__community_discuss_access' ) . ' WHERE '
				. $db->quoteName( 'disscussionid' ) . '=' . $db->Quote( $chatid ) . ' '
                                . 'AND ' . $db->quoteName( 'userid' ) . '=' . $db->Quote( $memberid );
                $db->setQuery( $query_check );
		$check = $db->loadResult();

                if (count($check) > 0) {
                    $query	= 'UPDATE ' . $db->quoteName( '#__community_discuss_access' ) . ' SET '
                                . $db->quoteName( 'last_access' ) . '=' . $db->Quote( $time ) . ' '
				. 'WHERE ' . $db->quoteName( 'disscussionid' ) . '=' . $db->Quote( $chatid ) . ' '
				. 'AND ' . $db->quoteName( 'userid' ) . '=' . $db->Quote( $memberid );
                } else { 'INSERT INTO `par_community_discuss_access`(`disscussionid`, `userid`, `last_access`) VALUES ([value-1],[value-2],[value-3])';
                    $query	= 'INSERT INTO ' . $db->quoteName( '#__community_discuss_access' ) . ' ( '
                                . $db->quoteName( 'disscussionid' ) . ', ' . $db->quoteName( 'userid' ) . ', ' . $db->quoteName( 'last_access' ) . ') VALUES ('
                                . $db->Quote( $chatid ) . ', ' . $db->Quote( $memberid ) . ', ' . $db->Quote( $time ) . ') ';
}

		$db->setQuery( $query );
		$db->query();

		if($db->getErrorNum())
		{
			JError::raiseError( 500, $db->stderr());
		}
            }
	}
        
        public function getNotificationChat ($chatid, $userid) {
            $db		= $this->getDBO();
            
            $query	= 'SELECT a.* '
                                    . ' FROM ' . $db->quoteName( '#__community_wall' ) . ' AS a '
                                    . ' WHERE a.' . $db->quoteName( 'date' ) . ' > ( SELECT ' . $db->quoteName('last_access') 
                                                        . ' FROM ' . $db->quoteName('#__community_discuss_access')
                                                        . ' WHERE ' . $db->quoteName('userid').'=' . $db->Quote( $userid )
                                                        . ' AND ' . $db->quoteName('disscussionid').'=' . $db->Quote( $chatid ) . ' )'
                                    . ' AND a.' . $db->quoteName( 'contentid' ) . '=' . $db->Quote( $chatid ) 
                                    . ' AND a.' . $db->quoteName( 'post_by' ) . '!=' . $db->Quote( $userid )
                                    . ' GROUP BY a.' . $db->quoteName('id');

            $db->setQuery( $query );
            $result	= $db->loadObjectList();

            if($db->getErrorNum())
            {
                    JError::raiseError( 500, $db->stderr());
            }

            return count($result);
        }
}
