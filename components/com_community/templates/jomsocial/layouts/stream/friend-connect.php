<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();

// The target/actor is of no importance. If the current user is either of of them, they should read it as 'you'
$user1 = CFactory::getUser($act->actor);
$user2 = CFactory::getUser($act->target);

$my = CFactory::getUser();
$you = null;
$other = null;

if($my->id == $user1->id) {
	$you = $user1;
	$other = $user2;
}

if($my->id == $user2->id) {
	$you = $user2;
	$other = $user1;
}

$userModel = CFactory::getModel('Groups');

/* Temporary fix since we not yet complete move to CActivity */
if ( $this->act instanceof  CTableActivity ) {
    /* If this's CTableActivity then we use getProperties() */
    $activity = new CActivity($this->act->getProperties());
}else {
    /* If it's standard object than we just passing it */
    $activity = new CActivity($this->act);
}


?>
<div class="joms-stream__header">
    
        <?php if(!is_null($you)){ ?>
            <div class= "joms-avatar--stream <?php echo CUserHelper::onlineIndicator($user1); ?>">
                <a href="<?php echo CUrlHelper::userLink($user1->id); ?>">
                    <img src="<?php echo $user1->getAvatar().'?_='.time(); ?>" alt="<?php echo $user1->getDisplayName(); ?>">
                </a>
            </div>

            <div class="joms-stream__meta">
                <div class="joms-stream_title">
                <span> <?php echo JText::sprintf('COM_COMMUNITY_STREAM_MY_FRIENDS', $other->getDisplayName(), CUrlHelper::userLink($other->id)); ?> </span>
                <?php      
                $users = $userModel->getUserStatus($user1->id);
                $userStatus = '';
                if(!empty($users)) $userStatus = $users[0]->status;
                ?>
                </div>
                <div class="joms-stream-post_time">
                <span class="joms-stream__time"> <?php echo $userStatus; ?> </span>
                <span class="joms-stream__time" style="float: right"><small>
                <?php echo $activity->getCreateTimeFormatted(); ?>
                </small></span>
                </div>
            </div>
       
        <?php } else { ?>

            <div class= "joms-avatar--stream <?php echo CUserHelper::onlineIndicator($user1); ?>">
                <a href="<?php echo CUrlHelper::userLink($user1->id); ?>">
                    <img data-author="<?php echo $user1->id; ?>" src="<?php echo $user1->getAvatar().'?_='.time(); ?>" alt="<?php echo $user1->getDisplayName(); ?>">
                </a>
            </div>

            <div class="joms-stream__meta">
                <div class="joms-stream_title">
                <span> <?php echo JText::sprintf('COM_COMMUNITY_STREAM_OTHER_FRIENDS', $user1->getDisplayName(),$user2->getDisplayName(), CUrlHelper::userLink($user1->id), CUrlHelper::userLink($user2->id)); ?> </span>
                <?php      
                $users = $userModel->getUserStatus($user1->id);
                $userStatus = '';
                if(!empty($users)) $userStatus = $users[0]->status;
                ?>
                </div>
                <div class="joms-stream-post_time">
                <!--<span class="joms-stream__time"> <?php // echo $userStatus; ?> </span>-->
                <span class="joms-stream__time" style="float: right"><small>
                <?php echo $activity->getCreateTimeFormatted(); ?>
                </small></span>
                </div>
            </div>
        
        <?php } ?>

</div>
