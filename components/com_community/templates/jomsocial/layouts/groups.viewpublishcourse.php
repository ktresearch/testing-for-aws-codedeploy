<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
/*
Added by Chris course publish to social circles
*/

$config = CFactory::getConfig();
$showGroupDetails = ($group->approvals == 0 || ( $group->approvals == 1 && $isMember ) || ($group->approvals == 2 && $isMember ) || $isSuperAdmin); //who can see the group details

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root().'/modules/mod_joomdle_my_courses/css/swiper.min.css');
$document->addScript(JUri::root().'/modules/mod_joomdle_my_courses/swiper.min.js');
require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
JLoader::import('joomla.user.helper'); 

$session = JFactory::getSession();
$device = $session->get('device');

$my = CFactory::getUser();
$username = $my->username;
$categoryid = $group->moodlecategoryid;

$class = '';
$idstr = 'id="mobile-show"';
if ($device != 'mobile') {
    $class = 'desktop-tablet';
    $idstr = '';
}

function custom_echo($x, $length) {
    if (strlen($x) <= $length) {
        return $x;
    } else {
        $y = substr($x, 0, $length) . '...';
        return $y;
    }
}

?>

<div class="home">
    <div class="container" style="padding-bottom: 30px">
        <div class="my-course have-courses" id="pending-approve-course">
            <div class="course-title-block">
                <h2><a href="#">Courses<span class=""> (<?php echo count($publishedCourses); ?>)</span></a>
                </h2>
            </div>

            <div class="course-content-block <?php echo $class; ?>">
                <div class="swiper-container swiper-container-horizontal swiper-container-free-mode">
                    <div class="swiper-wrapper" id="pending-course">
                        <?php foreach ($publishedCourses as $course) {
                            $hasPer = false;
                            if ($course['userRoles']) {
                            $userRoles = json_decode($course['userRoles'], true);
                                $roles = json_decode($userRoles['roles'][0]['role'], true);
                            foreach ($roles as $key => $value) {
                                    if ($value['sortname'] == 'coursecreator' || $value['sortname'] == 'manager' || $value['sortname'] == 'editingteacher' || $value['sortname'] == 'teacher') $hasPer = true;
                            }
                            }
                            ?>
                            <div id="published-<?php echo $course['remoteid'] ?>" class="swiper-slide single_course swiper-slide-active" style="width: 497.2px; margin-right: 8px;">
                                <div class="title-course" style="width: 718px;">
                                    <div class="course-image-wrapper" style="background-image: url('<?php echo $course['filepath'] . $course['filename'] ?>')">
                                    </div>

                                    <?php if($isManager || $isAdmin || $isSuperAdmin || $course['isStudent']) { ?>
                                        <a class="course-title" href="<?php echo CRoute::_('index.php?option=com_joomdle&view=course&course_id='.$course['remoteid']) ?>"><?php echo $course['fullname'] ?></a>
                                    <?php }else{ ?>
                                        <div class="course-title"><?php echo $course['fullname'] ?></div>
                                    <?php }?>
                                    
                                    <div class="courseDetails">
                                    
                                        <?php
                                            $userid = JUserHelper::getUserId($course['creator']);
                                            $user_owner = JFactory::getUser($userid); 
                                            if ($course['count_learner'] && $course['count_learner'] > 1) {
                                                echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $course['count_learner'] . ' ' . JText::_('COM_COMMUNITY_COURSE_SUBSCRIBERS') . '</span></br>';
                                            } else {
                                                echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $course['count_learner'] . ' ' . JText::_('COM_COMMUNITY_COURSE_SUBSCRIBER') . '</span></br>';
                                            }
                                            $time_view = '';
                                            if ($course['timepublish'] && $course['timepublish'] > 0) {
                                                $time_published = $course['timepublish']; 
//                                                if ($time_published < 60*60*24) {   // < 24h
//                                                    $time_view = 'Today';
//                                                } else if ($time_published) {
                                                    $date2 = JFactory::getDate($time_published);
                                                    $time_view = CTimeHelper::timeLapseNew($date2, false);
//                                                }
                                            }
                                            if ($time_view != '') {
                                                echo number_format((float)$course['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                            } else {
                                                echo number_format((float)$course['duration'], 2, '.', '') . ' hr';
                                            }
                                        ?>
                                    </div>
                                        <p class="course-description"><?php echo mb_strimwidth($course['summary'], 0, 130, "..."); ?></p>
<!--                                    <div class="course_timepublish"><?php
//                                        echo ($course['timepublish'] != 0) ? JText::_('COM_COMMUNITY_RELEASED').': '. date('d/m/Y', $course['timepublish']) : JText::_('COM_COMMUNITY_RELEASED').': '. JText::_('COM_COMMUNITY_NOT_YET');
                                        ?></div>-->
                                    <div>

                                        <?php if($isManager || $isAdmin || $isSuperAdmin) { ?>
                                            <div>
                                                <a class="right-side-swiper" id="unpublish" data-id="<?php echo $course['remoteid'] ?>" href="#">
                                                    <?php echo JText::_('COM_COMMUNITY_BUTTON_UNPUBLISH'); ?> </a>
                                            </div>
                                        <?php }else{ ?>
                                            <?php if(!$course['isStudent']){?>
                                                <div>
                                                    <a class="right-side-swiper" id="subscribe" data-id="<?php echo $course['remoteid'] ?>" href="#">
                                                        <?php echo JText::_('COM_COMMUNITY_BUTTON_SUBSCRIBE'); ?> </a>
                                                </div>
                                            <?php }else {?>
                                                <div>
                                                    <a class="right-side-swiper" id="unsubscribe" data-id="<?php echo $course['remoteid'] ?>" href="#">
                                                        <?php echo JText::_('COM_COMMUNITY_BUTTON_UNSUBSCRIBE'); ?> </a>
                                                </div>
                                            <?php }?>

                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="lp-start" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" >
    <div class="modal-dialog modal-sm" style="width: 30%">
        <div class="modal-content">
            <span id="message"></span>
            <br/> <br/>

            <div style="padding-bottom: 20px">

                <div style="float: left">
                    <a id="closemodal" style="width: 100%;" href="#" class="joms-focus__button--message">Close</a>
                </div>

                <div style="float: right" id="link">
                    <a id="linkto" style="width: 100%;" href="#" class="joms-focus__button--message" data-id="0">Ok</a>
                </div>

            </div>
        </div>
    </div>
</div>

<style>
    .swiper-slide{
        width: 337.6px !important;
        margin-right: 12px !important;
    }
</style>

<?php
if($device == 'mobile') {
    $slideview = '1';
    $space = '-8';

} else if($device == 'tablet') {
    $slideview = '2.5';
    $space = '8';

} else {
    $slideview = '3';
    $space = '8';
}
?>

<script type="text/javascript">
    var username = '<?php echo $username ?>';
    var categoryid = <?php echo $categoryid ?>;
    var courseid = 0;
    var action = '';
    var groupid = '<?php echo $group->id ?>';

    var swiper = new Swiper('.my-course .swiper-container', {
        pagination: '.my-course .swiper-pagination',
        slidesPerView: <?php echo $slideview ;?>,
        nextButton: '.my-course .swiper-button-next',
        prevButton: '.my-course .swiper-button-prev',
        spaceBetween: <?php echo $space; ?>,
        freeMode: true,
        freeModeMomentum: true,
        preventClicks:false,
        preventClicksPropagation:false
    });

    (function ($) {
        var deviceWidth = $(window).width();
        var newWidth = deviceWidth - 50;
        $('.home .my-course .course-content-block .swiper-slide .title-course').width(newWidth + 'px');
    })(jQuery);

    jQuery(document).ready(function() {

        jQuery('img').each(function() {
            jQuery(this).attr('src', jQuery(this).attr('kvsrc') );
        });


        /* Javascript for message popup - remove course*/

        jQuery('.title-course #remove').on('click',function(e){
            e.preventDefault();
            jQuery('#lp-start').modal('toggle');
            jQuery('#lp-start').modal('show');
            jQuery('#message').text("Are you sure you want to remove the course?");
            jQuery('#link .joms-focus__button--message').attr('id', 'linkto-remove');
            jQuery('#linkto-remove').attr("data-id", jQuery(this).data("id"));
            courseid = jQuery(this).data("id");
        });

        /* Javascript message popup - unpublish course */

        jQuery('.title-course #unpublish').on('click',function(e){
            e.stopImmediatePropagation();
            e.preventDefault();
            var clickedid = e.target.id; // approve and unapprove (action)
            var dataid = jQuery(this).data("id"); // course id
            var linkto = 'linkto-' + clickedid;
            var message = "Are you sure you want to " + clickedid + " the course?";
            jQuery('#lp-start').modal('toggle');
            jQuery('#lp-start').modal('show');

            jQuery('#message').text(message);
            jQuery('#link .joms-focus__button--message').attr('id', linkto);
            jQuery('#'+ linkto).attr("data-id", jQuery(this).data("id"));
            courseid = dataid;
        });

        /* Javascript message subscribe course */

        jQuery('.title-course #subscribe').on('click',function(e){
            e.stopImmediatePropagation();
            e.preventDefault();
            courseid = jQuery(this).data("id");
            act = 1;
            $.ajax({
                type: "POST",
                data: {courseid: courseid, act: act},
                url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxEnrolUser&tmpl=component&format=json') ?>",
                dataType: "json",
                success: function(data){
                    location.reload();
                    //console.log(data);
                }
            });
        });

        /* Javascript message unsubscribe course */

        jQuery('.title-course #unsubscribe').on('click',function(e){
            e.stopImmediatePropagation();
            e.preventDefault();
            courseid = jQuery(this).data("id");
            act = 0;
            $.ajax({
                type: "POST",
                data: {courseid: courseid, act: act},
                url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxEnrolUser&tmpl=component&format=json') ?>",
                dataType: "json",
                success: function(data){
                    location.reload();
                }
            });
        });

        /* Javascript for Unpublish course */

        jQuery('#link').on('click', '#linkto-unpublish', function(e){
            e.preventDefault();
            $.ajax({
                type: "POST",
                data: {courseid: courseid, groupid: groupid},
                url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxRemovePublishedCourse&tmpl=component&format=json') ?>",
                dataType: "json",
                success: function(data){
                    jQuery('#lp-start').modal('hide');
                    location.reload();
                    //console.log(data);
                }
            });
        });


        /* Javascript for Removing Course */

        jQuery('#link').on('click','#linkto-remove', function(e){
            var newcourse = '#newcourse-' + courseid;
            //e.stopImmediatePropagation();
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxRemoveCourse&tmpl=component&format=json') ?>",
                dataType: "json",
                data: {username: username, courseid: courseid, categoryid: categoryid},
                success: function(data){
                    jQuery('#lp-start').modal('hide');
                    jQuery(newcourse).fadeIn('slow').hide();
                }
            });
        });

        /* Javascript for Edit Course */

        jQuery('#link').on('click','#linkto-edit', function(e){
            window.location.href = "<?php echo JURI::base();?>mycourses/edit_"+courseid+".html";
        });

        /* Javascript when click the close button */

        jQuery('#closemodal').click(function(e){
            e.preventDefault();
            jQuery('#lp-start').modal('toggle');
            jQuery('#lp-start').modal('hide');
        });

    });

</script>
