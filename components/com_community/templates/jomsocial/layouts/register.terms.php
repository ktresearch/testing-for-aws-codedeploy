<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();
?>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Terms and Conditions</h3>
  </div>
  <div class="panel-body">
  	<div>
    Parenthexis’ products and services are provided by Parenthesis Pte Ltd, headquartered in Singapore. These Terms of Use ("Terms") govern your use of Parenthesis’ website, learning environment and apps (collectively, the “Site” or “Sites”), and other products and services ("Services").   By continuing to access Parenthesis’ Sites, you agree to comply with the following Terms of Use (the “Agreement” or “Terms”). The terms “Parenthesis”, "we," "us," and "our" refer to Parenthesis Pte Ltd.  The term "You", "you" or “your” refers to the entity or individual who is accessing Parenthesis’ Sites and Services under this agreement.
	</div>

	<div style="margin-top: 20px">
	As some of our Services may be software that is downloaded to your computer, phone, tablet, or other devices, you agree that we may automatically update this software, and that these Terms will apply to such updates. Please read these Terms carefully, and contact us if you have any questions. By using our Services, you agree to be bound by these Terms, including the policies referenced in these Terms (such as our Privacy Policy).
	</div>
  </div>
</div>

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Privacy Policy</h3>
  </div>
  <div class="panel-body">
  <h4>Parenthesis Personal Data Protection Statement</h4>

  	<div>
	Parenthesis Pte Ltd (collectively referred to herein as “Parenthesis”, “us”, “we” or “our”) is committed to protect the privacy of individuals and recognises the importance of the personal data you  have entrusted to us and believe that it is our responsibility to properly protect, manage, process and disclose your personal data. As such, this Personal Data Protection Statement is to assist you in understanding how we collect, use and/or disclose your personal data.
	</div>

	<div style="margin-top: 20px">
	We will collect, use and disclose your personal data in compliance with the Personal Data Protection Act 2012 (“Act”) under Singapore law. In general, subject to applicable exceptions permitted in the Act, before we collect any personal data from you, we will notify you of the purposes for which your personal data may be collected, used and/or disclosed, as well as obtain consent for the collection, use and/or disclosure of your personal data for the intended purpose.
	</div>

	<div style="margin-top: 20px">
	We may from time to time update this Data Protection Policy to ensure that this Data Protection Policy is consistent with our future developments, industry trends and/or any changes in legal or regulatory requirements. Please check back regularly for updated information on the handling of your Personal Data.
	</div>
  </div>
</div>