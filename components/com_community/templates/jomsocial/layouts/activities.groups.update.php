<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/

defined('_JEXEC') or die('Restricted access');

$user = CFactory::getUser($this->act->actor);
$userModel = CFactory::getModel('Groups');
/* Temporary fix since we not yet complete move to CActivity */
if ( $this->act instanceof  CTableActivity ) {
    /* If this's CTableActivity then we use getProperties() */
    $activity = new CActivity($this->act->getProperties());
}else {
    /* If it's standard object than we just passing it */
    $activity = new CActivity($this->act);
}
$my = CFactory::getUser();
?>

<div class="joms-stream__header">
  
    <div class= "joms-avatar--stream <?php echo CUserHelper::onlineIndicator($user); ?>">
        <a href="<?php echo CUrlHelper::userLink($user->id); ?>">
            <img src="<?php echo $user->getAvatar().'?_='.time(); ?>" alt="<?php echo $user->getDisplayName(); ?>">
        </a>
    </div>

    <div class="joms-stream__meta">
        <div class="joms-stream_title">
    	<a href="<?php echo CUrlHelper::userLink($user->id); ?>"><?php if($my->id === $user->id) echo JText::_('COM_COMMUNITY_POST_ME'); else echo $user->getDisplayName(); ?></a> 
        <span> <?php echo JText::sprintf('COM_COMMUNITY_GROUPS_GROUP_UPDATED', $this->group->getLink(), $this->group->name); ?> </span>
        <?php      
        $users = $userModel->getUserStatus($user->id);
        $userStatus = '';
        if(!empty($users)) $userStatus = $users[0]->status;
        ?>
        </div>
        <!--<span class="joms-stream__time"> <?php // echo $userStatus; ?> </span>-->
        <div class="joms-stream-post_time">
        <span class="joms-stream__time" style="float: right"><small>
        <?php echo $activity->getCreateTimeFormatted(); ?>
        </small></span>
        </div>

    </div>

    <?php
        $this->load('activities.stream.options');
    ?>
    
</div>
