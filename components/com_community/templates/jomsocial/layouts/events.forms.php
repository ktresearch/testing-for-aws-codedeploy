<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();
$document = JFactory::getDocument();

$session = JFactory::getSession();
$device = $session->get('device');
if ($device == 'mobile') {
    $document->addStyleSheet(JURI::root(true) . 'templates/t3_bs3_blank/local/css/events.css');
} else $document->addStyleSheet(JURI::root(true) . 'templates/t3_bs3_tablet_desktop_template/local/css/events.css');
$startDate = new JDate($event->startdate);
$endDate = new JDate($event->enddate);
$isReadOnlyDate = CEventHelper::isToday($event) || CEventHelper::isPast($event);
$isReadOnlyDate = $isReadOnlyDate && ($event->id > 0);
$repeatEndDate = false;

if ($event->id && $event->repeat) {
    $repeatEndDate = new JDate($event->repeatend);
}
?>

<style>
    @media (min-width: 320px) and (max-width: 767px) {
        .picker__holder {
            position: fixed !important;
        }
    }
</style>

<link href="<?php echo $this->baseurl ?>./components/com_community/templates/jomsocial/assets/css/pygments.css"
      type="text/css" rel="stylesheet">
<link href="<?php echo $this->baseurl ?>./components/com_community/templates/jomsocial/assets/css/prettify.css"
      type="text/css" rel="stylesheet">
<link rel="stylesheet/less" type="text/css"
      href="<?php echo $this->baseurl ?>./components/com_community/templates/jomsocial/assets/css/timepicker.less">


<script src="<?php echo $this->baseurl ?>./components/com_community/templates/jomsocial/assets/js/less.min.js"></script>
<script type="text/javascript"
        src="<?php echo $this->baseurl ?>./components/com_community/templates/jomsocial/assets/js/bootstrap-timepicker.js"></script>

<script src="<?php echo JURI::base(); ?>components/com_community/assets/release/js/picker.js"></script>
<script src="<?php echo JURI::base(); ?>components/com_community/assets/release/js/picker.date.js"></script>
<link rel="stylesheet"
      href="<?php echo JURI::base(); ?>components/com_community/assets/pickadate/themes/classic.combined.css"
      type="text/css"/>
<div class="joms-page">
    <form method="POST" id="creat_event" enctype="multipart/form-data" action="<?php echo CRoute::getURI(); ?>">

        <?php
        if ($evnetHeader) {
            echo $evnetHeader;
        }
        ?>
        <?php if (!$event->id && $eventcreatelimit != 0) { ?>
            <?php if ($eventCreated / $eventcreatelimit >= COMMUNITY_SHOW_LIMIT) { ?>
                <div class="joms-form__group">
                    <p><?php echo JText::sprintf('COM_COMMUNITY_EVENTS_CREATION_LIMIT_STATUS', $eventCreated, $eventcreatelimit); ?></p>
                </div>
            <?php } ?>
        <?php } ?>

        <?php if ($beforeFormDisplay) { ?>
            <div class="joms-form__group"><?php echo $beforeFormDisplay; ?></div>
        <?php } ?>
        <?php
        // only show if there are more than 1 member in the group
        if ($showGroupMemberInvitation) { ?>
            <div class="joms-form__group" style="display: none;">
                <span></span>
                <input type="checkbox" checked="" class="joms-checkbox" name="invitegroupmembers"
                       onclick="joms_checkPrivacy();" value="1">
                <span title="Only group members will be able to see the group's content"><?php echo JText::_('COM_COMMUNITY_EVENT_INVITE_ALL_GROUP_MEMBERS'); ?></span>
            </div>
        <?php } ?>

        <div class="joms-form__group joms-textarea--mobile">
        <span class="title"><?php if ($event->id) echo JText::_('COM_COMMUNITY_EVENTS_EDIT'); else echo JText::_('COM_COMMUNITY_EVENTS_CREATE_TITLE'); ?>
        </span>

            <h3 class="event-title">
                <?php
                $event_name = JText::_('COM_COMMUNITY_EVENTS_NAME_DEF');
                if ($event->title) {
                    $event_name = CActivities::truncateComplex($event->title, $titleLength, true);
                }
                ?>
                <p class='mnEventName'><?php echo JText::_('COM_COMMUNITY_EVENTS_NAME') ?><span
                            class="joms-required">*</span></p>
                <input type="text" class="joms-input" name="title" id="ev-name" style="display: block; height: 30px;"
                       value="<?php if ($event->title) echo $event_name; ?>">
                <p class="nullname"></p>

            </h3>
            <p class="evDecription"><?php echo JText::_('COM_COMMUNITY_EVENTS_DESCRIPTION'); ?></p>
            <textarea class="joms-textarea js-event-text js-event-input"
                      name="description"><?php echo $this->escape($event->description); ?></textarea>
        </div>
        <div class="joms-form__group">
            <p class="evLocation"><?php echo JText::_('COM_COMMUNITY_EVENTS_LOCATION'); ?><span
                        class="joms-required">*</span></p>
            <input type="text" class=" evInputLocation joms-input js-event-text js-event-input" name="location"
                   id="ev-location"
                   title="<?php echo JText::_('COM_COMMUNITY_EVENTS_LOCATION_TIPS'); ?>"
                   value="<?php echo $this->escape($event->location); ?>"
            >
        </div>
        <div class="joms-form__group" style="display: none;">
            <p class="evCategory"><?php echo JText::_('COM_COMMUNITY_EVENTS_CATEGORY'); ?><span
                        class="joms-required">*</span></p>
            <?php echo $lists['categoryid']; ?>
        </div>

        <script>

            joms_tmp_pickadateOpts = {
                min: true,
                format: 'yyyy-mm-dd',
                firstDay: <?php echo $config->get('event_calendar_firstday') === 'Monday' ? 1 : 0 ?>,
                today: '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_CURRENT", true) ?>',
                'clear': '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_CLEAR", true) ?>'
            };

            joms_tmp_pickadateOpts.weekdaysFull = [
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_DAY_1", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_DAY_2", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_DAY_3", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_DAY_4", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_DAY_5", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_DAY_6", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_DAY_7", true) ?>'
            ];

            joms_tmp_pickadateOpts.weekdaysShort = [];
            for (i = 0; i < joms_tmp_pickadateOpts.weekdaysFull.length; i++)
                joms_tmp_pickadateOpts.weekdaysShort[i] = joms_tmp_pickadateOpts.weekdaysFull[i].substr(0, 3);

            joms_tmp_pickadateOpts.monthsFull = [
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_MONTH_1", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_MONTH_2", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_MONTH_3", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_MONTH_4", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_MONTH_5", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_MONTH_6", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_MONTH_7", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_MONTH_8", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_MONTH_9", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_MONTH_10", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_MONTH_11", true) ?>',
                '<?php echo JText::_("COM_COMMUNITY_DATEPICKER_MONTH_12", true) ?>'
            ];

            joms_tmp_pickadateOpts.monthsShort = [];
            for (i = 0; i < joms_tmp_pickadateOpts.monthsFull.length; i++)
                joms_tmp_pickadateOpts.monthsShort[i] = joms_tmp_pickadateOpts.monthsFull[i].substr(0, 3);

            <?php if ($timezoneserver) { ?>
            var timezoneserver = <?php echo $timezoneserver; ?>;
            var check_time_server_and_local = timezoneserver - timezonelocal;
            <?php } ?>
        </script>
        <div class="eventCatagory row joms-form__group">

            <div class="js-event-start">
                <span class="header"><?php echo JText::_('COM_COMMUNITY_EVENTS_START_DATE'); ?><span class="joms-required">*</span></span>
                <div class="col-xs-12 col-sm-6 col-md-6 full" style="clear: both;">
                    <div class="start-date col-xs-5 col-sm-3 col-md-3 full">
                        <input style="cursor: text; text-align: center;" type="text"
                               placeholder="<?php echo JText::_('COM_COMMUNITY_POSTBOX_EVENT_START_DATE_HINT'); ?>"
                               value='<?php if ($event->id) echo $startDate->format('d/m/Y'); else echo date('d/m/Y'); ?>'
                               name="startdate" id="startdate"/>
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 full space" style="text-align: center;">-</div>
                    <div class="start-time col-xs-4 col-sm-3 col-md-3 full">
                        <div class="input-group bootstrap-timepicker timepicker">
                            <input type="text" style="text-align: left;" name="starttime"
                                   id="starttime"/>
                        </div>
                    </div>
                </div>
            </div>

            

            <div class="js-event-end">
                <span class="header"><?php echo JText::_('COM_COMMUNITY_EVENTS_END_DATE'); ?><span class="joms-required">*</span></span>
                <div class="col-xs-12 col-sm-6 col-md-6 full" style="clear: both;">
                    <div class="end-date col-xs-5 col-sm-3 col-md-3 full">
                        <input style="cursor: text; text-align: center;" type="text"
                               placeholder="<?php echo JText::_('COM_COMMUNITY_POSTBOX_EVENT_END_DATE_HINT'); ?>"
                               value='<?php if ($event->id) echo $endDate->format('d/m/Y'); else echo date('d/m/Y'); ?>'
                               name="enddate" id="enddate"/>
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 full space" style="text-align: center;">-</div>
                    <div class="end-time col-xs-4 col-sm-3 col-md-3 full">
                        <div class="input-group bootstrap-timepicker timepicker">
                            <input type="text" style="text-align: left;" name="endtime"
                                   id="endtime"/>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <input id="timezonelocal" name="timezonelocal" class="form-control input-small js-event-text" type="hidden">
            </div>

        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
        <script type="text/javascript">
            (function ($) {


            })(jQuery);

            window.joms_queue || (joms_queue = []);
            joms_queue.push(function( $ ) {
                var $stime = $('#starttime'),
                    $etime = $('#endtime');

                var $startdate = $('#startdate').pickadate({
                    format: 'dd/mm/yyyy',
                    // formatSubmit: 'dd-mm-yyyy'
                    min: 'picker__day--today',
                    onSet: function(context) {
                        syncTime();
                    }
                });
                var startdatepicker = $startdate.pickadate('picker');

                var $enddate = $('#enddate').pickadate({
                    format: 'dd/mm/yyyy',
                    // formatSubmit: 'dd-mm-yyyy'
                    min: 'picker__day--today'
                });
                var enddatepicker = $enddate.pickadate('picker');

                if ($('#system-message-container .alert').hasClass('alert-dateinpast')) {
                    $('.js-event-date .evCategory').addClass('error1');
                    $('#starttime').addClass('error1');
                    $('#endtime').addClass('error1');
                    $('#enddate').addClass('error1');
                    $('#startdate').addClass('error1');
                }

                Date.prototype.addMinutes = function(h){
                    this.setMinutes(this.getMinutes()+h);
                    return this;
                };
                Date.prototype.addHours= function(h){
                    this.setHours(this.getHours()+h);
                    return this;
                };

                var id = "<?php echo $event->id ? $event->id : 0; ?>";

                var default_starttime = new Date();
                var default_endtime = new Date();
                if (id != 0) {
                    default_starttime = "<?php echo date("H:i:s A", strtotime($event->startdate));?>";
                    default_endtime = "<?php echo date("H:i:s A", strtotime($event->enddate));?>";
                } else {
                    default_starttime.addMinutes(30);
                    default_endtime.addHours(1).addMinutes(30);
                }

                $('#starttime').timepicker({
                    template: false,
                    showInputs: false,
                    minuteStep: 5,
                    defaultTime: default_starttime
                }).on('blur', function(e) {
                    if (!$(this).val()) $(this).timepicker('setTime', moment(default_starttime).format('h:mm A'));
                }).on('changeTime.timepicker', function(e) {
                    jQuery('#starttime').removeClass('error1');
                    jQuery('#endtime').removeClass('error1');
                    syncTime();
                });

                $('#endtime').timepicker({
                    template: false,
                    showInputs: false,
                    minuteStep: 5,
                    defaultTime: default_endtime
                });

                function syncTime() {
                    var sdateValue = $('#startdate').val();
                    var edateValue = $('#enddate').val();

                    var sdateText = sdateValue.split('/').reverse().join('/');
                    var edateText = edateValue.split('/').reverse().join('/');

                    var sdate = new Date( sdateText ).getTime(),
                        edate = new Date( edateText ).getTime(),
                        ehour, emin, etime, etime_arr, emin_arr,
                        shour, smin, stime, stime_arr, smin_arr;

                    stime = $('#starttime').val();

                    stime_arr = stime.split(':');
                    smin_arr = stime_arr[1].split(' ');
                    shour = +stime_arr[0];
                    smin  = +smin_arr[0];

                    etime = $('#endtime').val();

                    etime_arr = etime.split(':');
                    emin_arr = etime_arr[1].split(' ');
                    ehour = +etime_arr[0];
                    emin  = +emin_arr[0];

                    var lastSTimeData = $('#endtime').attr('data-last-stime') ? $('#endtime').attr('data-last-stime') : default_starttime;
                    var d = new Date(lastSTimeData).toLocaleString('en-US', {year: 'numeric', month: '2-digit', day: '2-digit', hour: 'numeric', minute: 'numeric', hour12: true });
                    var lastSTime = moment(d, 'MM-DD-YYYY, h:mm A').isValid() ? moment(d, 'MM-DD-YYYY, h:mm A').toDate() : moment("<?php echo date("d/m/Y H:i:s", strtotime($event->startdate));?>", 'DD/MM/YYYY, h:mm A').toDate();

                    var newSTime = moment(sdateValue + ' ' + shour + ':' + smin + ' ' + smin_arr[1], 'DD/MM/YYYY, h:mm A').toDate();

                    var diff = newSTime.getTime() - lastSTime.getTime();
                    var originalETime = moment(edateValue + ' ' + ehour + ':' + emin + ' ' + emin_arr[1], 'DD/MM/YYYY, h:mm A').toDate();

                    var newETime;
                    if (isNaN(lastSTime.getTime()) || lastSTime.getTime() > originalETime.getTime())
                        newETime = newSTime.getTime() + 60 * 60 * 1000;
                    else
                        newETime = originalETime.getTime() + diff;

                    $('#endtime').attr('data-last-stime', newSTime).timepicker('setTime', moment(newETime).format('h:mm A'));

                    if ( !sdate && !edate ) {
                        startdatepicker.set({ select: new Date() });
                        enddatepicker.set({ select: new Date() });
                    } else if (!sdate) {
                        startdatepicker.set({ select:new Date() });
                    } else if (!edate) {
                        enddatepicker.set({ select: newSTime.getTime() });
                    } else {
                        enddatepicker.set({ select: newETime });
                    }
                }
            });
        </script>

<?php if ($config->get('eventshowtimezone')) { ?>
    <div class="joms-form__group">
        <span><?php echo JText::_('COM_COMMUNITY_TIMEZONE'); ?> <span class="joms-required">*</span></span>
        <select class="joms-select" name="offset"
                title="<?php echo JText::_('COM_COMMUNITY_EVENTS_SET_TIMEZONE'); ?>"><?php

            $defaultTimeZone = isset($event->offset) ? $params->get('timezone') : $systemOffset;
            foreach ($timezones as $offset => $value) {

                ?>
                <option value="<?php echo $offset; ?>"<?php echo $defaultTimeZone == $offset ? ' selected="selected"' : ''; ?>><?php echo $value; ?></option><?php
            }
            ?></select>
    </div>

<?php } ?>

<div class="joms-form__group"<?php echo $helper->hasInvitation() ? ' style="margin-bottom:5px;display: none;"' : ''; ?>
     style="display: none;">
    <span><?php echo JText::_('COM_COMMUNITY_EVENTS_NO_SEAT'); ?></span>
    <input type="text" class="joms-input" name="ticket"
           title="<?php echo JText::_('COM_COMMUNITY_EVENTS_NO_SEAT_DESCRIPTION'); ?>"
           value="<?php echo empty($event->ticket) ? 0 : $this->escape($event->ticket); ?>">
</div>

<?php if ($config->get('eventphotos')) { ?>
    <div class="joms-form__group">
        <span><?php echo JText::_('COM_COMMUNITY_EVENTS_RECENT_PHOTO'); ?></span>
        <div>
            <label class="joms-checkbox">
                <input type="checkbox" class="joms-checkbox joms-js--event-photo-flag"
                       name="photopermission-admin" <?php echo ($params->get('photopermission') != EVENT_PHOTO_PERMISSION_DISABLE || $params->get('photopermission') == '') ? 'checked' : '' ?>
                       value="1">
                <span title="<?php echo JText::_('COM_COMMUNITY_EVENTS_PHOTO_PERMISSION_TIPS'); ?>"><?php echo JText::_('COM_COMMUNITY_EVENTS_PHOTO_UPLOAD_ALLOW_ADMIN'); ?></span>
            </label>
        </div>
        <div class="joms-js--event-photo-setting" style="display:none">
            <label class="joms-checkbox">
                <input type="checkbox" class="joms-checkbox"
                       name="photopermission-member" <?php echo ($params->get('photopermission') == 2 || $params->get('photopermission') == '') ? 'checked' : '' ?>
                       value="1">
                <span title="<?php echo JText::_('COM_COMMUNITY_EVENTS_PHOTO_UPLOAD_ALLOW_MEMBER_TIPS'); ?>"><?php echo JText::_('COM_COMMUNITY_EVENTS_PHOTO_UPLOAD_ALLOW_MEMBER'); ?></span>
            </label>
            <select type="text" class="joms-select" name="eventrecentphotos"
                    title="<?php echo JText::_('COM_COMMUNITY_EVENTS_RECENT_PHOTO_TIPS'); ?>">
                <?php for ($i = 2; $i <= 10; $i = $i + 2) { ?>
                    <option value="<?php echo $i; ?>" <?php echo ($params->get('eventrecentphotos') == $i || ($i == 6 && $params->get('eventrecentphotos') == 0)) ? 'selected' : ''; ?>><?php echo $i; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
<?php } ?>

<?php if ($config->get('eventvideos')) { ?>
    <div class="joms-form__group">
        <span><?php echo JText::_('COM_COMMUNITY_EVENTS_RECENT_VIDEO'); ?></span>
        <div>
            <label class="joms-checkbox">
                <input type="checkbox" class="joms-checkbox joms-js--event-video-flag"
                       name="videopermission-admin" <?php echo ($params->get('videopermission') != EVENT_VIDEO_PERMISSION_DISABLE || $params->get('videopermission') == '') ? 'checked' : '' ?>
                       value="1">
                <span title="<?php echo JText::_('COM_COMMUNITY_EVENTS_VIDEO_PERMISSION_TIPS'); ?>"><?php echo JText::_('COM_COMMUNITY_EVENTS_VIDEO_UPLOAD_ALLOW_ADMIN'); ?></span>
            </label>
        </div>
        <div class="joms-js--event-video-setting" style="display:none">
            <label class="joms-checkbox">
                <input type="checkbox" class="joms-checkbox"
                       name="videopermission-member" <?php echo ($params->get('videopermission') == 2 || $params->get('videopermission') == '') ? 'checked' : '' ?>
                       value="1">
                <span title="<?php echo JText::_('COM_COMMUNITY_EVENTS_VIDEO_UPLOAD_ALLOW_MEMBER_TIPS'); ?>"><?php echo JText::_('COM_COMMUNITY_EVENTS_VIDEO_UPLOAD_ALLOW_MEMBER'); ?></span>
            </label>
            <select type="text" class="joms-select" name="eventrecentvideos"
                    title="<?php echo JText::_('COM_COMMUNITY_EVENTS_RECENT_VIDEO_TIPS'); ?>">
                <?php for ($i = 2; $i <= 10; $i = $i + 2) { ?>
                    <option value="<?php echo $i; ?>" <?php echo ($params->get('eventrecentvideos') == $i || ($i == 6 && $params->get('eventrecentvideos') == 0)) ? 'selected' : ''; ?>><?php echo $i; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
<?php } ?>

<script>
    joms.onStart(function ($) {
        $('.joms-js--event-photo-flag').on('click', function () {
            var $div = $('.joms-js--event-photo-setting'),
                $checkbox = $div.find('input');

            if (this.checked) {
                $checkbox.removeAttr('disabled');
                $div.show();
            } else {
                $checkbox[0].checked = false;
                $checkbox.attr('disabled', 'disabled');
                $div.hide();
            }
        }).triggerHandler('click');

        $('.joms-js--event-video-flag').on('click', function () {
            var $div = $('.joms-js--event-video-setting'),
                $checkbox = $div.find('input');

            if (this.checked) {
                $checkbox.removeAttr('disabled');
                $div.show();
            } else {
                $checkbox[0].checked = false;
                $checkbox.attr('disabled', 'disabled');
                $div.hide();
            }
        }).triggerHandler('click');
    });
</script>

<?php if ($afterFormDisplay) { ?>
    <div class="joms-form__group"><?php echo $afterFormDisplay; ?></div>
<?php } ?>

<div class="joms-form__group">
    <span></span>
    <div>
        <?php if (!$event->id) { ?>
            <input name="action" type="hidden" value="save">
        <?php } ?>
        <input type="hidden" name="eventid" value="<?php echo $event->id; ?>">
        <input type="hidden" name="repeataction" id="repeataction" value="">
        <?php echo JHTML::_('form.token'); ?>
        <button type="submit" class="joms-button--primary joms-button--smaller js-button-right btPost">
                    <span class="btAction">
                        <?php echo JText::_($event->id ? 'COM_COMMUNITY_SAVE_BUTTON' : 'COM_COMMUNITY_EVENTS_CREATE_BUTTON'); ?>
                    </span>
            <span class="joms-loading" style="display:none">&nbsp;
                        <img src="<?php echo JURI::root(true) ?>/components/com_community/assets/ajax-loader.gif"
                             alt="loader">
                    </span>
        </button>
        <input type="button" value="<?php echo JText::_('COM_COMMUNITY_CANCEL_BUTTON'); ?>"
               style="margin-right: 10px" class=" joms-button--primary joms-button--smaller js-button-right"
               onclick="history.go(-1); return false;">
    </div>
</div>

</form>
</div>
<script type="text/javascript">
    var currentDate = new Date();
    var timezonelocal = currentDate.getTimezoneOffset() / (-60);
    jQuery('#timezonelocal').val(timezonelocal);

    function ConvertTimeformat(format, str) {
        var time = str;
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM == "PM" && hours < 12) hours = hours + 12;
        if (AMPM == "AM" && hours == 12) hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;
        return (sHours + ":" + sMinutes);
    }

    jQuery('#creat_event').submit(function () {
        jQuery('.btPost').attr("disabled", "disabled");
        var date_start = jQuery("#startdate").val();
        var date_end = jQuery("#enddate").val();
        var endtime = jQuery('#endtime').val();
        var name = jQuery('#ev-name').val().trim();
        var location = jQuery('#ev-location').val().trim();
        var starttime = jQuery('#starttime').val();

        if (name == '' && location != '') {
            jQuery(".mnEventName").addClass("error1");
            jQuery("#ev-name").addClass("error1");
            jQuery('.btPost').removeAttr('disabled');
            return false;
        }
        if (location == '' && name != '') {
            jQuery(".evLocation").addClass("error1");
            jQuery("#ev-location").addClass("error1");
            jQuery('.btPost').removeAttr('disabled');
            return false;
        }
        if (location == '' && name == '') {
            jQuery(".evLocation").addClass("error1");
            jQuery("#ev-location").addClass("error1");
            jQuery(".mnEventName").addClass("error1");
            jQuery("#ev-name").addClass("error1");
            jQuery('.btPost').removeAttr('disabled');
            return false;
        }

        jQuery('#startdate, #starttime, #enddate, #endtime').on('keyup, change', function() {
            validate();
        });

        if (!validate()) {
            return false;
        }

        return true;
    });
    
    function validate() {
        var date_start = jQuery("#startdate").val();
        var date_end = jQuery("#enddate").val();
        var endtime = jQuery('#endtime').val();
        var name = jQuery('#ev-name').val().trim();
        var location = jQuery('#ev-location').val().trim();
        var starttime = jQuery('#starttime').val();

        var valid = true;

        if (date_start.trim() == '') {
            jQuery('.js-event-start .start-date').addClass('hasError');
            valid = false;
        } else jQuery('.js-event-start .start-date').removeClass('hasError');

        if (date_end.trim() == '') {
            jQuery('.js-event-end .end-date').addClass('hasError');
            valid = false;
        } else jQuery('.js-event-end .end-date').removeClass('hasError');

        if (starttime.trim() == '') {
            jQuery('.js-event-start .start-time').addClass('hasError');
            valid = false;
        } else jQuery('.js-event-start .start-time').removeClass('hasError');

        if (endtime.trim() == '' || endtime.trim() == starttime.trim()) {
            jQuery('.js-event-end .end-time').addClass('hasError');
            valid = false;
        } else jQuery('.js-event-end .end-time').removeClass('hasError');
        
        if (date_start.trim() != '' && date_end.trim() != '' && starttime.trim() != '' && endtime.trim() != '') {
            // Get start date element
            var date_start_array =  date_start.split("/");
            var syear = date_start_array[2];
            var smonth = date_start_array[1];
            var sday = date_start_array[0];

            // Get start time element
            var time_start_convert = ConvertTimeformat("24",starttime);
            var time_start_array =  time_start_convert.split(":");
            var smin = time_start_array[1];
            var shour = time_start_array[0];

            // Convert start datetime to object date
            var start_event = new Date(syear, smonth - 1, sday, shour, smin, 0);
            var start_event_to_milliseconds = start_event.getTime();

            //convert only date start_date
            var start_only_date = new Date(syear, smonth - 1, sday).getTime();

            // Get end date element
            var date_end_array =  date_end.split("/");
            var eyear = date_end_array[2];
            var emonth = date_end_array[1];
            var eday = date_end_array[0];

            // Get end time element
            var time_end_convert = ConvertTimeformat("24",endtime);
            var time_end_array =  time_end_convert.split(":");
            var emin = time_end_array[1];
            var ehour = time_end_array[0];

            // Convert end datetime to object date
            var end_event = new Date(eyear, emonth - 1, eday, ehour, emin, 0);
            var end_event_to_milliseconds = end_event.getTime();

            //convert only date start_date
            var end_only_date = new Date(eyear, emonth - 1, eday).getTime();

            // Current time
            var current = new Date();
            var current_to_milliseconds = current.getTime();        

            if(start_only_date > end_only_date){
                jQuery('.js-event-date .evCategory').addClass('error1');
                jQuery('.js-event-end .end-date').addClass('hasError');
                jQuery('.btPost').removeAttr('disabled');
                valid = false;
            }

            if ((start_event_to_milliseconds > end_event_to_milliseconds && start_only_date == end_only_date) || (end_event_to_milliseconds - start_event_to_milliseconds < 60000)) {
                jQuery('.js-event-date .evCategory').addClass('error1');
                jQuery('.js-event-end .end-time').addClass('hasError');
                valid = false;
            }

            if(start_event_to_milliseconds < current_to_milliseconds){
                jQuery('.js-event-date .evCategory').addClass('error1');
                jQuery('.js-event-start .start-time').addClass('hasError');
                jQuery('.btPost').removeAttr('disabled');
                valid = false;
            }
        }
        
        jQuery('.btPost').removeAttr('disabled');
        return valid;
    }

    jQuery("#ev-name").keyup(function () {
        if (jQuery(this).val().trim()) {
            jQuery('.mnEventName').removeClass("error1");
            jQuery('#ev-name').removeClass("error1");
        }

        jQuery('.btPost').removeAttr('disabled');
    });
    jQuery("#ev-location").keyup(function () {
        if (jQuery(this).val().trim()) {
            jQuery('.evLocation').removeClass("error1");
            jQuery('#ev-location').removeClass("error1")
        }
        jQuery('.btPost').removeAttr('disabled');
    });

</script>