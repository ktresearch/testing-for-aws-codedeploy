<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();

if (!isset($pageTitle)) {
    $task = JFactory::getApplication()->input->getCmd('task');
    $pageTitle = JText::_($task === 'display' ? 'COM_COMMUNITY_ADVANCESEARCH_SEARCH_RESULTS' : 'COM_COMMUNITY_ALL_MEMBERS');
}
$session = JFactory::getSession();
$device = $session->get('device');
$searchModel = CFactory::getModel('search');

foreach($resultRows as $row ) : ?>
        <?php $displayname = $row->user->getDisplayName(); ?>
        <?php if(!empty($row->user->id) && !empty($displayname)&& $my->id!=$row->user->id) : ?>
            <?php 
            $isFriend = CFriendsHelper::isConnected($row ->id, $my->id);
        $isWaitingApproval = CFriendsHelper::isWaitingApproval($my->id, $row ->id);
        $isWaitingResponse = CFriendsHelper::isWaitingApproval($row ->id, $my->id);
            ?>
          
        <li class="col-xs-12">
            <div class='joms-list--pic col-xs-3'>
             <a href="<?php echo $row->profileLink; ?>" alt="<?php echo $row->user->getDisplayName(); ?>">
                <img src="<?php echo $row->user->getThumbAvatar(); ?>"/>
                </a>
            </div>
            <div class='joms-list--info col-xs-9'>
                
                <a href="<?php echo $row->profileLink; ?>" alt="<?php echo $row->user->getDisplayName(); ?>">
                <div class="content">
                <p class='name'><?php echo $row->user->getDisplayName(); ?></p>
                </a>
                <?php 
                    $cUser = CFactory::getUser($row->user->id);
                    $occupation = $cUser->getInfo('FIELD_OCCUPATION');          
                ?>
                <p class='occupation joms--description' style="margin-top: -10px">
                    <?php echo $occupation; ?>
                </p>
                </div>
             
                <div class="bt_friend">
                        <?php if ($isFriend) { ?>                  
                <a href="javascript:" class="joms-focus__button--friend" onclick="joms.api.friendRemove('<?php echo $row->id;?>')">
                               <?php echo JText::_('COM_COMMUNITY_CIRCLE_FRIEND'); ?>
                           </a>   
                           
                                  <?php } else if ( $isWaitingApproval ) { ?>
                        <a href="javascript:" onclick="joms.api.friendAdd('<?php echo $row->id;?>')">
                            <button type="button" class="joms-button--primary button--small pending" style="float: right;">
                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span> 
                                <?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING'); ?>
                            </button>
                        </a>
                    <?php } else if ($isWaitingResponse ) { ?>
                        <a href="javascript:" onclick="joms.api.friendResponse('<?php echo $row->id;?>')">
                            <button style="float: right;" type="button" class="joms-button--primary button--small pending">
                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span> 
                                <?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING'); ?>
                            </button>
                        </a>
                    <?php  } else {?>
   
                           <a href="javascript:" class="joms-focus__button--add" onclick="joms.api.friendAdd('<?php echo $row->id?>')">
                                <?php echo JText::_('COM_COMMUNITY_CIRCLE_ADD_FRIEND'); ?>
                            </a>
                       <?php }?> 
               </div>
                            </div>
               </li>
                          
        <?php endif; ?>
    <?php endforeach; ?>  
