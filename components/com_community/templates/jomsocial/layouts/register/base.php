<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();
?>
<style type="text/css">
    body {
        padding: 0;
        margin: 0;
    }
    #system-message-container {
        position: fixed;
        top: 10px;
        right: 10px;
        z-index: 1;
        margin-left: 10px;
    }
    #system-message-container .alert {
        padding: 10px 15px;
        border-radius: 1px;
    }
    #system-message-container .alert h4 {
        margin: 0;
        line-height: 30px;
    }
    #system-message-container .alert p {
        margin: 0;
    }
    body.overlay2:after {
        content: "";
        display: block;
        position: fixed;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        z-index: 10000;
        background-color: rgba(189, 189, 189, 0.47);
    }
    .notification {
        position: fixed;
        top: 50%;
        left: 50%;
        min-width: 270px;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        -webkit-transform:  translate(-50%, -50%);
        -moz-transform:  translate(-50%, -50%);
        -o-transform:  translate(-50%, -50%);
        text-align: center;
        background: #126DB6;
        color: white;
        padding: 20px;
        border-radius: 5px;
        z-index: 11111;
        display: none;
    }

    @keyframes spin {
        0% { -webkit-transform: translate(0, -50%) rotate(0deg);
            -moz-transform: translate(0, -50%) rotate(0deg);
            -ms-transform: translate(0, -50%) rotate(0deg);
            -o-transform: translate(0, -50%) rotate(0deg);
            transform: translate(0, -50%) rotate(0deg);
        }
        100% {
            -webkit-transform: translate(0, -50%) rotate(360deg);
            -moz-transform: translate(0, -50%) rotate(360deg);
            -ms-transform: translate(0, -50%) rotate(360deg);
            -o-transform: translate(0, -50%) rotate(360deg);
            transform: translate(0, -50%) rotate(360deg);
        }
    }
</style>
<div class="signup-back">
    <a href="javascript:void(0);" onclick="goBack();"> <img src="<?php echo JURI::root();?>images/backbutton.png"></a>
</div>
<div class="joms-page sign-up-page">

    <div class="signup-title"><h4><?php echo JText::_("COM_COMMUNITY_FRONT_SIGN_UP"); ?></h4></div>

    <div class="signup-message">
        <?php echo JText::_("COM_COMMUNITY_FRONT_SIGN_UP_MESSAGE"); ?>
    </div>

    <form method="POST" action="<?php echo CRoute::getURI(); ?>" onsubmit="return joms_validate_form( this );">

        <?php if ($isUseFirstLastName) { ?>

            <div class="joms-form__group joms-input--append">

                <input type="text" placeholder="<?php echo JText::_('COM_COMMUNITY_FIRST_NAME'); ?>" name="jsfirstname" value="<?php echo $data['html_field']['jsfirstname']; ?>" class="joms-input"
                       data-required="true">
            </div>

            <div class="joms-form__group joms-input--append">

                <input type="text" placeholder="<?php echo JText::_('COM_COMMUNITY_LAST_NAME'); ?>" name="jslastname" value="<?php echo $data['html_field']['jslastname']; ?>" class="joms-input"
                       data-required="true">
            </div>

        <?php } else { ?>

            <div class="joms-form__group joms-input--append">

                <input type="text" placeholder="<?php echo JText::_('COM_COMMUNITY_FIRST_NAME'); ?>" name="jsfirstname" value="<?php echo $data['html_field']['jsfirstname']; ?>" class="joms-input"
                       data-required="true">
            </div>

            <div class="joms-form__group joms-input--append">

                <input type="text" placeholder="<?php echo JText::_('COM_COMMUNITY_LAST_NAME'); ?>" name="jslastname" value="<?php echo $data['html_field']['jslastname']; ?>" class="joms-input"
                       data-required="true">
            </div>

        <?php } ?>

        <div class="joms-form__group joms-input--append">

            <input type="text" placeholder="<?php echo JText::_('COM_COMMUNITY_EMAIL'); ?>" id="jsemail" name="jsemail" value="<?php echo $data['html_field']['jsemail']; ?>" class="joms-input"
                   data-required="true" data-validation="email">
        </div>

        <div class="joms-form__group joms-input--append">

            <input type="password" placeholder="<?php echo JText::_('COM_COMMUNITY_PASSWORD'); ?>" id="jspassword" name="jspassword" value="" class="joms-input" autocomplete="off"
                   data-required="true" data-validation="password">
        </div>

        <div class="joms-form__group joms-input--append">

            <input type="password" placeholder="<?php echo JText::_('COM_COMMUNITY_RE_ENTER_PASSWORD'); ?>" name="jspassword2" value="" class="joms-input" autocomplete="off"
                   data-required="true" data-validation="password:#jspassword">
        </div>
        <div class="signup-condition"><?php echo JText::_("COM_COMMUNITY_SIGN_UP_CONDITION") ?>
            <a href="<?php echo JURI::root().'terms' ?>?banner=1" ><?php echo JText::_("COM_COMMUNITY_TERMS") ?></a>
            <?php echo JText::_("COM_COMMUNITY_AND") ?> <a href="<?php echo JURI::root().'privacy-policy' ?>?banner=1"><?php echo JText::_("COM_COMMUNITY_PRIVACY_POLICY") ?></a>.
        </div>
        <?php if ( !empty($recaptchaHTML) ) { ?>
            <div class="joms-form__group">
                <span></span>
                <?php echo $recaptchaHTML; ?>
            </div>
        <?php } ?>

        <div class="joms-form__group signup-submit">
            <!--<span></span>-->
            <button type="submit" name="submit" class="joms-button--login joms-button--full-small buttonSignup">
                <?php echo JText::_('COM_COMMUNITY_SIGN_UP'); ?>
                <span class="joms-loading" style="display:none">&nbsp;
                    <img src="<?php echo JURI::root(true) ?>/components/com_community/assets/ajax-loader.gif" alt="loader">
                </span>
            </button>

            <?php if ( $config->get('enableterms') ) { ?>
                <div class="joms-checkbox hidden">
                    <input type="checkbox" checked name="tnc" value="Y" data-message="<?php echo JText::_('COM_COMMUNITY_REGISTER_ACCEPT_TNC'); ?>">
                    <span><?php echo JText::_('COM_COMMUNITY_I_HAVE_READ') . ' <a href=" '. CRoute::_('index.php?option=com_community&view=register&task=registerTerms') .'" target="_blank">' . JText::_('COM_COMMUNITY_TERMS_AND_CONDITION') . '</a>.'; ?></span>
                </div>
            <?php } ?>

        </div>

        <?php if ( $fbHtml ) { ?>
            <div class="joms-form__group">
                <span></span>
                <?php //echo $fbHtml;?>
            </div>
        <?php } ?>

        <input type="hidden" name="isUseFirstLastName" value="<?php echo $isUseFirstLastName; ?>">
        <input type="hidden" name="task" value="register_save">
        <input type="hidden" name="id" value="0">
        <input type="hidden" name="gid" value="0">
        <input type="hidden" id="authenticate" name="authenticate" value="0">
        <input type="hidden" id="authkey" name="authkey" value="">

    </form>

    <div class="login-using-text">
        <span class="first"></span>
        <span class="second"><?php echo JText::_('COM_COMMUNITY_SIGN_UP_WITH'); ?></span>
        <span class="third"></span>
    </div>
    <div class="login-using-social">
        <a id="btFacebook" href="#" onclick="checkLoginState(event);">
            <img src="<?php echo JURI::root(true); ?>/images/login/facebook_150.png">
        </a>
        <a id="btGooglePlus" href="#">
            <img src="<?php echo JURI::root(true); ?>/images/login/google_150.png">
        </a>
        <a id="btLinkedin" href="#" onclick="checkLinkedInLogin(event);">
            <img src="<?php echo JURI::root(true); ?>/images/login/linkedin_150.png">
        </a>
    </div>
    <?php
    $userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
    $iPod    = stripos($userAgent, "ipod");
    $iPhone  = stripos($userAgent, "iphone");
    $iPad    = stripos($userAgent, "ipad");
    $android = stripos($userAgent, "android");

    $iOS = ( $iPod || $iPhone || $iPad ) ? true : false;
    ?>
    <div class="divDownloadApp">
        <p><?php echo JText::_('COM_COMMUNITY_GET_THE_APP');//echo JText::_('COM_COMMUNITY_DOWNLOAD_APP'); ?></p>
        <a <?php echo ($iOS) ? "style='display: none;'" : ""; ?> href="#">
            <img src="<?php echo JURI::root(true); ?>/images/login/googleplay.png">
        </a>
        <a <?php echo ($android) ? "style='display: none;'" : ""; ?> href="#">
            <img src="<?php echo JURI::root(true); ?>/images/login/appstore.png">
        </a>
    </div>
    <div class="divLogin">
        <p><?php echo JText::_('COM_COMMUNITY_HAVE_AN_ACCOUNT'); ?><a href="<?php echo JUri::base();?>">
                <?php echo JText::_('COM_COMMUNITY_LOG_IN'); ?>
            </a></p>

    </div>

</div>
<div class="notification"></div>

<div class="div-footer-menu">
    <ul>
        <li><a href="<?php echo JURI::root(true); ?>/about"><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/terms"><?php echo JText::_('COM_COMMUNITY_TERMS'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/privacy-policy"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/support"><?php echo JText::_('COM_COMMUNITY_SUPPORT'); ?></a></li>
        <!-- <li><a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/business"><?php echo JText::_('COM_COMMUNITY_BUSINESS'); ?></a></li> -->
    </ul>
</div>
<div class="divCopyright">
    <p>© <?php echo Date("Y", time());?> PARENTHEXIS</p>
</div>


<script>

    // Validate form before submit.
    function joms_validate_form( form ) {
        if ( window.joms && joms.util && joms.util.validation ) {
            // Prevents repeated clicks.
            if ( window.joms_validating_form ) return;
            window.joms_validating_form = true;
            joms.jQuery('.joms-loading').show();

            joms.util.validation.validate( form, function( errors ) {
                if ( !errors ) {
                    joms.jQuery( form ).removeAttr('onsubmit');
                    setTimeout(function() {
                        joms.jQuery( form ).find('button[type=submit]').click();
                    }, 500 );
                } else {
                    joms.jQuery('.joms-loading').hide();
                    window.joms_validating_form = false;
                }
            });
        }
        return false;
    }

    joms.onStart(function( $ ) {
        function insertAuthkey() {
            joms.ajax({
                func: 'register,ajaxGenerateAuthKey',
                data: [ '_dummy_' ],
                callback: function( json ) {
                    joms.jQuery('#authenticate').val( 1 );
                    joms.jQuery('#authkey').val( json.authKey );
                    joms.jQuery('#login-form input, #form-login input, form[name=login] input').filter(function() {
                        return this.name.match(/[0-9a-z]{32}/);
                    }).prop( 'name', json.authKey );
                }
            });
        }

        var timer = setInterval(function() {
            if ( joms.ajax ) {
                clearInterval( timer );
                insertAuthkey();
            }
        }, 100);

    });

    jQuery(".outerpgbar").hide();
    function goBack() {
        // window.history.back();
        var nav = window.navigator;
        if( this.phonegapNavigationEnabled &&
            nav &&
            nav.app &&
            nav.app.backHistory ){
            nav.app.backHistory();
        } else {
            window.history.back();
        }
    }
</script>
