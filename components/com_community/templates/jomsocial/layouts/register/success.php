<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();
?>
<style type="text/css">
    .tab .register-success-page {
        margin-top: 65px;
    }
    .mob .register-success-page {
        margin-top: 51px;
    }
</style>
<div class="joms-page register-success-page">
    <h4><?php echo JText::_('COM_COMMUNITY_YOUR_ACCOUNT_IS_CREATED'); ?></h4>
	<p class="register-message"><?php echo $message; ?></p>
	<a href="<?php echo JURI::root(true); ?>" class="joms-button--primary joms-button--full-small btBacktoHome">
		<?php echo JText::_('COM_COMMUNITY_BACK_HOME') ?>
	</a>
</div>
<div class="div-footer-menu">
    <ul>
        <li><a href="<?php echo JURI::root(true); ?>/about"><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/terms"><?php echo JText::_('COM_COMMUNITY_TERMS'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/privacy-policy"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/support"><?php echo JText::_('COM_COMMUNITY_SUPPORT'); ?></a></li>
        <!-- <li><a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/business"><?php echo JText::_('COM_COMMUNITY_BUSINESS'); ?></a></li> -->
    </ul>
</div>
<div class="divCopyright">
    <p>© <?php echo Date("Y", time()).' '.JFactory::getApplication()->get('sitename');?> </p>
</div>