<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();

$title = '';

if ($type == COMMUNITY_EVENT_STATUS_ATTEND) {
    $title = JText::_('COM_COMMUNITY_EVENTS_CONFIRMED_GUESTS');
} else if ($type == COMMUNITY_EVENT_STATUS_WONTATTEND) {
    $title = JText::_('COM_COMMUNITY_EVENTS_WONT_ATTEND');
} else if ($type == COMMUNITY_EVENT_STATUS_MAYBE) {
    $title = JText::_('COM_COMMUNITY_EVENTS_MAYBE_ATTEND');
} else if($type == COMMUNITY_EVENT_STATUS_BANNED) {
    $title = JText::_('COM_COMMUNITY_EVENTS_BANNED_MEMBERS');
}else{
    $title = JText::_('COM_COMMUNITY_REQUESTED_INVITATION');
}

?>

<div class="joms-list__item bordered-box">
    <?php if( $guests ) { ?>
    <div id="notice" class="alert alert-notice" style="display:none;">
        <a class="close" data-dismiss="alert">×</a>
        <div id="notice-message"></div>
    </div>

    <div class="col-event joms-list--friend col-xs-12 custom-header" style="background: none">
        <h1><?php echo JText::_('COM_COMMUNITY_EVENTS_PEO_ATTENDING');?></h1>
    <?php foreach( $guests as $guest ){ ?>
      	<div class="userAvatar">
            <a class="cIndex-Avatar" href="<?php echo CRoute::_('index.php?option=com_community&view=profile&userid=' . $guest->id); ?>">
                <img src="<?php echo $guest->getThumbAvatar(); ?>" data-author="<?php echo $guest->id; ?>" class="cAvatar"/>
                <p><?php echo $guest->getDisplayName(); ?></p>
            </a>
        </div>
    <?php } ?>
    </div>

    <?php } else { ?>
    <div class="joms-alert"><?php echo JText::_('COM_COMMUNITY_EVENTS_NO_USERS'); ?></div>
    <?php } ?>

    <?php if ($pagination->getPagesLinks() && ($pagination->pagesTotal > 1 || $pagination->get('pages.total') > 1) ) { ?>
        <div class="joms-pagination">
            <?php echo $pagination->getPagesLinks(); ?>
        </div>
    <?php } ?>
</div>

<script>
	jQuery( ".joms-subnav__menu" ).remove();
</script>
