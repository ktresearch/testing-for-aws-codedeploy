<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();
$enddate = empty($bulletin->endtime) ? '' : date('d/m/Y', $bulletin->endtime);
$endtime = empty($bulletin->endtime) ? '' : date('H:i A', $bulletin->endtime);
?>
<script src="<?php echo JURI::base(); ?>components/com_community/assets/autosize.min.js"></script>
<script src="<?php echo JURI::base(); ?>components/com_community/assets/release/js/picker.js"></script>
<script src="<?php echo JURI::base(); ?>components/com_community/assets/release/js/picker.date.js"></script>
<script src="<?php echo JURI::base(); ?>components/com_community/templates/jomsocial/assets/js/bootstrap-timepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo JURI::base(); ?>components/com_community/templates/jomsocial/assets/css/bootstrap-timepicker.min.css" />

<?php if(!$isAdmin) { ?>
    <div class="joms-list__item bordered-box">
        <div class="col-event custom-header pull-left">
            <h1><?php echo JText::_( 'COM_COMMUNITY_GROUPS_BULETIN_TITLE' ); ?></h1>
            <p class="margin-0 joms--description"><?php echo $bulletin->title; ?></p>

            <h1><?php echo JText::_('COM_COMMUNITY_GROUPS_BULLETIN_DESCRIPTION'); ?></h1>
            <p class="margin-0 joms--description"> <?php echo $bulletin->message;?> </p>

            <h1><?php echo JText::_('COM_COMMUNITY_EVENTS_END_DATE'); ?> and <?php echo JText::_('COM_COMMUNITY_EVENTS_END_TIME'); ?></h1>
            <p class="margin-0 joms--description"><?php echo $enddate . ' ' . $endtime ;?></p>
        </div>
    </div>
<?php } ?>

<div class="joms-sidebar">
    <div class="joms-module__wrapper">
        <?php //echo $filesharingHTML;?>
    </div>
</div>

<div class="joms-main">
    <div class="joms-page">

        <div class="joms-comment--bulletin">
            <!--            <div class="joms-gap"></div>-->
            <div class="joms-js--announcement-view-<?php echo $bulletin->groupid ?>-<?php echo $bulletin->id ?>">
                <p>
                    <?php //echo 'Description: '. $bulletin->message;?>
                </p>
            </div>
            <?php if($isAdmin) { ?>
                <div class="joms-js--announcement-edit-<?php echo $bulletin->groupid ?>-<?php echo $bulletin->id ?>">
                    <form method="POST" id="announcement_create" action="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=editnews'); ?>">
                        <div class="">
                            <!-- <span><?php //echo JText::_('COM_COMMUNITY_GROUPS_BULLETIN_TITLE'); ?> <span class="joms-required">*</span></span> -->
                            <input type="text" value="<?php echo $bulletin->title; ?>" name="title" id="title"
                                   class="joms-input hidden"
                                   placeholder="<?php echo JText::_('COM_COMMUNITY_GROUPS_ADD_NEWS_TITLE_PLACEHOLDER'); ?>"/>
                        </div>

                        <div class="joms-form__group content_announcement">
                            <span class="header"><?php echo JText::_('COM_COMMUNITY_ANNOUNCEMENT_CONTENT'); ?><span class="joms-required">*</span></span>
                            <textarea name="message" id="message" class="joms-textarea"><?php echo $editorMessage; ?></textarea>
                            <script type="text/javascript">
                                jQuery('#message').each(function () {
                                    autosize(this);
                                }).on('input', function () {
                                    autosize(this);
                                });
                            </script>
                        </div>


                        <div class="joms-form__group container time_announcement">
                            <span class="header"><?php echo JText::_('COM_COMMUNITY_ANNOUNCEMENT_END_DATE_TIME'); ?><span class="joms-required">*</span></span>

                            <div class="col-xs-12 col-sm-6 col-md-6 full" style="clear: both;">
                                <div class="end-date col-xs-5 col-sm-3 col-md-3 full">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input type="text" style="text-align: center; cursor: pointer;" placeholder="dd/mm/yy" value="<?php echo empty($bulletin->endtime) ? '' : $enddate;?>" readonly name="announcement-enddate" id="announcement-enddate" />
                                    </div>

                                    <script type="text/javascript">
                                        jQuery('#announcement-enddate').pickadate({
                                            format: 'dd/mm/yyyy',
                                            formatSubmit: 'dd-mm-yyyy',
                                            min: 'picker__day--today'
                                        });
                                    </script>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 full space" style="text-align: center;">-</div>
                                <div class="end-time col-xs-4 col-sm-3 col-md-3 full">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input type="text" style="text-align: left;" name="announcement-endtime"
                                               id="announcement-endtime"/>
                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span> -->
                                    </div>
                                    <script type="text/javascript">
                                        var time = new Date();
                                        var  hour = time.getHours(); // 0-24 format
                                        var  minute= time.getMinutes();
                                        if(hour >= 12) var currenttime = hour+":"+minute+" PM";
                                        else var currenttime = hour+":"+minute+" AM";
                                        jQuery('#announcement-endtime').timepicker({
                                            defaultTime: '<?php echo empty($bulletin->endtime) ? 'currenttime' : $endtime?>'
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                        <div class="notifi"></div>


                        <div class="joms-form__group js-form-edit-group">
                            <span></span>
                            <input type="hidden" value="<?php echo $bulletin->groupid; ?>" name="groupid">
                            <input type="hidden" value="<?php echo $bulletin->id; ?>" name="bulletinid">
                            <input type="submit" class="joms-button--primary button--small btSave pull-right" value="<?php echo JText::_('COM_COMMUNITY_SAVE_BUTTON');?>">
                            <input type="button" onclick="location.href='<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewdiscussions&groupid=' . $bulletin->groupid); ?>';" class="joms-button--primary button--small btCancel pull-right" name="cancel" value="<?php echo JText::_('COM_COMMUNITY_CANCEL_BUTTON'); ?>"/>
                            <?php echo JHTML::_( 'form.token' ); ?>
                        </div>

                        <script type="text/javascript">
                            jQuery('#announcement_create').submit(function () {
                                if(jQuery('#announcement-endtime').val() == ''  ){
                                    jQuery('.end-time').addClass('hasError');
                                    return false;
                                }
                                jQuery('#announcement-enddate, #announcement-endtime').on('keyup, change', function() {
                                    validate();
                                });

                                if (!validate()) {
                                    return false;
                                }

                                return true;
                            });

                            function validate() {
                                if (jQuery('#message').val().trim() == '') {
                                    jQuery('.content_announcement').addClass('hasError');
                                    return false;
                                } else jQuery('.content_announcement').removeClass('hasError');

                                var date_announcement = jQuery("input[name=announcement-enddate_submit]").val();
                                
                                if (date_announcement.trim() == '') {
                                    jQuery('.end-date').addClass('hasError');
                                    return false;
                                }
                                else jQuery('.end-date').removeClass('hasError');

                                var arrDate = date_announcement.split('-');

                                var now = new Date();
                                var time;

                                time = new Date(arrDate[2], (parseInt(arrDate[1]) - 1), arrDate[0], 23, 59, 59, 0);
                                if (now > time) {
                                    jQuery('.end-date').addClass('hasError');
                                    return false;
                                } else {
                                    jQuery('.end-date, .end-time').removeClass('hasError');
                                    var arrTime = jQuery('#announcement-endtime').val().split(' ');
                                    var endtime = arrTime[0].split(':');

                                    if (arrTime[1] == 'AM') {
                                        time = new Date(arrDate[2], (parseInt(arrDate[1]) - 1), arrDate[0], endtime[0] == 12 ? 0 : endtime[0], endtime[1], 0, 0);
                                    } else {
                                        time = new Date(arrDate[2], (parseInt(arrDate[1]) - 1), arrDate[0], (parseInt(endtime[0] == 12 ? 0 : endtime[0]) + 12), endtime[1], 0, 0);
                                    }
                                    if (now > time) {
                                        jQuery('.end-time').addClass('hasError');  
                                        return false;
                                    } else jQuery('.end-date, .end-time').removeClass('hasError');
                                }
                                return true;
                            }

                            jQuery('#message').keyup(function () {
                                if(jQuery('.content_announcement').hasClass('hasError')){
                                    jQuery('.content_announcement').removeClass('hasError');
                                }
                            });

                            jQuery(document).ready(function() {
                                if(jQuery('#system-message-container .alert').hasClass('alert-error-time')){
                                    jQuery('#announcement-endtime').addClass('error1');
                                }
                            });
                        </script>
                    </form>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
