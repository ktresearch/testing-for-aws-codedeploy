<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();
CAssets::attach('assets/script-1.2.min.js', 'js');
CAssets::attach('assets/easytabs/jquery.easytabs.min.js', 'js');
$name = "";
$session = JFactory::getSession();
$device = $session->get('device');

if($device !== 'mobile')
{
    $title_admin = JText::_('COM_COMMUNITY_MEMBERS_TITLE_ADMINISTRATOR');
}
else
{
         $title_admin = JText::_('COM_COMMUNITY_MEMBERS_TITLE_MANAGERS');
     }
if(isset($_REQUEST['name'])) {
    $name = $_REQUEST['name'];
}
?>
<script src="components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        var title_p = '<?php echo JText::_('COM_COMMUNITY_TITLE_CIRCLE_MEMBERS')?>';
        $('.navbar-header .navbar-title span').html(title_p);    
    });
    
          </script> 
    <style>
  .joms-page {
    background: #e1e1e1;
  margin-top: -2%;
}
 .arrow-down {
    left: 82%;
}
    </style>
    <div class="viewmember">
  <div class="joms-page">
                <div id="joms-group--details" class="joms-tab__content" style="">
<div id="Administrator">
       <p class="name_admin"><?php echo  $title_admin ?></p>
  <?php foreach ($members as $member) {?>
    <?php  if($member->isAdmin||$member->isOwner) {?>
<div class="view_member">  
  
      <div class="joms-avatar--comment <?php echo CUserHelper::onlineIndicator($member); ?>">
                            <a class="joms-avatar" href="<?php echo CRoute::_('index.php?option=com_community&view=profile&userid=' . $member->id); ?>">
                                <img src="<?php echo $member->getThumbAvatar(); ?>" alt="<?php echo $member->getDisplayName(); ?>" data-author="<?php echo $member->id; ?>" />
                            </a>
                        </div>
     <p class="name_member"><?php echo $member->getDisplayName(); ?></p>
     <p class="name_member"> <?php echo JText::_('COM_COMMUNITY_CIRCLE_ADMIN'); ?></p>
 
</div> 


<?php } ?>
  <?php } ?>
   </div>
        <div class="list_member">   
                   <p class="title_member"> <?php echo JText::_('COM_COMMUNITY_TITLE_CIRCLE_MEMBERS'); ?></p>
      <?php foreach ($members as $member) {?>
           <?php  if(!$member->isAdmin && !$member->isOwner) {?>
      <?php $isFriend = CFriendsHelper::isConnected($member->id, $my->id); ?>
<div class="view_member">
<?php if($isMember) { ?>
                   <div class="button">
                                 <?php if($isFriend) {?>
      <a href="javascript:" class="joms-focus__button--add" onclick="joms.api.friendAdd('<?php echo $member->id?>')">
                            <?php echo JText::_('COM_COMMUNITY_CIRCLE_ADD_FRIEND'); ?>
                        </a>
                                 <?php } else { ?>
                        <a href="javascript:" class="joms-focus__button--friend" onclick="joms.api.friendRemove('<?php echo $profile->id;?>')">
                        <?php echo JText::_('COM_COMMUNITY_CIRCLE_FRIEND'); ?>
                    </a>
                                 <?php } ?>
                  </div>
                  <?php } ?>
    <div class="content-right">
   <p class="name_member1"><?php echo $member->getDisplayName(); ?></p> 
      <div class="joms-avatar--comment <?php echo CUserHelper::onlineIndicator($member); ?>">
                            <a class="joms-avatar" href="<?php echo CRoute::_('index.php?option=com_community&view=profile&userid=' . $member->id); ?>">
                                <img src="<?php echo $member->getThumbAvatar(); ?>" alt="<?php echo $member->getDisplayName(); ?>" data-author="<?php echo $member->id; ?>" />
                            </a>
                        </div>

    </div>

</div> 
<?php } ?>
                   <?php } ?>
    </div>
    </div>
    </div>
    </div>



