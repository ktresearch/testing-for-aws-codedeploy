<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/

/*
Added by Chris for hikashop categories listing
*/

?>
<div class="hikashop_products_listing" style="padding-top: 10px; background-color: #FFF !important">
   <div class="hikashop_products" data-consistencyheight="">
      <div class="row-fluid">
         <ul class="thumbnails">
         <form action="/index.php?option=com_community&view=groups&task=publishhikashop" method="post">
            <input type="hidden" name="courseid" value="<?php echo $_GET['courseid'] ?>">
            <input type="hidden" name="groupid" value="<?php echo $_GET['groupid'] ?>">
            <input type="hidden" name="lpcatid" value="<?php echo $_GET['lpcatid'] ?>">
            <?php foreach ($categories as $category) { ?>
                <li class="span6 hikashop_product hikashop_product_column_1 hikashop_product_row_1">
                   <div class="hikashop_container">
                      <div class="hikashop_subcontainer ">
                         <div class="hika_listing_img_desc" style="font-family:'Open Sans';">
                            <div class="row" style="margin:0px;padding:15px;display:block;text-decoration:none;color:black;">
                               <div class="col-sm-6 hikashop_product_listing_middle" style="width:100%;float:left;">
                                    
                                    <div style="float: left; padding-right: 10px">
                                    <input id="chkbox" name="categoryid[]" value="<?php echo $category->category_id;?>" type="checkbox" />
                                    </div>

                                    <div class="row hikashop_product_title" style="margin:0px;">
                                            <?php echo $category->category_name ?>
                                    </div>

                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                </li>
            <?php } ?>
            <div style="padding: 30px">
            <input class="joms-button--primary button--small" type="submit" name="formSubmit" value="<?php echo JText::_('COM_COMMUNITY_BUTTON_PUBLISH'); ?>" />
            </div>
         </form>   
         </ul>
      </div>
      <div style="clear:both"></div>
   </div>
</div>
