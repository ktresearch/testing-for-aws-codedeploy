<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();

$is_mobile = false;
$session = JFactory::getSession();
$device = $session->get('device');
if($device == 'mobile')
{
    $is_mobile = true;
}
?>

<?php if(isset($groupMiniHeader) && $groupMiniHeader){ ?>
<div class="joms-body">
    <?php echo $groupMiniHeader; ?>
</div>
<?php } ?>


<?php
    $task = JFactory::getApplication()->input->getCmd('task');
	
 ?>
<style type="text/css">
    .view-events #txtKeyword.onkeyup {
        background-image: url('') !important;
    }
    .joms-tab__bar .active {background-color: #FFF !important; font-weight: bold;}
    .joms-tab__bar .border-left {border-left: thin solid #8F8F8F;}
</style>


<script src="components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery( "input.joms-input--search" ).keyup(function() {
           var value = jQuery( this ).val();          
           if (value != '') {
               jQuery(this).addClass('onkeyup');
           } else {
               jQuery(this).removeClass('onkeyup');
            }
        })
      .keyup();
      
    });
    
</script>
 
<div style="background: none;">	

<div class="joms-tab__bar" style="margin-bottom: 30px">
        <a href="<?php echo CRoute::_("index.php?option=com_community&view=groups&task=mygroups") ?>" ><?php echo JText::_('COM_COMMUNITY_TITLE_MYCIRCLES');?></a>
        <a href="<?php echo CRoute::_("index.php?option=com_community&view=groups&task=allgroups") ?>" class="border-left"> <?php echo JText::_('COM_COMMUNITY_TITLE_ALLCIRCLES');?></a>
        <a href="<?php //echo CRoute::_("index.php?option=com_community&view=events&task=myevents")
        echo JUri::base().'myevents/list.html'; ?>" class="border-left active"> <?php echo JText::_('COM_COMMUNITY_EVENTS');?></a>
</div>

<!--    <div class="joms-tab__bar">
        <a href="<?php // echo CRoute::_("index.php?option=com_community&view=events&task=myevents") ?>" <?php // if ($task == 'myevents') echo 'class="active"' ?>>My Events</a>
        <a href="<?php // echo CRoute::_("index.php?option=com_community&view=events&task=display") ?>"  <?php // if ($task != 'myevents') echo 'class="active"' ?>>All Events</a>
</div>-->
<!-- SEARCH REMOVED -->

<!--
    <div class="row custom-search-box margin-0">
        <form method="POST" action="<?php echo CRoute::_('index.php?option=com_community&view=events&task=search'); ?>">
            <div class="input-group">
              <span class="input-group-btn">
                <div class="icon-addon addon-lg">
                    
                    <?php if($is_mobile) {?>
                    <input id="txtKeyword" type="text" class="form-control joms-input--search" name="search">
                    <?php } else { ?>
                    <input id="txtKeyword" type="text" class="form-control joms-input--search" name="search" placeholder="<?php echo JText::_('COM_COMMUNITY_FRIENDS_SEARCH_FRIENDS'); ?>">
                    <?php } ?>
                    
                    <label for="search" class="glyphicon glyphicon-search" style="margin-top: -5px" color="#fff" rel="tooltip"></label>
                </div>
                <?php echo JHTML::_('form.token') ?>
                <span class="input-group-btn"> 
                </span>
                </span>
            </div>
            <input type="hidden" name="option" value="com_community"/>
            <input type="hidden" name="view" value="events"/>
            <input type="hidden" name="task" value="search"/>
            <input type="hidden" name="Itemid" value="<?php echo CRoute::getItemId(); ?>"/>
            <input type="hidden" name="posted" value="1"/>

        </form>
    </div>
-->   

    <?php echo $eventsHTML;?>
</div>
