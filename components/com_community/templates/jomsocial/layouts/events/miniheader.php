<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();

$featured = new CFeatured(FEATURED_EVENTS);
$featuredList = $featured->getItemIds();

$session = JFactory::getSession();
 $device = $session->get('device');
 $is_mobile = false;
if($device == 'mobile'){
    $is_mobile = true;
    $titleLength= $config->get('header_title_length', 25);
} else {
    $titleLength= $config->get('header_title_length', 70);
}
$summaryLength = $config->get('header_summary_length', 80);

$enableReporting = false;
if ( $config->get('enablereporting') == 1 && ( $my->id > 0 || $config->get('enableguestreporting') == 1 ) ) {
    $enableReporting = true;
}

$isGroupEvent = $event->type == CEventHelper::GROUP_TYPE;

$name = "";
if(isset($_REQUEST['task']))
{
    $name = $_REQUEST['task'];
}
$active_about= "";
$active_attending = "";

if($name == 'viewguest')
{
    $active_attending = "active";
}
 else {
    $active_about = "active";
}
$table =  JTable::getInstance( 'Group' , 'CTable' );
$table->load($event->contentid);
$link= 'index.php?option=com_community&view=groups&task=viewdiscussions&groupid='.$event->contentid;
 $linkgroup =  CRoute::_($link);
 if(!$table->published) $hide = 'hidden';

?>
<?php
    $mainframe = JFactory::getApplication();
     // update date circle donot image avatar
    $my = CFactory::getUser();
    $groupsModel = CFactory::getModel('groups');
    $course_group = $groupsModel->getIsCourseGroup($event->contentid);
     if($table->avatar)
            $avatarcourse= $mainframe->getCfg('wwwrootfile') . '/'.$table->avatar;
    else {
        $datacourse = JoomdleHelperContent::call_method('get_course_info', (int)$course_group, $my->username);
        $avatarcourse = $datacourse['filepath']. $datacourse['filename'];
    }
?>
<style type="text/css">
    .view-events #txtKeyword.onkeyup {
        background-image: url('') !important;
    }
</style>

 
<div class="joms-body" style="background: none;">

  
    <!-- focus area -->
    <div class="joms-focus">
        <div class="joms-focus__cover">
            <?php  if (in_array($event->id, $featuredList)) { ?>
            <div class="joms-ribbon__wrapper">
                <span class="joms-ribbon joms-ribbon--full"><?php echo JText::_('COM_COMMUNITY_FEATURED'); ?></span>
            </div>
            <?php } ?>

            <div class="joms-focus__cover-image joms-js--cover-image">
            </div>

             <div class="joms-focus__cover-image--mobile joms-js--cover-image-mobile"
                <?php if (!$event->defaultCover && $event->coverAlbum) { ?>
                    style="background:url(<?php if($event->getCover() != 'images/false') echo $event->getCover(); 
                else echo $table->getCover(); ?>) no-repeat center;cursor:pointer"
                    onclick="joms.api.coverClick(<?php echo $event->coverAlbum ?>, <?php echo $event->coverPhoto ?>);"
                <?php } else { ?>
                    style="background:url(<?php if($event->getCover() != 'images/false') echo $event->getCover(); 
                else echo ($table->getCover()) ? $table->getCover() : $avatarcourse; ?>) no-repeat center center"
                <?php } ?>>
            </div>

            <div class="joms-focus__header">
                <div class="joms--topright">
                <?php  if( $isMine || $isCommunityAdmin){

                }
                 else if( $handler->isAllowed() && !$isPastEvent && CEventHelper::showAttendButton($event)) { ?>
                    <?php $a =1; ?>
                <?php if ($event->getMemberStatus($my->id) != 1){ $a =1;?>
                    
                    <a href="javascript:" class="joms-button--primary button--small"
                        onclick="UpdateStatus('<?php echo $event->id; ?>',
                             <?php echo $a;?>);">
<!--                        <span class="glyphicon glyphicon-ok"></span>-->
                        <?php echo JText::_('COM_COMMUNITY_EVENTS_RSVP_ATTEND'); ?>
                        
                    </a>
                <?php } else { $a =2; ?>
                     <a href="javascript:" class=" block "
                       >
                        <img src="/images/icons/FriendsIcon.png">
                    </a>
                <?php }?>

                <?php } elseif($waitingApproval) {?>
                    <a href="javascript:" class="joms-button--primary button--small">
                        <?php echo JText::_('COM_COMMUNITY_EVENTS_PENDING_INVITATIONS'); ?>
                    </a>
                <?php } ?>  
                     
                </div>

                <div class="joms-avatar--focus">
                <a <?php if ( !$event->defaultAvatar && $event->avatarAlbum ) { ?>
                    href="<?php echo CRoute::_('index.php?option=com_community&view=photos&task=photo&albumid=' . $event->avatarAlbum); ?>" style="cursor:default"
                    onclick="joms.api.photoOpen(<?php echo $event->avatarAlbum ?>); return false;"
                <?php } ?>>
                    <img src="<?php if ($event->getAvatar() !='images/false') echo $event->getAvatar('avatar') . '?_=' . time(); else  echo JUri::base().'components/com_community/assets/event.png'; ?>" alt="<?php echo $this->escape($event->name); ?>">
                </a>
            </div>
<!--            <div class="joms-focus__date">
                <span><?php // echo CEventHelper::formatStartDate($event, JText::_('M') ); ?></span>
                <span><?php // echo CEventHelper::formatStartDate($event, JText::_('d') ); ?></span>
            </div>-->
            <div class="joms-focus__title joms-form__group">
                <h3 class="event-title">
                    <a class="back" href="javascript:void(0);" onclick="goBack();"><img src="<?php echo JUri::base();?>images/Back-button.png"></a>
                    
                    <a  class = "mnNameEvent">

                    <?php echo CActivities::truncateComplex($event->title , $titleLength, true); ?>
                    </a>
                    <?php if( $isMine || $isCommunityAdmin || $isAdmin) {?>
                        <a class="btn-edit-event <?php echo $hide; ?>" ><img src="/images/editicon30x30.png" alt="Edit Circle"/></a>
                    <?php } ?>
                    <div class="mntitle <?php if( $isMine || $isCommunityAdmin || $isAdmin) { echo 'hasediticon';}?>">Event</div>
                </h3>
                <div class="mnActive hidden">
                    <ul class="popupDisplay">
                        <li><a href="<?php echo $handler->getFormattedLink('../events/edit?eventid=' . $event->id );?>"><?php  echo JText::_("COM_COMMUNITY_EDIT")?></a></li>
                        <li><a class="mnRemove"><?php echo JText::_("COM_COMMUNITY_REMOVE")?></a></li>
                    </ul>
                </div>
<!--                <div class="joms-focus__header__actions">

                    <a class="joms-button--viewed nolink" title="<?php // echo JText::sprintf( $event->hits > 0 ? 'COM_COMMUNITY_VIDEOS_HITS_COUNT_MANY' : 'COM_COMMUNITY_VIDEOS_HITS_COUNT', $event->hits ); ?>">
                        <svg viewBox="0 0 16 16" class="joms-icon">
                            <use xlink:href="<?php // echo CRoute::getURI(); ?>#joms-icon-eye"></use>
                        </svg>
                        <span><?php // echo $event->hits; ?></span>
                    </a>

                    <?php // if ($config->get('enablesharethis') == 1) { ?>
                        <a class="joms-button--shared" title="<?php // echo JText::_('COM_COMMUNITY_SHARE_THIS'); ?>"
                           href="javascript:" onclick="joms.api.pageShare('<?php // echo CRoute::getExternalURL( 'index.php?option=com_community&view=events&task=viewevent&eventid=' . $event->id ); ?>')">
                            <svg viewBox="0 0 16 16" class="joms-icon">
                                <use xlink:href="<?php // echo CRoute::getURI(); ?>#joms-icon-redo"></use>
                            </svg>
                        </a>
                    <?php // } ?>

                    <?php // if ($enableReporting) { ?>
                        <a class="joms-button--viewed" title="<?php // echo JText::_('COM_COMMUNITY_EVENTS_REPORT'); ?>"
                           href="javascript:" onclick="joms.api.eventReport('<?php // echo $event->id; ?>');">
                            <svg viewBox="0 0 16 16" class="joms-icon">
                                <use xlink:href="<?php // echo CRoute::getURI(); ?>#joms-icon-warning"></use>
                            </svg>
                        </a>
                    <?php // } ?>

                </div>-->
                <p class="joms-focus__info--desktop">
                    <?php echo CActivities::truncateComplex($event->summary, $summaryLength, true); ?>
                </p>
            </div>
               
             
            </div>
            <div class="joms-focus__actions--reposition">
                <input type="button" class="joms-button--neutral" data-ui-object="button-cancel" value="<?php echo JText::_('COM_COMMUNITY_CANCEL'); ?>"> &nbsp;
                <input type="button" class="joms-button--primary" data-ui-object="button-save" value="<?php echo JText::_('COM_COMMUNITY_SAVE'); ?>">
            </div>
            <?php // if ($my->id != 0) { ?>
<!--            <div class="joms-focus__button--options--desktop">
                <a href="javascript:" data-ui-object="joms-dropdown-button">
                    <svg viewBox="0 0 16 16" class="joms-icon">
                        <use xlink:href="<?php // echo CRoute::getURI(); ?>#joms-icon-cog"></use>
                    </svg>
                </a>
                 No need to populate menus as it is cloned from mobile version. 
                <ul class="joms-dropdown"></ul>
            </div>-->
            <?php // } ?>
        </div>
       
        <!-- <div class="row margin-0">
            <div class="joms-tab__bar-event custom-tab-bar event-page">
                <a href="<?php echo CRoute::_('../index.php?option=com_community&view=events&task=viewevent&eventid=' . $event->id) ?>" class="<?php echo $active_about;?>"><?php echo JText::_('COM_COMMUNITY_EVENTS_ABOUT');?></a>
                <a href="<?php echo CRoute::_('../index.php?option=com_community&view=events&task=viewguest&eventid=' .$event->id. '&type=1&groupid='. $event->contentid) ?>" class="<?php echo $active_attending;?>"><?php echo JText::_('COM_COMMUNITY_EVENTS_ATTENDING');?></a>
            </div>  
        </div> -->
        
    </div>

    <div style="float: right; padding-right: 10px; padding-bottom: 7px; display: none;">    <!-- Luyen Vu an 25 May 2016 -->
    <?php if ($my->id != 0) { ?>               
        
        <?php if ( !$isPastEvent && CEventHelper::seatsAvailable($event) && ( $event->allowinvite || $event->isAdmin($my->id) || COwnerHelper::isCommunityAdmin() ) ) { ?>
            <a href="javascript:" class="joms-button--primary button--small" onclick="joms.api.eventInvite('<?php echo $event->id; ?>', '<?php echo $isGroupEvent ? "group" : "" ?>')">
                 <span class="glyphicon glyphicon-user"></span>
                <?php echo JText::_($isGroupEvent ? 'COM_COMMUNITY_EVENT_INVITE_GROUP_MEMBERS' : 'COM_COMMUNITY_TAB_INVITE'); ?>
            </a>
            
            <a class="joms-button--primary button--small" data-ui-object="joms-dropdown-button">
                <span class="glyphicon glyphicon-cog"></span>
                <?php echo JText::_('COM_COMMUNITY_GROUP_OPTIONS'); ?>
            </a>
            
        <?php } ?>
                <ul class="joms-dropdown">

                <?php if ($memberStatus != COMMUNITY_EVENT_STATUS_BLOCKED) { ?>
                   
                    <?php if( $handler->showExport() && $config->get('eventexportical') ) { ?>
                    <li><a tabindex="-1" href="<?php echo $handler->getFormattedLink('index.php?option=com_community&view=events&task=export&format=raw&eventid=' . $event->id); ?>" ><?php echo JText::_('COM_COMMUNITY_EVENTS_EXPORT_ICAL');?></a></li>
                    <?php } ?>

                    <?php if( (!$isMine) && !($waitingRespond) && (COwnerHelper::isRegisteredUser()) ) { ?>
                    <li><a tabindex="-1" href="javascript:" onclick="joms.api.eventLeave('<?php echo $event->id; ?>');"><?php echo JText::_('COM_COMMUNITY_EVENTS_IGNORE');?></a></li>
                    <?php } ?>

                    <li class="divider"></li>
                <?php } ?>

                <?php if( $isMine || COwnerHelper::isCommunityAdmin() || $isAdmin ){?>
                    <?php if(false) { //hide for now ?>
                    <li><a href="javascript:" onclick="joms.api.avatarChange('event', '<?php echo $event->id; ?>');"><?php echo JText::_('COM_COMMUNITY_CHANGE_AVATAR')?></a></li>
                    <?php } ?>
                    <li class="joms-js--menu-reposition"<?php echo $event->defaultCover ? ' style="display:none"' : '' ?>><a href="javascript:" data-propagate="1" onclick="joms.api.coverReposition('event', '<?php echo $event->id; ?>');"><?php echo JText::_('COM_COMMUNITY_REPOSITION_COVER')?></a></li>
                    <li><a href="javascript:" onclick="joms.api.coverChange('event', '<?php echo $event->id; ?>');" ><?php echo JText::_('COM_COMMUNITY_CHANGE_COVER'); ?></a></li>
                    <li class="joms-js--menu-remove-cover"<?php echo $event->defaultCover ? ' style="display:none"' : '' ?>><a href="javascript:" data-propagate="1" onclick="joms.api.coverRemove('event', <?php echo $event->id; ?>);"><?php echo JText::_('COM_COMMUNITY_REMOVE_COVER'); ?></a></li>
                <?php } ?>

             
                <!-- event administration -->
                <?php if($isMine || $isCommunityAdmin || $isAdmin || $handler->manageable()) { ?>
                    <?php if( $isMine || $isCommunityAdmin || $isAdmin) {?>
                        <li><a tabindex="-1" href="<?php echo $handler->getFormattedLink('index.php?option=com_community&view=events&task=edit&eventid=' . $event->id );?>"><?php echo JText::_('COM_COMMUNITY_EVENTS_EDIT');?></a></li>
                    <?php } ?>

                    <?php if($totalMembers = count($event->getMembers(COMMUNITY_EVENT_STATUS_BANNED))){ ?>
                        <li><a tabindex="-1" href="<?php echo $handler->getFormattedLink('index.php?option=com_community&view=events&task=viewguest&type=8&eventid=' . $event->id );?>"><?php echo JText::_('COM_COMMUNITY_EVENTS_VIEW_BANNED_MEMBERS');?></a></li>
                    <?php } ?>

                    <li class="divider"></li>

                    <?php if( $handler->isAdmin() ) { ?>
                        <li><a tabindex="-1" class="event-delete" href="javascript:" onclick="joms.api.eventDelete('<?php echo $event->id; ?>');"><?php echo JText::_('COM_COMMUNITY_EVENTS_DELETE'); ?></a></li>
                    <?php } ?>

                <?php } ?>

                <?php if( ( $isMine || $isAdmin || $isCommunityAdmin) && ( $unapproved > 0 ) ) { ?>
                    <li class="divider"></li>
                    <li>
                        <a tabindex="-1" href="<?php echo $handler->getFormattedLink('index.php?option=com_community&view=events&task=viewguest&type='.COMMUNITY_EVENT_STATUS_REQUESTINVITE.'&eventid=' . $event->id);?>">
                            <?php echo JText::sprintf((CStringHelper::isPlural($unapproved)) ? 'COM_COMMUNITY_EVENTS_PENDING_INVITE_MANY'    :'COM_COMMUNITY_EVENTS_PENDING_INVITE' , $unapproved ); ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        
        
        <?php if ($showRequestInvitationButton) { ?>
            <a href="javascript:" class="joms-focus__button--add" onclick="joms.api.eventJoin('<?php echo $event->id; ?>');"><?php echo JText::_('COM_COMMUNITY_EVENTS_INVITE_REQUEST'); ?></a>
        <?php } ?>
    <?php } ?>
    </div>
    <!--<div class="rmPopupBlock hidden ">
         <p class="titleBlock"><?php echo JText::_('COM_COMMUNITY_LEAVE_EVENTS'); ?></p>
         <button class="btRMCancel" type="button">CANCEL</button>
         <button  onclick="UpdateStatus('<?php echo $event->id; ?>',
         <?php echo $a;?>);" class="btRMYes" type="button">LEAVE</button>
    </div>
    <div class="rmPopupUnfriend removeEvent hidden ">
         <p class="titleBlock"><?php echo JText::_('COM_COMMUNITY_EVENTS_REMOVE'); ?></p>
         <button class="btRMCancel" type="button"><?php echo JText::_('COM_COMMUNITY_CANCEL_BUTTON'); ?></button>
         <button  onclick="deleteEvent('<?php echo $event->id; ?>',4,'');"
          class="btRMYes" type="button"><?php echo JText::_('COM_COMMUNITY_REMOVE_BUTTON'); ?></button>
    </div>-->
<div class="mnNotification"></div>
<script type="text/javascript">
    var eventId,status,step,action;
    jQuery('.block').click(function(){
    //        jQuery('body').addClass('overlay2');
    //        jQuery('.rmPopupBlock').removeClass('hidden');

        lgtCreatePopup('confirm', {
            content: '<?php echo JText::_('COM_COMMUNITY_LEAVE_EVENTS'); ?>',
            yesText: 'LEAVE'
        }, function() {
            UpdateStatus('<?php echo $event->id; ?>', <?php echo $a;?>);
        });
    });
    function UpdateStatus(eventId,status) {
         jQuery.ajax({
            type: 'post',
            url: 'index.php?option=com_community&view=events&task=ajaxUpdateStatus',
            data: {eventId: eventId,status:status },
            success: function(data) {
                 window.location.reload();
            }
        });
          jQuery('.rmPopupBlock').addClass('hidden');
        jQuery('body').addClass('overlay2');
        jQuery('.mnNotification').html('Loading...').fadeIn();
    }
    
        jQuery('.rmPopupBlock .btRMCancel').click(function(){
             jQuery('.rmPopupBlock').addClass('hidden');
             jQuery('body').removeClass('overlay2');
        });

        if (jQuery('#system-message').text().length > 0) {
            jQuery('.jomsocial').css({'padding-top': '0px'});
            /*jQuery('.alert-message').css({'margin-top' : '20px'});*/
            jQuery('#system-message').css({'padding-top': '0px'});
            
        } 
        jQuery('.btn-edit-event').click(function(e){
            e.stopPropagation();
            jQuery('.mnActive').toggleClass('hidden');
        });
        jQuery(document).click(function (e) {
            if (!jQuery(e.target).hasClass('mnActive')) {
                jQuery('.mnActive').addClass('hidden');
            }
        });

        jQuery('.mnRemove').click(function(){
             jQuery('.mnActive').addClass('hidden');
//             jQuery('body').addClass('overlay2');
//                jQuery('.removeEvent').removeClass('hidden');
            lgtCreatePopup('confirm', {'content': '<?php echo JText::_('COM_COMMUNITY_EVENTS_REMOVE'); ?>', 'yesText': 'Remove'}, function() {
                deleteEvent('<?php echo $event->id; ?>',4,'');
            });
        });
        jQuery('.rmPopupUnfriend .btRMCancel').click(function(){
                 jQuery('.rmPopupUnfriend').addClass('hidden');
                 jQuery('body').removeClass('overlay2');
        });
        function deleteEvent(eventId,step,action) {
            jQuery.ajax({
                type: 'post',
                url: 'index.php?option=com_community&view=events&task=ajaxDeleteEvent',
                data: {eventId: eventId,step:step,action:action },
                success: function(data) {
                     window.location='<?php echo  $linkgroup?>';

                }
            });
        }
    </script>