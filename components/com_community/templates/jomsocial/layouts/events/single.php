<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();
 $document = JFactory::getDocument();
 $session = JFactory::getSession();
 $device = $session->get('device');
if($device == 'mobile'){
     $document->addStyleSheet(JURI::root(true) . 'templates/t3_bs3_blank/local/css/events.css');
}
else $document->addStyleSheet(JURI::root(true) . 'templates/t3_bs3_tablet_desktop_template/local/css/events.css'); 

$showStream = ($isEventGuest || $isMine || $isAdmin || $isCommunityAdmin || $handler->manageable());
    $config = CFactory::getConfig();

$titleLength= $config->get('header_title_length', 60);
$summaryLength = $config->get('header_summary_length', 100);

$enableReporting = false;
if ( $config->get('enablereporting') == 1 && ( $my->id > 0 || $config->get('enableguestreporting') == 1 ) ) {
    $enableReporting = true;
}

$isGroupEvent = $event->type == CEventHelper::GROUP_TYPE;

$showRequestInvitationButton = false;
if (
    ($event->permission == COMMUNITY_PRIVATE_EVENT) &&
    (!$isEventGuest) &&
    (!$waitingApproval) &&
    (!$handler->isAllowed()) &&
    ($memberStatus != COMMUNITY_EVENT_STATUS_ATTEND) &&
    ($memberStatus != COMMUNITY_EVENT_STATUS_WONTATTEND) &&
    ($memberStatus != COMMUNITY_EVENT_STATUS_MAYBE) &&
    ($memberStatus != COMMUNITY_EVENT_STATUS_BLOCKED)
) {
    $showRequestInvitationButton = true;
}

?>
<?php
    echo CMiniHeader::showEventMiniHeader($event->id);
?>
<div class ="mnContentEvent">
<?php 
    // print_r($eventMembers);
    // $countMember = $eventMembersCount;
    $startweekday = date('l', strtotime($event->startdate));
    $startdate = date('d F Y', strtotime($event->startdate));

    $endweekday = date('l', strtotime($event->enddate));
    $enddate = date('d F Y', strtotime($event->enddate));
?>
        <div class="col-event-title joms-list__item bordered-box">
        <h1 class="mnEventTitle"><?php echo JText::_('COM_COMMUNITY_EVENTS_DETAIL_TIME'); ?></h1>
        <div class="mnEventDes">
            <?php if($startdate == $enddate){ ?>
                <span class="evenCaledarBlock"> <img src="/images/Calendar_hd.png" class="evenCaledar"/> <?php echo $startweekday . '<a class = "mnTime">.</a>' . $startdate; ?></span>
            <?php } else { ?>
                <span class="evenCaledarBlock"> <img src="/images/Calendar_hd.png" class="evenCaledar"/> <?php echo $startweekday . '<a class = "mnTime">.</a>' . $startdate; ?> - <?php echo $endweekday . '<a class = "mnTime">.</a>' . $enddate; ?></span>
            <?php } ?>
            <?php if ($device == 'mobile') { echo "<p></p>"; }?>

            <span class="evenLocationBlock"><img src="/images/Location_hd.png"
                                                 class="evenLocation"/> <?php echo $event->location; ?></span>
            <?php if ($device == 'mobile') { echo "<p></p>"; }?>
            <span class="evenTimeBlock"><img src="/images/Time_hd.png"
                                             class="evenTime"/><?php echo date('h:i A', strtotime($event->startdate)); ?> - <?php echo date('h:i A', strtotime($event->enddate)); ?></span>
        </div>
    </div>

    <div class="joms-list__item bordered-box">
           <h1 class="mnAboutEvent"><?php echo JText::_('COM_COMMUNITY_EVENTS_ABOUT_EVENT'); ?></h1>
                           <?php 
                           $description = JHtml::_('string.truncate', strip_tags( $event->description), 270, true, $allowHtml = true);
                           $show_more = '';
                           if(strlen(strip_tags( $event->description)) > 270) {
                               $show_more = '<a class="show-title">Show more</a>';
                           }
                           $show_less = '<a class="less-title">Show less</a>';
                           ?>

                          <p class="mn-title">
                          <?php echo $description.$show_more; ?></p>
                           <p class="mn-titlefull hidden">
                          <?php echo $event->description.$show_less; ?></p>

        </div>
        <div class="joms-list__item bordered-box ">

            <?php if( $eventMembers) { ?>
            <div id="notice" class="alert alert-notice" style="display:none;">
                <a class="close" data-dismiss="alert">×</a>
                <div id="notice-message"></div>
        </div>

            <div class="col-event joms-list--friend col-xs-12 custom-header" style="background: none">
                <h1 class="mnMemnerTitle"><?php echo JText::_('COM_COMMUNITY_EVENTS_PEO_ATTENDING');?></h1>
           <div class="swiper-container swiper-container-horizontal">
                <div class="container-fluid box_members">
                <?php $i = 0;?>
                 <?php foreach( $eventMembers as $guest ){ ?>
                    <div class="userAvatar1 col-md-4 col-sm-6 col-xs-12 ">
                        <a class="cIndex-Avatar" href="<?php echo CRoute::_('index.php?option=com_community&view=profile&userid=' . $guest->id); ?>">
                            <img src="<?php echo $guest->getThumbAvatar(); ?>" class="cAvatar col-md-4 col-sm-4 col-xs-4"/>
                            <span class="col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-1 col-xs-7"><?php echo $guest->getDisplayName(); ?></span>
                                </a>
                                </div>
                <?php  $i++; } ?>

                                </div>
                        
            <?php } else { ?>
            <div class="joms-alert"><?php echo JText::_('COM_COMMUNITY_EVENTS_NO_USERS'); ?></div>
                <?php } ?>

        </div>
    </div>
     <div class="memberSeeMore" style="display: none;"><?php echo strtoupper(JText::_('COM_COMMUNITY_SEE_MORE'));?></div>
</div>
<script type="text/javascript">


    (function($){
        if (jQuery('.col-event .userAvatar1').length < <?php echo $eventMembersCount ?> ) {
                jQuery('.memberSeeMore').show();
        }
        jQuery(".show-title").click(function() {
            jQuery('.mn-title').addClass('hidden');
            jQuery('.mn-titlefull').removeClass('hidden');

        });
         jQuery(".less-title").click(function() {
            jQuery('.mn-title').removeClass('hidden');
            jQuery('.mn-titlefull').addClass('hidden');

        });
        jQuery(".memberSeeMore").click(function() {
                  jQuery('.memberSeeMore').hide();
                        jQuery.ajax({
                            url: '<?php echo CRoute::_('index.php?option=com_community&view=events&task=ajaxGetMoreMember&eventid=' . $event->id)?>',
                            type: 'POST',
                            data: {
                                'limitstart': jQuery('.col-event .userAvatar1').length,
                                'limit': 6,
                                'ajaxSend': 1
                            },
                            beforeSend: function() {
                                jQuery('.memberSeeMore').after('<div class="circlesLoading" style="display: none;">Loading...</div>');
                                jQuery('.circlesLoading').fadeIn();
                            },
                            success: function(result) {
                                var res = JSON.parse(result);
                                console.log(res);
                                jQuery('.circlesLoading').fadeOut('400', function() {jQuery('.circlesLoading').remove();});
                                jQuery.each(res, function( index, value ) {
                                    var avatar = value._avatar ? '<?php echo JFactory::getApplication()->getCfg('wwwrootfile'); ?>/'+value._avatar : '/components/com_community/assets/ProfileIcon.png';
                                    var html = '<div class=" userAvatar1 col-md-4 col-sm-6 col-xs-12"> '+'<a  class="cIndex-Avatar" href="<?php echo CRoute::_("index.php?option=com_community&view=profile&")?>/'+value.id+'-'+value.name+'">'+
                                        '<img src="'+avatar+'" class="cAvatar col-md-4 col-sm-4 col-xs-4"/>'+
                                        '<span class="col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-1 col-xs-6 col-xs-offset-1">'+value.name+'</span>'
                                        +'</a>'
                                  +'</div>';

                                    jQuery('.col-event .box_members').append(html);
                                });
                                if (jQuery('.col-event .userAvatar1').length < <?php echo $eventMembersCount ?> ) {
                                    jQuery('.circlesSeeMore').fadeIn();
                                }
                                
                            },
                            error: function() {
                                jQuery('.circlesSeeMore').fadeIn();
                                jQuery('.circlesLoading').fadeOut('400', function() {jQuery('.circlesLoading').remove();});
                            }
                        });
                    });
        
    })(jQuery); 
</script>

<script>

    // override config setting
    joms || (joms = {});
    joms.constants || (joms.constants = {});
    joms.constants.conf || (joms.constants.conf = {});

    joms.constants.eventid = <?php echo $event->id; ?>;
    joms.constants.videocreatortype = '<?php echo VIDEO_EVENT_TYPE ?>';

    joms.constants.conf.enablephotos = <?php echo (isset($showPhotos) && $showPhotos == 1 && (( $isAdmin && $photoPermission == 1 ) || ($isEventGuest && $photoPermission == 2) ) ) ? 1 : 0 ; ?>;
    joms.constants.conf.enablevideos = <?php echo (isset($showVideos) && $showVideos == 1 && (( $isAdmin && $videoPermission == 1 ) || ($isEventGuest && $videoPermission == 2) ) ) ? 1 : 0 ; ?>;

    joms.constants.conf.enablevideosupload  = <?php echo $config->get('enablevideosupload');?>;

</script>
