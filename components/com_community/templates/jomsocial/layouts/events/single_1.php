<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();

$showStream = ($isEventGuest || $isMine || $isAdmin || $isCommunityAdmin || $handler->manageable());
    $config = CFactory::getConfig();

$titleLength= $config->get('header_title_length', 60);
$summaryLength = $config->get('header_summary_length', 100);

$enableReporting = false;
if ( $config->get('enablereporting') == 1 && ( $my->id > 0 || $config->get('enableguestreporting') == 1 ) ) {
    $enableReporting = true;
}

$isGroupEvent = $event->type == CEventHelper::GROUP_TYPE;

$showRequestInvitationButton = false;
if (
    ($event->permission == COMMUNITY_PRIVATE_EVENT) &&
    (!$isEventGuest) &&
    (!$waitingApproval) &&
    (!$handler->isAllowed()) &&
    ($memberStatus != COMMUNITY_EVENT_STATUS_ATTEND) &&
    ($memberStatus != COMMUNITY_EVENT_STATUS_WONTATTEND) &&
    ($memberStatus != COMMUNITY_EVENT_STATUS_MAYBE) &&
    ($memberStatus != COMMUNITY_EVENT_STATUS_BLOCKED)
) {
    $showRequestInvitationButton = true;
}

$session =  JFactory::getSession();
$is_mobile = $session->get('device') == 'mobile' ? true : false;
?>
<div class="joms-body" style="background: none;">

    <div class="joms-tab__bar">
        <a href="<?php //echo CRoute::_("index.php?option=com_community&view=events&task=myevents")
        echo JUri::base().'myevents/list.html'; ?>" class="active" ?>My Events</a>
        <a href="<?php echo CRoute::_("index.php?option=com_community&view=events&task=display") ?>">All Events</a>
    </div>
  
    <!-- focus area -->
    <div class="joms-focus">
        <div class="joms-focus__cover">
            <?php  if (in_array($event->id, $featuredList)) { ?>
            <div class="joms-ribbon__wrapper">
                <span class="joms-ribbon joms-ribbon--full"><?php echo JText::_('COM_COMMUNITY_FEATURED'); ?></span>
            </div>
            <?php } ?>

            <div class="joms-focus__cover-image joms-js--cover-image">
                <img src="<?php echo $event->getCover(); ?>" style="width:100%;top:<?php echo $event->coverPostion; ?>" alt="<?php echo $event->title; ?>"
                <?php if (!$event->defaultCover && $event->coverAlbum) { ?>
                    style="width:100%;top:<?php echo $event->coverPostion; ?>;cursor:pointer"
                    onclick="joms.api.coverClick(<?php echo $event->coverAlbum ?>, <?php echo $event->coverPhoto ?>);"
                <?php } else { ?>
                    style="width:100%;top:<?php echo $event->coverPostion; ?>"
                <?php } ?>>
            </div>

            <div class="joms-focus__cover-image--mobile joms-js--cover-image-mobile"
                <?php if (!$event->defaultCover && $event->coverAlbum) { ?>
                    style="background:url(<?php echo $event->getCover(); ?>) no-repeat center center;cursor:pointer"
                    onclick="joms.api.coverClick(<?php echo $event->coverAlbum ?>, <?php echo $event->coverPhoto ?>);"
                <?php } else { ?>
                    style="background:url(<?php echo $event->getCover(); ?>) no-repeat center center"
                <?php } ?>>
            </div>

            <div class="joms-focus__header">
            	<div class="joms--topright">
	            	<?php if( $handler->isAllowed() && !$isPastEvent && CEventHelper::showAttendButton($event)) { ?>
				    <a href="javascript:" class="joms-button--primary button--small"
				        onclick="joms.api.eventResponse('<?php echo $event->id; ?>',
				            ['<?php echo COMMUNITY_EVENT_STATUS_ATTEND; ?>', '<?php echo JText::_('COM_COMMUNITY_EVENTS_RSVP_ATTEND', true); ?>'],
				            ['<?php echo COMMUNITY_EVENT_STATUS_MAYBE; ?>', '<?php echo JText::_('COM_COMMUNITY_EVENTS_RSVP_MAYBE_ATTEND', true); ?>'],
				            ['<?php echo COMMUNITY_EVENT_STATUS_WONTATTEND; ?>', '<?php echo JText::_('COM_COMMUNITY_EVENTS_RSVP_NOT_ATTEND', true); ?>']);">
				        <?php if ($event->getMemberStatus($my->id) == COMMUNITY_EVENT_STATUS_ATTEND) { ?>
				        <span class="glyphicon glyphicon-ok"></span>
				        <?php echo JText::_('COM_COMMUNITY_EVENTS_RSVP_ATTEND'); ?>
				        <?php } else if ($event->getMemberStatus($my->id) >= COMMUNITY_EVENT_STATUS_MAYBE) { ?>
				        <span class="glyphicon glyphicon-question-sign"></span>
				        <?php echo JText::_('COM_COMMUNITY_EVENTS_RSVP_MAYBE_ATTEND'); ?>
				        <?php } else if ($event->getMemberStatus($my->id) >= COMMUNITY_EVENT_STATUS_WONTATTEND) { ?>
				        <span class="glyphicon glyphicon-remove"></span>
				        <?php echo JText::_('COM_COMMUNITY_EVENTS_RSVP_NOT_ATTEND'); ?>
				        <?php } else { ?>
				        <?php echo JText::_('COM_COMMUNITY_GROUPS_INVITATION_RESPONSE'); ?>
				        <svg class="joms-icon" viewBox="0 0 16 16">
				            <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-arrow-down"/>
				        </svg>
				        <?php } ?>
				    </a>
				<?php }elseif($waitingApproval){?>
				    <a href="javascript:" class="joms-button--primary button--small">
				        <?php echo JText::_('COM_COMMUNITY_EVENTS_PENDING_INVITATIONS'); ?>
				    </a>
				<?php } ?>	
	            	 
            	</div>
            	
                <div class="joms-focus__date">
                    <span><?php echo CEventHelper::formatStartDate($event, JText::_('M') ); ?></span>
                    <span><?php echo CEventHelper::formatStartDate($event, JText::_('d') ); ?></span>
                </div>
                
                <div class="joms-focus__title">
                    <h2><?php echo CActivities::truncateComplex($event->title , $titleLength, true); ?></h2>
                    <div class="joms-focus__header__actions">

                        <a class="joms-button--viewed nolink" title="<?php echo JText::sprintf( $event->hits > 0 ? 'COM_COMMUNITY_VIDEOS_HITS_COUNT_MANY' : 'COM_COMMUNITY_VIDEOS_HITS_COUNT', $event->hits ); ?>">
                            <svg viewBox="0 0 16 16" class="joms-icon">
                                <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-eye"></use>
                            </svg>
                            <span><?php echo $event->hits; ?></span>
                        </a>

                        <?php if ($config->get('enablesharethis') == 1) { ?>
                        <a class="joms-button--shared" title="<?php echo JText::_('COM_COMMUNITY_SHARE_THIS'); ?>"
                                href="javascript:" onclick="joms.api.pageShare('<?php echo CRoute::getExternalURL( 'index.php?option=com_community&view=events&task=viewevent&eventid=' . $event->id ); ?>')">
                            <svg viewBox="0 0 16 16" class="joms-icon">
                                <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-redo"></use>
                            </svg>
                        </a>
                        <?php } ?>

                        <?php if ($enableReporting) { ?>
                        <a class="joms-button--viewed" title="<?php echo JText::_('COM_COMMUNITY_EVENTS_REPORT'); ?>"
                                href="javascript:" onclick="joms.api.eventReport('<?php echo $event->id; ?>');">
                            <svg viewBox="0 0 16 16" class="joms-icon">
                                <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-warning"></use>
                            </svg>
                        </a>
                        <?php } ?>

                    </div>
                    <p class="joms-focus__info--desktop">
                        <?php echo CActivities::truncateComplex($event->summary, $summaryLength, true); ?>
                    </p>
                </div>
                
               
            </div>
            <div class="joms-focus__actions--reposition">
                <input type="button" class="joms-button--neutral" data-ui-object="button-cancel" value="<?php echo JText::_('COM_COMMUNITY_CANCEL'); ?>"> &nbsp;
                <input type="button" class="joms-button--primary" data-ui-object="button-save" value="<?php echo JText::_('COM_COMMUNITY_SAVE'); ?>">
            </div>
            <?php if ($my->id != 0) { ?>
            <div class="joms-focus__button--options--desktop">
                <a href="javascript:" data-ui-object="joms-dropdown-button">
                    <svg viewBox="0 0 16 16" class="joms-icon">
                        <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-cog"></use>
                    </svg>
                </a>
                <!-- No need to populate menus as it is cloned from mobile version. -->
                <ul class="joms-dropdown"></ul>
            </div>
            <?php } ?>
        </div>
       
        <div class="row margin-0">
		    <div class="joms-tab__bar custom-tab-bar">
		        <a href="<?php echo CRoute::_('index.php?option=com_community&view=events&task=viewevent&eventid=' . $event->id) ?>" class="active">About</a>
		        <a href="<?php echo CRoute::_('index.php?option=com_community&view=events&task=viewguest&eventid=' .$event->id. '&type=1&groupid='. $event->contentid) ?>">Attending</a>
		    </div>  
		</div>
		
    </div>

	<div style="float: right; padding-right: 10px; padding-bottom: 7px">    
	<?php if ($my->id != 0) { ?>               
		
		<?php if ( !$isPastEvent && CEventHelper::seatsAvailable($event) && ( $event->allowinvite || $event->isAdmin($my->id) || COwnerHelper::isCommunityAdmin() ) ) { ?>
		    <a href="javascript:" class="joms-button--primary button--small" onclick="joms.api.eventInvite('<?php echo $event->id; ?>', '<?php echo $isGroupEvent ? "group" : "" ?>')">
			     <span class="glyphicon glyphicon-user"></span>
		        <?php echo JText::_($isGroupEvent ? 'COM_COMMUNITY_EVENT_INVITE_GROUP_MEMBERS' : 'COM_COMMUNITY_TAB_INVITE'); ?>
		    </a>
		    
			<a class="joms-button--primary button--small" data-ui-object="joms-dropdown-button">
				<span class="glyphicon glyphicon-cog"></span>
				<?php echo JText::_('COM_COMMUNITY_GROUP_OPTIONS'); ?>
			</a>
			
		<?php } ?>
		        <ul class="joms-dropdown">

                <?php if ($memberStatus != COMMUNITY_EVENT_STATUS_BLOCKED) { ?>
                   
                    <?php if( $handler->showExport() && $config->get('eventexportical') ) { ?>
                    <li><a tabindex="-1" href="<?php echo $handler->getFormattedLink('index.php?option=com_community&view=events&task=export&format=raw&eventid=' . $event->id); ?>" ><?php echo JText::_('COM_COMMUNITY_EVENTS_EXPORT_ICAL');?></a></li>
                    <?php } ?>

                    <?php if( (!$isMine) && !($waitingRespond) && (COwnerHelper::isRegisteredUser()) ) { ?>
                    <li><a tabindex="-1" href="javascript:" onclick="joms.api.eventLeave('<?php echo $event->id; ?>');"><?php echo JText::_('COM_COMMUNITY_EVENTS_IGNORE');?></a></li>
                    <?php } ?>

                    <li class="divider"></li>
                <?php } ?>

                <?php if( $isMine || COwnerHelper::isCommunityAdmin() || $isAdmin ){?>
                    <?php if(false) { //hide for now ?>
                    <li><a href="javascript:" onclick="joms.api.avatarChange('event', '<?php echo $event->id; ?>');"><?php echo JText::_('COM_COMMUNITY_CHANGE_AVATAR')?></a></li>
                    <?php } ?>
                    <li class="joms-js--menu-reposition joms-hidden--small"<?php echo $event->defaultCover ? ' style="display:none"' : '' ?>><a href="javascript:" data-propagate="1" onclick="joms.api.coverReposition('event', '<?php echo $event->id; ?>');"><?php echo JText::_('COM_COMMUNITY_REPOSITION_COVER')?></a></li>
                    <li><a href="javascript:" onclick="joms.api.coverChange('event', '<?php echo $event->id; ?>');" ><?php echo JText::_('COM_COMMUNITY_CHANGE_COVER'); ?></a></li>
                    <li class="joms-js--menu-remove-cover"<?php echo $event->defaultCover ? ' style="display:none"' : '' ?>><a href="javascript:" data-propagate="1" onclick="joms.api.coverRemove('event', <?php echo $event->id; ?>);"><?php echo JText::_('COM_COMMUNITY_REMOVE_COVER'); ?></a></li>
                <?php } ?>

             
                <!-- event administration -->
                <?php if($isMine || $isCommunityAdmin || $isAdmin || $handler->manageable()) { ?>
                    <?php if( $isMine || $isCommunityAdmin || $isAdmin) {?>
                        <li><a tabindex="-1" href="<?php echo $handler->getFormattedLink('index.php?option=com_community&view=events&task=edit&eventid=' . $event->id );?>"><?php echo JText::_('COM_COMMUNITY_EVENTS_EDIT');?></a></li>
                    <?php } ?>

                    <?php if($totalMembers = count($event->getMembers(COMMUNITY_EVENT_STATUS_BANNED))){ ?>
                        <li><a tabindex="-1" href="<?php echo $handler->getFormattedLink('index.php?option=com_community&view=events&task=viewguest&type=8&eventid=' . $event->id );?>"><?php echo JText::_('COM_COMMUNITY_EVENTS_VIEW_BANNED_MEMBERS');?></a></li>
                    <?php } ?>

                    <li class="divider"></li>

                    <?php if( $handler->isAdmin() ) { ?>
                        <li><a tabindex="-1" class="event-delete" href="javascript:" onclick="joms.api.eventDelete('<?php echo $event->id; ?>');"><?php echo JText::_('COM_COMMUNITY_EVENTS_DELETE'); ?></a></li>
                    <?php } ?>

                <?php } ?>

                <?php if( ( $isMine || $isAdmin || $isCommunityAdmin) && ( $unapproved > 0 ) ) { ?>
                    <li class="divider"></li>
                    <li>
                        <a tabindex="-1" href="<?php echo $handler->getFormattedLink('index.php?option=com_community&view=events&task=viewguest&type='.COMMUNITY_EVENT_STATUS_REQUESTINVITE.'&eventid=' . $event->id);?>">
                            <?php echo JText::sprintf((CStringHelper::isPlural($unapproved)) ? 'COM_COMMUNITY_EVENTS_PENDING_INVITE_MANY'    :'COM_COMMUNITY_EVENTS_PENDING_INVITE' , $unapproved ); ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
		
		
		<?php if ($showRequestInvitationButton) { ?>
		    <a href="javascript:" class="joms-focus__button--add" onclick="joms.api.eventJoin('<?php echo $event->id; ?>');"><?php echo JText::_('COM_COMMUNITY_EVENTS_INVITE_REQUEST'); ?></a>
		<?php } ?>
	<?php } ?>
	</div>

    <div class="joms-list__item bordered-box">
	    <div class="col-xs-9 custom-header pull-left">
	       <h1>About Event</h1>
	       <p class="margin-0 joms--description"><?php echo $event->description; ?></p>
	       
	       <h1>Event Date and Time</h1>
	       <p class="margin-0 joms--description">
			    <?php echo ($allday) ? JText::sprintf('COM_COMMUNITY_EVENTS_ALLDAY_DATE',$event->startdateHTML) : JText::sprintf('COM_COMMUNITY_EVENTS_DURATION',$event->startdateHTML,$event->enddateHTML); ?>
	            <?php if( $config->get('eventshowtimezone') ) {
	                echo $timezone; ?>
	            <?php } ?>
	       </p>
	       
	       <h1>Location</h1>
	       <p class="margin-0 joms--description"><?php echo $event->location; ?></p>
	       
	       <h1>Organisers</h1>
	       <p class="margin-0 joms--description"><?php echo $adminsList;?></p>
	       
	    </div>
	</div>
    
</div>

<script>
    // Clone menu from mobile version to desktop version.
    (function( w ) {
        w.joms_queue || (w.joms_queue = []);
        w.joms_queue.push(function() {
            var src = joms.jQuery('.joms-focus__actions ul.joms-dropdown'),
                clone = joms.jQuery('.joms-focus__button--options--desktop ul.joms-dropdown');

            clone.html( src.html() );
        });
    })( window );
</script>

<script>

    // override config setting
    joms || (joms = {});
    joms.constants || (joms.constants = {});
    joms.constants.conf || (joms.constants.conf = {});

    joms.constants.eventid = <?php echo $event->id; ?>;
    joms.constants.videocreatortype = '<?php echo VIDEO_EVENT_TYPE ?>';

    joms.constants.conf.enablephotos = <?php echo (isset($showPhotos) && $showPhotos == 1 && (( $isAdmin && $photoPermission == 1 ) || ($isEventGuest && $photoPermission == 2) ) ) ? 1 : 0 ; ?>;
    joms.constants.conf.enablevideos = <?php echo (isset($showVideos) && $showVideos == 1 && (( $isAdmin && $videoPermission == 1 ) || ($isEventGuest && $videoPermission == 2) ) ) ? 1 : 0 ; ?>;

    joms.constants.conf.enablevideosupload  = <?php echo $config->get('enablevideosupload');?>;

</script>
