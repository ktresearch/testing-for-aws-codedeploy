<?php
    /**
     * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
     * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
     * @author iJoomla.com <webmaster@ijoomla.com>
     * @url https://www.jomsocial.com/license-agreement
     * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
     * More info at https://www.jomsocial.com/license-agreement
     */
    defined('_JEXEC') or die();
    $session = JFactory::getSession();
    $device = $session->get('device');
?>
<script>
    function joms_change_filter_type(value) {
        var urls = {
            <?php
                $task = JFactory::getApplication()->input->getCmd('task');
                $isMyEventFilter = ($task == 'myevents' || ($task == 'pastevents' && JFactory::getApplication()->input->get('userid')) );
                if($task == 'display' || $task == 'myevents'){
            ?>
            all: '<?php echo html_entity_decode(CRoute::_("index.php?option=com_community&view=events&task=displaysort=upcoming")); ?>',
//            mine: '<?php //echo html_entity_decode(CRoute::_("index.php?option=com_community&view=events&task=myevents")); ?>//'
            mine: '<?php echo html_entity_decode( JUri::base().'myevents/list.html' ); ?>'
            <?php
                }else{
            ?>
            all: '<?php echo html_entity_decode(CRoute::_("index.php?option=com_community&view=events&task=pastevents&sort=upcoming")); ?>',
            mine: '<?php echo html_entity_decode(CRoute::_("index.php?option=com_community&view=events&task=pastevents&userid=" . CFactory::getUser()->id)); ?>'
            <?php
                }
            ?>

        };

        window.location = urls[value] || '?';
    }

    function joms_change_filter( value, type ) {
        var url;

        // Category selector.
        if ( type === 'category' ) {
            if ( value ) {
                url = '<?php echo CRoute::_("index.php?option=com_community&view=events&task=display&categoryid=__cat__"); ?>';
                url = url.replace( '__cat__', value );
            } else {
                url = '<?php echo CRoute::_("index.php?option=com_community&view=events&task=display"); ?>';
            }

            window.location = url;
            return;
        }

        // Filter selector.
        // @todo
    }
</script>

<style type="text/css">
   body{
        background-color: #F8F8F8;
   }
</style>

<?php
// hide this is this is a search page
    if(!$isSearch && $task == 'display' && !$groupid){
?>
    
<div class="joms-page" style="background: none; padding: 0px">
<?php } ?>

<?php if ($events) { ?>
        <?php

            $my = CFactory::getUser();
            $now = new JDate();


            for ($i = 0; $i < count($events); $i++) {
                $event =& $events[$i];

                $isMine = $my->id == $event->creator;
                $isAdmin = $event->isAdmin($my->id);
                $user = CFactory::getUser($event->creator);
                $isPastEvent = $event->getEndDate(false)->toSql() < $now->toSql(true) ? true : false;
                $handler    = CEventHelper::getHandler( $event );
                $memberStatus = $event->getUserStatus($my->id);
                $isEventGuest   = $event->isMember( $my->id );
                $waitingApproval = $event->isPendingApproval($my->id);
                $isGroupEvent = $event->type == CEventHelper::GROUP_TYPE;

                //check if this event is group event
                $isGroup = false;
                $privateGroup = false;
                if($event->type == 'group' && $event->contentid){
                    $isGroup = true;
                    $groupModel = CFactory::getModel('groups');
                    $groupName = $groupModel->getGroupName($event->contentid);
                    $groupLink = CRoute::_( 'index.php?option=com_community&view=groups&task=viewgroup&groupid='.$event->contentid);
                    $privateGroup = $groupModel->needsApproval($event->contentid);
                }

                // Check if "Feature this" button should be added or not.
                $addFeaturedButton = false;
                $isFeatured = false;
                if ($isCommunityAdmin && $showFeatured) {
                    $addFeaturedButton = true;
                    if (in_array($event->id, $featuredList)) {
                        $isFeatured = true;
                    }
                }

                // Check if "Invite friends" and "Settings" buttons should be added or not.
                $canInvite = false;
                $canEdit = false;

                if ($isMine || $isAdmin || $isCommunityAdmin) {
                    if (!$isPastEvent && CEventHelper::seatsAvailable($event)) {
                        $canInvite = true;
                    }
                    $canEdit = true;
                }

                $showRequestInvitationButton = false;
                if (
                    ($event->permission == COMMUNITY_PRIVATE_EVENT) &&
                    (!$isEventGuest) &&
                    (!$waitingApproval) &&
                    (!$handler->isAllowed()) &&
                    ($memberStatus != COMMUNITY_EVENT_STATUS_ATTEND) &&
                    ($memberStatus != COMMUNITY_EVENT_STATUS_WONTATTEND) &&
                    ($memberStatus != COMMUNITY_EVENT_STATUS_MAYBE) &&
                    ($memberStatus != COMMUNITY_EVENT_STATUS_BLOCKED) &&
                    ($memberStatus != COMMUNITY_EVENT_STATUS_BANNED)
                ) {
                    $showRequestInvitationButton = true;
                }

                ?>
               
        <div class="joms-list__item bordered-box">
                    <div style="float: right; margin-top: 15px; <?php if($device == "mobile") { echo 'padding-right: 9px'; } else { echo 'padding-right: 25px'; } ?>">
               <?php if( $handler->isAllowed() && !$isPastEvent && CEventHelper::showAttendButton($event)) { ?>
                    <a href="javascript:" class="joms-button--primary button--small2" 
                        onclick="joms.api.eventResponse('<?php echo $event->id; ?>',
                            ['<?php echo COMMUNITY_EVENT_STATUS_ATTEND; ?>' , '<?php echo JText::_('COM_COMMUNITY_EVENTS_RSVP_ATTEND', true); ?>'],
                            ['<?php echo COMMUNITY_EVENT_STATUS_MAYBE; ?>', '<?php echo JText::_('COM_COMMUNITY_EVENTS_RSVP_MAYBE_ATTEND', true); ?>'],
                            ['<?php echo COMMUNITY_EVENT_STATUS_WONTATTEND; ?>', '<?php echo JText::_('COM_COMMUNITY_EVENTS_RSVP_NOT_ATTEND', true); ?>']);">
                        <?php if ($event->getMemberStatus($my->id) == COMMUNITY_EVENT_STATUS_ATTEND) { ?>
                        <?php echo JText::_('COM_COMMUNITY_EVENTS_RSVP_ATTEND'); ?>
                        <span class="glyphicon glyphicon-ok" style="font-weight:lighter"></span>
                        <?php } else if ($event->getMemberStatus($my->id) >= COMMUNITY_EVENT_STATUS_MAYBE) { ?>
                        <span class="glyphicon glyphicon-question-sign"></span>
                        <?php echo 'Maybe' ?>
                        <?php } else if ($event->getMemberStatus($my->id) >= COMMUNITY_EVENT_STATUS_WONTATTEND) { ?>
                        <span class="glyphicon glyphicon-remove"></span>
                        <?php echo JText::_('COM_COMMUNITY_EVENTS_RSVP_NOT_ATTEND'); ?>
                        <?php } else { ?>
                        <?php echo JText::_('COM_COMMUNITY_GROUPS_INVITATION_RESPONSE'); ?>
                        <svg class="joms-icon" viewBox="0 0 16 16">
                            <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-arrow-down"/>
                        </svg>
                        <?php } ?>
                        
                    </a>
                <?php }elseif($waitingApproval){?>
                    <a href="javascript:" class="joms-button--primary joms-button--small">
                        <?php echo JText::_('COM_COMMUNITY_EVENTS_PENDING_INVITATIONS'); ?>
                    </a>
                <?php } ?>
           </div> 
            
            <div class="col-xs-9 custom-header pull-left" style="<?php if($device == "mobile") { echo 'margin-left: 5px !important'; } else { echo 'margin-left: 30px !important'; } ?>">
               <h1><b><a href="<?php echo $event->getLink(); ?>"> <?php echo $this->escape( $event->title); ?> </a></b></h1>
               <h3><b><?php echo JText::_('COM_COMMUNITY_EVENTS_OWNER');?>: <?php echo $user->getDisplayName();?></b></h3>
               <p class="margin-0 joms--description"><?php echo $this->escape($event->description); ?></p>
            </div>
            
            <div class="col-xs-9 custom-header" style="padding-top: 10px; <?php if($device == "mobile") { echo 'margin-left: 5px !important'; } else { echo 'margin-left: 30px !important'; } ?>">
                <p class="joms--description">
                    <?php echo JText::_('COM_COMMUNITY_EVENTS_DATE_OF_EVENT');?>: <?php echo CEventHelper::formatStartDate($event, JText::_('d/m/Y') ); ?>
                </p>
               
            </div>
            
        </div>
  <?php }?>
</div>

<?php } else { ?>
<div style="padding: 10px 20px;" class="cEmpty cAlert"><?php echo JText::_('COM_COMMUNITY_EVENTS_NO_EVENTS_ERROR'); ?></div>
<?php } ?>

<?php if (isset($pagination) && $pagination->getPagesLinks() && ($pagination->pagesTotal > 1 || $pagination->get('pages.total') > 1) ) { ?>
    <div class="joms-pagination">
        <?php echo $pagination->getPagesLinks(); ?>
    </div>
<?php } ?>
