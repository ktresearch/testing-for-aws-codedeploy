<?php
    defined('_JEXEC') or die();

    $my = CFactory::getUser(); 

    foreach($people as $person) { 
    
    //$isFriend = CFriendsHelper::isConnected($person->id, $my->id);
    //if ($isFriend) continue;

    $displayname = $person->getDisplayName();
    $isWaitingApproval = CFriendsHelper::isWaitingApproval($my->id, $person->id);
    $isWaitingResponse = CFriendsHelper::isWaitingApproval($person->id, $my->id);
    if(in_array($person->id, $myFriendsIds)) continue;
    if($isWaitingApproval) continue;
    if($isWaitingResponse) continue;
    if($person->id == $my->id) continue;
    ?>
    
    <div class="swiper-slide" style="min-height: 140px !important; height: 140px; min-width: 150px !important; width: 150px !important; position: relative; background-color:#FAFCFC; border-color: #F5F5F5">
        <div class="group-title" style="margin: 0 auto !important; box-shadow: none; background-color:#FAFCFC !important; border-color: #F5F5F5 !important; padding-left: 0px; padding-right: 0px">
            <center>
            <img class="left cAvatar jomNameTips group-image" src="<?php echo $person->getThumbAvatar(); ?>" style="float: none; margin: 0 auto !important; display: block;">
            <span style="position: absolute; top: 45px; right: 27px">
                 <div>
                    <?php if ($isWaitingApproval) { ?>
                    <a href="javascript:" class="button_sent_request" onclick="joms.api.friendAdd('<?php echo $person->id;?>')"></a>  
                    <?php } else if ($isWaitingResponse) { ?>
                    <a href="javascript:" class="button_pending_friend" onclick="joms.api.friendResponse('<?php echo $person->id;?>')"></a> 
                    <?php } else if ($isFriend) { ?>
                    <a href="javascript:" class="button_friends" onclick="#"></a> 
                    <?php } else { ?>
                    <a href="javascript:" class="button_add_friend" onclick="joms.api.friendAdd('<?php echo $person->id;?>')"></a> 
                    <?php } ?>
                </div>
            </span>
            <a href="<?php echo CRoute::_(' index.php?option=com_community&view=profile&userid=' . $person->id); ?>">
            <span class="group-statistic displayname" style="float:none; text-align: center; font-weight: bold; padding-top: 15px"><?php echo $displayname; ?></span>
            </a>
            </center>  
        </div>
    </div>
<?php } ?>