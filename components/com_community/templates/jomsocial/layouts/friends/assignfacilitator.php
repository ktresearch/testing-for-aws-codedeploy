<?php
    /**
     * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
     * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
     * @author iJoomla.com <webmaster@ijoomla.com>
     * @url https://www.jomsocial.com/license-agreement
     * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
     * More info at https://www.jomsocial.com/license-agreement
     */
    defined('_JEXEC') or die();

    $currentTask = JFactory::getApplication()->input->getCmd('task');
    $task = JFactory::getApplication()->input->getCmd('task');

    if ($task == 'online') {
        $title = JText::_('COM_COMMUNITY_FRIENDS_ONLINE');
    } else {
        $title = JText::_('COM_COMMUNITY_FRIENDS');
    }
    
    $session = JFactory::getSession();
    $device = $session->get('device');

    $role = JoomdleHelperContent::call_method ('get_user_role', (int)$_GET['courseid']);
    $userroles = array();

    $managers = array();
    $facilitators = array();
    foreach ($role['roles'] as $value) {
        $user_roles = $value['role'];
        $decode_user_roles = json_decode($user_roles);
        foreach ($decode_user_roles as $dur) {
            if($dur->roleid == 1 ) array_push($managers, $value['username']);
            if($dur->roleid == 4 ) array_push($facilitators, $value['username']);
        }
    }

?>
<?php  //echo $sortings; ?>
<script src="components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {   
        jQuery( "input.form-control" ).keyup(function() {
           var value = jQuery( this ).val();          
           if (value != '') {
               jQuery(this).addClass('onkeyup');
           } else {
               jQuery(this).removeClass('onkeyup');
            }
        })
      .keyup();
      
    });
</script>
<div class="joms-page view_friends"> 
    <div class="joms-tab__content">
        <?php $courseid = $_GET['courseid']; ?>
        <form action="/index.php?option=com_community&view=friends&task=assignfacilitatorroles" method="post">
        <input type="hidden" name="courseid" value="<?php echo $courseid; ?>">
        <input type="hidden" name="cgroupid" value="<?php echo $_GET['cgroupid']; ?>">
        <div id="friend-selected-list">
        <div id="friends-list" style="padding-left: 5%">
        
        <div class="container"> 
          <div class="row" >               
        <?php foreach ($friends as $user) { ?>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
              <div class="friend-list">
                  <div class="img_member" style="float: left; padding-right: 15px">
                    <span class="selection">
                    <img width="45" height="45" src="<?php echo $user->getAvatar();?>" alt="" />
                    </span>
                  </div>
                  <div style="text-align: left; display: block;">
                  
                  <?php
                    $hidefacilitator = '';
                    $name_margin_top = "margin-top: -27px"; 
                    if (isset($_GET['notfacilitatedcourse'])) { 
                        $name_margin_top = "margin-top: 0px"; 
                        $hidefacilitator = 'style="display:none;"'; 
                    }
                  ?>
                  
                  <div <?php echo $hidefacilitator; ?>>  
                  <input class="cl-<?php echo $user->id ?>" id="<?php echo 'f' . $user->id ?>" name="facilitator[]" value="<?php echo $user->username;?>" type="checkbox" <?php if(in_array($user->username, $facilitators)) echo ' checked'; ?> /> 
                  <label for="<?php echo 'f' . $user->id ?>"><span></span> <?php echo JText::_('COM_COMMUNITY_ASSIGN_TITLE_FACILITATOR'); ?></label>
                  </div>
                                    
                  <div style="<?php echo $name_margin_top; ?>">
                  <input class="cl-<?php echo $user->id ?>" id="<?php echo 'm' . $user->id ?>" name="manager[]" value="<?php echo $user->username;?>" type="checkbox" <?php if(in_array($user->username, $managers)) echo ' checked'; ?> /> 
                  <label for="<?php echo 'm' . $user->id ?>"><span></span> <?php echo JText::_('COM_COMMUNITY_ASSIGN_TITLE_MANAGER'); ?></label>
                  </div>
                  

                  </div>
                  <div style="text-align: left;">
                  <span class="friend-name">
                  <a href="javascript:void(0);"><?php echo $user->getDisplayName();?></a>
                  </span>
                  </div>
              </div>
            </div>
        <?php } ?>
          </div>
        </div>

       </div>
       </div>   

      <div style="clear:both; float: right; padding-right: 5%; padding-top: 20px">
            <input class="joms-button--primary button--small" type="submit" name="formSubmit" value="<?php echo JText::_('COM_COMMUNITY_ASSIGN_ROLES'); ?>" />
      </div>

       </form>  

        </div>
    </div>
</div>
<style type="text/css">
body
{
    background: #fff;
}
.joms-popup__action .joms-button--small {
    padding: 5.34752px 10.65248px;
}
button.joms-button--primary.joms-button--small {
    margin-top: -4%;
}
#t3-mainnav.navbar-fixed-bottom .t3-megamenu > ul > li > a {
    padding: 3px 0;
    line-height: 15px;
}
.cGroups .jomsocial {
    background: #fff;
}

.jomsocial-wrapper .jomsocial{
  background: none !important;
}
/*** CSS styling the checkbox and radio ***/

input[type="checkbox"] {
    display:none;
}

input[type="checkbox"] + label span{
    display:inline-block;
    width:20px;
    height:20px;
    margin-top: 5px;
    margin-bottom: 5px;
    margin-right: 0;
    background:url("/images/icons/untick30x30.png");
    cursor:pointer;
    background-repeat: no-repeat;
    background-size: 20px 20px;
    background-position: center;
    vertical-align: middle;
    padding-left: 5px;
}

input[type="checkbox"]:checked + label span {
    background:url("/images/icons/tick30x30.png");
    background-size: 20px 20px;
}
</style>

<script>
$(function(){
  $("input:checkbox").click(function(){
    var checkboxgroup = "input:checkbox[class='"+$(this).attr("class")+"']";
    if (!this.checked){
        //do nothing
    }else{
        $(checkboxgroup).attr("checked", false);
        this.checked = true;
    }
  });
});
</script>