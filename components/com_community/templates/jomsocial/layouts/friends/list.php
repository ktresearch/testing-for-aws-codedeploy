<?php
    /**
     * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
     * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
     * @author iJoomla.com <webmaster@ijoomla.com>
     * @url https://www.jomsocial.com/license-agreement
     * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
     * More info at https://www.jomsocial.com/license-agreement
     */
    defined('_JEXEC') or die();

    $currentTask = JFactory::getApplication()->input->getCmd('task');
    $task = JFactory::getApplication()->input->getCmd('task');

    if ($task == 'online') {
        $title = JText::_('COM_COMMUNITY_FRIENDS_ONLINE');
    } else {
        $title = JText::_('COM_COMMUNITY_FRIENDS');
    }
$session = JFactory::getSession();
$device = $session->get('device');
?>
<?php  //echo $sortings; ?>
<script src="components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {   
        jQuery( "input.form-control" ).keyup(function() {
           var value = jQuery( this ).val();          
           if (value != '') {
               jQuery(this).addClass('onkeyup');
           } else {
               jQuery(this).removeClass('onkeyup');
            }
        })
      .keyup();
      
    });
</script>
<div class="joms-page view_friends">  
    <div class="joms-tab__content">

        <div class="joms-list--friend col-xs-12">
            
        <?php if (!empty($friends)) { ?>
            <?php foreach ($friends as $user) { ?>
                <div class="col-xs-4 userAvatar">
                   <div class="remove">   
                   <a style="color: #fff !important;"href="javascript:" class="joms-focus__button--remove" onclick="joms.popup.friend.remove('<?php echo $user->id?>')">
                                x
                            </a>
                    </div>
                        <a href="<?php echo $user->profileLink; ?>" class="joms-avatar">
                    <img src="<?php echo $user->getAvatar(); ?>"/>
                
                    <p><?php echo $user->getDisplayName(); ?></p>
                         
                    </a>
                </div>
            <?php } ?>
            <div class="col-xs-4 addFriend" style="text-align: center">              
                    <a href="<?php echo JURI::base(); ?>component/community/search/?task=browse"
                    >
                        <span class="glyphicon glyphicon-plus-sign"  aria-hidden="true"></span>
                    <p><?php echo JText::_('COM_COMMUNITY_FRIENDS_ADD_FRIENDS'); ?></p>
                    </a>
            </div>
        <?php } else {
            $searchString = JRequest::getVar('q');
            ?>
            <div class="col-xs-4 addFriend" style="width:100% !important;text-align: center">
                <?php if($searchString != null){ ?>
                    <div class="title_friend"><?php echo JText::sprintf('COM_COMMUNITY_NO_FRIENDS_YOU_WANT_FIND', $searchString); ?></div>
                <?php } else { ?>
                    <div class="title_friend"><?php echo JText::_('COM_COMMUNITY_NO_FRIENDS'); ?></div>
                <?php } ?>

                        <div class="add_friend" style="    float: left;">
                    
                    <a href="<?php echo JURI::base(); ?>component/community/search/?task=browse">
                    <span class="glyphicon glyphicon-plus-sign" style="  background: #dddddd; border-radius: 30px;" aria-hidden="true"></span>
                    <p><?php echo JText::_('COM_COMMUNITY_FRIENDS_ADD_BUTTON'); ?></p>
                      </a>
                    </div>               
            </div>
        <?php } ?> 
        
        </div>
    </div>
</div>
<style type="text/css">
body
{
    background: #fff;
}
.joms-popup__action .joms-button--small {
    padding: 5.34752px 10.65248px;
}
button.joms-button--primary.joms-button--small {
    margin-top: -4%;
}
#t3-mainnav.navbar-fixed-bottom .t3-megamenu > ul > li > a {
    padding: 3px 0;
    line-height: 15px;
}

.jomsocial{
    background: #fff !important;
} 
</style>
