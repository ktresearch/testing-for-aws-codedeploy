<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();

$currentTask = JFactory::getApplication()->input->getCmd('task');
$task = JFactory::getApplication()->input->getCmd('task');

if ($task == 'online') {
    $title = JText::_('COM_COMMUNITY_FRIENDS_ONLINE');
} else {
    $title = JText::_('COM_COMMUNITY_FRIENDS');
}
$session = JFactory::getSession();
$device = $session->get('device');

$role = JoomdleHelperContent::call_method('get_user_role', (int) $_GET['courseid']);
$userroles = array();

$learners = array();
foreach ($role['roles'] as $value) {
    $user_roles = $value['role'];
    $decode_user_roles = json_decode($user_roles);
    foreach ($decode_user_roles as $dur) {
        if ($dur->roleid == 5)
            array_push($learners, $value['username']);
    }
}
$groupAvatar = $group->getOriginalAvatar() . '?_=' . time();
?>

<?php //echo $sortings;  ?>
<script src="components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("input.form-control").keyup(function () {
            var value = jQuery(this).val();
            if (value != '') {
                jQuery(this).addClass('onkeyup');
            } else {
                jQuery(this).removeClass('onkeyup');
            }
        })
                .keyup();

    });
</script>
<div class="joms-focus__cover1 joms-focus--mini tablet header_friend_view" style="padding-top: 0px;">
    <div class="circle-image" style="background-image: url('<?php echo $groupAvatar; ?>')">
    </div>
    <div class="joms-focus__title ">
        <a class="back" href="javascript:void(0);" onclick="goBack();"><img src="<?php echo JUri::base(); ?>images/Back-button.png"></a>
        <div class="title_circle">
            <span>
                <?php
                echo $is_mobile ? CActivities::truncateComplex($this->escape($group->name), $titleLength, true) : $this->escape($group->name);
                ?>
            </span>
        </div>

    </div>

</div>
<div class="joms-page view_friends"> 
    <div class="joms-tab__content">
        <h2 class="pageTitle"><?php echo JText::_('COM_COMMUNITY_ASSIGN_LEARNERS');?></h2>
<?php $courseid = $_GET['courseid']; ?>
        <form action="/index.php?option=com_community&view=friends&task=assignlearnerroles" method="post">
            <input type="hidden" name="courseid" value="<?php echo $courseid; ?>">
            <input type="hidden" name="cgroupid" value="<?php echo $_GET['cgroupid']; ?>">
            <div id="friend-selected-list">
                <div id="friends-list" style="padding-left: 5%">
                    <div class="container"> 
                        <?php if (count($friends)) :?>
                            <div class="row" >  
                                <?php foreach ($friends as $k => $user) { ?>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 page page-<?php echo (int)(($k)/6); ?>" style="display: none;">
                                            <label for="<?php echo $user->id ?>" class="labelFriend">
                                            <div class="friend-list">
                                                <div class="inputCheckbox">
                                                    <input name="learner[]" class="learner_input" value="<?php echo $user->username; ?>" type="checkbox" id="<?php echo $user->id ?>" <?php if (in_array($user->username, $learners)) echo ' checked'; ?>/> 
                                                    <span></span>
                                                </div>
                                                <div class="img_member">
                                                    <img src="<?php echo $user->getAvatar(); ?>" alt="" />
                                                </div>
                                                <div class="friend-name">
                                                    <span>
                                                        <?php echo $user->getDisplayName(); ?>
                                                    </span>
                                                </div>
                                            </div>
                                            </label>
                                        </div>
                                <?php } ?>
                            </div>
                            <div class="friendsSeeMore btSeeMore" data-page="1" <?php echo count($friends) <= 6 ? 'style="display: none;"' : '';?>>SEE MORE</div>
                            
                            <div class="divButtons clearfix">
                                <button class="btCancel" type="button"><?php echo JText::_('COM_COMMUNITY_CANCEL_BUTTON');?></button>
                                <button class="btAssign" type="submit" name="formSubmit"><?php echo JText::_('COM_COMMUNITY_ASSIGN_LEARNERS');?></button>
                            </div>
                        <?php else:?>
                            <p><?php echo JText::_('COM_COMMUNITY_ASSIGN_LEARNERS_NULL'); ?></p>
                        <?php endif;?>
                    </div>

                </div>
            </div>   

        </form>  

    </div>
</div>
</div>
<style type="text/css">
    body
    {
        background: #fff;
    }
    .joms-popup__action .joms-button--small {
        padding: 5.34752px 10.65248px;
    }
    button.joms-button--primary.joms-button--small {
        margin-top: -4%;
    }
    #t3-mainnav.navbar-fixed-bottom .t3-megamenu > ul > li > a {
        padding: 3px 0;
        line-height: 15px;
    }

    .cGroups .jomsocial {
        background: #fff;
    }

    .jomsocial-wrapper .jomsocial{
        background: none !important;
    }

    /*** CSS styling the checkbox and radio ***/

    input[type="checkbox"] {
        display:none;
    }

    input[type="checkbox"] + span{
        display:inline-block;
        width:30px;
        height:30px;
        margin-top: 5px;
        margin-bottom: 5px;
        margin-right: 0;
        background:url("/media/joomdle/images/icon/Completed_Uncompleted/NotCompletedIcon.png");
        cursor:pointer;
        background-repeat: no-repeat;
        background-size: 30px 30px;
        background-position: center;
        vertical-align: middle;
        padding-left: 5px;
    }

    input[type="checkbox"]:checked + span {
        background:url("/media/joomdle/images/icon/Completed_Uncompleted/CompetedIcon.png");
        background-size: 30px 30px;
    }
    .pageTitle {
        font-size: 17px;
        font-family: 'Open Sans', OpenSans-Bold, sans-serif;
        font-weight: 700;
        color: #5e5e5e;
        text-align: center;
        margin: 0 0 22px 0;
        line-height: 30px;
    }
    .friend-list {
        display: table !important;
        width: 100%;
        padding-bottom: 22px;
    }
    .friend-list .inputCheckbox {
        display: table-cell;
        width: 45px;
        vertical-align: middle;
        text-align: left;
    }
    .friend-list .img_member {
        display: table-cell;
        width: 86px;
        min-height: 86px;
        margin-left: 15px;
    }
    .friend-list .img_member img { width: 86px; height: 86px; border-radius: 50% !important; }
    .friend-list .friend-name {
        display: table-cell;
        padding: 0 20px;
        font-size: 16px;
        font-family: 'Open Sans', OpenSans-Semibold, sans-serif;
        font-weight: 600;
        color: #828282;
        text-align: left;
    }
    .divButtons {
        text-align: right;
        margin-top: 15px;
    }
    .divButtons button {
        margin: 0 5px;
        border: none;
        font-size: 16px;
        color: #FFFFFF;
        padding: 0 15px;
        background: #4F5355;
        text-align: center;
        line-height: 30px;
        font-family: 'Open Sans', OpenSans, sans-serif;
        font-weight: 400;
        text-transform: uppercase;
        border-radius: 100px;
    }
    .labelFriend { cursor: pointer; width: 100%;}

    @media (max-width: 768px) {
        .tab.task-assigngroupmembers .joms-page.view_friends {
            padding: 0 5%;
        }
        .divButtons {
            text-align: center;
        }
    }
</style>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.divButtons .btCancel').on('click', function() {
            window.location.href = '<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewabout&groupid=' . $_GET['cgroupid']);?>';
        });

        jQuery('.page').filter('.page-'+(jQuery('.btSeeMore').data('page')-1)).css('display', 'block');

        jQuery('body').on('click', '.btSeeMore', function() {
            var page = jQuery(this).data('page');
            jQuery('.page-'+page).css('display', 'block');

            if (jQuery('.page-'+(page+1)).length) {
                jQuery(this).data('page', page+1);
            } else {
                jQuery(this).remove();
            }
        });

        if (jQuery('.learner_input').length) {
            jQuery('.learner_input').change(function() {
                var checked = false;
                $.each(jQuery('.learner_input'), function() {
                    if (jQuery(this).is(':checked')) {
                        checked = true;
                    }
                })

                if (checked) {
                    jQuery('.btAssign').prop('type', 'submit');
                    jQuery('.btAssign').removeClass('alert-choise');
                } else {
                    jQuery('.btAssign').prop('type', 'button');
                    jQuery('.btAssign').addClass('alert-choise');
                }
            })

            var checked = false;
            $.each(jQuery('.learner_input'), function() {
                if (jQuery(this).is(':checked')) {
                    checked = true;
                }
            })

            if (checked) {
                jQuery('.btAssign').prop('type', 'submit');
                jQuery('.btAssign').removeClass('alert-choise');
            } else {
                jQuery('.btAssign').prop('type', 'button');
                jQuery('.btAssign').addClass('alert-choise');
            }
        }

        jQuery('body').on('click', '.alert-choise', function() {
            lgtCreatePopup('withCloseButton', {'content':'<?php echo JText::_('COM_COMMUNITY_ASSIGN_LEARNERS_CHOISE');?>'});
        })

        jQuery('.t3-submenu').find('.t3-megamenu ul').find('li').removeClass('current active');
        jQuery('.t3-submenu').find('.t3-megamenu ul').find('li:nth-child(2)').addClass('current active');
    })
</script>
