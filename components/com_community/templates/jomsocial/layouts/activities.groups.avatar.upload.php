<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/

defined('_JEXEC') or die('Restricted access');

$user = CFactory::getUser($act->actor);

$date = JFactory::getDate($act->created);
if ( $config->get('activitydateformat') == "lapse" ) {
  $createdTime = CTimeHelper::timeLapse($date);
} else {
  $createdTime = $date->format($config->get('profileDateFormat'));
}

$groupLink = CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid='.$act->groupid);
$group = JTable::getInstance('Group', 'CTable');
$group->load($act->groupid);
    $groupParams = new JRegistry($group->params);

$isPhotoModal = $config->get('album_mode') == 1;

if(!isset($act->appTitle)) {
  $act->appTitle = $group->name;
}
$my = CFactory::getUser();
 $photoModel = CFactory::getModel('photos');
 $photo = $photoModel->getPhotoGroup($date,$act->actor);
 
?>

<div class="joms-stream__header">
    <div class="joms-avatar--comment <?php echo CUserHelper::onlineIndicator($user); ?>">
        <img src="<?php echo $user->getAvatar().'?_='.time(); ?>" alt="<?php echo $user->getDisplayName(); ?>">
    </div>
    <div class="joms-stream__meta">
        <div class="joms-stream_title">
        <a href="<?php echo CUrlHelper::userLink($user->id); ?>"><?php if($my->id === $user->id) echo JText::_('COM_COMMUNITY_POST_ME'); else echo $user->getDisplayName(); ?></a>
        <span><?php echo JText::sprintf('COM_COMMUNITY_CHANGE_GROUP_AVATAR', $groupLink, $group->name); ?></span>
        </div>
        <div class="joms-stream-post_time">
         <!--<span class="joms-stream__time"> <?php // echo $user->_status; ?> </span>-->
        <span class="joms-stream__time" style="float: right"><small><?php echo $createdTime; ?></small></span>
        </div>
    </div>
    <?php
        $this->load('activities.stream.options');
    ?>
</div>

<div class="joms-stream__body">
    <div class="joms-avatar-new">
        <a href="javascript:" onclick="joms.api.photoZoom('<?php echo $photo; ?>');">
            <img src="<?php echo $photo; ?>" alt="<?php echo $group->name; ?>" >
        </a>
    </div>
</div>

<?php $this->load('stream/footer'); ?>
