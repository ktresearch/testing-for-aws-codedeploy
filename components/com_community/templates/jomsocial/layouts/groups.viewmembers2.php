<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();
$is_mobile = false;
$session = JFactory::getSession();
$device = $session->get('device');
if($device == 'mobile') {
    $is_mobile = true;
}
$groupsModel = CFactory::getModel('groups');

$membersList = array();
foreach ($members as $member) {
    $user = CFactory::getUser($member->id);

    $user->friendsCount = $user->getFriendCount();
    $user->approved = $member->approved;
    $user->date = $member->date;
    $user->isMe = ( $my->id == $member->id ) ? true : false;
    $user->isAdmin = $groupsModel->isAdmin($user->id, $group->id);
    $user->isOwner = ( $member->id == $group->ownerid ) ? true : false;

    // Check user's permission
    $groupmember = JTable::getInstance('GroupMembers', 'CTable');
    $keys['groupId'] = $group->id;
    $keys['memberId'] = $member->id;
    $groupmember->load($keys);
    $user->isBanned = ( $groupmember->permissions == COMMUNITY_GROUP_BANNED ) ? true : false;

    $membersList[] = $user;
}
$users = array();

foreach($membersList as $member){
    if($member->isAdmin || $member->isOwner){
        $admins[] = $member;
    }else{
        $users[] = $member;
    }
}

$members = $users;
foreach($members as $member){?>

    <div class="joms-list__item bordered-box">
        <div class="col-xs-3 pull-left">
            <div class="jom-list__avatar list_avatar_member" style="padding-top: 15px">
                <a class="cIndex-Avatar" href="<?php echo CRoute::_('index.php?option=com_community&view=profile&fromcircle=1&userid=' . $member->id); ?>">
                    <!--<p class="joms-avatar">-->
                    <img class="cAvatar" src="<?php echo $member->getThumbAvatar(); ?>" alt="<?php echo $member->getDisplayName(); ?>" data-author="<?php echo $member->id; ?>" class="img-responsive">
                    <!--</p>-->
                </a>
            </div>
        </div>

        <div class="custom-header pull-left">
            <h1><a href="<?php echo CRoute::_('index.php?option=com_community&view=profile&fromcircle=1&userid=' . $member->id); ?>">
                    <?php echo CActivities::truncateComplex($member->getDisplayName(), 30, true); ?></a>

                <?php if ($isAdmin || $isSuperAdmin) { ?>
                    <a href="javascript:" class="joms-dropdown-button">
                        <span class="glyphicon glyphicon-menu-down" aria-hidden="true" style="color:#868484"></span>
                    </a>

                    <ul class="joms-dropdown">

                        <li>
                            <a href="javascript:void(0);" onclick="jax.call('community','groups,ajaxAddAdmin', '<?php echo $member->id; ?>', '<?php echo $group->id; ?>');setTimeout('window.location.href=window.location.href', 100);">
                                <i class="icon-make-admin"></i>
                                <span><?php echo JText::_('COM_COMMUNITY_GROUPS_ADMIN'); ?></span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:" onclick="joms.api.groupRemoveMember('<?php echo $group->id; ?>', '<?php echo $member->id; ?>')">
                                <i class="icon-remove-member"></i>
                                <span><?php echo JText::_('COM_COMMUNITY_GROUPS_REMOVE_FROM_GROUP');?></span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:" onclick="joms.api.groupBanMember('<?php echo $group->id; ?>', '<?php echo $member->id; ?>')">
                                <i class="icon-ban-member"></i>
                                <span><?php echo JText::_('COM_COMMUNITY_BAN_MEMBER');?></span>
                            </a>
                        </li>
                    </ul>
                <?php } ?>
                <?php if ($type) { ?>
                    <div class="joms-js--request-notice-group-<?php echo $group->id; ?>-<?php echo $member->id; ?>"></div>
                    <div class="joms-js--request-buttons-group-<?php echo $groupid; ?>-<?php echo $member->id; ?>" style="white-space:nowrap">
                        <a href="javascript:" onclick="joms.api.groupApprove('<?php echo $groupid; ?>', '<?php echo $member->id; ?>');">
                            <button class="joms-button--primary joms-button--smallest joms-button--full-small"><?php echo JText::_('COM_COMMUNITY_PENDING_ACTION_APPROVE'); ?></button>
                        </a>
                        <a href="javascript:" onclick="joms.api.groupRemoveMember('<?php echo $group->id; ?>', '<?php echo $member->id; ?>');">
                            <button class="joms-button--neutral joms-button--smallest joms-button--full-small"><?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING_ACTION_REJECT'); ?></button>
                        </a>
                    </div>
                <?php } ?>

            </h1>

            <!-- <p class="margin-0 joms--description member_since"><?php //echo JText::_('COM_COMMUNITY_MEMBER_SINCE'); ?> <?php //if(!empty($member->date)) {echo date('d/m/Y',$member->date); } else { echo "1/1/2014";}?></p>   -->
        </div>
        <?php if($isMember && $my->id!=$member->id) {  ?>
            <?php
            $isFriend = CFriendsHelper::isConnected($member ->id, $my->id);
            $isWaitingApproval = CFriendsHelper::isWaitingApproval($my->id, $member->id);
            $isWaitingResponse = CFriendsHelper::isWaitingApproval($member ->id, $my->id);
            ?>
            <div class="js-memb-btn-friend">
                <div class="button">
                    <?php if ( $isFriend ) { ?>
                        <a href="javascript:" class="joms-focus__button--friend" onclick="joms.api.friendRemove('<?php echo $member->id;?>')">
                            <?php echo JText::_('COM_COMMUNITY_CIRCLE_FRIEND'); ?>
                        </a>
                    <?php } else if ( $isWaitingApproval ) { ?>
                        <a href="javascript:" onclick="joms.api.friendAdd('<?php echo $member->id;?>')">
                            <button type="button" class="joms-button--primary button--small pending" style="float: right;">
                                <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                <?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING'); ?>
                            </button>
                        </a>
                    <?php } else if ( $isWaitingResponse ) { ?>
                        <a href="javascript:" onclick="joms.api.friendResponse('<?php echo $member->id;?>')">
                            <button style="float: right;" type="button" class="joms-button--primary button--small pending">
                                <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                <?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING'); ?>
                            </button>
                        </a>
                    <?php } else { ?>
                        <a href="javascript:" class="joms-focus__button--add" onclick="joms.api.friendAdd('<?php echo $member->id?>')">
                            <?php echo JText::_('COM_COMMUNITY_CIRCLE_ADD_FRIEND'); ?>
                        </a>
                    <?php } ?>
                </div>

            </div>

        <?php } ?>
    </div>

<?php  } ?>

