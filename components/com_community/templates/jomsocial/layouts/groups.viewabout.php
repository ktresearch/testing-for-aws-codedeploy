<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();

$session = JFactory::getSession();
$is_mobile = ($session->get('device') == 'mobile') ? true : false;
$notification = JRequest::getInt('notification', '');

$config = CFactory::getConfig();
$showGroupDetails = ($group->approvals == 0 || $group->approvals == 1 || ($group->approvals == 2 && $isMember ) || $isSuperAdmin); //who can see the group details
$token = $session->getFormToken();

$my = CFactory::getUser();
if($my->_permission) $cla = 'hidden';
$username = $my->username;
if(!$group->published)
    $hide = 'hidden';
$circleClass = '';
if ($isLearningCircle)
    $circleClass .= 'isLearningCircle ';
if ($isCommunityCircle)
    $circleClass .= 'isCommunityCircle ';
if ($isCommunityCircleWithCourse)
    $circleClass .= 'isCommunityCircleWithCourse ';
if ($isLPCircle)
    $circleClass .= 'isLPCircle ';
if ($isOpenCircle)
    $circleClass .= 'isOpenCircle ';
if ($isPrivateCircle)
    $circleClass .= 'isPrivateCircle ';
if ($isClosedCircle)
    $circleClass .= 'isClosedCircle ';

?>

<div class="joms-page <?php echo $circleClass; ?>">
    <div class="left-col">
        <?php if ($isLPCircle) : ?>
            <?php if ($isOwner || $isAdmin || $isSuperAdmin || $isContentCreator || $isFacilitator) : ?>
                <div class="divMenuChangeTabs">
                    <div class="menuChangeTabs menuPortFaciCata">
                        <?php if ($isOwner || $isAdmin || $isSuperAdmin || $isContentCreator) : ?>
                            <div class="tab lgtActive" data-col="colPortfolio"><?php echo JText::_('COM_COMMUNITY_PORTFOLIO'); ?></div>
                        <?php endif; ?>
                        <?php if ($isFacilitator) : ?>
                            <div class="tab <?php echo (!$isOwner && !$isAdmin && !$isSuperAdmin && !$isContentCreator) ? 'lgtActive' : '' ?>" data-col="colFacilitating"><?php echo JText::_('COM_COMMUNITY_FACILITATING_TEXT'); ?></div>
                        <?php endif; ?>

                        <div class="tab" data-col="colCatalogue"><?php echo JText::_('COM_COMMUNITY_GROUP_CATALOGUE'); ?></div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="divPortfolioCatalogue divColumns">
                <?php if ($isOwner || $isAdmin || $isSuperAdmin || $isContentCreator) : ?>
                    <div class="col colPortfolio lgt-visible">
                        <div class="divMenuChangeTabs">
                            <div class="menuChangeTabs menuCompPendAppr">
                                <div class="tab lgtActive" data-col="colComposing"><?php echo $isContentCreator ? JText::_('COM_COMMUNITY_LP_WIP') : JText::_('COM_COMMUNITY_COMPOSING_TEXT'); ?></div>
                                <div class="tab" data-col="colPending"><?php echo JText::_('COM_COMMUNITY_PENDING_TEXT'); ?></div>
                                <?php if (!$isContentCreator) : ?>
                                    <div class="tab" data-col="colApproved"><?php echo JText::_('COM_COMMUNITY_APPROVED_TEXT'); ?></div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php if ($is_mobile) : ?>
                            <div class="mob-first-block">
                                <div class="left">
                                    <?php if ($isAdmin) { ?>
                                        <div class="btAdd">
                                            <a class="btCreateCourse" href="<?php echo JUri::base() . 'mycourses/create_0_' . $moodlecategoryid . '_lpcourse_.html'; ?>">
                                                <i class="bg-second-img icon-create-course"></i>
                                            </a>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="right btAbout">
                                    <span><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></span>
                                    <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="divComposingPendingApproved divColumns">
                            <div class="col colComposing lgt-visible" <?php echo ($is_mobile) ? 'style="left: 0"' : '' ?>>
                                <div class="lp-composing-courses-box courses-box">
                                    <div class="box-title">
                                        <?php if ($isAdmin) { ?>
                                            <div class="btAdd">
                                                <a class="btCreateCourse" href="<?php echo JUri::base() . 'mycourses/create_0_' . $moodlecategoryid . '_lpcourse_.html'; ?>">
                                                    <i class="bg-second-img icon-create-course"></i>
                                                </a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="box-content">
                                        <div class="listComposingCourses">
                                            <?php
                                            $composingCourses = array_merge($newcourse, $editingCourses);
                                            if ($composingCourses) {
                                                ksort($composingCourses);
                                                $i = 1;
                                                foreach ($composingCourses as $course) {
                                                    $isNewCourse = (in_array($course, $newcourse)) ? true : false;
                                                    $isWIP = (in_array($course, $editingCourses)) ? true : false;
                                                    ?>
                                                    <div id="lpComposingCourse<?php echo $course['id'] ?>" class="courseItem lpComposingCourse lpComposingCourse<?php echo $i; ?> <?php
                                                    echo ($i > 3) ? 'lgt-invisible ' : '';
                                                    echo $isNewCourse ? 'isNewCourse ' : '';
                                                    echo $isWIP ? 'isWIP ' : '';
                                                    ?>">
                                                        <div class="courseImage lazyload" data-src="<?php echo $course['filepath'] . $course['filename'] ?>">
                                                            <?php if ($isWIP) : ?>
                                                                <div class="courseStatus"><?php echo JText::_('COM_COMMUNITY_LP_WIP'); ?></div>
                                                            <?php endif; ?>
                                                            <div class="courseCreator"><?php echo $isWIP ? CFactory::getUser($course['ccUsername'])->getDisplayName() : $course['creator']; ?></div>
                                                        </div>
                                                        <div class="courseContent">
                                                            <div class="courseTitle"><a href="<?php echo JUri::base() . 'course/' . $course['id'] . '.html'; ?>"><?php echo $course['fullname'] ?></a></div>
                                                            <p class="courseDescription"><?php echo CActivities::truncateComplex($course['summary'], 130, true); ?></p>

                                                            <?php if ($isNewCourse) : ?>
                                                                <div class="coursesButtons">
                                                                    <a href="javascript:" class="joms-dropdown-button">
                                                                        <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                                                    </a>

                                                                    <ul class="joms-dropdown">
                                                                        <?php if ($isOwner || $course['is_mycourse']) { ?>
                                                                            <li class=" action ">
                                                                                <a class="btLPRemove btLPComposingRemove" data-id="<?php echo $course['id'] ?>" href="javascript: void(0);"><?php echo JText::_('COM_COMMUNITY_BUTTON_REMOVE'); ?></a>
                                                                            </li>
                                                                        <?php } ?>
                                                                        <?php if (!$course['hasEditingTeacher']) { ?>
                                                                            <li class=" action ">
                                                                                <a class="btLPAssign btLPComposingAssign" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=assignRole&r=cc&coid=' . $course['id'] . '&grid=' . $group->id) ?>"><?php echo JText::_('COM_COMMUNITY_BUTTON_ASSIGN'); ?></a>
                                                                            </li>
                                                                        <?php } ?>
                                                                        <?php if ($course['is_mycourse']) { ?>
                                                                            <li class=" action ">
                                                                                <a class="btLPSendApproval btLPComposingSendingApproval" data-groupid="<?php echo $group->id ?>" data-id="<?php echo $course['id'] ?>" href="javascript: void(0);"><?php echo JText::_('COM_COMMUNITY_SUBMIT'); ?></a>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </div>
                                                            <?php endif; ?>
                                                            <?php if ($isWIP) : ?>
                                                                <a class="btLPSendApproval btLPComposingSendingApproval" data-groupid="<?php echo $group->id ?>" data-id="<?php echo $course['id'] ?>" href="javascript: void(0);"><?php echo JText::_('COM_COMMUNITY_SUBMIT'); ?></a>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $i++;
                                                }
                                                ?>
                                            <?php
                                            }

                                            if ($isContentCreator)
                                                echo (count($composingCourses) == 0) ? '<p class="noCourseWarning">' . JText::_('COM_COMMUNITY_LP_WIP_COURSES_EMPTY_WARNING') . '</p>' : '<p class="noCourseWarning" style="display:none;">' . JText::_('COM_COMMUNITY_LP_WIP_COURSES_EMPTY_WARNING') . '</p>';
                                            else
                                                echo (count($composingCourses) == 0) ? '<p class="noCourseWarning">' . JText::_('COM_COMMUNITY_LP_COMPOSING_COURSES_EMPTY_WARNING') . '</p>' : '<p class="noCourseWarning" style="display:none;">' . JText::_('COM_COMMUNITY_LP_COMPOSING_COURSES_EMPTY_WARNING') . '</p>';
                                            ?>
                                        </div>
                                        <?php if (count($composingCourses) > 3) echo '<div class="lpComposingSeeMore btSeeMore">' . strtoupper(JText::_('COM_COMMUNITY_SEE_MORE')) . '</div>'; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col colPending" <?php echo ($is_mobile) ? 'style="left: 100%"' : '' ?>>
                                <div class="lp-pending-courses-box courses-box">
                                    <div class="box-content">
                                        <div class="listPendingCourses">
                                            <?php
                                            if ($pendingcourse) {
                                                $i = 1;
                                                foreach ($pendingcourse as $course) {
                                                    ?>
                                                    <div id="lpPendingCourse<?php echo $course['id'] ?>" class="courseItem lpPendingCourse lpPendingCourse<?php echo $i; ?> <?php echo ($i > 3) ? 'lgt-invisible' : ''; ?>">
                                                        <div class="courseImage lazyload" data-src="<?php echo $course['filepath'] . $course['filename'] ?>">
                                                            <div class="courseCreator"><?php echo $course['creator']; ?></div>
                                                        </div>
                                                        <div class="courseContent">
                                                            <div class="courseTitle"><a href="<?php echo JUri::base() . 'course/' . $course['id'] . '.html'; ?>"><?php echo $course['fullname'] ?></a></div>
                                                            <p class="courseDescription"><?php echo strip_tags($course['summary']); ?></p>

                                                            <?php if (!$isContentCreator) : ?>
                                                                <div class="coursesButtons">
                                                                    <a href="javascript:" class="joms-dropdown-button">
                                                                        <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                                                    </a>

                                                                    <ul class="joms-dropdown">
                                                                        <li class=" action ">
                                                                            <a class="btLPApprove" data-id="<?php echo $course['id'] ?>" href="javascript: void(0);"><?php echo JText::_('COM_COMMUNITY_BUTTON_APPROVE'); ?> </a>
                                                                        </li>
                                                                        <li class=" action ">
                                                                            <a class="btLPUnapprove" data-id="<?php echo $course['id'] ?>" href="javascript: void(0);"><?php echo JText::_('COM_COMMUNITY_BUTTON_UNAPPROVE'); ?></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $i++;
                                                }
                                                ?>
                                            <?php
                                            }

                                            echo (count($pendingcourse) == 0) ? '<p class="noCourseWarning">' . JText::_('COM_COMMUNITY_LP_PENDING_COURSES_EMPTY_WARNING') . '</p>' : '<p class="noCourseWarning" style="display:none;">' . JText::_('COM_COMMUNITY_LP_PENDING_COURSES_EMPTY_WARNING') . '</p>';
                                            ?>
                                        </div>
                                        <?php if (count($pendingcourse) > 3) echo '<div class="lpPendingSeeMore btSeeMore">' . strtoupper(JText::_('COM_COMMUNITY_SEE_MORE')) . '</div>'; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col colApproved" <?php echo ($is_mobile) ? 'style="left: 200%"' : '' ?>>
                                <div class="lp-approved-courses-box courses-box">
                                    <div class="box-content">
                                        <div class="listApprovedCourses">
                                            <?php
                                            if ($approvedcourse) {
                                                $i = 1;
                                                foreach ($approvedcourse as $course) {
                                                    ?>
                                                    <div id="lpApprovedCourse<?php echo $course['id'] ?>" class="courseItem lpApprovedCourse lpApprovedCourse<?php echo $i; ?> <?php echo ($i > 3) ? 'lgt-invisible' : ''; ?>">
                                                        <div class="courseImage lazyload" data-src="<?php echo $course['filepath'] . $course['filename'] ?>"></div>
                                                        <div class="courseContent">
                                                            <div class="courseTitle"><a href="<?php echo JUri::base() . 'course/' . $course['id'] . '.html'; ?>"><?php echo $course['fullname'] ?></a></div>
                                                            <p class="courseDescription"><?php echo strip_tags($course['summary']); ?></p>

                                                            <a class="btLPPublish" data-id="<?php echo $course['id'] ?>" href="javascript:">
                                                                <?php echo JText::_('COM_COMMUNITY_BUTTON_PUBLISH'); ?>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $i++;
                                                }
                                                ?>
                                            <?php
                                            }

                                            echo (count($approvedcourse) == 0) ? '<p class="noCourseWarning">' . JText::_('COM_COMMUNITY_LP_APPROVED_COURSES_EMPTY_WARNING') . '</p>' : '<p class="noCourseWarning" style="display:none;">' . JText::_('COM_COMMUNITY_LP_APPROVED_COURSES_EMPTY_WARNING') . '</p>';
                                            ?>
                                        </div>
                                        <?php if (count($approvedcourse) > 3) echo '<div class="lpApprovedSeeMore btSeeMore">' . strtoupper(JText::_('COM_COMMUNITY_SEE_MORE')) . '</div>'; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ($isFacilitator) : ?>
                    <div class="col colFacilitating <?php echo (!$isOwner && !$isAdmin && !$isSuperAdmin && !$isContentCreator) ? 'lgt-visible' : '' ?>">
                        <?php if ($is_mobile) : ?>
                            <div class="mob-first-block">
                                <div class="left"></div>
                                <div class="right btAbout">
                                    <span><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></span>
                                    <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="lp-facilitating-courses-box courses-box">
                            <div class="box-content">
                                <div class="listFacilitatingCourses">
                                    <?php
                                    if ($facilitatingCourses) {
                                        $i = 1;
                                        foreach ($facilitatingCourses as $course) {
                                            ?>
                                            <div id="lpFacilitatingCourse<?php echo $course['id'] ?>" class="courseItem lpFacilitatingCourse lpFacilitatingCourse<?php echo $i; ?> <?php echo ($i > 3) ? 'lgt-invisible' : ''; ?>">
                                                <div class="courseImage lazyload" data-src="<?php echo $course['filepath'] . $course['filename'] ?>"></div>
                                                <div class="courseContent">
                                                    <div class="courseTitle"><a href="<?php echo JUri::base() . 'course/' . $course['id'] . '.html'; ?>"><?php echo $course['fullname'] ?></a></div>
                                                    <p class="courseDescription"><?php echo strip_tags($course['summary']); ?></p>
                                                </div>
                                            </div>
                                            <?php
                                            $i++;
                                        }
                                        ?>
                                    <?php
                                    }

                                    echo (count($facilitatingCourses) == 0) ? '<p class="noCourseWarning">' . JText::_('COM_COMMUNITY_LP_FACILITATING_COURSES_EMPTY_WARNING') . '</p>' : '<p class="noCourseWarning" style="display:none;">' . JText::_('COM_COMMUNITY_LP_FACILITATING_COURSES_EMPTY_WARNING') . '</p>';
                                    ?>
                                </div>
                                <?php if (count($facilitatingCourses) > 3) echo '<div class="lpFacilitatingSeeMore btSeeMore">' . strtoupper(JText::_('COM_COMMUNITY_SEE_MORE')) . '</div>'; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="col colCatalogue <?php echo ($isGuest) ? 'lgt-visible' : ''; ?>" <?php echo ($isGuest && !$is_mobile) ? 'style="border-radius: 10px;"' : ''; ?>>
                    <?php if ($isOwner || $isAdmin || $isSuperAdmin) : ?>
                        <div class="divMenuChangeTabs">
                            <div class="menuChangeTabs menuPublUnpu">
                                <div class="tab lgtActive" data-col="colPublish"><?php echo JText::_('COM_COMMUNITY_PUBLISHED_TEXT'); ?></div>
                                <div class="tab" data-col="colUnpublish"><?php echo JText::_('COM_COMMUNITY_BUTTON_UNPUBLISH'); ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($is_mobile) : ?>
                        <div class="mob-first-block">
                            <div class="left"></div>
                            <div class="right btAbout">
                                <span><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></span>
                                <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="divPublishUnpublish divColumns">
                        <div class="col colPublish lgt-visible" <?php echo ($is_mobile) ? 'style="left: 0"' : '' ?>>
                            <div class="lp-published-courses-box courses-box" <?php echo ($isGuest && !$is_mobile) ? 'style="border: 1px solid rgba(130, 130, 130, 0.5);border-radius: 10px;"' : ''; ?>>
                                <?php if ($isGuest && !$is_mobile) : ?>
                                    <h4 class="box-title"><span><?php echo JText::_('COM_COMMUNITY_GROUP_CATALOGUE'); ?></span></h4>
                                <?php endif; ?>
                                <div class="box-content">
                                    <div class="listPublishedCourses">
                                        <?php
                                        if ($lpPublished) {
                                            $i = 1;
                                            foreach ($lpPublished as $course) {
                                                ?>
                                                <div id="lpPublishedCourse<?php echo $course['id'] ?>" class="courseItem lpPublishedCourse lpPublishedCourse<?php echo $i; ?> <?php echo ($i > 3) ? 'lgt-invisible' : ''; ?>">
                                                    <div class="courseImage lazyload" data-src="<?php echo $course['filepath'] . $course['filename'] ?>">
                                                        <?php if (!$isOwner && !$isAdmin && !$isSuperAdmin) :
                                                            $money = ($course['cost'] > 0) ? '$' . number_format((float) $course['cost'], 2, '.', '') : JText::_('COM_COMMUNITY_FREE_TEXT');
                                                            ?>
                                                            <div class="courseDurationPrice"><?php echo $money . ' • ' . $course['duration'] . 'h'; ?></div>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="courseContent">
                                                        <div class="courseTitle"><a href="<?php echo JUri::base() . "providers/product/" . $course['hikashopId'] . '-' . $course['hikashopAlias']; ?>"><?php echo $course['fullname'] ?></a></div>
                                                        <p class="courseDescription"><?php echo strip_tags($course['summary']); ?></p>

                                                        <?php if (!$isGuest && !$isFacilitator && !$isContentCreator) : ?>
                                                            <a class="btUnpublish btLPPublishedUnpublish" data-id="<?php echo $course['id'] ?>" href="javascript: void(0);">
                                                                <?php echo JText::_('COM_COMMUNITY_BUTTON_UNPUBLISH'); ?>
                                                            </a>

                                                            <div class="coursesButtons">
                                                                <a href="javascript:" class="joms-dropdown-button">
                                                                    <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                                                </a>

                                                                <ul class="joms-dropdown">
                                                                    <li class=" action ">
                                                                        <a class="btLPRemove btLPUnpublishedRemove" data-id="<?php echo $course['id'] ?>" href="javascript: void(0);"><?php echo JText::_('COM_COMMUNITY_BUTTON_REMOVE'); ?></a>
                                                                    </li>
                                                                    <li class=" action ">
                                                                        <a class="btLPAssign btLPUnpublishedAssign" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=assignRole&r=cc&coid=' . $course['id'] . '&grid=' . $group->id) ?>"><?php echo JText::_('COM_COMMUNITY_BUTTON_ASSIGN'); ?></a>
                                                                    </li>
                                                                    <li class=" action ">
                                                                        <a class="btLPSendApproval btLPUnpublishedSendingApproval" data-groupid="<?php echo $group->id ?>" data-id="<?php echo $course['id'] ?>" href="javascript: void(0);"><?php echo JText::_('COM_COMMUNITY_SUBMIT'); ?></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        <?php
                                        }
                                        echo (count($lpPublished) == 0) ? '<p class="noCourseWarning">' . JText::_('COM_COMMUNITY_LP_PUBLISHED_COURSES_EMPTY_WARNING') . '</p>' : '<p class="noCourseWarning" style="display:none;">' . JText::_('COM_COMMUNITY_LP_PUBLISHED_COURSES_EMPTY_WARNING') . '</p>';
                                        ?>
                                    </div>
                                    <?php if (count($lpPublished) > 3) echo '<div class="lpPublishedSeeMore btSeeMore">' . strtoupper(JText::_('COM_COMMUNITY_SEE_MORE')) . '</div>'; ?>
                                </div>
                            </div>
                        </div>
                        <?php if ($isOwner || $isAdmin || $isSuperAdmin) : ?>
                            <div class="col colUnpublish" <?php echo ($is_mobile) ? 'style="left: 100%"' : '' ?>>
                                <div class="lp-unpublished-courses-box courses-box">
                                    <div class="box-content">
                                        <div class="listUnpublishedCourses">
                                            <?php
                                            if ($lpUnpublished) {
                                                $i = 1;
                                                foreach ($lpUnpublished as $course) {
                                                    ?>
                                                    <div id="lpUnpublishedCourse<?php echo $course['id'] ?>" class="courseItem lpUnpublishedCourse lpUnpublishedCourse<?php echo $i; ?> <?php echo ($i > 3) ? 'lgt-invisible' : ''; ?>">
                                                        <div class="courseImage lazyload" data-src="<?php echo $course['filepath'] . $course['filename'] ?>"></div>
                                                        <div class="courseContent">
                                                            <div class="courseTitle"><a href="<?php echo JUri::base() . 'course/' . $course['id'] . '.html'; ?>"><?php echo $course['fullname'] ?></a></div>
                                                            <p class="courseDescription"><?php echo strip_tags($course['summary']); ?></p>

                                                            <a class="btUnpublish btLPPublishedUnpublish" data-id="<?php echo $course['id'] ?>" href="javascript: void(0);">
                                                                <?php echo JText::_('COM_COMMUNITY_BUTTON_UNPUBLISH'); ?>
                                                            </a>

                                                            <div class="coursesButtons">
                                                                <a href="javascript:" class="joms-dropdown-button">
                                                                    <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                                                </a>

                                                                <ul class="joms-dropdown">
                                                                    <li class=" action ">
                                                                        <a class="btLPRemove btLPUnpublishedRemove" data-id="<?php echo $course['id'] ?>" href="javascript: void(0);"><?php echo JText::_('COM_COMMUNITY_BUTTON_REMOVE'); ?></a>
                                                                    </li>
                                                                    <li class=" action ">
                                                                        <a class="btLPAssign btLPUnpublishedAssign" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=assignRole&r=cc&coid=' . $course['id'] . '&grid=' . $group->id) ?>"><?php echo JText::_('COM_COMMUNITY_BUTTON_ASSIGN'); ?></a>
                                                                    </li>
                                                                    <li class=" action ">
                                                                        <a class="btLPSendApproval btLPUnpublishedSendingApproval" data-groupid="<?php echo $group->id ?>" data-id="<?php echo $course['id'] ?>" href="javascript: void(0);"><?php echo JText::_('COM_COMMUNITY_SUBMIT'); ?></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $i++;
                                                }
                                                ?>
                                            <?php
                                            }
                                            echo (count($lpUnpublished) == 0) ? '<p class="noCourseWarning">' . JText::_('COM_COMMUNITY_LP_UNPUBLISHED_COURSES_EMPTY_WARNING') . '</p>' : '<p class="noCourseWarning" style="display:none;">' . JText::_('COM_COMMUNITY_LP_UNPUBLISHED_COURSES_EMPTY_WARNING') . '</p>';
                                            ?>
                                        </div>
                                        <?php if (count($lpUnpublished) > 3) echo '<div class="lpUnpublishedSeeMore btSeeMore">' . strtoupper(JText::_('COM_COMMUNITY_SEE_MORE')) . '</div>'; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div><!--end div .colCatalogue-->
            </div><!--end div .divPortfolioCatalogue-->
        <?php else: ?>
            <?php if ($is_mobile) : ?>
                <?php if ($isCommunityCircleWithCourse) : ?>
                    <div class="chatOrCourseMenu">
                        <div class="left lgt-selected"><?php echo JText::_('COM_COMMUNITY_CHATS'); ?></div>
                        <div class="right"><?php echo JText::_('COM_COMMUNITY_LP_COURSES'); ?></div>
                        <div class="selector">
                            <div class="selectedTab"></div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="mob-first-block">
                    <div class="left">
                        <?php if ($isAdmin) { ?>
                            <div class="btAdd">
                                <a class="btCreateAnnouncement" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=addnews&groupid=' . $group->id); ?>">
                                    <!-- <img src="/components/com_community/templates/jomsocial/assets/images/announcement.png"> -->
                                    <i class="bg-second-img icon-announcement-blue <?php echo $hide; ?>"></i>
                                </a>
                            </div>
                        <?php } ?>
                        <?php if ($canCreate && $canCreateChats) { ?>
                            <div class="btAdd">
                                <a class="btCreateDiscussion <?php echo $hide; ?>" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&groupid=' . $group->id . '&task=adddiscussion'); ?>">
                                    <!-- <img src="/components/com_community/templates/jomsocial/assets/images/chat.png"> -->
                                    <i class="bg-second-img icon-discussion-blue"></i>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="right btAbout">
                        <span><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></span>
                        <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                    </div>
                </div>

                <?php
                if (!empty($bulletins)) :
                    $firstCreator = CFactory::getUser(reset($bulletins)->created_by)->getName();
                    ?>
                    <div class="announcements-summary-box">
                        <div class="box-content">
                            <div class="announcement-item">
                                <div class="announcement-content">
                                    <div class="announcementName">
                                        <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewbulletin&groupid=' . $group->id . '&bulletinid=' . reset($bulletins)->id); ?>"><?php
                                            echo $is_mobile ? CActivities::truncateComplex($firstCreator, 30, true) : CActivities::truncateComplex($firstCreator, 45, true);
                                            ?></a>
                                    </div>
                                </div>
                                <div class="des joms--description">
                                    <p class="more"><?php echo nl2br(reset($bulletins)->message); ?></p>
                                </div>
                            </div>
                            <div class="btSeeAll">
                                <span><?php echo JText::_('COM_COMMUNITY_SEE_ALL'); ?></span>
                                <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <div class="announcements-box <?php echo $is_mobile ? 'lgt-hidden' : ''; ?>">
                <h4 class="box-title"><span><?php echo JText::_('COM_COMMUNITY_ANNOUNCEMENTS'); ?></span>
                    <?php if ($isAdmin && !$is_mobile) { ?>
                        <div class="btAdd">
                            <a class="btCreateAnnouncement" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=addnews&groupid=' . $group->id); ?>">
                                <!-- <img src="/components/com_community/templates/jomsocial/assets/images/announcement.png"> -->
                                <i class="bg-second-img icon-announcement-blue <?php echo $hide; ?>"></i>
                            </a>
                        </div>
                    <?php } ?>
                </h4>
                <div class="box-content">
                    <?php
                    if ($bulletins) {
                        if ($is_mobile)
                            echo '<div class="divider" style="padding:0;"></div>';
                        ?>
                        <?php
                        foreach ($bulletins as $k => $v) {
                            $time = CTimeHelper::discussionsTimeLapse($v->date, false);
                            $annCreator = CFactory::getUser($v->created_by);
                            ?>
                            <div class="joms-list__item makeox announcement">
                                <div class="announcement-image">
                                    <div class="jom-list__avatar">
                                        <p class="joms-avatar">
                                            <img src="<?php echo $annCreator->getAvatar(); ?>" class="img-responsive">
                                        </p>
                                    </div>
                                </div>
                                <div class="announcement-item">
                                    <span class="margin-0 date joms--description"><?php echo trim($time); ?></span>
                                    <div class="announcement-content">
                                        <div class="announcementName">
                                            <img width="18" height="18" src="/components/com_community/templates/jomsocial/assets/images/announcement-notification_grey.png"/>
                                            <a onclick="return false;" style="cursor: default;" href="#<?php // echo CRoute::_('index.php?option=com_community&view=groups&task=viewbulletin&groupid=' . $group->id . '&bulletinid=' . $v->id);  ?>"><?php
                                                echo $is_mobile ? CActivities::truncateComplex($annCreator->getName(), 30, true) : CActivities::truncateComplex($annCreator->getName(), 45, true);
                                                ?></a>

                                            <?php if ($isAdmin || $isSuperAdmin) { ?>
                                                <div class="announcementButtons <?php echo $hide; ?>">
                                                    <a href="javascript:" class="joms-dropdown-button">
                                                        <!--<a href="javascript:" class="joms-dropdown-button" data-ui-object="joms-dropdown-button">-->
                                                        <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                                    </a>

                                                    <ul class="joms-dropdown">
                                                        <li class=" action ">
                                                            <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewbulletin&bulletinid=' . $v->id . '&groupid=' . $group->id) ?>">
                                                                <?php echo JText::_('COM_COMMUNITY_EDIT'); ?>
                                                            </a>
                                                        </li>
                                                        <li class=" action ">
                                                            <a href="javascript: void(0);"
                                                               onclick="joms.api.announcementRemove('<?php echo $group->id ?>', '<?php echo $v->id ?>');">
                                                                <?php echo JText::_('COM_COMMUNITY_REMOVE'); ?>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="des joms--description">
                                        <p class="more"><?php echo nl2br($v->message); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="divider"></div>
                        <?php } ?>
                    <?php
                    } else {
                        echo '<p>' . JText::_('COM_COMMUNITY_GROUPS_ANNOUNCEMENT_EMPTY_WARNING') . '</p>';
                    }
                    ?>
                </div>
            </div>
            <?php if (!$is_mobile && $isCommunityCircleWithCourse) : ?>
                <div class="chatOrCourseMenu">
                    <div class="left lgt-selected"><?php echo JText::_('COM_COMMUNITY_CHATS'); ?></div>
                    <div class="right"><?php echo JText::_('COM_COMMUNITY_LP_COURSES'); ?></div>
                    <div class="selector">
                        <div class="selectedTab"></div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($isCommunityCircleWithCourse) : ?>
                <div class="published-courses-box courses-box <?php echo $is_mobile ? 'lgt-hidden' : ''; ?>">
                    <div class="box-content">
                        <div class="listCourses">
                            <?php
                            $i = 1;
                            JPluginHelper::importPlugin('joomdlesocialgroups');
                            $dispatcher = JDispatcher::getInstance();
                            foreach ($publishedCourses as $course) {
                                $course_group = $dispatcher->trigger('get_group_by_course_id', $course['remoteid']);
                            ?>
                                <div id="publishedCourse<?php echo $course['remoteid'] ?>" class="courseItem publishedCourse publishedCourse<?php echo $i; ?> <?php echo ($i > 3) ? 'lgt-invisible' : ''; ?>">
                                    <?php
                                    if(!$group->published) : ?>
                                        <div class="content_publish"></div>
                                    <?php endif; ?>
                                    <div class="courseImage lazyload" data-src="<?php echo $course['filepath'] . $course['filename'] ?>">
                                    </div>
                                    <?php if(!$group->published) {?> <div class="notePublised"><div class="content"><?php  echo JText::_('COM_COMMUNITY_COURSE_ARCHIVED');?></div></div><?php }?>

                                    <div class="courseContent">
                                        <?php if($isOwner || $isSuperAdmin ){ ?>
                                            <div class="courseTitle" style="<?php echo (!$group->published) ? ' z-index: 100; position:relative;': ''  ?>"><a href="<?php echo JUri::base() . 'course/' . $course['remoteid'] . '.html'; ?>"><?php echo $course['fullname'] ?></a></div>
                                        <?php } elseif ($course['isStudent']) { ?>
                                            <div class="courseTitle"><a href="<?php echo JUri::base() . 'course/' . $course['remoteid'] . '_subcribed.html'; ?>"><?php echo $course['fullname'] ?></a></div>
                                        <?php } else { ?>
                                            <div class="courseTitle" ><?php echo $course['fullname'] ?></div>
                                        <?php  } ?>
                                        <p class="courseDescription"><?php echo strip_tags($course['summary']); ?></p>
                                        <?php if (($isOwner || $isSuperAdmin) && $group->published) { ?>
                                            <a class="btUnpublish btPublishedUnpublish" data-id="<?php echo $course['remoteid'] ?>" href="javascript:">
                                                <?php echo JText::_('COM_COMMUNITY_BUTTON_UNPUBLISH'); ?> </a>
                                            <!--                                        --><?php //} else if ($isMember) { ?>
                                        <?php } else { if($group->published){ ?>
                                            <a class="btSubscribe" <?php echo ($course['isStudent']) ? 'style="display: none;"' : ''; ?> data-gc ="<?php echo $course_group[0]?>" data-id="<?php echo $course['remoteid'] ?>" href="#">
                                                <?php echo JText::_('COM_COMMUNITY_BUTTON_SUBSCRIBE'); ?> </a>
                                            <a class="btUnsubscribe" <?php echo (!$course['isStudent']) ? 'style="display: none;"' : ''; ?> data-gc ="<?php echo $course_group[0]?>" data-id="<?php echo $course['remoteid'] ?>" href="#">
                                                <?php echo JText::_('COM_COMMUNITY_BUTTON_UNSUBSCRIBE'); ?> </a>
                                        <?php } }?>
                                    </div>
                                </div>
                                <?php
                                $i++;
                            }
                            ?>
                        </div>
                        <?php
                        if (count($publishedCourses) > 3)
                            echo '<div class="coursesSeeMore btSeeMore">' . strtoupper(JText::_('COM_COMMUNITY_SEE_MORE')) . '</div>';
                        ?>
                    </div>

                    <?php //if (!$isOwner && !$isAdmin && !$isSuperAdmin && !$isMember) : ?>
<!--                         <div class="requireJoinCirclePopup">
                            <div class="closePopup">×</div>
                            <span><?php //echo JText::_('COM_COMMUNITY_REQUIRE_JOIN_CIRCLE') ?></span>
                        </div> -->
                    <?php //endif; ?>

                </div>
            <?php endif; ?>
            <div class="discussions-box">
                <h4 class="box-title"><span><?php echo JText::_('COM_COMMUNITY_CHATS'); ?></span>
                    <?php if ($canCreate && $canCreateChats && !$is_mobile) { ?>
                        <div class="btAdd">
                            <a class="btCreateDiscussion <?php echo $hide; ?>" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&groupid=' . $group->id . '&task=adddiscussion'); ?>">
                                <!-- <img src="/components/com_community/templates/jomsocial/assets/images/chat.png"> -->
                                <i class="bg-second-img icon-discussion-blue <?php echo $hide; ?>"></i>
                            </a>
                        </div>
                    <?php } ?>
                </h4>

                <div class="box-content">
                    <?php
                    if ($discussions) {
                        foreach ($discussions as $row) {
                            $thumbnail = (!empty($row->thumb)) ? $rooturl . '/' . $row->thumb : '/components/com_community/assets/chat-default.png';
                            $date = JFactory::getDate($row->lastreplied);
                            $time = CTimeHelper::discussionsTimeLapse($date, false);
                            $row->message = strip_tags($row->message, true);
                            ?>
                            <div class="joms-list__item discussion <?php echo ($row->notification > 0) ? 'hasNewMes' : ''; ?>">
                                <div class="discussion-image">
                                    <div class="jom-list__avatar">
                                        <p class="joms-avatar">
                                            <img src="<?php echo $thumbnail; ?>" alt="<?php echo $row->user->getDisplayName(); ?>" class="img-responsive">
                                        </p>
                                    </div>
                                </div>

                                <div class="topic-item">
                                    <span class="margin-0 date joms--description"><?php echo $time; ?></span>
                                    <div class="topic-content">
                                        <div class="discussionName">
                                            <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $group->id . '&topicid=' . $row->id); ?>"><?php
                                                echo $is_mobile ? CActivities::truncateComplex($row->title, 16, true) : CActivities::truncateComplex($row->title, 40, true);
                                                ?></a>

                                            <?php if (($isAdmin || $isSuperAdmin || $row->creator == $my->id) && (1 == 0)) { //Disable this feature  ?>
                                                <div class="discussionButtons">
                                                    <a href="javascript:" class="joms-dropdown-button">
                                                        <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                                    </a>

                                                    <ul class="joms-dropdown" id="dropdown-menu-<?php echo $row->id; ?>">
                                                        <li class="setAdmin" onclick="joms.api.avatarChange('discussion', '<?php echo $row->id; ?>');">
                                                            <a href="javascript: void(0);" onclick="">
                                                                <i class="icon-topic-editphoto"></i>
                                                                <span><?php echo JText::_('COM_COMMUNITY_TOPIC_EDIT_PHOTO'); ?></span>
                                                            </a>
                                                        </li>
                                                        <li class="setAdmin">
                                                            <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=editdiscussion&groupid=' . $group->id . '&topicid=' . $row->id); ?>" onclick="">
                                                                <i class="icon-topic-edit"></i>
                                                                <span><?php echo JText::_('COM_COMMUNITY_TOPIC_EDIT'); ?></span>
                                                            </a>
                                                        </li>
                                                        <?php if (!$row->lock) { ?>
                                                            <li class=" action ">
                                                                <a href="javascript: void(0);" onclick="joms.api.discussionLock('<?php echo $group->id ?>', '<?php echo $row->id ?>');">
                                                                    <i class="icon-topic-lock"></i>
                                                                    <span><?php echo JText::_('COM_COMMUNITY_LOCK_TOPIC'); ?></span>
                                                                </a>
                                                            </li>
                                                        <?php } else { ?>
                                                            <li class=" action ">
                                                                <a href="javascript: void(0);" onclick="joms.api.discussionLock('<?php echo $group->id ?>', '<?php echo $row->id ?>');">
                                                                    <i class="icon-topic-unlock"></i>
                                                                    <span><?php echo JText::_('COM_COMMUNITY_UNLOCK_TOPIC'); ?></span>
                                                                </a>
                                                            </li>
                                                        <?php } ?>
                                                        <li class=" action ">
                                                            <a href="javascript: void(0);" onclick="joms.api.discussionRemove('<?php echo $group->id ?>', '<?php echo $row->id ?>');">
                                                                <i class="icon-topic-delete"></i>
                                                                <span><?php echo JText::_('COM_COMMUNITY_TOPIC_DELETE'); ?></span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="des joms--description">
                                        <p class="des-text"><?php
                                            echo $is_mobile ? CActivities::truncateComplex($row->message, 70, true) : CActivities::truncateComplex($row->message, 120, true);
                                            ?></p>
                                        <?php echo ($row->notification > 0) ? '<p class="mes-count"><span>' . $row->notification . '</span></p>' : ''; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="divider"></div>
                        <?php } ?>
                    <?php
                    } else {
                        echo '<p>' . JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_EMPTY_WARNING') . '</p>';
                    }
                    ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="right-col <?php echo $is_mobile ? 'lgt-hidden' : ''; ?>">

        <div class="viewabout about-box">
            <h4 class="box-title"><span><?php echo JText::_('COM_COMMUNITY_ABOUT_TITLE'); ?></span></h4>
            <div id="joms-group--details" class="joms-tab__content box-content" style="<?php echo ((!$config->get('default_group_tab') == 1 && $showGroupDetails) || !$showGroupDetails) ? '' : 'display:none'; ?>">
                <div class="about-content">
                    <?php
                    $des = $group->description;
                    if (strlen($des) > 60) {
                        $shortdes = CActivities::truncateComplex($des, 60, true);
                        echo '<span class="shortdes">' . $shortdes . '<span class="btshow"> ' . JText::_('COM_COMMUNITY_SHOW_MORE') . '</span></span>';
                        echo '<span class="des hidden">' . $des . '<span class="btshow"> ' . JText::_('COM_COMMUNITY_SHOW_LESS') . '</span></span>';
                    } else
                        echo $des;
                    ?>
                </div>
            </div>
        </div>

        <?php if (!$isLearningCircle) { ?>
        <div class="subcircles-box">
            <h4 class="box-title">
                <?php if ($canCreate) { ?>
                    <div class="btAdd">
                        <a class="btCreateSubcircle <?php echo $cla; echo $hide;?>"  href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=create&parentid='.$group->id); ?>">
                            <?php echo JText::_('COM_COMMUNITY_SUBCIRCLES_BOX_CREATE'); ?>
                            <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                        </a>
                    </div>
                <?php } ?>
                <span><?php echo JText::_('COM_COMMUNITY_SUBCIRCLES_BOX_HEADER'); ?></span>
            </h4>
            <div class="box-content">
                <div class="circles-tree">
                    <?php
                    if (!isset($subgroups) || empty($subgroups)) $subgroups = [];
                      $m = 0;
                      $own = 0;
                        foreach ($subgroups as $profileGroup) {
                            $own++;
                            if(!$profileGroup->published && $profileGroup->ownerid != $my->id) continue;
                              $m++;
                            
                        }
                        
                    if($group->ownerid == $my->id)    
                    $countSubgroups = $own;
                    else   $countSubgroups = $m;
                     
                    $level = 1;

                    if ($group->parentid) {
                        $hasSibling = count($siblingsCircles);
                        ?>
                        <div class="parent-circle circle-branch level1">
                            <a href="javascript: ">
                                <div class="circle-image ">
                                    <img src="<?php echo $parentCircle->getThumbAvatar(); ?>" alt="<?php echo $parentCircle->name; ?>" title="<?php echo $parentCircle->name; ?>" onclick="location.href='<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $parentCircle->id); ?>'">
                                    <svg class="svg">
                                        <path d="M 2 0 L 2 32 q 0 15 10 15" stroke="#126db6" stroke-width="1.5" fill="none"></path>
                                    </svg>

                                    <?php if ($hasSibling) { ?>
                                    <svg class="svg" style="left:calc(50% - 24px);width:22px;">
                                        <path d="M 20 0 L 20 32 q 0 15 -15 15 L 0 47" stroke="#929292" stroke-width="1.5" fill="none"></path>
                                    </svg>
                                    <div class="circle-sibling"></div>
                                    <?php } ?>
                                </div>
                                <span class="circle-name" onclick="location.href='<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $parentCircle->id); ?>'" title="<?php echo $parentCircle->name; ?>"><?php echo CActivities::truncateComplex($parentCircle->name, 60, true); ?></span>
                            </a>
                        </div>
                        <?php
                        $level++;
                    }
                    ?>
                    <div class="this-circle circle-branch level<?php echo $level ?>">
                        <a href="javascript: ">
                            <div class="circle-image">
                                <img src="<?php echo $group->getThumbAvatar(); ?>" alt="<?php echo $group->name; ?>" title="<?php echo $group->name; ?>">
                                <?php
                                $h = 32;
                                if ($countSubgroups > 0) {
                                    for ($i = 0; $i < $countSubgroups; $i++) {
                                        ?>
                                        <svg class="svg <?php echo ($i > 2) ? 'lgt-invisible' : ''; ?>" style="height: <?php echo ($h+32).'px'; ?>">
                                            <path d="M 2 0 L 2 <?php echo $h; ?> q 0 15 10 15" stroke="#126db6" stroke-width="1.5"
                                                  fill="none"></path>
                                        </svg>
                                        <?php
                                        $h += 78;
                                    }
                                    ?>
                                    <?php if ($canCreate) {?>
                                    <svg class=" <?php echo $hide;?> svg <?php echo ($countSubgroups >= 3) ? 'lgt-invisible' : ''; ?>" style="z-index:1;stroke-dasharray:7,5;height: <?php echo ($h+32).'px'; ?>">
                                        <path d="M 2 0 L 2 <?php echo $h; ?> q 0 15 10 15" stroke="#828282" stroke-width="1"
                                              fill="none"></path>
                                    </svg>
                                    <?php } ?>
                                <?php
                                } else { ?>
                                    <?php if ($canCreate) {?>
                                    <svg class="svg <?php echo $hide;?> " style="z-index:1;stroke-dasharray:7,5;height: <?php echo ($h+32).'px'; ?>">
                                        <path d="M 2 0 L 2 <?php echo $h; ?> q 0 15 10 15" stroke="#828282" stroke-width="1"
                                              fill="none"></path>
                                    </svg>
                                <?php } ?>
                                <?php } ?>
                            </div>
                            <span class="circle-name" title="<?php echo $group->name; ?>"><?php echo '< '.JText::_('COM_COMMUNITY_SUBCIRCLES_BOX_THIS_CIRCLE_POSITION'); ?></span>
                        </a>

                    </div>
                    <?php
                    $level++;
                    if ($countSubgroups > 0) {
                        $t = 1;
                        foreach ($subgroups as $profileGroup) {
                            if(!$profileGroup->published && $profileGroup->ownerid != $my->id) continue;
                            $table = JTable::getInstance('Group', 'CTable');
                            $table->load($profileGroup->id);

                            $profileGroupAvatar = $table->getThumbAvatar();

                            $gname = strip_tags($profileGroup->name);
                            $gname = $is_mobile ? CActivities::truncateComplex($gname, 30, true) : CActivities::truncateComplex($gname, 30, true);
                            ?>
                    
                            <?php if($profileGroup->published) {  ?>
                            <div class="subcircle circle-branch level<?php echo $level; echo ' '; echo ($t > 3) ? 'lgt-invisible' : ''; ?>">
                                <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $profileGroup->id); ?>">
                                    <div class="circle-image">
                                        <img src="<?php echo $profileGroupAvatar; ?>" alt="<?php echo $profileGroup->name; ?>" title="<?php echo $profileGroup->name; ?>">
                                    </div>
                                    <span class="circle-name" title="<?php echo $profileGroup->name; ?>"><?php echo $gname; ?></span>
                                </a>
                            </div>
                    
                        <?php  }
                        else if(!$profileGroup->published && $profileGroup->ownerid == $my->id ) { ?>
                            <div class="subcircle circle-branch level<?php echo $level; echo ' '; echo ($t > 3) ? 'lgt-invisible' : ''; ?>">
                                <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $profileGroup->id); ?>">
                                    <div class="circle-image">
                                        <img style=" position: absolute; z-index: 1" src="<?php echo $profileGroupAvatar; ?>" alt="<?php echo $profileGroup->name; ?>" title="<?php echo $profileGroup->name; ?>">
                                        <div class='circlePublished'></div>
                                    </div>
                                    <span class="circle-name" style=" margin-top: -50px" title="<?php echo $profileGroup->name; ?>"><?php echo $gname; ?></span>
                                </a>
                            </div>
                        <?php
                        }
                        $t++;
                        } ?>
                    <?php if ($canCreate) {?>
                        <div class="subcircle circle-branch level<?php echo $level?>  btCreateSubcircle <?php echo $cla;?> <?php echo ($t > 3) ? 'lgt-invisible' : ''; ?>">
                            <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=create&parentid='.$group->id); ?>">
                                <div class="circle-image">
                                    <img src="images/icons/CreateSubCircle_1x.png" alt="<?php echo JText::_('COM_COMMUNITY_SUBCIRCLES_BOX_CREATE'); ?>" title="<?php echo JText::_('COM_COMMUNITY_SUBCIRCLES_BOX_CREATE'); ?>">
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                        <?php
                        if ($countSubgroups >= 3) {
                            echo '<div class="subcirclesSeeMore btSeeMore">' . strtoupper(JText::_('COM_COMMUNITY_SEE_MORE')) . '</div>';
                        }
                        ?>
                    <?php } else { ?>
                    <?php if ($canCreate) {?>
                        <div class="subcircle circle-branch  <?php echo $hide;?>  level<?php echo $level?> btCreateSubcircle <?php echo $cla;?>">
                            <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=create&parentid='.$group->id); ?>">
                                <div class="circle-image">
                                    <img src="images/icons/CreateSubCircle_1x.png" alt="<?php echo JText::_('COM_COMMUNITY_SUBCIRCLES_BOX_CREATE'); ?>" title="<?php echo JText::_('COM_COMMUNITY_SUBCIRCLES_BOX_CREATE'); ?>">
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                    <?php } ?>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <?php } ?>

        <?php if (!$isLPCircle) : ?>
            <div class="events-box">
                <h4 class="box-title"><span><?php echo JText::_('COM_COMMUNITY_EVENTS'); ?></span>
                    <?php if ($canCreate) { ?>
                        <div class="btAdd">
                            <a class="btCreateEvent <?php echo $hide;?>" href="<?php echo CRoute::_('events/create?groupid=' . $group->id); ?>">
                                <?php echo JText::_('COM_COMMUNITY_CREATE_EVENT'); ?>
                                <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                            </a>
                        </div>
                    <?php } ?>
                </h4>
                <div class="box-content">
                    <?php
                    if ($tmpEvents) {
                        foreach ($tmpEvents as $event) {
                            $date = strtotime($event->startdate);
                            ?>
                            <div class="joms-list__item event">
                                <div class="eventAvatar">
                                    <img class="cAvatar" src="<?php echo ($event->thumb != '') ? $rooturl . '/' . $event->thumb : JUri::base() . 'components/com_community/assets/event.png' ?>" alt="Event Image"/>
                                </div>
                                <div class="eventInfo">
                                    <div class="eventName">
                                        <a href="<?php echo CRoute::_('events/viewevent/?eventid=' . $event->id); ?>"><?php
                                            echo $is_mobile ? CActivities::truncateComplex($event->title, 20, true) : CActivities::truncateComplex($event->title, 20, true);
                                            ?></a>

                                        <?php if (($isAdmin || $isSuperAdmin || $my->id == $event->creator) && (1 == 0)) { //disable this feature ?>
                                            <div class="eventButtons">
                                                <a href="javascript:" class="joms-dropdown-button">
                                                    <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                                </a>

                                                <ul class="joms-dropdown" id="dropdown-menu-<?php echo $event->id; ?>">
                                                    <li class=" action ">
                                                        <a href="<?php echo CRoute::_('events/edit?eventid=' . $event->id . '&groupid=' . $group->id); ?>">
                                                            <?php echo JText::_('COM_COMMUNITY_EDIT'); ?>
                                                        </a>
                                                    </li>
                                                    <li class=" action ">
                                                        <a href="javascript: void(0);" onclick="joms.api.eventDelete('<?php echo $event->id ?>');">
                                                            <?php echo JText::_('COM_COMMUNITY_REMOVE'); ?>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="eventInfoContent">
                                        <div class="margin-0 joms--description date">
                                            <p class="mon-text"><?php echo date('M', $date); //echo $date->format('M', true)  ?></p>
                                            <p class="date-text"><?php echo date('j', $date); //echo $date->format('j', true) ?></p>
                                        </div>
                                        <div class="margin-0 joms--description des">
                                            <p><?php echo date('D', $date) . ' • ' . CActivities::truncateComplex($event->location, 15, true); //echo $date->format('D', true) .' • '. CActivities::truncateComplex($event->description, 15, true); ?></p>
                                            <p><?php echo $event->confirmedcount . ' ' . JText::_('COM_COMMUNITY_PEOPLE_GOING') ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="clear"></div>
                    <?php
                    } else {
                        echo '<p>' . JText::_('COM_COMMUNITY_GROUPS_EVENT_EMPTY_WARNING') . '</p>';
                    }
                    ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="joms-list__item managers-box" style="<?php echo (!$isMember && $group->approvals == 1) ? 'display: none' : ''; ?>">
            <h4 class="box-title"><span><?php echo JText::_('COM_COMMUNITY_MEMBERS_TITLE_MANAGERS'); ?></span>
                <?php if ($isOwner || $isAdmin) { ?>
                    <div class="btAdd">
                        <a class="btAssignManagers <?php echo $hide;?>"
                           href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=assignRole&r=mn&grid=' . $group->id); ?>">
                            <?php echo JText::_('COM_COMMUNITY_ASSIGN_ROLES'); ?>
                            <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                        </a>
                    </div>
                <?php } ?>
            </h4>
            <div class="box-content">
                <div class="js-memb-list-admin">
                    <?php
                    foreach ($admins as $admin) {
                        $role = [];

                        if ($admin->id == $group->ownerid)
                            $role[] = JText::_('COM_COMMUNITY_VIEWABOUT_OWNER');
                        if (isset($moodleManagers) && in_array($admin->username, $moodleManagers)) {
                            $role[] = JText::_('COM_COMMUNITY_ASSIGN_TITLE_MANAGER');
                            $roleId = 1;
                        }
                        if (isset($facilitators) && in_array($admin->username, $facilitators)) {
                            $role[] = JText::_('COM_COMMUNITY_ASSIGN_TITLE_FACILITATOR');
                            $roleId = 4;
                        }
                        if (empty($role))
                            $role[] = JText::_('COM_COMMUNITY_ASSIGN_TITLE_MANAGER');

                        $role = implode('/', $role);
                        ?>
                        <?php if(!empty($admin->_userid)):?>
                        <div class="admin">
                            <div class="adminAvatar">
                                <a class="cIndex-Avatar" href="<?php echo CRoute::_('index.php?option=com_community&fromcircle=1&view=profile&userid=' . $admin->id); ?>">
                                    <img src="<?php echo $admin->getAvatar(); ?>" data-author-remove="<?php echo $admin->id; ?>" class="cAvatar"/>
                                </a>
                            </div>
                            <div class="adminInfo">
                                <div class="adminName">
                                    <a href="<?php echo CRoute::_('index.php?option=com_community&view=profile&fromcircle=1&userid=' . $admin->id); ?>">
                                        <?php echo CActivities::truncateComplex($admin->getDisplayName(), 30, true); ?></a>
                                    <?php if (($isOwner || $isSuperAdmin || $isAdmin) && ( $admin->id != $group->ownerid)) { ?>
                                        <div class="adminButtons <?php echo $hide;?>">
                                            <?php //echo CFriendsHelper::getUserCog($admin->id, $group->id, null, true);  ?>

                                            <a href="javascript:" class="joms-dropdown-button">
                                                <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                            </a>

                                            <ul class="joms-dropdown">
                                                <li>
                                                    <a href="javascript:" onclick="jax.call('community', 'groups,ajaxRemoveAdmin', '<?php echo $admin->id; ?>', '<?php echo $group->id; ?>', '<?php echo $roleId; ?>', '<?php echo $course_group; ?>');">
                                                        <?php echo JText::_('COM_COMMUNITY_GROUPS_REVERT_ADMIN'); ?>
                                                    </a>
                                                </li>
                                                <?php if(!$isBLNCircle){ ?>
                                                <li>
                                                    <a href="javascript:" onclick="removeMember(<?php echo $group->id . ', ' . $admin->id . ', \'' . $admin->getDisplayName() . '\''; ?>)">
                                                        <span><?php echo JText::_('COM_COMMUNITY_REMOVE'); ?></span>
                                                    </a>
                                                </li>
                                                <li class="btBanMember" data-grid="<?php echo $group->id; ?>" data-mid="<?php echo $admin->id; ?>" data-mname="<?php echo $admin->getDisplayName(); ?>">
                                                    <a href="javascript:" onclick="banAdmin(<?php echo $group->id . ', ' . $admin->id . ', \'' . $admin->getDisplayName() . '\''; ?>)">
                                                        <span><?php echo JText::_('COM_COMMUNITY_GROUPS_BAN_MEMBER'); ?></span>
                                                    </a>
                                                </li>
                                                <?php }?>
                                            </ul>

                                        </div>
                                    <?php } ?>
                                </div>

                                <p class="adminRole"><?php echo $role; ?></p>

                                <?php if ($type) { ?>
                                    <div class="joms-js--request-notice-group-<?php echo $group->id; ?>-<?php echo $admin->id; ?>"></div>
                                    <div class="joms-js--request-buttons-group-<?php echo $group->id; ?>-<?php echo $admin->id; ?>">
                                        <a href="javascript:" onclick="joms.api.groupApprove('<?php echo $group->id; ?>', '<?php echo $admin->id; ?>');">
                                            <button class="joms-button--primary joms-button--smallest joms-button--full-small"><?php echo JText::_('COM_COMMUNITY_PENDING_ACTION_APPROVE'); ?></button>
                                        </a>
                                        <a href="javascript:" onclick="joms.api.groupRemoveMember('<?php echo $group->id; ?>', '<?php echo $admin->id; ?>');">
                                            <button class="joms-button--neutral joms-button--smallest joms-button--full-small"><?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING_ACTION_REJECT'); ?></button>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php endif;?>
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php if (!$isLPCircle) : ?>
            <div class="js-memb-list members-box" style="<?php echo (!$isMember && $group->approvals == 1) ? 'display: none' : ''; ?>">
                <?php if ($isOwner || $isAdmin || $isSuperAdmin) {
                    if ($isBLNCircle) { ?>
                        <h4 class="box-title"><span><?php echo JText::_('COM_COMMUNITY_TITLE_CIRCLE_MEMBERS'); ?></span></h4>
                    <?php } elseif (($isCommunityCircle || $isCommunityCircleWithCourse) && $isPrivateCircle) {
                        ?>
                        <h4 class="box-title"><span><?php echo JText::_('COM_COMMUNITY_TITLE_CIRCLE_MEMBERS'); ?></span>
                            <div class="btAdd">
                                <a class="btAddMembers <?php echo $hide;?>" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=invitemembers&groupid=' . $group->id) ?>">
                                    <?php echo JText::_('COM_COMMUNITY_ADD') . '/' . JText::_('COM_COMMUNITY_BUTTON_APPROVE'); ?>
                                    <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                                </a>
                            </div>
                        </h4>
                    <?php } elseif (!$isLearningCircle) {
                        ?>
                        <h4 class="box-title"><span><?php echo JText::_('COM_COMMUNITY_TITLE_CIRCLE_MEMBERS'); ?></span>
                            <div class="btAdd">
                                <a class="btAddMembers <?php echo $hide;?>" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=invitemembers&groupid=' . $group->id); ?>">
                                    <?php echo JText::_('COM_COMMUNITY_ADD_MEMBERS'); ?>
                                    <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                                </a>
                            </div>
                        </h4>
                    <?php } elseif ($courseInfo['course_status'] == 'published') { ?>
                        <h4 class="box-title"><span><?php echo JText::_('COM_COMMUNITY_LEARNERS'); ?></span>
                            <div class="btAdd" style="<?php echo in_array($username, $facilitators) ? 'display: none' : ''; ?>" >
                                <a class="btAddMembers <?php echo $hide;?>" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=assignRole&r=ln&coid=' . $course_group . '&grid=' . $group->id);//echo CRoute::_('index.php?option=com_community&view=friends&task=assigngroupmembers&assignroles=learner&courseid=' . $course_group . '&cgroupid=' . $group->id) ?>">
                                    <?php echo JText::_('COM_COMMUNITY_ASSIGN_LEARNERS'); ?>
                                    <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                                </a>
                            </div>
                        </h4>
                    <?php } ?>
                <?php } ?>
                <div class="box-content">
                    <div class="list_member" >
                        <?php
                            echo $members ? $membersListHTML : '<p>' . JText::_('COM_COMMUNITY_GROUPS_MEMBER_EMPTY_WARNING') . '</p>';
                        ?>
                    </div>
                    <?php
                    if (count($members) < $membersCount)
                        echo '<div class="membersSeeMore btSeeMore">' . strtoupper(JText::_('COM_COMMUNITY_SEE_MORE')) . '</div>';
                    ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="keywords-box">
            <h4 class="box-title"><span><?php echo JText::_('COM_COMMUNITY_GROUPS_ENTER_KEYWORD'); ?></span></h4>
            <div class="box-content">
                <?php
                if ($group->keyword != null) {
                    $keywords = explode(",", $group->keyword);
                    foreach ($keywords as $keyword) {
                        ?>
                        <a href="<?php echo CRoute::_('index.php?search=' . trim($keyword) . '&' . $token . '=1' . '&option=com_community&view=groups&task=search'); ?>" class="keyword"> <?php echo $keyword ?></a>
                    <?php
                    }
                } else
                    echo '<span>' . JText::_('COM_COMMUNITY_GROUP_NO_KEYS') . '</span>';
                ?>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<div class="notification"></div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        
        <?php if (isset($_REQUEST['loadabout']) && $_REQUEST['loadabout'] == '1' && $is_mobile) { ?>

        jQuery('.back').addClass('backFromRightCol').removeAttr('onclick');
        jQuery('.back.backFromRightCol').on('click', function (e) {
            e.preventDefault();
            jQuery(this).off().removeClass('backFromRightCol').attr('onclick', 'goBack()');
            jQuery('.left-col, .right-col').toggleClass('lgt-hidden');
        });
        jQuery('.left-col, .right-col').toggleClass('lgt-hidden');
        <?php } ?>

        // Configure/customize these variables.

        var showChar = <?php echo ($is_mobile) ? 100 : 250 ?>;  // How many characters are shown by default
        var ellipsestext = "...";
        var moretext = "Show more";
        var lesstext = "Show less";


        jQuery('.more').each(function () {
            var content = jQuery(this).html();

            if (content.length > showChar) {

                var c = content.substr(0, showChar);
                var h = content.substr(showChar, content.length - showChar);

                var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                jQuery(this).html(html);
            }

        });

        jQuery(".morelink").click(function () {
            if (jQuery(this).hasClass("less")) {
                jQuery(this).removeClass("less");
                jQuery(this).html(moretext);
            } else {
                jQuery(this).addClass("less");
                jQuery(this).html(lesstext);
            }
            jQuery(this).parent().prev().toggle();
            jQuery(this).prev().toggle();
            return false;
        });
    });

    jQuery(document).ready(function () {
        jQuery('.menuChangeTabs').each(function () {
            var colnum = jQuery(this).children('.tab').length;
            var colwidth;
            switch (colnum) {
                case 2:
                    colwidth = 50;
                    break;
                case 3:
                    colwidth = 33.33;
                    break;
                case 4:
                    colwidth = 25;
                    break;
                case 5:
                    colwidth = 20;
                    break;
                default:
                    colwidth = 100;
                    break;
            }
            jQuery(this).children('.tab').css('width', colwidth + '%');
        });

        jQuery('.menuChangeTabs .tab').click(function () {
            jQuery(this).parent('.menuChangeTabs').children().removeClass('lgtActive');
            jQuery(this).addClass('lgtActive');
            var clickedTab = jQuery(this).parent('.menuChangeTabs').parent('.divMenuChangeTabs').nextAll('.divColumns').children('.' + jQuery(this).attr('data-col'));
            clickedTab.siblings('.col').removeClass('lgt-visible');
            clickedTab.addClass('lgt-visible');

            <?php if ($is_mobile) : ?>
            clickedTab.css('left', '0');
            var order = jQuery(this).parent('.menuChangeTabs').parent('.divMenuChangeTabs').nextAll('.divColumns').children('.col').index(clickedTab);
            clickedTab.siblings('.col').each(function () {
                var ord = clickedTab.parent().children('.col').index(jQuery(this));
                if (ord > order) {
                    jQuery(this).css('left', parseInt((ord - order) * 100) + '%');
                } else {
                    jQuery(this).css('left', '-' + parseInt((order - ord) * 100) + '%');
                }
            });
            <?php endif; ?>

            jQuery('.courseContent').each(function () {
                console.log(jQuery(this).find('p').height());
                if (jQuery(this).find('p').height() > 40) {
                    jQuery(this).find('p').addClass('limited');
                }
            });
        });

        <?php if ($isLPCircle) : ?>
        jQuery('.menuLeftRight .leftPart').click(function () {
            jQuery('.menuLeftRight .leftPart, .menuLeftRight .rightPart').removeClass('lgt-selected');
            jQuery(this).addClass('lgt-selected');
            jQuery('.menuLeftRight .selector .selectedTab').removeClass('rightSelected');
        });
        jQuery('.menuLeftRight .rightPart').click(function () {
            jQuery('.menuLeftRight .leftPart, .menuLeftRight .rightPart').removeClass('lgt-selected');
            jQuery(this).addClass('lgt-selected');
            jQuery('.menuLeftRight .selector .selectedTab').addClass('rightSelected');
        });
        jQuery('.tab .menuPortFaciCata .leftPart').click(function () {
            jQuery('.colCatalogue').hide();
            jQuery('.colPortfolio').show();
            jQuery('.colFacilitating').show();
        });
        jQuery('.tab .menuPortFaciCata .rightPart').click(function () {
            jQuery('.colPortfolio').hide();
            jQuery('.colFacilitating').hide();
            jQuery('.colCatalogue').show();
        });
        jQuery('.mob .menuPortFaciCata .leftPart').click(function () {
            jQuery('.colCatalogue').addClass('lgt-hidden');
            jQuery('.colFacilitating').removeClass('lgt-hidden');
            jQuery('.colPortfolio').removeClass('lgt-hidden');
        });
        jQuery('.mob .menuPortFaciCata .rightPart').click(function () {
            jQuery('.colFacilitating').addClass('lgt-hidden');
            jQuery('.colPortfolio').addClass('lgt-hidden');
            jQuery('.colCatalogue').removeClass('lgt-hidden');
        });

        jQuery('.mob .menuCompPendAppr.menuChangeTabs .tab').on('click', function () {
            var col = jQuery(this).attr('data-col');
            if (col == 'colComposing')
                jQuery('.colPortfolio .mob-first-block > .left').show();
            else
                jQuery('.colPortfolio .mob-first-block > .left').hide();
            jQuery('.courseContent').each(function () {
                if (jQuery(this).find('p').height() > 40) {
                    jQuery(this).find('p').addClass('limited');
                }
            });
        });
        jQuery('.courseContent').each(function () {
            if (jQuery(this).find('p').height() > 40) {
                jQuery(this).find('p').addClass('limited');
            }
        });

        jQuery('.btUnpublish.btLPPublishedUnpublish').on('click', function (e) {
            e.preventDefault();
            var courseid = jQuery(this).attr('data-id');
            var data = {
                'content': '<?php echo JText::_('COM_COMMUNITY_UNPUBLISH_COURSE_CONFIRMATION'); ?>',
                'yesText': '<?php echo JText::_('COM_COMMUNITY_BUTTON_UNPUBLISH'); ?>'};
            lgtCreatePopup('confirm', data, function () {
                var url = "<?php echo JURI::base(); ?>index.php?option=com_hikashop&ctrl=lpapi&task=unpublishcourse&categoryid=" +<?php echo $moodlecategoryid; ?> + "&courseid=" + courseid;
                var action = 'unpublishlp';
                jQuery.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        course_id: courseid,
                        action: action,
                        cat_id: <?php echo $moodlecategoryid; ?>,
                        act: 'unpublishlp'
                    },
                    beforeSend: function () {
                        lgtCreatePopup('', {'content': 'Loading...'});
                    },
                    success: function (data, textStatus, jqXHR) {
                        lgtRemovePopup();
                        if (data['status']) {
                            jQuery('#lpPublishedCourse' + courseid).fadeOut('400', function () {
                                jQuery(this).prependTo('.listUnpublishedCourses').show();
                                if (jQuery('.listPublishedCourses .lpPublishedCourse').length == 0)
                                    jQuery('.listPublishedCourses .noCourseWarning').show();
                                jQuery('.listUnpublishedCourses .noCourseWarning').hide();
                            });
                        } else {
                            lgtCreatePopup('withCloseButton', {'content': data['message']});
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus)
                        lgtRemovePopup();
                    }
                });

            });
        });

        jQuery('.btLPUnpublishedRemove.btLPRemove').on('click', function (e) {
            e.preventDefault();
            var courseid = jQuery(this).attr('data-id');
            var data = {
                'content': '<?php echo JText::_('COM_COMMUNITY_REMOVE_COURSE_CONFIRMATION'); ?>',
                'yesText': '<?php echo JText::_('COM_COMMUNITY_BUTTON_REMOVE'); ?>'};
            lgtCreatePopup('confirm', data, function () {
                jQuery.ajax({
                    url: "index.php?option=com_hikashop&ctrl=lpapi&task=removecourse&courseid=" + courseid + "&categoryid=" +<?php echo $moodlecategoryid; ?>,
                    type: 'POST',
                    data: {
                        course_id: courseid
                    },
                    beforeSend: function () {
                        lgtCreatePopup('', {'content': 'Loading...'});
                    },
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        lgtRemovePopup();
                        if (data['status']) {
                            jQuery('.listUnpublishedCourses #lpPublishedCourse' + courseid + ', .listUnpublishedCourses #lpUnpublishedCourse' + courseid).fadeOut('400', function () {
                                jQuery(this).remove();
                                if (jQuery('.listUnpublishedCourses .lpUnpublishedCourse').length == 0)
                                    jQuery('.listUnpublishedCourses .noCourseWarning').show();
                            });
                        } else {
                            lgtCreatePopup('withCloseButton', {'content': data['message']});
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                        lgtRemovePopup();
                    }
                });
            });
        });

        jQuery('.btLPComposingRemove.btLPRemove').on('click', function (e) {
            e.preventDefault();
            var courseid = jQuery(this).attr('data-id');
            var data = {
                'content': '<?php echo JText::_('COM_COMMUNITY_REMOVE_COURSE_CONFIRMATION'); ?>',
                'yesText': '<?php echo JText::_('COM_COMMUNITY_BUTTON_REMOVE'); ?>'};
            lgtCreatePopup('confirm', data, function () {
                var url = "<?php echo JURI::base(); ?>mycourses/remove_" + courseid + ".html";
                var act = 'remove';
                jQuery.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        courseid: courseid,
                        act: act
                    },
                    beforeSend: function () {
                        lgtCreatePopup('', {'content': 'Loading...'});
                    },
                    success: function (data, textStatus, jqXHR) {
                        var res = JSON.parse(data);

                        lgtRemovePopup();
                        if (res.error == 1) {
                            lgtCreatePopup('withCloseButton', {'content': res.comment});
                        } else {
                            console.log(res.comment);
                            jQuery('#lpComposingCourse' + courseid).fadeOut('400', function () {
                                jQuery(this).remove();
                                if (jQuery('.listComposingCourses .lpComposingCourse').length == 0)
                                    jQuery('.listComposingCourses .noCourseWarning').show();
                            });
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                        lgtRemovePopup();
                    }
                });
            });
        });

        jQuery('.btLPApprove, .btLPUnapprove').on('click', function (e) {
            e.preventDefault();
            var courseid = jQuery(this).attr('data-id');
            var act = jQuery(this).hasClass('btLPUnapprove') ? 'unapprove' : 'approve';

            var url = "<?php echo JURI::base(); ?>mycourses/approval_" + courseid + "_" + <?php echo $moodlecategoryid; ?> + "_.html";
            jQuery.ajax({
                url: url,
                type: 'POST',
                data: {
                    course_id: courseid,
                    action: 'approval',
                    cat_id: <?php echo $moodlecategoryid; ?>,
                    act: act
                },
                beforeSend: function () {
                    lgtCreatePopup('', {'content': 'Loading...'});
                },
                success: function (data, textStatus, jqXHR) {
                    var res = JSON.parse(data);

                    lgtRemovePopup();
                    if (res.error == 1) {
                        lgtCreatePopup('withCloseButton', {'content': res.message});
                    } else {
                        console.log(res.message);
                        location.reload();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                    lgtRemovePopup();
                }
            });
        });

        jQuery('.btLPComposingSendingApproval, .btLPUnpublishedSendingApproval').on('click', function (e) {
            e.preventDefault();
            var courseid = jQuery(this).attr('data-id');

            var url = "<?php echo JURI::base(); ?>mycourses/sendapprove_" + courseid + "_" + <?php echo $moodlecategoryid; ?> + "_.html";
            var act = 'sendapprove';
            jQuery.ajax({
                url: url,
                type: 'POST',
                data: {
                    course_id: courseid,
                    action: act,
                    cat_id: <?php echo $moodlecategoryid; ?>
                },
                beforeSend: function () {
                    lgtCreatePopup('', {'content': 'Loading...'});
                },
                success: function (data, textStatus, jqXHR) {
                    var res = JSON.parse(data);

                    lgtRemovePopup();
                    if (res.error == 1) {
                        lgtCreatePopup('withCloseButton', {'content': res.message});
                    } else {
                        console.log(res.message);
                        location.reload();
                    }
                },
                error: function () {
                    lgtRemovePopup();
                }
            });
        });

        jQuery('.listApprovedCourses .btLPPublish').on('click', function (e) {
            e.preventDefault();
            var courseid = jQuery(this).attr('data-id');
            var catid = <?php echo $moodlecategoryid; ?>;
            var data = [<?php echo $lpHikashopCategory->category_id; ?>];

            var url = "<?php echo JURI::base(); ?>index.php?option=com_joomdle&view=mycourses&action=publish&course_id=" + courseid + "&catid=" + catid;
            jQuery.ajax({
                url: url,
                type: 'POST',
                data: {
                    course_id: courseid,
                    action: 'publish',
                    cat_id: catid,
                    act: 'publishtohikashop',
                    data: data
                },
                beforeSend: function () {
                    lgtCreatePopup('', {'content': 'Loading...'});
                },
                success: function (data, textStatus, jqXHR) {
                    var res = JSON.parse(data);

                    lgtRemovePopup();
                    if (res.error == 1) {
                        lgtCreatePopup('withCloseButton', {'content': res.message});
                    } else {
                        console.log(res.message);
                        location.reload();
                    }
                },
                error: function () {
                    lgtRemovePopup();
                }
            });
        });

        <?php else : ?>
        jQuery('.btSeeAll > span').click(function () {
            jQuery('.back').addClass('backFromAnno').removeAttr('onclick');
            jQuery('.back.backFromAnno').on('click', function (e) {
                e.preventDefault();
                jQuery(this).off().removeClass('backFromAnno').attr('onclick', 'goBack()');
                jQuery('.announcements-box, .announcements-summary-box, .discussions-box, .mob-first-block, .chatOrCourseMenu').toggleClass('lgt-hidden');
            });
            jQuery('.announcements-box, .announcements-summary-box, .discussions-box, .mob-first-block, .chatOrCourseMenu').toggleClass('lgt-hidden');
        });

        <?php if ($is_mobile) : ?>
        var check_notification =  <?php  echo $notification; ?>;
        if(check_notification == 1){
            jQuery('.back').addClass('backFromAnno').removeAttr('onclick');
            jQuery('.back.backFromAnno').on('click', function (e) {
                e.preventDefault();
                jQuery(this).off().removeClass('backFromAnno').attr('onclick', 'goBack()');
                jQuery('.announcements-box, .announcements-summary-box, .discussions-box, .mob-first-block, .chatOrCourseMenu').toggleClass('lgt-hidden');
            });
            jQuery('.announcements-box, .announcements-summary-box, .discussions-box, .mob-first-block, .chatOrCourseMenu').toggleClass('lgt-hidden');
        }
        <?php endif; ?>

        jQuery('.tab .chatOrCourseMenu .left').click(function () {
            jQuery('.published-courses-box').hide();
            jQuery('.discussions-box').show();
            jQuery('.chatOrCourseMenu .left').addClass('lgt-selected');
            jQuery('.chatOrCourseMenu .right').removeClass('lgt-selected');
            jQuery('.chatOrCourseMenu .selector .selectedTab').removeClass('courseSelected');
        });

        jQuery('.tab .chatOrCourseMenu .right').click(function () {
            jQuery('.discussions-box').hide();
            jQuery('.published-courses-box').show();
            // if (showRequireJoinCirclePopup) {
            //     jQuery('body').addClass('overlay2');
            //     jQuery('.requireJoinCirclePopup').fadeIn();
            //     // showRequireJoinCirclePopup = false;
            // }
            jQuery('.chatOrCourseMenu .left').removeClass('lgt-selected');
            jQuery('.chatOrCourseMenu .right').addClass('lgt-selected');
            jQuery('.chatOrCourseMenu .selector .selectedTab').addClass('courseSelected');
            jQuery('.courseContent').each(function () {
                if (jQuery(this).find('p').height() > 40) {
                    jQuery(this).find('p').addClass('limited');
                }
            });
        });

        jQuery('.mob .chatOrCourseMenu .left').click(function () {
            jQuery('.chatOrCourseMenu .left').addClass('lgt-selected');
            jQuery('.chatOrCourseMenu .right').removeClass('lgt-selected');
            jQuery('.chatOrCourseMenu .selector .selectedTab').removeClass('courseSelected');
            jQuery('.mob-first-block .left').show();
            jQuery('.published-courses-box').addClass('lgt-hidden');
            jQuery('.announcements-summary-box, .discussions-box').removeClass('lgt-hidden');
            jQuery('.mob-first-block').css({'background-color': 'transparent'});
        });
        jQuery('.mob .chatOrCourseMenu .right').click(function () {
            jQuery('.chatOrCourseMenu .left').removeClass('lgt-selected');
            jQuery('.chatOrCourseMenu .right').addClass('lgt-selected');
            jQuery('.chatOrCourseMenu .selector .selectedTab').addClass('courseSelected');
            jQuery('.mob-first-block .left').hide();
            // if (showRequireJoinCirclePopup) {
            //     jQuery('body').addClass('overlay2');
            //     jQuery('.requireJoinCirclePopup').fadeIn();
            //     showRequireJoinCirclePopup = false;
            // }
            jQuery('.published-courses-box').removeClass('lgt-hidden');
            jQuery('.announcements-summary-box, .discussions-box').addClass('lgt-hidden');
            jQuery('.mob-first-block').css({'background-color': '#fff'});
            jQuery('.courseContent').each(function () {
                if (jQuery(this).find('p').height() > 40) {
                    jQuery(this).find('p').addClass('limited');
                }
            });
        });
        <?php endif; ?>

        jQuery('.mob-first-block .btAbout').click(function () {
            jQuery('.back').addClass('backFromRightCol').removeAttr('onclick');
            jQuery('.back.backFromRightCol').on('click', function (e) {
                e.preventDefault();
                jQuery(this).off().removeClass('backFromRightCol').attr('onclick', 'goBack()');
                jQuery('.left-col, .right-col').toggleClass('lgt-hidden');
            });
            jQuery('.left-col, .right-col').toggleClass('lgt-hidden');
        });

        jQuery('.about-box .shortdes .btshow, .about-box .des .btshow').click(function () {
            jQuery('.about-box .shortdes, .about-box .des').toggleClass('hidden');
        });

        <?php if ($isOwner || $isSuperAdmin) : ?>
        jQuery('.btPublishedUnpublish.btUnpublish').click(function (e) {
            e.preventDefault();
            var courseid = jQuery(this).attr('data-id');
            jQuery.ajax({
                type: "POST",
                data: {courseid: courseid, groupid: <?php echo $group->id; ?>},
                url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxRemovePublishedCourse&tmpl=component&format=json') ?>",
                dataType: "json",
                beforeSend: function () {
                    lgtCreatePopup('', {'content': 'Loading...'});
                },
                success: function (data) {
                    lgtRemovePopup();
                    jQuery('#publishedCourse' + courseid).fadeOut();
                },
                error: function () {
                    lgtRemovePopup();
                }
            });
        });

        <?php endif; ?>
        var showRequireJoinCirclePopup = <?php echo (!$isOwner && !$isAdmin && !$isSuperAdmin && !$isMember) ? 'true' : 'false'; ?>;

        jQuery('.btSubscribe').on('click', function (e) {
            if (showRequireJoinCirclePopup) {
                e.preventDefault();

                lgtCreatePopup('oneButton', {content: "<?php echo JText::_('COM_COMMUNITY_REQUIRE_JOIN_CIRCLE') ?>"}, function () {
                    lgtRemovePopup();
                });
                // showRequireJoinCirclePopup = false;
                return false;
            } else {
                e.stopImmediatePropagation();
                e.preventDefault();
                var courseid = jQuery(this).attr('data-id');
                var groupid = jQuery(this).attr('data-gc');
                jQuery.ajax({
                    type: "POST",
                    data: {courseid: courseid,groupid: groupid, act: 1},
                    url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxEnrolUser&tmpl=component&format=json') ?>",
                    dataType: "json",
                    beforeSend: function () {
                        lgtCreatePopup('', {'content': 'Loading...'});
                    },
                    success: function (data) {
                        lgtRemovePopup();
                        jQuery('#publishedCourse' + courseid + ' .btSubscribe').fadeOut();
                        jQuery('#publishedCourse' + courseid + ' .btUnsubscribe').fadeIn();
                        jQuery('#publishedCourse' + courseid + ' .courseTitle').click(function () {
                            window.location.href = '<?php echo JURI::base() . 'course/';?>'+ courseid +'_subcribed.html';
                        });
                        jQuery('#publishedCourse' + courseid + ' .courseTitle').hover(function(){
                            e.preventDefault();
                            jQuery(this).css("cursor", "pointer");
                        });
                    },
                    error: function () {
                        lgtRemovePopup();
                    }
                });
                return false;
            }
        });
        // jQuery('.requireJoinCirclePopup .closePopup').click(function () {
        //     jQuery('body').removeClass('overlay2');
        //     jQuery('.requireJoinCirclePopup').fadeOut();
        // });

        jQuery('.btUnsubscribe').on('click', function (e) {
            e.stopImmediatePropagation();
            e.preventDefault();
            var courseid = jQuery(this).attr('data-id');
            var groupid = jQuery(this).attr('data-gc');
            jQuery.ajax({
                type: "POST",
                data: {courseid: courseid,groupid: groupid, act: 0},
                url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxEnrolUser&tmpl=component&format=json') ?>",
                dataType: "json",
                beforeSend: function () {
                    lgtCreatePopup('', {'content': 'Loading...'});
                },
                success: function (data) {
                    lgtRemovePopup();
                    jQuery('#publishedCourse' + courseid + ' .btSubscribe').fadeIn();
                    jQuery('#publishedCourse' + courseid + ' .btUnsubscribe').fadeOut();
                    jQuery('#publishedCourse' + courseid + ' .courseTitle').click(function () {
                        e.preventDefault();
                        return false;
                    });

                    jQuery('#publishedCourse' + courseid + ' .courseTitle').hover(function(){
                        e.preventDefault();
                        jQuery(this).css("cursor", "default");
                    });
                },
                error: function () {
                    lgtRemovePopup();
                }
            });
        });

        jQuery('.courses-box .btSeeMore').on('click', function () {
            var list = jQuery(this).prev();
            var size = list.find('.courseItem').size();
            var shown = list.find('.courseItem:not(.lgt-invisible)').length;
            list.find('.courseItem:lt(' + (shown + 3) + ')').removeClass('lgt-invisible');
            if (size <= (shown + 3))
                jQuery(this).fadeOut();
            jQuery('.courseContent').each(function () {
                if (jQuery(this).find('p').height() > 40) {
                    jQuery(this).find('p').addClass('limited');
                }
            });
        });

        jQuery('.subcirclesSeeMore').on('click', function () {
            var list = jQuery(this).parent();
            var size = list.find('.subcircle').size();
            var shown = list.find('.subcircle:not(.lgt-invisible)').length;
            list.find('.subcircle:lt(' + (shown + 3) + ')').removeClass('lgt-invisible');
            if (size <= (shown + 3))
                jQuery(this).fadeOut();

            var thiscircle = jQuery(this).parent().find('.this-circle .circle-image');
            var svgsize = thiscircle.find('.svg').size();
            var svgshown = thiscircle.find('.svg:not(.lgt-invisible)').length;
            thiscircle.find('.svg:lt(' + (svgshown + 3) + ')').attr('class', 'svg');
        });

        jQuery('.membersSeeMore').click(function () {
            jQuery('.membersSeeMore').hide();
            jQuery.ajax({
                url: '<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxGetMoreMembers') ?>',
                type: 'POST',
                data: {
                    'groupid': <?php echo $group->id; ?>,
                    'limitstart': jQuery('.list_member .member').length,
                    'limit': 5,
                    'ajaxSend': 1
                },
                beforeSend: function () {
                    jQuery('.membersSeeMore').after('<div class="circlesLoading" style="display: none;">Loading...</div>');
                    jQuery('.circlesLoading').fadeIn();
                },
                success: function (result) {
                    var res = JSON.parse(result);
                    console.log(res);
                    jQuery('.circlesLoading').fadeOut('200', function () {
                        jQuery('.circlesLoading').remove();
                        if (res.html != '') {
                            jQuery('.list_member').append(res.html);
                            if (jQuery('.list_member .member').length < <?php echo $membersCount ?>) {
                                jQuery('.membersSeeMore').fadeIn();
                            }
                        }
                    });
                },
                error: function () {
                    jQuery('.membersSeeMore').fadeIn();
                    jQuery('.circlesLoading').fadeOut('200', function () {
                        jQuery('.circlesLoading').remove();
                    });
                }
            });
        });

        jQuery(document).delegate('.com_community.view-groups.task-viewmembers .joms-popup .joms-popup__action button.joms-button--primary.btPopupAddMembers', 'click', function () {
            location.reload();
        });

        jQuery(document).delegate('.btPopupAddMembers', 'click', function () {
            location.reload();
        });

        // jQuery(document).delegate('.joms-dropdown-button', 'click', function () {
        //     jQuery(this).next('.joms-dropdown').toggle();
        // });

        // jQuery('*:not(".joms-dropdown-button")').click(function () {
        //     jQuery('.joms-dropdown').hide();
        // });

        jQuery(document).on('click','.joms-dropdown-button',function (){
            jQuery(this).next('.joms-dropdown').toggle();
        }) 

//        jQuery(document).on('click', '.btBanMember', function (e) {
//            e.preventDefault();
//            var groupid = jQuery(this).attr('data-grid');
//            var memberid = jQuery(this).attr('data-mid');
//            var membername = jQuery(this).attr('data-mname');
//            lgtCreatePopup('confirm', {'content': 'Are you sure you want to ban ' + membername + '?', 'yesText': 'Ban'}, function () {
//                joms.api.groupBanMember(groupid, memberid);
//            });
//        });


    }(jQuery));

    function banAdmin(groupid, memberid, membername) {
        lgtCreatePopup('confirm', {'content': 'Are you sure you want to ban ' + membername + '?', 'yesText': 'Ban'}, function () {
            joms.api.groupBanMember(groupid, memberid);
        });
    }

    function removeMember(groupid, memberid, membername) {
        console.log(this);
        lgtCreatePopup('confirm', {'content': 'Are you sure you want to remove ' + membername + '?', 'yesText': 'Remove'}, function () {
            joms.ajax({
                func: 'groups,ajaxRemoveMember',
                data: [ memberid, groupid ],
                callback: function( json ) {
                    if (json.success) {
                        location.reload();
                    } else {
                        lgtCreatePopup('withCloseButton', {content: "Something wrong."});
                    }
                }
            });
        });
    }
</script>
