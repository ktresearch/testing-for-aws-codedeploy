<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();
clearstatcache();
$noData = true;
$document = JFactory::getDocument();
$session = JFactory::getSession();
$device = $session->get('device');
$is_mobile = ($device == 'mobile') ? true : false;
$num = $is_mobile ? 5 : 6;
if(strcmp((JFactory::getLanguage()->getTag()),'vi-VN')==0) {
    $langcurrent = 'vn';
}
if(strcmp((JFactory::getLanguage()->getTag()),'en-GB')==0){
    $langcurrent = 'en';

}
$certificates = $profile['certificates'];
// if (isset($profile['certificates'][0]['courseid'])) {
//     foreach ($profile['certificates'] as $key => $value) {
//         $file = JFactory::getApplication()->getCfg('dataroot').'/images/certificates/' .md5(JFactory::getUser($userid)->username . $value['courseid']) . '.jpg';
//         if (is_file($file)) {
//             $certificates[] = $value;
//         }
        
//     }
// }

$showArray = array('status', 'about me', 'interest', 'circles', 'achievements');

$document->addScript(JURI::root(true) . '/modules/mod_joomdle_my_courses/swiper.min.js');
$document->addStyleSheet(JURI::root(true) . '/modules/mod_joomdle_my_courses/css/swiper.min.css');
?>

<div class="app-box-content">
    <ul class="joms-list__row joms-push">
        <div class="status-post">
            <?php
            $statusdisplay = 1;
            $userstatusdisplay = 1;

            if ($userstatusdisplay == 1 && $isMine) {
                ?>
                <form method="post" action="" id="formStatusPost">
                    <input type="text" name="txtStatusPost" id="txtStatusPost" value="" maxlength="140"
                           placeholder="<?php echo JText::_('COM_COMMUNITY_HEADLINE_PLACEHOLDER'); ?>">
                    <!--                        <input type="submit" id="btPost" value="-->
                    <?php //echo JText::_('POST'); ?><!--">-->
                </form>
                <script type="text/javascript">
                    jQuery(document).ready(function () {
                        jQuery('#formStatusPost').submit(function (e) {
                            e.preventDefault();
                            var inputVal = jQuery('.status-post #txtStatusPost').val();
                            if (inputVal == '') {
                                jQuery('<p class="notif error"><?php echo JText::_('COM_COMMUNITY_ERROR'); ?></p>').appendTo('#formStatusPost')
                                    .delay(2000).fadeOut('normal', function () {
                                    jQuery(this).remove()
                                });
                            } else {
                                jax.call('community', 'status,ajaxUpdate', inputVal);
                                jQuery('.status-post #txtStatusPost').val('');
                                jQuery('<p class="notif success"><?php echo JText::_('COM_COMMUNITY_SUCCESSFULLY'); ?></p>').appendTo('#formStatusPost')
                                    .delay(2000).fadeOut('normal', function () {
                                    jQuery(this).remove()
                                });
                                jQuery('.status--data').fadeIn().html(inputVal);
                                jQuery('.posted-on').fadeIn().html("<?php echo date('d F', time()); ?>");
                            }
                        });
                    });
                </script>
                <?php
                $userstatusdisplay = 0;
            }
            ?>
        </div>
        <?php
        if ($statusdisplay == 1) {
            ?>

            <li>
                <h5 class="joms-text--light" style=""><?php echo JText::_('COM_COMMUNITY_HEADLINE'); ?><span
                            class="posted-on"><?php echo date('d F', strtotime($postedOn)); ?></span></h5>
                <span class="status--data joms--description"><?php echo $status; ?></span>
            </li>
            <?php $statusdisplay = 0;
        } ?>
    </ul>
    <?php

    foreach ($profile['fields'] as $groupName => $items) {
        // Gather display data for the group. If there is no data, we can
        // later completely hide the whole segment
        $hasData = false;
        ob_start();
        if ($items != null) {
            ?>
            <ul class="joms-list__row joms-push">
                <?php
                foreach ($items as $item) {
                    if (!in_array(strtolower($item['name']), $showArray)) continue;

                    if (CPrivacy::isAccessAllowed($my->id, $profile['id'], 'custom', $item['access'])) {
                        // There is some displayable data here
                        $hasData = $hasData || CProfileLibrary::getFieldData($item) != '';

                        $fieldData = CProfileLibrary::getFieldData($item);

                        // Escape unless it is URL type, since URL type is in HTML format
                        if ($item['type'] != 'url' && $item['type'] != 'email' && $item['type'] != 'list' && $item['type'] != 'checkbox') {
                            $fieldData = $this->escape($fieldData);
                        }

                        // If textarea, we need to support multiline entry
                        if ($item['type'] == 'textarea') {
                            $fieldData = nl2br($fieldData);
                        }

                        if (!empty($fieldData)) { ?>
                            <?php if (JText::_($item['name']) != 'Interest') {
                                $profileHeader = ($isMine) ? JText::_('COM_COMMUNITY_MY_PROFILE') : $firstName . '\'s Profile';
                                $header = (JText::_($item['name']) != 'About me') ? JText::_($item['name']) : $profileHeader;
                                ?>
                                <li>
                                    <h5 class="joms-text--light"><?php echo $header; ?></h5>
                                    <?php if (!empty($item['searchLink']) && is_array($item['searchLink'])) { ?>
                                        <span class="joms--description">
                                        <?php
                                        foreach ($item['searchLink'] as $linkKey => $linkValue) {
                                            $item['value'] = $linkKey;
                                            if ($item['type'] == 'checkbox') {
                                                echo '<a href="' . $linkValue . '">' . JText::_($item['value']) . '</a><br />';
                                            } else {
                                                echo '<a href="' . $linkValue . '">' . $fieldData . '</a><br />';
                                            }
                                        }
                                        ?>
                                    </span>
                                    <?php } else { ?>
                                        <span class="joms--description">
                                            <?php echo (!empty($item['searchLink'])) ? '<a href="' . $item['searchLink'] . '"> ' . $fieldData . ' </a>' : $fieldData; ?>
                                        </span>
                                    <?php } ?>
                                </li>
                            <?php } else { ?>
                                <li style="overflow:hidden">
                                    <h5 class="joms-text--light"><?php echo JText::_('COM_COMMUNITY_INTERESTS'); ?></h5>

                                    <div class="content">
                                        <?php
                                        $interests = explode(',', $item['value']);
                                        foreach ($interests as $interest) {
                                            echo '<span class="interest--tag joms--description">' . $interest . '</span>';
                                        }
                                        ?>
                                    </div>
                                </li>
                                <div class="clear"></div>
                            <?php } ?>
                            <?php
                        }
                    }
                } ?>
            </ul>

            <?php
        }
        $html = ob_get_contents();
        ob_end_clean();

        // We would only display the profile data in the group if there is actually some
        // data to be displayed
//        if ($hasData) {
        echo $html;
        $noData = false;
//        }
    }

    ?>
    <div class="joms-list__row divCircles">
        <h5 class="joms-text--light"><?php echo JText::_('COM_COMMUNITY_CIRCLES'); ?><span class="circlesCount"><?php echo ($countGroups > 0) ? $countGroups : '';?></span></h5>
        <?php if (!empty($profileGroups)) { ?>
            <div class="mygroups have-groups ">
                <div class="mygroup-content-block">
                    <div class="swiper-container swiper-container-horizontal">
                        <div class="swiper-wrapper">
                            <?php
                            $i = 1;
                            foreach ($profileGroups as $profileGroup) {
                                $table = JTable::getInstance('Group', 'CTable');
                                $table->load($profileGroup->id);

                                $profileGroupAvatar = ($profileGroup->avatar == '') ? JURI::root(true) . '/components/com_community/assets/group.png' : $table->getOriginalAvatar(). '?_=' . time();

                                $gname = strip_tags($profileGroup->name);
                                ?>
                                <div class="swiper-slide <?php if ($i > 2) echo 'hidden';?>">
                                    <a class="gr-image" style="background-image: url('<?php echo $profileGroupAvatar; ?>')" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $profileGroup->id); ?>">
                                    </a>
                                    <div class="group-title">
                                        <div class="gr-title-box">
                                            <a class="gr-title"
                                               href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $profileGroup->id); ?>"><?php echo $gname; ?>

                                            </a>
                                        </div>

                                        <div class="gr-description-box">
                                            <?php
                                            $desc = strip_tags($profileGroup->description);
                                            ?>

                                            <p class="group-description"><?php echo $desc; ?></p>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                $i++;
                            }?>

                        </div>
                    </div>
                    <div style="clear:both"></div>
                </div>
            </div>
            <div class="circlesSeeMore" style="<?php if ($countGroups <= 2) echo 'display: none;'; ?>"><?php echo strtoupper(JText::_('COM_COMMUNITY_SEE_MORE'));?></div>
            <script type="text/javascript">
                jQuery(document).ready(function() {
                    /*
                    var swiper = new Swiper('.mygroups .swiper-container', {
                        pagination: '.mygroups .swiper-pagination',
                        slidesPerView: <?php echo ($device == 'mobile') ? '1' : '2.5'; ?>,
                        nextButton: '.mygroups .swiper-button-next',
                        prevButton: '.mygroups .swiper-button-prev',
                        spaceBetween: 10,
                        freeMode: true,
                        freeModeMomentum: true
                    });
                    */
                    /*
                    if (jQuery('.divCircles .swiper-slide').length < <?php echo $countGroups ?> ) {
                        jQuery('.circlesSeeMore').show();
                    }
                    jQuery('.circlesSeeMore').click(function() {
                        jQuery('.circlesSeeMore').hide();
                        jQuery.ajax({
                            url: '<?php echo CRoute::_('index.php?option=com_community&view=profile&task=ajaxGetMoreCircles&userid=' . $userid)?>',
                            type: 'POST',
                            data: {
                                'limitstart': jQuery('.divCircles .swiper-slide').length,
                                'limit': 10,
                                'ajaxSend': 1
                            },
                            beforeSend: function() {
                                jQuery('.circlesSeeMore').after('<div class="circlesLoading" style="display: none;">Loading...</div>');
                                jQuery('.circlesLoading').fadeIn();
                            },
                            success: function(result) {
                                var res = JSON.parse(result);
                                console.log(res);
                                jQuery('.circlesLoading').fadeOut('400', function() {jQuery('.circlesLoading').remove();});
                                jQuery.each(res, function( index, value ) {
                                    var avatar = value.avatar ? '<?php echo JFactory::getApplication()->getCfg('wwwrootfile'); ?>/'+value.avatar : '/components/com_community/assets/group.png';
                                    var html = '<div class="swiper-slide">'+
                                        '<a class="gr-image" style="background-image: url(\''+avatar+'\')" href="/circles/viewabout?groupid='+value.id+'">'+
                                        '</a>'+
                                        '<div class="group-title">'+
                                        '<div class="gr-title-box">'+
                                        '<a class="gr-title" href="/circles/viewabout?groupid='+value.id+'">'+value.name+
                                        '</a>'+
                                        '</div>'+
                                        '<div class="gr-description-box">'+
                                        '<p class="group-description">'+value.description+'</p>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>';
                                    jQuery('.divCircles .swiper-wrapper').append(html);
                                });
                                if (jQuery('.divCircles .swiper-slide').length < <?php echo $countGroups ?> ) {
                                    jQuery('.circlesSeeMore').fadeIn();
                                }
                                reCss();
                            },
                            error: function() {
                                jQuery('.circlesSeeMore').fadeIn();
                                jQuery('.circlesLoading').fadeOut('400', function() {jQuery('.circlesLoading').remove();});
                            }
                        });
                    });
                    */
                    reCss();
                });
                jQuery(window).load(function(){
                    reCss();
                });
                function reCss() {
                    jQuery('.divCircles .group-title a.gr-title').each(function() {
                        if (jQuery(this).outerHeight() > 22) jQuery(this).addClass('limited');
                    });
                    jQuery('.divCircles .gr-description-box p.group-description').each(function() {
                        if (jQuery(this).outerHeight() > 44) jQuery(this).addClass('limited');
                    });
                }
            </script>

        <?php } else { ?>
            <div class="divIcon"><img src="<?php echo JURI::base();?>/images/MyCirclesIcon_555555.png" alt="<?php echo JText::_('COM_COMMUNITY_CIRCLES'); ?>"></div>
            <p class="divText"><?php echo ($isMine) ? JText::_('COM_COMMUNITY_DIV_CIRCLES_TEXT') : CFactory::getUser($userid)->getName().' '.JText::_('COM_COMMUNITY_DIV_CIRCLES_TEXT_NOT_MINE') ; ?></p>
        <?php }
        echo '</div>';

        if (in_array( 'achievements' , $showArray)) {
            ?>
            <div class="joms-list__row divAchievements">
                <h5 class="joms-text--light"><?php echo JText::_('PROFILE_ACHIEVEMENTS'); ?><span class="achievementsCount"><?php echo (isset($certificates[0]['courseid']) && count($certificates) > 0) ? count($certificates) : '';?></span></h5>
                    <?php if (isset($certificates[0]['courseid']) && !empty($certificates)) { ?>
                        <div class="achievements">
                            <div class='swiper-container'>
                                <div class='swiper-wrapper'>
                                    <?php
                                    $i = 1;
                                    foreach ($certificates as $k => $v) {
                                        $cid = $v['courseid'];
                                        $file = JFactory::getApplication()->getCfg('dataroot').'/images/certificates/' .md5(JFactory::getUser($userid)->username . $cid) . '.jpg';
                                        // $url = JRoute::_("index.php?option=com_joomdle&view=certificate&courseid=".$cid);
                                        $url = JURI::base() .'certificate/' . $cid . '.html';
                                        ?>
                                        <div class="swiper-slide <?php if ($i > 2) echo 'hidden';?>">
                                            <?php if (is_file($file)) :?>
                                            <a href="<?php echo JFactory::getApplication()->getCfg('wwwrootfile'); ?>/images/certificates/<?php echo md5(JFactory::getUser($userid)->username . $v['courseid']) . '.jpg?_=' . time(); ?>">
    <!--                                            <img data-kvsrc="--><?php //echo JFactory::getApplication()->getCfg('wwwrootfile'); ?><!--/images/certificates/--><?php //echo md5(JFactory::getUser($userid)->username . $v['courseid']) . '.jpg?_=' . time(); ?><!--" alt="Cert Thumbnail">-->
                                                <div class="cert-img" style="background-image: url('<?php echo JFactory::getApplication()->getCfg('wwwrootfile'); ?>/images/certificates/<?php echo md5(JFactory::getUser($userid)->username . $v['courseid']) . '.jpg?_=' . time(); ?>')"></div>
                                                <p class="cert-name"><?php echo $v['name']; ?></p>
                                                <p class="course-name"><?php echo $v['coursename']; ?></p>
                                            </a>
                                            <?php else:?>
                                                <a href="<?php echo $url;?>">
                                                    <div id="images" class="left lazyload" data-src="<?php echo JURI::base();?>/courses/pluginfile.php/26753/course/overviewfiles/20597378_1650767854942021_7528165735509091444_n.jpg?1530586705000" style="height: 254px;">
                                                        <div class="lgt-fail" alt="Fail" title="Load Certificate Failed" style=""></div>
                                                    </div>
                                                    <p class="cert-name"><?php echo $v['name']; ?></p>
                                                    <p class="course-name"><?php echo $v['coursename']; ?></p>
                                                </a>
                                            <?php endif;?>
                                        </div>
                                    <?php $i++;} ?>
                                </div>
                            </div>
                        </div>
                        <div class="certificatesSeeMore" style="<?php if (count($certificates) <= 2) echo 'display: none;';?>"><?php echo strtoupper(JText::_('COM_COMMUNITY_SEE_MORE'));?></div>

                    <?php } else { ?>
                        <div class="achievements emptyAchievements">
                            <div class="divIcon"><img src="<?php echo JURI::base();?>/images/MyLearningIcon_555555.png" alt="<?php echo JText::_('PROFILE_ACHIEVEMENTS'); ?>"></div>
                            <p class="divText"><?php echo ($isMine) ? JText::_('COM_COMMUNITY_DIV_ACHIEVEMENTS_TEXT') : CFactory::getUser($userid)->getName().' '.JText::_('COM_COMMUNITY_DIV_ACHIEVEMENTS_TEXT_NOT_MINE') ; ?></p>
                        </div>
                    <?php } ?>

            </div>
        <?php
        }

        if ($noData) {
            if ($isMine) {
                ?>
                <!--            <div class="joms-list__row">-->
                <!--                --><?php //echo JText::_('COM_COMMUNITY_PROFILES_SHARE_ABOUT_YOURSELF'); ?>
                <!--            </div>-->
            <?php
            } else {
                ?>
<!--                <div class="joms-list__row">-->
<!--                    --><?php //echo JText::_('COM_COMMUNITY_PROFILES_NO_INFORMATION_SHARE'); ?>
<!--                </div>-->
            <?php
            }
        }
        ?>
    </div>

    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function() {
            /*
            var swiper_nd = new Swiper('.achievements .swiper-container', {
                pagination: '.achievements .swiper-pagination',
                slidesPerView: <?php echo ($device == 'mobile') ? '2.5' : '3.5'; ?>,
                nextButton: '.achievements .swiper-button-next',
                prevButton: '.achievements .swiper-button-prev',
                spaceBetween: 8,
                freeMode: true,
                freeModeMomentum: true
            });
            */
            /*
            jQuery('.certificatesSeeMore').click(function() {
                for (var i = 0; i < 10; i++) {
                    jQuery('.achievements .swiper-slide.hidden:nth-of-type('+i+')').removeClass('hidden');
                }
                if (jQuery('.achievements .swiper-slide.hidden').length == 0) jQuery(this).fadeOut();
            });
            */
            jQuery('img').each(function() {
                jQuery(this).attr('src', jQuery(this).attr('data-kvsrc') );
            });

            if (jQuery('#system-message').text().length > 0) {
                jQuery('.jomsocial').css({'padding-top': '0px'});
                jQuery('.alert-message').css("cssText", "padding-top: 20px !important;");
            }

            jQuery('.circlesSeeMore, .certificatesSeeMore').on('click', function() {
                var list = jQuery(this).parent();
                var size = list.find('.swiper-slide').size();
                var shown = list.find('.swiper-slide:not(.hidden)').length;
                list.find('.swiper-slide:lt(' + (shown + <?php echo $num;?>) + ')').removeClass('hidden');
                if (size <= (shown + <?php echo $num;?>)) jQuery(this).fadeOut();
            });
        });
    </script>