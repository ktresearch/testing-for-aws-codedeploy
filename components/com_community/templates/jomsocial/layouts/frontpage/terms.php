<?php
defined('_JEXEC') or die();
$isGuest = (JFactory::getUser()->guest == 1) ? true : false;
$check_is_guest = "";
$jinput = JFactory::getApplication()->input;
$appview = $jinput->get('type', '', 'GET');
$banner = $jinput->get('banner', 'int');
if($appview == 'appview') {
    $class = 'appview';
}
?>
<div class="terms-page">

	<?php if ($isGuest) :
        $check_is_guest = "guest";
        ?>
    <?php if($banner!=1){ ?>
    <div class="privacy-policy-page-banner <?php echo $class; ?>">
		<div class="left">
			<a href="<?php echo JURI::root( false );?>">
	        	<img alt="Parenthexis" src="templates/t3_bs3_tablet_desktop_template/images/PARENTHEXIS_LOGO_NEW.png">
	        </a>
        </div>
        <div class="right <?php echo $class; ?>">
            <a href="<?php echo JURI::root( false );?>">
                <?php echo JText::_('COM_COMMUNITY_LOG_IN'); ?>
            </a>
            <a href="<?php echo JRoute::_('index.php?option=com_community&view=register', false); ?>">
                <?php echo JText::_('COM_COMMUNITY_FRONT_SIGN_UP'); ?>
            </a>
        </div>
    </div>
    <?php }else{ ?>
        <div id="signup-back">
            <a href="javascript:void(0);" onclick="goBack();"> <img id="image_back" src="<?php echo JURI::root();?>images/backbutton.png"></a>
        </div>
        <?php } ?>
	<?php endif; ?>
    <div class="terms-page-content <?php echo $check_is_guest; ?>">
        <h1 class="title" <?php if($banner == 1) { ?>style="margin-top: -75px;" <?php }?> >
            <?php if($banner == 2){ ?>
                <div id="back-quick">
                    <a href="javascript:void(0);" onclick="goBack();"> <img id="image_back" src="<?php echo JURI::root();?>images/backbutton.png"></a>
                </div>
            <?php }?>
            <?php echo JText::_('COM_COMMUNITY_TERMS');?>
        </h1>
        <div class="paragraph">
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT1');?></p>
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT2');?></p>
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT3');?></p>
        </div>
        <div class="paragraph">
            <p class="letter">I.</p>
            <p class="paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_PARAGRAPH_TITLE1');?></p>
            <div class="sub-paragraph">
                <p class="letter">A.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_A');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT4');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT5');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT6');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT7');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="letter">B.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_B');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT8');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT9');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="letter">C.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_C');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT10');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT11');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT12');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT13');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT14');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="letter">D.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_D');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT15');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="letter">E.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_E');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT17');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="letter">F.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_F');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT18');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT19');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="letter">G.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_G');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT20');?></p>
                <p class="bullet">(i)</p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT21');?></p>
                <p class="bullet">(ii)</p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT22');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT23');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT24');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="letter">H.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_H');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT25');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT26');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="letter">I.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_I');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT27');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="letter">J.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_J');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT28');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="letter">K.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_K');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT29');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT30');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="letter">L.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_L');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT31');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="letter">M.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_M');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT32');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="letter">N.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_N');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT33');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="letter">O.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_O');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT34');?></p>
            </div>

        </div>
        <!-- end paragraph-->
        <div class="paragraph">
            <p class="letter">II.</p>
            <p class="paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_PARAGRAPH_TITLE2');?></p>
            <div class="sub-paragraph">
                <p class="letter">A.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_A_P2');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT35');?></p>
            </div>
        </div>
        <!-- end paragraph-->
        <div class="paragraph">
            <p class="letter">III.</p>
            <p class="paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_PARAGRAPH_TITLE3');?></p>
            <div class="sub-paragraph">
                <p class="letter">A.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_A_P3');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT36');?></p>
                <p class="bullet">•</p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT37');?></p>
                <p class="bullet">•</p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT38');?></p>
                <p class="bullet">•</p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT39');?></p>
                <p class="bullet">•</p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT40');?></p>
                <p class="bullet">•</p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT41');?></p>
                <p class="bullet">•</p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT42');?></p>
                <p class="bullet">•</p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT43');?></p>
                <p class="bullet">•</p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT44');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT45');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT46');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT47');?></p>
            </div>
        </div>
        <!-- end paragraph-->
        <div class="paragraph">
            <p class="letter">IV.</p>
            <p class="paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_PARAGRAPH_TITLE4');?></p>
            <div class="sub-paragraph">
                <p class="letter">A.</p>
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_SUB_PARAGRAPH_TITLE_A_P4');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT48');?> <a href="<?php echo JURI::root( false ).'/privacy-policy';?>"><?php echo JText::_('COM_COMMUNITY_HERE');?></a>.</p>
            </div>
        </div>
        <!-- end paragraph-->
        <div class="paragraph">
            <p class="letter">V.</p>
            <p class="paragraph-title"><?php echo JText::_('COM_COMMUNITY_TERMS_PARAGRAPH_TITLE5');?></p>
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT49');?></p>
            <p class="bullet">•</p>
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT50');?></p>
            <p class="bullet">•</p>
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT51');?></p>
            <p class="bullet">•</p>
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT52');?></p>
            <p class="bullet">•</p>
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT53');?></p>
            <p class="bullet">•</p>
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_TEXT54');?></p>
        </div>
        <!-- end paragraph-->
    </div>
    <?php if($banner!=1){ ?>
    <?php if ($isGuest) :?>
        <div class="connect-with-us <?php echo $class; ?>">
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_CONNECT_WITH_US');?></p>
            <div class="connect-icons">
                <a class="fbook" href="https://www.facebook.com/parenthexis/"><img src="<?php echo JURI::root(true); ?>/images/login/facebook_icon.png"></a>
                <a class="insta" href="https://www.instagram.com/parenthexis/"><img src="<?php echo JURI::root(true); ?>/images/login/instagram_icon.png"></a>
                <a class="email" href="mailto:contact@parenthexis.com"><img src="<?php echo JURI::root(true); ?>/images/login/email_icon.png"></a>
            </div>
        </div>
        <div class="div-footer-menu <?php echo $class; ?>">
            <ul>
                <li><a href="<?php echo JURI::root(true); ?>/about"><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></a></li>
                <li><a href="<?php echo JURI::root(true); ?>/terms"><?php echo JText::_('COM_COMMUNITY_TERMS'); ?></a></li>
                <li><a href="<?php echo JURI::root(true); ?>/privacy-policy"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY'); ?></a></li>
                <li><a href="<?php echo JURI::root(true); ?>/support"><?php echo JText::_('COM_COMMUNITY_SUPPORT'); ?></a></li>
                <li><a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a></li>
                <li><a href="<?php echo JURI::root(true); ?>/business"><?php echo JText::_('COM_COMMUNITY_BUSINESS'); ?></a></li>

        </ul>
    </div>
    <div class="divCopyright <?php echo $class; ?>">
            <p>© <?php echo Date("Y", time());?> PARENTHEXIS</p>
    </div>
    <?php endif; }?>
</div>
<script>
    function goBack() {
        // window.history.back();
        var nav = window.navigator;
        if( this.phonegapNavigationEnabled &&
            nav &&
            nav.app &&
            nav.app.backHistory ){
            nav.app.backHistory();
        } else {
            window.history.back();
        }
    }
</script>