<?php
defined('_JEXEC') or die();
$isGuest = (JFactory::getUser()->guest == 1) ? true : false;
$jinput = JFactory::getApplication()->input;
$appview = $jinput->get('type', '', 'GET');
if($appview == 'appview') {
    $class = 'appview';
}
?>
<div class="user-story-page">
    <?php if ($isGuest) :
        $check_is_guest = "guest";
        ?>
        <style type="text/css">
            .mob.view-frontpage.task-user-story .home {
                padding-bottom: 25px;
            }
        </style>
        <div class="user-story-page-banner <?php echo $class; ?>">
            <div class="left">
                <a href="<?php echo JURI::root(false); ?>">
                    <img alt="Parenthexis"
                         src="templates/t3_bs3_tablet_desktop_template/images/PARENTHEXIS_LOGO_NEW.png">
                </a>
            </div>
            <div class="right">
                <a style="font-weight: bold;" href="<?php echo JURI::root(true); ?>/user-story">
                    <?php echo JText::_('COM_COMMUNITY_USER_STORIES'); ?>
                </a>
                <a href="<?php echo JURI::root(false); ?>">
                    <?php echo JText::_('COM_COMMUNITY_LOG_IN'); ?>
                </a>
                <a href="<?php echo JRoute::_('index.php?option=com_community&view=register', false); ?>">
                    <?php echo JText::_('COM_COMMUNITY_FRONT_SIGN_UP'); ?>
                </a>
            </div>
            <div class="center">
                <p><?php echo JText::_('COM_COMMUNITY_USER_STORY_BANER_1'); ?></p>
                <p>"<?php echo JText::_('COM_COMMUNITY_USER_STORY_BANER_2_1'); ?></p>
                <p><?php echo JText::_('COM_COMMUNITY_USER_STORY_BANER_2_2'); ?>"</p>
                <p><?php echo JText::_('COM_COMMUNITY_USER_STORY_BANER_3'); ?></p>
            </div>
        </div>
    <?php endif; ?>
    <div class="user-story-page-content container-fluid <?php echo $check_is_guest; ?>">
        <div class="paragraph row">
            <img class="col-xs-2 col-sm-2 col-md-1 col-lg-1" alt="Parenthexis"
                 src="/images/login/user1.jpg">
            <div class="col-xs-10 col-sm-10 col-md-11 col-lg-11">
                <p class="username"><span><?php echo JText::_('COM_COMMUNITY_USER_STORY_PARAGRAPH_TITLE_A_1'); ?></span>
                    <?php echo JText::_('COM_COMMUNITY_USER_STORY_PARAGRAPH_TITLE_A_2'); ?>
                </p>
                <div class="sub-paragraph">
                    <p class="sub1"><?php echo JText::_('COM_COMMUNITY_USER_STORY_TEXT1'); ?></p>
                    <p class="sub2"><?php echo JText::_('COM_COMMUNITY_USER_STORY_TEXT2'); ?></p>
                </div>
            </div>
        </div>

        <div class="paragraph row">
            <img class="col-xs-2 col-sm-2 col-md-1 col-lg-1" alt="Parenthexis"
                 src="/images/login/user2.jpg">
            <div class="col-xs-10 col-sm-10 col-md-11 col-lg-11">
                <p class="username"><span><?php echo JText::_('COM_COMMUNITY_USER_STORY_PARAGRAPH_TITLE_B_1'); ?></span>
                    <?php echo JText::_('COM_COMMUNITY_USER_STORY_PARAGRAPH_TITLE_B_2'); ?>
                </p>
                <div class="sub-paragraph">
                    <p class="sub1"><?php echo JText::_('COM_COMMUNITY_USER_STORY_TEXT3'); ?></p>
                    <p class="sub2"><?php echo JText::_('COM_COMMUNITY_USER_STORY_TEXT4'); ?></p>
                </div>
            </div>
        </div>
        <div class="paragraph row">
            <img class="col-xs-2 col-sm-2 col-md-1 col-lg-1" alt="Parenthexis"
                 src="/images/login/user3.jpg">
            <div class="col-xs-10 col-sm-10 col-md-11 col-lg-11">
                <p class="username"><span><?php echo JText::_('COM_COMMUNITY_USER_STORY_PARAGRAPH_TITLE_C_1'); ?></span>
                    <?php echo JText::_('COM_COMMUNITY_USER_STORY_PARAGRAPH_TITLE_C_2'); ?>
                </p>
                <div class="sub-paragraph">
                    <p class="sub1"><?php echo JText::_('COM_COMMUNITY_USER_STORY_TEXT5'); ?></p>
                    <p class="sub2"><?php echo JText::_('COM_COMMUNITY_USER_STORY_TEXT6'); ?></p>
                </div>
            </div>
        </div>
        <div class="paragraph row">
            <img class="col-xs-2 col-sm-2 col-md-1 col-lg-1" alt="Parenthexis"
                 src="/images/login/user4.jpg">
            <div class="col-xs-10 col-sm-10 col-md-11 col-lg-11">
                <p class="username"><span><?php echo JText::_('COM_COMMUNITY_USER_STORY_PARAGRAPH_TITLE_D_1'); ?></span>
                    <?php echo JText::_('COM_COMMUNITY_USER_STORY_PARAGRAPH_TITLE_D_2'); ?>
                </p>
                <div class="sub-paragraph">
                    <p class="sub1"><?php echo JText::_('COM_COMMUNITY_USER_STORY_TEXT7'); ?></p>
                    <p class="sub2"><?php echo JText::_('COM_COMMUNITY_USER_STORY_TEXT8'); ?></p>
                </div>
            </div>
        </div>
        <div class="paragraph row">
            <img class="col-xs-2 col-sm-2 col-md-1 col-lg-1" alt="Parenthexis"
                 src="/images/login/user5.jpg">
            <div class="col-xs-10 col-sm-10 col-md-11 col-lg-11">
                <p class="username"><span><?php echo JText::_('COM_COMMUNITY_USER_STORY_PARAGRAPH_TITLE_E_1'); ?></span>
                    <?php echo JText::_('COM_COMMUNITY_USER_STORY_PARAGRAPH_TITLE_E_2'); ?>
                </p>
                <div class="sub-paragraph">
                    <p class="sub1"><?php echo JText::_('COM_COMMUNITY_USER_STORY_TEXT9'); ?></p>
                    <p class="sub2"><?php echo JText::_('COM_COMMUNITY_USER_STORY_TEXT10'); ?></p>
                </div>
            </div>
        </div>
    </div>
    <?php if ($isGuest) : ?>
        <div class="connect-with-us <?php echo $class; ?>">
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_CONNECT_WITH_US'); ?></p>
            <div class="connect-icons">
                <a class="fbook" href="https://www.facebook.com/parenthexis/"><img src="<?php echo JURI::root(true); ?>/images/login/facebook_icon.png"></a>
                <a class="insta" href="https://www.instagram.com/parenthexis/"><img src="<?php echo JURI::root(true); ?>/images/login/instagram_icon.png"></a>
                <a class="email" href="mailto:contact@parenthexis.com"><img src="<?php echo JURI::root(true); ?>/images/login/email_icon.png"></a>
            </div>
        </div>
        <div class="div-footer-menu <?php echo $class; ?>">
            <ul>
                <li><a href="<?php echo JURI::root(true); ?>/about"><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></a>
                </li>
                <li><a href="<?php echo JURI::root(true); ?>/terms"><?php echo JText::_('COM_COMMUNITY_TERMS'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/privacy-policy"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/support"><?php echo JText::_('COM_COMMUNITY_SUPPORT'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/business"><?php echo JText::_('COM_COMMUNITY_BUSINESS'); ?></a>
                </li>
                <!-- <li><a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a></li>
<!--        <li>-->
                <!--        <a href="--><?php //echo JURI::root(true); ?><!---->
                <?php //echo JText::_('COM_COMMUNITY_BUSINESS'); ?><!--</a>-->
            </ul>
        </div>
        <div class="divCopyright <?php echo $class; ?>">
            <p>© <?php echo Date("Y", time());?> PARENTHEXIS</p>
        </div>
    <?php endif; ?>
</div>