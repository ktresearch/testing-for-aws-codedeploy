<?php
defined('_JEXEC') or die();
$isGuest = (JFactory::getUser()->guest == 1) ? true : false;
$jinput = JFactory::getApplication()->input;
$appview = $jinput->get('type', '', 'GET');
$style = 'margin-left: 45%';
$style1 = 'margin-left: 42%';
if($appview == 'appview') {
    $class = 'appview';
    $style = '';
    $style1 = '';
}
?>
<div class="partners-page <?php echo $isGuest ? 'guest' : ''; ?>">
<?php if ($isGuest) : ?>
    <div class="partners-banner <?php echo $class; ?>">
        <div class="left">
            <a href="<?php echo JURI::root(false); ?>">
                <img alt="Parenthexis"
                     src="templates/t3_bs3_tablet_desktop_template/images/PARENTHEXIS_LOGO_NEW.png">
            </a>
        </div>
        <div class="right">
                <a href="<?php echo JURI::root(false); ?>">
                    <?php echo JText::_('COM_COMMUNITY_LOG_IN'); ?>
                </a>
                <a href="<?php echo JRoute::_('index.php?option=com_community&view=register', false); ?>">
                    <?php echo JText::_('COM_COMMUNITY_FRONT_SIGN_UP'); ?>
                </a>
        </div>
        <div class="center">
            <p class="des1"><?php echo JText::_('COM_COMMUNITY_PARTNERS_BANNER_DESCRITION1'); ?></p>
             <p class="des2"><?php echo JText::_('COM_COMMUNITY_PARTNERS_BANNER_DESCRITION2'); ?></p>
        </div>
    </div>
<?php endif; ?>   
    <div class="partners-content">
        <div class="partner-page-para1">
            <h1><?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA1_CAPTION'); ?></h1>
            <p>
                <?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA1_TEXT'); ?>
            </p>
        </div>
         <div class="clear"></div>
        <div class="partner-page-reap row">
            <h1><?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA2_CAPTION'); ?></h1>
            <div class=" reap-text col-md-4 col-lg-4">
               <img  width="50" height="50" src="<?php echo JURI::root(true); ?>/images/login/par_icon1.png">
               <p class="cap"><?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA2_TEXT1'); ?></p>
               <p class="des_text"> Expand your reach globally. Use
                    your <b >Learning Provider Circle</b> to
                    set yourself apart in PX!<p>

            </div>
            <div class=" reap-text col-md-4 col-lg-4">
                 <img  width="50" height="50" src="<?php echo JURI::root(true); ?>/images/login/par_icon2.png">
               <p class="cap"><?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA2_TEXT2'); ?></p>
                <p class="des_text"> With a
                 <b >low service fee</b> of 10%, get
                    the highest return on your<br>
                    course sale.<p>
            </div>
            <div class="reap-text col-md-4 col-lg-4" >
                <img width="45" height="50" src="<?php echo JURI::root(true); ?>/images/login/par_icon4.png">
                <p class="cap3"><?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA2_TEXT3'); ?></p>
                <p class="des_text">PX is <b >designed with learners and teachers in mind</b>. Reach your
                    learners easily with PX.<p>  
            </div>
            
        </div>
         <div class="clear"></div>
        <div class = "partner-page-resources row">
            <h1><?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA3_CAPTION'); ?></h1>
            <div class="title">
                <p>
                    <?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA3_TITLE'); ?>
                </p>
            </div>
            <div class="res-text col-md-3 col-lg-3">
                 <img width="37" height="44" src="<?php echo JURI::root(true); ?>/images/login/par_icon5.png">
                <p class="cap">BUILD<br/> with user-friendly course template</p>
            </div>
            <div class="res-text col-md-3 col-lg-3">
                 <img width="37" height="44" style="<?php echo $style; ?>" src="<?php echo JURI::root(true); ?>/images/login/par_icon6.png">
                <p class="cap">ADD<br/> quizzes with <br> grading options</p>
            </div>
            <div class="res-text col-md-3 col-lg-3">
                 <img width="50" height="44" style="<?php echo $style1; ?>" src="<?php echo JURI::root(true); ?>/images/login/par_icon3.png">
                <p class="cap">UPLOAD <br/>important documents or assignments</p>
            </div>
               <div class="res-text col-md-3 col-lg-3">
                 <img width="39" height="44" src="<?php echo JURI::root(true); ?>/images/login/par_icon10.png">
                <p class="cap" >PUBLISH<br/> to Store</p>
            </div>
        </div>

        <div class="partner-page-free row">
            <h1><?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA4_CAPTION'); ?></h1>
             <div class="title">
                <p>
                    <?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA4_TITLE'); ?>
                </p>
            </div>
            <div class=" free-text col-md-4 col-lg-4">
               <img  width="36" height="47" src="<?php echo JURI::root(true); ?>/images/login/par_icon7.png">
               <p class="cap"><?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA4_TEXT1'); ?></p>
               <p class="des_text"> Customize with your organizational logo<p>

            </div>
            <div class=" free-text col-md-4 col-lg-4">
                 <img  width="48" height="47" src="<?php echo JURI::root(true); ?>/images/login/par_icon8.png">
               <p class="cap"><?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA4_TEXT2'); ?></p>
                <p class="des_text"> Appoint one or more facilitators to run your course<p>
            </div>
            <div class="free-text col-md-4 col-lg-4" >
                <img width="60" height="47" src="<?php echo JURI::root(true); ?>/images/login/par_icon9.png">
                <p class="cap"><?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA4_TEXT3'); ?></p>
                <p class="des_text">Use our course survey to gather valuable insights<p>  
            </div>
        </div>
    </div>
        <div class="partner-page-how row">
             <h1><?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA5_CAPTION'); ?></h1>
            <?php  
                 $session = JFactory::getSession();
                 $device = $session->get('device');
                 if($device == "mobile")
                    echo '<img  src="'. JURI::root(true).'/images/login/par_chart.png">';
                else{
                   echo '<img  src="'. JURI::root(true).'/images/login/par_image4.png">';
                }

            ?>
             <p><?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA5_NOTE'); ?></p>
        </div>
        <div class="partner-page-success row">
             <h1><?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA7_CAPTION'); ?></h1>
             <p>As a PX Partner, we are committed to work alongside you to make your online academy business a success.
                Find out more in our <span class="capt"><a href="<?php echo JURI::root(true); ?>/support">FAQ</a></span> or email us at <span class="capt"><a href="mailto:contact@parenthexis.com">contact@parenthexis.com</a></span> to connect with a PX Partnerships
                Manager for assistance.</p>

        </div>
        <div class="partner-page-can <?php echo $class; ?>">
            <h1><?php echo JText::_('COM_COMMUNITY_PARTNERS_PARA6_CAPTION'); ?></h1>
            <div class="button-center">
                  <a href="<?php echo JRoute::_('index.php?option=com_community&view=register', false); ?>">
                    <p><?php echo JText::_('COM_COMMUNITY_ABOUT_BANNER_BUTTON'); ?></p>
                </a>
            </div>
        </div>
<?php if ($isGuest) : ?>   
    <div class="connect-with-us <?php echo $class; ?>">
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_CONNECT_WITH_US'); ?></p>
                <div class="connect-icons">
                 <a class="fbook" href="https://www.facebook.com/parenthexis/"><img src="<?php echo JURI::root(true); ?>/images/login/facebook_icon.png"></a>
                <a class="insta" href="https://www.instagram.com/parenthexis/"><img src="<?php echo JURI::root(true); ?>/images/login/instagram_icon.png"></a>
                <a class="email" href="mailto:contact@parenthexis.com"><img src="<?php echo JURI::root(true); ?>/images/login/email_icon.png"></a>
            </div>
        </div>
        <div class="div-footer-menu <?php echo $class; ?>">
            <ul>
                <li><a href="<?php echo JURI::root(true); ?>/about"><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></a>
                </li>
                <li><a href="<?php echo JURI::root(true); ?>/terms"><?php echo JText::_('COM_COMMUNITY_TERMS'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/privacy-policy"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/support"><?php echo JText::_('COM_COMMUNITY_SUPPORT'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/business"><?php echo JText::_('COM_COMMUNITY_BUSINESS'); ?></a>
                </li>              
            </ul>
        </div>
        <div class="divCopyright <?php echo $class; ?>">
            <p>© <?php echo Date("Y", time());?> PARENTHEXIS</p>
        </div>
<?php endif; ?>
</div>
