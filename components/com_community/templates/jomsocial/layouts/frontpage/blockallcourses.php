<?php // no direct access
defined('_JEXEC') or die('Restricted access');

$session = JFactory::getSession();
$device = $session->get('device');
$first_time_login = $session->get('first_time');
$itemid = JoomdleHelperContent::getMenuItem();

$class = ($device != 'mobile') ? 'desktop-tablet' : '';
$idstr = ($device != 'mobile') ? '' : 'id="mobile-show"';

$linkto = $params->get( 'linkto' , 'moodle');

$moodle_xmlrpc_server_url = $params->get( 'MOODLE_URL' ).'/mnet/xmlrpc/server.php';
$moodle_auth_land_url = $params->get( 'MOODLE_URL' ).'/auth/joomdle/land.php';
$linkstarget = $params->get( 'linkstarget' );
$default_itemid = $params->get( 'default_itemid' );
$joomdle_itemid = $params->get( 'joomdle_itemid' );
$courseview_itemid = $params->get( 'courseview_itemid' );
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$show_unenrol_link = $params->get( 'show_unenrol_link' );

if ($linkto == 'moodle') {
    if ($default_itemid)
        $itemid = $default_itemid;
    else
        $itemid = JoomdleHelperContent::getMenuItem();
} else if ($linkto == 'detail') {
    $itemid = JoomdleHelperContent::getMenuItem();

    if ($joomdle_itemid)
        $itemid = $joomdle_itemid;
} else {
    $itemid = JoomdleHelperContent::getMenuItem();

    if ($joomdle_itemid)
        $itemid = $joomdle_itemid;
    if ($courseview_itemid)
        $itemid = $courseview_itemid;
}

$open_in_wrapper = ($linkstarget == 'wrapper') ? 1 : 0;

$target = ($linkstarget == "new") ? " target='_blank'" : "";

$lang = JoomdleHelperContent::get_lang();
$prev_cat = 0;

$count = 0;
?>
<div class="my-course <?php echo $class . ' ' . $device; echo ($first_time_login || $noCourseFound) ? ' first-time-login' : ' have-courses '; ?>">
    <div class="course-title-block">
        <span class="title-text" href="mylearning/list.html"><?php echo JText::_("MOD_BLOCK_TITLE_MY_LEARNING"); ?></span>
        <?php
        if (count($cursos) != 0) {
            ?>
            <span class=""> <?php echo count($cursos);?></span>
        <?php }
        ?>
    </div>
    <div class="course-content-block <?php echo $class; ?>">
        <?php if (!$noCourseFound) { ?>

            <div class='swiper-container'>
                <div class='swiper-wrapper'>
                    <?php
                    $group_by_category = $params->get('group_by_category');

                    if (is_array($cursos)) {
                        $tempProgArr = array();
                        foreach ($cursos as $ids => $curso) {

                            if (empty($curso['id'])) {
                                $id = $curso['remoteid'];
                            } else {
                                $id = $curso['id'];
                            }

                            if ($group_by_category) {
                                // Group by category
                                if ($curso['category'] != $prev_cat) :
                                    $prev_cat = $curso['category'];
                                    $cat_name = $curso['cat_name'];
                                    ?>
                                    <h4>
                                        <?php echo $cat_name; ?>
                                    </h4>
                                <?php
                                endif;
                            }

                            if ($linkto == 'moodle') {
                                if (($curso['coursetype'] == 'program') && (array_key_exists($curso['cat_id'], $tempProgArr))) {
                                    continue;
                                }
                                $count++;
                                if (empty($curso['id'])) {
                                    $id = $curso['remoteid'];
                                } else {
                                    $id = $curso['id'];
                                }
                                $fname = JHtml::_('string.truncate', strip_tags($curso['fullname']), 20, true, $allowHtml = true);
                                $des = $curso['summary'];

                                if (strlen($des) > 30) {
                                    if (strpos($des, ' ', 30) === false) {
                                        $des = JHtml::_('string.truncate', strip_tags($curso['summary']), 30, true, $allowHtml = true);
                                    } else {
                                        $des = JHtml::_('string.truncate', strip_tags($curso['summary']), 30, true, $allowHtml = true);
                                    }
                                }

                                if ($curso['coursetype'] == 'program') {
                                    $programid = $curso['cat_id'];
                                    $redirect_url = 'courseprogram/' . $programid . '.html';
                                    $tempProgArr[$programid] = $programid;
                                } else {
                                    $redirect_url = 'course/' . $id . '.html';
                                }

                                $course_image = $curso['filepath'] . $curso['filename'];
                                $linkText = ($first_time_login || $noCourseFound) ? JText::_("MOD_LINK_LEARN_MORE") : JText::_("CONTINUE");
                                if ($curso['coursetype'] == 'program') $linkText = JText::_("DETAILS");
                                ?>
                                <div class='swiper-slide <?php echo $curso['coursetype']; ?>' style='width: 100%;'>
                                    <div class='box_learning course-box' style='width: 100%; height: 100%'>
                                        <div class='course_content'>
                                            <div class='clearfix'></div>
                                            <div class='mylearning-img'>
                                                <div class="left lazyload" id="images" data-src="<?php echo $course_image; ?>"></div>
                                            </div>
                                            <div class='content_c'>
                                                <div class='clearfix'></div>

                                                <a class="course-title" <?php echo $target; ?> href="<?php echo $redirect_url; ?>"><?php echo $fname; ?></a>
                                                <p class="course-description" ><?php echo $des; ?></p>
                                            </div>
                                            <div class='clearfix'></div>
                                        </div>
                                    </div>
                                </div>

                            <?php } else if ($linkto == 'detail') {
                                // Link to detail view
                                $redirect_url = JRoute::_("index.php?option=com_joomdle&view=detail&course_id=" . $curso['id'] . ':' . JFilterOutput::stringURLSafe($curso['fullname']) . "&Itemid=$itemid");
                                echo "<li><img kvsrc='' /><a href=\"" . $redirect_url . "\">" . $curso['fullname'] . "</a></li>";
                            } else {
                                // Link to course view
                                $redirect_url = JRoute::_("index.php?option=com_joomdle&view=course&course_id=" . $curso['id'] . ':' . JFilterOutput::stringURLSafe($curso['fullname']) . "&Itemid=$itemid");
                                echo "<li><img kvsrc='' /><a href=\"" . $redirect_url . "\">" . $curso['fullname'] . "</a></li>";
                            }

                            if ($show_unenrol_link) {
                                if ($curso['can_unenrol']) {
                                    $redirect_url = "index.php?option=com_joomdle&view=course&task=unenrol&course_id=" . $curso['id'];
                                    echo "<a href=\"" . $redirect_url . "\"> (" . JText::_('COM_JOOMDLE_UNENROL') . ")</a>";
                                }
                            }

                        }
                    }

                    ?>
                </div>
                <div class="swiper-button-next2"><i class="bg-second-img icon-next-white-shadow"></i></div>
                <div class="swiper-button-prev2"><i class="bg-second-img icon-prev-white-shadow"></i></div>
            </div>

        <?php } else {
            JFactory::getDocument()->setTitle(
                JText::sprintf('JFRONTPAGE_TITLE', JFactory::getUser()->name)
            );
            ?>

            <div class="ft-browse-courses">
                <div class="empty-course-icon">
                    <img src="/images/courseicon_hd.png">
                </div>
                <div class="empty-course-description">
                    <span><?php echo JText::_("JFT_BROWSE_COURSES_DES"); ?></span>
                </div>
                <div class="empty-course-action">
                    <?php
                    if (strcmp((JFactory::getLanguage()->getTag()), 'vi-VN') == 0) { ?>
                        <a href="#"
                           class="empty-course-search"><?php echo JText::_("MOD_LINK_BROWSE_COURSE"); ?></a>
                    <?php }
                    ?>
                    <?php
                    if (strcmp((JFactory::getLanguage()->getTag()), 'en-GB') == 0) {
                        ?>
                        <a href="#"
                           class="empty-course-search"><?php echo JText::_("MOD_LINK_BROWSE_COURSE"); ?></a>
                    <?php }
                    ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php
if ($device == 'mobile') {
    $slideview = '1.1';
    $space = '10';
//            $deviceWidth = '50';
} else if ($device == 'tablet') {
    $slideview = '2.5';
    $space = '15';
//            $deviceWidth = '568';
} else {
    $slideview = '3';
    $space = '15';
//            $deviceWidth = '400';
}
?>
<script type="text/javascript">
    (function ($) {
        $('img').each(function () {
            $(this).attr('src', $(this).attr('kvsrc'));
        });

        var x = screen.width;
        var a = 0;

        if(x < 390){
            a = 1.1;
        }
        if(x >= 390 && x < 481){
            a = 1.3;
        }
        if(x>=481 && x<=736){
            a = 1.5;
        }
        if(x>736 && x<1024){
            a = 2.5;
        }
        if(x>=1024 && x<=1366){
            a = 3;
        }
        if(x>1366 && x <=1800){
            a = 4;
        }
        if(x>1800){
            a = 5;
        }

        var swiper1 = new Swiper('.my-course .swiper-container', {
            direction: 'horizontal',
            autoplayDisableOnInteraction: true,
            spaceBetween: 30,
            nextButton: '.swiper-button-next2',
            prevButton: '.swiper-button-prev2',
            touchMoveStopPropagation: false,
            preventClicks: false,
            allowTouchMove : false
        });
    })(jQuery);

</script>