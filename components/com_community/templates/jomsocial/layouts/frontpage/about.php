<?php
defined('_JEXEC') or die();
$isGuest = (JFactory::getUser()->guest == 1) ? true : false;
$jinput = JFactory::getApplication()->input;
$appview = $jinput->get('type', '', 'GET');
$col = 2;
if($appview == 'appview') {
    $class = 'appview';
    $col = 4;
}
?>
<div class="about-page">
    <?php if ($isGuest) :
        $check_is_guest = "guest";
        ?>
        <style type="text/css">
            .mob.view-frontpage.task-about .home {
                padding-bottom: 25px;
            }
        </style>
        <div class="about-page-banner <?php echo $class; ?>">
            <div class="left">
                <a href="<?php echo JURI::root(false); ?>">
                    <img alt="Parenthexis"
                         src="templates/t3_bs3_tablet_desktop_template/images/PARENTHEXIS_LOGO_NEW.png">
                </a>
            </div>
            <div class="right">
                <a href="<?php echo JURI::root(true); ?>/user-story">
                    <?php echo JText::_('COM_COMMUNITY_USER_STORIES'); ?>
                </a>
                <a href="<?php echo JURI::root(false); ?>">
                    <?php echo JText::_('COM_COMMUNITY_LOG_IN'); ?>
                </a>
                <a href="<?php echo JRoute::_('index.php?option=com_community&view=register', false); ?>">
                    <?php echo JText::_('COM_COMMUNITY_FRONT_SIGN_UP'); ?>
                </a>
            </div>
            <div class="center">
                <p><?php echo JText::_('COM_COMMUNITY_ABOUT_BANNER_DESCRITION1'); ?></p>
                <p><?php echo JText::_('COM_COMMUNITY_ABOUT_BANNER_DESCRITION2'); ?></p>
            </div>
            <div class="button-center">
                <a href="<?php echo JRoute::_('index.php?option=com_community&view=register', false); ?>">
                    <p><?php echo JText::_('COM_COMMUNITY_ABOUT_BANNER_BUTTON'); ?></p>
                </a>
            </div>
        </div>
    <?php endif; ?>
    <div class="about-page-content <?php echo $check_is_guest; ?>">
        <h1 class="title">
            <?php echo JText::_('COM_COMMUNITY_ABOUT_PARENTHEXIS'); ?>
        </h1>
        <div class="paragraph">
            <div class="sub-paragraph">
                <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT1'); ?></p>
                <span class="dot">•</span><p class="pdot"><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT2'); ?></p>
                <span class="dot">•</span><p class="pdot"><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT3'); ?></p>
                <span class="dot">•</span><p class="pdot"><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT4'); ?></p>
                <span class="dot">•</span><p class="pdot"><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT5'); ?></p>
            </div>
        </div>
        <div class="paragraph">
            <div class="sub-paragraph">
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_ABOUT_SUB_PARAGRAPH_TITLE_A'); ?></p>
                <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT6'); ?></p>
                <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT7'); ?></p>
                <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT8'); ?></p>
                <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT9'); ?></p>
            </div>
        </div>
        <p class="paragraph-title"><?php echo JText::_('COM_COMMUNITY_ABOUT_PARAGRAPH_TITLE_A'); ?></p>
        <div class="background-step paragraph">
            <div class="content-step container-fluid">
            <!--            step 1-->
                <div class="step_block row">
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        <div class="step">
                            <span>1</span>
                        </div>
                    </div>
                    <div class="step-title col-xs-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-md-<?php echo $col; ?> col-lg-<?php echo $col; ?>">
                        <div class="text">
                            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT25_1'); ?></p>
                            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT25_2'); ?></p>
                        </div>
                        <?php if($appview == 'appview') { ?>
                        <div class="image">
                            <img class="icon_step1" alt="Parenthexis" src="images/login/about_step1.png">
                        </div>
                        <?php } ?>
                    </div>
                    <?php if(!isset($appview)) { ?>
                    <div class="step-icon col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <img class="icon_step1" alt="Parenthexis" src="images/login/about_step1.png">
                    </div>
                    <?php } ?>
                    <p class="step-des col-xs-7 col-sm-7 col-md-7 col-lg-7"><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT10'); ?></p>
                </div>
                <div class="line"></div>
                <!--            step 2-->
                <div class="step_block row">
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        <div class="step">
                            <span>2</span>
                        </div>
                    </div>
                    <div class="step-title col-xs-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-md-<?php echo $col; ?> col-lg-<?php echo $col; ?>">
                        <div class="text">
                            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT26_1'); ?></p>
                            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT26_2'); ?></p>
                        </div>
                        <?php if($appview == 'appview') { ?>
                        <div class="image">
                            <img class="icon_step2" alt="Parenthexis" src="images/login/about_step2.png">
                        </div>
                        <?php } ?>
                    </div>
                    <?php if(!isset($appview)) { ?>
                    <div class="step-icon col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <img class="icon_step2" alt="Parenthexis" src="images/login/about_step2.png">
                    </div>
                    <?php } ?>
                    <p class="step-des col-xs-7 col-sm-7 col-md-7 col-lg-7"><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT11'); ?></p>
                </div>
                <div class="line"></div>
                <!--            step 3-->
                <div class="step_block row">
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        <div class="step">
                            <span>3</span>
                        </div>
                    </div>
                    <div class="step-title col-xs-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-md-<?php echo $col; ?> col-lg-<?php echo $col; ?>">
                        <div class="text">
                            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT27_1'); ?></p>
                            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT27_2'); ?></p>
                        </div>
                        <?php if($appview == 'appview') { ?>
                        <div class="image">
                            <img class="icon_step3" alt="Parenthexis" src="images/login/about_step3.png">
                        </div>
                        <?php } ?>
                    </div>
                    <?php if(!isset($appview)) { ?>
                    <div class="step-icon col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <img class="icon_step3" alt="Parenthexis" src="images/login/about_step3.png">
                    </div>
                    <?php } ?>
                    <p class="step-des col-xs-7 col-sm-7 col-md-7 col-lg-7"><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT12'); ?></p>
                </div>
                <div class="line"></div>
                <!--            step 4-->
                <div class="step_block row">
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        <div class="step">
                            <span>4</span>
                        </div>
                    </div>
                    <div class="step-title col-xs-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-md-<?php echo $col; ?> col-lg-<?php echo $col; ?>">
                        <div class="text">
                            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT28_1'); ?></p>
                            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT28_2'); ?></p>
                        </div>
                        <?php if($appview == 'appview') { ?>
                        <div class="image">
                            <img class="icon_step4" alt="Parenthexis" src="images/login/about_step4.png">
                        </div>
                        <?php } ?>
                    </div>
                    <?php if(!isset($appview)) { ?>
                    <div class="step-icon col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <img class="icon_step4" alt="Parenthexis" src="images/login/about_step4.png">
                    </div>
                    <?php } ?>
                    <p class="step-des col-xs-7 col-sm-7 col-md-7 col-lg-7"><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT13'); ?></p>
                </div>
                <div class="line"></div>
                <!--            step 5-->
                <div class="step_block row">
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        <div class="step">
                            <span>5</span>
                        </div>
                    </div>
                    <div class="step-title col-xs-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-md-<?php echo $col; ?> col-lg-<?php echo $col; ?>">
                        <div class="text">
                            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT29_1'); ?></p>
                            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT29_2'); ?></p>
                            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT29_3'); ?></p>
                        </div>
                        <?php if($appview == 'appview') { ?>
                        <div class="image">
                            <img class="icon_step5" alt="Parenthexis" src="images/login/about_step5.png">
                        </div>
                        <?php } ?>
                    </div>
                    <?php if(!isset($appview)) { ?>
                    <div class="step-icon col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <img class="icon_step5" alt="Parenthexis" src="images/login/about_step5.png">
                    </div>
                    <?php } ?>
                    <p class="step-des col-xs-7 col-sm-7 col-md-7 col-lg-7"><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT14'); ?></p>
                </div>
            </div>
        </div>
        <div class="paragraph">
            <p class="paragraph-title"><?php echo JText::_('COM_COMMUNITY_ABOUT_PARAGRAPH_TITLE_B'); ?></p>
            <div class="sub-paragraph">
                <div class="feature_highl_block">
                    <img class="circle-and-chat" alt="Circles and Chats" src="images/login/about_feature1.png">
                    <p><?php echo JText::_('COM_COMMUNITY_ABOUT_CIRCLE_AND_CHAT'); ?></p>
                    <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT15'); ?></p>

                </div>
                <div class="feature_highl_block">
                    <img class="leanrning-network" alt="Learning Networks" src="images/login/about_feature2.png">
                    <p><?php echo JText::_('COM_COMMUNITY_ABOUT_LEARNING_NETWORK'); ?></p>
                    <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT16'); ?></p>
                </div>
                <div class="feature_highl_block">
                    <img class="course-builder" alt="Course Builder" src="images/login/about_feature3.png">
                    <p><?php echo JText::_('COM_COMMUNITY_ABOUT_COURSE_BUILDER'); ?></p>
                    <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT17'); ?></p>
                </div>
                <div class="feature_highl_block">
                    <img class="learning-store" alt="Learning Store" src="images/login/about_feature4.png">
                    <p><?php echo JText::_('COM_COMMUNITY_ABOUT_LEARNING_STORE'); ?></p>
                    <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT18'); ?></p>
                </div>
            </div>
        </div>
        <div class="paragraph container-fluid">
            <p class="paragraph-title row"><?php echo JText::_('COM_COMMUNITY_ABOUT_PARAGRAPH_TITLE_C'); ?></p>
            <div class="sub-paragraph user_comment col-md-5 col-lg-5">
                <img class="quotation_marks1" alt="quotation_marks" src="images/login/about_quotation_marks.png">
                <div class="background-comment"><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT19'); ?></div>
                <img class="quotation_marks2" alt="quotation_marks" src="images/login/about_quotation_marks1.png">
                    <p class="author"><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT20'); ?></p>
            </div>
            <div class="sub-paragraph user_comment col-md-5 col-lg-5 col-md-offset-2 col-lg-offset-2">
                <img class="quotation_marks1" alt="quotation_marks" src="images/login/about_quotation_marks.png">
                <div class="background-comment"><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT21'); ?></div>
                <img class="quotation_marks2" alt="quotation_marks" src="images/login/about_quotation_marks1.png">
                <p class="author"><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT22'); ?></p>
            </div>
        </div>
    </div>
    <div class="about-page-learn-more">
        <div class="paragraph learn-more1">
            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT30'); ?></p>
            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT31_1'); ?></p>
<!--             <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT31_2'); ?></p>
 -->            <div class="bt-learn-more">
                <a href="<?php echo JURI::root(true); ?>/partners"> <span>Learn More</span></a>
            </div>
        </div>
        <div class="paragraph learn-more2">
            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT32'); ?></p>
            <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT33_1'); ?></p>
<!--             <p><?php echo JText::_('COM_COMMUNITY_ABOUT_TEXT33_2'); ?></p>
 -->            <div class="bt-learn-more">
                <a href="<?php echo JURI::root(true); ?>/business"> <span>Learn More</span></a>
            </div>
        </div>
    </div>

    <?php if ($isGuest) : ?>
        <div class="connect-with-us <?php echo $class; ?>">
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_CONNECT_WITH_US'); ?></p>
            <div class="connect-icons">
                <a class="fbook" href="https://www.facebook.com/parenthexis/"><img src="<?php echo JURI::root(true); ?>/images/login/facebook_icon.png"></a>
                <a class="insta" href="https://www.instagram.com/parenthexis/"><img src="<?php echo JURI::root(true); ?>/images/login/instagram_icon.png"></a>
                <a class="email" href="mailto:contact@parenthexis.com"><img src="<?php echo JURI::root(true); ?>/images/login/email_icon.png"></a>
            </div>
        </div>
        <div class="div-footer-menu <?php echo $class; ?>">
            <ul>
                <li><a href="<?php echo JURI::root(true); ?>/about"><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></a>
                </li>
                <li><a href="<?php echo JURI::root(true); ?>/terms"><?php echo JText::_('COM_COMMUNITY_TERMS'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/privacy-policy"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/support"><?php echo JText::_('COM_COMMUNITY_SUPPORT'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/business"><?php echo JText::_('COM_COMMUNITY_BUSINESS'); ?></a>
                </li>
                <!-- <li><a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a></li>
<!--        <li>-->
<!--        <a href="--><?php //echo JURI::root(true); ?><!----><?php //echo JText::_('COM_COMMUNITY_BUSINESS'); ?><!--</a>-->
            </ul>
        </div>
        <div class="divCopyright <?php echo $class; ?>">
            <p>© <?php echo Date("Y", time());?> PARENTHEXIS</p>
        </div>
    <?php endif; ?>
</div>