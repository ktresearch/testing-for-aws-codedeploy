<?php
defined('_JEXEC') or die();
$isGuest = (JFactory::getUser()->guest == 1) ? true : false;
?>
<div class="privacy-policy-page">
    <?php if ($isGuest) :?>
        <style type="text/css">
            .mob.view-frontpage.task-privacy-policy .home {
                padding-bottom: 25px;
            }
        </style>
        <div class="privacy-policy-page-banner">
            <div class="left">
                <a href="<?php echo JURI::root( false );?>">
                    <img alt="Parenthexis" src="templates/t3_bs3_tablet_desktop_template/images/PARENTHEXIS_LOGO_NEW.png">
                </a>
            </div>
            <div class="right">
                <a href="<?php echo JURI::root( false );?>">
                    <?php echo JText::_('COM_COMMUNITY_LOG_IN'); ?>
                </a>
                <a href="<?php echo JRoute::_('index.php?option=com_community&view=register', false); ?>">
                    <?php echo JText::_('COM_COMMUNITY_FRONT_SIGN_UP'); ?>
                </a>
            </div>
        </div>
    <?php endif; ?>
    <div class="privacy-policy-page-content">
        <h1 class="title">
            <?php echo JText::_('COM_COMMUNITY_SUPPORT');?>
        </h1>

        <div class="paragraph">
            <div class="sub-paragraph">
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_SUB_PARAGRAPH_TITLE_A');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT1');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT2');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT3');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_SUB_PARAGRAPH_TITLE_B');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT4');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT5');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT6');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT7');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT8');?></p>
            </div>

            <div class="sub-paragraph">
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_SUB_PARAGRAPH_TITLE_C');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT9');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT10');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT11');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT12');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT13');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT14');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT15');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT16');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT17');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT18');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT19');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT20');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT21');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT22');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT23');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_SUB_PARAGRAPH_TITLE_D');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT24');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT25');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT26');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT27');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT28');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT29');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT30');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT31');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT32');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT33');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT34');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT35');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT36');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT37');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT38');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT39');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT40');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT41');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT42');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT43');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_SUB_PARAGRAPH_TITLE_E');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT44');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT45');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT46');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT47');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT48');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT49');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT50');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_SUB_PARAGRAPH_TITLE_F');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT51');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT52');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT53');?></p>
                <p><span class="dot">•</span>  <?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT54');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT55');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT56');?></p>
            </div>
            <div class="sub-paragraph">
                <p class="sub-paragraph-title"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_SUB_PARAGRAPH_TITLE_G');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT57');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT58');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT59');?></p>
                <p><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY_TEXT60');?></p>

            </div>
        </div>
    </div>

    <?php if ($isGuest) :?>
        <div class="connect-with-us">
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_CONNECT_WITH_US');?></p>
            <div class="connect-icons">
                <a class="fbook" href=""><img src="<?php echo JURI::root(true); ?>/images/login/facebook_icon.png"></a>
                <a class="insta" href=""><img src="<?php echo JURI::root(true); ?>/images/login/instagram_icon.png"></a>
                <a class="email" href=""><img src="<?php echo JURI::root(true); ?>/images/login/email_icon.png"></a>
            </div>
        </div>
        <div class="div-footer-menu">
            <ul>
                <li><a href="<?php echo JURI::root(true); ?>/about"><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></a></li>
                <li><a href="<?php echo JURI::root(true); ?>/terms"><?php echo JText::_('COM_COMMUNITY_TERMS'); ?></a></li>
                <li><a href="<?php echo JURI::root(true); ?>/privacy-policy"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY'); ?></a></li>
                <li><a href="<?php echo JURI::root(true); ?>/support"><?php echo JText::_('COM_COMMUNITY_SUPPORT'); ?></a></li>
                <!-- <li><a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/business"><?php echo JText::_('COM_COMMUNITY_BUSINESS'); ?></a></li> -->
            </ul>
        </div>
        <div class="divCopyright">
            <p>© <?php echo Date("Y", time());?> PARENTHEXIS</p>
        </div>
    <?php endif; ?>
</div>