<?php
defined('_JEXEC') or die();
$isGuest = (JFactory::getUser()->guest == 1) ? true : false;
$jinput = JFactory::getApplication()->input;
$appview = $jinput->get('type', '', 'GET');
if($appview == 'appview') {
    $class = 'appview';
}
?>
<div class="business-page">
    <?php if ($isGuest) :
        $check_is_guest = "guest";
        ?>
        <style type="text/css">
            .mob.view-frontpage.task-business .home {
                padding-bottom: 25px;
            }
        </style>
        <div class="business-page-banner <?php echo $class; ?>">
            <div class="left">
                <a href="<?php echo JURI::root(false); ?>">
                    <img alt="Parenthexis"
                         src="templates/t3_bs3_tablet_desktop_template/images/PARENTHEXIS_LOGO_NEW.png">
                </a>
            </div>
            <div class="right">
                <a href="<?php echo JURI::root(false); ?>">
                    <?php echo JText::_('COM_COMMUNITY_LOG_IN'); ?>
                </a>
                <a href="<?php echo JRoute::_('index.php?option=com_community&view=register', false); ?>">
                    <?php echo JText::_('COM_COMMUNITY_FRONT_SIGN_UP'); ?>
                </a>
            </div>
            <div class="center">
                <p><?php echo JText::_('COM_COMMUNITY_BUSINESS_BANNER_DESCRITION1'); ?></p>
                <p class="des2"><?php echo JText::_('COM_COMMUNITY_BUSINESS_BANNER_DESCRITION2'); ?></p>
            </div>
        </div>
    <?php endif; ?>
    <div class="business-page-content <?php echo $check_is_guest; ?>">
        <h1 class="title">
            <?php echo JText::_('COM_COMMUNITY_BUSINESS_PARENTHEXIS'); ?>
        </h1>
        <div class="paragraph">
            <div class="sub-paragraph">
                <p><?php echo JText::_('COM_COMMUNITY_BUSINESS_TEXT1'); ?></p>
                <p><?php echo JText::_('COM_COMMUNITY_BUSINESS_TEXT2_A').' <a href="mailto:contact@parenthexis.com">contact@parenthexis.com</a> '.JText::_('COM_COMMUNITY_BUSINESS_TEXT2_B'); ?></p>
            </div>
        </div>

        <table class="business-table" width="100%">
            <tr>
                <th><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_TITLE1'); ?></span></th>
                <th><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_TITLE2'); ?></span></th>
                <th><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_TITLE3'); ?></span></th>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW1_1'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW1_2'); ?></span></td>
                <td><p><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW1_31'); ?></p>
                    <p><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW1_32'); ?></p></td>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW2_1'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_YES'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_YES'); ?></span></td>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW3_1'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_YES'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_YES'); ?></span></td>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW4_1'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_YES'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_YES'); ?></span></td>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW5_1'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_YES'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_YES'); ?></span></td>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW6_1'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_YES'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_NO'); ?></span></td>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW7_1'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_YES'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_NO'); ?></span></td>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW8_1'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW8_2'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW8_3'); ?></span></td>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW9_1'); ?></span></td>
                <td><p><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_YES'); ?></p>
                    <p class="note"><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW9_2'); ?></p></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW9_3'); ?></span></td>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW10_1'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_YES'); ?></span></td>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW_YES'); ?></span></td>
            </tr>
            <tr>
                <td><p><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW11_1'); ?></p></td>
                <td colspan="2"><p><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW11_2'); ?></p><p><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW11_3'); ?></p></td>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW12_1'); ?></span></td>
                <td colspan="2"><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW12_2'); ?></span></td>
            </tr>
            <tr>
                <td><span><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW13_1'); ?></span></td>
                <td colspan="2"><p><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW11_2'); ?></p><p><?php echo JText::_('COM_COMMUNITY_BUSINESS_TABLE_ROW11_3'); ?></p></td>
            </tr>
        </table>

    </div>

    <?php if ($isGuest) : ?>
        <div class="connect-with-us <?php echo $class; ?>">
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_CONNECT_WITH_US'); ?></p>
            <div class="connect-icons">
                <a class="fbook" href="https://www.facebook.com/parenthexis/"><img src="<?php echo JURI::root(true); ?>/images/login/facebook_icon.png"></a>
                <a class="insta" href="https://www.instagram.com/parenthexis/"><img src="<?php echo JURI::root(true); ?>/images/login/instagram_icon.png"></a>
                <a class="email" href="mailto:contact@parenthexis.com"><img src="<?php echo JURI::root(true); ?>/images/login/email_icon.png"></a>
            </div>
        </div>
        <div class="div-footer-menu <?php echo $class; ?>">
            <ul>
                <li><a href="<?php echo JURI::root(true); ?>/about"><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></a>
                </li>
                <li><a href="<?php echo JURI::root(true); ?>/terms"><?php echo JText::_('COM_COMMUNITY_TERMS'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/privacy-policy"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/support"><?php echo JText::_('COM_COMMUNITY_SUPPORT'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a>
                </li>
                <li>
                    <a href="<?php echo JURI::root(true); ?>/business"><?php echo JText::_('COM_COMMUNITY_BUSINESS'); ?></a>
                </li>
                <!-- <li><a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a></li>
<!--        <li>-->
                <!--        <a href="--><?php //echo JURI::root(true); ?><!----><?php //echo JText::_('COM_COMMUNITY_BUSINESS'); ?><!--</a>-->
            </ul>
        </div>
        <div class="divCopyright <?php echo $class; ?>">
            <p>© <?php echo Date("Y", time());?> PARENTHEXIS</p>
        </div>
    <?php endif; ?>
</div>