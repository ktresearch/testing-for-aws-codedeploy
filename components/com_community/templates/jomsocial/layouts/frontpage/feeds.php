<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();

$defaultFilter = $my->_cparams->get('frontpageactivitydefault');
$document = JFactory::getDocument();
$document->addStyleSheet(JURI::root(true) . '/components/com_community/assets/release/css/feeds.css');

$session = JFactory::getSession();
$device = $session->get('device');
$is_mobile = $device == 'mobile' ? true : false;

echo $header;

if ($alreadyLogin > 0) {
?>
<script type="text/javascript">joms.filters && joms.filters.bind();</script>

<div class="joms-body">
    <div class="col-sm-9 col-md-9 col-lg-9 homepage left-part">
        <div class="leftside">
        <?php if ($config->get('showactivitystream') == '1' || ($config->get('showactivitystream') == '2' && $my->id != 0 )) { ?>
            <?php ($my->id) ? $userstatus->render() : ''; ?>
            <div class="div-list-activities">
                <?php
                if ($countUserActivities <= 5)
                    echo $userActivities;
                else {
                ?>
                <div class="list-activities" style="display: none;"></div>
                <div class="placeholder-loading">
                            <div class="ph-item">
                                <div class="ph-col-2">
                                    <div class="ph-avatar"></div>
                                </div>
                                <div class="ph-col-10" style="justify-content: center">
                                    <div class="ph-row">
                                        <div class="ph-col-10 big "></div>
                                        <div class="ph-col-2 big empty"></div>
                                        <div class="ph-col-4"></div>
                                        <div class="ph-col-8 empty"></div>
                                    </div>
                                </div>
                                <div class="ph-col-12">
                                    <div class="ph-row">
                                        <div class="ph-col-4"></div>
                                        <div class="ph-col-8 empty"></div>
                                        <div class="ph-col-6"></div>
                                        <div class="ph-col-6 empty"></div>
                                        <div class="ph-col-12"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="ph-item">
                                <div class="ph-col-2">
                                    <div class="ph-avatar"></div>
                                </div>
                                <div class="ph-col-10" style="justify-content: center">
                                    <div class="ph-row">
                                        <div class="ph-col-10 big "></div>
                                        <div class="ph-col-2 big empty"></div>
                                        <div class="ph-col-4"></div>
                                        <div class="ph-col-8 empty"></div>
                                        <div class="ph-col-12"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="ph-item">
                                <div class="ph-col-2">
                                    <div class="ph-avatar"></div>
                                </div>
                                <div class="ph-col-10" style="justify-content: center">
                                    <div class="ph-row">
                                        <div class="ph-col-10 big "></div>
                                        <div class="ph-col-2 big empty"></div>
                                        <div class="ph-col-4"></div>
                                        <div class="ph-col-8 empty"></div>
                                    </div>
                                </div>
                                <div class="ph-col-12">
                                    <div class="ph-row">
                                        <div class="ph-col-4"></div>
                                        <div class="ph-col-8 empty"></div>
                                        <div class="ph-col-6"></div>
                                        <div class="ph-col-6 empty"></div>
                                        <div class="ph-col-12"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="ph-item">
                                <div class="ph-col-2">
                                    <div class="ph-avatar"></div>
                                </div>
                                <div class="ph-col-10" style="justify-content: center">
                                    <div class="ph-row">
                                        <div class="ph-col-10 big "></div>
                                        <div class="ph-col-2 big empty"></div>
                                        <div class="ph-col-4"></div>
                                        <div class="ph-col-8 empty"></div>
                                        <div class="ph-col-12"></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                <script>
                    jQuery(document).ready(function() {
                        jQuery.ajax({
                            url: '<?php echo JUri::base(). 'index.php?option=com_community&view=frontpage&task=ajaxGetLatestActivities' ?>',
                            type: 'post',
                            success: function(res) {
                                res = JSON.parse(res);
                                if (res.success) {
                                    jQuery('.div-list-activities .placeholder-loading').fadeOut(0, function() {
                                        jQuery('.div-list-activities .list-activities').html(res.html.HTML).show();
                                    });
                                }
                            }
                        });
                                    
                        jQuery(document).on('click', '.joms-button--options', function() {
                            jQuery(this).next('.joms-dropdown').toggle();
                        });
                    });
                </script>
                <?php } ?>
            </div>
        <?php } ?>
        </div>
    </div>
    <?php if (!$is_mobile) { ?>
    <div class="col-sm-3 col-md-3 col-lg-3 col-xs-3 right-part">
        <div style="position:absolute;">
            <div class="banner">
                <?php echo $mycircle; ?>
            </div>
            <div class="banner">
                <?php echo $mycourse; ?>
            </div>
            <?php
            $disabledRightBottomAds = true;
            if (!$disabledRightBottomAds) { ?>
                <div class="banner">
                    <img class="banner-image" style="display:none;" src="<?php echo JUri::base() . 'images/banners/' . $banner2['filename']; ?>">
                </div>
            <?php } ?>
        </div>
    </div>
    <?php } ?>
</div>

<?php } ?>
<script>
    joms_filter_params = <?php echo json_encode(
        array(
            'filter'  => isset($filter) ? $filter : '',
            'value'   => isset($filterValue) ? $filterValue : '',
            'hashtag' => isset($filterHashtag) ? $filterHashtag : ''
        )
    ) ?>;
</script>

<?php if ($filterHashtag) { ?>
<script>joms_filter_hashtag = true;</script>
<?php } else if ($filterKeyword) { ?>
<script>joms_filter_keyword = true;</script>
<?php } ?>
<script>
    jQuery(document).ready(function() {
        jQuery('.banner-image').one("load", function() {
            jQuery(this).fadeIn();
        }).each(function() {
            console.log(jQuery(this));
            if(this.complete) jQuery(this).load();
        });
        jQuery('.empty-search-circle').click(function() {
            jQuery('#toggle-search').click();
            jQuery('#searchboxcircle').click();
            jQuery('input[id="txtKeyword"]').focus();
            jQuery('#searchlearning').hide();
            jQuery('#searchfriend').hide();
            jQuery('.navbar-notification').addClass('hide');
        });
        jQuery('.empty-course-search').click(function() {
            jQuery('#toggle-search').click();
            jQuery('#searchboxlearning').click();
            jQuery('input.search-text').focus();
            jQuery('#searchcircle').hide();
            jQuery('#searchfriend').hide();
            jQuery('.navbar-notification').addClass('hide');
        });
        jQuery('.empty-search-friend').click(function() {
            jQuery('#toggle-search').click();
            jQuery('#searchboxfriend').click();
            jQuery('input.search-text').focus();
            jQuery('#searchcircle').hide();
            jQuery('#searchlearning').hide();
            jQuery('.navbar-notification').addClass('hide');
        });
    });
</script>