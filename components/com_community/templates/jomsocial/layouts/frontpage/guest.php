<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();
$session = JFactory::getSession();
$device = $session->get('device');
?>
<!--<meta name="google-signin-client_id" content="217112268561-j1tp7iie9uoihhgc9avk9mima09fnqkr.apps.googleusercontent.com">-->
<style type="text/css">
    body {
        margin: 0;
        padding: 0;
    }
</style>
<div class="notification"></div>
<div class="joms-landing <?php if (isset($settings) && !$settings['general']['enable-frontpage-image']) echo "no-image"; ?>">
    <section class="section-white section-slider">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="">
            <div class="carousel-inner">
                <?php
                $i = 1;
                foreach ($banners as $bannerImage) { ?>
                    <div class="item banner-item blurload <?php echo $i == 1 ? 'active' : ''; ?>" data-original="<?php echo isset($bannerImage['banner']) ? $bannerImage['banner'].'?_='.time() : ''; ?>" style="<?php echo isset($bannerImage['tinyBanner']) ? 'background-image: url(\''.$bannerImage['tinyBanner'].'?_='.time().'\')' : ''; ?>">
                    </div>
                    <?php $i++;
                } ?>
            </div>
            <?php if (count($banners) > 1) { ?>
                <ol class="carousel-indicators" >
                    <?php
                    for ($k = 0; $k < count($banners); $k++) {
                        ?>
                        <li data-target="#carousel-example-generic" data-slide-to="<?php echo $k; ?>" class="<?php echo $k == 0 ? 'active' : ''; ?>"></li>
                    <?php } ?>
                </ol>
            <?php } ?>
        </div>

        <div class="frontLayer">
            <?php if ($logo) { ?>
            <img src="<?php echo $logo; ?>" alt="Parenthexis"/>
            <?php } ?>
        </div>
    </section>

    <section class="section-main">
        <div class="titlePage center writeup-content">
            <p><?php echo !empty($writeup) ? nl2br($writeup) : JText::_('COM_COMMUNITY_MLP_WRITE_UP'); ?></p>
        </div>
        <div class="div-form-login joms-landing__action <?php if(CSystemHelper::tfaEnabled()) { echo 'tfaenabled'; } ?>">
            <form class="joms-form joms-js-form--login" action="<?php echo CRoute::getURI(); ?>" method="post" name="login" id="form-login">
                <div class="joms-input--append">
                    <input class="joms-input" id="user_name" type="text" placeholder="<?php echo JText::_('COM_COMMUNITY_EMAIL'); ?>" name="username" autocapitalize="off">
                </div>
                <div class="joms-input--append">
                    <input class="joms-input" id="pass_word" type="password" placeholder="<?php echo JText::_('COM_COMMUNITY_PASSWORD'); ?>" name="password" autocapitalize="off">
                </div>

                <div class="signup-forgotpassword">
                    <div class="left">
                        <?php if ($allowUserRegister) { ?>
                            <span><?php echo JText::_("COM_COMMUNITY_NO_ACCOUNT_YET"); ?></span>
                            <a href="<?php echo JRoute::_('index.php?option=com_community&view=register', false); ?>"
                               tabindex="6"><?php echo JText::_('COM_COMMUNITY_FRONT_SIGN_UP'); ?></a>
                        <?php }?>
                    </div>
                    <div class="right">
                        <div class="divButtonLogin">
                            <button class="joms-button--login"><?php echo JText::_('COM_COMMUNITY_LOG_IN') ?></button>
                        </div>
                    </div>
                </div>
                <?php if(CSystemHelper::tfaEnabled()){?>
                    <div class="joms-input--append">
                        <svg viewBox="0 0 16 16" class="joms-icon">
                            <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-key"></use>
                        </svg>
                        <input class="joms-input" type="text"
                               placeholder="<?php echo JText::_('COM_COMMUNITY_AUTHENTICATION_KEY'); ?>" name="secretkey" autocapitalize="off">
                    </div>
                <?php } ?>
                <div class="divForgotPassword">
                    <a href="<?php echo CRoute::_('index.php?option=' . COM_USER_NAME . '&view=reset'); ?>"
                       tabindex="6"><?php echo JText::_('COM_COMMUNITY_FORGOT_PASSWORD_LOGIN'); ?></a>
                </div>
                <input type="hidden" name="option" value="<?php echo COM_USER_NAME; ?>"/>
                <input type="hidden" name="task" value="<?php echo COM_USER_TAKS_LOGIN; ?>"/>
                <input type="hidden" name="return" value="<?php echo $return; ?>"/>
                <div class="joms-js--token"><?php echo JHTML::_('form.token'); ?></div>

            </form>

            <script>
                jQuery(document).ready(function() {
                    jQuery('.checked-status, .remember-reset > .joms-checkbox > span').click(function() {
                        if (jQuery('.remember-reset > .joms-checkbox > input').val() == 'yes') {
                            jQuery('.remember-reset > .joms-checkbox > input').val('no');
                        } else {
                            jQuery('.remember-reset > .joms-checkbox > input').val('yes')
                        }
                        jQuery('.checked-status').toggleClass('checked');
                    });
                    var check = jQuery('#check_value').val();
                    if(check ==1){
                        jQuery('.joms-js-form--login .joms-input').addClass('error1');
                    }
                    jQuery('.joms-button--login').click( function() {
                        var user_name = jQuery('#user_name').val();
                        var pass_word = jQuery('#pass_word').val();
                        if(user_name=='' || pass_word ==''){
                            if(user_name==''){
                                jQuery('.joms-js-form--login #user_name').addClass('error1');
                            }
                            if(pass_word==''){
                                jQuery('.joms-js-form--login #pass_word').addClass('error1');
                            }
                            return false;
                        }
                });
                joms.onStart(function( $ ) {
                    $('.joms-js-form--login').on( 'submit', function( e ) {
                        e.preventDefault();
                        e.stopPropagation();
                        joms.ajax({
                            func: 'system,ajaxGetLoginFormToken',
                            data: [],
                            beforeSend: function() {
                                $('body').addClass('overlay2');
                                $('.notification').html('Loading...').fadeIn();
                            },
                            callback: function( json ) {
                                var form = $('.joms-js-form--login');
                                if ( json.token ) {
                                    form.find('.joms-js--token input').prop('name', json.token);
                                }
                                form.off('submit').submit();
                            }
                        });
                    });
                });
                jQuery('.joms-input').click(function() {
                    jQuery('.joms-input').keyup(function () {
                        jQuery('.joms-js-form--login .joms-input').removeClass('error1');
                    });
                });
            });
            </script>
        </div>
    </section>
</div>

<!--<div class="login-using-text" style="visibility: hidden;">
    <span class="first"></span>
    <span class="second"><?php echo JText::_('COM_COMMUNITY_LOG_IN_WITH'); ?></span>
    <span class="third"></span>
</div>
<div class="login-using-social" style="visibility: hidden;">
    <a id="btFacebook" href="#" onclick="checkLoginState(event);">
        <img src="<?php echo JURI::root(true); ?>/images/login/facebook_150.png">
    </a>
    <a id="btGooglePlus" href="#">
        <img src="<?php echo JURI::root(true); ?>/images/login/google_150.png">
    </a>
    <a id="btLinkedin" href="#" onclick="checkLinkedInLogin(event);">
        <img src="<?php echo JURI::root(true); ?>/images/login/linkedin_150.png">
    </a>
</div>-->
<?php
$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
$iPod    = stripos($userAgent, "ipod");
$iPhone  = stripos($userAgent, "iphone");
$iPad    = stripos($userAgent, "ipad");
$android = stripos($userAgent, "android");

$iOS = ( $iPod || $iPhone || $iPad ) ? true : false;
// $webOS   = ( $iOS || $android ) ? false : true;
?>
<!--<div class="divDownloadApp">
    <p><?php echo JText::_('COM_COMMUNITY_DOWNLOAD_APP'); ?></p>
    <a <?php echo ($iOS) ? "style='display: none;'" : ""; ?> href="#">
        <img src="<?php echo JURI::root(true); ?>/images/login/googleplay.png">
    </a>
    <a <?php echo ($android) ? "style='display: none;'" : ""; ?> href="#">
        <img src="<?php echo JURI::root(true); ?>/images/login/appstore.png">
    </a>
</div>
<div class="div-footer-menu hidden">
    <ul>
        <li><a href="<?php echo JURI::root(true); ?>/about"><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/terms"><?php echo JText::_('COM_COMMUNITY_TERMS'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/privacy-policy"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/support"><?php echo JText::_('COM_COMMUNITY_SUPPORT'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a></li>
        <li><a href="<?php echo JURI::root(true); ?>/business"><?php echo JText::_('COM_COMMUNITY_BUSINESS'); ?></a></li>
    </ul>
</div>-->
<div class="divCopyright">
    <p>Powered by <img class="icon-px" src="<?php echo JURI::root(true); ?>/templates/t3_bs3_tablet_desktop_template/images/Parenthexis_Pantone432.png"/></p>
</div>

<!-- Facebook -->
<script>
    //    window.fbAsyncInit = function() {
    //        FB.init({
    //            appId      : '1957633501170674',
    //            cookie     : true,  // enable cookies to allow the server to access
    //                                // the session
    //            xfbml      : true,  // parse social plugins on this page
    //            version    : 'v2.8' // use graph api version 2.8
    //        });
    //
    ////        checkLoginState();
    //        FB.getLoginStatus(function(response) {
    ////            statusChangeCallback(response);
    //            console.log(response);
    //        });
    //    };

    //    (function(d, s, id) {
    //        var js, fjs = d.getElementsByTagName(s)[0];
    //        if (d.getElementById(id)) return;
    //        js = d.createElement(s); js.id = id;
    //        js.src = "//connect.facebook.net/en_US/sdk.js";
    //        fjs.parentNode.insertBefore(js, fjs);
    //    }(document, 'script', 'facebook-jssdk'));

    function statusChangeCallback(response) {
        console.log(response);

        if (response.status === 'connected') {
            var currentFBUser;
            FB.api('/me', function(result) {
                currentFBUser = result;
                var r = confirm("Current Facebook user: "+currentFBUser.name+". Do you want to change account? ");
                if (r) {
                    FB.logout(function(res) {
                        FB.login(function(res) {
                            if (res.status === 'connected') {
                                testAPI(res.authResponse.accessToken);
                            }
                        });
                    });
                } else {
                    sendSocialNetworkInfo(response.authResponse.accessToken, currentFBUser.id, 'FB');
                }
            });
        } else {
            FB.login(function(res) {
                if (res.status === 'connected') {
                    testAPI(res.authResponse.accessToken);
                }
            });
        }
    }

    function checkLoginState(event) {
        event.preventDefault();
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }

    function testAPI(appToken) {
        FB.api('/me', function(response) {
            console.log(response);
            console.log('Successful login for: ' + response.name);

            sendSocialNetworkInfo(appToken, response.id, 'FB');
        });
    }
</script>

<!-- Google Plus -->
<!--<script src="https://apis.google.com/js/api:client.js"></script>-->
<script>
    function sendSocialNetworkInfo(appToken, appID, appType) {
        jQuery.ajax({
            url: '<?php echo JUri::base();?>index.php?option=com_userapi&task=login_social',
            type: 'POST',
            data: {
                appToken: appToken,
                appID: appID,
                appType: appType,
                appPlatform: 'web'
            },
            beforeSend: function() {
                jQuery('body').addClass('overlay2');
                jQuery('.notification').html('Loading...').fadeIn();
            },
            success: function(res) {
                console.log(res);
                if (res.status) window.location.href = "<?php echo JUri::base();?>";
                else {
                    jQuery('.lgt-overlay').removeClass('lgt-visible');
                    jQuery('#system-message-container').html('<div id="system-message">'+
                    '<div class="alert alert-warning">'+
                    '<a class="close" data-dismiss="alert">×</a>'+
                    '<h4 class="alert-heading">Warning</h4>'+
                    '<div>'+
                    '<p>'+res.error_message+'</p>'+
                    '</div>'+
                    '</div>'+
                    '</div>');
                    jQuery('body').removeClass('overlay2');
                    jQuery('.notification').fadeOut();
                }
            }
        });
    }
    //    var googleUser = {};
    //    var startApp = function() {
    //        gapi.load('auth2', function(){
    //            auth2 = gapi.auth2.init({
    //                client_id: '217112268561-j1tp7iie9uoihhgc9avk9mima09fnqkr.apps.googleusercontent.com',
    //                cookiepolicy: 'single_host_origin'
    //                //scope: 'additional_scope'
    //            });
    //            attachSignin(document.getElementById('btGooglePlus'));
    //        });
    //    };

    function attachSignin(element) {
        auth2.attachClickHandler(element, {},
            function(googleUser) {
                console.log(googleUser.getAuthResponse());
                var id_token = googleUser.getAuthResponse().id_token;
                console.log(googleUser.getBasicProfile());
                sendSocialNetworkInfo(id_token, googleUser.getBasicProfile().Eea, 'GG' );
            }, function(error) {
                console.log(JSON.stringify(error, undefined, 2));
            });
    }

    jQuery('#btGooglePlus').click(function(e) {
        e.preventDefault();
    });
</script>
<!--<script>startApp();</script>-->

<!-- LinkedIN -->
<!--<script type="text/javascript" src="//platform.linkedin.com/in.js">
    api_key: 817lrlamtcx4ys
    authorize: true
    onLoad: onLinkedInLoad
</script>-->
<script type="text/javascript">
    var currentLinkedinUser;
    var linkedinReady = false;

    function onLinkedInLoad() {
        linkedinReady = true;
        IN.Event.on(IN, "auth", getProfileData);
        // IN.Event.on(IN, "logout", requireLinkedInLogin);
    }
    function checkLinkedInLogin(event) {
        event.preventDefault();
        if (linkedinReady) {
            console.log('Linkedin login status: ' + IN.User.isAuthorized());
            if (IN.User.isAuthorized()) {

                var r = confirm("Current Linkedin user: "+currentLinkedinUser.firstName+' '+currentLinkedinUser.lastName+' - '+currentLinkedinUser.headline+". Do you want to change account? ");
                if (r) {
                    IN.User.logout(function() {
                        requireLinkedInLogin();
                    });
                    location.reload();
                } else {
                    requireLinkedInLogin();
                }

            } else {
                requireLinkedInLogin();
            }
        } else {
            alert('Loading necessary files for Linkedin Login. Please wait some seconds and try again later...');
        }
    }
    function requireLinkedInLogin() {
        IN.User.authorize(function() {
            IN.API.Profile( "me" )
                .fields( 'id', 'first-name', 'last-name', 'location', 'industry', 'headline', 'picture-urls::(original)', 'email-address' )
                .result( function ( response ) {
                    console.log(response.values[0]);
                    var info = response.values[0];
                    console.log(IN.ENV.auth.oauth_token);

                    sendSocialNetworkInfo( IN.ENV.auth.oauth_token, info.id, 'IN' );
                } )
                .error( function ( err ) { } )
        });
    }

    function onSuccess(data) {
        currentLinkedinUser = data;
    }

    function onError(error) {
        console.log(error);
    }

    function getProfileData() {
        IN.API.Raw("/people/~").result(onSuccess).error(onError);
    }

</script>
