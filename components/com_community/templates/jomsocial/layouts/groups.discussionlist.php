<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();

$input = new JInput();
$groupId = $input->get('groupid');
$topic = "";
$session = JFactory::getSession();
$device = $session->get('device');
$isMobile = false;
if($device == 'mobile') $isMobile = true;
?>
<?php if(!($isGroupAdmin)){ $topic = "topic"?>
    <style>    
        .topic {
            margin-top: -2%;
        }
    </style>
<?php } ?>
<div class="joms-page view_topic <?php echo $topic ?>" style="background: none; padding: 0px">

    <div class="row custom-search-box margin-0" style="display: none;">
        <form method="POST">
            <div class="input-group">
              <span class="input-group-btn">
                <div class="icon-addon addon-lg">
                    <input style="border-radius:21px; height: 35px; line-height:20px; background: #c3c3c3" type="text" class="form-control" class="joms-input--search" name="q" id="q">
                    <label for="search" class="glyphicon glyphicon-search" style="margin-top: -5px" color="#fff" rel="tooltip"></label>
                </div>
                
             </span>
            </div>
            <input type="hidden" name="option" value="com_community"/>
            <input type="hidden" name="view" value="groups"/>
            <input type="hidden" name="task" value="viewdiscussions"/>
        </form>
    </div>

    <?php if($isGroupAdmin||$canCreate) {?>
        <div class="joms-group--button" style="<?php if($device == 'mobile') echo 'margin-bottom: -5px; margin-top: -15px'; ?>">
            <div style="float: right">
            <?php if($isGroupAdmin){?>
                <div style="float:left; padding-right: 10px">
                    <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=addnews&groupid=' . $input->get('groupid')); ?>">
                        <button type="button" class="joms-button--primary button--small make_a js-btn-add-announcement-button"></button>
                    </a>
                </div>
            <?php } ?>
            <?php if($canCreate) { ?>
                <div style="float:left; padding-right: 10px">
                    <a href="<?php //echo CRoute::_('index.php?option=com_community&view=events&task=create&groupid=' . $input->get('groupid')); 
                                    echo CRoute::_('events/create?groupid=' . $input->get('groupid')); 
                                ?>">
                        <button type="button" class="joms-button--primary button--small js-btn-add-event-button"></button>
                    </a>
                </div>
                <div style="float:left">
                    <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&groupid=' . $input->get('groupid') . '&task=adddiscussion'); ?>">
                        <button type="button" class="joms-button--primary button--small js-btn-add-topic-buton"></button>
                    </a>
                </div>
            <?php } ?>
            </div>
        </div>
    <?php } ?>
    <div style="/*padding-top: 15px*/"></div>
    <?php
    $model = CFactory::getModel('bulletins');
    $new = 1;
    $bulletins = $model->getBulletins($groupId, 0 , $new);

    //Chris: check if announcement is expired or not
    ?>

    <?php if(array_key_exists(0, $bulletins)) { ?>
        <?php foreach($bulletins as $k => $v) { ?>
            <div class="joms-list__item bordered-box make">
    
                <div class="col-xs-9 custom-header pull-left" style="width: 100% !important;">
                   
                    <h1>
                        <div style="float:left; padding-right: 5px; <?php if (!$isMobile) echo 'padding-left: 20px;' ?>">
                        
                        <span class="glyphicon glyphicon-bullhorn" aria-hidden="true" style="color:#000000; padding-right: 5px; margin-left: 0 !important"></span>
                        <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewbulletin&groupid='. $groupId .'&bulletinid=' . $v->id); ?>"> 
                        <?php //echo CActivities::truncateComplex($v->title, 30, true); 

                            if ($device == 'mobile'){
                                echo CActivities::truncateComplex($v->title, 30, true); 
                            }elseif ($device == 'tablet'){
                                echo CActivities::truncateComplex($v->title, 45, true); 
                            }else{
                                echo CActivities::truncateComplex($v->title, 130, true);
                            }
                        
                        ?>
                            
                        </a>
                        </div>
                        <?php if ($isGroupAdmin || $isSuperAdmin) { ?>
                        <div style="float: left; height: 0px; width: 0px">
                            <a href="javascript:" class="joms-dropdown-button">
                                <span class="glyphicon glyphicon-menu-down" aria-hidden="true" style="color:#868484;" ></span>
                            </a>
                            
                            <ul class="joms-dropdown">
                                <li class=" action ">
                                    <a href="javascript: void(0);" onclick="joms.api.announcementRemove('<?php echo $groupId ?>', '<?php echo $v->id ?>');">
                                        <?php echo JText::_('COM_COMMUNITY_DELETE');?>
                                    </a>
                                </li>
                               
                                <li class=" action ">
                                    <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewbulletin&bulletinid='. $v->id .'&groupid=' . $groupId)?>">
                                        <?php echo JText::_('COM_COMMUNITY_EDIT');?>
                                    </a>
                                </li>                                
                            </ul>
                        </div>   
                        <?php } ?>
                   
                    </h1>
                    
               <span class="margin-0 date joms--description" style="float:right"> 
               
               <?php
               $date = JFactory::getDate($v->date);
               $time = CTimeHelper::timeLapse($date, false);
               echo trim($time);
               ?>
               
               </span>
               <div style="clear: both">
                <p class="margin-0 joms--description" style="text-align: left"><?php echo CActivities::truncateComplex($v->message, 100, true); ?></p>
                </div>
            </div>
            </div>
        <?php } ?>
    <?php } ?>

    <?php
    $group = JTable::getInstance('Group', 'CTable');
    $group->load($groupId);
    $params = $group->getParams();

    $eventsModel = CFactory::getModel('Events');
    $tmpEvents = $eventsModel->getGroupEvents($groupId, $params->get('grouprecentevents', GROUP_EVENT_RECENT_LIMIT));
    $totalEvents = $eventsModel->getTotalGroupEvents($group->id);


    ?>

    <?php foreach ($tmpEvents as $event) { ?>

        <div class="joms-list__item bordered-box event" style="padding: 0px 10px 10px 10px;">

            <div class="col-xs-9 custom-header pull-left" style="width: 100% !important;">
                <h1>
                    <div style="float:left; padding-right: 5px; <?php if (!$isMobile) echo 'padding-left: 10px;' ?>">
                    <span class="glyphicon glyphicon-calendar" aria-hidden="true" style="color:#000000; padding-right: 5px; margin-left: 0 !important"></span>
                    <a href="<?php  //echo $event->getLink(); 
                        echo CRoute::_('events/viewevent/?eventid='. $event->id); 
                    ?>"> 
            

                    <?php 
                        //echo CActivities::truncateComplex($event->title, 30, true); 
                        if ($device == 'mobile'){
                            echo CActivities::truncateComplex($event->title, 30, true); 
                            }elseif ($device == 'tablet'){
                                echo CActivities::truncateComplex($event->title, 45, true); 
                            }else{
                                echo CActivities::truncateComplex($event->title, 130, true);
                        }
                    ?>
                        
                    </a>
                    </div>
                    <?php if ($isGroupAdmin || $isSuperAdmin || $my->id == $event->creator) { ?>
                        <div style="float: left; height: 0px; width: 0px">
                        <a href="javascript:" class="joms-dropdown-button">
                            <span class="glyphicon glyphicon-menu-down" aria-hidden="true" style="color:#868484"></span>
                        </a>

                        <ul class="joms-dropdown" id="dropdown-menu-<?php echo $event->id;?>">
                            <li class=" action ">
                                <a href="javascript: void(0);" onclick="joms.api.eventDelete('<?php echo $event->id ?>');">
                                    <?php echo JText::_('COM_COMMUNITY_DELETE');?>
                                </a>
                            </li>
                            <li class=" action ">
                                <a href="<?php echo CRoute::_('events/viewevent/?eventid='. $event->id); ?>">
                                    <?php echo JText::_('COM_COMMUNITY_EDIT');?>
                                </a>
                            </li>
                        </ul>
                        </div>
                    <?php } ?>
                    
                </h1>
                
           <span class="margin-0 joms--description date" style="float:right;<?php if ($isMobile) echo ' width: 50px;' ?>"> 
           
           <?php
           $date = JFactory::getDate($event->created);
           $time = CTimeHelper::timeLapse($date, false);
           echo $time;
           ?>
           
           </span>
                <p class="margin-0 joms--description"><?php echo CActivities::truncateComplex($event->description, 100, true); ?></p>
            </div>
        </div>

    <?php } ?>

    <?php
    //echo $device . 'device';
    $mainframe = JFactory::getApplication(); 
    $rooturl = $mainframe->getCfg('wwwrootfile');
    
    if ($discussions) {
        foreach ($discussions as $row) {
            
                if(!empty($row->thumb))  {
                    $thumbnail = $rooturl . '/' . $row->thumb; 
                }else{
                    $thumbnail = '/components/com_community/assets/group64x64.png';
                }

            ?>
            <div class="joms-list__item bordered-box discussion">

                <div class="col-xs-3 pull-left" style="width: 100px !important">
                    <div class="jom-list__avatar" style="padding-top: 15px">
                        <p class="joms-avatar">
                            <img src="<?php echo $thumbnail; ?>" alt="<?php echo $row->user->getDisplayName(); ?>" data-author="<?php echo $row->user->id; ?>" class="img-responsive">
                        </p>
                    </div>
                </div>

                <div class="col-xs-9 custom-header pull-left topic-item">
                    <h1 class="topic-content"><a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $groupId . '&topicid=' . $row->id); ?>"> 
                    <div style="float:left; padding-right: 5px;">
                    <?php 

                        if ($device == 'mobile'){
                            echo CActivities::truncateComplex($row->title, 16, true); 
                        }elseif ($device == 'tablet'){
                            echo CActivities::truncateComplex($row->title, 40, true); 
                        }else{
                            echo CActivities::truncateComplex($row->title, 120, true);
                        }
                        
                    ?>
                        
                    </a>
                    </div>

                        <?php if ($isGroupAdmin || $isSuperAdmin || $row->creator == $my->id) { ?>
                        <div style="float: left; height: 0px; width: 0px">
                            <a href="javascript:" class="joms-dropdown-button" style="position: relative;"> <!-- data-ui-object="joms-dropdown-button" -->
                                <span class="glyphicon glyphicon-menu-down" aria-hidden="true" style="color:#868484"></span>
                            </a>
                           
                            <ul class="joms-dropdown" id="dropdown-menu-<?php echo $row->id;?>">
                                <li class="setAdmin" onclick="joms.api.avatarChange('discussion', '<?php echo $row->id;?>');">
                                    <a href="javascript: void(0);" onclick="">
                                        <i class="icon-topic-editphoto"></i>
                                        <span><?php echo JText::_('COM_COMMUNITY_TOPIC_EDIT_PHOTO'); ?></span>
                                    </a>
                                </li>
                                <li class="setAdmin">
                                    <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=editdiscussion&groupid='. $groupId .'&topicid='. $row->id); ?>" onclick="">
                                        <i class="icon-topic-edit"></i>
                                        <span><?php echo JText::_('COM_COMMUNITY_TOPIC_EDIT');?></span>
                                    </a>
                                </li>
                                <?php if (!$row->lock) { ?>
                                    <li class=" action ">
                                        <a href="javascript: void(0);" onclick="joms.api.discussionLock('<?php echo $groupId ?>', '<?php echo $row->id ?>');">
                                            <i class="icon-topic-lock"></i>
                                            <span><?php echo JText::_('COM_COMMUNITY_LOCK_TOPIC');?></span>
                                        </a>
                                    </li>
                                <?php } else { ?>
                                    <li class=" action ">
                                        <a href="javascript: void(0);" onclick="joms.api.discussionLock('<?php echo $groupId ?>', '<?php echo $row->id ?>');">
                                            <i class="icon-topic-unlock"></i>
                                            <span><?php echo JText::_('COM_COMMUNITY_UNLOCK_TOPIC');?></span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <li class=" action ">
                                    <a href="javascript: void(0);" onclick="joms.api.discussionRemove('<?php echo $groupId ?>', '<?php echo $row->id ?>');">
                                        <i class="icon-topic-delete"></i>
                                        <span><?php echo JText::_('COM_COMMUNITY_TOPIC_DELETE');?></span>
                                    </a>
                                </li>
                            </ul>
                            
                        </div>
                        <?php } ?>

                    </h1>
                    <span class="margin-0 date joms--description" style="float:right"> 

                    <?php
                    $date = JFactory::getDate($row->created);
                    $time = CTimeHelper::timeLapse($date, false);
                    echo $time;
                    ?>
                    </span>
                    <p class="margin-0 joms--description"><?php 
                    $row->message = strip_tags($row->message,true);
                    if($device=='mobile') { echo CActivities::truncateComplex($row->message, 70, true); } else { echo CActivities::truncateComplex($row->message, 120, true); } ?></p>
                </div>
            </div>

        <?php
        }
        ?>
    <?php
    } else {
        ?>

        <p <?php if($isGroupAdmin) echo 'class="massage_topic"';?>style="text-align: center; background: white;
    padding: 10px;">
            <?php
            echo sprintf(JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_EMPTY_WARNING'), CRoute::_(
                'index.php?option=com_community&view=groups&groupid=' . $input->get('groupid') . '&task=adddiscussion')); ?>
        </p>

    <?php
    }
    ?>

</div>
<script>
    jQuery(document).click(function(e) {
        var target = e.target;
        jQuery(target).toggleClass('active');
//          jQuery('ul').removeClass('show');
        jQuery(target).parents().next('ul').toggleClass('show');
        if (!$(target).is('.joms-dropdown-button') && !$(target).parents().is('.joms-dropdown-button')) {
            jQuery('ul').removeClass('show');
        }
    });
    //    jQuery(document).ready(function() {
    //        jQuery('.joms-dropdown-button').click(function() {
    //            jQuery(this).toggleClass('active');
    //            jQuery('.topic-item ul').toggleClass('show');
    //        });
    //
    //
    //    }(jQuery));
</script>
