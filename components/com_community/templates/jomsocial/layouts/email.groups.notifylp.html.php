<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();
?>
<?php 
if ($email_type == 'assign') echo JText::sprintf( 'COM_COMMUNITY_EMAIL_GROUP_NOTIFY_LP_ASSIGN'); 
if ($email_type == 'approval') echo JText::sprintf( 'COM_COMMUNITY_EMAIL_GROUP_NOTIFY_LP_APPROVAL'); 
if ($email_type == 'approved') echo JText::sprintf( 'COM_COMMUNITY_EMAIL_GROUP_NOTIFY_LP_APPROVED'); 
if ($email_type == 'unapproved') echo JText::sprintf( 'COM_COMMUNITY_EMAIL_GROUP_NOTIFY_LP_UNAPPROVED'); 
if ($email_type == 'published') echo JText::sprintf( 'COM_COMMUNITY_EMAIL_GROUP_NOTIFY_LP_PUBLISHED'); 
?>