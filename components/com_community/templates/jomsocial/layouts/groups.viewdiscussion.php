<?php
    /**
     * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
     * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
     * @author iJoomla.com <webmaster@ijoomla.com>
     * @url https://www.jomsocial.com/license-agreement
     * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
     * More info at https://www.jomsocial.com/license-agreement
     */
    defined('_JEXEC') or die();

    $string = 'COM_COMMUNITY_GROUPS_DISCUSSION_CREATOR_TIME_LINK';

    if ($isTimeLapsed == 'lapse') {
        $string = 'COM_COMMUNITY_GROUPS_DISCUSSION_CREATOR_TIME_LINK_LAPSED';
    }

    $document = JFactory::getDocument();
    $document->addStyleSheet('components/com_community/assets/release/css/discussions.css');

    //$titleValue = CActivities::truncateComplex($discussion->title, 60, true);
    $titleValue = $discussion->title;
    //$descValue = CActivities::truncateComplex($discussion->message, 60, true);
    
    $descValue = $discussion->message;

    $mainframe = JFactory::getApplication(); 
    $rooturl = $mainframe->getCfg('wwwrootfile');
    $thumbnail = '/components/com_community/assets/chat-default.png';
    if($discussion->avatar != '')  $thumbnail = $rooturl . '/' . $discussion->avatar;

    $session = JFactory::getSession();

    $device = $session->get('device');
    $isMobile = false;
    if($device == 'mobile') $isMobile = true; 
    if(!$group->published)
    $hide = 'hidden';
?>
<div class="joms-main">
    <div class="joms-page">
        <div class='discussionBanner'>
            <div class="discussionProfile">
                <img src="<?php echo $thumbnail; ?>"/>
            </div>
            <div class="discussionTextOuterWrapper">
                <div class="discussionTextTopWrapper">
                    <div class="titleContent  limitDesc" title="<?php echo $discussion->title; ?>"> <?php echo $discussion->title; ?>
                    </div>

                    <div class="arrowDown <?php echo $hide;?>" <?php echo $canedit ? '' : 'style="display: none;"'; ?>>
                        <?php //echo $submenu;?>

                        <div class="joms-subnav">
                        <ul>             
<!--                                <li class=" action " onclick="joms.api.avatarChange('discussion', '<?php // echo $discussion->id;?>');">
                                <a href="javascript: void(0);" onclick=""><?php // echo JText::_('COM_COMMUNITY_TOPIC_EDIT_PHOTO');?></a>
                                </li>-->

                                <li class=" action ">
                                <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=editdiscussion&groupid='. $group->id .'&topicid='.$discussion->id); ?>" onclick=""><?php echo JText::_('COM_COMMUNITY_TOPIC_EDIT');?></a>
                                </li>

<!--                                <li class=" action ">
                                <?php // if(!$discussion->lock) {?>
                                <a href="javascript: void(0);" onclick="joms.api.discussionLock('<?php // echo $group->id ?>', '<?php // echo $discussion->id ?>');"><?php // echo JText::_('COM_COMMUNITY_LOCK_TOPIC');?></a>
                                <?php // } else { ?>
                                <a href="javascript: void(0);" onclick="joms.api.discussionLock('<?php // echo $group->id ?>', '<?php // echo $discussion->id ?>');"><?php // echo JText::_('COM_COMMUNITY_UNLOCK_TOPIC');?></a>
                                <?php // } ?>
                                </li>-->
                                <li class=" action ">
                                <a href="javascript: void(0);" onclick="joms.api.discussionRemove('<?php echo $group->id ?>', '<?php echo $discussion->id ?>');"><?php echo JText::_('COM_COMMUNITY_TOPIC_DELETE');?></a>
                                </li>
                                
                            </ul>
                    </div>

                    <div class="joms-subnav__menu">
<!--                        <a href="javascript:" class="joms-button--neutral joms-button--full" data-ui-object="joms-dropdown-button">-->
<!--                            Options-->
<!--                        </a>-->
                        <ul>

<!--                                <li class=" action " onclick="joms.api.avatarChange('discussion', '<?php // echo $discussion->id;?>');">
                                <a href="javascript: void(0);" onclick=""><?php // echo JText::_('COM_COMMUNITY_TOPIC_EDIT_PHOTO');?></a>
                                </li>-->
                    
                                <li class=" action ">
                                <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=editdiscussion&groupid='. $group->id .'&topicid='.$discussion->id); ?>" onclick=""><?php echo JText::_('COM_COMMUNITY_TOPIC_EDIT');?></a>
                                </li>

<!--                                <li class=" action ">
                                <?php // if(!$discussion->lock) {?>
                                <a href="javascript: void(0);" onclick="joms.api.discussionLock('<?php // echo $group->id ?>', '<?php // echo $discussion->id ?>');"><?php // echo JText::_('COM_COMMUNITY_LOCK_TOPIC');?></a>
                                //<?php // } else { ?>
                                <a href="javascript: void(0);" onclick="joms.api.discussionLock('<?php // echo $group->id ?>', '<?php // echo $discussion->id ?>');"><?php // echo JText::_('COM_COMMUNITY_UNLOCK_TOPIC');?></a>
                                <?php // } ?>
                                </li>-->
                                <li class=" action ">
                                <a href="javascript: void(0);" onclick="joms.api.discussionRemove('<?php echo $group->id ?>', '<?php echo $discussion->id ?>');"><?php echo JText::_('COM_COMMUNITY_TOPIC_DELETE');?></a>
                                </li>
                                
                            </ul>
<!--                        <div class="joms-gap"></div>-->
                    </div>

                    <div class="joms-subnav--desktop" style="display: none;">
                        <ul>
<!--                                <li class=" action " onclick="joms.api.avatarChange('discussion', '<?php // echo $discussion->id;?>');">
                                <a href="javascript: void(0);" onclick=""><?php // echo JText::_('COM_COMMUNITY_TOPIC_EDIT_PHOTO');?></a>
                                </li>-->

                                <li class=" action ">
                                <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=editdiscussion&groupid='. $group->id .'&topicid='.$discussion->id); ?>" onclick=""><?php echo JText::_('COM_COMMUNITY_TOPIC_EDIT');?></a>
                                </li>

<!--                                <li class=" action ">
                                <?php // if(!$discussion->lock) {?>
                                <a href="javascript: void(0);" onclick="joms.api.discussionLock('<?php // echo $group->id ?>', '<?php // echo $discussion->id ?>');"><?php // echo JText::_('COM_COMMUNITY_LOCK_TOPIC');?></a>
                                <?php // } else { ?>
                                <a href="javascript: void(0);" onclick="joms.api.discussionLock('<?php // echo $group->id ?>', '<?php // echo $discussion->id ?>');"><?php // echo JText::_('COM_COMMUNITY_UNLOCK_TOPIC');?></a>
                                <?php // } ?>
                                </li>-->
                                <li class=" action ">
                                <a href="javascript: void(0);" onclick="joms.api.discussionRemove('<?php echo $group->id ?>', '<?php echo $discussion->id ?>');"><?php echo JText::_('COM_COMMUNITY_TOPIC_DELETE');?></a>
                                </li>
                                
                            </ul>
                    </div>


                    </div>
                    
                </div>
                <div class="discussionTextBottomWrapper">
                    <div class="descContent limitDesc"> 
                       <?php
                            $des = JHtml::_('string.truncate', $descValue, 0, true, $allowHtml = true);
                            if ($isMobile && strlen($des) > 50) {
                                $shortdes = CActivities::truncateComplex($des, 45, true);
                                echo '<span class="shortdes">'.$shortdes.'<span class="btshow"> '.JText::_('COM_COMMUNITY_SHOW_MORE').'</span></span>';
                                echo '<span class="des hidden">'.$des.'<span class="btshow"> '.JText::_('COM_COMMUNITY_SHOW_LESS').'</span></span>';
                            } else if (!$isMobile && strlen($des) > 200) {
                                $shortdes = CActivities::truncateComplex($des, 210, true);
                                echo '<span class="shortdes">'.$shortdes.'<span class="btshow"> '.JText::_('COM_COMMUNITY_SHOW_MORE').'</span></span>';
                                echo '<span class="des hidden">'.$des.'<span class="btshow"> '.JText::_('COM_COMMUNITY_SHOW_LESS').'</span></span>';
                            } else echo $des; 
                       ?>
                    </div>
                    </div>
                <div class="clearBoth"></div>
            </div>
        </div>
        <div class='joms-discussion--line  col-xs-12'></div>

        <div class="joms-gap"></div>
        <div class="joms-stream__status--mobile">
            <a href="javascript:" onclick="joms.api.streamShowComments('<?php echo $discussion->id ?>', 'discussions');">
                <span class="joms-comment__counter--<?php echo $discussion->id; ?>"><?php echo $wallCount; ?></span>
            </a>
           
            <?php echo $wallContent; ?>
        </div>
       
        <div style="display:none"><?php echo $wallViewAll; ?></div>
        <?php echo $wallContent; ?>
        <?php echo $wallForm; ?>
        <div>
        <a name="msgbottom"></a>
        </div>
         <div id="kv-popup-video" class="kv-popup-video">
            <span class="btClose">X</span>
            <iframe id="kv-youtube-video" width="420" height="315" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe>
        </div>
       

        <script>
           
            (function($) {

//                jQuery(document).on('DOMNodeInserted', '.joms-comment', function(e) {    
                
//                });
                <?php if ($device == 'mobile') {?>
                jQuery(document).on('DOMNodeInserted', function(e) {   

//                    var banner = jQuery(".discussionBanner").outerHeight(true);
//                    jQuery(".joms-page > .joms-js--comments").css('padding-top', banner);

                var totalHeight = 0;

                jQuery(".joms-page > .joms-js--comments").children().each(function(){
                    totalHeight = totalHeight + jQuery(this).outerHeight(true);
                });
                    jQuery(".joms-page").animate({ scrollTop: totalHeight }, 0);  
                });
                <?php } else { ?>
//                    var banner = jQuery(".discussionBanner").outerHeight(true);
//                    jQuery(".joms-page > .joms-js--comments").css('padding-top', banner);
                
                    var totalHeight = 0;

                    jQuery(".joms-page > .joms-js--comments").children().each(function(){
                        totalHeight = totalHeight + jQuery(this).outerHeight(true);
                    });
                    jQuery(".joms-page").animate({ scrollTop: totalHeight }, 0);
                <?php } ?>
                
               var chatid = <?php echo $discussion->id;?>;
                
                jQuery(document).on('click','.joms-button--comment', function(e) {    
                    var totalHeight = 0;

                    jQuery(".joms-page > .joms-js--comments").children().each(function(){
                        totalHeight = totalHeight + jQuery(this).outerHeight(true);
                    });
                    jQuery(".joms-page").animate({ scrollTop: totalHeight }, 1000);
                });

                
                jQuery(document).on('keyup','.joms-textarea', function(e) { 
                    if (e.keyCode == 13) {
                        var totalHeight = 0;

                        jQuery(".joms-page > .joms-js--comments").children().each(function(){
                            totalHeight = totalHeight + jQuery(this).outerHeight(true);
                        });
                        jQuery(".joms-page").animate({ scrollTop: totalHeight }, 1000);
                    }
                });
                

                jQuery(document).on('click', '.joms-media--video[data-type="youtube"]', function(e) {
                    if (!jQuery(this).hasClass('being-played')) {
                        var code = jQuery(this).attr('data-path');
                    
                        if (code.indexOf('watch?v=') != -1) {
                            var vid = code.substr((code.indexOf('watch?v=') + 8), 11);
                        } else if (code.indexOf('youtu.be/') != -1) {
                            var vid = code.substr((code.indexOf('youtu.be/') + 9), 11);
                        }
                        if ( typeof(vid) != 'undefined') {
                            jQuery('#kv-youtube-video').attr('src', 'https://www.youtube.com/embed/'+vid);
                            jQuery('#kv-popup-video').fadeIn();
                            jQuery('body').addClass('overlay');
                        }
                    }                    
                });
                // jQuery(document).not('.kv-popup-video, .kv-popup-video *, .joms-media--video, .joms-media--video * ').click(function(e) {
                jQuery('#kv-popup-video > .btClose').click(function(e) {
                    var src = jQuery('#kv-youtube-video').attr('src');
                    jQuery('#kv-youtube-video').attr('src',src);  
                    jQuery('#kv-popup-video').fadeOut();
                    jQuery('body').removeClass('overlay');
                });
                jQuery('.discussionBanner .arrowDown').click(function() {
                    if (jQuery('html').hasClass('tab')) {
                        jQuery('.discussionBanner .arrowDown .joms-subnav--desktop').fadeToggle();
                    } else {
                        jQuery('.discussionBanner .arrowDown .joms-subnav__menu').fadeToggle();
                    }
                });
                if ($('.descContent')[0].scrollHeight < 50) {
                    $('.discussionTextBottomWrapper .expansionWrapper').hide();
                } else {
                    $('.discussionTextBottomWrapper .expansionWrapper').show();
                }
                if ($('.titleContent')[0].scrollHeight < 30) {
                    $('.discussionTextTopWrapper .expansionWrapper').hide();
                } else {
                    $('.discussionTextTopWrapper .expansionWrapper').show();
                }
            })(jQuery);
            
            (function( w ) {
                
                   w.joms_queue || (w.joms_queue = []);
                   w.joms_queue.push(function( $ ) {
                   var element = $('.joms-js--comments').prepend( $('.joms-js--more-comments').parent().html() );
                   
                    $('.joms-comment__item').each(function() {
                        var fullURL = $(this).find( "a" ).attr('href');
                        var start = fullURL.indexOf("userid=")+7;
                        var end = fullURL.indexOf("&Item");
                        var commentorID = fullURL.substring(start,end);
                        var myID = <?php echo $my->id ?>;
                        
                        // console.log(commentorID + " "+ myID);
                        if(commentorID==myID) 
                            $(this).addClass('blueBubble');
                   });
                   
                    $('.discussionTextBottomWrapper .expansionWrapper .expandBtn').on('click',function(){
                        $('.descContent').toggleClass('limitDesc');
                    });

                   $('.discussionTextTopWrapper .expansionWrapper .expandBtn').on('click',function(){
                       $('.titleContent').toggleClass('limitDesc');
                   });
                }); 
                
            })( window );

//            jQuery('.joms-comment').on('click', '.joms-comment__item', function(e){
//                var dataid = jQuery(this).data("id");
//                jQuery('.joms-comment #removebutton-'+dataid).toggle();
//            });
              
            jQuery('.joms-comment').on('click', '.removebutton', function(e){
                e.preventDefault();
                var dataid = jQuery(this).data("id");
                if (confirm('Are you sure you want to delete?')) {
                    joms.api.commentRemove(dataid, 'wall');
                } else {
                    // Do nothing! Just chill..
                }
            });    
              
            jQuery('.descContent .shortdes .btshow, .descContent .des .btshow').click(function() {
                jQuery('.descContent .shortdes, .descContent .des').toggleClass('hidden');
            });
            
            jQuery('.joms-js--comment-content .shortdes .btshow, .joms-js--comment-content .des .btshow').click(function() {
                jQuery(this).closest('.joms-js--comment-content').find('.shortdes').toggleClass('hidden');
                jQuery(this).closest('.joms-js--comment-content').find('.des').toggleClass('hidden');
            });
            
            jQuery('.btn_backListChats').click(function() {
                jQuery('.discussion_content .left-column').removeClass('hide');
                jQuery('.discussion_content .right-column').removeClass('show');
                jQuery('.joms-tab__bar').removeClass('hide');
                jQuery('.jomsocial').removeClass('showTopic');
                jQuery('.discussion_content').removeClass('showTopic');
            });
            jQuery(window).load(function() {
              jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, 1000);
            });
        </script>
    </div>
</div>