<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('_JEXEC') or die();
?>
<div class="joms-focus__cover">
    <?php  if (isset($featuredList) && in_array($group->id, $featuredList)) { ?>
        <div class="joms-ribbon__wrapper">
            <span class="joms-ribbon joms-ribbon--full"><?php echo JText::_('COM_COMMUNITY_FEATURED'); ?></span>
        </div>
    <?php } ?>

    <div class="joms-focus__cover-image--mobile joms-js--cover-image-mobile"
        <?php if (isset($group->defaultCover) && isset($group->coverAlbum) && !$group->defaultCover && $group->coverAlbum) { ?>
            style="background:url(<?php echo $group->getCover(); ?>) no-repeat center center;cursor:pointer"
            onclick="joms.api.coverClick(<?php echo $group->coverAlbum ?>, <?php echo $group->coverPhoto ?>);"
        <?php } else { ?>
            style="background:url(<?php echo $group->getCover(); ?>) no-repeat center center"
        <?php } ?>>
    </div>

    <div class="joms-focus__cover-image joms-js--cover-image">
        <img id="img-cover" src="<?php echo $group->getCover(); ?>" alt=""
            <?php if (isset($group->defaultCover) && isset($group->coverAlbum) && !$group->defaultCover && $group->coverAlbum) { ?>
                style="width:100%;top:<?php echo $group->coverPosition; ?>;cursor:pointer"
                onclick="joms.api.coverClick(<?php echo $group->coverAlbum ?>, <?php echo $group->coverPhoto ?>);"
            <?php } else { ?>
                style="width:100%;top:<?php echo isset($group->coverPosition) ? $group->coverPosition : 0; ?>"
            <?php } ?>>
        <input type="file" name="cover" id="coverImg" style="display:none" onchange="readFile(this);" />
    </div>
    <?php
    if($_REQUEST['task'] == 'create') {
        $addClass = 'create';
        $addAvatarClass = 'create-avatar';
    }
    ?>
    <div class="joms-focus__header">
        <div class="joms-focus__button--options--mobile">
            <a href="javascript:" class="<?php echo $addClass; ?>">
                <div class="cover-change-icon">
                    <img src="images/icons/photoicon.png" class="cover-change-icon-img" />
                    <span>COVER</span>
                </div>
            </a>
            <!-- No need to populate menus as it is cloned from mobile version. -->
            <?php
            if($_REQUEST['task'] != 'create') { ?>
                <ul class="joms-dropdown"></ul>
            <?php } ?>
        </div>
        <div class="joms-avatar--focus">
            <a class="avatarChange  <?php echo $addAvatarClass; ?>" <?php if (isset($group->defaultAvatar) && isset( $group->avatarAlbum) && !$group->defaultAvatar && $group->avatarAlbum ) { ?>
                href="<?php echo CRoute::_('index.php?option=com_community&view=photos&task=photo&albumid=' . $group->avatarAlbum); ?>" style="cursor:default"
                onclick="joms.api.photoOpen(<?php echo $group->avatarAlbum ?>); return false;"
            <?php } ?>>
                <img src="<?php echo $group->getAvatar('avatar') . '?_=' . time(); ?>" alt="<?php echo $this->escape($group->name); ?>" class="avatar-image" style="background-color: #fff !important; border: 1px solid rgba(130, 130, 130, 0.5);">
                <?php // if ($isAdmin || $isSuperAdmin || $isMine || !$group->id) { ?>
                <div class="photo-icon">
                    <img src="images/icons/photoicon.png" class="photo-icon-img" <?php if($_REQUEST['task'] != 'create') { echo 'onclick="joms.api.avatarChange(\'group\', \''.$group->id.'\', arguments && arguments[0]); return false; "'; } ?>>
                </div>
                <?php // } ?>
            </a>
            <input type="file" name="avatar" id="avatarImg" style="display:none" onchange="readAvatar(this);" />
        </div>
    </div>
    <div class="joms-focus__actions--reposition">
        <input type="button" class="joms-button--neutral" data-ui-object="button-cancel" value="<?php echo JText::_('COM_COMMUNITY_CANCEL'); ?>"> &nbsp;
        <input type="button" class="joms-button--primary" data-ui-object="button-save" value="<?php echo JText::_('COM_COMMUNITY_SAVE'); ?>">
    </div>
</div>
<div class="joms-focus__actions">
    <ul class="joms-dropdown">
        <?php if ($group->id || COwnerHelper::isCommunityAdmin()) { ?>

            <li><a href="javascript:" data-propagate="1" onclick="joms.api.coverReposition('group', '<?php echo $group->id; ?>');"><?php echo JText::_('COM_COMMUNITY_REPOSITION_COVER'); ?></a></li>
            <li><a href="javascript:" onclick="joms.api.coverChange('group', '<?php echo $group->id; ?>');"><?php echo JText::_('COM_COMMUNITY_CHANGE_COVER'); ?></a></li>

        <?php } else { ?>
            <li><a href="javascript:" onclick="joms.api.coverChange('group', '<?php echo $group->id; ?>');"><?php echo JText::_('COM_COMMUNITY_CHANGE_COVER'); ?></a></li>
        <?php } ?>
    </ul>

</div>
<script>
    (function ($) {
        $('#gr-name').css('display', 'none');
        $('h2 .name-inline').on('click', function(e) {
            $('#gr-name').css('display', 'block');
            $('#gr-name').css('height', '30px');
            $('#gr-name').css('margin-top', '35px');
            $('#gr-name').prop('required',true);
            $(this).css('display', 'none');
        });
        $('.create').on('click', function(e) {
            $('#coverImg').click();
            e.preventDefault();
        });
        $('.avatarChange.create-avatar').on('click', function(e) {
            $('#avatarImg').click();
            e.preventDefault();
        });
        jQuery(document).on('click', '.joms-focus__button--options--mobile', function() {
            jQuery(this).children('.joms-dropdown').toggle();
        });

    })(jQuery);
    // Clone menu from mobile version to desktop version.
    (function( w ) {
        w.joms_queue || (w.joms_queue = []);
        w.joms_queue.push(function() {
            var src = joms.jQuery('.joms-focus__actions ul.joms-dropdown'),
                clone = joms.jQuery('.joms-focus__button--options--mobile ul.joms-dropdown');

            clone.html( src.html() );
        });
    })( window );
    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                console.log('read file');
                joms.jQuery('#img-cover')
                    .attr('src', e.target.result)
                    .width('100%')
                    .height('180');
                joms.jQuery('.joms-focus__cover-image--mobile')
                    .css('background', 'url('+e.target.result+') center center');
                joms.jQuery('.joms-focus__cover-image--mobile')
                    .css('display', 'block');

            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    function readAvatar(input) {
        if (input.files && input.files[0]) {
            var reader2 = new FileReader();
            reader2.onload = function (e) {
                console.log('read file');
                joms.jQuery('.avatar-image')
                    .attr('src', e.target.result)
                    .width('100px')
                    .height('100px');


            };
            reader2.readAsDataURL(input.files[0]);
        }
    }
</script>
