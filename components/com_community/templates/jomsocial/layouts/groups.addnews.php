<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();
$my = CFactory::getUser();
$groupid = $_GET['groupid'];
$title_announcement = $my->name;
?>
<link rel="stylesheet" type="text/css" href="<?php echo JURI::base(); ?>components/com_community/templates/jomsocial/assets/css/bootstrap-timepicker.min.css"/>
<script src="<?php echo JURI::base(); ?>components/com_community/assets/autosize.min.js"></script>
<script src="<?php echo JURI::base(); ?>components/com_community/assets/release/js/picker.js"></script>
<script src="<?php echo JURI::base(); ?>components/com_community/assets/release/js/picker.date.js"></script>
<script src="<?php echo JURI::base(); ?>components/com_community/templates/jomsocial/assets/js/bootstrap-timepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<div class="joms-page hvAnnouncement">
    <div class="title"><?php echo JText::_('COM_COMMUNITY_ANNOUNCEMENT_CREATE'); ?></div>
    <form class="js-form" id="announcement_create" name="addnews" method="post" action="<?php echo CRoute::getURI(); ?>">
        <div class="">
            <!-- <span><?php //echo JText::_('COM_COMMUNITY_GROUPS_BULLETIN_TITLE'); ?> <span class="joms-required">*</span></span> -->
            <input type="text" value="<?php echo $title_announcement; ?>" name="title" id="title"
                   class="joms-input hidden"
                   placeholder="<?php echo JText::_('COM_COMMUNITY_GROUPS_ADD_NEWS_TITLE_PLACEHOLDER'); ?>"/>
        </div>

        <div class="joms-form__group content_announcement">
            <span class="header"><?php echo JText::_('COM_COMMUNITY_ANNOUNCEMENT_CONTENT'); ?><span class="joms-required">*</span></span>
            <textarea name="message" id="message" class="joms-textarea"><?php echo $message; ?></textarea>
            <script type="text/javascript">
                jQuery('#message').each(function () {
                }).on('input', function () {
                    autosize(this);
                });
            </script>
        </div>


        <div class="joms-form__group container time_announcement">
            <span class="header"><?php echo JText::_('COM_COMMUNITY_ANNOUNCEMENT_END_DATE_TIME'); ?><span class="joms-required">*</span></span>
            <div class="col-xs-12 col-sm-6 col-md-6 full" style="clear: both;">
                <div class="end-date col-xs-5 col-sm-3 col-md-3 full">
                    <input style="cursor: text; text-align: center;" type="text" placeholder="dd/mm/yy"
                           name="announcement-enddate" id="announcement-enddate"/>
                    <script type="text/javascript">
                        jQuery('#announcement-enddate').pickadate({
                            format: 'dd/mm/yy',
                            formatSubmit: 'dd-mm-yyyy',
                            min: 'picker__day--today'
                        });
                    </script>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 full space" style="text-align: center;">-</div>
                <div class="end-time col-xs-4 col-sm-3 col-md-3 full">
                    <div class="input-group bootstrap-timepicker timepicker">
                        <input type="text" style="text-align: left;" name="announcement-endtime"
                               id="announcement-endtime"/>
                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span> -->
                    </div>
                    <script type="text/javascript">
                        var time = new Date();
                        var  hour = time.getHours(); // 0-24 format
                        var  minute= time.getMinutes();
                        if(hour >= 12) var currenttime = hour+":"+minute+" PM";
                        else var currenttime = hour+":"+minute+" AM";

                        jQuery('#announcement-endtime').timepicker({
                            timeFormat: 'H:i A',
                            defaultTime: currenttime,
                            minuteStep: 1
                        });
                    </script>

                </div>
            </div>
        </div>
        <div class="notifi"></div>

        <div class="joms-form__group bulletin-buttons">
            <div class="pull-right">
                <input type="submit" name="submit"
                       value="<?php echo JText::_('COM_COMMUNITY_GROUPS_CREATE_ANNOUNCEMENT'); ?>"
                       class="joms-button--primary button--small btPost"/>
            </div>
            <div class="pull-right">
                <input type="button" name="cancel" value="<?php echo JText::_('COM_COMMUNITY_CANCEL_BUTTON'); ?>"
                       onclick="goBack()"
                       class="joms-button--primary button--small btCancel"/>
            </div>
            <?php echo JHTML::_('form.token'); ?>
        </div>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('#announcement_create').submit(function () {
                    if(jQuery('#announcement-endtime').val() == ''  ){
                        jQuery('.end-time').addClass('hasError');
                        return false;
                    }
                    jQuery('#announcement-enddate, #announcement-endtime').on('keyup, change', function() {
                        validate();
                    });

                    if (!validate()) {
                        return false;
                    }

                    return true;
                });

                function validate() {
                    if (jQuery('#message').val().trim() == '') {
                        jQuery('.content_announcement').addClass('hasError');
                        return false;
                    } else jQuery('.content_announcement').removeClass('hasError');

                    var date_announcement = jQuery("input[name=announcement-enddate_submit]").val();

                    if (date_announcement.trim() == '') {
                        jQuery('.end-date').addClass('hasError');
                        return false;
                    }
                    else jQuery('.end-date').removeClass('hasError');

                    var arrDate = date_announcement.split('-');

                    var now = new Date();
                    var time;

                    time = new Date(arrDate[2], (parseInt(arrDate[1]) - 1), arrDate[0], 23, 59, 59, 0);
                    if (now > time) {
                        jQuery('.end-date').addClass('hasError');
                        return false;
                    } else {
                        jQuery('.end-date, .end-time').removeClass('hasError');
                        var arrTime = jQuery('#announcement-endtime').val().split(' ');
                        var endtime = arrTime[0].split(':');

                        if (arrTime[1] == 'AM') {
                            time = new Date(arrDate[2], (parseInt(arrDate[1]) - 1), arrDate[0], endtime[0] == 12 ? 0 : endtime[0], endtime[1], 0, 0);
                        } else {
                            time = new Date(arrDate[2], (parseInt(arrDate[1]) - 1), arrDate[0], (parseInt(endtime[0] == 12 ? 0 : endtime[0]) + 12), endtime[1], 0, 0);
                        }
                        if (now > time) {
                            jQuery('.end-time').addClass('hasError');
                            return false;
                        } else jQuery('.end-date, .end-time').removeClass('hasError');
                    }
                    return true;
                }



                jQuery('#announcement-enddate').change(function () {
                    if(jQuery('.time_announcement').hasClass('hasError')){
                        jQuery('.time_announcement').removeClass('hasError');
                    }
                });
                jQuery('#message').keyup(function () {
                    if (jQuery('.content_announcement').hasClass('hasError') && jQuery('#message').val().trim() != ''){
                        jQuery('.content_announcement').removeClass('hasError');
                    }
                });

                jQuery('#system-message-container').submit(function () {
                    return  validate_form() ? true : false;
                });

                if(jQuery('#system-message-container .alert').hasClass('alert-error-time')){
                    jQuery('#announcement-endtime').addClass('error1');
                }
            });
        </script>
    </form>
</div>
