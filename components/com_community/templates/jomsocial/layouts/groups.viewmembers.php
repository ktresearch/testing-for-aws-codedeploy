<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();
$is_mobile = false;
$session = JFactory::getSession();
$device = $session->get('device');
if($device == 'mobile')
{
    $is_mobile = true;
}


?>

<div class="joms-page viewmember" style="background: none; padding: 0px;">
   
    <?php if ($type == '1' && !( $isMine || $isAdmin || $isSuperAdmin )) { ?>
        <div>
            <?php echo JText::_('COM_COMMUNITY_PERMISSION_DENIED_WARNING'); ?>
        </div>
    <?php } else { ?>
        <?php if ($members): ?>
            
            
            <?php 
                $users = array();
                
                foreach($members as $member){
                    if($member->isAdmin || $member->isOwner){
                        $admins[] = $member;        
                    }else{
                        $users[] = $member;        
                    }
                }
                
                $members = $users; 
            
            ?>
            <?php if($isAdmin || $isSuperAdmin) { ?>
            <div style="float: right; padding: 12px 15px;">
                <a href="javascript:" onclick="joms.api.groupInvite('<?php echo $group->id; ?>')">
                        <button type="button" class="joms-button--primary button--small-member">
                        <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> 
                        <?php echo JText::_('COM_COMMUNITY_ADD_FRIENDS'); ?>
                        </button>
                </a>
            </div>  
            <?php } ?>
            
            <div class="joms-list__item bordered-box">
                <div class="joms-list--friend pull-left col-xs-12" style="background: none">
                    <h4 style="padding-left: 20px; padding-bottom: 10px"><?php echo JText::_('COM_COMMUNITY_MEMBERS_TITLE_MANAGERS');?></h4>
                    <div class="js-memb-list-admin">
                <?php foreach($admins as $admin) { ?>
                            <div class="userAvatar">
                                <a class="cIndex-Avatar" href="<?php echo CRoute::_('index.php?option=com_community&fromcircle=1&view=profile&userid=' . $admin->id); ?>">
                                    <img src="<?php echo $admin->getThumbAvatar(); ?>" data-author="<?php echo $admin->id; ?>" class="cAvatar"/>
                                </a>
<!--                                <a href="<?php // echo CRoute::_('index.php?option=com_community&view=profile&userid=' . $admin->id); ?>">-->
                                <p><?php echo $admin->getDisplayName(); ?></p>
                                <!--</a>-->
                                <p class="name_member"><?php 
                       if( $admin->id == $group->ownerid) {
                            echo JText::_('COM_COMMUNITY_VIEWABOUT_OWNER');
                       } else {
                           echo JText::_('COM_COMMUNITY_MEMBERS_TITLE_MANAGER');
                       }
                    ?></p>
                <?php if($is_mobile) { ?>
                    <!--  <p class="margin-0 joms--description admin_since"><?php //echo JText::_('COM_COMMUNITY_MEMBER_SINCE'); ?></p>
                     <p><?php //if(!empty($admin->date)) {echo date('d/m/Y',$admin->date); } else { echo "1/1/2014";} ?></p> -->      
                <?php } ?>   
                                <?php if ($type) { ?>
                                <div class="joms-js--request-notice-group-<?php echo $group->id; ?>-<?php echo $admin->id; ?>"></div>
                                <div class="joms-js--request-buttons-group-<?php echo $group->id; ?>-<?php echo $admin->id; ?>" style="white-space:nowrap">
                                    <a href="javascript:" onclick="joms.api.groupApprove('<?php echo $groupid; ?>', '<?php echo $admin->id; ?>');">
                                        <button class="joms-button--primary joms-button--smallest joms-button--full-small"><?php echo JText::_('COM_COMMUNITY_PENDING_ACTION_APPROVE'); ?></button>
                                    </a>
                                    <a href="javascript:" onclick="joms.api.groupRemoveMember('<?php echo $group->id; ?>', '<?php echo $admin->id; ?>');">
                                        <button class="joms-button--neutral joms-button--smallest joms-button--full-small"><?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING_ACTION_REJECT'); ?></button>
                                    </a>
                                </div>
                                <?php } ?>
                                <?php if ($isAdmin || $isSuperAdmin) { ?>
                                <div class="joms-list__actions">
                                    <?php echo CFriendsHelper::getUserCog($admin->id,$group->id,null,true); ?>
                                </div>
                                <?php } ?>
                        
                            </div>    
                <?php } ?>
                    </div>
                </div>
            </div>
           
            <div class="js-memb-list bordered-box ">
                <h4 style="padding-left: 20px; padding-bottom: 10px"><?php echo JText::_('COM_COMMUNITY_TITLE_CIRCLE_MEMBERS'); ?></h4>
             <div class="list_member" >
            <?php 
            if(count($members)<=10)
            {
            if($members) { 
                foreach($members as $member){?>
                 
                <div class="joms-list__item bordered-box">
                    <div class="col-xs-3 pull-left">
                       <div class="jom-list__avatar list_avatar_member" style="padding-top: 15px">
                            <a class="cIndex-Avatar" href="<?php echo CRoute::_('index.php?option=com_community&view=profile&fromcircle=1&userid=' . $member->id); ?>">
                                 <!--<p class="joms-avatar">-->
                                <img class="cAvatar" src="<?php echo $member->getThumbAvatar(); ?>" alt="<?php echo $member->getDisplayName(); ?>" data-author="<?php echo $member->id; ?>" class="img-responsive">
                                 <!--</p>-->
                            </a>
            </div>
           </div>
                    
                    <div class="custom-header pull-left">
                        <h1><a href="<?php echo CRoute::_('index.php?option=com_community&view=profile&fromcircle=1&userid=' . $member->id); ?>"> 
                            <?php echo CActivities::truncateComplex($member->getDisplayName(), 30, true); ?></a> 
                 
                            <?php if ($isAdmin || $isSuperAdmin) { ?>
                            <a href="javascript:" class="joms-dropdown-button">
                                <span class="glyphicon glyphicon-menu-down" aria-hidden="true" style="color:#868484"></span>
                            </a>
                       
                            <ul class="joms-dropdown">
                           
                                <li>
                                    <a href="javascript:void(0);" onclick="jax.call('community','groups,ajaxAddAdmin', '<?php echo $member->id; ?>', '<?php echo $group->id; ?>');setTimeout('window.location.href=window.location.href', 100);">
                                        <i class="icon-make-admin"></i>
                                        <span><?php echo JText::_('COM_COMMUNITY_GROUPS_ADMIN'); ?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:" onclick="joms.api.groupRemoveMember('<?php echo $group->id; ?>', '<?php echo $member->id; ?>')">
                                        <i class="icon-remove-member"></i>
                                        <span><?php echo JText::_('COM_COMMUNITY_GROUPS_REMOVE_FROM_GROUP');?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:" onclick="joms.api.groupBanMember('<?php echo $group->id; ?>', '<?php echo $member->id; ?>')">
                                        <i class="icon-ban-member"></i>
                                        <span><?php echo JText::_('COM_COMMUNITY_BAN_MEMBER');?></span>
                                    </a>
                                </li>
                            </ul>
                            <?php } ?>
                            <?php if ($type) { ?>
                            <div class="joms-js--request-notice-group-<?php echo $group->id; ?>-<?php echo $member->id; ?>"></div>
                            <div class="joms-js--request-buttons-group-<?php echo $groupid; ?>-<?php echo $member->id; ?>" style="white-space:nowrap">
                                <a href="javascript:" onclick="joms.api.groupApprove('<?php echo $groupid; ?>', '<?php echo $member->id; ?>');">
                                    <button class="joms-button--primary joms-button--smallest joms-button--full-small"><?php echo JText::_('COM_COMMUNITY_PENDING_ACTION_APPROVE'); ?></button>
                                </a>
                                <a href="javascript:" onclick="joms.api.groupRemoveMember('<?php echo $group->id; ?>', '<?php echo $member->id; ?>');">
                                    <button class="joms-button--neutral joms-button--smallest joms-button--full-small"><?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING_ACTION_REJECT'); ?></button>
                                </a>
                            </div>
                            <?php } ?>
                       
                        </h1>
                
                   <!--  <p class="margin-0 joms--description member_since"><?php //echo JText::_('COM_COMMUNITY_MEMBER_SINCE'); ?> <?php //if(!empty($member->date)) {echo date('d/m/Y',$member->date); } else { echo "1/1/2014";}?></p>  -->      
                    </div>
                      <?php if($isMember && $my->id!=$member->id) {  ?>
                        <?php 
                         $isFriend = CFriendsHelper::isConnected($member ->id, $my->id);
                         $isWaitingApproval = CFriendsHelper::isWaitingApproval($my->id, $member->id);
                         $isWaitingResponse = CFriendsHelper::isWaitingApproval($member ->id, $my->id);
            ?>
                    <div class="js-memb-btn-friend">                      
                        <div class="button">
                                 <?php if ( $isFriend ) { ?>
                    <a href="javascript:" class="joms-focus__button--friend" onclick="joms.api.friendRemove('<?php echo $member->id;?>')">
                               <?php echo JText::_('COM_COMMUNITY_CIRCLE_FRIEND'); ?>
                           </a>  
                <?php } else if ( $isWaitingApproval ) { ?>
                        <a href="javascript:" onclick="joms.api.friendAdd('<?php echo $member->id;?>')">
                            <button type="button" class="joms-button--primary button--small pending" style="float: right;">
                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span> 
                                <?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING'); ?>
                            </button>
                        </a>
                    <?php } else if ( $isWaitingResponse ) { ?>
                        <a href="javascript:" onclick="joms.api.friendResponse('<?php echo $member->id;?>')">
                            <button style="float: right;" type="button" class="joms-button--primary button--small pending">
                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span> 
                                <?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING'); ?>
                            </button>
                        </a>
                    <?php } else { ?>
                           <a href="javascript:" class="joms-focus__button--add" onclick="joms.api.friendAdd('<?php echo $member->id?>')">
                                <?php echo JText::_('COM_COMMUNITY_CIRCLE_ADD_FRIEND'); ?>
                            </a>
                    <?php } ?>           
                        </div>
                           
                              </div>
                        <?php } ?>
                  
                </div>
            <?php } }?>
     <?php } ?> 
            </div>
        </div>
                         
        <?php endif; ?>
        <?php
    } 
    ?>
</div>
<script>
    jQuery(document).click(function(e) {
          var target = e.target;
          jQuery(target).toggleClass('active');
//          jQuery('ul').removeClass('show');
          jQuery(target).parents().next('ul').toggleClass('show');
          if (!$(target).is('.joms-dropdown-button') && !$(target).parents().is('.joms-dropdown-button')) {
                jQuery('ul').removeClass('show');
          }
    });
//    jQuery(document).ready(function() {
//        jQuery('.joms-dropdown-button').click(function() {
//            jQuery(this).toggleClass('active');
//            jQuery('.topic-item ul').toggleClass('show');
//        });
//
//
//    }(jQuery));
    jQuery(document).ready(function() {
        jQuery(document).delegate('.com_community.view-groups.task-viewmembers .joms-popup .joms-popup__action button.joms-button--primary.btPopupAddMembers', 'click', function() {
            location.reload();
        });

    }(jQuery));

</script>
<?php if(count($members)>10) {?>
<div class="loadmore" style="float: right;display:none;">
         <a class="load_more"><?php echo JText::_('COM_COMMUNITY_LOAD_MORE'); ?></a>
         </div>
<script src="/components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script>
         $(document).ready(function() {
    var track_load = 0; //total loaded record group(s)
    var loading  = false; //to prevents multipal ajax loads
    var total_groups = <?php echo  $number ?>; //total record group(s)
    var i = 1;
    $('.list_member').load("<?php echo CRoute::_('index.php?option=com_community&view=groups&task=loadmember&groupid=' .$groupid) ?>", {'group_no':track_load}, function() {track_load++;$(".loadmore").show();}); //load first group
    
    $(".load_more").click(function(){
         i++;     
     
               $button = $(this);
             $button.html('<?php echo JText::_('COM_COMMUNITY_LOADING'); ?>');
            if(track_load <= total_groups && loading==false) //there's more data to load
            {
                            
                loading = true; //prevent further ajax loading
                $('.animation_image').show(); //show loading image
                
                //load data from the server using a HTTP POST request
                $.post('<?php echo CRoute::_('index.php?option=com_community&view=groups&task=loadmember&groupid=' .$groupid) ?>',{'group_no': track_load}, function(data){
                                    
                    $(".list_member").append(data); //append received data into the element

                    //hide loading image
                    $('.animation_image').hide(); //hide loading image once data is received
                    
                    track_load++; //loaded group increment
                    loading = false; 
                    $button.html('<?php echo JText::_('COM_COMMUNITY_LOAD_MORE'); ?>');
                          if(i>=total_groups)
                    {
               $(".loadmore").hide();
                     }
                }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                    
                    alert(thrownError); //alert with HTTP error
                    $('.animation_image').hide(); //hide loading image
                    loading = false;
                
                });
                
            }
        
    });
});
</script>
<?php } ?>

