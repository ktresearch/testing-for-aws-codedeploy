<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();

$mainframe = JFactory::getApplication();

$featured = new CFeatured(FEATURED_EVENTS);
$featuredList = $featured->getItemIds();

$titleLength= $config->get('header_title_length', 70);
$summaryLength = $config->get('header_summary_length', 80);

$enableReporting = false;
if ( $config->get('enablereporting') == 1 && ( $my->id > 0 || $config->get('enableguestreporting') == 1 ) ) {
    $enableReporting = true;
}

$isGroupEvent = $event->type == CEventHelper::GROUP_TYPE;
$table =  JTable::getInstance( 'Group' , 'CTable' );
$jinput = JFactory::getApplication()->input;
$groupid = $jinput->get->get('groupid', 0, 'Int');
$id = ($groupid) ? $groupid : $event->contentid;
$table->load($id);

// update date circle donot image avatar
 $my = CFactory::getUser();
 $groupsModel = CFactory::getModel('groups');
 $course_group = $groupsModel->getIsCourseGroup($groupid);
  if($table->avatar)
         $avatarcourse= $mainframe->getCfg('wwwrootfile') . '/'.$table->avatar;
 else {
     $datacourse = JoomdleHelperContent::call_method('get_course_info', (int)$course_group, $my->username);
     $avatarcourse = $datacourse['filepath']. $datacourse['filename'];
 }
//$avatarcourse = $mainframe->getCfg('wwwrootfile') . '/'.$table->avatar;
$session =  JFactory::getSession();
$is_mobile = $session->get('device') == 'mobile' ? true : false;
?>

<div class="joms-focus">
    <div class="joms-focus__cover joms-focus--mini" style="padding-top: 30px !important;">
        <?php  if (in_array($event->id, $featuredList)) { ?>
            <div class="joms-ribbon__wrapper">
                <span class="joms-ribbon"><?php echo JText::_('COM_COMMUNITY_FEATURED'); ?></span>
            </div>
        <?php } ?>

        <div class="joms-focus__cover-image--mobile" style="background:url(<?php if($event->getCover() != 'images/false') echo $event->getCover();
        else echo ($table->getCover()) ? $table->getCover() : '' ; ?>) no-repeat center center;">
        </div>

        <div class="joms-focus__header">
            <div class="joms-avatar--focus">
                <a <?php if (isset($event->defaultAvatar) && isset($event->avatarAlbum) && !$event->defaultAvatar && $event->avatarAlbum ) { ?>
                    href="<?php echo CRoute::_('index.php?option=com_community&view=photos&task=photo&albumid=' . $event->avatarAlbum); ?>" style="cursor:default"
                    onclick="joms.api.photoOpen(<?php echo $event->avatarAlbum ?>); return false;"
                <?php } ?>>

                    <img class="img-Eventavatar" src= "<?php if ($event->getAvatar() !='images/false') echo $event->getAvatar();  else echo ''; ?>  ">

                    <?php // if ($isAdmin || $isSuperAdmin || $isMine) { ?>
                    <!--                    <svg class="joms-icon" viewBox="0 0 16 16" onclick="joms.api.avatarChange('group', '<?php // echo $event->id ?>', arguments && arguments[0]);">
                        <use xlink:href="#joms-icon-camera">
                    </svg>-->
                    <div class="mnPhotoIcon">
                        <a href="javascript:" data-ui-object="joms-dropdown-button" class="create">
                            <img  src="images/icons/photoicon.png" class="photo-icon">
                        </a>
                    </div>
                    <?php // } ?>
                </a>
                <input type="file" name="avatar" id="coverImg" style="display:none" onchange="readFile(this);" />
            </div>
            <!--            <div class="joms-focus__date">
                <span><?php // echo CEventHelper::formatStartDate($event, JText::_('M') ); ?></span>
                <span><?php // echo CEventHelper::formatStartDate($event, JText::_('d') ); ?></span>
            </div>-->
            <div class="joms-focus__title joms-form__group">

                <div class="joms-focus__header__actions">

                    <a class="joms-button--viewed nolink" title="<?php echo JText::sprintf( $event->hits > 0 ? 'COM_COMMUNITY_VIDEOS_HITS_COUNT_MANY' : 'COM_COMMUNITY_VIDEOS_HITS_COUNT', $event->hits ); ?>">
                        <svg viewBox="0 0 16 16" class="joms-icon">
                            <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-eye"></use>
                        </svg>
                        <span><?php echo $event->hits; ?></span>
                    </a>

                    <?php if ($config->get('enablesharethis') == 1) { ?>
                        <a class="joms-button--shared" title="<?php echo JText::_('COM_COMMUNITY_SHARE_THIS'); ?>"
                           href="javascript:" onclick="joms.api.pageShare('<?php echo CRoute::getExternalURL( 'index.php?option=com_community&view=events&task=viewevent&eventid=' . $event->id ); ?>')">
                            <svg viewBox="0 0 16 16" class="joms-icon">
                                <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-redo"></use>
                            </svg>
                        </a>
                    <?php } ?>

                    <?php if ($enableReporting) { ?>
                        <a class="joms-button--viewed" title="<?php echo JText::_('COM_COMMUNITY_EVENTS_REPORT'); ?>"
                           href="javascript:" onclick="joms.api.eventReport('<?php echo $event->id; ?>');">
                            <svg viewBox="0 0 16 16" class="joms-icon">
                                <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-warning"></use>
                            </svg>
                        </a>
                    <?php } ?>

                </div>
                <p class="joms-focus__info--desktop">
                    <?php echo CActivities::truncateComplex($event->summary, $summaryLength, true); ?>
                </p>
            </div>
        </div>
    </div>
    <div class="row margin-0" style="padding-top: -14px; margin-top: -14px;">
        <!--    <div class="joms-tab__bar custom-tab-bar">
        <a href="<?php // echo CRoute::_('index.php?option=com_community&view=events&task=viewevent&eventid=' . $event->id) ?>">About</a>
        <a href="<?php // echo CRoute::_('index.php?option=com_community&view=events&task=viewguest&eventid=' .$event->id. '&type=1&groupid='. $event->contentid) ?>" class="active">Attending</a>
    </div>  -->
    </div>

</div>
<script>
    /* 
     (function ($) {

     $('#ev-name').css('display', 'none');
     $('h3 .name-inline').on('click', function(e) {
     $('#ev-name').css('display', 'block');
     $('#ev-name').css('height', '30px');
     $('#ev-name').css('margin-top', '35px');
     $(this).css('display', 'none');
     });
     })(jQuery);
     */

    jQuery('.create').click(function(e) {
        jQuery('#coverImg').click();
        e.preventDefault();
    });
    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                console.log('read file');
                joms.jQuery('.img-Eventavatar')
                    .attr('src', e.target.result)
                    .width('auto')
                    .height('auto');


            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
