<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root().'/modules/mod_joomdle_my_courses/css/swiper.min.css');
$document->addScript(JUri::root().'/modules/mod_joomdle_my_courses/swiper.min.js');

if (!isset($pageTitle)) {
    $task = JFactory::getApplication()->input->getCmd('task');
    $pageTitle = JText::_($task === 'display' ? 'COM_COMMUNITY_ADVANCESEARCH_SEARCH_RESULTS' : 'COM_COMMUNITY_ALL_MEMBERS');
}

$session = JFactory::getSession();
$device = $session->get('device');
$searchModel = CFactory::getModel('search');
?>
    <!-- Advanced Search Results -->
<div class="joms-page view_friends" style="padding: 0">
    <!--<center><span class="header-title-text">People</span></center>-->
    <div class="mnNotification"></div>
    <div class="joms-list__row">
<!--        <div class="mygroups" style="padding-left: 20px;">
            <div class="mygroup-content-block">
                <div class="swiper-container swiper-container-horizontal swiper-container-free-mode">
                    <div class="swiper-wrapper" style="transition-duration: 0ms; transform: translate3d(0px, 0px, 0px);">
                    <?php foreach ($data as $person) { 
                        $displayname = $person->getDisplayName();
                        $isWaitingApproval = CFriendsHelper::isWaitingApproval($my->id, $person->id);
                        $isWaitingResponse = CFriendsHelper::isWaitingApproval($person->id, $my->id);
                        $isFriend = CFriendsHelper::isConnected($person->id, $my->id);
                        if ($person->id == $my->id) continue;
                        //if(in_array($person->id, $myFriendsIds)) continue;
                    ?>
                        <div class="swiper-slide" style="min-height: 140px !important; height: 140px; min-width: 150px !important; width: 150px !important; position: relative; background-color:#FAFCFC !important; border-color: #F5F5F5 !important">
                            <div class="group-title" style="margin: 0 auto !important; box-shadow: none; background-color:#FAFCFC !important; border-color: #F5F5F5 !important; padding-left: 0px; padding-right: 0px">
                                <center>
                                <img class="left cAvatar jomNameTips group-image" src="<?php echo $person->getThumbAvatar(); ?>" style="float: none; margin: 0 auto !important; display: block;">
                                <span style="position: absolute; top: 45px; right: 27px">
                                    <div>
                                    <?php if ($isWaitingApproval) { ?>
                                    <a href="javascript:" class="button_sent_request" onclick="joms.api.friendAdd('<?php echo $person->id;?>')"></a>  
                                    <?php } else if ($isWaitingResponse) { ?>
                                    <a href="javascript:" class="button_pending_friend" onclick="joms.api.friendResponse('<?php echo $person->id;?>')"></a> 
                                    <?php } else if ($isFriend) { ?>
                                    <a href="javascript:" class="button_friends" onclick="#"></a> 
                                    <?php } else { ?>
                                    <a href="javascript:" class="button_add_friend" onclick="joms.api.friendAdd('<?php echo $person->id;?>')"></a> 
                                    <?php } ?>
                                   </div>
                                </span>
                                <a href="<?php echo CRoute::_(' index.php?option=com_community&view=profile&userid=' . $person->id); ?>">
                                <span class="group-statistic displayname" style="float:none; text-align: center; font-weight: bold; padding-top: 15px;"><?php echo $displayname; ?></span>
                                </a>
                                </center>  
                            </div>
                        </div>
                    <?php } ?> 
                    </div>
                </div>
                <div style="clear:both"></div>
            </div>
        </div>-->
        </div>
    <?php if($device == 'mobile'){?>
     <div class="mn-headerFriend">
        <div class="mn-headerL"><span class="headerl-title-text"><?php echo JText::_('COM_COMMUNITY_MY_FRIENDS'); ?></span></div>
        <div class="mn-headerR"><span class="headerr-title-text"><?php echo JText::_('COM_COMMUNITY_FRIEND_REQUESTS'); ?></span></div>
    </div>
         <div class="mn-mlistFriend">
    <ul class="joms-list--friend friends" style="padding: 11px">
        <?php 
        if($_REQUEST['task']=='browse') {
        $blockModel = CFactory::getModel('block');
        if($myFriends != array()){
        foreach($myFriends as $row) :
            $displayname = $row->getDisplayName();
            if(!empty($row->id) && !empty($displayname)&& $my->id!=$row->id) : ?>
            <li class="col-xs-12">
                <div class='joms-list--pic col-xs-3'<?php if ($device == 'mobile') echo 'style="width: 30%"'?>>
                    <a href="<?php echo CRoute::_(' index.php?option=com_community&view=profile&userid=' . $row->id); ?>" alt="<?php echo $row->getDisplayName(); ?>">
                        <img src="<?php echo $row->getThumbAvatar(); ?>"/>
                    </a>
                </div>
                <div class='joms-list--info col-xs-9' style='width: 30%'>
                    <a href="<?php echo CRoute::_(' index.php?option=com_community&view=profile&userid=' . $row->id); ?>" alt="<?php echo $row->getDisplayName(); ?>">
                    <div class="content" >
                    <p class='name name_<?php echo $row->id;?>'><?php echo $row->getDisplayName(); ?></p>
                    </a>
                    <?php 
                        $cUser = CFactory::getUser($row->id);
                        $occupation = $cUser->getInfo('FIELD_OCCUPATION');          
                    ?>
                    <p class='occupation joms--description' style="margin-top: -10px">
                        <?php echo $occupation; ?>
                    </p>
                </div>

                <div class="bt_friendx" id="mn-friend_<?php echo $row->id;?>" id_tam="<?php echo $row->id; ?>" style="float: right; margin-top: 17px">          
                        <button href="#" class="mn-friend joms-button--primary joms-button--small" 
                        >
                           <?php echo JText::_('COM_COMMUNITY_CIRCLE_FRIEND'); ?>
                        </button>
                        <div class="fr-title"><a href="javascript:" onclick="showUnfriend('<?php echo $row->id ?>')">Unfriend</a></div>
                       <div class="fr-content" ><a href="javascript:" onclick="showBlock('<?php echo $row->id ?>')">Block</a></div>
                </div>
               </div>       
            </li>                    
        <?php endif; ?>
        <?php endforeach; ?>  
        <?php } else{?>
            <div class="noFriend center">
                <center class="mnImage"><img  src="../images/MyFriendsIcon_555555.png" ></center>
                <p><?php echo JText::_('COM_COMMUNITY_NO_MY_FRIENDS'); ?></p>
                <div class="no-activity-action">
                    <a class="empty-search-friend" ><?php echo JText::_('COM_COMMUNITY_FIND_FRIENDS'); ?></a>
                </div>
            </div>

        <?php } ?>
        <?php } ?>
        </ul>
    </div>
      <div class="mn-mfriendRequest hidden">
        <ul class="joms-list--friend friends" >
    <?php 
    if($_REQUEST['task']=='browse') {
    $blockModel = CFactory::getModel('block');
        if($requestFriend != array()){
        foreach($requestFriend as $row ) :
                $displayname = $row->getDisplayName(); 
            ?>
        <?php if(!empty($row->id) && !empty($displayname)&& $my->id!=$row->id) : ?>
        <li class="col-xs-12">
            <div class='joms-list--pic col-xs-3'<?php if ($device == 'mobile') echo 'style="width: 30%"'?>>
                <a href="<?php echo CRoute::_(' index.php?option=com_community&view=profile&userid=' . $row->id); ?>" alt="<?php echo $row->getDisplayName(); ?>">
                    <img src="<?php echo $row->getThumbAvatar(); ?>"/>
                </a>
            </div>
            <div class='joms-list--info col-xs-9' style='width: 30%'>
                <a href="<?php echo CRoute::_(' index.php?option=com_community&view=profile&userid=' . $row->id); ?>" alt="<?php echo $row->getDisplayName(); ?>">
                    <p class='name'><?php echo $row->getDisplayName(); ?></p>
                </a>
                <div class="content" >
                    <?php
                        $cUser = CFactory::getUser($row->id);
                        $occupation = $cUser->getInfo('FIELD_OCCUPATION');
                    ?>
                    <p class='occupation joms--description' style="margin-top: -10px">
                        <?php echo $occupation; ?>
                    </p>
                </div>

                <div class="bt_friendx" id = 'remove<?php echo $row->id?>' style="float: left; margin-top: 0px">
                    <button class="approvefriend<?php echo $row->id?> joms-button--primary joms-button--small .joms-js--button-save" onclick="mnfriendsResponse('<?php echo $row->id; ?>','approve')"><?php echo JText::_('COM_COMMUNITY_PENDING_ACTION_APPROVE'); ?></button>
                     <button class="deleterequest<?php echo $row->id?> joms-button--primary joms-button--small" onclick="mnfriendsremove('<?php echo $row->id; ?>','reject');"><?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING_ACTION_REJECT'); ?></button>
               </div>
                <p class="textdelete" id="textdelete<?php echo $row->id?>"></p>
            </div>
        </li>                    
    <?php endif; ?>
    <?php endforeach; ?>  
    <?php } else{?>
          <div class="noRequest">  <center><?php echo JText::_('COM_COMMUNITY_NO_REQUEST_FRIENDS'); ?></center></div>
    <?php } ?>
    <?php } ?>
    </ul>
    </div>
</div>
    <?php } else {?>
    <style type="text/css">
        body {
            background-image: url(/images/social-feeds-bg3.jpg);
        }
        .jomsocial{
            background-image: url(/images/social-feeds-bg3.jpg) !important;
            background-size: 700px 494px !important;
        } 
        .right-column{
            background-image: url(/images/social-feeds-bg3.jpg) !important;
            height: 100%;
        }
    </style>
    <div class="joms-list__row">
        <div class="col-md-6 col-sm-6 col-xs-10 col-md-offset-0 col-sm-offset-0 col-xs-offset-1 col-sm-offset-0" style="padding: 0 5px 0 5px;">
            <div class="mn-listFriend col-md-8 col-sm-10 col-xs-12 col-md-offset-4 col-sm-offset-2">
        <div class="mn-header"><span class="header-title-text"><?php echo JText::_('COM_COMMUNITY_MY_FRIENDS'); ?></span></div>

         <ul class="joms-list--friend friends" style="padding: 11px">   
    <?php 
    if($_REQUEST['task']=='browse') {
    $blockModel = CFactory::getModel('block');
//         print_r($myFriends);
        if($myFriends != array()){
        foreach($myFriends as $row) :
            $displayname = $row->getDisplayName();
            if(!empty($row->id) && !empty($displayname)&& $my->id!=$row->id) { ?>
        <li class="col-xs-12">
            <div class='joms-list--pic col-xs-3'<?php if ($device == 'mobile') echo 'style="width: 30%"'?>>
                <a href="<?php echo CRoute::_(' index.php?option=com_community&view=profile&userid=' . $row->id); ?>" alt="<?php echo $row->getDisplayName(); ?>">
                    <img src="<?php echo $row->getThumbAvatar(); ?>"/>
                </a>
            </div>
            <div class='joms-list--info col-xs-9' style='width: 30%'>
            <div class="content" >
                <a href="<?php echo CRoute::_(' index.php?option=com_community&view=profile&userid=' . $row->id); ?>" alt="<?php echo $row->getDisplayName(); ?>">
                <p class='name name_<?php echo $row->id;?>'><?php echo $row->getDisplayName(); ?></p>
                </a>
                <?php 
                    $cUser = CFactory::getUser($row->id);
                    $occupation = $cUser->getInfo('FIELD_OCCUPATION');          
                ?>
                <p class='occupation joms--description' style="margin-top: -10px">
                    <?php echo $occupation; ?>
                </p>
            </div>
             
                 <div class="bt_friendx" id="mn-friend_<?php echo $row->id;?>" id_tam="<?php echo $row->id; ?>" style="float: right; margin-top: 17px">            
                        <button href="#" class="mn-friend joms-button--primary joms-button--small">
                           <?php echo JText::_('COM_COMMUNITY_CIRCLE_FRIEND'); ?>
                        </button>
                        <div class="fr-title"><a href="javascript:" onclick="showUnfriend('<?php echo $row->id ?>')"><?php echo JText::_('COM_COMMUNITY_UNFRIEND_FRIENDS'); ?></a></div>
                       <div class="fr-content" ><a href="javascript:" onclick="showBlock('<?php echo $row->id ?>')"><?php echo JText::_('COM_COMMUNITY_BLOCK_FRIENDS'); ?></a></div>
                   </div>
                </div>
            </li>                    
         <?php } ?>
        <?php endforeach; ?>  
        <?php } else{?>
            <div class="noFriend center">
                <center class="mnImage"><img  src="../images/MyFriendsIcon_555555.png" ></center>
                <p><?php echo JText::_('COM_COMMUNITY_NO_MY_FRIENDS'); ?></p>
                <div class="no-activity-action">
                    <a class="empty-search-friend" ><?php echo JText::_('COM_COMMUNITY_FIND_FRIENDS'); ?></a>
                </div>
            </div>

        <?php } ?>
        <?php } ?>
        </ul>
     </div>
    </div>
      <div class=" col-md-6 col-xs-10 col-sm-6 col-md-offset-0 col-xs-offset-1 col-sm-offset-0" style="padding: 0 5px 0 5px;">
       <div class="mn-friendRequest col-md-8 col-sm-10 col-xs-12 ">
        <div class="mn-header"><span class="header-title-text"><?php echo JText::_('COM_COMMUNITY_FRIEND_REQUESTS'); ?></span></div>
        <ul class="joms-list--friend friends" >
        <?php 
        if($_REQUEST['task']=='browse') {
        $blockModel = CFactory::getModel('block');
        if($requestFriend != array()){
        foreach($requestFriend as $row ) :
                $displayname = $row->getDisplayName(); 
            ?>
            <?php if(!empty($row->id) && !empty($displayname)&& $my->id!=$row->id) : ?>
            <li class="col-xs-12">
                <div class='joms-list--pic col-xs-3'<?php if ($device == 'mobile') echo 'style="width: 30%"'?>>
                    <a href="<?php echo CRoute::_(' index.php?option=com_community&view=profile&userid=' . $row->id); ?>" alt="<?php echo $row->getDisplayName(); ?>">
                        <img src="<?php echo $row->getThumbAvatar(); ?>"/>
                    </a>   
               </div>
                <div class='joms-list--info col-xs-9' style='width: 30%'>
                    <a href="<?php echo CRoute::_(' index.php?option=com_community&view=profile&userid=' . $row->id); ?>" alt="<?php echo $row->getDisplayName(); ?>">
                        <p class='name'><?php echo $row->getDisplayName(); ?></p>
                    </a>
                    <div class="content" >
                    <?php 
                        $cUser = CFactory::getUser($row->id);
                        $occupation = $cUser->getInfo('FIELD_OCCUPATION');          
                    ?>
                        <p class='occupation joms--description' style="margin-top: -10px">
                            <?php echo $occupation; ?>
                        </p>
                    </div>
                 
                    <div class="bt_friendx" id = 'remove<?php echo $row->id?>' style="float: left; margin-top: 0px">
                        <button class="deleterequest<?php echo $row->id?> joms-button--primary joms-button--small" onclick="mnfriendsremove('<?php echo $row->id; ?>','reject');"><?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING_ACTION_REJECT'); ?></button>
                        <button class="approvefriend<?php echo $row->id?> joms-button--primary joms-button--small " onclick="mnfriendsResponse('<?php echo $row->id; ?>','approve')"><?php echo JText::_('COM_COMMUNITY_PENDING_ACTION_APPROVE'); ?></button>
                    </div>
                    <p class="textdelete" id="textdelete<?php echo $row->id?>"></p>
                </div>
            </li>
        <?php endif; ?>
        <?php endforeach; ?>  
        <?php } else{?>
          <div class="noRequest">  <center><?php echo JText::_('COM_COMMUNITY_NO_REQUEST_FRIENDS'); ?></center></div>
    <?php } ?>
        <?php } ?>

    </ul>
    </div>
    </div> 
    </div>
    <div class="notification"></div>
    <?php }?>
    <div class="mnModalUnfriend" id="mytitle">
       <div class="rmPopupUnfriend changePosition">
            <p class="titleUnfriend"></p>
            <button class="btRMCancel" type="button"><?php echo JText::_('COM_COMMUNITY_CANCEL'); ?></button>
            <button class="btRMYes" type="button"><?php echo JText::_('COM_COMMUNITY_UNFRIEND_FRIENDS'); ?></button>
    </div>
    </div>

    <div class="mnModalBlock" id="mytitle">
       <div class="rmPopupBlock ">
            <p class="titleBlock"></p>
            <button class="btRMCancel" type="button"><?php echo JText::_('COM_COMMUNITY_CANCEL'); ?></button>
            <button class="btRMYes" type="button"><?php echo JText::_('COM_COMMUNITY_BLOCK_FRIENDS'); ?></button>
    </div>
    </div>
<script type="text/javascript">
    var id;
    var type;
    var name;
     function mnfriendsResponse(id,type){
        jQuery.ajax({
            type: 'post',
            url: 'index.php?option=com_community&view=friends&task=approvedFriends',
            data: {id: id,type:type },
            beforeSend: function() {
                 jQuery('body').addClass('overlay2');
                 jQuery('.notification').html('Loading...').fadeIn();
            },
            success: function(data) {
                 jQuery('body').removeClass('overlay2');
                 jQuery('.notification').fadeOut();
                 window.location.reload();
            }
        });
        jQuery('.approvefriend'+id).html('friends');
        jQuery('.deleterequest'+id).addClass('hidden');
        
     }
     function mnfriendsremove(id,type){
        jQuery.ajax({
            type: 'post',
            url: 'index.php?option=com_community&view=friends&task=approvedFriends',
            data: {id: id,type:type },
            success: function(data) {
                 jQuery('#remove'+id).addClass('hidden');
                jQuery('#textdelete'+id).html('Request removed');
            }
        });
       
     }
    function showUnfriend(id){
        var name = jQuery('.name_'+id).html();
        lgtCreatePopup('confirm', {'content': 'Are you sure you want to unfriend '+ name+'?', 'yesText': 'Unfriend'}, function() {
            jQuery.ajax({
                type: 'post',
                url: 'index.php?option=com_community&view=friends&task=ajaxRemoveFriend',
                data: {id: id},
                success: function(data) {
                    window.location.reload();
                }
            });
        });
//        jQuery('.titleUnfriend').html('Are you sure you want to unfriend '+ name+'?');
//         jQuery('.rmPopupUnfriend').attr('id',id);
//        jQuery('.rmPopupUnfriend').show();
//        jQuery('.popover').hide();
//        jQuery('.mnModalUnfriend').css('display','block');
//        jQuery('body').addClass('overlay2');
    }
    jQuery('.rmPopupUnfriend .btRMCancel').click(function(){
         jQuery('.rmPopupUnfriend').hide();
         jQuery('.mnModalUnfriend').css('display','none');
         jQuery('body').removeClass('overlay2');
    });
    jQuery('.rmPopupUnfriend .btRMYes').click(function(){
        var idFriend = jQuery('.rmPopupUnfriend').attr('id');
        jQuery.ajax({
                type: 'post',
                url: 'index.php?option=com_community&view=friends&task=ajaxRemoveFriend',
                data: {id: idFriend},
                success: function(data) {
                     window.location.reload();
                }
            });
        jQuery('.rmPopupUnfriend').css('display','none');
        jQuery('body').addClass('overlay2');
        jQuery('.mnNotification').html('Loading...').fadeIn();
    });
    function showBlock(id){
        var name = jQuery('.name_'+id).html();
        lgtCreatePopup('confirm', {'content': 'Are you sure you want to block '+ name+'?', 'yesText': 'Block'}, function() {
            jQuery.ajax({
                    type: 'post',
                    url: 'index.php?option=com_community&view=friends&task=ajaxBlockFriend',
                    data: {id: id},
                    success: function(data) {
                         window.location.reload();
                    }
                });
        });
        // jQuery('.titleBlock').html('Are you sure you want to block '+ name+'?');
        //  jQuery('.rmPopupBlock').attr('id',id);
        // jQuery('.rmPopupBlock').show();
        // jQuery('.popover').hide();
        // jQuery('.mnModalBlock').css('display','block');
        // jQuery('body').addClass('overlay2');
    }
    jQuery('.rmPopupBlock .btRMCancel').click(function(){
         jQuery('.rmPopupBlock').hide();
         jQuery('.mnModalBlock').css('display','none');
         jQuery('body').removeClass('overlay2');
    });
    jQuery('.rmPopupBlock .btRMYes').click(function(){
        var idFriend = jQuery('.rmPopupBlock').attr('id');
        jQuery.ajax({
                type: 'post',
                url: 'index.php?option=com_community&view=friends&task=ajaxBlockFriend',
                data: {id: idFriend},
                success: function(data) {
                     window.location.reload();
                }
            });
        jQuery('.rmPopupBlock').css('display','none');
        jQuery('body').addClass('overlay2');
        jQuery('.mnNotification').html('Loading...').fadeIn();
    });
    jQuery('.empty-search-friend').click(function() {
                jQuery('#toggle-search').click();
                jQuery('#searchboxfriend').click();
                jQuery('input.search-text').focus();
                jQuery('#searchcircle').hide();
                jQuery('#searchlearning').hide();
    });
</script>
<script >
var limit = 15;
var plimitstart = limit;

jQuery(document).ready(function() {
        jQuery('.mn-friend').popover({

           html: true,
           width:150,
           placement: "bottom",
             // trigger: 'focus',
           title: function () {
               return jQuery(this).next().html();
           },
           content: function () {
               return jQuery(this).next().next().html();
           }
       });
    var swiper = new Swiper('.mygroups .swiper-container', {
        pagination: '.mygroups .swiper-pagination',
        slidesPerView: <?php echo ($device == 'mobile') ? '1' : '2.5'; ?>,
        nextButton: '.mygroups .swiper-button-next',
        prevButton: '.mygroups .swiper-button-prev',
        spaceBetween: 10,
        width: 150,
        freeMode: true,
        freeModeMomentum: true,
        onTransitionEnd: function(swiper){
            jQuery.post("<?php echo CRoute::_('index.php?option=com_community&view=friends&task=loadPeople') ?>",{'plimitstart': plimitstart}, function(data){
                swiper.appendSlide(data);
                plimitstart = plimitstart + limit; 
            }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                alert(thrownError); //alert with HTTP error
            });
        }
    });
      jQuery('.mn-headerR').click(function(){
        jQuery('.mn-mfriendRequest').removeClass('hidden');
        jQuery('.mn-mlistFriend').addClass('hidden');
        jQuery('.mn-headerR').css("border-bottom","2px solid #126db6");
        jQuery('.mn-headerR .headerr-title-text').css({"color":"#126db6","font-weight": "bold"});
        jQuery('.mn-headerL').css("border-bottom","2px solid #B2B2B2");
        jQuery('.mn-headerL .headerl-title-text'). css({"color":"#B2B2B2","font-weight": "normal"});
});
        jQuery('.mn-headerL').click(function(){
        jQuery('.mn-mfriendRequest').addClass('hidden');
        jQuery('.mn-mlistFriend').removeClass('hidden');
         jQuery('.mn-headerL').css("border-bottom","2px solid #126db6");
        jQuery('.mn-headerL .headerl-title-text').css({"color":"#126db6","font-weight": "bold"});
        jQuery('.mn-headerR').css("border-bottom","2px solid #B2B2B2");
        jQuery('.mn-headerR .headerr-title-text'). css({"color":"#B2B2B2","font-weight": "normal"});
      });
 });
</script>

<style type="text/css">
body
{
    background: #fff;
}
svg:not(:root) {
    display: none;
}
.t3-content,.t3-mainbody{
    padding-bottom: 0px !important;
}
.jomsocial{
    background: #fff;
} 
.fr-title {
    display: none;
    color: blue;
    font-size: 15px;
}
.fr-content {
    display: none;
    color: red;
    font-size: 10px;
}
</style>
   
