<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
/*
Added by Nyi for Course Catalogue for Learning Provider Circle
*/


$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root().'/modules/mod_joomdle_my_courses/css/swiper.min.css');
$document->addScript(JUri::root().'/modules/mod_joomdle_my_courses/swiper.min.js');
require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
JLoader::import('joomla.user.helper'); 

$session = JFactory::getSession();
$device = $session->get('device');

$my = CFactory::getUser();
$username = $my->username;
$categoryid = $group->moodlecategoryid;

$class = '';
$idstr = 'id="mobile-show"';
if($device != 'mobile') {
    $class = 'desktop-tablet';
    $idstr = '';
}

function custom_echo($x, $length) {
    if (strlen($x) <= $length) {
        return $x;
    } else {
        $y = substr($x, 0, $length) . '...';
        return $y;
    }
}

?>

<div class="home">
    <div class="container">

    	<div class="my-course <?php echo $class.' '.$device; ?> have-courses" id="pending-approve-course">
            <div class="course-title-block">
                <h2><a href="#">Published<span class=""> (<?php echo count($published) ?>)</span></a>
                </h2>
            </div>

            <div class="course-content-block <?php echo $class; ?>">
                <div class="swiper-container swiper-container-horizontal swiper-container-free-mode">
                    <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);" id="pending-course">

                        <?php foreach ($published as $course) { ?>

                        <div id="pending-course-<?php echo $course['id'] ?>" class="swiper-slide single_course swiper-slide-active" style="width: 497.2px; margin-right: 8px;">
                            <div class="title-course" style="width: 718px;">
                                <div class="course-image-wrapper" style="background-image: url('<?php echo $course['filepath'].$course['filename'] ?>')">
                                </div>
                                <?php
                                    $link = JUri::base()."providers/product/".$course['hikashopId'].'-'.$course['hikashopAlias'];//hikashop_contentLink('product&task=show&cid='.$course-> product_id.'&name='.$course->product_alias);//hikashop_completelink("product&task=show&cid[]=".$course->product_id);// 
                                ?>
                                <a class="course-title" href="<?php echo $link; ?>"><?php echo $course['fullname'] ?></a>
                                <!--<p class="course-timeend">Valid Until: <?php // echo $course['timeend'] ?></p>-->
                                <div class="courseDetails">
                                    <?php
                                    $userid = JUserHelper::getUserId($course['creator']);
                                    $user_owner = JFactory::getUser($userid); 
                                    if ($course['count_learner'] && $course['count_learner'] > 1) {
                                        echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $course['count_learner'] . ' ' . JText::_('COM_COMMUNITY_COURSE_SUBSCRIBERS') . '</span></br>';
                                    } else {
                                        echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $course['count_learner'] . ' ' . JText::_('COM_COMMUNITY_COURSE_SUBSCRIBER') . '</span></br>';
                                    }
                                    $time_view = '';
                                    if ($course['timepublish'] && $course['timepublish'] > 0) {
                                        $time_published = $course['timepublish']; 
                                        $date2 = JFactory::getDate($time_published);
                                        $time_view = CTimeHelper::timeLapseNew($date2, false);
                                    }
                                    if ($time_view != '') {
                                        echo number_format((float)$course['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                    } else {
                                        echo number_format((float)$course['duration'], 2, '.', '') . ' hr';
                                    }
                                    ?>
                                </div>
                                <p class="course-description">
                                	<?php
                                        $course['summary'] = custom_echo($course['summary'], 54);
                                        if ($course['summary'] == '') {
                                            echo "<br><br>";
                                        } else if (strlen($course['summary']) < 35) {
                                            echo $course['summary'] . "<br/><br/>";
                                        } else {
                                            echo $course['summary'];
                                        }
                                   	?>
                                </p>
                                <div>
                                    <div>
                                    	<a class="right-side-swiper" id="unpublish" data-id="<?php echo $course['id'] ?>" href="#">
											<?php echo JText::_('COM_COMMUNITY_BUTTON_UNPUBLISH'); ?>
                                    	</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="my-course <?php echo $class.' '.$device; ?> have-courses" id="new-course">
            <div class="course-title-block">
                <h2><a href="#">Unpublish<span class=""> (<?php echo count($unpublished) ?>)</span></a>
                </h2>
            </div>

            <div class="course-content-block <?php echo $class; ?>">
                <div class="swiper-container swiper-container-horizontal swiper-container-free-mode">
                    <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);">

                        <?php foreach ($unpublished as $course) { ?>
                        <div id="newcourse-<?php echo $course['id'] ?>" class="swiper-slide single_course swiper-slide-active" style="width: 497.2px; margin-right: 8px;">
                            <div class="title-course" style="width: 718px;">
                                <div class="course-image-wrapper" style="background-image: url('<?php echo $course['filepath'].$course['filename'] ?>')">
                                </div>
                                <a class="course-title" href="<?php echo JURI::base();?>course/<?php echo $course['id']; ?>.html">
                                    <?php echo $course['fullname'] ?>
                                </a>
                                <!--<p class="course-timeend">Valid Until: <?php // echo $course['timeend'] ?></p>-->
                                <div class="courseDetails">
                                    <?php
                                    $userid = JUserHelper::getUserId($course['creator']);
                                    $user_owner = JFactory::getUser($userid); 
                                    if ($course['count_learner'] && $course['count_learner'] > 1) {
                                        echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $course['count_learner'] . ' ' . JText::_('COM_COMMUNITY_COURSE_SUBSCRIBERS') . '</span></br>';
                                    } else {
                                        echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $course['count_learner'] . ' ' . JText::_('COM_COMMUNITY_COURSE_SUBSCRIBER') . '</span></br>';
                                    }
                                    $time_view = '';
                                    if ($course['timepublish'] && $course['timepublish'] > 0) {
                                        $time_published = $course['timepublish']; 
                                        $date2 = JFactory::getDate($time_published);
                                        $time_view = CTimeHelper::timeLapseNew($date2, false);
                                    }
                                    if ($time_view != '') {
                                        echo number_format((float)$course['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                    } else {
                                        echo number_format((float)$course['duration'], 2, '.', '') . ' hr';
                                    }
                                    ?>
                                </div>
                                <p class="course-description">
                                	<?php
                                        $course['summary'] = custom_echo($course['summary'], 54);
                                        if ($course['summary'] == '') {
                                            echo "<br><br>";
                                        } else if (strlen($course['summary']) < 35) {
                                            echo $course['summary'] . "<br/><br/>";
                                        } else {
                                            echo $course['summary'];
                                        }
                                   	?>
                                </p>
                                <div>

                                    <div>
                                        <a class="left-side" id="remove" data-id="<?php echo $course['id'] ?>" href="#"><?php echo JText::_('COM_COMMUNITY_BUTTON_REMOVE'); ?></a>
                                    </div>

                                    <div style="padding-left:36%;">
                                        <a class="right-side-swiper" id="assign" style="right:auto;" href="<?php echo CRoute::_('index.php?option=com_community&view=friends&task=assignfriends&assignroles=true&courseid='.$course['id'].'&lpgroupid='.$group->id) ?>"><?php echo JText::_('COM_COMMUNITY_BUTTON_ASSIGN'); ?></a>
                                    </div>
                                     <div>
                                            <a class="right-side-swiper" id="sendapproval" data-groupid="<?php echo $group->id ?>" data-id="<?php echo $course['id'] ?>" href="#"><?php echo JText::_('COM_COMMUNITY_SUBMIT'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>


    </div>
</div>


<div id="lp-start" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" >
     <div class="modal-dialog modal-sm" style="width: 30%">
        <div class="modal-content">
            <span id="message"></span>
        <br/> <br/>

        <div style="padding-bottom: 20px">

        <div style="float: left">
        <a id="closemodal" style="width: 100%;" href="#" class="joms-focus__button--message">Close</a>
        </div>

        <div style="float: right" id="link">
        <a id="linkto" style="width: 100%;" href="#" class="joms-focus__button--message" data-id="0">Ok</a>
        </div>

        </div>
        </div>
    </div>
</div>


<?php
if($device == 'mobile') {
    $slideview = '1';
    $space = '-8';

} else if($device == 'tablet') {
    $slideview = '2.5';
    $space = '8';

} else {
    $slideview = '3';
    $space = '8';
}
?>

<script type="text/javascript">
    var username = '<?php echo $username ?>';
    var categoryid = <?php echo $categoryid ?>;
    var courseid = 0;
    var action = '';


    var x = screen.width;
    var a = 0;
    console.log(x);
    if(x < 390){
        a = 1.1;
    }
    if(x >= 390 && x < 481){
        a = 1.3;
    }
    if(x>=481 && x<=736){
        a = 1.5;
    }
    if(x>736 && x<1024){
        a = 2.5;
    }
    if(x>=1024 && x<=1366){
        a = 3;
    }
    if(x>1366 && x <=1800){
        a = 4;
    }
    if(x>1800){
        a = 5;
    }
    console.log(a);

    var swiper = new Swiper('.my-course .swiper-container', {
        pagination: '.my-course .swiper-pagination',
        slidesPerView: a,
        nextButton: '.my-course .swiper-button-next',
        prevButton: '.my-course .swiper-button-prev',
        spaceBetween: <?php echo $space; ?>,
        freeMode: true,
        freeModeMomentum: true,
        preventClicks:false,
        preventClicksPropagation:false
    });

    (function ($) {
        var deviceWidth = $(window).width();
        var newWidth = deviceWidth - 50;
        $('.home .my-course .course-content-block .swiper-slide .title-course').width(newWidth + 'px');
    })(jQuery);

    jQuery(document).ready(function() {

        jQuery('img').each(function() {
            jQuery(this).attr('src', jQuery(this).attr('kvsrc') );
        });

        /* Javascript for message popup - remove course*/

        jQuery('.title-course #remove').on('click',function(e){
            e.preventDefault();
            jQuery('#lp-start').modal('toggle');
            jQuery('#lp-start').modal('show');
            jQuery('#message').text("Are you sure you want to remove the course?");
            jQuery('#link .joms-focus__button--message').attr('id', 'linkto-remove');
            jQuery('#linkto-remove').attr("data-id", jQuery(this).data("id"));
            courseid = jQuery(this).data("id");
        });

        /* Javascript message popup - unpublish course */

        jQuery('.title-course #unpublish').on('click',function(e){
            e.stopImmediatePropagation();
            e.preventDefault();
            var clickedid = e.target.id; // approve and unapprove (action)
            var dataid = jQuery(this).data("id"); // course id
            var linkto = 'linkto-' + clickedid;
            var message = "Are you sure you want to " + clickedid + " the course?";
            jQuery('#lp-start').modal('toggle');
            jQuery('#lp-start').modal('show');

            jQuery('#message').text(message);
            jQuery('#link .joms-focus__button--message').attr('id', linkto);
            jQuery('#'+ linkto).attr("data-id", jQuery(this).data("id"));
            courseid = dataid;
            action = clickedid;

        });

        /* Javascript for Unpublish course */

        jQuery('#link').on('click', '#linkto-unpublish', function(e){
                var pendingcourse =  'pending-course-'+ courseid;
                var categoryid = <?php echo $categoryid ?>;
                console.log(categoryid);
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxUnpublishCourse&tmpl=component&format=json') ?>",
                    dataType:"json",
                    data: {username: username, courseid: courseid, categoryid: categoryid},
                    beforeSend: function () {

                    },
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                    }
                });
        });


        /* Javascript for Removing Course */

        jQuery('#link').on('click','#linkto-remove', function(e){
            var newcourse = '#newcourse-' + courseid;
            //e.stopImmediatePropagation();
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxRemoveCourseHika&tmpl=component&format=json') ?>",
                dataType: "json",
                data: {username: username, courseid: courseid, categoryid: categoryid},
                  beforeSend: function () {

                    },
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        // jQuery('#lp-start').modal('hide');
                        // jQuery(newcourse).fadeIn('slow').hide();
                        // location.reload();
                        location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                    }
            });
        });

        /* Ajax for Send for approval*/
        jQuery('.title-course #sendapproval').on('click', function(e){
              courseid = jQuery(this).data("id");
              groupid = jQuery(this).data("groupid");
              //e.stopImmediatePropagation();
              e.preventDefault();
              
              jQuery.ajax({
                    type: "POST",
                    url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxSendApproval&tmpl=component&format=json') ?>",
                    dataType: "json",
                    data: {categoryid: categoryid, courseid: courseid, username: username, groupid: groupid},
                    success: function(data){
                        location.reload();
                    }
              });
              
        });

        /* Javascript when click the close button */

        jQuery('#closemodal').click(function(e){
            e.preventDefault();
            jQuery('#lp-start').modal('toggle');
            jQuery('#lp-start').modal('hide');
        });

    });

</script>
