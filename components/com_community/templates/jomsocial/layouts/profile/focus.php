<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();

$isMine = COwnerHelper::isMine($my->id, $user->id);
$avatar = $user->getAvatarInfo();
$loggedIn = $my->id != 0;

$profileFields = '';
$themeModel = CFactory::getModel('theme');
$profileModel = CFactory::getModel('profile');
$settings = $themeModel->getSettings('profile');

$my = CFactory::getUser($profile->id);
$config = CFactory::getConfig();

$featured = new CFeatured(FEATURED_USERS);
$featuredList = $featured->getItemIds();

$cUser = CFactory::getUser($profile->id);
$aboutMe = $cUser->getInfo('FIELD_ABOUTME');
$occupation = $cUser->getInfo('FIELD_OCCUPATION');

$inEditingProcess = (isset($_REQUEST['task']) && ($_REQUEST['task'] == 'edit')) ? true : false;
if(isset($settings['profile']['tagline']) && strlen($settings['profile']['tagline'])) {

    $blocks = json_decode($settings['profile']['tagline'], true);

    $blockEnabled = true;
    foreach ($blocks as $block) {

        $blockString = "";

        if($block['spacebefore']) $blockString .= " ";

        if(strlen($block['before'])) $blockString .= JText::_($block['before']) . " ";

        if(strlen($block['field'])) {

            if (
                isset($profile->fieldsById->{$block['field']}) &&
                strlen($profile->fieldsById->{$block['field']}->value)
            ) {
                $blockString .= $themeModel->formatField($profile->fieldsById->{$block['field']});
            } else {
                $blockEnabled = false;
            }
        }
        if(strlen($block['after'])) $blockString .= " ".JText::_($block['after']);

        if($block['spaceafter']) $blockString .= " ";

        if($blockEnabled) {
            $profileFields .= $blockString;
        }
    }
}

$enableReporting = false;
if ( !$isMine && $config->get('enablereporting') == 1 && ( $my->id > 0 || $config->get('enableguestreporting') == 1 ) ) {
    $enableReporting = true;
}

$document = JFactory::getDocument();
//$document->addStyleSheet($this->baseurl .'/components/com_community/templates/jomsocial/assets/css/custom.css');

//$document->addScript($this->baseurl .'/components/com_community/templates/jomsocial/assets/tag_manager/tagmanager.js');
//$document->addStyleSheet($this->baseurl .'/components/com_community/templates/jomsocial/assets/tag_manager/tagmanager.css');
    $config = CFactory::getConfig();
    $mainframe = JFactory::getApplication();
    $data_root = $mainframe->getCfg('dataroot');

    $coverImg = ($user->getCover()) ?  $user->getCover().'?_=' . time() : $profile->cover;
    $originalPath = $mainframe->getCfg('wwwrootfile').'/'.$config->getString('imagefolder'). '/cover' . '/profile/' . $profile->id . '/cover_'.JFile::getName($coverImg) ;
    $dataimage  = $data_root . '/' . $config->getString('imagefolder') . '/cover' . '/profile/' . $profile->id . '/cover_'.JFile::getName($profile->cover);
//}

$session =  JFactory::getSession();
$is_mobile = $session->get('device') == 'mobile' ? true : false;
?>

<div class="joms-focus">

    <div class="joms-focus__cover">
        <?php  if (in_array($profile->id, $featuredList)) { ?>
            <div class="joms-ribbon__wrapper">
                <span class="joms-ribbon joms-ribbon--full"><?php echo JText::_('COM_COMMUNITY_FEATURED'); ?></span>
            </div>
        <?php } ?>
        <?php if($is_mobile){?>
        <div class="joms-focus__cover-image--mobile joms-js--cover-image-mobile"
            <?php if ($coverImg) : if (!$profile->defaultCover && $profile->coverAlbum) { ?>
                style="background:url(<?php echo $coverImg; ?>) no-repeat center center;" top = "<?php echo $profile->coverPosition; ?>"
                onclick="joms.api.coverClick(<?php echo $profile->coverAlbum ?>, <?php echo $profile->coverPhoto ?>);"
            <?php } else { ?>
                style="background:url(<?php echo $coverImg; ?>) no-repeat center center"
            <?php } endif; ?>>
        </div>
        <?php } else{?>
        <div class="joms-focus__cover-image joms-js--cover-image">
            <img src="<?php echo $coverImg; ?>" oldsrc ="<?php  if (JFile::exists($dataimage))  echo $originalPath ;else echo $coverImg;  ?>" alt="<?php echo $user->getDisplayName(); ?>"
                <?php if (!$profile->defaultCover && $profile->coverAlbum) { ?>
                    style="width:100%;" top = "<?php echo $profile->coverPosition; ?>"
                    onclick="joms.api.coverClick(<?php echo $profile->coverAlbum ?>, <?php echo $profile->coverPhoto ?>);"
                <?php } else { ?>
                    style="width:100%;" top = "<?php echo $profile->coverPosition; ?>"
                <?php } ?>>
        </div>
        <?php }?>
        <div class="joms-focus__header">
            <?php if ($isMine): ?>
                <div class="joms--topright">
                    <a href="<?php echo CRoute::_('index.php?option=com_community&view=profile&task=edit'); ?>" title="<?php echo JText::_('COM_COMMUNITY_PROFILE_EDIT'); ?>">

                        <button type="button" class="joms-button--primary button--small btnEditProfile">
                            <!--                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> -->
                            <img src="<?php echo JUri::base();?>images/editicon30x30.png" alt="Edit"/>
                        </button>
                    </a>
                </div>
            <?php endif; ?>

            <?php if ( !$isMine ) { ?>
                <div class="joms--topright">
                    <!-- Friending buton -->
                    <?php if ( $isFriend ) { ?>

                        <button type="button" class="joms-button--primary button--small btn-unfriend btUnfriend">
                            <!--<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>-->
                        </button>
                        <ul class="joms-dropdown">
                            <li>
                                <a href="javascript:" onclick="showUnfriend('<?php echo $profile->id ?>')">
                                    <!--<a href="javascript:">-->
                                    <span><?php echo JText::_('COM_COMMUNITY_FRIENDS_REMOVE'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:" onclick="joms.api.userBlock('<?php echo $profile->id; ?>');">
                                    <span><?php echo JText::_('COM_COMMUNITY_BLOCK');?></span>
                                </a>
                            </li>
                        </ul>
                    <?php } else if ( !$beingBlocked ) { ?>
                        <?php if ( $isWaitingApproval ) { ?>
                            <a href="javascript:" onclick="joms.api.friendAdd('<?php echo $profile->id;?>')">
                                <button type="button" class="joms-button--primary button--small btn-friendrq">
                                    <!--<span class="glyphicon glyphicon-time" aria-hidden="true"></span>-->
                                </button>
                            </a>
                        <?php } else if ( $isWaitingResponse ) { ?>
                            <a href="javascript:" onclick="joms.api.friendResponse('<?php echo $profile->id;?>')">
                                <button type="button" class="joms-button--primary button--small btn-friendrq">
                                    <!--<span class="glyphicon glyphicon-time" aria-hidden="true"></span>-->
                                </button>
                            </a>
                        <?php } else if ($blockingUser) { ?>
                            <a href="javascript:" onclick="joms.api.userUnblock('<?php echo $profile->id; ?>');">
                                <button type="button" class="btBlock">
                                    <?php echo JText::_('COM_COMMUNITY_UNBLOCK'); ?>
                                </button>
                            </a>
                        <?php } else { ?>
                            <!--<a href="javascript:" onclick="joms.api.friendAdd('<?php echo $profile->id;?>')">-->
                            <a href="javascript:">
                                <button type="button" class="joms-button--primary button--small btn-friend btAddFriend">
                                    <!-- <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>  -->
                                </button>
                            </a>
                        <?php } ?>
                    <?php } ?>

                    <!-- Send Message button -->
                    <?php if ( $config->get('enablepm') ) { ?>
                        <a href="javascript:" class="joms-focus__button--message" onclick="<?php echo $sendMsg; ?>">
                            <?php echo JText::_('COM_COMMUNITY_INBOX_SEND_MESSAGE'); ?>
                        </a>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php if ($loggedIn) { ?>
                <?php if ($isMine || COwnerHelper::isCommunityAdmin()) { ?>
                    <?php if ($inEditingProcess) { ?>
                        <div class="joms-focus__button--options--mobile">
                            <a href="javascript:">
                                <div class="cover-change-icon">
                                    <img src="images/icons/photoicon.png" class="cover-change-icon-img" />
                                    <span>COVER</span>
                                </div>
                            </a>
                            <!-- No need to populate menus as it is cloned from mobile version. -->
                            <ul class="joms-dropdown"></ul>
                        </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>

            <div class="joms-avatar--focus">
                <a <?php if ( !$profile->defaultAvatar && $profile->avatarAlbum ) { ?>
                <?php } ?>>
                    <img class="avatar-image" <?php if (($isMine || COwnerHelper::isCommunityAdmin()) && ($inEditingProcess)) echo 'onclick="joms.api.avatarChange(\'profile\', \''.$profile->id.'\', arguments && arguments[0]); return false; "' ?> src="<?php echo $user->getAvatar() . '?_=' . time(); ?>" alt="<?php echo $this->escape( $user->getDisplayName() ); ?>">
                    <?php if (($isMine || COwnerHelper::isCommunityAdmin()) && ($inEditingProcess)) : ?>
                    <div class="photo-icon">
                        <img src="images/icons/photoicon.png" class="photo-icon-img" <?php echo 'onclick="joms.api.avatarChange(\'profile\', \''.$profile->id.'\', arguments && arguments[0]); return false; "' ?>>
                    </div>
                    <?php endif; ?>
                </a>
            </div>

            <?php if ($inEditingProcess) { ?>


            <?php } else { ?>

                <div class="joms-focus__title focus--title">
                    <h2><?php echo $user->getDisplayName(); ?></h2>

                    <div class="joms-focus__header__actions">
                        <div style="color: #fff"><?php echo $occupation; ?></div>
                    </div>

                    <div class="joms-focus__info--desktop">
                        <?php echo JHTML::_('string.truncate', $this->escape(strip_tags($profileFields)), 140); ?>
                    </div>
                </div>
            <?php } ?>



            <div class="joms-focus__actions__wrapper">

                <div class="joms-focus__actions--desktop">
                    <?php if ( !$isMine ) { ?>

                        <!-- Friending buton -->
                        <?php if ( $isFriend ) { ?>
                            <a href="javascript:" class="joms-focus__button--add btUnfriend" onclick="joms.api.friendRemove('<?php echo $profile->id;?>')">
                            <!--<a href="javascript:" class="joms-focus__button--add btUnfriend">-->
                                <?php echo JText::_('COM_COMMUNITY_FRIENDS_REMOVE'); ?>
                            </a>
                        <?php } else if ( !$beingBlocked ) { ?>
                            <?php if ( $isWaitingApproval ) { ?>
                                <a href="javascript:" class="joms-focus__button--add" onclick="joms.api.friendAdd('<?php echo $profile->id;?>')">
                                    <?php echo JText::_('COM_COMMUNITY_PROFILE_PENDING_FRIEND_REQUEST'); ?>
                                </a>
                            <?php } else if ( $isWaitingResponse ) { ?>
                                <a href="javascript:" class="joms-focus__button--add" onclick="joms.api.friendResponse('<?php echo $profile->id;?>')">
                                    <?php echo JText::_('COM_COMMUNITY_PROFILE_PENDING_FRIEND_REQUEST'); ?>
                                </a>
                            <?php } else { ?>
                                <a href="javascript:" class="joms-focus__button--add" onclick="joms.api.friendAdd('<?php echo $profile->id;?>')">
                                    <?php echo JText::_('COM_COMMUNITY_PROFILE_ADD_AS_FRIEND'); ?>
                                </a>
                            <?php } ?>
                        <?php } ?>

                        <!-- Send Message button -->
                        <?php if ( $config->get('enablepm') ) { ?>
                            <a href="javascript:" class="joms-focus__button--message" onclick="<?php echo $sendMsg; ?>">
                                <?php echo JText::_('COM_COMMUNITY_INBOX_SEND_MESSAGE'); ?>
                            </a>
                        <?php } ?>


                    <?php } ?>
                </div>

                <div class="joms-focus__header__actions--desktop">

                    <a class="joms-button--viewed nolink" title="<?php echo JText::sprintf( $user->getViewCount() > 0 ? 'COM_COMMUNITY_VIDEOS_HITS_COUNT_MANY' : 'COM_COMMUNITY_VIDEOS_HITS_COUNT', $user->getViewCount() ); ?>">
                        <svg viewBox="0 0 16 16" class="joms-icon">
                            <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-eye"></use>
                        </svg>
                        <span><?php echo number_format($user->getViewCount()) ;?></span>
                    </a>

                    <?php if ($config->get('enablesharethis') == 1) { ?>
                        <a class="joms-button--shared" title="<?php echo JText::_('COM_COMMUNITY_SHARE_THIS'); ?>"
                           href="javascript:" onclick="joms.api.pageShare('<?php echo CRoute::getExternalURL('index.php?option=com_community&view=profile&userid=' . $profile->id); ?>')">
                            <svg viewBox="0 0 16 16" class="joms-icon">
                                <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-redo"></use>
                            </svg>
                        </a>
                    <?php } ?>

                    <?php if ($enableReporting) { ?>
                        <a class="joms-button--viewed" title="<?php echo JText::_('COM_COMMUNITY_REPORT_USER'); ?>"
                           href="javascript:" onclick="joms.api.userReport('<?php echo $user->id; ?>');">
                            <svg viewBox="0 0 16 16" class="joms-icon">
                                <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-warning"></use>
                            </svg>
                        </a>
                    <?php } ?>

                </div>

            </div>

        </div>

        <div class="joms-focus__actions--reposition">
            <input type="button" class="joms-button--neutral" data-ui-object="button-cancel" value="<?php echo JText::_('COM_COMMUNITY_CANCEL'); ?>"> &nbsp;
            <input type="button" class="joms-button--primary" data-ui-object="button-save" value="<?php echo JText::_('COM_COMMUNITY_SAVE'); ?>">
        </div>

        <?php if ($loggedIn) { ?>
            <div class="joms-focus__button--options--desktop">
                <a href="javascript:" data-ui-object="joms-dropdown-button">
                    <svg viewBox="0 0 16 16" class="joms-icon">
                        <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-cog"></use>
                    </svg>
                </a>
                <!-- No need to populate menus as it is cloned from mobile version. -->
                <ul class="joms-dropdown"></ul>
            </div>
        <?php } ?>

    </div>

    <div class="joms-focus__actions">
        <ul class="joms-dropdown">
            <?php if ($isMine || COwnerHelper::isCommunityAdmin()) { ?>
                <li><a href="javascript:" data-propagate="1" onclick="joms.api.coverReposition('profile', '<?php echo $profile->id; ?>');"><?php echo JText::_('COM_COMMUNITY_REPOSITION_COVER'); ?></a></li>
                <li><a href="javascript:" onclick="joms.api.coverChange('profile', '<?php echo $profile->id; ?>');"><?php echo JText::_('COM_COMMUNITY_CHANGE_COVER'); ?></a></li>

            <?php } ?>

            <?php if (!$isMine) { ?>

                <?php if($isFriend) { ?>
                    <li class="btUnfriend"><a href="javascript:" onclick="joms.api.friendRemove('<?php echo $profile->id;?>')"><?php echo JText::_('COM_COMMUNITY_FRIENDS_REMOVE'); ?></a></li>
                <?php } ?>
                <?php if ($beingBlocked) { ?>
                    <li><a href="javascript:" onclick="joms.api.userUnblock('<?php echo $profile->id; ?>');"><?php echo JText::_('COM_COMMUNITY_UNBLOCK_USER'); ?></a></li>
                <?php } else { ?>
                    <li><a href="javascript:" onclick="joms.api.userBlock('<?php echo $profile->id; ?>');"><?php echo JText::_('COM_COMMUNITY_BLOCK_USER'); ?></a></li>
                <?php } ?>

                <?php if (COwnerHelper::isCommunityAdmin()) { ?>
                    <?php if ($blocked) { ?>
                        <li><a href="javascript:" onclick="joms.api.userUnban('<?php echo $profile->id; ?>');"><?php echo JText::_('COM_COMMUNITY_UNBAN_USER'); ?></a></li>
                    <?php } else { ?>
                        <li><a href="javascript:" onclick="joms.api.userBan('<?php echo $profile->id; ?>');"><?php echo JText::_('COM_COMMUNITY_BAN_USER'); ?></a></li>
                    <?php } ?>
                <?php } ?>

            <?php } ?>
        </ul>

    </div>
    <div class="mnModalUnfriend" id="mytitle">
        <div class="rmPopupUnfriend changePosition">
            <p class="titleUnfriend"></p>
            <button class="btRMCancel" type="button"><?php echo JText::_('COM_COMMUNITY_CANCEL'); ?></button>
            <button class="btRMYes" type="button"><?php echo JText::_('COM_COMMUNITY_UNFRIEND_FRIENDS'); ?></button>
        </div>
    </div>

</div>

<style>
    .joms-popup__action .joms-button--small {
        line-height: 15px !important;
    }
</style>

<script>

    jQuery(document).ready(function() {
        <?php if ( !$isMine ) { ?>
        jQuery(document).delegate('.btAddFriend', 'click', function(e) {
            e.preventDefault();
            joms.ajax({
                func: 'friends,ajaxSaveFriend',
                data: [[[ 'msg', 'Hi, I\'d like to add you as friend.' ], [ 'userid', <?php echo $profile->id ?> ]]],

                beforeSend: function() {
                    lgtCreatePopup('', {'content': 'Loading...'});
                },
                callback: function( json ) {
                    lgtRemovePopup();
                    console.log(json);
                    jQuery('.joms--topright').load(location.href + " .joms--topright");
                },
                error: function() {
                    lgtRemovePopup();
                }
            });
        });
/*
        jQuery(document).delegate('.btUnfriend', 'click', function(e) {
            e.preventDefault();
            joms.ajax({
                func: 'friends,ajaxRemoveFriend',
                data: [ <?php echo $profile->id ?> ],
                beforeSend: function() {
                    lgtCreatePopup('', {'content': 'Loading...'});
                },
                callback: function( json ) {
                    lgtRemovePopup();
                    console.log(json);
                    location.reload();
                },
                error: function() {
                    lgtRemovePopup();
                }
            });
        });
*/
        jQuery(document).delegate('.btUnfriend', 'click', function(e) {
            jQuery(this).next('.joms-dropdown').toggle();
        });
        <?php } ?>

        jQuery("#profession").on("change paste keyup", function() {
            jQuery("#field19").val(jQuery("#profession").val());
        });
        
        jQuery(document).on('click', '.joms-focus__button--options--mobile', function() {
            jQuery(this).children('.joms-dropdown').toggle();
        });

    }(jQuery));

    // Clone menu from mobile version to desktop version.
    (function( w ) {
        w.joms_queue || (w.joms_queue = []);
        w.joms_queue.push(function() {
            var src = joms.jQuery('.joms-focus__actions ul.joms-dropdown'),
                clone = joms.jQuery('.joms-focus__button--options--mobile ul.joms-dropdown');

            clone.html( src.html() );
        });
    })( window );

    function showUnfriend(id){
        var name = '<?php echo addslashes($user->getDisplayName()); ?>';
        jQuery('.titleUnfriend').html('Are you sure you want to unfriend '+ name+'?');
        jQuery('.rmPopupUnfriend').attr('id',id);
        jQuery('.rmPopupUnfriend').show();
        jQuery('.popover').hide();
        jQuery('.mnModalUnfriend').css('display','block');
        jQuery('body').addClass('overlay2');
    }
    jQuery('.rmPopupUnfriend .btRMCancel').click(function(){
        jQuery('.rmPopupUnfriend').hide();
        jQuery('.mnModalUnfriend').css('display','none');
        jQuery('body').removeClass('overlay2');
    });
    jQuery('.rmPopupUnfriend .btRMYes').click(function(){
        var idFriend = jQuery('.rmPopupUnfriend').attr('id');
        jQuery.ajax({
            type: 'post',
            url: 'index.php?option=com_community&view=friends&task=ajaxRemoveFriend',
            data: {id: idFriend},
            success: function(data) {
                window.location.reload();
            }
        });
        jQuery('.rmPopupUnfriend').css('display','none');
        jQuery('body').addClass('overlay2');
        jQuery('.mnNotification').html('Loading...').fadeIn();
    });

</script>
