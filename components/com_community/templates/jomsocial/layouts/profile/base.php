<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') OR DIE();
$session = JFactory::getSession();
$device = $session->get('device');
$is_mobile = $device == "mobile" ? true : false;
?>

<div class="joms-body">
    <?php if($isMine) { ?>
<!--        <div class="joms-tab__bar">-->
<!--            <a class="joms-profile--information" href="#joms-profile--information">--><?php //echo JText::_('COM_COMMUNITY_MY_PROFILE'); ?><!--</a>-->
<!--            <a class="joms-profile--account" href="#joms-profile--account">--><?php //echo JText::_('COM_COMMUNITY_MY_ACCOUNT'); ?><!--</a>-->
<!--        </div>-->
    <?php } ?>
    
        <div id="joms-profile--information" class="joms-tab__content">
        <div id="js_profile_top" class="joms-module__wrapper"><?php $this->renderModules( 'js_profile_top' ); ?></div>
        <div id="js_profile_top_stacked" class="joms-module__wrapper--stacked"><?php $this->renderModules('js_profile_top_stacked'); ?></div>
        <?php if($isMine) { ?>
        <div id="js_profile_mine_top" class="joms-module__wrapper"><?php $this->renderModules( 'js_profile_mine_top' ); ?> </div>
        <div id="js_profile_mine_top_stacked" class="joms-module__wrapper--stacked"><?php $this->renderModules('js_profile_mine_top_stacked'); ?></div>
        <?php } ?>

        <!-- begin: focus area -->
        <?php $this->view('profile')->modProfileUserinfo($limitstart, $limit); ?>
        <!-- end: focus area -->

        <div class="joms-sidebar">

            <div id="js_side_top" class="joms-module__wrapper"><?php $this->renderModules( 'js_side_top' ); ?></div>
            <div id="js_side_top_stacked" class="joms-module__wrapper--stacked"><?php $this->renderModules('js_side_top_stacked'); ?></div>
            <div id="js_profile_side_top" class="joms-module__wrapper"><?php $this->renderModules( 'js_profile_side_top' ); ?> </div>
            <div id="js_profile_side_top_stacked" class="joms-module__wrapper--stacked"><?php $this->renderModules('js_profile_side_top_stacked'); ?></div>

            <div class="joms-module__wrapper"><?php echo $sidebarTop; ?></div>
            <div class="js_profile_side_top"><?php echo $sidebarTopStacked; ?></div>

            <?php if($isMine) { ?>
                <div id="js_profile_mine_side_top" class="joms-module__wrapper"><?php $this->renderModules( 'js_profile_mine_side_top' ); ?></div>
                <div id="js_profile_mine_side_top_stacked" class="joms-module__wrapper--stacked"><?php $this->renderModules('js_profile_mine_side_top_stacked'); ?></div>
            <?php } ?>

            <?php echo $this->view('profile')->modProfileUserVideo(); ?>

            <div id="js_profile_side_middle" class="joms-module__wrapper"><?php $this->renderModules( 'js_profile_side_middle' ); ?></div>
            <div id="js_profile_side_middle_stacked" class="joms-module__wrapper--stacked"><?php $this->renderModules('js_profile_side_middle_stacked'); ?></div>

            <?php if($isMine) { ?>
                <div id="js_profile_mine_side_middle" class="joms-module__wrapper"><?php $this->renderModules( 'js_profile_mine_side_middle' ); ?></div>
                <div id="js_profile_mine_side_middle_stacked" class="joms-module__wrapper--stacked"><?php $this->renderModules('js_profile_mine_side_middle_stacked'); ?></div>
            <?php } ?>

            <?php if($isMine) { ?>
                <div id="js_profile_mine_side_bottom" class="joms-module__wrapper"><?php  $this->renderModules( 'js_profile_mine_side_bottom' );?></div>
                <div id="js_profile_mine_side_bottom_stacked" class="joms-module__wrapper--stacked"><?php $this->renderModules('js_profile_mine_side_bottom_stacked'); ?></div>
            <?php } ?>
            <div class="joms-module__wrapper"><?php echo $sidebarBottom;?></div>
            <div class="js_profile_side_bottom"><?php echo $sidebarBottomStacked; ?></div>


            <div id="js_profile_side_bottom" class="joms-module__wrapper"><?php $this->renderModules( 'js_profile_side_bottom' ); ?></div>
            <div id="js_profile_side_bottom_stacked" class="joms-module__wrapper--stacked"><?php $this->renderModules('js_profile_side_bottom_stacked'); ?></div>
            <div id="js_side_bottom" class="joms-module__wrapper"><?php $this->renderModules( 'js_side_bottom' ); ?></div>
            <div id="js_side_bottom_stacked" class="joms-module__wrapper--stacked"><?php $this->renderModules('js_side_bottom_stacked'); ?></div>

        </div>

        <div class="joms-main">

            <div id="js_profile_feed_top" class="joms-module__wrapper"><?php $this->renderModules( 'js_profile_feed_top' ); ?></div>
            <div id="js_profile_feed_top_stacked" class="joms-module__wrapper--stacked"><?php $this->renderModules('js_profile_feed_top_stacked'); ?></div>

            <div data-ui-object="frontpage-main">
                <?php $this->view('profile')->modProfileUserstatus(); ?>
                <div class="joms-middlezone" data-ui-object="joms-tabs">
                    <?php echo $content; ?>
                </div>
            </div>

            <div id="js_profile_feed_bottom" class="joms-module__wrapper"><?php $this->renderModules( 'js_profile_feed_bottom' ); ?></div>
            <div id="js_profile_feed_bottom_stacked" class="joms-module__wrapper--stacked"><?php $this->renderModules('js_profile_feed_bottom_stacked'); ?></div>

        </div>

        <?php if($isMine) { ?>
            <div id="js_profile_mine_bottom" class="joms-module__wrapper"><?php $this->renderModules( 'js_profile_mine_bottom' ); ?></div>
            <div id="js_profile_mine_bottom_stacked" class="joms-module__wrapper--stacked"><?php $this->renderModules('js_profile_mine_bottom_stacked'); ?></div>
        <?php } ?>

        <div id="js_profile_bottom" class="joms-module__wrapper"><?php $this->renderModules( 'js_profile_bottom' ); ?></div>
        <div id="js_profile_bottom_stacked" class="joms-module__wrapper--stacked"><?php $this->renderModules('js_profile_bottom_stacked'); ?></div>
    </div>
    <div id="joms-profile--account" class="joms-tab__content" style="display:none;">
        
           <!-- start here -->

        <form name="jsform-profile-edit" id="frmSaveDetailProfile" action="<?php echo CRoute::getURI(); ?>"
              method="POST" class="js-form" onsubmit="return joms_validate_form( this );">

            <div class="js-form_account">
                <?php
                if($device == 'mobile')
                {
                ?>
                    <!--    MOBILE  -->
                    <!-- email -->
                    <div class="joms-form__group" for="email">
                        <span><?php echo JText::_('COM_COMMUNITY_EMAIL'); ?></span>
                        <input type="text" class="joms-input" id="jsemail" name="jsemail"
                               value="<?php echo $this->escape($user->get('email')); ?>"/>
                        <input type="hidden" id="email" name="email" value="<?php echo $user->get('email'); ?>"/>
                        <input type="hidden" id="emailpass" name="emailpass" id="emailpass"
                               value="<?php echo $this->escape($user->get('email')); ?>"/>
                    </div>

                    <?php if (isset($associated) && !$associated) : ?>
                        <?php if ($user->get('password')) : ?>
                            <!-- old password -->
                            <div class="joms-form__group" for="password">
                                <span><?php echo JText::_('COM_COMMUNITY_OLD_PASSWORD'); ?></span>
                                <input id="jspassword_old" name="jspassword_old" class="joms-input" type="password" value=""/>
                            </div>
                            <!-- password -->
                            <div class="joms-form__group" for="password">
                                <span><?php echo JText::_('COM_COMMUNITY_NEW_PASSWORD'); ?></span>
                                <input id="jspassword" name="jspassword" class="joms-input" type="password" value=""/>
                            </div>
                            <!-- verify password -->
                            <div class="joms-form__group" for="verifypassword">
                                <span><?php echo JText::_('COM_COMMUNITY_RE_ENTER_PASSWORD'); ?></span>
                                <input id="jspassword2" name="jspassword2" class="joms-input" type="password" value=""/>
                            </div>

                        <?php endif; ?>
                    <?php endif; ?>
                <?php } else { ?>
                            <!--    TABLET  -->
                    <!-- email -->
                    <div class="js-foms-email-tab">
                        <div class="joms-form__group" for="email">
                            <span><?php echo JText::_('COM_COMMUNITY_EMAIL'); ?></span>
                            <input type="text" class="joms-input" id="jsemail" name="jsemail"
                                   value="<?php echo $this->escape($user->get('email')); ?>"/>
                            <input type="hidden" id="email" name="email" value="<?php echo $user->get('email'); ?>"/>
                            <input type="hidden" id="emailpass" name="emailpass" id="emailpass"
                                   value="<?php echo $this->escape($user->get('email')); ?>"/>
                        </div>
                    </div>
                    <div class="js-foms-pass-tab">
                        <?php if (isset($associated) && !$associated) : ?>
                            <?php if ($user->get('password')) : ?>
                                <!-- old password -->
                                <div class="joms-form__group" for="password">
                                    <span><?php echo JText::_('COM_COMMUNITY_OLD_PASSWORD'); ?></span>
                                    <input id="jspassword_old" name="jspassword_old" class="joms-input" type="password" value=""/>
                                </div>
                                <!-- password -->
                                <div class="joms-form__group" for="password">
                                    <span><?php echo JText::_('COM_COMMUNITY_NEW_PASSWORD'); ?></span>
                                    <input id="jspassword" name="jspassword" class="joms-input" type="password" value=""/>
                                </div>
                                <!-- verify password -->
                                <div class="joms-form__group" for="verifypassword">
                                    <span><?php echo JText::_('COM_COMMUNITY_RE_ENTER_PASSWORD'); ?></span>
                                    <input id="jspassword2" name="jspassword2" class="joms-input" type="password" value=""/>
                                </div>

                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                <?php } ?>
            </div>
            <div class="js-form_detail_profile">
                <?php if (!empty($beforeFormDisplay)) { ?>
                    <div class="before-form">
                        <?php echo $beforeFormDisplay; ?>
                    </div>
                <?php } ?>

                <!-- username  -->
    <!--            <div class="joms-form__group" for="username" style="margin-top: 20px">
                    <span><?php // echo JText::_('COM_COMMUNITY_PROFILE_USERNAME'); ?></span>
                    <input class="joms-input" type="text" name="username" <?php // echo ($canEditUsername) ? 'data-required="true" data-validation="username"' : 'disabled' ?>
                           value="<?php // echo $this->escape($user->get('username')); ?>"
                           data-current="<?php // echo $this->escape($user->get('username')); ?>">
                </div>-->

                <!-- fullname -->
                <?php //if (!$isUseFirstLastName) { ?>
                    <div class="joms-form__group" for="fullname" style="display: none;"">
                        <span><?php echo JText::_('COM_COMMUNITY_PROFILE_FULLNAME'); ?></span>
                        <input class="joms-input" type="text" id="name" name="name" size="40"
                               value="<?php echo $this->escape($user->get('name')); ?>"/>
                    </div>
                <?php //} ?>

                <!-- contact informations -->
                <?php   //var_dump($fields2);
                    foreach ($fields2 as $name => $fieldGroup) {                    
                        // if there is no field for this group, move to next.
                        if (count($fieldGroup) == 0) {
                            continue;
                        } 
                        $arr_file_show = array('');
                        if ($name == 'Contact Information') {
    //                        if ($name != 'ungrouped') {
                                ?>

                                <!--<legend class="joms-form__legend"><?php // echo JText::_($name); ?></legend>-->

                            <?php
    //                        }
                            ?>

                            <?php
                            foreach ($fieldGroup as $f) {
                                $f = JArrayHelper::toObject($f);
                                // DO not escape 'SELECT' values. Otherwise, comparison for
                                // selected values won't work
                                // Chris - add tag for interest

                                if ($f->type != 'select') {
                                    $f->value = $this->escape($f->value);
                                }
                                ?>

                                <div class="joms-form__group has-privacy" for="field<?php echo $f->id; ?>">
                                    <span title="<?php echo JText::_($f->tips)?>"><?php echo JText::_($f->name); ?></span>
                                    <?php echo CProfileLibrary::getFieldHTML($f, ''); ?>
                                    <?php //echo CPrivacy::getHTML('privacy' . $f->id, $f->access); ?>
                                </div>

                            <?php
                            }
                            ?>

                    <?php
                        }
                    }
                ?>

            </div>
            <div class="joms-form__group has-privacy" style="padding-bottom: 30px; padding-left: 20px; padding-right: 30px">
                <span class="fieldName" title="<?php echo JText::_('COM_COMMUNITY_MY_PREFERENCE'); ?>"><?php echo JText::_('COM_COMMUNITY_MY_PREFERENCE'); ?></span>
                <span class="email-notifications"><span class="email-notifications-text"><br><?php echo JText::_('COM_COMMUNITY_EMAIL_NOTIFICATIONS'); ?></span>

                <a href="<?php echo CRoute::_('index.php?option=com_community&view=profile&task=preferences'); ?>"><span class="arrow" style="float: right">></span></a>
                </span>
            </div>
            <?php if (isset($params)) {
                //Chris: disable language 
                //echo $params->render('params');
            }
            ?>

            <div class="joms-form__group" for="dst-offset">
                <span><?php //echo JText::_('COM_COMMUNITY_DAYLIGHT_SAVING_OFFSET'); ?></span>
                <?php //echo $offsetList; ?>
            </div>

            <!-- group buttons -->
            <input type="hidden" name="id" value="<?php echo $user->get('id'); ?>"/>
            <input type="hidden" name="gid" value="<?php echo $user->get('gid'); ?>"/>
            <input type="hidden" name="option" value="com_community"/>
            <input type="hidden" name="view" value="profile"/>
            <input type="hidden" name="task" value="edit"/>
            <input type="hidden" id="password" name="password"/>
            <input type="hidden" id="password2" name="password2"/>


            <?php if(CSystemHelper::tfaEnabled()){?>
                <div class="control-group">
                    <div class="control-label">
                        <label id="jform_twofactor_method-lbl" for="jform_twofactor_method" class="hasTooltip"
                               title="<strong><?php echo JText::_('COM_COMMUNITY_SETUP_AUTH_CONFIG') ?></strong><br /><?php echo JText::_('COM_COMMUNITY_SETUP_AUTH_CONFIG') ?>">
                            <?php echo JText::_('COM_COMMUNITY_SETUP_AUTH_CONFIG'); ?>
                        </label>
                    </div>
                    <div class="controls">
                        <?php echo JHtml::_('select.genericlist', Usershelper::getTwoFactorMethods(), 'jform[twofactor][method]', array('onchange' => 'Joomla.twoFactorMethodChange()'), 'value', 'text', $otpConfig->method, 'jform_twofactor_method', false) ?>
                    </div>
                </div>
                <div id="com_users_twofactor_forms_container">
                    <?php foreach($tfaForm as $form): ?>
                        <?php $style = $form['method'] == $otpConfig->method ? 'display: block' : 'display: none'; ?>
                        <div id="com_users_twofactor_<?php echo $form['method'] ?>" style="<?php echo $style; ?>">
                            <?php echo $form['form'] ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php } ?>

            <div class="joms-form__group" style="padding-right: 20px">
                <span></span>
                <input type="hidden" name="action" value="detail"/>
                <?php echo JHTML::_('form.token'); ?>
                <input type="button" class="joms-button--primary joms-button--smaller js-button-left"
                       value="<?php echo JText::_('COM_COMMUNITY_CANCEL_BUTTON'); ?>" onclick="redirectToProfilePage()"/>
                <input type="submit" class="joms-button--primary joms-button--smaller js-button-right"
                       value="<?php echo JText::_('COM_COMMUNITY_SAVE_BUTTON'); ?>"/>
            </div>

        </form>

        <!-- end here -->
            
    </div>
    
</div>

<script type="text/javascript">
 
// set initial opened tab based on the href
//(function( href ) {
    //    var id = 'joms-profile--' + ( href.match(/#detailSet/) ? 'account' : 'information' );
//    document.getElementById( id ).style.display = '';
//    document.getElementsByClassName( id )[0].className += ' active';
//})( window.location.href );

jQuery(document).ready(function() {
    var title_acc = '<?php echo JText::_('COM_COMMUNITY_MY_ACCOUNT'); ?>';
    var url      = window.location.href;
    if (url.match(/#detailSet/)) jQuery('.navbar-header .navbar-title span').html(title_acc);
//    jQuery(".joms-tab__bar > a").click(function(){
//        var title = jQuery(this).text();
//        jQuery('.navbar-header .navbar-title span').html(title);
//    });
});

</script>