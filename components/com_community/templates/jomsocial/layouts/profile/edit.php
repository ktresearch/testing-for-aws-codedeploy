<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();

if (CSystemHelper::tfaEnabled()) {
    // Include the component HTML helpers.
    JHtml::_('behavior.formvalidator');

    JFactory::getDocument()->addScriptDeclaration("
        Joomla.submitbutton = function(task)
        {
            if (task == 'user.cancel' || document.formvalidator.isValid(document.getElementById('user-form')))
            {
                Joomla.submitform(task, document.getElementById('user-form'));
            }
        };

        Joomla.twoFactorMethodChange = function(e)
        {
            var selectedPane = 'com_users_twofactor_' + jQuery('#jform_twofactor_method').val();

            jQuery.each(jQuery('#com_users_twofactor_forms_container>div'), function(i, el) {
                if (el.id != selectedPane)
                {
                    jQuery('#' + el.id).hide(0);
                }
                else
                {
                    jQuery('#' + el.id).show(0);
                }
            });
        };
    ");
}

$document = JFactory::getDocument();
$user = CFactory::getUser();
//$document->addStyleSheet($this->baseurl .'/components/com_community/templates/jomsocial/assets/css/custom.css');

$session = JFactory::getSession();
$device = $session->get('device');
$is_mobile = $device == 'mobile' ? true : false;

$firstName = (strpos($user->getName(), ' ')) ? substr($user->getName(), 0, strpos($user->getName(), ' ')) : $user->getName();
$showArray = array('status', 'about me', 'interest');

$app = JFactory::getApplication();

$tmpFileName = 'tmp_' . $user->username . '.json';
$tmp_file = dirname($app->getCfg('dataroot')) . '/temp/tmpkey/' . $tmpFileName;

$tokenkey = file_get_contents($tmp_file);
$datas = base64_decode($tokenkey);
$data = explode(':', $datas);
?>

<!--<script src="--><?php //echo $this->baseurl ?><!--./components/com_community/templates/jomsocial/assets/js/jquery.tagsinput.js"></script>-->
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo $this->baseurl ?><!--./components/com_community/templates/jomsocial/assets/css/jquery.tagsinput.css" />-->

<div class="joms-page">

    <?php //echo $submenu; ?>

    <!--    <div class="joms-tab__bar">-->
    <!--        <a class="joms-profile--information" href="#joms-profile--information">-->
    <?php //echo JText::_('COM_COMMUNITY_MY_PROFILE'); ?><!--</a>-->
    <!--        <a class="joms-profile--account" href="#joms-profile--account">-->
    <?php //echo JText::_('COM_COMMUNITY_MY_ACCOUNT'); ?><!--</a>-->
    <!--    </div>-->

    <div id="joms-profile--information" class="joms-tab__content">


        <?php if ($showProfileType) { ?>
            <div>
                <?php if ($multiprofile->id != COMMUNITY_DEFAULT_PROFILE) { ?>
                    <?php echo JText::sprintf('COM_COMMUNITY_CURRENT_PROFILE_TYPE', $multiprofile->name); ?>
                <?php } else { ?>
                    <?php echo JText::_('COM_COMMUNITY_CURRENT_DEFAULT_PROFILE_TYPE'); ?>
                <?php } ?>
                <a href="<?php echo CRoute::_('index.php?option=com_community&view=multiprofile&task=changeprofile'); ?>"><?php echo JText::_('COM_COMMUNITY_CHANGE'); ?></a>
            </div>
        <?php } ?>

        <form name="jsform-profile-edit" id="frmSaveProfile" action="<?php echo CRoute::getURI(); ?>" method="POST"
              class="js-form">
            <!-- begin: focus area -->
            <?php $this->view('profile')->modProfileUserinfo(); ?>
            <!-- end: focus area -->
            <!-- fullname -->
            <?php // if (!$isUseFirstLastName) { ?>
            <!--                <div class="joms-form__group input-fullname" for="fullname" style="display: none;">
                    <span><?php // echo JText::_('COM_COMMUNITY_PROFILE_FULLNAME'); ?></span>
                    <input class="joms-input" type="text" id="name" name="name" size="40"
                           value="<?php // echo $this->escape($user->get('name')); ?>"/>
                </div>-->
            <?php // } ?>
            <script>
                //                jQuery(document).ready(function() {
                //                    jQuery('#joms-profile--information .joms-focus__cover .joms-focus__header .joms-focus__title').click(function() {
                //                        jQuery('.input-fullname').fadeToggle();
                //                    });
                //                });
            </script>

            <div class="joms-form__group inputName">
                <span class="fieldName"><?php echo JText::_('COM_COMMUNITY_NAME');?><span class="red">*</span></span>
                <input type="text" name="name" id="name" value="<?php echo $user->id ? $user->getDisplayName() : ''; ?>" class="joms-input mandatory"/>
            </div>

            <?php
            foreach ($fields as $name => $fieldGroup) {

                // if there is no field for this group, move to next.
                if (count($fieldGroup) == 0) {
                    continue;
                }

                if ($name == 'Basic Information' || $name == 'Other Information') {

                    foreach ($fieldGroup as $f) {
                        $f = JArrayHelper::toObject($f);
                        // DO not escape 'SELECT' values. Otherwise, comparison for
                        // selected values won't work
                        // Chris - add tag for interest
                        $css = '';

                        if (($f->required == 1) && is_null($f->value)) $css = '';

                        if ($f->type != 'select') {
                            $f->value = $this->escape($f->value);
                        }
                        ?>

                        <div class="joms-form__group has-privacy" id="<?php echo $f->fieldcode; ?>"
                             for="field<?php echo $f->id; ?>"
                             style="">
                            <span class="fieldName" title="<?php echo JText::_($f->tips) ?>"><?php
                                switch (strtolower($f->name)) {
                                    case 'about me':
//                                        echo $firstName.'\'s Profile';
                                        echo JText::_('COM_COMMUNITY_PROFILE');
                                        break;
                                    case 'interest':
                                        $interest_value = $f->value;
                                        echo JText::_('COM_COMMUNITY_INTERESTS');
                                        break;
                                }
                                ?></span>
                            <?php echo CProfileLibrary::getFieldHTML($f, ''); ?>
                            <?php //echo CPrivacy::getHTML('privacy' . $f->id, $f->access);
                            ?>
                        </div>

                        <?php


                    }
                }
                ?>

                <?php
            }
            ?>

            <div class="clear"></div>

            <!-- <div class="my-preference">
                <span class="close">X</span>
                <label>Indicate your preference if you wish to receive these notifications through e-mail:</label>
            </div> -->

            <?php if (!empty($afterFormDisplay)) { ?>
                <?php echo $afterFormDisplay; ?>
            <?php } ?>

            <div class="joms-form__group" style="display: none;">
                <span></span>
                <?php echo JHTML::_('form.token'); ?>
                <!-- <input type="button" class="btCancel" value="<?php echo JText::_('JCANCEL'); ?>" onclick="redirectToProfilePage()" />
                <input type="submit"
                       class="joms-button--primary joms-button--full-small"
                       value="<?php //echo JText::_('COM_COMMUNITY_SAVE_CHANGES_BUTTON'); ?>"/> -->

                <!--                <input type="button" class="joms-button--primary joms-button--smaller js-button-left"-->
                <!--                       value="-->
                <?php //echo JText::_('COM_COMMUNITY_CANCEL_BUTTON'); ?><!--" onclick="redirectToProfilePage()"/>-->
                <!--                <input type="submit" class="joms-button--primary joms-button--smaller js-button-right"-->
                <!--                       value="--><?php //echo JText::_('COM_COMMUNITY_SAVE_BUTTON'); ?><!--"/>-->
            </div>
            <!-- </form> -->

    </div>

    <div id="joms-profile--account" class="joms-tab__content">
        <div class="divider"></div>

        <div class="js-form_account">
            <?php
            if ($device == 'mobile') {
                ?>
                <!--    MOBILE  -->
                <!-- email -->
                <div class="joms-form__group" for="email">
                    <span><?php echo JText::_('COM_COMMUNITY_EMAIL'); ?><span class="red">*</span></span>
                    <input type="text" class="joms-input mandatory" id="jsemail" name="jsemail"
                           value="<?php echo $this->escape($user->get('email')); ?>"/>
                    <input type="hidden" id="email" name="email" value="<?php echo $user->get('email'); ?>"/>
                    <input type="hidden" id="emailpass" name="emailpass" class="emailpass"
                           value="<?php echo $this->escape($user->get('email')); ?>"/>
                </div>

                <?php if (!$associated) : ?>
                    <?php if ($user->get('password')) : ?>
                        <!-- old password -->
                        <div class="joms-form__group" for="password">
                            <span><?php echo JText::_('COM_COMMUNITY_OLD_PASSWORD'); ?></span>
                            <input id="jspassword_old" name="jspassword_old" class="joms-input" type="password"
                                   value=""/>
                        </div>
                        <!-- password -->
                        <div class="joms-form__group" for="password">
                            <span><?php echo JText::_('COM_COMMUNITY_NEW_PASSWORD'); ?></span>
                            <input id="jspassword" name="jspassword" class="joms-input" type="password" value=""/>
                        </div>
                        <!-- verify password -->
                        <div class="joms-form__group" for="verifypassword">
                            <span><?php echo JText::_('COM_COMMUNITY_RE_ENTER_PASSWORD'); ?></span>
                            <input id="jspassword2" name="jspassword2" class="joms-input" type="password" value=""/>
                        </div>

                    <?php endif; ?>
                <?php endif; ?>
            <?php } else { ?>
                <!--    TABLET  -->
                <!-- email -->
                <div class="js-foms-email-tab">
                    <div class="joms-form__group" for="email">
                        <span><?php echo JText::_('COM_COMMUNITY_EMAIL'); ?><span class="red">*</span></span>
                        <input type="text" class="joms-input mandatory" id="jsemail" name="jsemail"
                               value="<?php echo $this->escape($user->get('email')); ?>"/>
                        <input type="hidden" id="email" name="email" value="<?php echo $user->get('email'); ?>"/>
                        <input type="hidden" id="emailpass" name="emailpass" class="emailpass"
                               value="<?php echo $this->escape($user->get('email')); ?>"/>
                    </div>
                </div>
                <div class="js-foms-pass-tab">
                    <?php if (!$associated) : ?>
                        <?php if ($user->get('password')) : ?>
                            <!-- old password -->
                            <div class="joms-form__group" for="password">
                                <span><?php echo JText::_('COM_COMMUNITY_OLD_PASSWORD'); ?></span>
                                <input id="jspassword_old" name="jspassword_old" class="joms-input" type="password"
                                       value=""/>
                            </div>
                            <!-- password -->
                            <div class="joms-form__group" for="password">
                                <span><?php echo JText::_('COM_COMMUNITY_NEW_PASSWORD'); ?></span>
                                <input id="jspassword" name="jspassword" class="joms-input" type="password"
                                       value=""/>
                            </div>
                            <!-- verify password -->
                            <div class="joms-form__group" for="verifypassword">
                                <span><?php echo JText::_('COM_COMMUNITY_RE_ENTER_PASSWORD'); ?></span>
                                <input id="jspassword2" name="jspassword2" class="joms-input" type="password"
                                       value=""/>
                            </div>

                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            <?php } ?>
        </div>
        <div class="js-form_detail_profile">
            <?php if (!empty($beforeFormDisplay)) { ?>
                <div class="before-form">
                    <?php echo $beforeFormDisplay; ?>
                </div>
            <?php } ?>

            <!-- username  -->
            <!--            <div class="joms-form__group" for="username" style="margin-top: 20px">
                    <span><?php // echo JText::_('COM_COMMUNITY_PROFILE_USERNAME'); ?></span>
                    <input class="joms-input" type="text" name="username" <?php // echo ($canEditUsername) ? 'data-required="true" data-validation="username"' : 'disabled' ?>
                           value="<?php // echo $this->escape($user->get('username')); ?>"
                           data-current="<?php // echo $this->escape($user->get('username')); ?>">
                </div>-->

            <!-- fullname -->
            <?php //if (!$isUseFirstLastName) { ?>
            <!--                    <div class="joms-form__group" for="fullname">-->
            <!--                        <span>-->
            <?php //echo JText::_('COM_COMMUNITY_PROFILE_FULLNAME'); ?><!--</span>-->
            <!--                        <input class="joms-input" type="text" id="name" name="name" size="40"-->
            <!--                               value="-->
            <?php //echo $this->escape($user->get('name')); ?><!--"/>-->
            <!--                    </div>-->
            <?php //} ?>

            <!-- contact informations -->
            <?php
            foreach ($fields as $name => $fieldGroup) {
                // if there is no field for this group, move to next.
                if (count($fieldGroup) == 0) {
                    continue;
                }
                $arr_file_show = array('');
                if ($name == 'Contact Information') {
                    foreach ($fieldGroup as $f) {
                        $f = JArrayHelper::toObject($f);
                        // DO not escape 'SELECT' values. Otherwise, comparison for
                        // selected values won't work
                        // Chris - add tag for interest

                        if (JText::_($f->name) == "Post Code") continue;

                        if ($f->type != 'select') {
                            $f->value = $this->escape($f->value);
                        }
                        ?>

                        <div class="joms-form__group has-privacy" id="<?php echo $f->fieldcode; ?>"
                             for="field<?php echo $f->id; ?>">
                                    <span title="<?php echo JText::_($f->tips) ?>"><?php
                                        if (JText::_($f->name) == "Number") {
                                            echo JText::_('COM_COMMUNITY_CONTACT_NUMBER_PROFILE');
                                        }
                                        if (JText::_($f->name) == "Address") {
                                            echo JText::_('COM_COMMUNITY_ADDRESS_PROFILE');
                                        }
                                        //                                        if(JText::_($f->name)=="Post Code"){
                                        //                                            echo JText::_('COM_COMMUNITY_POST_CODE_PROFILE');
                                        //                                        }
                                        ?>

                                            </span>
                            <?php echo CProfileLibrary::getFieldHTML($f, ''); ?>
                            <?php //echo CPrivacy::getHTML('privacy' . $f->id, $f->access); ?>
                        </div>


                        <?php
                    }
                    ?>

                    <?php
                }
            }
            ?>

        </div>
        <!--            <div class="joms-form__group has-privacy" style="padding-bottom: 30px; padding-left: 20px; padding-right: 30px">-->
        <!--                <span class="fieldName" title="-->
        <?php //echo JText::_('COM_COMMUNITY_MY_PREFERENCE'); ?><!--">-->
        <?php //echo JText::_('COM_COMMUNITY_MY_PREFERENCE'); ?><!--</span>-->
        <!--                <span class="email-notifications"><span class="email-notifications-text">-->
        <?php //echo JText::_('COM_COMMUNITY_EMAIL_NOTIFICATIONS'); ?><!--</span><span class="arrow">></span></span>-->
        <!--            </div>-->
        <?php if (isset($params)) {
            //Chris: disable language
            //echo $params->render('params');
        }
        ?>

        <div class="joms-form__group" for="dst-offset">
            <span><?php //echo JText::_('COM_COMMUNITY_DAYLIGHT_SAVING_OFFSET'); ?></span>
            <?php //echo $offsetList; ?>
        </div>

        <!-- group buttons -->
        <input type="hidden" name="id" value="<?php echo $user->get('id'); ?>"/>
        <input type="hidden" name="gid" value="<?php echo $user->get('gid'); ?>"/>
        <input type="hidden" name="option" value="com_community"/>
        <input type="hidden" name="view" value="profile"/>
        <input type="hidden" name="task" value="edit"/>
        <input type="hidden" id="password" name="password"/>
        <input type="hidden" id="password2" name="password2"/>


        <?php if (CSystemHelper::tfaEnabled()) { ?>
            <div class="control-group">
                <div class="control-label">
                    <label id="jform_twofactor_method-lbl" for="jform_twofactor_method" class="hasTooltip"
                           title="<strong><?php echo JText::_('COM_COMMUNITY_SETUP_AUTH_CONFIG') ?></strong><br /><?php echo JText::_('COM_COMMUNITY_SETUP_AUTH_CONFIG') ?>">
                        <?php echo JText::_('COM_COMMUNITY_SETUP_AUTH_CONFIG'); ?>
                    </label>
                </div>
                <div class="controls">
                    <?php echo JHtml::_('select.genericlist', Usershelper::getTwoFactorMethods(), 'jform[twofactor][method]', array('onchange' => 'Joomla.twoFactorMethodChange()'), 'value', 'text', $otpConfig->method, 'jform_twofactor_method', false) ?>
                </div>
            </div>
            <div id="com_users_twofactor_forms_container">
                <?php foreach ($tfaForm as $form): ?>
                    <?php $style = $form['method'] == $otpConfig->method ? 'display: block' : 'display: none'; ?>
                    <div id="com_users_twofactor_<?php echo $form['method'] ?>" style="<?php echo $style; ?>">
                        <?php echo $form['form'] ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php } ?>
        <!--            <button id="load" type="button">load</button>-->
        <!--            <button id="save" type="button">save</button>-->

        <div class="joms-form__group divButtons" id="email-notification">
            <?php echo JHTML::_('form.token'); ?>
            <!--                <a href="-->
            <?php //echo CRoute::_('index.php?option=com_community&view=profile&task=preferences'); ?><!--">Email-->
            <!--                    Notification Preferences<span class="glyphicon glyphicon-chevron-right"></span></a>-->
            <div style="cursor: pointer;" class="bt-enmail-notification">Email Notification Preferences
                <img src="<?php echo JUri::base(); ?>/components/com_community/templates/jomsocial/assets/images/expand_blue.png">
            </div>
        </div>

        <div class="joms-form__group divButtons" id="divbuttons">
            <span></span>
            <?php echo JHTML::_('form.token'); ?>
            <input type="button" class="joms-button--primary joms-button--smaller js-button-left btCancel"
                   value="<?php echo JText::_('COM_COMMUNITY_CANCEL_BUTTON'); ?>"/>
            <input type="button" class="joms-button--primary joms-button--smaller js-button-right btSave btSave-submit"
                   value="<?php echo JText::_('COM_COMMUNITY_SAVE_BUTTON'); ?>"/>
        </div>
        </form>
    </div>
</div>
<div class="notification"></div>
<!--<div class="joomdle-cancel hienPopup">-->
<!--    <p><span>--><?php //echo JText::_('COM_COMMUNITY_PROFILE_EDIT1'); ?><!--</span></p>-->
<!--    <p>--><?php //echo JText::_('COM_COMMUNITY_PROFILE_EDIT2'); ?><!--</p>-->
<!--    <button class="no" type="button">--><?php //echo JText::_('COM_COMMUNITY_PROFILE_CANCEL'); ?><!--</button>-->
<!--    <button class="yes" type="button">--><?php //echo JText::_('COM_COMMUNITY_PROFILE_PROCEED'); ?><!--</button>-->
<!--</div>-->
<script type="text/javascript">
    // Validate form before submit.
    function joms_validate_form(form) {
//        if ( window.joms && joms.util && joms.util.validation ) {
//            joms.util.validation.validate( form, function( errors ) {
//                if ( !errors ) {
//                    joms.jQuery( form ).removeAttr('onsubmit');
//                    setTimeout(function() {
//                        joms.jQuery( form ).find('input[type=submit]').click();
//                    }, 500 );
//                }
//            });
//        }
//        return false;
    }

    // set initial opened tab based on the href
    //(function( href ) {
    //    var id = 'joms-profile--' + ( href.match(/#detailSet/) ? 'account' : 'information' );
    //    document.getElementById( id ).style.display = '';
    //    document.getElementsByClassName( id )[0].className += ' active';
    //})( window.location.href );


    jQuery(document).on("keypress", "form", function (event) {
        return event.keyCode != 13;
    });

    jQuery(".joms-button--neutral.joms-button--full").remove();
    jQuery(".joms-form__legend").hide();

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }


    // function cancelLeave() {
    //     jQuery('body').removeClass('overlay2');
    //     jQuery(window).on("beforeunload");
    //     return true;
    // }
    jQuery(document).ready(function () {
        var formChanged = false;

        jQuery("#name,#FIELD_ABOUTME textarea, #jsemail, #jspassword_old, #jspassword, #jspassword2, #FIELD_MOBILE input, #FIELD_ADDRESS textarea").keyup(function () {
            formChanged = true;
        });

        var input_keyword_old = "<?php echo $interest_value; ?>";


        jQuery('.btCancel').click(function () {
            var input_keyword = jQuery('#FIELD_INTEREST input').val();
            if ((input_keyword != input_keyword_old)) {
                formChanged = true;
            }
            if (formChanged)
                lgtCreatePopup('confirm', {
                        yesText: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2_BUTTON_PROCEED'); ?>',
                        content: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE1'); ?>',
                        subcontent: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2'); ?>'
                    }, function () {
                        jQuery(window).off("beforeunload");
                        window.location.href = '<?php echo CRoute::_('index.php?option=com_community&view=profile'); ?>';
                    }
                );
            else window.location.href = "<?php echo CRoute::_('index.php?option=com_community&view=profile'); ?>";
        });

        jQuery('.top-circles-icon, .bottom-circles-icon').click(function (e) {
            e.preventDefault();
            var input_keyword = jQuery('#FIELD_INTEREST input').val();
            if ((input_keyword != input_keyword_old)) {
                formChanged = true;
            }
            if (formChanged)
                lgtCreatePopup('confirm', {
                        yesText: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2_BUTTON_PROCEED'); ?>',
                        content: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE1'); ?>',
                        subcontent: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2'); ?>'
                    }, function () {
                        jQuery(window).off("beforeunload");
                        window.location.href = '<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewchats'); ?>';
                    }
                );
            else window.location.href = "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewchats'); ?>";
        });
        jQuery('.top-learning-icon, .bottom-learning-icon').click(function (e) {
            e.preventDefault();
            var input_keyword = jQuery('#FIELD_INTEREST input').val();
            if ((input_keyword != input_keyword_old)) {
                formChanged = true;
            }

            if (formChanged)
                lgtCreatePopup('confirm', {
                        yesText: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2_BUTTON_PROCEED'); ?>',
                        content: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE1'); ?>',
                        subcontent: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2'); ?>'
                    }, function () {
                        jQuery(window).off("beforeunload");
                        window.location.href = '<?php echo CRoute::_('index.php?option=com_joomdle&view=mylearning'); ?>';
                    }
                );
            else window.location.href = "<?php echo CRoute::_('index.php?option=com_joomdle&view=mylearning'); ?>";
        });
        jQuery('.top-manage-icon, .bottom-manage-icon').click(function (e) {
            e.preventDefault();
            var input_keyword = jQuery('#FIELD_INTEREST input').val();
            if ((input_keyword != input_keyword_old)) {
                formChanged = true;
            }
            if (formChanged)
                lgtCreatePopup('confirm', {
                        yesText: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2_BUTTON_PROCEED'); ?>',
                        content: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE1'); ?>',
                        subcontent: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2'); ?>'
                    }, function () {
                        jQuery(window).off("beforeunload");
                        window.location.href = '<?php echo CRoute::_('index.php?option=com_joomdle&view=mycourses'); ?>';
                    }
                );
            else window.location.href = "<?php echo CRoute::_('index.php?option=com_joomdle&view=mycourses'); ?>";
        });
        jQuery('.top-friends-icon, .bottom-friends-icon').click(function (e) {
            e.preventDefault();
            var input_keyword = jQuery('#FIELD_INTEREST input').val();
            if ((input_keyword != input_keyword_old)) {
                formChanged = true;
            }
            if (formChanged)
                lgtCreatePopup('confirm', {
                        yesText: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2_BUTTON_PROCEED'); ?>',
                        content: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE1'); ?>',
                        subcontent: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2'); ?>'
                    }, function () {
                        jQuery(window).off("beforeunload");
                        window.location.href = '<?php echo CRoute::_('index.php?option=com_community&view=friends'); ?>';
                    }
                );
            else window.location.href = "<?php echo CRoute::_('index.php?option=com_community&view=friends'); ?>";
        });
        jQuery('.top-store-icon, .bottom-store-icon').click(function (e) {
            e.preventDefault();
            var input_keyword = jQuery('#FIELD_INTEREST input').val();
            if ((input_keyword != input_keyword_old)) {
                formChanged = true;
            }
            if (formChanged)
                lgtCreatePopup('confirm', {
                        yesText: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2_BUTTON_PROCEED'); ?>',
                        content: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE1'); ?>',
                        subcontent: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2'); ?>'
                    }, function () {
                        jQuery(window).off("beforeunload");
                        window.location.href = '<?php echo CRoute::_('index.php?Itemid='); ?>';
                    }
                );
            else window.location.href = "<?php echo CRoute::_('index.php?Itemid='); ?>";
        });
        jQuery('.navbar-back, .navbar-brand').click(function (e) {
            e.preventDefault();
            var input_keyword = jQuery('#FIELD_INTEREST input').val();
            if ((input_keyword != input_keyword_old)) {
                formChanged = true;
            }
            if (formChanged)
                lgtCreatePopup('confirm', {
                        yesText: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2_BUTTON_PROCEED'); ?>',
                        content: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE1'); ?>',
                        subcontent: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2'); ?>'
                    }, function () {
                        jQuery(window).off("beforeunload");
                        window.location.href = '<?php echo JURI::root(false);?>';
                    }
                );
            else window.location.href = "<?php echo JURI::root(false);?>";
        });

        jQuery('.bt-enmail-notification').click(function () {
            var input_keyword = jQuery('#FIELD_INTEREST input').val();
            if ((input_keyword != input_keyword_old)) {
                formChanged = true;
            }
            if (formChanged)
                lgtCreatePopup('confirm', {
                        yesText: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2_BUTTON_PROCEED'); ?>',
                        content: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE1'); ?>',
                        subcontent: '<?php echo JText::_('COM_COMMUNITY_EDIT_PROFILE2'); ?>'
                    }, function () {
                        jQuery(window).off("beforeunload");
                        window.location.href = '<?php echo CRoute::_('index.php?option=com_community&view=profile&task=preferences'); ?>';
                    }
                );
            else window.location.href = "<?php echo CRoute::_('index.php?option=com_community&view=profile&task=preferences'); ?>";
        });


        jQuery('body').on('click', '.btSave-submit', function (e) {
            e.preventDefault();
            e.stopPropagation();
            jQuery(window).off("beforeunload");
            var valid = true;
            jQuery('.mandatory').each(function () {
                if (jQuery(this).val() == '') {
                    jQuery(this).parent('.joms-form__group').addClass('invalid');
                    valid = false;
                } else jQuery(this).parent('.joms-form__group').removeClass('invalid');
            });
            if (jQuery('#field17_tag').hasClass('not_valid') == true){
                valid = false;
            }
            if (!checkPwInputGroup()) valid = false;
            if (valid) jQuery.ajax({
                type: "POST",
                url: window.location.href,
                data: jQuery('#frmSaveProfile').serialize(),
                beforeSend: function () {
                    jQuery('body').addClass('overlay2');
                    jQuery('.notification').html('Loading...').fadeIn();
                },
                success: function (result) {
                    // sessionStorage.clear();
                    formChanged = false;
                    window.location.href = "/profile";
                }
            }); else
                jQuery('#system-message-container').html('<div id="system-message">' +
                    '<div class="alert alert-warning">' +
                    '<a class="close" data-dismiss="alert">×</a>' +

                    '<h4 class="alert-heading">Warning</h4>' +
                    '<div>' +
                    '<p>Some fields are invalid. Please fix them.</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>');
            formChanged = true;
        });

        jQuery(window).bind("beforeunload", function (event) {
            var input_keyword = jQuery('#FIELD_INTEREST input').val();
            if ((input_keyword != input_keyword_old)) {
                formChanged = true;
            }
            if (formChanged)
                return "<?php echo JText::_('COM_JOOMDLE_CHECK_SAVE_PROGRESS'); ?>";
        });

        jQuery('.btSave').click(function () {
            if (isEmail(jQuery('#jsemail').val())) {
                return true;
            } else {
                jQuery('#jsemail').addClass('error1');
                return false;
            }
        });

        jQuery('#jsemail').keyup(function () {
            jQuery('#jsemail').removeClass("error1");
        });


        jQuery('.mandatory').on('blur', function () {
            if (jQuery(this).val() == '') {
                jQuery(this).parent('.joms-form__group').addClass('invalid');
            } else {
                jQuery(this).parent('.joms-form__group').removeClass('invalid');
            }
        });

        jQuery('#jspassword_old, #jspassword, #jspassword2').on('blur', function () {
            checkPwInputGroup();
        });

        function checkPwInputGroup() {
            var token = '<?php echo $data[1];?>';
            if (jQuery('#jspassword_old').val() == '' && jQuery('#jspassword').val() == '' & jQuery('#jspassword2').val() == '') {
                jQuery('#jspassword_old, #jspassword, #jspassword2').parent('.joms-form__group').removeClass('invalid');
                return true;
            } else if (jQuery('#jspassword_old').val() !== token) {
                // check old password
                jQuery('#jspassword_old').parent('.joms-form__group').addClass('invalid');
                return false;
            } else if (jQuery('#jspassword_old').val() == '' || jQuery('#jspassword').val() == '' || jQuery('#jspassword2').val() == '') {
                jQuery('#jspassword_old, #jspassword, #jspassword2').parent('.joms-form__group').removeClass('invalid');
                if (jQuery('#jspassword_old').val() == '') jQuery('#jspassword_old').parent('.joms-form__group').addClass('invalid');
                if (jQuery('#jspassword').val() == '') jQuery('#jspassword').parent('.joms-form__group').addClass('invalid');
                if (jQuery('#jspassword2').val() == '') jQuery('#jspassword2').parent('.joms-form__group').addClass('invalid');
                return false;
            } else if (jQuery('#jspassword').val() != jQuery('#jspassword2').val()) {
                jQuery('#jspassword2').parent('.joms-form__group').addClass('invalid');
                return false;
            } else {
                jQuery('#jspassword_old, #jspassword, #jspassword2').parent('.joms-form__group').removeClass('invalid');
                return true;
            }
        }

        var title_acc = '<?php echo JText::_('COM_COMMUNITY_ACCOUNT_EDIT'); ?>';
        var url = window.location.href;
        if (url.match(/#detailSet/)) jQuery('.navbar-header .navbar-title span').html(title_acc);
        jQuery(".joms-tab__bar > a.joms-profile--account").click(function () {
            jQuery('.navbar-header .navbar-title span').html(title_acc);
        });
        jQuery(".joms-tab__bar > a.joms-profile--information").click(function () {
            var title = '<?php echo JText::_('COM_COMMUNITY_PROFILE_EDIT'); ?>';
            jQuery('.navbar-header .navbar-title span').html(title);
        });
    });
    jQuery('#name').keyup(function () {
        jQuery('.joms-focus__header .joms-form__group').removeClass('invalid');
    });
    jQuery('#jsemail').keyup(function () {
        jQuery('#joms-profile--account .joms-form__group').removeClass('invalid');
    });

</script>
<script>
    (function ($) {


        $(document).ready(function () {
            $('#field17').tagsInput();

            $("#field17_tag").focus(function () {
                $("#field17_tagsinput").addClass("active");
            });

            $("#field17_tag").focusout(function () {
                $("#field17_tagsinput").removeClass("active");
            });


            // $('input[type="text"]').each(function () {
            //
            //     var id = $(this).attr('id');
            //     if (sessionStorage.getItem(id)) {
            //         var value = sessionStorage.getItem(id);
            //         $(this).val(value);
            //     }
            // });
            // $('textarea').each(function () {
            //     var id = $(this).attr('id');
            //     if (sessionStorage.getItem(id)) {
            //         var value = sessionStorage.getItem(id);
            //         $(this).val(value);
            //     }
            // });
            // $('input[type="password"]').each(function () {
            //     var id = $(this).attr('id');
            //     if (sessionStorage.getItem(id)) {
            //         var value = sessionStorage.getItem(id);
            //         $(this).val(value);
            //     }
            // });
        });
    })(jQuery);
</script>