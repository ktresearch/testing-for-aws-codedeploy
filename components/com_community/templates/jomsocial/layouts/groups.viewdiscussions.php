<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();
?>
<script type="text/javascript">
  if (jQuery('#system-message').text().length > 0) {
        $('.jomsocial').css({'padding-top': '0px'});
  }  
</script>
<style>
.joms-button--primary{
	height: 30px !important;
	line-height: 10px !important;
}
</style>

<?php echo $discussionsHTML; ?>

<?php if ($pagination->getPagesLinks() && ($pagination->pagesTotal > 1 || $pagination->get('pages.total') > 1) ) { ?>
    <div class="joms-pagination">
        <?php echo $pagination->getPagesLinks(); ?>
    </div>
<?php } ?>