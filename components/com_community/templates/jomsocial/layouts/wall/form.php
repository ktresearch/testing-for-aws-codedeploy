<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();

$isAjax = false;
if ( JRequest::getCmd('task', '') == 'azrul_ajax') {
    $isAjax = true;
}

?>
<?php 
  $groupid = JRequest::getVar('groupid', '', 'REQUEST');
  $group      = JTable::getInstance('Group', 'CTable');
  $group->load($groupid);

  if ($group->published) {
?>
<div class="joms-comment__reply joms-js--newcomment joms-js--newcomment-<?php echo $uniqueId; ?>">

        <?php if ($isDiscussion == 1 && $group->published != 0) { ?>
        <div class="btAttachment"></div>
        <div class="addAttachment-list">
            <ul>
                <!--<li class="take-photo"><?php // echo JText::_('COM_COMMUNITY_TAKE_A_PHOTO'); ?></li>-->
                <li class="upload-photo" onclick="joms.view.comment.addAttachment(this.parentElement.parentElement);"><?php echo JText::_('COM_COMMUNITY_UPLOAD_PHOTO'); ?></li>
                <li class="upload-document" onclick="joms.api.fileUpload('discussion', '<?php echo $uniqueId; ?>');"><?php echo JText::_('COM_COMMUNITY_UPLOAD_DOCUMENT'); ?></li>
                <li class="cancel"><?php echo JText::_('COM_COMMUNITY_CANCEL'); ?></li>
            </ul>
        </div>
    <?php } ?>

    <div class="joms-textarea__wrapper">
        <div class="joms-comment__replyClipIcon" onclick="joms.view.comment.addAttachment(this);"></div>
        <div class="joms-textarea joms-textarea__beautifier"></div>
        <textarea name="comment" id ="commenttext" class="joms-textarea"
                  style="line-height: 21px;"
            data-id="<?php echo $uniqueId; ?>"
            data-func="<?php echo $ajaxAddFunction; ?>"
            value=""
            rows="2"></textarea>
        <div class="joms-textarea__loading"><img src="<?php echo JURI::root(true); ?>/components/com_community/assets/ajax-loader.gif" alt="loader" ></div>
        <div class="joms-textarea joms-textarea__attachment">
            <button onclick="joms.view.comment.removeAttachment(this);">×</button>
            <div class="joms-textarea__attachment--loading"><img src="<?php echo JURI::root(true); ?>/components/com_community/assets/ajax-loader.gif" alt="loader"></div>
            <div class="joms-textarea__attachment--thumbnail"><img src="#" alt="attachment"></div>
        </div>
    </div>
    <!-- <svg viewBox="0 0 16 16" class="joms-icon joms-icon--add" onclick="joms.view.comment.addAttachment(this);">
        <use xlink:href="<?php // echo $isAjax ? '' : CRoute::getURI(); ?>#joms-icon-camera"></use>
    </svg> -->
    <span class="btPost">
        <input class="joms-button--comment joms-button--small joms-js--btn-send" type="submit" value="">
    </span>
</div>
<?php }
else{?>
<div class="joms-comment__reply joms-js--newcomment joms-comment-archived"> <p><?php echo JText::_('COM_COMMUNITY_DISABLED_CHAT'); ?></p></div>
<?php }?>
 <script src="<?php echo JURI::base(); ?>components/com_community/assets/autosize.min.js"></script>
<script>
    // jQuery('.addAttachment-list .cancel').click(function() {
    //     jQuery('.addAttachment-list').fadeOut();
    // });
    jQuery(document).click(function(e) {
        if( !jQuery(e.target).hasClass("btAttachment") ) {
            jQuery('.addAttachment-list').fadeOut();
        }
    });
    jQuery('.btAttachment').click(function() {
        jQuery('.addAttachment-list').fadeToggle();
    });
    joms_wall_remove_func = '<?php echo $ajaxRemoveFunc; ?>';


    jQuery('.joms-textarea').on('click',function(){
            jQuery('.joms-textarea').attr('rows','7');
    });
    jQuery('.joms-textarea').each(function () {
                autosize(this);
                 }).on('input', function () {
                 autosize(this);
            });
    jQuery(document).on('keyup','.joms-textarea', function(e) { 
        a =  jQuery('#commenttext').prop('scrollHeight');
        jQuery(this).scrollTop(a); 
      });

</script>