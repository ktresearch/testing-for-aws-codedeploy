<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();

if (!isset($pageTitle)) {
    $task = JFactory::getApplication()->input->getCmd('task');
    $pageTitle = JText::_($task === 'display' ? 'COM_COMMUNITY_ADVANCESEARCH_SEARCH_RESULTS' : 'COM_COMMUNITY_ALL_MEMBERS');
}
$session = JFactory::getSession();
$device = $session->get('device');
$searchModel = CFactory::getModel('search');

?>
    <!-- Advanced Search Results -->
<div class="joms-page view_friends" style="padding: 0">
    <ul class="joms-list--friend friends">
    <?php if($_REQUEST['task']=='display') {
    $blockModel = CFactory::getModel('block');
    // Find user blocked
    $results = $blockModel->getBlockedList($my->id);
    
    $block_ids = [];
    if ($results) {
      foreach ($results as $key => $value) {
        $block_ids[] = $value->blocked_userid;
      }
    }

    foreach($data as $row ) : 
      if (!in_array($row->id, $block_ids)) :
      ?>
        <?php $displayname = $row->user->getDisplayName(); ?>
        <?php if(!empty($row->user->id) && !empty($displayname)&& $my->id!=$row->user->id) : ?>
            <?php 
            $isFriend = CFriendsHelper::isConnected($row ->id, $my->id);
            $isWaitingApproval = CFriendsHelper::isWaitingApproval($my->id, $row ->id);
            $isWaitingResponse = CFriendsHelper::isWaitingApproval($row ->id, $my->id);
            ?>
          
        <li class="col-xs-12">
            <div class='joms-list--pic'>
                <a class="cIndex-Avatar" href="<?php echo $row->profileLink; ?>">
                    <img src="<?php echo $row->user->getAvatar(); ?>" data-author-remove="<?php echo $row->user->id; ?>"  class="cAvatar"/>
                </a>
            </div>
            <div class='joms-list--info'>
                
                <a href="<?php echo $row->profileLink; ?>" alt="<?php echo $row->user->getDisplayName(); ?>">
                    <?php echo $row->user->getDisplayName(); ?>
                </a>
             
               </div>
               </li>
                          
        <?php endif; ?>
      <?php endif; ?>
    <?php endforeach; ?>  
    <?php } ?>
    </ul>
    <div class="loadmore" style="float: right;display:none;">
         <a class="load_more"><?php echo JText::_('COM_COMMUNITY_LOAD_MORE'); ?></a>
         </div>
</div>

<script src="components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    
        var title_p = '<?php echo JText::_('COM_COMMUNITY_FRIENDS_ADD_FRIENDS'); ?>';
        $('.navbar-header .navbar-title span').html(title_p);
        jQuery( "input.form-control" ).keyup(function() {
           var value = jQuery( this ).val();          
           if (value != '') {
               jQuery(this).addClass('onkeyup');
           } else {
               jQuery(this).removeClass('onkeyup');
            }
        })
      .keyup();
      

    $(document).ready(function() {
    var track_load = 0; //total loaded record group(s)
    var loading  = false; //to prevents multipal ajax loads
    var total_groups = <?php echo $number?>; //total record group(s)
    var i = 1;
    $('.friends').load("/index.php?option=com_community&view=friends&task=load", {'group_no':track_load}, function() {track_load++;$(".loadmore").show();}); //load first group
 
       $(".load_more").click(function(){
           i++;        
         
                 $button = $(this);
                 $button.html('<?php echo JText::_('COM_COMMUNITY_LOADING'); ?>');
            if(track_load <= total_groups && loading==false) //there's more data to load
            {
                            
                loading = true; //prevent further ajax loading
                $('.animation_image').show(); //show loading image
                
                //load data from the server using a HTTP POST request
                $.post('/index.php?option=com_community&view=friends&task=load',{'group_no': track_load}, function(data){
                                    
                    $(".friends").append(data); //append received data into the element

                    //hide loading image
                    $('.animation_image').hide(); //hide loading image once data is received
                    
                    track_load++; //loaded group increment
                    loading = false; 
                    $button.html('<?php echo JText::_('COM_COMMUNITY_LOAD_MORE'); ?>');
                      if(i>=total_groups)
                     {
                         $(".loadmore").hide();
                    }               
                }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                    
                    alert(thrownError); //alert with HTTP error
                    $('.animation_image').hide(); //hide loading image
                    loading = false;
                
                });
                
            }
             
 
    });
   
        
});
jQuery(document).ready(function () {  
           size_li = $(".joms-list--friend li").size();
       
     
        }(jQuery));
 </script>
<style type="text/css">
body
{
    background: #fff;
}

svg:not(:root) {

    display: none;
}

</style>
   
