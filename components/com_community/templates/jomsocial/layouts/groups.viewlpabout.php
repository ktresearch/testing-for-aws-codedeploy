<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/


$is_mobile = false;
$session = JFactory::getSession();
$device = $session->get('device');
if($device == 'mobile')
{
    $is_mobile = true;
}


$document = JFactory::getDocument();
$session = JFactory::getSession();
$document->addScript(JURI::root(true) . '/modules/mod_joomdle_my_courses/swiper.min.js');
$document->addStyleSheet(JURI::root(true) . '/modules/mod_joomdle_my_courses/css/swiper.min.css');

?>

<?php 
$config = CFactory::getConfig();
$showGroupDetails = ($group->approvals == 0 || ( $group->approvals == 1 && $isMember ) || ($group->approvals == 2 && $isMember ) || $isSuperAdmin); //who can see the group details
$token = JFactory::getSession()->getFormToken();


?>

<style>

body{
    background:  #e1e1e1;
}

</style>

<script src="components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        var title_p = '<?php echo $group->name; ?>';
        $('.navbar-header .navbar-title span').html(title_p);    
    });
    
</script> 

<div class="joms-page viewmember" style="background: none; padding: 0px;">
    
    <div class="viewabout">
        <div class="joms-page">
        <?php if ($showGroupDetails){ ?>
        <div id="joms-group--details" class="joms-tab__content" style="<?php echo ((!$config->get('default_group_tab') == 1 && $showGroupDetails) || !$showGroupDetails) ? '' : 'display:none'; ?>">
                <div class="description">
                    <p class="title"><?php echo JText::_('COM_COMMUNITY_ABOUT_TITLE');?></p>
                    <div class="content">
                        <?php echo $group->description; ?>
                    </div>
                </div>
                <div class="keyword">
                    <h5 class="joms-text--light"><?php echo JText::_('COM_COMMUNITY_GROUPS_ENTER_KEYWORD'); ?></h5>
                     <div class="content">
                    <?php 
                        if ($group->keyword != null){                            
                            $keywords = explode(",", $group->keyword);
                            foreach ($keywords as $keyword){
                                ?>
                               
                                                           
                       <a href="<?php echo CRoute::_('index.php?search='. trim($keyword) . '&' . $token . '=1' . '&option=com_community&view=groups&task=search'); ?>" class="styled-button-1 border-radius-1"> <?php echo $keyword ?></a>
                       
                                <?php
                            }
                        }else{
                            echo '<span>'.JText::_('COM_COMMUNITY_GROUP_NO_KEYS').'</span>';
                        }
                    ?>
                    </div>                                
                </div>

                <?php 
                 if (!empty($subgroups)) {
                ?>
                <div class="admin_gr"> 
                <h5 class="joms-text--light"><?php echo JText::_('COM_COMMUNITY_GROUPS'); ?></h5>
                    <div class="joms-list__row">
                        <div class="mygroups have-groups ">
                            <div class="mygroup-content-block">
                                <div class="swiper-container swiper-container-horizontal">
                                    <div class="swiper-wrapper" style="/*transition-duration: 0ms; transform: translate3d(-394px, 0px, 0px);*/">
                                        <!-- <ul> -->

                                        <?php
                                        $i = 0;

                                        foreach($subgroups as $profileGroup) {
                                            $table =  JTable::getInstance( 'Group' , 'CTable' );
                                            $table->load($profileGroup->id);

                                            if($profileGroup->avatar == ''){
                                                $profileGroupAvatar = JURI::root(true) . '/components/com_community/assets/group.png';
                                            }else{
                                                //$profileGroupAvatar = JURI::root(true) . $profileGroup->avatar;
                                                 $profileGroupAvatar = $table->getThumbAvatar();
                                            }
                                            $gname = strip_tags($profileGroup->name);
                                            if(strlen($gname) > 16) {
                                                if(strpos($gname, ' ', 16) === false) {
                                                    $gname = substr($gname, 0, 16).'...';
                                                } else {
                                                    $gname = substr($gname, 0, strpos($gname, ' ', 16)).'...';
                                                } 
                                            }
                                            ?>
                                            <div class="swiper-slide" style="/*width: 384px; margin-right: 10px;*/">
                                                <div class="group-title">
                                                    <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $profileGroup->id); ?>">
                                                    <img src="<?php echo $profileGroupAvatar; ?>" class="left cAvatar jomNameTips group-image" alt="<?php echo $profileGroup->name; ?>" title="<?php echo $profileGroup->name; ?>">
                                                    </a>
                                                    <div class="right">
                                                        <a class="gr-title" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $profileGroup->id); ?>">
                                                        <?php echo $gname; ?></a>
                                                        <p class="gr-owner"><?php echo JText::_('OWNER').': '.ucfirst(JFactory::getUser($profileGroup->ownerid)->name); ?></p>
                                                        <!-- <li> -->
                                                        <?php 
                                                            $desc = strip_tags($profileGroup->description);
                                                            if(strlen($desc) > 70) {
                                                                if(strpos($desc, ' ', 70) === false) {
                                                                    $desc = substr($desc, 0, 70).'...';
                                                                } else {
                                                                    $desc = substr($desc, 0, strpos($desc, ' ', 70)).'...';
                                                                } 
                                                            }
                                                        ?>
                                                        <p class="group-description"><?php echo $desc; ?></p>
                                                    </div>
                                                    <span class="group-statistic"> <?php echo $profileGroup->membercount . ' Members ' . $profileGroup->discusscount . ' Topics'; ?>  </span>

                                                </div>
                                            </div>
                                            <!-- </li> -->

                                            <?php
                                            $i++;
                                        } ?>

                                    </div>
                                </div>
                                <!-- </ul> -->
                                <!-- <span class="count"><?php echo $i ?></span> -->
                                <div style="clear:both"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php 
                    }
                ?>   

        </div>        
        <?php } ?>
        </div>
    </div>
    

    <?php       
        $users = array();
        
        foreach($members as $member){
            if($member->isAdmin || $member->isOwner){
                $admins[] = $member;        
            }else{
                $users[] = $member;        
            }
        }
        
        $members = $users;         
    ?>

    <div class="joms-list__item bordered-box">
                <div class="joms-list--friend pull-left col-xs-12" style="background: none">
                    <h4 style="padding-left: 20px; padding-bottom: 10px"><?php echo JText::_('COM_COMMUNITY_MEMBERS_TITLE_MANAGERS');?></h4>
                    <div class="js-memb-list-admin">
                    
                <?php foreach($admins as $admin) { ?>
                            <div class="userAvatar">
                                <a class="cIndex-Avatar" href="<?php echo CRoute::_('index.php?option=com_community&fromcircle=1&view=profile&userid=' . $admin->id); ?>">
                                    <img src="<?php echo $admin->getThumbAvatar(); ?>" data-author-remove="<?php echo $admin->id; ?>" class="cAvatar"/>
                                </a>
                                <p><?php echo $admin->getDisplayName(); ?></p>
                                <p class="name_member" style="font-size: 14px"><?php 
                                                    
                       if( $admin->id == $group->ownerid) {
                            echo JText::_('COM_COMMUNITY_VIEWABOUT_OWNER');
                       } else {
                            $roles = str_replace('creator', 'Content Creator', $admin->roles);
                            $roles = str_replace('manager', 'Manager', $roles);
                            echo $roles; 
                       }
                ?></p>
       
                                <?php if ($type) { ?>
                                <div class="joms-js--request-notice-group-<?php echo $group->id; ?>-<?php echo $admin->id; ?>"></div>
                                <div class="joms-js--request-buttons-group-<?php echo $group->id; ?>-<?php echo $admin->id; ?>" style="white-space:nowrap">
                                    <a href="javascript:" onclick="joms.api.groupApprove('<?php echo $groupid; ?>', '<?php echo $admin->id; ?>');">
                                        <button class="joms-button--primary joms-button--smallest joms-button--full-small"><?php echo JText::_('COM_COMMUNITY_PENDING_ACTION_APPROVE'); ?></button>
                                    </a>
                                    <a href="javascript:" onclick="joms.api.groupRemoveMember('<?php echo $group->id; ?>', '<?php echo $admin->id; ?>');">
                                        <button class="joms-button--neutral joms-button--smallest joms-button--full-small"><?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING_ACTION_REJECT'); ?></button>
                                    </a>
                                </div>
                                <?php } ?>
                                <?php if ($isAdmin || $isSuperAdmin) { ?>
                                <div class="joms-list__actions">

                                <div class="joms-list__options">
                                    <a href="javascript:" data-ui-object="joms-dropdown-button" class="active">
                                        <?php if( !($admin->id == $group->ownerid)) { ?>
                                        <i class="joms-icon-delete"></i>
                                        <?php } ?>
                                    </a>    
                                    <ul class="joms-dropdown" style="display: none;">
                                        <li><a href="javascript:" onclick="joms.api.groupRemoveMember(<?php echo $group->id ?>, <?php echo $admin->id ?>)">Remove Member</a></li>
                                    </ul>
                                </div>
                                 
                                <?php //echo CFriendsHelper::getUserCog($admin->id,$group->id,null,true); ?>

                                </div>
                                <?php } ?>
                        
                            </div>    
                <?php } ?>
                    </div>
                </div>
                    <?php if ($isAdmin || $isSuperAdmin) { ?>
                    <div style="float: right; padding: 12px; padding-right: 5%">
                        <a href="<?php echo CRoute::_('index.php?option=com_community&view=friends&task=assignfriends&group='.$group->id .'&categoryid='.$group->moodlecategoryid); ?>">
                        <button type="button" class="joms-button--primary button--small-member">
                        <?php echo JText::_('COM_COMMUNITY_ASSIGN_MEMBERS');?> +                 
                        </button>
                        </a>
                    </div>
                    <?php } ?>
            </div>

    
    
</div>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            var swiper = new Swiper('.mygroups .swiper-container', {
                pagination: '.mygroups .swiper-pagination',
                slidesPerView: <?php echo ($device == 'mobile') ? '1' : '2.5'; ?>,
                nextButton: '.mygroups .swiper-button-next',
                prevButton: '.mygroups .swiper-button-prev',
                spaceBetween: 10,
                freeMode: true,
                freeModeMomentum: true
            });
        });
    </script>

    <script>
    jQuery(document).click(function(e) {
          var target = e.target;
          jQuery(target).toggleClass('active');

          jQuery(target).parents().next('ul').toggleClass('show');
          if (!$(target).is('.joms-dropdown-button') && !$(target).parents().is('.joms-dropdown-button')) {
                jQuery('ul').removeClass('show');
          }
    });
    jQuery(document).ready(function() {
        jQuery(document).delegate('.com_community.view-groups.task-viewmembers .joms-popup .joms-popup__action button.joms-button--primary.btPopupAddMembers', 'click', function() {
            location.reload();
        });

    }(jQuery));

    </script>

    <?php if(count($members)>10) {?>
    <div class="loadmore" style="float: right;display:none;">
             <a class="load_more"><?php echo JText::_('COM_COMMUNITY_LOAD_MORE'); ?></a>
             </div>
    <script src="/components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script>
        

        $(document).ready(function() {
        var track_load = 0; //total loaded record group(s)
        var loading  = false; //to prevents multipal ajax loads
        var total_groups = <?php echo  $number ?>; //total record group(s)
        var i = 1;
        $('.list_member').load("<?php echo CRoute::_('index.php?option=com_community&view=groups&task=loadmember&groupid=' .$groupid) ?>", {'group_no':track_load}, function() {track_load++;$(".loadmore").show();}); //load first group

        $(".joms-dropdown li").eq(2).remove();

        
        $(".load_more").click(function(){
             i++;     
         
                $button = $(this);
                $button.html('<?php echo JText::_('COM_COMMUNITY_LOADING'); ?>');
                if(track_load <= total_groups && loading==false) //there's more data to load
                {
                                
                    loading = true; //prevent further ajax loading
                    $('.animation_image').show(); //show loading image
                    
                    //load data from the server using a HTTP POST request
                    $.post('<?php echo CRoute::_('index.php?option=com_community&view=groups&task=loadmember&groupid=' .$groupid) ?>',{'group_no': track_load}, function(data){
                                        
                        $(".list_member").append(data); //append received data into the element

                        //hide loading image
                        $('.animation_image').hide(); //hide loading image once data is received
                        
                        track_load++; //loaded group increment
                        loading = false; 
                        $button.html('<?php echo JText::_('COM_COMMUNITY_LOAD_MORE'); ?>');
                              if(i>=total_groups)
                        {
                   			$(".loadmore").hide();
                         }
                    }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                        
                        alert(thrownError); //alert with HTTP error
                        $('.animation_image').hide(); //hide loading image
                        loading = false;
                    
                    });
                    
                }
        });


    });
    </script>
    <?php } ?>

