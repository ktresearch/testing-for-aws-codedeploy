<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/

defined('_JEXEC') or die('Restricted access');

$user = CFactory::getUser($this->act->actor);
$config = CFactory::getConfig();

// Setup group table
$group = $this->group;

// Setup Announcement Table
$bulletin = JTable::getInstance('Bulletin', 'CTable');
$bulletin->load($act->cid);

// get created date time
$date = JFactory::getDate($act->created);
if ( $config->get('activitydateformat') == "lapse" ) {
  $createdTime = CTimeHelper::timeLapse($date);
} else {
  $createdTime = $date->format($config->get('profileDateFormat'));
}

// Load params
$param = new JRegistry($this->act->params);
$action = $param->get('action');
$actors = $param->get('actors');
$this->set('actors', $actors);

$stream = new stdClass();
$stream->actor = $user;
$stream->target = null;
$stream->datetime = $createdTime;
//$stream->headline = '<a href="' .CUrlHelper::userLink($user->id).'">'.$user->getDisplayName().'</a> ' . JTEXT::_('COM_COMMUNITY_CIRCLE_NEW_GROUP_NEWS') . '<span class="joms-stream__time"><small>' . $stream->datetime . '</small></span>';

$stream->message = "";
$stream->group = $group;
$stream->groupid = $group->id;
$stream->attachments = array();
$stream->title = $bulletin->title;
$stream->link = CRoute::_('index.php?option=com_community&view=groups&task=viewbulletin&groupid=' . $group->id . '&bulletinid=' . $bulletin->id );
$stream->access = $act->access;

$attachment = new stdClass();
$attachment->type = 'create_announcement';
$attachment->message = $this->escape(JHTML::_('string.truncate', $bulletin->message, $config->getInt('streamcontentlength'), true, false ));

//socialfeeds-edit: made announcement
if($my->id === $user->id)
    $username = JText::_('COM_COMMUNITY_POST_ME');
else
    $username = $user->getDisplayName();
$stream->headline = '<div class="joms-stream_title"><a href="' .CUrlHelper::userLink($user->id).'">'.$username.'</a> ' . JTEXT::sprintf('COM_COMMUNITY_CIRCLE_NEW_GROUP_NEWS', $stream->link,$stream->title,CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid='.$group->id), $this->escape($group->name)) . '</div><div class="joms-stream-post_time"><span class="joms-stream__time" style="float:right"><small>' . $stream->datetime . '</small></span></div>';

$stream->attachments[] = $attachment;

$this->set('stream', $stream);
$this->load('activities.stream');
