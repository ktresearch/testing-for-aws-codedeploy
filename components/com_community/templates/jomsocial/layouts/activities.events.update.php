<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/

defined('_JEXEC') or die('Restricted access');

$user = CFactory::getUser($this->act->actor);
$truncateVal = 60;
$date = JFactory::getDate($act->created);
if ( $config->get('activitydateformat') == "lapse" ) {
  $createdTime = CTimeHelper::timeLapse($date);
} else {
  $createdTime = $date->format($config->get('profileDateFormat'));
}
$my = CFactory::getUser();
?>

<div class="joms-stream__header">
    <div class="joms-avatar--comment <?php echo CUserHelper::onlineIndicator($user); ?>">
        <img src="<?php echo $user->getAvatar().'?_='.time(); ?>" alt="<?php echo $user->getDisplayName(); ?>">
    </div>
    <div class="joms-stream__meta">
        <div class="joms-stream_title">
        <a href="<?php echo CUrlHelper::userLink($user->id); ?>"><?php if($my->id === $user->id) echo JText::_('COM_COMMUNITY_POST_ME'); else echo $user->getDisplayName(); ?></a> - <?php echo JText::sprintf('COM_COMMUNITY_EVENTS_EVENT_UPDATED', $this->event->getLink(), $this->event->title); ?>
        </div>
        <div class="joms-stream-post_time">
        <span class="joms-stream__time"><small><?php echo $createdTime; ?></small></span>
        </div>
    </div>
    <?php

        $this->load('activities.stream.options');

    ?>
</div>
