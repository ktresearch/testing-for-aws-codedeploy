<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
/*
Added by Nyi for Course Catalogue for Learning Provider Circle
*/

$document = JFactory::getDocument();


$session = JFactory::getSession();
$device = $session->get('device');

$my = CFactory::getUser();
$username = $my->username;
$categoryid = $group->moodlecategoryid; 

$class = '';
$idstr = 'id="mobile-show"';
if($device != 'mobile') {
    $class = 'desktop-tablet';
    $idstr = '';
}

?>


<?php
    if($my->id==0){
        if($device=='mobile'){
            ?>
            <style type="text/css">
                body{
                    padding: 0px;
                }
                .joms-tab__bar{
                    display: none !important;
                }
                .joms-avatar--focus img{
                    height: 64px;
                    height: 64px;
                }
                .container{
                    margin: 0px;
                    width: 100%;
                }
            </style>
            <?php
        }else{
            ?>
            <style type="text/css">
                .joms-tab__bar{
                    display: none !important;
                }
                .joms-avatar--focus img{
                    height: 100px;
                    width: 100px;
                }
                .container{
                    margin: 0px;
                    width: 100%;
                }
            </style>
        <?php
        }
    }
?>


<style>
body, #t3-mainbody{
    background-color:#dcdcdc;
}
</style>
<div class="home">
    <div class="container" style="padding:0px;">
        <?php 
            if(empty($hikashopCourses) || !isset($hikashopCourses)){
                echo "<span style='padding-left:15px;'>".JText::_('COM_COMMUNITY_NO_COURSE_FOUND')."</span>";
            }else{
                foreach ($hikashopCourses as $course) {
                    $link = JUri::base()."providers/product/".$course->product_id.'-'.$course->product_alias;//hikashop_contentLink('product&task=show&cid='.$course->product_id.'&name='.$course->product_alias);//hikashop_completelink("product&task=show&cid[]=".$course->product_id);//
                    $productClass = hikashop_get('class.product');
                    $product =$productClass->get($course->product_id);

                    $config =& hikashop_config();
                    $currencyClass = hikashop_get('class.currency');
                    $main_currency = $currency_id = (int)$config->get('main_currency',1);
                    $zone_id = explode(',',$config->get('main_tax_zone',0));

                    if(count($zone_id)){
                        $zone_id = array_shift($zone_id);
                    }else{
                        $zone_id=0;
                    }
                    $ids = array($product->product_id);
                    $discount_before_tax = (int)$config->get('discount_before_tax',0);
                    $currencyClass->getPrices($product,$ids,$currency_id,$main_currency,$zone_id,$discount_before_tax);
                    
                    $db  = JFactory::getDBO();
                    $query = "SELECT * FROM #__hikashop_file WHERE file_ref_id =".$course->product_id;
                    $db->setQuery($query);
                    $load_files = $db->loadObjectList();
                    if(!empty($load_files)){

                        $course->file_path = $load_files[0]->file_path;
                    }

                    $image_path="";
                    $imageHelper = hikashop_get('helper.image');
                    if(!empty($course->file_path)){

                        $img = $imageHelper->getThumbnail(@$course->file_path, array('width' =>"400px" , 'height' => "300px"), null);
                        $file_path=str_replace('\\','/',$img->path);
                        $image_path=$imageHelper->uploadFolder_url.$file_path;
                    }else{
                        $image_path="http://placehold.it/350x150";
                    }

                    if(1== $course->product_published){
                        if(strpos($course->product_code,'bundle_')!==false || strpos($course->product_code,'prog_')!==false ){
                            $bkColor="#C5C7C9";
                        }else{
                            $bkColor="#FFFFFF";
                        }
                        if($device == 'mobile'){
                            ?>
                                <div class="hika_listing_img_desc" style="font-family:'Open Sans';">
                                    <a href="<?php echo $link;?>" style="display:block;text-decoration:none;color:black;background-color:<?php echo $bkColor; ?>">
                                            <div class="row" style="height:225px;background-image:url('<?php echo $image_path; ?>');background-repeat:no-repeat;background-size:100% 100%;background-position:center;">
                                            
                                            </div>
                                        <div class="row"  style="margin:0px;margin-top:15px;">
                                            <div class="col-xs-8" style="color:#126DB6;font-weight:700;">
                                                <!-- PRODUCT NAME & LINK -->
                                                    <?php 
                                                        echo $course->product_name
                                                    ?>
                                                    <!-- PRODUCT NAME & LINK -->
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="row" style="margin-right:15px;text-align:right;">
                                                    <!-- PRICE -->
                                                   <span style="font-family: OpenSans,'Open Sans';font-style: normal;font-weight: 300;font-size: 14px;color:#494949;line-height: 33px;display: block;text-align: right !important;padding-right: 0px;">
                                                        <?php
                                                                if(empty($product->prices)){
                                                                    echo JText::_('FREE_PRICE');
                                                                }else{
                                                                    echo $currencyClass->format($product->prices[0]->price_value);
                                                                }
                                                        ?>
                                                    <!-- PRICE -->
                                                    </span>
                                                    <!-- PRICE -->
                                                </div>
                                                <div class="row" style="margin-right:15px;text-align:right !important;">
                                                    <!-- ESTIMATED COURSE DURATION -->
                                                       <?php
                                                        if(!empty($course->estimated_duration)){
                                                            $est_time=(double)$course->estimated_duration;
                                                            $suffix="h";
                                                            if($est_time <= 1)
                                                                $suffix="h";
                                                                echo $course->estimated_duration.$suffix;
                                                            }
                                                        ?>
                                                    <!-- ESTIMATED COURSE DURATION -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row"  style="margin:0px;margin-top:10px;margin-bottom:10px;">
                                            <div class="col-md-1" style="margin-bottom:15px;max-height:60px;overflow:hidden;word-wrap:break-word;text-overflow:ellipsis;-o-text-overflow:ellipsis;-webkit-line-clamp:3;display:-webkit-box;-webkit-box-orient:vertical;">
                                                <!-- PRODUCT DESCRIPTION -->
                                                    <?php
                                                    $description_text=strip_tags(JHTML::_('content.prepare',preg_replace('#<hr *id="system-readmore" */>#i','',$course->product_description)));
                                                                if (str_word_count($description_text, 0) > 20) {
                                                                        $words = str_word_count($description_text,2);
                                                                        $pos = array_keys($words);
                                                                        $text = substr($description_text, 0, $pos[20]) . '...';
                                                                }else{
                                                                        $text=$description_text;
                                                                }
                                                                echo strip_tags($text);
                                                ?>
                                                <!-- PRODUCT DESCRIPTION -->
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php

                        }else{
                            ?>

                                <div class="hika_listing_img_desc" style="font-family:'Open Sans';padding-bottom: 15px;">
                                    <div class="row" style="margin:0px;padding:15px;display:block;text-decoration:none;color:black;background-color:#ffffff;" onclick="location.href='<?php echo $link; ?>'">
                                        <div class="col-sm-4 hikashop_product_listing_image" style="cursor:pointer;height:195px;background-image:url('<?php echo $image_path; ?>');background-repeat:no-repeat;background-size:100% 100%;background-position:center;width:300px;">
                    
                                        </div>
                                        <div class="col-sm-6 hikashop_product_listing_middle" style="width:calc( 100% - 400px);float:left;">
                                            <div class="row hikashop_product_title" style="margin:0px;color:#126DB6;">
                                                <!-- PRODUCT NAME & LINK -->
                                                    <?php
                                                        echo $course->product_name;
                                                    ?>
                                                    <!-- PRODUCT NAME & LINK -->
                                            </div>
                                            <div class="row hikashop_product_listing_description" style="margin:0px;">
                                                <!-- PRODUCT DESCRIPTION -->
                                                    <?php
                                                    $description_text=JHTML::_('content.prepare',preg_replace('#<hr *id="system-readmore" */>#i','',$course->product_description));
                                                        if (str_word_count($description_text, 0) > 35) {
                                                                $words = str_word_count($description_text,2);
                                                                $pos = array_keys($words);
                                                                $text = substr($description_text, 0, $pos[35]) . '...';
                                                        }else{
                                                                $text=$description_text;
                                                        }
                                                        echo strip_tags($text);
                                                        //echo preg_replace('#<hr *id="system-readmore" */>.*#is','',$text);
                                                ?>
                                                <!-- PRODUCT DESCRIPTION -->
                                            </div>
                                        </div>
                                        <div class="col-sm-2" style="width:100px;float:left;cursor:pointer;padding-right: 15px;">
                                            <div class="row">
                                               <!-- PRICE -->
                                                <span style="font-family: OpenSans,'Open Sans';font-style: normal;font-weight: 300;font-size: 16px;color:#494949;line-height: 33px;display: block;text-align: right !important;padding-right: 0px;">
                                                        <?php
                                                                if(empty($product->prices)){
                                                                    echo JText::_('FREE_PRICE');
                                                                }else{
                                                                    echo $currencyClass->format($product->prices[0]->price_value);
                                                                }
                                                        ?>
                                                <!-- PRICE -->
                                                </span>
                                            </div>
                                            <div class="row hikashop_product_listing_estimatedhours" style="padding-right:0px;">
                                                <!-- ESTIMATED COURSE DURATION -->
                                                <?php
                                                    if(!empty($course->estimated_duration)){
                                                        $est_time=(double)$course->estimated_duration;
                                                        $suffix="h";
                                                        if($est_time <= 1)
                                                            $suffix="h";
                                                            echo $course->estimated_duration.$suffix;
                                                    }
                                                ?>
                                                <!-- ESTIMATED COURSE DURATION -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                        }
                    }
                }
            }
        ?>
    </div>
</div>

