<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();

$document = JFactory::getDocument();
$document->addStyleSheet('components/com_community/assets/release/css/discussions.css');

if($_REQUEST['task'] == 'adddiscussion') {
    $addClass = 'create';
    $addAvatarClass = 'create-avatar';
}
?>
<script src="./plugins/system/t3/base-bs3/bootstrap/js/bootstrap.js" type="text/javascript"></script>
<script src="<?php echo $this->baseurl ?>./components/com_community/assets/window-1.0.js"></script>

<div class="joms-page">
    <h2 class="create-chat_title"><?php echo JText::_('COM_COMMUNITY_GROUPS_CREATE_CHAT');?></h2>
    <form class="joms-form chat-from" name="jsform-groups-discussionform" action="<?php echo CRoute::getURI(); ?>" method="POST" onsubmit="return joms_discussion_onsubmit( this );" enctype="multipart/form-data">

        <?php if ($beforeFormDisplay) { ?>
            <div class="joms-form__group"><?php echo $beforeFormDisplay; ?></div>
        <?php } ?>

        <div class="joms-form__group discussion_from">
            <!-- <span><?php //echo JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_TITLE'); ?> <span class="joms-required">*</span></span> -->
            <!--            <div class="current-user-pic">
                <img src="<?php // echo CFactory::getUser()->getAvatar().'?_='.time();?>">
            </div>-->

            <div class="joms-avatar--focus discussion-avatar">
                <a class="avatarChange  <?php echo $addAvatarClass; ?>" <?php if ( isset($discussion->defaultAvatar) && isset($discussion->avatarAlbum) && !$discussion->defaultAvatar && $discussion->avatarAlbum ) { ?>
                    href="<?php echo CRoute::_('index.php?option=com_community&view=photos&task=photo&albumid=' . $discussion->avatarAlbum); ?>" style="cursor:default"
                    onclick="joms.api.photoOpen(<?php echo $discussion->avatarAlbum ?>); return false;"
                <?php } ?>>
                    <img src="<?php echo ($_REQUEST['task'] != 'adddiscussion') ? ($discussion->getAvatar('avatar') . '?_=' . time()) : '/components/com_community/assets/bg_avatar_chat.png'; ?>" alt="<?php echo $this->escape($discussion->name); ?>" class="avatar-image">
                    <?php // if ($isAdmin || $isSuperAdmin || $isMine || !$discussion->id) { ?>
                    <div class="photo-icon">
                        <img src="images/icons/photoicon.png" class="photo-icon-img" <?php if($_REQUEST['task'] != 'adddiscussion') { echo 'onclick="joms.api.avatarChange(\'discussion\', \''.$discussion->id.'\', arguments && arguments[0]); return false; "'; } ?>>
                    </div>
                    <?php // } ?>
                </a>
                <input type="file" name="avatar" id="avatarChatImg" style="display:none" onchange="readChatAvatar(this);" />
            </div>

            <div class="joms-form__group discussion-name">
                <label id = "chat-name"><?php echo JText::_('COM_COMMUNITY_GROUPS_CHAT_NAME');?><span class="joms-required">*</span></label>
                <input type="text" name="title" id="title" class="joms-input" value="<?php echo $discussion->title;?>" />
            </div>
        </div>
        <div class="joms-form__group discussion-des" style="margin-top: 33px;width: 100%;margin-left: 0;">
            <label id="about-chat"><?php echo JText::_('COM_COMMUNITY_GROUPS_ABOUT_CHAT'); ?><span class="joms-required">*</span></label>
            <textarea name="message" id="message" class="joms-textarea"><?php echo $discussion->message; ?></textarea>
        </div>
        <div class="joms-form__group discussion-notify" style="display: none;">
            <div class="checked-status checked"></div>
            <input type="hidden" name="discussion-notification" id="discussion-notification" value="1" />
            <span><?php echo JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_NOTIFY'); ?></span>
        </div>

        <?php if($gparams->get('groupdiscussionfilesharing') > 0) {?>
            <div class="joms-form__group joms-required" style="display: none;">
                <span><?php echo JText::_('COM_COMMUNITY_FILES_ALLOW_MEMBERS')?></span>
                <label for="filepermission-member">
                    <input type="checkbox" class="joms-input" value="1" name="filepermission-member" <?php echo ($params->get('filepermission-member') > 0 || $_REQUEST['task'] == 'adddiscussion') ? 'checked="checked"' : '' ?>/>
                </label>
            </div>
        <?php }?>

        <?php if ($afterFormDisplay) { ?>
            <div class="joms-form__group"><?php echo $afterFormDisplay; ?></div>
        <?php } ?>

        <script>
            jQuery('form[name="jsform-groups-discussionform"] .discussion-notify ').click(function() {
                if ( jQuery(this).find('.checked-status').hasClass('checked') ) {
                    jQuery('#discussion-notification').val('0');
                } else {
                    jQuery('#discussion-notification').val('1');
                }
                jQuery(this).find('.checked-status').toggleClass('checked');
            });

            function joms_discussion_onsubmit() {
                return false;
            }

            window.joms_queue || (window.joms_queue = []);
            window.joms_queue.push(function( $ ) {
                joms_discussion_onsubmit = function() {
                    var $message = $('#message'),
                        value = $message.val();
                    if ((jQuery('input[name="title"]').val().trim()) ==  '' || (jQuery('textarea[name="message"]').val().trim()) == '') {

                        if ((jQuery('input[name="title"]').val().trim()) == '') {
                            jQuery('input[name="title"]').addClass('js-error');
                            jQuery('#chat-name').addClass('js-error');
                        }
                        if ((jQuery('textarea[name="message"]').val().trim()) == '') {
                            jQuery('textarea[name="message"]').addClass('js-error');
                            jQuery('#about-chat').addClass('js-error');
                        }
                        return false;

                    }
                    value = value.replace( /<\/p><p>/ig, '</div><div><br></div><div>' );
                    value = value.replace( /<(\/?)p>/ig, '<$1div>' );
                    $message.val( value );
                    jQuery('.btPost').attr("disabled", "disabled");
                    return true;
                };
            });
            jQuery('#title').keyup(function () {
                if (jQuery('input[name="title"]').val().trim() != '') {
                    jQuery('input[name="title"]').removeClass('js-error');
                    jQuery('#chat-name').removeClass('js-error');
                }
            });
            jQuery('#message').keyup(function () {
                if (jQuery('textarea[name="message"]').val().trim() != '') {
                    jQuery('textarea[name="message"]').removeClass('js-error');
                    jQuery('#about-chat').removeClass('js-error');
                }
            });

        </script>

        <div class="joms-form__group discussion-buttons">

            <div>
                <input type="hidden" value="<?php echo $group->id; ?>" name="groupid" />

                <input type="submit" class="joms-button--primary button--small button-right btPost" value="<?php echo ($_REQUEST['task'] == 'adddiscussion') ? JText::_('COM_COMMUNITY_CREATE_BUTTON') : JText::_('COM_COMMUNITY_SAVE_BUTTON'); ?>" />
                <input type="button" class="joms-button--primary button--small button-right cancel" name="cancel" value="<?php echo JText::_('COM_COMMUNITY_CANCEL_BUTTON'); ?>" onclick="javascript:history.go(-1);return false;" />

                <?php echo JHTML::_( 'form.token' ); ?>
            </div>
        </div>

    </form>
</div>
<script>
    var textarea = document.querySelector('textarea');

    textarea.addEventListener('keydown', autosize);

    function autosize(){
        var el = this;
        setTimeout(function(){
            el.style.cssText = 'height:auto; padding:0';
            // for box-sizing other than "content-box" use:
            // el.style.cssText = '-moz-box-sizing:content-box';
            el.style.cssText = 'height:' + el.scrollHeight + 'px';
        },0);
    }
    (function ($) {
        $('.avatarChange.create-avatar').on('click', function(e) {
            $('#avatarChatImg').click();
            e.preventDefault();
        });

    })(jQuery);

    function readChatAvatar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                console.log('read file');
                joms.jQuery('.avatar-image')
                    .attr('src', e.target.result)
                    .width(86)
                    .height(86);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
