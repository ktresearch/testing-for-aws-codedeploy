<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();
$groupsModel = CFactory::getModel('groups');
$blnCircle = $groupsModel->getBLNCircle();
$validOptions = ['open', 'private', 'closed'];
if ($parentCircle && $parentCircle->id != $blnCircle->id ) {
    unset($validOptions[0]);
    if ($parentCircle->unlisted == 1) unset($validOptions[1]);
}
?>

<script src="./plugins/system/t3/base-bs3/bootstrap/js/bootstrap.js" type="text/javascript"></script>
<script src="<?php echo $this->baseurl ?>./components/com_community/assets/window-1.0.js"></script>
<script src="<?php echo $this->baseurl ?>./components/com_community/templates/jomsocial/assets/js/jquery.tagsinput.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>./components/com_community/templates/jomsocial/assets/css/jquery.tagsinput.css" />

<div class="joms-page">
    <h3 class="joms-page__title"><?php //echo JText::_($isNew ? 'COM_COMMUNITY_GROUPS_CREATE_NEW_GROUP' : 'COM_COMMUNITY_GROUPS_EDIT_TITLE'); ?></h3>
    <form method="POST" class="new-circle" id="new_circle" action="<?php echo CRoute::getURI(); ?>" onsubmit="return joms_validate_form( this );" enctype="multipart/form-data">
        <?php
        echo $groupHeader;
        ?>
        <div class="content-circle"  >
            <?php if($isNew): ?>
                <div class="content-create-titlle">
                    <center><h2><?php echo JText::_('COM_COMMUNITY_SUBCIRCLES_BOX_CREATE'); ?></h2></center>
                </div>
            <?php endif; ?>
            <div class="joms-form__group joms-textarea--mobile">
                <span class="title" id ="circle_name" title="<?php echo JText::_('COM_COMMUNITY_CIRCLE_NAME')?>"><?php echo JText::_('COM_COMMUNITY_CIRCLE_NAME'); ?><span class="joms-required">*</span></span>
                <input type="text" name="name" id="gr-name" value="<?php echo $group->name; ?>" class="txtaTitle" <?php echo isset($txtboxstatus) ? $txtboxstatus : ''; ?> />
            </div>
            <?php if ($isNew && empty($parentid)) { ?>
                <div class="option-lp" style="padding-bottom: 25px">
                    <span class="checkProvider" id="lp"></span>
            <span class="becomeProvider" title="<?php echo JText::_('COM_COMMUNITY_GROUPS_APPROVAL_TIPS'); ?>">
                 <?php echo JText::_('COM_COMMUNITY_BECOME_LEARNING_PROVIDER'); ?></span>
                </div>
            <?php } ?>
            <?php if ($beforeFormDisplay) { ?>
                <div class="joms-form__group"><?php echo $beforeFormDisplay; ?></div>
            <?php } ?>
            <div class="joms-form__group joms-textarea--mobile">
                <span class="title" id = "circle_description" title="<?php echo JText::_('COM_COMMUNITY_ABOUT_TITLE')?>"><?php echo JText::_('COM_COMMUNITY_ABOUT_TITLE'); ?><span class="joms-required">*</span></span>
                <textarea class="txtaDescription" name="description" id = "gr-description"><?php echo $this->escape($group->description); ?></textarea>
            </div>
            <div class="joms-form__group">
                <span class="title"><?php echo JText::_('COM_COMMUNITY_GROUPS_ENTER_KEYWORD'); ?></span>
                <input name="keyword" type="text" class="joms-input" id="keywords" value="<?php echo $this->escape($group->keyword); ?>" />
            </div>

            <?php
            // circle privacy
            $private = '';
            $private_value = '';
            $closed = '';
            $closed_value = '';
            if ($group->id && $group->approvals == 0 && $group->unlisted == 0 || !$group->id && in_array('open', $validOptions)) {
                $public = 'checked';
                $choosePrivacy = 1;
            }
            if ($group->id && $group->approvals == 1 && $group->unlisted == 0 || !$group->id && in_array('private', $validOptions) && !in_array('open', $validOptions)) {
                $private = 'checked';
                $private_value = 1;
                $choosePrivacy = 1;
            }
            if ($group->id && $group->approvals == 1 && $group->unlisted == 1 || !$group->id && !in_array('private', $validOptions) && !in_array('open', $validOptions)) {
                $closed = 'checked';
                $closed_value = 1;
                $private_value = 1;
                $choosePrivacy = 1;
            }

            $displaynone = '';
            if (!$isNew && ($group->categoryid == 7 || $group->categoryid == 6 || $group->id == $blnCircle->id)) $displaynone = 'style="display: none;"';
            $groupsModel = CFactory::getModel('groups');
            $course_group = $groupsModel->getIsCourseGroup($group->id);
            ?>
            <input type="hidden" name="groupcourseid" value="<?php echo $course_group ?>">
            <div class="joms-form__group" id="privacy-option" <?php echo $displaynone ?>>
                <div class="privacyHead">
                    <span class="title" id ="circle-privacy"><?php echo JText::_('COM_COMMUNITY_GROUPS_PRIVACY_OPTIONS'); ?><span class="joms-required">*</span></span>
                </div>
                <div class="circlePrivacy">
                    <?php if (in_array('open', $validOptions)) { ?>
                    <div class="privacyPublic">
                        <span class="circleOption checkPublic <?php echo $public; ?>" id="public"></span>
                        <span class="privacyTip"><span class="privacyTipTitle"><?php echo JText::_('COM_COMMUNITY_CIRCLE_OPEN'); ?>: </span><?php echo JText::_('COM_COMMUNITY_CIRCLE_OPEN_TIP'); ?></span>
                    </div>
                    <?php } ?>
                    <?php if (in_array('private', $validOptions)) { ?>
                    <div class="privacyPrivate">
                        <span class="circleOption checkPrivate <?php echo $private; ?>" id="private"></span>
                        <span class="privacyTip"><span class="privacyTipTitle"><?php echo JText::_('COM_COMMUNITY_GROUPS_TEXT_PRIVATE'); ?>: </span><?php echo JText::_('COM_COMMUNITY_GROUPS_LABEL_PRIVATE'); ?></span>
                    </div>
                    <?php } ?>
                    <?php if (in_array('closed', $validOptions)) { ?>
                    <input type="hidden" id="circlePrivate" name="approvals" value="<?php echo $private_value; ?>" />
                    <div class="privacyClose">
                        <span class="circleOption checkClose <?php echo $closed; ?>" id="close"></span>
                        <span class="privacyTip"><span class="privacyTipTitle"><?php echo JText::_('COM_COMMUNITY_GROUPS_TEXT_CLOSED'); ?>: </span><?php echo JText::_('COM_COMMUNITY_GROUPS_LABEL_SECRET'); ?></span>
                        <input type="hidden" id="circleClose" name="unlisted" value="<?php echo $closed_value; ?>" />
                    </div>
                    <?php } ?>
                    <input type="hidden" id="choosePrivacy" name="privacy" value="<?php echo $choosePrivacy; ?>" />
                </div>
                <div class="error">
                    <p class="joms-help privacy" style="display: none; color: red;">Please choose Privacy</p>
                </div>
                <input name="parentid" type="hidden" value="<?php echo $isNew ? $this->escape($parentid) : $this->escape($group->parentid) ?>">
            </div>
            <?php
            if(!$isNew && ($group->categoryid == 7) && ($group->moodlecategoryid != 0)) {
                ?>
                <div class="joms-form__group paypalDetail">
                    <h2><?php echo JText::_('COM_COMMUNITY_CIRCLE_PAYPAL_DETAIL'); ?></h2>
                </div>
                <div class="joms-form__group joms-textarea--mobile">
                    <span class="title" title="<?php echo JText::_('COM_COMMUNITY_CIRCLE_EMAIL')?>"><?php echo JText::_('COM_COMMUNITY_CIRCLE_EMAIL'); ?><span class="joms-required">*</span></span>
                    <input type="text" name="email" id="gr-email" value="<?php echo $group->email; ?>" required class="joms-input" />
                </div>
                <div class="joms-form__group joms-textarea--mobile">
                    <span class="title" title="<?php echo JText::_('COM_COMMUNITY_CIRCLE_BILLING_ADDRESS')?>"><?php echo JText::_('COM_COMMUNITY_CIRCLE_BILLING_ADDRESS'); ?><span class="joms-required">*</span></span>
                    <input type="text" name="billingaddress" id="gr-billingaddress" value="<?php echo $group->billingaddress; ?>" required class="joms-input" />
                </div>
                <div class="joms-form__group joms-textarea--mobile">
                    <span class="title" title="<?php echo JText::_('COM_COMMUNITY_CIRCLE_POSTAL_CODE')?>"><?php echo JText::_('COM_COMMUNITY_CIRCLE_POSTAL_CODE'); ?><span class="joms-required">*</span></span>
                    <input type="text" name="postalcode" id="gr-postalcode" value="<?php echo $group->postalcode; ?>" required class="joms-input" />
                </div>
            <?php
            }
            ?>

            <script type="text/javascript">
                var lpstatus = 1;
                <?php // if (!$group->id) { ?>
                (function($) {
                    $('.checkProvider').on('click', function() {
                        $(this).parent().toggleClass('opt-checked');
                        $(this).toggleClass('checked').find('.checkProvider').prop('checked', $(this).hasClass('checked'));
                        joms_checkPrivacy();
                    });
                    $('.checkPublic').toggle(function() {
                        $(this).addClass('checked');
                        $('.checkPrivate').removeClass('checked');
                        $('.checkClose').removeClass('checked');
                        $('#circlePrivate').val(0);
                        $('#circleClose').val(0);
                        $('#choosePrivacy').val(1);
                    },function () {
                        $(this).removeClass('checked');
                        $('#choosePrivacy').val(0);
                    });
                    $('.checkPrivate').toggle(function() {
                        $(this).addClass('checked');
                        $('.checkPublic').removeClass('checked');
                        $('.checkClose').removeClass('checked');
                        $('#circlePrivate').val(1);
                        $('#circleClose').val(0);
                        $('#choosePrivacy').val(1);
                    },function () {
                        $(this).removeClass('checked');
                        $('#choosePrivacy').val(0);
                        $('#circlePrivate').val(0);
                    });
                    $('.checkClose').toggle(function() {
                        $(this).addClass('checked');
                        $('.checkPublic').removeClass('checked');
                        $('.checkPrivate').removeClass('checked');
                        $('#circlePrivate').val(1);
                        $('#circleClose').val(1);
                        $('#choosePrivacy').val(1);
                    },function () {
                        $(this).removeClass('checked');
                        $('#choosePrivacy').val(0);
                        $('#circlePrivate').val(0);
                    });
//                $('.checkPrivate').on('click', function() {
//                    if($('.checkPublic').hasClass('checked') || $('.checkClose').hasClass('checked')) {
//                        $('.checkPublic').removeClass('checked');
//                        $('.checkClose').removeClass('checked');
//                    }
//                    $('#circlePrivate').val(1);
//                    $('#circleClose').val(0);
//                    $('#choosePrivacy').val(1);
//                    $(this).toggleClass('checked').find('.checkPrivate').prop('checked', $(this).hasClass('checked'));
//                });
                    if ($('#public').is(':disabled')){
                        $('#public').parent().removeClass("opt-checked");
                        $('#private').parent().addClass("opt-checked");
                        $('#secret').removeAttr('disabled');
                    }

                    $('input:radio').on('click',function(){
                        $('input:not(:checked)').parent().removeClass("opt-checked");
                        $('input:checked').parent().addClass("opt-checked");
                    });

                })(jQuery);


                <?php // } ?>
                function joms_checkPrivacy() {
                    var eventClosedCheckbox = joms.jQuery('[id=private]');
                    var eventPublicCheckbox = joms.jQuery('[id=public]');
                    var eventUnlistedCheckbox = joms.jQuery('[id=close]');
                    var eventLPCheckbox = joms.jQuery('#lp');

                    if( eventLPCheckbox.hasClass('checked')){
                        //joms.jQuery('#lp-start').modal('toggle');
                        //joms.jQuery('#lp-start').modal('show');
                        joms.jQuery('#register-form').show();
                        joms.jQuery('#submit-form').hide();
                        eventClosedCheckbox.addClass('disabled');
                        eventPublicCheckbox.addClass('disabled');
                        eventUnlistedCheckbox.addClass('disabled');
                    }else{
                        joms.jQuery('#register-form').hide();
                        joms.jQuery('#submit-form').show();
                        eventClosedCheckbox.removeClass('disabled');
                        eventPublicCheckbox.removeClass('disabled');
                        eventUnlistedCheckbox.removeClass('disabled');
                    }

                    if( eventClosedCheckbox.prop('checked') === true ) {
                        eventUnlistedCheckbox.removeAttr('disabled');

                    } else {
                        eventUnlistedCheckbox[0].checked = false;
                        eventUnlistedCheckbox.attr('disabled', 'disabled');
                    }

                    if( eventPublicCheckbox('checked') === true ) {
                        eventUnlistedCheckbox[0].checked = false;
                        eventUnlistedCheckbox.attr('disabled', 'disabled');
                    }
                }

                jQuery('.option-lp').on('click',function(){
                    var eventLPCheckbox = joms.jQuery('[id=lp]');
                    if(eventLPCheckbox.prop('checked') === true){
                        if(lpstatus == 1){
                            jQuery('#lp').prop('checked', true);
                            jQuery('#lp').parent().addClass("opt-checked");
                            lpstatus = 0;
                        }else{
                            jQuery('#lp').prop('checked', false);
                            jQuery('#lp').parent().removeClass("opt-checked");
                            joms.jQuery('#register-form').hide();
                            joms.jQuery('#submit-form').show();

                            jQuery('#public').prop('checked', true);
                            jQuery('#public').parent().addClass("opt-checked");
                            lpstatus = 1;
                        }
                    }

                });

            </script>

            <!-- start -->
            <div style="visibility: hidden; display: none;">
                <div class="joms-form__group">
                    <span title="<?php echo JText::_('COM_COMMUNITY_GROUPS_SUMMARY_TIPS')?>"><?php echo JText::_('COM_COMMUNITY_GROUPS_SUMMARY'); ?></span>
                    <textarea class="joms-textarea" name="summary" data-maxchars="120"><?php echo $this->escape($group->summary); ?></textarea>
                </div>
            </div>

            <script type="text/javascript">
                function joms_changePrivacy() {
                    groupVal = jQuery("#subgroup :selected").val();
                    if(groupVal > 0){
                        jQuery("#private").prop("checked", true);
                    }
                }
            </script>

            <!-- start -->
            <div style="visibility: hidden; display: none;">

                <?php if ($config->get('enablephotos') && $config->get('groupphotos')) { ?>
                    <?php $photoAllowed = $params->get('photopermission', 1) >= 1; ?>

                    <div class="joms-form__group">
                        <span><?php echo JText::_('COM_COMMUNITY_GROUPS_RECENT_PHOTO'); ?></span>
                        <div>
                            <label class="joms-checkbox">
                                <input type="checkbox" class="joms-checkbox joms-js--group-photo-flag" name="photopermission-admin" value="1"<?php echo $photoAllowed ? ' checked="checked"' : ''; ?>>
                                <span title="<?php echo JText::_('COM_COMMUNITY_GROUPS_PHOTO_PERMISSION_TIPS'); ?>"><?php echo JText::_('COM_COMMUNITY_GROUPS_PHOTO_UPLOAD_ALOW_ADMIN'); ?></span>
                            </label>
                        </div>
                        <div class="joms-js--group-photo-setting" style="display:none">
                            <label class="joms-checkbox">
                                <input type="checkbox" class="joms-checkbox" name="photopermission-member" value="1"<?php echo $photoAllowed ? '' : ' disabled="disabled"'; ?><?php echo $photoAllowed && ( $params->get('photopermission') == GROUP_PHOTO_PERMISSION_ALL ) ? ' checked="checked"' : ''; ?>>
                                <span title="<?php echo JText::_('COM_COMMUNITY_GROUPS_PHOTO_UPLOAD_ALLOW_MEMBER_TIPS')?>"><?php echo JText::_('COM_COMMUNITY_GROUPS_PHOTO_UPLOAD_ALLOW_MEMBER'); ?></span>
                            </label>
                            <select type="text" class="joms-select" name="grouprecentphotos" title="<?php echo JText::_('COM_COMMUNITY_GROUPS_RECENT_PHOTOS_TIPS'); ?>">
                                <?php for($i = 2; $i <= 10; $i = $i+2){ ?>
                                    <option value="<?php echo $i; ?>" <?php echo ($group->grouprecentphotos == $i || ($i == 6 && $group->grouprecentphotos == 0)) ? 'selected': ''; ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                <?php } ?>

                <?php if ($config->get('enablevideos') && $config->get('groupvideos')) { ?>
                    <?php $videoAllowed = $params->get('videopermission', 1) >= 1; ?>

                    <div class="joms-form__group">
                        <span><?php echo JText::_('COM_COMMUNITY_GROUPS_RECENT_VIDEO'); ?></span>
                        <div>
                            <label class="joms-checkbox">
                                <input type="checkbox" class="joms-checkbox joms-js--group-video-flag" name="videopermission-admin" value="1"<?php echo $videoAllowed ? ' checked="checked"' : ''; ?>>
                                <span title="<?php echo JText::_('COM_COMMUNITY_GROUPS_VIDEOS_PERMISSION_TIPS'); ?>"><?php echo JText::_('COM_COMMUNITY_GROUPS_VIDEO_UPLOAD_ALLOW_ADMIN'); ?></span>
                            </label>
                        </div>
                        <div class="joms-js--group-video-setting" style="display:none">
                            <label class="joms-checkbox">
                                <input type="checkbox" class="joms-checkbox" name="videopermission-member" value="1"<?php echo $videoAllowed ? '' : ' disabled="disabled"'; ?><?php echo $videoAllowed && ( $params->get('videopermission') == GROUP_VIDEO_PERMISSION_ALL ) ? ' checked="checked"' : ''; ?>>
                                <span title="<?php echo JText::_('COM_COMMUNITY_GROUPS_VIDEO_UPLOAD_ALLOW_MEMBER_TIPS')?>"><?php echo JText::_('COM_COMMUNITY_GROUPS_VIDEO_UPLOAD_ALLOW_MEMBER'); ?></span>
                            </label>
                            <select type="text" class="joms-select" name="grouprecentvideos" title="<?php echo JText::_('COM_COMMUNITY_GROUPS_RECENT_VIDEO_TIPS'); ?>">
                                <?php for ($i = 2; $i <= 10; $i = $i+2) { ?>
                                    <option value="<?php echo $i; ?>" <?php echo ($group->grouprecentvideos == $i || ($i == 6 && $group->grouprecentvideos == 0)) ? 'selected': ''; ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                <?php } ?>

                <?php if ($config->get('enableevents') && $config->get('group_events')) { ?>
                    <?php $eventAllowed = $params->get('eventpermission', 1) >= 1; ?>

                    <div class="joms-form__group">
                        <span><?php echo JText::_('COM_COMMUNITY_GROUP_EVENTS'); ?></span>
                        <div>
                            <label class="joms-checkbox">
                                <input type="checkbox" class="joms-checkbox joms-js--group-event-flag" name="eventpermission-admin" value="1"<?php echo $eventAllowed ? ' checked="checked"' : ''; ?>>
                                <span title="<?php echo JText::_('COM_COMMUNITY_GROUP_EVENTS_PERMISSIONS'); ?>"><?php echo JText::_('COM_COMMUNITY_GROUP_EVENTS_ADMIN_CREATION'); ?></span>
                            </label>
                        </div>
                        <div class="joms-js--group-event-setting" style="display:none">
                            <label class="joms-checkbox">
                                <input type="checkbox" class="joms-checkbox" name="eventpermission-member" value="2" checked="checked">
                                <span title="<?php echo JText::_('COM_COMMUNITY_GROUPS_EVENTS_MEMBERS_CREATION_TIPS')?>"><?php echo JText::_('COM_COMMUNITY_GROUP_EVENTS_MEMBERS_CREATION'); ?></span>
                            </label>
                            <select type="text" class="joms-select" name="grouprecentevents" title="<?php echo JText::_('COM_COMMUNITY_GROUPS_EVENT_TIPS'); ?>">
                                <?php for ($i = 2; $i <= 10; $i = $i+2) { ?>
                                    <option value="<?php echo $i; ?>" <?php echo ($group->grouprecentevents == $i || ($i == 6 && $group->grouprecentevents == 0)) ? 'selected': ''; ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                <?php } ?>

                <!-- end -->
            </div>

            <script>
                joms.onStart(function( $ ) {
                    $('.joms-js--group-photo-flag').on( 'click', function() {
                        var $div = $('.joms-js--group-photo-setting'),
                            $checkbox = $div.find('input');

                        if ( this.checked ) {
                            $checkbox.removeAttr('disabled');
                            $div.show();
                        } else {
                            $checkbox[0].checked = false;
                            $checkbox.attr('disabled', 'disabled');
                            $div.hide();
                        }
                    }).triggerHandler('click');

                    $('.joms-js--group-video-flag').on( 'click', function() {
                        var $div = $('.joms-js--group-video-setting'),
                            $checkbox = $div.find('input');

                        if ( this.checked ) {
                            $checkbox.removeAttr('disabled');
                            $div.show();
                        } else {
                            $checkbox[0].checked = false;
                            $checkbox.attr('disabled', 'disabled');
                            $div.hide();
                        }
                    }).triggerHandler('click');

                    $('.joms-js--group-event-flag').on( 'click', function() {
                        var $div = $('.joms-js--group-event-setting'),
                            $checkbox = $div.find('input');

                        if ( this.checked ) {
                            $checkbox.removeAttr('disabled');
                            $div.show();
                        } else {
                            $checkbox[0].checked = false;
                            $checkbox.attr('disabled', 'disabled');
                            $div.hide();
                        }
                    }).triggerHandler('click');
                });
            </script>

            <!-- start -->
            <div style="visibility: hidden; display: none;">
                <?php if ($config->get('groupdiscussfilesharing') && $config->get('creatediscussion')) { ?>

                    <div class="joms-form__group">
                        <span><?php echo JText::_('COM_COMMUNITY_DISCUSSION'); ?></span>
                        <label class="joms-checkbox">
                            <input type="checkbox" class="joms-checkbox" name="groupdiscussionfilesharing" value="1"<?php echo ($params->get('groupdiscussionfilesharing') >= 1 || $isNew) ? ' checked="checked"' : ''; ?>>
                            <span title="<?php echo JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_ENABLE_FILE_SHARING_TIPS'); ?>"><?php echo JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_ENABLE_FILE_SHARING'); ?></span>
                        </label>
                        <input type="hidden" name="discussordering" value="0" />
                        <!--<label>
                <input type="checkbox" class="joms-checkbox" name="discussordering" value="1"<?php echo ($group->discussordering == 1 || $isNew) ? ' checked="checked"' : ''; ?>>
                <span title="<?php echo JText::_('COM_COMMUNITY_GROUPS_ORDERING_TIPS'); ?>"><?php echo JText::_('COM_COMMUNITY_GROUPS_DISCUSS_ORDER_CREATION_DATE'); ?></span>
            </label>-->
                    </div>

                <?php } ?>

                <?php if ($config->get('createannouncement')) { ?>

                    <div class="joms-form__group">
                        <span><?php echo JText::_('COM_COMMUNITY_ANNOUNCEMENT'); ?></span>
                        <label class="joms-checkbox">
                            <input type="checkbox" class="joms-checkbox" name="groupannouncementfilesharing" value="1"<?php echo ($params->get('groupannouncementfilesharing') >= 1 || $isNew) ? ' checked="checked"' : ''; ?>>
                            <span title="<?php echo JText::_('COM_COMMUNITY_GROUPS_ANNOUNCEMENT_ENABLE_FILE_SHARING_TIPS'); ?>"><?php echo JText::_('COM_COMMUNITY_GROUPS_ANNOUNCEMENT_ENABLE_FILE_SHARING'); ?></span>
                        </label>
                    </div>

                <?php } ?>

                <div class="joms-form__group">
                    <span><?php echo JText::_('COM_COMMUNITY_GROUPS_NOTIFICATION'); ?></span>
                    <label class="joms-checkbox">
                        <input type="checkbox" class="joms-checkbox" name="newmembernotification" value="1"<?php echo ($params->get('newmembernotification') || $isNew) ? ' checked="checked"' : ''; ?>>
                        <span title="<?php echo JText::_('COM_COMMUNITY_GROUPS_NEW_MEMBER_NOTIFICATION_TIPS'); ?>"><?php echo JText::_('COM_COMMUNITY_GROUPS_NEW_MEMBER_NOTIFICATION'); ?></span>
                    </label>
                    <label class="joms-checkbox">
                        <input type="checkbox" class="joms-checkbox" name="joinrequestnotification" value="1"<?php echo $params->get('joinrequestnotification', '1') == true  ? ' checked="checked"' : ''; ?>>
                        <span title="<?php echo JText::_('COM_COMMUNITY_GROUPS_JOIN_REQUEST_NOTIFICATION_TIPS'); ?>"><?php echo JText::_('COM_COMMUNITY_GROUPS_JOIN_REQUEST_NOTIFICATION'); ?></span>
                    </label>
                    <label class="joms-checkbox">
                        <input type="checkbox" class="joms-checkbox" name="wallnotification" value="1"<?php echo $params->get('wallnotification', '1') == true ? ' checked="checked"' : ''; ?>>
                        <span title="<?php echo JText::_('COM_COMMUNITY_GROUPS_WALL_NOTIFICATION_TIPS'); ?>"><?php echo JText::_('COM_COMMUNITY_GROUPS_WALL_NOTIFICATION'); ?></span>
                    </label>
                </div>

            </div>
            <!-- end -->
            <?php if ($afterFormDisplay) { ?>
                <div class="joms-form__group"><?php echo $afterFormDisplay; ?></div>
            <?php } ?>

            <div class="joms-form__group js-form-edit-group">
                <span></span>
                <div>
                    <?php if ($isNew) { ?>
                        <input name="action" type="hidden" value="save">
                    <?php } ?>
                    <input type="hidden" name="groupid" value="<?php echo $group->id; ?>">
                    <input type="hidden" id="created" value="<?php echo isset($justcreated) ? $justcreated : false; ?>">
                    <?php echo JHTML::_('form.token'); ?>
                    <input type="button" value="<?php echo JText::_('COM_COMMUNITY_CANCEL_BUTTON'); ?>" class="joms-button--neutral joms-button--smaller js-button-left" onclick="history.go(-1); return false;">
                    <input type="button" id="submit-form" value="<?php echo JText::_('COM_COMMUNITY_SAVE_BUTTON'); ?>" class="joms-button--primary joms-button--smaller js-button-right">
                    <input type="submit" id="register-form" style="display: none" value="<?php echo JText::_('COM_COMMUNITY_GROUPS_REGISTER'); ?>" class="joms-button--primary joms-button--smaller js-button-right">
                </div>
            </div>
        </div>
    </form>

    <div id="success" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="joms-popup__title"><button class="mfp-close" type="button" title="Close (Esc)">×</button>Add Members</div>
                <span><?php echo JText::_('COM_COMMUNITY_CREATE_CIRCLE_SUCCESS'); ?></span>
                <br/><br/>
                <a id="addMember" style="width: 100%;" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=invitemembers&groupid='.$group->id); ?>" class="joms-focus__button--message"><?php echo JText::_('COM_COMMUNITY_ADDMEMBERS_TO_CIRCLE'); ?> <span class="arrow">→</span></a>
            </div>
        </div>
    </div>
    <div id="lp-start" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="lpstartTitle">
                    <center><h3><?php echo JText::_('COM_COMMUNITY_CREATE_LEARNING_CIRCLE_START'); ?></h3></center>
                </div>
                <div class="lpstartContent">
                    <?php echo JText::_('COM_COMMUNITY_CREATE_LEARNING_CIRCLE'); ?>
                </div>
                <br/>
                <a id="registerLearningProvider" href="<?php echo CRoute::_('index.php?option=com_rsform&formId=5'); ?>" class="joms-focus__button--message"><?php echo JText::_('COM_COMMUNITY_NEXT'); ?></a>
            </div>
        </div>
    </div>

    <div id="lp-thanks" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <span><?php echo JText::_('COM_COMMUNITY_THANKS_LEARNING_CIRCLE'); ?></span>
                <br /> <br />
                <a id="closepopup" href="<?php echo CRoute::_("index.php?option=com_community&view=groups&task=mygroups"); ?>" class="joms-focus__button--message"> <?php echo JText::_('COM_COMMUNITY_BACK_CIRCLE'); ?> <span class="arrow"></span></a>
            </div>
        </div>
    </div>
    <div id="nameCircle" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static">
        <div class="modal-dialog modal-sm">
            <div class="modal-content hienPopup">
                <p><?php echo JText::_('COM_COMMUNITY_SAME_NAME_CIRCLE'); ?></p>
                <button id="closepopup" class="closetitle" type="button"><?php echo JText::_('COM_COMMUNITY_CLOSE'); ?></button>
            </div>
        </div>
    </div>
</div>

<script>
    function joms_validate_form() {
        return false;
    }
    (function( w ) {
        w.joms_queue || (w.joms_queue = []);
        w.joms_queue.push(function() {
            joms_validate_form = function( $form ) {
                var errors = 0;
                $form = joms.jQuery( $form );
                $form.find('[required]').each(function() {
                    var $el = joms.jQuery( this );
                    if ( !joms.jQuery.trim( $el.val() ) ) {
                        $el.triggerHandler('blur');
                        errors++;
                    }
                });
                $form.find('input[name="privacy"]').each(function() {
                    var $el = joms.jQuery( this );
                    if ( !joms.jQuery.trim( $el.val() ) ) {
                        $el.triggerHandler('blur');
                        errors++;
                        joms.jQuery('.joms-help.privacy').css('display', 'block');
                    }
                });

                return !errors;
            }
        });
    })( window );
    (function($) {

        $(document).on("keypress", "form", function(event) {
            return event.keyCode != 13;
        });

        $('#addMember').on('click', function() {
            joms.jQuery('#success').modal('hide');
        })

        //success popup after created group
        var type = $('#created').val();
        if (type) {
            $('#success').modal('toggle');
            $('#success').modal('show');
        }

        $('.mfp-close').on('click', function(){
            joms.jQuery('#success').modal('hide');
        });

        //$('#lp-start').modal('toggle');
        //$('#lp-start').modal('show'); 

        $('#register-form').on('click', function(e) {
            e.preventDefault();
            $('#lp-start').modal('toggle');
            $('#lp-start').modal('show');
        })

        $('#nameCircle #closepopup').on('click', function(e) {
         e.preventDefault();
            $("#submit-form").removeAttr('disabled');
            $('#nameCircle').modal('hide');
        });

        if (window.location.href.indexOf("lpthanks") > -1) {
            $('#lp-thanks').modal('toggle');
            $('#lp-thanks').modal('show');
        }
        $( "#submit-form" ).click(function( event ) {
            $(this).attr("disabled", "disabled");
            if(jQuery(".txtaTitle").val().trim() == '' && (jQuery('#gr-description').val().trim()) !=  ''){
                jQuery('#circle_name').addClass('error1');
                jQuery('#gr-name').addClass('error1');
                $(this).removeAttr('disabled');
                return false;
            }
            if ((jQuery('#gr-description').val().trim()) ==  '' && jQuery(".txtaTitle").val().trim() != '') {
                jQuery('#gr-description').addClass('error1');
                jQuery('#circle_description').addClass('error1');
                $(this).removeAttr('disabled');
                return false;
            }
            if ((jQuery('#gr-description').val().trim()) ==  '' && jQuery(".txtaTitle").val().trim() == '') {
                jQuery('#gr-description').addClass('error1');
                jQuery('#circle_description').addClass('error1');
                jQuery('#circle_name').addClass('error1');
                jQuery('#gr-name').addClass('error1');
                $(this).removeAttr('disabled');
                return false;
            }

            if ($('.joms-page .circlePrivacy .checked').length < 1) {
                jQuery('#circle-privacy').addClass('error1');
                $('.joms-page .circlePrivacy .circleOption').each(function () {
                    $(this).addClass("error1");
                    $(this).click(function () {
                        jQuery('#circle-privacy').removeClass('error1');
                        $('.joms-page .circlePrivacy .circleOption').removeClass("error1");
                    });
                });
                $(this).removeAttr('disabled');
                return false;
            }
            if(jQuery('.txtaTitle').val().trim() !=''  && (jQuery('#gr-description').val().trim()) !=  ''){
               var name = jQuery('.txtaTitle').val().trim();
               var sta;
               var categoryid = 0;
                <?php if ($_REQUEST['task'] == 'edit') { ?>
                var old_name = "<?php echo $group->name; ?>";
                if(old_name == name){
                    $( "#new_circle" ).submit();
                    return false;
                }
                <?php } ?>
               jQuery.ajax({
               url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxCheckNameCircle') ?>",
               type: 'POST',
               data: {
                   'name': name,
                   'categoryid': categoryid
               },
               beforeSend: function () {
               },
               success: function (result) {
                    var res = jQuery.parseJSON(result);
                    if(res.status == true){
                        jQuery('#circle_name').addClass('error1');
                        jQuery('#gr-name').addClass('error1');
                        lgtCreatePopup('oneButton', {
                            content: '<?php echo JText::_('COM_COMMUNITY_SAME_NAME_CIRCLE'); ?>',
                        }, function() {
                            lgtRemovePopup();
                        });
                        return false;
                    }
                    else{
                        $( "#new_circle" ).submit();
                        event.preventDefault();
                    }
               },
               error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
               }
            });
            }             
        });
        jQuery('#gr-name').keyup(function () {
            if (jQuery('#gr-name').val().trim() !=  '') {
                jQuery('#circle_name').removeClass('error1');
                jQuery('#gr-name').removeClass('error1');
            }
        });
        jQuery('#gr-description').keyup(function () {
            if (jQuery('#gr-description').val().trim() !=  '') {
                jQuery('#gr-description').removeClass('error1');
                jQuery('#circle_description').removeClass('error1');
            }
        });

    })(jQuery);
    
</script>

<script>
    (function($) {
        $('#keywords').tagsInput();
        $(document).ready(function(){
            $("#keywords_tag").focus(function(){
                $("#keywords_tagsinput").addClass("active");
            });

            $("#keywords_tag").focusout(function(){
                $("#keywords_tagsinput").removeClass("active");
            });

        });
    })(jQuery);
</script>