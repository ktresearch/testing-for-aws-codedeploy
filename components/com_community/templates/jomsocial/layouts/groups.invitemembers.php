<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();
$device = JFactory::getSession()->get('device');
$is_mobile = ($device == 'mobile') ? true : false;
$num = $is_mobile ? 5 : 6;
?>
<div id="community-groups-wrap" class="joms-page pageAddApprove">
    <?php
    if (!$isAdmin) {
        echo '<div class="message-error">' . JText::_('COM_COMMUNITY_ACCESS_FORBIDDEN') . '</div>';
    } else {
    ?>
    <div class="pageContent">
        <?php if ($isPrivateCircle) { ?>
        <div class="divMenuChangeTabs">
            <div class="menuChangeTabs menuAddApprove">
                <div class="tab lgtActive" id="hvnAddMembers" data-col="colAddMembers" style="width:50%;"><?php echo JText::_('COM_COMMUNITY_ADD_MEMBERS');?></div>
                <div class="tab" id="hvnApproveRequests" data-col="colApproveRequests" style="width:50%;"><?php echo JText::_('COM_COMMUNITY_APPROVE_REQUESTS');?></div>
            </div>
        </div>
        <?php } else { ?>
            <h2 class="pageTitle"><?php echo JText::_('COM_COMMUNITY_ADD_MEMBERS');?></h2>
        <?php } ?>
        <div class="divAddApprove divColumns">
            <div class="col colAddMembers lgt-visible" style="left: 0">
                <form name="invited-list" class="formAddMembers" id="formAddMembers" action="<?php echo CRoute::getURI();?>" method="post">
                    <input type="hidden" name="groupid" value="<?php echo $group->id;?>" />

                    <div id="friendsList">
                        <div class="container">
                            <div class="row">
                                <div class="listFriends">
                                    <?php if (!empty($friends)) {
                                        $i = 1;
                                        ?>
                                        <?php foreach ($friends as $user) { ?>
                                            <div class="col-xs-12 col-sm-6">
                                                <div id="friend<?php echo $i; ?>" class="friend friend<?php echo $i; echo ($i > $num) ? ' lgt-invisible ' : '' ?>">
                                                    <div class="inputCheckbox">
                                                        <div class="status"></div>
                                                        <input name="invite-list[]" type="checkbox" value="<?php echo $user->id ?>"/>
                                                    </div>
                                                    <div class="friendDetail">
                                                        <div class="friendImage">
                                                            <img src="<?php echo $user->getThumbAvatar();?>" alt="Avatar" />
                                                        </div>
                                                        <div class="friendName">
                                                            <span><?php echo $user->getDisplayName();?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            $i++;
                                        } ?>
                                    <?php } else {
                                        echo '<p class="noApproveRequestsText">'.JText::_('COM_COMMUNITY_ADD_MEMBERS_TEXT1').'</p>';
                                        echo '<p class="noApproveRequestsText">'.JText::_('COM_COMMUNITY_ADD_MEMBERS_TEXT2').'</p>';
                                    } ?>
                                </div>
                            </div>
                            <?php if (count($friends) > $num) echo '<div class="friendsSeeMore btSeeMore">'.strtoupper(JText::_('COM_COMMUNITY_SEE_MORE')).'</div>';?>
                        </div>
                    </div>
                    <?php if (!empty($friends)) { ?>
                    <div class="divButtons">
                        <input class="btCancel" type="button" value="<?php echo JText::_('COM_COMMUNITY_CANCEL'); ?>" />
                        <input class="btAddMembers" type="submit" name="formSubmit" value="<?php echo JText::_('COM_COMMUNITY_ADD_MEMBERS'); ?>" />
                    </div>
                    <?php } ?>
                    <?php echo JHTML::_( 'form.token' ); ?>
                </form>
            </div>

            <?php if ($isPrivateCircle) { ?>
            <div class="col colApproveRequests" <?php echo ($is_mobile) ? 'style="left: 100%"':''?>>
                <div class="divPAMembers">
                    <div class="container">
                        <div class="row">
                            <div class="listPAMembers">
                                <?php if (!empty($pendingApprovalMembers)) {
                                    $i = 1;
                                    ?>
                                    <?php foreach ($pendingApprovalMembers as $user) { ?>
                                        <div class="col-xs-12 col-sm-6">
                                            <div id="friend<?php echo $i; ?>" class="friend friend<?php echo $i; echo ($i > $num) ? ' lgt-invisible ' : '' ?>" data-memid="<?php echo $user->id; ?>">
                                                <div class="friendDetail">
                                                    <div class="friendImage">
                                                        <img src="<?php echo $user->getThumbAvatar();?>" alt="Avatar" />
                                                    </div>
                                                    <div class="friendNameAndButtons">
                                                        <div class="friendName">
                                                            <span><?php echo $user->getDisplayName();?></span>
                                                        </div>
                                                        <div class="friendButtons">
                                                            <div class="btAccept"><?php echo JText::_('COM_COMMUNITY_PENDING_ACTION_APPROVE'); ?></div>
                                                            <div class="btDelete"><?php echo JText::_('COM_COMMUNITY_DELETE'); ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $i++;
                                    } ?>
                                <?php } else {
                                    echo '<p class="noApproveRequestsText">'.JText::_('COM_COMMUNITY_NO_APPROVE_REQUESTS').'</p>';
                                } ?>
                            </div>
                        </div>
                        <?php if (count($pendingApprovalMembers) > $num) echo '<div class="friendsSeeMore btSeeMore">'.strtoupper(JText::_('COM_COMMUNITY_SEE_MORE')).'</div>';?>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    
    <?php } ?>
</div>
<script>
    jQuery('html').addClass('lgtEnableCircleBanner');
    jQuery(document).ready(function() {
        jQuery('.divButtons .btCancel').click(function() {
            window.location.href = '<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewabout&groupid='.$group->id); ?>';
        });

        var tab_first =  '<?php  echo $tab; ?>';
        if(tab_first == 'approverequest'){
            jQuery('#hvnApproveRequests').parent('.menuChangeTabs').children().removeClass('lgtActive');
            jQuery('#hvnApproveRequests').addClass('lgtActive');
            var clickedTab = jQuery('#hvnApproveRequests').parent('.menuChangeTabs').parent('.divMenuChangeTabs').nextAll('.divColumns').children('.'+ jQuery('#hvnApproveRequests').attr('data-col'));
            clickedTab.siblings('.col').removeClass('lgt-visible');
            clickedTab.addClass('lgt-visible');

            <?php if ($is_mobile) : ?>
            clickedTab.css('left', '0');
            var order = jQuery('#hvnApproveRequests').parent('.menuChangeTabs').parent('.divMenuChangeTabs').nextAll('.divColumns').children('.col').index(clickedTab);
            clickedTab.siblings('.col').each(function() {
                var ord = clickedTab.parent().children('.col').index(jQuery('#hvnApproveRequests'));
                if (ord > order) {
                    jQuery('#hvnApproveRequests').css('left', parseInt((ord - order)*100)+'%');
                } else {
                    jQuery('#hvnApproveRequests').css('left', '-'+parseInt((order - ord)*100)+'%');
                }
            });
            <?php endif; ?>
        }

        jQuery('.menuChangeTabs .tab').click(function() {
            jQuery(this).parent('.menuChangeTabs').children().removeClass('lgtActive');
            jQuery(this).addClass('lgtActive');
            var clickedTab = jQuery(this).parent('.menuChangeTabs').parent('.divMenuChangeTabs').nextAll('.divColumns').children('.'+ jQuery(this).attr('data-col'));
            clickedTab.siblings('.col').removeClass('lgt-visible');
            clickedTab.addClass('lgt-visible');

            <?php if ($is_mobile) : ?>
            clickedTab.css('left', '0');
            var order = jQuery(this).parent('.menuChangeTabs').parent('.divMenuChangeTabs').nextAll('.divColumns').children('.col').index(clickedTab);
            clickedTab.siblings('.col').each(function() {
                var ord = clickedTab.parent().children('.col').index(jQuery(this));
                if (ord > order) {
                    jQuery(this).css('left', parseInt((ord - order)*100)+'%');
                } else {
                    jQuery(this).css('left', '-'+parseInt((order - ord)*100)+'%');
                }
            });
            <?php endif; ?>
        });

        jQuery('.friendButtons .btAccept').click(function() {
            var memberid = jQuery(this).parents('.friend').attr('data-memid');

            joms.ajax({func:"groups,ajaxApproveMember",
                data:[memberid,<?php echo $group->id;?>],
                beforeSend: function() {
                    lgtCreatePopup('', {'content': 'Loading...'});
                },
                callback:function(res){
                    console.log(res);
                    lgtRemovePopup();
                    if (res.hasOwnProperty('error')) {
                        lgtCreatePopup('withCloseButton', {'content': res.error});
                    } else {
                        jQuery('.friend[data-memid="'+memberid+'"]').parent().remove();
//                        lgtCreatePopup('withCloseButton', {'content': res.message});
                    }
                },
                error: function() {
                    lgtRemovePopup();
                }
            });
        });

        jQuery('.friendButtons .btDelete').click(function() {
            var memberid = jQuery(this).parents('.friend').attr('data-memid');
            joms.ajax({func:"groups,ajaxRemoveMember",
                data:[memberid,<?php echo $group->id;?>],
                beforeSend: function() {
                    lgtCreatePopup('', {'content': 'Loading...'});
                },
                callback:function(res){
                    console.log(res);
                    lgtRemovePopup();
                    if (res.hasOwnProperty('error')) {
                        lgtCreatePopup('withCloseButton', {'content': res.error});
                    } else {
                        jQuery('.friend[data-memid="'+memberid+'"] .friendButtons').html('<span>Request removed.</span>');
                        lgtCreatePopup('withCloseButton', {'content': res.message});
                    }
                },
                error: function() {
                    lgtRemovePopup();
                }
            });
        });

        jQuery('.pageAddApprove #friendsList .friend .status').click(function() {
            jQuery(this).next('input[type="checkbox"]').click();
            jQuery(this).toggleClass('lgtSelected');
        });

        jQuery('.pageAddApprove #friendsList .friend .friendDetail').click(function() {
            jQuery(this).prev('.inputCheckbox').find('.status').click();
        });

        jQuery('.friendsSeeMore').on('click', function() {
            var list = jQuery(this).prev();
            var size = list.find('.friend').size();
            var shown = list.find('.friend:not(.lgt-invisible)').length;
            list.find('.friend:lt(' + (shown + <?php echo $num;?>) + ')').removeClass('lgt-invisible');
            if (size <= (shown + <?php echo $num;?>)) jQuery(this).fadeOut();
        });

        jQuery('.btAddMembers').click(function(e) {
            e.preventDefault();
            var valid = false;
            jQuery('.friend .status').each(function() {
                if (jQuery(this).hasClass('lgtSelected')) {
                    valid = true;
                    return false;
                }
            });
            if (valid) jQuery('#formAddMembers').submit();
            else lgtCreatePopup('withCloseButton', {'content':'You have not selected anyone yet.'});
        });
    });
</script>