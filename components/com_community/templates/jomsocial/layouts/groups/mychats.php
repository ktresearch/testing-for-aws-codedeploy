<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();

$document = JFactory::getDocument();
$document->addStyleSheet('components/com_community/assets/release/css/discussions.css');

$input = new JInput();
$groupId = $input->get('groupid');
$topic = "";
$session = JFactory::getSession();
$device = $session->get('device');
$isMobile = false;
if ($device == 'mobile') $isMobile = true;

$mainframe = JFactory::getApplication();
$rooturl = $mainframe->getCfg('wwwrootfile');

$group = JTable::getInstance('Group', 'CTable');
$task = JFactory::getApplication()->input->getCmd('task');
?>
<?php if ($discussions == null) { ?>
    <div class="joms-page list_my_chats" style="<?php echo isset($listcirclestyle) ? $listcirclestyle : '' ?>;">
        <div class="joms-tab__bar">
            <a href="<?php echo CRoute::_("index.php?option=com_community&view=groups&task=viewchats") ?>"
               class="border-left <?php if ($task != 'mygroups' && $task != '0a-guest') echo ' active' ?>"><?php echo JText::_('COM_COMMUNITY_CHATS'); ?></a>
            <a href="<?php echo CRoute::_("index.php?option=com_community&view=groups&task=mygroups") ?>" <?php if ($task == 'mygroups' || $task == '0a-guest') echo 'class="active"' ?>><?php echo JText::_('COM_COMMUNITY_TITLE_MYCIRCLES'); ?></a>
        </div>
        <div class="noChat">
            <img src="/images/MyCircles.png">
            <p class="description"><?php echo JText::_("COM_COMMUNITY_NO_CHAT_DESCRIPTION"); ?></p>
            <button id="noChatSearchCircle"><?php echo JText::_("COM_COMMUNITY_BUTTON_SEARCH_CIRCLE"); ?></button>
        </div>
    </div>
    <script type="text/javascript">
        (function ($) {
            var check_device = "<?php echo $device; ?>";
            $("#noChatSearchCircle").click(function () {
                $(".search-all-type").css("display", "none");
                $("#searchlearning").css("display", "none");
                $("#searchfriend").css("display", "none");
                $(".navbar-menu-icon").css("display", "none");
                $(".navbar-search-all").addClass("hide");
                $(".navbar-notification").addClass("hide");
                $(".search-all-type-form").css("display", "block");
                $("#searchcircle").css("display", "inline-block");
                $("#searchcircle .search-text").focus();
                if (check_device == 'mobile') {
                    $(".navbar-brand").hide();
                    $('.navbar-toggle').hide();
                }
            });
        })(jQuery);
    </script>
<?php } else { ?>

    <div class="joms-page list_my_chats" style="<?php echo $listcirclestyle ?>;">
        <div class="joms-tab__bar">
            <a href="<?php echo CRoute::_("index.php?option=com_community&view=groups&task=viewchats") ?>"
               class="border-left <?php if ($task != 'mygroups' && $task != '0a-guest') echo ' active' ?>"><?php echo JText::_('COM_COMMUNITY_CHATS'); ?></a>
            <a href="<?php echo CRoute::_("index.php?option=com_community&view=groups&task=mygroups") ?>" <?php if ($task == 'mygroups' || $task == '0a-guest') echo 'class="active"' ?>><?php echo JText::_('COM_COMMUNITY_TITLE_MYCIRCLES'); ?></a>
        </div>

        <div class="discussion_content">
            <!-- Box list my chats -->

            <div class="left-column">
                <div class="topic_scroll">
                    <?php
                    if ($discussions) {
                        $i = 0;
                        foreach ($discussions as $row) {
                            if (!empty($row->cover)) {
                                $thumbnail = $rooturl . '/' . $row->cover;
                            } else {
                                $thumbnail = '/components/com_community/assets/chat-default.png';
                            }
                            $date = JFactory::getDate($row->lastreplied);
                            $time = CTimeHelper::discussionsTimeLapse($date, false);
                            $row->message = strip_tags($row->message, true);
                            $groupId = $row->groupid;
                            $creator = CFactory::getUser($row->creator);

                            if (!$isMobile && $i == 0) {
                                $firstgroupid = $groupId;
                                $firsttopicid = $row->id;
                            }
                            $group->load($groupId);

                            $groupavatar = '/components/com_community/assets/group.png';
                            if ($group->avatar != '') $groupavatar = $rooturl . '/' . $group->avatar;
                            
                            $link = 'index.php?option=com_community&view=groups&task=viewabout&groupid=' . $groupId;
                            ?>
                            <div class="joms-list__item topic topicid_<?php echo $row->id; ?>" onclick="loadChat(<?php echo $groupId . ', ' . $row->id; ?>);">
                                <div class="topic-image">
                                    <div class="jom-list__avatar">
                                        <img src="<?php echo $thumbnail; ?>" alt="<?php echo $row->title; ?>"
                                             class="img_chatavatar">
                                        <a class="linkcircle" href="<?php echo CRoute::_($link); ?>">
                                            <img src="<?php echo $groupavatar; ?>" alt="<?php echo $group->name; ?>" class="img_circleavatar <?php echo ($row->course_group > 0) ? 'img_learning' : ''; ?>">
                                        </a>
                                    </div>
                                </div>

                                <div class="topic-item">
                                    <div class="topic-content">
                                        <div class="topicName <?php echo ($row->notification > 0) ? 'name_highlight' : ''; ?>">
                                            <a href="#"><?php
                                                echo $isMobile ? CActivities::truncateComplex($row->title, 12, true) : CActivities::truncateComplex($row->title, 40, true);
                                                ?></a>
                                        </div>
                                        <div class="time"><span
                                                    class="margin-0 date joms--time"><?php echo $time; ?></span></div>
                                    </div>

                                    <div class="des joms--description">
                                        <div class="des_text <?php echo ($row->notification > 0) ? 'col1' : '' ?>">
                                            <?php
                                            if ($row->lastreplytype == 'uploadphoto') {
                                                echo '<img src="/components/com_community/assets/camera-alt.png"/>' . ' <p class="hasicon">' . $row->lastmessage . '</p>';
                                            } else if ($row->lastreplytype == 'uploadfile') {
                                                echo '<img src="/components/com_community/assets/file.png"/>' . ' <p class="hasicon">' . $row->lastmessage . '</p>';
                                            } else if ($row->lastreplytype == 'website') {
                                                echo '<img src="/components/com_community/assets/link.png"/>' . ' <p class="hasicon">' . $row->lastmessage . '</p>';
                                            } else {
                                                echo '<p>';
                                                echo $isMobile ? CActivities::truncateComplex($row->lastmessage, 35, true) : CActivities::truncateComplex($row->lastmessage, 70, true);
                                                echo '</p>';
                                            }
                                            ?>
                                        </div>
                                        <?php echo ($row->notification > 0) ? '<div class="mes-count col2"><div>' . $row->notification . '</div></div>' : ''; ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $i++;
                        }
                    }
                    ?></div>
            </div>

            <!-- Box view detail chat -->
            <div class="right-column">
                <?php // $this->renderModules('mod_community_notifications'); ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        (function ($) {
            jQuery('.topicName').each(function () {
                if (jQuery(this).find('a').height() > 24) {
                    jQuery(this).find('a').addClass('limited');
                }
            });
            jQuery('.des_text').each(function () {
                if (jQuery(this).find('p').height() > 48) {
                    jQuery(this).find('p').addClass('limited');
                }
            });

            jQuery('.topic .linkcircle').click(function(){ 
                window.location = jQuery(this).attr('href');;
                return false;
            });

            // Load first chat
            <?php if (!$isMobile) { ?>
            jQuery.post("<?php echo CRoute::_('index.php?option=com_community&view=groups&task=loadDiscussionDetail') ?>", {
                'groupid': <?php echo $firstgroupid;?>,
                'topicid': <?php echo $firsttopicid;?>
            }, function (data) {
                jQuery('.joms-list__item').removeClass('active');
                jQuery('.topicid_<?php echo $firsttopicid;?>').addClass('active');
                jQuery('.discussion_content .right-column').html(data);
                jQuery('.topicid_<?php echo $firsttopicid;?> .topicName').removeClass('name_highlight');
                jQuery('.topicid_<?php echo $firsttopicid;?> .mes-count').hide();
                jQuery(document).on('chatAppended', '.joms-js--comments', function() {
                    var count = 0;
                    jQuery('.topicDetail_scroll > .joms-comment.joms-js--comments').css('overflow', 'hidden');
                    var iframeHeight = jQuery('.topicDetail_scroll > .joms-comment.joms-js--comments').height();
                    var interval = setInterval(function() {
                        console.log(iframeHeight);
                        if (jQuery('.topicDetail_scroll > .joms-comment.joms-js--comments').height() == iframeHeight) {
                            count++;
                            console.log(count);
                        } else {
                            count = 0;
                            iframeHeight = jQuery('.topicDetail_scroll > .joms-comment.joms-js--comments').height();
                        }
                        if (count == 3) {
                            clearInterval(interval);
                            jQuery(".topicDetail_scroll").animate({ scrollTop: iframeHeight }, 300);
                        }
                    }, 500);
                });
                jQuery('.joms-js--comments').prepend(jQuery('.joms-js--more-comments').parent().html()).trigger('chatAppended');

            }).fail(function (xhr, ajaxOptions, thrownError) { //any errors?
                console.log(thrownError); //alert with HTTP error
            }).done(function() {
            });
            <?php } ?>
        })(jQuery);

        function loadChat(groupid, topicid) {
            jQuery.post("<?php echo CRoute::_('index.php?option=com_community&view=groups&task=loadDiscussionDetail') ?>", {
                'groupid': groupid,
                'topicid': topicid
            }, function (data) {
                jQuery('.joms-list__item').removeClass('active');
                jQuery('.topicid_' + topicid).addClass('active');
                jQuery('.discussion_content .right-column').html(data);
                jQuery('.topicid_' + topicid + ' .topicName').removeClass('name_highlight');
                jQuery('.topicid_' + topicid + ' .mes-count').hide();
                <?php if ($isMobile) { ?>
                jQuery('.discussion_content .left-column').addClass('hide');
                jQuery('.discussion_content .right-column').addClass('show');
                jQuery('.joms-tab__bar').addClass('hide');
                jQuery('.jomsocial').addClass('showTopic');
                jQuery('.discussion_content').addClass('showTopic');
                <?php } ?>
                jQuery('.joms-js--comments').prepend(jQuery('.joms-js--more-comments').parent().html());
                var totalHeight = 0;
                var x = 0;
                jQuery(".topicDetail_scroll > .joms-js--comments").children().each(function(){
                    totalHeight = totalHeight + jQuery(this).outerHeight(true);
                    console.log("height " +x+ ": "+ jQuery(this).outerHeight(true));
                    x++;
                });
                console.log("total height sau load: " + totalHeight);
                jQuery(".topicDetail_scroll").animate({ scrollTop: totalHeight }, 100);
            }).fail(function (xhr, ajaxOptions, thrownError) { //any errors?
                consloe.log(thrownError); //alert with HTTP error
            });
        }
    </script>
<?php } ?>
