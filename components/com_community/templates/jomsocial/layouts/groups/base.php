<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();
$display = $_REQUEST['task'];
$session = JFactory::getSession();
$device = $session->get('device');
$id = JRequest::getVar('groupid');
$groupsModel = CFactory::getModel('groups');
$course_group = $groupsModel->getIsCourseGroup($id);
?>
<!--<script src="components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>-->
<script>

    function joms_change_filter(value, type) {
        var url;

        // Category selector.
        if (type === 'category') {
            if (value) {
                url = '<?php echo html_entity_decode(CRoute::_("index.php?option=com_community&view=groups&task=display&categoryid=__cat__")); ?>';
                url = url.replace('__cat__', value);
            } else {
                url = '<?php echo html_entity_decode(CRoute::_("index.php?option=com_community&view=groups&task=display")); ?>';
            }

            window.location = url;
            return;
        }

        // Filter selector.
        // @todo
    }
    jQuery(document).ready(function () {
        var check = '<?php echo $display?>';
        if (check == 'display') {
            var title_p = '<?php echo JText::_('COM_COMMUNITY_TITLE_ALLCIRCLES') ?>';
        }
        else {
            var title_p = '<?php echo JText::_('COM_COMMUNITY_TITLE_MYCIRCLES') ?>';
        }

        jQuery('.navbar-header .navbar-title span').html(title_p);

        jQuery("input.form-control").keyup(function () {
            var value = jQuery(this).val();
            if (value != '') {
                jQuery(this).addClass('onkeyup');
            } else {
                jQuery(this).removeClass('onkeyup');
            }
        })
            .keyup();
    });
</script>

<style type="text/css">
    .joms-tab__bar .active {background-color: #FFF !important; font-weight: bold;}
    body{
        background-color: #FFFFFF;
    }
    
    .jomsocial {
        background-color: #FFFFFF !important;
    }
    
/*    @media screen and (min-width: 600px){
        .home .swiper-slide {
        border-top-left-radius: 0px !important;
        border-top-right-radius: 0px !important;
    }
}*/
</style>

<?php
$task = JFactory::getApplication()->input->getCmd('task');
if ($task == 'mygroups') {
    $title = JText::_('COM_COMMUNITY_GROUPS_MY_GROUPS');
} else {
    $title = JText::_('COM_COMMUNITY_GROUPS');
}

    if($device == 'mobile'){
        $listcirclestyle = "background: none; padding: 12px 0 0 0";
    }else{
        $listcirclestyle = "background: none; padding: 0 0 0 0";
    }
?>


<div class="joms-page list_circle" style="<?php echo $listcirclestyle ?>;">
        <div class="joms-tab__bar">
            <a href="<?php echo CRoute::_("index.php?option=com_community&view=groups&task=viewchats") ?>" class="border-left <?php if ($task != 'mygroups' && $task != '0a-guest') echo ' active' ?>"><?php echo JText::_('COM_COMMUNITY_CHATS');?></a>
            <a href="<?php echo CRoute::_("index.php?option=com_community&view=groups&task=mygroups") ?>" <?php if ($task == 'mygroups' || $task == '0a-guest') echo 'class="active"' ?>><?php echo JText::_('COM_COMMUNITY_TITLE_MYCIRCLES'); ?></a>
            <!--<a href="<?php // echo JUri::base().'myevents/list.html'; ?>" class="border-left"> <?php // echo JText::_('COM_COMMUNITY_EVENTS');?></a>-->
        </div>

    <?php if ($my->authorise('community.create', 'groups')) { ?>
<!--    <div class="row custom-box-1 margin-0" style="background-color: #FFFFFF">
        <button class="joms-button--login custom-button-1 margin-style-1 margin-right-1 pull-right creategroup" onclick="window.location='<?php // echo CRoute::_('index.php?option=com_community&view=groups&task=create'); ?>';">
            </button>
        </div>-->
    <?php } ?>
    <?php echo $groupsHTML; ?>
</div>
