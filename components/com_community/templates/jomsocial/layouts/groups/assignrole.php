<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();

$device = JFactory::getSession()->get('device');
$isMobile = ($device == 'mobile') ? true : false;

$data = $listUsers;
$class = "pageAssign" . strtoupper($role);
$noFriendsText = JText::_('COM_COMMUNITY_FRIENDS_NOT_FRIENDS');
$buttonText = JText::_('COM_COMMUNITY_ASSIGN');

switch ($role) {
    case 'cc':
        $roleText = JText::_('COM_COMMUNITY_ASSIGN').' '.JText::_('COM_COMMUNITY_CONTENT_CREATOR');
        break;
    case 'mn':
        if ($isLPCircle) {
            $noFriendsText = JText::_('COM_COMMUNITY_LP_ASSIGN_MANAGER_NOT_FRIENDS');
        } else {
            $noFriendsText = JText::_('COM_COMMUNITY_GROUPS_MEMBER_EMPTY_WARNING');
        }
        $roleText = $isLearningCircle ? JText::_('COM_COMMUNITY_ASSIGN_ROLES') : JText::_('COM_COMMUNITY_ASSIGN').' '.JText::_('COM_COMMUNITY_MEMBERS_TITLE_MANAGERS');
        $buttonText = JText::_('COM_COMMUNITY_ASSIGN_ROLE');
        break;
    case 'fa':
        $roleText = JText::_('COM_COMMUNITY_ASSIGN_ROLES');
        $buttonText = JText::_('COM_COMMUNITY_ASSIGN_ROLE');
        break;
    case 'ln':
        $roleText = JText::_('COM_COMMUNITY_ASSIGN_LEARNERS');
        $buttonText = $roleText;
        break;
    default:
        break;
}
?>
<div class="joms-page pageAssign <?php echo $class; ?>">
    <h2 class="pageTitle"><?php echo $roleText; ?></h2>
    <?php if ($isLearningCircle && $role != 'ln') { ?>
        <?php 
        $display = 'display: none';
        if($isLearningCircle && $facilitatedcourse == 0){
            $show = 'display: none'; 
            $display = "display: block";
        }?>
    <div class="menuSelectRole" style="<?php echo $show; ?>">
        <div class="roleTab <?php echo ($role == 'mn') ? 'lgtActive':''?>"><a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=assignRole&r=mn&grid='. $groupid) ?>"><?php echo JText::_('COM_COMMUNITY_MANAGER'); ?></a></div>
        <div class="roleTab <?php echo ($role == 'fa') ? 'lgtActive':''?>"><a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=assignRole&r=fa&grid='. $groupid) ?>"><?php echo JText::_('COM_COMMUNITY_ASSIGN_TITLE_FACILITATOR'); ?></a></div>
    </div>
    <div class=" menuSelectmanage roleTab <?php echo ($role == 'mn') ? 'lgtActive':''?>"  style="<?php echo $display; ?>" ><a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=assignRole&r=mn&grid='. $groupid) ?>"><?php echo JText::_('COM_COMMUNITY_MANAGER'); ?></a></div>
    <?php } ?>
    <div class="pageContent">
        <form class="formAssignRole" action="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=handleAssignRole', false); ?>" method="post">
            <?php if ($courseid) :?>
            <input type="hidden" name="courseid" value="<?php echo $courseid; ?>">
            <?php endif; ?>
            <input type="hidden" name="groupid" value="<?php echo $groupid; ?>">
            <input type="hidden" name="role" value="<?php echo $role; ?>">
            <input type="hidden" name="mapage" value="<?php echo $_REQUEST['ma'];?>">

            <div id="friendsList">
                <div class="container">
                    <div class="row">
                        <div class="listFriends">
                            <?php if (!empty($data)) {
                                $i = 1;
                                ?>
                                <?php foreach ($data as $user) { ?>
                                    <div class="col-xs-12 col-sm-6">
                                        <div id="friend<?php echo $i; ?>" class="friend friend<?php echo $i; echo ($i > 6) ? ' lgt-invisible ' : '' ?>">
                                            <div class="inputCheckbox">
                                                <div class="status <?php echo (isset($arrAssignedUsersIds) && in_array($user->id, $arrAssignedUsersIds)) ? 'lgtSelected' : ''; ?> "></div>
                                                <input name="assignedUsers[]" type="checkbox" <?php echo (isset($arrAssignedUsersIds) && in_array($user->id, $arrAssignedUsersIds)) ? 'checked' : ''; ?> value="<?php echo $user->id ?>"/>
                                            </div>
                                            <div class="friendDetail">
                                                <div class="friendImage">
                                                    <img src="<?php echo $user->getThumbAvatar();?>" alt="Avatar" />
                                                </div>
                                                <div class="friendName">
                                                    <span><?php echo $user->getDisplayName();?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                    $i++;
                                } ?>
                            <?php } else {
                                echo '<p class="noApproveRequestsText">'.$noFriendsText.'</p>';
                            } ?>
                        </div>
                    </div>
                    <?php if (count($data) > 6) echo '<div class="friendsSeeMore btSeeMore">'.strtoupper(JText::_('COM_COMMUNITY_SEE_MORE')).'</div>';?>
                </div>
            </div>
            <?php if (!empty($data)) { ?>
            <div class="divButtons">
                <input class="btCancel" type="button" value="<?php echo JText::_('COM_COMMUNITY_CANCEL'); ?>" />
                <input class="btAssign" type="submit" name="formSubmit" value="<?php echo $buttonText; ?>" />
            </div>
            <?php } ?>
        </form>
    </div>
</div>
<script>
    jQuery(document).ready(function() {
        jQuery('.divButtons .btCancel').click(function() {
            window.location.href = '<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewabout&groupid='.$groupid); ?>';
        });
        
        jQuery('.pageAssign #friendsList .friend .status').click(function() {
            <?php if ($role == 'cc') :?>
            jQuery('.pageAssign #friendsList .friend input[type="checkbox"]').prop('checked', false);
            jQuery(this).next('input[type="checkbox"]').click();
            jQuery('.pageAssign #friendsList .friend .status').removeClass('lgtSelected');
            jQuery(this).addClass('lgtSelected');
            <?php else : ?>
            jQuery(this).next('input[type="checkbox"]').click();
            jQuery(this).toggleClass('lgtSelected');
            <?php endif; ?>
        });

        jQuery('.pageAssign #friendsList .friend .friendDetail').click(function() {
            jQuery(this).prev('.inputCheckbox').find('.status').click();
        });

        jQuery('.friendsSeeMore').on('click', function() {
            var list = jQuery(this).prev();
            var size = list.find('.friend').size();
            var shown = list.find('.friend:not(.lgt-invisible)').length;
            list.find('.friend:lt(' + (shown + 3) + ')').removeClass('lgt-invisible');
            if (size <= (shown + 3)) jQuery(this).fadeOut();
        });

        jQuery('.btAssign').click(function(e) {
            e.preventDefault();
            var valid;

            <?php if ($role == 'ln') { ?>

            valid = true;

            <?php } else { ?>

            valid = false;
            jQuery('.friend .status').each(function() {
                if (jQuery(this).hasClass('lgtSelected')) {
                    valid = true;
                    return false;
                }
            });

            <?php } ?>
            if (valid) jQuery('.formAssignRole').submit();
            else lgtCreatePopup('withCloseButton', {'content':'You have not selected anyone yet.'});
        });
    });
</script>