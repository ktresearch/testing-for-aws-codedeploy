<?php 
$document = JFactory::getDocument();
$session = JFactory::getSession();
$device = $session->get('device');
$config = CFactory::getConfig();
$my = CFactory::getUser();
$table =  JTable::getInstance( 'Discussion' , 'CTable' );
$discussionModel = CFactory::getModel('Discussion');
$group      = JTable::getInstance('Group', 'CTable');
$groupsModel = CFactory::getModel('groups');

$isMobile = false;
if($device == 'mobile') $isMobile = true; 

$string = 'COM_COMMUNITY_GROUPS_DISCUSSION_CREATOR_TIME_LINK';

if ($isTimeLapsed == 'lapse') {
    $string = 'COM_COMMUNITY_GROUPS_DISCUSSION_CREATOR_TIME_LINK_LAPSED';
}

$document->addStyleSheet('components/com_community/assets/release/css/discussions.css');

//$titleValue = CActivities::truncateComplex($discussion->title, 60, true);
$titleValue = $discussion->title;
//$descValue = CActivities::truncateComplex($discussion->message, 60, true);

$descValue = $discussion->message;

$mainframe = JFactory::getApplication(); 
$rooturl = $mainframe->getCfg('wwwrootfile');
$thumbnail = '/components/com_community/assets/chat-default.png';
if($discussion->avatar != '')  $thumbnail = $rooturl . '/' . $discussion->avatar;

$displaynone = '';
if (!$canedit){
    $displaynone = 'style="display:none"';
}
$creator = CFactory::getUser($discussion->creator);
$course_group = $groupsModel->getIsCourseGroup($discussion->groupid);

$isGroupAdmin = $groupsModel->isAdmin($my->id, $discussion->groupid);

$wallContent = CWallLibrary::getWallContents('discussions', $discussion->id, $isGroupAdmin, $config->get('stream_default_comments'), 0, 'wall/content2', 'groups,discussion');
$wallCount = CWallLibrary::getWallCount('discussions', $discussion->id);

$viewAllLink = CRoute::_('index.php?option=com_community&view=groups&task=discussapp&topicid=' . $discussion->id . '&app=walls');

$wallViewAll = '';
if ( $wallCount > $config->get('stream_default_comments') ) {
    $wallViewAll = CWallLibrary::getViewAllLinkHTML($viewAllLink, $wallCount);
}

$group->load($discussion->groupid);

$groupavatar = '/components/com_community/assets/group.png';
if($group->thumb != '')  $groupavatar = $rooturl . '/' . $group->thumb;

// Test if the current browser is a member of the group
$isMember = $group->isMember($my->id);
$waitingApproval = false;

// If I have tried to join this group, but not yet approved, display a notice
if ($groupsModel->isWaitingAuthorization($my->id, $group->id)) {
    $waitingApproval = true;
}
            
$wallForm = '';
// Only get the wall form if user is really allowed to see it.
if (!$config->get('lockgroupwalls') || ($config->get('lockgroupwalls') && ($isMember) && (!$isBanned) && !($waitingApproval) ) || COwnerHelper::isCommunityAdmin()) {
    $outputLock = '<div class="cAlert">' . JText::_('COM_COMMUNITY_DISCUSSION_LOCKED_NOTICE') . '</div>';
    $outputUnLock = CWallLibrary::getWallInputForm($discussion->id, 'groups,ajaxSaveDiscussionWall', 'groups,ajaxRemoveReply', '' , 1);
    $wallForm = $discussion->lock ? $outputLock : $outputUnLock;
}
$groupId = $group->id;
if (empty($wallForm)) {
    //user must join in order to see this page
    $tmpl = new CTemplate();
    $wallForm = $tmpl->set('groupid', $groupId)
            ->fetch('groups.joingroup');

    $outputLock   = '<div class="cAlert">' . JText::_('COM_COMMUNITY_DISCUSSION_LOCKED_NOTICE') . '</div>';
    $outputUnLock = CWallLibrary::getWallInputForm($discussion->id, 'groups,ajaxSaveDiscussionWall', 'groups,ajaxRemoveReply', '' , 1);
    $wallForm2    = '<div class="cWall-Header">' . JText::_('COM_COMMUNITY_REPLIES') . '</div>';
    $wallForm2    .= $discussion->lock ? $outputLock : $outputUnLock;
    $wallForm     = $wallForm . '<div style="display:none" class="reply-form">' . $wallForm2 . '</div>';
}
    
    $link = 'index.php?option=com_community&view=groups&task=viewabout&groupid=' . $groupId;
?>

<div class="topicDetail_scroll">
    <div class='discussionBanner'>
        <?php if ($isMobile) { ?>
        <span class="btn_backListChats"></span>
        <?php } ?>
        <div class="topic-image">
            <div class="discussionProfile">
                <img src="<?php echo $thumbnail; ?>" alt="<?php echo $discussion->title; ?>" class="img_chatavatar">
                <a href="<?php echo CRoute::_($link);?>">
                    <img src="<?php echo $groupavatar; ?>" alt="<?php echo $group->name; ?>" class="img_circleavatar <?php echo ($course_group > 0) ? 'img_learning' : ''; ?>">
                </a>
            </div>
        </div>
        <div class="discussionTextOuterWrapper">
            <div class="discussionTextTopWrapper">
                <div class="titleContent  limitDesc isLess" title="<?php echo $discussion->title; ?>"> <?php echo $isMobile ? CActivities::truncateComplex($discussion->title, 20, true) : CActivities::truncateComplex($discussion->title, 46, true)?>
                </div>
                <!-- <p class="show-title" data-more="Show more" data-less="Show less">Show more</p> -->
<!--
                <div class="expansionWrapper">
                    <span class='expandBtn'></span>
                </div>-->
            </div>
            <div class="discussionTextBottomWrapper">
                <div class="descContent limitDesc"> 
                    <?php
                        $des = JHtml::_('string.truncate', $descValue, 0, true, $allowHtml = true);
                        if($isMobile){
                            if (strlen($des) > 35) {
                                $shortdes = CActivities::truncateComplex($des,35,true);
                            echo '<span class="shortdes">'.$shortdes.'<span class="btshow"> '.JText::_('COM_COMMUNITY_SHOW_MORE').'</span></span>';
                            echo '<div class="des hidden"><div class="des-text">'.$des.'</div><span class="btshow"> '.JText::_('COM_COMMUNITY_SHOW_LESS').'</span></div>';
                            }
                            else echo $des;
                        }
                        else {
                            if (strlen($des) > 80) {
                                $shortdes = CActivities::truncateComplex($des,80,true);
                                echo '<span class="shortdes">'.$shortdes.'<span class="btshow"> '.JText::_('COM_COMMUNITY_SHOW_MORE').'</span></span>';
                                echo '<div class="des hidden"><div class="des-text">'.$des.'</div><span class="btshow"> '.JText::_('COM_COMMUNITY_SHOW_LESS').'</span></div>';
                            }
                            else echo $des;
                        }
                    ?>
                </div>
<!--                <div class="expansionWrapper">
                    <span class='expandBtn'></span>
                </div>-->
            </div>
            <div class="clearBoth"></div>
        </div>
    </div>
    <div class='joms-discussion--line  col-xs-12'></div>

    <div class="joms-gap"></div>
    <div class="joms-stream__status--mobile">
        <a href="javascript:" onclick="joms.api.streamShowComments('<?php echo $discussion->id ?>', 'discussions');">
            <span class="joms-comment__counter--<?php echo $discussion->id; ?>"><?php echo $wallCount; ?></span>
        </a>

        <?php echo $wallContent; ?>
    </div>

    <div style="display:none"><?php echo $wallViewAll; ?></div>
    <?php echo $wallContent; ?>
    <?php echo $wallForm; ?>
    <div>
        <a name="msgbottom"></a>
    </div>
     <div id="kv-popup-video" class="kv-popup-video">
        <span class="btClose">X</span>
        <iframe id="kv-youtube-video" width="420" height="315" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe>
    </div>
</div>
<script type="text/javascript">

    (function($) {
        var chatid = <?php echo $discussion->id;?>;
                   
        <?php if ($device == 'mobile') {?>
        jQuery(document).on('DOMNodeInserted', function(e) {   

            var banner = jQuery(".discussionBanner").outerHeight(true); 
            if (banner < 109) banner = 109;
            jQuery(".topicDetail_scroll > .joms-js--comments").css('padding-top', banner);
            
            var totalHeight = 0;

            jQuery(".topicDetail_scroll > .joms-js--comments").children().each(function(){
                totalHeight = totalHeight + jQuery(this).outerHeight(true);
            });
            jQuery(".topicDetail_scroll").animate({ scrollTop: totalHeight }, 0);   
        });
        <?php } else { ?>
            var banner = jQuery(".discussionBanner").outerHeight(true);
            jQuery(".topicDetail_scroll > .joms-js--comments").css('padding-top', banner);
        
            // var totalHeight = 0;
            //
            // jQuery(".topicDetail_scroll > .joms-js--comments").children().each(function(){
            //     totalHeight = totalHeight + jQuery(this).outerHeight(true);
            // });
            // jQuery(".topicDetail_scroll").animate({ scrollTop: totalHeight }, 200);
        <?php } ?>
        
        jQuery(document).on('click','.joms-button--comment', function(e) {    
            var totalHeight = 0;

            jQuery(".topicDetail_scroll > .joms-js--comments").children().each(function(){
                totalHeight = totalHeight + jQuery(this).outerHeight(true);
            });
            jQuery(".topicDetail_scroll").animate({ scrollTop: totalHeight }, 1000); 
            jQuery('.topic_scroll').find('.topicid_'+chatid).prependTo('.topic_scroll');
            jQuery(".topic_scroll").animate({ scrollTop: 0 }, 100);
        });


        jQuery(document).on('keyup','.joms-textarea', function(e) { 
            if (e.keyCode == 13) {
                var totalHeight = 0;

                jQuery(".topicDetail_scroll > .joms-js--comments").children().each(function(){
                    totalHeight = totalHeight + jQuery(this).outerHeight(true);
                });
                jQuery(".topicDetail_scroll").animate({ scrollTop: totalHeight }, 1000); 
                jQuery('.topic_scroll').find('.topicid_'+chatid).prependTo('.topic_scroll');
                jQuery(".topic_scroll").animate({ scrollTop: 0 }, 100);
            }
        });


        jQuery(document).on('click', '.joms-media--video[data-type="youtube"]', function(e) {
            if (!jQuery(this).hasClass('being-played')) {
                var code = jQuery(this).attr('data-path');

                if (code.indexOf('watch?v=') != -1) {
                    var vid = code.substr((code.indexOf('watch?v=') + 8), 11);
                } else if (code.indexOf('youtu.be/') != -1) {
                    var vid = code.substr((code.indexOf('youtu.be/') + 9), 11);
                }
                if ( typeof(vid) != 'undefined') {
                    jQuery('#kv-youtube-video').attr('src', 'https://www.youtube.com/embed/'+vid);
                    jQuery('#kv-popup-video').fadeIn();
                    jQuery('body').addClass('overlay');
                }
            }                    
        });
        // jQuery(document).not('.kv-popup-video, .kv-popup-video *, .joms-media--video, .joms-media--video * ').click(function(e) {
        jQuery('#kv-popup-video > .btClose').click(function(e) {
            var src = jQuery('#kv-youtube-video').attr('src');
            jQuery('#kv-youtube-video').attr('src',src);  
            jQuery('#kv-popup-video').fadeOut();
            jQuery('body').removeClass('overlay');
        });
        jQuery('.discussionBanner .arrowDown').click(function() {
            if (jQuery('html').hasClass('tab')) {
                jQuery('.discussionBanner .arrowDown .joms-subnav--desktop').fadeToggle();
            } else {
                jQuery('.discussionBanner .arrowDown .joms-subnav__menu').fadeToggle();
            }
        });
        if ($('.descContent')[0].scrollHeight < 50) {
            $('.discussionTextBottomWrapper .expansionWrapper').hide();
        } else {
            $('.discussionTextBottomWrapper .expansionWrapper').show();
        }
        if ($('.titleContent')[0].scrollHeight < 30) {
            $('.discussionTextTopWrapper .expansionWrapper').hide();
        } else {
            $('.discussionTextTopWrapper .expansionWrapper').show();
        }
    })(jQuery);

    (function( w ) {

           w.joms_queue || (w.joms_queue = []);
           w.joms_queue.push(function( $ ) {
           $('.joms-js--comments').prepend( $('.joms-js--more-comments').parent().html() );

            $('.joms-comment__item').each(function() {
                var fullURL = $(this).find( "a" ).attr('href');
                var start = fullURL.indexOf("userid=")+7;
                var end = fullURL.indexOf("&Item");
                var commentorID = fullURL.substring(start,end);
                var myID = <?php echo $my->id ?>;

                // console.log(commentorID + " "+ myID);
                if(commentorID==myID) 
                    $(this).addClass('blueBubble');
           });

            $('.discussionTextBottomWrapper .expansionWrapper .expandBtn').on('click',function(){
                $('.descContent').toggleClass('limitDesc');
            });

           $('.discussionTextTopWrapper .expansionWrapper .expandBtn').on('click',function(){
               $('.titleContent').toggleClass('limitDesc');
           });
        }); 

    })( window );

//    jQuery('.joms-comment').on('click', '.joms-comment__item', function(e){
//        var dataid = jQuery(this).data("id");
//        jQuery('.joms-comment #removebutton-'+dataid).toggle();
//    });

    jQuery('.joms-comment').on('click', '.removebutton', function(e){
        e.preventDefault();
        var dataid = jQuery(this).data("id");
        if (confirm('Are you sure you want to delete?')) {
            joms.api.commentRemove(dataid, 'wall');
        } else {
            // Do nothing! Just chill..
        }
    });    
    jQuery('.descContent .shortdes .btshow, .descContent .des .btshow').click(function() {
        jQuery('.descContent .shortdes, .descContent .des').toggleClass('hidden');
    });

    jQuery('body').on('click', '.joms-js--comment-content .shortdes .btshow, .joms-js--comment-content .des .btshow', function() {
        jQuery(this).closest('.joms-js--comment-content').find('.shortdes').toggleClass('hidden');
        jQuery(this).closest('.joms-js--comment-content').find('.des').toggleClass('hidden');
     });
    
    jQuery('.btn_backListChats').click(function() {
        jQuery('.discussion_content .left-column').removeClass('hide');
        jQuery('.discussion_content .right-column').removeClass('show');
        jQuery('.joms-tab__bar').removeClass('hide');
        jQuery('.jomsocial').removeClass('showTopic');
        jQuery('.discussion_content').removeClass('showTopic');
    });
</script>
<style type="text/css">
    .des-text { overflow-y: auto; max-height: 150px; }
</style>
