<?php 
$document = JFactory::getDocument();
$session = JFactory::getSession();
$device = $session->get('device');
$config = CFactory::getConfig();
$my = CFactory::getUser();
$table =  JTable::getInstance( 'Group' , 'CTable' );
$eventsModel = CFactory::getModel('Events');
$groupsModel = CFactory::getModel('groups');

require_once(JPATH_SITE.'/modules/mod_mygroups/helper.php');

foreach($circles as $circle) {

$table->load($circle->id);
$link = 'index.php?option=com_community&view=groups&task=viewdiscussions&groupid=' . $circle->id;
if($circle->categoryid == 7){
    $link = 'index.php?option=com_community&view=groups&task=viewcourses&groupid='. $circle->id;
}
$course_group = $groupsModel->getIsCourseGroup($circle->id);
/*
$course_group = $groupsModel->getIsCourseGroup($circle->id);
 
if($course_group){
    $course_info = JoomdleHelperContent::call_method('get_course_info', (int)$course_group, '');
    $course_image_avatar = $course_info['filepath'] . $course_info['filename'];
    $course_description = mb_strimwidth(strip_tags($course_info['summary']), 0, 70, "..");
    $group_avatar = $course_image_avatar;
}else{
    $course_description = $circle->description;
    $group_avatar = $table->getAvatar();
}
*/

$course_description = $circle->description;

//$params = $circle->getParams();
//$tmpevents = $eventsModel->getGroupEvents($circle->id, $params->get('grouprecentevents', GROUP_EVENT_RECENT_LIMIT));
$tmpevents = array();
if ($circle->notification != 0) {
    $class_notif = "notification";
} else { $class_notif = "notiflearn";}
$notification = My_Groups::groupActivities($circle->id);
?>

<div class="swiper-slide swiper-slide-active" style="width: 263.333px; margin-right: 12px">
    <div class="group-title" style="">
        <div class="circle_title_img">
            <?php
                   $sub = $groupsModel->checkSubgroup($circle->id);
                   $image = $table->getOriginalAvatar();
                    if (strstr ( $image, 'group.png' ))
                        $style = "background-size: contain;background-color: #f2f7fb;";
                    else $style = "";
                    if($course_group){
                        if (strstr ( $image, 'group.png' )){
                            $style = "background-size: contain;background-color: #f2f7fb;";
                            $datacourse = JoomdleHelperContent::call_method('get_course_info', (int)$course_group, $username);
                            $groupAvatar = $datacourse['filepath']. $datacourse['filename'];
                        }
                        else {
                            $style = "";
                            $groupAvatar = $image . '?_=' . time();
                        }
                    }
                    else 
                        $groupAvatar = $image . '?_=' . time();
            ?>
            <a href="<?php echo ($circle->published) ? CRoute::_($link) : '#';?>">
                <div class="left" id="images"
                     style="background-image: url('<?php echo $groupAvatar; ?>'); <?php echo $style ?>"></div>
            </a>
            <?php 
            $blnCircle = $groupsModel->getBLNCircle();
            if($circle->ownerid == $my->id && $circle->categoryid == 0 && $circle->id != $blnCircle->id){?>
             <!-- coursesButtons-->
            <div class="coursesButtons">
                <a href="javascript:" class="joms-dropdown-button">
                    <img class="dropdown-button" onclick="" src="<?php echo JURI::root(); ?>media/joomdle/images/icon/ManageCourses/dropdown.png"/>
                </a>

                <ul class="joms-dropdown">
                    <?php if($circle->published) {?>
                    <li class=" action ">
                        <a href="javascript:" class="btArchare" onclick="myArchive(<?php echo $sub ?>,<?php echo $circle->id; ?>,1)" data-bt ="1" data-sub ="<?php echo $sub ?>" data-id="<?php echo $circle->id; ?>"><?php echo JText::_('COM_COMMUNITY_BUTTON_ARCHIVE'); ?> </a>
                    </li>
                    <?php }
                        else {
                    ?>
                     <li class=" action ">
                        <a class="btArchare" onclick="myArchive(<?php echo $sub ?>,<?php echo $circle->id; ?>,0)" data-bt ="0" data-sub ="<?php echo $sub ?>" data-id="<?php echo $circle->id; ?>"><?php echo JText::_('COM_COMMUNITY_BUTTON_UNARCHIVE'); ?> </a>                    </li>
                    <?php }?>
                    <li class=" action ">
                        <a href="javascript:" class="btRemove" onclick="myRemove(<?php echo $sub ?>,<?php echo $circle->id; ?>)" data-sub ="<?php echo $sub ?>" data-id="<?php echo $circle->id; ?>"  ><?php echo JText::_('COM_COMMUNITY_REMOVE_BUTTON'); ?></a>
                    </li>
                </ul>
            </div>
            <?php }?>
            <div class="<?php echo $class_notif?>">
              <span >
                    <?php 
                    echo $notification;
                    ?>
                </span>
            </div>
        </div>
        <div class="circle_title">
        <div class="gr-title-box" style="height: 24px; overflow: hidden;">
                <a class="gr-title gr_<?php echo $circle->id;?>" href="<?php echo ($circle->published) ? CRoute::_($link) : '#';?>" ><?php echo $circle->name?></a>
        </div>
        <div class="gr-description-box">
                <p class="group-description des_<?php echo $circle->id;?>"><?php echo $course_description; ?></p>
        </div>
        </div>
        </div>
    </div>
<?php } ?>
<script type="text/javascript">
(function ($) {
    jQuery('.gr-title-box').each(function () { 
        if (jQuery(this).find('a').height() > 48) {
            jQuery(this).find('a').addClass('limited');
        }
    });
    jQuery('.gr-description-box').each(function () { 
        if (jQuery(this).find('p').height() > 48) { 
            jQuery(this).find('p').addClass('limited');
        }
    });
})(jQuery);
</script>