<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();

CWindow::load();
$mainframe = JFactory::getApplication();
$document = JFactory::getDocument();
$session =  JFactory::getSession();

$user = JFactory::getUser();
$username = $user->username;
$idgroup = JRequest::getVar('grid', '');
$coursegroup = JRequest::getVar('groupid', $idgroup);

$jinput = $mainframe->input;

$featured = new CFeatured(FEATURED_GROUPS);
$featuredList = $featured->getItemIds();

$groupsModel = CFactory::getModel('groups');
$course_group = $groupsModel->getIsCourseGroup($group->id);
$publishedCourses = $groupsModel->getPublishCoursesToSocial($group->id);

$name = isset($_REQUEST['task']) ? $_REQUEST['task'] : '';
$editing = ($_REQUEST['task'] == 'edit') ? true : false;
$assigningRole = ($_REQUEST['task'] == 'assignRole') ? true : false;

$isOpenCircle = ($group->approvals) == 0 ? true : false;
$isPrivateCircle = ($group->approvals == 1 && $group->unlisted == 0) ? true : false;
$isClosedCircle = ($group->approvals == 1 && $group->unlisted == 1) ? true : false;

$uri = explode('?', $_SERVER['REQUEST_URI']);
$ex_uri = explode('/', $uri[0]); 
$isLearningCircle = $course_group && !in_array($ex_uri[2],['addnews', 'viewbulletin']) ? true : false;

if ($isLearningCircle) {
    $jparams = JComponentHelper::getParams('com_joomdle');
    if ($jparams->get('use_new_performance_method')) {
        $mods = JHelperLGT::getCourseMods(['id' => $course_group, 'username' => $username]);
        $userRoles = JHelperLGT::getUserRole(['id' => $course_group, 'username' => $username]);
    } else {
        $course_data = $session->get('currentCourseData');
        if (empty($course_data)) {
            require_once(JPATH_ROOT.'/administrator/components/com_joomdle/helpers/content.php');
            $course_data = JoomdleHelperContent::call_method('get_course_page_data', (int)$course_group, $username);
        }

        $mods = json_decode($course_data['mods'], true);
        $userRoles = json_decode($course_data['userRoles'], true);

    }
    $myRoles = array();
    if (!empty($userRoles['roles'])) {
        foreach (json_decode($userRoles['roles'][0]['role']) as $r) {
            if (!in_array($r->sortname, $myRoles)) $myRoles[] = $r->sortname;
        }
    }
    if (in_array('teacher', $myRoles)) $facilitator = true;
}
$isLPCircle = ($group->moodlecategoryid > 0) ? true : false;
$isCommunityCircleWithCourse = (count($publishedCourses) > 0) ? true : false;
$isCommunityCircle = (!$isLearningCircle && !$isLPCircle && !$isCommunityCircleWithCourse) ? true : false;

$blnCircle = $groupsModel->getBLNCircle();
$isBLNCircle = $blnCircle->id == $group->id ? true : false;

$is_mobile = $session->get('device') == 'mobile' ? true : false;
$titleLength = $config->get('header_title_length', 30);
$summaryLength = $config->get('header_summary_length', 80);

if ( $config->get('enablereporting') == 1 && ( $my->id > 0 || $config->get('enableguestreporting') == 1 ) ) {
    $enableReporting = true;
} else $enableReporting = false;

$enableChangingAvatar = false; //Disable this feature
// update image for course circle
if($isLearningCircle){
    
    if(strpos($group->getOriginalAvatar(), 'group.png') == true)
    {
        $datacourse = JoomdleHelperContent::call_method('get_course_info', (int)$course_group, $username);
        $groupAvatar = $datacourse['filepath']. $datacourse['filename'];
    }
    else  $groupAvatar = $group->getOriginalAvatar() . '?_=' . time();
}
else 
$groupAvatar = $group->getOriginalAvatar() . '?_=' . time();

if (!$editing) {
    if ($isLearningCircle) $circleType = 'lgtLearningCircle';
    else if ($isLPCircle) $circleType = 'lgtLPCircle';
    else $circleType = 'lgtCommunityCircle';

    if ($assigningRole) {
        if ($isLPCircle) {
            $role = $jinput->getString('r');
            $courseid = $jinput->get('coid', '', 'INT');
            if ($courseid && ($role == 'cc')) {
                $learningCircleId = $groupsModel->getLearningCircleId($courseid);
                $learningCircle = JTable::getInstance('Group', 'CTable');
                $learningCircle->load($learningCircleId);
                // $groupAvatar = $learningCircle->getOriginalAvatar() . '?_=' . time();
                $circleType .= ' assigningCC ';
                $enableChangingAvatar = false;
            }
        }
    }
    ?>
    <div class="joms-body <?php echo $circleType;?>">
        <div class="joms-focus">
            <div class="joms-focus__cover joms-focus--mini <?php echo $is_mobile ? 'mobile' : 'tablet'; ?>">
                <?php  if (in_array($group->id, $featuredList)) { ?>
                    <div class="joms-ribbon__wrapper">
                        <span class="joms-ribbon"><?php echo JText::_('COM_COMMUNITY_FEATURED'); ?></span>
                    </div>
                <?php } ?>
                <div <?php
                    if (!empty($group->getCover())) {
                        if (isset($group->defaultCover) && !$group->defaultCover && $group->coverAlbum) { ?>
                        style="background-position: 0 0;top:<?php echo $group->coverPosition; ?>" data-src="<?php echo $group->getCover(); ?>"
                        onclick="joms.api.coverClick(<?php echo $group->coverAlbum ?>, <?php echo $group->coverPhoto ?>);"
                        <?php } else { ?>
                            style="background-position: 0 0;top:<?php echo $group->coverPosition; ?>" data-src="<?php echo $group->getCover(); ?>"
                        <?php } 
                    } ?>
                    class="joms-focus__cover-image--mobile joms-js--cover-image-mobile <?php echo !empty($group->getCover()) ? 'lazyload' : '' ?>">
                </div>
                <?php if (($isAdmin || $isSuperAdmin || $isMine) && (!$editing) && (!$isLearningCircle) && (1 == 0)) { //Disable this feature ?>
                    <?php if ($name != 'addnews' && $name != 'viewbulletin') { ?>
                        <i class="bg-second-img icon-photo photo-cover-icon" onclick="joms.api.coverChange('group', <?php echo $group->id; ?> ); return false; "></i>
                    <?php } ?>
                <?php } ?>
                <div class="joms-focus__header">
                    <div class="joms-avatar--focus" style="z-index: 1">
                        <div class="circle-image lazyload" data-src="<?php echo $groupAvatar; ?>" style="background-color: #fff !important; border: 1px solid rgba(130, 130, 130, 0.5);"></div>
                        <?php if ($enableChangingAvatar) { ?>
                            <i class="bg-second-img icon-photo photo-icon" onclick="joms.api.avatarChange('group', '<?php echo $group->id ?>', arguments && arguments[0]); return false; "></i>
                        <?php } ?>
                    </div>

                    <div class="joms-focus__title">
                        <a class="back" href="javascript:void(0);" onclick="goBackCircle();">
                            <i class="bg-second-img icon-left-arrow"></i>
                        </a>
                        <?php if (!$editing) { ?>
                            <div class="title_circle <?php echo (($isAdmin || $isSuperAdmin) && !$assigningRole && !$isLearningCircle) ? 'hasiconedit' : '';?>">
                                <span>
                                <?php
                                echo $is_mobile ? CActivities::truncateComplex($this->escape($group->name), $titleLength, true) : $this->escape($group->name);
                                ?>
                                </span>
                            </div>
                        
                            <div class="number_member">
                                <?php if ($isLPCircle) { ?>
                                    <img style="width: 15px !important;" class="lgtCircleIcon" src="/media/joomdle/images/icon/LearningCircle/lp_circle.png">
                                    <span><?php echo JText::_('COM_COMMUNITY_TITLE_LEARNING_PROVIDER');?></span>
                                <?php } else if ($isPrivateCircle || $isClosedCircle) { ?>
                                    <?php if ($isPrivateCircle) { ?>
                                        <i class="bg-second-img icon-private-circle lgtCircleIcon"></i>
                                        <span><?php echo JText::_('COM_COMMUNITY_GROUPS_TEXT_PRIVATE');?></span>
                                    <?php } else { ?>
                                        <i class="bg-second-img icon-closed-circle lgtCircleIcon"></i>
                                        <span><?php echo JText::_('COM_COMMUNITY_GROUPS_TEXT_CLOSED');?></span>
                                    <?php } ?>
                                <?php } else { ?>
                                    <i class="bg-second-img icon-open-circle lgtCircleIcon"></i>
                                    <span><?php echo JText::_('COM_COMMUNITY_GROUPS_TEXT_OPEN');?></span>
                                <?php } ?>
                            </div>
                            <script >
                                function goBackCircle() {                                
                                   console.log(document.referrer);
                                    var iscourse = '<?php echo $isLearningCircle ;?>';
                                    strs = window.location.href;
                                    var links = strs.includes("viewabout");
                                    if(!iscourse && links)
                                          window.location.href= '<?php echo JUri::base().'index.php?option=com_community&view=groups&task=mygroups'; ?>';
                                    else if (document.referrer == <?php echo '\''.JUri::base().'\''?>+'component/community/' ||
                                       document.referrer == <?php echo '\''.JUri::base().'\''?>+'component/community' ) {
                                       console.log(1);
                                       window.location.href=<?php echo '\''.JUri::base().'\''?>;
                                   }
                                   else {
                                       if( this.phonegapNavigationEnabled &&
                                           nav &&
                                           nav.app &&
                                           nav.app.backHistory ){
                                           nav.app.backHistory();
                                       } else {
                                           var str = document.referrer;
                                           var chat = document.URL;
                                           var link = str.includes("viewabout");
                                           if(link)
                                               window.location.href = document.referrer;
                                           else if(chat.includes("viewdiscussion"))
                                           window.location.href = '<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid='. $group->id) ?>';


                                           else window.history.back();
                                        }
                                    }
                                }  
                            </script>
                            <?php if (($isAdmin || $isSuperAdmin) && !$assigningRole && !$isLearningCircle && $group->published) { ?>
                                <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=edit&groupid=' . $group->id); ?>" class="lgtBtEditCircle">
                                    <i class="bg-second-img icon-pencil"></i>
                                </a>
                            <?php } ?>
                        <?php } ?>
                        
                        <div class="joms-focus__header__actions">
                            <a class="joms-button--viewed nolink" title="<?php echo JText::sprintf($group->hits > 0 ? 'COM_COMMUNITY_VIDEOS_HITS_COUNT_MANY' : 'COM_COMMUNITY_VIDEOS_HITS_COUNT', $group->hits); ?>">
                                <svg viewBox="0 0 16 16" class="joms-icon">
                                    <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-eye"></use>
                                </svg>
                                <span><?php echo $group->hits; ?></span>
                            </a>

                            <?php if ($config->get('enablesharethis') == 1) { ?>
                                <a class="joms-button--shared" title="<?php echo JText::_('COM_COMMUNITY_SHARE_THIS'); ?>" href="javascript:" onclick="joms.api.pageShare('<?php echo CRoute::getExternalURL('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id); ?>')">
                                    <svg viewBox="0 0 16 16" class="joms-icon">
                                        <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-redo"></use>
                                    </svg>
                                </a>
                            <?php } ?>

                            <?php if ($enableReporting) { ?>
                                <a class="joms-button--viewed" title="<?php echo JText::_('COM_COMMUNITY_REPORT_GROUP'); ?>" href="javascript:" onclick="joms.api.groupReport('<?php echo $group->id; ?>');">
                                    <svg viewBox="0 0 16 16" class="joms-icon">
                                        <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-warning"></use>
                                    </svg>
                                </a>
                            <?php } ?>
                        </div>

                        <?php if ($isLearningCircle && !$isAdmin && !$isSuperAmin && !$facilitator && $isMember) :
                            $userModel = CFactory::getModel('user');
                            $groupOwnerUsername = $userModel->getUsername($group->ownerid);

                            $count_compled = 0;
                            $count_activity = 0;
                            if (is_array($mods)) {
                                foreach ($mods as $tema) {
                                    $resources = $tema['mods'];
                                    foreach ($resources as $id => $res) {
                                        if (($res['mod'] != 'forum') && ($res['mod'] != 'certificate') && ($res['mod'] != 'feedback') && ($res['mod'] != 'questionnaire')) {
                                            if (!$res['mod']) continue;
                                            if ($res['mod_completion']) $count_compled++;

                                            $count_activity++;
                                        }
                                    }
                                }
                            }
                            $percent = intval(($count_compled / $count_activity) * 100);
                            ?>
                            <div class="progressPercent">
                                <div class="percent" style="width: <?php echo $percent.'%';?>"></div>
                                <div class="percent-text"><?php echo $percent.'%';?></div>
                            </div>
                        <?php endif; ?>
                    </div>

                    <div class="joms--topright joms-group--button">
                        <div class="joms-focus__actions--desktop">
                            <?php if (!$isAdmin && !$isSuperAdmin && !$isBLNCircle) : 
                                if ($isMember && !$editing) { ?>
                                    <a href="javascript:" class="joms-focus__button--add lgtBtCircleLeave" onclick="joms.api.groupLeave('<?php echo $group->id; ?>')">
                                    </a>
                                <?php } else if (!$isBanned && !$editing) { ?>
                                    <?php if (!$isClosedCircle && !$isLPCircle) { ?>
                                        <?php if ($waitingApproval) { ?>
                                            <a href="javascript:void(0);" class="joms-focus__button--message lgtBtAwaitingApproval">
                                            </a>
                                        <?php } else if ($canJoin) { ?>
                                            <a href="javascript:" class="joms-focus__button--message lgtBtJoinCircle" onclick="joms.api.groupJoin('<?php echo $group->id; ?>')">
                                                <?php echo JText::_('COM_COMMUNITY_GROUPS_TEXT_JOIN'); ?>
                                            </a>
                                        <?php } ?>
                                    <?php } ?>
                                <?php }
                            endif;?>
                        </div>

                        <div class="joms-focus__header__actions--desktop">
                            <a class="joms-button--viewed nolink" title="<?php echo JText::sprintf($group->hits > 0 ? 'COM_COMMUNITY_VIDEOS_HITS_COUNT_MANY' : 'COM_COMMUNITY_VIDEOS_HITS_COUNT',
                                   $group->hits); ?>">
                                <svg viewBox="0 0 16 16" class="joms-icon">
                                    <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-eye"></use>
                                </svg>
                                <span><?php echo $group->hits; ?></span>
                            </a>

                            <?php if ($config->get('enablesharethis') == 1) { ?>
                                <a class="joms-button--shared" title="<?php echo JText::_('COM_COMMUNITY_SHARE_THIS'); ?>" href="javascript:" onclick="joms.api.pageShare('<?php echo CRoute::getExternalURL('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id); ?>')">
                                    <svg viewBox="0 0 16 16" class="joms-icon">
                                        <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-redo"></use>
                                    </svg>
                                </a>
                            <?php } ?>

                            <?php if ($enableReporting) { ?>
                                <a class="joms-button--viewed" title="<?php echo JText::_('COM_COMMUNITY_REPORT_GROUP'); ?>" href="javascript:" onclick="joms.api.groupReport('<?php echo $group->id; ?>');">
                                    <svg viewBox="0 0 16 16" class="joms-icon">
                                        <use xlink:href="<?php echo CRoute::getURI(); ?>#joms-icon-warning"></use>
                                    </svg>
                                </a>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    if ($isLearningCircle) {
        $myRoles = array();
        if (!empty($userRoles['roles'])) {
            foreach (json_decode($userRoles['roles'][0]['role']) as $r) {
                if (!in_array($r->sortname, $myRoles)) $myRoles[] = $r->sortname;
            }
        }
        ?>
        <style type="text/css">
            .tab #community-wrap.jomsocial-wrapper .jomsocial {
                padding: 121px 0 0 0;
            }

            .tab.com_community.view-groups.task-viewdiscussions #community-wrap.jomsocial-wrapper .jomsocial.course-menu,
            .tab.com_community.view-groups.task-viewdiscussions #community-wrap.jomsocial-wrapper .jomsocial,
            .tab.com_community.view-groups.task-viewmembers #community-wrap.jomsocial-wrapper .jomsocial,
            .tab.com_community.view-groups.task-viewabout #community-wrap.jomsocial-wrapper .jomsocial,
            .tab.com_community.view-events.task-edit #community-wrap.jomsocial-wrapper .jomsocial,
            .tab.com_community.view-groups.task-addnews #community-wrap.jomsocial-wrapper .jomsocial {
                padding: 65px 0 0 0;
            }

            .tab.com_community.view-groups.task-viewdiscussion .course-menu,
            .tab.com_community.view-groups.task-viewdiscussions .course-menu,
            .tab.com_community.view-groups.task-viewmembers .course-menu,
            .tab.com_community.view-groups.task-viewabout .course-menu,
            .tab.com_community.view-groups.task-adddiscussion .course-menu,
            .tab.com_community.view-groups.task-addnews .course-menu,
            .tab.com_community.view-events.task-viewevent .course-menu,
            .tab.com_community.view-events.task-viewguest .course-menu,
            .tab.com_community.view-events.task-edit .course-menu {
                /*top: 80px;*/
            }

            .tab.com_community.view-events.task-create .course-menu{
                top: 180px !important;
                position: absolute;
                left: 0;
            }

            .tab.com_community.view-events.task-create .jomsocial .joms-page{
                padding-top: 0 !important;
            }

            .tab.com_community.view-events.task-create .jomsocial .com-event-header{
                margin-top: 0 !important;
            }

            .com_community.view-events.task-create #community-wrap.jomsocial-wrapper .jomsocial .joms-form__group{
                margin-top: 60px !important;
            }


            .mob.com_community.view-groups.task-viewdiscussions #community-wrap.jomsocial-wrapper .jomsocial.course-menu,
            .mob.com_community.view-groups.task-viewdiscussions #community-wrap.jomsocial-wrapper .jomsocial,
            .mob.com_community.view-groups.task-viewmembers #community-wrap.jomsocial-wrapper .jomsocial,
            .mob.com_community.view-groups.task-viewabout #community-wrap.jomsocial-wrapper .jomsocial,
            .mob.com_community.view-events.task-edit #community-wrap.jomsocial-wrapper .jomsocial {
                padding: 101px 0 0 0;
            }

            .mob.com_community.view-groups.task-viewdiscussion .course-menu,
            .mob.com_community.view-groups.task-viewdiscussions .course-menu,
            .mob.com_community.view-groups.task-viewmembers .course-menu,
            .mob.com_community.view-groups.task-viewabout .course-menu,
            .mob.com_community.view-groups.task-adddiscussion .course-menu,
            .mob.com_community.view-groups.task-addnews .course-menu,
            .mob.com_community.view-events.task-viewevent .course-menu,
            .mob.com_community.view-events.task-viewguest .course-menu,
            .mob.com_community.view-events.task-edit .course-menu {
                top: 86px;
            }

            .mob.com_community.view-events.task-create .course-menu{
                top: 180px !important;
                position: absolute;
                left: 0;
            }

            .com_community.view-events.task-viewevent .jomsocial .joms-focus,
            .com_community.view-events.task-viewguest .jomsocial .joms-body {
                padding-top: 65px !important;
            }
        </style>

        <?php if (!in_array('editingteacher', $myRoles)) { ?>
            <div class="course-menu <?php echo $myRoles[0]; ?>">
                <div class="course-nav">
                    <nav>
                        <ul>
                            <li class="course-outline">
                                <a href="<?php echo JUri::base() . 'course/' . $course_group . '.html'; ?>">
                                    <span><?php echo JText::_('COM_COMMUNITY_OUTLINE'); ?></span>
                                </a>
                            </li>

                            <li class="course-circle active">
                                <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&task=viewabout&groupid='.$coursegroup);?>">
                                    <span><?php echo JText::_("COM_COMMUNITY_CIRCLE"); ?></span>
                                </a>
                            </li>

                            <li class="course-gradebook">
                                <?php if ((in_array('teacher', $myRoles)) || (in_array('coursecreator', $myRoles)) || (in_array('manager', $myRoles))) { ?>
                                <a href="<?php echo JURI::base() . $langcurrent . '/viewstudents/' . $course_group . '.html'; ?>">
                                    <?php } else { ?>
                                <a href="<?php echo JUri::base() . $langcurrent . '/progress/' . $course_group . '.html'; ?>">
                                    <?php } ?>
                                    <span><?php echo JText::_("COM_COMMUNITY_PROGRESS"); ?></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        <?php } ?>

        <script type="text/javascript">
            (function ($) {
                $('ul li a').on('click', function () {
                    $(this).parent().addClass('active').siblings().removeClass('active');
                });
            })(jQuery);
        </script>
    <?php } ?>

<?php } ?>
<script>
    jQuery(document).ready(function() {
        if (jQuery('.title_circle').find('span').height() > 35) { 
            jQuery('.title_circle').addClass('limited');
        }
    });
</script>