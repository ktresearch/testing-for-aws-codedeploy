

<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/

/*
Added by Chris for Cicle Listing
*/

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root().'/modules/mod_joomdle_my_courses/css/swiper.min.css');
$document->addScript(JUri::root().'/modules/mod_joomdle_my_courses/swiper.min.js');

$session = JFactory::getSession();
$device = $session->get('device');

$config = CFactory::getConfig();
$my = CFactory::getUser();
$username = $my->username;
$categoryid = $group->moodlecategoryid; 

$class = '';
$idstr = 'id="mobile-show"';
if($device != 'mobile') {
    $class = 'desktop-tablet';
    $idstr = '';
}

$groupsModel = CFactory::getModel('groups');
$limit = $groupsModel->paginationlimit;

$socialcircle = 0;
$coursecircle = 6;
$lpcircle = 7;

$totalsocialgroups = $groupsModel->getAllCount($socialcircle);
$totalcoursegroups = $groupsModel->getAllCount($coursecircle);
$totallpgroups = $groupsModel->getAllCount($lpcircle);

?>

<div class="home">
    <div class="container">

        <div class="mygroups <?php echo $class; ?> have-groups">
            <div class="mygroup-title-block">
                <span class="icon-circles"><img src="<?php echo JURI::root();?>images/socialcircles.png"></span> <span class="title-text"><?php echo JText::_('COM_COMMUNITY_GROUPS');?> (<?php echo $totalsocialgroups ?>)</span>
            </div>
            <div class="mygroup-content-block">
                <div class="swiper-container-social swiper-container-horizontal swiper-container-free-mode" style="overflow: hidden;">
                    <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);" id="socialcircles">
                        <?php 
                            $eventsModel = CFactory::getModel('Events');
                            foreach ($socialcircles as $socialcircle){ 
                                $params = $socialcircle->getParams();
                                $tmpevents = $eventsModel->getGroupEvents($socialcircle->id, $params->get('grouprecentevents', GROUP_EVENT_RECENT_LIMIT));
                                $table =  JTable::getInstance( 'Group' , 'CTable' );
                                $table->load($socialcircle->id);
                                $link = 'index.php?option=com_community&view=groups&task=viewdiscussions&groupid=' . $socialcircle->id;
                                if($socialcircle->categoryid == 7){
                                    $link = 'index.php?option=com_community&view=groups&task=viewcourses&groupid='. $socialcircle->id;
                                }
                        ?>
                        <!-- <ul> -->
                        <div class="swiper-slide swiper-slide-active" style="width: 263.333px; margin-right: 12px;">
                            <div class="group-title" style="width: 1870px;">
                                <a href="<?php echo $socialcircle->getLink(); ?>">
                                <img alt="<?php echo $socialcircle->name?>" class="cAvatar jomNameTips group-image" src="<?php echo $table->getAvatar(); ?>" title="<?php echo $socialcircle->name?>"></a>
                                <div class="gr-title-box" style="height: 24px; overflow: hidden;">
                                    <a class="gr-title" href="<?php echo CRoute::_($link);?>"><?php echo $socialcircle->name?></a>
                                </div>
                                <div class="gr-owner-box">
                                    <p class="gr-owner"><?php echo JText::_('OWNER')?>: <?php echo ucfirst(JFactory::getUser($socialcircle->ownerid)->name) ?></p>
                                </div>
                                <div class="gr-description-box">
                                    <p class="group-description"><?php echo $socialcircle->description ?></p>
                                </div>
                                <?php if($device == 'mobile'){ ?>
                                <div class="mobile-group-statistic">
                                    <span class="group-statistic">
                                        <span title="Member count"><?php echo $socialcircle->membercount; ?>
                                            <img class="icon_member" src="<?php echo JURI::root();?>templates/t3_bs3_blank/images/Member30x30.png" style="width: 13px; height: 13px">
                                        </span> 
                                        <span title="Discuss count"><?php echo $socialcircle->discusscount; ?>
                                            <img class="icon_member" src="<?php echo JURI::root();?>templates/t3_bs3_blank/images/Topics30x30.png" style="width: 13px; height: 13px; margin-bottom:2px;"></span> 
                                        <span title="Event count"><?php echo count($tmpevents) ?>
                                            <img class="icon_member" src="<?php echo JURI::root();?>templates/t3_bs3_blank/images/Events30x30.png" style="width: 13px; height: 13px">
                                        </span>
                                    </span>
                                </div>
                                <?php } else {?>
                                <div class="desktop-group-statistic">
                                     <span class="group-statistic">
                                        <span title="Member count"><?php echo $socialcircle->membercount; ?>
                                            <img class="icon_member" src="<?php echo JURI::root();?>templates/t3_bs3_blank/images/Member30x30.png" style="width: 15px; height: 15px">
                                        </span> 
                                        <span title="Discuss count"><?php echo $socialcircle->discusscount; ?>
                                            <img class="icon_member" src="<?php echo JURI::root();?>templates/t3_bs3_blank/images/Topics30x30.png" style="width: 15px; height: 15px; margin-bottom:2px;"></span> 
                                        <span title="Event count"><?php echo count($tmpevents) ?>
                                            <img class="icon_member" src="<?php echo JURI::root();?>templates/t3_bs3_blank/images/Events30x30.png" style="width: 15px; height: 15px">
                                        </span>
                                    </span>
                                </div>
                                <?php }?>
                            </div>
                        </div><!-- </li> -->
                        <?php }?>
                        
                    </div>
                </div>
                <div style="clear:both"></div>
            </div>
        </div>

        <div class="mygroups <?php echo $class; ?> have-groups">
            <div class="mygroup-title-block">
                 <span class="icon-circles"><img src="<?php echo JURI::root();?>media/joomdle/images/icon/ManageCourses/CS_30x30.png"></span> <span class="title-text"><?php echo JText::_('COM_COMMUNITY_TITLE_LEARNING_PROVIDER');?><span class=""> (<?php echo $totallpgroups ?>)</span></span>
            </div>
            <div class="mygroup-content-block">
                <div class="swiper-container-lpcircle swiper-container-horizontal swiper-container-free-mode" style="overflow: hidden;">
                    <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);" id="lpcircles">
                        <?php 
                            $eventsModel = CFactory::getModel('Events');
                            foreach ($lpcircles as $lpcircle){ 
                                $params = $lpcircle->getParams();
                                $tmpevents = $eventsModel->getGroupEvents($lpcircle->id, $params->get('grouprecentevents', GROUP_EVENT_RECENT_LIMIT));
                                $table =  JTable::getInstance( 'Group' , 'CTable' );
                                $table->load($lpcircle->id);
                                $link = 'index.php?option=com_community&view=groups&task=viewdiscussions&groupid=' . $lpcircle->id;
                                if($lpcircle->categoryid == 7){
                                    $link = 'index.php?option=com_community&view=groups&task=viewcourses&groupid='. $lpcircle->id;
                                }
                        ?>
                        <!-- <ul> -->
                        <div class="swiper-slide swiper-slide-active" style="width: 263.333px; margin-right: 12px;">
                            <div class="group-title" style="width: 1870px;">
                                <a href="<?php echo $lpcircle->getLink(); ?>">
                                <img alt="<?php echo $lpcircle->name?>" class="cAvatar jomNameTips group-image" src="<?php echo $table->getAvatar(); ?>" title="<?php echo $lpcircle->name?>"></a>
                                <div class="gr-title-box" style="height: 24px; overflow: hidden;">
                                    <a class="gr-title" href="<?php echo CRoute::_($link);?>"><?php echo $lpcircle->name?></a>
                                </div>
                                <div class="gr-owner-box">
                                    <p class="gr-owner"><?php echo JText::_('OWNER')?>: <?php echo ucfirst(JFactory::getUser($lpcircle->ownerid)->name) ?></p>
                                </div>
                                <div class="gr-description-box">
                                    <p class="group-description"><?php echo $lpcircle->description ?></p>
                                </div>
                                <?php if($device == 'mobile'){ ?>
                                <div class="mobile-group-statistic">
                                    <span class="group-statistic">
                                        <span title="Member count"><?php echo $lpcircle->membercount; ?>
                                            <img class="icon_member" src="<?php echo JURI::root();?>templates/t3_bs3_blank/images/Member30x30.png" style="width: 13px; height: 13px">
                                        </span> 
                                        <span title="Discuss count"><?php echo $lpcircle->discusscount; ?>
                                            <img class="icon_member" src="<?php echo JURI::root();?>templates/t3_bs3_blank/images/Topics30x30.png" style="width: 13px; height: 13px; margin-bottom:2px;"></span> 
                                        <span title="Event count"><?php echo count($tmpevents) ?>
                                            <img class="icon_member" src="<?php echo JURI::root();?>templates/t3_bs3_blank/images/Events30x30.png" style="width: 13px; height: 13px">
                                        </span>
                                    </span>
                                </div>
                                <?php } else {?>
                                <div class="desktop-group-statistic">
                                     <span class="group-statistic">
                                        <span title="Member count"><?php echo $lpcircle->membercount; ?>
                                            <img class="icon_member" src="<?php echo JURI::root();?>templates/t3_bs3_blank/images/Member30x30.png" style="width: 15px; height: 15px">
                                        </span> 
                                        <span title="Discuss count"><?php echo $lpcircle->discusscount; ?>
                                            <img class="icon_member" src="<?php echo JURI::root();?>templates/t3_bs3_blank/images/Topics30x30.png" style="width: 15px; height: 15px; margin-bottom:2px;"></span> 
                                        <span title="Event count"><?php echo count($tmpevents) ?>
                                            <img class="icon_member" src="<?php echo JURI::root();?>templates/t3_bs3_blank/images/Events30x30.png" style="width: 15px; height: 15px">
                                        </span>
                                    </span>
                                </div>
                                <?php }?>
                            </div>
                        </div><!-- </li> -->
                        <?php }?>
                        
                    </div>
                </div>
                <div style="clear:both"></div>
            </div>
        </div>

        
    </div>
</div>


<?php
if ($device == 'mobile') {
    $slideview = '1';
    $space = '-5';
//        $deviceWidth = '50';
} else if ($device == 'tablet') {
    $slideview = '2.5';
    $space = '12';
//        $deviceWidth = '300';
} else {
    $slideview = '3';
    $space = '12';
//        $deviceWidth = '400';
}
?>

<script type="text/javascript">
    var sociallimit = <?php echo $limit ?>; 
    var sociallimitstart = sociallimit;
    var courselimit = <?php echo $limit ?>; 
    var courselimitstart = courselimit;
    var lplimit = <?php echo $limit ?>; 
    var lplimitstart = lplimit;
    (function ($) {
        var x = screen.width;
        var a = 0;
//        console.log(x);
        if(x < 390){
            a = 1;
        }
        if(x >= 390 && x<=736){
            a = 1.5;
        }
        if(x>736 && x<=1366){
            a = 2;
        }
        if(x>1366){
            a = 3;
        }
        jQuery('img').each(function () {
            jQuery(this).attr('src', jQuery(this).attr('kvsrc'));
        });

        var swipersocial = new Swiper('.mygroups .swiper-container-social', {
            pagination: '.mygroups .swiper-pagination',
            slidesPerView: a,
            nextButton: '.mygroups .swiper-button-next',
            prevButton: '.mygroups .swiper-button-prev',
            spaceBetween: <?php echo $space; ?>,
            freeMode: true,
            freeModeMomentum: true,
            preventClicks: false,
            preventClicksPropagation: false,
            onTransitionEnd: function(swiper){
                 $.post("<?php echo CRoute::_('index.php?option=com_community&view=groups&task=loadAllCircles') ?>",{'circlecategoryid': 0, 'limitstart': sociallimitstart}, function(data){       
                    swiper.appendSlide(data);
                    sociallimitstart = sociallimitstart + sociallimit; 
                }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                    alert(thrownError); //alert with HTTP error
                });
            }
        });

        var swipercourse = new Swiper('.mygroups .swiper-container-course', {
            pagination: '.mygroups .swiper-pagination',
            slidesPerView: a,
            nextButton: '.mygroups .swiper-button-next',
            prevButton: '.mygroups .swiper-button-prev',
            spaceBetween: <?php echo $space; ?>,
            freeMode: true,
            freeModeMomentum: true,
            preventClicks: false,
            preventClicksPropagation: false,
            onTransitionEnd: function(swiper){
                $.post("<?php echo CRoute::_('index.php?option=com_community&view=groups&task=loadAllCircles') ?>",{'circlecategoryid': 6, 'limitstart': courselimitstart}, function(data){       
                    swiper.appendSlide(data);
                    courselimitstart = courselimitstart + courselimit; 
                }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                    alert(thrownError); //alert with HTTP error
                });
            }
        });

        var swiperlpcircle = new Swiper('.mygroups .swiper-container-lpcircle', {
            pagination: '.mygroups .swiper-pagination',
            slidesPerView: a,
            nextButton: '.mygroups .swiper-button-next',
            prevButton: '.mygroups .swiper-button-prev',
            spaceBetween: <?php echo $space; ?>,
            freeMode: true,
            freeModeMomentum: true,
            preventClicks: false,
            preventClicksPropagation: false,
            onTransitionEnd: function(swiper){
                $.post("<?php echo CRoute::_('index.php?option=com_community&view=groups&task=loadAllCircles') ?>",{'circlecategoryid': 7, 'limitstart': lplimitstart}, function(data){       
                    swiper.appendSlide(data);
                    lplimitstart = lplimitstart + lplimit; 
                }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                    alert(thrownError); //alert with HTTP error
                });
            }
        });
        
        var deviceGroup = $(window).width();
        var widthGr = deviceGroup - 50;
        $('.home .mygroups .mygroup-content-block .group-title').css('width', widthGr + 'px');
      
    })(jQuery);
</script>
