<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/

$config = CFactory::getConfig();
$groupsModel = CFactory::getModel('groups');

$session = JFactory::getSession();
$device = $session->get('device');

$search = JRequest::getVar('search');
$search = (string)$search;

$my = CFactory::getUser();
$listgroups = array();
$mygroups = $groupsModel->getCircles($my->id, 0, 0, 0, 0, false);
if ($mygroups) {
    foreach ($mygroups as $gr) {
        $listgroups[] = $gr->id;
    }
}

$all = $groupsModel->getAllGroups( $categoryId = null , $sorting = null , $search , $limit=0, $skipDefaultAvatar = false , $hidePrivateGroup = false, $pagination = true, $nolimit = false,$position =NULL, implode(',', $listgroups));

if ($device == 'mobile') {
    $the_first_search_circle = JText::_('COM_COMMUNITY_LIMIT_MOBILE');
} else {
    $the_first_search_circle = JText::_('COM_COMMUNITY_LIMIT');
}
$number = ceil((count($all) - $the_first_search_circle)/JText::_('COM_COMMUNITY_LIMIT_MORE')) + 1;

?>

<?php if( $groups ) {
if($_REQUEST['task']!=='display') {
?>
<div class="joms-page" style="background: none; padding: 0px">
    <?php
            $session = JFactory::getSession();
            $device = $session->get('device');
    $my = CFactory::getUser();
    $groupModel = CFactory::getModel('groups');
    if($my->_userid > 0){
        for ( $i = 0; $i < count( $groups ); $i++ ) {
            $group =& $groups[$i];
            if(!$group->published && ($my->id != $group->ownerid) ) continue;
            $isMine = $my->id == $group->ownerid;
            $isAdmin = $groupModel->isAdmin($my->id, $group->id);
            $isMember = $groupModel->isMember($my->id, $group->id);
            $isBanned = $group->isBanned($my->id);
            $creator = CFactory::getUser($group->ownerid);
            $groupAvatar = $group->getOriginalAvatar() . '?_=' . time();
            $waitingApproval = $groupModel->isWaitingAuthorization($my->id, $group->id);

            // (Chris)
            // approvals 0 = public; 1 = private; 2 = secret group
            // TODO: (Chris) Define secret group

            if ($group->approvals == 2 && !$isMember) {
                continue;
            }

            // Check if "Feature this" button should be added or not.
            $addFeaturedButton = false;
            $isFeatured = false;
            if ( $isCommunityAdmin && $showFeatured ) {
                $addFeaturedButton = true;
                if ( in_array($group->id, $featuredList) ) {
                    $isFeatured = true;
                }
            }

            //all the information needed to fill up the summary
            $params = $group->getParams();

            $eventsModel = CFactory::getModel('Events');
            $totalEvents = $eventsModel->getTotalGroupEvents($group->id);
            $showEvents = ($config->get('group_events') && $config->get('enableevents') && $params->get('eventpermission',
                1) >= 1);

            $videoModel = CFactory::getModel('videos');
            $showVideo = ($params->get('videopermission') != -1) && $config->get('enablevideos') && $config->get('groupvideos');
            if($showVideo) {
                $videoModel->getGroupVideos($group->id, '',
                    $params->get('grouprecentvideos', GROUP_VIDEO_RECENT_LIMIT));
                $totalVideos = $videoModel->total ? $videoModel->total : 0;
            }

            $showPhoto = ($params->get('photopermission') != -1) && $config->get('enablephotos') && $config->get('groupphotos');
            $photosModel = CFactory::getModel('photos');
            $albums = $photosModel->getGroupAlbums($group->id, true, false);
            $totalPhotos = 0;
            foreach ($albums as $album) {
                $albumParams = new CParameter($album->params);
                $totalPhotos = $totalPhotos + $albumParams->get('count');
            }

            $bulletinModel = CFactory::getModel('bulletins');
            $bulletins = $bulletinModel->getBulletins($group->id);
            $totalBulletin = $bulletinModel->total;

            // Check if "Invite friends" and "Settings" buttons should be added or not.
            $canInvite = false;
            $canEdit = false;

            //if (($isMember && !$isBanned) || $isCommunityAdmin) {
            if (!$isBanned || $isCommunityAdmin) {
                $canInvite = true;
                if ($isMine || $isAdmin || $isCommunityAdmin) {
                    $canEdit = true;
                }
            }
            $table =  JTable::getInstance( 'Group' , 'CTable' );
            $table->load($group->id);
            
            $class_cl = 'scircle_left';
            if ($i % 2 != 0) {
                $class_cl = 'scircle_right';
            }
        ?>
                    <?php

                    $groupId = $row->id;
                    $eventsModel = CFactory::getModel('Events');
                    $tmpEvents = $eventsModel->getGroupEvents($groupId, $params->get('grouprecentevents', GROUP_EVENT_RECENT_LIMIT));
                    $totalEvents = $eventsModel->getTotalGroupEvents($group->id);

                    ?>
        <div class="joms-list__item bordered-box <?php echo $class_cl;?>">
        <?php 
        $link = 'index.php?option=com_community&view=groups&task=viewdiscussions&groupid=' . $group->id;
        if($group->categoryid == 7){
            $link = 'index.php?option=com_community&view=groups&task=viewcourses&groupid='. $group->id;
        }
        ?>
            <div class="group-title" style="">
                <div class="circle_title_img">
                    <a href="<?php echo $group->getLink();?>">
                        <div class="left" id="images"
                             style="background-image: url('<?php echo $groupAvatar; ?>')"></div>
                    </a>
                    <div class="<?php echo $class_notif?>">
                      <span >
                     </span>
                            </div>
                                </div>
                <div class="circle_title">
                    <div class="gr-title-box" style="">
                        <a class="gr-title gr_<?php echo $group->id;?>" href="<?php echo CRoute::_($link);?>"><?php echo $this->escape($group->name);?></a>
                    </div>
                    <div class="gr-description-box">
                        <p class="group-description des_<?php echo $group->id;?>"><?php echo $group->description ?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php }?>
    <?php }?>
</div>
<div class="divLoadMore hide">Loading...<div class="loading"></div></div>
<script type="text/javascript">
(function ($) {
    jQuery('.gr-description-box').each(function () { 
        if (jQuery(this).find('p').height() > 58) { 
            jQuery(this).find('p').addClass('limited');
        }
    });
    jQuery('.gr-title-box').each(function () { 
        if (jQuery(this).find('a').height() > 44) { 
            jQuery(this).find('a').addClass('limited');
        }
    });
})(jQuery);
    
    var track_load = 1; //total loaded record group(s)
    var loading  = false; //to prevents multipal ajax loads
    var total_groups = <?php echo  $number ?>; //total record group(s)
    var i = 1;
    var searchstr = '<?php echo $search; ?>';
    
    jQuery(window).scroll(function () { 
        if (jQuery(window).scrollTop() + jQuery(window).height() >=
            jQuery('.cGroups').offset().top + jQuery('.cGroups').height()) {
        
            if (track_load < total_groups && loading==false) {
                loading = true; 
                console.log('test');
                jQuery('.divLoadMore').removeClass('hide');
                loadmore();
            }
        }
    });
    
    function loadmore() {
        jQuery.post("<?php echo CRoute::_('index.php?option=com_community&view=groups&task=loadSearchMore') ?>",{'group_no': track_load, 'searchstr': searchstr}, function(data){
            i++;
            jQuery(".joms-page").append(data); //append received data into the element

            //hide loading image
            jQuery('.divLoadMore').addClass('hide');

            track_load++; //loaded group increment
            loading = false;
            $button.html('<?php echo JText::_('COM_COMMUNITY_LOAD_MORE'); ?>');
            if (i >= total_groups) {
                $(".loadmore").hide();
            }
        }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?

            alert(thrownError); //alert with HTTP error
            jQuery('.divLoadMore').addClass('hide'); //hide loading image
            loading = false;

        });
    }
</script>
    <?php }
} else { ?>
<style>
    .jomsocial-wrapper .jomsocial {
    min-height: 120px;
}


</style>
<div class="joms-page">
    <div class="cEmpty cAlert"><?php echo JText::_('COM_COMMUNITY_GROUPS_NOITEM'); ?></div>
</div>
<?php } ?>
 <?php if($_REQUEST['task']=='display') { ?>
<div class="joms-page" style="background: none; padding: 0px"></div>
<div class="loadmore" style="float: right;display:none;">
         <a class="load_more"><?php echo JText::_('COM_COMMUNITY_LOAD_MORE'); ?></a>
         </div>
<script src="components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
            $(document).ready(function() {
    var track_load = 0; //total loaded record group(s)
    var loading  = false; //to prevents multipal ajax loads
    var total_groups = <?php echo  $number ?>; //total record group(s)
    var i = 1;
            $('.list_circle .joms-page').load("<?php echo CRoute::_('index.php?option=com_community&view=groups&task=load') ?>", {'group_no': track_load}, function () {
                track_load++;
                $(".loadmore").show();
            }); //load first group

    $(".load_more").click(function(){
         i++;

         $button = $(this);
         $button.html('<?php echo JText::_('COM_COMMUNITY_LOADING'); ?>');
            if(track_load <= total_groups && loading==false) //there's more data to load
            {

                loading = true; //prevent further ajax loading
                $('.animation_image').show(); //show loading image

                //load data from the server using a HTTP POST request
                $.post("<?php echo CRoute::_('index.php?option=com_community&view=groups&task=load') ?>",{'group_no': track_load}, function(data){

                    $(".list_circle .joms-page").append(data); //append received data into the element

                    //hide loading image
                    $('.animation_image').hide(); //hide loading image once data is received

                    track_load++; //loaded group increment
                    loading = false;
                     $button.html('<?php echo JText::_('COM_COMMUNITY_LOAD_MORE'); ?>');
                        if (i >= total_groups) {
               $(".loadmore").hide();
           }
                }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?

                    alert(thrownError); //alert with HTTP error
                    $('.animation_image').hide(); //hide loading image
                    loading = false;

                });

            }

    });

});

</script>
<?php } ?>
