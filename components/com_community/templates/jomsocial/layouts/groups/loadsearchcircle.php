<?php
/**
 * Created by PhpStorm.
 * User: kydonvn
 * Date: 26/07/2016
 * Time: 16:08
 */
defined('_JEXEC') or die();
$table =  JTable::getInstance( 'Group' , 'CTable' );
$i = 0;
foreach ($groups as $group) {
    $table->load($group->id);
    $class_cl = 'scircle_left';
    if ($i % 2 != 0) {
        $class_cl = 'scircle_right';
    }
    $i++;
    ?>
    <div class="joms-list__item bordered-box <?php echo $class_cl;?>">
        <div class="group-title" style="">
            <div class="circle_title_img">
                <a href="/index.php?option=com_community&view=groups&task=viewgroup&groupid=<?php echo $group->id ?>">
                    <div class="left" id="images"
                         style="background-image: url('<?php echo $table->getOriginalAvatar() . '?_=' . time(); ?>')"></div>
                </a>
                <div class="<?php echo $class_notif?>">
                  <span >
                    </span>
                </div>
            </div>
            <div class="circle_title">
                <div class="gr-title-box" style="">
                    <a class="gr-title gr_<?php echo $group->id;?>" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);?>"><?php echo $group->name;?></a>
                </div>
                <div class="gr-description-box">
                    <p class="group-description des_<?php echo $group->id;?>"><?php echo $group->description ?></p>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
