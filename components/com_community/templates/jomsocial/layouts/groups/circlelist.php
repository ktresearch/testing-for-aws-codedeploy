<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
/*
Added by Chris for Cicle Listing
*/

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . '/modules/mod_joomdle_my_courses/css/swiper.min.css');
$document->addScript(JUri::root() . '/modules/mod_joomdle_my_courses/swiper.min.js');


$session = JFactory::getSession();
$device = $session->get('device');

$config = CFactory::getConfig();
$my = CFactory::getUser();
$username = $my->username;
if($my->_permission) $cla = 'hidden';
$class = '';
$idstr = 'id="mobile-show"';
if ($device != 'mobile') {
    $class = 'desktop-tablet';
    $idstr = '';
}

$groupsModel = CFactory::getModel('groups');
$limit = $groupsModel->paginationlimit;

$socialcircle = 0;
$coursecircle = 6;
$lpcircle = 7;

$totalsocialgroups = $groupsModel->getCirclesCount($my->_userid, $socialcircle, 'owner');
$totalcommunitygroups = $groupsModel->getCirclesCount($my->_userid, $socialcircle, 'member');
$totalcoursegroups = $groupsModel->countgetCirclescourse($my->_userid, $coursecircle);
$totallpgroups = $groupsModel->getCirclesCount($my->_userid, $lpcircle);
$totalargroups = $groupsModel->getCirclesCount($my->_userid, $socialcircle,'archive');

require_once(JPATH_SITE . '/components/com_joomdle/helpers/content.php');
if ($totalsocialgroups == 0 && $totalcommunitygroups == 0 && $totalcoursegroups == 0 && $totallpgroups == 0) {
    ?>
    <div class="home">
        <div class="container">
            <div class="ownedCircle <?php echo $cla; ?>">
                <p class="title"><?php echo Jtext::_("COM_COMMUNITY_OWNED_CIRCLE"); ?></p>
                <img src="/images/MyCircles.png">
                <p class="description"><?php echo JText::_("COM_COMMUNITY_NO_OWNED_CIRCLE_DESCRIPTION"); ?></p>
                <button id="noCircleCreateCircle"
                        onclick="window.location='<?php echo CRoute::_('index.php?option=com_community&view=groups&task=create'); ?>';"><?php echo JText::_("COM_COMMUNITY_BUTTON_CREATE_CIRCLE"); ?></button>
            </div>
            <div class="communityCircle">
                <p class="title"><?php echo Jtext::_("COM_COMMUNITY_COMMUNITY_CIRCLE"); ?></p>
                <img src="/images/MyCircles.png">
                <p class="description"><?php echo JText::_("COM_COMMUNITY_NO_COMMUNITY_CIRCLE_DESCRIPTION"); ?></p>
                <button id="noCircleSearchCircle"><?php echo JText::_("COM_COMMUNITY_BUTTON_SEARCH_CIRCLE"); ?></button>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function ($) {
            jQuery("#noCircleSearchCircle").click(function () {
                var check_device = "<?php echo $device; ?>";
                jQuery(".search-all-type").css("display", "none");
                jQuery("#searchlearning").css("display", "none");
                jQuery("#searchfriend").css("display", "none");
                jQuery(".navbar-menu-icon").css("display", "none");
                jQuery(".navbar-search-all").addClass("hide");
                jQuery(".navbar-notification").addClass("hide");
                jQuery(".search-all-type-form").css("display", "block");
                jQuery("#searchcircle").css("display", "inline-block");
                jQuery("#searchcircle .search-text").focus();
                if (check_device == 'mobile') {
                    jQuery(".navbar-brand").hide();
                    jQuery('.navbar-toggle').hide();
                }
            });

        })(jQuery);
    </script>
    <?php
} else { ?>

    <div class="home">
        <div class="container">

            <div class="mygroups <?php echo $class; ?> have-groups">
                <i class="bg-second-img icon-create-circle  icon_create_circle <?php echo $cla; ?> >" onclick="window.location='<?php echo CRoute::_('index.php?option=com_community&view=groups&task=create'); ?>';"></i>
                <div class="mygroup-title-block <?php echo $cla; ?>">
                    <!--<span class="icon-circles"><img src="<?php // echo JURI::root();?>images/socialcircles.png"></span>-->
                    <span class="title-text"><?php echo JText::_('COM_COMMUNITY_MY_GROUPS'); ?></span>
                    <span class="title_number"><?php echo $totalsocialgroups ?></span>
                </div>
                <div class="mygroup-content-block <?php echo $cla; ?>">
                    <div class="swiper-container-social swiper-container-horizontal swiper-container-free-mode"
                         style="">
                        <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);" id="socialcircles">
                            <?php
                            $eventsModel = CFactory::getModel('Events');
                            foreach ($socialcircles as $socialcircle) {
                                $params = $socialcircle->getParams();
                                $tmpevents = $eventsModel->getGroupEvents($socialcircle->id, $params->get('grouprecentevents', GROUP_EVENT_RECENT_LIMIT));
                                $table = JTable::getInstance('Group', 'CTable');
                                $table->load($socialcircle->id);
                                $link = 'index.php?option=com_community&view=groups&task=viewdiscussions&groupid=' . $socialcircle->id;
                                if ($socialcircle->categoryid == 7) {
                                    $link = 'index.php?option=com_community&view=groups&task=viewabout&groupid=' . $socialcircle->id;
                                }
                                if ($socialcircle->notification != 0) {
                                    $class_notif = "notification";
                                } else {
                                    $class_notif = "notiflearn";
                                }
                                ?>
                                <!-- <ul> -->
                                <div class="swiper-slide swiper-slide-active"
                                     style="width: 263.333px; margin-right: 12px;">
                                    <div class="group-title" style="">
                                        <div class="circle_title_img">
                                            <?php
                                            $sub = $groupsModel->checkSubgroup($socialcircle->id);
                                                $image = $table->getOriginalAvatar();
                                                if (strstr ( $image, 'group.png' ))
                                                    $style = "background-size: contain;background-color: #f2f7fb;";
                                                else $style = "";
                                             ?>

                                            <a href="<?php echo CRoute::_($link); ?>">
                                                <div class="left" id="images"
                                                     style="background-image: url('<?php echo $table->getOriginalAvatar() . '?_=' . time(); ?>'); <?php echo $style ?>"></div>
                                            </a>
                                            <!-- coursesButtons-->
                                            <?php 
                                            $blnCircle = $groupsModel->getBLNCircle();
                                            if($socialcircle->id != $blnCircle->id ) {?>
                                                <div class="coursesButtons">
                                                    <a href="javascript:" class="joms-dropdown-button">
                                                        <img class="dropdown-button" onclick="" src="<?php echo JURI::root(); ?>media/joomdle/images/icon/ManageCourses/dropdown.png"/>
                                                    </a>

                                                    <ul class="joms-dropdown">
                                                        <li class=" action ">
                                                            <a class="btArchare" onclick="myArchive(<?php echo $sub ?>,<?php echo $socialcircle->id; ?>,1)" data-bt ="1" data-sub ="<?php echo $sub ?>" data-id="<?php echo $socialcircle->id; ?>"><?php echo JText::_('COM_COMMUNITY_BUTTON_ARCHIVE'); ?> </a>
                                                        </li>
                                                        <li class=" action ">
                                                            <a class="btRemove" onclick="myRemove(<?php echo $sub ?>,<?php echo $socialcircle->id; ?>)"  data-sub ="<?php echo $sub ?>" data-id="<?php echo $socialcircle->id; ?>"  ><?php echo JText::_('COM_COMMUNITY_REMOVE_BUTTON'); ?></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            <?php }?>
                                            <div class="<?php echo $class_notif ?>">

                                      <span>
                                            <?php
                                            echo $socialcircle->notification;
                                            ?>
                                        </span>
                                            </div>
                                        </div>
                                        <div class="circle_title">
                                            <div class="gr-title-box" style="">
                                                <a class="gr-title gr_<?php echo $socialcircle->id; ?>"
                                                   href="<?php echo CRoute::_($link); ?>"><?php echo $socialcircle->name; ?></a>
                                            </div>
                                            <div class="gr-description-box">
                                                <p class="group-description des_<?php echo $socialcircle->id; ?>"><?php echo $socialcircle->description ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- </li> -->
                            <?php } ?>

                        </div>
                    </div>
                    <?php if (count($totalsocialgroups) != 0 && $device != 'mobile') { ?>
                        <div class="swiper-button-next1"><i class="bg-second-img icon-next-black"></i></div>
                        <div class="swiper-button-prev1"><i class="bg-second-img icon-prev-black"></i></div>
                    <?php } ?>
                    <div style="clear:both"></div>
                </div>
            </div>

            <!-- List community circles -->
            <div class="mygroups <?php echo $class; ?> have-groups">
                <div class="mygroup-title-block">
                    <span class="title-text"><?php echo JText::_('COM_COMMUNITY_COMMUNITY_GROUPS'); ?></span>
                    <span class="title_number"><?php echo $totalcommunitygroups ?></span>
                </div>
                <div class="mygroup-content-block">
                    <div class="swiper-container-community swiper-container-horizontal swiper-container-free-mode"
                         style="">
                        <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);"
                             id="communitycircles">
                            <?php
                            $eventsModel = CFactory::getModel('Events');
                            foreach ($communitycircles as $communitycircle) {
                                $params = $communitycircle->getParams();
                                $tmpevents = $eventsModel->getGroupEvents($communitycircle->id, $params->get('grouprecentevents', GROUP_EVENT_RECENT_LIMIT));
                                $table = JTable::getInstance('Group', 'CTable');
                                $table->load($communitycircle->id);
                                $link = 'index.php?option=com_community&view=groups&task=viewabout&groupid=' . $communitycircle->id;
                                if ($communitycircle->categoryid == 7) {
                                    $link = 'index.php?option=com_community&view=groups&task=viewabout&groupid=' . $communitycircle->id;
                                }
                                if ($communitycircle->notification != 0) {
                                    $class_notif = "notification";
                                } else {
                                    $class_notif = "notiflearn";
                                }
                                ?>
                                <!-- <ul> -->
                                <div class="swiper-slide swiper-slide-active"
                                     style="width: 263.333px; margin-right: 12px;">
                                    <div class="group-title" style="">
                                        <div class="circle_title_img">
                                            <?php
                                                $image = $table->getOriginalAvatar();
                                                if (strstr ( $image, 'group.png' ))
                                                    $style = "background-size: contain;background-color: #f2f7fb;";
                                                else $style = "";
                                             ?>
                                            <a href="<?php echo CRoute::_($link); ?>">
                                                <div class="left" id="images"
                                                     style="background-image: url('<?php echo $table->getOriginalAvatar() . '?_=' . time(); ?>'); <?php echo $style ?>"></div>
                                            </a>
                                            <div class="<?php echo $class_notif ?>">
                                      <span>
                                            <?php
                                            echo $communitycircle->notification;
                                            ?>
                                        </span>
                                            </div>
                                        </div>
                                        <div class="circle_title">
                                            <div class="gr-title-box" style="">
                                                <a class="gr-title gr_<?php echo $communitycircle->id; ?>"
                                                   href="<?php echo CRoute::_($link); ?>"><?php echo $communitycircle->name ?></a>
                                            </div>
                                            <div class="gr-description-box">
                                                <p class="group-description des_<?php echo $communitycircle->id; ?>"><?php echo $communitycircle->description ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- </li> -->
                            <?php } ?>

                        </div>
                    </div>
                    <?php if (count($totalcommunitygroups) != 0 && $device != 'mobile') { ?>
                        <div class="swiper-button-next5"><i class="bg-second-img icon-next-black"></i></div>
                        <div class="swiper-button-prev5"><i class="bg-second-img icon-prev-black"></i></div>
                    <?php } ?>
                    <div style="clear:both"></div>
                </div>
            </div>

            <div class="mygroups <?php echo $class; ?> have-groups">
                <div class="mygroup-title-block">
                    <!--<span class="icon-circles"><img src="<?php // echo JURI::root();?>images/coursecircles.png"></span>-->
                    <span class="title-text"><?php echo JText::_('COM_COMMUNITY_TITLE_COURSE_CIRCLES'); ?></span>
                    <span class="title_number "><?php echo $totalcoursegroups ?></span>
                </div>
                <div class="mygroup-content-block">
                    <div class="swiper-container-course swiper-container-horizontal swiper-container-free-mode"
                         style="overflow: hidden;">
                        <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);" id="coursecircles">
                            <?php
                            $eventsModel = CFactory::getModel('Events');
                            $groupsModel = CFactory::getModel('groups');
                            foreach ($coursecircles as $coursecircle) {

                                $course_group = $groupsModel->getIsCourseGroup($coursecircle->id);
                                /*
                                if($course_group){
                                    $course_info = JoomdleHelperContent::call_method('get_course_info', (int)$course_group, '');
                                    $course_name = $course_info['fullname'];
                                    $course_image_avatar = $course_info['filepath'] . $course_info['filename'];
                                    $course_description = mb_strimwidth(strip_tags($course_info['summary']), 0, 70, "..");
                                }
                                */

                                $params = $coursecircle->getParams();
                                $tmpevents = $eventsModel->getGroupEvents($coursecircle->id, $params->get('grouprecentevents', GROUP_EVENT_RECENT_LIMIT));
                                $table = JTable::getInstance('Group', 'CTable');
                                $table->load($coursecircle->id);
                                $link = 'index.php?option=com_community&view=groups&task=viewdiscussions&groupid=' . $coursecircle->id;
                                if ($coursecircle->categoryid == 7) {
                                    $link = 'index.php?option=com_community&view=groups&task=viewabout&groupid=' . $coursecircle->id;
                                }

                                //$course_image_avatar = $table->getAvatar();
                                $course_description = mb_strimwidth(strip_tags($coursecircle->description), 0, 70, "..");
                                $course_name = $coursecircle->name;
                                if ($coursecircle->notification != 0) {
                                    $class_notif = "notification";
                                } else {
                                    $class_notif = "notiflearn";
                                }
                                ?>
                                <!-- <ul> -->
                               
                                <div class="swiper-slide swiper-slide-active"
                                     style="width: 263.333px; margin-right: 12px;">
                                    <div class="group-title" style="">
                                        <div class="circle_title_img">
                                            <?php
                                            $image = $table->getOriginalAvatar();
                                            if (strstr ( $image, 'group.png' )){
                                                $style = "background-size: contain;background-color: #f2f7fb;";
                                                $datacourse = JoomdleHelperContent::call_method('get_course_info', (int)$course_group, $username);
                                                    $groupAvatar = $datacourse['filepath']. $datacourse['filename'];
                                            }
                                            else {
                                                $style = "";
                                                $groupAvatar = $image . '?_=' . time();
                                            }
                                            
                                            ?>
                                               
                                            <a href="<?php echo CRoute::_($link); ?>">
                                                <div class="left" id="images"
                                                     style="background-image: url('<?php echo $groupAvatar; ?>'); <?php echo $style ?> "></div>
                                            </a>
                                            <div class="<?php echo $class_notif ?>">
                                      <span>
                                            <?php
                                            echo $coursecircle->notification;
                                            ?>
                                        </span>
                                            </div>
                                        </div>
                                        <div class="circle_title">
                                            <div class="gr-title-box" style="">
                                                <a class="gr-title gr_<?php echo $coursecircle->id; ?>"
                                                   href="<?php echo CRoute::_($link); ?>"><?php echo $course_name; ?></a>
                                            </div>
                                            <div class="gr-description-box">
                                                <p class="group-description des_<?php echo $coursecircle->id; ?>"><?php echo $coursecircle->description; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- </li> -->
                            <?php  }?>

                        </div>
                    </div>
                    <?php if (count($totalcoursegroups) != 0 && $device != 'mobile') { ?>
                        <div class="swiper-button-next2"><i class="bg-second-img icon-next-black"></i></div>
                        <div class="swiper-button-prev2"><i class="bg-second-img icon-prev-black"></i></div>
                    <?php } ?>
                    <div style="clear:both"></div>
                </div>
            </div>

<!--            <div class="mygroups <?php echo $class; ?> have-groups">
    <?php if ($totallpgroups) { ?>
                    <div class="mygroup-title-block">
                        <span class="icon-circles"><img src="<?php // echo JURI::root(); ?>media/joomdle/images/icon/ManageCourses/CS_30x30.png"></span>
                        <span class="title-text"><?php echo JText::_('COM_COMMUNITY_TITLE_LEARNING_PROVIDER'); ?></span>
                        <span class="title_number"><?php echo $totallpgroups ?></span>
                    </div>
    <?php } ?>

                <div class="mygroup-content-block">
                    <div class="swiper-container-lpcircles swiper-container-horizontal swiper-container-free-mode"
                         style="overflow: hidden;">
                        <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);" id="lpcircles">
                            <?php
                            $eventsModel = CFactory::getModel('Events');
                            foreach ($lpcircles as $lpcircle) {
                                $params = $lpcircle->getParams();
                                $tmpevents = $eventsModel->getGroupEvents($lpcircle->id, $params->get('grouprecentevents', GROUP_EVENT_RECENT_LIMIT));
                                $table = JTable::getInstance('Group', 'CTable');
                                $table->load($lpcircle->id);
                                $link = 'index.php?option=com_community&view=groups&task=viewdiscussions&groupid=' . $lpcircle->id;
                                if ($lpcircle->categoryid == 7) {
                                    $link = 'index.php?option=com_community&view=groups&task=viewabout&groupid=' . $lpcircle->id;
                                }
                                if ($lpcircle->notification != 0) {
                                    $class_notif = "notification";
                                } else {
                                    $class_notif = "notiflearn";
                                }
                                ?>
                                 <ul> 
                                <div class="swiper-slide swiper-slide-active"
                                     style="width: 263.333px; margin-right: 12px;">
                                    <div class="group-title" style="">
                                        <div class="circle_title_img">
                                            <?php
                                            $image = $table->getOriginalAvatar();
                                            if (strstr($image, 'group.png'))
                                                $style = "background-size: contain;background-color: #f2f7fb;";
                                            else
                                                $style = "";
                                            ?>
                                            <a href="<?php echo CRoute::_($link); ?>">
                                                <div class="left" id="images"
                                                     style="background-image: url('<?php echo $table->getOriginalAvatar() . '?_=' . time(); ?>'); <?php echo $style ?>"></div>
                                            </a>
                                            <div class="<?php echo $class_notif ?>">
                                                <span>
        <?php
        echo $lpcircle->notification;
        ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="circle_title">
                                            <div class="gr-title-box" style="">
                                                <a class="gr-title gr_<?php echo $lpcircle->id; ?>"
                                                   href="<?php echo CRoute::_($link); ?>"><?php echo $lpcircle->name ?></a>
                                            </div>
                                            <div class="gr-description-box">
                                                <p class="group-description des_<?php echo $lpcircle->id; ?>"><?php echo $lpcircle->description ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div> </li> 
                    <?php } ?>

                        </div>
                    </div>
    <?php if (count($totallpgroups) != 0 && $device != 'mobile') { ?>
                        <div class="swiper-button-next4"><img class="icon-next" src="/images/NextIcon.png"></div>
                        <div class="swiper-button-prev4"><img class="icon-next" src="/images/PreviousIcon.png"></div>
    <?php } ?>
                    <div style="clear:both"></div>
                </div>
            </div>-->
            
            <!--dfgdfg-->
                <div class="mygroups <?php echo $class; ?> have-groups">
                <div class="mygroup-title-block <?php echo $cla; ?>">
                    <!--<span class="icon-circles"><img src="<?php // echo JURI::root(); ?>images/socialcircles.png"></span>-->
                    <?php if($totalargroups) {?>
                    <span class="title-text"><?php echo JText::_('COM_COMMUNITY_MY_ARCHIVE_GROUP'); ?></span>
                    <span class="title_number"><?php echo $totalargroups ?></span>
                    <?php }?>
                </div>
                <div class="mygroup-content-block <?php echo $cla; ?>">
                    <div class="swiper-container-lpcircle swiper-container-horizontal swiper-container-free-mode" style="overflow: hidden;"
                         style="">
                        <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);" id="socialcircles">
                            <?php
//                                var_dump($arcircles);
                            $eventsModel = CFactory::getModel('Events');
                            foreach ($arcircles as $socialcircle) {
                                $params = $socialcircle->getParams();
                                $tmpevents = $eventsModel->getGroupEvents($socialcircle->id, $params->get('grouprecentevents', GROUP_EVENT_RECENT_LIMIT));
                                $table = JTable::getInstance('Group', 'CTable');
                                $table->load($socialcircle->id);
                                $link = 'index.php?option=com_community&view=groups&task=viewdiscussions&groupid=' . $socialcircle->id;
                                if ($socialcircle->categoryid == 7) {
                                    $link = 'index.php?option=com_community&view=groups&task=viewabout&groupid=' . $socialcircle->id;
                                }
                                if ($socialcircle->notification != 0) {
                                    $class_notif = "notification";
                                } else {
                                    $class_notif = "notiflearn";
                                }
                                ?>
                                <!-- <ul> -->
                                <div class="swiper-slide swiper-slide-active"
                                       style="width: 263.333px; margin-right: 12px;">
                                    <div class="group-title" style="">
                                        <div class="circle_title_img">
                                            <?php
                                            $sub = $groupsModel->checkSubgroup($socialcircle->id);
                                            $image = $table->getOriginalAvatar();
                                            if (strstr($image, 'group.png'))
                                                $style = "background-size: contain;background-color: #f2f7fb;";
                                            else
                                                $style = "";
                                            ?>

                                             <a href="<?php echo CRoute::_($link); ?>">
                                                <div class="left" id="images"
                                                     style="background-image: url('<?php echo $table->getOriginalAvatar() . '?_=' . time(); ?>'); <?php echo $style ?>"></div>
                                            </a>
                                            <!-- coursesButtons-->
                                            <div class="coursesButtons">
                                                <a href="javascript:" class="joms-dropdown-button">
                                                    <img class="dropdown-button" onclick="" src="<?php echo JURI::root(); ?>media/joomdle/images/icon/ManageCourses/dropdown.png"/>
                                                </a>

                                                <ul class="joms-dropdown">
                                                    <li class=" action ">
                                                        <a class="btArchare" onclick="myArchive(<?php echo $sub ?>,<?php echo $socialcircle->id; ?>,0)" data-bt ="0" data-sub ="<?php echo $sub ?>" data-id="<?php echo $socialcircle->id; ?>"><?php echo JText::_('COM_COMMUNITY_BUTTON_UNARCHIVE'); ?> </a>
                                                    </li>
                                                    <li class=" action ">
                                                        <a class="btRemove" onclick="myRemove(<?php echo $sub ?>,<?php echo $socialcircle->id; ?>)"  data-sub ="<?php echo $sub ?>" data-id="<?php echo $socialcircle->id; ?>"  ><?php echo JText::_('COM_COMMUNITY_REMOVE_BUTTON'); ?></a>
                                                    </li>
                                                </ul>
                                            </div>
<!--                                            <div class="<?php echo $class_notif ?>">

                                                <span>
                                                    <?php
                                                    echo $socialcircle->notification;
                                                    ?>
                                                </span>
                                            </div>-->

                                        </div>
                                        <div class="circle_title">
                                            <div class="gr-title-box" style="">
                                                <a class="gr-title gr_<?php echo $socialcircle->id; ?>"
                                                    href="<?php echo CRoute::_($link); ?>"><?php echo $socialcircle->name; ?></a>
                                            </div>
                                            <div class="gr-description-box">
                                                <p class="group-description des_<?php echo $socialcircle->id; ?>"><?php echo $socialcircle->description ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- </li> -->
    <?php } ?>

                        </div>
                    </div>
    <?php if (count($totalargroups) != 0 && $device != 'mobile') { ?>
                        <div class="swiper-button-next3"><i class="bg-second-img icon-next-black"></i></div>
                        <div class="swiper-button-prev3"><i class="bg-second-img icon-prev-black"></i></div>
    <?php } ?>
                    <div style="clear:both"></div>
                </div>
            </div>
            <!--rêt-->

        </div>
    </div>
     <div class="modal" id="upfile-success">
        <div class="modal-content">
            <p id = "error"  class="messsuccess"></p>
            <button class="closetitle"><?php echo JText::_('COM_COMMUNITY_BUTTON_CLOSE_BUTTON');?></button>
        </div>
    </div>

    <?php
    if ($device == 'mobile') {
        $slideview = '1.3';
        $space = '11';
//        $deviceWidth = '50';
    } else if ($device == 'tablet') {
        $slideview = '2.01';
        $space = '11';
//        $deviceWidth = '300';
    } else {
        $slideview = '2.01';
        $space = '11';
//        $deviceWidth = '400';
    }
    if ($device == 'mobile') {
        $touch = ' allowTouchMove : true,';
    } else {
        $touch = ' allowTouchMove : false,noSwipingClass : \'swiper-slide\',
            noswipingClass: \'swiper-slide\',';
    }
    ?>

    <script type="text/javascript">
        var sociallimit = <?php echo $limit ?>;
        var sociallimitstart = sociallimit;
        var communitylimit = <?php echo $limit ?>;
        var communitylimitstart = communitylimit;
        var courselimit = <?php echo $limit ?>;
        var courselimitstart = courselimit;
        var lplimit = <?php echo $limit ?>;
        var lplimitstart = lplimit;
        var arsociallimit = <?php echo $limit ?>;
        var arsociallimitstart = arsociallimit;
        (function ($) {
            var x = screen.width;
            var a = 0;
    //        console.log(x);
            if(x < 390){
                a = 1;
            }
            if(x >= 390 && x<=736){
                a = 1.5;
            }
            if(x>736 && x<=1366){
                a = 2;
            }
            if(x>1366){
                a = 3;
            }
            jQuery('img').each(function () {
                jQuery(this).attr('src', jQuery(this).attr('kvsrc'));
            });

            var swipersocial = new Swiper('.mygroups .swiper-container-social', {
                <?php echo $touch; ?>
                pagination: '.mygroups .swiper-pagination',
                slidesPerView: a,
                nextButton: '.mygroups .swiper-button-next1',
                prevButton: '.mygroups .swiper-button-prev1',
                spaceBetween: <?php echo $space; ?>,
                freeMode: true,
                freeModeMomentum: true,
                preventClicks: false,
                preventClicksPropagation: false,
                onTransitionEnd: function (swiper) {
                    jQuery.post("<?php echo CRoute::_('index.php?option=com_community&view=groups&task=loadMyCircles') ?>", {
                        'circlecategoryid': 0,
                        'limitstart': sociallimitstart,
                        'checklist': 'owner'
                    }, function (data) {
                        swiper.appendSlide(data);
                        sociallimitstart = sociallimitstart + sociallimit;
                    }).fail(function (xhr, ajaxOptions, thrownError) { //any errors?
                        alert(thrownError); //alert with HTTP error
                    });
                }
            });

            var swipersocial = new Swiper('.mygroups .swiper-container-community', {
                <?php echo $touch; ?>
                pagination: '.mygroups .swiper-pagination',
                slidesPerView: a,
                nextButton: '.mygroups .swiper-button-next5',
                prevButton: '.mygroups .swiper-button-prev5',
                spaceBetween: <?php echo $space; ?>,
                freeMode: true,
                freeModeMomentum: true,
                preventClicks: false,
                preventClicksPropagation: false,
                onTransitionEnd: function (swiper) {
                    jQuery.post("<?php echo CRoute::_('index.php?option=com_community&view=groups&task=loadMyCircles') ?>", {
                        'circlecategoryid': 0,
                        'limitstart': communitylimitstart,
                        'checklist': 'member'
                    }, function (data) {
                        swiper.appendSlide(data);
                        communitylimitstart = communitylimitstart + communitylimit;
                    }).fail(function (xhr, ajaxOptions, thrownError) { //any errors?
                        alert(thrownError); //alert with HTTP error
                    });
                }
            });

            var swipercourse = new Swiper('.mygroups .swiper-container-course', {
                <?php echo $touch; ?>
                pagination: '.mygroups .swiper-pagination',
                slidesPerView: a,
                nextButton: '.mygroups .swiper-button-next2',
                prevButton: '.mygroups .swiper-button-prev2',
                spaceBetween: <?php echo $space; ?>,
                freeMode: true,
                freeModeMomentum: true,
                preventClicks: false,
                preventClicksPropagation: false,
                onTransitionEnd: function (swiper) {
                    jQuery.post("<?php echo CRoute::_('index.php?option=com_community&view=groups&task=loadMyCirclesCourse') ?>", {
                        'circlecategoryid': 6,
                        'limitstart': courselimitstart
                    }, function (data) {
                        swiper.appendSlide(data);
                        courselimitstart = courselimitstart + courselimit;
                    }).fail(function (xhr, ajaxOptions, thrownError) { //any errors?
                        alert(thrownError); //alert with HTTP error
                    });
                }
            });

            var swiperlpcircle = new Swiper('.mygroups .swiper-container-lpcircle', {
                <?php echo $touch; ?>
                pagination: '.mygroups .swiper-pagination',
                slidesPerView: a,
                nextButton: '.mygroups .swiper-button-next3',
                prevButton: '.mygroups .swiper-button-prev3',
                spaceBetween: <?php echo $space; ?>,
                freeMode: true,
                freeModeMomentum: true,
                preventClicks: false,
                preventClicksPropagation: false,
                onTransitionEnd: function (swiper) {
                    jQuery.post("<?php echo CRoute::_('index.php?option=com_community&view=groups&task=loadMyCircles') ?>", {
                    'circlecategoryid': 0,
                    'limitstart': lplimitstart,
                    'checklist': 'archived'
                    }, function (data) {
                        swiper.appendSlide(data);
                        lplimitstart = lplimitstart + lplimit;
                    }).fail(function (xhr, ajaxOptions, thrownError) { //any errors?
                        alert(thrownError); //alert with HTTP error
                    });
                }
            });

            jQuery('.gr-title-box').each(function () {
                if (jQuery(this).find('a').height() > 48) {
                    jQuery(this).find('a').addClass('limited');
                }
            });
            jQuery('.gr-description-box').each(function () {
                if (jQuery(this).find('p').height() > 48) {
                    jQuery(this).find('p').addClass('limited');
                }
            });

//        var deviceGroup = jQuery(window).width();
//        var widthGr = deviceGroup - 50;
//        jQuery('.home .mygroups .mygroup-content-block .group-title').css('width', widthGr + 'px');

        })(jQuery);
        jQuery(document).ready(function () {
            jQuery(document).delegate('.joms-dropdown-button', 'click', function () {
                jQuery(this).next('.joms-dropdown').toggle();
            });
        }(jQuery));
         function myArchive(subgroup,id,bt){
                jQuery('.joms-dropdown').hide();
//                var subgroup = jQuery(this).attr('data-sub');
//                var id = jQuery(this).attr('data-id');
//                var bt = jQuery(this).attr('data-bt');
            if (bt == 1) action = 'archive';
            else action = 'unarchive';
            if (subgroup == 0){
            if (bt == 1){
            lgtCreatePopup('confirm', {
            'yesText': "<?php echo JText::_('COM_COMMUNITY_BUTTON_PROCEED'); ?>",
                    'content': '<?php echo '<b>' . JText::_('COM_COMMUNITY_MESSAGE_POPUP_ARCHIVE1') . '</b><br>' . JText::_('COM_COMMUNITY_MESSAGE_POPUP_ARCHIVE2'); ?>'
            }, function () {

            // Run AJAX here
            jQuery.ajax({
            url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxArchiveCircle') ?>",
                    type: 'POST',
                    data: {id: id, action: action},
                    beforeSend: function () {
                    lgtCreatePopup('', {'content': 'Loading...'});
                    },
                    success: function (result) {
                    var res = JSON.parse(result);
                    console.log(res);
                    lgtRemovePopup();
                    window.location.href = window.location.href;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                    }
            });
            });
        }
        else {
            jQuery.ajax({
            url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxArchiveCircle') ?>",
                    type: 'POST',
                    data: {id: id, action: action},
                    beforeSend: function () {
                    lgtCreatePopup('', {'content': 'Loading...'});
                    },
                    success: function (result) {
                    var res = JSON.parse(result);
                    console.log(res);
                    lgtRemovePopup();
                    window.location.href = window.location.href;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                    }
            });
            }

        }
        else{
            var modal2 = document.getElementById('upfile-success');
            jQuery('.messsuccess').html('<?php echo JText::_("COM_COMMUNITY_WARNING_ARCHIVE"); ?>')
                    jQuery('body').removeClass('overlay2');
            modal2.style.display = "block";
            }

        }
        jQuery(' .closetitle').click(function(){
            var modal = document.getElementById('upfile-success');
            modal.style.display = "none";
        });
        function myRemove(subgroup,id){
            jQuery('.joms-dropdown').hide();
//                var subgroup = jQuery(this).attr('data-sub');
//                var id = jQuery(this).attr('data-id');
            if (subgroup == 0){
            lgtCreatePopup('confirm', {
            'yesText': "<?php echo JText::_('COM_COMMUNITY_BUTTON_PROCEED'); ?>",
                    'content': '<?php echo JText::_('COM_COMMUNITY_MESSAGE_REMOVE_CIRCLE'); ?>'
            }, function () {

            // Run AJAX here
            jQuery.ajax({
            url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxRemoveCircle') ?>",
                type: 'POST',
                data: {id: id},
                beforeSend: function () {
                lgtCreatePopup('', {'content': 'Loading...'});
                },
                success: function (result) {
                var res = JSON.parse(result);
                console.log(res);
                lgtRemovePopup();
                window.location.href = window.location.href;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
                }
            });
        });
        }
        else{
            var modal2 = document.getElementById('upfile-success');
            jQuery('.messsuccess').html('<?php echo JText::_("COM_COMMUNITY_WARNING_REMOVE_CIRCLE"); ?>')
                    jQuery('body').removeClass('overlay2');
            modal2.style.display = "block";
            }
        }
    </script>
<?php } ?>
