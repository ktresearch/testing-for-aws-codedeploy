<?php
/**
 * Created by PhpStorm.
 * User: kydonvn
 * Date: 26/07/2016
 * Time: 16:08
 */
defined('_JEXEC') or die();
$table =  JTable::getInstance( 'Group' , 'CTable' );
foreach ($groups as $group) {
    $table->load($group->id);
    ?>
    <div class="joms-list__item bordered-box">
        <div class="col-xs-3 pull-left">
            <div class="jom-list__avatar" style="padding-top: 15px">
                <a href="/index.php?option=com_community&view=groups&task=viewgroup&groupid=<?php echo $group->id ?>&Itemid=266" class="joms-avatar">
                    <img src="<?php echo $table->getAvatar(); ?>"  class="img-responsive">
                </a>
            </div>
        </div>
        <div class="col-xs-9 custom-header pull-left">
            <h1><a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);?>"> <?php echo $group->name  ?></a></h1>
            <h3><?php echo JText::_('OWNER').': '.ucfirst(JFactory::getUser($group->ownerid)->name); ?></h3>
            <p class="margin-0 joms--description"><?php echo $group->description ?></p>
        </div>
        <div class="col-xs-12">
            <p class="joms--description" style="padding-top: 10px"><?php echo $group->membercount ?> <?php echo JText::_('COM_COMMUNITY_MEMBERS'); ?> <?php echo $group->discusscount ?> <?php echo JText::_('COM_COMMUNITY_TOPICS'); ?></p>
        </div>
    </div>
<?php } ?>
