<?php
defined('_JEXEC') or die();

$my = CFactory::getUser();
if(!$group->published)
    $hide = 'hidden';
foreach ($members as $member) {
    ?>
    <div class="joms-list__item member">
        <div class="jom-list__avatar list_avatar_member memberAvatar">
            <a class="cIndex-Avatar" href="<?php echo CRoute::_('index.php?option=com_community&view=profile&fromcircle=1&userid=' . $member->id); ?>">
                <img class="cAvatar img-responsive" src="<?php echo $member->getAvatar(); ?>" alt="<?php echo $member->getDisplayName(); ?>" data-author-remove="<?php echo $member->id; ?>">
            </a>
        </div>

        <div class="memberInfo">
            <div class="memberName">
                <a href="<?php echo CRoute::_('index.php?option=com_community&view=profile&fromcircle=1&userid=' . $member->id); ?>"><?php echo CActivities::truncateComplex($member->getDisplayName(), 30, true); ?></a>

                <div class="memberButtons <?php echo $hide; ?>">
                    <?php if (($isAdmin || $isSuperAdmin) && !$isBLNCircle) { ?>
                        <a href="javascript:" class="joms-dropdown-button">
                            <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                        </a>

                        <ul class="joms-dropdown">
                            <?php if (!$course_group && 1 == 0) { //disable this  ?>
                                <li>
                                    <a href="javascript:void(0);" onclick="jax.call('community', 'groups,ajaxAddAdmin', '<?php echo $member->id; ?>', '<?php echo $group->id; ?>');
                                        setTimeout('window.location.href=window.location.href', 100);">
                                        <span><?php echo JText::_('COM_COMMUNITY_GROUPS_ADMIN'); ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                            <li>
                                <a href="javascript:" onclick="removeMember(<?php echo $group->id . ', ' . $member->id . ', \'' . $member->getDisplayName() . '\''; ?>)">
                                    <span><?php echo JText::_('COM_COMMUNITY_REMOVE'); ?></span>
                                </a>
                            </li>
                            <li class="btBanMember" data-grid="<?php echo $group->id; ?>" data-mid="<?php echo $member->id; ?>" data-mname="<?php echo $member->getDisplayName(); ?>">
                                <a href="javascript:" onclick="banMember(<?php echo $group->id . ', ' . $member->id . ', \'' . $member->getDisplayName() . '\''; ?>)">
                                    <span><?php echo JText::_('COM_COMMUNITY_GROUPS_BAN_MEMBER'); ?></span>
                                </a>
                            </li>
                        </ul>
                    <?php } ?>
                </div>
            </div>
            <?php if ($type) { ?>
                <div class="joms-js--request-notice-group-<?php echo $group->id; ?>-<?php echo $member->id; ?>"></div>
                <div class="joms-js--request-buttons-group-<?php echo $group->id; ?>-<?php echo $member->id; ?>">
                    <a href="javascript:" onclick="joms.api.groupApprove('<?php echo $group->id; ?>', '<?php echo $member->id; ?>');">
                        <button class="joms-button--primary joms-button--smallest joms-button--full-small"><?php echo JText::_('COM_COMMUNITY_PENDING_ACTION_APPROVE'); ?></button>
                    </a>
                    <a href="javascript:" onclick="joms.api.groupRemoveMember('<?php echo $group->id; ?>', '<?php echo $member->id; ?>');">
                        <button class="joms-button--neutral joms-button--smallest joms-button--full-small"><?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING_ACTION_REJECT'); ?></button>
                    </a>
                </div>
            <?php } ?>
        </div>

        <?php
        $disableFriendButton = 1;
        if ($isMember && $my->id != $member->id && !$disableFriendButton) {
            $isFriend = CFriendsHelper::isConnected($member->id, $my->id);
            $isWaitingApproval = CFriendsHelper::isWaitingApproval($my->id, $member->id);
            $isWaitingResponse = CFriendsHelper::isWaitingApproval($member->id, $my->id);
            ?>
            <div class="js-memb-btn-friend">
                <div class="button">
                    <?php if ($isFriend) { ?>
                        <a href="javascript:" class="joms-focus__button--friend" onclick="joms.api.friendRemove('<?php echo $member->id; ?>')">
                            <?php echo JText::_('COM_COMMUNITY_CIRCLE_FRIEND'); ?>
                        </a>
                    <?php } else if ($isWaitingApproval) { ?>
                        <a href="javascript:" onclick="joms.api.friendAdd('<?php echo $member->id; ?>')">
                            <button type="button" class="joms-button--primary button--small pending">
                                <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                <?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING'); ?>
                            </button>
                        </a>
                    <?php } else if ($isWaitingResponse) { ?>
                        <a href="javascript:" onclick="joms.api.friendResponse('<?php echo $member->id; ?>')">
                            <button type="button" class="joms-button--primary button--small pending">
                                <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                <?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING'); ?>
                            </button>
                        </a>
                    <?php } else { ?>
                        <a href="javascript:" class="joms-focus__button--add" onclick="joms.api.friendAdd('<?php echo $member->id ?>')">
                            <?php echo JText::_('COM_COMMUNITY_CIRCLE_ADD_FRIEND'); ?>
                        </a>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>
<?php
}
?>
<style type="text/css">
    @media only screen and (orientation: landscape) {
        .joms-dropdown{
            right: auto !important;
            top : 16px !important;
        }
    }

    @media only screen and (orientation: portrait) {
        .joms-popup--whiteblock
        {
            left: 0px !important;
            right: 0px !important;
        }
    }
</style>
<script>
    function banMember(groupid, memberid, membername) {
        lgtCreatePopup('confirm', {'content': 'Are you sure you want to ban ' + membername + '?', 'yesText': 'Ban'}, function () {
            joms.api.groupBanMember(groupid, memberid);
        });
    }
</script>