<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/

/*
Added by Chris for view course tab
*/

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root().'/modules/mod_joomdle_my_courses/css/swiper.min.css');
$document->addScript(JUri::root().'/modules/mod_joomdle_my_courses/swiper.min.js');
require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
JLoader::import('joomla.user.helper'); 

$session = JFactory::getSession();
$device = $session->get('device');

$my = CFactory::getUser();
$username = $my->username;
$categoryid = $group->moodlecategoryid; 

$class = '';
$idstr = 'id="mobile-show"';
if($device != 'mobile') {
    $class = 'desktop-tablet';
    $idstr = '';
}
?>

<div class="home">
    <div class="container">
        
        <div class="my-course have-courses" id="new-course">
            <div class="course-title-block">
                <?php if($isManager || $isAdmin || $isSuperAdmin){ ?>
                <h2><a href="#"><?php echo JText::_('COM_COMMUNITY_LP_COURSES');?><span class=""> (<?php echo count($newcourse) ?>)</span></a></h2>
                <?php } else { ?>
                <h2><a href="#"><?php echo JText::_('COM_COMMUNITY_LP_WIP');?><span class=""> (<?php echo count($newcourse) ?>)</span></a></h2>
                <?php } ?>

            </div>
          
            <div class="course-content-block <?php echo $class; ?>">
                <div class="swiper-container swiper-container-horizontal swiper-container-free-mode">
                    <div class="swiper-wrapper" >

                        <?php foreach ($newcourse as $course) { ?>
                        <?php 
                            $string = $course['userroles']; 
                            $pos = strpos($string, "editingteacher");
                        ?>
                        <div id="newcourse-<?php echo $course['id'] ?>" class="swiper-slide single_course swiper-slide-active" style="width: 497.2px; margin-right: 8px;">
                            <div class="title-course" style="width: 718px;">
                                <div class="course-image-wrapper" style="background-image: url('<?php echo $course['filepath'] . $course['filename'] ?>')">
                                </div>
                                <a class="course-title" href="<?php echo CRoute::_('index.php?option=com_joomdle&view=course&course_id='.$course['id']) ?>"><?php echo $course['fullname'] ?></a>
                                <!--<p class="course-timeend">Valid Until: <?php // echo $course['timeend'] ?></p>-->
                                <div class="courseDetails">
                                    <?php
                                    $userid = JUserHelper::getUserId($course['creator']);
                                    $user_owner = JFactory::getUser($userid); 
                                    if ($course['count_learner'] && $course['count_learner'] > 1) {
                                        echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $course['count_learner'] . ' ' . JText::_('COM_COMMUNITY_COURSE_SUBSCRIBERS') . '</span></br>';
                                    } else {
                                        echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $course['count_learner'] . ' ' . JText::_('COM_COMMUNITY_COURSE_SUBSCRIBER') . '</span></br>';
                                    }
                                    $time_view = '';
                                    if ($course['timepublish'] && $course['timepublish'] > 0) {
                                        $time_published = $course['timepublish']; 
                                        $date2 = JFactory::getDate($time_published);
                                        $time_view = CTimeHelper::timeLapseNew($date2, false);
                                    }
                                    if ($time_view != '') {
                                        echo number_format((float)$course['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                    } else {
                                        echo number_format((float)$course['duration'], 2, '.', '') . ' hr';
                                    }
                                    ?>
                                </div>
                                <p class="course-description"><?php echo mb_strimwidth($course['summary'], 0, 130, "..."); ?></p>

                                <?php // getting the user roles -> $course['userroles'] 
                                      // content creator = editingteacher  
                                ?>
                                
                                <div>
                                    <?php if($isManager || $isAdmin || $isSuperAdmin){ ?>
                                    <div>
                                    <a class="left-side" id="remove" data-id="<?php echo $course['id'] ?>" href="#"><?php echo JText::_('COM_COMMUNITY_BUTTON_REMOVE'); ?></a>
                                    </div>
                                        <?php if(!$course['hasEditingTeacher']){?>
                                        
                                        <div style="padding-left: 36%">
                                        <a class="left-side" id="assign" href="<?php echo CRoute::_('index.php?option=com_community&view=friends&task=assignfriends&assignroles=true&courseid='.$course['id'].'&lpgroupid='.$group->id) ?>"><?php echo JText::_('COM_COMMUNITY_BUTTON_ASSIGN'); ?> <span class="arrow">+</span></a>
                                        </div>
                                        <?php }?>
                                        <?php if($course['is_mycourse']) {?>
                                            <div>
                                            <a class="right-side-swiper" id="sendapproval" data-id="<?php echo $course['id'] ?>" href="#"><?php echo JText::_('COM_COMMUNITY_SUBMIT'); ?></a>
                                            </div>
                                        <?php } ?>
                                    <?php } elseif ($pos !== false){ ?>
                                    <div>
                                    <a class="right-side-swiper" id="sendapproval" data-id="<?php echo $course['id'] ?>" href="#"><?php echo JText::_('COM_COMMUNITY_SEND_APPROVAL'); ?></a>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
    
                    </div>
                </div>   
            </div>
        </div>

        <?php if($isManager || $isAdmin || $isSuperAdmin){ ?>
        <div class="my-course have-courses" id="pending-approve-course">
            <div class="course-title-block">
                <h2><a href="#"><?php echo JText::_('COM_COMMUNITY_LP_WIP');?><span class=""> (<?php echo count($unapprovedcourse) ?>)</span></a>
                </h2>
            </div>
          
            <div class="course-content-block <?php echo $class; ?>">
                <div class="swiper-container swiper-container-horizontal swiper-container-free-mode">
                    <div class="swiper-wrapper" id="pending-course">
                        
                        <?php foreach ($unapprovedcourse as $course) { ?>
                        <div id="pending-course-<?php echo $course['id'] ?>" class="swiper-slide single_course swiper-slide-active" style="width: 497.2px; margin-right: 8px;">
                            <div class="title-course" style="width: 718px;">
                                <div class="course-image-wrapper" style="background-image: url('<?php echo $course['filepath'] . $course['filename'] ?>')">
                                </div>
                                <a class="course-title" href="<?php echo CRoute::_('index.php?option=com_joomdle&view=course&course_id='.$course['id']) ?>"><?php echo $course['fullname'] ?></a>
<!--                                <p class="course-timeend">Valid Until: <?php // echo $course['timeend'] ?></p>-->
                                <div class="courseDetails">
                                    <?php
                                    $userid = JUserHelper::getUserId($course['creator']);
                                    $user_owner = JFactory::getUser($userid); 
                                    if ($course['count_learner'] && $course['count_learner'] > 1) {
                                        echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $course['count_learner'] . ' ' . JText::_('COM_COMMUNITY_COURSE_SUBSCRIBERS') . '</span></br>';
                                    } else {
                                        echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $course['count_learner'] . ' ' . JText::_('COM_COMMUNITY_COURSE_SUBSCRIBER') . '</span></br>';
                                    }
                                    $time_view = '';
                                    if ($course['timepublish'] && $course['timepublish'] > 0) {
                                        $time_published = $course['timepublish']; 
                                        $date2 = JFactory::getDate($time_published);
                                        $time_view = CTimeHelper::timeLapseNew($date2, false);
                                    }
                                    if ($time_view != '') {
                                        echo number_format((float)$course['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                    } else {
                                        echo number_format((float)$course['duration'], 2, '.', '') . ' hr';
                                    }
                                    ?>
                                </div>
                                <p class="course-description"><?php echo mb_strimwidth($course['summary'], 0, 130, "..."); ?></p>
                        
                                <?php if($course['is_mycourse']) {?>
                                    <div>
                                    <a class="right-side-swiper" id="sendapproval" data-id="<?php echo $course['id'] ?>" href="#"><?php echo JText::_('COM_COMMUNITY_SUBMIT'); ?></a>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>

                        <?php } ?>  
                    </div>
                </div>   
            </div>
        </div>
        <?php } ?>

        <div class="my-course have-courses" id="pending-approve-course">
            <div class="course-title-block">
                <h2><a href="#"><?php echo JText::_('COM_COMMUNITY_LP_PENDING_APPROVAL');?><span class=""> (<?php echo count($pendingcourse) ?>)</span></a>
                </h2>
            </div>
          
            <div class="course-content-block <?php echo $class; ?>">
                <div class="swiper-container swiper-container-horizontal swiper-container-free-mode">
                    <div class="swiper-wrapper" id="pending-course">
                        
                        <?php foreach ($pendingcourse as $course) { ?>
                        <div id="pending-course-<?php echo $course['id'] ?>" class="swiper-slide single_course swiper-slide-active" style="width: 497.2px; margin-right: 8px;">
                            <div class="title-course" style="width: 718px;">
                                <div class="course-image-wrapper" style="background-image: url('<?php echo $course['filepath'] . $course['filename'] ?>')">
                                </div>
                                <a class="course-title" href="<?php echo CRoute::_('index.php?option=com_joomdle&view=course&course_id='.$course['id']) ?>"><?php echo $course['fullname'] ?></a>
                                <!--<p class="course-timeend">Valid Until: <?php // echo $course['timeend'] ?></p>-->
                                <div class="courseDetails">
                                    <?php
                                    $userid = JUserHelper::getUserId($course['creator']);
                                    $user_owner = JFactory::getUser($userid); 
                                    if ($course['count_learner'] && $course['count_learner'] > 1) {
                                        echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $course['count_learner'] . ' ' . JText::_('COM_COMMUNITY_COURSE_SUBSCRIBERS') . '</span></br>';
                                    } else {
                                        echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $course['count_learner'] . ' ' . JText::_('COM_COMMUNITY_COURSE_SUBSCRIBER') . '</span></br>';
                                    }
                                    $time_view = '';
                                    if ($course['timepublish'] && $course['timepublish'] > 0) {
                                        $time_published = $course['timepublish']; 
                                        $date2 = JFactory::getDate($time_published);
                                        $time_view = CTimeHelper::timeLapseNew($date2, false);
                                    }
                                    if ($time_view != '') {
                                        echo number_format((float)$course['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                    } else {
                                        echo number_format((float)$course['duration'], 2, '.', '') . ' hr';
                                    }
                                    ?>
                                </div>
                                <p class="course-description"><?php echo mb_strimwidth($course['summary'], 0, 130, "..."); ?></p>
                                <div>

                                    <?php if($isManager || $isAdmin || $isSuperAdmin){ ?>
                                    <div>
                                    <a class="left-side" id="unapprove" data-id="<?php echo $course['id'] ?>" href="#"><?php echo JText::_('COM_COMMUNITY_BUTTON_UNAPPROVE'); ?></a>
                                    </div>

                                    <div>
                                    <a class="right-side-swiper" id="approve" data-id="<?php echo $course['id'] ?>" href="#"><?php echo JText::_('COM_COMMUNITY_BUTTON_APPROVE'); ?> </a>
                                    </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                        <?php } ?>  
                    </div>
                </div>   
            </div>
        </div>

        <div class="my-course have-courses" id="approved-course">
            <div class="course-title-block">
                <h2><a href="#"><?php echo JText::_('COM_COMMUNITY_LP_APPROVED');?><span class=""> (<?php echo count($approvedcourse) ?>)</span></a>
                </h2>
            </div>
          
            <div class="course-content-block <?php echo $class; ?>">
                <div class="swiper-container swiper-container-horizontal swiper-container-free-mode">
                    <div class="swiper-wrapper">
                        
                        <?php foreach ($approvedcourse as $course) { ?>
                        <div id="approvedcourse-<?php echo $course['id'] ?>" class="swiper-slide single_course swiper-slide-active" style="width: 497.2px; margin-right: 8px;">
                            <div class="title-course" style="width: 718px;">
                                <div class="course-image-wrapper" style="background-image: url('<?php echo $course['filepath'] . $course['filename'] ?>')">
                                </div>
                                <a class="course-title" href="<?php echo CRoute::_('index.php?option=com_joomdle&view=course&course_id='.$course['id']) ?>"><?php echo $course['fullname'] ?></a>
                                <!--<p class="course-timeend">Valid Until: <?php // echo $course['timeend'] ?></p>-->
                                <div class="courseDetails">
                                    <?php
                                    $userid = JUserHelper::getUserId($course['creator']);
                                    $user_owner = JFactory::getUser($userid); 
                                    if ($course['count_learner'] && $course['count_learner'] > 1) {
                                        echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $course['count_learner'] . ' ' . JText::_('COM_COMMUNITY_COURSE_SUBSCRIBERS') . '</span></br>';
                                    } else {
                                        echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $course['count_learner'] . ' ' . JText::_('COM_COMMUNITY_COURSE_SUBSCRIBER') . '</span></br>';
                                    }
                                    $time_view = '';
                                    if ($course['timepublish'] && $course['timepublish'] > 0) {
                                        $time_published = $course['timepublish']; 
                                        $date2 = JFactory::getDate($time_published);
                                        $time_view = CTimeHelper::timeLapseNew($date2, false);
                                    }
                                    if ($time_view != '') {
                                        echo number_format((float)$course['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                    } else {
                                        echo number_format((float)$course['duration'], 2, '.', '') . ' hr';
                                    }
                                    ?>
                                </div>
                                <p class="course-description"><?php echo mb_strimwidth($course['summary'], 0, 130, "..."); ?></p>
                                <div>
                                    
                                    <?php if($isManager || $isAdmin || $isSuperAdmin){ ?>
                                    <div>
                                    <a class="right-side-swiper" id="publish" data-id="<?php echo $course['id'] ?>" href="<?php echo Juri::base().'mycourses/publish_'.$course['id'].'.html';//echo CRoute::_('index.php?option=com_community&view=groups&task=hikashopcategories&courseid='.$course['id'] . '&groupid=' . $group->id . '&lpcatid=' . $categoryid  ); ?>">
                                    <?php echo JText::_('COM_COMMUNITY_BUTTON_PUBLISH'); ?> </a>
                                    </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                        <?php } ?>

                    </div>
                </div>   
            </div>
        </div>
    


    </div>
</div>


<div id="lp-start" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" >
     <div class="modal-dialog modal-sm" style="width: 30%">
        <div class="modal-content">
            <span id="message"></span>
        <br/> <br/>
        
        <div style="padding-bottom: 20px">
        
        <div style="float: left">
        <a id="closemodal" style="width: 100%;" href="#" class="joms-focus__button--message">Close</a>
        </div>
        
        <div style="float: right" id="link">
        <a id="linkto" style="width: 100%;" href="#" class="joms-focus__button--message" data-id="0">Ok</a>
        </div>
        
        </div>
        </div>
    </div>
</div>

<style>
    .swiper-slide{
        width: 337.6px !important;
        margin-right: 12px !important;
    }
</style>

<?php 
if($device == 'mobile') {
    $slideview = '1';
    $space = '-8';

} else if($device == 'tablet') {
    $slideview = '2.5';
    $space = '8';

} else {
    $slideview = '3';
    $space = '8';
}
?>

<script type="text/javascript">
jQuery(document).ready(function(){
    var username = '<?php echo $username ?>';
    var categoryid = <?php echo $categoryid ?>;
    var courseid = 0;
    var action = '';
    var groupid = <?php echo $_GET['groupid'] ?>;

    var swiper = new Swiper('.my-course .swiper-container', {
        pagination: '.my-course .swiper-pagination',
        slidesPerView: <?php echo $slideview ;?>,
        nextButton: '.my-course .swiper-button-next',
        prevButton: '.my-course .swiper-button-prev',
        spaceBetween: <?php echo $space; ?>,
        freeMode: true,
        freeModeMomentum: true,
        preventClicks:false,
        preventClicksPropagation:false
    });
    
    (function ($) {  
        var deviceWidth = $(window).width();
        var newWidth = deviceWidth - 50;
        $('.home .my-course .course-content-block .swiper-slide .title-course').width(newWidth + 'px');
    })(jQuery);

    (function ($) {  

        jQuery('img').each(function() {
            jQuery(this).attr('src', jQuery(this).attr('kvsrc') );
        });
       
        /* Javascript for message popup - remove course*/

        jQuery('.title-course #remove').on('click',function(e){
            e.preventDefault();
            jQuery('#lp-start').modal('toggle');
            jQuery('#lp-start').modal('show');
            jQuery('#message').text("Are you sure you want to remove the course?");
            jQuery('#link .joms-focus__button--message').attr('id', 'linkto-remove');
            jQuery('#linkto-remove').attr("data-id", jQuery(this).data("id"));
            courseid = jQuery(this).data("id");
        });

        /* Javascript message popup - approve / unapprove course */
        
        jQuery('.title-course #unapprove, .title-course #approve').on('click',function(e){
            //e.stopImmediatePropagation();
            e.preventDefault();
            var clickedid = e.target.id; // approve and unapprove (action)
            var dataid = jQuery(this).data("id"); // course id
            var linkto = 'linkto-' + clickedid;
            var message = "Are you sure you want to " + clickedid + " the course?";
            jQuery('#lp-start').modal('toggle');
            jQuery('#lp-start').modal('show');
            
            jQuery('#message').text(message);
            jQuery('#link .joms-focus__button--message').attr('id', linkto);
            jQuery('#'+ linkto).attr("data-id", jQuery(this).data("id"));
            courseid = dataid; 
            action = clickedid;
        });

        /* Javascript for Approve / Unapprove course */

        jQuery('#link').on('click', '#linkto-approve, #linkto-unapprove', function(e){
              var pendingcourse =  'pending-course-'+ courseid;
              e.preventDefault();
              $.ajax({
                    type: "POST",
                    url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxUnApproveCourse&tmpl=component&format=json') ?>",
                    dataType: "json",
                    data: {username: username, courseid: courseid, categoryid: categoryid, action: action, groupid: groupid},
                    success: function(data){
                        jQuery('#lp-start').modal('hide');
                        location.reload();
                    }
              });
        });


        /* Javascript for Removing Course */
        
        jQuery('#link').on('click','#linkto-remove', function(e){
            var newcourse = '#newcourse-' + courseid;
            //e.stopImmediatePropagation();
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxRemoveCourse&tmpl=component&format=json') ?>",
                dataType: "json",
                data: {username: username, courseid: courseid, categoryid: categoryid},
                success: function(data){
                    jQuery('#lp-start').modal('hide');
                    jQuery(newcourse).fadeIn('slow').hide();
                }
            });
        });

        /* Javascript when click the close button */
        
        jQuery('#closemodal').click(function(e){
            e.preventDefault();
            jQuery('#lp-start').modal('toggle');
            jQuery('#lp-start').modal('hide');
        });

        /* Ajax for Send for approval*/
        jQuery('.title-course #sendapproval').on('click', function(e){
              courseid = jQuery(this).data("id");
              //e.stopImmediatePropagation();
              e.preventDefault();
              
              $.ajax({
                    type: "POST",
                    url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxSendApproval&tmpl=component&format=json') ?>",
                    dataType: "json",
                    data: {categoryid: categoryid, courseid: courseid, username: username, groupid: groupid},
                    success: function(data){
                        location.reload();
                    }
              });
              
        });

    })(jQuery);
});
</script>
