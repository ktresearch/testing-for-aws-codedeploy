<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();
$session = JFactory::getSession();
$device = $session->get('device');
?>


<?php
if( $posted )
{
	?>

	<!--<div class="joms-page" style="background: none; background-color: none">-->

		<?php echo $groupsHTML; ?>
	<!--</div>-->
<script type="text/javascript">
    var width = jQuery(window).width();
    jQuery(document).ready(function() {
        jQuery(".search-all-type-form").show();
        jQuery("#searchcircle").show();
        jQuery(".navbar-brand").hide();
        jQuery('.navbar-toggle').hide();
        jQuery('.navbar-notification').toggleClass('hide');
        jQuery('.navbar-search-all').toggleClass('hide');
        jQuery(".navbar-menu-icon").hide();
        jQuery('#searchcircle .search-text').val('<?php echo $search;?>');
        if (width >= 930 && width <= 1005) {
            jQuery(".topmenu-bar .navbar-nav li").last().hide();
        }
        if (width >= 830 && width < 930) {
            jQuery(".topmenu-bar .navbar-nav li").last().hide();
            jQuery(".topmenu-bar .navbar-nav li:nth-last-child(2)").last().hide();
        }
        if (width >= 754 && width < 830) {
            jQuery(".topmenu-bar .navbar-nav li").last().hide();
            jQuery(".topmenu-bar .navbar-nav li:nth-last-child(2)").last().hide();
            jQuery(".topmenu-bar .navbar-nav li:nth-last-child(3)").last().hide();
        }
    });
</script>
	<?php
}
?>
