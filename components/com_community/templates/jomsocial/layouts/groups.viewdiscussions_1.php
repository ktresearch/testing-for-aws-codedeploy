<?php
    /**
     * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
     * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
     * @author iJoomla.com <webmaster@ijoomla.com>
     * @url https://www.jomsocial.com/license-agreement
     * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
     * More info at https://www.jomsocial.com/license-agreement
     */
    defined('_JEXEC') or die();
    $config    = CFactory::getConfig();
   $createLink_event = CRoute::_('index.php?option=com_community&view=events&groupid='.$group->id.'&task=create');
   $createLink_topic = CRoute::_('index.php?option=com_community&view=groups&groupid='.$group->id.'&task=adddiscussion');
   ?>
<div class="viewtopic">
   <div class="joms-page">
        <div id="joms-group--details" class="joms-tab__content" style="">
            <div class="joms-group--button">
                <?php if ($isAdmin && $config->get('createannouncement',0)) { ?>
                <button class="joms-button--add js-btn-add-announcement" onclick="window.location='<?php echo CRoute::_('index.php?option=com_community&view=groups&task=addnews&groupid=' . $group->id); ?>';">
                        <?php echo JText::_('COM_COMMUNITY_GROUPS_BULLETIN_CREATE'); ?>         
                </button>
                <?php } ?>
                <?php if($isMember) { ?>
                <button onclick="window.location='<?php echo $createLink_topic; ?>';" class="joms-button--add js-btn-add-topic">
                <?php echo JText::_('COM_COMMUNITY_MEMBERS_CREATE_TOPIC'); ?>         
                </button>
                    <button onclick="window.location='<?php echo $createLink_event; ?>';" class="joms-button--add js-btn-add-event">
                    <?php echo JText::_('COM_COMMUNITY_MEMBERS_CREATE_EVENT') ?>         
                </button>
                <?php } ?>
            </div>
            <div class="list_topic">
            <?php   foreach ($discussions as $discussion) {
                        if ($config->get('activitydateformat') == COMMUNITY_DATE_FIXED) {
                            $discussion->created = CTimeHelper::getDate($discussion->created)->format(JText::_('DATE_FORMAT_LC2'), true);
                        } else {
                            $discussion->created = CTimeHelper::timeLapse(CTimeHelper::getDate($discussion->created));
                        }
                        $creator = CFactory::getUser($discussion->creator);

            ?>    
                <div class="topic-member">
                    <p class="create"><?php echo $discussion->created?></p>
                    <div class="joms-avatar--comment <?php echo CUserHelper::onlineIndicator($creator); ?> ">
                        <a href="<?php echo CUrlHelper::userLink($creator->id); ?>">
                            <img src="<?php echo $creator->getThumbAvatar(); ?>" alt="<?php echo $creator->getDisplayName(); ?>" data-author="<?php echo $creator->id; ?>" />
                        </a>
                    </div>
                    <div class="conten-right">
                        <a href=""><?php echo $discussion->title?></a>
                        <span class="js-topic-icon dropdown-toggle" onclick="showdropdownmenu(<?php echo $discussion->id;?>); return false;"></span>
                        <ul class="dropdown-menu dropdown-menu-up" id="dropdown-menu-<?php echo $discussion->id;?>">
                            <!-- if user already is group admin than we don't show this option -->
                            <?php // if (!$member->isAdmin) { ?>
                                <li class="setAdmin">
                                    <a href="javascript:void(0);" onclick="">
                                        <?php echo JText::_('COM_COMMUNITY_TOPIC_EDIT_PHOTO'); ?>
                                    </a>
                                </li>
                            <?php // } ?>
                            <!-- not self revert and require group admin or superadmin and this's admin -->
                            <?php // if (((!$member->isMe) && $isAdmin && !$isSuperAdmin && !$isMoodleSiteAdmin && ($member->isAdmin) && !$member->isOwner) || ($member->isAdmin && ($isSuperAdmin ||$isMoodleSiteAdmin))) { ?>
                                <li class="setAdmin">
                                    <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=editdiscussion&groupid='.$group->id.'&topicid='.$discussion->id); ?>">
                                        <?php echo JText::_('COM_COMMUNITY_TOPIC_EDIT'); ?>
                                    </a>
                                </li>
                            <?php // }  ?>
                            <?php
//                            if ($member->id != $group->ownerid && !$group->isAdmin($member->id) && $my->id != $member->id) {

                                if (!$discussion->lock) {
//                                    $urlbanlist = CRoute::_('index.php?option=com_community&view=groups&task=banlist&list=' . COMMUNITY_GROUP_BANNED . '&groupid=' . $group->id);
                                    ?>
                                    <li>
                                        <a href="javascript: void(0);" onclick="joms.api.discussionLock('<?php echo $group->id ?>', '<?php echo $discussion->id ?>');">
                                            <?php echo JText::_('COM_COMMUNITY_LOCK_TOPIC'); ?>
                                        </a>
                                    </li>
                                    <?php
                                } else {
                                    ?>
                                    <li>
                                        <a href="javascript: void(0);" onclick="joms.api.discussionLock('<?php echo $group->id ?>', '<?php echo $discussion->id ?>');">
                                            <?php echo JText::_('COM_COMMUNITY_UNLOCK_TOPIC'); ?>
                                        </a>
                                    </li>
                                    <?php
                                }
//                            }
                            ?>
                            <?php
//                            if (($isMine || $isAdmin || $isSuperAdmin || $isMoodleSiteAdmin) && $my->id != $member->id && !$member->isOwner) {
                                ?>
                                <li class="hasSeperator">
                                    <a href="javascript: void(0);" onclick="joms.api.discussionRemove('<?php echo $group->id ?>', '<?php echo $discussion->id ?>');">
                                        <?php echo JText::_('COM_COMMUNITY_TOPIC_DELETE'); ?>
                                    </a>
                                </li>
                                <?php
//                            }
                            ?>
                        </ul>
                        <p><?php echo $discussion->message?></p>   
                    </div>
                </div>

            <?php } ?>
            </div>
        </div>
    </div>
</div>
<style>
body{
    background:  #e1e1e1;
}

 .arrow-down {
    left: 49%;
}
</style>
<script src="components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        var title_p = '<?php echo JText::_('COM_COMMUNITY_TITLE_CIRCLE_TOPIC')?>';
        $('.navbar-header .navbar-title span').html(title_p);    
    });
    
    function showdropdownmenu(id) {
        $('#dropdown-menu-'+id).fadeToggle(200);
    }

    jQuery(document).click(function(e) {
          var target = e.target;
          if (!$(target).is('.dropdown-toggle') && !$(target).parents().is('.dropdown-toggle')) {
            $('.dropdown-menu').hide();
          }
    });
    
</script>  
