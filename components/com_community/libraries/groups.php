<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */

defined('_JEXEC') or die('Restricted access');

require_once( JPATH_ROOT .'/components/com_community/libraries/core.php' );
//CFactory::load( 'libraries' , 'comment' );

class CGroups implements
    CCommentInterface, CStreamable
{
    static public function getActivityTitleHTML($act)
    {
        return "GROUP";
    }

    static public function getActivityContentHTML($act)
    {
        // Ok, the activity could be an upload OR a wall comment. In the future, the content should
        // indicate which is which
        $html = '';
        $param = new CParameter( $act->params );
        $action = $param->get('action' , false);

        $config = CFactory::getConfig();

        $groupModel		= CFactory::getModel( 'groups' );

        if( $action == CGroupsAction::DISCUSSION_CREATE )
        {
            // Old discussion might not have 'action', and we can't display their
            // discussion summary
            $topicId = $param->get('topic_id', false);
            if( $topicId ){

                $group			= JTable::getInstance( 'Group' , 'CTable' );
                $discussion		= JTable::getInstance( 'Discussion' , 'CTable' );

                $group->load( $act->cid );
                $discussion->load( $topicId );

                $discussion->message = strip_tags($discussion->message);
                $topic = CStringHelper::escape($discussion->message);
                $tmpl	= new CTemplate();
                $tmpl->set( 'comment' , JString::substr($topic, 0, $config->getInt('streamcontentlength')) );
                $html	= $tmpl->fetch( 'activity.groups.discussion.create' );
            }
            return $html;
        }
        else if ($action == CGroupsAction::WALLPOST_CREATE )
        {
            // a new wall post for group
            // @since 1.8
            $group	= JTable::getInstance( 'Group' , 'CTable' );
            $group->load( $act->cid );

            $wallModel	= CFactory::getModel( 'Wall' );
            $wall		= JTable::getInstance( 'Wall' , 'CTable' );
            $my			= CFactory::getUser();

            // make sure the group is a public group or current use is
            // a member of the group
            if( ($group->approvals == 0) || $group->isMember($my->id))
            {
                //CFactory::load( 'libraries' , 'comment' );
                $wall->load( $param->get('wallid' ));
                $comment	= strip_tags( $wall->comment , '<comment>');
                $comment	= CComment::stripCommentData( $comment );
                $tmpl	= new CTemplate();
                $tmpl->set( 'comment' , JString::substr($comment, 0, $config->getInt('streamcontentlength')) );
                $html	= $tmpl->fetch( 'activity.groups.wall.create' );
            }
            return $html;
        }
        else if($action == CGroupsAction::DISCUSSION_REPLY)
        {
            // @since 1.8
            $group	= JTable::getInstance( 'Group' , 'CTable' );
            $group->load( $act->cid );

            $wallModel	= CFactory::getModel( 'Wall' );
            $wall		= JTable::getInstance( 'Wall' , 'CTable' );
            $my			= CFactory::getUser();

            // make sure the group is a public group or current use is
            // a member of the group
            if( ($group->approvals == 0) || $group->isMember($my->id))
            {
                $wallid = $param->get('wallid' );
                //CFactory::load( 'libraries' , 'wall' );
                $html = CWallLibrary::getWallContentSummary($wallid);
            }
            return $html;
        }
        else if ($action == CGroupsAction::CREATE)
        {
            $group	= JTable::getInstance( 'Group' , 'CTable' );
            $group->load( $act->cid );

            $tmpl	= new CTemplate();
            $tmpl->set( 'group' , $group );
            $html	= $tmpl->fetch( 'activity.groups.create' );
        }


        return $html;
    }

    /**
     * Return an array of valid 'app' code to fetch from the stream
     * @return array
     */
    static public function getStreamAppCode(){
        return array('groups.wall', 'groups.attend', 'events.wall', 'videos',
            'groups.discussion', 'groups.discussion.reply', 'groups.bulletin',
            'photos', 'events');
    }


    static public function sendCommentNotification( CTableWall $wall , $message )
    {
        //CFactory::load( 'libraries' , 'notification' );

        $my			= CFactory::getUser();
        $targetUser	= CFactory::getUser( $wall->post_by );
        $url		= 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $wall->contentid;
        $params 	= $targetUser->getParams();

        $params		= new CParameter( '' );
        $params->set( 'url' , $url );
        $params->set( 'message' , $message );

        CNotificationLibrary::add( 'groups_submit_wall_comment' , $my->id , $targetUser->id , JText::sprintf('PLG_WALLS_WALL_COMMENT_EMAIL_SUBJECT' , $my->getDisplayName() ) , '' , 'groups.wallcomment' , $params );

        return true;
    }

    /**
     *
     */
    static public function joinApproved($groupId, $userid)
    {
        $group		= JTable::getInstance( 'Group' , 'CTable' );
        $member		= JTable::getInstance( 'GroupMembers' , 'CTable' );

        $group->load( $groupId );

        $act = new stdClass();
        $act->cmd 		= 'group.join';
        $act->actor   	= $userid;
        $act->target  	= 0;
        $act->title	  	= '';//JText::sprintf('COM_COMMUNITY_GROUPS_GROUP_JOIN' , '{group_url}' , $group->name );
        $act->content	= '';
        $act->app		= 'groups.join';
        $act->cid		= $group->id;
        $act->groupid	= $group->id;

        $params = new CParameter('');
        $params->set( 'group_url' , 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id );
        $params->set( 'action', 'group.join');

        // Add logging
        if(CUserPoints::assignPoint('group.join')){
            CActivityStream::addActor($act, $params->toString() );
        }

        // Store the group and update stats
        $group->updateStats();
        $group->store();
    }


    /**
     * Return HTML formatted stream for groups
     * @param object $group
     * @deprecated use activities library instead
     */
    public function getStreamHTML( $group, $filters = array())
    {

        $activities = new CActivities();
        $streamHTML = $activities->getOlderStream(1000000000, 'active-group', $group->id, null, $filters);

        // $streamHTML = $activities->getAppHTML(
        // 			array(
        // 				'app' => CActivities::APP_GROUPS,
        // 				'groupid' => $group->id,
        // 				'apptype' => 'group'
        // 			)
        // 		);

        return $streamHTML;
    }

    /**
     * Return true is the user can post to the stream
     **/
    public function isAllowStreamPost( $userid, $option )
    {
        // Guest cannot post.
        if( $userid == 0){
            return false;
        }

        // Admin can comment on any post
        if(COwnerHelper::isCommunityAdmin()){
            return true;
        }

        // if the groupid not specified, obviously stream comment is not allowed
        if(empty($option['groupid'])){
            return false;
        }

        $group	= JTable::getInstance( 'Group' , 'CTable' );
        $group->load( $option['groupid'] );
        return $group->isMember($userid);
    }
    /**
     * Return true is the user is a group admin
     **/
    public function isAdmin($userid,$groupid)
    {
        $group	= JTable::getInstance( 'Group' , 'CTable' );
        $group->load( $groupid );
        return $group->isAdmin($userid);
    }

    public function setAvatarPhoto($file, $id, $type)
    {
        $params = new JRegistry();
        $mainframe = JFactory::getApplication();

        $cTable = JTable::getInstance(ucfirst($type), 'CTable');
        $cTable->load($id);

        if($type=="profile"){
            $my = CFactory::getUser($id);
        }else{
            $my = CFactory::getUser();
        }

        $config = CFactory::getConfig();
        $userid = $my->id;

        CImageHelper::autoRotate($file['tmp_name']);

        $album = JTable::getInstance('Album', 'CTable');

        //create the avatar default album if it does not exists
        if (!$albumId = $album->isAvatarAlbumExists($id, $type)) {
            $albumId = $album->addAvatarAlbum($id, $type);
        }

        //start image processing
        // Get a hash for the file name.
        $fileName = JApplication::getHash($file['tmp_name'] . time());
        $hashFileName = JString::substr($fileName, 0, 24);
        $avatarFolder = ($type != 'profile' && $type != '') ? $type . '/' : '';

        //avatar store path
        $storage = $mainframe->getCfg('dataroot') . '/' . $config->getString('imagefolder') . '/avatar' . '/' . $avatarFolder;
        if (!JFolder::exists($storage)) {
            JFolder::create($storage);
        }
        $storageImage = $storage . '/' . $hashFileName . CImageHelper::getExtension($file['type']);
        $image = $config->getString(
                'imagefolder'
            ) . '/avatar/' . $avatarFolder . $hashFileName . CImageHelper::getExtension($file['type']);

        /**
         * reverse image use for cropping feature
         * @uses <type>-<hashFileName>.<ext>
         */
        $storageReserve = $storage . '/' . $type . '-' . $hashFileName . CImageHelper::getExtension($file['type']);

        // filename for stream attachment
        $imageAttachment = $config->getString(
                'imagefolder'
            ) . '/avatar/' . $hashFileName . '_stream_' . CImageHelper::getExtension($file['type']);

        //avatar thumbnail path
        $storageThumbnail = $storage . '/thumb_' . $hashFileName . CImageHelper::getExtension($file['type']);
        $thumbnail = $config->getString(
                'imagefolder'
            ) . '/avatar/' . $avatarFolder . 'thumb_' . $hashFileName . CImageHelper::getExtension($file['type']);

        //Minimum height/width checking for Avatar uploads
        list($currentWidth, $currentHeight) = getimagesize($file['tmp_name']);
//        if($type != "discussion") {
//            if ($currentWidth < COMMUNITY_AVATAR_PROFILE_WIDTH || $currentHeight < COMMUNITY_AVATAR_PROFILE_HEIGHT) {
//                $msg['error'] = JText::sprintf(
//                    'COM_COMMUNITY_ERROR_MINIMUM_AVATAR_DIMENSION',
//                    COMMUNITY_AVATAR_PROFILE_WIDTH,
//                    COMMUNITY_AVATAR_PROFILE_HEIGHT
//                );
//                return;
//            }
//        }

        /**
         * Generate square avatar
         */
        if (!CImageHelper::createThumb(
            $file['tmp_name'],
            $storageImage,
            $file['type'],
            COMMUNITY_AVATAR_PROFILE_WIDTH,
            COMMUNITY_AVATAR_PROFILE_HEIGHT
        )
        ) {
            $msg['error'] = JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $storageImage);
            return;
        }

        // Generate thumbnail
        if (!CImageHelper::createThumb($file['tmp_name'], $storageThumbnail, $file['type'],COMMUNITY_SMALL_AVATAR_WIDTH,COMMUNITY_SMALL_AVATAR_WIDTH)) {
            $msg['error'] = JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $storageImage);
            return;
        }

        /**
         * Generate large image use for avatar thumb cropping
         * It must be larget than profile avatar size because we'll use it for profile avatar recrop also
         */
        $newWidth = 0;
        $newHeight = 0;
        if ($currentWidth >= $currentHeight) {
            if (CGroups::testResize(
                $currentWidth,
                $currentHeight,
                COMMUNITY_AVATAR_RESERVE_WIDTH,
                0,
                COMMUNITY_AVATAR_PROFILE_WIDTH,
                COMMUNITY_AVATAR_RESERVE_WIDTH
            )
            ) {
                $newWidth = COMMUNITY_AVATAR_RESERVE_WIDTH;
                $newHeight = 0;
            } else {
                $newWidth = 0;
                $newHeight = COMMUNITY_AVATAR_RESERVE_HEIGHT;
            }
        } else {
            if (CGroups::testResize(
                $currentWidth,
                $currentHeight,
                0,
                COMMUNITY_AVATAR_RESERVE_HEIGHT,
                COMMUNITY_AVATAR_PROFILE_HEIGHT,
                COMMUNITY_AVATAR_RESERVE_HEIGHT
            )
            ) {
                $newWidth = 0;
                $newHeight = COMMUNITY_AVATAR_RESERVE_HEIGHT;
            } else {
                $newWidth = COMMUNITY_AVATAR_RESERVE_WIDTH;
                $newHeight = 0;
            }
        }

        if (!CImageHelper::resizeProportional(
            $file['tmp_name'],
            $storageReserve,
            $file['type'],
            $newWidth,
            $newHeight
        )
        ) {
            $msg['error'] = JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $storageReserve);
            return;
        }

        /*
     * Generate photo to be stored in default avatar album
     * notes: just in case this need to be used in registration, just get the code below.
     */
        $originalName = 'original_' . md5($my->id . '_avatar' . time()) . CImageHelper::getExtension(
                $file['type']
            );
        $originalPath = $storage . $originalName;
        $fullImagePath = $storage . md5($my->id . '_avatar' . time()) . CImageHelper::getExtension($file['type']);
        $thumbPath = $storage . 'thumb_' . md5($my->id . '_avatar' . time()) . CImageHelper::getExtension(
                $file['type']
            );

        // Generate full image
        if (!CImageHelper::resizeProportional($file['tmp_name'], $fullImagePath, $file['type'], 1024)) {
            $msg['error'] = JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $file['tmp_name']);
            echo json_encode($msg);
            exit;
        }

        CPhotos::generateThumbnail($file['tmp_name'], $thumbPath, $file['type']);

        if (!JFile::copy($file['tmp_name'], $originalPath)) {
            exit;
        }

        //store this picture into default avatar album
        $now = new JDate();
        $photo = JTable::getInstance('Photo', 'CTable');

        $photo->albumid = $albumId;
//            $photo->image = str_replace(JPATH_ROOT . '/', '', $fullImagePath);
        $photo->image = str_replace($mainframe->getCfg('dataroot') . '/', '', $fullImagePath);
        $photo->caption = $file['name'];
        $photo->filesize = $file['size'];
        $photo->creator = $my->id;
        $photo->created = $now->toSql();
        $photo->published = 1;
//            $photo->thumbnail = str_replace(JPATH_ROOT . '/', '', $thumbPath);
//            $photo->original = str_replace(JPATH_ROOT . '/', '', $originalPath);
        $photo->thumbnail = str_replace($mainframe->getCfg('dataroot') . '/', '', $thumbPath);
        $photo->original = str_replace($mainframe->getCfg('dataroot') . '/', '', $originalPath);

        if ($photo->store()) {
            $album->load($albumId);
            $album->photoid = $photo->id;
            $album->setParam('thumbnail', $photo->thumbnail);
            $album->store();
        }

        //end storing user avatar in avatar album

        if ($type == 'profile') {
            $profileType = $my->getProfileType();
            $multiprofile = JTable::getInstance('MultiProfile', 'CTable');
            $multiprofile->load($profileType);

            $useWatermark = $profileType != COMMUNITY_DEFAULT_PROFILE && $config->get(
                'profile_multiprofile'
            ) && !empty($multiprofile->watermark) ? true : false;

            if ($useWatermark && $multiprofile->watermark) {
                JFile::copy(
                    $storageImage,
                    $mainframe->getCfg('dataroot') . '/images/watermarks/original' . '/' . md5(
                        $my->id . '_avatar'
                    ) . CImageHelper::getExtension($file['type'])
                );
                JFile::copy(
                    $storageThumbnail,
                    $mainframe->getCfg('dataroot') . '/images/watermarks/original' . '/' . md5(
                        $my->id . '_thumb'
                    ) . CImageHelper::getExtension($file['type'])
                );

//                    $watermarkPath = JPATH_ROOT . '/' . CString::str_ireplace('/', '/', $multiprofile->watermark);
                $watermarkPath = $mainframe->getCfg('dataroot') . '/' . CString::str_ireplace('/', '/', $multiprofile->watermark);

                list($watermarkWidth, $watermarkHeight) = getimagesize($watermarkPath);
                list($avatarWidth, $avatarHeight) = getimagesize($storageImage);
                list($thumbWidth, $thumbHeight) = getimagesize($storageThumbnail);

                $watermarkImage = $storageImage;
                $watermarkThumbnail = $storageThumbnail;

                // Avatar Properties
                $avatarPosition = CImageHelper::getPositions(
                    $multiprofile->watermark_location,
                    $avatarWidth,
                    $avatarHeight,
                    $watermarkWidth,
                    $watermarkHeight
                );

                // The original image file will be removed from the system once it generates a new watermark image.
                CImageHelper::addWatermark(
                    $storageImage,
                    $watermarkImage,
                    $file['type'],
                    $watermarkPath,
                    $avatarPosition->x,
                    $avatarPosition->y
                );

                //Thumbnail Properties
                $thumbPosition = CImageHelper::getPositions(
                    $multiprofile->watermark_location,
                    $thumbWidth,
                    $thumbHeight,
                    $watermarkWidth,
                    $watermarkHeight
                );

                // The original thumbnail file will be removed from the system once it generates a new watermark image.
                CImageHelper::addWatermark(
                    $storageThumbnail,
                    $watermarkThumbnail,
                    $file['type'],
                    $watermarkPath,
                    $thumbPosition->x,
                    $thumbPosition->y
                );

                $my->set('_watermark_hash', $multiprofile->watermark_hash);
            }

            // We need to make a copy of current avatar and set it as stream 'attachement'
            // which will only gets deleted once teh stream is deleted

            $my->_cparams->set('avatar_photo_id', $photo->id); //we also set the id of the avatar photo

            $my->save();

            JFile::copy($image, $imageAttachment);
            $params->set('attachment', $imageAttachment);
        }

        //end of storing this picture into default avatar album

        if (empty($saveAction)) {
            $cTable->setImage($image, 'avatar');
            $cTable->setImage($thumbnail, 'thumb');
        } else {
            // This is for event recurring save option ( current / future event )
            $cTable->setImage($image, 'avatar', $saveAction);
            $cTable->setImage($thumbnail, 'thumb', $saveAction);
        }

        // add points & activity stream
        $generateStream = true; //only set to false if any of this type doesnt give points
        switch ($type) {
            case 'profile':

                /**
                 * Generate activity stream
                 * @todo Should we use CApiActivities::add
                 */
                // do not have to generate a stream if the user is not the user itself (eg admin change user avatar)
                if(CUserPoints::assignPoint('profile.avatar.upload') && $my->id == CFactory::getUser()->id){
                    $act = new stdClass();
                    $act->cmd = 'profile.avatar.upload';
                    $act->actor = $userid;
                    $act->target = 0;
                    $act->title = '';
                    $act->content = '';
                    //$act->access = $my->_cparams->get("privacyPhotoView", 0);

                    // When changing profile avatar - set to friends only (30)
                    $act->access = 30;

                    $act->app = 'profile.avatar.upload'; /* Profile app */
                    $act->cid = (isset($photo->id) && $photo->id) ? $photo->id : 0 ;
                    $act->verb = 'upload'; /* We uploaded new avatar - NOT change avatar */
                    $act->params = $params;
                    $params->set('photo_id', $photo->id);
                    $params->set('album_id', $photo->albumid);
                    $act->comment_id = CActivities::COMMENT_SELF;
                    $act->comment_type = 'profile.avatar.upload';
                    $act->like_id = CActivities::LIKE_SELF;
                    $act->like_type = 'profile.avatar.upload';
                }else{
                    $generateStream = false;
                }
                break;

            case 'group':
                /**
                 * Generate activity stream
                 * @todo Should we use CApiActivities::add
                 */

                $groupModel = CFactory::getModel('Groups');
                $group = JTable::getInstance('Group', 'CTable');
                $group->load($id);
                $groupModel->setImage($group->id, $originalName, 'origimage');

                if(CUserPoints::assignPoint('group.avatar.upload')){
                    $act = new stdClass();
                    $act->cmd = 'groups.avatar.upload';
                    $act->actor = $userid;
                    $act->target = 0;
                    $act->title = '';
                    $act->content = '';
                    $act->app = 'groups.avatar.upload'; /* Groups app */
                    $act->cid = $id;
                    $act->groupid = $id;
                    $act->verb = 'update'; /* We do update */
                    $params->set('photo_id', $photo->id);
                    $params->set('album_id', $photo->albumid);

                    $act->comment_id = CActivities::COMMENT_SELF;
                    $act->comment_type = 'groups.avatar.upload';
                    $act->like_id = CActivities::LIKE_SELF;
                    $act->like_type = 'groups.avatar.upload';
                    $generateStream = true;
                }else{
                    $generateStream = false;
                }

                break;

            case 'event':
                //CUserPoints::assignPoint('events.avatar.upload'); @disabled since 4.0
                /**
                 * Generate activity stream
                 * @todo Should we use CApiActivities::add
                 */
                $act = new stdClass();
                $act->cmd = 'events.avatar.upload';
                $act->actor = $userid;
                $act->target = 0;
                $act->title = '';
                $act->content = '';
                $act->app = 'events.avatar.upload'; /* Events app */
                $act->cid = $id;
                $act->eventid = $id;
                $act->verb = 'update'; /* We do update */

                $act->comment_id = CActivities::COMMENT_SELF;
                $act->comment_type = 'events.avatar.upload';
                $act->like_id = CActivities::LIKE_SELF;
                $act->like_type = 'events.avatar.upload';


                break;
            case 'discussion':
                //CUserPoints::assignPoint('events.avatar.upload'); @disabled since 4.0
                /**
                 * Generate activity stream
                 * @todo Should we use CApiActivities::add
                 */
                $act = new stdClass();
                $act->cmd = 'discussion.avatar.upload';
                $act->actor = $userid;
                $act->target = 0;
                $act->title = '';
                $act->content = '';
                $act->app = 'discussion.avatar.upload'; /* Events app */
                $act->cid = $id;
                $act->eventid = $id;
                $act->verb = 'update'; /* We do update */

                $act->comment_id = CActivities::COMMENT_SELF;
                $act->comment_type = 'discussion.avatar.upload';
                $act->like_id = CActivities::LIKE_SELF;
                $act->like_type = 'discussion.avatar.upload';

                $generateStream = false;

                break;
        }

        //we only generate stream if the uploader is the user himself, not admin or anyone else
        if ( ((isset($act) && $my->id == $id) || $type != 'profile') && $generateStream) {
            // $return = CApiActivities::add($act);

            /**
             * use internal Stream instead use for 3rd part API
             */
            $return = CActivityStream::add($act, $params->toString());

            //add the reference to the activity so that we can do something when someone update the avatar
            if($type == 'profile'){
                // overwrite the params because some of the param might be updated through $my object above
                $cTableParams = $my->_cparams;
            }else{
                $cTableParams = new JRegistry($cTable->params);
            }

            $cTableParams->set('avatar_activity_id',$return->id);

            $cTable->params = $cTableParams->toString();
            $cTable->store();
        }


        if($type == 'group'){
            // Added by NYI
            // LP Circle Info Update
            require_once(JPATH_SITE.'/components/com_hikashop/helpers/lpapi.php');
            // NYI API - update group information in Hikashop Category
            $categories = LpApiController::updatecategory($id);
        }

    }

    public function setCoverPhoto($file, $parentId, $type)
    {
        $mainframe = JFactory::getApplication();
        $data_root = $mainframe->getCfg('dataroot');
        $config = CFactory::getConfig();
        $my = CFactory::getUser();
        $now = new JDate();

        if (!CImageHelper::checkImageSize(filesize($file['tmp_name']))) {
            $msg['error'] = JText::sprintf('COM_COMMUNITY_VIDEOS_IMAGE_FILE_SIZE_EXCEEDED_MB',CFactory::getConfig()->get('maxuploadsize'));
            echo json_encode($msg);
            exit;
        }

        //check if file is allwoed
        if (!CImageHelper::isValidType($file['type'])) {
            $msg['error'] = JText::_('COM_COMMUNITY_IMAGE_FILE_NOT_SUPPORTED');
            echo json_encode($msg);
            exit;
        }

        CImageHelper::autoRotate($file['tmp_name']);

        $album = JTable::getInstance('Album', 'CTable');

        if (!$albumId = $album->isCoverExist($type, $parentId)) {
            $albumId = $album->addCoverAlbum($type, $parentId);
        }

        $imgMaxWidht = 1140;

        // Get a hash for the file name.
        $fileName = JApplication::getHash($file['tmp_name'] . time());
        $hashFileName = JString::substr($fileName, 0, 24);

//            if (!JFolder::exists(
//                JPATH_ROOT . '/' . $config->getString('imagefolder') . '/cover/' . $type . '/' . $parentId . '/'
//            )
//            ) {
//                JFolder::create(
//                    JPATH_ROOT . '/' . $config->getString('imagefolder') . '/cover/' . $type . '/' . $parentId . '/'
//                );
//            }
        if (!JFolder::exists(
            $data_root . '/' . $config->getString('imagefolder') . '/cover/' . $type . '/' . $parentId . '/'
        )
        ) {
            JFolder::create(
                $data_root . '/' . $config->getString('imagefolder') . '/cover/' . $type . '/' . $parentId . '/'
            );
        }

//            $dest = JPATH_ROOT . '/' . $config->getString('imagefolder') . '/cover/' . $type . '/' . $parentId . '/' . md5(
//                    $type . '_cover' . time()
//                ) . CImageHelper::getExtension($file['type']);
        $dest = $data_root . '/' . $config->getString('imagefolder') . '/cover/' . $type . '/' . $parentId . '/' . md5(
                $type . '_cover' . time()
            ) . CImageHelper::getExtension($file['type']);
//            $thumbPath = JPATH_ROOT . '/' . $config->getString(
//                    'imagefolder'
//                ) . '/cover/' . $type . '/' . $parentId . '/thumb_' . md5(
//                    $type . '_cover' . time()
//                ) . CImageHelper::getExtension($file['type']);
        $thumbPath = $data_root . '/' . $config->getString(
                'imagefolder'
            ) . '/cover/' . $type . '/' . $parentId . '/thumb_' . md5(
                $type . '_cover' . time()
            ) . CImageHelper::getExtension($file['type']);
        // Generate full image
        if (!CImageHelper::resizeProportional($file['tmp_name'], $dest, $file['type'], $imgMaxWidht)) {
            $msg['error'] = JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $storageImage);
            echo json_encode($msg);
            exit;
        }

        CPhotos::generateThumbnail($file['tmp_name'], $thumbPath, $file['type']);

        $cTable = JTable::getInstance(ucfirst($type), 'CTable');
        $cTable->load($parentId);

//            if ($cTable->setCover(str_replace(JPATH_ROOT . '/', '', $dest))) {
        if ($cTable->setCover(str_replace($data_root . '/', '', $dest))) {
            $photo = JTable::getInstance('Photo', 'CTable');

            $photo->albumid = $albumId;
//                $photo->image = str_replace(JPATH_ROOT . '/', '', $dest);
            $photo->image = str_replace($data_root . '/', '', $dest);
            $photo->caption = $file['name'];
            $photo->filesize = $file['size'];
            $photo->creator = $my->id;
            $photo->created = $now->toSql();
            $photo->published = 1;
//                $photo->thumbnail = str_replace(JPATH_ROOT . '/', '', $thumbPath);
            $photo->thumbnail = str_replace($data_root . '/', '', $thumbPath);

            if ($photo->store()) {
                $album->load($albumId);
                $album->photoid = $photo->id;
                $album->store();
            }

            $msg['success'] = true;
//                $msg['path'] = JURI::root() . str_replace(JPATH_ROOT . '/', '', $dest);
            $msg['path'] = $mainframe->getCfg('wwwrootfile') . '/' . str_replace($data_root . '/', '', $dest);
            $msg['cancelbutton'] = JText::_('COM_COMMUNITY_CANCEL_BUTTON');
            $msg['savebutton'] = JText::_("COM_COMMUNITY_SAVE_BUTTON");

            // Generate activity stream.
            $act = new stdClass();
            $act->cmd = 'cover.upload';
            $act->actor = $my->id;
            $act->target = 0;
            $act->title = '';
            $act->content = '';
            $act->access = ($type == 'profile') ? $my->_cparams->get("privacyPhotoView") : 0;
            $act->app = 'cover.upload';
            $act->cid = $photo->id;
            $act->comment_id = CActivities::COMMENT_SELF;
            $act->comment_type = 'cover.upload';
            $act->groupid = ($type == 'group') ? $parentId : 0;
            $act->eventid = ($type == 'event') ? $parentId : 0;
            $act->group_access = ($type == 'group') ? $cTable->approvals : 0;
            $act->event_access = ($type == 'event') ? $cTable->permission : 0;
            $act->like_id = CActivities::LIKE_SELF;;
            $act->like_type = 'cover.upload';

            $params = new JRegistry();
//                $params->set('attachment', str_replace(JPATH_ROOT . '/', '', $dest));
            $params->set('attachment', str_replace($data_root . '/', '', $dest));
            $params->set('type', $type);
            $params->set('album_id', $albumId);
            $params->set('photo_id', $photo->id);

            //assign points based on types.
            switch($type){
                case 'group':
                    $addStream = CUserPoints::assignPoint('group.cover.upload');
                    break;
                case 'event':
                    $addStream =  CUserPoints::assignPoint('event.cover.upload');
                    break;
                default:
                    $addStream = CUserPoints::assignPoint('profile.cover.upload');
            }

            if ($type == 'event') {
                $event = JTable::getInstance('Event', 'CTable');
                $event->load($parentId);

                $group = JTable::getInstance('Group', 'CTable');
                $group->load($event->contentid);

                if ($group->approvals == 1) {
                    $addStream = false;
                }
            }

            if ($addStream) {
                // Add activity logging
                if( $type != 'profile' || ($type=='profile' && $parentId == $my->id) ) {
                    CActivityStream::add($act, $params->toString());
                }
            }
        }
    }
    public function testResize($orgW, $orgH, $newW, $newH, $minVal, $maxVal)
    {
        $newValue = 0;
        if ($newH == 0) {
            /* New height value */
            $newValue = round(($newW * $orgH) / $orgW);
        } elseif ($newW == 0) {
            /* New width value */
            $newValue = round(($newH * $orgW) / $orgH);
        } else {
            return false;
        }
        return ($newValue >= $minVal && $newValue <= $maxVal) ? true : false;
    }



}

class CGroupsAction
{
    const DISCUSSION_CREATE	= 'group.discussion.create';
    const DISCUSSION_REPLY	= 'group.discussion.reply';
    const WALLPOST_CREATE		= 'group.wall.create';
    const CREATE						= 'group.create';
}