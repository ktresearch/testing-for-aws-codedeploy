<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
// no direct access

defined('_JEXEC') or die('Restricted access');

/**
 *
 */

class CommunityGroupsController extends CommunityBaseController {

    /**
     * Call the View object to compose the resulting HTML display
     *
     * @param string View function to be called
     * @param mixed extra data to be passed to the View
     */
    public function renderView($viewfunc, $var = NULL) {

        $my = CFactory::getUser();
        $document = JFactory::getDocument();
        $viewType = $document->getType();
        $viewName = JRequest::getCmd('view', $this->getName());
        $view = $this->getView($viewName, '', $viewType);

        echo $view->get($viewfunc, $var);
    }

    /**
     * Responsible to return necessary contents to the Invitation library
     * so that it can add the mails into the queue
     * */
    public function inviteUsers($cid, $users, $emails, $message) {
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($cid);
        $content = '';
        $text = '';
        $title = JText::sprintf('COM_COMMUNITY_GROUPS_JOIN_INVITATION_MESSAGE', $group->name);
        $params = '';
        $my = CFactory::getUser();


        if (!$my->authorise('community.view', 'groups.invite.' . $cid, $group)) {
            return false;
        }

        $params = new CParameter('');
        $params->set('url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
        $params->set('groupname', $group->name);
        $params->set('group', $group->name);
        $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);

        //Chris - Automatic add friends for circle admin
        
        $isAdmin = $group->isAdmin($my->id);

        if ($users) {
            if($isAdmin){
                foreach ($users as $id) {                  
                    $member = JTable::getInstance('GroupMembers', 'CTable');
                    $member->groupid = $group->id;
                    $member->memberid = $id;
                    $member->approved = 1;
                    $member->permissions = '0';
                    $member->store();

                    $gparams = json_decode($group->params, true);

                    if (isset($gparams['course_id'])) {
                        require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/content.php');
                        JoomdleHelperContent::call_method('enrol_user', JFactory::getUser($id)->username, (int)$gparams['course_id'], 5);
                    }

                    $group->updateStats();
                    $group->store();                                       
                }
            }else{
                foreach ($users as $id) {                    
                    $groupInvite = JTable::getInstance('GroupInvite', 'CTable');
                    $groupInvite->groupid = $group->id;
                    $groupInvite->userid = $id;
                    $groupInvite->creator = $my->id;
                    $groupInvite->store();                    
                }
            }

        }

        $htmlTemplate = new CTemplate();
        $htmlTemplate->set('groupname', $group->name);
        $htmlTemplate->set('url', CRoute::getExternalURL('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id));
        $htmlTemplate->set('message', $message);

        $html = $htmlTemplate->fetch('email.groups.invite.html');

        $textTemplate = new CTemplate();
        $textTemplate->set('groupname', $group->name);
        $textTemplate->set('url', CRoute::getExternalURL('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id));
        $textTemplate->set('message', $message);
        $text = $textTemplate->fetch('email.groups.invite.text');

        return new CInvitationMail('groups_invite', $html, $text, $title, $params);
    }

    public function editGroupWall($wallId) {
        $wall = JTable::getInstance('Wall', 'CTable');
        $wall->load($wallId);

        $my = CFactory::getUser();

        if ($my->authorise('community.edit', 'groups.wall.' . $wall->contentid, $wall)) {
            return true;
        }
        return false;
    }

    public function loadMyCircles(){

        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $circlecategoryid = $jinput->post->get('circlecategoryid', '', 'INT');
        $limitstart = $jinput->post->get('limitstart', '', 'INT');
        $checklist = $jinput->post->get('checklist', '', 'STRING');
        $groupsModel = CFactory::getModel('groups');

        $limit = $groupsModel->paginationlimit;
        $user = CFactory::getUser();

        $circles = $groupsModel->getCircles($user->id, 1, $limitstart, $limit, $circlecategoryid, $checklist);
        //$circles = $groupsModel->getCircles($user->_userid, 1, 10, $limit, 6);

        $htmlTemplate = new CTemplate();
        $htmlTemplate->set('circles', $circles);

        $html = $htmlTemplate->fetch('groups/loadcircles');

        echo $html;
        jexit();

    }
        public function loadMyCirclesCourse(){

        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $circlecategoryid = $jinput->post->get('circlecategoryid', '', 'INT');
        $limitstart = $jinput->post->get('limitstart', '', 'INT');
        $checklist = $jinput->post->get('checklist', '', 'STRING');
        $groupsModel = CFactory::getModel('groups');

        $limit = $groupsModel->paginationlimit;
        $user = CFactory::getUser();
        $circles = $groupsModel->getCirclescourse($user->id, 1, $limitstart, $limit, $circlecategoryid, $checklist);
        // $circles = $groupsModel->getCirclescourse($user->_userid, 1, 10, $limit, 6);

        $htmlTemplate = new CTemplate();
        $htmlTemplate->set('circles', $circles);

        $html = $htmlTemplate->fetch('groups/loadcircles');

        echo $html;
        jexit();

    }

    public function loadAllCircles(){

        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $circlecategoryid = $jinput->post->get('circlecategoryid', '', 'INT');
        $limitstart = $jinput->post->get('limitstart', '', 'INT');
        $groupsModel = CFactory::getModel('groups');
        $limit = $groupsModel->paginationlimit;
  
        $circles = $groupsModel->getAllCircles(1, $limitstart, $limit, $circlecategoryid);

        $htmlTemplate = new CTemplate();
        $htmlTemplate->set('circles', $circles);

        $html = $htmlTemplate->fetch('groups/loadcircles');

        echo $html;
        jexit();
    }

    /**
     * Luyen Vu added: load discussion detail for my chats page
     */
    public function loadDiscussionDetail(){

        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $circleid = $jinput->post->get('groupid', '', 'INT');
        $topicid = $jinput->post->get('topicid', '', 'INT');
        $discussion = JTable::getInstance('Discussion', 'CTable');

        $my = CFactory::getUser();
        
        $groupModel = CFactory::getModel('groups');
        $group      = JTable::getInstance('Group', 'CTable');
        $group->load($circleid);
        $discussion->load($topicid);
        $isBanned = $group->isBanned($my->id);

        // Update by Luyenvt - 22 DEC 2017
        // Lastaccess discussion
        $discussionModel = CFactory::getModel('discussions');
        $discussionModel->memberLastaccessDiscuss($discussion->id, $my->id);
        // End update

        $htmlTemplate = new CTemplate();
        $htmlTemplate->set('discussion', $discussion);

        $html = $htmlTemplate->fetch('groups/loaddiscussion');

        echo $html;
        jexit();

    }

    public function load()
    {
            $mainframe = JFactory::getApplication();
            $jinput = $mainframe->input;

            $my = CFactory::getUser();
                    $groupsModel = CFactory::getModel('groups');
            $avatarModel = CFactory::getModel('avatar');
            $wallsModel = CFactory::getModel('wall');
            $activityModel = CFactory::getModel('activities');
            $discussionModel = CFactory::getModel('discussions');
            $sorted = $jinput->get->get('sort', 'latest', 'STRING'); //JRequest::getVar( 'sort' , 'latest' , 'GET' );
            // @todo: proper check with CError::assertion
            // Make sure the sort value is not other than the array keys
            $limit = JText::_('COM_COMMUNITY_LIMIT');
            $position= 0;
            if(isset($_POST['group_no']))
            {
            $group_number = $_POST["group_no"]; 
            $position = ($group_number *  $limit);   
            }
            $groups = $groupsModel->getAllGroups( $categoryId = null , $sorting = null , $search = null , $limit, $skipDefaultAvatar = false , $hidePrivateGroup = false, $pagination = true, $nolimit = false,$position );         
                 foreach ($groups as $group) {
               $table =  JTable::getInstance( 'Group' , 'CTable' );
             $table->load($group->id);
          ?>
           <div class="joms-list__item bordered-box">
    <div class="col-xs-3 pull-left">
       <div class="jom-list__avatar" style="padding-top: 15px">
            <a href="/index.php?option=com_community&view=groups&task=viewabout&groupid=<?php echo $group->id ?>&Itemid=266" class="joms-avatar">
                <img src="<?php echo $table->getAvatar(); ?>"  class="img-responsive">
            </a>
       </div>
    </div>
    <div class="col-xs-9 custom-header pull-left">
       <h1><a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewabout&groupid=' . $group->id);?>"> <?php echo $group->name  ?></a></h1>
       <h3><?php echo JText::_('OWNER').': '.ucfirst(JFactory::getUser($group->ownerid)->name); ?></h3>
       <p class="margin-0 joms--description"><?php echo $group->description ?></p>
    </div>
    <div class="col-xs-12">
        <p class="joms--description" style="padding-top: 10px"><?php echo $group->membercount ?> Members <?php echo $group->discusscount ?> Topics</p>
    </div>
    </div>
                <?php } exit(); 
    }
    
    /**
     * load search circle more
     */
    public function loadSearchMore()
    {
            $mainframe = JFactory::getApplication();
            $jinput = $mainframe->input;
            
            $session = JFactory::getSession();
            $device = $session->get('device');

            $my = CFactory::getUser();
            $groupsModel = CFactory::getModel('groups');
            $avatarModel = CFactory::getModel('avatar');
            $wallsModel = CFactory::getModel('wall');
            $activityModel = CFactory::getModel('activities');
            $discussionModel = CFactory::getModel('discussions');
            $sorted = $jinput->get->get('sort', 'latest', 'STRING'); //JRequest::getVar( 'sort' , 'latest' , 'GET' );
            // @todo: proper check with CError::assertion
            // Make sure the sort value is not other than the array keys
            if ($device == 'mobile') {
                $limit = JText::_('COM_COMMUNITY_LIMIT_MOBILE');
            } else {
                $limit = JText::_('COM_COMMUNITY_LIMIT');
            }
            $limit_new = JText::_('COM_COMMUNITY_LIMIT_MORE');
            $position= 0;
            if(isset($_POST['group_no']))
            {
                $group_number = $_POST["group_no"];
                if ($group_number > 0) { // load second page: 8 circles
                    $position = (($group_number - 1) * $limit_new) + $limit;
                } else { // load frist page: 4 circles
                    $position = ($group_number *  $limit);   
                }
            }
            if (isset($_POST['searchstr'])) {
                $search = $_POST['searchstr'];
            } else {
                $search = null;
            } 
            
            if(isset($_POST['group_no']) && $_POST['group_no'] > 0) {
                $groups = $groupsModel->getAllGroups( $categoryId = "" , null , $search , $limit_new, false , false, true, false, $position ); 
            } else {
                $groups = $groupsModel->getAllGroups( $categoryId = "" , null , $search , $limit, false , false, true, false, $position ); 
            }
            
            foreach ($groups as $key => $group) {
                if (($group->unlisted == 1 && $group->approvals == COMMUNITY_PRIVATE_GROUP) || $group->categoryid == 6) {
                    unset($groups[$key]);  //Unset secret group and course group
                } //else if (isset(json_decode($group->params)->course_id)) unset($groups[$key]); //Unset Course Group
                if(!$group->published && ($my->id != $group->ownerid) ) unset($groups[$key]);;
            }
            
            $htmlTemplate = new CTemplate();
            $htmlTemplate->set('groups', $groups);

            $html = $htmlTemplate->fetch('groups/loadsearchcircle');

            echo $html;
            jexit();
            
    }
    
    public function loadmember()
    {
      
            $mainframe = JFactory::getApplication();
            $jinput = $mainframe->input;
            $groupid = $_REQUEST['groupid'];
            $groupsModel = CFactory::getModel('groups');
            $friendsModel = CFactory::getModel('friends');
            $userModel = CFactory::getModel('user');
            $my = CFactory::getUser();
            $config = CFactory::getConfig();
            $type = $jinput->get->get('approve', '', 'NONE');
            $group = JTable::getInstance('Group', 'CTable');     
            if (!$group->load( $groupid)) {
                echo JText::_('COM_COMMUNITY_GROUPS_NOT_FOUND_ERROR');
                return;
            }

            $limit = JText::_('COM_COMMUNITY_LIMIT');
            $position= 0;
            if(isset($_POST['group_no']))
            {
            $group_number = $_POST["group_no"]; 
            $position = ($group_number *  $limit);   
            }
            // @rule: Test if the group is unpublished, don't display it at all.
            
            $isSuperAdmin = COwnerHelper::isCommunityAdmin();
            $isAdmin = $groupsModel->isAdmin($my->id,$groupid);
            $isMember = $group->isMember($my->id);
            $isMine = ($my->id == $group->ownerid);
            $isBanned = $group->isBanned($my->id); 
                   
            if (!empty($type) && ( $type == '1' )) {
                        $members = $groupsModel->getMembers($groupid, $limit, false,false,false,false,$position);
                    } else {
                        $members = $groupsModel->getMembers($groupid, $limit, true, false, SHOW_GROUP_ADMIN,false,$position);
                    }               
                      $membersList = array();
               foreach ($members as $member) {
                $user = CFactory::getUser($member->id);
          
                $user->friendsCount = $user->getFriendCount();
                $user->approved = $member->approved;
                $user->date = $member->date;
                $user->isMe = ( $my->id == $member->id ) ? true : false;
                $user->isAdmin = $groupsModel->isAdmin($user->id, $groupid);
                $user->isOwner = ( $member->id == $group->ownerid ) ? true : false;

                // Check user's permission
                $groupmember = JTable::getInstance('GroupMembers', 'CTable');
                $keys['groupId'] = $group->id;
                $keys['memberId'] = $member->id;
                $groupmember->load($keys);
                $user->isBanned = ( $groupmember->permissions == COMMUNITY_GROUP_BANNED ) ? true : false;

                $membersList[] = $user;
            }
             $users = array();
                
                foreach($membersList as $member){
                    if($member->isAdmin || $member->isOwner){
                        $admins[] = $member;        
                    }else{
                        $users[] = $member;        
                    }
                }
           
                $members = $users; 
                foreach($members as $member){?>
                
                <div class="joms-list__item bordered-box">
                    <div class="col-xs-3 pull-left">
                       <div class="jom-list__avatar list_avatar_member" style="padding-top: 15px">
                            <a class="cIndex-Avatar" href="<?php echo CRoute::_('index.php?option=com_community&view=profile&fromcircle=1&userid=' . $member->id); ?>">
                                 <!--<p class="joms-avatar">-->
                                <img class="cAvatar" src="<?php echo $member->getThumbAvatar(); ?>" alt="<?php echo $member->getDisplayName(); ?>" class="img-responsive">
                                 <!--</p>-->
                            </a>
                       </div>
                    </div>
                    
                    <div class="custom-header pull-left">
                        <h1><a href="<?php echo CRoute::_('index.php?option=com_community&view=profile&fromcircle=1&userid=' . $member->id); ?>"> 
                            <?php echo CActivities::truncateComplex($member->getDisplayName(), 30, true); ?></a> 
                 
                            <?php if ($isAdmin || $isSuperAdmin) { ?>
                            <a href="javascript:" class="joms-dropdown-button">
                                <span class="glyphicon glyphicon-menu-down" aria-hidden="true" style="color:#868484"></span>
                            </a>
                       
                            <ul class="joms-dropdown">
                           
                                <li>
                                    <a href="javascript:void(0);" onclick="jax.call('community','groups,ajaxAddAdmin', '<?php echo $member->id; ?>', '<?php echo $group->id; ?>');setTimeout('window.location.href=window.location.href', 100);">
                                        <i class="icon-make-admin"></i>
                                        <span><?php echo JText::_('COM_COMMUNITY_GROUPS_ADMIN'); ?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:" onclick="joms.api.groupRemoveMember('<?php echo $group->id; ?>', '<?php echo $member->id; ?>')">
                                        <i class="icon-remove-member"></i>
                                        <span><?php echo JText::_('COM_COMMUNITY_GROUPS_REMOVE_FROM_GROUP');?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:" onclick="joms.api.groupBanMember('<?php echo $group->id; ?>', '<?php echo $member->id; ?>')">
                                        <i class="icon-ban-member"></i>
                                        <span><?php echo JText::_('COM_COMMUNITY_BAN_MEMBER');?></span>
                                    </a>
                                </li>
                            </ul>
                            <?php } ?>
                            <?php if ($type) { ?>
                            <div class="joms-js--request-notice-group-<?php echo $group->id; ?>-<?php echo $member->id; ?>"></div>
                            <div class="joms-js--request-buttons-group-<?php echo $groupid; ?>-<?php echo $member->id; ?>" style="white-space:nowrap">
                                <a href="javascript:" onclick="joms.api.groupApprove('<?php echo $groupid; ?>', '<?php echo $member->id; ?>');">
                                    <button class="joms-button--primary joms-button--smallest joms-button--full-small"><?php echo JText::_('COM_COMMUNITY_PENDING_ACTION_APPROVE'); ?></button>
                                </a>
                                <a href="javascript:" onclick="joms.api.groupRemoveMember('<?php echo $group->id; ?>', '<?php echo $member->id; ?>');">
                                    <button class="joms-button--neutral joms-button--smallest joms-button--full-small"><?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING_ACTION_REJECT'); ?></button>
                                </a>
                            </div>
                            <?php } ?>
                       
                        </h1>
                
                    <p class="margin-0 joms--description member_since"><?php echo JText::_('COM_COMMUNITY_MEMBER_SINCE'); ?> <?php if(!empty($member->date)) {echo date('d/m/Y',$member->date); } else { echo "1/1/2014";}?></p>       
                    </div>
                     <?php if($isMember && $my->id!=$member->id) {  ?>
                        <?php 
                       $isFriend = CFriendsHelper::isConnected($member ->id, $my->id);
                      $isWaitingApproval = CFriendsHelper::isWaitingApproval($my->id, $member->id);
                     $isWaitingResponse = CFriendsHelper::isWaitingApproval($member ->id, $my->id);
            ?>
                    <div class="js-memb-btn-friend">                      
                        <div class="button">
                                 <?php if ( $isFriend ) { ?>
                    <a href="javascript:" class="joms-focus__button--friend" onclick="joms.api.friendRemove('<?php echo $member->id;?>')">
                               <?php echo JText::_('COM_COMMUNITY_CIRCLE_FRIEND'); ?>
                           </a>  
                <?php } else if ( $isWaitingApproval ) { ?>
                        <a href="javascript:" onclick="joms.api.friendAdd('<?php echo $member->id;?>')">
                            <button type="button" class="joms-button--primary button--small pending" style="float: right;">
                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span> 
                                <?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING'); ?>
                            </button>
                        </a>
                    <?php } else if ( $isWaitingResponse ) { ?>
                        <a href="javascript:" onclick="joms.api.friendResponse('<?php echo $member->id;?>')">
                            <button style="float: right;" type="button" class="joms-button--primary button--small pending">
                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span> 
                                <?php echo JText::_('COM_COMMUNITY_FRIENDS_PENDING'); ?>
                            </button>
                        </a>
                    <?php } else { ?>
                           <a href="javascript:" class="joms-focus__button--add" onclick="joms.api.friendAdd('<?php echo $member->id?>')">
                                <?php echo JText::_('COM_COMMUNITY_CIRCLE_ADD_FRIEND'); ?>
                            </a>
                    <?php } ?>           
                        </div>
                           
                              </div>
                        
                     <?php } ?>
                </div>
             
                <?php  } exit();}
    public function editDiscussionWall($wallId) {
        $wall = JTable::getInstance('Wall', 'CTable');
        $wall->load($wallId);

        $discussion = JTable::getInstance('Discussion', 'CTable');
        $discussion->load($wall->contentid);

        $my = CFactory::getUser();

        if ($my->authorise('community.edit', 'groups.discussion.' . $discussion->groupid, $wall)) {
            return true;
        }
        return false;
    }

    public function ajaxRemoveFeatured($groupId) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');

        $json = array();

        if (COwnerHelper::isCommunityAdmin()) {
            $model = CFactory::getModel('Featured');

            $featured = new CFeatured(FEATURED_GROUPS);
            $my = CFactory::getUser();

            if ($featured->delete($groupId)) {
                $json['success'] = true;
                $json['html'] = JText::_('COM_COMMUNITY_GROUP_REMOVED_FROM_FEATURED');
            } else {
                $json['error'] = JText::_('COM_COMMUNITY_REMOVING_GROUP_FROM_FEATURED_ERROR');
            }
        } else {
            $json['error'] = JText::_('COM_COMMUNITY_NOT_ALLOWED_TO_ACCESS_SECTION');
        }

        //ClearCache in Featured List
        $this->cacheClean(array(COMMUNITY_CACHE_TAG_FEATURED, COMMUNITY_CACHE_TAG_GROUPS));

        die( json_encode($json) );
    }

    /**
     * Admin feature the given group
     */
    public function ajaxAddFeatured($groupId) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');

        $json = array();

        if (COwnerHelper::isCommunityAdmin()) {
            $model = CFactory::getModel('Featured');

            if (!$model->isExists(FEATURED_GROUPS, $groupId)) {

                $featured = new CFeatured(FEATURED_GROUPS);
                $table = JTable::getInstance('Group', 'CTable');
                $table->load($groupId);
                $my = CFactory::getUser();
                $config = CFactory::getConfig();
                $limit = $config->get( 'featured' . FEATURED_GROUPS . 'limit' , 10 );

                if($featured->add($groupId, $my->id)===true){
                    $json['success'] = true;
                    $json['html'] = JText::sprintf('COM_COMMUNITY_GROUP_IS_FEATURED', $table->name);
                }else{
                    $json['error'] = JText::sprintf('COM_COMMUNITY_GROUP_LIMIT_REACHED_FEATURED', $table->name, $limit);
                }
            } else {
                $json['error'] = JText::_('COM_COMMUNITY_GROUPS_ALREADY_FEATURED');
            }
        } else {
            $json['error'] = JText::_('COM_COMMUNITY_NOT_ALLOWED_TO_ACCESS_SECTION');
        }

        //ClearCache in Featured List
        $this->cacheClean(array(COMMUNITY_CACHE_TAG_FEATURED, COMMUNITY_CACHE_TAG_GROUPS));

        die( json_encode($json) );
    }

    /**
     * Method is called from the reporting library. Function calls should be
     * registered here.
     *
     * return   String  Message that will be displayed to user upon submission.
     * */
    public function reportDiscussion($link, $message, $discussionId) {
        $report = new CReportingLibrary();

        $report->createReport(JText::_('COM_COMMUNITY_INVALID_DISCUSSION'), $link, $message);

        $action = new stdClass();
        $action->label = 'Remove discussion';
        $action->method = 'groups,removeDiscussion';
        $action->parameters = $discussionId;
        $action->defaultAction = true;

        $report->addActions(array($action));

        return JText::_('COM_COMMUNITY_REPORT_SUBMITTED');
    }

    public function removeDiscussion($discussionId) {
        $model = CFactory::getModel('groups');
        $my = CFactory::getUser();

        if ($my->id == 0) {
            return $this->blockUnregister();
        }

        //CFactory::load( 'models' , 'discussions' );
        $discussion = JTable::getInstance('Discussion', 'CTable');

        $discussion->load($discussionId);
        $discussion->delete();

        //Clear Cache for groups
        $this->cacheClean(array(COMMUNITY_CACHE_TAG_FRONTPAGE, COMMUNITY_CACHE_TAG_GROUPS, COMMUNITY_CACHE_TAG_FEATURED, COMMUNITY_CACHE_TAG_GROUPS_CAT, COMMUNITY_CACHE_TAG_ACTIVITIES));

        return JText::_('COM_COMMUNITY_DISCUSSION_REMOVED');
    }

    /**
     * Method is called from the reporting library. Function calls should be
     * registered here.
     *
     * return   String  Message that will be displayed to user upon submission.
     * */
    public function reportGroup($link, $message, $groupId) {
        //CFactory::load( 'libraries' , 'reporting' );
        $config = CFactory::getConfig();
        $my = CFactory::getUser();
        $report = new CReportingLibrary();

        if (!$my->authorise('community.view', 'groups.report')) {
            return '';
        }

        $report->createReport(JText::_('Bad group'), $link, $message);

        $action = new stdClass();
        $action->label = 'COM_COMMUNITY_GROUPS_UNPUBLISH';
        $action->method = 'groups,unpublishGroup';
        $action->parameters = $groupId;
        $action->defaultAction = true;

        $report->addActions(array($action));

        return JText::_('COM_COMMUNITY_REPORT_SUBMITTED');
    }

    public function unpublishGroup($groupId) {
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);
        if($group->published == 1)
        {
            $group->published = '0';
            $msg = JText::_('COM_COMMUNITY_GROUPS_UNPUBLISH_SUCCESS');
        }
        else
        {
            $group->published = 1;
            $msg = JText::_('COM_COMMUNITY_GROUPS_PUBLISH_SUCCESS');
        }
        $group->store();

        return $msg;
    }

    /**
     * Displays the default groups view
     * */
    public function display($cacheable = false, $urlparams = false) {
        $config = CFactory::getConfig();
        $my = CFactory::getUser();

        if (!$my->authorise('community.view', 'groups.list')) {
            echo JText::_('COM_COMMUNITY_GROUPS_DISABLE');
            return;
        }

        $this->renderView(__FUNCTION__);
    }

    public function allgroups($cacheable = false, $urlparams = false) {
        $config = CFactory::getConfig();
        $my = CFactory::getUser();

        if (!$my->authorise('community.view', 'groups.list')) {
            echo JText::_('COM_COMMUNITY_GROUPS_DISABLE');
            return;
        }

        $this->renderView(__FUNCTION__);
    }

    /**
     * Full application view
     */
    public function app() {
        $view = $this->getView('groups');

        echo $view->get('appFullView');
    }

    /**
     * Full application view for discussion
     */
    public function discussApp() {
        $view = $this->getView('groups');

        echo $view->get('discussAppFullView');
    }

    public function ajaxAcceptInvitation($groupId) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');

        $response = new JAXResponse();
        $my = CFactory::getUser();
        $table = JTable::getInstance('GroupInvite', 'CTable');
        $keys = array('groupid' => $groupId, 'userid' => $my->id);
        $table->load($keys);

        if (!$table->isOwner()) {
            $response->addScriptCall('COM_COMMUNITY_INVALID_ACCESS');
            return $response->sendResponse();
        }

        $this->_saveMember($groupId);
        // delete invitation after approve
        $table->delete();

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($table->groupid);

        $url = CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
        $response->addScriptCall("joms.jQuery('#groups-invite-" . $groupId . "').html('<span class=\"community-invitation-message\">" . JText::sprintf('COM_COMMUNITY_GROUPS_ACCEPTED_INVIT', $group->name, $url) . "</span>');location.reload(true)");

        return $response->sendResponse();
    }

    public function ajaxRejectInvitation($groupId) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');

        $response = new JAXResponse();
        $my = CFactory::getUser();
        $table = JTable::getInstance('GroupInvite', 'CTable');
        $keys = array('groupid' => $groupId, 'userid' => $my->id);
        $table->load($keys);

        if (!$table->isOwner()) {
            // when the user is the owner group we need avoid the invitation
            $table->delete();

            $response->addScriptCall('COM_COMMUNITY_INVALID_ACCESS');
            return $response->sendResponse();
        }

        if ($table->delete()) {
            $group = JTable::getInstance('Group', 'CTable');
            $group->load($table->groupid);
            $url = CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
            $response->addScriptCall("joms.jQuery('#groups-invite-" . $groupId . "').html('<span class=\"community-invitation-message\">" . JText::sprintf('COM_COMMUNITY_GROUPS_REJECTED_INVIT', $group->name, $url) . "</span>')");
        }

        return $response->sendResponse();
    }
    
    public function ajaxUnpublishGroup($groupId=null) {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $groupId = $jinput->post->get('groupid', '', 'INT');
        $response = new JAXResponse();

        CError::assert($groupId, '', '!empty', __FILE__, __LINE__);

        if ( ! COwnerHelper::isCommunityAdmin()) {
            $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_ACCESS_FORBIDDEN'), 'error');
            return false;
        } else {
            $group = JTable::getInstance('Group', 'CTable');
            $group->load($groupId);

            if ($group->id == 0) {
                $response->addScriptCall('alert', JText::_('COM_COMMUNITY_GROUPS_ID_NOITEM'));
            } else {
                $group->published = 0;

                if ($group->store()) {
                    //trigger for onGroupDisable
                    $this->triggerGroupEvents('onGroupDisable', $group);
                    $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups', false), JText::_('COM_COMMUNITY_GROUPS_UNPUBLISH_SUCCESS'));
                } else {
                    $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUPS_SAVE_ERROR'), 'error');
                    return false;
                }
            }
        }
    }

    /**
     *  Ajax function to delete a group
     *
     * @param   $groupId    The specific group id to unpublish
     * */
    public function ajaxDeleteGroup($groupId, $step = 1) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');
        $step = $filter->clean($step, 'int');

        $json = array();
        $response = new JAXResponse();

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);

        $groupModel = CFactory::getModel('groups');
        $membersCount = $groupModel->getMembersCount($groupId);
        $my = CFactory::getUser();

        // @rule: Do not allow anyone that tries to be funky!
        if (!$my->authorise('community.delete', 'groups.' . $groupId, $group)) {
            $json['error'] = JText::_('COM_COMMUNITY_GROUPS_NOT_ALLOWED_DELETE');
            die( json_encode($json) );
        }

        $doneMessage = ' - <span class=\'success\'>' . JText::_('COM_COMMUNITY_DONE') . '</span><br />';
        $failedMessage = ' - <span class=\'failed\'>' . JText::_('COM_COMMUNITY_FAILED') . '</span><br />';
        $childId = 0;
        switch ($step) {
            case 1:
                // Nothing gets deleted yet. Just show a messge to the next step
                if (empty($groupId)) {
                    $json['error'] = JText::_('COM_COMMUNITY_GROUPS_ID_NOITEM');
                } else {
                    $json['message']  = '<strong>' . JText::sprintf('COM_COMMUNITY_GROUPS_DELETE_GROUP', $group->name) . '</strong><br/>';
                    $json['message'] .= JText::_('COM_COMMUNITY_GROUPS_DELETE_BULLETIN');
                    $json['next'] = 2;

                    //trigger for onBeforeGroupDelete
                    $this->triggerGroupEvents('onBeforeGroupDelete', $group);
                }
                break;

            case 2:
                // Delete all group bulletins
                CommunityModelGroups::getGroupChildId($groupId);
                if (CommunityModelGroups::deleteGroupBulletins($groupId)) {
                    $content = $doneMessage;
                } else {
                    $content = $failedMessage;
                }

                $content .= JText::_('COM_COMMUNITY_GROUPS_DELETE_GROUP_MEMBERS');

                $json['message'] = $content;
                $json['next'] = 3;
                break;

            case 3:
                // Delete all group members
                if (CommunityModelGroups::deleteGroupMembers($groupId)) {
                    $content = $doneMessage;
                } else {
                    $content = $failedMessage;
                }

                $content .= JText::_('COM_COMMUNITY_GROUPS_WALLS_DELETE');

                $json['message'] = $content;
                $json['next'] = 4;
                break;

            case 4:
                // Delete all group wall
                if (CommunityModelGroups::deleteGroupWall($groupId)) {
                    $content = $doneMessage;
                } else {
                    $content = $failedMessage;
                }

                $content .= JText::_('COM_COMMUNITY_GROUPS_DISCUSSIONS_DELETEL');

                $json['message'] = $content;
                $json['next'] = 5;
                break;

            case 5:
                // Delete all group discussions
                if (CommunityModelGroups::deleteGroupDiscussions($groupId)) {
                    $content = $doneMessage;
                } else {
                    $content = $failedMessage;
                }

                $content .= JText::_('COM_COMMUNITY_GROUPS_DELETE_MEDIA');

                $json['message'] = $content;
                $json['next'] = 6;
                break;

            case 6:
                // Delete all group's media files
                if (CommunityModelGroups::deleteGroupMedia($groupId)) {
                    $content = $doneMessage;
                } else {
                    $content = $failedMessage;
                }

                $json['message'] = $content;
                $json['next'] = 7;
                break;

            case 7:
                // Delete group
                $group = JTable::getInstance('Group', 'CTable');
                $group->load($groupId);
                $groupData = $group;

                if ($group->delete($groupId)) {
                    //CFactory::load( 'libraries' , 'featured' );
                    $featured = new CFeatured(FEATURED_GROUPS);
                    $featured->delete($groupId);

                    jimport('joomla.filesystem.file');

                    //@rule: Delete only thumbnail and avatars that exists for the specific group
                    if ($groupData->avatar != "components/com_community/assets/group.jpg" && !empty($groupData->avatar)) {
                        $path = explode('/', $groupData->avatar);
                        $file = JPATH_ROOT . '/' . $path[0] . '/' . $path[1] . '/' . $path[2] . '/' . $path[3];
                        if (JFile::exists($file)) {
                            JFile::delete($file);
                        }
                    }

                    if ($groupData->thumb != "components/com_community/assets/group_thumb.jpg" && !empty($groupData->thumb)) {
                        $path = explode('/', $groupData->thumb);
                        $file = JPATH_ROOT . '/' . $path[0] . '/' . $path[1] . '/' . $path[2] . '/' . $path[3];
                        if (JFile::exists($file)) {
                            JFile::delete($file);
                        }
                    }

                    $content = JText::_('COM_COMMUNITY_GROUPS_DELETED');

                    //trigger for onGroupDelete
                    $this->triggerGroupEvents('onAfterGroupDelete', $groupData);
                    // Pushed notification
                    $members = $groupModel->getMembers($groupData->id, 0, true, false, false);
                    $mems = array();
                    foreach ($members as $mem) {
                        $mems[] = $mem->id;
                    }
                            JPluginHelper::importPlugin('groupsapi');
                            $dispatcher = JDispatcher::getInstance();
                            $message = array(
                        'mtitle' => 'The circle has been deleted!',
                        'mdesc' => 'The circle has been deleted, please contact to administrator!'
                            );

                    $dispatcher->trigger('pushNotification', array($message, $mems, $groupData->id, 0, 'delete_circle', ''));
                } else {
                    $content = JText::_('COM_COMMUNITY_GROUPS_DELETE_ERROR');
                }

                $redirect = CRoute::_('index.php?option=com_community&view=groups');

                $json['message'] = $content;
                $json['redirect'] = $redirect;
                $json['btnDone'] = JText::_('COM_COMMUNITY_DONE_BUTTON');
                break;

            default:
                break;
        }
        //Clear Cache for groups
        $this->cacheClean(array(COMMUNITY_CACHE_TAG_FRONTPAGE, COMMUNITY_CACHE_TAG_GROUPS, COMMUNITY_CACHE_TAG_FEATURED, COMMUNITY_CACHE_TAG_GROUPS_CAT, COMMUNITY_CACHE_TAG_ACTIVITIES));

        die( json_encode($json) );

        // return $response->sendResponse();
    }

    /**
     *  Ajax function to prompt warning during group deletion
     *
     * @param   $groupId    The specific group id to unpublish
     * */
    public function ajaxWarnGroupDeletion($groupId) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);

        $json = array(
            'title' => JText::sprintf('COM_COMMUNITY_GROUPS_DELETE_GROUP', $group->name),
            'html'  => JText::_('COM_COMMUNITY_GROUPS_DELETE_WARNING'),
            'btnDelete' => JText::_('COM_COMMUNITY_DELETE'),
            'btnCancel' => JText::_('COM_COMMUNITY_CANCEL_BUTTON')
        );

        die( json_encode($json) );
    }

    /**
     * Ajax function to remove a reply from the discussions
     *
     * @params $discussId   An string that determines the discussion id
     * */
    public function ajaxRemoveReply($wallId) {
        require_once( JPATH_COMPONENT . '/libraries/activities.php' );

        $filter = JFilterInput::getInstance();
        $wallId = $filter->clean($wallId, 'int');

        CError::assert($wallId, '', '!empty', __FILE__, __LINE__);

        $response = new JAXResponse();
        $json = array();

        //@rule: Check if user is really allowed to remove the current wall
        $my = CFactory::getUser();
        $model = $this->getModel('wall');
        $wall = $model->get($wallId);
        //CFactory::load( 'models' , 'discussions' );

        $discussion = JTable::getInstance('Discussion', 'CTable');
        $discussion->load($wall->contentid);

        //CFactory::load( 'helpers' , 'owner' );

        if (!$my->authorise('community.delete', 'groups.discussion.' . $discussion->groupid) && $wall->post_by != $my->id) {
            $errorMsg = $my->authoriseErrorMsg();
            if ($errorMsg == 'blockUnregister') {
                return $this->ajaxBlockUnregister();
            } else {
                $json['error'] = $errorMsg;
            }
        } else {
            if (!$model->deletePost($wallId)) {
                $json['error'] = JText::_('COM_COMMUNITY_GROUPS_REMOVE_WALL_ERROR');
            } else {
                // Update activity.
                CActivities::removeWallActivities(array('app' => 'groups.discussion.reply', 'cid' => $wall->contentid, 'createdAfter' => $wall->date), $wallId);

                //add user points
                if ($wall->post_by != 0) {
                    //CFactory::load( 'libraries' , 'userpoints' );
                    CUserPoints::assignPoint('wall.remove', $wall->post_by);
                }
            }
        }

        $this->cacheClean(array(COMMUNITY_CACHE_TAG_GROUPS_DETAIL));

        if ( !isset($json['error']) ) {
            $json['success'] = true;
        }

        die( json_encode($json) );
    }
    
    /**
     * Ajax function to display the remove bulletin information
     * */
    public function ajaxShowRemoveBulletin($groupid, $bulletinId) {
        $filter = JFilterInput::getInstance();
        $groupid = $filter->clean($groupid, 'int');
        $bulletinId = $filter->clean($bulletinId, 'int');

        $contents  = JText::_('COM_COMMUNITY_GROUPS_BULLETIN_DELET_CONFIRMATION');
        $contents .= '<form method="POST" action="' . CRoute::_('index.php?option=com_community&view=groups&task=deleteBulletin') . '" style="margin:0;padding:0">';
        $contents .= '<input type="hidden" value="' . $groupid . '" name="groupid">';
        $contents .= '<input type="hidden" value="' . $bulletinId . '" name="bulletinid">';
        $contents .= '</form>';

        $json = array(
            'title'  => JText::_('COM_COMMUNITY_DELETE'),
            'html'   => $contents,
            'btnYes' => JText::_('COM_COMMUNITY_BUTTON_REMOVE'),
            'btnNo'  => JText::_('COM_COMMUNITY_CANCEL')
        );

        die( json_encode($json) );
    }

    /**
     * Ajax function to display the remove discussion information
     * */
    public function ajaxShowRemoveDiscussion($groupid, $topicid) {
        $filter = JFilterInput::getInstance();
        $groupid = $filter->clean($groupid, 'int');
        $topicid = $filter->clean($topicid, 'int');

        $contents  = JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_DELETE_CONFIRMATION');
        $contents .= '<form method="POST" action="' . CRoute::_('index.php?option=com_community&view=groups&task=deleteTopic') . '" style="margin:0;padding:0">';
        $contents .= '<input type="hidden" value="' . $groupid . '" name="groupid">';
        $contents .= '<input type="hidden" value="' . $topicid . '" name="topicid">';
        $contents .= '</form>';

        $json = array(
            'title'  => JText::_('COM_COMMUNITY_DELETE'),
            'html'   => $contents,
            'btnYes' => JText::_('COM_COMMUNITY_REMOVE_BUTTON'),
            'btnNo'  => JText::_('COM_COMMUNITY_CANCEL_BUTTON')
        );

        die( json_encode($json) );
    }

    public function ajaxShowLockDiscussion($groupid, $topicid) {
        $filter = JFilterInput::getInstance();
        $groupid = $filter->clean($groupid, 'int');
        $topicid = $filter->clean($topicid, 'int');

        $discussion = JTable::getInstance('Discussion', 'CTable');
        $discussion->load($topicid);

        $titleLock = $discussion->lock ? JText::_('COM_COMMUNITY_UNLOCK_DISCUSSION') : JText::_('COM_COMMUNITY_LOCK_DISCUSSION');

        $questionLock  = $discussion->lock ? JText::_('COM_COMMUNITY_DISCUSSION_UNLOCK_MESSAGE') : JText::_('COM_COMMUNITY_DISCUSSION_LOCK_MESSAGE');
        $questionLock .= '<form method="POST" action="' . CRoute::_('index.php?option=com_community&view=groups&task=lockTopic') . '" style="margin:0;padding:0">';
        $questionLock .= '<input type="hidden" value="' . $groupid . '" name="groupid">';
        $questionLock .= '<input type="hidden" value="' . $topicid . '" name="topicid">';
        $questionLock .= '</form>';

        $json = array(
            'title'  => $titleLock,
            'html'   => $questionLock,
            'btnYes' => JText::_('COM_COMMUNITY_YES_BUTTON'),
            'btnNo'  => JText::_('COM_COMMUNITY_NO_BUTTON')
        );

        die( json_encode($json) );
    }

    /**
     * Ajax function to approve a specific member
     *
     * @params  string  id  The member's id that needs to be approved.
     * @params  string  groupid The group id that the user is in.
     * */
    public function ajaxApproveMember($memberId, $groupId) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');
        $memberId = $filter->clean($memberId, 'int');

        $json = array();

        $my = CFactory::getUser();
        $gMember = CFactory::getUser($memberId);
        //CFactory::load( 'helpers' , 'owner' );

        if (!$my->authorise('community.approve', 'groups.member.' . $groupId)) {
            $json['error'] = JText::_('COM_COMMUNITY_NOT_ALLOWED_TO_ACCESS_SECTION');
        } else {
            // Load required tables
            $member = JTable::getInstance('GroupMembers', 'CTable');
            $group = JTable::getInstance('Group', 'CTable');

            // Load the group and the members table
            $group->load($groupId);
            $keys = array('groupId' => $groupId, 'memberId' => $memberId);
            $member->load($keys);

            //trigger for onGroupJoinApproved
            $this->triggerGroupEvents('onGroupJoinApproved', $group, $memberId);
            $this->triggerGroupEvents('onGroupJoin', $group, $memberId);

            // Only approve members that is really not approved yet.
            if ($member->approved) {
                $json['error'] = JText::_('COM_COMMUNITY_MEMBER_ALREADY_APPROVED');
            } else {
                $member->approve();

                //Update member user table
                $gMember->updateGroupList(true);

                CGroups::joinApproved($group->id, $memberId);

                $json['success'] = true;
                $json['message'] = JText::_('COM_COMMUNITY_GROUPS_APPROVE_MEMBER');

                // email notification
                $params = new CParameter('');
                $params->set('url', CRoute::getExternalURL('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id));
                $params->set('group', $group->name);
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);

                CNotificationLibrary::add('groups_member_approved', $group->ownerid, $memberId, JText::sprintf('COM_COMMUNITY_GROUP_MEMBER_APPROVED_EMAIL_SUBJECT'), '', 'groups.memberapproved', $params);
            }
        }

        $this->cacheClean(array(COMMUNITY_CACHE_TAG_GROUPS, COMMUNITY_CACHE_TAG_ACTIVITIES));

        die( json_encode($json) );
    }

    public function nonAjaxApproveMember($memberId, $groupId) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');
        $memberId = $filter->clean($memberId, 'int');

        $json = array();

        $my = CFactory::getUser();
        $gMember = CFactory::getUser($memberId);
        //CFactory::load( 'helpers' , 'owner' );

        if (!$my->authorise('community.approve', 'groups.member.' . $groupId)) {
            $json['error'] = JText::_('COM_COMMUNITY_NOT_ALLOWED_TO_ACCESS_SECTION');
        } else {
            // Load required tables
            $member = JTable::getInstance('GroupMembers', 'CTable');
            $group = JTable::getInstance('Group', 'CTable');

            // Load the group and the members table
            $group->load($groupId);
            $keys = array('groupId' => $groupId, 'memberId' => $memberId);
            $member->load($keys);

            //trigger for onGroupJoinApproved
            $this->triggerGroupEvents('onGroupJoinApproved', $group, $memberId);
            $this->triggerGroupEvents('onGroupJoin', $group, $memberId);

            // Only approve members that is really not approved yet.
            if ($member->approved) {
                $json['error'] = JText::_('COM_COMMUNITY_MEMBER_ALREADY_APPROVED');
            } else {
                $member->approve();

                //Update member user table
                $gMember->updateGroupList(true);

                CGroups::joinApproved($group->id, $memberId);

                $json['success'] = true;
                $json['message'] = JText::_('COM_COMMUNITY_GROUPS_APPROVE_MEMBER');

                // email notification
                $params = new CParameter('');
                $params->set('url', CRoute::getExternalURL('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id));
                $params->set('group', $group->name);
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);

                CNotificationLibrary::add('groups_member_approved', $group->ownerid, $memberId, JText::sprintf('COM_COMMUNITY_GROUP_MEMBER_APPROVED_EMAIL_SUBJECT'), '', 'groups.memberapproved', $params);
            }
        }

        $this->cacheClean(array(COMMUNITY_CACHE_TAG_GROUPS, COMMUNITY_CACHE_TAG_ACTIVITIES));

    }

    public function ajaxConfirmMemberRemoval($memberId, $groupId) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');
        $memberId = $filter->clean($memberId, 'int');

        $json = array();

        // Get html
        $member = CFactory::getUser($memberId);
        $html  = JText::sprintf('COM_COMMUNITY_GROUPS_MEMBER_REMOVAL_WARNING', $member->getDisplayName());
        $html .= '<div style="display:none;"><label><input type="checkbox" name="block" class="joms-checkbox">&nbsp;' . JText::_('COM_COMMUNITY_ALSO_BAN_MEMBER') . '</label></div>';

        $this->cacheClean(array(COMMUNITY_CACHE_TAG_GROUPS));

        $json = array(
            'title'  => JText::_('COM_COMMUNITY_REMOVE_MEMBER'),
            'html'   => $html,
            'btnYes' => JText::_('COM_COMMUNITY_BUTTON_REMOVE'),
            'btnNo'  => JText::_('COM_COMMUNITY_CANCEL')
        );

        die( json_encode($json) );
    }

    /**
     * Ajax method to remove specific member
     *
     * @params  string  id  The member's id that needs to be approved.
     * @params  string  groupid The group id that the user is in.
     * */
    public function ajaxRemoveMember($memberId, $groupId) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');
        $memberId = $filter->clean($memberId, 'int');

        $json = array();

        $model = $this->getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);

        $my = CFactory::getUser();

        $groupsModel = $this->getModel('groups');
        $isAdmin = $groupsModel->isAdmin($my->id, $groupId);
        $isLPCircle = ($group->moodlecategoryid > 0) ? true : false;

        //if (!$my->authorise('community.remove', 'groups.member.' . $memberId, $group)) {
        if(!$isAdmin){
            $errorMsg = $my->authoriseErrorMsg();
            if ($errorMsg == 'blockUnregister') {
                return $this->ajaxBlockUnregister();
            } else {
                $json['error'] = $errorMsg;
            }
        } else {
            $groupMember = JTable::getInstance('GroupMembers', 'CTable');
            $keys = array('groupId' => $groupId, 'memberId' => $memberId);
            $groupMember->load($keys);

            $data = new stdClass();

            $data->groupid = $groupId;
            $data->memberid = $memberId;

            $model->removeMember($data);
            $user = CFactory::getUser($memberId);
            $user->updateGroupList(true);

            //trigger for onGroupLeave
            $this->triggerGroupEvents('onGroupLeave', $group, $memberId);

            if ($isLPCircle) {
                require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/content.php');
                $ob = new stdClass();
                $ob->user = $user->username;
                $ob->roleid = 1; // roleid for manager
                $ob->status = 0;
                $datamanager[] = $ob;
                $datamanager = json_encode($datamanager);
                $response = JoomdleHelperContent::call_method('set_user_category_role', $my->username, (int)$group->moodlecategoryid, $datamanager);
            } else {
                $gparams = json_decode($group->params, true);
                if (isset($gparams['course_id'])) {
                    require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/content.php');
                    JoomdleHelperContent::call_method('unenrol_user', $user->username, $gparams['course_id']);
                }
            }

            //add user points
            CUserPoints::assignPoint('group.member.remove', $memberId);

            //delete invitation
            $invitation = JTable::getInstance('Invitation', 'CTable');
            $invitation->deleteInvitation($groupId, $memberId, 'groups,inviteUsers');

            $json['success'] = true;
            $json['message'] = JText::_('COM_COMMUNITY_GROUPS_MEMBERS_DELETE_SUCCESS');
        }

        // Store the group and update the data
        $group->updateStats();
        $group->store();

        die( json_encode($json) );
    }

    public function nonAjaxRemoveMember($memberId, $groupId) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');
        $memberId = $filter->clean($memberId, 'int');

        $json = array();

        $model = $this->getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);

        $my = CFactory::getUser();

        $groupsModel = $this->getModel('groups');
        $isAdmin = $groupsModel->isAdmin($my->id, $groupId);
        
        //if (!$my->authorise('community.remove', 'groups.member.' . $memberId, $group)) {
        if(!$isAdmin){
            $errorMsg = $my->authoriseErrorMsg();
            if ($errorMsg == 'blockUnregister') {
                return $this->ajaxBlockUnregister();
            } else {
                $json['error'] = $errorMsg;
            }
        } else {
            $groupMember = JTable::getInstance('GroupMembers', 'CTable');
            $keys = array('groupId' => $groupId, 'memberId' => $memberId);
            $groupMember->load($keys);

            $data = new stdClass();

            $data->groupid = $groupId;
            $data->memberid = $memberId;

            $model->removeMember($data);
            $user = CFactory::getUser($memberId);
            $user->updateGroupList(true);

            //trigger for onGroupLeave
            $this->triggerGroupEvents('onGroupLeave', $group, $memberId);

            $gparams = json_decode($group->params, true);
            if (isset($gparams['course_id'])) {
                require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/content.php');
                JoomdleHelperContent::call_method('unenrol_user', $user->username, $gparams['course_id']);
            }

            //add user points
            CUserPoints::assignPoint('group.member.remove', $memberId);

            //delete invitation
            $invitation = JTable::getInstance('Invitation', 'CTable');
            $invitation->deleteInvitation($groupId, $memberId, 'groups,inviteUsers');

            $json['success'] = true;
            $json['message'] = JText::_('COM_COMMUNITY_GROUPS_MEMBERS_DELETE_SUCCESS');
        }

        // Store the group and update the data
        $group->updateStats();
        $group->store();
    }

    /**
     * Ajax method to display HTML codes to unpublish group
     *
     * @params  string  groupid The group id that the user is in.
     * */
    public function ajaxShowUnpublishGroup($groupId) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');

        $response = new JAXResponse();

        $model = $this->getModel('groups');
        $my = CFactory::getUser();

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);

        $html  = JText::_('COM_COMMUNITY_GROUPS_UNPUBLISH_CONFIRMATION') . ' <strong>' . $group->name . '</strong>?';
        $html .= '<form method="POST" action="' . CRoute::_('index.php?option=com_community&view=groups&task=ajaxUnpublishGroup') . '" style="margin:0">';
        $html .= '<input type="hidden" value="' . $groupId . '" name="groupid">';
        $html .= '</form>';

        $json = array(
            'title'  => JText::_('COM_COMMUNITY_GROUPS_UNPUBLISH'),
            'html'   => $html,
            'btnYes' => JText::_('COM_COMMUNITY_YES_BUTTON'),
            'btnNo'  => JText::_('COM_COMMUNITY_NO_BUTTON'),
        );

        die( json_encode($json) );
    }

    /**
     * Ajax method to display HTML codes to leave group
     *
     * @params  string  id  The member's id that needs to be approved.
     * @params  string  groupid The group id that the user is in.
     * */
    public function ajaxShowLeaveGroup($groupId) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');

        $json = array();

        $model = $this->getModel('groups');
        $my = CFactory::getUser();

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);

        $html  = JText::_('COM_COMMUNITY_GROUPS_MEMBERS_LEAVE_CONFIRMATION') . ' <strong>' . $group->name . '</strong>?';
        $html .= '<form method="POST" action="' . CRoute::_('index.php?option=com_community&view=groups&task=leavegroup') . '" style="margin:0;padding:0;">';
        $html .= '<input type="hidden" value="' . $groupId . '" name="groupid">';
        $html .= '</form>';

        $json = array(
//            'title'  => JText::_('COM_COMMUNITY_GROUPS_LEAVE'),
            'html'   => $html,
            'btnYes' => JText::_('COM_COMMUNITY_LEAVE_BUTTON'),
            'btnNo'  => JText::_('COM_COMMUNITY_CANCEL_BUTTON')
        );

        die( json_encode($json) );
    }

    /**
     * Ajax function to display the join group
     *
     * @params $groupid A string that determines the group id
     * */
    public function ajaxShowJoinGroup($groupId, $redirectUrl) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');
        $redirectUrl = $filter->clean($redirectUrl, 'string');

        if (!COwnerHelper::isRegisteredUser()) {
            return $this->ajaxBlockUnregister();
        }

        $response = new JAXResponse();

        $model = $this->getModel('groups');
        $my = CFactory::getUser();
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);

        $members = $model->getMembersId($groupId);

        ob_start();
        ?>
        <div id="community-groups-join">
            <?php if (in_array($my->id, $members)): ?>
                <?php
                $buttons = '<input onclick="cWindowHide();" type="submit" value="' . JText::_('COM_COMMUNITY_BUTTON_CLOSE_BUTTON') . '" class="btn" name="Submit"/>';
                ?>
                <p><?php echo JText::_('COM_COMMUNITY_GROUPS_ALREADY_MEMBER'); ?></p>
            <?php else: ?>
                <?php
                $buttons = '<form class="reset-gap" name="jsform-groups-ajaxshowjoingroup" method="post" action="' . CRoute::_('index.php?option=com_community&view=groups&task=joingroup') . '">';
                $buttons .= '<input type="hidden" value="' . $groupId . '" name="groupid" />';
                $buttons .= '<input onclick="cWindowHide();" type="button" value="' . JText::_('COM_COMMUNITY_NO_BUTTON') . '" class="btn" name="Submit" />';
                $buttons .= '<input type="submit" value="' . JText::_('COM_COMMUNITY_YES_BUTTON') . '" class="btn btn-primary pull-right" name="Submit"/>';
                $buttons .= '</form>';
                ?>
                <p>
                    <?php echo JText::sprintf('COM_COMMUNITY_GROUPS_JOIN_CONFIRMATION', $group->name); ?>
                </p>
            <?php endif; ?>
        </div>
        <?php
        $contents = ob_get_contents();
        ob_end_clean();

        // Change cWindow title
        $response->addAssign('cwin_logo', 'innerHTML', JText::_('COM_COMMUNITY_GROUPS_JOIN'));
        $response->addScriptCall('cWindowAddContent', $contents, $buttons);

        return $response->sendResponse();
    }

    /**
     * Ajax Method to remove specific wall from the specific group
     *
     * @param wallId    The unique wall id that needs to be removed.
     * @todo: check for permission
     * */
    public function ajaxRemoveWall($wallId) {
        $filter = JFilterInput::getInstance();
        $wallId = $filter->clean($wallId, 'int');

        CError::assert($wallId, '', '!empty', __FILE__, __LINE__);

        $response = new JAXResponse();

        //@rule: Check if user is really allowed to remove the current wall
        $my = CFactory::getUser();
        $model = $this->getModel('wall');
        $wall = $model->get($wallId);

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($wall->contentid);

        //CFactory::load( 'helpers' , 'owner' );

        if (!$my->authorise('community.delete', 'groups.wall.' . $group->id)) {
            $errorMsg = $my->authoriseErrorMsg();

            if ($errorMsg == 'blockUnregister') {
                return $this->ajaxBlockUnregister();
            } else {
                $response->addScriptCall('alert', $errorMsg);
            }
        } else {
            if (!$model->deletePost($wallId)) {
                $response->addAlert(JText::_('COM_COMMUNITY_GROUPS_REMOVE_WALL_ERROR'));
            } else {
                if ($wall->post_by != 0) {
                    //add user points
                    //CFactory::load( 'libraries' , 'userpoints' );
                    CUserPoints::assignPoint('wall.remove', $wall->post_by);
                }
            }

            $group->updateStats();
            $group->store();
        }
        $this->cacheClean(array(COMMUNITY_CACHE_TAG_GROUPS));
        return $response->sendResponse();
    }

    /**
     * Ajax function to add new admin to the group
     *
     * @param memberid  Members id
     * @param groupid   Groupid
     *
     * */
    public function ajaxRemoveAdmin($memberId, $groupId,$roleId = false,$courseId = false) {     
        return $this->updateAdmin($memberId, $groupId, false, (int)$courseId, (int)$roleId);
    }

    /**
     * Ajax function to add new admin to the group
     *
     * @param memberid  Members id
     * @param groupid   Groupid
     *
     * */
    public function ajaxAddAdmin($memberId, $groupId) {
        return $this->updateAdmin($memberId, $groupId, true);
    }

    public function updateAdmin($memberId, $groupId, $doAdd = true,$courseId = false, $roleId = false) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');
        $memberId = $filter->clean($memberId, 'int');

        $response = new JAXResponse();

        $my = CFactory::getUser();

        $model = $this->getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);

        //CFactory::load( 'helpers' , 'owner' );
        // update manage can remome manange in block manage
//        if (!$my->authorise('community.edit', 'groups.admin.' . $groupId, $group)) {
//            $response->addScriptCall('joms.jQuery("#notice-message").html("' . JText::_('COM_COMMUNITY_PERMISSION_DENIED_WARNING') . '");');
//            $response->addScriptCall('joms.jQuery("#notice").css("display","block");');
//            $response->addScriptCall('joms.jQuery("#notice").attr("class","alert alert-danger");');
//        } else {
        $member = JTable::getInstance('GroupMembers', 'CTable');

        $keys = array('groupId' => $group->id, 'memberId' => $memberId);
        $member->load($keys);
        $member->permissions = $doAdd ? 1 : 0;

        $member->store();
        $response->addScriptCall('location.reload();');
        /*
        $message = $doAdd ? JText::_('COM_COMMUNITY_GROUPS_NEW_ADMIN_MESSAGE') : JText::_('COM_COMMUNITY_GROUPS_NEW_USER_MESSAGE');
        $response->addScriptCall('joms.jQuery("#member_' . $memberId . '");');
        $response->addScriptCall('joms.jQuery("#notice-message").html("' . $message . '");');
        $response->addScriptCall('joms.jQuery("#notice").css("display","block");');

        if ($doAdd) {
            $response->addScriptCall('joms.jQuery("#member_' . $memberId . ' ul li.setAdmin")[0].addClass("hide");');
            $response->addScriptCall('joms.jQuery("#member_' . $memberId . ' ul li.setAdmin")[1].removeClass("hide");');
        } else {
            $response->addScriptCall('joms.jQuery("#member_' . $memberId . ' ul li.setAdmin")[1].addClass("hide");');
            $response->addScriptCall('joms.jQuery("#member_' . $memberId . ' ul li.setAdmin")[0].removeClass("hide");');
        }
        */
//        }
        if ($courseId && $roleId) {
            require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
            $memberusername = JFactory::getUser($memberId)->username;
            $action = 1;
            JoomdleHelperContent::call_method('remove_user_role', $memberusername, (int)$courseId, (int)$roleId,$action);
        }
        return $response->sendResponse();
    }

    /**
     * Ajax function to save a new wall entry
     *
     * @param message   A message that is submitted by the user
     * @param uniqueId  The unique id for this group
     *
     * */
    public function ajaxSaveDiscussionWall($message, $uniqueId, $photoId = 0) {

        $filter = JFilterInput::getInstance();
        //$message = $filter->clean($message, 'string');
        $uniqueId = $filter->clean($uniqueId, 'int');

        if (!COwnerHelper::isRegisteredUser()) {
            return $this->ajaxBlockUnregister();
        }

        $response = new JAXResponse();
        $json = array();

        $my = CFactory::getUser();

        // Load models
        $group = JTable::getInstance('Group', 'CTable');
        $discussionModel = CFactory::getModel('Discussions');
        $discussion = JTable::getInstance('Discussion', 'CTable');
        //$message      = strip_tags( $message );
        $discussion->load($uniqueId);
        $group->load($discussion->groupid);

        // If the content is false, the message might be empty.
        if (empty($message) && $photoId == 0) {
            $json['error'] = JText::_('COM_COMMUNITY_EMPTY_MESSAGE');
            die( json_encode($json) );
        }
        $config = CFactory::getConfig();

        // @rule: Spam checks
        if ($config->get('antispam_akismet_walls')) {
            //CFactory::load( 'libraries' , 'spamfilter' );

            $filter = CSpamFilter::getFilter();
            $filter->setAuthor($my->getDisplayName());
            $filter->setMessage($message);
            $filter->setEmail($my->email);
            $filter->setURL(CRoute::_('index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $discussion->groupid . '&topicid=' . $discussion->id));
            $filter->setType('message');
            $filter->setIP($_SERVER['REMOTE_ADDR']);

            if ($filter->isSpam()) {
                $json['error'] = JText::_('COM_COMMUNITY_WALLS_MARKED_SPAM');
                die( json_encode($json) );
            }
        }
        // Save the wall content
        $wall = CWallLibrary::saveWall($uniqueId, $message, 'discussions', $my, ($my->id == $discussion->creator), 'groups,discussion', 'wall/content2', 0, $photoId);
        $date = JFactory::getDate();

        $discussion->lastreplied = $date->toSql();
        $discussion->store();

        // @rule: only add the activities of the wall if the group is not private.
        //if( $group->approvals == COMMUNITY_PUBLIC_GROUP ) {
        // Build the URL
        $discussURL = CUrl::build('groups', 'viewdiscussion', array('groupid' => $discussion->groupid, 'topicid' => $discussion->id), true);

        $act = new stdClass();
        $act->cmd = 'group.discussion.reply';
        $act->actor = $my->id;
        $act->target = 0;
        $act->title = '';
        $act->content = $message;
        $act->app = 'groups.discussion.reply';
        $act->cid = $discussion->id;
        $act->groupid = $group->id;
        $act->group_access = $group->approvals;

        $act->like_id = $wall->id;
        $act->like_type = 'groups.discussion.reply';

        $params = new CParameter('');
        $params->set('action', 'group.discussion.reply');
        $params->set('wallid', $wall->id);
        $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
        $params->set('group_name', $group->name);
        $params->set('discuss_url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $discussion->groupid . '&topicid=' . $discussion->id);

        // Add activity log
        CActivityStream::add($act, $params->toString());


        // Get repliers for this discussion and notify the discussion creator too
        $users = $discussionModel->getRepliers($discussion->id, $group->id);
        $users[] = $discussion->creator;

        // Make sure that each person gets only 1 email
        $users = array_unique($users);

        // The person who post this, should not be getting notification email
        $key = array_search($my->id, $users);

        if ($key !== false && isset($users[$key])) {
            unset($users[$key]);
        }

        // Add notification
        $notifyCreator = true;
        if (!empty($discussion->params)) {
            $paramsOb = json_decode($discussion->params);
            if (isset( $paramsOb->{'notify-creator'} )) {
                if ( $paramsOb->{'notify-creator'} == 0 ) {
                    $notifyCreator = false;
                }
            }
        }
        if ($notifyCreator) {
            $params = new CParameter('');
            $params->set('url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $discussion->groupid . '&topicid=' . $discussion->id);
            $params->set('title', $discussion->title);
            $params->set('discussion', $discussion->title);
            $params->set('discussion_url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $discussion->groupid . '&topicid=' . $discussion->id);
            if($photoThumbnail == '') $params->set('message',  CUserHelper::replaceAliasURL($message));
            else
            {
                $params->set('message', $photoThumbnail);
                $params->set('photo', 'photo');
            }
            CNotificationLibrary::add('groups_discussion_reply', $my->id, $users, JText::_('COM_COMMUNITY_GROUP_NEW_DISCUSSION_REPLY_SUBJECT'), '', 'groups.discussion.reply', $params);
        }

        //email and add notification if user are tagged
        CUserHelper::parseTaggedUserNotification($message, $my, null, array('type' => 'discussion-comment','group_id' => $group->id, 'discussion_id' => $discussion->id ));

        //add user points
        //CFactory::load( 'libraries' , 'userpoints' );
        CUserPoints::assignPoint('group.discussion.reply');

        $config = CFactory::getConfig();
        $order = $config->get('group_discuss_order');
        $order = ($order == 'DESC') ? 'prepend' : 'append';

        // set message
        if ($photoId > 0) {     // photo
            $mess = '<img src="/components/com_community/assets/camera-alt.png"/> <p class="hasicon">Photo</p>';
        } else if (CParsers::linkFetch($message)) {         // links
            $mess = '<img src="/components/com_community/assets/link.png"/> <p class="hasicon">' . $message . '</p>';
        } else {        // message
            $mess = '<p>'.$message.'</p>';
        }
        
        $json['html'] = $wall->content;
        $json['topicid'] = $discussion->id;
        $json['time'] = CTimeHelper::discussionsTimeLapse($wall->date, false);
        $json['message'] = $mess;
        $json['success'] = true;
        
        $this->cacheClean(array(COMMUNITY_CACHE_TAG_ACTIVITIES, COMMUNITY_CACHE_TAG_GROUPS_DETAIL));

        die( json_encode($json) );
    }

    /**
     * Ajax function to save a new wall entry
     *
     * @param message   A message that is submitted by the user
     * @param uniqueId  The unique id for this group
     * @deprecated since 2.4
     *
     * */
    public function ajaxSaveWall($message, $groupId) {
        $filter = JFilterInput::getInstance();
        $message = $filter->clean($message, 'string');
        $groupId = $filter->clean($groupId, 'int');

        $response = new JAXResponse();
        $my = CFactory::getUser();

        $groupModel = CFactory::getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);
        $config = CFactory::getConfig();

        // @rule: If configuration is set for walls in group to be restricted to memebers only,
        // we need to respect this.
        if (!$my->authorise('community.save', 'groups.wall.' . $groupId, $group)) {
            $response->addScriptCall('alert', JText::_('COM_COMMUNITY_ACCESS_FORBIDDEN'));
            return $response->sendResponse();
        }

        $message = strip_tags($message);
        // If the content is false, the message might be empty.
        if (empty($message)) {
            $response->addAlert(JText::_('COM_COMMUNITY_EMPTY_MESSAGE'));
        } else {
            $isAdmin = $groupModel->isAdmin($my->id, $group->id);

            // @rule: Spam checks
            if ($config->get('antispam_akismet_walls')) {
                //CFactory::load( 'libraries' , 'spamfilter' );

                $filter = CSpamFilter::getFilter();
                $filter->setAuthor($my->getDisplayName());
                $filter->setMessage($message);
                $filter->setEmail($my->email);
                $filter->setURL(CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupId));
                $filter->setType('message');
                $filter->setIP($_SERVER['REMOTE_ADDR']);

                if ($filter->isSpam()) {
                    $response->addAlert(JText::_('COM_COMMUNITY_WALLS_MARKED_SPAM'));
                    return $response->sendResponse();
                }
            }

            // Save the wall content
            $wall = CWallLibrary::saveWall($groupId, $message, 'groups', $my, $isAdmin, 'groups,group');

            // Store event will update all stats count data
            $group->updateStats();
            $group->store();

            // @rule: only add the activities of the wall if the group is not private.
            if ($group->approvals == COMMUNITY_PUBLIC_GROUP) {

                $params = new CParameter('');
                $params->set('action', 'group.wall.create');
                $params->set('wallid', $wall->id);
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupId);

                $act = new stdClass();
                $act->cmd = 'group.wall.create';
                $act->actor = $my->id;
                $act->target = 0;
                $act->title = JText::sprintf('COM_COMMUNITY_GROUPS_WALL_POST_GROUP', '{group_url}', $group->name);
                $act->content = $message;
                $act->app = 'groups.wall';
                $act->cid = $wall->id;
                $act->groupid = $group->id;

                // Allow comments
                $act->comment_type = 'groups.wall';
                $act->comment_id = $wall->id;

                CActivityStream::add($act, $params->toString());
            }

            // @rule: Add user points
            //CFactory::load( 'libraries' , 'userpoints' );
            CUserPoints::assignPoint('group.wall.create');

            // @rule: Send email notification to members
            $groupParams = $group->getParams();

            if ($groupParams->get('wallnotification') == '1') {
                $model = $this->getModel('groups');
                $members = $model->getMembers($groupId, null);
                $admins = $model->getAdmins($groupId, null);

                $membersArray = array();

                foreach ($members as $row) {
                    if ($my->id != $row->id) {
                        $membersArray[] = $row->id;
                    }
                }

                foreach ($admins as $row) {
                    if ($my->id != $row->id) {
                        $membersArray[] = $row->id;
                    }
                }
                unset($members);
                unset($admins);

                // Add notification
                //CFactory::load( 'libraries' , 'notification' );

                $params = new CParameter('');
                $params->set('url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupId);
                $params->set('group', $group->name);
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupId);
                $params->set('message', $message);
                CNotificationLibrary::add('groups_wall_create', $my->id, $membersArray, JText::sprintf('COM_COMMUNITY_NEW_WALL_POST_NOTIFICATION_EMAIL_SUBJECT', $my->getDisplayName(), $group->name), '', 'groups.wall', $params);
            }
            $response->addScriptCall('joms.walls.insert', $wall->content);
        }

        $this->cacheClean(array(COMMUNITY_CACHE_TAG_GROUPS, COMMUNITY_CACHE_TAG_ACTIVITIES));

        return $response->sendResponse();
    }

    public function ajaxUpdateCount($type, $groupid) {
        $response = new JAXResponse();
        $my = CFactory::getUser();

        if ($my->id) {
            //CFactory::load( 'libraries' , 'groups' );
            $group = JTable::getInstance('Group', 'CTable');
            $group->load($groupid);

            switch ($type) {
                case 'discussion':
                    $discussModel = CFactory::getModel('discussions');
                    $discussions = $discussModel->getDiscussionTopics($groupid, '10');
                    $totalDiscussion = $discussModel->total;

                    $my->setCount('group_discussion_' . $groupid, $totalDiscussion);

                    break;
                case 'bulletin':
                    $bulletinModel = CFactory::getModel('bulletins');
                    $bulletins = $bulletinModel->getBulletins($groupid);
                    $totalBulletin = $bulletinModel->total;

                    $my->setCount('group_bulletin_' . $groupid, $totalBulletin);
                    break;
            }
        }

        return $response->sendResponse();
    }

    public function ajaxUnbanMember($memberId, $groupId) {
        return $this->updateMemberBan($memberId, $groupId, FALSE);
    }

    /**
     * Ban the member from the group
     * @param type $memberId
     * @param type $groupId
     * @return type
     */
    public function ajaxBanMember($memberId, $groupId) {
        return $this->updateMemberBan($memberId, $groupId, TRUE);
    }

    /**
     * Refactored from AjaxUnBanMember and AjaxBanMember
     */
    public function updateMemberBan($memberId, $groupId, $doBan = true) {
        $filter = JFilterInput::getInstance();
        $groupId = $filter->clean($groupId, 'int');
        $memberId = $filter->clean($memberId, 'int');

        if (!COwnerHelper::isRegisteredUser()) {
            return $this->ajaxBlockUnregister();
        }

        $json = array();
        $my = CFactory::getUser();

        $groupModel = CFactory::getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);

        if (!$my->authorise('community.update', 'groups.member.ban.' . $groupId, $group)) {
            $json['error'] = JText::_('COM_COMMUNITY_PERMISSION_DENIED_WARNING');
        } else {
            $member = JTable::getInstance('GroupMembers', 'CTable');
            $keys = array('groupId' => $group->id, 'memberId' => $memberId);
            $member->load($keys);

            $member->permissions = ($doBan) ? COMMUNITY_GROUP_BANNED : COMMUNITY_GROUP_MEMBER;

            $member->store();

            $group->updateStats();

            $group->store();

            if ($doBan) { //if user is banned, display the appropriate response and color code
                //trigger for onGroupBanned
                $this->triggerGroupEvents('onGroupBanned', $group, $memberId);
                $json['success'] = true;
                $json['message'] = JText::_('COM_COMMUNITY_GROUPS_MEMBER_BEEN_BANNED');
            } else {
                //trigger for onGroupUnbanned
                $this->triggerGroupEvents('onGroupUnbanned', $group, $memberId);
                $json['success'] = true;
                $json['message'] = JText::_('COM_COMMUNITY_GROUPS_MEMBER_BEEN_UNBANNED');
            }
        }

        die( json_encode($json) );
    }

    /**
     * Ajax retreive Featured Group Information
     * @since 2.6
     */
    public function ajaxShowGroupFeatured($groupId) {
        $my = CFactory::getUser();
        $objResponse = new JAXResponse();

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);
        $group->updateStats(); //ensure that stats are up-to-date
        // Get Avatar
        $avatar = $group->getAvatar('avatar');

        // group date
        $config = CFactory::getConfig();
        $groupDate = JHTML::_('date', $group->created, JText::_('DATE_FORMAT_LC'));

        // Get group link
        $groupLink = CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);

        // Get unfeature icon
        $groupUnfeature = '<a class="album-action remove-featured" title="' . JText::_('COM_COMMUNITY_REMOVE_FEATURED') . '" onclick="joms.featured.remove(\'' . $group->id . '\',\'groups\');" href="javascript:void(0);">' . JText::_('COM_COMMUNITY_REMOVE_FEATURED') . '</a>';

        // Get misc data
        $membercount = JText::sprintf((CStringHelper::isPlural($group->membercount)) ? 'COM_COMMUNITY_GROUPS_MEMBER_COUNT_MANY' : 'COM_COMMUNITY_GROUPS_MEMBER_COUNT', $group->membercount);
        $discussion = JText::sprintf((!CStringHelper::isPlural($group->discusscount)) ? 'COM_COMMUNITY_GROUPS_DISCUSSION_COUNT_MANY' : 'COM_COMMUNITY_GROUPS_DISCUSSION_COUNT', $group->discusscount);
        $wallposts = JText::sprintf((CStringHelper::isPlural($group->wallcount)) ? 'COM_COMMUNITY_GROUPS_WALL_COUNT_MANY' : 'COM_COMMUNITY_GROUPS_WALL_COUNT', $group->wallcount);
        $memberCountLink = CRoute::_('index.php?option=com_community&view=groups&task=viewmembers&groupid=' . $group->id);

        // Get like
        $likes = new CLike();
        $likesHTML = $likes->getHTML('groups', $groupId, $my->id);

        $objResponse->addScriptCall('updateGroup', $groupId, $group->name, $group->getCategoryName(), $likesHTML, $avatar, $groupDate, $groupLink, JHTML::_('string.truncate', strip_tags($group->description), 300), $membercount, $discussion, $wallposts, $memberCountLink, $groupUnfeature);
        $objResponse->sendResponse();
    }

    public function edit() {
        $document = JFactory::getDocument();
        $viewType = $document->getType();
        $viewName = JRequest::getCmd('view', $this->getName());
        $config = CFactory::getConfig();

        $view = $this->getView($viewName, '', $viewType);
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $groupId = JRequest::getInt('groupid', '', 'REQUEST');
        $model = $this->getModel('groups');
        $my = CFactory::getUser();
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);

        if (empty($group->id)) {
            return JError::raiseWarning(404, JText::_('COM_COMMUNITY_GROUPS_NOT_FOUND_ERROR'));
        }

        if (!$my->authorise('community.edit', 'groups.' . $groupId, $group)) {
            header('Location: /404.html');
            exit;
//            $errorMsg = $my->authoriseErrorMsg();
//            if ($errorMsg == 'blockUnregister') {
//                return $this->blockUnregister();
//            } else {
//                echo $errorMsg;
//            }
//            return;
        }

        $passedData = new stdClass();
        if ($group->parentid) {
            $parentCircle = JTable::getInstance('Group', 'CTable');
            $parentCircle->load($group->parentid);
            $passedData->parentCircle = $parentCircle;
        } else $passedData->parentCircle = [];

        $blnCircle = $model->getBLNCircle();
        $passedData->blnCircle = $blnCircle;

        if ($jinput->getMethod() == 'POST') {
            JRequest::checkToken() or jexit(JText::_('COM_COMMUNITY_INVALID_TOKEN'));
            $data = JRequest::get('POST');

            $config = CFactory::getConfig();
            $inputFilter = CFactory::getInputFilter($config->get('allowhtml'));
            $description = JRequest::getVar('description', '', 'post', 'string', JREQUEST_ALLOWRAW);
            $data['description'] = $inputFilter->clean($description);

            $summary = JRequest::getVar('summary', '', 'post', 'string', JREQUEST_ALLOWRAW);
            $data['summary'] = $inputFilter->clean($summary);

            $data['unlisted'] = JRequest::getVar('unlisted', 0, 'post', 'int', JREQUEST_ALLOWRAW);
            $courseid = JRequest::getVar('groupcourseid', 0, 'post', 'int', JREQUEST_ALLOWRAW);

            if (!isset($data['approvals'])) {
                $data['approvals'] = 0;
            }

            $approvals = $jinput->post->get('approvals', 0, 'INT');
            $unlisted = $jinput->post->get('unlisted', 0, 'INT');
           
            if (!empty($parentCircle) && $parentCircle->id != $blnCircle->id) {
                if (!$approvals || ($approvals && $parentCircle->unlisted && !$unlisted) ) {
                    echo JText::_('COM_COMMUNITY_INVALID_CIRCLE_TYPE');
                    return;
                }
            }
            if($approvals == 1 && $unlisted == 1){
                $model->changePrivacyToClose($group->id, $group->path);
            }
            if ($group->id == $blnCircle->id) {
                if ($approvals != $group->approvals || $unlisted != $group->unlisted) {
                    echo JText::_('COM_COMMUNITY_INVALID_CIRCLE_TYPE');
                    return;
                }
            }

            $group->bind($data);

            //CFactory::load( 'libraries' , 'apps' );
            $appsLib = CAppPlugins::getInstance();
            $saveSuccess = $appsLib->triggerEvent('onFormSave', array('jsform-groups-forms'));

            if (empty($saveSuccess) || !in_array(false, $saveSuccess)) {
                $redirect = CRoute::_('index.php?option=com_community&view=groups&task=edit&groupid=' . $groupId, false);

                $removeActivity = $config->get('removeactivities');

                if ($removeActivity) {
                    $activityModel = CFactory::getModel('activities');
                    $activityModel->removeActivity('groups', $group->id);
                }

                // validate all fields
                if (empty($group->name)) {
                    $mainframe->redirect($redirect, JText::_('COM_COMMUNITY_GROUPS_EMPTY_NAME_ERROR'));
                    return;
                }

//                if ($model->groupExist($group->name, $group->id)) {
//                    $mainframe->redirect($redirect, JText::_('COM_COMMUNITY_GROUPS_NAME_TAKEN_ERROR'));
//                    return;
//                }

                if (empty($group->description)) {
                    $mainframe->redirect($redirect, JText::_('COM_COMMUNITY_GROUPS_DESCRIPTION_EMPTY_ERROR'));
                    return;
                }

                /*
                if (empty($group->categoryid)) {
                    $mainframe->redirect($redirect, JText::_('COM_COMMUNITY_GROUP_CATEGORY_NOT_SELECTED'));
                    return;
                }
                */

                //Update the moodle description
                if($group->categoryid == 6){
                    $this->update_course($courseid, $data['description']);
                }
               
                // @rule: Retrieve params and store it back as raw string

                $params = $this->_bindParams();
                $oldParams = new CParameter($group->params);
                if ( $oldParams->get('coverPosition') ) {
                    $params->set('coverPosition', $oldParams->get('coverPosition'));
                }

                $group->params = $params->toString();

                $group->updateStats();
                $group->store();

                $params = new CParameter('');
                $params->set('action', 'group.update');
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupId);

                //add user points
                if(CUserPoints::assignPoint('group.updated')){
                    $act = new stdClass();
                    $act->cmd = 'group.update';
                    $act->actor = $my->id;
                    $act->target = 0;
                    $act->title = ''; //JText::sprintf('COM_COMMUNITY_GROUPS_GROUP_UPDATED' , '{group_url}' , $group->name );
                    $act->content = '';
                    $act->app = 'groups.update';
                    $act->cid = $group->id;
                    $act->groupid = $group->id;
                    $act->group_access = $group->approvals;

                    // Add activity logging. Delete old ones
                    CActivityStream::remove($act->app, $act->cid);
                    CActivityStream::add($act, $params->toString());
                }

                // Update photos privacy
                $photoPermission = $group->approvals ? PRIVACY_GROUP_PRIVATE_ITEM : 0;
                $photoModel = CFactory::getModel('photos');
                $photoModel->updatePermissionByGroup($group->id, $photoPermission);

                // Added by NYI
                // LP Circle Info Update
                if($group->categoryid == 7){
                    require_once(JPATH_SITE.'/components/com_hikashop/helpers/lpapi.php');
                    // NYI API - update group information in Hikashop Category
                    $categories = LpApiController::updatecategory($group->id);
                }

                // Reupdate the display.
                $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id, false), JText::_('COM_COMMUNITY_GROUPS_UPDATED'));
                return;
            }
        }
        $this->cacheClean(array(COMMUNITY_CACHE_TAG_GROUPS_CAT, COMMUNITY_CACHE_TAG_GROUPS, COMMUNITY_CACHE_TAG_ACTIVITIES));
//        echo $view->get(__FUNCTION__);
        $this->renderView(__FUNCTION__, $passedData);
    }

    /**
     * Method to display the create group form
     * */
    public function create() {
        $my = CFactory::getUser();
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $config = CFactory::getConfig();

        if ($my->authorise('community.add', 'groups')) {
            $model = CFactory::getModel('Groups');
            if (CLimitsLibrary::exceedDaily('groups')) {
                $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups', false), JText::_('COM_COMMUNITY_GROUPS_LIMIT_REACHED'), 'error');
            }

            $model = $this->getModel('groups');
            $data = new stdClass();
            $data->categories = $model->getCategories();
            $BLNCircle = $model->getBLNCircle();

            if (!empty($BLNCircle)) $data->parentid = $jinput->get->get('parentid', $BLNCircle->id, 'INT');
            else {
                echo JText::_('COM_COMMUNITY_BLN_CIRCLE_DOES_NOT_EXIST');
                return;
            }

            $canCreate = $my->authorise('community.create', 'groups.discussions.' . $data->parentid);
            if (!$canCreate) {
                header('Location: /404.html');
                exit;
//                echo '<div class="message-error">' . JText::_('COM_COMMUNITY_ACCESS_FORBIDDEN') . '</div>';
//                return;
            }

            if ($data->parentid) {
                $parentCircle = JTable::getInstance('Group', 'CTable');
                $parentCircle->load($data->parentid);
                $data->parentCircle = $parentCircle;
            } else $data->parentCircle = [];

            if ($data->parentCircle) {
                $isLearningCircle = $model->getIsCourseGroup($data->parentCircle->id);
                if ($isLearningCircle) {
                    echo JText::_('COM_COMMUNITY_CAN_NOT_CREATE_SUBCIRCLE_OF_LEARNING_CIRCLE');
                    return;
                }
            }

            if ($jinput->post->get('action', '', 'STRING') == 'save') {
                $appsLib = CAppPlugins::getInstance();
                $saveSuccess = $appsLib->triggerEvent('onFormSave', array('jsform-groups-forms'));

                if (empty($saveSuccess) || !in_array(false, $saveSuccess)) {
                    $gid = $this->save();

                    if ($gid !== FALSE) {
                        $mainframe = JFactory::getApplication();

                        $group = JTable::getInstance('Group', 'CTable');
                        $group->load($gid);

                        // Set the user as group member
                        $my->updateGroupList();

                        //lets create the default avatar for the group
                        $avatarAlbum = JTable::getInstance('Album', 'CTable');
                        $avatarAlbum->addAvatarAlbum($group->id, 'group');
                        $coverAlbum = JTable::getInstance('Album', 'CTable');
                        $coverAlbum->addCoverAlbum('group',$group->id);
                        $defaultAlbum = JTable::getInstance('Album', 'CTable');
                        $defaultAlbum->addDefaultAlbum($group->id, 'group');


                        //trigger for onGroupCreate
                        $this->triggerGroupEvents('onGroupCreate', $group);

                        if ($config->get('moderategroupcreation')) {
                            $mainframe->enqueueMessage(JText::sprintf('COM_COMMUNITY_GROUPS_MODERATION_MSG', $group->name), $group->name);
                            $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups'));
                            return;
                        }
                        $type = '&new=true';
                        //change url redirect if save successfuly
                        $url = CRoute::_('index.php?option=com_community&view=groups&task=viewabout&groupid=' . $gid.$type, false);
                        $mainframe->redirect($url);
                        return; 
                    }
                }
            }
        } else {
            $errorMsg = $my->authoriseErrorMsg();
            if ($errorMsg == 'blockUnregister') {
                return $this->blockUnregister();
            } else {
                echo $errorMsg; 
            }
            return;
        }
        //Clear Cache in front page
        $this->cacheClean(array(COMMUNITY_CACHE_TAG_FRONTPAGE, COMMUNITY_CACHE_TAG_GROUPS, COMMUNITY_CACHE_TAG_GROUPS_CAT, COMMUNITY_CACHE_TAG_ACTIVITIES));
        $this->renderView(__FUNCTION__, $data);
    }

    /**
     * A new group has been created
     */
    public function created() {
        $this->renderView(__FUNCTION__);
    }

    private function _bindParams() {
        $params = new CParameter('');
        $groupId = JRequest::getInt('groupid', '', 'REQUEST');
        $mainframe = JFactory::getApplication();
        $redirect = CRoute::_('index.php?option=com_community&view=groups&task=edit&groupid=' . $groupId, false);

        //$discussordering = JRequest::getInt('discussordering', 0, 'POST');
        $params->set('discussordering', 0);
        // Updated by KV team 31Aug 2016
        $groupModel = $this->getModel('groups');
        $is_course_group = $groupModel->getIsCourseGroup($groupId);
        // Set the group photo permission
        if (array_key_exists('photopermission-admin', $_POST)) {
            $params->set('photopermission', GROUP_PHOTO_PERMISSION_ADMINS);

            if (array_key_exists('photopermission-member', $_POST)) {
                $params->set('photopermission', GROUP_PHOTO_PERMISSION_ALL);
            }
        } else {
            $params->set('photopermission', GROUP_PHOTO_PERMISSION_DISABLE);
        }

        // Set the group video permission
        if (array_key_exists('videopermission-admin', $_POST)) {
            $params->set('videopermission', GROUP_VIDEO_PERMISSION_ADMINS);
            if (array_key_exists('videopermission-member', $_POST)) {
                $params->set('videopermission', GROUP_VIDEO_PERMISSION_ALL);
            }
        } else {
            $params->set('videopermission', GROUP_VIDEO_PERMISSION_DISABLE);
        }


        // Set the group event permission
        if (array_key_exists('eventpermission-admin', $_POST)) {
            $params->set('eventpermission', GROUP_EVENT_PERMISSION_ADMINS);

            if (array_key_exists('eventpermission-member', $_POST)) {
                $params->set('eventpermission', GROUP_EVENT_PERMISSION_ALL);
            }
        } else {
            $params->set('eventpermission', GROUP_EVENT_PERMISSION_DISABLE);
        }

        $config = CFactory::getConfig();
        $grouprecentphotos = JRequest::getInt('grouprecentphotos', GROUP_PHOTO_RECENT_LIMIT, 'REQUEST');
        if ($grouprecentphotos < 1 && $config->get('enablephotos')) {
            $mainframe->redirect($redirect, JText::_('COM_COMMUNITY_GROUP_RECENT_ALBUM_SETTING_ERROR'));
            return;
        }
        $params->set('grouprecentphotos', $grouprecentphotos);

        $grouprecentvideos = JRequest::getInt('grouprecentvideos', GROUP_VIDEO_RECENT_LIMIT, 'REQUEST');
        if ($grouprecentvideos < 1 && $config->get('enablevideos')) {
            $mainframe->redirect($redirect, JText::_('COM_COMMUNITY_GROUP_RECENT_VIDEOS_SETTING_ERROR'));
            return;
        }
        $params->set('grouprecentvideos', $grouprecentvideos);

        $grouprecentevent = JRequest::getInt('grouprecentevents', GROUP_EVENT_RECENT_LIMIT, 'REQUEST');
        if ($grouprecentevent < 1) {
            $mainframe->redirect($redirect, JText::_('COM_COMMUNITY_GROUP_RECENT_EVENTS_SETTING_ERROR'));
            return;
        }
        $params->set('grouprecentevents', $grouprecentevent);

        $newmembernotification = JRequest::getInt('newmembernotification', 0, 'POST');
        $params->set('newmembernotification', $newmembernotification);

        $joinrequestnotification = JRequest::getInt('joinrequestnotification', 0, 'POST');
        $params->set('joinrequestnotification', $joinrequestnotification);

        $wallnotification = JRequest::getInt('wallnotification', 0, 'POST');
        $params->set('wallnotification', $wallnotification);

        $groupdiscussionfilesharing = JRequest::getInt('groupdiscussionfilesharing', 0, 'POST');
        $params->set('groupdiscussionfilesharing', $groupdiscussionfilesharing);

        $groupannouncementfilesharing = JRequest::getInt('groupannouncementfilesharing', 0, 'POST');
        $params->set('groupannouncementfilesharing', $groupannouncementfilesharing);
        if($is_course_group) {
            $params->set('course_id', $is_course_group);
        }
        $groupcourseid = JRequest::getInt('groupcourseid', 0, 'POST');
        $params->set('course_id', $groupcourseid);

        return $params;
    }

    private function getCourseDetails($courseid, $username){
        return JoomdleHelperContent::call_method('get_course_info', (int)$courseid, $username);
    }

    // Chris: Call api to update the moodle course
    private function update_course($courseid, $description){

        $coursedetails = $this->getCourseDetails($courseid, $username);
       
        $user = CFactory::getUser();
        $username = $user->get('username');

        $course['id'] = $courseid;
        $course['summary'] =  $description;
        $course['fullname'] = $coursedetails['fullname'];
        $course['shortname'] = $coursedetails['shortname'];
        $course['duration'] = (int)$coursedetails['duration'] / 3600;
        $course['price'] = $coursedetails['price'];
        $course['course_lang'] = $coursedetails['course_lang'];
        $course['startdate'] = $coursedetails['startdate'];
        $course['idnumber'] =  $coursedetails['idnumber'];
        $course['category'] = $coursedetails['cat_id'];
        $course['coursesurvey'] = $coursedetails['coursesurvey'];
        $course['facilitatedcourse'] = $coursedetails['facilitatedcourse'];
        $course['learningoutcomes'] = strip_tags($coursedetails['learningoutcomes']);
        $course['targetaudience'] = $coursedetails['targetaudience'];
        $course['username'] = addslashes(strip_tags($username));
        
        JoomdleHelperContent::call_method('update_course', $course);

    }

    /**
     * Method to save the group
     * @return false if create fail, return the group id if create is successful
     * */
    public function save() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;

        if (JString::strtoupper($jinput->getMethod()) != 'POST') {
            $document = JFactory::getDocument();
            $viewType = $document->getType();
            $viewName = JRequest::getCmd('view', $this->getName());
            $view = $this->getView($viewName, '', $viewType);
            $view->addWarning(JText::_('COM_COMMUNITY_PERMISSION_DENIED_WARNING'));
            return false;
        }
        
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        JRequest::checkToken() or jexit(JText::_('COM_COMMUNITY_INVALID_TOKEN'));

        // Get my current data.
        $my = CFactory::getUser();
        $username = $my->get('id');
        $validated = true;

        $group = JTable::getInstance('Group', 'CTable');
        $model = $this->getModel('groups');

        $name = $jinput->post->get('name', '', 'STRING');
        $keyword = $jinput->post->get('keyword', '', 'STRING');
        $parentid = $jinput->post->get('parentid', '', 'INT');
        $approvals = $jinput->post->get('approvals', 0, 'INT');
        $unlisted = $jinput->post->get('unlisted', 0, 'INT');

        if ($parentid) {
            $parentCircle = JTable::getInstance('Group', 'CTable');
            $parentCircle->load($parentid);
            $pathparent =  $parentCircle->path;
            if ($parentCircle->id) 
                {
                $isLearningCircle = $model->getIsCourseGroup($parentCircle->id);
                if ($isLearningCircle) {
                    echo JText::_('COM_COMMUNITY_CAN_NOT_CREATE_SUBCIRCLE_OF_LEARNING_CIRCLE');
                    return;
                }
//                 if (!$approvals || ($approvals && $parentCircle->unlisted && !$unlisted) ) {
//                    echo(JText::_('COM_COMMUNITY_INVALID_CIRCLE_TYPE'));
//                    return;
//                }
    }
        } else {
            echo JText::_('COM_COMMUNITY_PARENT_CIRCLE_REQUIRED');
            return;
        }

        $config = CFactory::getConfig();
        $inputFilter = CFactory::getInputFilter($config->get('allowhtml'));

        $description = JRequest::getVar('description', '', 'post', 'string', JREQUEST_ALLOWRAW);
        $description = $inputFilter->clean($description);

        $summary = JRequest::getVar('summary', '', 'post', 'string', JREQUEST_ALLOWRAW);
        $summary = $inputFilter->clean($summary);

        // Chris: Forced category to 0
        // $categoryId = $jinput->post->get('categoryid', '', 'INT');
        $categoryId = 0;
        
        $subgroup = $jinput->post->get('subgroup', '', 'INT');
        $website = $jinput->post->get('website', '', 'STRING');
        $grouprecentphotos = $jinput->post->get('grouprecentphotos', '', 'NONE');
        $grouprecentvideos = $jinput->post->get('grouprecentvideos', '', 'NONE');
        $grouprecentevents = $jinput->post->get('grouprecentevents', '', 'NONE');

        // @rule: Test for emptyness
        if (empty($name)) {
            $validated = false;
            $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUPS_EMPTY_NAME_ERROR'), 'error');
        }

        // @rule: Test if group exists
//        if ($model->groupExist($name)) {
//            $validated = false;
//            $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUPS_NAME_TAKEN_ERROR'), 'error');
//        }
        // @rule: Test if group exists same owner
        $userid = $my->get('id');

//        if ($model->groupExistUser($name,$userid)) {
//            $validated = false;
//            $mainframe->enqueueMessage("Circle name already exists. Please choose another name.", 'input-name-error');
//        }

        // @rule: Test for emptyness
        if (empty($description)) {
            $validated = false;
            $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUPS_DESCRIPTION_EMPTY_ERROR'), 'error');
        }
        
        //Chris: removed categoryid
        
        /*
        if (empty($categoryId)) {
            $validated = false;
            $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUP_CATEGORY_NOT_SELECTED'), 'error');
        }
        */
        

        if ($grouprecentphotos < 1 && $config->get('enablephotos') && $config->get('groupphotos')) {
            $validated = false;
            $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUP_RECENT_ALBUM_SETTING_ERROR'), 'error');
        }

        if ($grouprecentvideos < 1 && $config->get('enablevideos') && $config->get('groupvideos')) {
            $validated = false;
            $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUP_RECENT_VIDEOS_SETTING_ERROR'), 'error');
        }

        if ($grouprecentevents < 1 && $config->get('enableevents') && $config->get('group_events')) {
            $validated = false;
            $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUP_RECENT_EVENTS_SETTING_ERROR'), 'error');
        }

        if ($validated) {
            // Assertions
            // Category Id must not be empty and will cause failure on this group if its empty.

            // Chris: removed categoryid

            // CError::assert($categoryId, '', '!empty', __FILE__, __LINE__);

            // @rule: Retrieve params and store it back as raw string
            $params = $this->_bindParams();

            $now = new JDate();

            // Bind the post with the table first
            $group->name = $name;
            $group->description = $description;
            $group->summary= $summary;
            //Chris: removed categoryid
            //$group->categoryid = $categoryId;
            $group->keyword = $keyword;
            $group->parentid = $parentid;
            $group->subgroup = $subgroup;
            $group->website = $website;
            $group->ownerid = $my->id;
            $group->created = $now->toSql();
            $group->unlisted = $unlisted;
            $group->approvals = $approvals;
            
            $group->params = $params->toString();

            // @rule: check if moderation is turned on.
            $group->published = ( $config->get('moderategroupcreation') ) ? 0 : 1;

            // we here save the group 1st. else the group->id will be missing and causing the member connection and activities broken.
            $group->store();
            
            //store path
            $group->path = $pathparent.'/'.$group->id;
            $group->store();
            

            // Since this is storing groups, we also need to store the creator / admin
            // into the groups members table
            $member = JTable::getInstance('GroupMembers', 'CTable');
            $member->groupid = $group->id;
            $member->memberid = $group->ownerid;

            // Creator should always be 1 as approved as they are the creator.
            $member->approved = 1;

            // @todo: Setup required permissions in the future
            $member->permissions = '1';
            $member->store();

            // @rule: Only add into activity once a group is created and it is published.
            if ($group->published && !$group->unlisted) {

                $act = new stdClass();
                $act->cmd = 'group.create';
                $act->actor = $my->id;
                $act->target = 0;
                //$act->title       = JText::sprintf('COM_COMMUNITY_GROUPS_NEW_GROUP' , '{group_url}' , $group->name );
                $act->title = JText::sprintf('COM_COMMUNITY_GROUPS_NEW_GROUP_CATEGORY', '{group_url}', $group->name, '{category_url}', $group->getCategoryName());
                $act->content = ( $group->approvals == 0) ? $group->description : '';
                $act->app = 'groups';
                $act->cid = $group->id;
                $act->groupid = $group->id;
                $act->group_access = $group->approvals;

                // Allow comments
                $act->comment_type = 'groups.create';
                $act->like_type = 'groups.create';
                $act->comment_id = CActivities::COMMENT_SELF;
                $act->like_id = CActivities::LIKE_SELF;

                // Store the group now.
                $group->updateStats();
                $group->store();

                $params = new CParameter('');
                $params->set('action', 'group.create');
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                //Chris: removed categoryid
                $params->set('category_url', 'index.php?option=com_community&view=groups&task=display&categoryid=' . $group->categoryid);

                // Add activity logging
                CActivityStream::add($act, $params->toString());
            }
            
            /*
             *  Change with UX scrubbing
             *  Upload cover and avatar
             */
            
            if(isset($_FILES)) {
                // need a new function
                if(!empty($_FILES['cover']['name'])) {
                    $uploaded = CGroups::setCoverPhoto($_FILES['cover'], $group->id, 'group');
                }
                if(!empty($_FILES['avatar']['name'])) {
                    $uploaded = CGroups::setAvatarPhoto($_FILES['avatar'], $group->id, 'group');
                }
            }

            // if need approval should send email notification to admin
            if ($config->get('moderategroupcreation')) {
                $title_email = JText::_('COM_COMMUNITY_EMAIL_NEW_GROUP_NEED_APPROVAL_TITLE');
                $message_email = JText::sprintf('COM_COMMUNITY_EMAIL_NEW_GROUP_NEED_APPROVAL_MESSAGE', $my->getDisplayName(), $group->name);
                $from = $mainframe->getCfg('mailfrom'); //$jConfig->getValue( 'mailfrom' );
                $to = $config->get('notifyMaxReport');
                CNotificationLibrary::add('groups_create', $from, $to, $title_email, $message_email, '', '');
            }

            //add user points
            CUserPoints::assignPoint('group.create');

            $validated = $group->id;
        }
        return $validated;
    }

    /**
     * Method to search for a group based on the parameter given
     * in a POST request
     * */
    public function search() {
        $my = CFactory::getUser();
        $mainframe = JFactory::getApplication();
        $config = CFactory::getConfig();

        if (!$my->authorise('community.view', 'groups.search')) {
            $errorMsg = $my->authoriseErrorMsg();
            if ($errorMsg == 'blockUnregister') {
                $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_RESTRICTED_ACCESS'), 'notice');
                return $this->blockUnregister();
            } else {
                echo $errorMsg;
            }
            return;
        }

        $this->renderView(__FUNCTION__);
    }

    /**
     * Ajax function call that allows user to leave group
     *
     * @param groupId   The groupid that the user wants to leave from the group
     *
     * */
    public function leaveGroup() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $groupId = $jinput->post->get('groupid', '', 'INT');

        CError::assert($groupId, '', '!empty', __FILE__, __LINE__);

        $model = $this->getModel('groups');
        $my = CFactory::getUser();

        if (!$my->authorise('community.leave', 'groups.' . $groupId)) {
            $errorMsg = $my->authoriseErrorMsg();
            if ($errorMsg == 'blockUnregister') {
                return $this->blockUnregister();
            }
        }

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);

        $data = new stdClass();
        $data->groupid = $groupId;
        $data->memberid = $my->id;

        $model->removeMember($data);

        //trigger for onGroupLeave
        $this->triggerGroupEvents('onGroupLeave', $group, $my->id);

        //add user points
        CUserPoints::assignPoint('group.leave');

        $mainframe = JFactory::getApplication();

        $my->updateGroupList();

        // STore the group and update the data
        $group->updateStats();
        $group->store();

        //delete invitation
        $invitation = JTable::getInstance('Invitation', 'CTable');
        $invitation->deleteInvitation($groupId, $my->id, 'groups,inviteUsers');

        $this->cacheClean(array(COMMUNITY_CACHE_TAG_GROUPS));

        $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups', false), JText::_('COM_COMMUNITY_GROUPS_LEFT_MESSAGE'));
    }

    /**
     * @since 2.6
     * mygroupupdates page
     *
     */
    public function myGroupUpdate() {
        $config = CFactory::getConfig();
        $my = CFactory::getUser();

        if (!$my->authorise('community.view', 'groups.list')) {
            echo JText::_('COM_COMMUNITY_GROUPS_DISABLE');
            return;
        }

        $this->renderView(__FUNCTION__);
    }

    /**
     * Method is used to receive POST requests from specific user
     * that wants to join a group
     *
     * @return  void
     * */
    public function joinGroup() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;

        $groupId = $jinput->post->get('groupid', '', 'INT'); //JRequest::getVar('groupid' , '' , 'POST');
        // Add assertion to the group id since it must be specified in the post request
        CError::assert($groupId, '', '!empty', __FILE__, __LINE__);

        // Get the current user's object
        $my = CFactory::getUser();

        if (!$my->authorise('community.join', 'groups.' . $groupId)) {
            return $this->blockUnregister();
        }

        // Load necessary tables
        $groupModel = CFactory::getModel('groups');

        if ($groupModel->isMember($my->id, $groupId)) {

            $url = CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupId, false);
            $mainframe->redirect($url, JText::_('COM_COMMUNITY_GROUPS_ALREADY_MEMBER'));
        } else {
            $url = CRoute::getExternalURL('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupId, false);

            $member = $this->_saveMember($groupId);
            $this->cacheClean(array(COMMUNITY_CACHE_TAG_GROUPS, COMMUNITY_CACHE_TAG_ACTIVITIES));
            if ($member->approved) {
                $mainframe->redirect($url, JText::_('COM_COMMUNITY_GROUPS_JOIN_SUCCESS'));
            }
            $mainframe->redirect($url, JText::_('COM_COMMUNITY_GROUPS_APPROVAL_NEED'));
        }
    }

    private function _saveMember($groupId) {

        $group = JTable::getInstance('Group', 'CTable');
        $member = JTable::getInstance('GroupMembers', 'CTable');

        $group->load($groupId);
        $params = $group->getParams();
        $my = CFactory::getUser();

        // Set the properties for the members table
        $member->groupid = $group->id;
        $member->memberid = $my->id;

        // @rule: If approvals is required, set the approved status accordingly.
        $member->approved = ( $group->approvals == COMMUNITY_PRIVATE_GROUP ) ? '0' : 1;

        // @rule: Special users should be able to join the group regardless if it requires approval or not
        $member->approved = COwnerHelper::isCommunityAdmin() ? 1 : $member->approved;

        // @rule: Invited users should be able to join the group immediately.
        $groupInvite = JTable::getInstance('GroupInvite', 'CTable');
        $keys = array('groupid' => $groupId, 'userid' => $my->id);
        if ($groupInvite->load($keys)) {
            $member->approved = 1;
        }

        //@todo: need to set the privileges
        $member->permissions = '0';
        $member->date = time();
        $member->store();
        $owner = CFactory::getUser($group->ownerid);

        // Update user group list
        $my->updateGroupList();

        // Test if member is approved, then we add logging to the activities.
        if ($member->approved) {
            CGroups::joinApproved($groupId, $my->id);

            //trigger for onGroupJoin
            $this->triggerGroupEvents('onGroupJoin', $group, $my->id);
        }
        return $member;
    }

    public function uploadAvatar() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;

        $document = JFactory::getDocument();
        $viewType = $document->getType();
        $viewName = JRequest::getCmd('view', $this->getName());
        $view = $this->getView($viewName, '', $viewType);
        $my = CFactory::getUser();
        $config = CFactory::getConfig();

        $groupid = $jinput->request->get('groupid', '', 'INT');
        $data = new stdClass();
        $data->id = $groupid;

        $groupsModel = $this->getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);

        if (!$my->authorise('community.upload', 'groups.avatar.' . $groupid, $group)) {
            $errorMsg = $my->authoriseErrorMsg();
            if (!$errorMsg) {
                return $this->blockUnregister();
            } else {
                echo $errorMsg;
            }
            return;
        }

        if ($jinput->getMethod() == 'POST') {
            //CFactory::load( 'helpers' , 'image' );
            $fileFilter = new JInput($_FILES);
            $file = $fileFilter->get('filedata', '', 'array');
            //$file     = JRequest::getVar('filedata' , '' , 'FILES' , 'array');

            if (!CImageHelper::isValidType($file['type'])) {
                $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_IMAGE_FILE_NOT_SUPPORTED'), 'error');
                $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id . '&task=uploadAvatar', false));
            }

            //CFactory::load( 'libraries' , 'apps' );
            $appsLib = CAppPlugins::getInstance();
            $saveSuccess = $appsLib->triggerEvent('onFormSave', array('jsform-groups-uploadavatar'));

            if (empty($saveSuccess) || !in_array(false, $saveSuccess)) {
                if (empty($file)) {
                    $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_NO_POST_DATA'), 'error');
                } else {
                    $uploadLimit = (double) $config->get('maxuploadsize');
                    $uploadLimit = ( $uploadLimit * 1024 * 1024 );

                    // @rule: Limit image size based on the maximum upload allowed.
                    if (filesize($file['tmp_name']) > $uploadLimit && $uploadLimit != 0) {
                        $mainframe->enqueueMessage(JText::sprintf('COM_COMMUNITY_VIDEOS_IMAGE_FILE_SIZE_EXCEEDED_MB',CFactory::getConfig()->get('maxuploadsize')), 'error');
                        $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=uploadavatar&groupid=' . $group->id, false));
                    }

                    if (!CImageHelper::isValid($file['tmp_name'])) {
                        $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_IMAGE_FILE_NOT_SUPPORTED'), 'error');
                    } else {
                        // @todo: configurable width?
                        $imageMaxWidth = 160;

                        // Get a hash for the file name.
                        $fileName = JApplication::getHash($file['tmp_name'] . time());
                        $hashFileName = JString::substr($fileName, 0, 24);

                        // @todo: configurable path for avatar storage?
                        $storage = JPATH_ROOT . '/' . $config->getString('imagefolder') . '/avatar/groups';
                        $storageImage = $storage . '/' . $hashFileName . CImageHelper::getExtension($file['type']);
                        $storageThumbnail = $storage . '/thumb_' . $hashFileName . CImageHelper::getExtension($file['type']);
                        $image = $config->getString('imagefolder') . '/avatar/groups/' . $hashFileName . CImageHelper::getExtension($file['type']);
                        $thumbnail = $config->getString('imagefolder') . '/avatar/groups/' . 'thumb_' . $hashFileName . CImageHelper::getExtension($file['type']);

                        // Generate full image
                        if (!CImageHelper::resizeProportional($file['tmp_name'], $storageImage, $file['type'], $imageMaxWidth)) {
                            $mainframe->enqueueMessage(JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $storageImage), 'error');
                        }

                        // Generate thumbnail
                        if (!CImageHelper::createThumb($file['tmp_name'], $storageThumbnail, $file['type'])) {
                            $mainframe->enqueueMessage(JText::sprintf('COM_COMMUNITY_ERROR_MOVING_UPLOADED_FILE', $storageThumbnail), 'error');
                        }

                        // Autorotate avatar based on EXIF orientation value
                        if ($file['type'] == 'image/jpeg') {
                            $orientation = CImageHelper::getOrientation($file['tmp_name']);
                            CImageHelper::autoRotate($storageImage, $orientation);
                            CImageHelper::autoRotate($storageThumbnail, $orientation);
                        }

                        // Update the group with the new image
                        $groupsModel->setImage($groupid, $image, 'avatar');
                        $groupsModel->setImage($groupid, $thumbnail, 'thumb');

                        // add points and generate stream if needed
                        $generateStream = CUserPoints::assignPoint('group.avatar.upload');
                        // @rule: only add the activities of the news if the group is not private.
                        if ($group->approvals == COMMUNITY_PUBLIC_GROUP && $generateStream) {
                            $url = CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupid);
                            $act = new stdClass();
                            $act->cmd = 'group.avatar.upload';
                            $act->actor = $my->id;
                            $act->target = 0;
                            $act->title = JText::sprintf('COM_COMMUNITY_GROUPS_NEW_GROUP_AVATAR', '{group_url}', $group->name);
                            $act->content = '<img src="' . JURI::root(true) . '/' . $thumbnail . '" style="border: 1px solid #eee;margin-right: 3px;" />';
                            $act->app = 'groups';
                            $act->cid = $group->id;
                            $act->groupid = $group->id;

                            $params = new CParameter('');
                            $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);


                            CActivityStream::add($act, $params->toString());
                        }

                        $mainframe = JFactory::getApplication();
                        $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupid, false), JText::_('COM_COMMUNITY_GROUPS_AVATAR_UPLOADED'));
                        exit;
                    }
                }
            }
        }
        //ClearCache in frontpage
        $this->cacheClean(array(COMMUNITY_CACHE_TAG_FRONTPAGE, COMMUNITY_CACHE_TAG_GROUPS, COMMUNITY_CACHE_TAG_FEATURED, COMMUNITY_CACHE_TAG_ACTIVITIES));

        echo $view->get(__FUNCTION__, $data);
    }

    /**
     * Method that loads the viewing of a specific group
     * */
    public function viewGroup() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $config = CFactory::getConfig();
        $my = CFactory::getUser();
        if (!$my->authorise('community.view', 'groups.list')) {
            echo JText::_('COM_COMMUNITY_GROUPS_DISABLE');
            return;
        }

        // Load the group table.
        $groupid = JRequest::getInt('groupid', '');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        $notification = JRequest::getInt('notification', '');

        $activityId = $jinput->get->get('actid', 0, 'INT');
        if($activityId){
            $activity = JTable::getInstance('Activity', 'CTable');
            $activity->load($activityId);
            JRequest::setVar('userid', $activity->actor);
            $userid = $activity->actor;
        }
        
        // Chris: redirect to about page
        if($notification == 1){
            $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewabout&groupid=' . $group->id .'&notification=1', false));
        } else {
            $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewabout&groupid=' . $group->id, false));
        }
        if (empty($group->id)) {
            return JError::raiseWarning(404, JText::_('COM_COMMUNITY_GROUPS_NOT_FOUND_ERROR'));
        }

        $groupModel = CFactory::getModel('groups');
        if($group->unlisted && !$groupModel->isMember($my->id, $group->id) && !$groupModel->isInvited($my->id, $group->id) && !COwnerHelper::isCommunityAdmin()){
                return JError::raiseWarning(403, JText::_('COM_COMMUNITY_GROUPS_UNLISTED_ERROR'));
        }
        if($activityId) {
            $activity->group = $group;
            echo $this->renderView('singleActivity', $activity);
        } else {
            $this->renderView(__FUNCTION__, $group);
        }
    }

    /**
     * Show only current user group
     */
    public function mygroups() {

        $my = CFactory::getUser();

        if (!$my->authorise('community.view', 'groups.my')) {
            $errorMsg = $my->authoriseErrorMsg();
            if ($errorMsg == 'blockUnregister') {
                return $this->blockUnregister();
            } else {
                echo $errorMsg;
            }
            return;
        }

        $userid = JRequest::getInt('userid', null);
        $this->renderView(__FUNCTION__, $userid);
    }

    public function myinvites() {
        $config = CFactory::getConfig();
        $my = CFactory::getUser();

        if (!$my->authorise('community.view', 'groups.invitelist')) {
            $errorMsg = $my->authoriseErrorMsg();
            echo $errorMsg;
            return;
        }
        $this->renderView(__FUNCTION__);
    }

    public function viewmembers() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;

        $config = CFactory::getConfig();
        $my = CFactory::getUser();
        $data = new stdClass();
        $data->id = $jinput->get('groupid', '', 'INT');

        if (!$my->authorise('community.view', 'groups.member.' . $data->id)) {
            $errorMsg = $my->authoriseErrorMsg();
            echo $errorMsg;
            return;
        }

        $this->renderView(__FUNCTION__, $data);
    }

    //(Chris) added view for administer
    public function viewadminister() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;

        $config = CFactory::getConfig();
        $my = CFactory::getUser();
        $data = new stdClass();
        $data->id = $jinput->get('groupid', '', 'INT');

        if (!$my->authorise('community.view', 'groups.member.' . $data->id)) {
            $errorMsg = $my->authoriseErrorMsg();
            echo $errorMsg;
            return;
        }

        $this->renderView(__FUNCTION__, $data);
    }

    public function ajaxGetMoreMembers() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $groupsModel = CFactory::getModel('groups');
        $groupid = $jinput->post->get('groupid', '', 'INT');
        $limit = $jinput->post->get('limit', 5, 'INT');
        $limitstart = $jinput->post->get('limitstart', 0, 'INT');

        if ($limitstart != 0) {
            $ajaxSend = $jinput->post->get('ajaxSend', 0, 'INT');
            if ($ajaxSend) {
                $my = CFactory::getUser();

                $group = JTable::getInstance('Group', 'CTable');
                $group->load($groupid);
                $type = $jinput->get->get('approve', '', 'NONE');
                $isOwner = $groupsModel->isCreator($my->id, $group->id);
                $isMember = $group->isMember($my->id);

                $blnCircle = $groupsModel->getBLNCircle();
                $isBLNCircle = $blnCircle->id == $group->id ? true : false;

                $isAdmin = $groupsModel->isAdmin($my->id, $groupid);
                $isSuperAdmin = COwnerHelper::isCommunityAdmin();

                $showCircleAdmins = false;
                // get user is member
                $members = $groupsModel->getMembers($groupid, $limit, true, false, $showCircleAdmins, false, $limitstart);
                $membersCount = count($groupsModel->getMembers($groupid, 0, true, false, $showCircleAdmins, false, null));

                $membersList = array();
                foreach ($members as $member) {
                    $user = CFactory::getUser($member->id);

                    $user->friendsCount = $user->getFriendCount();
                    $user->approved = $member->approved;
                    $user->date = $member->date;
                    $user->isMe = ( $my->id == $member->id ) ? true : false;
//                    $user->isAdmin = $groupsModel->isAdmin($user->id, $group->id);
                    $user->isOwner = ( $member->id == $group->ownerid ) ? true : false;

                    // Check user's permission
                    $groupmember = JTable::getInstance('GroupMembers', 'CTable');
                    $keys['groupId'] = $group->id;
                    $keys['memberId'] = $member->id;
                    $groupmember->load($keys);
                    $user->isBanned = ( $groupmember->permissions == COMMUNITY_GROUP_BANNED ) ? true : false;

                    $membersList[] = $user;
                }

                $membersListHTML = '';
                if ($membersList) {
                    $tmpl = new CTemplate();
                    $membersListHTML = $tmpl
                        ->set('group', $group)
                        ->set('type', $type)
                        ->set('isOwner', $isOwner)
                        ->set('isAdmin', $isAdmin)
                        ->set('isMember', $isMember)
                        ->set('isSuperAdmin', $isSuperAdmin)
                        ->set('isBLNCircle', $isBLNCircle)
                        ->set('members', $membersList)
                        ->set('membersCount', $membersCount)
                        ->fetch('groups/memberslist');
                }

                $return = new stdClass();
                $return->html = $membersListHTML;

                echo json_encode($return);
                die;
            }
        }
    }
    // (Chris) added for the about page
    public function viewabout() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;

        $config = CFactory::getConfig();
        $my = CFactory::getUser();
        $data = new stdClass();
        $data->id = $jinput->get('groupid', '', 'INT');
        $data->limit = $jinput->get('limit', 5, 'INT');
        $data->limitstart = $jinput->get('limitstart', 0, 'INT');

        if (!$my->authorise('community.view', 'groups.member.' . $data->id)) {
            $errorMsg = $my->authoriseErrorMsg();
            echo $errorMsg;
            return;
        }

        $this->renderView(__FUNCTION__, $data);
    }

    public function assignRole() {
        $mainframe = JFactory::getApplication();
        $document = JFactory::getDocument();
        $jinput = $mainframe->input;

        $my = CFactory::getUser();

        $viewType = $document->getType();
        $viewName = JRequest::getCmd('view', $this->getName());
        $view = $this->getView($viewName, '', $viewType);

        $data = new stdClass();
        $data->onlineFriendsOnly = false;
        $data->role = $jinput->getString('r');
        $data->courseid = $jinput->get('coid', '', 'INT');
        $data->groupid = $jinput->get('grid', '', 'INT');

        if (!$my->authorise('community.view', 'groups.member.' . $data->groupid)) {
            $errorMsg = $my->authoriseErrorMsg();
            echo $errorMsg;
            return;
        }

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($data->groupid);

        $params = json_decode($group->params);

        if ($params->course_id != 0 && !JHelperLGT::checkCreatorCourse($params->course_id)) {
            header('Location: /404.html');
            exit;
        }
        $modelGroups = CFactory::getModel('groups');
        $modelFriends = CFactory::getModel('friends');

        $isSuperAdmin = COwnerHelper::isCommunityAdmin();
        $isAdmin = $modelGroups->isAdmin($my->id, $data->groupid);
        $isOwner = ( $my->id == $group->ownerid ) ? true : false;

        if ( !$isSuperAdmin && !$isOwner && !$isAdmin ) {
            JFactory::handleErrors();
        }

        $data->isLPCircle = ($group->moodlecategoryid > 0) ? true : false;

        $data->isLearningCircle = false;
        $data->isCommunityCircle = false;
        if (!$data->isLPCircle) {
            $data->courseid = $modelGroups->getIsCourseGroup($data->groupid);
            $data->isLearningCircle = $data->courseid ? true : false;
            $data->isCommunityCircle = $data->isLearningCircle ? false : true;
        } else {
            if ($data->role == 'cc') {
                $hasContentCreator = $modelGroups->isCourseHas($data->courseid, 'editingteacher');
                if ($hasContentCreator) {
                    $view->noAccess();
                    return;
                }
            }
        }

        $groupAdmins = $group->getAdmins();
        $groupAdminsIds = array_map(function($admin) {
            return $admin->id;
        }, $groupAdmins);

        $members = $modelGroups->getMembers($data->groupid);
        $friends = $modelFriends->getFriends($my->id, 'latest', true, 'all');

        $membersList = [];
        foreach ($members as $member) {
            $membersList[$member->id] = CFactory::getUser($member->id);
        }

        if ($data->isLearningCircle) {
            // Get members of social circle
            $groupshare = $modelGroups->getShareGroups($data->courseid);
            if (count($groupshare) > 0) {
                foreach ($groupshare as $gs) {
                    $membergroups = $modelGroups->getMembers($gs->groupid, 0, true, false, true);
                    foreach ($membergroups as $m) {
                        if (!array_key_exists($m->id, $membersList)) {
                            $membersList[$m->id] = CFactory::getUser($m->id);
                        }
                    }
                }
            }
        }

        $data->listUsers = [];
        $data->arrAssignedUsersIds = [];

        switch ($data->role) {
            case 'cc':
                $data->listUsers = $friends;
                break;
            case 'mn':
                $data->arrAssignedUsersIds = $groupAdminsIds;

                foreach ($friends as $key => $friend) {
                    if (in_array($friend->id, $data->arrAssignedUsersIds)) unset($friends[$key]);
                }

                if ($data->isLearningCircle) {
                    foreach ($data->arrAssignedUsersIds as $k => $v) {
                        unset($membersList[$v]);
                    }
                }

                if ($data->isLPCircle) {
                    $data->listUsers = $friends;
                } else {
                    $data->listUsers = $membersList;
                }
                break;
            case 'fa':
                $data->arrAssignedUsersIds = $groupAdminsIds;

                foreach ($friends as $key => $friend) {
                    if (in_array($friend->id, $data->arrAssignedUsersIds)) unset($friends[$key]);
                }
                if ($data->isLearningCircle) {
                    foreach ($data->arrAssignedUsersIds as $k => $v) {
                        unset($membersList[$v]);
                    }
                }

                $data->listUsers = $membersList;
                break;
            case 'ln':
                foreach ($members as $m) {
                    if (!in_array($m->id, $groupAdminsIds)) $data->arrAssignedUsersIds[] = $m->id;
                }
                if ($data->isLearningCircle) {
                    foreach ($groupAdminsIds as $k => $v) {
                        unset($membersList[$v]);
                    }
                }
                $data->listUsers = $membersList;
                break;
            default:
                JFactory::handleErrors();
                break;
        }

        $data->members = $membersList;
        $data->friends = $friends;

        $this->renderView(__FUNCTION__, $data);
    }

    public function handleAssignRole() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;

        $courseid = $jinput->post->get('courseid', '', 'INT');
        $arrAssignedUsers = $jinput->request->get('assignedUsers', '', 'ARRAY');
        $groupid = $jinput->post->get('groupid', '', 'INT');
        $role = $jinput->getString('role');
        $mapage = $jinput->post->get('mapage', 0, 'INT');

        $my = CFactory::getUser();
        $username = $my->get('username');

        $modelGroups = CFactory::getModel('groups');
        $modelFriends = $this->getModel('friends');
        $group = JTable::getInstance( 'Group' , 'CTable' );
        $group->load( $groupid );

        $isSuperAdmin = COwnerHelper::isCommunityAdmin();
        $isAdmin = $modelGroups->isAdmin($my->id, $groupid);
        $isOwner = ( $my->id == $group->ownerid ) ? true : false;

        if ( !$isSuperAdmin && !$isOwner && !$isAdmin ) {
            return;
        }

        $course_group = $modelGroups->getIsCourseGroup($group->id);
        // Escape the output
        $group->name  = CStringHelper::escape($group->name);
        $isLPCircle = ($group->moodlecategoryid > 0) ? true : false;
        if (!$isLPCircle) {
            $learningCircleCourseId = $modelGroups->getIsCourseGroup($groupid);
            if ($courseid != $learningCircleCourseId) {
                echo 'Invalid Courseid.';
                return;
            }
            $isLearningCircle = $learningCircleCourseId ? true : false;
            $isCommunityCircle = $isLearningCircle ? false : true;
        } else {
            if ($role == 'cc') {
                $hasContentCreator = $modelGroups->isCourseHas($courseid, 'editingteacher');
                if ($hasContentCreator) {
                    return;
                }
            }
        }

        switch ($role) {
            case 'cc':
                $role = 'contentCreator';

                foreach ($arrAssignedUsers as $au) {
                    $user = JFactory::getUser($au);
                    if (!$user) continue;
                    $modelFriends->assignnewrole($groupid, $user->id, $role);
                    $ob = new stdClass();
                    $ob->frname = $user->username;
                    $ob->rid = 3;
                    $ob->status = 1;
                    $datacreator[] = $ob;
                }

                if (!empty($datacreator)) {
                    $datacreator = json_encode($datacreator);
                    require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
                    $response = JoomdleHelperContent::call_method('set_user_role', 1, (int)$courseid, $username, $datacreator);
                }

                break;
            case 'mn':
                $role = 'manager';
                $member = JTable::getInstance('GroupMembers', 'CTable');

                $datamanager = [];
                foreach ($arrAssignedUsers as $au) {
                    $user = JFactory::getUser($au);
                    if (!$user) continue;

                    if (!$isLPCircle) {
//                        if (!$group->isMember($user->id)) continue;

                        if ($group->isMember($user->id)) {
                        $keys = array('groupId' => $group->id, 'memberId' => $user->id);
                        $member->load($keys);
                        $member->permissions = 1;

                        $member->store();
                        } else {
                            $mem = JTable::getInstance('GroupMembers', 'CTable');
                            $mem->groupid = $group->id;
                            $mem->memberid = $user->id;
                            $mem->approved = 1;
                            $mem->permissions = '1';
                            $mem->store();
                            $group->updateStats();
                            $group->store();
                        }

                        if ($isLearningCircle) {
                            $ob = new stdClass();
                            $ob->frname = $user->username;
                            $ob->rid = 1; // roleid for manager
                            $ob->status = 1;
                            $datamanager[] = $ob;
                            
                            // Set role learner
                            $ob2 = new stdClass();
                            $ob2->frname = $user->username;
                            $ob2->rid = 5;
                            $ob2->status = 1;
                            $datamanager[] = $ob2;
                        }
                    } else {
                        $member->groupid = $group->id;
                        $member->memberid = $user->id;
                        $member->approved = 1;
                        $member->permissions = '1';
                        $member->store();
                        $group->updateStats();
                        $group->store();

                        $ob = new stdClass();
                        $ob->user = $user->username;
                        $ob->roleid = 1; // roleid for manager
                        $ob->status = 1;
                        $datamanager[] = $ob;
                    }
                }
                if ($isLearningCircle && !empty($datamanager)) {
                    $datamanager = json_encode($datamanager);
                    $response = JoomdleHelperContent::call_method('set_user_role', 1, (int)$courseid, $username, $datamanager);
                } else if ($isLPCircle) {
                    $datamanager = json_encode($datamanager);
                    $response = JoomdleHelperContent::call_method('set_user_category_role', $username, (int)$group->moodlecategoryid, $datamanager);
                }
                break;
            case 'fa':
                $role = 'facilitator';
                if (!$isLearningCircle) {
                    echo 'This circle is not learning circle.';
                    return;
                }
//                $this->unassignrole($courseid, $facilitator, $manager);

                foreach ($arrAssignedUsers as $facilitator) {
                    $ob = new stdClass();
                    $ob->frname = JFactory::getUser($facilitator)->username;
                    $ob->rid = 4; //role id for facilitator
                    $ob->status = 1;
                    $datafacilitator[] = $ob;
                    
                    // Set role learner
                    $ob2 = new stdClass();
                    $ob2->frname = JFactory::getUser($facilitator)->username;
                    $ob2->rid = 5;
                    $ob2->status = 1;
                    $datafacilitator[] = $ob2;
                    
                    $user = JFactory::getUser($facilitator);
                    if (!$user) continue;
                    $mem = JTable::getInstance('GroupMembers', 'CTable');
                    $mem->groupid = $group->id;
                    $mem->memberid = $user->id;
                    $mem->approved = 1;
                    $mem->permissions = '1';
                    $mem->store();
                    $group->updateStats();
                    $group->store();
                }

                require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
                if (!empty($datafacilitator)){
                    $datafacilitator = json_encode($datafacilitator);
                    $response = JoomdleHelperContent::call_method('set_user_role', 1, (int)$course_group, $username, $datafacilitator);
                }

                break;
            case 'ln':
                $learnerids = [];

                $groupMembers = $modelGroups->getMembers($groupid, 0, true, false, false);
                $groupMembersIds = array_map(function($member) {
                    return $member->id;
                }, $groupMembers);
                $dataLearners = [];

                foreach ($arrAssignedUsers as $learner) {
                    $learnerUser = JFactory::getUser($learner);
                    $learnerUsername = $learnerUser->username;
                    $learnerids[] = $learnerUser->id;

                    $db = JFactory::getDbo();
                    $query = $db->getQuery(true);
                    $query->select('*')->from($db->getPrefix() . 'community_groups_members')->where('groupid = ' . $group->id . ' AND memberid = ' . $learnerUser->id . ' AND approved = 1');
                    $db->setQuery($query);
                    $result = $db->loadObjectList();

                    if (empty($result)) {
                        $mem = JTable::getInstance('GroupMembers', 'CTable');
                        $mem->groupid = $group->id;
                        $mem->memberid = $learnerUser->id;
                        $mem->approved = 1;
                        $mem->permissions = '0';
                        $mem->store();

                        $ob = new stdClass();
                        $ob->frname = $learnerUsername;
                        $ob->rid = 5;
                        $ob->status = 1;
                        $dataLearners[] = $ob;
                    }
                }

                foreach ($groupMembers as $member) {
                    if (empty($learnerids) || (!empty($learnerids) && !in_array($member->id, $learnerids))) {
                        $learnerUser = JFactory::getUser($member->id);
                        $learnerUsername = $learnerUser->username;

                        $db = JFactory::getDbo();
                        $query = $db->getQuery(true);
                        $query->delete($db->getPrefix() . 'community_groups_members');
                        $query->where('groupid = ' . $group->id . ' AND memberid = ' . $learnerUser->id . ' AND approved = 1 AND permissions = 0');
                        $db->setQuery($query);
                        $db->execute();

                        $ob = new stdClass();
                        $ob->frname = $learnerUsername;
                        $ob->rid = 5;
                        $ob->status = 0;
                        $dataLearners[] = $ob;
                    }
                }

                if (!empty($dataLearners)) {
                    $dataLearners = json_encode($dataLearners);
                    $response = JoomdleHelperContent::call_method('set_user_role', 1, (int)$course_group, $my->username, $dataLearners);
                }

                $group->updateStats();
                $group->store();
                break;
            default:
                $role = 'contentCreator';
                break;
        }

        if ($mapage) { 
            $groupurl = CRoute::_('index.php?option=com_joomdle&view=mycourses&action=list');
        } else {
            $groupurl = CRoute::_('index.php?option=com_community&view=groups&task=viewabout&groupid='.$groupid.'&loadabout=1');
        }
        $mainframe->redirect($groupurl);
    }

    /**
     * Show full view of the news for the group
     * */
    public function viewbulletin() {
        $config = CFactory::getConfig();
        $my = CFactory::getUser();
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('bulletinid', 0, 'INT');

        // Load necessary libraries
        $groupsModel = CFactory::getModel('groups');

        $bulletin = JTable::getInstance('Bulletin', 'CTable');
        $bulletin->load($id);

        $gmdate = gmdate("M d Y H:i:s", time());
        $utc = strtotime($gmdate);

        if (!$id || $utc > $bulletin->endtime) {
            JFactory::handleErrors();
        }
        
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($bulletin->groupid);

        $isSuperAdmin = COwnerHelper::isCommunityAdmin();
        $isOwner = $groupsModel->isCreator($my->id, $group->id);
        $isAdmin = $groupsModel->isAdmin($my->id, $group->id);
        $isMember = $group->isMember($my->id);

        if ((!$isSuperAdmin && !$isOwner && !$isAdmin && ($bulletin->created_by != $my->id) && $isMember) || !$isMember) {
            JFactory::handleErrors();
        }

        if (!$my->authorise('community.view', 'groups.bulletin.' . $id)) {
            $erroMsg = $my->authoriseErrorMsg();
            echo $erroMsg;
            return;
        }

        $this->renderView(__FUNCTION__);
    }

    /**
     * Show all news from specific groups
     * */
    public function viewbulletins() {
        $config = CFactory::getConfig();
        $my = CFactory::getUser();

        if (!$my->authorise('community.view', 'groups.bulletins')) {
            $errorMsg = $my->authoriseErrorMsg();
            echo $errorMsg;
            return;
        }

        $this->renderView(__FUNCTION__);
    }

    /**
     * Show all discussions from specific groups
     * */
    public function viewdiscussions() {
        $this->renderView(__FUNCTION__);
    }

     /**
     * Show all courses from specific groups
     * */
    public function viewcourses() {
        $this->renderView(__FUNCTION__);
    }

    /**
     * Show all courses from specific groups
     * */
    public function viewpublishcourse() {
        $this->renderView(__FUNCTION__);
    }

    /**
     * Show all categories from hikashop
     * */
    public function hikashopcategories(){
        $this->renderView(__FUNCTION__);   
    }

    /**
     * Show all categories from hikashop
     * */
    public function publishhikashop(){
        $this->renderView(__FUNCTION__);   
    }

    /**
     * Show all my chats from my all groups
     * */
    public function viewchats() {
        $this->renderView(__FUNCTION__);
    }

    public function ajaxEnrolUser(){
        
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $courseid = $jinput->post->get('courseid', '', 'INT');
        $act = $jinput->post->get('act', '', 'INT');
        $groupid = $jinput->post->get('groupid', '', 'INT');
        $groupsModel = $this->getModel('groups');
        $user = CFactory::getUser();
        $username = $user->get('username');
        $learner = 5;

        if((int)$act == 1){
            //enroll
            $response = JoomdleHelperContent::call_method('enrol_user', $username, (int)$courseid, (int)$learner);
            $response1 = $groupsModel->addSubscribeMemberToCourse((int)$courseid,$groupid,$user->id,$act);

        }else{
            //unenroll
            $response = JoomdleHelperContent::call_method('unenrol_user', $username, (int)$courseid);
            $response1 = $groupsModel->addSubscribeMemberToCourse((int)$courseid,$groupid,$user->id,$act);
        }
        
        if ($response['status']){
            $return['message'] = $response['message'];
        }else{
            $return['message'] = 'Something went wrong';
        }

        echo json_encode($return);

        jexit();
    }


    // Chris - ajax send for approval
    public function ajaxSendApproval(){
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $username   = $jinput->getString('username');
        $courseid = $jinput->post->get('courseid', '', 'INT');
        $categoryid = $jinput->post->get('categoryid', '', 'INT');
        $groupid = $jinput->post->get('groupid', '', 'INT');

        // KV_API_HERE - Send course for approval
        if (!$username) $username = CFactory::getUser()->username;

        $response = JoomdleHelperContent::call_method('send_course_for_approve', (int)$courseid, $username, (int)$categoryid);
 
        if ($response['status']){
            //$return['message'] = $response['message'];
            $return['message'] = $groupid;
        }else{
            $return['message'] = 'Something went wrong.';
        }

        //Send Email
        $group = JTable::getInstance( 'Group' , 'CTable' );
        $group->load( $groupid );

        // Escape the output
        $group->name  = CStringHelper::escape($group->name);

        $params = new CParameter( '' );
        $params->set('url' , 'index.php?option=com_community&view=groups&task=viewcourses&groupid='. $groupid);
        $params->set('group_url' , 'index.php?option=com_community&view=groups&task=viewcourses&groupid='. $groupid);
        $params->set('courseName', $group->name);
        $params->set('email_type', 'approval');
        
        $usercc = CFactory::getUser();
        $usernamecc = $usercc->get('username');
        $actor = intval(JUserHelper::getUserId($usernamecc));

        //Get the targets - Course Creators and Managers
        $role = JoomdleHelperContent::call_method ('get_user_role', (int)$courseid);     
                    
        $userroles = array();
        $adminlist = array();
        foreach ($role['roles'] as $value) {
            $isAdmin = false;
            $username = $value['username'];
            $user_roles = $value['role'];
            $decode_user_roles = json_decode($user_roles);

            foreach ($decode_user_roles as $dur) {
                if($dur->roleid == 1 || $dur->roleid == 2){
                    $isAdmin = true;
                }
            }
            if($isAdmin) array_push($adminlist, intval(JUserHelper::getUserId($username)));
        }

        foreach ($adminlist as $adminlist) {
            CNotificationLibrary::add( 'groups_invite', $actor, $adminlist, JText::_('COM_COMMUNITY_EMAIL_LP_APPROVAL_SUBJECT'), '', 'groups.notifylp', $params );
        }

        echo json_encode($return);

        jexit();

    }

    //Chris - ajax remove course

    public function ajaxRemoveCourse() {
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');

        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
//        $username   = $jinput->getString('username');
        $username = CFactory::getUser()->username;
        $courseid = $jinput->post->get('courseid', '', 'INT');
        $categoryid = $jinput->post->get('categoryid', '', 'INT');

        // KV_API_HERE - Remove Course

        $response = JoomdleHelperContent::call_method('remove_course_platform', (int)$courseid, $username, (int)$categoryid);
         
        if ($response['status']){
            $return['message'] = $response['message'];
        }else{
            $return['message'] = 'Course cannot be deleted.';
        }

        echo json_encode($return);

        jexit();
    }

    public function ajaxUnApproveCourse(){
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');

        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
//        $username   = $jinput->getString('username');
        $username = CFactory::getUser()->username;
        $courseid = $jinput->post->get('courseid', '', 'INT');
        $categoryid = $jinput->post->get('categoryid', '', 'INT');
        $action = $jinput->getString('action');
        $groupid = $jinput->post->get('groupid', '', 'INT');

        // KV_API_HERE - Approve and Unapprove course
        
        $response = JoomdleHelperContent::call_method('approve_or_unapprove_course',  $username, (int)$courseid, (int)$categoryid, $action);

        if($response['status']){
            $return['message'] = 'Course ' . $action .'d.';
        }else{
            $return['message'] = 'Course cannot be ' . $action;
        }

        //Send Email
        $group = JTable::getInstance( 'Group' , 'CTable' );
        $group->load( $groupid );

        // Escape the output
        $group->name  = CStringHelper::escape($group->name);

        $params = new CParameter( '' );
        $params->set('url' , 'index.php?option=com_community&view=groups&task=viewcourses&groupid='. $groupid);
        $params->set('group_url' , 'index.php?option=com_community&view=groups&task=viewcourses&groupid='. $groupid);
        $params->set('courseName', $group->name);
        if($action == 'approve'){
            $params->set('email_type', 'approved');
        }else{
            $params->set('email_type', 'unapproved');
        }
        
        $usercc = CFactory::getUser();
        $usernamecc = $usercc->get('username');
        $actor = intval(JUserHelper::getUserId($usernamecc));

        //Get the targets - Course Creators and Managers
        $role = JoomdleHelperContent::call_method ('get_user_role', (int)$courseid);     
                    
        $userroles = array();
        $cclist = array();
        foreach ($role['roles'] as $value) {
            $isContentCreator = false;
            $username = $value['username'];
            $user_roles = $value['role'];
            $decode_user_roles = json_decode($user_roles);

            foreach ($decode_user_roles as $dur) {
                if($dur->roleid == 3){
                    $isContentCreator = true;
                }
            }
            if($isContentCreator) array_push($cclist, intval(JUserHelper::getUserId($username)));
        }

        foreach ($cclist as $cclist) {
            if($action == 'approve'){
                 CNotificationLibrary::add( 'groups_invite', $actor, $cclist, JText::_('COM_COMMUNITY_EMAIL_LP_APPROVED_SUBJECT'), '', 'groups.notifylp', $params );
             }else{
                 CNotificationLibrary::add( 'groups_invite', $actor, $cclist, JText::_('COM_COMMUNITY_EMAIL_LP_UNAPPROVED_SUBJECT'), '', 'groups.notifylp', $params );
             }
        }

        echo json_encode($return);

        jexit();
    }

    // Remove published course
    public function ajaxRemovePublishedCourse(){
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        
        $user = CFactory::getUser();
        $username = $user->get('username');

        $courseid = $jinput->post->get('courseid', '', 'INT');
        $groupid = $jinput->post->get('groupid', '', 'INT');

        $groupsModel = $this->getModel('groups');

        $response = $groupsModel->UnpublishCourseFromSocial($courseid, $groupid);
        if($response){
            $return['message'] = "Group successfully removed";
            if(count($groupsModel->isCourseExistFromShareGroup($courseid)) == 0){
                JoomdleHelperContent::call_method('course_enable_self_enrolment', (int)$courseid, 'unenable', $username); 
            }
        }

        echo json_encode($return);
        jexit();

    }

    // Nyi Added this function to unpublish and publish
    public function ajaxUnpublishCourse(){

        require_once(JPATH_SITE.'/components/com_hikashop/helpers/lpapi.php');
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $username   = $jinput->getString('username');
        $courseid = $jinput->post->get('courseid', '', 'INT');
        $categoryid = $jinput->post->get('categoryid', '', 'INT');
        $action = $jinput->getString('action');
        $response = LpApiController::unpublishcourse($categoryid,$courseid);
        if($response['status']){
            $retrun['status']=true;
            $return['message'] = 'Course ' .$response['message'];
        }else{
            $return['message'] = 'Course cannot be ' . $action;
        }
 
        echo json_encode($response);

        jexit();
    }

    public function ajaxRemoveCourseHika(){

        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        require_once(JPATH_SITE.'/components/com_hikashop/helpers/lpapi.php');

        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $username   = $jinput->getString('username');
        $courseid = $jinput->post->get('courseid', '', 'INT');
        $categoryid = $jinput->post->get('categoryid', '', 'INT');

        // KV_API_HERE - Remove Course and NYI remove course from HIKA

        $responseHika = LpApiController::removecourse($courseid,$categoryid);
        if ($responseHika['status']){
            $return['status'] = true;
            $return['message'] = $responseHika['message'];
        }else{
            $return['status'] = false;
            $return['message'] = 'Course cannot be deleted.';
        }

        echo json_encode($responseHika);

        jexit();
    }

    /**
     * Show catalogue from specific groups
     * */
    public function viewcatalogue() {
        $this->renderView(__FUNCTION__);
    }

    /**
     * Show learning provider about page
     * */
    public function viewlpabout() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;

        $config = CFactory::getConfig();
        $my = CFactory::getUser();
        $data = new stdClass();
        $data->id = $jinput->get('groupid', '', 'INT');

        if (!$my->authorise('community.view', 'groups.member.' . $data->id)) {
            $errorMsg = $my->authoriseErrorMsg();
            echo $errorMsg;
            return;
        }
        
        $this->renderView(__FUNCTION__, $data);
    }

    /**
     * Save a new discussion
     * @param type $discussion
     * @return boolean
     *
     */
    private function _saveDiscussion(&$discussion) {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $topicId = $jinput->get('topicid', '', 'NONE'); //$jinput->get('topicid' , 'POST'); //JRequest::getVar( 'topicid' , 'POST' );
//        $postData = JRequest::get('post');
        $postData = $_POST;
        $postData['message'] = htmlspecialchars($postData['message']);
        $inputFilter = CFactory::getInputFilter(true, array('b','u','i','li','ul','ol', 'br', 'div', 'p', 'img', 'a'));
        $groupid = $jinput->request->get('groupid', '', 'INT'); //JRequest::getVar('groupid' , '' , 'REQUEST');
        $my = CFactory::getUser();
        $mainframe = JFactory::getApplication();
        $groupsModel = $this->getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);

        $discussion->bind($postData);

        //CFactory::load( 'helpers' , 'owner' );

        $creator = CFactory::getUser($discussion->creator);

        if ($my->id != $creator->id && !empty($discussion->creator) && !$groupsModel->isAdmin($my->id, $discussion->groupid) && !COwnerHelper::isCommunityAdmin()) {
            $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_ACCESS_FORBIDDEN'), 'error');
            return false;
        }

        $isNew = is_null($discussion->id) || !$discussion->id ? true : false;

        if ($isNew) {
            $discussion->creator = $my->id;
        }

        $now = new JDate();

        $discussion->groupid = $groupid;
        $discussion->created = isset($discussion->created) && $discussion->created != '' ? $discussion->created : $now->toSql();
        $discussion->lastreplied = (isset($discussion->lastreplied)) ? $discussion->lastreplied : $discussion->created;
        $discussion->message = JRequest::getVar('message', '', 'post', 'string', JREQUEST_ALLOWRAW);
        $discussion->message = $inputFilter->clean($discussion->message);

        // @rule: do not allow html tags in the title
        $discussion->title = strip_tags($discussion->title);

        //CFactory::load( 'libraries' , 'apps' );
        $appsLib = CAppPlugins::getInstance();
        $saveSuccess = $appsLib->triggerEvent('onFormSave', array('jsform-groups-discussionform'));
        $validated = true;

        if (empty($saveSuccess) || !in_array(false, $saveSuccess)) {
            $config = CFactory::getConfig();

            // @rule: Spam checks
            if ($config->get('antispam_akismet_discussions')) {
                //CFactory::load( 'libraries' , 'spamfilter' );

                $filter = CSpamFilter::getFilter();
                $filter->setAuthor($my->getDisplayName());
                $filter->setMessage($discussion->title . ' ' . $discussion->message);
                $filter->setEmail($my->email);
                $filter->setURL(CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id));
                $filter->setType('message');
                $filter->setIP($_SERVER['REMOTE_ADDR']);

                if ($filter->isSpam()) {
                    $validated = false;
                    $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_DISCUSSIONS_MARKED_SPAM'), 'error');
                }
            }

            if (empty($discussion->title)) {
                $validated = false;
                $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_TITLE_EMPTY'), 'error');
            }

            if (empty($discussion->message)) {
                $validated = false;
                $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_BODY_EMPTY'), 'error');
            }

            if ($validated) {
                //CFactory::load( 'models' , 'discussions' );

                $params = new CParameter('');

                $params->set('filepermission-member', JRequest::getInt('filepermission-member', 0));
                $params->set('notify-creator', JRequest::getInt('discussion-notification', 1));

                $discussion->params = $params->toString();

                $discussion->store();

                if ($isNew) {
                    $group = JTable::getInstance('Group', 'CTable');
                    $group->load($groupid);

                    /*
                    *  Change with UX scrubbing
                    *  Upload avatar
                    */
                    if(isset($_FILES) && !empty($_FILES['avatar']['name'])) {
                        if(isset($_FILES['avatar'])) {
                            $uploaded = CGroups::setAvatarPhoto($_FILES['avatar'], $discussion->id, 'discussion');
                        }
                    }
                    
                    // Add logging.
                    $url = CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupid);


                    $act = new stdClass();
                    $act->cmd = 'group.discussion.create';
                    $act->actor = $my->id;
                    $act->target = 0;
                    $act->title = ''; //JText::sprintf('COM_COMMUNITY_GROUPS_NEW_GROUP_DISCUSSION' , '{group_url}' , $group->name );
                    $act->content = $discussion->message;
                    $act->app = 'groups.discussion';
                    $act->cid = $discussion->id;
                    $act->groupid = $group->id;
                    $act->group_access = $group->approvals;

                    $act->like_id = CActivities::LIKE_SELF;
                    $act->comment_id = CActivities::COMMENT_SELF;
                    $act->like_type = 'groups.discussion';
                    $act->comment_type = 'groups.discussion';

                    $params = new CParameter('');
                    $params->set('action', 'group.discussion.create');
                    $params->set('topic_id', $discussion->id);
                    $params->set('topic', $discussion->title);
                    $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                    $params->set('topic_url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $group->id . '&topicid=' . $discussion->id);

                    $activity = CActivityStream::add($act, $params->toString());

                    $hashtags = CContentHelper::getHashTags($discussion->message); //find the hashtags in the discussion message if there is any
                    //$oldHashtags = CContentHelper::getHashTags($activity->title); //old hashtag from the prebious message or title if there is any

                    //$removeTags = array_diff($oldHashtags, $hashtags); // this are the tags need to be removed
                    //$addTags = array_diff($hashtags, $oldHashtags); // tags that need to be added
                    // add new tags if there's any
                    if(count($hashtags)){
                        $hashtagModel = CFactory::getModel('hashtags');
                        foreach($hashtags as $tag){
                            $hashtagModel->addActivityHashtag($tag, $activity->id);
                        }
                    }

                    //@rule: Add notification for group members whenever a new discussion created.
                    $config = CFactory::getConfig();

                    if ($config->get('groupdiscussnotification') == 1) {
                        $model = $this->getModel('groups');
                        $members = $model->getMembers($groupid, null);
                        $admins = $model->getAdmins($groupid, null);

                        $membersArray = array();

                        foreach ($members as $row) {
                            $membersArray[] = $row->id;
                        }

                        foreach ($admins as $row) {
                            $membersArray[] = $row->id;
                        }
                        unset($members);
                        unset($admins);

                        // Add notification
                        //CFactory::load( 'libraries' , 'notification' );

                        $params = new CParameter('');
                        $params->set('url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $group->id . '&topicid=' . $discussion->id);
                        $params->set('group', $group->name);
                        $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                        $params->set('discussion', $discussion->title);
                        $params->set('discussion_url', 'index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $group->id . '&topicid=' . $discussion->id);
                        $params->set('user', $my->getDisplayName());
                        $params->set('subject', $discussion->title);
                        $params->set('message', $discussion->message);

                        CNotificationLibrary::addMultiple('groups_create_discussion', $discussion->creator, $membersArray, JText::sprintf('COM_COMMUNITY_NEW_DISCUSSION_NOTIFICATION_EMAIL_SUBJECT', $group->name), '', 'groups.discussion', $params);
                    }
                }

                //add user points
                //CFactory::load( 'libraries' , 'userpoints' );
                CUserPoints::assignPoint('group.discussion.create');
            }
        } else {
            $validated = false;
        }

        return $validated;
    }

    public function adddiscussion() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $document = JFactory::getDocument();
        $viewType = $document->getType();
        $viewName = JRequest::getCmd('view', $this->getName());
        $view = $this->getView($viewName, '', $viewType);
        $my = CFactory::getUser();
        $groupid = $jinput->get('groupid', '', 'INT'); //JRequest::getVar('groupid' , '' , 'REQUEST');
        $groupsModel = $this->getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);

        $config = CFactory::getConfig();

        // Check if the user is banned
        $isBanned = $group->isBanned($my->id);

        if ($my->id == 0) {
            return $this->blockUnregister();
        }


        $config = CFactory::getConfig();

        if (!$config->get('creatediscussion') || (!$group->isMember($my->id) || $isBanned) && !COwnerHelper::isCommunityAdmin()) {
            header('Location: /404.html');
            exit;
//            echo $view->noAccess();
//            return;
        }

        $discussion = JTable::getInstance('Discussion', 'CTable');

        if ($jinput->getMethod() == 'POST') {
            JRequest::checkToken() or jexit(JText::_('COM_COMMUNITY_INVALID_TOKEN'));

            if ($this->_saveDiscussion($discussion) !== false) {
                $redirectUrl = CRoute::_('index.php?option=com_community&view=groups&task=viewdiscussion&topicid=' . $discussion->id . '&groupid=' . $groupid, false);
                $this->cacheClean(array(COMMUNITY_CACHE_TAG_GROUPS, COMMUNITY_CACHE_TAG_ACTIVITIES, COMMUNITY_CACHE_TAG_GROUPS_DETAIL));
                $mainframe->redirect($redirectUrl, JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_CREATE_SUCCESS'));
                exit;
            }
        }

        echo $view->get(__FUNCTION__, $discussion);
    }

    /**
     * Show discussion
     */
    public function viewdiscussion() {
        $config = CFactory::getConfig();
        if (!$config->get('enablegroups')) {
            echo JText::_('COM_COMMUNITY_GROUPS_DISABLE');
            return;
        }

        $jinput = JFactory::getApplication()->input;

        $my = CFactory::getUser();
        $groupId = $jinput->get('groupid', 0, 'INT');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);

        $isMember = $group->isMember($my->id);

        if (!$isMember) {
            JFactory::handleErrors();
        }

        $this->renderView(__FUNCTION__);
    }

    /**
     * Show Invite
     */
    public function invitefriends() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;

        $document = JFactory::getDocument();
        $viewType = $document->getType();
        $viewName = JRequest::getCmd('view', $this->getName());
        $view = $this->getView($viewName, '', $viewType);

        $my = CFactory::getUser();
        $invited = $jinput->post->get('invite-list', '', 'NONE'); //JRequest::getVar( 'invite-list' , '' , 'POST' );
      
        
        $inviteMessage = $jinput->post->get('invite-message', '', 'STRING'); //JRequest::getVar( 'invite-message' , '' , 'POST' );
        $groupId = $jinput->request->get('groupid', '', 'INT'); //JRequest::getVar( 'groupid' , '' , 'REQUEST' );
        $groupsModel = $this->getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);
        // Check if the user is banned
        $isBanned = $group->isBanned($my->id);

        if ($my->id == 0) {
            return $this->blockUnregister();
        }

        if ((!$group->isMember($my->id) || $isBanned) && !COwnerHelper::isCommunityAdmin()) {
            echo JText::_('COM_COMMUNITY_ACCESS_FORBIDDEN');
            return;
        }

        if ($jinput->getMethod() == 'POST') {
            JRequest::checkToken() or jexit(JText::_('COM_COMMUNITY_INVALID_TOKEN'));
            if (!empty($invited)) {
                $mainframe = JFactory::getApplication();
                $groupsModel = CFactory::getModel('Groups');
                $group = JTable::getInstance('Group', 'CTable');
                $group->load($groupId);

                $gparams = json_decode($group->params, true);

                foreach ($invited as $invitedUserId) {
                    $groupInvite = JTable::getInstance('GroupInvite', 'CTable');
                    $groupInvite->groupid = $group->id;
                    $groupInvite->userid = $invitedUserId;
                    $groupInvite->creator = $my->id;

                    $groupInvite->store();
                }
                // Add notification
                //CFactory::load( 'libraries' , 'notification' );

                $params = new CParameter('');
                $params->set('url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                $params->set('groupname', $group->name);
                $params->set('message', $inviteMessage);
                $params->set('group', $group->name);
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);

                CNotificationLibrary::add('groups_invite', $my->id, $invited, JText::sprintf('COM_COMMUNITY_GROUPS_JOIN_INVITATION_MESSAGE'), '', 'groups.invite', $params);

                $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id, false), JText::_('COM_COMMUNITY_GROUPS_INVITATION_SEND_MESSAGE'));
            } else {
                $view->addWarning(JText::_('COM_COMMUNITY_INVITE_NEED_AT_LEAST_1_FRIEND'));
            }
        }
        echo $view->get(__FUNCTION__);
    }
    
    public function invitemembers() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;

        $document = JFactory::getDocument();
        $viewType = $document->getType();
        $viewName = JRequest::getCmd('view', $this->getName());
        $view = $this->getView($viewName, '', $viewType);
        
        $my = CFactory::getUser();
        $invited = $jinput->post->get('invite-list', '', 'NONE'); //JRequest::getVar( 'invite-list' , '' , 'POST' );
         
        $inviteMessage = $jinput->post->get('invite-message', '', 'STRING'); //JRequest::getVar( 'invite-message' , '' , 'POST' );
        $groupId = $jinput->request->get('groupid', '', 'INT'); //JRequest::getVar( 'groupid' , '' , 'REQUEST' );
        $groupsModel = $this->getModel('groups');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);
        // Check if the user is banned
        $isBanned = $group->isBanned($my->id);
        $isSuperAdmin = COwnerHelper::isCommunityAdmin();
        $isOwner = $groupsModel->isCreator($my->id, $group->id);
        $isAdmin = $groupsModel->isAdmin($my->id, $group->id);
        $isMember = $group->isMember($my->id);

        if (!$isSuperAdmin && !$isOwner && !$isAdmin) {
            JFactory::handleErrors();
        }

        if ($my->id == 0) {
            return $this->blockUnregister();
        }

        if ((!$group->isMember($my->id) || $isBanned) && !COwnerHelper::isCommunityAdmin()) {
            header('Location: /404.html');
            exit;
//            echo '<div class="message-error">' . JText::_('COM_COMMUNITY_ACCESS_FORBIDDEN') . '</div>';
//            return;
        }

        if ($jinput->getMethod() == 'POST') {
            JRequest::checkToken() or jexit(JText::_('COM_COMMUNITY_INVALID_TOKEN'));
            if (!empty($invited)) {
                $mainframe = JFactory::getApplication();
                $groupsModel = CFactory::getModel('Groups');
                $group = JTable::getInstance('Group', 'CTable');
                $group->load($groupId);

                $member = JTable::getInstance('GroupMembers', 'CTable');
                foreach ($invited as $invitedUserId) {
                    $groupsModel->addUserToCircle($invitedUserId, $group->id);
                }
                // Add notification
                //CFactory::load( 'libraries' , 'notification' );

                $params = new CParameter('');
                $params->set('url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                $params->set('groupname', $group->name);
                $params->set('message', $inviteMessage);
                $params->set('group', $group->name);
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);

                CNotificationLibrary::add('groups_invite', $my->id, $invited, JText::sprintf('COM_COMMUNITY_GROUPS_ADD_INVITATION_MESSAGE'), '', 'groups.invite', $params);

                $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewabout&groupid=' . $group->id . '&loadabout=1', false), JText::_('COM_COMMUNITY_GROUPS_ADD_SEND_MESSAGE'));
            } else {
                $view->addWarning(JText::_('COM_COMMUNITY_INVITE_NEED_AT_LEAST_1_FRIEND'));
            }
        }
        //echo 'test';
        //exit;
        echo $view->get(__FUNCTION__);
    }

    public function editDiscussion() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $topicId = $jinput->get('topicid', '', 'INT');

        $discussion = JTable::getInstance('Discussion', 'CTable');
        $discussion->load($topicId);

        $groupId = $jinput->get('groupid', '', 'INT');
        $groupsModel = CFactory::getModel('Groups');
        $my = CFactory::getUser();
        $creator = CFactory::getUser($discussion->creator);

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($discussion->groupid);

        if ($my->id == 0 || $groupId != $group->id) {
            return $this->blockUserAccess();
        }

        $isSuperAdmin = COwnerHelper::isCommunityAdmin();
        $isOwner = $groupsModel->isCreator($my->id, $group->id);
        $isAdmin = $groupsModel->isAdmin($my->id, $group->id);
        $isMember = $group->isMember($my->id);
        
        // Make sure this user is a member of this group
        if ((!$isSuperAdmin && !$isOwner && !$isAdmin && ($discussion->creator != $my->id) && $isMember) || !$isMember) {
            JFactory::handleErrors();
        } else {
            if ($jinput->getMethod() == 'POST') {
                JRequest::checkToken() or jexit(JText::_('COM_COMMUNITY_INVALID_TOKEN'));

                if ($this->_saveDiscussion($discussion) !== false) {
                    $redirectUrl = CRoute::_('index.php?option=com_community&view=groups&task=viewdiscussion&topicid=' . $discussion->id . '&groupid=' . $groupId, false);

                    $mainframe = JFactory::getApplication();
                    $mainframe->redirect($redirectUrl, JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_UPDATED'));
                }
            }
            $this->renderView(__FUNCTION__, $discussion);
        }
    }

    public function editNews() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $my = CFactory::getUser();

        if ($my->id == 0) {
            return $this->blockUnregister();
        }

        $document = JFactory::getDocument();
        $viewType = $document->getType();
        $viewName = JRequest::getCmd('view', $this->getName());
        $view = $this->getView($viewName, '', $viewType);
        // Load necessary models
        $groupsModel = CFactory::getModel('groups');
        //CFactory::load( 'models' , 'bulletins' );

        $groupId = JRequest::getInt('groupid', '', 'REQUEST');

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);
        //CFactory::load( 'helpers' , 'owner' );
        // Ensure user has really the privilege to view this page.
        if ($my->id != $group->ownerid && !COwnerHelper::isCommunityAdmin() && !$groupsModel->isAdmin($my->id, $groupId)) {
            echo JText::_('COM_COMMUNITY_PERMISSION_DENIED_WARNING');
            return;
        }
        $validated = true;
        if ($jinput->getMethod() == 'POST') {
            JRequest::checkToken() or jexit(JText::_('COM_COMMUNITY_INVALID_TOKEN'));
            // Get variables from query
            $bulletin = JTable::getInstance('Bulletin', 'CTable');
            $bulletinId = $jinput->post->get('bulletinid', '', 'INT'); //JRequest::getVar( 'bulletinid' , '' , 'POST' );

            $bulletin->load($bulletinId);
            $bulletin->message = JRequest::getVar('message', '', 'post', 'string', JREQUEST_ALLOWRAW);
            $bulletin->title = $jinput->post->get('title', '', 'string'); //JRequest::getVar( 'title', '', 'post', 'string' );
            // Groupid should never be empty. Add some assert codes here
            CError::assert($groupId, '', '!empty', __FILE__, __LINE__);
            CError::assert($bulletinId, '', '!empty', __FILE__, __LINE__);

            if (empty($bulletin->message)) {
                $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewbulletin&bulletinid=' . $bulletinId . '&groupid=' . $groupId, false), JText::_('COM_COMMUNITY_GROUPS_BULLETIN_BODY_EMPTY'));
            }

            $params = new CParameter('');
            $params->set('filepermission-member', JRequest::getInt('filepermission-member', 0));
            $bulletin->params = $params->toString();
            if ($jinput->post->get('announcement-enddate_submit', '', 'STRING') == '') {
                if (!empty($bulletin->endtime)) {
                    $enddate = date('d-m-Y', $bulletin->endtime);
                    $bulletin->endtime = strtotime($enddate.' '.$jinput->post->get('announcement-endtime', '', 'STRING'));
                }
            } else
                $bulletin->endtime = ($jinput->post->get('announcement-enddate_submit', '', 'STRING') == '') ? '' : strtotime($jinput->post->get('announcement-enddate_submit', '', 'STRING').' '.$jinput->post->get('announcement-endtime', '', 'STRING'));

            if ($validated) {
                $bulletin->store();
                //$mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewbulletin&bulletinid=' . $bulletinId . '&groupid=' . $groupId, false), JText::_('COM_COMMUNITY_BULLETIN_UPDATED'));
                $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewdiscussions&groupid=' . $groupId, false), JText::_('COM_COMMUNITY_BULLETIN_UPDATED'));
            } else {
                echo $view->get(__FUNCTION__, $bulletin);
                return;
            }

        }
    }

    /**
     * Method to add a new discussion
     * */
    public function addNews() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;

        $my = CFactory::getUser();

        if ($my->id == 0) {
            return $this->blockUnregister();
        }

        $document = JFactory::getDocument();
        $viewType = $document->getType();
        $viewName = JRequest::getCmd('view', $this->getName());
        $view = $this->getView($viewName, '', $viewType);

        // Load necessary models
        $groupsModel = CFactory::getModel('groups');
        $groupId = $jinput->request->get('groupid', '', 'INT');
        $config = CFactory::getConfig();
        if (!$config->get('createannouncement')) {
            $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupId));
        }

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);

        // Ensure user has really the privilege to view this page.
        if ($my->id != $group->ownerid && !COwnerHelper::isCommunityAdmin() && !$groupsModel->isAdmin($my->id, $groupId)) {
            header('Location: /404.html');
            exit;
//            echo $view->noAccess();
//            return;
        }

        $title = '';
        $message = '';

        if ($jinput->getMethod() == 'POST') {
            JRequest::checkToken() or jexit(JText::_('COM_COMMUNITY_INVALID_TOKEN'));

            // Get variables from query
            $bulletin = JTable::getInstance('Bulletin', 'CTable');
            $bulletin->title = $jinput->post->get('title', '', 'STRING');
            $bulletin->message = JRequest::getVar('message', '', 'post', 'string', JREQUEST_ALLOWRAW);

            // Groupid should never be empty. Add some assert codes here
            CError::assert($groupId, '', '!empty', __FILE__, __LINE__);

            $validated = true;

            if (empty($bulletin->title)) {
                $validated = false;
                $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUPS_BULLETIN_EMPTY'), 'notice');
            }

            if (empty($bulletin->message)) {
                $validated = false;
                $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_GROUPS_BULLETIN_BODY_EMPTY'), 'notice');
            }
            if ($validated) {
                $params = new CParameter('');
                $bulletin->groupid = $groupId;
                $bulletin->date = gmdate('Y-m-d H:i:s');
                $bulletin->created_by = $my->id;

                // @todo: Add moderators for the groups.
                // Since now is default to the admin, default to publish the news
                $bulletin->published = 1;
                $params->set('filepermission-member', JRequest::getInt('filepermission-member', 0));
                $bulletin->params = $params->toString();
                $bulletin->endtime = ($jinput->post->get('announcement-enddate_submit', '', 'STRING') == '') ? '' : strtotime($jinput->post->get('announcement-enddate_submit', '', 'STRING').' '.$jinput->post->get('announcement-endtime', '', 'STRING'));

                $bulletin->store();

                // Send notification to all user
                $model = $this->getModel('groups');
                $memberCount = $model->getMembersCount($groupId);
                $members = $model->getMembers($groupId, $memberCount, true, false, SHOW_GROUP_ADMIN);

                $membersArray = array();

                foreach ($members as $row) {
                    $membersArray[] = $row->id;
                }
                unset($members);

                // Add notification
                //CFactory::load( 'libraries' , 'notification' );

                $params->set('url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupId);
                $params->set('group', $group->name);
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupId);
                $params->set('subject', $bulletin->title);
                $params->set('announcement', $bulletin->title);
                $params->set('message_news', $bulletin->message);
                $params->set('message', $bulletin->message);
                $params->set('announcement_url', 'index.php?option=com_community&view=groups&task=viewbulletin&groupid=' . $group->id . '&bulletinid=' . $bulletin->id);

                CNotificationLibrary::add('groups_create_news', $my->id, $membersArray, JText::sprintf('COM_COMMUNITY_GROUPS_EMAIL_NEW_BULLETIN_SUBJECT'), '', 'groups.bulletin', $params);

                // Add logging to the bulletin
                $url = CRoute::_('index.php?option=com_community&view=groups&task=viewbulletin&groupid=' . $group->id . '&bulletinid=' . $bulletin->id);

                // Add activity logging

                $act = new stdClass();
                $act->cmd = 'group.news.create';
                $act->actor = $my->id;
                $act->target = 0;
                $act->title = ''; //JText::sprintf('COM_COMMUNITY_GROUPS_NEW_GROUP_NEWS' , '{group_url}' , $bulletin->title );
                $act->content = ( $group->approvals == 0 ) ? JString::substr(strip_tags($bulletin->message), 0, 100) : '';
                $act->app = 'groups.bulletin';
                $act->cid = $bulletin->id;
                $act->groupid = $group->id;
                $act->group_access = $group->approvals;

                $act->comment_id = CActivities::COMMENT_SELF;
                $act->comment_type = 'groups.bulletin';
                $act->like_id = CActivities::LIKE_SELF;
                $act->like_type = 'groups.bulletin';

                $params = new CParameter('');
//              $params->set( 'group_url' , 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id );
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewbulletin&groupid=' . $group->id . '&bulletinid=' . $bulletin->id);


                CActivityStream::add($act, $params->toString());

                //add user points
                CUserPoints::assignPoint('group.news.create');
                $this->cacheClean(array(COMMUNITY_CACHE_TAG_ACTIVITIES, COMMUNITY_CACHE_TAG_GROUPS_DETAIL));
                //$mainframe->redirect( CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupId , false ), JText::_('COM_COMMUNITY_GROUPS_BULLETIN_CREATE_SUCCESS') );
                // $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewbulletin&groupid=' . $groupId . '&bulletinid=' . $bulletin->id, false), JText::_('COM_COMMUNITY_GROUPS_BULLETIN_CREATE_SUCCESS'));
                $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewdiscussions&groupid=' . $groupId, false), JText::_('COM_COMMUNITY_GROUPS_BULLETIN_CREATE_SUCCESS'));
            } else {
                echo $view->get(__FUNCTION__, $bulletin);
                return;
            }
        }

        echo $view->get(__FUNCTION__, false);
    }

    public function deleteTopic() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        //CFactory::load( 'libraries' , 'activities' );
        $my = CFactory::getUser();
        if ($my->id == 0) {
            return $this->blockUnregister();
        }

        $topicid = $jinput->post->get('topicid', '', 'INT'); //JRequest::getVar( 'topicid' , '' , 'POST' );
        $groupid = $jinput->post->get('groupid', '', 'INT'); //JRequest::getVar( 'groupid' , '' , 'POST' );

        if (empty($topicid) || empty($groupid)) {
            echo JText::_('COM_COMMUNITY_INVALID_ID');
            return;
        }

        //CFactory::load( 'helpers' , 'owner' );
        //CFactory::load( 'models' , 'discussions' );
        
        $groupsModel = CFactory::getModel('groups');
        $wallModel = CFactory::getModel('wall');
        $activityModel = CFactory::getModel('activities');
        $fileModel = CFactory::getModel('files');
        $discussion = JTable::getInstance('Discussion', 'CTable');
        $discussion->load($topicid);

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        $isGroupAdmin = $groupsModel->isAdmin($my->id, $group->id);

        if ($my->id == $discussion->creator || $isGroupAdmin || COwnerHelper::isCommunityAdmin()) {


            if ($discussion->delete()) {
                // Remove the replies to this discussion as well since we no longer need them
                $wallModel->deleteAllChildPosts($topicid, 'discussions');
                // Remove from activity stream
                CActivityStream::remove('groups.discussion', $topicid);
                // Remove Discussion Files
                $fileModel->alldelete($topicid, 'discussion');
                //delete notification
                CNotificationLibrary::deleteNotificationchat($discussion->title);
                // Assuming all files are deleted, remove the folder if exists
                if(JFolder::exists(JPATH_ROOT . '/' . 'images/files/discussion/'.$topicid)){
                    JFolder::delete(JPATH_ROOT . '/' . 'images/files/discussion/'.$topicid);
                }

                $this->cacheClean(array(COMMUNITY_CACHE_TAG_GROUPS, COMMUNITY_CACHE_TAG_GROUPS_DETAIL, COMMUNITY_CACHE_TAG_ACTIVITIES));
                $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupid, false), JText::_('COM_COMMUNITY_DISCUSSION_REMOVED'));
            }
        } else {
            $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupid, false), JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_DELETE_WARNING'));
        }
    }

    public function lockTopic() {
        $mainframe = JFactory::getApplication();

        $my = CFactory::getUser();
        if ($my->id == 0) {
            return $this->blockUnregister();
        }

        $topicid = JRequest::getInt('topicid', '', 'POST');
        $groupid = JRequest::getInt('groupid', '', 'POST');

        if (empty($topicid) || empty($groupid)) {
            echo JText::_('COM_COMMUNITY_INVALID_ID');
            return;
        }

        //CFactory::load( 'helpers' , 'owner' );
        //CFactory::load( 'models' , 'discussions' );

        $groupsModel = CFactory::getModel('groups');
        $wallModel = CFactory::getModel('wall');
        $discussion = JTable::getInstance('Discussion', 'CTable');
        $discussion->load($topicid);

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);
        $isGroupAdmin = $groupsModel->isAdmin($my->id, $group->id);


        if ($my->id == $discussion->creator || $isGroupAdmin || COwnerHelper::isCommunityAdmin()) {
            $lockStatus = $discussion->lock ? false : true;
            $confirmMsg = $lockStatus ? JText::_('COM_COMMUNITY_DISCUSSION_LOCKED') : JText::_('COM_COMMUNITY_DISCUSSION_UNLOCKED');

            if ($discussion->lock($topicid, $lockStatus)) {
                $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $groupid . '&topicid=' . $topicid, false), $confirmMsg);
            }
        } else {
            $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewdiscussion&groupid=' . $groupid . '&topicid=' . $topicid, false), JText::_('COM_COMMUNITY_NOT_ALLOWED_TO_LOCK_GROUP_TOPIC'));
        }
    }

    public function deleteBulletin() {
        $mainframe = JFactory::getApplication();
        $my = CFactory::getUser();
        //CFactory::load( 'libraries' , 'activities' );
        if ($my->id == 0) {
            return $this->blockUnregister();
        }

        $bulletinId = JRequest::getInt('bulletinid', '', 'POST');
        $groupid = JRequest::getInt('groupid', '', 'POST');

        if (empty($bulletinId) || empty($groupid)) {
            echo JText::_('COM_COMMUNITY_INVALID_ID');
            return;
        }

        //CFactory::load( 'helpers' , 'owner' );
        //CFactory::load( 'models' , 'bulletins' );

        $groupsModel = CFactory::getModel('groups');
        $bulletin = JTable::getInstance('Bulletin', 'CTable');
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupid);

        $fileModel = CFactory::getModel('files');

        if ($groupsModel->isAdmin($my->id, $group->id) || COwnerHelper::isCommunityAdmin()) {
            $bulletin->load($bulletinId);

            if ($bulletin->delete()) {

                //add user points
                //CFactory::load( 'libraries' , 'userpoints' );
                CUserPoints::assignPoint('group.news.remove');
                CActivityStream::remove('groups.bulletin', $bulletinId);
                //delete notification 
                CNotificationLibrary::deleteNotification($my->id,$bulletin->date);
                // Remove Bulletin Files
                $fileModel->alldelete($bulletinId, 'bulletin');

                $this->cacheClean(array(COMMUNITY_CACHE_TAG_ACTIVITIES, COMMUNITY_CACHE_TAG_GROUPS_DETAIL));
                //$mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupid, false), JText::_('COM_COMMUNITY_BULLETIN_REMOVED'));
                $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewdiscussions&groupid=' . $groupid, false), JText::_('COM_COMMUNITY_BULLETIN_REMOVED'));
                  
            }
        } else {
            $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $groupid, false), JText::_('COM_COMMUNITY_GROUPS_DISCUSSION_DELETE_WARNING'));
        }
    }

    /**
     * Displays send email form and processes the sendmail
     * */
    public function sendmail() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $id = JRequest::getInt('groupid', '');
        $message = JRequest::getVar('message', '', 'post', 'string', JREQUEST_ALLOWRAW);
        $title = $jinput->get('title', '', 'STRING'); //JRequest::getVar( 'title'   , '' );
        $my = CFactory::getUser();

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($id);

        //CFactory::load( 'helpers' , 'owner' );

        if (empty($id) || (!$group->isAdmin($my->id) && !COwnerHelper::isCommunityAdmin() )) {
            echo JText::_('COM_COMMUNITY_ACCESS_FORBIDDEN');
            return;
        }

        if ($jinput->getMethod() == 'POST') {
            // Check for request forgeries
            JRequest::checkToken() or jexit(JText::_('COM_COMMUNITY_INVALID_TOKEN'));

            $model = CFactory::getModel('Groups');
            $members = $model->getMembers($group->id, COMMUNITY_GROUPS_NO_LIMIT, COMMUNITY_GROUPS_ONLY_APPROVED, COMMUNITY_GROUPS_NO_RANDOM, COMMUNITY_GROUPS_SHOW_ADMINS);

            $errors = false;

            if (empty($message)) {
                $errors = true;
                $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_INBOX_MESSAGE_REQUIRED'), 'error');
            }

            if (empty($title)) {
                $errors = true;
                $mainframe->enqueueMessage(JText::_('COM_COMMUNITY_TITLE_REQUIRED'), 'error');
            }

            if (!$errors) {
                // Add notification
                //CFactory::load( 'libraries' , 'notification' );
                $emails = array();
                $total = 0;
                foreach ($members as $member) {
                    $total += 1;
                    $user = CFactory::getUser($member->id);
                    $emails[] = $user->id;

                    // Exclude the actor
                    if ($user->id == $my->id) {
                        $total -= 1;
                    }
                }

                $params = new CParameter('');
                $params->set('url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                $params->set('group', $group->name);
                $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id);
                $params->set('email', $title);
                $params->set('title', $title);
                $params->set('message', $message);
                CNotificationLibrary::add('groups_sendmail', $my->id, $emails, JText::sprintf('COM_COMMUNITY_GROUPS_SENDMAIL_SUBJECT'), '', 'groups.sendmail', $params, true, '', JText::sprintf('COM_COMMUNITY_GROUPS_SENDMAIL_NOTIFICATIONS', CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id),$group->name));

                $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id, false), JText::sprintf('COM_COMMUNITY_EMAIL_SENT_TO_GROUP_MEMBERS', $total));
            }
        }

        $this->renderView(__FUNCTION__);
    }

    /*
     * group event name
     * object array
     */

    public function triggerGroupEvents($eventName, &$args, $target = null) {
        CError::assert($args, 'object', 'istype', __FILE__, __LINE__);

        require_once( COMMUNITY_COM_PATH . '/libraries/apps.php' );
        $appsLib = CAppPlugins::getInstance();
        $appsLib->loadApplications();

        $params = array();
        $params[] = $args;

        if (!is_null($target))
            $params[] = $target;

        $appsLib->triggerEvent($eventName, $params);
        return true;
    }

    public function banlist() {
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;

        $data = new stdClass();
        $data->id = $jinput->get->get('groupid', '', 'INT'); //JRequest::getVar('groupid' , '' , 'GET');
        $this->renderView(__FUNCTION__, $data);
    }

    public function memberapprove(){
        $mainframe = JFactory::getApplication();
        $memberid = $_GET['memberid'];
        $groupid = $_GET['groupid'];
        $this->ajaxApproveMember($memberid, $groupid);
        $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewabout&groupid=' . $groupid));
    }

    public function memberreject(){
        $memberid = $_GET['memberid'];
        $groupid = $_GET['groupid'];
        $this->ajaxRemoveMember($memberid, $groupid);
        $mainframe = JFactory::getApplication();
        $mainframe->redirect(CRoute::_('index.php?option=com_community&view=groups&task=viewabout&groupid=' . $groupid));
    }

    /**
     * Method is used to receive POST requests from specific user
     * that wants to join a group
     * @param $groupId
     * @param string $fastJoin join from discussion page
     */
    public function ajaxJoinGroup($groupId, $fastJoin = 'no') {
        

        $json = array();
        $objResponse = new JAXResponse();

        $filter = JFilterInput::getInstance();

        $groupId = $filter->clean($groupId, 'int');
        if (empty($fastJoin)) {
            $fastJoin = 'no';
        }
        $fastJoin = $filter->clean($fastJoin, 'string');

        // Add assertion to the group id since it must be specified in the post request
        CError::assert($groupId, '', '!empty', __FILE__, __LINE__);

        // Get the current user's object
        $my = CFactory::getUser();

        if (!$my->authorise('community.join', 'groups.' . $groupId)) {
            return $this->ajaxBlockUnregister();
        }

        // Load necessary tables
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($groupId);

        $groupModel = CFactory::getModel('groups');
       
        if ($fastJoin == 'yes') {
            $member = $this->_saveMember($groupId);
            $this->cacheClean(array(COMMUNITY_CACHE_TAG_GROUPS, COMMUNITY_CACHE_TAG_ACTIVITIES));

            if ($member->approved) {
                $objResponse->addScriptCall("joms.groups.joinComplete('" . JText::_('COM_COMMUNITY_GROUPS_JOIN_SUCCESS_BUTTON', true) . "'); location.reload(true);");
            } else {
                $objResponse->addScriptCall("joms.groups.joinComplete('" . JText::_('COM_COMMUNITY_GROUPS_JOIN_SUCCESS_BUTTON', true) . "'); location.reload(true);");
                //$objResponse->addScriptCall("joms.jQuery('.group-top').prepend('<div class=\"info\">".JText::_('COM_COMMUNITY_GROUPS_APPROVAL_NEED')."</div>');");
            }
        } else {
            if ($groupModel->isMember($my->id, $groupId)) {
                $json['message'] = JText::_('COM_COMMUNITY_GROUPS_ALREADY_MEMBER');
            } else {
                $member = $this->_saveMember($groupId);
                $this->cacheClean(array(COMMUNITY_CACHE_TAG_GROUPS, COMMUNITY_CACHE_TAG_ACTIVITIES));

                if ($member->approved) {
                    $json['message'] = JText::_('COM_COMMUNITY_GROUPS_JOIN_SUCCESS');
                    $params = new CParameter('');
                    $params->set('circle',$group->name);
                    $params->set('url', 'index.php?option=com_community&view=groups&task=viewabout&groupid=' . $group->id);
                    $params->set('group_url', 'index.php?option=com_community&view=groups&task=viewabout&groupid=' . $group->id);

                    //CNotificationLibrary::add('groups_join_request', $my->id, $group->ownerid, JText::sprintf('COM_COMMUNITY_GROUP_JOIN_REQUEST_SUBJECT'), '', 'groups.joinrequest', $params );
                    CNotificationLibrary::add('groups_member_join', $my->id, $group->ownerid, JText::sprintf('COM_COMMUNITY_NEWS_GROUP_JOIN'), '', 'groups.joinrequest', $params );
                } else {
                    $json['message'] = JText::_('COM_COMMUNITY_GROUPS_APPROVAL_NEED');

                    $params = new CParameter('');
                    $params->set('group',$group->name);
                    $params->set('url', 'index.php?option=com_community&view=groups&task=invitemembers&groupid=' . $group->id);
                    $params->set('group_url', 'index.php?option=com_community&view=groups&task=invitemembers&groupid=' . $group->id . '&tab=approverequest');
                    $params->set('url_approve', 'index.php?option=com_community&view=groups&task=memberapprove&groupid=' . $group->id .'&memberid=' . $my->id);
                    $params->set('url_reject', 'index.php?option=com_community&view=groups&task=memberreject&groupid=' . $group->id .'&memberid=' . $my->id);

                    //CNotificationLibrary::add('groups_join_request', $my->id, $group->ownerid, JText::sprintf('COM_COMMUNITY_GROUP_JOIN_REQUEST_SUBJECT'), '', 'groups.joinrequest', $params );
                    CNotificationLibrary::add('groups_member_join', $my->id, $group->ownerid, JText::sprintf('COM_COMMUNITY_GROUP_JOIN_REQUEST_SUBJECT'), '', 'groups.joinrequest', $params );

                }
            }
        }

        die( json_encode($json) );
    }
    public function ajaxCheckNameCircle(){
        // call ajax when create social circle (categoryid = 0)
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $name = $jinput->get('name', '', 'STRING');
        $categoryid = $jinput->get('categoryid', '', 'STRING');
        $my = CFactory::getUser();
        $model = $this->getModel('Groups');
        $userid = $my->get('id');
        if ($model->groupExistUserCategory($name,$userid,$categoryid)) {
            $status = true;
        }
        $result = array();
        $result['name'] = $name;
        $result['status'] = $status;
        die( json_encode($result) );
     
    }

    
    public function ajaxArchiveCircle(){
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $username = CFactory::getUser()->username;
        $circleid = $jinput->post->get('id', '', 'INT');
        $action = $jinput->getString('action');
         
        $groupsModel = CFactory::getModel('groups');
        $courses = $groupsModel->getPublishCoursesToSocial($circleid);
        
        foreach($courses as $course){
            $coursecircleid = $groupsModel->getLearningCircleId($course->courseid);
            $group = JTable::getInstance('Group', 'CTable');
            $group->load($coursecircleid);
            if($action == "archive"){
                if($group->published == 1)
                    $group->published = 0;
                $group->store();
//                    $r = JoomdleHelperContent::call_method('course_enable_self_enrolment', (int)$course->courseid, 'unenable', $username);	
        }
            else 
            {
                if($group->published == 0)
                    $group->published = 1;
                $group->store();
//                $r = JoomdleHelperContent::call_method('course_enable_self_enrolment', (int)$course->courseid, 'enable', $username);
        
            }
        }
        
        $this->unpublishGroup($circleid);
        // get member of circle
        $members = $groupsModel->getMembers($circleid, 0, true, false, false);
        $mems = array();
        foreach ($members as $mem) {
            $mems[] = $mem->id;
        }
        $member_notifi = array_unique($mems);
        $user = CFactory::getUser();
        $key_mem = array_search($user->id, $member_notifi);
        if ($key_mem !== false && isset($member_notifi[$key_mem])) {
            unset($member_notifi[$key_mem]);
        }
        $groupParent = JTable::getInstance('Group', 'CTable');
        $groupParent->load($circleid);
        $user_owner = CFactory::getUser($groupParent->ownerid);

        $ownername = $user_owner->getDisplayName();
        
        if($action == 'archive') {
            $type_ar = 'archive_circle';
            $mtitle = 'Circle '.$groupParent->name.' has been archived, please contact to '.$ownername.' for more information.';
        } else {
            $type_ar = 'unarchive_circle';
            $mtitle = 'Circle '.$groupParent->name.' has been uarchived.';
        }
                JPluginHelper::importPlugin('groupsapi');
                $dispatcher = JDispatcher::getInstance();
                $message = array(
            'mtitle' => $mtitle,
            'mdesc' => $mtitle
                );
                
        $dispatcher->trigger('pushNotification', array($message, $member_notifi, $circleid, 0, $type_ar, ''));
        $result = array();
        $result['id'] = $r;
        $result['comment'] = 'Successfully';
        echo json_encode($result);
        die;
    }
    public function ajaxRemoveCircle(){
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $username = CFactory::getUser()->username;
        $circleid = $jinput->post->get('id', '', 'INT');
        $groupsModel = CFactory::getModel('groups');
        $courses = $groupsModel->getPublishCoursesToSocial($circleid);
         $myid = CFactory::getUser()->id;
        foreach($courses as $course){
           $response = $groupsModel->UnpublishCourseFromSocial($course->courseid, $circleid,true);
           $coursecircleid = $groupsModel->getLearningCircleId($course->courseid);
            $group = JTable::getInstance('Group', 'CTable');
            $group->load($coursecircleid);
            if($group->published == 0)
                $group->published = 1;
            $group->store();
            if($response){
                $content = "Group successfully removed";
                if(count($groupsModel->isCourseExistFromShareGroup($course->courseid)) <= 1){
                    JoomdleHelperContent::call_method('course_enable_self_enrolment', (int)$course->courseid, 'unenable', $username); 
                    }
            }
            $memberCircles = $groupsModel->getListMembers($coursecircleid, 0, true, true);
            foreach($memberCircles as $member){
                if($member->id == $myid) continue;
                $this->nonAjaxRemoveMember($member->id, $coursecircleid);
            }
            
        }
        // Pushed notification
        $members = $groupsModel->getMembers($circleid, 0, true, false, false);
        $mems = array();
        foreach ($members as $mem) {
            $mems[] = $mem->id;
        }
        $member_notifi = array_unique($mems);
        $user = CFactory::getUser();
        $key_mem = array_search($user->id, $member_notifi);
        if ($key_mem !== false && isset($member_notifi[$key_mem])) {
            unset($member_notifi[$key_mem]);
        }
                    JPluginHelper::importPlugin('groupsapi');
                    $dispatcher = JDispatcher::getInstance();
                    $message = array(
            'mtitle' => 'The circle has been removed!',
            'mdesc' => 'The circle has been removed, please contact to administrator for more infomation!'
                    );

        $dispatcher->trigger('pushNotification', array($message, $member_notifi, $circleid, 0, 'delete_circle', ''));
        
        CommunityModelGroups::deleteGroupMembers($circleid);
        CommunityModelGroups::deleteGroupBulletins($circleid);
        CommunityModelGroups::deleteGroupDiscussions($circleid);
        CommunityModelGroups::deleteGroupMedia($circleid);
        CommunityModelGroups::deleteGroupWall($circleid);
        CommunityModelGroups::deleteGroupEvents($circleid);
          
        // Delete group
        $group = JTable::getInstance('Group', 'CTable');
        $group->load($circleid);
        $groupData = $group;
        if ($group->delete($circleid)) {
            //CFactory::load( 'libraries' , 'featured' );
            $featured = new CFeatured(FEATURED_GROUPS);
            $featured->delete($circleid);

            $content = JText::_('COM_COMMUNITY_GROUPS_DELETED');

            //trigger for onGroupDelete
            $this->triggerGroupEvents('onAfterGroupDelete', $groupData);
        } else {
                $content = JText::_('COM_COMMUNITY_GROUPS_DELETE_ERROR');
            }
            
            $result = array();
            $result['comment'] = $content;
            echo json_encode($result);
            die;
        }
    }
