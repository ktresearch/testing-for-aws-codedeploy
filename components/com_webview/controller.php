<?php 
/*
 *  Com web view for app
 *  $package: web view
 *  $author: Kydon vietnam team
 *  $date: 22 Jul 2016
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

require_once(JPATH_SITE.'/components/com_joomdle/controller.php');

class WebviewController extends JControllerLegacy {
    
   public function redirectWebview() {
        // params
        
	$app    = JFactory::getApplication();
	$input  = $app->input;
	$method = $input->getMethod();
                
        $data = array();
	$data['username']  = $input->$method->get('username', '', 'USERNAME');
	$data['password']  = $input->$method->get('password', '', 'RAW');
	$data['wantsurl']  = $input->$method->get('wantsurl', '', 'RAW');
	$data['type']  = $input->$method->get('type', '', 'RAW');
        
        
	$credentials = array();
	$credentials['username']  = $data['username'];
	$credentials['password']  = $data['password'];
                
        jimport('joomla.user.helper');
        // Get a database object
        $db		= JFactory::getDbo();
        $query	= $db->getQuery(true);

        $query->select('id, password');
        $query->from('#__users');
        $query->where('username=' . $db->quote($credentials['username']));

        $db->setQuery($query);
        $resultapi = $db->loadObject();

        if($resultapi){
            $apicrypt = JUserHelper::verifyPassword($credentials['password'], $resultapi->password, $resultapi->id);
            if ($apicrypt === true) {
                $user_api = JUser::getInstance($resultapi->id); // Bring this in line with the rest of the system
                $login_data = base64_encode ($credentials['username'].':'.$credentials['password']);
                $url = $data['wantsurl'].'&type='.$data['type'];
                $wantsurl = base64_encode($url);
                JoomdleController::webviewLogin($login_data, $wantsurl);
            } else {
                    echo "Password does not match.";
                    exit();
               }
        } else{
              echo "Username or password does not exits";
              exit();
        }
    }
    
}
?>