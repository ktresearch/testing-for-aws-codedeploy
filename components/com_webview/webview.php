<?php
defined('_JEXEC') or die;

// Create the controller
$controller = JControllerLegacy::getInstance('webview');

// Perform the Request task
$controller->execute(JRequest::getCmd('task'));

// Redirect if set by the controller
$controller->redirect();
