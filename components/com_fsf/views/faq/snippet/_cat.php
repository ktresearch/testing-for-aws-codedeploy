<?php
/**
 * @package Freestyle Joomla
 * @author Freestyle Joomla
 * @copyright (C) 2013 Freestyle Joomla
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/
defined('_JEXEC') or die;
?>
<div class='faq_category <?php if ($this->view_mode_cat == "accordian") echo "accordion_toggler_$acl"; ?>'
     id="<?php echo $cat['id']; ?>">

    <?php if ($cat['image']) : ?>
        <div class='faq_category_image'>
            <?php if (substr($cat['image'], 0, 1) == "/") : ?>
                <img src='<?php echo JURI::root(true); ?><?php echo $cat['image']; ?>' width='64' height='64'>
            <?php else: ?>
                <img src='<?php echo JURI::root(true); ?>/images/fsf/faqcats/<?php echo $cat['image']; ?>' width='64'
                     height='64'>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <div class='faq_category_head'>
        <?php if ($cat['id'] == $this->curcatid) : ?><b><?php endif; ?>

            <?php if ($this->view_mode_cat == "popup") : ?>

                <a class="fsf_modal fsf_highlight"
                   href='<?php echo FSFRoute::x('&tmpl=component&limitstart=&catid=' . $cat['id'] . '&view_mode=' . $this->view_mode_incat); ?>'
                   rel="{handler: 'iframe', size: {x: 650, y: 375}}">
                    <?php echo $cat['title'] ?>
                </a>

            <?php elseif ($this->view_mode_cat == "accordian"): ?>
                <div class="fsf_highlight">
                    <span class="fsf_title"><?php echo $cat['title'] ?></span>
                    <span class="btn-icon btn-icon-right" id="<?php echo $cat['id']; ?>"></span>
                </div>


            <?php else: ?>

                <A class="fsf_highlight"
                   href='<?php echo FSFRoute::x('&limitstart=&catid=' . $cat['id']); ?>'><?php echo $cat['title'] ?></a>

            <?php endif; ?>

            <?php if ($cat['id'] == $this->curcatid) : ?></b><?php endif; ?>
    </div>
</div>





