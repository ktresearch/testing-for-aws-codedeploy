<?php
/**
 * @package Freestyle Joomla
 * @author Freestyle Joomla
 * @copyright (C) 2013 Freestyle Joomla
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/
$is_mobile = false;
$session = JFactory::getSession();
$device = $session->get('device');
if ($device == 'mobile') {
    $is_mobile = true;
}
$isGuest = (JFactory::getUser()->guest == 1) ? true : false;
$check_is_guest = "";
defined('_JEXEC') or die;

function start_faq_col_item()
{

}

function end_faq_col_item()
{

}

?>
<style type="text/css">
    .com_fsf.view-faq #txtKeyword.onkeyup {
        background-image: url('') !important;
    }
</style>
<!--<script src="components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>-->
<script type="text/javascript">
    jQuery(document).ready(function () {
        // Updated to Help Centre ( previously FAQ)
        var title_p = "<?php echo JText::_('HELP_CENTRE'); ?>";
        $('.navbar-header .navbar-title span').html(title_p);
        jQuery("input.search-faqs-box-tablet").keyup(function () {
            var value = jQuery(this).val();
            if (value != '') {
                jQuery(this).addClass('onkeyup');
            } else {
                jQuery(this).removeClass('onkeyup');
            }
        })
            .keyup();

    });

</script>
<!--<div class="fsf_title">
    <?php // echo FSF_Helper::PageTitle("FREQUENTLY_ASKED_QUESTIONS"); ?>
</div>-->
<div class="fsf_spacer"></div>
<style type="text/css">
    .t3-content {
        margin-left: 0px;
        margin-right: 0px;
    }
</style>
<?php $acl = 1; ?>

<!-- START SEARCH -->
<?php if (!$this->hide_search) : ?>
    <?php
//                  if ($column == 1)
//                  echo "<tr><td width='$colwidth' class='fsf_faq_cat_col_first' valign='top'>";
//              else
//                  echo "<td width='$colwidth' class='fsf_faq_cat_col' valign='top'>";
    ?>
    <div class='faq_category faq_search'>
        <?php if ($is_mobile) { ?>
            <div class='faq_category_image'>
                <img src='<?php echo JURI::root(true); ?>/components/com_fsf/assets/images/icon-search.png' width='20'
                     height='20'>
            </div>
        <?php } ?>
        <!--                <div class='faq_category_head' style="padding-top:6px;padding-bottom:6px;">
                    <?php // if ($this->curcatid == -1) : ?><b><?php // endif; ?>
                    <?php // echo JText::_("SEARCH_FAQS"); ?>
                    <?php // if ($this->curcatid == -1) : ?></b><?php // endif; ?>
                </div>-->
        <?php if ($is_mobile) { ?>
            <form action="<?php echo FSFRoute::x('index.php?option=com_fsf&view=faq'); ?>" method="get"
                  name="adminForm">
                <input type='hidden' name='option' value='com_fsf'/>
                <input type='hidden' name='Itemid' value='<?php echo JRequest::getVar('Itemid'); ?>'/>
                <input type='hidden' name='view' value='faq'/>
                <input type='hidden' name='catid' value='<?php echo $this->curcatid; ?>'/>
                <input class="search-faqs-box" name='search' value="<?php echo JViewLegacy::escape($this->search); ?>">
                <!--<input type='submit' class='button' value='<?php // echo JText::_("SEARCH"); ?>' >-->
            </form>
        <?php } else { ?>
            <form action="<?php echo FSFRoute::x('index.php?option=com_fsf&view=faq'); ?>" method="get"
                  name="adminForm">
                <input type='hidden' name='catid' value='

                    <?php echo $this->curcatid; ?>'/>
                <input type="text" name="keyword" size="30" id="txtKeyword" class="search-faqs-box-tablet"
                       placeholder="<?php echo JText::_('SEARCH_PLACEHOLDER'); ?>" name='search'
                       value="<?php echo JViewLegacy::escape($this->search); ?>">
                <input type='hidden' name='option' value='com_fsf'/>
                <input type='hidden' name='Itemid' value='<?php echo JRequest::getVar('Itemid'); ?>'/>
                <input type='hidden' name='view' value='faq'/>
                <!--    <img src='<?php echo JURI::root(true); ?>/components/com_fsf/assets/images/icon-search.png' width='20' height='20'> -->

                <!--<input type='submit' class='button' value='<?php // echo JText::_("SEARCH"); ?>' >-->
            </form>
        <?php } ?>
    </div>
    <div class='faq_category_faqlist'></div>

    <?php
//              if ($column == $this->num_cat_colums)
//                  echo "</td></tr>";
//              else
//                  echo "</td>";
//
//              $column++;
//              if ($column > $this->num_cat_colums)
//                  $column = 1;
    ?>

<?php endif; ?>
<!-- END SEARCH -->
<div class="support-page" style="background-color: #fff;">
    <?php if ($isGuest) :
        $check_is_guest = "guest";
        ?>
        <div class="support-page-banner">
            <div class="left">
                <a href="<?php echo JURI::root( false );?>">
                    <img alt="Parenthexis" src="templates/t3_bs3_tablet_desktop_template/images/PARENTHEXIS_LOGO_NEW.png">
                </a>
            </div>
            <div class="right">
                <a style="text-decoration: none;" href="<?php echo JURI::root( false );?>">
                    Log in
                </a>
                <a style="text-decoration: none;" href="<?php echo JRoute::_('index.php?option=com_community&view=register', false); ?>">
                    Sign up
                </a>
            </div>
        </div>
    <?php endif; ?>
    <div class="support-page-content <?php echo $check_is_guest; ?>">
        <img class="iconBack hidden" src="components/com_fsf/assets/images/expand.png">

        <h1 class="title">Support</h1>
        <?php if ($this->showcats) : ?>

            <?php // echo FSF_Helper::PageSubTitle("PLEASE_SELECT_YOUR_QUESTION_CATEGORY"); ?>
            <div class="fsf_faq_catlist" id='fsf_faq_catlist'>
                <?php $colwidth = floor(100 / $this->num_cat_colums) . "%"; ?>
                <?php $column = 1; ?>
                <table width='100%' cellspacing="0" cellpadding="0" style="background-color: #fff;">


                    <!-- ALL FAQS START -->
                    <?php if (!$this->hide_allfaqs) : ?>
                        <?php
                        if ($column == 1)
                            echo "<tr><td width='$colwidth' class='fsf_faq_cat_col_first' valign='top'>";
                        else
                            echo "<td width='$colwidth' class='fsf_faq_cat_col' valign='top'>";
                        ?>
                        <div class='faq_category'>
                            <!--                <div class='faq_category_image'>
                        <img src='<?php // echo JURI::root( true ); ?>/components/com_fsf/assets/images/allfaqs.png' width='64' height='64'>
                    </div>-->
                            <div class='faq_category_head'>
                                <?php if ($this->curcatid == 0) : ?><b><?php endif; ?>
                                    <A class="fsf_highlight"
                                       href='<?php echo FSFRoute::x('&limitstart=&catid=' . 0); ?>'><?php echo JText::_("ALL_FAQS"); ?></a>
                                    <?php if ($this->curcatid == 0) : ?></b><?php endif; ?>
                            </div>
                            <div class='faq_category_desc'><?php echo JText::_("VIEW_ALL_FREQUENTLY_ASKED_QUESTIONS"); ?></div>
                        </div>
                        <div class='faq_category_faqlist'></div>
                        <?php
                        if ($column == $this->num_cat_colums)
                            echo "</td></tr>";
                        else
                            echo "</td>";

                        $column++;
                        if ($column > $this->num_cat_colums)
                            $column = 1;
                        ?>

                    <?php endif; ?>
                    <!-- END ALL FAQS -->


                    <!-- TAGS START -->
                    <?php if (!$this->hide_tags) : ?>
                        <?php
                        if ($column == 1)
                            echo "<tr><td width='$colwidth' class='fsf_faq_cat_col_first' valign='top'>";
                        else
                            echo "<td width='$colwidth' class='fsf_faq_cat_col' valign='top'>";
                        ?>
                        <div class='faq_category'>
                            <!--                    <div class='faq_category_image'>
                        <img src='<?php // echo JURI::root( true ); ?>/components/com_fsf/assets/images/tags-64x64.png' width='64' height='64'>
                    </div>-->
                            <div class='faq_category_head'>
                                <?php if ($this->curcatid == 0) : ?><b><?php endif; ?>
                                    <A class="fsf_highlight"
                                       href='<?php echo FSFRoute::x('&limitstart=&catid=' . -4); ?>'><?php echo JText::_("TAGS"); ?></a>
                                    <?php if ($this->curcatid == 0) : ?></b><?php endif; ?>
                            </div>
                            <div class='faq_category_desc'><?php echo JText::_("VIEW_FAQ_TAGS"); ?></div>
                        </div>
                        <div class='faq_category_faqlist'></div>
                        <?php
                        if ($column == $this->num_cat_colums)
                            echo "</td></tr>";
                        else
                            echo "</td>";

                        $column++;
                        if ($column > $this->num_cat_colums)
                            $column = 1;
                        ?>

                    <?php endif; ?>
                    <!-- END TAGS -->

                    <!-- FEATURED FAQS START -->
                    <?php
                    if ($this->show_featured) {

                        if ($column == 1)
                            echo "<tr><td width='$colwidth' class='fsf_faq_cat_col_first' valign='top'>";
                        else
                            echo "<td width='$colwidth' class='fsf_faq_cat_col' valign='top'>";

                        // set up fake $cat object and include the _cat.php template
                        $cat = array();
                        $cat['image'] = '/components/com_fsf/assets/images/featured.png';
                        $cat['id'] = -5;
                        $cat['title'] = JText::_('FEATURED_FAQS');
                        $cat['description'] = JText::_('VIEW_FEATURED_FREQUENTLY_ASKED_QUESTIONS');
                        $cat['faqs'] = array();
                        if (!empty($this->featured_faqs))
                            $cat['faqs'] = $this->featured_faqs;

                        include JPATH_SITE . DS . 'components' . DS . 'com_fsf' . DS . 'views' . DS . 'faq' . DS . 'snippet' . DS . '_cat.php';

                        if ($column == $this->num_cat_colums)
                            echo "</td></tr>";
                        else
                            echo "</td>";

                        $column++;
                        if ($column > $this->num_cat_colums)
                            $column = 1;
                    }
                    ?>
                    <!-- END FEATURED FAQS -->

                    <!-- ALL CATS -->
                    <?php $i = 1; ?>
                    <?php foreach ($this->catlistparent as $cat) : ?>
                        <?php
                        if ($column == 1)
                            echo "<tr><td width='$colwidth' class='fsf_faq_cat_col_first' valign='top'>";
                        else
                            echo "<td width='$colwidth' class='fsf_faq_cat_col' valign='top'>";
                        ?>
                        <?php include JPATH_SITE . DS . 'components' . DS . 'com_fsf' . DS . 'views' . DS . 'faq' . DS . 'snippet' . DS . '_cat.php' ?>
                        <?php
                        if ($column == $this->num_cat_colums)
                            echo "</td></tr>";
                        else
                            echo "</td>";

                        $column++;
                        $i++;
                        if ($column > $this->num_cat_colums)
                            $column = 1;
                        ?>
                    <?php endforeach; ?>
                    <!-- END CATS -->
                    <!-- CAT LIST END -->
                    <?php
                    if ($column > 1) {
                        while ($column <= $this->num_cat_colums) {
                            echo "<td class='fsf_faq_cat_col' valign='top'><div class='faq_category'></div></td>";
                            $column++;
                        }
                        echo "</tr>";
                        $column = 1;
                    }
                    ?>

                    <!-- <tr><td colspan='<?php echo $this->num_cat_colums; ?>'>
            <div class='faq_category_footer'></div>
        </td></tr> -->
                </table>

            </div>
            <div class="fsf_faq_queslist hidden">
                <!-- ALL QUESTION CATS -->

                <!-- INLINE FAQS -->
                <?php foreach ($this->catlist as $cat) : ?>

                    <?php if ($this->view_mode_cat == "inline" || $this->view_mode_cat == "accordian") : ?>
                        <div class='faq_category_faqlist hidden <?php if($cat['parent_category'] != null){ echo "sub_category sub_category_".$cat['parent_category']; } ?>'
                             id="faq_category_faqlist_<?php echo $cat['id']; ?>" parent="<?php echo $cat['parent_category']; ?>">
                            <p class="title_category"><?php echo $cat['title']; ?></p>
                            <span class="shape1"></span>
                            <?php if ($this->view_mode_cat == "accordian") $acl = 2; ?>
                            <?php $this->view_mode = $this->view_mode_incat; ?>
                            <?php if (array_key_exists('faqs', $cat) && count($cat['faqs']) > 0): ?>
                                <?php foreach ($cat['faqs'] as &$faq) : ?>
                                    <?php include JPATH_SITE . DS . 'components' . DS . 'com_fsf' . DS . 'views' . DS . 'faq' . DS . 'snippet' . DS . '_faq.php' ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php if ($this->view_mode_cat == "accordian") $acl = 1; ?>
                        </div>
                    <?php endif; ?>
                    <!-- END INLINE FAQS -->
                <?php endforeach; ?>

                <!-- END QUESTION CATS -->
            </div>


        <?php endif; ?>
        <?php echo $this->showfaqs; ?>
        <?php if ($this->showfaqs) : ?>

            <div class='faq_category'>
                <?php if ($this->curcatimage) : ?>
                    <div class='faq_category_image'>
                        <?php if (substr($this->curcatimage, 0, 1) == "/") : ?>
                            <img src='<?php echo JURI::root(true); ?><?php echo $this->curcatimage; ?>' width='64'
                                 height='64'>
                        <?php else: ?>
                            <img src='<?php echo JURI::root(true); ?>/images/fsf/faqcats/<?php echo $this->curcatimage; ?>'
                                 width='64' height='64'>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <div class='fsf_spacer contentheading' style="padding-top:6px;padding-bottom:6px;">

                    <?php if (FSF_Settings::Get('faq_cat_prefix') || !$this->curcattitle): ?>
                        <?php echo JText::_("FAQS"); ?>
                        <?php if ($this->curcattitle) echo " - "; ?>
                    <?php endif; ?>
                    <?php echo $this->curcattitle; ?>

                </div>
                -->
                <div class='faq_category_desc'><?php echo $this->curcatdesc; ?></div>
            </div>
            <div class='fsf_clear'></div>


            <?php if ($this->curcatid == -1): ?>
                <div class='faq_category'>
                    <!--            <div class='faq_category_image'>
                <img src='<?php // echo JURI::root( true ); ?>/components/com_fsf/assets/images/search.png' width='64' height='64'>
            </div>-->
                    <!--            <div class='faq_category_head' style="padding-top:6px;padding-bottom:6px;">
                <?php // echo JText::_("SEARCH_FAQS"); ?>
            </div>-->

                    <!--            <form action="<?php // echo FSFRoute::x( 'index.php?option=com_fsf&view=faqs' );?>" method="get" name="adminForm">
                <input type='hidden' name='option' value='com_fsf' />
                <input type='hidden' name='Itemid' value='<?php // echo JRequest::getVar('Itemid'); ?>' />
                <input type='hidden' name='view' value='faq' />
                <input type='hidden' name='catid' value='<?php // echo $this->curcatid; ?>' />
                <input name='search' value="<?php // echo JViewLegacy::escape($this->search); ?>">
                <input type='submit' class='button' value='<?php // echo JText::_("SEARCH"); ?>' >
            </form>-->
                </div>
            <?php endif; ?>


            <div class='fsf_faqs' id='fsf_faqs'>
                <?php if (count($this->items)) foreach ($this->items as $faq) : ?>
                    <?php include JPATH_SITE . DS . 'components' . DS . 'com_fsf' . DS . 'views' . DS . 'faq' . DS . 'snippet' . DS . '_faq.php';
                    //include "components/com_fsf/views/faq/snippet/_faq.php" ?>
                <?php endforeach; ?>
                <?php if (count($this->items) == 0): ?>
                    <div class="fsf_no_results"><?php echo JText::_("NO_FAQS_MATCH_YOUR_SEARCH_CRITERIA"); ?></div>
                <?php endif; ?>
            </div>

            <?php if ($this->enable_pages): ?>
                <form id="adminForm"
                      action="<?php echo FSFRoute::x('index.php?option=com_fsf&view=faq&catid=' . $this->curcatid); ?>"
                      method="post" name="adminForm">
                    <input type='hidden' name='catid' value='<?php echo $this->curcatid; ?>'/>
                    <input type='hidden' name='enable_pages' value='<?php echo $this->enable_pages; ?>'/>
                    <input type='hidden' name='view_mode' value='<?php echo $this->view_mode; ?>'/>
                    <?php echo $this->pagination->getListFooter(); ?>
                </form>
            <?php endif; ?>

        <?php endif; ?>
        <div class="btBack hidden"><span>Back</span></div>

        <?php // include JPATH_SITE.DS.'components'.DS.'com_fsf'.DS.'_powered.php'; ?>
        <?php if (FSF_Settings::get('glossary_faqs')) echo FSF_Glossary::Footer(); ?>

        <?php echo FSF_Helper::PageStyleEnd(); ?>
    </div>

    <?php if ($isGuest) :?>
        <div class="connect-with-us">
            <p><?php echo JText::_('COM_COMMUNITY_TERMS_CONNECT_WITH_US');?></p>
            <div class="connect-icons">
                <a class="fbook" href="https://www.facebook.com/parenthexis/"><img src="<?php echo JURI::root(true); ?>/images/login/facebook_icon.png"></a>
                <a class="insta" href="https://www.instagram.com/parenthexis/"><img src="<?php echo JURI::root(true); ?>/images/login/instagram_icon.png"></a>
                <a class="email" href="mailto:contact@parenthexis.com"><img src="<?php echo JURI::root(true); ?>/images/login/email_icon.png"></a>
            </div>
        </div>
        <div class="div-footer-menu">
            <ul>
                <li><a href="<?php echo JURI::root(true); ?>/about"><?php echo JText::_('COM_COMMUNITY_ABOUT'); ?></a></li>
                <li><a href="<?php echo JURI::root(true); ?>/terms"><?php echo JText::_('COM_COMMUNITY_TERMS'); ?></a></li>
                <li><a href="<?php echo JURI::root(true); ?>/privacy-policy"><?php echo JText::_('COM_COMMUNITY_PRIVACY_POLICY'); ?></a></li>
                <li><a href="<?php echo JURI::root(true); ?>/support"><?php echo JText::_('COM_COMMUNITY_SUPPORT'); ?></a></li>
                <li><a href="<?php echo JURI::root(true); ?>/partners"><?php echo JText::_('COM_COMMUNITY_PARTNERS'); ?></a></li>
                <li><a href="<?php echo JURI::root(true); ?>/business"><?php echo JText::_('COM_COMMUNITY_BUSINESS'); ?></a></li>

            </ul>
        </div>
        <div class="divCopyright">
            <p>© <?php echo Date("Y", time());?> PARENTHEXIS</p>
        </div>
    <?php endif; ?>

</div>
<script type="text/javascript">
    jQuery(function ($) {
        jQuery('.right-column').click(function () {
            jQuery('#t3-mainnav .navbar-menu-icon').removeClass('show');
            jQuery('.t3-wrapper .right-column').removeClass('show-secondmenu');
            jQuery('.t3-wrapper .tab.left-column').removeClass('show-secondmenu');
            jQuery('.navbar-fixed-top').removeClass('show-secondmenu');
            jQuery('.course-menu').removeClass('show-secondmenu');
            jQuery('.joomdle-overview-header').removeClass('show-secondmenu');
            jQuery('.navbar-menu-icon .t3-megamenu > ul > li').removeClass('active');
        });
    });

</script>


<script type="text/javascript">
    (function ($) {
        $(".iconBack").click(function (e) {
            $(".fsf_faq_catlist").removeClass('hidden');
            $(".fsf_faq_queslist").addClass('hidden');

            if ($(".faq_category_faqlist").siblings().not('.hidden')) {
                $(".faq_category_faqlist").addClass('hidden');
            }
            if ($(".fsf_faq_answer").siblings().not('.hidden')) {
                $(".fsf_faq_answer").addClass('hidden');
            }

            if ($(".iconBack").siblings().not('.hidden')) {
                $(".iconBack").addClass('hidden');
            }

            if ($(".btBack").siblings().not('.hidden')) {
                $(".btBack").addClass('hidden');
            }
            if ($(".expand_answer").hasClass('expanded')) {
                $(".expand_answer").removeClass('expanded');
            }
        });

        $(".btBack").click(function (e) {
            $(".fsf_faq_catlist").removeClass('hidden');
            $(".fsf_faq_queslist").addClass('hidden');

            if ($(".faq_category_faqlist").siblings().not('.hidden')) {
                $(".faq_category_faqlist").addClass('hidden');
            }
            if ($(".fsf_faq_answer").siblings().not('.hidden')) {
                $(".fsf_faq_answer").addClass('hidden');
            }

            if ($(".btBack").siblings().not('.hidden')) {
                $(".btBack").addClass('hidden');
            }

            if ($(".iconBack").siblings().not('.hidden')) {
                $(".iconBack").addClass('hidden');
            }
            if ($(".expand_answer").hasClass('expanded')) {
                $(".expand_answer").removeClass('expanded');
            }
        });

        $(".faq_category").click(function (e) {
            var id_cat = $(this).attr('id');
            // category
            $(".fsf_faq_catlist").addClass('hidden');

            // question, answer list
            $(".fsf_faq_queslist").removeClass('hidden');

            // question, answer of categoty
            $("#faq_category_faqlist_" + id_cat).removeClass('hidden');
            $(".sub_category_" + id_cat).removeClass('hidden');

            if($(".btBack").hasClass('hidden')){
                $(".btBack").removeClass('hidden');
            }
            if($(".iconBack").hasClass('hidden')){
                $(".iconBack").removeClass('hidden');
            }
        });

        $(".expand_answer").click(function (e) {
            var id_ques = $(this).attr('id');
            $('#ans_' + id_ques).toggleClass('hidden');
            $(this).toggleClass('expanded');
        });

        $(".fsf_faq_question").click(function (e) {
            var id_ques = $(this).attr('id');
            $('#ans_' + id_ques).toggleClass('hidden');
            $("span[id="+id_ques+"]").toggleClass("expanded");
        });


        $(".link_to").click(function (e){
            var link = $(this).attr('id');
            var link_to_id = 'go_'+link;
            var faq_category_faqlist_element = $("#"+link_to_id).parents().parents().parents().attr('id');
            var faq_category_faqlist_element_parent = $("#"+link_to_id).parents().parents().parents().attr('parent');
            var faq_category_faqlist_answer_element = $("#"+link_to_id).parents().attr('id');

            if ($(".expand_answer").hasClass('expanded')) {
                $(".expand_answer").removeClass('expanded');
            }
            if ($(".fsf_faq_answer").siblings().not('.hidden')) {
                $(".fsf_faq_answer").addClass('hidden');
            }

            if ($(".faq_category_faqlist").siblings().not('.hidden')) {
                $(".faq_category_faqlist").addClass('hidden');
            }

            if((faq_category_faqlist_element_parent != null) && ($(".sub_category_"+faq_category_faqlist_element_parent).hasClass('hidden'))){
                $(".sub_category_"+faq_category_faqlist_element_parent).removeClass('hidden');
            }

            if($("#"+faq_category_faqlist_answer_element).hasClass('hidden')) {
                $("#" + faq_category_faqlist_answer_element).removeClass('hidden');
            }

            if((faq_category_faqlist_element_parent !== '') && ($("#faq_category_faqlist_"+faq_category_faqlist_element_parent).hasClass('hidden'))) {
                $("#faq_category_faqlist_" + faq_category_faqlist_element_parent).removeClass('hidden');
                $("span[answer="+faq_category_faqlist_answer_element+"]").addClass("expanded");
                window.location.replace('<?php echo JUri::base()."support"; ?>#'+link_to_id);
                window.scrollBy(0, -200);
            }

            if((faq_category_faqlist_element_parent === '') && ($("#"+faq_category_faqlist_element).hasClass('hidden'))){
                $("#" + faq_category_faqlist_element).removeClass('hidden');
                $("span[answer="+faq_category_faqlist_answer_element+"]").addClass("expanded");
                window.location.replace('<?php echo JUri::base()."support"; ?>#'+link_to_id);
                window.scrollBy(0, -200);
            }
        });

    })(jQuery);
</script>
