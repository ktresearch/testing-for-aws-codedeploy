<?php
/**
 * Joomla! 1.5 component Joomdle
 *
 * @version $Id: controller.php 2009-04-17 03:54:05 svn $
 * @author Antonio DurÃ¡n TerrÃ©s
 * @package Joomla
 * @subpackage Joomdle
 * @license GNU/GPL
 *
 * Shows information about Moodle courses
 *
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/content.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/parents.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/shop.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/applications.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/mappings.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/system.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/profiletypes.php');

/**
 * Joomdle Component Controller
 */
class JoomdleController extends JControllerLegacy {

	function display ($cachable = false, $urlparams = false) {

		JFactory::require_login();
        //document object
        $jdoc = JFactory::getDocument();
        //add the stylesheet
        $jdoc->addStyleSheet(JURI::root ().'components/com_joomdle/css/joomdle.css');

        // Make sure we have a default view
        if( !JRequest::getVar( 'view' )) {
		    JRequest::setVar('view', 'joomdle' );
        } else {
		$view = JRequest::getVar( 'view' );
		JRequest::setVar('view', $view );
		}

        $mainframe =JFactory::getApplication();
        $document  =JFactory::getDocument();
        $pathway   = $mainframe->getPathway();

		parent::display();
	}

    public function ajaxShowLearningDefaultPage() {
        $document = JFactory::getDocument();
        $viewType = $document->getType();
        $viewName = $this->input->get('view', $this->default_view);
        $viewLayout = $this->input->get('layout', 'default', 'string');

        $view = $this->getView('mylearning', $viewType, '', array('base_path' => $this->basePath, 'layout' => $viewLayout));

        $html = $view->showDefaultPage();

        $json = array(
            'success' => true,
            'html' => $html
        );

        die( json_encode($json) );
    }

    public function ajaxShowManageDefaultPage() {
        $document = JFactory::getDocument();
        $viewType = $document->getType();
        $viewName = $this->input->get('view', $this->default_view);
        $viewLayout = $this->input->get('layout', 'default', 'string');

        $view = $this->getView('mycourses', $viewType, '', array('base_path' => $this->basePath, 'layout' => $viewLayout));

        $html = $view->showDefaultPage();

        $json = array(
            'success' => true,
            'html' => $html
        );

        die( json_encode($json) );
    }

	/* User enrols manually from Joomla */
	function enrol () {

		$mainframe = JFactory::getApplication();

		$user = JFactory::getUser();

		$course_id = JRequest::getVar( 'course_id' );
		$course_id = (int) $course_id;

		$login_url = JoomdleHelperMappings::get_login_url ($course_id);
		if (!$user->id)
			$mainframe->redirect($login_url);

		$params =$mainframe->getParams();

		/* Check that self enrolments are OK in course */
		$enrol_methods = JoomdleHelperContent::call_method ('course_enrol_methods', $course_id);
		$self_ok = false;
		foreach ($enrol_methods as $method)
		{
			if ($method['enrol'] == 'self')
			{
				$self_ok = true;
				break;
			}
		}

		if (!$self_ok)
		{
			$url = JRoute::_ ("index.php?option=com_joomdle&view=detail&course_id=$course_id");
			$message = JText::_( 'COM_JOOMDLE_SELF_ENROLMENT_NOT_PERMITTED' );
			$this->setRedirect($url, $message);
			return;
		}


		$user = JFactory::getUser();
		$username = $user->get('username');
		JoomdleHelperContent::enrolUser ($username, $course_id);

		// Redirect to course
		$url = JoomdleHelperContent::get_course_url ($course_id);
		$mainframe->redirect ($url);
	}

	function applicate () {

		$mainframe = JFactory::getApplication();

		$params =$mainframe->getParams();
		$show_motivation = $params->get( 'show_detail_application_motivation', 'no' );
		$show_experience = $params->get( 'show_detail_application_experience', 'no' );

		$user = JFactory::getUser();

		$course_id = JRequest::getVar( 'course_id' );
		$course_id = (int) $course_id;

		$login_url = JoomdleHelperMappings::get_login_url ($course_id);
		if (!$user->id)
			$mainframe->redirect($login_url);
		//	$mainframe->redirect(JURI::base ().'index.php?option=com_user&view=login');

		$motivation = JRequest::getVar( 'motivation' );
		$experience = JRequest::getVar( 'experience' );

		$message = '';
		if (($show_motivation == 'mandatory') && (!$motivation))
		{
			$url = JRoute::_ ("index.php?option=com_joomdle&view=detail&course_id=$course_id");
			$message = JText::_( 'COM_JOOMDLE_MOTIVATION_MISSING' );
			$this->setRedirect($url, $message);
			return;
		}
		if (($show_experience == 'mandatory') && (!$experience))
		{
			$url = JRoute::_ ("index.php?option=com_joomdle&view=detail&course_id=$course_id");
			$message = JText::_( 'COM_JOOMDLE_EXPERIENCE_MISSING' );
			$this->setRedirect($url, $message);
			return;
		}

		$user = JFactory::getUser();
		$username = $user->get('username');

		$message = JText::_( 'COM_JOOMDLE_MAX_APPLICATIONS_REACHED' );
		if (!JoomdleHelperApplications::user_can_applicate ($user->id, $course_id, $message))
        {
            $url = JRoute::_ ("index.php?option=com_joomdle&view=detail&course_id=$course_id");
            $this->setRedirect($url, $message);
            return;
        }


		if (JoomdleHelperApplications::applicate_for_course ($username, $course_id, $motivation, $experience))
		{
			// Redirect to course detail page by default
			$url = JRoute::_ ("index.php?option=com_joomdle&view=detail&course_id=$course_id");
			$message = JText::_( 'COM_JOOMDLE_APPLICATION_FOR_COURSE_DONE' );

			// Get custom redirect url and message
			$additional_message = '';
			$new_url = '';
			$app                = JFactory::getApplication();
			$results = $app->triggerEvent('onCourseApplicationDone', array($course_id, $user->id, &$additional_message, &$new_url));

			if ($additional_message)
				$message .= '<br>' . $additional_message;
			if ($new_url)
				$url = $new_url;
		}
		else {
			$url = JRoute::_ ("index.php?option=com_joomdle&view=detail&course_id=$course_id");
			$message = JText::_( 'COM_JOOMDLE_APPLICATION_FOR_COURSE_ALREADY_DONE' );
		}



		//$mainframe->redirect ($url);
		$this->setRedirect($url, $message);
	}


	function assigncourses ()
	{

		$children = JRequest::getVar( 'children' );

		if (!JoomdleHelperParents::check_assign_availability ($children))
		{
			$message = JText::_( 'COM_JOOMDLE_NOT_ENOUGH_COURSES' );
			$this->setRedirect('index.php?option=com_joomdle&view=assigncourses', $message); //XXX poenr un get current uri
		}
		else
		{
			JoomdleHelperParents::assign_courses ($children);
			$message = JText::_( 'COM_JOOMDLE_COURSES_ASSIGNED' );
			$this->setRedirect('index.php?option=com_joomdle&view=assigncourses', $message); //XXX poenr un get current uri
		}
	}

	function register_save ()
	{

		$otherlanguage = JFactory::getLanguage();
		$otherlanguage->load( 'com_user', JPATH_SITE );

		$usersConfig =JComponentHelper::getParams( 'com_users' );
		if ($usersConfig->get('allowUserRegistration') == '0') {
				JError::raiseError( 403, JText::_( 'Access Forbidden' ));
				return;
		}

		$authorize      = JFactory::getACL();
		$user = new JUser ();

		$system = 2; // ID of Registered
		$user->groups = array ();
		$user->groups[] = $system;


		// Bind the post array to the user object
		if (!$user->bind( JRequest::get('post'), 'usertype' )) {
				JError::raiseError( 500, $user->getError());
		}

		// Set some initial user values
		$user->set('id', 0);

		$date = JFactory::getDate();
		$user->set('registerDate', $date->toSql());

		$parent = JFactory::getUser();
		$user->setParam('u'.$parent->id.'_parent_id', $parent->id);

		// If user activation is turned on, we need to set the activation information
		$useractivation = $usersConfig->get( 'useractivation' );
		if ($useractivation == '1')
		{
				jimport('joomla.user.helper');
				$user->set('activation', JApplication::getHash( JUserHelper::genRandomPassword()) );
				$user->set('block', '1');
		}

		// If there was an error with registration, set the message and display form
		if ( !$user->save() )
		{
				JError::raiseWarning('', JText::_( $user->getError()));
				$this->setRedirect('index.php?option=com_joomdle&view=register'); //XXX poenr un get current uri
				return false;
		}

		// Add to profile type if needed
        $params =JComponentHelper::getParams( 'com_joomdle' );
        $children_pt = $params->get('children_profiletype');
        if ($children_pt)
        {
            JoomdleHelperProfiletypes::add_user_to_profile ($user->id, $children_pt);
        }

		// Send registration confirmation mail
		$password = JRequest::getString('password', '', 'post', JREQUEST_ALLOWRAW);
		$password = preg_replace('/[\x00-\x1F\x7F]/', '', $password); //Disallow control chars in the email
	   // UserController::_sendMail($user, $password);
		JoomdleHelperSystem::send_registration_email ($user->username, $password);


		$parent_user   = JFactory::getUser();
		// Set parent role in Moodle
		JoomdleHelperContent::call_method ("add_parent_role", $user->username, $parent_user->username);

		$message = JText::_( 'COM_JOOMDLE_USER_CREATED' );
		$this->setRedirect('index.php?option=com_joomdle&view=register', $message); //XXX poenr un get current uri
	}

	function login ()
	{
		$mainframe = JFactory::getApplication();

		$params = &$mainframe->getParams();
		$moodle_url = $params->get( 'MOODLE_URL' );

		$login_data =  JRequest::getVar( 'data' );
		$wantsurl =  JRequest::getVar( 'wantsurl' );

		$type =  JRequest::getVar( 'type' );

		if (!$login_data)
		{
			echo "Login error";
			exit ();
		}

		$data = base64_decode ($login_data);

		$fields = explode (':', $data);

		$credentials['username'] = $fields[0];
		$credentials['password'] = $fields[1];

		$options = array ('skip_joomdlehooks' => '1');

		$mainframe->login($credentials, $options);
                if($type == 'appview') {
                    $_SESSION['type'] = 'appview';
                }
                if($type == 'appview') {
                    $wantsurl = base64_decode($wantsurl);
                } else {
                    if (!$wantsurl)
                            $wantsurl = $moodle_url;
                }
		$mainframe->redirect( $wantsurl );

	}

	/* User unenrols manually from Joomla */
	function unenrol () {

		$mainframe = JFactory::getApplication();

		$user = JFactory::getUser();

		$course_id = JRequest::getVar( 'course_id' );
		$course_id = (int) $course_id;

		$login_url = JoomdleHelperMappings::get_login_url ($course_id);
		if (!$user->id)
			$mainframe->redirect($login_url);

		$params =$mainframe->getParams();

		$user = JFactory::getUser();
		$username = $user->username;
		JoomdleHelperContent::call_method ('unenrol_user', $username, $course_id);

		// Redirect to caller URI
		$url = htmlspecialchars($_SERVER['HTTP_REFERER']);
		$mainframe->redirect ($url);
	}
        /*
         * function not use
         *
        function getGroup($groupname) {

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query
                  ->select('*')
                  ->from($db->quoteName('#__community_groups'))
                  ->where($db->quoteName('name')."=".$db->quote($groupname));

           $db->setQuery($query);
           $groups = $db->loadObject();
           return $groups;
    }*/
    /*
     * KV team updated 15 jul 2016
     * save grade for user by facilitator
     */
    function saveGrade() {

        $username = JRequest::getVar( 'username' );
        $grade_id = JRequest::getVar( 'grade_id' );
        $gradeitem_id = JRequest::getVar( 'gradeitem_id' );
        $grade_value = JRequest::getVar( 'grade_value' );
        $feedback = JRequest::getVar( 'feedback' );
        $course = JRequest::getVar( 'course' );

        $grade_id = (int) $grade_id;
        $grade_value = (float) $grade_value;
        $gradeitem_id = (int) $gradeitem_id;

        // call api save data
        JoomdleHelperContent::call_method ('save_grade_user', $username, $grade_id, $gradeitem_id, $grade_value, $feedback);

//        $message = JText::_( 'COM_JOOMDLE_GRADE_SAVED' );
	$this->setRedirect('index.php?option=com_joomdle&view=overallgrade&course_id='.$course); //XXX poenr un get current uri

    }

    function saveStudentGrade() {

        $activity = JRequest::getVar('activite');
        $actyvity_type = JRequest::getVar('mod');
        $gradeitem = JRequest::getVar('gradeitem');
        $gradeid = JRequest::getVar('gradeid');
        $feedback = JRequest::getVar('feedback');
        $feedback_grade = JRequest::getVar('feedbackgrade');
        $status = JRequest::getVar('status');
        $username = JRequest::getVar('username');
        $grade_value = JRequest::getVar('grade_value');
        $courseid = JRequest::getVar('courseid');
        $type = JRequest::getVar('type');
        $grade_course_value = JRequest::getVar('grade_course_value');
        $feedback_course_grade = JRequest::getVar('feedbackgrade_course');
        $gradeid_course = JRequest::getVar( 'coursegradeid' );
        $gradeitemid_course = JRequest::getVar( 'coursegradeitemid' );
        // 1 - case for activity support grade
        for($i = 0; $i < count($activity); $i++) {
            $gradeitemi = (int)$gradeitem[$i];

            if($gradeitemi > 0) {
                // call api update grade and feedback
                JoomdleHelperContent::call_method ('save_grade_user', (string) $username, (int) $gradeid[$i], $gradeitemi, (float) $grade_value[$i], (string) $feedback_grade[$i]);
            }
            // 2 - save all status and feedback
            JoomdleHelperContent::call_method ('save_completion_activity', (string) $username, (int) $activity[$i], $status[$i], (string) $feedback[$i]);
        }

        // 3 - save grade course
        JoomdleHelperContent::call_method ('save_grade_user', (string) $username, (int) $gradeid_course, (int) $gradeitemid_course, (float) $grade_course_value, (string) $feedback_course_grade);

        if($type == 'activity_grade') {
            $this->setRedirect(JUri::base().'activitydetail/'.$courseid.'_'.$activity[0].'_'.$actyvity_type[0].'.html');
        } else {
            $this->setRedirect(JUri::base().'viewstudents/'.$courseid.'.html');
        }
    }

    function downloadSubmission() {

		$activity = JRequest::getVar('assignid');
		$courseid = JRequest::getVar('course_id');

		$files = JoomdleHelperContent::call_method('get_submission_file', (int)$activity);
//        $urltes = 'http://s.hswstatic.com/gif/animal-stereotype-orig.jpg';
		if (!$files || empty($files)) {
			$url = JRoute::_("index.php?option=com_joomdle&view=activitydetail&course_id=$courseid&activity=$activity&type=assign");
			$message = JText::_('COM_JOOMDLE_SUBMISSION_ERROR');
			$this->setRedirect($url, $message);
			return;
		}

        $zip = new ZipArchive();
        $zip_name = 'submission.zip'; // Zip name
        $zip->open($zip_name,  ZipArchive::CREATE);
        foreach ($files as $file) {
            echo $path = "uploadpdf/".$file;
            if(file_exists($file['filepath'])){
                $zip->addFromString(basename($file['filename']),  file_get_contents($file['filepath']));
            }
            else{
                echo"file does not exist";
            }
        }
        $zip->close();
		$mime_type = "application/zip";
		@ob_end_clean();
		@clearstatcache();

		if (ini_get('zlib.output_compression')) {
			ini_set('zlib.output_compression', 'Off');
		}

		header('Content-Type: ' . $mime_type);
		header('Content-Disposition: attachment; filename="' . $zip_name . '"');
		header("Content-Transfer-Encoding: binary");
		header('Accept-Ranges: bytes');
		header('Expires: 0');
		header('Cache-Control: no-cache, must-revalidate');
		header('Pragma: no-cache');
		header('Content-Length: ' . filesize($zip_name));
		readfile("$zip_name");
		unlink("$zip_name");
		exit();
//        flush();
		/*$chunksize = 1 * (1024 * 1024); //you may want to change this

        $bytes_send = 0;
        if ($file_d = fopen($files, 'rb')) {
            if (isset($_SERVER['HTTP_RANGE']))
                fseek($file_d, 0);

            while (!feof($file_d) && (!connection_aborted()) && ($bytes_send < $filesize)) {
                $buffer = fpassthru($file_d, $chunksize);
                print($buffer); //echo($buffer); // is also possible
                flush();
                $bytes_send += strlen($buffer);
            }
            fclose($file_d);
        } else
            die('Error - can not open file.');*/
//        }

	}
    function downloadSubmissionUser() {

        $activity = JRequest::getVar('assignid');
        $courseid = JRequest::getVar('course_id');
        $userid = JRequest::getVar('userid');
        $user = JFactory::getUser($userid);
        $username = $this->VNtoEN($user->name);


        $files = JoomdleHelperContent::call_method('get_submission_file', (int)$activity,$userid);
    //        $urltes = 'http://s.hswstatic.com/gif/animal-stereotype-orig.jpg';
        if (!$files || empty($files)) {
                $url = JRoute::_("index.php?option=com_joomdle&view=activitydetail&course_id=$courseid&activity=$activity&type=assign");
                $message = JText::_('COM_JOOMDLE_SUBMISSION_ERROR');
                $this->setRedirect($url, $message);
                return;
        }

        foreach ($files as $file) {
            if(file_exists($file['filepath'])) {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename='.basename($username.'_'.$file['filename']));
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file['filepath']));
                    ob_clean();
                    flush();
                    readfile($file['filepath']);
                    exit;
            }
            else{
                echo"file does not exist";
            }
        }
    }
    function VNtoEN($str)
    {
    // In thường
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(,)/", '_', $str);
    // In đậm
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        $str = preg_replace("/(,)/", '_', $str);
        return $str; // Trả về chuỗi đã chuyển
    }
    function generateRandomString($length = 10) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
    }
    function download() {
        $params = JComponentHelper::getParams( 'com_joomdle' );
        $moodle_url = $params->get( 'MOODLE_URL');
        $url = 'http://test2.parenthesis.asia/components/com_community/assets/group.png';
		$moodle_file_url = 'http://localhost/parentheXisv2/courses/pluginfile.php/18347/assignsubmission_file/submission_files/53/New member card 2.docx';
		$result = JoomdleHelperContent::get_file ($moodle_file_url);
        $output_filename = '/home/kydonvn/Downloads/'.  basename($moodle_file_url);

        $local_file = "/home/kydonvn/Downloads/New member card 2.docx";//This is the file where we save the information

        if(file_exists($local_file)){
            unlink($local_file);
        }
        $fp = fopen($local_file,'x+');
        fwrite($fp, $result);
        fclose($fp);

        /*$ch = curl_init();
        $fp = fopen ($local_file, 'w+');
        $ch = curl_init($moodle_file_url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);*/
        /*$name = basename($url);
        OB_END_CLEAN();
        $path = "/home/kydonvn/Downloads/".$name;
        IF (!IS_FILE($path) or CONNECTION_STATUS()!=0) RETURN(FALSE);
        HEADER("Cache-Control: no-store, no-cache, must-revalidate");
        HEADER("Cache-Control: post-check=0, pre-check=0", FALSE);
        HEADER("Pragma: no-cache");
        HEADER("Expires: ".GMDATE("D, d M Y H:i:s", MKTIME(DATE("H")+2, DATE("i"), DATE("s"), DATE("m"), DATE("d"), DATE("Y")))." GMT");
        HEADER("Last-Modified: ".GMDATE("D, d M Y H:i:s")." GMT");
        HEADER("Content-Type: application/octet-stream");
        HEADER("Content-Length: ".(string)(FILESIZE($path)));
        HEADER("Content-Disposition: inline; filename=$name");
        HEADER("Content-Transfer-Encoding: binary\n");
        IF ($file = FOPEN($path, 'rb')) {
         WHILE(!FEOF($file) and (CONNECTION_STATUS()==0)) {
           PRINT(FREAD($file, 1024*8));
           FLUSH();
         }
         FCLOSE($file);
        }
        RETURN((CONNECTION_STATUS()==0) and !CONNECTION_ABORTED());
        exit;*/
    }

    function saveQuestion() {
        $feedback = array();
        $question = array();
        $questionChoose = JRequest::getVar('questionnum');

        $intQuestion_array = array();

        if (JRequest::getVar('moduleid')) $feedback['mid'] = JRequest::getVar('moduleid');

        $feedback['title'] = JRequest::getVar('txtTitle');
        $feedback['description'] = JRequest::getVar('txtaDescription');
        $feedback['rate'] = JRequest::getVar('rate');

        $stringQuestion_array = explode(',', $questionChoose);

        foreach ($stringQuestion_array as $each_number) {
            $intQuestion_array[] = (int)$each_number;
        }

        $courseid = JRequest::getVar('courseid');
        $courseid = (int)$courseid;
        $username = JFactory::getUser()->username;

        $params = array();
        $params[1] = JRequest::getVar('firstrate');
        $params[2] = JRequest::getVar('secondrate');
        $params[3] = JRequest::getVar('thirdrate');
        $params[4] = JRequest::getVar('fourthrate');
        $params[5] = JRequest::getVar('fifthrate');
        $params = json_encode($params);

        for ($i = 0; $i < count($intQuestion_array); $i++) {
            if (JRequest::getVar('question' . $intQuestion_array[$i] . 'name') != '') {
                $question['question'+$i]['name'] = JRequest::getVar('question' . $intQuestion_array[$i] . 'name');
                $question['question'+$i]['type_id'] = JRequest::getVar('question' . $intQuestion_array[$i] . 'type');
                $question['question'+$i]['position'] = JRequest::getVar('question' . $intQuestion_array[$i] . 'position');
                $question['question'+$i]['content'] = JRequest::getVar('question' . $intQuestion_array[$i] . 'content');
                $question['question'+$i]['deleted'] = JRequest::getVar('question' . $intQuestion_array[$i] . 'deleted');
                $question['question'+$i]['params'] = $params;
                $question['question'+$i]['type_id'] = (int)$question['question'+$i]['type_id'];
                $question['question'+$i]['position'] = (int)$question['question'+$i]['position'];
            }
        }
        $data_question = json_encode($question);
        $data_feedback = json_encode($feedback);
        
        JoomdleHelperContent::call_method('create_and_add_question_for_feedback', $data_feedback, $data_question, $courseid, $username);

        $this->setRedirect(JURI::base().'course/'.$courseid.'.html');
    }


	function saveUpdatevisible()
	{
    //      Get Course ID (string->int)

           $sectionId = JRequest::getVar('addSectionId');
           $sectionId = (int) $sectionId;

           $courseid = JRequest::getVar('courseid');
           $courseid = (int) $courseid;
           // Many module id is string with ','
           $moduleIdNum = JRequest::getVar('moduleIdNum');
           if ($moduleIdNum != NULL) {
               //          string -> array string
               $stringModuleId_array = explode(',', $moduleIdNum);

               //          array string -> array int.
               $intModuleId_array = array();
               foreach ($stringModuleId_array as $each_number) {
                   $intModuleId_array[] = (int) $each_number;
               }

               $moduleIdStatus1 = array();
               for ($i = 0; $i < count($intModuleId_array); $i++) {
                   $moduleIdStatus1[$i]['module_id'] = $intModuleId_array[$i];
                   $moduleIdStatus1[$i]['status'] = 1;
               }

               JoomdleHelperContent::call_method('update_multi_activity_module', $moduleIdStatus1, $sectionId, $courseid);
           }

   //          Many module id is string with ','
//           $moduleIdNumNotCheck = JRequest::getVar('moduleIdNumNotCheck');
//           if ($moduleIdNumNotCheck != NULL) {
//               // string -> array string
//               $stringModuleNotCheckId_array = explode(',', $moduleIdNumNotCheck);
//               //array string -> array int.
//               $intModuleNotCheckId_array = array();
//               foreach ($stringModuleNotCheckId_array as $each_number1) {
//                   $intModuleNotCheckId_array[] = (int) $each_number1;
//               }
//               $moduleIdStatus0 = array();
//   //		    Checked status = 0;
//               for ($i = 0; $i < count($intModuleNotCheckId_array); $i++) {
//                   $moduleIdStatus0[$i]['module_id'] = $intModuleNotCheckId_array[$i];
//                   $moduleIdStatus0[$i]['status'] = 0;
//               }
//   //			update_multi_activity_module visible 1 ->0
//               JoomdleHelperContent::call_method('update_multi_activity_module', $moduleIdStatus0);
//           }
           $this->setRedirect('course/'.$courseid.'.html'); //XXX poenr un get current uri
	}

        function uploadfile() {
            header('Content-type: application/json; charset=UTF-8');
            $app = JFactory::getApplication();
            $user = JFactory::getUser();
            $username = $user->username;
            $config = new JConfig();
            
            if(empty($_FILES)) {
                $response['status'] = false;
                $response['message'] = 'File can not empty.';
                $response['filename'] = '';
                echo json_encode($response); exit;
            }
            $ext = pathinfo($_FILES['file_data']['name'], PATHINFO_EXTENSION);
            if(!isset($_POST['action'])) {
                $response['status'] = false;
                $response['message'] = 'Action can not empty.';
                $response['filename'] = '';
                echo json_encode($response); exit;
            }
            if($_POST['action'] == 'create_course') {
                $ext_arr = array('jpg', 'jepg', 'png', 'JPG', 'PNG');
                if(!in_array($ext, $ext_arr)) {
                    $response['status'] = false;
                    $response['message'] = 'Invalid image. Please choose image jpg or png';
                    $response['filename'] = '';
                    echo json_encode($response); exit;
                }
                $tmp_file = dirname($app->getCfg('dataroot')).'/temp/'.'tmp_pic';
                file_put_contents ($tmp_file, file_get_contents($_FILES['file_data']['tmp_name']));
                $response['status'] = true;
                $response['message'] = 'success';
                $response['filename'] = $_FILES['file_data']['name'];
                echo json_encode($response); exit;
            }
            elseif($_POST['action'] == 'create_scorm') {
                if($ext != 'zip') {
                    $response['status'] = false;
                    $response['message'] = 'Invalid SCORM file, Please select package zip file.';
                    $response['filename'] = '';
                    echo json_encode($response); exit;
                }
                $filename = 'scormtmpfile'.(microtime(true)*10000);
                $tmp_file = dirname($app->getCfg('dataroot')).'/temp/'.$filename;
                file_put_contents ($tmp_file, file_get_contents($_FILES['file_data']['tmp_name']));
                $response['status'] = true;
                $response['message'] = 'success';
                $response['filename'] = $_FILES['file_data']['name'].'_'.$filename;//$_FILES['file_data']['name'];
                echo json_encode($response); exit;
            }
            elseif($_POST['action'] == 'create_assignment') {
//                print_r($_FILES['file_data']['name']);die;
                $ext_arr = array('xls', 'xlsx', 'doc', 'docx', 'pdf', 'txt');
                if(!in_array($ext, $ext_arr)) {
                     $response['status'] = false;
                     $response['message'] = 'Invalid assignment file. Please select document format file: doc, docx,xls, xlsx or pdf';
                     $response['filename'] = '';
                     echo json_encode($response); exit;
                }
                $tmp_assign = dirname($app->getCfg('dataroot')).'/temp/'.$_FILES['file_data']['name'];
                file_put_contents($tmp_assign, file_get_contents($_FILES['file_data']['tmp_name']));
                $response['status'] = true;
                $response['message'] = 'success';
                $response['filename'] = $_FILES['file_data']['name'];
                echo json_encode($response); exit;
            } elseif($_POST['action'] == 'create_content') {
                $position = $_POST['position'];
                $courseid = $_POST['courseid'];
                if ($_FILES['file_data']['size'] >= 88*1024*1024) {
                    $response['status'] = false;
                    $response['message'] = 'File size is too large (< 88MB)';
                    $response['filename'] = '';
                    echo json_encode($response); exit;
                }
                // create folder
                if (!is_dir(dirname($app->getCfg('dataroot')) . '/temp/content')) {
                        mkdir(dirname($app->getCfg('dataroot')) . '/temp/content', 0755);
                }
                if (!is_dir(dirname($app->getCfg('dataroot')) . '/temp/content/'.$courseid)) {
                        mkdir(dirname($app->getCfg('dataroot')) . '/temp/content/'.$courseid, 0755);
                }
                if (!is_dir(dirname($app->getCfg('dataroot')) . '/temp/content/'.$courseid.'/'.$position)) {
                        mkdir(dirname($app->getCfg('dataroot')) . '/temp/content/'.$courseid.'/'.$position, 0755);
                }
                $tmp_content = dirname($app->getCfg('dataroot')).'/temp/content/'.$courseid.'/'.$position.'/'.$_FILES['file_data']['name'];
//                print_r($tmp_content); die;
                file_put_contents($tmp_content, file_get_contents($_FILES['file_data']['tmp_name']));
                $response['status'] = true;
                $response['message'] = 'success';
                $response['filename'] = $_FILES['file_data']['name'];
                $response['filepath'] = $tmp_content;
                echo json_encode($response); exit;
            }
        }

        function updateActivity()
    {

        $courseid = JRequest::getVar('courseid');
        $courseid = (int)$courseid; // Get Course ID (string->int)
        $username = JRequest::getVar('username'); // Get Username
        $string_arr_section_id = JRequest::getVar('arr_section_id');
        $arr_section_id = array_map('intval', explode(',', $string_arr_section_id));

        $all_section_id_string = JRequest::getVar('all_section_id'); // all id section of course
        $all_section_id = array_map('intval', explode(',', $all_section_id_string));

        $stringModuleIdNumNotCheck = JRequest::getVar('moduleIdNumHidden'); //10190,10191

//        $string_arr_section_id_delete = JRequest::getVar('sectionIdNumDelete'); //10190,10191
//        $arr_section_id_delete = array_map('intval', explode(',', $string_arr_section_id_delete));

        $stringSectionIdDelete = JRequest::getVar('sectionIdNumDelete'); //all id section delete

        $data = array();
        $tam = count($arr_section_id);
        for ($i = 0; $i < count($arr_section_id); $i++) {
            $string_arr_module_id_in_another_section = JRequest::getVar($arr_section_id[$i]);
            if ($string_arr_module_id_in_another_section != Null) {
                $arr_module_id_in_another_section = array_map('intval', explode(',', $string_arr_module_id_in_another_section));
//                var_dump($string_arr_module_id_in_another_section);
                $data[$i]['sectionname'] = JRequest::getVar('titleSection' . $arr_section_id[$i]);
                $data[$i]['sectionnum'] = $i;
                $data[$i]['action'] = "sort";
                $data[$i]['sectionid'] = $arr_section_id[$i];
                $data[$i]['mods'] = [];
                for ($t = 0; $t < count($arr_module_id_in_another_section); $t++) {
                    $data[$i]['mods'][$t]['moduleid'] = $arr_module_id_in_another_section[$t];
                    $data[$i]['mods'][$t]['moduletitle'] = JRequest::getVar('titleModule' . $arr_module_id_in_another_section[$t]);
                    $data[$i]['mods'][$t]['position'] = $t;
                    $data[$i]['mods'][$t]['action'] = 'sort';
                }
            } else {
                $data[$i]['sectionname'] = JRequest::getVar('titleSection' . $arr_section_id[$i]);
                $data[$i]['sectionnum'] = $i;
                $data[$i]['action'] = "sort";
                $data[$i]['sectionid'] = $arr_section_id[$i];
                $data[$i]['mods'] = [];
            }
        }

        if ($stringSectionIdDelete != NULL) {
            $sectionIdDelete = array_map('intval', explode(',', $stringSectionIdDelete));
            for ($i = 0; $i < count($sectionIdDelete); $i++) {
                $new = $tam + $i;
                $data[$new]['sectionname'] = "";
                $data[$new]['sectionnum'] = 20;
                $data[$new]['action'] = "delete";
                $data[$new]['sectionid'] = $sectionIdDelete[$i];
                $data[$new]['mods'] = [];
            }
        }
        if ($stringModuleIdNumNotCheck != NULL) {
            $arrModuleId = array_map('intval', explode(',', $stringModuleIdNumNotCheck));
            for ($i = 0; $i < count($arrModuleId); $i++) {
                $a['moduleid'] = $arrModuleId[$i];
                $a['moduletitle'] = "a";
                $a['position'] = 100;
                $a['action'] = 'delete';
                array_push($data[0]['mods'], $a);
            }
        }
        $a = json_encode($data);
        JoomdleHelperContent::call_method('multiple_sort_module_in_section_course', $username, $courseid, $a);
            $this->setRedirect('course/'.$courseid.'.html'); //XXX poenr un get current uri

    }


        function uploadCertificate() {
            header('Content-type: application/json; charset=UTF-8');
            $app = JFactory::getApplication();
            $user = JFactory::getUser();
            $config = new JConfig();

            $username = $user->username;
            if(empty($_POST['course_id'])) {
                $response['status'] = false;
                $response['message'] = 'Course id can not empty.';
                $response['filename'] = '';
                echo json_encode($response); exit;
            }
            $courseid = $_POST['course_id'];

            $course_info = JoomdleHelperContent::getCourseInfo($courseid, $user->username);

            $ext = pathinfo($_FILES['template']['name'], PATHINFO_EXTENSION);
            // upload certificate
            if(isset($_POST['action']) && $_POST['action'] == 'create') {
                if(empty($_FILES)) {
                    $response['status'] = false;
                    $response['message'] = 'File can not empty.';
                    $response['filename'] = '';
                    echo json_encode($response); exit;
                }
                $ext_arr = array('jpg', 'jepg', 'png', 'JPG', 'PNG');
                if(!in_array($ext, $ext_arr)) {
                    $response['status'] = false;
                    $response['message'] = 'Invalid image. Please choose image jpg or png';
                    $response['filename'] = '';
                    echo json_encode($response); exit;
                }
                // setup and get default certificate
                if (!is_dir($config->dataroot)) {
                    mkdir($config->dataroot, 0775);
                }
                if (!is_dir($config->dataroot . '/certificatetemp')) {
                    mkdir($config->dataroot . '/certificatetemp', 0775);
                }
                if (!is_dir($config->dataroot . '/certificatetemp/pix')) {
                    mkdir($config->dataroot . '/certificatetemp/pix', 0775);
                }
                if (!is_dir($config->dataroot . '/certificatetemp/pix/borders')) {
                    mkdir($config->dataroot . '/certificatetemp/pix/borders', 0775);
                }
                if (!is_dir($config->dataroot . '/certificatetemp/pix/borders/' . $course_info['remoteid'])) {
                    mkdir($config->dataroot . '/certificatetemp/pix/borders/' . $course_info['remoteid'], 0775);
                    if (!file_exists($config->dataroot .'/certificatetemp/pix/borders/'.$course_info['remoteid'].'/parenthesis_certificate_default.png')) { 
                    copy($config->dirmoodle . '/mod/certificate/pix/borders/parenthesis_certificate_default.png', $config->dataroot . '/certificatetemp/pix/borders/' . $course_info['remoteid'] . '/parenthesis_certificate_default.png');
                    }
    //                                        chmod($config->sitedataroot . '/moodle/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.png',0700);
                }
                $dir = $config->dataroot . '/certificatetemp/pix/borders/' . $course_info['remoteid'];
                $files_folder = scandir($dir);
                $count = count($files_folder) - 2;
                if($count == 5) {
                    $response['status'] = false;
                    $response['message'] = 'You have reached the maximum of 5 customized certificate. To add a new one, you need to delete an existing one.';
                    $response['filename'] = '';
                    echo json_encode($response); die;
                }

                if(isset($_FILES['template']['type'])){
                    $path = $_FILES['template']['tmp_name'];
                    $temp = explode(".", $_FILES["template"]["name"]);

                    $file_name = 'PXtemplate'.$course_info['remoteid'].substr(time(), -3).'.'.end($temp);
                    // create folder if not exits
                    if (!is_dir($config->dataroot . '/certificatetemp/pix/borders/' . $course_info['remoteid'])) {
                        mkdir($config->dataroot . '/certificatetemp/pix/borders/' . $course_info['remoteid'], 0755);
                    }
                    $newPath     = $config->dataroot.'/certificatetemp/pix/borders/'.$course_info['remoteid'].'/'.$file_name;
                    file_put_contents($newPath, file_get_contents($_FILES['template']['tmp_name']));
//                    move_uploaded_file($path, $newPath);
                    $response['status'] = true;
                    $response['message'] = 'success.';
                    $response['filename'] = $file_name;
                    $response['filepath'] = $config->wwwrootfile . '/certificatetemp/pix/borders/' . $course_info['remoteid'] . '/' . $file_name;
                    echo json_encode($response);die;
                }
         }
         // list certificate
         if(isset($_POST['action']) && $_POST['action'] == 'list') {
            // setup and get default certificate 
            if (!is_dir($config->dataroot . '/certificatetemp')) {
                mkdir($config->dataroot . '/certificatetemp', 0777);
            }
            if (!is_dir($config->dataroot . '/certificatetemp/pix')) {
                mkdir($config->dataroot . '/certificatetemp/pix', 0777);
            }
            if (!is_dir($config->dataroot . '/certificatetemp/pix/borders')) {
                mkdir($config->dataroot . '/certificatetemp/pix/borders', 0777);
            }
            if (!is_dir($config->dataroot . '/certificatetemp/pix/borders/' . $courseid)) {
                mkdir($config->dataroot . '/certificatetemp/pix/borders/' . $courseid, 0777);
//                                        chmod($config->sitedataroot . '/moodle/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.png',0700);
            }
            if (!file_exists($config->dataroot .'/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.png') && !file_exists($config->dataroot .'/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.jpg')) { 
                copy($config->dirmoodle.'/mod/certificate/pix/borders/parenthesis_certificate_default.png', $config->dataroot .'/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.png');
            } 
            
             $dir = $config->dataroot . '/certificatetemp/pix/borders/' . $course_info['remoteid'];
             $files_folder = scandir($dir);
             $supportedtypes = array('.jpg', '.png', '.jpeg');
             $count_type = count($supportedtypes);
             $count_file = count($files_folder);
             $templates = array();
             for ($i = 0; $i < $count_file; $i++) {
                for ($j = 0; $j < $count_type; $j++) {
                    if (strlen(strstr($files_folder[$i], $supportedtypes[$j])) > 0) {
                        array_push($templates, $files_folder[$i]);
                    }
                }
            }
            $template_default = JoomdleHelperContent::call_method ( 'get_certificate_template_default', (int) $course_info['remoteid']);

            $template_cer = array();
            foreach ($templates as $temp) {
                $certificate = array();
                $certificate['filename'] = $temp;
                $certificate['filepath'] = $config->wwwrootfile . '/certificatetemp/pix/borders/' . $course_info['remoteid'] . '/' .$temp;
                $certificate['temp_default'] = false;
                $certificate['certificate_id'] = $template_default['template']['cer_id'];
                if($temp == $template_default['template']['cer_name']) {
                    $certificate['temp_default'] = true;
                }

                // check exist logo
                $exist_logo = false;
                $name = $temp;
                $pos = strrpos($name, ".");
                $a = substr($name ,$pos, 10000);
                $name_replace = str_replace($a, '', $name);
                $dir    = $config->dataroot.'/certificatetemp/pix/backups/'.$course_info['remoteid'];
                $files_folder = scandir($dir);
                $count_file = count($files_folder);
                for ($i=0; $i < $count_file; $i++) {
                        if (strlen(strstr($files_folder[$i],$name_replace)) > 0 ) {
                                $exist_logo = true;
                                $img_old = $config->wwwrootfile.'/certificatetemp/pix/backups/'.$course_info['remoteid'].'/'.$files_folder[$i];
//                                $name_old = $files_folder[$i];
                        }
                }
                $certificate['exist_logo'] = $exist_logo;
//                $certificate['logo'] = $img_old;

                $template_cer[] = $certificate;
            }
            $response['status'] = true;
            $response['message'] = 'success.';
            $response['certificate'] = $template_cer;
            echo json_encode($response);die;
         }
         // add logo
         if(isset($_POST['action']) && $_POST['action'] == 'addlogo') {
            if(empty($_FILES)) {
                $response['status'] = false;
                $response['message'] = 'File can not empty.';
                $response['filename'] = '';
                echo json_encode($response);
                exit;
            }
            if(empty($_POST['template'])) {
                $response['status'] = false;
                $response['message'] = 'template can not empty.';
                $response['filename'] = '';
                echo json_encode($response);
                exit;
            }
            $ext = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
            if($ext != 'png') {
                $response['status'] = false;
                $response['message'] = 'The file does not match images. Tip: Upload your own logo in PNG format.';
                $response['filename'] = '';
                echo json_encode($response);
                exit;
            }
            $name = $_POST['template'];
            $pos = strrpos($name, ".");
            $a = substr($name ,$pos, 10000);
            $name_replace = str_replace($a, '', $name);

            // check exist logo
            $exist_logo = false;
            $dir    = $config->dataroot.'/certificatetemp/pix/backups/'.$course_info['remoteid'];
            $files_folder = scandir($dir);
            $count_file = count($files_folder);
            for ($i=0; $i < $count_file; $i++) {
                    if (strlen(strstr($files_folder[$i],$name_replace)) > 0 ) {
                            $exist_logo = true;
                    }
            }
            if ($exist_logo) {
                $response['status'] = false;
                $response['message'] = 'The template has logo already.';
                $response['filename'] = '';
                echo json_encode($response);
                exit;
            }

            if(isset($_FILES['logo']['type'])){
                $logo = $_FILES['logo']['tmp_name'];
                require_once(JPATH_SITE.'/components/com_joomdle/views/certificatecreate/tmpl/resize.php');
                if (!is_dir($config->dataroot . '/certificatetemp/pix/seals')) {
                    mkdir($config->dataroot . '/certificatetemp/pix/seals', 0755);
                }
                $image = new SimpleImage();
                    $image->load($logo);
                    $t = 250/($image->getWidth());
                    $heightlogo = $t*($image->getHeight());
                    $image->resize(250,$heightlogo,$config->dataroot . '/certificatetemp/pix/seals/logo.jpg');
//                    $image->save($config->dirmoodle.'/mod/certificate/pix/seals/logo.jpg');
                    $image->save($config->dataroot . '/certificatetemp/pix/seals/logo.jpg');
                    if (!is_dir($config->dataroot . '/certificatetemp/pix/backups')) {
                        mkdir($config->dataroot . '/certificatetemp/pix/backups', 0755);
                    }
                mkdir($config->dataroot.'/certificatetemp/pix/backups/'.$course_info['remoteid'], 0755);
                copy($config->dataroot.'/certificatetemp/pix/borders/'.$course_info['remoteid'].'/'.$name, $config->dataroot.'/certificatetemp/pix/backups/'.$course_info['remoteid'].'/'.$name_replace.'.jpg');

//                $this->merge($config->dataroot.'/certificatetemp/pix/borders/'.$course_info['remoteid'].'/'.$name, $config->dirmoodle.'/mod/certificate/pix/seals/logo.jpg', $config->dataroot.'/certificatetemp/pix/borders/'.$course_info['remoteid'].'/'.$name_replace.'.jpg');
                $this->merge($config->dataroot.'/certificatetemp/pix/borders/'.$course_info['remoteid'].'/'.$name, $config->dataroot.'/certificatetemp/pix/seals/logo.jpg', $config->dataroot.'/certificatetemp/pix/borders/'.$course_info['remoteid'].'/'.$name_replace.'.jpg');
                $point = explode('.', $name);
                if(end($point) != 'jpg'){ 
                    chmod($config->dataroot.'/certificatetemp/pix/borders/'.$courseid.'/'.$name, 0777);
                    unlink($config->dataroot.'/certificatetemp/pix/borders/'.$courseid.'/'.$name);
                }
                // unlink($config->dirmoodle.'/mod/certificate/pix/seals/logo.jpg');
               $response['status'] = true;
               $response['message'] = 'success.';
               $response['filename'] = $name_replace.'.jpg';
               $response['filepath'] = $config->wwwrootfile . '/certificatetemp/pix/borders/' . $course_info['remoteid'] . '/' . $name_replace.'.jpg';
               echo json_encode($response);die;
            }
         }
         // remove logo
         if(isset($_POST['action']) && $_POST['action'] == 'removelogo') {
               $name_file_old = $_POST['template'];
               $copy_file_name = explode('.',$name_file_old);
               $file_old = $config->dataroot.'/certificatetemp/pix/backups/'.$course_info['remoteid'].'/'.$name_file_old;
               array_map("unlink", glob($config->dataroot.'/certificatetemp/pix/borders/'.$course_info['remoteid'].'/'.$copy_file_name[0].'*'));
               copy($file_old, $config->dataroot.'/certificatetemp/pix/borders/'.$course_info['remoteid'].'/'.$name_file_old);
               unlink($config->dataroot.'/certificatetemp/pix/backups/'.$course_info['remoteid'].'/'.$name_file_old);
               $response['status'] = true;
               $response['message'] = 'success.';
               echo json_encode($response);die;
         }
         // delete template
         if(isset($_POST['action']) && $_POST['action'] == 'delete') {
                if(empty($_POST['template'])) {
                    $response['status'] = false;
                    $response['message'] = 'Template name can not be empty.';
                    $response['filename'] = '';
                    echo json_encode($response);
                    exit;
                }
                $name = $_POST['template'];
                $pos = strrpos($name, ".");
                $a = substr($name, $pos, 10000);
                $name_replace = str_replace($a, '', $name);
                $dir = $config->dataroot . '/certificatetemp/pix/borders/' . $course_info['remoteid'] . '/';
                $dirHandle = opendir($dir);
                while ($file = readdir($dirHandle)) {
                    if ($file == $name) {
                        unlink($dir . '/' . $name);
                    }
                }
                $files = scandir($config->dataroot . '/certificatetemp/pix/backups/' . $course_info['remoteid'] . '/');
                $count_file = count($files);
                for ($i = 0; $i < $count_file; $i++) {
                    if (strlen(strstr($files[$i], $name_replace)) > 0) {
                        unlink($config->dataroot . '/certificatetemp/pix/backups/' . $course_info['remoteid'] . '/' . $files[$i]);
                    }
                }
                $response['status'] = true;
                $response['message'] = 'success.';
                echo json_encode($response);die;
         }
         // set default template
         if(isset($_POST['action']) && $_POST['action'] == 'set_default') {
             if(empty($_POST['cer_id'])) {
                 $response['status'] = false;
                 $response['message'] = 'Certificate id can not be empty.';
                 $response['filename'] = '';
                 echo json_encode($response);
                 exit;
             }
             if(empty($_POST['temp_default'])) {
                 $response['status'] = false;
                 $response['message'] = 'Certificate template can not be empty.';
                 $response['filename'] = '';
                 echo json_encode($response);
                 exit;
             }
             if (!empty($_POST['username'])) {
                 $username = $_POST['username'];
             }
             $tmp_default_id = $_POST['cer_id'];
             $tmp_name = $_POST['temp_default'];
             $setDefault = JoomdleHelperContent::call_method('set_default_certificate_template', (int)$tmp_default_id, (int)$course_info['remoteid'], $tmp_name, $username);
             if ($setDefault['status']) {
                $response['status'] = true;
                $response['message'] = 'success.';
                echo json_encode($response);die;
             } else {
                $response['status'] = false;
                $response['message'] = $setDefault['message'];
                echo json_encode($response);die;
             }
         }
        }

        function merge($filename_x, $filename_y, $filename_result) {
            // Get dimensions for specified images

            list($width_x, $height_x) = getimagesize($filename_x);
            list($width_y, $height_y) = getimagesize($filename_y);

            // Create new image with desired dimensions

            $image = imagecreatetruecolor($width_x , $height_x);

            $whiteBackground = imagecolorallocate($image, 255, 255, 255); 
            imagefill($image, 50, 50, $whiteBackground);

            // Load images and then copy to destination image
           if(exif_imagetype($filename_x)==2){
                   $image_x = imagecreatefromjpeg($filename_x);
                   if(exif_imagetype($filename_y)==2){
                           $image_y = imagecreatefromjpeg($filename_y);
                   }else if(exif_imagetype($filename_y)==3){
                           $image_y = imagecreatefrompng($filename_y);
                   }
            } else if (exif_imagetype($filename_x)==3){
                   $image_x = imagecreatefrompng($filename_x);
                   if(exif_imagetype($filename_y)==2){
                           $image_y = imagecreatefromjpeg($filename_y);
                   }else if(exif_imagetype($filename_y)==3){
                           $image_y = imagecreatefrompng($filename_y);
                   }
           }



            imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);
            imagecopy($image, $image_y, $width_x/3.4, $height_x/5.4, 0, 0, $width_y, $height_y);

            // Save the resulting image to disk (as JPEG)

            imagejpeg($image, $filename_result);

            // Clean up

            imagedestroy($image);
            imagedestroy($image_x);
            imagedestroy($image_y);

       }
       
       function getAdminEmailFrom(){
		$lpId = 5;
		$db = JFactory::getDbo();
		$db->setQuery("SELECT AdminEmailFrom FROM `#__rsform_forms` WHERE `FormId` = '".(int) $lpId."'");
			$AdminEmail = $db->loadObjectList();
			$db->execute();
			$AdminEmailFrom = $db->escape($AdminEmail[0]->AdminEmailFrom);
			return $AdminEmailFrom;
	}
       
       function emailEnrolSuccess($recipient, $courseid, $coursename, $rolename){
                $user = JFactory::getUser($recipient);
                
		//TODO: need to pickup the config admin email
                $mainframe = JFactory::getApplication();
                $from = $mainframe->getCfg('mailfrom');
                $fromname = $mainframe->getCfg('fromname');
//		$from = $this->getAdminEmailFrom();
//		$fromname = 'Parenthexis';
                $url = JURI::base() . 'index.php?option=com_joomdle&view=course&course_id=' . $courseid;
		$subject = JText::sprintf('COM_JOOMDLE_EMAIL_ENROL_USER_SUBJECT', $rolename, $coursename);
                $body = JText::sprintf('COM_JOOMDLE_EMAIL_ENROL_USER_BODY', $user->username, $rolename, $coursename, $url);

		$this->sendMail($from, $fromname, $user->email, $subject, $body, 'html', null, null, '', '');
	}

        function emailSendForApprovalSuccess ($userlp_id, $userfrom_id, $courseid, $coursename) {
                $userlp = JFactory::getUser($userlp_id);
                $userfrom = JFactory::getUser($userfrom_id);
                
		//TODO: need to pickup the config admin email
                $mainframe = JFactory::getApplication();
                $from = $mainframe->getCfg('mailfrom');
                $fromname = $mainframe->getCfg('fromname');
//		$from = $this->getAdminEmailFrom();
//		$fromname = 'Parenthexis';
                $url = JURI::base() . 'index.php?option=com_joomdle&view=course&course_id=' . $courseid;
		$subject = JText::sprintf('COM_JOOMDLE_EMAIL_SEND_FOR_APPROVAL_SUBJECT');
                $body = JText::sprintf('COM_JOOMDLE_EMAIL_SEND_FOR_APPROVAL_BODY', $userlp->name, $userfrom->name, $coursename, $url);

		$this->sendMail($from, $fromname, $userlp->email, $subject, $body, 'html', null, null, '', '');
}
        
        function emailApprovalSuccess ($userto_id, $courseid, $coursename, $action) {
                $userto = JFactory::getUser($userto_id);
                
		//TODO: need to pickup the config admin email
                $mainframe = JFactory::getApplication();
                $from = $mainframe->getCfg('mailfrom');
                $fromname = $mainframe->getCfg('fromname');
//		$from = $this->getAdminEmailFrom();
//		$fromname = 'Parenthexis';
                $url = JURI::base() . 'index.php?option=com_joomdle&view=course&course_id=' . $courseid;
                if ($action == 'approve') {
                    $subject = JText::sprintf('COM_JOOMDLE_EMAIL_COURSE_APPROVED_SUBJECT', $coursename);
                    $body = JText::sprintf('COM_JOOMDLE_EMAIL_COURSE_APPROVED_BODY', $userto->name, $coursename);
                } else if ($action == 'unapprove') {
                    $subject = JText::sprintf('COM_JOOMDLE_EMAIL_COURSE_UNAPPROVED_SUBJECT', $coursename);
                    $body = JText::sprintf('COM_JOOMDLE_EMAIL_COURSE_UNAPPROVED_BODY', $userto->name, $coursename);
                }

		$this->sendMail($from, $fromname, $userto->email, $subject, $body, 'html', null, null, '', '');
        }
        
        function emailSubscribeSuccess ($userto_id, $courseid, $coursename, $rolename, $userfrom_id) {
                $userto = JFactory::getUser($userto_id);
                $userfrom = JFactory::getUser($userfrom_id);
                
		//TODO: need to pickup the config admin email
                $mainframe = JFactory::getApplication();
                $from = $mainframe->getCfg('mailfrom');
                $fromname = $mainframe->getCfg('fromname');
//              $from = $this->getAdminEmailFrom();
//		$fromname = 'Parenthexis';
                $url = JURI::base() . 'index.php?option=com_joomdle&view=course&course_id=' . $courseid;
                if ($rolename == 'course_lp') {
                    $subject = JText::sprintf('COM_JOOMDLE_EMAIL_COURSE_SUBSCRIBED_LP_SUBJECT', $coursename);
                    $body = JText::sprintf('COM_JOOMDLE_EMAIL_COURSE_SUBSCRIBED_LP_BODY', $userto->name, $coursename, date('d/m/Y'));
                } else if ($rolename == 'course_non') {
                    $subject = JText::sprintf('COM_JOOMDLE_EMAIL_COURSE_SUBSCRIBED_CC_SUBJECT');
                    $body = JText::sprintf('COM_JOOMDLE_EMAIL_COURSE_SUBSCRIBED_CC_BODY', $userto->name, $userfrom->name, $coursename, $url);
                }

		$this->sendMail($from, $fromname, $userto->email, $subject, $body, 'html', null, null, '', '');
        }

        function sendMail($from, $fromname, $recipient, $subject, $body, $mode=0, $cc=null, $bcc=null, $attachment=null, $replyto=null, $replytoname=null)
	{
		// Get a JMail instance
		$mail 		= JFactory::getMailer();
		$config 	= JFactory::getConfig();
		$mailfrom	= $config->get('mailfrom');
		
		$mail->ClearReplyTos();
		$mail->setSender(array($from, $fromname));
		
		$mail->setSubject($subject);
		$mail->setBody($body);

		// Are we sending the email as HTML?
		if ($mode)
			$mail->IsHTML(true);
		
		// Some cleanup
		if (is_array($recipient)) {
			foreach ($recipient as $i => $r) {
				if (empty($r)) {
					unset($recipient[$i]);
                                }
			}
		}

		$mail->addRecipient($recipient);
		$mail->addCC($cc);
		$mail->addBCC($bcc);
		$mail->addAttachment($attachment);

		// Take care of reply email addresses
		if (is_array($replyto)) {
			$mail->ClearReplyTos();
			$numReplyTo = count($replyto);
			for ($i = 0; $i < $numReplyTo; $i++)
			{
				$mail->addReplyTo(array($replyto[$i], $replytoname[$i]));
			}
		}
		else if (!empty($replyto)) {
			$mail->ClearReplyTos();
			$mail->addReplyTo(array($replyto, $replytoname));
		}

		return $mail->Send();
        }

}
?>
