<?php
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/mappings.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/groups.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/users.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/shop.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/points.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/mailinglist.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/joomlagroups.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/forum.php');
require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/activities.php');

require_once(JPATH_SITE.'/components/com_joomdle/controller.php');


    function getUserInfo($method, $params)
    {
		$username = $params[0];
		if (array_key_exists (1, $params))
			$app = $params[1];
		else $app = '';
        $user_info = JoomdleHelperMappings::get_user_info ($params[0], $app);
        return $user_info;
    }

	function test ()
	{
		return "It works";
	}

    /* Web service used to log in from Moodle */
    function login ($method, $params)
    {
		$username = $params[0];
		$password = $params[1];

        $mainframe = JFactory::getApplication('site');

        $options = array ( 'skip_joomdlehooks' => '1', 'silent' => 1);
        $credentials = array ( 'username' => $username, 'password' => $password);
        if ($mainframe->login( $credentials, $options ))
            return true;
        return false;
	}

	function getDefaultItemid ()
    {
        $comp_params = JComponentHelper::getParams( 'com_joomdle' );
        $default_itemid = $comp_params->get( 'default_itemid' );
        return $default_itemid;
    }


  function confirmJoomlaSession($method, $params)
    {
		$username = $params[0];
		$token = $params[1];

        $db = JFactory::getDBO();
        $query = 'SELECT session_id' .
                ' FROM #__session' .
                " WHERE username = ". $db->Quote($username). " and  md5(session_id) = ". $db->Quote($token);
        $db->setQuery( $query );
        $session = $db->loadResult();


		if ($session)
			return true;
		else
			return false;
    }

	function logout($method, $params)
    {
		$username = $params[0];

        $mainframe = JFactory::getApplication('site');

        $id = JUserHelper::getUserId($username);

        $error = $mainframe->logout($id, array ( 'clientid' => 0, 'skip_joomdlehooks' => 1));

        $r = JApplication::getHash('JLOGIN_REMEMBER');
        return $r;
    }

	function createUser ($method, $params)
    {
		$user_info = $params[0];
        return JoomdleHelperUsers::create_joomla_user ($user_info);
    }

    function activateJoomlaUser ($method, $params)
    {
		$username = $params[0];

        $username = utf8_decode ($username);

        return JoomdleHelperUsers::activate_joomla_user ($username);
    }

    function updateUser ($method, $params)
    {
		$user_info = $params[0];
        return JoomdleHelperMappings::save_user_info ($user_info, false);
    }

    function changePassword ($method, $params)
    {
		$username = $params[0];
		$password = $params[1];

        $username = utf8_decode ($username);

		$user_id = JUserHelper::getUserId($username);
        $user = JFactory::getUser($user_id);


        $salt           = JUserHelper::genRandomPassword(32);
		$crypt          = JUserHelper::getCryptedPassword($password, $salt);
        $password_crypt       = $crypt.':'.$salt;

        $user->password = $password_crypt;
        @$user->save();

        return true;
    }

	function deleteUser ($method, $params)
    {
		$username = $params[0];
        $username = utf8_decode ($username);

        $user_id = JUserHelper::getUserId($username);
        $user = JFactory::getUser($user_id);
        $user->delete();
    }


	function addActivityCourse ($method, $params)
    {
		$id = $params[0];
		$name = $params[1];
		$desc = $params[2];
		$cat_id = $params[3];
		$cat_name = $params[4];

		return JoomdleHelperActivities::add_activity_course ($id, $name, $desc, $cat_id, $cat_name);
    }

	function addActivityCourseEnrolment ($method, $params)
    {
		$username = $params[0];
		$course_id = $params[1];
		$course_name = $params[2];
		$cat_id = $params[3];
		$cat_name = $params[4];

		return JoomdleHelperActivities::add_activity_course_enrolment ($username, $course_id, $course_name, $cat_id, $cat_name);
    }


/*
	function getJSGroupId ($method, $params)
    {
		$name = $params[0];
        return JoomdleHelperGroups::get_js_group_by_name ($name);
    }

    function getJSGroupImageLink ($method, $params)
    {
		$name = $params[0];
        return JoomdleHelperGroups::get_js_group_image_link ($name);
    }

    function addJSGroup ($method, $params)
    {
		$name = $params[0];
		$description = $params[1];
		$course_id = $params[2];
		$website = $params[3];

   //     $name = utf8_decode ($name);
   //     $description = utf8_decode ($description);

        return JoomdleHelperGroups::addJSGroup ($name, $description, $course_id, $website);
    }
*/

    function addSocialGroup ($method, $params)
    {
		$name = $params[0];
		$description = $params[1];
		$course_id = $params[2];
        $username = $params[3];
        $pic_url = $params[4];
        $pic_name = $params[5];
        
        return JoomdleHelperSocialgroups::add_group ($name, $description, $course_id, $username, '', $pic_url, $pic_name);
    }

    function copySocialGroup ($method, $params)
    {
        $old_course_id = $params[0];
        $new_course_id = $params[1];
        
        return JoomdleHelperSocialgroups::copy_group($old_course_id, $new_course_id);
    }

    function updateSocialGroup ($method, $params)
    {
		$name = $params[0];
		$description = $params[1];
		$course_id = $params[2];

        return JoomdleHelperSocialgroups::update_group ($name, $description, $course_id);
    }

    function deleteSocialGroup ($method, $params)
    {
		$course_id = $params[0];

        return JoomdleHelperSocialgroups::delete_group ($course_id);
    }

	function addSocialGroupMember ($method, $params)
    {
		$username = $params[0];
		$permissions = $params[1];
		$course_id = $params[2];

        $username = utf8_decode ($username);

        return JoomdleHelperSocialGroups::add_group_member ($username, $permissions, $course_id);
    }

    function removeSocialGroupMember ($method, $params)
    {
		$username = $params[0];
		$course_id = $params[1];

//        $username = utf8_decode ($username);

        return JoomdleHelperSocialGroups::remove_group_member ($username, $course_id);
    }

/*
    function updateJSGroup ($method, $params)
    {
		$name = $params[0];
		$description = $params[1];
		$course_id = $params[2];
		$website = $params[3];

 //       $name = utf8_decode ($name);
 //       $description = utf8_decode ($description);

        return JoomdleHelperGroups::updateJSGroup ($name, $description, $course_id, $website);
    }

    function removeJSGroup ($method, $params)
    {
		$name = $params[0];
        return JoomdleHelperGroups::removeJSGroup ($name);
    }


   function addJSGroupMember ($method, $params)
    {
		$group_name = $params[0];
		$username = $params[1];
		$permissions = $params[2];
		$course_id = $params[3];

        $username = utf8_decode ($username);
        $group_name = utf8_decode ($group_name);

        return JoomdleHelperGroups::addJSGroupMember ($group_name, $username, $permissions, $course_id);
    }

    function removeJSGroupMember ($method, $params)
    {
		$group_name = $params[0];
		$username = $params[1];

//        $username = utf8_decode ($username);

        return JoomdleHelperGroups::removeJSGroupMember ($group_name, $username);
    }
*/

    function addPoints ($method, $params)
    {
		$action = $params[0];
		$username = $params[1];
		$courseid = $params[2];
		$course_name = $params[3];

        $username = utf8_decode ($username);
        $course_name = utf8_decode ($course_name);

        return JoomdleHelperPoints::addPoints ($action, $username, $courseid, $course_name);
    }

	function addActivityQuizAttempt ($method, $params)
    {
		$username = $params[0];
		$course_id = $params[1];
		$course_name = $params[2];
		$quiz_name = $params[3];

		return JoomdleHelperActivities::add_activity_quiz_attempt ($username, $course_id, $course_name, $quiz_name);
    }


    function addMailingSub ($action, $params)
    {
		$username = $params[0];
		$course_id = $params[1];
		$type = $params[2];

        $username = utf8_decode ($username);

		return JoomdleHelperMailinglist::add_list_member ($username, $course_id, $type);
    }

    function removeMailingSub ($action, $params)
    {
		$username = $params[0];
		$course_id = $params[1];
		$type = $params[2];

        $username = utf8_decode ($username);

		return JoomdleHelperMailinglist::remove_list_member ($username, $course_id, $type);
    }

    function addUserGroups ($action, $params)
    {
		$course_id = $params[0];
		$course_name = $params[1];

        $course_name = utf8_decode ($course_name);

		return JoomdleHelperJoomlagroups::add_course_groups ($course_id, $course_name);
    }

    function removeUserGroups ($action, $params)
    {
		$course_id = $params[0];

		return JoomdleHelperJoomlagroups::remove_course_groups ($course_id);
    }

    function addGroupMember ($action, $params)
    {
		$course_id = $params[0];
		$username = $params[1];
		$type = $params[2];

        $username = utf8_decode ($username);

		return JoomdleHelperJoomlagroups::add_group_member ($course_id, $username, $type);
    }

    function removeGroupMember ($action, $params)
    {
		$course_id = $params[0];
		$username = $params[1];
		$type = $params[2];

        $username = utf8_decode ($username);

		return JoomdleHelperJoomlagroups::remove_group_member ($course_id, $username, $type);
    }

    function addForum ($action, $params)
    {
		$course_id = $params[0];
		$forum_id = $params[1];
		$forum_name = $params[2];

        $forum_name = utf8_decode ($forum_name);

		return JoomdleHelperForum::add_forum ($course_id, $forum_id, $forum_name);
    }

    function removeForum ($action, $params)
    {
		$course_id = $params[0];
		$forum_id = $params[1];

		return JoomdleHelperForum::remove_forum ($course_id, $forum_id);
    }

    function addForumsModerator ($action, $params)
    {
		$course_id = $params[0];
		$username = $params[1];

        $username = utf8_decode ($username);

		return JoomdleHelperForum::add_forums_moderator ($course_id, $username);
    }

    function removeForumsModerator ($action, $params)
    {
		$course_id = $params[0];
		$username = $params[1];

        $username = utf8_decode ($username);

		return JoomdleHelperForum::remove_forums_moderator ($course_id, $username);
    }

    function removeCourseForums ($action, $params)
    {
		$course_id = $params[0];

		return JoomdleHelperForum::remove_course_forums ($course_id);
    }

    function getSellUrl ($action, $params)
    {
		$course_id = $params[0];

        return JoomdleHelperShop::get_sell_url ($course_id);
    }

	function addActivityCourseCompleted ($method, $params)
    {
		$username = $params[0];
		$course_id = $params[1];
		$course_name = $params[2];

		return JoomdleHelperActivities::add_activity_course_completed ($username, $course_id, $course_name);
    }
    
    // add function get group of course
    // added by Dungnv 24 Sept 2016
    // updated by Luyenvt 30 Oct 2017 (get activity count circle)
    function getGroupbyCourseID ($method, $params) {
        $courseid = $params[0];
        $username = $params[1];
        if (!isset($username) || $username == '') {
            $user = JFactory::getUser();
            $userid = $user->id;
        } else {
            $userid = JUserHelper::getUserId($username);   
        }
        JPluginHelper::importPlugin( 'joomdlesocialgroups' );
        $dispatcher = JDispatcher::getInstance();
        $course_group = $dispatcher->trigger('get_group_by_course_id', array ($courseid));
        $activity_count = $dispatcher->trigger('get_activity_count_circle', array ($course_group[0], $userid));
        $course_group[] = (isset($activity_count[0])) ? $activity_count[0] : 0;
        return $course_group;
    }
    
    function getBLNMoodleCategoryId ($method, $params) {
        $username = $params[0];
        if (!isset($username) || $username == '') {
            $user = JFactory::getUser();
            $userid = $user->id;
        } else {
            $userid = JUserHelper::getUserId($username);   
        }
        JPluginHelper::importPlugin( 'joomdlesocialgroups' );
        $dispatcher = JDispatcher::getInstance();
        $blnCircle = $dispatcher->trigger('getBLNMoodleCategoryId', array ($username));
        
        return $blnCircle;
    }
    
    function publishCourse($method,$params) {
        $courseid = $params[0];
        $categoryid = $params[1];
        $hikashopcatids = $params[2];
        $action = $params[3];
        
        $course = JoomdleHelperContent::getCourseInfo($courseid);
        if ($action == 'publish') {
        // Publish coure to hikashop category
        require_once(JPATH_SITE.'/components/com_hikashop/helpers/lpapi.php');
        $groupid = JoomdleHelperJoomlagroups::get_groupid_coursecategory($categoryid);
        LpApiController::publishcourse((int)$courseid, (string)$hikashopcatids, (int)$groupid);
        
            return JoomdleHelperShop::publish_courses ($course, $action);
        } else if ($action == 'unpublish') {
            return JoomdleHelperShop::publish_courses ($course, $action);
    }
    }
    function courseProductHikshop($method,$params) {

        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $course_id = $params[0];

        $wheres = (' WHERE product_code = ' . $course_id);
        $db->setQuery('SELECT `product_id`, `product_code` , `product_name`, `product_published` FROM ' . $db->quoteName('#__hikashop_product') . $wheres);
        $product_hikashop_id = $db->loadObject();//        $text = $params[0];
        return ($product_hikashop_id);
    }
    
    function shareCourseToCircle($method,$params) {
        $action = $params[0];
        $courseid = $params[1];
        $circleids = $params[2];
        
        require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
        $groups = CFactory::getModel('groups');
        
        if ($action && $action == 'enable') {
            $circleid = explode(',', $circleids);
            if (count($circleid) > 0) {
                foreach ($circleid as $gid) {  
                    $check = $groups->getShareGroups($courseid, $gid);
                    if (!$check) {
                        $groups->courseShareToGroup($courseid, $gid);
                    }
                }
            }
        } else if ($action && $action == 'unenable') {
            $groups->deleteCourseShareToGroup($courseid);
        }
        
        return true;
    }
    function searchCourseProductHikshop($method,$params) {

        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $text = $params[0];
        $sortby = $params[1];
        $limit = $params[2];
        $page = $params[3];
        $position = $params[4];
        if (isset($position) && $position != 0) {
            $position = $position;
        } else {
        if($page!==NULL) {
            $position = ($page - 1) * $limit;
        }
        }
        $text = utf8_decode($text);
        $sortby = utf8_decode($sortby);
        $str = preg_replace('/\s+/', '', $text);
        $text_search_have_space = '\'%' . $text . '%\'';
        $text_search_dont_have_space = '\'%' . $str . '%\'';
        $wheres2 = array();
        $wheres2[] = 'hp.product_name LIKE ' . $text_search_have_space;
        $wheres2[] = 'hp.product_description LIKE ' . $text_search_have_space;
        $wheres2[] = 'hp.learning_outcomes LIKE ' . $text_search_have_space;
        if($text_search_dont_have_space != $text_search_have_space) {
            $wheres2[] = 'hp.product_name LIKE ' . $text_search_dont_have_space;
            $wheres2[] = 'hp.product_description LIKE ' . $text_search_dont_have_space;
            $wheres2[] = 'hp.learning_outcomes LIKE ' . $text_search_dont_have_space;
        }
        $wheres = implode(' OR ', $wheres2);
        $db->setQuery('SELECT * FROM ' . $db->quoteName('#__hikashop_product') .' hp WHERE hp.product_published = 1 AND hp.product_code > 0 AND ( '. $wheres . ' ) ORDER BY ' . $db->Quote( $sortby ) . ' LIMIT '.$position  .','. $limit );
        $product_hikashop = $db->loadObjectList();//        $text = $params[0];
        return ($product_hikashop);

    }
    function countSearchCourseProductHikshop($method,$params) {

    $app = JFactory::getApplication();
    $db = JFactory::getDbo();
    $text = $params[0];

    $text = utf8_decode($text);
    $str = preg_replace('/\s+/', '', $text);
    $text_search_have_space = '\'%' . $text . '%\'';
    $text_search_dont_have_space = '\'%' . $str . '%\'';
    $wheres2 = array();
    $wheres2[] = 'hp.product_name LIKE ' . $text_search_have_space;
    $wheres2[] = 'hp.product_description LIKE ' . $text_search_have_space;
    $wheres2[] = 'hp.learning_outcomes LIKE ' . $text_search_have_space;
    if($text_search_dont_have_space != $text_search_have_space) {
        $wheres2[] = 'hp.product_name LIKE ' . $text_search_dont_have_space;
        $wheres2[] = 'hp.product_description LIKE ' . $text_search_dont_have_space;
        $wheres2[] = 'hp.learning_outcomes LIKE ' . $text_search_dont_have_space;
    }
    $wheres = implode(' OR ', $wheres2);
    $db->setQuery('SELECT count(product_id) FROM ' . $db->quoteName('#__hikashop_product') .' hp WHERE hp.product_published = 1 AND hp.product_code > 0 AND ( '. $wheres .' )' );
    $count_search_text = $db->loadObject();//        $text = $params[0];
    return ($count_search_text);
}
    function courseNotifications($method,$params) {
        $courseid = $params[0];
        $coursename = $params[1];
        $usernamefrom = $params[2];
        $usernameto = $params[3];
        $rolename = $params[4];
        
        require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
        
        if ($usernamefrom == '' || !isset($usernamefrom)) {
            $userfrom = JFactory::getUser();
            $userfrom_id = $userfrom->id;
        } else {
            $userfrom_id = JUserHelper::getUserId($usernamefrom);   
        }
        if ($usernameto == '' || !isset($usernameto)) {
            $userto = JFactory::getUser();
            $userto_id = $userto->id;
        } else {
            $userto_id = JUserHelper::getUserId($usernameto);
        }
        
        // add notification
        $params = new CParameter('');
        $params->set('url' , 'index.php?option=com_joomdle&view=course&course_id='.$courseid);
        $params->set('course_url' , 'index.php?option=com_joomdle&view=course&course_id='.$courseid);
        $params->set('course', $coursename);
        CNotificationLibrary::add('course_enrol', $userfrom_id, $userto_id, JText::sprintf('COM_JOOMDLE_USER_ENROL_NOTIFICATION', $rolename), '', 'course.enrol', $params);
        
        // send email
        $this->emailEnrolSuccess($userto_id, $courseid, $coursename, $rolename);
        
        // Push notification
        $message = array(
            'mtitle' => JText::sprintf('COM_JOOMDLE_EMAIL_ENROL_USER_SUBJECT', $rolename, $coursename),
            'mdesc' => JText::_('COM_JOOMDLE_EMAIL_ENROL_USER_BODY')
        );
        JPluginHelper::importPlugin( 'groupsapi' );
        $dispatcher = JDispatcher::getInstance();
        $dispatcher->trigger('pushNotification', array ($message, $userto_id, 'creator'));
        
        return true;
    }
    
    function courseSubscribeNotif($method,$params) {
        $courseid = $params[0];
        $coursename = $params[1];
        $usernamefrom = $params[2];
        $usernameto = $params[3];
        $rolename = $params[4];
        
        require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
        
        if ($usernamefrom == '' || !isset($usernamefrom)) {
            $userfrom = JFactory::getUser();
            $userfrom_id = $userfrom->id;
        } else {
            $userfrom_id = JUserHelper::getUserId($usernamefrom);   
        }
        if ($usernameto == '' || !isset($usernameto)) {
            $userto = JFactory::getUser();
            $userto_id = $userto->id;
        } else {
            $userto_id = JUserHelper::getUserId($usernameto);
        }
        
        $message = '';
        if ($rolename == 'course_lp') {
            $message = JText::sprintf('COM_JOOMDLE_COURSE_SUBSCRIBED_LP_NOTIF', date('d/m/Y'));
            $cmtype = 'course_subscribe_lp';
        } else if ($rolename == 'course_non') {
            $message = JText::sprintf('COM_JOOMDLE_COURSE_SUBSCRIBED_CC_NOTIF');
            $cmtype = 'course_subscribe_non';
        }
        
        
        // add notification
        $params = new CParameter('');
        $params->set('url' , 'index.php?option=com_joomdle&view=course&course_id='.$courseid);
        $params->set('course_url' , 'index.php?option=com_joomdle&view=course&course_id='.$courseid);
        $params->set('course', $coursename);
        CNotificationLibrary::add($cmtype, $userfrom_id, $userto_id, $message, '', 'course.subscribe', $params);
        
        // send email
        $this->emailSubscribeSuccess($userto_id, $courseid, $coursename, $rolename, $userfrom_id);
                
        return true;
    }
    
    function sendForApprovalNotif ($method,$params) {
        $courseid = $params[0];
        $coursename = $params[1];
        $usernamefrom = $params[2];
        $usernameto = $params[3];
        
        require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
        
        if ($usernamefrom == '' || !isset($usernamefrom)) {
            $userfrom = JFactory::getUser();
            $userfrom_id = $userfrom->id;
        } else {
            $userfrom_id = JUserHelper::getUserId($usernamefrom);   
        }
        if ($usernameto == '' || !isset($usernameto)) {
            $userto = JFactory::getUser();
            $userto_id = $userto->id;
        } else {
            $userto_id = JUserHelper::getUserId($usernameto);
        }
        
        // add notification
        $params = new CParameter('');
        $params->set('url' , 'index.php?option=com_joomdle&view=mycourses#pendingapproval');
        $params->set('course_url' , 'index.php?option=com_joomdle&view=course&course_id='.$courseid);
        $params->set('course', $coursename);
        $params->set('coursename', $coursename);
        CNotificationLibrary::add('course_sendapprove', $userfrom_id, $userto_id, JText::sprintf('COM_JOOMDLE_SEND_FOR_APPROVAL_NOTIF'), '', 'course.sendapprove', $params);
        
        // send email
        $this->emailSendForApprovalSuccess($userto_id, $userfrom_id, $courseid, $coursename);
        
        // Push notification
        $message = array(
            'mtitle' => JText::sprintf('COM_JOOMDLE_EMAIL_SEND_FOR_APPROVAL_SUBJECT'),
            'mdesc' => JText::_('COM_JOOMDLE_EMAIL_SEND_FOR_APPROVAL_BODY')
        );
        
        JPluginHelper::importPlugin( 'groupsapi' );
        $dispatcher = JDispatcher::getInstance();
        $dispatcher->trigger('pushNotification', array ($message, $userto_id, 'creator'));
                
        return true;
    }
    
    function courseApprovalNotif ($method,$params) {
        $courseid = $params[0];
        $coursename = $params[1];
        $usernamefrom = $params[2];
        $usernameto = $params[3];
        $action = $params[4];
        
        require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
        
        if ($usernamefrom == '' || !isset($usernamefrom)) {
            $userfrom = JFactory::getUser();
            $userfrom_id = $userfrom->id;
        } else {
            $userfrom_id = JUserHelper::getUserId($usernamefrom);   
        }
        if ($usernameto == '' || !isset($usernameto)) {
            $userto = JFactory::getUser();
            $userto_id = $userto->id;
        } else {
            $userto_id = JUserHelper::getUserId($usernameto);
            $userto = JFactory::getUser($userto_id);
        }
        
        if ($action == 'approve') {
            $message = JText::sprintf('COM_JOOMDLE_COURSE_APPROVED_NOTIF');
            $cmtype = 'course_approval';
            $message_push = array(
                'mtitle' => JText::sprintf('COM_JOOMDLE_COURSE_APPROVED_NOTIF'),
                'mdesc' => JText::_('COM_JOOMDLE_EMAIL_COURSE_APPROVED_BODY')
            );
            $subject = JText::sprintf('COM_JOOMDLE_EMAIL_COURSE_APPROVED_SUBJECT', $coursename);
            $body = JText::sprintf('COM_JOOMDLE_EMAIL_COURSE_APPROVED_BODY', $userto->name, $coursename);

        } else if ($action == 'unapprove') {
            $message = JText::sprintf('COM_JOOMDLE_COURSE_UNAPPROVED_NOTIF');
            $cmtype = 'course_approval';
            $message_push = array(
                'mtitle' => JText::sprintf('COM_JOOMDLE_COURSE_UNAPPROVED_NOTIF'),
                'mdesc' => JText::_('COM_JOOMDLE_EMAIL_COURSE_UNAPPROVED_BODY')
            );

            $subject = JText::sprintf('COM_JOOMDLE_EMAIL_COURSE_UNAPPROVED_SUBJECT', $coursename);
            $body = JText::sprintf('COM_JOOMDLE_EMAIL_COURSE_UNAPPROVED_BODY', $userto->name, $coursename);
        }
        
        // add notification
        $params = new CParameter('');
        $params->set('url' , 'index.php?option=com_joomdle&view=course&course_id='.$courseid);
        $params->set('course_url' , 'index.php?option=com_joomdle&view=course&course_id='.$courseid);
        $params->set('course', $coursename);
        $params->set('coursename', $coursename);
        
        // send email
         $config = JFactory::getConfig();
        $fromname = $config->get('fromname');
        $from = $config->get('mailfrom');
        
        $url = JURI::base() . 'index.php?option=com_joomdle&view=course&course_id=' . $courseid;
        JFactory::getMailer()->sendMail($from, $fromname, $userto->email, $subject, $body);

       // $this->emailApprovalSuccess($userto_id, $courseid, $coursename, $action);
       
        CNotificationLibrary::add($cmtype, $userfrom_id, $userto_id, $message, '', 'course.approval', $params);
        // Push notification
        
        JPluginHelper::importPlugin( 'groupsapi' );
        $dispatcher = JDispatcher::getInstance();
        $dispatcher->trigger('pushNotification', array ($message_push, $userto_id, 'creator'));
        
        return true;
    }
    
    function updateHikashopProduct ($method, $params)
    {
        require_once(JPATH_ADMINISTRATOR.'/components/com_hikashop/helpers/helper.php');
        require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/shop.php');
        $app = JFactory::getApplication();
        $config = hikashop_config();
        $allowed = $config->get('allowedfiles');
        $imageHelper = hikashop_get('helper.image');
        $file_class = hikashop_get('class.file');
        $uploadPath = $file_class->getPath('product','');

        $db           = JFactory::getDBO();
        $query = "SELECT * FROM #__hikashop_product WHERE product_code = ". $db->Quote($params[0]);
        $db->setQuery($query);
        $products = $db->loadObjectList();
        if (count ($products))
        {
            $product_id = $products[0]->product_id;
            $element = new stdClass();
            $element = $products[0];
            $product_class = hikashop_get('class.product');

            $course_info = JoomdleHelperContent::getCourseInfo ($params[0]);

            if (!empty($course_info)) {
                if (!isset($course_info['filename'])) {
                    $file_class->deleteFiles('product', $product_id, false);
                } else {
                    $pic_url = $course_info['filepath'] . rawurlencode($course_info['filename']);
                    $pic = JoomdleHelperContent::get_file($pic_url);

                    $file = new stdClass();
                    $file->file_name = '';
                    $file->file_description = '';

                    $filename = basename($pic_url);
                    $file_path = strtolower(JFile::makeSafe($filename));

                    if (!preg_match('#\.(' . str_replace(array(',', '.'), array('|', '\.'), $allowed) . ')$#Ui', $file_path, $extension) || preg_match('#\.(php.?|.?htm.?|pl|py|jsp|asp|sh|cgi)$#Ui', $file_path)) {
                        $app->enqueueMessage(JText::sprintf('ACCEPTED_TYPE', substr($file_path, strrpos($file_path, '.') + 1), $allowed), 'notice');
                        continue;
                    }
                    $file_path = str_replace(array('.', ' '), '_', substr($file_path, 0, strpos($file_path, $extension[0]))) . $extension[0];

                    file_put_contents($uploadPath . $file_path, $pic);

                    $imageHelper->resizeImage($file_path);
                    $imageHelper->generateThumbnail($file_path);

                    $file->file_path = $file_path;
                    $file->file_type = 'product';
                    $file->file_ref_id = $product_id;

                    $image_id = $file_class->save($file);
                    $element->images[] = $image_id;
                }
                $product_class->updateFiles($element, $product_id, 'images');
            }
        }
        return $products;
    }

class JoomdleControllerWs extends JControllerLegacy
{
	/*
	function check_origin ()
    {
        $request_ip = JRequest::getVar ('REMOTE_ADDR', '', 'server');
        $comp_params = JComponentHelper::getParams( 'com_joomdle' );

        $internal_ip = $comp_params->get( 'internal_ip' );
		if ($internal_ip)
		{
			$moodle_ip = $internal_ip;
		}
		else
		{
			$moodle_url = $comp_params->get( 'MOODLE_URL' );
			$url = parse_url ($moodle_url);
			$domain = $url['host'];
			$moodle_ip = gethostbyname ($domain);
		}

        return  ($request_ip == $moodle_ip);
    }
	*/

    function check_token ()
    {
        $token = JRequest::getVar ('token');
        $comp_params = JComponentHelper::getParams( 'com_joomdle' );

        $joomla_token = $comp_params->get( 'joomla_auth_token' );

        return  ($token == $joomla_token);
    }

	public function server ()
    {
		/*
		if (!$this->check_origin ())
		{
			print_r (xmlrpc_encode ( "XML-RPC Error (1): Access Denied" . JRequest::getVar ('REMOTE_ADDR', '', 'server')));
			return;
		}
		*/
		if (!$this->check_token ())
		{
			print_r (xmlrpc_encode ( "XML-RPC Error (1): Invalid token:" . JRequest::getVar ('token')));
			return;
		}

	$document = JFactory::getDocument();
	$document->setMimeEncoding('text/xml') ;

	$xmlrpc_server = xmlrpc_server_create();

	xmlrpc_server_register_method($xmlrpc_server, "joomdle.getUserInfo", "getUserInfo");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.test", "test");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.login", "login");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.confirmJoomlaSession", "confirmJoomlaSession");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.getDefaultItemid", "getDefaultItemid");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.logout", "logout");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.createUser", "createUser");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.activateUser", "activateJoomlaUser");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.updateUser", "updateUser");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.changePassword", "changePassword");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.deleteUser", "deleteUser");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.addActivityCourse", "addActivityCourse");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.addActivityCourseEnrolment", "addActivityCourseEnrolment");


	xmlrpc_server_register_method($xmlrpc_server, "joomdle.addSocialGroup", "addSocialGroup");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.updateSocialGroup", "updateSocialGroup");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.deleteSocialGroup", "deleteSocialGroup");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.addSocialGroupMember", "addSocialGroupMember");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.removeSocialGroupMember", "removeSocialGroupMember");

	xmlrpc_server_register_method($xmlrpc_server, "joomdle.addPoints", "addPoints");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.addActivityQuizAttempt", "addActivityQuizAttempt");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.addMailingSub", "addMailingSub");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.removeMailingSub", "removeMailingSub");

	xmlrpc_server_register_method($xmlrpc_server, "joomdle.addUserGroups", "addUserGroups");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.removeUserGroups", "removeUserGroups");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.addGroupMember", "addGroupMember");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.removeGroupMember", "removeGroupMember");

	xmlrpc_server_register_method($xmlrpc_server, "joomdle.addForum", "addForum");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.removeForum", "removeForum");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.addForumsModerator", "addForumsModerator");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.removeForumsModerator", "removeForumsModerator");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.removeCourseForums", "removeCourseForums");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.getSellUrl", "getSellUrl");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.addActivityCourseCompleted", "addActivityCourseCompleted");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.getGroupbyCourseID", "getGroupbyCourseID");
	xmlrpc_server_register_method($xmlrpc_server, "joomdle.publishCourse", "publishCourse");
        xmlrpc_server_register_method($xmlrpc_server, "joomdle.shareCourseToCircle", "shareCourseToCircle");
        xmlrpc_server_register_method($xmlrpc_server, "joomdle.courseNotifications", "courseNotifications");
        xmlrpc_server_register_method($xmlrpc_server, "joomdle.sendForApprovalNotif", "sendForApprovalNotif");
        xmlrpc_server_register_method($xmlrpc_server, "joomdle.courseApprovalNotif", "courseApprovalNotif");
        xmlrpc_server_register_method($xmlrpc_server, "joomdle.courseProductHikshop", "courseProductHikshop");
        xmlrpc_server_register_method($xmlrpc_server, "joomdle.courseSubscribeNotif", "courseSubscribeNotif");
        xmlrpc_server_register_method($xmlrpc_server, "joomdle.copySocialGroup", "copySocialGroup");
        xmlrpc_server_register_method($xmlrpc_server, "joomdle.searchCourseProductHikshop", "searchCourseProductHikshop");
        xmlrpc_server_register_method($xmlrpc_server, "joomdle.countSearchCourseProductHikshop", "countSearchCourseProductHikshop");
        xmlrpc_server_register_method($xmlrpc_server, "joomdle.getBLNMoodleCategoryId", "getBLNMoodleCategoryId");


	//$request_xml = $HTTP_RAW_POST_DATA;
	$request_xml = @file_get_contents('php://input');

	$response = xmlrpc_server_call_method($xmlrpc_server, $request_xml, '');
	print_r ( $response );
	//echo xmlrpc_encode ($response);

//	$xmlrpc_server->service($response);

	exit ();

    }


}

?>
