<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewCoursegroups extends JViewLegacy
{
    function display($tpl = null)
    {

        global $mainframe;
        $app = JFactory::getApplication();
        $params = $app->getParams();

        $this->assignRef('params', $params);
        /* Get courseid from URL */
        $courseid = JRequest::getVar('course_id', null, 'NEWURLFORM');
        if (!$courseid) $courseid = JRequest::getVar('course_id');
        if (!$courseid) $courseid = $params->get('course_id');
        $courseid = (int)$courseid;
        if (!$courseid) {
            echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
            return;
        }
        require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
        $document = JFactory::getDocument();
        $groups = CFactory::getModel('groups');
        $this->course_info = JoomdleHelperContent::call_method('get_course_info', (int)$courseid, '');
        if (isset($_POST['act']) && $_POST['act'] == 'joingroup') {
            $groupid = $_POST['groupid'];
            $groupid = (int)$groupid;
            JPluginHelper::importPlugin('groupsapi');
            $dispatcher = JDispatcher::getInstance();
            $result = $dispatcher->trigger('joinGroup', $groupid);
            echo json_encode($result);
            die;
        }

        // Get all social groups of course
        $this->groups = $groups->getShareGroups($courseid);
        $document->setTitle('Course Groups');
        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
        parent::display($tpl);
    }
}

?>