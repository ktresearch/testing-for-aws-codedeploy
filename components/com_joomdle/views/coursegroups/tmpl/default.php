<?php
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . '/components/com_joomdle/css/swiper.min.css');
$document->addScript(JUri::root() . '/components/com_joomdle/js/swiper.min.js');
require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
?>
<script src="components/com_joomdle/js/jquery-1.11.1.js"></script>

<div class="container">
    <div class="mygroupcourse-title-block">
        <span class="icon-circles"><img src="https://test2.parenthexis.com/images/socialcircles.png"></span> <span
                class="title-text"><?php echo JText::_('COM_JOOMDLE_SOCIAL_CIRCLE_OF_COURSE') . ' '.$this->course_info['fullname'] ; ?></span>
    </div>
    <div class="mygroup-content-block">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php
                foreach ($this->groups as $group) {
                    $table = JTable::getInstance('Group', 'CTable');
                    $table->load($group->groupid);
                    ?>
                    <div class="swiper-slide">
                        <div class="group-title">
                            <a href="index.php?option=com_community&amp;view=groups&amp;task=viewgroup&amp;groupid=<?php echo $group->groupid; ?>">
                                <img class="cAvatar jomNameTips group-image"
                                     src="<?php echo $table->getAvatar(); ?>">
                            </a>
                            <!--                            check user is nember of group-->
                            <?php
                            $document = JFactory::getDocument();
                            $groups = CFactory::getModel('groups');
                            $user = JFactory::getUser();
                            $check = $groups->isMember($user->id, $group->groupid);
                            if ($check == 0) {
                                ?>
                                <span class="bt-join-group" id="bt-join-group<?php echo $group->groupid; ?>"
                                        groupid="<?php echo $group->groupid; ?>">Join group
                                </span>
                            <?php } ?>
                            <div class="gr-title-box" style="height: 24px; overflow: hidden;	">
                                <a class="gr-title"
                                   href="index.php?option=com_community&amp;view=groups&amp;task=viewgroup&amp;groupid=<?php echo $group->groupid; ?>"><?php echo $table->name; ?></a>
                            </div>
                            <div class="gr-owner-box">
                                <p class="gr-owner"><?php echo JText::_('OWNER') . ': ' . JFactory::getUser($table->ownerid)->name; ?></p>
                            </div>

                            <div class="gr-description-box">
                                <p class="group-description"><?php if (strlen($table->description) >= 50) echo substr_replace($table->description, '...', 50); else echo $table->description; ?></p>
                            </div>

                            <div class="desktop-group-statistic">
                                <span class="group-statistic">
                                        <span title="Member count"><?php echo $table->membercount; ?>
                                            <img class="icon_member" src="templates/t3_bs3_blank/images/Member30x30.png"
                                                 style="width: 15px; height: 15px">
                                        </span>
                                        <span title="Discuss count"><?php echo $table->discusscount; ?>
                                            <img class="icon_member" src="templates/t3_bs3_blank/images/Topics30x30.png"
                                                 style="width: 15px; height: 15px; margin-bottom:2px;"></span>
                                    <?php
                                    $eventsModel = CFactory::getModel('Events');
                                    $tmpEvents = $eventsModel->getGroupEvents($group->groupid, 0);
                                    ?>
                                    <span title="Event count"><?php echo count($tmpEvents); ?>
                                        <img class="icon_member" src="templates/t3_bs3_blank/images/Events30x30.png"
                                             style="width: 15px; height: 15px">
                                        </span>
                                    </span>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php
if ($device == 'mobile') {
    $slideview = '1.1';
    $space = '10';
} else if ($device == 'tablet') {
    $slideview = '2.5';
    $space = '15';
} else {
    $slideview = '3';
    $space = '15';
}
?>
<script type="text/javascript">
    var x = screen.width;
    var a = 0;
    if (x < 390) a = 1.1;
    if (x >= 390 && x < 481) a = 1.3;
    if (x >= 481 && x <= 736) a = 1.5;
    if (x > 736 && x < 1024) a = 2.3;
    if (x >= 1024) a = 3;

    var swiper = new Swiper('.swiper-container', {
        pagination: '.my-course .swiper-pagination',
        slidesPerView: a,
        nextButton: '.my-course .swiper-button-next',
        prevButton: '.my-course .swiper-button-prev',
        spaceBetween: <?php echo $space; ?>,
        freeMode: true,
        freeModeMomentum: true,
        preventClicks: false,
        preventClicksPropagation: false
    });
</script>
<script>
    (function ($) {
        $(".bt-join-group").click(function () {
            var groupid = $(this).attr("groupid");
            $.ajax({
                url: window.location.href,
                type: 'post',
                data: {
                    groupid: groupid,
                    act: 'joingroup'
                },

                beforeSend: function () {
                    $('.lgt-notif').html('Saving...').addClass('lgt-visible');
                },
                success: function (result) {
                    $('#bt-join-group' + groupid).addClass('hidden');
                    $('.lgt-notif').removeClass('lgt-visible');
                }
            });
        });
    })(jQuery);
</script>