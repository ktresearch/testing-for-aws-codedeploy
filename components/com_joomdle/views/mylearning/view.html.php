<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewMylearning extends JViewLegacy
{
    function display($tpl = null)
    {
        $app = JFactory::getApplication();
        $params = $app->getParams();
        if (!$params->get('use_new_performance_method') || ($params->get('use_new_performance_method') && JHelperLGT::countMyLearningCourses() <= 10)) {
            echo $this->showDefaultPage($tpl);
        } else {
            JFactory::getDocument()->addStyleSheet(JUri::base().'components/com_community/assets/release/css/placeholder-loading.min.css');
            echo $this->showDefaultPage($tpl, true);
        }
    }

    function showDefaultPage($tpl = null, $useSample = false) {
        require_once(JPATH_SITE . '/modules/mod_mygroups/helper.php');
        $user = JFactory::getUser();
        $username = $user->username;
        $app = JFactory::getApplication();
        $params = $app->getParams();
        $this->assignRef('params', $params);
        $this->assign('useSample', $useSample);

        $enrollable_only = $params->get('enrollable_only');
        $sort_by = $params->get('sort_by', 'name');

        switch ($sort_by) {
            case 'date':
                $order = 'timestart DESC ';
                break;
            case 'sortorder':
                $order = 'sortorder DESC';
                break;
            default:
                $order = 'timestart DESC ';
                break;
        }

        $where = '';
        $swhere = '';
        $version = 'web';

        if ($useSample) {
            $this->cursos = [];
            $this->recently_accessed = [];
            $this->not_yet_started = [];
            $this->completed = [];
            $this->course_facifitator = [];
        } else {
            if ($params->get('use_new_performance_method')) {
                $learningPageData = JHelperLGT::getLearningPageData();
                $this->cursos = $learningPageData['courses'];
            } else {
                $this->cursos = JoomdleHelperContent::getCourseList((int)$enrollable_only, $order, 0, $username, $where, $swhere, $version);
            }

            if (is_array($this->cursos) && !empty($this->cursos)) {
                $this->recently_accessed = [];
                $this->not_yet_started = [];
                $this->completed = [];
                $this->course_facifitator = [];
                foreach ($this->cursos as $curso) {
                    if (JHelperLGT::checkCreatorCourse($curso['remoteid'])) {
                        if ($curso['isFacilitator']) {
                            $this->course_facifitator[] = $curso;
                        } else if ($curso['isLearner']) {
                            if ($curso['completion_status'] >= 50) {
                                $this->completed[] = $curso;
                            } elseif ($curso['last_access'] > 0) {
                                $this->recently_accessed[] = $curso;
                            } else {
                                $this->not_yet_started[] = $curso;
                            }
                        }
                    }
                }
            }
        }
        $this->_prepareDocument();

        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
        if ( empty($this->recently_accessed)
            && empty($this->not_yet_started)
            && empty($this->completed)
            && empty($this->course_facifitator)
            && !$useSample) {
            return $this->loadTemplate('emptylearning');
        } else {
            return $this->loadTemplate($tpl);
        }
    }

    protected function _prepareDocument()
    {
        $app = JFactory::getApplication();
        $menus = $app->getMenu();
        $title = null;
        // Because the application sets a default page title,
        // we need to get it from the menu item itself
        $menu = $menus->getActive();
        if ($menu) {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        } else {
            $this->params->def('page_heading', JText::_('COM_JOOMDLE_MY_NEWS'));
        }
    }

}

?>