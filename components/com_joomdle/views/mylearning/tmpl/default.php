<?php defined('_JEXEC') or die('Restricted access');
$session = JFactory::getSession();
$device = $session->get('device');
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'components/com_joomdle/css/swiper.min.css');
$document->addScript(JUri::root() . 'components/com_joomdle/js/swiper.min.js');
require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
JLoader::import('joomla.user.helper'); 
JPluginHelper::importPlugin('joomdlesocialgroups');
$dispatcher = JDispatcher::getInstance();

function custom_echo($x, $length) {
    if (strlen($x) <= $length) {
        return $x;
    } else {
        $y = substr($x, 0, $length) . '...';
        return $y;
    }
}

if ($device == 'mobile') {
    $class_tablet = '';
    ?>
    <div class="joomdle-mylearning isMA">
        <div class="joomdle_mylearning_header">
        <span class="left myLearning">
            <a href="<?php echo 'mylearning/list.html'; ?>"><?php echo JText::_('MY_LEARNING'); ?></a>
        </span>
            <span class="left myCourses">
            <a href="<?php echo 'mycourses/list.html'; ?>"><?php echo JText::_('MY_COURSES'); ?></a>
        </span>
            <span class="right learningStore">
            <!--    Check language -->
            <?php
            if (strcmp((JFactory::getLanguage()->getTag()), 'vi-VN') == 0) {
                ?>
                <a href="<?php echo JRoute::_('/dac-sac'); ?>"><?php echo JText::_('COM_JOOMDLE_LEARNING_STORE'); ?></a>
                <?php
            }
            if (strcmp((JFactory::getLanguage()->getTag()), 'en-GB') == 0) {
                ?>
                <a href="<?php echo JRoute::_('/featured'); ?>"><?php echo JText::_('COM_JOOMDLE_LEARNING_STORE'); ?></a>

                <?php
            }
            ?>
        </span>
        </div>
        <div class="joomdle_mylearning">
            <div class="list_recently_accessed active">
                <div class="swiper-container">
                    <?php 
                    $recently_accessed = 0;
                    foreach ($this->recently_accessed as $curso){
                       $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                       if(!$publish_group[0]) continue;
                       $recently_accessed ++;
                    }
                    if ($recently_accessed != 0) { ?>
                        <span class="block-title">
                            <?php echo JText::_('RECENTLY_ACCESSED'); ?> 
                            </span><span class="count_course"><?php echo $recently_accessed; ?>
                        </span>
                    <?php } else { ?>
                        <span class="block-title"><?php echo JText::_('RECENTLY_ACCESSED'); ?> </span>
                    <?php } ?>

                    <div class="swiper-wrapper">
                        <?php if ($this->useSample) { ?>
                            <div class="placeholder-loading">
                                <div class="ph-item">
                                    <div class="ph-picture"></div>
                                    <div class="ph-col-12">
                                        <div class="ph-row">
                                            <div class="ph-col-6 big"></div>
                                            <div class="ph-col-6 big empty"></div>
                                            <div class="ph-col-12"></div>
                                            <div class="ph-col-12"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } else {
                        if (!is_array($this->recently_accessed) || empty($this->recently_accessed)) {
                            echo '<span class="bold cred tcenter bg">' . JText::_('NO_COURSE_FOUND') . '.</span>';
                        } else {
                        $i = 0;
                        foreach ($this->recently_accessed as $curso) :
                            // $curso['fullname'] = custom_echo($curso['fullname'], 27);
                            $notification = My_Groups::groupActivities($curso['course_group']);
                             $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                              if(!$publish_group[0]) continue;
                        if ($notification != 0) {
                                    $class_notif = "notification";
                                }
                                else   $class_notif = "notiflearn";
                             $class_a = "";
                            $class_p = "";
                            if(strlen($curso['fullname']) > 44){
                                $class_a = "limited";
                            }
                             if(strlen($curso['summary'])>44){
                                $class_p= "limited";
                            }
                            $display = "";
                            if ($curso['coursetype'] == 'program') {
                                $programid = $curso['cat_id'];
                                $url = JURI::base() . 'courseprogram/' . $programid . '.html';
                                $url_course = JURI::base() . 'courseprogram/' . $programid . '.html';
                                $course_button = JText::_('DETAILS');
                                $color = "";
                            } else {
                                $course_button = JText::_('CONTINUE');
                                $color = "background:#ffffff";
                                $display = "display:none;";
                                $course_id = $curso['remoteid'];
                                $url = JURI::base() . 'course/' . $course_id . '.html';
                            }
                            $cat_id = $curso['cat_id'];
                            $cat_slug = JFilterOutput::stringURLSafe($curso['cat_name']);
                            $course_slug = JFilterOutput::stringURLSafe($curso['fullname']);
                            $course_image = $curso['filepath'] . $curso['filename'];

                            (array_key_exists('last_access', $curso) && !is_null($curso['last_access'])) ? $time = date('d/m/Y', $curso['last_access']) : JText::_('UNKNOWN');
                            $completion_des = JText::_('LAST_ACCESSED') . ': ' . $time;
                            ?>
                            <div class="swiper-slide">
                                <div class="box_learning_mobile <?php echo $class_tablet; ?>">
                                    <div class="course_content">
                                        <div class="clearfix"></div>
                                        <div class="mylearning-img">
                                            <a href="<?php echo $url; ?>">
                                                <div id="images" class="left lazyload" data-src="<?php echo $course_image.'?'.(time()*1000); ?>"></div>
                                            </a>
                                            <div class="<?php echo $class_notif?>">
                                                  <span >
                                                        <?php 
                                                        echo $notification;
                                                        ?>
                                                    </span>
                                        </div>
                                        </div>
                                        <div class="content_c" style="<?php echo $color ?>">
                                            <div class="clearfix"></div>
                                            <div class="course_title" style="margin-top:10px;">
                                                <?php echo "<a class=\" $class_a\" href=\"$url\">" . $curso['fullname'] . "</a>"; ?>
                                            </div>
                                            <div class="right">
                                               <!--  <div class="courseDetails">
                                                <?php
                                                        $userid = JUserHelper::getUserId($curso['creator']);
                                                        $user_owner = JFactory::getUser($userid); 
                                                        if ($curso['count_learner'] && $curso['count_learner'] > 1) {
                                                            echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBERS') . '</span></br>';
                                                        } else {
                                                            echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBER') . '</span></br>';
                                                        }
                                                        $time_view = '';
                                                        if ($curso['timepublish'] && $curso['timepublish'] > 0) {
                                                            $time_published = $curso['timepublish']; 
                                                            $date2 = JFactory::getDate($time_published);
                                                            $time_view = CTimeHelper::timeLapseNew($date2, false);
                                                        }
                                                        if ($time_view != '') {
                                                            echo number_format((float)$curso['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                                        } else {
                                                            echo number_format((float)$curso['duration'], 2, '.', '') . ' hr';
                                                        }
                                                    ?>
                                                </div> -->
                                                <p class = "<?php echo $class_p;?>">
                                                <?php
                                                    echo $curso['summary'];
                                                ?>
                                                </p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </div>
                <?php
                } }
                ?>
                </div>
            </div>

            <?php if (!$this->useSample) { ?>
            <div class="list_not_yet_started">
                <div class="swiper-container">
                    <?php 
                     $not_yet_started = 0;
                    foreach ($this->not_yet_started as $curso){
                        $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                        if(!$publish_group[0]) continue;
                        $not_yet_started ++;
                     }
                    if ($not_yet_started != 0) { ?>
                        <span class="block-title"><?php echo JText::_('NOT_YET_STARTED'); ?></span>
                        <span class="count_course">
                            <?php echo $not_yet_started; ?></span>
                    <?php } else { ?>
                        <span class="block-title"><?php echo JText::_('NOT_YET_STARTED'); ?> </span>
                    <?php } ?>

                    <div class="swiper-wrapper">
                        <?php if (!is_array($this->not_yet_started) || empty($this->not_yet_started)) {
                            echo '<span class="bold cred tcenter bg">' . JText::_('NO_COURSE_FOUND') . '.</span>';
                        } else {
                        $i = 0;
                        foreach ($this->not_yet_started as $curso) :
                            // $curso['fullname'] = custom_echo($curso['fullname'], 27);

                            $notification = My_Groups::groupActivities($curso['course_group']);
                            $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                            if(!$publish_group[0]) continue;
                            if ($notification != 0) {
                                    $class_notif = "notification";
                                }
                            else   $class_notif = "notiflearn";
                            $class_a = "";
                            $class_p = "";
                            if(strlen($curso['fullname']) > 44){
                                $class_a = "limited";
                            }
                             if(strlen($curso['summary'])>44){
                                $class_p= "limited";
                            }
                            if ($curso['coursetype'] == 'program') {
                                $programid = $curso['cat_id'];
                                $url = JURI::base() . 'courseprogram/' . $programid . '.html';
                                $url_course = JURI::base() . 'courseprogram/' . $programid . '.html';
                                $course_button = JText::_('DETAILS');
                                $display = "";
                                $color = "";
                            } else {
                                $color = "background:#ffffff";
                                $display = "display:none;";
                                $course_id = $curso['remoteid'];
                                $url = JURI::base() . 'course/' . $course_id . '.html';
                                $course_button = JText::_('CONTINUE');
                            }
                            $cat_id = $curso['cat_id'];
                            $cat_slug = JFilterOutput::stringURLSafe($curso['cat_name']);
                            $course_slug = JFilterOutput::stringURLSafe($curso['fullname']);
                            $course_image = $curso['filepath'] . $curso['filename'];

                            (array_key_exists('enrol_time', $curso) && !is_null($curso['enrol_time'])) ? $time = date('d/m/Y', $curso['enrol_time']) : JText::_('UNKNOWN');
                            $completion_des = JText::_('SUBCRIBED') . ': ' . $time;
                            $course_button = JText::_('START_LEARNING');

                            ?>
                            <div class="swiper-slide">
                                <div class="box_learning_mobile <?php echo $class_tablet; ?>">
                                    <div class="course_content">
                                        <div class="clearfix"></div>
                                        <div class="mylearning-img">
                                            <a href="<?php echo $url; ?>">
                                                <div id="images" class="left lazyload" data-src="<?php echo $course_image.'?'.(time()*1000); ?>"></div>
                                            </a>

                                            <div class="<?php echo $class_notif ?>">
                                                  <span >
                                                        <?php 
                                                        echo $notification;
                                                        ?>
                                                    </span>
                                        </div>
                                        </div>
                                        <div class="content_c" style="<?php echo $color ?>">
                                            <div class="clearfix"></div>
                                            <div class="course_title" style="margin-top:10px;">
                                                <?php echo "<a class=\" $class_a\" href=\"$url\">" . $curso['fullname'] . "</a>"; ?>
                                            </div>
                                            <div class="right">
                                                <!-- <div class="courseDetails">
                                                <?php
                                                        $userid = JUserHelper::getUserId($curso['creator']);
                                                        $user_owner = JFactory::getUser($userid); 
                                                        if ($curso['count_learner'] && $curso['count_learner'] > 1) {
                                                            echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBERS') . '</span></br>';
                                                        } else {
                                                            echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBER') . '</span></br>';
                                                        }
                                                        $time_view = '';
                                                        if ($curso['timepublish'] && $curso['timepublish'] > 0) {
                                                            $time_published = $curso['timepublish']; 
                                                            $date2 = JFactory::getDate($time_published);
                                                            $time_view = CTimeHelper::timeLapseNew($date2, false);
                                                        }
                                                        if ($time_view != '') {
                                                            echo number_format((float)$curso['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                                        } else {
                                                            echo number_format((float)$curso['duration'], 2, '.', '') . ' hr';
                                                        }
                                                    ?>
                                                </div> -->
                                                <p class = "<?php echo $class_p;?>">
                                                <?php
                                                    echo $curso['summary'];
                                                ?>
                                                </p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </div>
                <?php
                }
                ?>
                </div>
            </div>
            <div class="list_completed">
                <div class="swiper-container">
                    <?php 
                    $completed = 0;
                     foreach ($this->completed as $curso){
                        $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                        if(!$publish_group[0]) continue;
                        $completed ++;
                     }
                    if ($completed != 0) { ?>
                        <span class="block-title"><?php echo JText::_('COMPLETED'); ?></span>
                        <span class="count_course">
                            <?php echo $completed; ?></span>
                    <?php } else { ?>
                        <span class="block-title"><?php echo JText::_('COMPLETED'); ?> </span>
                    <?php } ?>

                    <div class="swiper-wrapper">
                        <?php if (!is_array($this->completed) || empty($this->completed)) {
                            echo '<span class="bold cred tcenter bg">' . JText::_('NO_COURSE_FOUND') . '.</span>';
                        } else {
                        $i = 0;
                        foreach ($this->completed as $curso) :
                            // $curso['fullname'] = custom_echo($curso['fullname'], 27);
                            $notification = My_Groups::groupActivities($curso['course_group']);
                            $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                            if(!$publish_group[0]) continue;
                        if ($notification != 0) {
                                    $class_notif = "notification";
                                }
                                else   $class_notif = "notiflearn";
                             $class_a = "";
                            $class_p = "";
                            if(strlen($curso['fullname']) > 44){
                                $class_a = "limited";
                            }
                             if(strlen($curso['summary'])>44){
                                $class_p= "limited";
                            }
                            if ($curso['coursetype'] == 'program') {
                                $programid = $curso['cat_id'];
                                $url = JURI::base() . 'courseprogram/' . $programid . '.html';
                                $url_course = JURI::base() . 'courseprogram/' . $programid . '.html';
                                $course_button = JText::_('DETAILS');
                                $display = "";
                                $color = "";
                            } else {
                                $color = "background:#ffffff";
                                $display = "display:none;";
                                $course_id = $curso['remoteid'];
                                $url = JURI::base() . 'course/' . $course_id . '.html';
                                $course_button = JText::_('CONTINUE');
                            }
                            $cat_id = $curso['cat_id'];
                            $cat_slug = JFilterOutput::stringURLSafe($curso['cat_name']);
                            $course_slug = JFilterOutput::stringURLSafe($curso['fullname']);
                            $course_image = $curso['filepath'] . $curso['filename'];

                            (array_key_exists('enrol_time', $curso) && !is_null($curso['enrol_time'])) ? $time = date('d/m/Y', $curso['enrol_time']) : JText::_('UNKNOWN');
                            $completion_des = JText::_('SUBCRIBED') . ': ' . $time;
                            $course_button = JText::_('START_LEARNING');

                            ?>
                            <div class="swiper-slide">
                                <div class="box_learning_mobile <?php echo $class_tablet; ?>">
                                    <div class="course_content">
                                        <div class="clearfix"></div>
                                        <div class="mylearning-img">
                                            <a href="<?php echo $url; ?>">
                                                <div id="images" class="left lazyload" data-src="<?php echo $course_image.'?'.(time()*1000); ?>"></div>
                                            </a>
                                            <div class="<?php echo $class_notif?>">
                                                  <span >
                                                        <?php 
                                                        echo $notification;
                                                        ?>
                                                    </span>
                                        </div>
                                        </div>
                                        <div class="content_c" style="<?php echo $color ?>">
                                            <div class="clearfix"></div>
                                            <div class="course_title" style="margin-top:10px;">
                                                <?php echo "<a class=\" $class_a\" href=\"$url\">" . $curso['fullname'] . "</a>"; ?>

                                            </div>
                                            <div class="right">
                                                <!-- <div class="courseDetails">
                                                <?php
                                                        $userid = JUserHelper::getUserId($curso['creator']);
                                                        $user_owner = JFactory::getUser($userid); 
                                                        if ($curso['count_learner'] && $curso['count_learner'] > 1) {
                                                            echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBERS') . '</span></br>';
                                                        } else {
                                                            echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBER') . '</span></br>';
                                                        }
                                                        $time_view = '';
                                                        if ($curso['timepublish'] && $curso['timepublish'] > 0) {
                                                            $time_published = $curso['timepublish']; 
                                                            $date2 = JFactory::getDate($time_published);
                                                            $time_view = CTimeHelper::timeLapseNew($date2, false);
                                                        }
                                                        if ($time_view != '') {
                                                            echo number_format((float)$curso['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                                        } else {
                                                            echo number_format((float)$curso['duration'], 2, '.', '') . ' hr';
                                                        }
                                                    ?>
                                                </div> -->
                                                <p class = "<?php echo $class_p;?>">
                                                    <?php
                                                    echo $curso['summary'];
                                                    ?>
                                                </p>
                                                </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                <?php
                            $i++;
                        endforeach;
                        ?>
                    </div>
                <?php
                }
                ?>
                </div>
            </div>
            <!-- ENG -->
             <div class="list_facifitator">
                <div class="swiper-container">
                    <?php 
                    $course_facifitator = 0;
                    foreach ($this->course_facifitator as $curso){
                       $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                       if(!$publish_group[0]) continue;
                       $course_facifitator ++;
                    }
                    if ($course_facifitator != 0) { ?>
                        <span class="block-title"><?php echo JText::_('FACIFITATOR'); ?></span>
                        <span class="count_course">
                            <?php echo $course_facifitator; ?></span>
                    <?php } else { ?>
                        <span class="block-title"><?php echo JText::_('FACIFITATOR'); ?> </span>
                    <?php } ?>

                    <div class="swiper-wrapper">
                        <?php if (!is_array($this->course_facifitator) || empty($this->course_facifitator)) {
                            echo '<span class="bold cred tcenter bg">' . JText::_('NO_COURSE_FOUND') . '.</span>';
                        } else {
                        $i = 0;
                        foreach ($this->course_facifitator as $curso) :
                            // $curso['fullname'] = custom_echo($curso['fullname'], 27);
                            $notification = My_Groups::groupActivities($curso['course_group']);
                            $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                            if(!$publish_group[0]) continue;
                        if ($notification != 0) {
                                    $class_notif = "notification";
                                }
                                else   $class_notif = "notiflearn";
                            $class_a = "";
                            $class_p = "";
                            if(strlen($curso['fullname']) > 44){
                                $class_a = "limited";
                            }
                             if(strlen($curso['summary'])>44){
                                $class_p= "limited";
                            }
                            if ($curso['coursetype'] == 'program') {
                                $programid = $curso['cat_id'];
                                $url = JURI::base() . 'courseprogram/' . $programid . '.html';
                                $url_course = JURI::base() . 'courseprogram/' . $programid . '.html';
                                $course_button = JText::_('DETAILS');
                                $display = "";
                                $color = "";
                            } else {
                                $color = "background:#ffffff";
                                $display = "display:none;";
                                $course_id = $curso['remoteid'];
                                $url = JURI::base() . 'course/' . $course_id . '.html';
                                $course_button = JText::_('CONTINUE');
                            }
                            $cat_id = $curso['cat_id'];
                            $cat_slug = JFilterOutput::stringURLSafe($curso['cat_name']);
                            $course_slug = JFilterOutput::stringURLSafe($curso['fullname']);
                            $course_image = $curso['filepath'] . $curso['filename'];

                            (array_key_exists('enrol_time', $curso) && !is_null($curso['enrol_time'])) ? $time = date('d/m/Y', $curso['enrol_time']) : JText::_('UNKNOWN');
                            $completion_des = JText::_('SUBCRIBED') . ': ' . $time;
                            $course_button = JText::_('START_LEARNING');

                            ?>
                            <div class="swiper-slide">
                                <div class="box_learning_mobile <?php echo $class_tablet; ?>">
                                    <div class="course_content">
                                        <div class="clearfix"></div>
                                        <div class="mylearning-img">
                                            <a href="<?php echo $url; ?>">
                                                <div id="images" class="left lazyload" data-src="<?php echo $course_image.'?'.(time()*1000); ?>"></div>
                                            </a>
                                            <div class="<?php echo $class_notif?>">
                                                  <span >
                                                        <?php 
                                                        echo $notification;
                                                        ?>
                                                    </span>
                                        </div>
                                        </div>
                                        <div class="content_c" style="<?php echo $color ?>">
                                            <div class="clearfix"></div>
                                            <div class="course_title" style="margin-top:10px;">
                                                <?php echo "<a class=\" $class_a\" href=\"$url\">" . $curso['fullname'] . "</a>"; ?>

                                            </div>
                                            <div class="right">
                                               <!--  <div class="courseDetails">
                                                    <?php
                                                    if ($curso['count_learner'] && $curso['count_learner'] > 1) {
                                                        echo '<span>'.$curso['creator'] . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBERS') . '</span></br>';
                                                    } else {
                                                        echo '<span>'.$curso['creator'] . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBER') . '</span></br>';
                                                    }
                                                    $time_view = '';
                                                    if ($curso['timepublish'] && $curso['timepublish'] > 0) {
                                                        $time_published = $curso['timepublish'];
                                                        $date2 = JFactory::getDate($time_published);
                                                        $time_view = CTimeHelper::timeLapseNew($date2, false);
                                                    }
                                                    if ($time_view != '') {
                                                        echo number_format((float)$curso['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                                    } else {
                                                        echo number_format((float)$curso['duration'], 2, '.', '') . ' hr';
                                                    }
                                                    ?>
                                                </div> -->
                                                <p class = "<?php echo $class_p;?>">
                                                    <?php
                                                    echo $curso['summary'];
                                                ?>
                                                </p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </div>
                <?php
                }
                ?>
                </div>
            </div>
            <!-- end-facifitator -->
            <?php } ?>
        </div>

    </div>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('.joomdle-mylearning-status > div').click(function () {
                jQuery('.joomdle-mylearning-status > div').removeClass('active');
                jQuery(this).addClass('active');
                if (jQuery(this).hasClass('recently-accessed')) {
                    jQuery('.joomdle-mylearning .joomdle_mylearning > div').removeClass('active');
                    jQuery('.joomdle-mylearning .joomdle_mylearning > .list_recently_accessed').addClass('active');
                }
                if (jQuery(this).hasClass('not-yet-started')) {
                    jQuery('.joomdle-mylearning .joomdle_mylearning > div').removeClass('active');
                    jQuery('.joomdle-mylearning .joomdle_mylearning > .list_not_yet_started').addClass('active');
                }
                if (jQuery(this).hasClass('completed')) {
                    jQuery('.joomdle-mylearning .joomdle_mylearning > div').removeClass('active');
                    jQuery('.joomdle-mylearning .joomdle_mylearning > .list_completed').addClass('active');
                }
            });
        });
    </script>
<?php } else {
    $class_tablet = 'course-box';
    ?>
    <!--    TABLET-->
    <style>
        .com_joomdle .home {
            display: none;
        }

    </style>

    <div class="joomdle-mylearning isMA">
        <div class="joomdle_mylearning_header changePosition">
        <span class="left myLearning">
            <a href="<?php echo 'mylearning/list.html'; ?>"><?php echo JText::_('MY_LEARNING'); ?></a>
        </span>
            <span class="left myCourses">
            <a href="<?php echo 'mycourses/list.html'; ?>"><?php echo JText::_('MY_COURSES'); ?></a>
        </span>
            <span class="right learningStore">
            <?php
            if (strcmp((JFactory::getLanguage()->getTag()), 'vi-VN') == 0) {
                ?>
                <a href="<?php echo JRoute::_('/dac-sac'); ?>"><?php echo JText::_('COM_JOOMDLE_LEARNING_STORE'); ?></a>
                <?php
            }
            if (strcmp((JFactory::getLanguage()->getTag()), 'en-GB') == 0) {
                ?>
                <a href="<?php echo JRoute::_('/featured'); ?>"><?php echo JText::_('COM_JOOMDLE_LEARNING_STORE'); ?></a>

                <?php
            }
            ?>
        </span>
        </div>
        <div class="joomdle_mylearning">
            <div class="list_recently_accessed active">
                <div class="swiper-container">
                    <?php
                    $recently_accessed = 0;
                     foreach ($this->recently_accessed as $curso){
                        $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                        if(!$publish_group[0]) continue;
                        $recently_accessed ++;
                     }
                    if ( $recently_accessed != 0) { ?>
                        <span class="block-title"><?php echo JText::_('RECENTLY_ACCESSED'); ?></span>
                        <span class="count_course">
                            <?php echo $recently_accessed; ?></span>
                    <?php } else { ?>
                        <span class="block-title"><?php echo JText::_('RECENTLY_ACCESSED'); ?> </span>
                    <?php } ?>
                    <?php if ((!is_array($this->recently_accessed) || empty($this->recently_accessed)) && !$this->useSample) {
                        echo '<span class="bold cred tcenter bg">' . JText::_('NO_COURSE_FOUND') . '.</span>';
                    } else {
                        $i = 0;
                        ?>
                        <div class="swiper-wrapper">
                        <?php if ($this->useSample) { ?>
                            <div class="placeholder-loading">
                                <div class="ph-item">
                                    <div class="ph-picture"></div>
                                    <div class="ph-col-12">
                                        <div class="ph-row">
                                            <div class="ph-col-6 big"></div>
                                            <div class="ph-col-6 big empty"></div>
                                            <div class="ph-col-12"></div>
                                            <div class="ph-col-12"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ph-item">
                                    <div class="ph-picture"></div>
                                    <div class="ph-col-12">
                                        <div class="ph-row">
                                            <div class="ph-col-6 big"></div>
                                            <div class="ph-col-6 big empty"></div>
                                            <div class="ph-col-12"></div>
                                            <div class="ph-col-12"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } else {
                            foreach ($this->recently_accessed as $curso) :
                                // $curso['fullname'] = custom_echo($curso['fullname'], 26);
                                $notification = My_Groups::groupActivities($curso['course_group']);
                                $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                                if(!$publish_group[0]) continue;
                                if ($notification != 0) {
                                    $class_notif = "notification";
                                } else   $class_notif = "notiflearn";
                                $class_a = "";
                                $class_p = "";
                                if (strlen($curso['fullname']) > 44) {
                                    $class_a = "limited";
                                }
                                if (strlen($curso['summary']) > 44) {
                                    $class_p = "limited";
                                }
                                $display = "";
                                if ($curso['coursetype'] == 'program') {
                                    $programid = $curso['cat_id'];
                                    $url = JURI::base() . 'courseprogram/' . $programid . '.html';
                                    $url_course = JURI::base() . 'courseprogram/' . $programid . '.html';
                                    $course_button = JText::_('DETAILS');
                                    $color = "";
                                    $background = 'style="background:#ccc !important;"';
                                } else {
                                    $course_button = JText::_('CONTINUE');
                                    $color = "background:#DCDCDC";
                                    $display = "display:none;";
                                    $course_id = $curso['remoteid'];

                                    $url = JURI::base() . 'course/' . $course_id . '.html';
                                    $background = '';
                                }
                                $cat_id = $curso['cat_id'];
                                $cat_slug = JFilterOutput::stringURLSafe($curso['cat_name']);
                                $course_slug = JFilterOutput::stringURLSafe($curso['fullname']);
                                $course_image = $curso['filepath'] . $curso['filename'];

                                (array_key_exists('last_access', $curso) && !is_null($curso['last_access'])) ? $time = date('d/m/Y', $curso['last_access']) : JText::_('UNKNOWN');
                                $completion_des = JText::_('LAST_ACCESSED') . ': ' . $time;

                                ?>
                                <div class="swiper-slide">
                                    <div class="box_learning <?php echo $class_tablet; ?>" style="<?php echo $color ?>">
                                        <div class="course_content">
                                            <div class="clearfix"></div>
                                            <div class="mylearning-img">
                                                <a href="<?php echo $url; ?>">
                                                    <div id="images" class="left lazyload"
                                                         data-src="<?php echo $course_image . '?' . (time() * 1000); ?>"></div>
                                                </a>

                                                <div class="<?php echo $class_notif?>">
                                                  <span>
                                                        <?php
                                                        echo $notification;
                                                        ?>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="content_c" <?php echo $background; ?>>
                                                <div class="clearfix"></div>
                                                <div class="course_title">
                                                    <?php echo "<a class=\" $class_a\"  href=\"$url\">" . $curso['fullname'] . "</a>";
                                                    ?>
                                                </div>
                                                <div class="right">
                                                    <!--  <div class="courseDetails">
                                                        <?php                                                      
                                                        $userid = JUserHelper::getUserId($curso['creator']);
                                                        $user_owner = JFactory::getUser($userid); 
                                                    if ($curso['count_learner'] && $curso['count_learner'] > 1) {
                                                        echo '<span>' . $curso['creator'] . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBERS') . '</span></br>';
                                                    } else {
                                                        echo '<span>' . $curso['creator'] . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBER') . '</span></br>';
                                                    }
                                                    $time_view = '';
                                                    if ($curso['timepublish'] && $curso['timepublish'] > 0) {
                                                        $time_published = $curso['timepublish'];
                                                        $date2 = JFactory::getDate($time_published);
                                                        $time_view = CTimeHelper::timeLapseNew($date2, false);
                                                    }
                                                    if ($time_view != '') {
                                                        echo number_format((float)$curso['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                                    } else {
                                                        echo number_format((float)$curso['duration'], 2, '.', '') . ' hr';
                                                    }
                                                    ?>
                                                    </div> -->
                                                    <p class="<?php echo $class_p;?>"><?php echo $curso['summary'];//custom_echo($curso['summary'], 80);
                                                        ?></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <?php
                                $i++;
                            endforeach;
                        }
                            ?>
                        </div>
                        <?php
                    }
                    ?>    

                </div>
                <?php if ($recently_accessed != 0){?>
                    <div class="swiper-button-next1"><i class="bg-second-img icon-next-black"></i></div>
                    <div class="swiper-button-prev1"><i class="bg-second-img icon-prev-black"></i></div>
                <?php }?>
                    
            </div><!--end div list_recently_accessed-->

            <?php if (!$this->useSample) { ?>
            <div class="list_not_yet_started">
                <div class="swiper-container">
                    <?php 
                    $not_yet_started = 0;
                    foreach ($this->not_yet_started as $curso){
                        $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                        if(!$publish_group[0]) continue;
                        $not_yet_started ++;
                     }
                    if ($not_yet_started != 0) { ?>
                        <span class="block-title"><?php echo JText::_('NOT_YET_STARTED'); ?></span>
                        <span class="count_course">
                            <?php echo $not_yet_started; ?></span>
                    <?php } else { ?>
                        <span class="block-title"><?php echo JText::_('NOT_YET_STARTED'); ?> </span>
                    <?php } ?>
                    <?php if (!is_array($this->not_yet_started) || empty($this->not_yet_started)) {
                        echo '<span class="bold cred tcenter bg">' . JText::_('NO_COURSE_FOUND') . '.</span>';
                    } else {
                        $i = 0;
                        ?>
                        <div class="swiper-wrapper">
                            <?php
                            foreach ($this->not_yet_started as $curso) :
                                // $curso['fullname'] = custom_echo($curso['fullname'], 26);
                                $notification = My_Groups::groupActivities($curso['course_group']);
                                $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                                if(!$publish_group[0]) continue;
                            if ($notification != 0) {
                                    $class_notif = "notification";
                                }
                                else   $class_notif = "notiflearn";
                                 $class_a = "";
                                    $class_p = "";
                                    if(strlen($curso['fullname']) > 44){
                                        $class_a = "limited";
                                    }
                                     if(strlen($curso['summary'])>44){
                                        $class_p= "limited";
                                    }
                                if ($curso['coursetype'] == 'program') {
                                    $programid = $curso['cat_id'];
                                    $url = JURI::base() . 'courseprogram/' . $programid . '.html';
                                    $url_course = JURI::base() . 'courseprogram/' . $programid . '.html';
                                    $course_button = JText::_('DETAILS');
                                    $display = "";
                                    $color = "";
                                    $background = 'style="background:#ccc !important;"';
                                } else {
                                    $color = "background:#DCDCDC";
                                    $display = "display:none;";
                                    $course_id = $curso['remoteid'];
                                    $url = JURI::base() . 'course/' . $course_id . '.html';
                                    $course_button = JText::_('CONTINUE');
                                    $background = '';
                                }
                                $cat_id = $curso['cat_id'];
                                $cat_slug = JFilterOutput::stringURLSafe($curso['cat_name']);
                                $course_slug = JFilterOutput::stringURLSafe($curso['fullname']);
                                $course_image = $curso['filepath'] . $curso['filename'];

                                (array_key_exists('enrol_time', $curso) && !is_null($curso['enrol_time'])) ? $time = date('d/m/Y', $curso['enrol_time']) : JText::_('UNKNOWN');
                                $completion_des = JText::_('SUBCRIBED') . ': ' . $time;
                                $course_button = JText::_('START_LEARNING');

                                ?>
                                <div class="swiper-slide">
                                    <div class="box_learning <?php echo $class_tablet; ?>" style="<?php echo $color ?>">
                                        <div class="course_content">
                                            <div class="clearfix"></div>
                                            <div class="mylearning-img">
                                                <a href="<?php echo $url; ?>">
                                                    <div id="images" class="left lazyload" data-src="<?php echo $course_image.'?'.(time()*1000); ?>"></div>
                                                </a>
                                                <div class="<?php echo $class_notif?>">
                                                  <span >
                                                        <?php 
                                                        echo $notification;
                                                        ?>
                                                    </span>
                                            </div>
                                            </div>
                                            <div class="content_c" <?php echo $background; ?>>
                                                <div class="clearfix"></div>
                                                <div class="course_title">
                                                    <?php echo "<a class=\" $class_a\" href=\"$url\">" . $curso['fullname'] . "</a>"; ?>
                                                </div>
                                                <div class="right">
                                                    <!-- <div class="courseDetails">
                                                        <?php                                                  
                                                        $userid = JUserHelper::getUserId($curso['creator']);
                                                        $user_owner = JFactory::getUser($userid); 
                                                        if ($curso['count_learner'] && $curso['count_learner'] > 1) {
                                                            echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBERS') . '</span></br>';
                                                        } else {
                                                            echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBER') . '</span></br>';
                                                        }
                                                        $time_view = '';
                                                        if ($curso['timepublish'] && $curso['timepublish'] > 0) {
                                                            $time_published = $curso['timepublish']; 
                                                            $date2 = JFactory::getDate($time_published);
                                                            $time_view = CTimeHelper::timeLapseNew($date2, false);
                                                        }
                                                        if ($time_view != '') {
                                                            echo number_format((float)$curso['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                                        } else {
                                                            echo number_format((float)$curso['duration'], 2, '.', '') . ' hr';
                                                        }
                                                        ?>
                                                    </div> -->
                                                    <p class = "<?php echo $class_p;?>"><?php echo $curso['summary'];//custom_echo($curso['summary'], 80); ?></p>
                                                    </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $i++;
                            endforeach;
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            <?php if ($not_yet_started != 0){?>
                <div class="swiper-button-next2"><i class="bg-second-img icon-next-black"></i></div>
                <div class="swiper-button-prev2"><i class="bg-second-img icon-prev-black"></i></div>
            <?php } ?>
            </div><!--end div list_not_yet_started-->
            <div class="list_completed">
                <div class="swiper-container">
                    <?php 
                     $completed = 0;
                     foreach ($this->completed as $curso){
                        $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                        if(!$publish_group[0]) continue;
                        $completed ++;
                     }
                    if ($completed != 0) { ?>
                            <span class="block-title"><?php echo JText::_('COMPLETED'); ?></span>
                            <span class="count_course">
                                <?php echo $completed; ?></span>
                    <?php } else { ?>
                        <span class="block-title"><?php echo JText::_('COMPLETED'); ?> </span>
                    <?php } ?>
                    <?php if (!is_array($this->completed) || empty($this->completed)) {
                        echo '<span class="bold cred tcenter bg">' . JText::_('NO_COURSE_FOUND') . '.</span>';
                    } else {
                        $i = 0;
                        ?>
                        <div class="swiper-wrapper">
                            <?php
                            $display = "";
                            foreach ($this->completed as $curso) :
                                    // $curso['fullname'] = custom_echo($curso['fullname'], 26);
                                $notification = My_Groups::groupActivities($curso['course_group']);
                            $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                            if(!$publish_group[0]) continue;
                            if ($notification != 0) {
                                    $class_notif = "notification";
                                }
                                else   $class_notif = "notiflearn";
                                    $class_a = "";
                                    $class_p = "";
                                    if(strlen($curso['fullname']) > 44){
                                        $class_a = "limited";
                                    }
                                     if(strlen($curso['summary'])>44){
                                        $class_p= "limited";
                                    }
                                if ($curso['coursetype'] == 'program') {
                                    $programid = $curso['cat_id'];
                                    $url = JURI::base() . 'courseprogram/' . $programid . '.html';
                                    $url_course = JURI::base() . 'courseprogram/' . $programid . '.html';
                                    $course_button = JText::_('VIEW_PROGRAM_COMPLETION');
                                    $button_com = "course_button_pro";
                                    $color = "";
                                    $background = 'style="background:#ccc !important;"';
                                } else {
                                    $display = "display:none;";
                                    $course_id = $curso['remoteid'];
                                    $url_course = JURI::base() . 'course/' . $course_id . '.html';
                                    $course_button = JText::_('VIEW_CERTIFICATE');
                                    $button_com = "course_button";
                                    $color = "background:#DCDCDC";
                                    $background = '';
                                }
                                $cat_id = $curso['cat_id'];
                                $cat_slug = JFilterOutput::stringURLSafe($curso['cat_name']);
                                $course_slug = JFilterOutput::stringURLSafe($curso['fullname']);
                                $course_image = $curso['filepath'] . $curso['filename'];

                                (array_key_exists('time_completed', $curso) && !is_null($curso['time_completed'])) ? $time = date('d/m/Y', $curso['time_completed']) : JText::_('UNKNOWN');
                                $completion_des = JText::_('COMPLETED') . ': ' . $time;

                                ?>
                                <div class="swiper-slide">
                                    <div class="box_learning <?php echo $class_tablet; ?>" style="<?php echo $color ?>">
                                        <div class="course_content">
                                            <div class="clearfix"></div>
                                            <div class="mylearning-img">
                                                <a href="<?php echo $url_course; ?>">
                                                    <div id="images" class="left lazyload" data-src="<?php echo $course_image.'?'.(time()*1000); ?>"></div>
                                                </a>
                                                <div class="<?php echo $class_notif?>">
                                                  <span >
                                                        <?php 
                                                        echo $notification;
                                                        ?>
                                                    </span>
                                            </div>
                                            </div>
                                            <div class="content_c" <?php echo $background; ?>>
                                                <div class="clearfix"></div>
                                                <div class="course_title">
                                                        <?php echo "<a class=\" $class_a\" href=\"$url_course\">" . $curso['fullname'] . "</a>"; ?>
                                                </div>
                                                <div class="right">
                                                       <!--  <div class="courseDetails">
                                                        <?php                                                  
                                                        $userid = JUserHelper::getUserId($curso['creator']);
                                                        $user_owner = JFactory::getUser($userid); 
                                                        if ($curso['count_learner'] && $curso['count_learner'] > 1) {
                                                            echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBERS') . '</span></br>';
                                                        } else {
                                                            echo '<span>'.$user_owner->name . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBER') . '</span></br>';
                                                        }
                                                        $time_view = '';
                                                        if ($curso['timepublish'] && $curso['timepublish'] > 0) {
                                                            $time_published = $curso['timepublish']; 
                                                            $date2 = JFactory::getDate($time_published);
                                                            $time_view = CTimeHelper::timeLapseNew($date2, false);
                                                        }
                                                        if ($time_view != '') {
                                                            echo number_format((float)$curso['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                                        } else {
                                                            echo number_format((float)$curso['duration'], 2, '.', '') . ' hr';
                                                        }
                                                        ?>
                                                        </div> -->
                                                        <p class = "<?php echo $class_p;?>"><?php echo $curso['summary'];//custom_echo($curso['summary'], 80); ?></p>
                                                    </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $i++;
                            endforeach;
                            ?>
                        </div>
                        <?php
                    }
                    ?>

                </div>

                 <?php if ($completed != 0){?>
                     <div class="swiper-button-next3"><i class="bg-second-img icon-next-black"></i></div>
                     <div class="swiper-button-prev3"><i class="bg-second-img icon-prev-black"></i></div>

            <?php }?>
            </div><!--end div list_completed-->
            <div class="list_facifitator">
                    <div class="swiper-container">
                        <?php 
                        $course_facifitator = 0;
                        foreach ($this->course_facifitator as $curso){
                           $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                           if(!$publish_group[0]) continue;
                           $course_facifitator ++;
                        }
                        if ($course_facifitator != 0) { ?>
                            <span class="block-title"><?php echo JText::_('FACIFITATOR'); ?></span>
                            <span class="count_course">
                                <?php echo $course_facifitator; ?></span>
                        <?php } else { ?>
                            <span class="block-title"><?php echo JText::_('FACIFITATOR'); ?> </span>
                        <?php } ?>
                        <?php if (!is_array($this->course_facifitator) || empty($this->course_facifitator)) {
                            echo '<span class="bold cred tcenter bg">' . JText::_('NO_COURSE_FOUND') . '.</span>';
                        } else {
                            $i = 0;
                            ?>
                            <div class="swiper-wrapper">
                                <?php
                                $display = "";
                                foreach ($this->course_facifitator as $curso) :
                                    // $curso['fullname'] = custom_echo($curso['fullname'], 26);
                                    $notification = My_Groups::groupActivities($curso['course_group']);
                                 $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['remoteid']));
                                if(!$publish_group[0]) continue;
                                if ($notification != 0) {
                                    $class_notif = "notification";
                                }
                                else   $class_notif = "notiflearn";
                                    $class_a = "";
                                    $class_p = "";
                                    if(strlen($curso['fullname']) > 44){
                                        $class_a = "limited";
                                    }
                                     if(strlen($curso['summary'])>44){
                                        $class_p= "limited";
                                    }

                                    if ($curso['coursetype'] == 'program') {
                                        $programid = $curso['cat_id'];
                                        $url = JURI::base() . 'courseprogram/' . $programid . '.html';
                                        $url_course = JURI::base() . 'courseprogram/' . $programid . '.html';
                                        $course_button = JText::_('VIEW_PROGRAM_COMPLETION');
                                        $button_com = "course_button_pro";
                                        $color = "";
                                        $background = 'style="background:#ccc !important;"';
                                    } else {
                                        $display = "display:none;";
                                        $course_id = $curso['remoteid'];
                                        $url_course = JURI::base() . 'course/' . $course_id . '.html';
                                        $course_button = JText::_('VIEW_CERTIFICATE');
                                        $button_com = "course_button";
                                        $color = "background:#DCDCDC";
                                        $background = '';
                                    }
                                    $cat_id = $curso['cat_id'];
                                    $cat_slug = JFilterOutput::stringURLSafe($curso['cat_name']);
                                    $course_slug = JFilterOutput::stringURLSafe($curso['fullname']);
                                    $course_image = $curso['filepath'] . $curso['filename'];

                                    (array_key_exists('time_completed', $curso) && !is_null($curso['time_completed'])) ? $time = date('d/m/Y', $curso['time_completed']) : JText::_('UNKNOWN');
                                    $completion_des = JText::_('COMPLETED') . ': ' . $time;

                                    ?>
                                    <div class="swiper-slide">
                                        <div class="box_learning <?php echo $class_tablet; ?>" style="<?php echo $color ?>">
                                            <div class="course_content">
                                                <div class="clearfix"></div>
                                                <div class="mylearning-img">
                                                    <a href="<?php echo $url_course; ?>">
                                                        <div id="images" class="left lazyload" data-src="<?php echo $course_image.'?'.(time()*1000); ?>"></div>
                                                    </a>
                                                    <div class="<?php echo $class_notif?>">
                                                  <span >
                                                        <?php 
                                                        echo $notification;
                                                        ?>
                                                    </span>
        </div>
                                           </div>
                                                <div class="content_c" <?php echo $background; ?>>
                                                    <div class="clearfix"></div>
                                                    <div class="course_title">
                                                        <?php echo "<a class=\" $class_a\" href=\"$url_course\">" . $curso['fullname'] . "</a>"; ?>
                                                    </div>
                                                    <div class="right">
                                                       <!--  <div class="courseDetails">
                                                            <?php

                                                            if ($curso['count_learner'] && $curso['count_learner'] > 1) {
                                                                echo '<span>'.$curso['creator'] . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBERS') . '</span></br>';
                                                            } else {
                                                                echo '<span>'.$curso['creator'] . ' <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $curso['count_learner'] . ' ' . JText::_('COM_JOOMDLE_COURSE_SUBSCRIBER') . '</span></br>';
                                                            }
                                                            $time_view = '';
                                                            if ($curso['timepublish'] && $curso['timepublish'] > 0) {
                                                                $time_published = $curso['timepublish'];
                                                                $date2 = JFactory::getDate($time_published);
                                                                $time_view = CTimeHelper::timeLapseNew($date2, false);
                                                            }
                                                            if ($time_view != '') {
                                                                echo number_format((float)$curso['duration'], 2, '.', '') . ' hr <img class="icon-dots" src="/media/joomdle/images/icon/ManageCourses/30x30_dotdot.png"/> ' . $time_view;
                                                            } else {
                                                                echo number_format((float)$curso['duration'], 2, '.', '') . ' hr';
                                                            }
                                                            ?>
                                                        </div> -->
                                                        <p  class = "<?php echo $class_p;?>" ><?php echo $curso['summary'];//custom_echo($curso['summary'], 80); ?></p>
    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $i++;
                                endforeach;
                                ?>
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                    <?php if ($course_facifitator != 0){?>
                        <div class="swiper-button-next4"><i class="bg-second-img icon-next-black"></i></div>
                        <div class="swiper-button-prev4"><i class="bg-second-img icon-prev-black"></i></div>
                    <?php }?>
            </div><!--end div couse facifitator-->
            <?php } ?>
        </div>

    </div>
    <script type="text/javascript">
        (function($) {
            var title_p = 'Learning';
            $('.navbar-header .navbar-title span').html(title_p);
            $('.joomdle-mylearning-status > div').click(function () {
                $('.joomdle-mylearning-status > div').removeClass('active');
                $(this).addClass('active');
                var title = $(this).text();
                $('.navbar-header .navbar-title span').html(title);
                if ($(this).hasClass('recently-accessed')) {
                    $('.joomdle-mylearning .joomdle_mylearning > div').removeClass('active');
                    $('.joomdle-mylearning .joomdle_mylearning > .list_recently_accessed').addClass('active');
                }
                if ($(this).hasClass('not-yet-started')) {
                    $('.joomdle-mylearning .joomdle_mylearning > div').removeClass('active');
                    $('.joomdle-mylearning .joomdle_mylearning > .list_not_yet_started').addClass('active');
                }
                if ($(this).hasClass('completed')) {
                    $('.joomdle-mylearning .joomdle_mylearning > div').removeClass('active');
                    $('.joomdle-mylearning .joomdle_mylearning > .list_completed').addClass('active');
                }
            });
        })(jQuery);

    </script>
    <?php
}
?>
<?php
if ($device == 'mobile') {
    $slideview = '1.1';
    $space = '10';
} else if ($device == 'tablet') {
    $slideview = '2.5';
    $space = '15';
} else {
    $slideview = '3';
    $space = '15';
}

if ($device == 'mobile'){
    $touch = ' allowTouchMove : true,';
}
else  {$touch = ' allowTouchMove : false,noSwipingClass : \'swiper-slide\',
            noswipingClass: \'swiper-slide\',';}
?>

<?php if (!$this->useSample) { ?>
<script type="text/javascript">
    (function($) {
        var x = screen.width;
        var a = 0;
//        console.log(x);
         if(x < 390){
            a = 1;
        }
        if(x >= 390 && x<=736){
            a = 1.5;
        }
        if(x>736 && x<=1366){
            a = 2;
        }
        if(x>1366){
            a = 3;
        }
      
        var swiper = new Swiper('.list_recently_accessed .swiper-container', {
            <?php echo $touch ; ?>
            pagination: '.my-course .swiper-pagination',
            slidesPerView: a,
            spaceBetween: <?php echo $space; ?>,
            // pagination: '.swiper-pagination',
            nextButton: '.swiper-button-next1',
            prevButton: '.swiper-button-prev1',
        });
         var swiper = new Swiper('.list_not_yet_started .swiper-container', {
            <?php echo $touch ; ?>
             pagination: '.my-course .swiper-pagination',
            slidesPerView: a,
            spaceBetween: <?php echo $space; ?>,
            // pagination: '.swiper-pagination',
            nextButton: '.swiper-button-next2',
            prevButton: '.swiper-button-prev2',
        });
          var swiper = new Swiper('.list_completed .swiper-container', {
            <?php echo $touch ; ?>
             pagination: '.my-course .swiper-pagination',
            slidesPerView: a,
            spaceBetween: <?php echo $space; ?>,
            // pagination: '.swiper-pagination',
            nextButton: '.swiper-button-next3',
            prevButton: '.swiper-button-prev3',
        });
         var swiper = new Swiper('.list_facifitator .swiper-container', {
            <?php echo $touch ; ?>
         pagination: '.my-course .swiper-pagination',
        slidesPerView: a,
        spaceBetween: <?php echo $space; ?>,
        // pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next4',
        prevButton: '.swiper-button-prev4',
        });
     // $('.list_recently_accessed .swiper-button-next').on('click', function () {
     //     swiper.navigation.nextEl;
     // });
    })(jQuery);
</script>
<?php } else { ?>
<script type="text/javascript">
    (function($) {
        $.ajax({
            url: '<?php echo JUri::base().'index.php?option=com_joomdle&task=ajaxShowLearningDefaultPage'?>',
            type: 'post',
            success: function(res) {
                res = JSON.parse(res);
                if (res.success) {
                    $('.joomdle-mylearning .placeholder-loading').fadeOut(0, function() {
                        var a = $(this).parents('.joomdle-mylearning');
                        a.hide();
                        a.after(res.html);
                        a.remove();
                        lazyload();
                    });
                } else {
                    $('.joomdle-mylearning').html("Error").fadeIn(0);
                }
            }
        });
    })(jQuery);
</script>
<?php } ?>