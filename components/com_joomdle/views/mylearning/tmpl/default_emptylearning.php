<?php
$session = JFactory::getSession();
$device = $session->get('device');
$document = JFactory::getDocument();
require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
JLoader::import('joomla.user.helper');

?>
<div class="emptyLearning">
    <img src="/images/MyLearning.png">
    <p class="description"><?php echo JText::_("COM_JOOMDLE_EMPTY_LEARNING_DESCRIPTION"); ?></p>
    <button id="emptyLeanrningSearchCourse"><?php echo JText::_("COM_JOOMDLE_EMPTY_LEARNING_BROWSE_COURSE"); ?></button>

    <script type="text/javascript">
        (function ($) {
            var check_device = "<?php echo $device; ?>";
            $("#emptyLeanrningSearchCourse").click(function () {
                $(".search-all-type").css("display", "none");
                $("#searchcircle").css("display", "none");
                $("#searchfriend").css("display", "none");
                $(".navbar-menu-icon").css("display", "none");
                $(".navbar-search-all").addClass("hide");
                $(".navbar-notification").addClass("hide");
                $(".search-all-type-form").css("display", "block");
                $("#searchlearning").css("display", "inline-block");
                $("#searchlearning .search-text").focus();
                if (check_device == 'mobile') {
                    $(".navbar-brand").hide();
                    $('.navbar-toggle').hide();
                }
            });

        })(jQuery);
    </script>
</div>
