<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewCertificateCreate extends JViewLegacy {
    function display($tpl = null) {
        $app = JFactory::getApplication();
        $pathway = $app->getPathWay();
        $menus = $app->getMenu();
        $menu = $menus->getActive();
        $params = $app->getParams();
        $this->assignRef('params', $params);
        $user = JFactory::getUser();
        $document = JFactory::getDocument();
        $username = $user->username;

        $courseid = JRequest::getVar( 'course_id' , null, 'NEWURLFORM');
        $courseid = (int) $courseid;

        $this->hasPermission = JFactory::hasPermission($courseid, $username);
        if (!$this->hasPermission[0]['hasPermission']) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//            return;
        }

        $act =  JRequest::getVar( 'action', null, 'NEWURLFORM' );

        if (!$act) $act =  JRequest::getVar( 'action' );
        switch ($act) {
            case 'add':
                $config = new JConfig();
                $courseid = JRequest::getVar( 'course_id' , null, 'NEWURLFORM');
                if (!$courseid) $courseid = JRequest::getVar( 'course_id' );

                $dir = $config->dataroot . '/certificatetemp/pix/borders/'.$courseid;
                $files_folder = scandir($dir);
                $this->count = count($files_folder);

                if ($params->get('use_new_performance_method'))
                    $this->course_info = JHelperLGT::getCourseInfo(['id' => $courseid, 'username' => $username]);
                else
                    $this->course_info = JoomdleHelperContent::getCourseInfo($courseid, $user->username);

                $document->setTitle(JText::_('COM_JOOMDLE_CERTIFICATE_UPLOAD_TEMPLATE'));
                parent::display('add');
                break;
            case 'detail':
                $config = new JConfig();
                $courseid = JRequest::getVar( 'course_id' , null, 'NEWURLFORM');
                if (!$courseid) $courseid = JRequest::getVar( 'course_id' );

                if ($params->get('use_new_performance_method'))
                    $this->course_info = JHelperLGT::getCourseInfo(['id' => $courseid, 'username' => $username]);
                else
                    $this->course_info = JoomdleHelperContent::getCourseInfo($courseid, $user->username);

                $name = JRequest::getVar( 'template' , null, 'NEWURLFORM');
                if (!$name) $name = JRequest::getVar( 'template' );
                $defalt_cer_img = 'parenthesis_certificate_default.png';
                $defalt_cer_name = 'parenthesis_certificate_default';
                $this->courseid = $courseid;

                //get certificate default template
                if ($params->get('use_new_performance_method'))
                    $this->template_default = JHelperLGT::getCertificateTemplateDefault($courseid);
                else
                    $this->template_default = JoomdleHelperContent::call_method ( 'get_certificate_template_default', (int) $courseid);

                $this->username = $username;

                // setup and get default certificate
                if (!is_dir($config->dataroot . '/certificatetemp')) {
                    mkdir($config->dataroot . '/certificatetemp', 0777);
                }
                if (!is_dir($config->dataroot . '/certificatetemp/pix')) {
                    mkdir($config->dataroot . '/certificatetemp/pix', 0777);
                }
                if (!is_dir($config->dataroot . '/certificatetemp/pix/borders')) {
                    mkdir($config->dataroot . '/certificatetemp/pix/borders', 0777);
                }
                if (!is_dir($config->dataroot . '/certificatetemp/pix/borders/' . $courseid)) {
                    mkdir($config->dataroot . '/certificatetemp/pix/borders/' . $courseid, 0777);
                    //                                        copy($config->dirmoodle.'/mod/certificate/pix/borders/parenthesis_certificate_default.png', $config->dataroot .'/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.png');
                    //                                        chmod($config->sitedataroot . '/moodle/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.png',0700);
                }
                if (!file_exists($config->dataroot .'/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.png') && !file_exists($config->dataroot .'/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.jpg')) {
                    copy($config->dirmoodle.'/mod/certificate/pix/borders/parenthesis_certificate_default.png', $config->dataroot .'/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.png');
                }

                $dir    = $config->dataroot . '/certificatetemp/pix/borders/'.$courseid;
                $files_folder = scandir($dir);
                $count_file = count($files_folder);

                $supportedtypes = array( '.jpg',  '.png','.jpeg');
                $count_type = count($supportedtypes);
                for ($i=0; $i < $count_file; $i++) {
                    for ($j=0; $j < $count_type; $j++) {
                        if (strlen(strstr($files_folder[$i],$supportedtypes[$j])) > 0 ) {
                            $ext = explode('.', $files_folder[$i]);
                            if($name == $ext[0]) {
                                $current_name = $name.'.'.$ext[1];
                            }
                        }
                    }
                }
                if($name == str_replace("_", "", $defalt_cer_name)) {
                    if(in_array($defalt_cer_img, $files_folder) !== false) {
                        $this->name = $defalt_cer_img;
                    } else
                        $this->name = $defalt_cer_name.'.jpg';
                } else {
                    /*
                     *  Not use for CR 71
                     *   $this->name = $current_name;
                     */
                    $this->name = $files_folder[2];
                }
                $document->setTitle(JText::_('COM_JOOMDLE_CERTIFICATE'));
                parent::display('detail');
                break;
            default:
                $config = new JConfig();
                $courseid = JRequest::getVar( 'course_id' , null, 'NEWURLFORM');
                if (!$courseid) $courseid = JRequest::getVar( 'course_id' );
                $this->courseid = $courseid;

                if ($params->get('use_new_performance_method'))
                    $this->course_info = JHelperLGT::getCourseInfo(['id' => $courseid, 'username' => $username]);
                else
                    $this->course_info = JoomdleHelperContent::getCourseInfo($courseid, $user->username);

                //get certificate default template
                if ($params->get('use_new_performance_method'))
                    $this->template_default = JHelperLGT::getCertificateTemplateDefault($courseid);
                else
                    $this->template_default = JoomdleHelperContent::call_method ( 'get_certificate_template_default', (int) $courseid);

                $this->username = $username;
                // setup and get default certificate
                if (!is_dir($config->dataroot . '/certificatetemp')) {
                    mkdir($config->dataroot . '/certificatetemp', 0777);
                }
                if (!is_dir($config->dataroot . '/certificatetemp/pix')) {
                    mkdir($config->dataroot . '/certificatetemp/pix', 0777);
                }
                if (!is_dir($config->dataroot . '/certificatetemp/pix/borders')) {
                    mkdir($config->dataroot . '/certificatetemp/pix/borders', 0777);
                }
                if (!is_dir($config->dataroot . '/certificatetemp/pix/borders/' . $courseid)) {
                    mkdir($config->dataroot . '/certificatetemp/pix/borders/' . $courseid, 0777);
//                                        copy($config->dirmoodle.'/mod/certificate/pix/borders/parenthesis_certificate_default.png', $config->dataroot .'/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.png');
//                                        chmod($config->sitedataroot . '/moodle/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.png',0700);
                }
                if (!file_exists($config->dataroot .'/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.png') && !file_exists($config->dataroot .'/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.jpg')) {
                    copy($config->dirmoodle.'/mod/certificate/pix/borders/parenthesis_certificate_default.png', $config->dataroot .'/certificatetemp/pix/borders/'.$courseid.'/parenthesis_certificate_default.png');
                }

                $dir    = $config->dataroot . '/certificatetemp/pix/borders/'.$this->courseid;
                $files_folder = scandir($dir);
                $supportedtypes = array( '.jpg',  '.png','.jpeg');
                $count_type = count($supportedtypes);
                $count_file = count($files_folder);
                $this->templates = array();
                for ($i=0; $i < $count_file; $i++) {
                    for ($j=0; $j < $count_type; $j++) {
                        if (strlen(strstr($files_folder[$i],$supportedtypes[$j])) > 0 ) {
                            array_push($this->templates, $files_folder[$i]);
                        }
                    }
                }
                $document->setTitle(JText::_('COM_JOOMDLE_CERTIFICATE'));
                parent::display($tpl);
                break;
        }

    }



}
?>
