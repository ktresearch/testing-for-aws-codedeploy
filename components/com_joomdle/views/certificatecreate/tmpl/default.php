<?php defined('_JEXEC') or die('Restricted access');
$config = new JConfig();
$session = JFactory::getSession();
$device = $session->get('device');
if ($_POST['name']) {
    if ($this->course_info['course_status'] == 'pending_approval' || $this->course_info['course_status'] == 'approved') {
        echo json_encode('Course is pending approval, You can not delete template.');
        exit();
    }
    $name = $_POST['name'];
    $pos = strrpos($name, ".");
    $a = substr($name, $pos, 10000);
    $name_replace = str_replace($a, '', $name);
    $dir = $config->dataroot . '/certificatetemp/pix/borders/' . $this->courseid . '/';
    $dirHandle = opendir($dir);
    while ($file = readdir($dirHandle)) {
        if ($file == $name) {
            unlink($dir . '/' . $name);
        }
    }
    $files = scandir($config->dataroot . '/certificatetemp/pix/backups/' . $this->courseid . '/');
    $count_file = count($files);
    for ($i = 0; $i < $count_file; $i++) {
        if (strlen(strstr($files[$i], $name_replace)) > 0) {
            unlink($config->dataroot . '/certificatetemp/pix/backups/' . $this->courseid . '/' . $files[$i]);
        }
    }
    echo json_encode('delete file success');
    die;
}
// set default template
if (isset($_POST['action'])) {
    if ($this->course_info['course_status'] == 'pending_approval' || $this->course_info['course_status'] == 'approved') {
        echo json_encode('Course is pending approval, You can not set template for course.');
        exit();
    }
    $tmp_default_id = $this->template_default['template']['cer_id'];
    $tmp_name = $_POST['temp_default'];
    $setDefault = JoomdleHelperContent::call_method('set_default_certificate_template', (int)$tmp_default_id, (int)$this->courseid, $tmp_name, $this->username);
    if ($setDefault) {
        echo json_encode('Certificate template have been set');
        exit();
    }
}

?>
<link rel="stylesheet"
      href="<?php echo JURI::base() . 'components/com_joomdle/views/certificatecreate/tmpl/css/certificate.css'; ?>">
<?php if ($this->course_info['course_status'] != 'pending_approval' && $this->course_info['course_status'] != 'approved'): ?>
<div class="list-certificate">
    <div class="buttons">
        <button class="btCreateCertificate">+</button>
    </div>
    <?php endif; ?>
    <div class="title-certificate">
    <center><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_ADD'); ?></center>
    </div>
    <div class="tip">
    <center><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_CHOOSE_TEMPLATE_DES'); ?></center>
    </div>
    <div class="item-certificate">
    <div class="row">
        <?php
        $this->templates = array_reverse($this->templates);
        $this->templates = array_reverse($this->templates, true);
        $t = count($this->templates);
        for ($i = 0; $i < $t; $i++) {
            $cut_name = explode('.', $this->templates[$i]);
            $cut_name_str = str_replace("_", "", $cut_name[0]);
            $template_default = $this->template_default['template']['cer_name'];
            $class_default = '';
            $default = false;
            if ($this->templates[$i] == $template_default) {
                $class_default = 'default';
                $default = true;
            }
            ?>

            <div class="col-md-3 item">
                <a href="<?php echo JURI::base() . "certificatetemplate/detail_" . $this->courseid . "_" . $cut_name_str . ".html"; ?> ">
                    <img class="certificate-detail <?php echo $class_default; ?>"
                             src="<?php echo $config->wwwrootfile . '/certificatetemp/pix/borders/' . $this->courseid . '/' . $this->templates[$i].'?'.(time()*1000); ?>"/>
                </a>
                <?php if (!$default && ($this->course_info['course_status'] != 'pending_approval' && $this->course_info['course_status'] != 'approved')) { ?>
                    <button class="setDefault"
                            data="<?php echo $this->templates[$i]; ?>"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_SET_DEFAULT'); ?></button>
                <?php } ?>
                <?php if ($this->templates[$i] != 'parenthesis_certificate_default.png' && !$default) { ?>
                    <b class="rm" data='<?php echo $this->templates[$i]; ?>'><img src="../images/deleteIcon.png"></b>
                <?php } ?>
            </div>

        <?php } ?>

    </div>
    </div>
    <div class="popupFull col-md-4 col-md-offset-3 col-xs-12">
    <center><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_CHOOSE_TEMPLATE_INTRO'); ?></center>
    </div>
    <div class="hihi"
         style="position : fixed;top:0px;left:0px;width:100%;height:100%;background:#ccc;z-index:1000000;opacity:0.3;display:none;"></div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function (e) {
        var st = parseInt(<?php echo count($this->templates); ?>);
        jQuery('.rm').click(function () {
            var y = jQuery(this);
            var t = jQuery(this).attr('data');
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: {name: t,},
                success: function (res) {
                    y.parent().remove();
                    st--;
                },
            });
        });
        jQuery('.btCreateCertificate').click(function (e) {
            if (st >= 5) {
                jQuery('.popupFull').show();
                jQuery('.hihi').show();
                e.stopPropagation();
            } else {
                window.location.href = '<?php echo JURI::base() . "certificatetemplate/add_" . $this->courseid . ".html";?>';
            }
        });
    });
    jQuery('.hihi').click(function () {
        jQuery(".hihi").hide();
        jQuery(".popupFull").hide();
    });
    // set default template
    jQuery('.setDefault').click(function () {
        var temp_default = jQuery(this).attr('data');
        var action = 'set_deafult';
        console.log(temp_default);
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: {temp_default: temp_default, action: action},
            success: function (data) {
                location.reload();
            }
        });
    });

</script>