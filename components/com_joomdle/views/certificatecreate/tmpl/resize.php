<?php
 
 class SimpleImage {
 
   var $image;
   var $image_type;
 
   function load($filename) {
 
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
      if( $this->image_type == IMAGETYPE_JPEG ) {
 
         $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
 
         $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
 
         $this->image = imagecreatefrompng($filename);
      }
   }
   function save($filename, $image_type, $compression=75, $permissions=null) {
 
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image,$filename,$compression);
      } elseif( $image_type == IMAGETYPE_GIF ) {
 
         imagegif($this->image,$filename);
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image,$filename);
      }
      if( $permissions != null) {
 
         chmod($filename,$permissions);
      }
   }
   function output($image_type=IMAGETYPE_JPEG) {
 
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
 
         imagegif($this->image);
      } elseif( $image_type == IMAGETYPE_PNG ) {
 
         imagepng($this->image);
      }
   }
   function getWidth() {
 
      return imagesx($this->image);
   }
   function getHeight() {
 
      return imagesy($this->image);
   }
   function resizeToHeight($height) {
 
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
 
   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
   }
 
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100;
      $this->resize($width,$height);
   }
 
   function resize($width,$height, $to = NULL) {
       $original_width = 1020;
       $original_height = 300;
       $ratio = $width / $height;
       $thumb_ratio = $original_width / $original_height;
      // check image landscape or portrait
      if($ratio >= $thumb_ratio) {
          // landscape image
          $new_height = $original_height;
          $new_width = $width / ($height / $original_height);
      } else {
          $new_width = $original_width;
          $new_height = $height / ($width / $original_width);
      }
      
      $new_image = imagecreatetruecolor($original_width, $original_height);
      $whiteBackground = imagecolorallocate($new_image, 255, 255, 255); 
      imagefill($new_image,0,0,$whiteBackground);
      imagecolortransparent($new_image, $whiteBackground);
      imagecopyresampled($new_image, $this->image, 0 - ($new_width - $original_width) / 2, 0 - ($new_height - $original_height) / 2, 0, 0, $new_width, $new_height, $this->getWidth(), $this->getHeight());
//      ImageJpeg ($new_image,null,100);
//      imagepng ($new_image,null);
      $this->image = $new_image;
   }      
 
}
?>