<?php 
defined('_JEXEC') or die('Restricted access');
    $config = new JConfig();
    $courseid = $this->course_info['remoteid'];
    if(isset($_FILES['image']['type'])){
        $path = $_FILES['image']['tmp_name'];
        $temp = explode(".", $_FILES["image"]["name"]);
        
        $file_name = 'PXtemplate'.$courseid.$this->count.'.'.end($temp);
        // create folder if not exits
        if (!is_dir($config->dataroot . '/certificatetemp/pix/borders/' . $courseid)) {
            mkdir($config->dataroot . '/certificatetemp/pix/borders/' . $courseid, 0755);
        }
        $newPath     = $config->dataroot.'/certificatetemp/pix/borders/'.$courseid.'/'.$file_name;
        move_uploaded_file($path, $newPath);
        echo json_encode('success');die;
    }
?>
<link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/certificatecreate/tmpl/css/certificate.css';?>">
<link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/certificatecreate/tmpl/css/jquery-confirm.css';?>">
<script type="text/javascript" src="<?php echo JURI::base().'components/com_joomdle/views/certificatecreate/tmpl/js/jquery-confirm.js';?>"></script>

<!--<style>-->
<!--    .com_joomdle .home {-->
<!--        display: none;-->
<!--    }-->
<!---->
<!--</style>-->
<div class="joomdle-certificatecreate">

<div class="title-certificate" style="margin-bottom:15px;"><center><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_UPLOAD_TEMPLATE'); ?></center></div>
<div class="col-xs-10 col-xs-offset-1">
<div class="item-certificate-default col-md-4 col-md-offset-4">
    <i class="glyphicon glyphicon-picture"></i>
</div>
<div class="alan col-md-4 col-md-offset-4">
    <img id="output"/>
</div>
</div>
<div class="buttons">
    <button class="uploadCertificate"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_UPLOAD_CERTIFICATE'); ?></button>
</div>
<div class="buttons">
    <button class="btnSave"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_BUTTON_SAVE'); ?> <img class="ajax-load" style="margin-left:7px;margin-top:-5px;display:none;" width="15" height="15" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/load.gif';?>"></button>
    <a href="<?php echo JURI::base()."certificatetemplate/list_".$courseid.".html";?> "><button class="btnCancel"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_BUTTON_CANCEL'); ?></button></a>
</div>
<form method="post" class="uploadFile" action="" enctype='multipart/form-data' style="display:none;">
   <input name="image" class="file-certificate" id="toan" type="file" accept="image/*" onchange="loadFile(event)">
</form>
</div>
<script>
    jQuery(document).ready(function(){

    });
    function loadFile(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
        jQuery('.item-certificate-default').hide();
        jQuery('#output').show();
    };
    jQuery('.uploadCertificate').click(function(){
        jQuery('.file-certificate').click();
    });
    jQuery('.btnSave').click( function(){
        var file = jQuery('.file-certificate')[0].files[0];
        if(file === undefined) {
            jQuery.alert({
                title: 'Alert!',
                content: "File empty",
                columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
                theme: 'blue',
                title: false,
            });
        }
        else if(file.type != 'image/png' && file.type != 'image/jpg'  && file.type != 'image/jpeg' ) {
            jQuery.alert({
                title: 'Alert!',
                content: "The file does not match images.",
                columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
                theme: 'blue',
                title: false,
            });
        }else{
            jQuery('.uploadFile').submit();
        };
    });
    jQuery('.uploadFile').on('submit', function(event){
        var file = jQuery('.file-certificate')[0].files[0];
        event.stopPropagation();
        event.preventDefault();
        jQuery.ajax({
            url : window.location.href,
            type : 'POST',
            data : new FormData(this),
            contentType: false,       
            cache: false,          
            processData:false,
            beforeSend: function () {
                jQuery('.btnSave').attr('disabled',true);
                jQuery('.ajax-load').show();
            },
            complete: function () {
                jQuery('.btnSave').attr('disabled',true);
                jQuery('.ajax-load').hide();
            },
            success : function(res){
                jQuery('.btnSave').attr('disabled',true);
                window.location.href = '<?php echo JURI::base()."certificatetemplate/list_".$courseid.".html";?>';
            }
        });
    });
    jQuery('.item-certificate-default').click(function(){
        jQuery('.file-certificate').click();
    });
    </script>