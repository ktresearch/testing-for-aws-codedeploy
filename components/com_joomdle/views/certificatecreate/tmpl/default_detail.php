<?php defined('_JEXEC') or die('Restricted access');
include('resize.php');
$session = JFactory::getSession();
$device = $session->get('device');
require_once(JPATH_SITE . '/components/com_community/libraries/core.php');
require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$document = JFactory::getDocument();
$banner = new HeaderBanner(new JoomdleBanner);

$render_banner = $banner->renderBanner($this->course_info, $this->mods['sections'], $role);
$config = new JConfig();
$name = $this->name;
$pos = strrpos($name, ".");
$a = substr($name, $pos, 10000);
$name_replace = str_replace($a, '', $name);

if (isset($_FILES['logo']['type'])) {
    if ($this->course_info['course_status'] == 'pending_approval' || $this->course_info['course_status'] == 'approved' || $this->course_info['course_status'] == 'published') {
        echo json_encode('Course is pending approval, You can not add logo to tempalte.');
        exit();
    }
    require_once(JPATH_SITE.'/components/com_joomdle/views/certificatecreate/tmpl/resize.php');
    if (!is_dir($config->dataroot . '/certificatetemp/pix/seals')) {
        mkdir($config->dataroot . '/certificatetemp/pix/seals', 0755);
    }
    mkdir($config->dataroot . '/certificatetemp/pix/seals/' . $this->courseid, 0755);
                
    $logo = $_FILES['logo']['tmp_name'];
    $image = new SimpleImage();
    $image->load($logo);
//   	$t = 250/($image->getWidth());
//        $heightlogo = $t*($image->getHeight());
    list($width, $height) = getimagesize($logo);
    $image->resize($width, $height);
//        $image->resize(250,$heightlogo,$config->dataroot . '/certificatetemp/pix/seals/'. $this->courseid . '/logo.jpg');
    $image->save($config->dataroot . '/certificatetemp/pix/seals/'. $this->courseid . '/logo.png', IMAGETYPE_PNG);
    if (!is_dir($config->dataroot . '/certificatetemp/pix/backups')) {
        mkdir($config->dataroot . '/certificatetemp/pix/backups', 0755);
    }
    mkdir($config->dataroot . '/certificatetemp/pix/backups/' . $this->courseid, 0755);
    copy($config->dataroot . '/certificatetemp/pix/borders/' . $this->courseid . '/' . $name, $config->dataroot . '/certificatetemp/pix/backups/' . $this->courseid . '/' . $name);

    merge($config->dataroot . '/certificatetemp/pix/borders/' . $this->courseid . '/' . $name, $config->dataroot . '/certificatetemp/pix/seals/'. $this->courseid . '/logo.png', $config->dataroot . '/certificatetemp/pix/borders/' . $this->courseid . '/' . $name_replace . '.png');
    $tmp_default_id = $this->template_default['template']['cer_id'];
    $tmp_name = $name_replace . '.jpg';
    $tmp_name_png = $name_replace . '.png';
    $tmp_default_name = $this->template_default['template']['cer_name'];
//    if($name == $tmp_default_name) {
    $setDefault = JoomdleHelperContent::call_method('set_default_certificate_template', (int)$tmp_default_id, (int)$this->courseid, $tmp_name_png, $this->username);
//    }
    // unlink($config->dirmoodle.'/mod/certificate/pix/seals/logo.jpg');
    $src = $config->wwwrootfile . '/certificatetemp/pix/borders/' . $this->courseid . '/' . $name . '?' . (time() * 1000);
    echo json_encode($src);
    die;
}

if ($_POST['name']) {
    if ($this->course_info['course_status'] == 'pending_approval' || $this->course_info['course_status'] == 'approved' || $this->course_info['course_status'] == 'published') {
        echo json_encode('Course is pending approval, You can not delete logo for template.');
        exit();
    }
    $name_file_old = $_POST['name'];
    $file_old = $config->dataroot . '/certificatetemp/pix/backups/' . $this->courseid . '/' . $name_file_old;
    unlink($config->dataroot . '/certificatetemp/pix/borders/' . $this->courseid . '/' . $name);
    copy($file_old, $config->dataroot . '/certificatetemp/pix/borders/' . $this->courseid . '/' . $name_file_old);
    unlink($config->dataroot . '/certificatetemp/pix/backups/' . $this->courseid . '/' . $name_file_old);
    $tmp_default_name = $this->template_default['template']['cer_name'];
    $tmp_default_id = $this->template_default['template']['cer_id'];

//	if($name == $tmp_default_name) {
    $setDefault = JoomdleHelperContent::call_method('set_default_certificate_template', (int)$tmp_default_id, (int)$this->courseid, $name_file_old, $this->username);
    if ($setDefault) {
        echo json_encode('Certificate template have been set');
        exit();
    }
//	}
    echo json_encode('delete logo success');
    die;
}
// save logo
if (isset($_POST['change']) && isset($_POST['name_change'])) {
    $name_file_old = $_POST['name_change'];
    $file_old = $config->dataroot . '/certificatetemp/pix/backups/' . $this->courseid . '/' . $name_file_old;
    unlink($config->dataroot . '/certificatetemp/pix/borders/' . $this->courseid . '/' . $name);
    copy($file_old, $config->dataroot . '/certificatetemp/pix/borders/' . $this->courseid . '/' . $name_file_old);
    unlink($config->dataroot . '/certificatetemp/pix/backups/' . $this->courseid . '/' . $name_file_old);
    $tmp_default_name = $this->template_default['template']['cer_name'];
    $tmp_default_id = $this->template_default['template']['cer_id'];

//	if($name == $tmp_default_name) {
    $setDefault = JoomdleHelperContent::call_method('set_default_certificate_template', (int)$tmp_default_id, (int)$this->courseid, $name_file_old, $this->username);
    if ($setDefault) {
        echo json_encode('Certificate template have been set');
        exit();
    }
//	}
    echo json_encode('delete logo success');
    die;
}

// ==============================================================
// ==============================================================
$dir = $config->dataroot . '/certificatetemp/pix/backups/' . $this->courseid;
$files_folder = scandir($dir);
$count_file = count($files_folder);
for ($i = 0; $i < $count_file; $i++) {
    if (strlen(strstr($files_folder[$i], $name_replace)) > 0) {
        $exist_logo = 1;
        $img_old = $config->wwwrootfile . '/certificatetemp/pix/backups/' . $this->courseid . '/' . $files_folder[$i];
        $name_old = $files_folder[$i];
    }
}
if (!isset($name_old)) {
    $name_old = 'parenthesis_certificate_default.png';
}
// ==============================================================
// ==============================================================
//print_r($name_old);
if ($device == 'mobile') {
    $position = 'style="left:29.7%;top:55px;"';
} else {
    $position = 'style="left:50%;top:19%;"';
}
$image_background = $config->wwwrootfile . '/certificatetemp/pix/borders/' . $this->courseid . '/' . $name . '?' . (time() * 1000);
?>
<link rel="stylesheet"
      href="<?php echo JURI::base() . 'components/com_joomdle/views/certificatecreate/tmpl/css/certificate.css'; ?>">
<link rel="stylesheet"
      href="<?php echo JURI::base() . 'components/com_joomdle/views/certificatecreate/tmpl/css/jquery-confirm.css'; ?>">
<script type="text/javascript"
        src="<?php echo JURI::base() . 'components/com_joomdle/views/certificatecreate/tmpl/js/jquery-confirm.js'; ?>"></script>

<div class="detail-certificate">
    <div class="title-certificate">
        <center><?php echo JText::_('COM_JOOMDLE_CERTIFICATE'); ?></center>
    </div>
    <div class="tip">
        <center style="color: #828282"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_UPLOAD_LOGO_DEFAULT'); ?>
            <p><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_LOGO_TIP'); ?></p>
        </center>

    </div>
    <div class="item-certificate">
        <div class="row old">
            <div class="col-md-3 item detail-item">
                <?php
                if (!$exist_logo) {
                    ?>
                    <div class="image-placeholder"></div>
                <?php } ?>
                <img id="default"
                     src="<?php echo $config->wwwrootfile . '/certificatetemp/pix/borders/' . $this->courseid . '/' . $name . '?' . (time() * 1000) ?>"/>
            </div>
        </div>
        <div class="row new">
            <div class="col-md-3 item detail-item">
                <img class="img-nen"
                     src="<?php echo $config->wwwrootfile . '/certificatetemp/pix/borders/' . $this->courseid . '/' . $name . '?' . (time() * 1000); ?>"/>
                <img width="100" class="img-logo" id="img_logo" <?php echo $position; ?>>
            </div>
        </div>
    </div>
    <?php if ($exist_logo && ($this->course_info['course_status'] != 'pending_approval' && $this->course_info['course_status'] != 'approved')) { ?>
        <div class="remove-logogo">
            <div class="buttons">
                <button class="btnSaveRemove"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_BUTTON_SAVE'); ?></button>
                <button class="removeLogo"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_BUTTON_REMOVE_LOGO'); ?></button>
                <!-- <button class="uploadCertificate" type="button" style="display: none;"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_BUTTON_ADD_LOGO'); ?></button> -->
                <!--  </div>
                         <div class="buttons" style="display: none;"> -->
                <!-- <span style="font-size:14px;margin-left:30px;"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_LOGO_TIP'); ?></span> -->

                <button class="btnCancel"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_BUTTON_CANCEL'); ?></button>
            </div>
            <!--  <button class="btnCancel" style="display: none;"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_BUTTON_CANCEL'); ?></button> -->
            <input type="hidden" name="change_logo" class="change"/>
            <input type="hidden" class="re-upload"/>
            <form class="uploadLogo" action="" enctype="multipart/form-data" style="display:none;">
                <input type="file" name="logo" class="logosss" accept="image/*" onchange="loadFileLogo(event)">
            </form>
        </div>
    <?php } else {
        ?>
        <div class="add-logogo">
            <?php
            if ($this->course_info['course_status'] != 'pending_approval' && $this->course_info['course_status'] != 'approved' || $this->course_info['course_status'] != 'published') {
                ?>
                <div class="buttons" id="btn-add">
                    <button class="uploadCertificate"
                            type="button"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_BUTTON_ADD_LOGO'); ?></button>
                    <button class="btnSave"
                            style="display: none;"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_BUTTON_SAVE'); ?> <img
                                class="ajax-load" style="margin-left:7px;margin-top:-5px;display:none;" width="15"
                                height="15"
                                src="<?php echo JURI::base() . 'components/com_joomdle/views/assigncreate/tmpl/load.gif'; ?>">
                    </button>

                    <button class="removeLogo" type="button"
                            style="display: none;"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_BUTTON_REMOVE_LOGO'); ?></button>
                    <!--  </div>
                <?php
                    ?>
        <div class="buttons" style="display: none;"> -->

                    <button class="btnCancel"
                            style="display: block;"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_BUTTON_CANCEL'); ?></button>
                </div>
                <?php // } ?>
                <form class="uploadLogo" action="" enctype="multipart/form-data" style="display:none;">
                    <input type="file" name="logo" class="logosss" accept="image/*" onchange="loadFile(event)">
                </form>
            <?php } ?>
            <input type="hidden" id="first_upload"/>
            <!-- <button style="display: none;" class="btnCancel"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE_BUTTON_CANCEL'); ?></button> -->
        </div>

    <?php } ?>
    <div class="notification"></div>
</div>
<?php
function merge($filename_x, $filename_y, $filename_result)
{

    // Get dimensions for specified images
    list($width_x, $height_x) = getimagesize($filename_x);
    list($width_y, $height_y) = getimagesize($filename_y);
    // Create new image with desired dimensions
    $image = imagecreatetruecolor($width_x, $height_x);
    //
    $whiteBackground = imagecolorallocate($image, 255, 255, 255);
    imagefill($image, 50, 50, $whiteBackground);
    //
    if (exif_imagetype($filename_x) == IMAGETYPE_JPEG) {
        $image_x = imagecreatefromjpeg($filename_x);
        if (exif_imagetype($filename_y) == IMAGETYPE_JPEG) {
            $image_y = imagecreatefromjpeg($filename_y);
        } else if (exif_imagetype($filename_y) == IMAGETYPE_PNG) {
            $image_y = imagecreatefrompng($filename_y);
        }
    } else if (exif_imagetype($filename_x) == IMAGETYPE_PNG) {
        $image_x = imagecreatefrompng($filename_x);
        if (exif_imagetype($filename_y) == IMAGETYPE_JPEG) {
            $image_y = imagecreatefromjpeg($filename_y);
        } else if (exif_imagetype($filename_y) == IMAGETYPE_PNG) {
            $image_y = imagecreatefrompng($filename_y);
        }
    }
    imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);
    imagecopy($image, $image_y, $width_x / 3.4, $height_x / 5.4, 0, 0, $width_y, $height_y);
    // Save the resulting image to disk (as JPEG)
    imagepng($image, $filename_result);
    // Clean up

    imagedestroy($image_x);
    imagedestroy($image_y);
    imagedestroy($image);
}

?>
<script type="text/javascript">
    jQuery(document).ready(function (e) {
        jQuery('.add-logogo .uploadCertificate').live('click', function (event) {
            jQuery('.add-logogo .logosss').click();
        });
        jQuery('.remove-logogo .uploadCertificate').live('click', function (event) {
            jQuery('.remove-logogo .logosss').click();
        });
        /*
                 * Change for CR 71. Not use click save button
                 *
                jQuery('.btnSave').click(function(){
            var file = jQuery('.logosss')[0].files[0];
            if(file === undefined) {
                jQuery.alert({
                    title: 'Alert!',
                    content: "File empty",
                    columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
                    theme: 'blue',
                    title: false,
                });
            }else if(file.type != 'image/png') {
                jQuery.alert({
                    title: 'Alert!',
                    content: "The file does not match images.Tip : Upload your own logo in PNG format.",
                    columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
                    theme: 'blue',
                    title: false,
                });
            }else{
                jQuery('.uploadLogo').submit();
            }
        });*/
        jQuery('.add-logogo .uploadLogo').on('submit', function (event) {
            event.stopPropagation();
            event.preventDefault();
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    jQuery('body').addClass('overlay2');
                    jQuery('.notification').html('Uploading...').fadeIn();
                    jQuery('.uploadCertificate').css('display', 'none');
                    jQuery('.buttons').css('display', 'block');
                    jQuery('.btnCancel').css('display', 'block');
                    jQuery('.btnSave').css('display', 'block');
                    jQuery('.removeLogo').css('display', 'block');
                    jQuery('.btnSave').attr('id', 'save_new');
                    jQuery('.btnCancel').attr('id', 'cancel_new');
                    jQuery('#first_upload').attr('value', 1);
                },
                success: function (res) {
                    jQuery('.notification').fadeOut();
                    jQuery('body').removeClass('overlay2');
                    jQuery('.detail-item img.img-logo').hide();
                    jQuery('.image-placeholder').css('display', 'none');
                    window.location.reload(true);
                    jQuery('.detail-item img.img-nen').attr('src', jQuery.parseJSON(res));
                }
            });
        });
        jQuery('.add-logogo .btnSave').click(function () {
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: {save: 'savelogo'},
                beforeSend: function () {
                    jQuery('body').addClass('overlay2');
                    jQuery('.notification').html('Loading...').fadeIn();
                },
                success: function (res) {
                    jQuery('.notification').fadeOut();
//                            window.location.reload(true);
                    window.location.href = '<?php echo JUri::base() . "course/" . $this->courseid . '.html'; ?>';
                }
            });
        });
        // Processing cancel button remove logo
        jQuery('.add-logogo .btnCancel').click(function () {
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    name: 'parenthesis_certificate_default.png',
                },
                beforeSend: function () {
                    jQuery('body').addClass('overlay2');
                    jQuery('.notification').html('Loading...').fadeIn();
                },
                success: function (res) {
                    jQuery('.notification').fadeOut();
                    jQuery('.btnSaveRemove').attr('disabled', true);
//                                    window.location.reload(true);
                    window.location.href = '<?php echo JUri::base() . "course/" . $this->courseid . '.html'; ?>';
                }
            });
        });
        jQuery('.add-logogo .removeLogo').click(function () {
            var first_upload = jQuery('#first_upload').val();
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    name: '<?php echo $name_old; ?>',
                },
                beforeSend: function () {
                    jQuery('body').addClass('overlay2');
                    jQuery('.notification').html('Removing...').fadeIn();
                },
                success: function (res) {
                    jQuery('.notification').fadeOut();
                    jQuery('.btnSaveRemove').attr('disabled', true);
                    jQuery('.buttons').css('display', 'block');
                    jQuery('.btnCancel').css('display', 'block');
                    jQuery('.detail-item img.img-nen').attr('src', '<?php echo $image_background; ?>');
                    jQuery('.removeLogo').css('display', 'none');
                    jQuery('.uploadCertificate').css('display', 'block');
                    jQuery('body').removeClass('overlay2');
                    if (first_upload == 1) {
                        window.location.reload(true);
                    }
                }
            });
        });

    });

    function loadFile(event) {
        var img_logo = document.getElementById('img_logo');
        img_logo.src = URL.createObjectURL(event.target.files[0]);
        jQuery('.old').hide();
        jQuery('.new').show();

        var file = jQuery('.add-logogo .logosss')[0].files[0];
        if (file === undefined) {
            // jQuery.alert({
            //     title: 'Alert!',
            //     content: "File empty",
            //     columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
            //     theme: 'blue',
            //     title: false,
            // });
            lgtCreatePopup('oneButton', {content: "File empty."}, function() {
                lgtRemovePopup();
            });
            jQuery('.add-logogo .logosss').val('');
        } else if (file.type != 'image/png') {
            // jQuery.alert({
            //     title: 'Alert!',
            //     content: "The file does not match images. Note : Upload your own logo in PNG format.",
            //     columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
            //     theme: 'blue',
            //     title: false,
            // });
            lgtCreatePopup('oneButton', {content: "The file does not match images. Note : Upload your own logo in PNG format."}, function() {
                lgtRemovePopup();
            });
            jQuery('.add-logogo .logosss').val('');
            jQuery('.img-nen').before('<div class="image-placeholder"></div>');
            jQuery('.image-placeholder').css('z-index', '1000');
            jQuery('.img-logo').hide();
        } else {
            scale();
            jQuery('.add-logogo .uploadLogo').submit();
        }
    };

    // ===== Part for remove Logo ===//
    function loadFileLogo(event) {
        var img_logo = document.getElementById('img_logo');
        img_logo.src = URL.createObjectURL(event.target.files[0]);
        jQuery('.old').hide();
        jQuery('.new').show();
        var file = jQuery('.remove-logogo .logosss')[0].files[0];
        if (file === undefined) {
            jQuery.alert({
                title: 'Alert!',
                content: "File empty",
                columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
                theme: 'blue',
                title: false,
            });
            lgtCreatePopup('oneButton', {content: "File empty."}, function() {
                lgtRemovePopup();
            });
            jQuery('.remove-logogo .logosss').val('');
        } else if (file.type != 'image/png') {
            // jQuery.alert({
            //     title: 'Alert!',
            //     content: "The file does not match images. Note : Upload your own logo in PNG format.",
            //     columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
            //     theme: 'blue',
            //     title: false,
            // });
            lgtCreatePopup('oneButton', {content: "The file does not match images. Note : Upload your own logo in PNG format."}, function() {
                lgtRemovePopup();
            });
            jQuery('.remove-logogo .logosss').val('');
            jQuery('.img-nen').before('<div class="image-placeholder"></div>');
            jQuery('.image-placeholder').css('z-index', '1000');
            jQuery('.img-logo').hide();
        } else {
            jQuery('.remove-logogo .uploadLogo').submit();
        }
    }

    jQuery('.remove-logogo .uploadLogo').on('submit', function (event) {
        removeOldLogo();
        event.stopPropagation();
        event.preventDefault();
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                jQuery('body').addClass('overlay2');
                jQuery('.notification').html('Uploading...').fadeIn();
                jQuery('.uploadCertificate').css('display', 'none');
                jQuery('.buttons').css('display', 'block');
                jQuery('.btnCancel').css('display', 'block');
                jQuery('body').removeClass('overlay2');
                jQuery('.removeLogo').css('display', 'block');
                jQuery('.btnSave').attr('id', 'save_new');
                jQuery('.btnCancel').attr('id', 'cancel_new');
                jQuery('.re-upload').attr('value', 1);
                jQuery('.image-placeholder').css('display', 'none');
            },
            success: function (res) {
                jQuery('.notification').fadeOut();
                jQuery('.detail-item img.img-nen').attr('src', jQuery.parseJSON(res));
                jQuery('.detail-item img.img-logo').hide();
//	                window.location.reload(true);
            }
        });
    });
    jQuery('.remove-logogo .removeLogo').click(function () {
        removeOldLogo();
        jQuery('.detail-item img').attr('src', '<?php echo $img_old; ?>');
        var ttt = jQuery('.detail-item img').attr('src');
        if (ttt == '<?php echo $config->wwwrootfile . '/certificatetemp/pix/borders/' . $this->courseid . '/' . $name;?>') {
            window.location.href = '<?php echo JURI::base() . "certificatetemplate/list_" . $this->courseid . ".html";?>';
        } else {
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    name: '',
                },
                beforeSend: function () {
                    jQuery('body').addClass('overlay2');
                    jQuery('.notification').html('Removing...').fadeIn();
                },
                success: function (res) {
                    jQuery('.notification').fadeOut();
                    jQuery('.buttons').css('display', 'block');
                    jQuery('.btnCancel').css('display', 'block');
                    jQuery('#default').before('<div class="image-placeholder"></div>');
//                        jQuery('.image-placeholder').css('top', '28%');
//                        jQuery('.image-placeholder').css('left', '46%');
                    jQuery('.detail-item img.img-nen').attr('src', '<?php echo $image_background; ?>');
                    jQuery('.removeLogo').css('display', 'none');
                    jQuery('.change').attr('value', 1);
                    jQuery('.uploadCertificate').css('display', 'block');
                    jQuery('body').removeClass('overlay2');
//	                window.location.reload(true);
                }
            });
        }
    });
    // Processing cancel button remove logo
    jQuery('.remove-logogo .btnCancel').click(function () {
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: {
                name: '',
            },
            beforeSend: function () {
                jQuery('body').addClass('overlay2');
                jQuery('.notification').html('Loading...').fadeIn();
            },
            success: function (res) {
                jQuery('.notification').fadeOut();
                jQuery('.btnSaveRemove').attr('disabled', true);
//	                window.location.reload(true);
                window.location.href = '<?php echo JUri::base() . "course/" . $this->courseid . '.html'; ?>';
            }
        });
    });

    jQuery('.remove-logogo .btnSaveRemove').click(function () {
        var change = jQuery('.change').val();
        var re_upload = jQuery('.re-upload').val();
        if (change == 1) {
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    change: change,
                    name_change: '<?php echo $name_old; ?>'
                },
                beforeSend: function () {
                    jQuery('body').addClass('overlay2');
                    jQuery('.notification').html('Loading...').fadeIn();
                },
                success: function (res) {
                    jQuery('.notification').fadeOut();
                    window.location.href = '<?php echo JUri::base() . "course/" . $this->courseid . '.html'; ?>';
                }
            });
        }
        if (re_upload == 1) {
            window.location.href = '<?php echo JUri::base() . "course/" . $this->courseid . '.html'; ?>';
        }
        else {
            jQuery('body').addClass('overlay2');
            jQuery('.notification').html('Loading...').fadeIn();
            jQuery('.notification').fadeOut();
            window.location.href = '<?php echo JUri::base() . "course/" . $this->courseid . '.html'; ?>';
        }
    });

    function removeOldLogo() {
        jQuery('.change').attr('value', 0);
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: {
                name: '<?php echo $name_old; ?>'
            },
            success: function (res) {
                // Do nothing
            }
        });
    }

    function scale() {
        jQuery('.img-logo img').each(function () {
            var maxWidth = 220; // Max width for the image
            var maxHeight = 160;    // Max height for the image
            var ratio = 0;  // Used for aspect ratio
            var width = jQuery(this).width();    // Current image width
            var height = jQuery(this).height();  // Current image height

            // Check if the current width is larger than the max
            if (width > maxWidth) {
                ratio = maxWidth / width;   // get ratio for scaling image
                jQuery(this).css("width", maxWidth); // Set new width
                jQuery(this).css("height", height * ratio);  // Scale height based on ratio
                jQuery(this).css("background-color", "rgba(0, 0, 0, 125)");
                height = height * ratio;    // Reset height to match scaled image
                width = width * ratio;    // Reset width to match scaled image
            }

            // Check if current height is larger than max
            if (height > maxHeight) {
                ratio = maxHeight / height; // get ratio for scaling image
                jQuery(this).css("height", maxHeight);   // Set new height
                jQuery(this).css("width", width * ratio);    // Scale width based on ratio
                jQuery(this).css("opacity", "0.6");
                jQuery(this).css("background-color", "rgba(0, 0, 0, 125)");
                width = width * ratio;    // Reset width to match scaled image
                height = height * ratio;    // Reset height to match scaled image
            }
        });
    }


</script>
