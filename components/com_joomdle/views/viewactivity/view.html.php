<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 *  $author: kydon vietnam team
 *  $package: Joomdle view activity
 *  $date: 28 Jul 2016
 */

defined('_JEXEC') or die('Restricted access');

class JoomdleViewViewactivity extends JViewLegacy {
    function display($tpl = null) {
        $app = JFactory::getApplication();
        
        $params = $app->getParams();
	    $this->assignRef('params', $params);

        $user = JFactory::getUser();
        $username = $user->username;

        $id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
        if (!$id) $id =  JRequest::getVar( 'course_id' );
        if (!$id)
            $id = $params->get( 'course_id' );

        $id = (int) $id;

        $this->hasPermission = JFactory::hasPermission($id, $username);
        $user_role = array();
        if (!empty($this->hasPermission)) {
            foreach (json_decode($this->hasPermission[0]['role']) as $r) {
                $user_role[] = $r->sortname;
            }
        }
        if (empty(array_intersect($user_role, ['teacher', 'editingteacher', 'manager', 'coursecreator']))) {
            JFactory::handleErrors();
        }
            
        $this->course_info = JoomdleHelperContent::getCourseInfo($id, '');
        $this->coursefullname = $this->course_info['fullname'];
        $this->courseid = $id;
        /* 
         * get course mods
         */

        if ($params->get('use_new_performance_method'))
            $this->mods = JHelperLGT::getModProgress($id, $username);
        else
            $this->mods = JoomdleHelperContent::call_method('get_mod_progress', (int)$id, $username);
        
        parent::display($tpl);
    }
}