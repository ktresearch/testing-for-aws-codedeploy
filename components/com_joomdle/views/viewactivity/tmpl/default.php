<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
defined('_JEXEC') or die('Restricted access');

require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);

$session = JFactory::getSession();
$device = $session->get('device');
if ($device == 'mobile') { 
    $dev = 1;
    $sizechart = 180;
} else {
    $sizechart = 180;
}

$render_banner = $banner->renderBanner($this->course_info, $this->mods['sections'], $this->hasPermission);

?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.js"></script>
<div class="joomdle-overview-header">
<?php
require_once(JPATH_SITE.'/components/com_joomdle/views/header_facilitator.php');
?>
</div>
<style type="text/css">
    @media screen and (max-width: 768px) {
        .abc { height: 50px; }
    }
</style>
<div class="joomdle-course joomdle-activity-content <?php echo $this->pageclass_sfx?>">
    <div class="joomdle_course_total">
        <span><?php echo JText::_('COM_JOOMDLE_ACTIVITY_TOTAL_LEARNER') . $this->mods['count_learner'];?></span>
        <span><?php echo JText::_('COM_JOOMDLE_ACTIVITY_TOTAL_COMPLETED') . $this->mods['count_completed'];?></span>
    </div>
    <?php 
     if(is_array($this->mods['sections'])) {
         $sec = 0;
         foreach ($this->mods['sections'] as $mod) {
             $activities = $mod['mods'];
             ?>
        <div class="joomdle_course_progress <?php echo 'sec_'.$sec;?>">
            <span class="progress-section-name"><?php echo $mod['sectionname'];?></span>
            <div class="progress-activities">
            <?php
                $i = 0;
                  foreach ($activities as $act) {
                       $mtype = JoomdleHelperSystem::get_mtype ($act['mod']);
				if (!$mtype) // skip unknow modules
					continue;
                        $css = '';
                        if (($act['mod'] == 'forum') || ($act['mod'] == 'certificate') || ($act['mod'] == 'questionnaire')) {
                                $css = 'display: none;';
                        }
                        $icon_url = JoomdleHelperSystem::get_icon_url ($act['mod'], $act['type']);
                        $hidden = '';
                        if (($act['mod'] == 'forum') || ($act['mod'] == 'certificate') || ($act['mod'] == 'questionnaire')) {
                            $hidden = 'display: none;';
                        }
                        $percentage = $this->mods['count_learner'] ? (round($act['totalcompleted']*100/$this->mods['count_learner'])) : 0;
                        if ($this->mods['count_learner'] > 0) {
                            $member_notcomplete = $this->mods['count_learner'] - $act['totalcompleted'];    
                        }
                        ?>
                        <div class="progress-activity" style="<?php echo $hidden; ?>">
                            <div class="activities">
                                <div class="title-activity">
                                <?php
                                if ($icon_url) {
                                    echo '<div class="activity-image">';
                                    echo '<div><img align="center" src="'. $icon_url.'"></div>';
                                    echo '<div><a href="'.JUri::base().'activitydetail/'.$this->courseid.'_'.$act['id'].'_'.$act['mod'].'.html'.'">';
                                    echo '<h5>' . $act['name'] . '</h5>';
                                    echo '</a></div>';
                                    echo '</div>';
                                }
                                ?>
                                    <!--<h5><?php // echo ucfirst($act['name']); ?></h5>-->
<!--                                    <div class="activity-detail">
                                        <a href="<?php // echo JUri::base().'activitydetail/'.$this->courseid.'_'.$act['id'].'_'.$act['mod'].'.html'; ?>">
                                            <span class="view-detail-more"></span>
                                        </a>
                                    </div>-->
                                   </div>
                            </div>
                            <div class="chartSub">
                                <a href="<?php echo JUri::base().'activitydetail/'.$this->courseid.'_'.$act['id'].'_'.$act['mod'].'.html';?>">
                                    <div class="activityCompleted">
                                        <canvas id="doughnut_<?php echo $act['id']; ?>" width="<?php echo $sizechart;?>" height="<?php echo $sizechart;?>"></canvas>
                                    </div>
                                </a>
                            </div>
                            <script type="text/javascript">
    var device = '<?php echo $dev; ?>';
    Chart.types.Doughnut.extend({
        name: "doughnutAlt",
        draw: function () {
            Chart.types.Doughnut.prototype.draw.apply(this, arguments);

            var width = this.chart.width,
                    height = this.chart.height;

            var fontSize = (height / 114).toFixed(2);
            this.chart.ctx.font = 'normal 16px "Open Sans", "OpenSans", sans-serif';
            this.chart.ctx.textAlign = 'center';
            this.chart.ctx.textBaseline = 'middle';
            this.chart.ctx.fillStyle = '#484848';
            this.chart.ctx.save();
            this.chart.ctx.beginPath();

            var text = '<?php echo $percentage; ?>%';
            if(device) this.chart.ctx.fillText(text, 90, 75);
            else this.chart.ctx.fillText(text, 90, 75);
            this.chart.ctx.font = '300 16px "Open Sans", "OpenSans-Light", sans-serif';
            this.chart.ctx.textAlign = 'center';
            this.chart.ctx.textBaseline = 'middle';
            this.chart.ctx.fillStyle = '#484848';
            this.chart.ctx.save();
            this.chart.ctx.beginPath();
            
            var text = '<?php echo JText::_('COM_JOOMDLE_COMPLETED'); ?>';
            if(device) this.chart.ctx.fillText(text, 90, 100);
            else this.chart.ctx.fillText(text, 90, 100);
            this.chart.ctx.font = '300 16px "Open Sans", "OpenSans-Light", sans-serif';
            this.chart.ctx.textAlign = 'center';
            this.chart.ctx.textBaseline = 'middle';
            this.chart.ctx.fillStyle = '#484848';
            this.chart.ctx.save();
            this.chart.ctx.beginPath();
        }
    });

    var chartOptions = {
        onAnimationComplete: function () {
            // Always show Tooltip
            this.showTooltip(this.segments, true);
        },
        customTooltips: function (tooltip) {
            // Tooltip Element
            var tooltipEl = $('#chartjs-tooltip');
            // Hide if no tooltip
            if (!tooltip) {
                tooltipEl.css({
                    opacity: 1
                });
                return;
            }
            // Set caret Position
            tooltipEl.removeClass('above below');
            tooltipEl.addClass(tooltip.yAlign);
            tooltipEl.addClass(tooltip.xAlign);
            // Set Text
            tooltipEl.html(tooltip.text);
            // Find Y Location on page
            var top;
            if (tooltip.yAlign == 'above') {
                top = tooltip.y - tooltip.caretHeight - tooltip.caretPadding;
            } else {
                top = tooltip.y + tooltip.caretHeight + tooltip.caretPadding;
            }
            // Display, position, and set styles for font
            tooltipEl.css({
                opacity: 0,
                left: tooltip.chart.canvas.offsetLeft + tooltip.x + 'px',
                top: tooltip.chart.canvas.offsetTop + top + 'px',
                fontFamily: tooltip.fontFamily,
                fontSize: tooltip.fontSize,
                fontStyle: tooltip.fontStyle,
                xOffset: tooltip.xOffset,
            });
        },
        tooltipEvents: [], // Remove to enable Default Mouseover events
        tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
        tooltipFillColor: "rgba(0,0,0,0.0)",
        tooltipFontColor: "#505050",
        tooltipFontSize: 24,
        tooltipXOffset: 0,
        tooltipXPadding: 0,
        tooltipYPadding: 0,
//    tooltipTemplate: "<%= value %>%",
        legends: true,
        showTooltips: true,
        segmentShowStroke: false,
        percentageInnerCutout: 65,
        animationEasing: "easeInOutQuart",
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    }

    var data = [{
            value: <?php echo $act['totalcompleted'] ? $act['totalcompleted'] : 0; ?>,
            color: "#126DB6",
            label: "Data 1"
        }, {
            value: <?php echo (isset($member_notcomplete)) ? $member_notcomplete : 1; ?>,
            color: "#DBECF8",
            label: "Data 2"
        }, ];
    var doughnutChart = new Chart(document.getElementById("doughnut_<?php echo $act['id']; ?>").getContext("2d")).doughnutAlt(data, chartOptions);
    
</script>
                        </div>
                        <?php
                $i++;
                  }
            ?>
        </div>
            <script>
                 var i = <?php echo $i;?>;
                 if (i == 0) { 
                     jQuery('.sec_<?php echo $sec;?> .progress-activities').addClass('hide');
                 }
            </script>
        </div>
    <?php 
            $sec++;
         }
     }
    ?>
    <div class="abc"></div>
</div>
