<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

 * @date: 04 Aug 2016
 * @author: KV team
 * @package: joomdle edit grade (for facilitator)
 */

defined('_JEXEC') or die('Restricted access');

require_once(JPATH_SITE . '/components/com_community/libraries/core.php');
require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);

$render_banner = $banner->renderBanner($this->course_info, $this->mods['sections'], $this->hasPermission);
$icon_url = JoomdleHelperSystem::get_icon_url($this->activity['mod'], $this->activity['type']);
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . '/components/com_joomdle/views/activitygradestudent/tmpl/activitygradestudent.css');
$grade_letter_assign = array();
$grade_letter_assign[0]['name'] = 'A';
$grade_letter_assign[0]['value'] = 93;

$grade_letter_assign[1]['name'] = 'B';
$grade_letter_assign[1]['value'] = 83;

$grade_letter_assign[2]['name'] = 'C';
$grade_letter_assign[2]['value'] = 73;

$grade_letter_assign[3]['name'] = 'D';
$grade_letter_assign[3]['value'] = 60;

?>
<div class="joomdle-overview-header">
    <?php
    require_once(JPATH_SITE . '/components/com_joomdle/views/header_facilitator.php');
    ?>
</div>
<div class="joomdle-course joomdle-edit <?php echo $this->pageclass_sfx ?>">
    <div class="title-mod">
        <img align="center" src="<?php echo $icon_url; ?>">
        <span class="title-course"><?php echo $this->activity['name']; ?></span>
    </div>
    <?php
    $user = JFactory::getUser($this->username);
    $users = CFactory::getUser($user->id);
    // get avatar
    $avatar = $users->getAvatar();
    $grade_letters = $this->overall_grade[0]['gradeoptionsletters'];
    ?>
    <form id="grade-student" action="<?php echo JRoute::_('index.php?option=com_joomdle&task=saveStudentGrade'); ?>"
          method="post">
        <div class="student-info">
            <div class="avatar-overall"><a class="nav-avatar"><img alt="<?php echo $users->name; ?>"
                                                                   src="<?php echo $avatar ?>"/></a></div>
            <div class="user-grade">
                <div class="name"><?php echo $users->name; ?></div>
                <div class="grade-title">
                    <span class="lable-grade"><?php echo JText::_('PROGRESS_GRADE'); ?></span>
                    <span class="time-completed-activity"><?php echo $this->activity['mod_completion_date']; ?></span>
                </div>
                <div class="select-grade-student">
                    <?php if ($this->activity['gradeitemid'] && $this->activity['mod'] == 'assign') { ?>
                        <div class="gradeActivitySelect">
                        <div class="gradeSelected">
                            <?php if ($this->activity['finalgradeletters'] != "Ungraded"  && $this->activity['finalgrade'] > 0) {
//                                echo strtok($this->activity['finalgradeletters'], " ");

                                if ($this->activity['grademax'] == 100) $finalGrade = $this->activity['finalgrade'];
                                else if ($this->activity['grademax'] == 10) $finalGrade = $this->activity['finalgrade'] * 10;

                                foreach ($grade_letter_assign as $grade_assign) {
                                    if ($finalGrade >= $grade_assign['value']) {
                                        $finalgradeletters_assign = $grade_assign['name'];
                                        break;
                                    }
                                }
                                echo $finalgradeletters_assign;

                            } else {
                                echo JText::_('PROGRESS_SELECT_OPTION');
                                $finalGrade = "";
                            } ?>

                        </div>
                        <ul class="ulGradeSelected" style="display: none;">
                            <?php
                            //check grade numeric or letter
                            foreach ($grade_letter_assign as $grade_assign) {
                                ?>
                                <li value="<?php echo $grade_assign['value']; ?>" <?php if ($this->activity['finalgrade'] == $grade_assign['value'] && $this->activity['gradeid'] != 0) { ?> class="selected" <?php } ?>>
                                    <p><?php echo $grade_assign['name']; ?></p>
                                </li>
                                <?php
                            }
                            ?>
                            <input type="hidden" class="grade-option"
                                   name="grade_value[]" value=""/>
                            <input type="hidden" class="status-option"
                                   name="status[]" value="<?php
                            if (empty($this->activity['mod_lastaccess']) && ($this->activity['finalgradeletters'] == "Ungraded")) {
                                echo "not yet started";
                            } elseif ($this->activity['mod_lastaccess'] && ($this->activity['finalgradeletters'] == "Ungraded") && $this->activity['mod_completion'] === false) {
                                echo "in-progress";
                            } elseif ($this->activity['mod_completion'] && $this->activity['mod_completion_date'] && ($this->activity['finalgradeletters'] != "Ungraded")) {
                                echo "completed";
                            } ?>"
                            />
                        </ul>
                        </div><?php } ?>
                    <?php if ($this->activity['gradeitemid'] && $this->activity['gradingscheme'] == 1 && $this->activity['mod'] == 'quiz') { ?>
                        <?php
                        $finalGrade = (float)$this->activity['finalgrade'] * 10;
                        $grade_letters_ass = $this->activity['lettergradeoption'];
                        // Get grade letter of assessment
                        if ($this->activity['finalgradeletters'] != "Ungraded") {
                            foreach ($grade_letters_ass as $grade_ass) {
                                if ($finalGrade >= $grade_ass['value']) {
                                    $finalgradeletters_ass = $grade_ass['name'];
                                    break;
                                }

                            }
                        }

                        if($this->activity['finalgradeletters'] == "Ungraded"){
                            $finalGrade = "";
                        }
                        ?>
                        <div class="gradeActivitySelect">
                            <div class="gradeSelected">
                                <?php if ($this->activity['finalgradeletters'] != "Ungraded"  && $this->activity['finalgrade'] > 0) {
                                    echo $finalgradeletters_ass;
                                } else {
                                    echo JText::_('PROGRESS_SELECT_OPTION');
                                } ?>

                            </div>
                            <ul class="ulGradeSelected" style="display: none;">
                                <?php
                                //check grade numeric or letter
                                foreach ($grade_letters_ass as $grade_ass) {
                                    ?>
                                    <li value="<?php echo $grade_ass['value']; ?>" <?php if ($act['finalgrade'] == $grade_ass['value'] && $act['gradeid'] != 0) { ?> class="selected" <?php } ?>>
                                        <p><?php echo $grade_ass['name']; ?></p>
                                    </li>
                                    <?php
                                }
                                ?>
                                <input type="hidden" class="grade-option"
                                       name="grade_value[]" value="<?php echo $finalGrade; ?>"/>
                                <input type="hidden" class="status-option"
                                       name="status[]" value="<?php
                                if (empty($this->activity['mod_lastaccess']) && ($this->activity['finalgradeletters'] == "Ungraded")) {
                                    echo "not yet started";
                                } elseif ($this->activity['mod_lastaccess'] && ($this->activity['finalgradeletters'] == "Ungraded") && $this->activity['mod_completion'] === false) {
                                    echo "in-progress";
                                } elseif ($this->activity['mod_completion'] && $this->activity['mod_completion_date'] && ($this->activity['finalgradeletters'] != "Ungraded")) {
                                    echo "completed";
                                } ?>"
                                />
                            </ul>
                            <?php
                            ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->activity['gradingscheme'] == 0 && $this->activity['mod'] == 'quiz') { ?>
                        <input type="hidden" name="grade_value[]"
                               value="<?php echo $finalGrade; ?>"/>
                        <input type="hidden" name="gradeitem[]"
                               value="<?php echo $this->activity['gradeitemid']; ?>"/>
                        <input type="hidden" name="gradeid[]"
                               value="<?php echo $this->activity['gradeid']; ?>"/>
                    <?php } ?>
                    <?php
                    if ($this->activity['mod'] == 'page') { ?>
                        <input type="hidden" name="grade_value[]" value="">
                        <input type="hidden" name="gradeitem[]"
                               value="<?php echo $this->activity['gradeitemid']; ?>"/>
                        <input type="hidden" name="gradeid[]"
                               value="<?php echo $this->activity['gradeid']; ?>"/>
                    <?php }
                    if ($this->activity['mod'] == 'scorm') { ?>
                        <input type="hidden" name="grade_value[]" value="">
                        <input type="hidden" name="gradeitem[]"
                               value="<?php echo $this->activity['gradeitemid']; ?>"/>
                        <input type="hidden" name="gradeid[]"
                               value="<?php echo $this->activity['gradeid']; ?>"/>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php
        // get device
        $session = JFactory::getSession();
        $device = $session->get('device');
        if ($device == 'mobile') {
            $cols = 5;
            $rows = 4;
        } else {
            $cols = 10;
            $rows = 3;
        }
        ?>
        <div class="grade-feedback feedback-content">
            <span><?php echo JText::_('COM_JOOMDLE_GRADE_COMMENT'); ?></span>

            <textarea id="txtFeedback"
                      name="feedback<?php if ($this->activity['gradeitemid']) { ?>grade<?php } ?>[]"
                      rows="<?php echo $rows; ?>"
                      cols="<?php echo $cols; ?>"><?php echo $this->activity['feedback']; ?></textarea>

        </div>
        <div class="grade-button">
            <input type="hidden" name="activite[]" value="<?php echo $this->activity['id']; ?>"/>
            <input type="hidden" name="username" value="<?php echo $this->username; ?>"/>
            <input type="hidden" name="gradeid[]" value="<?php echo $this->activity['gradeid']; ?>"/>
            <input type="hidden" name="gradeitem[]" value="<?php echo $this->activity['gradeitemid']; ?>"/>
            <input type="hidden" name="courseid" value="<?php echo $this->courseid; ?>"/>
            <input type="hidden" name="mod[]" value="<?php echo $this->type; ?>"/>
            <input type="hidden" name="type" value="activity_grade"/>
            <input type="submit" value="<?php echo JText::_('COM_JOOMDLE_GRADE_EDIT_SAVE'); ?>" class="button-right">
            <input type="button" onclick="history.go(-1)"
                   value="<?php echo JText::_('COM_JOOMDLE_GRADE_EDIT_CANCEL'); ?>" class="button-right cancel">
        </div>
    </form>
</div>

<script type="text/javascript">
    (function ($) {
        $('.gradeActivitySelect').each(function () {
            $(this).click(function () {
                $(this).find('.ulGradeSelected').toggle();
            });
        });

        $('.gradeActivitySelect .ulGradeSelected li').each(function () {
            $(this).click(function () {
                $(this).parent('.ulGradeSelected').prev('.gradeSelected').html($(this).html());
                $(this).parent('.ulGradeSelected').find('.grade-option').val($(this).attr('value'));
                if ($(this).parent('.ulGradeSelected').find('.grade-option').val($(this).attr('value')) != null) {
                    $(this).parent('.ulGradeSelected').find('.status-option').val("completed");
                }
            });
        });
    })(jQuery);
</script>