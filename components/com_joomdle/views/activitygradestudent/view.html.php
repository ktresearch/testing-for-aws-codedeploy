<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Grade for student
// $date: 04 Aug 2016

class JoomdleViewActivitygradestudent extends JViewLegacy
{

    public function display($tpl = null)
    {
        $app = JFactory::getApplication();

        $params = $app->getParams();
        $this->assignRef('params', $params);
        $currentUser = JFactory::getUser();
        $currentUsername = $currentUser->username;

        $id = JRequest::getVar('gradeid', null, 'NEWURLFORM');
        if (!$id) $id = JRequest::getVar('gradeid');
        if (!$id)
            $id = $params->get('gradeid');

        $id = (int)$id;

        $user = JRequest::getVar('user', null, 'NEWURLFORM');
        if (!$user) $user = JRequest::getVar('user');
        if (!$user)
            $user = $params->get('user');

        $gradeitem = JRequest::getVar('gradeitem', null, 'NEWURLFORM');
        if (!$gradeitem) $gradeitem = JRequest::getVar('gradeitem');
        if (!$gradeitem)
            $gradeitem = $params->get('gradeitem');

        $gradeitem = (int)$gradeitem;

        $courseid = JRequest::getVar('course_id', null, 'NEWURLFORM');
        if (!$courseid) $courseid = JRequest::getVar('course_id');
        if (!$courseid)
            $courseid = $params->get('course_id');

        $this->hasPermission = JFactory::hasPermission($courseid, $currentUsername);
        $user_role = array();
        if (!empty($this->hasPermission)) {
            foreach (json_decode($this->hasPermission[0]['role']) as $r) {
                $user_role[] = $r->sortname;
            }
        }
        if ($user_role[0] != 'manager' && $user_role[0] != 'teacher' && $user_role[0] != 'editingteacher' && $user_role[0] != 'coursecreator') {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//            return;
        }

        $type = JRequest::getVar('type', null, 'NEWURLFORM');
        if (!$type) $type = JRequest::getVar('type');
        if (!$type)
            $type = $params->get('type');

        $mid = JRequest::getVar('mid', null, 'NEWURLFORM');
        if (!$mid) $mid = JRequest::getVar('mid');
        if (!$mid)
            $mid = $params->get('mid');

        $this->courseid = $courseid;
        $this->type = $type;
        $this->mid = $mid;

        $username = JFactory::getUser($user)->username;
        $this->username = $username;
        $this->overall_grade = JoomdleHelperContent::call_method('get_course_grades_by_category', (int)$courseid, $username);
        $this->mods_student = JoomdleHelperContent::call_method('get_mod_progress', (int)$courseid, $username);
        if (isset($this->mods_student['sections']) && !empty($this->mods_student['sections'])) {
            foreach ($this->mods_student['sections'] as $section) {
                foreach ($section['mods'] as $mod) {
                    if ($mod['id'] == $mid) {
                        $this->activity = $mod;
                        break;
                    }
                }
            }

        }

        // course fullname
        $course_info = JoomdleHelperContent::getCourseInfo($courseid, $username);
        $this->course_info = $course_info;
        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
        parent::display($tpl);
    }
}