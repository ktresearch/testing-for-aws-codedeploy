<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * @date: 25 june 2016
 * @author: KV team
 * @package: joomdle (header for course overview facilitator)
 */

defined('_JEXEC') or die('Restricted access'); 

$app                = JFactory::getApplication();

$params = $app->getParams();
$this->assignRef('params', $params);

$id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
if (!$id) $id =  JRequest::getVar( 'course_id' );
if (!$id)
    $id = $params->get( 'course_id' );

$id = (int) $id;


$full_url = $_SERVER['REQUEST_URI'];
$active = '';
$active_student = '';
$active_activity = '';
if (strpos($full_url,'overallgrade') !== false) {
    $active = 'active';
} 
if(strpos($full_url,'viewstudents') !== false || strpos($full_url,'studentdetail') !== false) {
    $active_student = 'active';
}
if(strpos($full_url,'viewactivity') !== false || strpos($full_url,'activitydetail') !== false || strpos($full_url, 'activitygradestudent')) {
    $active_activity = 'active';
}
?>
<div class="fa-course">
    <nav>
        <ul>
            <li class="overal-student-view <?php echo $active_student; ?>">
                <a href="<?php echo Juri::base().'viewstudents/'.$id.'.html'; ?>"><?php echo JText::_('COM_JOOMDLE_COURSE_STUDENT_VIEW'); ?></a>
            </li>
            <li class="overall-activity-view <?php echo $active_activity; ?>">
                <a href="<?php echo Juri::base().'viewactivity/'.$id.'.html'; ?>"><?php echo JText::_('COM_JOOMDLE_COURSE_ACTIVITY_VIEW'); ?></a>
            </li>
        </ul>
    </nav>
</div>