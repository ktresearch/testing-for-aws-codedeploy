<?php defined('_JEXEC') or die('Restricted access');
?>
<?php
include_once('components/com_community/libraries/core.php');
require_once(JPATH_SITE . DS . 'components' . DS . 'com_joomdle' . DS . 'helpers' . DS . 'content.php');

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'components/com_joomdle/views/errpage/tmpl/errpage.css');

?>
<html>
    <body class="errorpage">
        <div class = "error_404">
            <div class = "error_404_top">
                <span>404 PAGE</span><br>
                <span>NOT FOUND</span><br>
            </div>
            <div class = "error_404_center">
                <span>Check URL or return to </span><a href="<?php echo $this->baseurl; ?>">Homepage</a>
            </div>
            <div class ="error_404_bottom">
                <a href="mailto:support@parenthexis.com">Contact us </a><span>about the problem.</span>
            </div>
        </div>
    </body>
</html>

