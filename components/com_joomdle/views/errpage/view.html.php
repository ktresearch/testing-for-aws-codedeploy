<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewErrpage extends JViewLegacy
{
    function display($tpl = null)
    {
        global $mainframe;
        $user = JFactory::getUser();
        $username = $user->username;
        $this->username = $username;
        $config = new JConfig();
        parent::display($tpl);
    }
}

?>
