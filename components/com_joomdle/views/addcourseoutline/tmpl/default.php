<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
$itemid = JoomdleHelperContent::getMenuItem();

$linkstarget = $this->params->get( 'linkstarget' );
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$session  = JFactory::getSession();
$jump_url =  JoomdleHelperContent::getJumpURL ();
$user = JFactory::getUser();
$username = $user->username;
$token = md5 ($session->getId());
$course_id = $this->course_id;

// Topic id need add content
$add_section_id = JRequest::getVar('section_id', null, 'NEWURLFORM');
$add_section_id = (int)$add_section_id;

if ($session->get('device') == 'mobile') {
    ?>
    <style type="text/css">
        .navbar-header {
            background-color: #126db6;
        }
    </style>
    <?php
}


require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
?>
<div class="joomdle-addcourseoutline <?php echo $this->pageclass_sfx?>">
    <?php
    $topic = 0;
    $content = 0;
    $scorm = 0;
    $assessment = 0;
    $assignment = 0;
    $count_not_visible =0;
    $count_all=0;

    // Param id name question
    if (is_array($questions)) {
        foreach ($questions as $question) :
            $question_all_id[] = $question['id'];
            $question_all_name[] = $question['name'];
        endforeach;
    }

    // Param name id all module not add course outline
    if (is_array ($this->mods_not_visible)) {
        foreach ($this->mods_not_visible as $tema) :
            $resources = $tema['mods'];
            if (is_array($resources)) :
                foreach ($resources as $id => $resource) {
                    $mtype = JoomdleHelperSystem::get_mtype ($resource['mod']);
                    if (!$mtype) // skip unknow modules
                        continue;
                    switch ($mtype) {
                        case 'quiz':
                            $count_not_visible++;
                            $quiz_not_add_outline_id[]=$resource['id'];
                            $quiz_not_add_outline_name[]=$resource['name'];
                            break;
                        case 'assign':
                            $count_not_visible++;
                            $assignment_not_add_outline_id[]=$resource['id'];
                            $assignment_not_add_outline_name[]=$resource['name'];
                            break;
                        case 'page':
                            $count_not_visible++;
                            $content_not_add_outline_id[]=$resource['id'];
                            $content_not_add_outline_name[]=$resource['name'];
                            break;
                        case 'scorm':
                            $count_not_visible++;
                            $scrom_not_add_outline_id[]=$resource['id'];
                            $scrom_not_add_outline_name[]=$resource['name'];
                            break;
                        default:
                            # code...
                            break;
                    }
                }
            endif;
        endforeach;
    }

    // Param name id all module quiz, scorm, assignment, page
    if (is_array ($this->mods)) {
        foreach ($this->mods as $tema) :
            $resources = $tema['mods'];
            if (is_array($resources)) :
                foreach ($resources as $id => $resource) {
                    $mtype = JoomdleHelperSystem::get_mtype ($resource['mod']);
                    if (!$mtype) // skip unknow modules
                        continue;
                    switch ($mtype) {
                        case 'quiz':
                            $assessment++;
                            $count_all++;
                            $quiz_all_id[]=$resource['id'];
                            $quiz_all_name[]=$resource['name'];
                            break;
                        case 'assign':
                            $count_all++;
                            $assignment++;
                            $assignment_all_id[]=$resource['id'];
                            $assignment_all_name[]=$resource['name'];
                            break;
                        case 'page':
                            $count_all++;
                            $content++;
                            $content_all_id[]=$resource['id'];
                            $content_all_name[]=$resource['name'];
                            break;
                        case 'scorm':
                            $scorm++;
                            $count_all++;
                            $scrom_all_id[]=$resource['id'];
                            $scrom_all_name[]=$resource['name'];
                            break;
                        default:
                            # code...
                            break;
                    }
                }
            endif;
        endforeach;
        $count =$count_all-$count_not_visible;
    }
    ?>
    <a class="btAddModule" href="<?php echo JUri::base()."coursecontent/".$this->course_id.".html";  ?>">+</a>
    <p class="selected countModule">Selected: <span class="selectedCount"><?php echo $count; ?></span></p>

    <input type="hidden" name="hdSelectedQuestions" class="hdSelectedQuestions"/>
    <div class="moduleType">
        <div class="topPart">
            <div class="moduleIcon"><img src="/images/ContentIcon.png"></div>
           <?php echo Jtext::_('COURSE_ACTIVITY_CONTENT'); ?> <?php echo '('.$content.')'; ?>
            <span class="glyphicon glyphicon-menu-down downContent" style=" float: right;margin-top: 6px;"></span>
            <span class="glyphicon glyphicon-menu-up upContent hidden" style=" float: right;margin-top: 6px;"></span>

        </div>

        <div class="contentModuleNotVisible hidden">
            <ul>
                <?php
                for($i=0;$i<count($content_all_id);$i++) {
                    if(in_array($content_all_id[$i],$content_not_add_outline_id)==true)
                        echo "<li class='optionCheckModule' id='optionCheckModule" . $content_all_id[$i] . "' ><div class=\"courseContentStatus\" id ='courseContentStatusPage".$i."' qmid=" . $content_all_id[$i] . "></div>" . $content_all_name[$i] . "</li>";
                    else
                        echo "<li class='optionCheckModule' id='optionCheckModule" . $content_all_id[$i] . "' ><div class=\"courseContentAdded\" id ='courseContentStatusPage".$i."' qmid=" . $content_all_id[$i] . "></div>" . $content_all_name[$i] . "</li>";

                }
                ?>
            </ul>
        </div>
    </div>

    <div class="moduleType">
        <div class="topPart">
            <div class="moduleIcon"><img src="/images/ScormIcon.png"></div>
           <?php echo Jtext::_('COM_JOOMDLE_SCORM'); ?> <?php echo '('.$scorm.')'; ?>
            <span class="glyphicon glyphicon-menu-down downScorm" style=" float: right;margin-top: 6px;"></span>
            <span class="glyphicon glyphicon-menu-up upScorm hidden" style=" float: right;margin-top: 6px;"></span>

        </div>

        <div class="scormModuleNotVisible hidden">
            <ul>
                <?php
                for($i=0;$i<count($scrom_all_id);$i++) {
                    if(in_array($scrom_all_id[$i],$scrom_not_add_outline_id)==true)
                        echo "<li class='optionCheckModule' id='optionCheckModule" . $scrom_all_id[$i] . "' ><div class=\"courseContentStatus\" id ='courseContentStatusScorm".$i."' qmid=" . $scrom_all_id[$i] . "></div>" . $scrom_all_name[$i] . "</li>";
                    else
                        echo "<li class='optionCheckModule' id='optionCheckModule" . $scrom_all_id[$i] . "' ><div class=\"courseContentAdded\" id ='courseContentStatusScorm".$i."' qmid=" . $scrom_all_id[$i] . "></div>" . $scrom_all_name[$i] . "</li>";
                }
                ?>
            </ul>
        </div>

    </div>
    <!---->
    <!--    <div class="moduleType">-->
    <!--        <div class="topPart">-->
    <!--            <div class="moduleIcon"><img src="/images/icons/questions30x30.png"></div>-->
    <!--            <a class="moduleName" href="--><?php //echo JUri::base()."question/list_".$this->course_id.".html";  ?><!--">--><?php //echo Jtext::_('COURSE_ACTIVITY_QUESTION'); ?><!-- --><?php //echo '('.count($question_all_id).')'; ?><!--</a>-->
    <!--            <span class="glyphicon glyphicon-menu-down downQuestion" style=" float: right;margin-top: 6px;"></span>-->
    <!--            <span class="glyphicon glyphicon-menu-up upQuestion hidden" style=" float: right;margin-top: 6px;"></span>-->
    <!---->
    <!--        </div>-->
    <!--        <div class="questionModuleNotVisible hidden">-->
    <!--            <ul>-->
    <!--                --><?php
    //                for($i=0;$i<count($question_all_id);$i++) {
    //                    echo "<li class='optionCheckModule' id='optionCheckModule" . $question_all_id[$i] . "' qmid=" . $question_all_id[$i] . ">" . $question_all_name[$i] . "</li>";
    //                }
    //                ?>
    <!--            </ul>-->
    <!--        </div>-->
    <!--    </div>-->

    <div class="moduleType">
        <div class="topPart">
            <div class="moduleIcon"><img src="/images/Assessment.png"></div>
            <?php echo Jtext::_('COURSE_ACTIVITY_ASSESSMENT'); ?> <?php echo '('.$assessment.')'; ?>
            <span class="glyphicon glyphicon-menu-down downAssessment" style=" float: right;margin-top: 6px;"></span>
            <span class="glyphicon glyphicon-menu-up upAssessment hidden" style=" float: right;margin-top: 6px;"></span>
        </div>

        <div class="assessmentModuleNotVisible hidden">
            <ul>
                <?php
                for($i=0;$i<count($quiz_all_id);$i++) {
                    if(in_array($quiz_all_id[$i],$quiz_not_add_outline_id)==true)
                        echo "<li class='optionCheckModule' id='optionCheckModule" . $quiz_all_id[$i] . "' ><div class=\"courseContentStatus\" id ='courseContentStatusAssessment".$i."' qmid=" . $quiz_all_id[$i] . "></div>" . $quiz_all_name[$i] . "</li>";
                    else
                        echo "<li class='optionCheckModule' id='optionCheckModule" . $quiz_all_id[$i] . "' ><div class=\"courseContentAdded\" id ='courseContentStatusAssessment".$i."' qmid=" . $quiz_all_id[$i] . "></div>" . $quiz_all_name[$i] . "</li>";
                }
                ?>
            </ul>
        </div>

    </div>

    <div class="moduleType" style="border-bottom: 0px;">
        <div class="topPart">
            <div class="moduleIcon"><img src="/images/Assignment.png"></div>
            <?php echo Jtext::_('COURSE_ACTIVITY_ASSIGNMENT'); ?> <?php echo '('.$assignment.')'; ?>
            <span class="glyphicon glyphicon-menu-down downAssignment" style=" float: right;margin-top: 6px;"></span>
            <span class="glyphicon glyphicon-menu-up upAssignment hidden" style=" float: right;margin-top: 6px;"></span>
        </div>

        <div class="assignmentModuleNotVisible hidden">
            <ul>
                <?php
                for($i=0;$i<count($assignment_all_id);$i++) {
                    if(in_array($assignment_all_id[$i],$assignment_not_add_outline_id)==true)
                        echo "<li class='optionCheckModule' id='optionCheckModule" . $assignment_all_id[$i] . "' ><div class=\"courseContentStatus\" id ='courseContentStatusAssignment".$i."' qmid=" . $assignment_all_id[$i] . "></div>" . $assignment_all_name[$i] . "</li>";
                    else
                        echo "<li class='optionCheckModule' id='optionCheckModule" . $assignment_all_id[$i] . "' ><div class=\"courseContentAdded\" id ='courseContentStatusAssignment".$i."' qmid=" . $assignment_all_id[$i] . "></div>" . $assignment_all_name[$i] . "</li>";
                }
                ?>
            </ul>
        </div>

    </div>
    <form id="formUpdateVisible" class="form" method="post" action="<?php echo JRoute::_('index.php?option=com_joomdle&task=saveUpdatevisible'); ?>">
        <input type="hidden" name="moduleIdNum" id="moduleIdNum"/>
        <input type="hidden" name="addSectionId" id="addSectionId" value="<?php echo $add_section_id ?>"/>
        <!--        <input type="hidden" name="moduleIdNumNotCheck" id="moduleIdNumNotCheck"/>-->
        <input type="hidden" name="courseid" id="courseid" value="<?php echo $this->course_id; ?>">
        <input class="btSave hidden" type="submit" value="<?php echo JText::_('COURSE_CONTENT_BTN_SAVE');?>"/>

    </form>
    <button class="btCancel hidden"  onClick="window.location.reload()"><?php echo JText::_('COURSE_CONTENT_BTN_CANCEL');?></button>
    <!--    <button class="btSave hidden">--><?php //echo JText::_('COURSE_CONTENT_BTN_SAVE');?><!--</button>-->


</div>
<script>
    (function ($) {
        // Check question selected return array option(array sort ASC)

        function load_module_check() {
            var module_check = [];
            var i=0;
            $("[id^='courseContentStatus']").each(function () {
                if ($(this).hasClass('active')) {
                    module_check.push($(this).attr('qmid'));
                }
            });
            if(module_check.length != 0){
                $(".btCancel").removeClass('hidden');
                $(".btSave").removeClass('hidden');

            }
            else{
                $(".btCancel").addClass('hidden');
                $(".btSave").addClass('hidden');
            }
//            console.log(module_check);
            var module_id_array = module_check;
            var module_id_string = String(module_id_array);
            document.getElementById("moduleIdNum").value = module_id_string;
            return module_check;
        }
//        function load_module_not_check() {
//            var module_not_check = [];
//            var i=0;
//            $("[id^='courseContentStatus']").each(function () {
//                if (!$(this).hasClass('active')) {
//                    module_not_check.push($(this).attr('qmid'));
//                }
//            });
//            if(module_not_check.length != 0){
//                $(".btCancel").removeClass('hidden');
//                $(".btSave").removeClass('hidden');
//
//            }
//            var module_id_array_not_check = module_not_check;
//            var module_id_string_not_check = String(module_id_array_not_check);
//            document.getElementById("moduleIdNumNotCheck").value = module_id_string_not_check;
//            return module_not_check;
//        }


        $('.courseContentStatus').click(function () {
            var oldv = $('.hdSelectedQuestions').val();
            var count = parseInt($('.selectedCount').html());
            if (!$(this).hasClass('active')) {
                $('.selectedCount').html(count + 1);
                var arr = oldv.split(',');
                arr.push($(this).attr('qmid'));
                $('.hdSelectedQuestions').val(arr.toString());
            } else {
                $('.selectedCount').html(count - 1);
                var arr = oldv.split(',');
                var index = arr.indexOf($(this).attr('qmid'));
                if (index > -1) {
                    arr.splice(index, 1);
                }
                $('.hdSelectedQuestions').val(arr.toString());
            }
            $(this).toggleClass('active');
            load_module_check();
//            load_module_not_check();
        });

        // click down up content
        $('.downContent').click(function() {
            $(".contentModuleNotVisible").removeClass('hidden');
            $(".downContent").addClass('hidden');
            $(".upContent").removeClass('hidden');

        });
        $('.upContent').click(function() {
            $(".contentModuleNotVisible").addClass('hidden');
            $(".upContent").addClass('hidden');
            $(".downContent").removeClass('hidden');

        });

        // click down up scorm
        $('.downScorm').click(function() {
            $(".scormModuleNotVisible").removeClass('hidden');
            $(".downScorm").addClass('hidden');
            $(".upScorm").removeClass('hidden');
        });
        $('.upScorm').click(function() {
            $(".scormModuleNotVisible").addClass('hidden');
            $(".upScorm").addClass('hidden');
            $(".downScorm").removeClass('hidden');
        });

        // click down up question
        $('.downQuestion').click(function() {
            $(".questionModuleNotVisible").removeClass('hidden');
            $(".downQuestion").addClass('hidden');
            $(".upQuestion").removeClass('hidden');
        });
        $('.upQuestion').click(function() {
            $(".questionModuleNotVisible").addClass('hidden');
            $(".upQuestion").addClass('hidden');
            $(".downQuestion").removeClass('hidden');
        });

        // click down up assessment
        $('.downAssessment').click(function() {
            $(".assessmentModuleNotVisible").removeClass('hidden');
            $(".downAssessment").addClass('hidden');
            $(".upAssessment").removeClass('hidden');
        });
        $('.upAssessment').click(function() {
            $(".assessmentModuleNotVisible").addClass('hidden');
            $(".upAssessment").addClass('hidden');
            $(".downAssessment").removeClass('hidden');
        });

        // click down up assignment
        $('.downAssignment').click(function() {
            $(".assignmentModuleNotVisible").removeClass('hidden');
            $(".downAssignment").addClass('hidden');
            $(".upAssignment").removeClass('hidden');

        });
        $('.upAssignment').click(function() {
            $(".assignmentModuleNotVisible").addClass('hidden');
            $(".upAssignment").addClass('hidden');
            $(".downAssignment").removeClass('hidden');

        });

        // click down up question
        $('.downquestion').click(function() {
//            $(".assignmentModuleNotVisible").removeClass('hidden');
            $(".downquestion").addClass('hidden');
            $(".upquestion").removeClass('hidden');

        });
        $('.upquestion').click(function() {
//            $(".assignmentModuleNotVisible").addClass('hidden');
            $(".upquestion").addClass('hidden');
            $(".downquestion").removeClass('hidden');

        });

    })(jQuery);
</script>