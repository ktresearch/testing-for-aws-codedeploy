<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewAddCourseOutline extends JViewLegacy {
	function display($tpl = null) {
		global $mainframe;

		$app = JFactory::getApplication();
		$menus = $app->getMenu();
		$user = JFactory::getUser();
		$username = $user->username;

		$params = $app->getParams();
		$this->assignRef('params', $params);

		$id =  JRequest::getVar( 'course_id' , null, 'NEWURLFORM' );
		if (!$id) $id =  JRequest::getVar( 'course_id' );
		if (!$id) $id = $params->get( 'course_id' );

		$id = (int) $id;

		if (!$id) {
                    header('Location: /404.html');
                    exit;
//			echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
//			return;
		}
		$this->hasPermission = JFactory::hasPermission($id, $username);
		if (!$this->hasPermission[0]['hasPermission']) {
                    header('Location: /404.html');
                    exit;
//			echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//			return;
		}
                $coursemods = JoomdleHelperContent::call_method ( 'get_course_mods', (int) $id, '');
                if ($coursemods['status']) {
                    $this->mods = $coursemods['coursemods'];
                } else {
                    $this->mods = array();
                }
//		$this->mods = JoomdleHelperContent::call_method ( 'get_course_mods', (int) $id, '');
		$this->course_id = (int) $id;

		// get module of course have visible = 0;
		$visible = 0;
		$this->mods_not_visible = JoomdleHelperContent::call_method ( 'get_course_mods_visible', (int) $id, '',$visible);

		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_JOOMDLE_COURSE_CONTENT'));

		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

		parent::display($tpl);
	}
}
?>
