<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewMyprogress extends JViewLegacy {
    function display($tpl = null) {
        $user = JFactory::getUser();
        $username = $user->username;

        $app        = JFactory::getApplication();
        $params = $app->getParams();

        $this->assignRef('params',              $params);

        $enrollable_only = $params->get( 'enrollable_only' );
        $show_buttons = $params->get( 'show_buttons' );

        $sort_by = $params->get( 'sort_by', 'name' );

        switch ($sort_by)
        {
            case 'date':
                $order = 'created DESC LIMIT 10';
                break;
            case 'sortorder':
                $order = 'sortorder ASC LIMIT 10';
                break;
            default:
                $order = 'fullname ASC LIMIT 10';
                break;
        }

        $where = '';
        $swhere = '';
//        if (empty($_POST)) {
//        } else if (empty($_POST['type']) || empty($_POST['sort']) || $_POST['type'] != 'ajaxsort') {
//        } else {
        if (!empty($_POST['type']) && !empty($_POST['sort']) && $_POST['type'] == 'ajaxsort') {
            switch ($_POST['sort']) {
                case 'keyword':
                    if (!empty($_POST['keyword'])) {
                        $keyword = $_POST['keyword'];
                    }
                    $where = ' AND (ca.name LIKE \'%'.$keyword.'%\' OR fullname LIKE \'%'.$keyword.'%\' OR shortname LIKE \'%'.$keyword.'%\')';
                    break;
                case 'completed':
                    $where = 'AND status = 50';
                    break;
                case 'inprogress':
                    $where = 'AND status = 25';
                    break;
                case 'notyetstarted':
                    $where = 'AND status IS NULL';
                    break;
                case 'lastweek':
                    $time = time();
                    $lasttime = time() - 60 * 60 * 24 * 7;
                    $swhere = ' AND lastaccess BETWEEN '.$lasttime.' AND '.$time.' ';
                    break;
                case 'lastmonth':
                    $time = time();
                    $lasttime = time() - 60 * 60 * 24 * 30;
                    $swhere = ' AND lastaccess BETWEEN '.$lasttime.' AND '.$time.' ';
                    break;
                case 'sixmonths':
                    $time = time();
                    $lasttime = time() - 60 * 60 * 24 * 30 * 6;
                    $swhere = ' AND lastaccess BETWEEN '.$lasttime.' AND '.$time.' ';
                    break;
                default:
                    $where = '';
                    break;
            }
        }

//		if (($show_buttons) && ($username))
        if ($username)
        {
            $this->cursos = JoomdleHelperContent::getCourseList( (int) $enrollable_only,  $order, 0, $username, $where, $swhere);
        }
        else
            $this->cursos = JoomdleHelperContent::getCourseList( (int) $enrollable_only, $order, 0, '',  $where, $swhere);

        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

        $this->_prepareDocument();

        parent::display($tpl);
    }

    protected function _prepareDocument()
    {
        $app    = JFactory::getApplication();
        $menus  = $app->getMenu();
        $title  = null;

        // Because the application sets a default page title,
        // we need to get it from the menu item itself
        $menu = $menus->getActive();
        if ($menu)
        {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        } else {
            $this->params->def('page_heading', JText::_('COM_JOOMDLE_MY_NEWS'));
        }
    }

}

?>
