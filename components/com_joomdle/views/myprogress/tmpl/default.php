<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
$itemid = JoomdleHelperContent::getMenuItem();
$free_courses_button = $this->params->get( 'free_courses_button' );
$paid_courses_button = $this->params->get( 'paid_courses_button' );
$show_buttons = $this->params->get( 'show_buttons' );
$show_description = $this->params->get( 'show_description' );

$this->params->set('page_heading','My progress');

$session = JFactory::getSession();
if ($session->get('device') == 'mobile') {
?>
<style type="text/css">
	.navbar-header {
        background-color: rgb(248, 166, 32);
    }
    .t3-submenu ul a {
        color: rgb(248, 166, 32);
    }
</style>
<div class="mob my-progress-header">
    <span class="left">
        <?php if ($this->params->get('show_page_heading', 1)) : ?>
            <?php echo $this->escape($this->params->get('page_heading')); ?>
        <?php endif; ?>
    </span>
    <span class="right">View course catalogue</span>
</div>
<div class="mob my-progress-content<?php echo $this->pageclass_sfx;?>">
    <table width="100%" class="tbl-my-progress">
        <thead>
        <tr>
            <th width="8%"><div class="rev"></div></th>
            <th>
                <span class="ct">Course title</span>
                <span class="select-sort">
                    <span class="select-sort-content">Sort</span>
                    <ul class="ul-select-sort">
                        <li opt="keyword">Search</li>
                        <li opt="completed">Completed</li>
                        <li opt="inprogress">In Progress</li>
                        <li opt="notyetstarted">Not yet started</li>
                        <li opt="lastweek">Accessed Last Week</li>
                        <li opt="lastmonth">Accessed Last month</li>
                        <li opt="sixmonths">Accessed Last 6 months</li>
                    </ul>

                </span>
                <div class="form-keyword-sort"><input type="text" placeholder="keyword..."/></div>

            </th>
            <th width="30%">Progress</th>
        </tr>
        </thead>
        <tbody>
        <?php if (!is_array ($this->cursos) || empty($this->cursos)) {
            echo "<tr>";
            echo '<td colspan="3" class="bold cred tcenter bg">No course is found.</td>';
            echo "</tr>";
        } else {
            $i = 1;
            foreach ($this->cursos as  $curso) :
                $cat_id = $curso['cat_id'];
                $course_id = $curso['remoteid'];
                $cat_slug = JFilterOutput::stringURLSafe ($curso['cat_name']);
                $course_slug = JFilterOutput::stringURLSafe ($curso['fullname']);
                $url = JRoute::_("index.php?option=com_joomdle&view=detail&cat_id=$cat_id:$cat_slug&course_id=$course_id:$course_slug&Itemid=$itemid");
                switch ($curso['completion_status']) {
                    case null:
                        if  (array_key_exists('last_access', $curso) && !is_null($curso['last_access'])) {
                            $completion_class = "in-progress";
                            $completion_status = 'In progress';
                            (array_key_exists('last_access', $curso) && !is_null($curso['last_access'])) ? $time = date('Y-m-d', $curso['last_access']) : 'Unknown';
                            $completion_des = 'Last viewed: '.$time;
                            break;
                        } else {
                            $completion_class = "not-yet-started";
                            $completion_status = 'Not yet started';
                            (array_key_exists('enrol_time', $curso) && !is_null($curso['enrol_time'])) ? $time = date('Y-m-d', $curso['enrol_time']) : 'Unknown';
                            $completion_des = 'Subscribed: '.$time;
                            break;
                        }
                    case '25':
                    case '50':
                    case '75':
                        $completion_class = "completed";
                        $completion_status = 'Completed';
                        (array_key_exists('time_completed', $curso) && !is_null($curso['time_completed'])) ? $time = date('Y-m-d', $curso['time_completed']) : 'Unknown';
                        $completion_des = 'Completed: '.$time;
                        break;
                    default:
                        $completion_class = "not-yet-started";
                        $completion_status = 'Not yet started';
                        (array_key_exists('enrol_time', $curso) && !is_null($curso['enrol_time'])) ? $time = date('Y-m-d', $curso['enrol_time']) : 'Unknown';
                        $completion_des = 'Subscribed: '.$time;
                        break;
                }

            ?>
                <tr>
                    <td><span class="num"><?php echo $i;?></span></td>
                    <td>
                        <span class="<?php echo $completion_class;?> course-cat"><?php echo $curso['cat_name'];?></span>
                        <span class="<?php echo $completion_class;?> course-name"><?php  echo "<a href=\"$url\">".$curso['fullname']."</a><br>"; ?></span>
                        <span class="<?php echo $completion_class;?> course-time"><?php echo $completion_des;?></span>
                    </td>
                    <td><span class="<?php echo $completion_class;?> course-status"><?php echo $completion_status;?></span>
                        <span class="<?php echo $completion_class;?> course-button"><?php echo ($completion_class == 'completed')?'Print certificate':'';?></span>
                    </td>

                </tr>
            <?php
            $i++;
            endforeach;
        }?>
        </tbody>
    </table>
<?php } else {

?>
<!--    TABLET-->
    <style type="text/css">
        #t3-content {
            background: white;
        }
        .t3-mainnav .container {
            background-color: #f8a620;
        }
        #t3-content {
            background: white;
        }
        .navbar-logout, .navbar-brand {
            background-color: #f36a25;
        }
        .navbar-title > span {
            color: #f36a25;
        }
    </style>
    <div class="tab my-progress-header">
        <span class="left">

            <span class="select-sort">
                <div class="rev"></div>
                <span class="select-sort-content">Sort</span>
                <ul class="ul-select-sort">
                    <li opt="keyword">Search</li>
                    <li opt="completed">Completed</li>
                    <li opt="inprogress">In Progress</li>
                    <li opt="notyetstarted">Not yet started</li>
                    <li opt="lastweek">Accessed Last Week</li>
                    <li opt="lastmonth">Accessed Last month</li>
                    <li opt="sixmonths">Accessed Last 6 months</li>
                </ul>
            </span>
            <div class="form-keyword-sort"><input type="text" placeholder="keyword..."/></div>
        </span>
        <span class="right">
            <?php
                $Module = &JModuleHelper::getModule('mod_courses_search');
                echo JModuleHelper::renderModule( $Module );
            ?>
        </span>
    </div>

    <div class="tab my-progress-content<?php echo $this->pageclass_sfx;?>">

            <?php if (!is_array ($this->cursos) || empty($this->cursos)) {
                echo "<tr>";
                echo '<td colspan="2" class="bold cred tcenter bg">No course is found.</td>';
                echo "</tr>";
            } else {
                $i = 1;
                foreach ($this->cursos as  $curso) :
                    $cat_id = $curso['cat_id'];
                    $course_id = $curso['remoteid'];
                    $cat_slug = JFilterOutput::stringURLSafe ($curso['cat_name']);
                    $course_slug = JFilterOutput::stringURLSafe ($curso['fullname']);
                    $url = JRoute::_("index.php?option=com_joomdle&view=detail&cat_id=$cat_id:$cat_slug&course_id=$course_id:$course_slug&Itemid=$itemid");

                    switch ($curso['completion_status']) {
                        case null:
                            if (array_key_exists('last_access', $curso) && !is_null($curso['last_access'])) {
                                $completion_class = "in-progress";
                                $completion_status = 'In progress';
                                (array_key_exists('last_access', $curso) && !is_null($curso['last_access'])) ? $time = date('Y-m-d', $curso['last_access']) : 'Unknown';
                                $completion_des = 'Last viewed: '.$time;
                                $course_button = 'Continue learning';
                                break;
                            } else {
                                $completion_class = "not-yet-started";
                                $completion_status = 'Not yet started';
                                (array_key_exists('enrol_time', $curso) && !is_null($curso['enrol_time'])) ? $time = date('Y-m-d', $curso['enrol_time']) : 'Unknown';
                                $completion_des = 'Subscribed: '.$time;
                                $course_button = 'Start learning';
                                break;
                            }
                        case '25':
                        case '50':
                        case '75':
                            $completion_class = "completed";
                            $completion_status = 'Completed';
                            (array_key_exists('time_completed', $curso) && !is_null($curso['time_completed'])) ? $time = date('Y-m-d', $curso['time_completed']) : 'Unknown';
                            $completion_des = 'Completed: '.$time;
                            $course_button = 'Print certificate';
                            $url = 'javascript:void(0);';
                            break;
                        default:
                            $completion_class = "not-yet-started";
                            $completion_status = 'Not yet started';
                            (array_key_exists('enrol_time', $curso) && !is_null($curso['enrol_time'])) ? $time = date('Y-m-d', $curso['enrol_time']) : 'Unknown';
                            $completion_des = 'Subscribed: '.$time;
                            $course_button = 'Start learning';
                            break;
                    }

                    ?>
                    <div class="tab-my-progress-div clear mpcc"></div>
                <table width="100%" class="tbl-my-progress mpcc">
                    <tbody>
                        <tr>
                            <td width="40%"><span class="<?php echo $completion_class;?> course-img">
                                    <img src="http://pasit.heutagon.com/images/course_images.png" alt=""/>
                                </span></td>
                            <td  width="60%">
                                <div class="td-header">
                                    <span class="<?php echo $completion_class;?> course-num"><?php echo 'Course <b>#'. $i.'</b>';?></span>
                                    <span class="<?php echo $completion_class;?> course-time"><?php echo $completion_des;?></span>
                                    <div class="<?php echo $completion_class;?> course-status-img"></div>
                                </div>
                                <div class="td-main">
                                    <span class="<?php echo $completion_class;?> course-name"><?php  echo "<a href=\"$url\">".$curso['fullname']."</a>"; ?></span>
                                    <span class="<?php echo $completion_class;?> course-summary"><?php echo $curso['summary'];?></span>
                                    <span class="<?php echo $completion_class;?> course-button"><a href="<?php echo $url;?>"><?php echo $course_button;?></a></span>
                                </div>
                            </td>

                        </tr>
                    </tbody>
                </table>

                    <?php
                    $i++;
                endforeach;
            }?>
            <div class="tab-my-progress-div clear mpcc"></div>

        <?php
        }?>
        </div>
<script type="text/javascript">
    jQuery(function() {
        jQuery('.mob .select-sort').click(function(e) {
            var target = e.target;
            if (!jQuery(target).is('.select-sort-bt, .tab .rev')) {
                jQuery('.ul-select-sort').fadeToggle();
            }
        });
//        jQuery(document).delegate('.tab.my-progress-header > .left', 'click', function(e) {
        jQuery('.tab.my-progress-header > .left').click(function(e) {
            var target = e.target;
            if (!jQuery(target).is('.select-sort-bt, .tab .rev, .form-keyword-sort input')) {
                jQuery('.ul-select-sort').fadeToggle();
            }
        });
        jQuery('.mob .rev').click(function() {
            jQuery(this).toggleClass('act');
            jQuery(".tbl-my-progress tbody").each(function(elem,index){
                var arr = jQuery.makeArray(jQuery("tr",this).detach());
                arr.reverse();
                jQuery(this).append(arr);
            });
        });
//        jQuery(document).delegate('.tab .rev', 'click', function() {
        jQuery('.tab .rev').click(function() {
            jQuery(this).toggleClass('act');
            jQuery(".tab.my-progress-content").each(function(elem,index){
                var arr = jQuery.makeArray(jQuery(".mpcc",this).detach());
                arr.reverse();
                jQuery(this).append(arr);
            });
        });
        jQuery('.select-sort li').click(function() {
            if (jQuery(this).attr('opt') == 'keyword') {
                jQuery('.select-sort-content').html( jQuery(this).html() );
                jQuery('.select-sort-content').append('<button class="select-sort-bt">'+ jQuery(this).html()+'</button>' );
                jQuery('.form-keyword-sort').stop(true, true).fadeIn();
            } else {
                jQuery('.form-keyword-sort').stop(true, true).fadeOut();
                jQuery('.select-sort-content').html( jQuery(this).html() );
                jQuery.ajax({
                    url: window.location.href,
                    method: 'post',
                    data: {
                        'type': 'ajaxsort',
                        'sort': jQuery(this).attr('opt')
                    },
                    beforeSend: function() {
                        if (jQuery('.my-progress-content').hasClass('mob')) {
                            jQuery('.tbl-my-progress tbody').html( '<tr><td colspan="3" class="tcenter vcenter cred bold">Loading.............</td></tr>' );
                        } else if (jQuery('.my-progress-content').hasClass('tab')) {
                            jQuery('.my-progress-content').html( '<div class="tcenter vcenter cred bold">Loading.............</div>' );
                        }
                    },
                    success: function(result) {
                        if (jQuery('.my-progress-content').hasClass('mob')) {
                            jQuery('.tbl-my-progress tbody').html( jQuery(result).find('.tbl-my-progress tbody').html() );
                        } else if (jQuery('.my-progress-content').hasClass('tab')) {
                            jQuery('.my-progress-content').html( jQuery(result).find('.my-progress-content').html() );
                        }
                    }
                });
            }
        });
        jQuery(document).delegate('.select-sort-bt', 'click', function() {
            if (jQuery('.form-keyword-sort input').val() != '') {
                jQuery.ajax({
                    url: window.location.href,
                    method: 'post',
                    data: {
                        'type': 'ajaxsort',
                        'sort': 'keyword',
                        'keyword': jQuery('.form-keyword-sort input').val()
                    },
                    beforeSend: function() {
                        if (jQuery('.my-progress-content').hasClass('mob')) {
                            jQuery('.tbl-my-progress tbody').html( '<tr><td colspan="3" class="tcenter vcenter cred bold">Loading.............</td></tr>' );
                        } else if (jQuery('.my-progress-content').hasClass('tab')) {
                            jQuery('.my-progress-content').html( '<div class="tcenter vcenter cred bold">Loading.............</div>' );
                        }
                    },
                    success: function(result) {
                        if (jQuery('.my-progress-content').hasClass('mob')) {
                            jQuery('.tbl-my-progress tbody').html( jQuery(result).find('.tbl-my-progress tbody').html() );
                        } else if (jQuery('.my-progress-content').hasClass('tab')) {
                            jQuery('.my-progress-content').html( jQuery(result).find('.my-progress-content').html() );
                        }
                    }
                });
            }
        });
    });
</script>