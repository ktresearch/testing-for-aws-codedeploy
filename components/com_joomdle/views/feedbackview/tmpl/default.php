<?php defined('_JEXEC') or die('Restricted access');
$module_id = $this->module_id;
$course_id = $this->course_id;
$session = JFactory::getSession();
$device = $session->get('device');
$questionnaire = JoomdleHelperContent::call_method ( 'get_info_questionnaire',(int)$module_id,$username);
$number_ques = count($questionnaire['questions']);
require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
?>

<link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/feedbackview/css/feedback.css';?>">
<link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/feedbackview/css/jquery-confirm.css';?>">
<script type="text/javascript" src="<?php echo JURI::base().'components/com_joomdle/views/feedbackview/js/jquery-confirm.js';?>"></script>

<div class="joomdle-course <?php echo $this->pageclass_sfx;?>">

<div class="feedbackview">
	<div class="title">
		<span><?php echo $questionnaire['name'] ?></span>
	</div>
	<div class="img-feedback col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
		<img src="<?php echo JURI::base().'components/com_joomdle/views/feedbackview/tmpl/feedback.jpg';?>" alt="">
	</div>
	<div class="content-feedback col-md-12 col-sm-12 col-xs-12">
		<?php echo $questionnaire['intro']; ?>
	</div>
	<div class="btnn">
        <a href="<?php echo JURI::base().'feedbackview/'.$course_id.'_'.$module_id.'_inview.html';?>">
			<button class="large start-questionnaire">Start Feedback
				<img width="15" height="15" src="<?php echo JURI::base().'components/com_joomdle/views/feedbackview/tmpl/icon_ar_right_white.png';?>" alt="">
			</button>
		</a>
	</div>
</div>
<div class="fac">
	<!-- <a href="<?php //echo JURI::base().'index.php?option=com_joomdle&view=feedbackview&module_id='.$module_id.'&course_id='.$course_id.'&action=review';?>"><button class="btn btn-primary">Facilitator view feedback</button></a> -->
</div>
<div class="joomdle-course1 <?php echo $this->pageclass_sfx;?>">
<script type="text/javascript">
	jQuery(document).ready(function() {
		<?php if($number_ques == 0){ ?>
			jQuery('.start-questionnaire').click(function(e){
				event.stopPropagation();
        		event.preventDefault();
        		jQuery.alert({
	                title: 'Alert!',
	                content: "The feedback no question.",
	                columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
	                theme: 'blue',
	                title: false,
	            });
			});
		<?php } ?>
	});
</script>