<?php
defined('_JEXEC') or die('Restricted access');
$module_id = $this->module_id;
$course_id = $this->course_id;
$session = JFactory::getSession();
$device = $session->get('device');
require_once(JPATH_SITE . '/components/com_community/libraries/core.php');
$questionnaire = $this->questionnaire;
$questions = $questionnaire['questions'];
$feedback_question_apply = array();
foreach ($questions as $ques) {
    if ($ques['deleted'] == 'n') {
        $feedback_question_apply[] = $ques;
    }
}
$num_ques = count($feedback_question_apply);
$feedback = $this->feedback;
$number_ques = count($questionnaire['questions']);
$ques_rank = $feedback['rank'];
$num_rank = count($ques_rank);
$ques_text = $feedback['text'];
$num_text = count($ques_text);

for ($f = 0; $f < count($feedback_question_apply); $f++) {
    if ($feedback_question_apply[$f]['type_ques'] == 'Rate (scale 1..5)') {
        $length_rank = $feedback_question_apply[$f]['length'];
        break;
    }
}
$arr_options = ['Strongly disagree', 'Disagree', 'Neutral', 'Agree', 'Strongly Agree'];
for ($i = 0; $i < $num_ques; $i++) {
    $t = array();
    if ($feedback_question_apply[$i]['type_ques'] == 'Rate (scale 1..5)') {
        $khong = 0;
        $mot = 0;
        $hai = 0;
        $ba = 0;
        $bon = 0;
        $t['id'] = $feedback_question_apply[$i]['id'];
        $t['content'] = $feedback_question_apply[$i]['content'];
        for ($n = 0; $n < $num_rank; $n++) {
            if ($ques_rank[$n]['ques_id'] == $feedback_question_apply[$i]['id']) {
                if ($ques_rank[$n]['ques_response'] == 0) {
                    $khong++;
                } else if ($ques_rank[$n]['ques_response'] == 1) {
                    $mot++;
                } else if ($ques_rank[$n]['ques_response'] == 2) {
                    $hai++;
                } else if ($ques_rank[$n]['ques_response'] == 3) {
                    $ba++;
                } else if ($ques_rank[$n]['ques_response'] == 4) {
                    $bon++;
                }
                $t['response']['0'] = $khong;
                $t['response']['1'] = $mot;
                $t['response']['2'] = $hai;
                $t['response']['3'] = $ba;
                $t['response']['4'] = $bon;
            }
        }
        $power['rank'][] = $t;
    } else if ($feedback_question_apply[$i]['type_ques'] == 'Text Box') {
        $t['id'] = $feedback_question_apply[$i]['id'];
        $t['content'] = $feedback_question_apply[$i]['content'];
        for ($j = 0; $j < $num_text; $j++) {
            if ($ques_text[$j]['ques_id'] == $feedback_question_apply[$i]['id']) {
                $a['user'] = $ques_text[$j]['user'];
                $get_user_id_joomla_by_username = JUserHelper::getUserId($a['user']);
                $users = CFactory::getUser($get_user_id_joomla_by_username);
                $a['name'] = $users->getName();
                $a['avatar'] = $users->getAvatar();
                $a['res'] = $ques_text[$j]['ques_response'];
                $t['response'][] = $a;
            }
        }
        $power['text'][] = $t;
    }
}
$rank = $power['rank'];
$count_rank = count($rank);
$text = $power['text'];
$count_text = count($text);
$zero = 0;
$one = 0;
$two = 0;
$three = 0;
$four = 0;
if (isset($_POST['create_pdf'])) {

    require_once(JPATH_SITE . '/components/com_joomdle/views/feedbackview/tmpl/tcpdf/examples/lang/eng.php');
    require_once(JPATH_SITE . '/components/com_joomdle/views/feedbackview/tmpl/tcpdf/tcpdf.php');
    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("Export HTML Table data to PDF using TCPDF in PHP");
    $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(false);
    $obj_pdf->setPrintFooter(false);
    $obj_pdf->SetAutoPageBreak(TRUE, 10);
    $obj_pdf->SetFont('helvetica', '', 12);
    $obj_pdf->AddPage();
    $content = '<h3 style="text-align:center;color:#4386F8;">Report Feedback for Facilitator</h3><div class="chart">
	<div id="container" style="color:#4E606A;min-width: 250px; height: 250px; max-width: 600px; margin: 0 auto"></div>
	</div><div class="span-review" style="color:#4E606A;padding-bottom:20px;">Likert Scale (Rate the statement form a scale of 1-' . $length_rank . ')</div><div style="padding-top:10px"></div><table style="color:#4E606A;width:100%;"><tr><td style=""><center></center></td>';
    for ($n = 0; $n < $length_rank; $n++) {
        $o = $n + 1;
        if ($n == $length_rank - 1) {
            $toann = 'border-right:1px #fff;';
        } else {
            $toann = '';
        };
        $content .= '<td style="text-align:center;border:1px solid #4AD8FE;border-top:1px #fff;' . $toann . '">' . $o . "<br/>" . $arr_options[$n] . '</td>';
    }
    $tt = 0;
    $content .= "</tr>";
    for ($l = 0; $l < $count_rank; $l++) {
        $zero = $rank[$l]['response']['0'] + $zero;
        $one = $rank[$l]['response']['1'] + $one;
        $two = $rank[$l]['response']['2'] + $two;
        $three = $rank[$l]['response']['3'] + $three;
        $four = $rank[$l]['response']['4'] + $four;
        $m = $l + 1;
        $content .= "<tr>";
        $content .= '<td class="text-ques" style="border:1px solid #4AD8FE;border-left:1px #fff;">
							<b>Question ' . $m . '</b> <br/>
							' . $rank[$l]['content'] . '
						</td>';
        for ($n = 0; $n < $length_rank; $n++) {
            if ($n == $length_rank - 1) {
                $toannn = 'border-right:1px #fff;';
            } else {
                $toannn = '';
            };
            $content .= '<td class="sl" style="text-align:center;margin-top:10px;border:1px solid #4AD8FE;' . $toannn . '"><center>' . $rank[$l]['response'][$n] . '</center></td>';
        }
        $content .= "</tr>";
        $tt++;
    }
    $content .= '</table>';
    $content .= '<div style="padding-top:20px"></div>';
    for ($y = 0; $y < $count_text; $y++) {
        $tt++;
        $content .= '<div class="ques-text-item">';
        $content .= '<b style="color:#4E606A;">Question ' . $tt . '</b><br/>';
        $content .= '<span style="color:#4E606A;">' . $text[$y]['content'] . '</span>';
        $content .= '<div style="padding-top:20px"></div>';
        $content .= '<table border="1"  class="review-feedback" style="color:#4E606A;text-align:center;border:1px solid #4AD8FE;"><thead class="options"><tr>
						<td class="response-text" style="border:1px solid #4AD8FE;border-top:1px #fff;border-left:1px #fff;"><center>Text Response</center></td>
						<td class="name-response" style="border:1px solid #4AD8FE;border-top:1px #fff;border-right:1px #fff;"><center>Student</center></td>
					</tr>
					</thead>
					<tbody class="options">';
        for ($r = 0; $r < count($text[$y]['response']); $r++) {
            $content .= '<tr>
							<td style="border:1px solid #4AD8FE;border-left:1px #fff;">' . $text[$y]['response'][$r]['res'] . '</td>
							<td style="border:1px solid #4AD8FE;border-right:1px #fff;"><center>' . $text[$y]['response'][$r]['name'] . '</center></td>
						</tr>';
        }
        $content .= '</tbody></table>';
        $content .= '</div>';
    }
    $content .= '</tr><table>';
    $obj_pdf->writeHTML($content);
    $obj_pdf->Output('feedback_' . $module_id . '.pdf', 'D');
    die;
}

//require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');

$role = $this->hasPermission;

require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);
$render_banner = $banner->renderBanner($this->course_info, $this->mods, $role);
?>
<script type="text/javascript"
        src="<?php echo JURI::base() . 'components/com_joomdle/views/feedbackview/js/jquery-confirm.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JURI::base() . 'components/com_joomdle/views/feedbackview/js/exporting.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JURI::base() . 'components/com_joomdle/views/feedbackview/js/highcharts.js'; ?>"></script>
<!--<link rel="stylesheet"-->
<!--      href="--><?php //echo JURI::base() . 'components/com_joomdle/views/feedbackview/css/feedback.css'; ?><!--"> <!--css-->

<div class="joomdle-course toan_pdf <?php echo $this->pageclass_sfx; ?>">

    <p class="feedback-title"><?php echo $questionnaire['name']; ?></p>
    <div class="start-review container-fluid row">
        <!--        Block likert scale-->
        <div class="nav-tabs col-xs-12 col-sm-4 col-md-4">
            <div id="tab0" class="tabs block-likert-scale <?php echo 'tabs-' . $device; ?>">
                <p>Likert Scale Responses</p>
                <div class="image-center">
                    <img class="icon-next" src="/images/backbutton.png" width="13">
                </div>
            </div>

            <?php for ($y = 0; $y < $count_text; $y++) { ?>
                <div id="<?php echo 'tab' . ($y + 1); ?>" class="tabs block-comment <?php echo 'tabs-' . $device; ?>">
                    <p><?php echo $text[$y]['content']; ?></p>
                    <div class="image-center">
                        <img class="icon-next" src="/images/backbutton.png" width="13">
                    </div>
                </div>
            <?php } ?>

            <form method="post" class="download_pdf">
                <input type="submit" name="create_pdf"  class="btDownloadCourseFeedback" value="Download Course Feedback">
            </form>

            <!--            <div class="btDownloadCourseFeedback">Download Course Feedback</div>-->

        </div>

        <div class="tab-content col-xs-12 col-sm-8 col-md-8">
            <div id="tab0-content" class="tabs-content">
                <div id="chart">
                    <p class="chart_header">Rate the following statements on a scare of 1 - <?php echo $length_rank; ?>
                        . </p>
                    <?php for ($l = 0; $l < $count_rank; $l++) {
                        $zero = $rank[$l]['response']['0'] + $zero;
                        $one = $rank[$l]['response']['1'] + $one;
                        $two = $rank[$l]['response']['2'] + $two;
                        $three = $rank[$l]['response']['3'] + $three;
                        $four = $rank[$l]['response']['4'] + $four;
                        ?>
                        <div class="block-question">
                            <?php
                            echo "<p class='chart_sub_header'>" . $rank[$l]['content'] . "</p>";
                            $tong[$l] = 0;
                            for ($k = 0; $k < $length_rank; $k++) {
                                $tong[$l] = $tong[$l] + $rank[$l]['response'][$k];
                            }
                            ?>
                            <ul id="bars" class="row">
                                <?php for ($n = 0; $n < $length_rank; $n++) { ?>
                                    <li>
                                        <div data-percentage="<?php echo(($rank[$l]['response'][$n] / $tong[$l]) * 100); ?>"
                                             class="bar"></div>
                                        <span><?php echo $arr_options[$n] . " (" . $rank[$l]['response'][$n] . ")"; ?></span>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>

                    <?php } ?>
                </div>
            </div>
            <?php
            for ($y = 0; $y < $count_text; $y++) { ?>
                <div id="<?php echo 'tab' . ($y + 1) . '-content'; ?>" class="tabs-content block-comment-content">
                    <div class="" id="user_comment">
                        <div class="sub_header_mobile">Comments</div>
                        <p class="user_comment_header"><?php echo $text[$y]['content']; ?></p>
                        <?php for ($r = 0; $r < count($text[$y]['response']); $r++) { ?>
                            <div class="user_ideal row">
                                <div class="col-xs-2 col-sm-2 col-md-2">
                                    <img src="<?php echo $text[$y]['response'][$r]['avatar']; ?>"
                                         alt="<?php echo $text[$y]['response'][$r]['name']; ?>" height="42" width="42">
                                </div>
                                <div class="col-xs-10 col-sm-10 col-md-10">
                                    <div class="col-xs-11 col-sm-11 col-md-11 col-xs-offset-1 col-sm-offset-1 col-md-offset-1">
                                        <p class="username"><?php echo $text[$y]['response'][$r]['name']; ?></p>
                                        <p class="comment"><?php echo $text[$y]['response'][$r]['res']; ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>


<div class="clear"></div>
<style type="text/css">
    .image-center img {
        -webkit-transform: rotate(180deg);
        -moz-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        -o-transform: rotate(180deg);
        transform: rotate(180deg);
    }
</style>

<script>
    (function ($) {
        $("#bars li .bar").each(function (key, bar) {
            var percentage = $(this).data('percentage');

            $(this).animate({
                'height': percentage + '%'
            }, 1000);
        });

        $("#tab0").addClass('actived');
        $("#tab0-content").addClass('actived');
        $(".nav-tabs").addClass('actived');

// click tabs on mobile device
        $(".tabs-mobile").click(function () {
            if ($(".tabs").hasClass('actived')) {
                $(".tabs").removeClass('actived');
            }
            if ($(".tabs-content").hasClass('actived')) {
                $(".tabs-content").removeClass('actived');
            }
            $(".nav-tabs").removeClass('actived');
            $(".tab-content").addClass('actived');
            $(".back-left").addClass('hidden');

            $(".header-title").before('<div class="back-feedback"><a><img src="/images/Back-button.png"></a></div>');
            $(this).addClass('actived');
            var tab = $(this).attr('id');
            $("#" + tab + '-content').addClass('actived');

            $(".back-feedback").click(function () {
                $(".nav-tabs").addClass('actived');
                $(".tab-content").removeClass('actived');
                $(".back-left").removeClass('hidden');
                $(".back-feedback").remove();
            });


        });

// click tabs on desktop device
        $(".tabs-desktop").click(function () {
            if ($(".tabs").hasClass('actived')) {
                $(".tabs").removeClass('actived');
            }
            if ($(".tabs-content").hasClass('actived')) {
                $(".tabs-content").removeClass('actived');
            }
            $(".nav-tabs").removeClass('actived');
            $(".tab-content").addClass('actived');
            $(this).addClass('actived');
            var tab = $(this).attr('id');
            $("#" + tab + '-content').addClass('actived');
        });

        $(".back-feedback").click(function () {
            $(".nav-tabs").addClass('actived');
            $(".tab-content").removeClass('actived');
            $(".back-left").removeClass('hidden');
            $(".back-feedback").remove();
        });
    })(jQuery);

</script>