<?php defined('_JEXEC') or die('Restricted access');
$session = JFactory::getSession();
$module_id = $this->module_id; // module_id id module
$course_id = $this->course_id; // course_id id course
$device = $session->get('device');

$questionnaire = $this->questionnaire;

$feedback_question_apply = array();
foreach ($questionnaire['questions'] as $ques) {
    if($ques['deleted'] == 'n') {
        $feedback_question_apply[] = $ques;
    }
}

$number_ques = count($feedback_question_apply); // number question in questionnaire

$this->wrapperurl = JURI::base().'courses/mod/questionnaire/complete.php?id='.$module_id; // url iframe
$arr_rate = ['Strongly disagree','Disagree','Neutral','Agree','Strongly Agree']; // array options only for question type rate
//require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
$role = $this->hasPermission;

require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);
$render_banner = $banner->renderBanner($this->course_info, $this->mods, $role);

?>
<link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/feedbackview/css/feedback.css';?>"> <!--css-->
<link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/feedbackview/css/jquery-confirm.css';?>">
<script type="text/javascript" src="<?php echo JURI::base().'components/com_joomdle/views/feedbackview/js/jquery-confirm.js';?>"></script>
<div class="joomdle-course <?php echo $this->pageclass_sfx;?>">
    <div class="feedback_learner">
        <?php for ($i=0; $i < $number_ques; $i++) {  //for $questionnaire['questions'] - list questions
            $length = (int)$feedback_question_apply[$i]['length']; //length answer
            $id_ques = (int)$feedback_question_apply[$i]['id']; //id question

            if ($feedback_question_apply[$i]['type_ques'] == 'Rate (scale 1..5)') { //(This if) for question with type = rate
                if($length==5){
                    if($device=='mobile'){
                        $css_span = 'style="margin-left:16%;"';
                        $css_text = 'style="left: -28px;width: 60px;"';
                    }else{
                        $css_span = 'style="margin-left:16%;"';
                        $css_text = 'style="left: -40px;width: 80px;"';
                    }
                }elseif($length==4){ //css for number answer - only question type rate
                    if($device=='mobile'){
                        $css_span = 'style="margin-left:18%;"';
                        $css_text = 'style="left: -40px;width: 80px;"';
                    }else{
                        $css_span = 'style="margin-left:19%;"';
                        $css_text = 'style="left: -75px;width: 150px;"';
                    }
                }else if($length==3){
                    if($device=='mobile'){
                        $css_span = 'style="margin-left:22.5%;"';
                        $css_text = 'style="left: -43px;width: 90px;"';
                    }else{
                        $css_span = 'style="margin-left:24%;"';
                        $css_text = 'style="left: -75px;width: 150px;"';
                    }
                }else if($length==2){
                    if($device=='mobile'){
                        $css_span = 'style="margin-left:31%;"';
                        $css_text = 'style="left: -53px;width: 110px;"';
                    }else{
                        $css_span = 'style="margin-left:31.5%;"';
                        $css_text = 'style="left: -75px;width: 150px;"';
                    }
                }else if($length==1){
                    if($device=='mobile'){
                        $css_span = 'style="margin-left:48%;"';
                        $css_text = 'style="left: -53px;width: 110px;"';
                    }else{
                        $css_span = 'style="margin-left:49.5%;"';
                        $css_text = 'style="left: -75px;width: 150px;"';
                    }
                }
                ?>

                <div  class="feedbackview question<?php echo $i+1; ?>" >
                    <div class="title">
                        <span><?php echo $questionnaire['name'] ?></span>
                    </div>
                    <div class="number-ques">
                        <!--		<span>Question --><?php //echo $i+1; ?><!--</span>-->
                        <p class="type_des">
                            Rate the following statements on a scale of 1 - <?php echo $length; ?> :
                        </p>
                        <p class="question_des">
                            <?php echo $feedback_question_apply[$i]['content']; ?>
                        </p>

                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <ul class="rate-point">
                            <?php for ($j=0; $j < $length; $j++) { ?>
                                <span value="<?php echo $j; ?>" data="<?php echo $id_ques; ?>" class="item-point" <?php echo $css_span; ?> >
                    <span class="stt"><?php echo $j+1; ?></span>
                    <span class="point-se point-se-<?php echo $id_ques; ?>"></span>
                    <span class="text" <?php echo $css_text; ?>><?php echo $arr_rate[$j]; ?></span></span>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php if($i == 0){ ?>
                        <p class="note_des">
                            Note: You can only submit course feedback once.
                        </p>
                    <?php } ?>
                </div>
            <?php }else if($feedback_question_apply[$i]['type_ques']=='Text Box'){ // for question with type = text box ?>
                <div class="feedbackview question<?php echo $i+1; ?>">
                    <div class="title">
                        <span><?php echo $questionnaire['name'] ?></span>
                    </div>
                    <div class="number-ques" >
                        <!--		<span>Question --><?php //echo $i+1; ?><!--</span>-->
                        <p class="type_des">
                            Comment
                        </p>
                        <p class="question_des">
                            <?php echo $feedback_question_apply[$i]['content']; ?>
                        </p>
                        <textarea  class="textarea-feedback textarea-<?php echo $id_ques; ?>" placeholder="Write your feedback here" maxlength="<?php echo $feedback_question_apply[$i]['length']; ?>" data="<?php echo $id_ques; ?>"></textarea>
                    </div>
                </div>
            <?php  } //(End if type questions)?>
        <?php } //(End for $questionnaire['questions'] - list questions) ?>
        <center class="seri-ques">

            <button class="btn-cancel" >Cancel </button>

            <button class="btn-submit" >Submit </button>

            <div class="div-popup"></div>
            <!-- <div class="notification"></div> -->

            <?php if($device == 'mobile'){
                $class_style = "all-point-mobile";
            } else {
                $class_style = "all-point-tablet";
            }
            ?>
            <!--            <div class="all-point --><?php //echo $class_style; ?><!--">-->
            <!--                --><?php //for ( $i=0; $i<$number_ques; $i++){ ?>
            <!--                    <span class="point--><?php //echo $i+1; ?><!-- point"></span>-->
            <!--                --><?php //} ?>
            <!--            </div>-->

        </center>

        <!------ --------------------- -->


        <div class="next-question-before">
            <img src="<?php echo JURI::base().'/images/PreviousIcon.png';?>">
        </div>
        <div class="next-question-after">
            <img src="<?php echo JURI::base().'/images/NextIcon.png';?>">
        </div>

        <div class="aaaaa">
            <iframe
                    id="iframe-questionnaire"
                    class="autoHeight"
                    src="<?php echo $this->wrapperurl; ?>"
                    width="<?php echo $this->params->get('width', '100%'); ?>"
                    scrolling="<?php //echo $this->params->get('scrolling', 'auto'); ?>yes"
                    allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"

                <?php if (!$this->params->get('autoheight', 1)) { ?>
                    height="<?php echo $this->params->get('height', '500'); ?>"
                    <?php
                }
                ?>
                    align="top"
                    frameborder="0"
                <?php if ($this->params->get('autoheight', 1)) { ?>
                    onload='itspower(this, false, true, 20)'
                    <?php
                }
                ?>
            ></iframe>
        </div>
    </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function(){
        /*----------------------------Answer question text--------------------------------------*/
        /*----------------------------Answer question text--------------------------------------*/
        var myVar;
        var id;

        jQuery('.textarea-feedback').click(function(){
            id = jQuery(this).attr('data');
            myFunction(id);
        });
        function myFunction(id) {
            myVar = setInterval(function(){
                jQuery('#iframe-questionnaire').contents().find("#qn-"+id+" .qn-answer input[type='text']").val(jQuery('.textarea-'+id).val());
            }, 1000);
        }

        function myStopFunction() {
            clearTimeout(myVar);
        }

        /*----------------------------End answer question text--------------------------------------*/
        /*----------------------------End answer question text--------------------------------------*/
        /*----------------------------Answer question rate--------------------------------------*/
        /*----------------------------Answer question rate--------------------------------------*/

        jQuery('.point-se').click(function(){
            var id = jQuery(this).parent().attr('data');
            jQuery('.point-se-'+id).removeClass('light-point');
            jQuery(this).addClass('light-point');
        });
        jQuery('.text').click(function(){
            var id = jQuery(this).parent().attr('data');
            jQuery('.point-se-'+id).removeClass('light-point');
            jQuery(this).parent().find('.point-se').addClass('light-point');
        });

        jQuery('.item-point').click(function(){
            var id = jQuery(this).attr('data');
            var value = jQuery(this).attr('value');
            jQuery('#iframe-questionnaire').contents().find("#qn-"+id+" .qn-answer table tbody .raterow td input[value='"+value+"']").click();
        });

        /*----------------------------End answer question rate--------------------------------------*/
        /*----------------------------End answer question rate--------------------------------------*/

        /* ------------------------------jQuery next question-------------------------- */
        /* ------------------------------jQuery next question-------------------------- */
        /* ------------------------------jQuery next question-------------------------- */

        var stt_ques = 1;
        var next = stt_ques + 1;
        var prev = stt_ques - 1;
        jQuery('.next-question-before').hide();

        jQuery('.next-question-before img').click(function(){
            next = stt_ques + 1;
            prev = stt_ques - 1;
            stt_ques--;
            if(prev == 1){
                myStopFunction();
                jQuery('.feedbackview').hide();
                jQuery('.question'+prev).show();
                jQuery('.next-question-before').hide();
                jQuery('.all-point span').removeClass('pointactive');
                jQuery('.point'+prev).addClass('pointactive');
            }else{
                myStopFunction();
                jQuery('.feedbackview').hide();
                jQuery('.question'+prev).show();
                if(stt_ques < <?php echo $number_ques; ?>){
                    jQuery('.next-question-after').show();
                    jQuery('.btn-submit').hide();
                    jQuery('.btn-cancel').hide();
                }
                jQuery('.all-point span').removeClass('pointactive');
                jQuery('.point'+stt_ques).addClass('pointactive');
            }
        });

        jQuery('.next-question-after img').click(function(){
            next = stt_ques + 1;
            prev = stt_ques - 1;
            stt_ques++;
            myStopFunction();
            jQuery('.feedbackview').hide();
            jQuery('.question'+next).show();
            jQuery('.next-question-before').show();
            if(stt_ques == <?php echo $number_ques; ?>){
                jQuery('.next-question-after').hide();
                jQuery('.btn-submit').show();
                jQuery('.btn-cancel').show();
            }
            jQuery('.all-point span').removeClass('pointactive');
            jQuery('.point'+stt_ques).addClass('pointactive');
        });

        jQuery('.btn-submit').click(function(){
            lgtCreatePopup('confirm', {content: 'You can only submit course feedback once, proceed?'}, function() {
                lgtCreatePopup('', {'content': 'Loading...'});
                jQuery('#iframe-questionnaire').contents().find(".buttons input[value='Submit questionnaire']").click().delay(1500).queue(function(){
                    window.location.href = '<?php echo JURI::base().'course/'.$course_id.'.html';?>';
                });
            })
        });

        jQuery('.btn-cancel').click(function(){
            window.location.href = '<?php echo JURI::base().'course/'.$course_id.'.html';?>';
        });

        jQuery('.feedbackview').hide();
        jQuery('.btn-submit').hide();
        jQuery('.btn-cancel').hide();
        jQuery('.question1').show();
        jQuery('.point1').addClass('pointactive');

    });
</script>