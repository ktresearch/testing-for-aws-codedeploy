<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewFeedbackView extends JViewLegacy {
    function display($tpl = null) {
        // ==================important=======================
        global $mainframe;
        $app = JFactory::getApplication();
        $pathway = $app->getPathWay();
        $menus = $app->getMenu();
        $menu = $menus->getActive();
        $params = $app->getParams();
        $this->assignRef('params', $params);
        $user = JFactory::getUser();
        $document = JFactory::getDocument();
        $username = $user->username;
        $course_id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );

        if ($params->get('use_new_performance_method'))
            $course_data = JHelperLGT::getCoursePageData(['id' => $course_id, 'username' => $username]);
        else
            $course_data = JoomdleHelperContent::call_method('get_course_page_data', (int)$course_id, $username);

        $this->course_info = json_decode($course_data['course_info'], true);
        $this->mods = json_decode($course_data['mods'], true);
        /* Check course enrol */
        $this->is_enroled = $this->course_info['enroled'];
        $userRoles = json_decode($course_data['userRoles'], true);
        $this->hasPermission = $userRoles['roles'];

        if ($this->is_enroled == 0 && !$this->hasPermission[0]['hasPermission']) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_COURSE_NOT_ENROLL');
//            return;
        }

        if (!$course_id)
            $course_id = $params->get( 'course_id' );
        $this->course_id = (int) $course_id;
        $module_id = JRequest::getVar( 'module_id', null, 'NEWURLFORM' );
        if (!$module_id)
            $module_id = $params->get( 'module_id' );
        $this->module_id = (int) $module_id;

        if (!$course_id)
        {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
//            return;
        }
        $act = JRequest::getVar( 'action', null, 'NEWURLFORM' );

        if($act != NULL){
            switch ($act) {
                case 'inview':
                    $document->setTitle('Feedback');
                    if (!isset($_SERVER['HTTP_REFERER'])) $document->setHTMLTagClass('learning-highlight');

                    if ($params->get('use_new_performance_method')) {
                        $this->questionnaire = JHelperLGT::getModuleInfo('questionnaire', $module_id, $course_id);
                    } else {
                        $this->questionnaire = JoomdleHelperContent::call_method ( 'get_info_questionnaire', (int)$module_id, $username);
                    }
                    $this->checkCompleteFeedback = $course_data['checkCompleteFeedback'];
                    if ($this->checkCompleteFeedback){
                        echo JText::_("COM_JOOMDLE_URL_CAN_NOT_ACCESS");
                        return;
                    }
                    parent::display('inview');return;
                    break;
                case 'review':
                    $document->setTitle('Feedback');
                    if (!isset($_SERVER['HTTP_REFERER'])) $document->setHTMLTagClass('manage-highlight');

                    if ($params->get('use_new_performance_method')) {
                        $this->questionnaire = JHelperLGT::getModuleInfo('questionnaire', $module_id, $course_id);
                        $this->feedback = JHelperLGT::getInfoFeedback($this->questionnaire['surveyid']);
                    } else {
                        $this->questionnaire = JoomdleHelperContent::call_method ( 'get_info_questionnaire', (int)$module_id, $username);
                        $this->feedback = JoomdleHelperContent::call_method ( 'get_info_feedback', (int)$this->questionnaire['surveyid']);
                    }

                    parent::display('review');return;
                    break;
                default:
                    break;
            }
        }else{
            $document->setTitle('Feedback');
            if (!isset($_SERVER['HTTP_REFERER'])) $document->setHTMLTagClass('learning-highlight');

            parent::display($tpl);
        }
    }
}
?>
