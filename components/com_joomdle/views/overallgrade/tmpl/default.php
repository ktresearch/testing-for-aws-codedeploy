<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * @date: 25 june 2016
 * @author: KV team
 * @package: joomdle view progress (for facilitator)
 */

defined('_JEXEC') or die('Restricted access');

JFactory::getDocument()->setTitle('Overview');
$itemid = JoomdleHelperContent::getMenuItem();
$username = JFactory::getUser()->username;

require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
?>
<div class="joomdle-overview-header">
<?php
require_once(JPATH_SITE.'/components/com_joomdle/views/header_facilitator.php');
?>
</div>

<div class="joomdle-course joomdle-overview-content <?php echo $this->pageclass_sfx?>">
    <h2 class="title-course"><?php echo $this->coursefullname; ?></h2>
    <ul class="joomdle-list" id="joomdle-list">
    <?php 
    // begin render student in course
    $i = 0;
    if(is_array($this->overview)) {
        foreach ($this->overview as $student) {
            $users_joomla = CFactory::getUser($student['username']);
            if ($student['username'] == $username) continue;
            $user_by_username = JFactory::getUser($student['username']); 
            if($user_by_username->id <= 0) {
                $avatar = '/components/com_community/assets/user-Male.png';
            } else {
                $users = CFactory::getUser($user_by_username->id);
                // get avatar
                $avatar = $users->getAvatar();
            }
            $final_grade = 0;
            $finalgradeletters = '';
            // foreach ($student['grades'] as $grade) {
                $final_grade = $student['grades'][0]['finalgrade']; 
                $finalgradeletters = $student['grades'][0]['finalgradeletters'];
            // }
            //Trung KKV - Update: first record is main grade category
            ?>
        <li class="list-user-grade">
            <div class="avatar-overall">
                <a class="nav-avatar">
                    <img alt="<?php echo $student['firstname'].' '.$student['lastname']; ?>" src="<?php echo $avatar; ?>"/>
                </a>
            </div>
            <div class="user-grade">
                <div class="name"><?php echo $student['firstname'].' '.$student['lastname']; ?></div>
                <div class="grade"><?php echo JText::_('COM_JOOMDLE_COURSE_OVERALLGRADE'); ?>: <?php echo $finalgradeletters; ?></div>
                
                <div class="edit-grade">
<!--                    <a href="--><?php //echo JUri::base().'editgrade/'.$this->course.'_'.$student['username'].'_'.$student['firstname'].'_'.$student['lastname'].'.html'; ?><!--">--><?php //echo JText::_('COM_JOOMDLE_COURSE_EDIT_GRADE'); ?><!--</a>-->
                    <a href="<?php echo JUri::base() . "editgrade/".$users_joomla->id."_". $this->course . ".html";?>"><?php echo JText::_('COM_JOOMDLE_COURSE_EDIT_GRADE'); ?></a>
                </div>
            </div>
        </li>
        <?php
            $i++;
        }
    }
    ?>
    </ul>
    <?php
    if ($i == 0) {
        echo '<div class="bold cred tcenter bg">'.JText::_('COM_JOOMDLE_NO_USERS_ENROLLED').'</div>';
    }
    ?>
    <div class="loadmore" style="float: right;display: none;">
        <a class="load_more"><?php echo JText::_('COM_COMMUNITY_INBOX_LOAD_MORE'); ?></a>
        <!--<input type="hidden" name="limitfrom" id="limitfrom" value="<?php // echo $limitfrom;?>"/>-->
    </div>
</div>
<script src="components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var size_li = $("#joomdle-list li").size();
        var x = 20; 
        $('#joomdle-list li:lt('+x+')').show();
        if (x < size_li) {
            $('.loadmore').css("display", "block");
        }
        $('.load_more').click(function () {
            x= (x+5 <= size_li) ? x+20 : size_li;
            $('#joomdle-list li:lt('+x+')').show();
            if (x < size_li) {
                $('.loadmore').css("display", "block");
            } else {
                $('.loadmore').css("display", "none");
            }
        });
    });
//    $(document).ready(function() {
//            var limitfrom = $("#limitfrom").val(); //total loaded record group(s)
//            var loading  = false; //to prevents multipal ajax loads
//            var limitnum = <?php // echo $limitnum?>; //total record group(s)
//            var courseid = <?php // echo $this->course;?>;
//            var i = 1;
//
//            $(".load_more").click(function(){
//                i++;
//
//                $button = $(this);
//                $button.html('<?php // echo JText::_('COM_COMMUNITY_LOADING'); ?>');
////                if(track_load <= total_groups && loading==false) //there's more data to load
////                {
//
//                    loading = true; //prevent further ajax loading
//                    $('.animation_image').show(); //show loading image
//
//                    jQuery.ajax({
//                        url: window.location.href,
//                        method: 'post',
//                        data: {
//                            'course_id': courseid,
//                            'limitfrom': limitfrom,
//                            'limitnum': limitnum
//                        },
//                        beforeSend: function() {
//                            $button = $(this);
//                            $button.html('<?php // echo JText::_('COM_COMMUNITY_LOADING'); ?>');
//
//                            loading = true; //prevent further ajax loading
//                            $('.animation_image').show(); //show loading image
//                        },
//                        success: function(result) {
//                            $(".joomdle-list").append(result);
//                            loading = false;
//                            $button.html('<?php // echo JText::_('COM_COMMUNITY_INBOX_LOAD_MORE'); ?>');
//                            $("#limitfrom").val(limitfrom);
//                        }
//                    });
//
////                }
//
//            });
//
//        });
</script>
