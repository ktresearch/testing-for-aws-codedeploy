<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * @date: 25 june 2016
 * @author: KV team
 * @package: joomdle course overview
 */


// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class JoomdleViewOverallgrade extends JViewLegacy {
    function display($tpl = null) {
        
        $app = JFactory::getApplication();
        $params = $app->getParams();
        $this->assignRef('params', $params);
        $user = JFactory::getUser();
        $username = $user->username;

        $id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
        if (!$id) $id =  JRequest::getVar( 'course_id' );
        if (!$id)
            $id = $params->get( 'course_id' );

        $id = (int) $id;

        $this->hasPermission = JFactory::hasPermission($id, $username);
        $user_role = array();
        if (!empty($this->hasPermission)) {
            foreach (json_decode($this->hasPermission[0]['role']) as $r) {
                $user_role[] = $r->sortname;
            }
        }
        if ($user_role[0] != 'manager'  && $user_role[0] != 'teacher' && $user_role[0] != 'editingteacher' && $user_role[0] != 'coursecreator') {
            echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
            return;
        }

        if (!$id) {
            echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
            return;
        }

        $coursename = JRequest::getVar('course');

        if (isset($coursename)) {
            $this->coursefullname = $coursename;
        } else {
            $course_info = JoomdleHelperContent::getCourseInfo($id, $username);
            $this->coursefullname = $course_info['fullname'];
        }
        $this->course = $id;

        $limitfrom = '';
        $limitnum = '';

        $this->overview = JoomdleHelperContent::call_method ( 'teacher_get_course_grades', (int) $id, '', $limitfrom, $limitnum);

        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

        parent::display($tpl);
    }
}

