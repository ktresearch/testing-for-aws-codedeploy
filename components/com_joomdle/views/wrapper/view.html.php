<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrésa
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewWrapper extends JViewLegacy {
    function display($tpl = null) {
        global $mainframe;

        $app = JFactory::getApplication();
        $params = $app->getParams();
        $this->assignRef('params', $params);
        $user = JFactory::getUser();
        $username = $user->username;

        $this->wrapper = new JObject ();
        $this->wrapper->load = '';

        $mtype =  JRequest::getVar( 'mtype', null, 'NEWURLFORM' );
        if (!$mtype) $mtype = JRequest::getVar( 'moodle_page_type' );
        if (!$mtype)
            $mtype = $params->get( 'moodle_page_type' );
        if (!$mtype)
            $mtype =  JRequest::getVar( 'mtype' );

        $type =  JRequest::getVar( 'type', null, 'NEWURLFORM' );
        if (!$type)
            $type = $params->get( 'type' );
        if (!$type)
            $type =  JRequest::getVar( 'type' );
        if (!$type)
            $typeview = '&type='.$type;
        else
            $typeview = '';

        $courseid = $params->get( 'course_id' );

        $courseid = JRequest::getVar( 'course_id' , null, 'NEWURLFORM');
        if (!$courseid) $courseid = JRequest::getVar( 'course_id' );

        $id =  JRequest::getVar( 'id', null, 'NEWURLFORM');
        if (!$id) $id =  JRequest::getVar( 'id' );

        $day =  JRequest::getVar( 'day' );
        $mon =  JRequest::getVar( 'mon' );
        $year =  JRequest::getVar( 'year' );

        $lang =  JRequest::getVar( 'lang' );

        $topic =  JRequest::getVar( 'topic' );
        $redirect =  JRequest::getVar( 'redirect' );

        switch ($mtype)
        {
            case "course" :
                $path = '/course/view.php?id=';
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$id.$typeview;
                if ($topic)
                    $this->wrapper->url .= '&topic='.$topic;
                break;
            case "news" :
                $path = '/mod/forum/discuss.php?d=';
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$id.$typeview;
                break;
            case "forum" :
                $path = '/mod/forum/view.php?id=';
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$id.$typeview;
                break;
            case "event" :
                //  $path = "/calendar/view.php?view=day&course=$id&cal_d=$day&cal_m=$mon&cal_y=$year";
                $path = "/calendar/view.php?view=day&cal_d=$day&cal_m=$mon&cal_y=$year";
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$typeview;
                break;
            case "user" :
                $path = '/user/view.php?id=';
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$id.$typeview;
                break;
            case "edituser" :
                $user = JFactory::getUser ();
                $id = JoomdleHelperContent::call_method ('user_id', $user->username);
                $path = '/user/edit.php?&course_id=1&id=';
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$id.$typeview;
                break;
            case "resource" :
                $path = '/mod/resource/view.php?id=';
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$id.$typeview;
                break;
            case "quiz" :
                $path = JURI::base().'index.php?option=com_joomdle&view=quiz&module_id='.$id.'&course_id='.$courseid.$typeview;
                JFactory::getApplication()->redirect($path);
                break;
            case "questionnaire" :
                $path = JURI::base().'index.php?option=com_joomdle&view=feedbackview&module_id='.$id.'&course_id='.$courseid.$typeview;
                JFactory::getApplication()->redirect($path);
                break;
            case "page" :
                if ($params->get('use_new_performance_method'))
                    $this->page = JHelperLGT::getModuleInfo('page', $id, $courseid);
                else
                    $this->page = JoomdleHelperContent::call_method('create_page_activity', 0, (int)$courseid, $username, array('pid'=>(int)$id), 0);

                $path = '/mod/page/view.php?id=';
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$id.$typeview;
                break;
            case "assignment" :
                $path = '/mod/assignment/view.php?id=';
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$id.$typeview;
                break;
            case "folder" :
                $path = '/mod/folder/view.php?id=';
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$id.$typeview;
                break;
            case "messages" :
                $path = '/message/index.php';
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$typeview;
                break;
            case "assign" :
                $path = '/mod/assign/view_assignment.php?id=';
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$id.$typeview;
                break;
            case "moodle" :
                $path = '/?a=1';
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$typeview;
                break;
            case "scorm" :
                $path = '/mod/scorm/view_mobile.php?id=';
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$id.$typeview;
                break;
            case "skillsoft" :
                $path = '/mod/skillsoft/view_mobile.php?id=';
                $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$id.$typeview;
                break;
            default:
                if ($mtype)
                {
                    $path = '/mod/'.$mtype.'/view.php?id=';
                    $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$id.$typeview;
                    break;
                }
                else
                {
                    $path = '/?a=1';
                    $this->wrapper->url = $params->get( 'MOODLE_URL' ).$path.$typeview;
                }
                break;
        }

        if ($lang)
            $this->wrapper->url .= "&lang=$lang";

        if ($redirect)
            $this->wrapper->url .= "&redirect=$redirect";

        $this->wrapper->mtype = $mtype;
        $this->wrapper->mid = $id;

        $user = JFactory::getUser();
        $username = $user->username;

        if ($params->get('use_new_performance_method'))
            $course_data = JHelperLGT::getCoursePageData(['id' => $courseid, 'username' => $username]);
        else
            $course_data = JoomdleHelperContent::call_method('get_course_page_data', (int)$courseid, $username);

        $this->course_info = json_decode($course_data['course_info'], true);
        $this->mods = json_decode($course_data['mods'], true);
        /* Check course enrol */
        $this->is_enroled = $this->course_info['enroled'];
        $userRoles = json_decode($course_data['userRoles'], true);
        $a = json_decode($userRoles["roles"][0]['role'], true);
        $user_role = $a[0]['sortname'];

        $this->hasPermission = $userRoles['roles'];
        if ($this->is_enroled == 0 && !$this->hasPermission[0]['hasPermission']) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_COURSE_NOT_ENROLL');
//            return;
        }


        $this->courseid = $courseid;

        // check perv and next activity
        $prev = '#';
        $next = '#';
        $mod_arr = array();
        $jump_url =  JoomdleHelperContent::getJumpURL ();
        if (is_array ($this->mods)) {
            foreach ($this->mods as $mod) {
                $activities = $mod['mods'];
                if (is_array($activities)) {
                    foreach ($activities as $act) {
                        if($act['mod'] != 'forum' && $act['mod'] != '' && $act['mod'] != 'certificate' && $act['mod'] != 'questionnaire') {
                            $mod_arr[] = $act;
                            if ($act['id'] == $id) {
                                $this->modname = $act['name'];
                                $this->mod = $act;
                                $this->topicname = $mod['name'];
                            }
                        }
                    }
                }

            }
        }
        $countact = count($mod_arr);
//        var_dump($mod_arr);die;
        if ($countact > 0) {
            for ($i = 0; $i < $countact; $i++) {
                if ($mod_arr[$i]['mod'] == $this->wrapper->mtype && $mod_arr[$i]['id'] == $this->wrapper->mid) {
                    if ($i > 0) {
                        $m_type = JoomdleHelperSystem::get_mtype($mod_arr[$i - 1]['mod']);
                        if ($m_type == 'assign') {
                            $prev = JURI::base() . 'viewassign/' . $courseid . '_' . $mod_arr[$i + 1]['id'] . '.html';
                        } else {
                            $prev = JURI::base() . 'mod/' . $m_type . '_' . $courseid . '_' . $mod_arr[$i - 1]['id'] . '_.html';
                        }
                    }
                    if ($i < ($countact - 1)) {
                        $m_type = JoomdleHelperSystem::get_mtype($mod_arr[$i + 1]['mod']);
                        if ($m_type == 'assign') {
                            $next = JURI::base() . 'viewassign/' . $courseid . '_' . $mod_arr[$i + 1]['id'] . '.html';
                        } else {
                            $next = JURI::base() . 'mod/' . $m_type . '_' . $courseid . '_' . $mod_arr[$i + 1]['id'] . '_.html';
                        }
                    }
                }
            }
        }
        $this->wrapper->prev = $prev;
        $this->wrapper->next = $next;

        // Moodle theme can be overriden by plugin
        JPluginHelper::importPlugin( 'joomdletheme' );
        $dispatcher = JDispatcher::getInstance();
        $result = $dispatcher->trigger('onGetMoodleTheme', array ());
        $theme = array_shift ($result);

        if (!$theme) // If no theme by plugin, check configuration
            $theme = $params->get('theme');
        if ($theme)
            $this->wrapper->url .= "&theme=".$theme;

        if (!empty($this->mod))
            JFactory::getDocument()->setTitle($this->mod['name']);

        if ($this->wrapper->mtype == 'page') parent::display('page');

        else if ($this->wrapper->mtype == 'scorm') parent::display('scorm');

        else parent::display($tpl);
    }
}
?>
