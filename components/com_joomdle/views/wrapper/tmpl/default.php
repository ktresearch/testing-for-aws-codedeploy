<?php

/**
 * This is view file for cpanel
 *
 * PHP version 5
 *
 * @category   JFusion
 * @package    ViewsFront
 * @subpackage Wrapper
 * @author     JFusion Team <webmaster@jfusion.org>
 * @copyright  2008 JFusion. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.jfusion.org

 * Mofified by Antonio Duran to work with Joomdle
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
$session = JFactory::getSession();
$device = $session->get('device');
 ?>
<script src="<?php echo JURI::root(); ?>/components/com_joomdle/js/autoheight.js" type="text/javascript"></script>
<?php
require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
$class = '';
$downloadable = false;

function isIphone($user_agent=NULL) {
    if(!isset($user_agent)) {
        $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    }
    return (strpos($user_agent, 'iPhone') !== FALSE);
}
$isIOS = isIphone();

if ($this->wrapper->mtype == 'resource' && isset($this->mod)) {
    $typeArr = array('pdf', 'document', 'powerpoint');
    if (in_array($this->mod['type'], $typeArr)) {
        if ($isIOS) {
            header('Location: '.$this->wrapper->url);
        } else {
            if($device == 'mobile' || $this->mod['type'] != 'pdf') {
                $class .= ' downloadable ';
                $downloadable = true;
            }
        }
    }
}

if ($this->wrapper->mtype == 'resource') {
    $class .= ' resource ';
}

if ($this->wrapper->mtype == 'assign') {
    $classes .= ' multikill ';
    $a = 'assign';
}

?>
<?php if ($this->wrapper->mtype == 'assign') { ?>
<style type="text/css">
    #t3-mainbody{
        background: #fff !important;
    }
</style>
<?php } ?>

<div class="contentpane <?php echo $class; ?> <?php echo $a; ?>" <?php echo ($isIOS) ? 'style="overflow:auto;-webkit-overflow-scrolling:touch"' : ''; ?>>
    <?php if ($downloadable) : ?>
    <p>Content is being downloaded onto your device. Please open the correct application to view
            it later.</p>
    <?php endif; ?>
    <iframe 
        id="blockrandom" 
        class="autoHeight <?php echo $classes; ?>"
        src="<?php echo $this->wrapper->url; ?>"
        width="<?php echo $this->params->get('width', '100%'); ?>"
        scrolling="<?php //echo $this->params->get('scrolling', 'auto'); ?>yes"
        allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"
        
        <?php if (!$this->params->get('autoheight', 1)) { ?>
            height="<?php echo $this->params->get('height', '500'); ?>"
        <?php
        }
        ?>

        align="top" 
        frameborder="0"
        <?php if ($this->params->get('autoheight', 1)) { ?>
            onload='itspower(this, false, true, 20)'
        <?php
        }
        ?>

 
        ></iframe>
    <?php if ($this->wrapper->prev != '#') { ?>
    <a class="btn-prev-wrapper" href="<?php echo $this->wrapper->prev;?>"><div class="btn-prev">&larr; Prev</div></a>
    <?php 
    } 
    if ($this->wrapper->next != '#') { 
    ?>
    <a class="btn-next-wrapper" href="<?php echo $this->wrapper->next;?>"><div class="btn-next">Next &rarr;</div></a>
    <?php } ?>
</div>
<script type="text/javascript">
//    // SCORM
    setTimeout(function () {
        jQuery('#blockrandom').contents().find('#scormviewform').submit();
    }, 2000);
//    // SCORM
</script>