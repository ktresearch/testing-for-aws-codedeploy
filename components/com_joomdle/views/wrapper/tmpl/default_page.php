<?php

/**
 * This is view file for cpanel
 *
 * PHP version 5
 *
 * @category   JFusion
 * @package    ViewsFront
 * @subpackage Wrapper
 * @author     JFusion Team <webmaster@jfusion.org>
 * @copyright  2008 JFusion. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.jfusion.org

 * Mofified by Antonio Duran to work with Joomdle
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
$session = JFactory::getSession();
$device = $session->get('device'); ?>
<script src="<?php echo JURI::root(); ?>/components/com_joomdle/js/autoheight.js" type="text/javascript"></script>
<?php
//require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');

$role = $this->hasPermission;

require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);
$render_banner = $banner->renderBanner($this->course_info, $this->mods, $role);

$class = '';
$downloadable = false;

function isIphone($user_agent=NULL) {
    if(!isset($user_agent)) {
        $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    }
    return (strpos($user_agent, 'iPhone') !== FALSE);
}
$isIOS = isIphone();

if ($this->wrapper->mtype == 'resource' && isset($this->mod)) {
    $typeArr = array('pdf', 'document', 'powerpoint');
    if (in_array($this->mod['type'], $typeArr)) {
        if ($isIOS) {
            header('Location: '.$this->wrapper->url);
        } else {
            if($device == 'mobile' || $this->mod['type'] != 'pdf') {
                $class .= ' downloadable ';
                $downloadable = true;
            }
        }
    }
}
$classes = '';

if ($this->wrapper->mtype == 'resource') {
    $class .= ' resource ';
}

if ($this->wrapper->mtype == 'assign') {
    $classes .= ' multikill ';
    $a = 'assign';
}

if ($this->wrapper->mtype == 'page') {
    $a = 'page';
    $this->content = $this->page;
}
//print_r($this->content);

?>
<div class="joomdle-view-content">
    <div class="mainContent">
        <p class="contentTitle"><span class="topicTitle"><?php echo $this->topicname .': '; ?></span><span class="activityTitle"><?php echo $this->content['name'];?></span></p>
        <?php if ( !empty($this->content['params']) && !empty($this->content['content'])) {
            $data = json_decode($this->content['params']);
            $i = 0;
             $typev = array('linkVideo', 'video', 'embedVideo');
            foreach ($data as $k => $v) {
                $w = $v->type;
                 if($v->type == 'text'){
                      echo '<div class="isContent">'.$v->content.'</div>';
                      echo '<div class="cline"></div>';
                 }
               else if(!in_array($v->type , $typev)){
                echo $v->content;
                    echo '<div class="cline"></div>';
                }
                else{
                     echo $v->content;
            }
            }
            ?>
        <?php } else if ( empty($this->content['params']) && !empty($this->content['content'])) {
                echo $this->content['content'];
            } else { ?>
            <div>Empty Content.</div>
        <?php } ?>
    </div>
</div>

<div class="contentpane <?php echo $class; ?> <?php echo $a; ?>" <?php echo ($isIOS) ? 'style="overflow:auto;-webkit-overflow-scrolling:touch"' : ''; ?>>
    <?php if ($downloadable) : ?>
        <p>Content is being downloaded onto your device. Please open the correct application to view
            it later.</p>
    <?php endif; ?>
    <iframe
        id="blockrandom"
        class="autoHeight <?php echo $classes; ?>"
        src="<?php echo $this->wrapper->url; ?>"
        width="<?php echo $this->params->get('width', '100%'); ?>"
        scrolling="<?php //echo $this->params->get('scrolling', 'auto'); ?>yes"
        allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"

        <?php if (!$this->params->get('autoheight', 1)) { ?>
            height="<?php echo $this->params->get('height', '500'); ?>"
        <?php
        }
        ?>

        align="top"
        frameborder="0"
        <?php if ($this->params->get('autoheight', 1)) { ?>
            onload='itspower(this, false, true, 20)'
        <?php
        }
        ?>


        ></iframe>
    <?php if ($this->wrapper->prev != '#') { ?>
        <a class="btn-prev-wrapper" href="<?php echo $this->wrapper->prev;?>"><div class="btn-prev"><img class="icon-next" src="./images/PreviousIcon.png"></div></a>
    <?php
    }
    if ($this->wrapper->next != '#') {
        ?>
        <a class="btn-next-wrapper" href="<?php echo $this->wrapper->next;?>"><div class="btn-next"><img class="icon-next" src="./images/NextIcon.png"></div></a>
    <?php } ?>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
        jQuery('img.uploadedImage').each(function() {
            jQuery(this).attr('onclick', 'joms.api.photoZoom(\''+ jQuery(this).attr('src') +'\')').css({'cursor':'pointer'});
        });
    });
    var type = '<?php echo $w ;?>';
    if(type == 'www'){
        jQuery('.contentTitle').addClass('hidden');
        jQuery('.joomdle-view-content').css('padding-top','0px');
        jQuery('.joomdle-view-content').css('margin-top','-22px');
        jQuery('.mob .joomdle-view-content').css('margin-top','-42px');

     }
     function downloadFile(filePath) {
                 var link = document.createElement('a');
                 link.href = filePath;
                  link.download = filePath.substr(filePath.lastIndexOf('/') + 1);
                  link.click();
            }
</script>
