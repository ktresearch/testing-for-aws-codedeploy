<?php

/**
 * This is view file for cpanel
 *
 * PHP version 5
 *
 * @category   JFusion
 * @package    ViewsFront
 * @subpackage Wrapper
 * @author     JFusion Team <webmaster@jfusion.org>
 * @copyright  2008 JFusion. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.jfusion.org

 * Mofified by Antonio Duran to work with Joomdle
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
$session = JFactory::getSession();
$device = $session->get('device');

//update for webview app
if (isset($_SESSION['type']) && $_SESSION['type'] == 'appview') {
$appview = $_SESSION['type'] == 'appview';
} ?>

<script src="<?php echo JURI::root(); ?>/components/com_joomdle/js/autoheight.js" type="text/javascript"></script>
<?php
//require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');

$role = $this->hasPermission;

require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);
$render_banner = $banner->renderBanner($this->course_info, $this->mods, $role);

$class = '';
$downloadable = false;

function isIphone($user_agent=NULL) {
    if(!isset($user_agent)) {
        $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    }
    return (strpos($user_agent, 'iPhone') !== FALSE);
}
$isIOS = isIphone();

if ($this->wrapper->mtype == 'resource' && isset($this->mod)) {
    $typeArr = array('pdf', 'document', 'powerpoint');
    if (in_array($this->mod['type'], $typeArr)) {
        if ($isIOS) {
            header('Location: '.$this->wrapper->url);
        } else {
            if($device == 'mobile' || $this->mod['type'] != 'pdf') {
                $class .= ' downloadable ';
                $downloadable = true;
            }
        }
    }
}

if ($this->wrapper->mtype == 'resource') {
    $class .= ' resource ';
}

if ($this->wrapper->mtype == 'assign') {
    $classes .= ' multikill ';
    $a = 'assign';
}

?>
<?php if ($this->wrapper->mtype == 'assign') { ?>
    <style type="text/css">
        #t3-mainbody{
            background: #fff !important;
        }
    </style>
<?php } ?>
<script type="text/javascript">
    function resizeIFrameToFitContent( iFrame ) {

        iFrame.width  = iFrame.contentWindow.document.body.scrollWidth;
        iFrame.height = iFrame.contentWindow.document.body.scrollHeight;
        console.log('iframe width:'+iFrame.width);
        console.log('iframe height:'+iFrame.height);
    }

    window.addEventListener('DOMContentLoaded', function(e) {
        console.log('iframe auto load.');
        var iFrame = document.getElementById( 'blockrandom' );
        resizeIFrameToFitContent( iFrame );

    });
</script>
<p class="scormTitle"><span class="topicTitle"><?php echo $this->topicname .': '; ?></span><span class="activityTitle"><?php echo $this->modname; ?></span></p>
<div class="contentpane <?php echo $class; ?> <?php echo $a; ?>" <?php echo ($isIOS) ? 'style="-webkit-overflow-scrolling:touch"' : ''; ?>>
    <?php if ($downloadable) : ?>
        <p>Content is being downloaded onto your device. Please open the correct application to view
            it later.</p>
    <?php endif; ?>
    <iframe
            id="blockrandom"+
            class="autoHeight <?php echo $classes; ?>"
            src="<?php echo $this->wrapper->url; ?>"
            width="<?php echo $this->params->get('width', '100%'); ?>"
            scrolling="<?php //echo $this->params->get('scrolling', 'auto'); ?>yes"
            allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"

        <?php // if (!$this->params->get('autoheight', 1)) { ?>
            height=""
            <?php
//        }
        ?>

            align="top"
            frameborder="0"
        <?php if ($this->params->get('autoheight', 1)) { ?>
            onload='itspower(this, false, true, 20)'
            <?php
        }
        ?>


    ></iframe>
    <?php if ($this->wrapper->prev != '#' && $appview != 'appview') { ?>
        <a class="btn-prev-wrapper" href="<?php echo $this->wrapper->prev;?>"><div class="btn-prev"><img class="icon-next" src="/images/PreviousIcon.png"></div></a>
        <?php
    }
    if ($this->wrapper->next != '#' && $appview != 'appview') {
        ?>
        <a class="btn-next-wrapper" href="<?php echo $this->wrapper->next;?>"><div class="btn-next"><img class="icon-next" src="/images/NextIcon.png"></div></a>
    <?php } ?>
</div>
<script type="text/javascript">
    //    // SCORM
    setTimeout(function () {
        jQuery('#blockrandom').contents().find('#scormviewform').submit();
    }, 2000);
    //    // SCORM
</script>
