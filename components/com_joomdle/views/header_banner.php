<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class HeaderBanner {
    public $view = '';

    function __construct(handleView $view) {
        $this->view = $view;
    }

    function renderBanner($data, $mods, $permission) {
        echo $this->view->renderView($data, $mods, $permission);
    }
}

interface handleView {
    public function renderView($data, $mods, $permission);
}
// for Joomdle course
class JoomdleBanner implements handleView {
    public function renderView($data, $mods, $permission, $includeResult = true) {
        $session = JFactory::getSession();
        $device = $session->get('device');

        $user = JFactory::getUser();
        $username = $user->username;

        if ($device == 'mobile')
            $fullname =  (strlen(strip_tags($data['fullname'])) > 30) ? substr(strip_tags($data['fullname']), 0, 30).'...' : $data['fullname'];
        else
            $fullname = (strlen(strip_tags($data['fullname'])) > 130) ? substr(strip_tags($data['fullname']), 0, 130).'...' : $data['fullname'];
        $html = '';
        $html .= '<div class="header-cover" id="nav">';
         $html .='<div class = "popupquiz">';
        $html .= '</div>';
        $html .= '<div class="header-cover-image lazyload" data-src="'.$data['filepath']. $data['filename'].'" style="background-color: #fff;">';
        // $html .= '<img src="'.$data['filepath']. $data['filename'].'" />';
        $html .= '</div>';
        $html .= '<div class="header-cover-content">';
        $html .= '<div class="back-left"><a class="back" href="#" onclick="goBack();"><img src="./images/Back-button.png" /></a></div>';
        $html .= '<div class="header-title">';
        $html .= '<h2>'.$fullname.'</h2>';
        $html .= '</div>';

        $user_role = array();
        if (!empty($permission)) {
            foreach (json_decode($permission[0]['role']) as $r) {
                $user_role[] = $r->sortname;
            }
        }

        if (in_array('student', $user_role) && empty(array_intersect($user_role, ['teacher', 'manager', 'coursecreator']))) {
            if ($includeResult) {
                $count_compled = 0;
                $count_activity = 0;
                if (is_array($mods)) {
                    foreach ($mods as $tema) {
                        $resources = $tema['mods'];
                        foreach ($resources as $id => $res) {
                            if (($res['mod'] != 'forum') && ($res['mod'] != 'certificate') && ($res['mod'] != 'feedback') && ($res['mod'] != 'questionnaire')) {
                                $mtype = JoomdleHelperSystem::get_mtype($res['mod']);
                                if (!$mtype) continue;

                                $completion = JHelperLGT::getModCompletionStatus(['modid' => $res['id'], 'username' => $username, 'act_type' => $res['mod']], false);
                                if ($completion['mod_completion']) $count_compled++;

                                $count_activity++;
                            }
                        }
                    }
                }
                $num_progress_bar = ($count_activity != 0) ? intval(($count_compled / $count_activity) * 100) : 0;
                $html .= '<div class="joomdle_course_progress_bar">';
                $html .= '<div class="course_progress_bar" style="width: '.$num_progress_bar.'%;">';
                $html .= '</div>';
                $html .= '<div id="completed-number">';
                $html .= '<span id="completed-percent">'.$num_progress_bar.'</span>';
                $html .= '<span id="percent">%</span>';
                $html .= '</div>';
                $html .= '</div>';
            } else {
                $html .= '<div class="joomdle_course_progress_bar">';
                $html .= '<div class="course_progress_bar" style="width: 0;">';
                $html .= '</div>';
                $html .= '<div id="completed-number">';
                $html .= '<span id="completed-percent">0%</span>';
                $html .= '<span id="percent">%</span>';
                $html .= '</div>';
                $html .= '</div>';
            }
        }

        if ($user_role[0] != 'teacher' && $user_role[0] !='student' && $data['course_status'] != 'pending_approval' && $data['course_status'] != 'approved' && $data['course_status'] != 'published') {
            if($data["course_type"] == 'course_non'){
                $edit_course_path = JURI::base().'mycourses/edit_'.$data["remoteid"].'_'.$data["cat_id"].'_noncourse.html';
            }
            if($data["course_type"] == 'course_lp'){
                $edit_course_path = JURI::base().'mycourses/edit_'.$data["remoteid"].'_'.$data["cat_id"].'_lpcourse.html';
            }
            $html .= '<a href="'.$edit_course_path.'" class="hvn_BtEditCourse">';
            $html .= '<img src="/images/editicon30x30.png" alt="Edit Course">';
            $html .= '</a>';
        }

        $html .= '</div>';
        // header menu

        $html .= $this->renderMenu($data, $permission);
            $html .= '</div>';

        return $html;
    }

    public function renderMenu ($data, $permission) {
        $id = $data['remoteid'];
        JPluginHelper::importPlugin('joomdlesocialgroups');
        $dispatcher = JDispatcher::getInstance();
        $course_group = $dispatcher->trigger('get_group_by_course_id', array($id));
        if (!empty($course_group)) {
            $href = 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $course_group[0];
        } else {
            $href = '#';
            $class = 'no-group';
        }
        if (strcmp((JFactory::getLanguage()->getTag()), 'vi-VN') == 0) {
            $langcurrent = 'vn';
        }
        if (strcmp((JFactory::getLanguage()->getTag()), 'en-GB') == 0) {
            $langcurrent = 'en';
        }

        $user_role = array();
        if (!empty($permission)) {
            foreach (json_decode($permission[0]['role']) as $r) {
                $user_role[] = $r->sortname;
            }
        }
        
        $views = !empty(array_intersect($user_role, ['teacher', 'manager', 'coursecreator'])) ? 'viewstudents' : 'progress';

//        $full_name = $_SERVER['PHP_SELF'];
//        $full_url = $_SERVER['REQUEST_URI'];
//        $name_array = explode('/', $full_name);
//        $count = count($name_array);
//        $page_name = $name_array[$count - 2];
        $page_name = JRequest::getVar( 'view', null, 'NEWURLFORM' );
        if (!$page_name) $page_name =  JRequest::getVar( 'view' );
        $request =  JRequest::getVar('request_id', null, 'NEWURLFORM');
        switch ($page_name) {
            case 'course':
            case 'page':
                $active = 'active';
                break;
            case 'content':
                $active = 'active2';
                break;
            case 'progress':
            case 'overallgrade':
            case 'viewstudents':
            case 'studentdetail':
            case 'viewactivity':
            case 'activitydetail':
             case 'activitygradestudent':
                $active_pro = 'active';
                break;
            case 'certificate':
                if($request == 1) $active_pro = 'active';
                else $active = 'active';
                break;
            case 'viewgroup':
                if (isset($course_group)) {
                    $active_group = 'active';
                } else {
                    $active_group = '';
                }
                break;
            case 'viewdiscussion':
                $active_group = 'active';
                break;
            case 'groups':
                $active_group = 'active';
                break;
            case 'coursecontent':
                $active = 'active2';
                break;
            default:
                $active = 'active';
                break;
        }
        if (strpos($full_url, 'overallgrade') !== false) {
            $active_pro = 'active';
        }
        if ($user_role[0] != 'coursecreator' || ($user_role[0] == 'coursecreator' && ($data['course_status'] == 'pending_approval' || $data['course_status'] == 'approved' || $data['course_status'] == 'published'))) {
            $html = '<div class="header-menu course-menu">';
            $html .= '<div class="course-nav">';
            $html .= '<nav>';
            $html .= '<ul>';
            $html .= '<li class="course-outline ' . $active . '">';
            $html .= '<a href="' . JURI::root() . $langcurrent . '/course/' . $id . '.html">';
            $html .= '<span>' . JText::_("COM_JOOMDLE_OUTLINE") . '</span>';
            $html .= '</a>';
            $html .= '</li>';
            $html .= '<li class="course-circle ' . $active_group . ' ' . $class . '">';
            $html .= '<a href="' . $href . '" class="' . $class . '">';
            $html .= '<span>' . JText::_("COM_JOOMDLE_COURSE_LEARNING_CIRCLE") . '</span>';
            $html .= '</a>';
            $html .= '</li>';
            $html .= '<li class="course-gradebook ' . $active_pro . '">';
            $html .= '<a href="' . JURI::base() . $langcurrent . '/' . $views . '/' . $id . '.html">';
            $html .= '<span>' . JText::_("COM_JOOMDLE_COURSE_PROGRESS") . '</span>';
            $html .= '</a>';
            $html .= '</li>';
            $html .= '</ul>';
            $html .= '</nav>';
            $html .= '</div>';
            $html .= '</div>';
        }
        $html .= '<div class="noGroupPopup" id="myModal">
                    <div class="modal-content hienPopup">
                        <p>'.JText::_("COM_JOOMDLE_NO_COURSE_GROUP").'</p>
                        <button type="button" class="closetitle">'.JText::_("COM_JOOMDLE_CLOSE").'</button>
                    </div>
                </div>';
        $html .= '<script tye="text/javascript">
            (function ($) {

                // Get the modal
                var modal = document.getElementById(\'myModal\');

                // Get the button that opens the modal
                var btn = document.getElementById("myBtn");

                // Get the <span> element that closes the modal
                var span = document.getElementsByClassName("close")[0];


                jQuery(\'.course-circle\').click(function () {
                    if (jQuery(this).hasClass(\'no-group\')) {
                        modal.style.display = "block";
                        jQuery("a.no-group").removeAttr("href");
                    }
                });
                $(\'.closetitle\').click(function () {
                    modal.style.display = "none";
                });


                // When the user clicks anywhere outside of the modal, close it
                window.onclick = function (event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                    }
                }
            })(jQuery);
        </script>';

        return $html;
    }
}

// For Community
class CommunityBanner implements handleView {
    public function renderView($data, $mods, $permission) {
        // code for header circle here
    }
}