<?php
defined('_JEXEC') or die('Restricted access');

$session = JFactory::getSession();
$device = $session->get('device');
$document = JFactory::getDocument();
$itemid = JoomdleHelperContent::getMenuItem();

// ----------------------List activity in course----------------
$list_modules = $this->list_modules;
$count_list_modules = count($list_modules);
$id_module = $this->module_id;
$id_course = $this->course_id;

?>
<?php
$role = $this->hasPermission;

require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);
$render_banner = $banner->renderBanner($this->course_info, $this->mods, $role); ?>

<div class="view_assigment">
    <div style="clear:both;"></div>
    <div class="title"><span class="topic"><?php echo ucwords($this->topic_name); ?>: </span><span
                class="activity"><?php echo $this->assign['name']; ?></span></div>
    <div class="content">
        <div class="intructions">
            <p class="intructions_title"><?php echo JText::_('COM_JOOMDLE_INSTRUCTIONS');?> </p>
            <?php echo $this->assign['intro']; ?>
        </div>
        <?php
            if ($this->assign['duedate'] <= 0) {
                $data_duedate = '-';
            } else if (date('d/m/Y', $this->assign['duedate']) == '01/01/1970')
                $data_duedate = date('D, d M Y H:i:s', time());
            else $data_duedate = date('D, d M Y H:i:s', $this->assign['duedate']);

            $target = '';
            if(strpos($this->link_download_brief,'.pdf') !== false) {
                $target = '_blank';
            }
        ?>
        <div class="date"><p><?php echo JText::_('COM_JOOMDLE_ASSIGNMENT_DUE_DATE');?>: <span class="status" data-duedate="<?php echo $data_duedate; ?>"></span></p>
        </div>
    </div>
    <div class="line"></div>
    <div class="submission_status"><p><?php echo JText::_('COM_JOOMDLE_ASSIGNMENT_SUBMIT_STATUS');?>: <span class="status"><?php echo $this->submission_status; ?></span></p></div>
    <div class="grade_status"><p><?php echo JText::_('COM_JOOMDLE_ASSIGNMENT_GRADING_STATUS');?>: <span class="status"><?php echo $this->grade_status; ?></span></p></div>
    <div class="bttn">
        <div class="btn-download"><a class="btn-assign" target="<?php echo $target; ?>" href="<?php echo $this->link_download_brief; ?>"><?php echo JText::_('COM_JOOMDLE_ASSIGNMENT_DOWNLOAD_BRIEF');?></a></div>

    </div>
    <?php if ($this->submission_status != 'Submitted') { ?>
        <div class="btn-upload">
            <span class="btnUpload"><?php echo JText::_('COM_JOOMDLE_ASSIGNMENT_SUBMIT_ASSIGNMENT');?></span>
        </div>
        <div class="uploadSubmission" action="" enctype='multipart/form-data' style="display:none;">
            <input name="assignment_file" class="fileassignment" id="fileassignment" type="file">
        </div>
        <div id="myProgress" class="progress hidden">
            <div id="bar_100">
                <div id="myBar" class="bar"></div>
            </div>
        </div>
    <?php } ?>

    <div class="next_prev_activity">
        <div class="introduction">
            <!-- Previous and next activity-->
            <div class="btn-prev-next">
                <?php for($i = 0; $i < $count_list_modules ; $i++){
                    if ($id_module == $list_modules[$i]['id'] && $i == 0) {
                        $next = $i+1;
                        switch ($list_modules[$next]['mod']) {
                            case 'quiz':
                                $url_next = JURI::base().'mod/quiz_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                            case 'assign':
                                $url_next = JURI::base().'viewassign/'.$id_course.'_'.$list_modules[$next]['id'].'.html';
                                break;
                            case 'scorm':
                                $url_next = JURI::base().'mod/scorm_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                            case 'page':
                                $url_next = JURI::base().'mod/page_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                            default:
                                # code...
                                break;
                        }?>
                        <div class="buttons">
                            <a class="icon-next" target="_parent" href="<?php echo $url_next;?>" style="float:right;" >
                                <!--                                <button class="btnQuiz">--><?php //echo Jtext::_('QUIZ_BTN_NEXT'); ?><!-- &rarr; </button>-->
                                <img src="<?php echo JUri::base();?>/images/NextIcon.png">
                            </a>
                        </div>

                    <?php } else if ($id_module == $list_modules[$i]['id'] && $i > 0 && $i <= $count_list_modules-2) {
                        $prev = $i-1;
                        $next = $i+1;
                        switch ($list_modules[$prev]['mod']) {
                            case 'quiz':
                                $url_prev = JURI::base().'mod/quiz_'.$id_course.'_'.$list_modules[$prev]['id'].'_.html';
                                break;
                            case 'assign':
                                $url_prev = JURI::base().'viewassign/'.$id_course.'_'.$list_modules[$prev]['id'].'.html';
                                break;
                            case 'scorm':
                                $url_prev = JURI::base().'mod/scorm_'.$id_course.'_'.$list_modules[$prev]['id'].'_.html';
                                break;
                            case 'page':
                                $url_prev = JURI::base().'mod/page_'.$id_course.'_'.$list_modules[$prev]['id'].'_.html';
                                break;

                            default:
                                # code...
                                break;
                        }
                        switch ($list_modules[$next]['mod']) {
                            case 'quiz':
                                $url_next = JURI::base().'mod/quiz_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                            case 'assign':
                                $url_next = JURI::base().'viewassign/'.$id_course.'_'.$list_modules[$next]['id'].'.html';
                                break;
                            case 'scorm':
                                $url_next = JURI::base().'mod/scorm_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                            case 'page':
                                $url_next = JURI::base().'mod/page_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                            default:
                                # code...
                                break;
                        }?>
                        <div class="buttons">
                            <a class="icon-prev" target="_parent" href="<?php echo $url_prev;?>" style="float:left;">
                                <img src="<?php echo JUri::base();?>/images/PreviousIcon.png">
                            </a>
                            <a class="icon-next" target="_parent" href="<?php echo $url_next;?>" style="float:right;" >
                                <img src="<?php echo JUri::base();?>/images/NextIcon.png">
                            </a>
                        </div>


                    <?php } else if ($id_module == $list_modules[$i]['id'] && $i==$count_list_modules-1) {
                        $prev = $i-1;
                        switch ($list_modules[$prev]['mod']) {
                            case 'quiz':
                                $url_prev = JURI::base().'mod/quiz_'.$id_course.'_'.$list_modules[$prev]['id'].'_.html';
                                break;
                            case 'assign':
                                $url_prev = JURI::base().'viewassign/'.$id_course.'_'.$list_modules[$prev]['id'].'.html';
                                break;
                            case 'scorm':
                                $url_prev = JURI::base().'mod/scorm_'.$id_course.'_'.$list_modules[$prev]['id'].'_.html';
                                break;
                            case 'page':
                                $url_prev = JURI::base().'mod/page_'.$id_course.'_'.$list_modules[$prev]['id'].'_.html';
                                break;

                            default:
                                # code...
                                break;
                        }?>
                        <div class="buttons">
                            <a class="icon-prev" target="_parent" href="<?php echo $url_prev;?>" style="float:left;">
                                <!--                                <button class="btnQuiz">&larr; --><?php //echo Jtext::_('QUIZ_BTN_PREV'); ?><!-- </button>-->
                                <img src="<?php echo JUri::base();?>/images/PreviousIcon.png">
                            </a>
                        </div>

                    <?php } }?>
            </div>
            <!-- Previous and next activity-->
        </div>
    </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {

		jQuery('.date .status').each(function() {
            if (jQuery(this).attr('data-duedate') == '-') {
                jQuery(this).html('-');
            } else {
                var data_duedate = jQuery(this).attr('data-duedate') + ' GMT';
                var date = new Date(data_duedate);
                jQuery(this).html(("0" + date.getDate()).slice(-2) + '/' + ("0" + parseInt(date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear());
            }
        });

        jQuery('.btnUpload').click(function () {
            jQuery('.fileassignment').click();
        });
        jQuery('.fileassignment').change(function (event) {
            var file = jQuery('#fileassignment').get(0).files[0],
                formData = new FormData();
            formData.append('file', file);
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                contentType: false,
                cache: false,
                processData: false,
                data: formData,
                xhr: function () {
                    var jqXHR = null;
                    if (window.ActiveXObject) {
                        jqXHR = new window.ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else {
                        jqXHR = new window.XMLHttpRequest();
                        jQuery('#myProgress').removeClass('hidden');
                        jQuery(".btn-upload").remove();
                    }
                    //Upload progress
                    jqXHR.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = Math.round((evt.loaded * 100) / evt.total);
                            //Do something with upload progress
                            jQuery('#myBar').css({width: percentComplete + '%'});
                            if (percentComplete == 100) {
                                jQuery('#myProgress').remove();
                            }
                            console.log('Uploaded percent', percentComplete);
                        }
                    }, false);
                    //Download progress
                    jqXHR.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = Math.round((evt.loaded * 100) / evt.total);
                        }
                    }, false);
                    return jqXHR;
                },
                success: function (data, textStatus, jqXHR) {
                    jQuery(".submission_status").html('<p>Submission Status: Submitted</p>');
                    jQuery(".grade_status").html('<p>Grading Status: In Progress</p>');
                    var res = JSON.parse(data);
                    console.log(res);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                }
            });
        });

    });
</script>