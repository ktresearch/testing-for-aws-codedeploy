<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewViewAssign extends JViewLegacy
{
    function display($tpl = null)
    {

        $app = JFactory::getApplication();
        $params = $app->getParams();
        $this->assignRef('params', $params);
        $currentUser = JFactory::getUser();
        $currentUsername = $currentUser->username;

        $course_id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
        if (!$course_id) $course_id =  JRequest::getVar( 'course_id' );
        if (!$course_id)
            $course_id = $params->get( 'course_id' );

        $assign_id = JRequest::getVar( 'assign_id', null, 'NEWURLFORM' );
        if (!$assign_id) $assign_id =  JRequest::getVar( 'assign_id' );
        if (!$assign_id)
            $assign_id = $params->get( 'assign_id' );


        $course_id = (int) $course_id;
        $assign_id = (int) $assign_id;

        if ($params->get('use_new_performance_method'))
            $assign_info = JHelperLGT::getAssignmentActivityDetail($course_id, $currentUsername, $assign_id);
        else
            $assign_info = JoomdleHelperContent::call_method ( 'get_assignment_activity_detail', $course_id, $currentUsername, $assign_id);
        
        $this->assign = json_decode($assign_info['assign_information'], true);
        $this->grade_status = $assign_info['grade_status'];
        $this->submission_status = $assign_info['submission_status'];
        $this->link_download_brief = $assign_info['link_download_brief'];
        $this->topic_name = $assign_info['topic_name'];

        if (!empty($_FILES)) {

            $tmp_file = dirname($app->getCfg('dataroot')).'/temp/'.'tmp_file';
            file_put_contents ($tmp_file, file_get_contents($_FILES['file']['tmp_name']));
            $r = JoomdleHelperContent::call_method('student_submission_assign', $course_id, $assign_id, $currentUsername, array('fname'=>$_FILES['file']['name']));
            echo json_encode($r);
            die;
        }

        $this->hasPermission = JFactory::hasPermission((int)$course_id, $currentUsername);

        if ($params->get('use_new_performance_method'))
            $course_data = JHelperLGT::getCoursePageData(['id' => $course_id, 'username' => $currentUsername]);
        else
            $course_data = JoomdleHelperContent::call_method('get_course_page_data', (int)$course_id, $currentUsername);

        $this->course_info = json_decode($course_data['course_info'], true);
        $this->mods = json_decode($course_data['mods'], true);
        /* Check course enrol */
        $this->is_enroled = $this->course_info['enroled'];
        $userRoles = json_decode($course_data['userRoles'], true);
        $a = json_decode($userRoles["roles"][0]['role'], true);
        $user_role = $a[0]['sortname'];

        // check permission
        if ($this->is_enroled == 0 && !$this->hasPermission[0]['hasPermission']) {
            header('Location: /404.html');
            exit;
        }

        $visible = 1;
        if ($params->get('use_new_performance_method'))
            $coursemods = JHelperLGT::getCourseMods(['id' => $course_id, 'username' => $currentUsername, 'visible' => $visible]);
        else
            $coursemods = JoomdleHelperContent::call_method ( 'get_course_mods_visible', $course_id, $currentUsername, $visible);

        $mod_arr = array();
        if (is_array ($coursemods)) {
            foreach ($coursemods as $mod) {
                $activities = $mod['mods'];
                if (is_array($activities)) {
                    foreach ($activities as $act) {
                        if($act['mod'] != 'forum' && $act['mod'] != '' && $act['mod'] != 'certificate' && $act['mod'] != 'questionnaire') {
                            $mod_arr[] = $act;
                        }
                    }
                }
            }
        }

        $this->module_id = $assign_id;
        $this->course_id = $course_id;
        $this->list_modules = $mod_arr;


        parent::display($tpl);
    }
}

?>
