<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewAssignment extends JViewLegacy {
    function display($tpl = null) {
        global $mainframe;
        $app = JFactory::getApplication();
        $pathway = $app->getPathWay();
        $document = JFactory::getDocument();
        $menus = $app->getMenu();
        $menu = $menus->getActive();
        $user = JFactory::getUser();
        $username = $user->username;
        $params = $app->getParams();
        $this->assignRef('params',$params);

        $course_id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
        if (!$course_id) $course_id = JRequest::getVar( 'course_id' );
        if (!$course_id) $course_id = $params->get( 'course_id' );

        $aid = JRequest::getVar( 'mid', null, 'NEWURLFORM' );
        if (!$aid) $aid = JRequest::getVar( 'mid' );
        if (!$aid) $aid = $params->get( 'mid' );

        $action = JRequest::getVar( 'action', null, 'NEWURLFORM' );
        if (!$action) $action = JRequest::getVar( 'action' );
        if (!$action) $action = $params->get( 'action' );

        $course_id = (int) $course_id;
        $this->course_id = $course_id;
        $this->aid = $aid;

        if (!$course_id) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
//            return;
        }

        $this->hasPermission = JFactory::hasPermission($course_id, $username);
        if (!$this->hasPermission[0]['hasPermission']) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//            return;
        }

        if ($params->get('use_new_performance_method'))
            $this->course_info = JHelperLGT::getCourseInfo(['id' => $course_id, 'username' => $username]);
        else
            $this->course_info = JoomdleHelperContent::getCourseInfo($course_id, $username);

        $this->course_status = $this->course_info['course_status'];
        if ($action == 'create' || $action == 'edit' || $action == 'remove') {
            if ($this->course_status == 'pending_approval' || $this->course_status == 'approved') {
                echo JText::_('COM_JOOMDLE_COURSE_APPROVED_OR_PENDING_APPROVAL');
                return;
            }
            if ($this->course_info['self_enrolment']) {
                echo JText::_('COM_JOOMDLE_COURSE_PUBLISHED');
                return;
            }
        }
        $this->is_enroled = $this->course_info['enroled'];
        $this->sectionid = JRequest::getVar( 'section_id', null, 'NEWURLFORM' );

//		$this->list =  JRequest::getVar( 'list' );
        if (isset($_POST['act']) && $_POST['act'] == 'remove') {
            $array = array();
            $act = 3;
//            $array['courseid'] = (int)$this->course_id;

            $array['aid'] = (int)$_POST['aid'];

            $r = JoomdleHelperContent::call_method('create_assignment_activity', $act, $course_id, $username, $array, 0);

            $result = array();
            $result['error'] = 0;
            $result['comment'] = 'Everything is fine';
            $result['data'] = $r;
            echo json_encode($result);
            die;
        } else if (isset($_POST['act']) && ($_POST['act'] == 'create' || $_POST['act'] == 'update')) {
            $array = array();
            if ($_POST['act'] == 'update') {
                $act = 2;
                $array['aid'] = (int)$_POST['aid'];
            } else if ($_POST['act'] == 'create') {
                $act = 1;
            }

            $array['name'] = strip_tags($_POST['name']);
            $array['des'] = strip_tags($_POST['des']);
            $array['duedate'] = $_POST['duedate'];
            $array['timezoneOffset'] = $_POST['timezoneOffset'];

            if (isset($_POST['fname'])) $array['fname'] = strip_tags($_POST['fname']);
            if (isset($_POST['itemid']))$array['itemid'] = (int)$_POST['itemid'];
            if (isset($_POST['sectionid'])) $sectionid = (int)$_POST['sectionid'];

            $r = JoomdleHelperContent::call_method('create_assignment_activity', $act, $course_id, $username, $array, $sectionid);

            $result = array();
            $result['error'] = $r['error'];
            $result['comment'] = $r['mes'];
            $fileData = json_decode($r['data']);
            $result['data'] = $fileData;
            echo json_encode($result);
            die;
        } else if (isset($_POST['act']) && ($_POST['act'] == 'deleteFile')) {
            $array = array();
            $act = 5;

            if (!empty($_POST['fname'])) $array['fname'] = strip_tags($_POST['fname']);
            $array['itemid'] = (int)$_POST['itemid'];

            $r = JoomdleHelperContent::call_method('create_assignment_activity', $act, $course_id, $username, $array, 0);

            $result = array();
            $result['error'] = $r['error'];
            $result['comment'] = $r['mes'];
            $fileData = json_decode($r['data']);
            $result['data'] = $fileData;
            echo json_encode($result);
            die;
        } else if (!empty($_FILES)) {
            $documentMIMEType = array(
                'application/vnd.oasis.opendocument.text',
                'application/msword',
                'application/pdf',
                // 'application/vnd.ms-excel',
                // 'application/vnd.oasis.opendocument.spreadsheet',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'text/plain',
                'application/zip',
                'application/x-zip-compressed',
                'application/x-rar-compressed',
                // 'application/octet-stream',
                // 'application/x-msdownload',
                // 'application/x-ms-dos-executable'

                // file excel
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.ms-excel',
                'application/excel',
                'application/x-excel',
                'application/x-msexcel',

                // file pptx
                'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                // file ppt
                'application/mspowerpoint',
                'application/powerpoint',
                'application/vnd.ms-powerpoint',
                'application/x-mspowerpoint',
            );

            if ($_FILES[0]['size'] > 88*1024*1024) {
                $result['error'] = 1;
                $result['comment'] = 'File size is too large (< 8mb)';
            } else if (strlen($_FILES[0]['name']) > 250) {
                $result['error'] = 1;
                $result['comment'] = 'File name is too long (< 250char)';
            } else if (!in_array($_FILES[0]['type'], $documentMIMEType)) {
                $result['error'] = 1;
                $result['comment'] = JText::_('COM_JOOMDLE_INCORRECT_FILE_FORMATS');
            } else {
                $tmp_file = dirname($app->getCfg('dataroot')).'/temp/'.'tmp_file';
                file_put_contents ($tmp_file, file_get_contents($_FILES[0]['tmp_name']));

                $result = array();
                $result['error'] = 0;
                $result['comment'] = 'Everything is fine';

                $r = JoomdleHelperContent::call_method('create_assignment_activity', 4, $course_id, $username, array('fname'=>$_FILES[0]['name']), 0);

                $result = array();
                $result['error'] = $r['error'];
                $result['comment'] = $r['mes'];
                $fileData = json_decode($r['data']);
                $result['data'][0]['url'] = $fileData->filepath.$fileData->filename;
                $result['data'][0]['name'] = $fileData->filename;
                $result['data'][0]['itemid'] = $fileData->itemid;
                $result['data'][0]['type'] = $_FILES[0]['type'];
            }
            // $result['data'] = $_FILES;
            echo json_encode($result);
            die;
        }

        switch ($action) {
            case 'list':
                $this->action = 'list';
                $document->setTitle('Assignment List');
                $this->mods = JoomdleHelperContent::call_method ( 'get_course_mods', (int) $course_id, '', 'assign');
//                $t = count($this->mods);
//                for($i = 0; $i < $t ; $i++){
//                    $this->mod_dt[$i] = $this->mods[$i]['mods'];
//                    for($j = 0; $j < count($this->mod_dt[$i]) ; $j++){
//                        $this->modss[] = $this->mod_dt[$i][$j];
//                    }
//                }

                $arr = array();
                foreach ($this->mods['coursemods'] as $section ) {
                    if (!empty($section['mods'])) {
                        foreach ($section['mods'] as $mod) {
                            $arr[$mod['id']] = $mod;
                        }
                    }
                }
                krsort($arr);
                $this->mods = array( array( 'mods'=>$arr ) );

                break;
            case 'create':
                $this->action = 'create';

                $document->setTitle(JText::_('COM_JOOMDLE_ASSIGNMENT'));

                break;
            case 'edit':
                $this->action = 'edit';
//                $this->assign_info = JoomdleHelperContent::call_method ( 'get_info_assignment',(int)$aid);
                if ($params->get('use_new_performance_method'))
                    $this->assign_info = JHelperLGT::getModuleInfo('assign', $aid, $course_id);
                else
                    $this->assign_info = JoomdleHelperContent::call_method('create_assignment_activity', 0, $course_id, $username, array('aid'=>(int)$aid), 0) ;
//                $this->intros = $this->assign_info['intro'];
//                $this->intro = array();
//                $r = explode('|_|_|', $this->intros);
//                $this->intro[0] = $r[0];
//                $this->intro[1] = $r[1];
//                $this->intro[1] = str_replace('"', '', $this->intro[1]);
                $document->setTitle(JText::_('COM_JOOMDLE_ASSIGNMENT'));
                break;
            case 'remove':
                $this->action = 'remove';
                if ($params->get('use_new_performance_method'))
                    $this->assign_info = JHelperLGT::getModuleInfo('assign', $aid, $course_id);
                else
                    $this->assign_info = JoomdleHelperContent::call_method('create_assignment_activity', 0, $course_id, $username, array('aid'=>(int)$aid), 0) ;

                $document->setTitle('Remove Assignment');
                break;
            default:
                break;
        }


//		if ($action == 'edit' && $aid) {
//			$this->assign_info = JoomdleHelperContent::call_method ( 'get_info_assignment',(int)$aid);
//			$this->intros = $this->assign_info['intro'];
//			$this->intro = array();
//			$r = explode('|_|_|', $this->intros);
//			$this->intro[0] = $r[0];
//			$this->intro[1] = $r[1];
//			$this->intro[1] = str_replace('"', '', $this->intro[1]);
//			$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
//	        parent::display('edit');
//	        return;
//		} else {
        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
        parent::display($tpl);
        return;
//		}
    }
}
?>
