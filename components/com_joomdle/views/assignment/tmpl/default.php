<?php
defined('_JEXEC') or die('Restricted access');

$session = JFactory::getSession();
$device = $session->get('device');
$document = JFactory::getDocument();
$itemid = JoomdleHelperContent::getMenuItem();

?>
    <!--autosize-->
    <script src="<?php echo JURI::base(); ?>components/com_community/assets/autosize.min.js"></script>
    <!-- datepicker and jquery-confirm-->
    <link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/assignment/tmpl/css/jquery-confirm.css';?>">
    <link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/assignment/tmpl/css/bootstrap-datepicker.css';?>">
    <!--    <script type="text/javascript" src="--><?php //echo JURI::base().'components/com_joomdle/views/assignment/tmpl/js/bootstrap-datepicker.js';?><!--"></script>-->

    <script src="<?php echo JURI::base(); ?>components/com_community/assets/release/js/picker.js"></script>
    <script src="<?php echo JURI::base(); ?>components/com_community/assets/release/js/picker.date.js"></script>
    <!-- <script src="<?php echo JURI::base(); ?>components/com_community/templates/jomsocial/assets/js/bootstrap-timepicker.min.js"></script> -->
    <link rel="stylesheet" href="<?php echo JURI::base(); ?>components/com_community/assets/pickadate/themes/classic.combined.css" type="text/css" />

    <script type="text/javascript" src="<?php echo JURI::base().'components/com_joomdle/views/assignment/tmpl/js/jquery-confirm.js';?>"></script>
    <script type="text/javascript" src="<?php echo JURI::base();?>/media/editors/tinymce/tinymce.min.js"></script>
    <!-- end datepicker and jquery-confirm -->
    <link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/assignment/tmpl/css/assign.css';?>">
    <script type="text/javascript" src="<?php echo JURI::base();?>/media/editors/tinymce/tinymce.min.js"></script>

<?php 
// require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');

$document = JFactory::getDocument();
$banner = new HeaderBanner(new JoomdleBanner);

$render_banner = $banner->renderBanner($this->course_info, $this->mods['sections'], $role);
?>


<?php if($this->action == 'list') {
    $jump_url =  JoomdleHelperContent::getJumpURL ();
    $count = 0;
    ?>
    <div class="assignment-list">
        <p class="lblTitle"><?php echo JText::_('COM_JOOMDLE_ASSIGNMENT'); ?><span class="contentCount"></span></p>
        <?php
        foreach ($this->mods as $tema) :
            ?>
            <div class="joomdle_course_section">
                <div class="joomdle_item_content joomdle_section_list_item_resources">
                    <?php
                    $resources = $tema['mods'];
                    if (is_array($resources)) : ?>
                        <?php
                        foreach ($resources as $id => $resource) {
                            $mtype = JoomdleHelperSystem::get_mtype ($resource['mod']);
                            if (!$mtype) // skip unknow modules
                                continue;

                            echo '<div class="assignment-list-item" data-mid="'.$resource['id'].'">';
                            $icon_url = JoomdleHelperSystem::get_icon_url ($resource['mod'], $resource['type']);
                            if ($icon_url) {
                                echo '<div class="assignment-icon">';
                                // echo '<img align="center" src="'. $icon_url.'">';
                                echo '<img align="center" src="/media/joomdle/images/AssignmentIcon.png">';
                                //echo '<p class="assessment-name">'.ucfirst($mtype).'</p>';
                                echo '</div>';
                            }

                            if ($resource['mod'] == 'label')
                            {
                                $label = JoomdleHelperContent::call_method ('get_label', $resource['id']);
                                echo '</P>';
                                echo $label['content'];
                                echo '</P>';
                                continue;
                            }

                            if (($resource['available']))
                            {
                                $direct_link = JoomdleHelperSystem::get_direct_link ($resource['mod'], $this->course_id, $resource['id'], $resource['type']);
                                if ($direct_link)
                                {
                                    // Open in new window if configured like that in moodle
                                    if ($resource['display'] == 6)
                                        $resource_target = 'target="_blank"';
                                    else
                                        $resource_target = '';

                                    if ($direct_link != 'none')
                                        echo "<a $resource_target  href=\"".$direct_link."\">".$resource['name']."</a>";
                                }
                                else
                                    echo "<a target='_blank' href=\"".JURI::base()."mod/".$mtype."_".$this->course_id."_".$resource['id']."_".$itemid.".html"."\">".$resource['name']."</a>";

                                //echo '<span class="glyphicon glyphicon-menu-down"></span>';
                                ?>
                                <div class="arrowMenuDown">
                                    <span class="glyphicon glyphicon-menu-down"></span>
                                    <div class="menuAction">
                                        <ul class="ulMenuAction">

                                            <!-- <a href="javascript: void(0);"> -->
                                            <li class="liEdit">
                                                <div class="icon-pencil"></div>
                                                <span><?php echo JText::_('COM_JOOMDLE_EDIT_ASSIGNMENT'); ?></span>
                                            </li>
                                            <!-- </a> -->

                                            <!-- <a href="javascript: void(0);"> -->
                                            <li class="liRemove">
                                                <div class="icon-delete"></div>
                                                <span><?php echo JText::_('COM_JOOMDLE_REMOVE_ASSIGNMENT'); ?></span>
                                            </li>
                                            <!-- </a> -->

                                        </ul>
                                    </div>
                                    <div class="triangle"></div>
                                </div>
                                <?php
                                echo '</div>';
                            }
                            else
                            {
                                echo $resource['name'] .'</div>';
                                if ((!$resource['available']) && ($resource['completion_info'] != '')) : ?>
                                    <div class="joomdle_completion_info">
                                        <?php echo $resource['completion_info']; ?>
                                    </div>
                                <?php
                                endif;
                            }
                            $count++;
                        }
                        ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach;
        if ($count == 0) {
            // echo '<p class="lblTitle">'.JText::_('COM_JOOMDLE_CONTENT').'</p>';
            echo '<p class="textNoContentpage">'.JText::_('COM_JOOMDLE_NO_ASSIGNMENT').'</p>';
        }
        ?>

        <div class="joomdle-remove">
            <p><?php echo Jtext::_('COM_JOOMDLE_REMOVE_ASSIGNMENT_TEXT'); ?></p>
            <button class="yes"><?php echo Jtext::_('COM_JOOMDLE_YES'); ?></button>
            <button class="no"><?php echo Jtext::_('COM_JOOMDLE_NO'); ?></button>
        </div>
        <div class="notification"><span></span></div>

    </div>
    <script type="text/javascript">
        (function($) {
            <?php //if ($count > 0) { ?>
            $('.assignment-list .contentCount').html(' (<?php echo $count; ?>)');
            <?php //} ?>
            $('.btAddContent').click(function() {
                window.location.href = "<?php echo JUri::base()."assignment/create_".$this->course_id.".html"; ?>";
            });

            $('.liEdit').click(function() {
                var mid = $(this).parent().parent().parent().parent('.assignment-list-item').attr('data-mid');
                window.location.href = "<?php echo JURI::base();?>assignment/edit_<?php echo $this->course_id;?>_"+mid+".html";
            });
            var removeMid;
            $('.liRemove').click(function() {
                removeMid = $(this).parent().parent().parent().parent('.assignment-list-item').attr('data-mid');
                $('body').addClass('overlay2');
                $('.joomdle-remove').fadeIn();
            });

            $('.assignment-list .arrowMenuDown').click(function() {
                $('.menuAction').hide();
                $('.triangle').hide();
                $(this).find('.menuAction').toggle();
                $(this).find('.triangle').toggle();
            });
            $('body').click(function(e) {
                if ( !$(e.target).hasClass('ulMenuAction') && !$(e.target).hasClass('menuAction') && !$(e.target).hasClass('arrowMenuDown') && !$(e.target).hasClass('glyphicon-menu-down') ) $('.menuAction, .triangle').hide();
            });

            $('.joomdle-remove .no').click(function () {
                $('body').removeClass('overlay2');
                $('.joomdle-remove').fadeOut();
            });
            $('.joomdle-remove .yes').click(function () {
                var modal = document.getElementById('ass-message');
                var er = document.getElementById('error');
                $('.closetitle').click(function(){
                    modal.style.display = "none";
                });
                var act = 'remove';
                $.ajax({
                    url: window.location.href,
                    type: 'POST',
                    data: {
                        aid: removeMid,
                        act: act
                    },
                    beforeSend: function () {
                        $('.joomdle-remove').fadeOut();
                        //$('.lgt-notif').html('<?php echo JText::_("REQUEST_PROCESSED");?>').addClass('lgt-visible');
                        lgtCreatePopup('', {'content': 'Loading...'});
                    },
                    success: function (data, textStatus, jqXHR) {
                        var res = JSON.parse(data);

                        lgtRemovePopup();
                        //$('.lgt-notif').removeClass('lgt-visible');
                        if (res.error == 1) {
                            // $('.errorAssign').html(res.comment);
                            // modal.style.display = "block";
                            lgtCreatePopup('oneButton', {content: res.comment}, function () {
                                lgtRemovePopup();
                            });
                            // $('.lgt-notif-error').html(res.comment).addClass('lgt-visible');
                        } else {
                            $('.joomdle_course_section .assignment-list-item[data-mid="'+removeMid+'"]').fadeOut();
                            var str = $('.contentCount').html();
                            str = str.replace("(", "");
                            str = str.replace(")", "");
                            str = parseInt(str) - 1;
                            $('.contentCount').html('(' + str + ')');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                    }
                });
            });

        })(jQuery);
    </script>

<?php } else if($this->action == 'create' || $this->action == 'edit') {
    $edit = false;
    if ($this->action == 'edit') {
        $edit = true;
        $data = $this->assign_info;
        $fileData = (array)json_decode($this->assign_info['params']);
        if (isset($fileData['fname']) && $fileData['fname'] != '') $hasFile = true; else $hasClass = false;
    }
//        $this->wrapperurl = $params->get( 'MOODLE_URL' ).'/course/modedit.php?add=assign&type=&course='.$course_id.'&section=0&return=0&sr=0'; // url iframe
    ?>
    <div class="create-assignment">
        <!-- <span class="lblTitle"> <?php //echo ($edit) ? JText::_('COM_JOOMDLE_EDIT_ASSIGNMENT') : JText::_('COM_JOOMDLE_CREATE_ASSIGNMENT'); ?><?php //echo JText::_('COM_JOOMDLE_ASSIGNMENT'); ?></span>  -->
        <div class="tit-des">
            <div class="title-assign form-group">
                <label for="tit-assign"><?php echo JText::_('COM_JOOMDLE_ASSIGNMENT_TITLE'); ?></label><span class="red">*</span><br>
                <h5 style="color:red;display:none;"><?php echo Jtext::_('ASSIGN_CREATE_NOT_EMPTY'); ?></h5>
                <input type="text" class="tit-assign" style="border: 1px solid rgba(130, 130, 130, 0.5);" required value="<?php echo $edit ? htmlentities(ucwords($data['name'])) : ''; ?>">
            </div>
            <div class="des form-group">
                <label for="des-assign"><?php echo JText::_('COM_JOOMDLE_ASSIGNMENT_INSTRUCTION'); ?></label><span class="red">*</span><br>
                <h5 style="color:red;display:none;"><?php echo Jtext::_('ASSIGN_CREATE_NOT_EMPTY'); ?></h5><br>
                <textarea style="border: 1px solid rgba(130, 130, 130, 0.5); resize: none;" class="des-assign" ><?php echo $edit ? $data['des'] : ''; ?></textarea>
            </div>

            <!--            <div class="col-md-7 col-sm-12 col-xs-12 time-quiz">-->
            <!--                <div class=" col-md-4 col-sm-5 col-xs-8 duadate">-->
            <div class="divDueDate" style="margin: 15px 0;">
                <span style="width: 30%;display: block"><?php echo JText::_('COM_JOOMDLE_DUE_DATE'); ?></span>
                <div class="date " style="position: relative;">
                    <input style="width: 140px;text-align:center; border: 1px solid rgba(130, 130, 130, 0.5);" type="text" class="form-control dua_date" id="txtDueDate" placeholder="dd/mm/yyyy" value="">
                </div>
            </div>
            <div class = "notetext"><p><?php echo JText::_('COM_JOOMDLE_ASSIGNMENT_NOTE_TEXT'); ?></p></div>
            <!--            </div>-->
            <div class="col-xs-12" style="/*border-top:1px solid #B8D7E2;*/"></div>

            <div class="btn-bottom" style="padding-top:15px !important;">
                <div class="file_assignment" <?php echo $hasFile ? 'style="display: block;"' : ''; ?>>
                    <div class="file-assignment pull-right" width="100%" <?php echo $hasFile ? 'data-itemid="'.$fileData['itemid'].'"' : ''; ?>><?php echo $hasFile ? $fileData['fname'] : ''; ?></div>
                    <b class="rm-assignment"><img src="../images/delete.png"></b>
                </div>

            </div>


            <!--                <div class="btn-bottom">-->
            <!--                    <a href="--><?php //echo JURI::base().'index.php?option=com_joomdle&view=coursecontent&course_id='.$course_id;?><!--"><span class="cancel">--><?php //echo Jtext::_('ASSIGN_CREATE_CANCEL'); ?><!--</span></a>-->
            <!--                    <a><button class="btnn create_assign">Save</button></a>-->
            <!--                    <a><span class="btnn preview-class">Preview</span></a>-->
            <!--                </div>-->

            <div class="clear"></div>
            <div class="buttons changePosition">
                <button class="btSave create_assign <?php echo $hasFile ? '' : 'hidden'; ?>"><?php echo JText::_('COM_JOOMDLE_SAVE'); ?></button>
                <button class="btnn assignment-brief <?php echo $hasFile ? 'hidden' : ''; ?>" style="<?php echo $hasFile ? 'background: rgb(155, 157, 158);display:none;' : ''; ?>" <?php echo $hasFile ? 'disabled' : ''; ?>><?php echo JText::_('COM_JOOMDLE_ASSIGNMENT_BRIEF');?>  </button>
                <button class="btPreview preview-class" type="button"><?php echo JText::_('COM_JOOMDLE_PREVIEW'); ?></button>
                <button class="btCancel" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>

            </div>
        </div>
    </div>
    <form method="post" class="uploadFile" action="" enctype='multipart/form-data' style="display:none;">
        <input name="assignment_file" class="fileassignment" id="toan" type="file">
    </form>

    <!-- End title-des ***-->
    <div class="rmPopup changePosition hienPopup">
        <p><?php echo JText::_('COM_JOOMDLE_REMOVE_ASSIGNMENT_BRIEF_TEXT'); ?></p>
        <button class="btRMCancel" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
        <button class="btRMYes" type="button"><?php echo JText::_('COM_JOOMDLE_YES'); ?></button>
    </div>

    <!-- This is Preview -->
    <div class="preview-assignment divPreview">
        <div class="prev" style="clear:both !important;">
            <div class="title-assign">
                <p class="title" ></p>
            </div>
        </div>
        <!--  <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2 col-sm-offset-2 img-preview">
            <img width="100%" height="350" style="border-radius:10px;" src="<?php echo JURI::base().'components/com_joomdle/views/assignment/tmpl/assignment.png';?>"  alt="">
        </div> -->
        <div class="intro-preview" style="clear:both;">
            <span><b><?php echo JText::_('COM_JOOMDLE_INTRODUCTION'); ?></b></span>
            <div class="clear" ></div>
            <div class="text-intro-preview" style="text-align: justify;"></div>
            <div class="content-preview"></div>

        </div>
        <div class = "mnduePreview">
            <span class="pull-right"><b><?php echo JText::_('COM_JOOMDLE_DUE_DATE'); ?>: <span class="due_date_preview">26/10/2016</span></b></span>
        </div>
        <!--   <div class="btn-bottom" style="padding-top:25px !important;">
            <a disable="disable"><button class="btnn" style="background:#9B9D9E !important;"><?php echo JText::_('COM_JOOMDLE_ASSIGNMENT_BRIEF'); ?>  <img style="margin-left:7px;margin-top:-5px;" width="15" height="15" src="<?php echo JURI::base().'components/com_joomdle/views/assignment/tmpl/Download.png';?>"></button></a>
        </div> -->
        <div class="btn-bottom" style="padding-top:50px !important;">
            <a disable="disable"><span class="cancel prev-preview previewBack" style="background:#4F5355 !important; color:white !important;cursor: pointer;"><?php echo JText::_('COM_JOOMDLE_EXIT_PREVIEW'); ?></span></a>
            <!-- <a disable="disable"><button class="btnn" style="background:#9B9D9E !important;margin-bottom: 15px;"><?php echo JText::_('COM_JOOMDLE_SUBMIT'); ?> <i style="margin-left:7px;margin-top:-5px;">→</i> </button></a> -->
        </div>

    </div>

    <div style="clear:both;"></div>

    <div class="notification"></div>
    <div class="modal" id="ass-message">
        <div class="modal-content hienPopup">
            <p id = "error" class="errorAssign"></p>
            <button class="closetitle" type="button"><?php echo JText::_('COM_JOOMDLE_CLOSE'); ?></button>
        </div>
    </div>
<?php } else if (isset($this->action) && $this->action == 'remove') {
    if ($this->assign_info['error'] != 1) {
        ?>
        <div class="joomdle-remove">
            <p><?php echo Jtext::_('COM_JOOMDLE_REMOVE_ASSIGNMENT_TEXT'); ?></p>
            <button class="yes"><?php echo Jtext::_('COM_JOOMDLE_YES'); ?></button>
            <button class="no"><?php echo Jtext::_('COM_JOOMDLE_NO'); ?></button>
        </div>
        <div class="notification"><span></span></div>

        <script type="text/javascript">
            (function ($) {
                var modal = document.getElementById('ass-message');
                var er = document.getElementById('error');
                $('.closetitle').click(function(){
                    modal.style.display = "none";
                });
                $('.joomdle-remove .no').click(function () {
                    window.location.href = "<?php echo JUri::base()."assignment/list_".$this->course_id.'.html'; ?>";
                });
                $('.joomdle-remove .yes').click(function () {
                    var act = 'remove';
                    $.ajax({
                        url: window.location.href,
                        type: 'POST',
                        data: {
                            <?php echo (isset($this->aid)) ? "aid: ".$this->aid."," : "" ; ?>
                            act: act
                        },
                        beforeSend: function () {
                            //$('.lgt-notif').html('<?php echo JText::_("REQUEST_PROCESSED");?>').addClass('lgt-visible');
                        },
                        success: function (data, textStatus, jqXHR) {
                            var res = JSON.parse(data);

                            $('body').removeClass('overlay2');
                            $('.notification').fadeOut();
                            // $('.lgt-notif').removeClass('lgt-visible');
                            if (res.error == 1) {
                                // $('.errorAssign').html(res.comment);
                                // modal.style.display = "block";
                                lgtCreatePopup('oneButton', {content: res.comment}, function () {
                                    lgtRemovePopup();
                                });
                                //$('.lgt-notif-error').html(res.comment).addClass('lgt-visible');
                            } else {
                                window.location.href = "<?php echo JUri::base()."assignment/list_".$this->course_id.'.html'; ?>";
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log('ERRORS: ' + textStatus);
                        }
                    });
                });

            })(jQuery);
        </script>
        <?php
    } else {
        echo '<p class="tcenter" style="padding-top:30px;">'.JText::_('COM_JOOMDLE_ASSIGNMENT_REMOVED').'</p>';
    }
} ?>

    <script type="text/javascript">
        jQuery(document).ready(function(e) {
            jQuery('.des-assign').each(function () {
                autosize(this);
            }).on('input', function () {
                autosize(this);
            });

            var offset = new Date().getTimezoneOffset();

            <?php if ( $this->action == 'edit' ) { ?>
            var dt = new Date (Date.UTC( <?php echo date('Y', $data['duedate']).','.(date('m', $data['duedate'])-1).','.date('d', $data['duedate']).','.date('H', $data['duedate']).', 0, 0'; ?> ));
            var dday = dt.getDate();
            var dmonth = parseInt(dt.getMonth()) + 1;
            if (dday < 10) dday = '0' + dday;
            if (dmonth < 10) dmonth = '0' + dmonth;
            var dd = dday + '/' + dmonth + '/' + dt.getFullYear();
            if (dd == '01/01/1970')
                dd = '';
            jQuery('.dua_date').val(dd);
            <?php } ?>

            /*jQuery(document).on('keyup', 'input, textarea', function() {
                if (jQuery(this).val().trim() != '') jQuery(this).parents('.form-group').removeClass('hasError');
            });*/
            jQuery('.create_assign').click(function(e) {
                e.preventDefault();

                var validateResult = true;
                var modal = document.getElementById('ass-message');
                var er = document.getElementById('error');
                jQuery('.closetitle').click(function(){
                    modal.style.display = "none";
                });
                var d = new Date();
                var month = d.getMonth() + 1;
                var day = d.getDate();

                var output = (day < 10 ? '0' : '') + day +'/'+ (month < 10 ? '0' : '') + month + '/' + d.getFullYear();
                var cont = '';
                var j = 0;
                var q = jQuery('.des-assign').val();
                cont += q + "<br>";
                var dua_date = jQuery('.dua_date').val().split('/');
                var publish_date = output.split('/');
                if (jQuery('.tit-assign').val().trim() == '') {
                    jQuery('.tit-assign').on('keyup', function() {
                        if (jQuery(this).val().trim()) jQuery(this).parents('.form-group').removeClass('hasError');
                        else jQuery(this).parents('.form-group').addClass('hasError');
                    }).val('').parents('.form-group').addClass('hasError');
                    validateResult = false;
                }
                if (jQuery('.des-assign').val().trim() == '') {
                    jQuery('.des-assign').on('keyup', function() {
                        if (jQuery(this).val().trim()) jQuery(this).parents('.form-group').removeClass('hasError');
                        else jQuery(this).parents('.form-group').addClass('hasError');
                    }).val('').parents('.form-group').addClass('hasError');
                    validateResult = false;
                }
                //  else if (dua_date == '') {
                //     $('.errorAssign').html("Due date must not be empty.");
                //     modal.style.display = "block";
                //    // jQuery('.lgt-notif-error').html("Due date must not be empty.").addClass('lgt-visible');
                // }
                // else
                if (jQuery('.dua_date').val().trim() != '' && !checkDateFormat(jQuery('.dua_date').val())) {
                    //  $('.errorAssign').html("Due date format is invalid.");
                    // modal.style.display = "block";
                    jQuery('.dua_date').css('border','1px solid #bb5a66');
                    validateResult = false;
                    //jQuery('.lgt-notif-error').html("Due date format is invalid.").addClass('lgt-visible');
                } else if ( parseInt(dua_date[2]) < parseInt(publish_date[2]) || parseInt(dua_date[2]) == parseInt(publish_date[2]) && parseInt(dua_date[1]) < parseInt(publish_date[1]) || parseInt(dua_date[2]) == parseInt(publish_date[2]) && parseInt(dua_date[1]) == parseInt(publish_date[1]) && parseInt(dua_date[0]) < parseInt(publish_date[0])) {
                    // jQuery('.errorAssign').html("Due date must not be set before today.");
                    // modal.style.display = "block";
                    lgtCreatePopup('oneButton', {content: "Due date must not be set before today."}, function () {
                        lgtRemovePopup();
                    });
                    validateResult = false;
                    //  jQuery('.lgt-notif-error').html("Due date must not be set before today.").addClass('lgt-visible');
                } else {
                    if (jQuery('.fileassignment').val() == '') {

                    } else {
                        var file = jQuery('.fileassignment')[0].files[0];
                        if ( file.type != 'application/msword' && file.type != 'application/pdf' && file.type != 'application/vnd.ms-excel' && file.type != 'application/vnd.oasis.opendocument.spreadsheet' && file.type != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' && file.type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && file.type != 'text/plain' && file.type != 'application/vnd.oasis.opendocument.text' ) {
                            lgtCreatePopup('oneButton', {content: "The file does not match format. Tip: File is doc, pdf, excel or text."}, function () {
                                lgtRemovePopup();
                            });
                            validateResult = false;
                        } else {
                            jQuery('.uploadFile').submit();
                        }
                    }
                }
                if (validateResult) {
                    var act = '<?php echo ($this->action == 'edit') ? "update" : "create" ; ?>';
                    var fn = (jQuery('.file_assignment').css('display') == 'none') ? '' : jQuery('.file-assignment').html();
                    var iid = (jQuery('.file_assignment').css('display') == 'none') ? '' : jQuery('.file-assignment').attr('data-itemid');
                    jQuery.ajax({
                        url: window.location.href,
                        type: 'POST',
                        data: {
                            <?php echo ($this->action == 'edit') ? "aid: ".$this->aid."," : "" ; ?>
                            act: act,
                            name: jQuery('.tit-assign').val(),
                            des: jQuery('.des-assign').val(),
                            fname: fn,
                            itemid: iid,
                            duedate: jQuery('.dua_date').val(),
                            courseid : <?php echo $this->course_id; ?>,
                            timezoneOffset: offset,
                            sectionid: <?php if($this->sectionid != Null){echo $this->sectionid;} else echo 0;?>
                        },
                        beforeSend: function() {
                            //jQuery('.lgt-notif').html('<?php echo JText::_("REQUEST_PROCESSED");?>').addClass('lgt-visible');
                            lgtCreatePopup('', {'content': 'Loading...'});
                        },
                        success: function(data, textStatus, jqXHR) {
//                            lgtRemovePopup();
                            var res = JSON.parse(data);

                            if (res.error == 1) {
                                // jQuery('body').addClass('overlay2');
                                // jQuery('.errorAssign').html(res.comment);
                                // modal.style.display = "block";
                                lgtCreatePopup('oneButton', {content: res.comment}, function () {
                                    lgtRemovePopup();
                                });
                            } else {
                                window.location.href = '<?php echo JUri::base()."course/".$this->course_id.".html"; ?>';
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log('ERRORS: ' + textStatus);
                        }
                    });
                }
            });

            jQuery('.preview-class').click(function() {
                var modal = document.getElementById('ass-message');
                var er = document.getElementById('error');
                jQuery('.closetitle').click(function(){
                    jQuery('body').removeClass('overlay2');
                    modal.style.display = "none";
                });

                jQuery('#title-mainnav').html('Preview');
                var d = new Date();
                var month = d.getMonth()+1;
                var day = d.getDate();

                var output = (day<10 ? '0' : '') + day +'/'+ (month<10 ? '0' : '') + month + '/' +d.getFullYear();
                var cont = '';
                var j = 0;
                var q = jQuery('.des-assign').val();
                cont += q + "<br>";
                var dua_date = jQuery('.dua_date').val().split('/');
                var publish_date = output.split('/');
                if (jQuery('.tit-assign').val().trim() == '' || jQuery('.des-assign').val().trim() == '' ) {
                    // jQuery('.errorAssign').html("Title or instructions must not be empty.");
                    // modal.style.display = "block";
                    lgtCreatePopup('oneButton', {content: "Title or instructions must not be empty."}, function () {
                        lgtRemovePopup();
                    });
                }
                //  else if(dua_date == '') {
                //      $('.errorAssign').html("Due date must not be empty.");
                //     modal.style.display = "block";

                //     //jQuery('.lgt-notif-error').html("Due date must not be empty.").addClass('lgt-visible');
                // }
                else if (dua_date != '' && !checkDateFormat(jQuery('.dua_date').val())) {
                    // jQuery('.errorAssign').html("Due date must not be empty.");
                    // modal.style.display = "block";
                    lgtCreatePopup('oneButton', {content: "Due date must not be empty."}, function () {
                        lgtRemovePopup();
                    });
                    // jQuery('.lgt-notif-error').html("Due date format is invalid.").addClass('lgt-visible');
                } else if( parseInt(dua_date[2]) < parseInt(publish_date[2]) || parseInt(dua_date[2]) == parseInt(publish_date[2]) && parseInt(dua_date[1]) < parseInt(publish_date[1]) || parseInt(dua_date[2]) == parseInt(publish_date[2]) && parseInt(dua_date[1]) == parseInt(publish_date[1]) && parseInt(dua_date[0]) < parseInt(publish_date[0])){
                    // jQuery('.errorAssign').html("Due date must not be set before today.");
                    // modal.style.display = "block";
                    lgtCreatePopup('oneButton', {content: "Due date must not be set before today."}, function () {
                        lgtRemovePopup();
                    });
                    // jQuery('.lgt-notif-error').html("Due date must not be set before today.").addClass('lgt-visible');
                } else {
                    jQuery('.course-nav').css("display","block");
                    jQuery('.prev .title-assign .title').html(jQuery('.tit-assign').val());
//                    jQuery('.text-intro-preview').html(jQuery('.des-assign').val());
                    var des = (jQuery('.des-assign').val());
                    var obj = jQuery('.text-intro-preview').text(des);
                    obj.html(obj.html().replace(/\n/g,'<br/>'));
                    jQuery('.due_date_preview').html(jQuery('.dua_date').val() || '-');
                    jQuery('.tit-des').hide();
                    jQuery('.create-assignment').hide();
                    jQuery('.preview-assignment').addClass('visible');
                }
            });

        });

        function checkDateFormat(string) {
            var res = string.split('/');
            var valid = true;
            if (res[0].length > 2 || res[1].length > 2 || res[2].length > 4 || parseInt(res[0]) > 31 || parseInt(res[1]) > 12 ) valid = false;
            return valid;
        }

        (function($) {

            var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);
            var $input = $('#txtDueDate').pickadate({
                format: 'dd/mm/yyyy',
                editable: 'true',
                disable: [
                    { from: [0,0,0], to: yesterday }
                ]
            });

            var picker = $input.pickadate('picker');
            $input.on('click', function(event) {
                if (picker.get('open')) {
                    picker.close();
                } else {
                    picker.open();
                    $('html,body').animate({scrollTop: $('.picker__holder').offset().top}, 'slow');
                }
                event.stopPropagation();
            });

            $('.rm-notification').click(function() {
                $(this).parent().fadeOut();
            });
            $('.rmPopup .btRMCancel').click(function() {
                $('.rmPopup').hide();
                $('body').removeClass('overlay2');
            });
            $('.rm-assignment').click(function() {
                $('.rmPopup').show();
                $('.assignment-brief').addClass('hidden');
                $('body').addClass('overlay2');
            });
            $('.rmPopup .btRMYes').click(function() {
                <?php if ($this->action == 'edit') { ?>
                $('.file_assignment').hide();
                $('.rmPopup').hide();
                $('.assignment-brief').removeAttr('disabled').css('background','#4F5355').fadeIn();
                $('.fileassignment').val('');
                $('.assignment-brief').removeClass('hidden');
                $('.create_assign').addClass('hidden');
                $('body').removeClass('overlay2');

                <?php } else { ?>
                $('.file_assignment').hide();
                $('.rmPopup').hide();
                $('.assignment-brief').removeAttr('disabled').css('background','#4F5355').fadeIn();
                $('.fileassignment').val('');
                $('.assignment-brief').removeClass('hidden');
                $('.create_assign').addClass('hidden');
                $.ajax({
                    url: window.location.href,
                    type: 'POST',
                    data: {
                        act: 'deleteFile',
                        fname: $('.file-assignment').html(),
                        itemid: $('.file-assignment').attr('data-itemid')
                    },
                    beforeSend: function() {
                        $('.rmPopup').hide();
                        //$('.lgt-notif').html('<?php echo JText::_("REQUEST_PROCESSED");?>').addClass('lgt-visible');
                        lgtCreatePopup('', {'content': 'Loading...'});
                    },
                    success: function(data, textStatus, jqXHR) {
                        var res = JSON.parse(data);

                        $('body').removeClass('overlay2');
                        $('.notification').fadeOut();

                        //$('.lgt-notif').removeClass('lgt-visible');
                        if (res.error == 1) {
                            // $('.errorAssign').html(res.comment);
                            // modal.style.display = "block";
                            jQuery('.lgtPopup').remove();
                            lgtCreatePopup('oneButton', {content: res.comment}, function () {
                                lgtRemovePopup();
                            });
                            // $('.lgt-notif-error').html(res.comment).addClass('lgt-visible');
                        } else {
                            lgtRemovePopup();
                            $('.file_assignment').hide();
                            $('.assignment-brief').removeAttr('disabled').css('background','#4F5355').fadeIn();
                            $('.fileassignment').val('');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                    }
                });
                <?php } ?>
            });

            $('.buttons .btCancel').click(function() {
                window.location.href = '<?php echo JUri::base()."course/".$this->course_id.".html"; ?>';
            });

            $('.assignment-brief').click(function(){
                $('.fileassignment').click();
            });

            $('.prev-preview').click(function() {
                jQuery('.course-nav').css("display","none");
                $('#title-mainnav').html('Add Assignment');
                $('.tit-des').show();
                $('.create-assignment').show();
                $('.preview-assignment').removeClass('visible');
            });

            $('.assign-edit').click(function() {
                var x = $(this).attr('data-aid');
                window.location.href = "<?php echo JUri::base()."assignment/edit_".$this->course_id; ?>_"+x+".html";
            });

            $('.edit-delete').hover(function() {
                $('.edit-delete #edit-delete').hide();
                $(this).find('#edit-delete').show();
            },function(){
                $(this).find('#edit-delete').hide();
            });


//            $(".dua_date").datepicker({
//                format: 'dd/mm/yyyy',
//                todayHighlight: true,
//                autoclose: true
//            });

            $('.t3-mainbody').click(function(e) {
                $("#edit-delete").hide();
            });

            var files;

            $('.fileassignment').change(function (event){
                files = event.target.files;
                if (files.length > 0)
                    $('.uploadFile').submit();
                $('.fileassignment').val('');
            });


            $('.uploadFile').on('submit', uploadFiles);
            function uploadFiles(event) {
                var modal = document.getElementById('ass-message');
                var er = document.getElementById('error');
                $('.closetitle').click(function(){
                    modal.style.display = "none";
                    jQuery('body').removeClass('overlay2');
                });
                event.stopPropagation();
                event.preventDefault();

                var data = new FormData();
                $.each(files, function(key, value) {
                    data.append(key, value);
                });

                $.ajax({
                    url: window.location.href,
                    type: 'POST',
                    data: data,
                    cache: false,
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        lgtCreatePopup('', {'content': 'Loading...'});
                        $('.ajax-load').attr('src','<?php echo JURI::base().'components/com_joomdle/views/assignment/tmpl/load.gif';?>');
                        $('.create_assign').attr('disabled',true);
                    },
                    complete: function () {
                        $('.ajax-load').attr('src','<?php echo JURI::base().'components/com_joomdle/views/assignment/tmpl/Upload.png';?>');
                    },
                    success: function(data, textStatus, jqXHR) {
                        var res = JSON.parse(data);

                        if (res.error == 1) {
                            // jQuery('body').addClass('overlay2');
                            // $('.errorAssign').html(res.comment);
                            // modal.style.display = "block";
                            jQuery('.lgtPopup').remove();
                            lgtCreatePopup('oneButton', {content: res.comment}, function () {
                                lgtRemovePopup();
                            });
                            //$('.lgt-notif-error').html(res.comment).addClass('lgt-visible');
                        } else {
                            lgtRemovePopup();
                            $('.create_assign').removeAttr('disabled');
                            var iconUrl;
                            var baseUrl = '<?php echo JURI::base();?>';
                            var imgArr = ['image/png', 'image/jpeg', 'image/gif', 'image/bmp'];
                            var videoArr = ['video/mp4'];
                            var documentArr = ['application/vnd.oasis.opendocument.text',
                                'application/msword',
                                'application/pdf',
                                'application/vnd.oasis.opendocument.spreadsheet',
                                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                                'text/plain',
                                'application/zip',
                                'application/x-zip-compressed',
                                // 'application/octet-stream',
                                'application/x-ms-dos-executable',

                                //file excel
                                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                'application/vnd.ms-excel',
                                'application/excel',
                                'application/x-excel',
                                'application/x-msexcel',
                                // file ppt
                                'application/mspowerpoint',
                                'application/powerpoint',
                                'application/vnd.ms-powerpoint',
                                'application/x-mspowerpoint',
                                // file pptx
                                'application/vnd.openxmlformats-officedocument.presentationml.presentation'];

                            for (var i = 0; i < res.data.length; i++) {
                                var name = res.data[i].name;
                                var type = res.data[i].type;
                                var url = res.data[i].url;
                                var itemid = res.data[i].itemid;

                                switch (type) {
                                    case "text/csv":
                                        iconUrl = baseUrl+'/images/icons/attach_csv30x30.png';
                                        break;
                                    case "application/msword":
                                        iconUrl = baseUrl+'/images/icons/attach_doc30x30.png';
                                        break;
                                    case "application/pdf":
                                        iconUrl = baseUrl+'/images/icons/attach_pdf30x30.png';
                                        break;
                                    case "application/mspowerpoint":
                                    case "application/powerpoint":
                                    case "application/vnd.ms-powerpoint":
                                    case "application/x-mspowerpoint":
                                        if (name.indexOf('ppt') !== -1) iconUrl = baseUrl+'/images/icons/attach_ppt30x30.png';
                                        if (name.indexOf('pps') !== -1) iconUrl = baseUrl+'/images/icons/attach_pps30x30.png';
                                        break;
                                    case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                                        iconUrl = baseUrl+'/images/icons/attach_ppt30x30.png';
                                        break;
                                    case "application/rtf":
                                    case "application/x-rtf":
                                    case "text/richtext":
                                        iconUrl = baseUrl+'/images/icons/attach_rtf30x30.png';
                                        break;
                                    case "image/svg+xml":
                                        iconUrl = baseUrl+'/images/icons/attach_svg30x30.png';
                                        break;
                                    case "text/plain":
                                        iconUrl = baseUrl+'/images/icons/attach_txt30x30.png';
                                        break;
                                    case "application/excel":
                                    case "application/vnd.ms-excel":
                                    case "application/x-excel":
                                    case "application/x-msexcel":
                                        iconUrl = baseUrl+'/images/icons/attach_xls30x30.png';
                                        break;
                                    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                                        iconUrl = baseUrl+'/images/icons/attach_xls30x30.png';
                                        break;
                                    case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                                        iconUrl = baseUrl+'/images/icons/attach_doc30x30.png';
                                        break;
                                    case "application/octet-stream":
                                    case "application/x-ms-dos-executable":
                                        iconUrl = baseUrl+'/images/icons/attach_file30x30.png';
                                        break;
                                    default:
                                        iconUrl = baseUrl+'/media/joomdle/images/icon/courseoutline/CourseContent_Inactive.png';
                                        break;
                                }

                                if (imgArr.indexOf(type) != -1) {

                                } else if (videoArr.indexOf(type) != -1) {

                                } else if (documentArr.indexOf(type) != -1) {
                                    $('.file-assignment').html('<img src="' + iconUrl + '"><span>' + name + '</span>').attr('data-itemid', itemid);
                                    $('.file_assignment').show();
                                    $('.assignment-brief').attr('disabled', true).css('background','#9B9D9E').hide();
                                    $('.create_assign').removeClass('hidden');
                                } else {

                                }
                            }
                            var $el = $('.fileassignment');
                            $el.wrap('<form>').closest('form').get(0).reset();
                            $el.unwrap();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                    }
                });
            }

        })(jQuery);
    </script>

<?php
function VNtoEN($str) {
    // In thường
    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
    $str = preg_replace("/(đ)/", 'd', $str);
    // In đậm
    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
    $str = preg_replace("/(Đ)/", 'D', $str);
    return $str; // Trả về chuỗi đã chuyển
}

?>