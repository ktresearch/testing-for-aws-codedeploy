<?php

require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'components/com_joomdle/views/assessment/tmpl/assessment.css');
?>
<?php if (JFactory::getUser()->isMoodleAdmin) : ?>
    <div class="joomdle-quiz-ar <?php echo $this->pageclass_sfx;?>">
        <h1><?php echo Jtext::_('ASSESSMENT_ACCESS_READINESS'); ?></h1>
        <form action="" method="post" class="formQuizAR">
            <label for=""><?php echo Jtext::_('ASSESSMENT_TITLE'); ?></label><input type="text" name="txtTitle" class="txtTitle"/>
            <label for=""><?php echo Jtext::_('ASSESSMENT_DESCRIPTION'); ?></label><textarea name="txtaDescription" class="txtaDescription"/></textarea>
            <button class="btAddQuestions"><?php echo Jtext::_('ASSESSMENT_BTN_ADD_QUESTIONS'); ?></button>
            <button class="btCancel"><?php echo Jtext::_('ASSESSMENT_CANCEL'); ?></button>
            <button class="btPreview"><?php echo Jtext::_('ASSESSMENT_PREVIEW'); ?></button>
            <button class="btSave"><?php echo Jtext::_('ASSESSMENT_SAVE'); ?></button>
        </form>
    </div>
    <script>
        (function($) {
            $('.formQuizAR').submit(function(e) {
                e.preventDefault();
            });
            $('.formQuizAR .btCancel').click(function() {
                var nav = window.navigator;
                if( this.phonegapNavigationEnabled &&
                    nav &&
                    nav.app &&
                    nav.app.backHistory ){
                    nav.app.backHistory();
                } else {
                    window.history.back();
                }

            });
            $('.formQuizAR .btSave').click(function() {

                $.ajax({
//                    'url': 'http://localhost/kv/index.php?option=com_joomdle&task=createAssessment',
                    'url' : 'http://localhost/kv/courses/course/modedit.php',
                    'data': {
                        'assessmentType': 1,
                        'title': $('.txtTitle').val(),
                        'description': $('.txtaDescription').val()
                    },
                    success: function(result) {
                        document.write(result);
                    }
                });
            });
        })(jQuery);
    </script>
<?php endif;?>
