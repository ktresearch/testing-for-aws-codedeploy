<?php defined('_JEXEC') or die('Restricted access');

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'components/com_joomdle/views/assessment/tmpl/assessment.css');
$session = JFactory::getSession();
$is_mobile = $session->get('device') == 'mobile' ? true : false;

$role = $this->hasPermission;
$this->selectedQues = explode(',', $this->assessment['questions']);

$itemid = JoomdleHelperContent::getMenuItem();

$linkstarget = $this->params->get('linkstarget');
if ($linkstarget == "new") $target = " target='_blank'"; else $target = "";

require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);
$render_banner = $banner->renderBanner($this->course_info, $this->mods, $role);

if ($this->hasPermission[0]['hasPermission']) :
    if (isset($this->action) && ($this->action == 'create' || $this->action == 'edit')) {
        if ($this->action == 'edit') $edit = true; else $edit = false;
        ?>
        <div class="joomdle-quiz-ar joomdle-course-assessment <?php echo $this->pageclass_sfx ?>" <?php echo ($edit) ? 'style="padding-top: 60px;"' : ''; ?>>
            <form action="" method="post" class="formQuizAR">
                <div class="form-group">
                    <label for="txtTitle" class="assessment_title"><?php echo Jtext::_('ASSESSMENT_TITLE'); ?></label><span class="red">*</span>
                    <input type="text" name="txtTitle" class="txtTitle" value="<?php echo ($edit) ? htmlentities(ucwords($this->assessment['name'])) : ''; ?>"/>
                </div>
                <div class="form-group">
                    <label for="" class="assessment_des"><?php echo Jtext::_('ASSESSMENT_DESCRIPTION'); ?></label><span class="red">*</span>
                    <textarea name="txtaDescription" <?php echo (!$edit) ? 'data-ft="1"' : ''; ?> class="txtaDescription"/><?php echo ($edit) ? $this->assessment['des'] : ''; ?></textarea>
                </div>
                <?php if ($edit == false) { ?>
                    <div class="firstPurpose form-group">
                        <label for=""><?php echo Jtext::_('COM_JOOMDLE_ASSESSMENT_TYPE_TITLE'); ?></label><span
                            class="red">*</span>
                        <div class="inputPurpose">
                            <div p="1" class="status"></div>
                            <div class="inputPurpose_des">
                                <span class="spanstatus"><?php echo Jtext::_('ASSESSMENT_ASSESS_READINESS'); ?>:</span>
                                <p><?php echo Jtext::_('ASSESSMENT_ASSESS_READINESS_DES'); ?></p>
                            </div>
                        </div>
                        <div class="inputPurpose">
                            <div p="2" class="status"></div>
                            <div class="inputPurpose_des">
                                <span class="spanstatus"><?php echo Jtext::_('ASSESSMENT_CHECK_UNDERSTANDING'); ?>:</span>
                                <p><?php echo Jtext::_('ASSESSMENT_CHECK_UNDERSTANDING_DES'); ?></p>
                            </div>
                        </div>

                        <button class="btNext pull-right" type="button"><?php echo Jtext::_('ASSESSMENT_SAVE'); ?></button>
                        <button class="btCancel1 pull-right" type="button"><?php echo Jtext::_('ASSESSMENT_CANCEL'); ?></button>
                    </div>
                <?php } ?>
                <input type="hidden" value="<?php echo ($edit) ? $this->assessmentType : ''; ?>" name="hdPurpose" class="hdPurpose"/>
                <div class="lastPurpose <?php echo ($edit) ? 'active' : ''; ?>">
                    <p class="lastPurposeDes"><?php echo Jtext::_('COM_JOOMDLE_ASSESSMENT_TYPE_TITLE'); ?><span class="red">*</span></p>
                    <div class="lastPurposestatus actived"></div>
                    <span class="assess_type">
                        <?php
                        if ($edit && $this->assessmentType && $this->assessmentType == 1) {
                            echo JText::_('ASSESSMENT_ASSESS_READINESS');
                        } else if ($edit && $this->assessmentType && $this->assessmentType == 2) {
                            echo JText::_('ASSESSMENT_CHECK_UNDERSTANDING');
                        } else if ($edit && $this->assessmentType && $this->assessmentType == 3) {
                            echo JText::_('ASSESSMENT_CREATE_QUIZ');
                        }
                        ?>
                    </span>
                </div>

                <div class="gradingScheme <?php echo ($edit && $this->assessmentType == 2) ? 'active' : ''; ?>">
                    <div class="divGradingScheme">
                        <label><?php echo JText::_('COM_JOOMDLE_GRADING_SCHEME'); ?></label>
                        <div class="yesnoIcon <?php echo (empty($this->assessment)) ? 'no' : (($this->assessment['gradingscheme']) ? 'yes' : 'no'); ?>">
                            <div class="block"></div>
                        </div>
                        <input type="hidden" name="hdGradingScheme" class="hdGradingScheme"
                               value="<?php echo (empty($this->assessment)) ? '0' : (($this->assessment['gradingscheme']) ? '1' : '0'); ?>"/>
                    </div>
                    <div class="divGradePercentage <?php echo ($edit && $this->assessment['gradingscheme']) ? 'active' : ''; ?>">
                        <label class="divGradePercentageDes"><?php echo JText::_('COM_JOOMDLE_GRADING_SCHEME_PERCENTAGE'); ?></label>
                        <div class="divSetPercent container-fluid">
                            <div class="row">
                                <div class="divGrade col-md-2 col-sm-2 col-xs-4">
                                    <label>A &ge;</label>
                                    <input type="text" class="txtGrade txtAssessmentGradeA" name="txtAssessmentGradeA"
                                           placeholder="0"
                                           value="<?php echo (empty($this->assessment['gradeboundaryA'])) ? '' : $this->assessment['gradeboundaryA']; ?>"/>
                                    <span><?php echo JText::_('GRADING_SCHEME_PERCENTAGE'); ?></span>
                                </div>
                                <div class="divGrade col-md-2 col-sm-2 col-xs-4">
                                    <label>B &ge;</label>
                                    <input type="text" class="txtGrade txtAssessmentGradeB" name="txtAssessmentGradeB"
                                           placeholder="0"
                                           value="<?php echo (empty($this->assessment['gradeboundaryB'])) ? '' : $this->assessment['gradeboundaryB']; ?>"/>
                                    <span><?php echo JText::_('GRADING_SCHEME_PERCENTAGE'); ?></span>
                                </div>
                                <div class="divGrade col-md-2 col-sm-2 col-xs-4">
                                    <label>C &ge;</label>
                                    <input type="text" class="txtGrade txtAssessmentGradeC" name="txtAssessmentGradeC"
                                           placeholder="0"
                                           value="<?php echo (empty($this->assessment['gradeboundaryC'])) ? '' : $this->assessment['gradeboundaryC']; ?>"/>
                                    <span><?php echo JText::_('GRADING_SCHEME_PERCENTAGE'); ?></span>
                                </div>
                                <div class="divGrade col-md-2 col-sm-2 col-xs-4">
                                    <label>D &ge;</label>
                                    <input type="text" class="txtGrade txtAssessmentGradeD" name="txtAssessmentGradeD"
                                           placeholder="0"
                                           value="<?php echo (!isset($this->assessment['gradeboundaryD'])) ? '' : $this->assessment['gradeboundaryD']; ?>"/>
                                    <span><?php echo JText::_('GRADING_SCHEME_PERCENTAGE'); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                $questions = (isset($this->questions['status']) && $this->questions['status'] == 1) ? $this->questions['questions'] : [];
                $listq = '';
                $count = 0;
                $selectedQuestions = array();
                $questions = array_reverse($questions);
                ?>
                <ol class="listQuestions <?php echo ($edit && count($questions) > 0) ? 'active' : ''; ?>">
                    <label><?php echo JText::_('COM_JOOMDLE_QUESTIONS'); ?></label>
                    <div class="listQuestionsUl">
                        <?php
                        if ($edit) {
                        foreach ($questions as $k => $v) {
                            $v2 = $v;
                            $v2['options'] = json_decode($v2['options'], true);
                            if ($edit && !in_array($v['id'], $this->selectedQues)) continue;
                            if (in_array($v['id'], $this->selectedQues)) $selectedQuestions[$v['id']] = $v2;
                            $listq = $listq . ',' . $v['id'];
                            $count++;
                            ?>
                            <li class="question q<?php echo $v['id']; ?>" qid="<?php echo $v['id']; ?>"
                                data-qtype="<?php echo $v['qtype']; ?>">
                                <div class="questionName"><?php echo $v['name']; ?></div>
                                <div class="questionArrowDown" onclick="showDown(<?php echo $v['id']; ?>);">
                                    <span class="glyphicon glyphicon-menu-down"></span>
                                    <div class="questionAction qA_<?php echo $v['id']; ?>">
                                        <ul class="ulQuestionAction">
                                            <li class="liEditQuestion">
                                                <span><?php echo Jtext::_('QUESTION_BTN_EDIT'); ?></span>
                                            </li>
                                            <li class="liRemoveQuestion">
                                                <span><?php echo Jtext::_('QUESTION_BTN_REMOVE'); ?></span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="triangle tr_<?php echo $v['id']; ?>"></div>
                                </div>

                                <div class="clear"></div>
                            </li>
                        <?php } ?>
                        <?php } ?>
                        <input type="hidden" name="hdSelectedQuestions" class="hdSelectedQuestions"
                               value="<?php echo $listq; ?>"/>
                    </div>
                </ol>

                <div class="choiceQuestionType <?php echo ($edit) ? 'active' : ''; ?>">
                    <label for=""
                           class="choiceQuesTitle"><?php echo JText::_('COM_JOOMDLE_CHOOSE_QUESTIONS_TITLE'); ?><span
                            class="red">*</span></label>
                    <div class="buttonsQues">
                        <button class="btQues btQuestionTF" type="button"><?php echo JText::_('QUESTION_TRUE_FALSE'); ?></button>
                        <button class="btQues btQuestionMC" type="button"><?php echo JText::_('QUESTION_MULTICHOICE'); ?></button>
                        <button class="btQues btQuestionMA" type="button"><?php echo JText::_('QUESTION_MATCHING'); ?></button>
                        <button class="btQues btQuestionTR" type="button"><?php echo JText::_('QUESTION_TEXT_RESPONSE'); ?></button>
                    </div>
                </div>

                <input type="hidden" name="hdNumberofAttempts" class="hdNumberofAttempts"
                       value="<?php echo ($edit) ? $this->assessment['attempts'] : 1; ?>"/>

                <?php if ($this->assessmentType == 3) { ?>
                    <script src="<?php echo JURI::base(); ?>components/com_community/assets/release/js/picker.js"></script>
                    <script src="<?php echo JURI::base(); ?>components/com_community/assets/release/js/picker.date.js"></script>
                    <!-- <script src="<?php echo JURI::base(); ?>components/com_community/templates/jomsocial/assets/js/bootstrap-timepicker.min.js"></script> -->
                    <link rel="stylesheet" href="<?php echo JURI::base(); ?>components/com_community/assets/pickadate/themes/classic.combined.css" type="text/css"/>
                    <div class="quizOpenTest">
                        <p><?php echo Jtext::_('ASSESSMENT_DATE_OPEN'); ?></p>
                        <input type="text" placeholder="dd/mm/yyyy" readonly name="txtQuizOpenTest"
                               id="txtQuizOpenTest"/>
                    </div>
                    <div class="quizEndTest">
                        <p><?php echo Jtext::_('ASSESSMENT_DATE_END'); ?></p>
                        <input type="text" placeholder="dd/mm/yyyy" readonly name="txtQuizEndTest" id="txtQuizEndTest"/>
                    </div>
                    <div class="quizDuration">
                        <p><?php echo Jtext::_('ASSESSMENT_DATE_DURATION'); ?></p>
                        <input type="text" placeholder="00" name="txtQuizDuration" id="txtQuizDuration"
                               value="<?php echo ($edit) ? $this->assessment['timelimit'] / 60 : ''; ?>"/>
                        <span class="mins"><?php echo Jtext::_('ASSESSMENT_MINUTE'); ?></span>
                    </div>
                <?php } ?>

                <div class="clear"></div>
                <div class="buttons <?php echo ($edit) ? 'active' : ''; ?>">
                    <button class="btSave pull-right"><?php echo Jtext::_('ASSESSMENT_SAVE'); ?></button>
                    <button class="btPreview <?php echo ($edit) ? 'active' : ''; ?> pull-right" type="button"><?php echo Jtext::_('ASSESSMENT_PREVIEW'); ?></button>
                    <button class="btCancel pull-right" type="button"><?php echo Jtext::_('ASSESSMENT_CANCEL'); ?></button>
                </div>
            </form>
        </div>

        <div class="divPreview preview-assessment">
            <div class="assessmentTitle"></div>
            <label class="previewDescriptionTitle"><?php echo JText::_('COM_JOOMDLE_INSTRUCTIONS')?></label>
            <p class="previewDescription"></p>
            <?php if ($this->assessmentType == 3) : ?>
                <p class="assessmentDuration"><?php echo JText::_('COM_JOOMDLE_TIME_LEFT')?>: <span class="assessmentDurationCountdown"></span> mins</p>
            <?php endif; ?>
            <div class="previewQuestions"></div>
            <button class="previewBack hv" type="button"><?php echo JText::_('COM_JOOMDLE_EXIT_PREVIEW'); ?></button>
        </div>
        <div class="joomdle-remove hienPopup">
            <p><?php echo Jtext::_('QUESTION_ASK_REMOVE'); ?></p>
            <button class="no" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL_POPUP'); ?></button>
            <button class="yes" type="button"><?php echo JText::_('COM_JOOMDLE_REMOVE_POPUP'); ?></button>
        </div>
        <div class="joomdle-create-question">
            <div class="true-false-question">
                <form action="" class="formTF">
                    <div class="assessmentTitle"></div>
                    <div class="form-group">
                        <label for="txtTFQuestion" class="formTFQues"><?php echo Jtext::_('COM_JOOMDLE_ADD_QUESTION_TF'); ?><span class="red">*</span></label>
                        <input type="text" name="txtTFQuestion" class="txtTFQuestion" value="<?php echo (isset($this->question)) ? (($this->question['questiontext'] == '') ? $this->question['name'] : $this->question['questiontext']) : ''; ?>"/>
                    </div>
                    <div class="form-group">
                        <label class="formTFAns"><?php echo Jtext::_('COM_JOOMDLE_ADD_ANSWER_TF'); ?><span class="red">*</span></label>
                        <input type="hidden" name="hdTF" class="hdTF" value="1"/>
                    </div>
                    <div class="btTF">
                        <div class="true chosen">
                            <div class="status"></div>
                            <span><?php echo Jtext::_('QUESTION_TRUE'); ?></span>
                        </div>
                        <div class="false">
                            <div class="status"></div>
                            <span><?php echo Jtext::_('QUESTION_FALSE'); ?></span>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <label class="formTFFeed"><?php echo Jtext::_('COM_JOOMDLE_ADD_FEEDBACK_TF'); ?></label>

                    <input type="text" name="txtaTFFeedback" class="txtaTFFeedback" value=""/>
                    <div class="buttons">
                        <button class="btSave pull-right"><?php echo Jtext::_('QUESTION_SAVE'); ?></button>
                        <button class="btPreview pull-right" type="button"><?php echo Jtext::_('QUESTION_PREVIEW'); ?></button>
                        <button class="btCancel pull-right" type="button"><?php echo Jtext::_('QUESTION_CANCEL'); ?></button>
                    </div>
                </form>
            </div>

            <div class="divPreview preview-true-false">
                <div class="previewQuestions">
                    <div class="assessmentTitle"></div>
                    <p class="questionTitle"><?php echo JText::_('COM_JOOMDLE_QUESTION') . ' 1'; ?></p>
                    <p class="questionDescription"></p>
                    <p class="questionAnswer"><?php echo JText::_('COM_JOOMDLE_ANSWER'); ?></p>
                    <div class="previewTFButtons">
                        <div class="previewTrue">
                            <div class="status"></div>
                            <span><?php echo Jtext::_('QUESTION_TRUE'); ?></span>
                        </div>
                        <div class="previewFalse">
                            <div class="status"></div>
                            <span><?php echo Jtext::_('QUESTION_FALSE'); ?></span>
                        </div>
                    </div>
                </div>
                <button class="previewSubmit" type="button" data-disabled="1"><?php echo JText::_('COM_JOOMDLE_SUBMIT'); ?></button>
                <button class="previewBack hv" type="button"><?php echo JText::_('COM_JOOMDLE_EXIT_PREVIEW'); ?></button>
            </div>
            <div class="multiple-choice-question">
                <form action="" class="formMC">
                    <div class="assessmentTitle"></div>
                    <div class="form-group">
                        <label for="txtaMCQuestion" class="formTFQues"><?php echo JText::_('COM_JOOMDLE_ADD_QUESTION_MUL'); ?><span class="red">*</span></label>
                        <input type="text" name="txtaMCQuestion" class="txtaMCQuestion" value=""/>
                    </div>
                    <div class="form-group">
                        <label class="formTFAns"><?php echo Jtext::_('COM_JOOMDLE_ADD_ANSWER_MUL'); ?><span class="red">*</span></label>
                        <div class="answers">
                            <div class="answer" data-order="1">
                                <div class="status"></div>
                                <span class="order">A.</span>
                                <input type="text" class="txtAnswerContent"/>
                                <input type="text" class="txtRespondContent"
                                       placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>"/>
                            </div>
                            <div class="answer" data-order="2">
                                <div class="status"></div>
                                <span class="order">B.</span>
                                <input type="text" class="txtAnswerContent"/>
                                <input type="text" class="txtRespondContent"
                                       placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>"/>
                            </div>
                            <div class="answer" data-order="3">
                                <div class="status"></div>
                                <span class="order">C.</span>
                                <input type="text" class="txtAnswerContent"/>
                                <input type="text" class="txtRespondContent"
                                       placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>"/>
                            </div>
                            <div class="answer" data-order="4">
                                <div class="status"></div>
                                <span class="order">D.</span>
                                <input type="text" class="txtAnswerContent"/>
                                <input type="text" class="txtRespondContent"
                                       placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>"/>
                            </div>
                            <div class="answer" data-order="5">
                                <div class="status"></div>
                                <span class="order">E.</span>
                                <input type="text" class="txtAnswerContent"/>
                                <input type="text" class="txtRespondContent"
                                       placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>"/>
                            </div>
                        </div>
                    </div>

                    <p class="formTFFeed"><?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK_MUL') ?></p>
                    <div class="divMCQuestionFeedback">
                        <input type="text" name="txtaMCQuestionFeedback" class="txtaMCQuestionFeedback" value=""/>
                    </div>
                    <div class="clear"></div>
                    <div class="buttons pull-right">
                        <button class="btCancel" type="button"><?php echo Jtext::_('QUESTION_CANCEL'); ?></button>
                        <button class="btPreview" type="button"><?php echo Jtext::_('QUESTION_PREVIEW'); ?></button>
                        <button class="btSave"><?php echo Jtext::_('QUESTION_SAVE'); ?></button>
                    </div>
                </form>
            </div>
            <div class="divPreview preview-multiple-choice">
                <div class="assessmentTitle"></div>
                <div class="previewQuestions">
                    <p class="questionTitle"><?php echo JText::_('COM_JOOMDLE_QUESTION') . ' 1'; ?></p>
                    <p class="questionDescription"></p>
                    <p class="questionAnswer"><?php echo JText::_('COM_JOOMDLE_ANSWER'); ?></p>
                    <div class="previewMCOptions">
                    </div>
                </div>
                <button class="previewSubmit" type="button" data-disabled="1"><?php echo JText::_('COM_JOOMDLE_SUBMIT'); ?></button>
                <button class="previewBack hv" type="button"><?php echo JText::_('COM_JOOMDLE_EXIT_PREVIEW'); ?></button>
            </div>

            <div class="matching-question">
                <form action="" class="formMatching">
                    <div class="assessmentTitle"></div>
                    <div class="divMatchingQuestion form-group">
                        <label for="txtaMatchingQuestion" class="formTFQues"><?php echo Jtext::_('COM_JOOMDLE_ADD_QUESTION_MATCH'); ?><span class="red">*</span></label>
                        <input type="text" name="txtaMatchingQuestion" class="txtaMatchingQuestion" value=""/>
                    </div>
                    <div class="form-group">
                        <label class="formTFAns"><?php echo Jtext::_('COM_JOOMDLE_ADD_ANSWER_MATCH'); ?><span
                                class="red">*</span></label>
                        <button class="btAddQuestions" type="button">ADD</button>
                        <div class="options">
                            <div class="block" data-order="1">
                                <label class="statementTitle"></label>
                                <input type="text" class="statement"/>
                                <label class="optionTitle"></label>
                                <input type="text" class="option"/>
                            </div>
                            <div class="block" data-order="2">
                                <label class="statementTitle"></label>
                                <input type="text" class="statement"/>
                                <label class="optionTitle"></label>
                                <input type="text" class="option"/>
                            </div>
                            <div class="block" data-order="3">
                                <label class="statementTitle"></label>
                                <input type="text" class="statement"/>
                                <label class="optionTitle"></label>
                                <input type="text" class="option"/>
                            </div>
                        </div>
                    </div>
                    <p class="formTFFeed"><?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK_MATCH') ?></p>
                    <div class="divMatchQuestionFeedback">
                        <input type="text" name="txtaMatchQuestionFeedback" class="txtaMatchQuestionFeedback" value=""/>
                    </div>
                    <div class="clear"></div>
                    <div class="buttons pull-right">
                        <button class="btCancel" type="button"><?php echo Jtext::_('QUESTION_CANCEL'); ?></button>
                        <button class="btPreview" type="button"><?php echo Jtext::_('QUESTION_PREVIEW'); ?></button>
                        <button class="btSave"><?php echo Jtext::_('QUESTION_SAVE'); ?></button>
                    </div>
                </form>
            </div>

            <div class="divPreview preview-matching">
                <div class="assessmentTitle"></div>
                <div class="previewQuestions">
                    <p class="questionTitle"><?php echo JText::_('COM_JOOMDLE_QUESTION') . ' 1'; ?></p>
                    <p class="questionDescription"></p>
                    <div class="previewMatchingBlocks">
                    </div>
                </div>
                <button class="previewSubmit" type="button" data-disabled="1"><?php echo JText::_('COM_JOOMDLE_SUBMIT'); ?></button>
                <button class="previewBack hv" type="button"><?php echo JText::_('COM_JOOMDLE_EXIT_PREVIEW'); ?></button>
            </div>

            <div class="text-response-question">
                <form action="" class="formTR">
                    <div class="assessmentTitle"></div>
                    <div class="form-group">
                        <label class="formTFQues"><?php echo Jtext::_('COM_JOOMDLE_ADD_QUESTION_TEXT'); ?><span class="red">*</span></label>
                        <input type="text" name="txtaTRQuestion" class="txtaTRQuestion" value=""/>
                    </div>
                    <div class="form-group">
                        <label class="formTFAns"><?php echo Jtext::_('COM_JOOMDLE_ADD_ANSWER_TEXT'); ?><span class="red">*</span></label>
                        <textarea type="text" name="txtTRAnswer" class="txtTRAnswer" value=""></textarea>
                    </div>
                    <div class="form-group">
                        <label class="formTFFeed"><?php echo Jtext::_('COM_JOOMDLE_ADD_FEEDBACK_TEXT'); ?></label>
                        <input type="text" name="txtTRResponse" class="txtTRResponse" value=""/>
                    </div>
                    <div class="clear"></div>
                    <div class="buttons pull-right">
                        <button class="btCancel" type="button"><?php echo Jtext::_('QUESTION_CANCEL'); ?></button>
                        <button class="btPreview" type="button"><?php echo Jtext::_('QUESTION_PREVIEW'); ?></button>
                        <button class="btSave"><?php echo Jtext::_('QUESTION_SAVE'); ?></button>
                    </div>
                </form>

            </div>

            <div class="divPreview preview-text-response">
                <div class="assessmentTitle"></div>
                <div class="previewQuestions">
                    <p class="questionTitle"><?php echo JText::_('COM_JOOMDLE_QUESTION') . ' 1'; ?></p>
                    <p class="questionDescription"></p>
                    <p class="questionAnswer"><?php echo JText::_('COM_JOOMDLE_ANSWER'); ?></p>
                    <textarea disabled name="previewTextareaAnswer" class="previewTextareaAnswer" cols="30" rows="10"></textarea>
                </div>
                <button class="previewSubmit" type="button"
                        data-disabled="1"><?php echo JText::_('COM_JOOMDLE_SUBMIT'); ?></button>
                <button class="previewBack hv"
                        type="button"><?php echo JText::_('COM_JOOMDLE_EXIT_PREVIEW'); ?></button>
            </div>
        </div>
        <div class="notification"></div>
        <div class="modal" id="ass-message">
            <div class="modal-content">
                <p id="error_mes"></p>
                <div class="closetitle" style="opacity: 1;"><img src="./images/deleteIcon.png"></div>
            </div>
        </div>

        <script type="text/javascript">
            jQuery('.btCancel1').click(function () {
                window.location.href = '<?php echo JUri::base() . "course/" . $this->course_id . ".html"; ?>';
            });

            function showDown(qid) {
                jQuery('.questionAction').hide();
                jQuery('.triangle').hide();
                jQuery('.qA_' + qid).toggle();
                jQuery('.tr_' + qid).toggle();

                if (jQuery('.qA_' + qid)[0].offsetLeft + jQuery('.qA_' + qid)[0].offsetWidth > screen.width) {
                    jQuery('.qA_' + qid).css({
                        right: '15px'
                    });
                }
            }

            (function ($) {
                jQuery(document).on('click', '.liEditQuestion', function () {
                    var qid = $(this).parents('.question').attr('qid');
                    var qtype = $(this).parents('.question').attr('data-qtype');
                    var qdata = JSON.parse(questionsArray[qid]);
                    var ca;
                    console.log(qdata);
                    switch (qtype) {
                        case 'truefalse':
                            $('.true-false-question .txtTFQuestion').val(qdata.name);
//                            if (parseInt(qdata.options.answers[qdata.options.trueanswer].fraction) == 1) {
//                                var ca = 1;
//                            } else if (parseInt(qdata.options.answers[qdata.options.falseanswer].fraction) == 1) {
//                                var ca = 0;
//                            }
                            $.each(qdata.options.answers, function (key, value) {
                             if(value.answer == 'True' && value.fraction == 1)
                                 ca = 1;
                             else if(value.answer == 'False' && value.fraction == 1)
                                    ca = 0
                             });
                            
                            $('.true-false-question .hdTF').val(ca);
                            $('.true-false-question .btTF > div').removeClass('chosen');
                            if (ca == 1) $('.true-false-question .btTF .true').addClass('chosen');
                            else if (ca == 0) $('.true-false-question .btTF .false').addClass('chosen');
                            $('.true-false-question .txtaTFFeedback').val(qdata.generalfeedback);
                            $('.true-false-question').attr('qid', qid);
                            $('.joomdle-course-assessment').hide();
                            $('.joomdle-create-question .true-false-question').fadeIn();

                            $('.back-left').addClass('hidden');
                            $('.header-title').before('<div class="back-question tf-back-question"><a><img src="/images/Back-button.png"></a></div>');

                            break;
                        case 'multichoice':
                            $('.multiple-choice-question .txtaMCQuestion').val(qdata.name);
                            $('.multiple-choice-question .txtaMCQuestionFeedback').val(qdata.generalfeedback);

                            $('.multiple-choice-question .answers').html('');
                            var c = 1;
                            $.each(qdata.options.answers, function (key, value) {
                                var html;
                                if (parseFloat(value.fraction) > 0) html = '<div class="answer active" data-order="' + c + '">'; else html = '<div class="answer" data-order="' + c + '">';
                                html += '<div class="status"></div>';
                                html += '<span class="order">' + alphabet[c] + '. </span>';
                                html += '<input type="text" class="txtAnswerContent" value="' + value.answer + '" />';
                                if (value.feedback) html += '<input type="text" class="txtRespondContent" value="' + value.feedback + '"  placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>" />';
                                else html += '<input type="text" class="txtRespondContent" value=""  placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>" />';
                                html += '</div>';
                                $('.multiple-choice-question .answers').append(html);
                                c++;
                            });

                            $('.multiple-choice-question').attr('qid', qid);
                            $('.joomdle-course-assessment').hide();
                            $('.joomdle-create-question .multiple-choice-question').fadeIn();

                            $('.back-left').addClass('hidden');
                            $('.header-title').before('<div class="back-question multiplechoice-back-question"><a><img src="/images/Back-button.png"></a></div>');

                            break;
                        case 'match':
                            $('.matching-question .txtaMatchingQuestion').val(qdata.name);
                            $('.matching-question').attr('qid', qid);
                            $('.matching-question .txtaMatchQuestionFeedback').val(qdata.generalfeedback);
                            $('.matching-question .options').html('');
                            var c = 1;
                            $.each(qdata.options.subquestions, function (key, value) {
                                var html;

                                html = '<div class="block" data-order="' + c + '">' +
                                '<label class="statementTitle">Statement ' + c + '</label>' +
                                '<input type="text" class="statement" value="' + value.questiontext + '" />' +
                                '<label class="optionTitle">Option ' + c + '</label>' +
                                '<input type="text" class="option" value="' + value.answertext + '" />' +
                                '</div>';
                                $('.matching-question .options').append(html);
                                c++;
                            });

                            $('.joomdle-course-assessment').hide();
                            $('.joomdle-create-question .matching-question').fadeIn();
                            
                            $('.back-left').addClass('hidden');
                            $('.header-title').before('<div class="back-question matching-back-question"><a><img src="/images/Back-button.png"></a></div>');

                            break;
                        case 'shortanswer':
                            $('.text-response-question .txtaTRQuestion').val(qdata.name);
                            var trAnswer, trFeedback;
                            $.each(qdata.options.answers, function (key, value) {
                                if (parseFloat(value.fraction) > 0) {
                                    trAnswer = value.answer;
                                    trFeedback = value.feedback;
                                }
                            });
                            $('.text-response-question .txtTRAnswer').val(trAnswer);
                            $('.text-response-question .txtTRResponse').val(trFeedback);
                            $('.text-response-question').attr('qid', qid);
                            $('.joomdle-course-assessment').hide();
                            $('.joomdle-create-question .text-response-question').fadeIn();

                            $('.back-left').addClass('hidden');
                            $('.header-title').before('<div class="back-question text-response-back-question"><a><img src="/images/Back-button.png"></a></div>');

                            break;
                    }
                });
                var removeQid, removeQtype;
                jQuery(document).on('click', '.liRemoveQuestion', function () {
                    removeQid = $(this).parents('.question').attr('qid');
                    removeQtype = $(this).parents('.question').attr('data-qtype');
                    $('body').addClass('overlay2');
                    $('.joomdle-remove').fadeIn();
                });

                $('body').click(function (e) {
                    if (!$(e.target).hasClass('ulQuestionAction') && !$(e.target).hasClass('questionAction') && !$(e.target).hasClass('questionArrowDown') && !$(e.target).hasClass('glyphicon-menu-down')) $('.questionAction, .triangle').hide().css('right', '');
                });
                $('.joomdle-remove .no').click(function () {
                    $('body').removeClass('overlay2');
                    $('.joomdle-remove').fadeOut();
                });
                $('.joomdle-remove .yes').click(function () {
                    var modal = document.getElementById('mytitle');
                    var er = jQuery('#error_mes');
                    $('.closetitle').click(function () {
                        modal.style.display = "none";
                    });
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                    console.log(removeQid);
                    console.log(removeQtype);
                    $.ajax({
                        url: '<?php echo JUri::base() . "question/list_" . $this->course_id . ".html"; ?>',
                        type: 'POST',
                        data: {
                            qid: removeQid,
                            act: 'remove'
                        },
                        beforeSend: function () {
                            $('.joomdle-remove').fadeOut();
                            lgtCreatePopup('', {'content': 'Loading...'});
                        },
                        success: function (data, textStatus, jqXHR) {
                            lgtRemovePopup();
                            var res = JSON.parse(data);
                            $('body').removeClass('overlay2');
                            if (res.error == 1) {
                                $('.linkVideoPopup').fadeOut();
                                lgtCreatePopup('oneButton', {content: res.comment}, function () {
                                    lgtRemovePopup();
                                });
                            } else {
                                $('.listQuestions .question[qid="' + removeQid + '"]').fadeOut().addClass('hidden');
                                if ($('.listQuestions .question:not(.hidden)').length == 0) $('.listQuestions').removeClass('active');

                                var arr = $('.hdSelectedQuestions').val().split(',');
                                if (arr.indexOf(removeQid) != -1) {
                                    arr.splice(arr.indexOf(removeQid), 1);
                                }

                                $('.hdSelectedQuestions').val( arr.toString() );
                            }
                        }
                    });
                });
                var questionsArray = [];
                <?php
                if ($edit) {
                    foreach ($selectedQuestions as $key => $value) {
                        echo 'questionsArray[' . $key . '] = \'' . mysql_escape_string(json_encode($value)) . '\';';
                    }
                }
                ?>
//                console.log(questionsArray);
                var alphabet = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
                $('.joomdle-course-assessment .status').click(function () {
                    $('.joomdle-course-assessment .status').removeClass('hasError');
                    $(this).parents('.form-group').removeClass('hasError');
                    $('.joomdle-course-assessment .status').removeClass('selected');
                    $(this).addClass('selected');
                    $('.hdPurpose').val($(this).attr('p'));
                });
                $('.spanstatus').click(function () {
                    $(this).parent('.inputPurpose_des').prev('.status').click();
                });
                var modal = document.getElementById('ass-message');
                var er = jQuery('#error_mes');
                $('.closetitle').click(function () {
                    modal.style.display = "none";
                });
                window.onclick = function (event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                    }
                };
                $('.joomdle-course-assessment .btNext').click(function () {
                    var validate = true;
                    if ($('.txtTitle').val().trim() == '') {
                        $('.txtTitle').val('').parents('.form-group').addClass('hasError');
                        validate = false;
                    } else $('.txtTitle').parents('.form-group').removeClass('hasError');

                    if ($('.txtaDescription').val().trim() == '') {
                        $('.txtaDescription').val('').parents('.form-group').addClass('hasError');
                        validate = false;
                    } else $('.txtaDescription').parents('.form-group').removeClass('hasError');

                    if ($('.hdPurpose').val() == '') {
                        $('.firstPurpose .inputPurpose .status').addClass('hasError');
                        $('.firstPurpose').addClass('hasError');
                        validate = false;
                    } else{
                        $('.firstPurpose .inputPurpose .status').removeClass('hasError');
                        $('.firstPurpose').removeClass('hasError');
                    }

                    if (validate) {
                        $('.firstPurpose').addClass('hidden');
                        $('.btPreview').addClass('active');
                        $('.buttons').addClass('active');
                        $('.lastPurpose').addClass('active');
                        $('.choiceQuestionType').addClass('active');

                        if ($('.hdPurpose').val() == 2) {
                            $('.gradingScheme').addClass('active');
                            $('.lastPurpose span.assess_type').html('<?php echo JText::_('ASSESSMENT_CHECK_UNDERSTANDING');?>');
                        } else {
                            $('.lastPurpose span.assess_type').html('<?php echo JText::_('ASSESSMENT_ASSESS_READINESS'); ?>');
                        }
                    }
                });

                $('.yesnoIcon').click(function () {
                    if ($(this).hasClass('yes')) {
                        $(this).removeClass('yes').addClass('no');
                        $(this).next().val(0);
                        $('.divGradePercentage').removeClass('active');
                    } else if ($(this).hasClass('no')) {
                        $(this).removeClass('no').addClass('yes');
                        $(this).next().val(1);
                        $('.divGradePercentage').addClass('active');
                    }
                });
                $('.buttonsQues .btQues').click(function () {
                    $('.choiceQuestionType').removeClass('hasError');
                    $('.joomdle-course-assessment').hide();
                    if ($(this).hasClass('btQuestionTF')) {
                        $('.true-false-question .txtTFQuestion').val('');
                        $('.true-false-question .hdTF').val('1');
                        $('.true-false-question .btTF > div').removeClass('chosen');
                        $('.true-false-question .btTF > div.true').addClass('chosen');
                        $('.true-false-question .txtaTFFeedback').val('');
                        $('.joomdle-create-question .true-false-question').removeAttr('qid').fadeIn();
                        $('.back-left').addClass('hidden');

                        $('.header-title').before('<div class="back-question tf-back-question"><a><img src="/images/Back-button.png"></a></div>');
                    } else if ($(this).hasClass('btQuestionMC')) {
                        $('.multiple-choice-question .txtaMCQuestion').val('');
                        $('.multiple-choice-question .txtaMCQuestionFeedback').val('');

                        $('.multiple-choice-question .answers').html('');
                        for (var i = 1; i <= 5; i++) {
                            var html;
                            html = '<div class="answer" data-order="' + i + '">';
                            html += '<div class="status"></div>';
                            html += '<span class="order">' + alphabet[i] + '. </span>';
                            html += '<input type="text" class="txtAnswerContent" value="" />';
                            html += '<input type="text" class="txtRespondContent" value=""  placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>" />';
                            html += '</div>';
                            $('.multiple-choice-question .answers').append(html);
                        }
                        $('.joomdle-create-question .multiple-choice-question').removeAttr('qid').fadeIn();

                        $('.back-left').addClass('hidden');
                        $('.header-title').before('<div class="back-question multiplechoice-back-question"><a><img src="/images/Back-button.png"></a></div>');

                    } else if ($(this).hasClass('btQuestionMA')) {
                        $('.matching-question .txtaMatchingQuestion').val('');
                        $('.matching-question .options').html('');
                        $('.matching-question .txtaMatchQuestionFeedback').val('');
                        for (var i = 1; i <= 3; i++) {
                            var html;
                            html = '<div class="block" data-order="' + i + '">' +

                            '<label class="statementTitle">Statement ' + i + '</label>' +
                            '<input type="text" class="statement" value="" />' +
                            '<label class="optionTitle">Option ' + i + '</label>' +
                            '<input type="text" class="option" value="" />' +
                            '</div>';
                            $('.matching-question .options').append(html);
                        }
                        $('.joomdle-create-question .matching-question').removeAttr('qid').fadeIn();

                        $('.back-left').addClass('hidden');
                        $('.header-title').before('<div class="back-question matching-back-question"><a><img src="/images/Back-button.png"></a></div>');
                    } else if ($(this).hasClass('btQuestionTR')) {
                        $('.text-response-question .txtaTRQuestion').val('');
                        $('.text-response-question .txtTRAnswer').val('');
                        $('.text-response-question .txtTRResponse').val('');
                        $('.joomdle-create-question .text-response-question').removeAttr('qid').fadeIn();

                        $('.back-left').addClass('hidden');
                        $('.header-title').before('<div class="back-question text-response-back-question"><a><img src="/images/Back-button.png"></a></div>');
                    }
                    $(window).scrollTop(0);
                });

                $('.formQuizAR').submit(function (e) {
                    e.preventDefault();
                });

                $('.selectedCount').html(<?php echo $count;?>);

                <?php if ($this->assessmentType == 3) { ?>
                $('#txtQuizOpenTest').pickadate({
                    format: 'dd/mm/yyyy'
                    // formatSubmit: 'dd-mm-yyyy'
                });
                $('#txtQuizEndTest').pickadate({
                    format: 'dd/mm/yyyy'
                    // formatSubmit: 'dd-mm-yyyy'
                });

                var offset = new Date().getTimezoneOffset();
                <?php if ($edit)  { ?>
                var to = new Date(Date.UTC( <?php echo date('Y, m, d, H, i, s', $this->assessment['timeopen']); ?> ));
                var tostr = to.getDate() + '/' + (parseInt(to.getMonth())) + '/' + to.getFullYear();
                var tc = new Date(Date.UTC( <?php echo date('Y, m, d', $this->assessment['timeclose']); ?> ));
                var tcstr = tc.getDate() + '/' + (parseInt(tc.getMonth())) + '/' + tc.getFullYear();
                $('#txtQuizOpenTest').val(tostr);
                $('#txtQuizEndTest').val(tcstr);
                <?php } ?>
                <?php } ?>

                $('.formQuizAR .btCancel').click(function () {
                    <?php if ($edit) { ?>
                    window.location.href = '<?php echo JUri::base() . "course/" . $this->course_id . ".html"; ?>';
                    <?php } else { ?>
                    var questions_del = $('.hdSelectedQuestions').val();
                    $.ajax({
                        url: '<?php echo JUri::base() . "question/list_" . $this->course_id . ".html"; ?>',
                        type: 'POST',
                        data: {
                            qid: questions_del,
                            act: 'removelist'
                        },
                        beforeSend: function () {
                            $('.joomdle-remove').fadeOut();
                            lgtCreatePopup('', {'content': 'Loading...'});
                        },
                        success: function (data, textStatus, jqXHR) {
                            lgtRemovePopup();
                            var res = JSON.parse(data);

                            window.location.href = '<?php echo JUri::base() . "assessment/create_" . $this->course_id . "_" . $this->sectionid . ".html"; ?>';
                        }
                    });
                    <?php } ?>
                });

                $('.formQuizAR .btPreview').click(function () {
                    $('.header-title').addClass('header-preview');
                    $('.divPreview.preview-assessment').addClass('visible');
                    $('.back-left').addClass('hidden');
                    $('.header-title').before('<div class="back-question quiz-back-preview"><a><img src="/images/Back-button.png"></a></div>');
                    $('.back-question a').css('display', 'none');

                    if ($('.txtTitle').val().trim() == '') {
                        $('.divPreview.preview-assessment .assessmentTitle').html('No title');
                    } else {
                        $('.divPreview.preview-assessment .assessmentTitle').html($('.txtTitle').val());
                    }
                    if ($('.txtaDescription').val().trim() == '') {
                        $('.divPreview.preview-assessment .previewDescription').html('No description');
                    } else {
                        var des = (jQuery('.txtaDescription').val());
                        var obj = jQuery('.divPreview.preview-assessment .previewDescription').text(des);
                        obj.html(obj.html().replace(/\n/g, '<br/>'));
                    }

                    var htmlCode = '';
                    $('.listQuestions.active .question').each(function (k, v) {
                        htmlCode += '<div class="question"><p class="index">'+(k+1)+ ')&nbsp;</p> ' + $(this).html() + '</div>';
                     });

                    console.log(htmlCode);
                    $('.preview-assessment .previewQuestions').html(htmlCode);
                    $('.divPreview.visible .question .questionArrowDown').css('display', 'none');
                    $('.assessmentDurationCountdown').html($('#txtQuizDuration').val());
                    $('.joomdle-quiz-ar').hide();
                });

                $('.divPreview.preview-assessment .previewBack').click(function () {
                    $('.divPreview.preview-assessment').removeClass('visible');
                    $('.joomdle-quiz-ar').fadeIn();
                    $('.header-title').removeClass('header-preview');
                    $('.back-left').removeClass('hidden');
                    $('.back-question').remove();
                });
                $('body').on('click', '.quiz-back-preview', function () {
                    $('.divPreview.preview-assessment').removeClass('visible');
                    $('.joomdle-quiz-ar').fadeIn();
                    $('.back-left').removeClass('hidden');
                    $('.back-question').remove();
                });

                $('.formQuizAR .btSave').click(function () {
                    var validate = true;
                    if ($('.listQuestions').hasClass('active') == false) {
                        $('.choiceQuestionType').addClass('hasError');
                        validate = false;
                    } else $('.choiceQuestionType').removeClass('hasError');
                    
                    if ($('.txtTitle').val().trim() == '') {
                        $('.txtTitle').val('').parents('.form-group').addClass('hasError');
                        validate = false;
                    } else $('.txtTitle').parents('.form-group').removeClass('hasError');

                    if ($('.txtaDescription').val().trim() == '') {
                        $('.txtaDescription').val('').parents('.form-group').addClass('hasError');
                        validate = false;
                    } else $('.txtaDescription').parents('.form-group').removeClass('hasError');

                    if (!validate) return false;

                    var modal = document.getElementById('ass-message');
                    var er = jQuery('#error_mes');
                    $('.closetitle').click(function () {
                        modal.style.display = "none";
                    });

                    <?php if ($this->assessmentType == 3) { ?>
                    if ($('.txtaDescription').val().trim() == '') {
                        $('.txtaDescription').val('');
                        lgtCreatePopup('oneButton', {content: 'Empty Input Value.'}, function () {
                            lgtRemovePopup();
                        });
                    } elseif (!$.isNumeric($('#txtQuizDuration').val())) {
                        lgtCreatePopup('oneButton', {content: 'Empty Input Value.'}, function () {
                            lgtRemovePopup();
                        });

                    } else {
                        var valueTimeOpen = $('#txtQuizOpenTest').val();
                        var d1arr = valueTimeOpen.split('/');
                        var d1 = new Date(d1arr[2], d1arr[1] - 1, d1arr[0], 0, 0, 0, 0);

                        var valueTimeClose = $('#txtQuizEndTest').val();
                        var d2arr = valueTimeClose.split('/');
                        var d2 = new Date(d2arr[2], d2arr[1] - 1, d2arr[0], 0, 0, 0, 0);

                        if (d1.getTime() > d2.getTime()) {
                            lgtCreatePopup('oneButton', {content: 'Open Test Time must be later than Close Test Time.'}, function () {
                                lgtRemovePopup();
                            });
                            return;
                        }
                        <?php } ?>

                        var act = '<?php echo ($edit) ? "update" : "create"; ?>';
                        if ($('.hdPurpose').val() == 2) {
                            var gradeact = $('.hdGradingScheme').val();
                        } else {
                            var gradeact = 0;
                        }
                        // check value for grading scheme
                        var gradeA = $('.txtAssessmentGradeA').val();
                        var gradeB = $('.txtAssessmentGradeB').val();
                        var gradeC = $('.txtAssessmentGradeC').val();
                        var gradeD = $('.txtAssessmentGradeD').val();
                        if (gradeact == 1) {
                            if (!$.isNumeric(gradeA) || !$.isNumeric(gradeB) || !$.isNumeric(gradeC) || !$.isNumeric(gradeD)) {
                                lgtCreatePopup('oneButton', {content: "Percentage is not numeric."}, function () {
                                    lgtRemovePopup();
                                });

                                $('.txtaDescription').focus();
                                return;
                            } else if (gradeA < 0 || gradeA > 99 || gradeA.length > 2 || gradeB < 0 || gradeB > 99 || gradeB.length > 2 || gradeC < 0 || gradeC > 99 || gradeC.length > 2 || gradeD < 0 || gradeD > 99 || gradeD.length > 2) { //.....
                                lgtCreatePopup('oneButton', {content: "There is an error in the values entered. Please ensure values are not repeated and in descending order from A to D."}, function () {
                                    lgtRemovePopup();
                                });

                                return;
                            } else if (gradeD >= gradeC || gradeD >= gradeB || gradeD >= gradeA || gradeC >= gradeB || gradeC >= gradeA || gradeB >= gradeA) {
                                lgtCreatePopup('oneButton', {content: "There is an error in the values entered. Please ensure values are not repeated and in descending order from A to D."}, function () {
                                    lgtRemovePopup();
                                });

                                return;
                            }
                        }

                        $.ajax({
                            url: window.location.href,
                            type: 'POST',
                            data: {
                                <?php echo ($edit) ? "aid: " . $this->aid . "," : ""; ?>
                                act: act,
                                name: $('.txtTitle').val(),
                                courseid: <?php echo $this->course_id; ?>,
                                des: $('.txtaDescription').val(),
                                questions: $('.hdSelectedQuestions').val(),
                                atype: $('.hdPurpose').val(),
                                attempts: $('.hdNumberofAttempts').val(),
                                gradingscheme: gradeact,
                                gradeboundaryA: gradeA,
                                gradeboundaryB: gradeB,
                                gradeboundaryC: gradeC,
                                gradeboundaryD: gradeD,
                                <?php if ($this->assessmentType == 3) { ?>
                                timeopen: valueTimeOpen,
                                timeclose: valueTimeClose,
                                timelimit: $('#txtQuizDuration').val(),
                                timezoneOffset: offset,
                                sectionid: <?php if ($this->sectionid != Null) {
                                    echo $this->sectionid;
                                } else echo 0;?>
                                <?php } ?>
                            },
                            beforeSend: function () {
                                lgtCreatePopup('', {'content': 'Loading...'});
                            },
                            success: function (data, textStatus, jqXHR) {
                                var res = JSON.parse(data);
                                
                                if (res.error == 1) {
                                    lgtRemovePopup();
                                    lgtCreatePopup('oneButton', {content: res.comment}, function () {
                                        lgtRemovePopup();
                                    });
                                } else {
                                    window.location.href = "<?php echo JUri::base() . "course/" . $this->course_id; ?>.html";
                                }
                            }
                        });

                        <?php if ($this->assessmentType == 3) { ?>
                    }
                    <?php } ?>
                });

                $('.formQuizAR .btAddQuestions').click(function () {
                    $('.joomdle-quiz-ar').addClass('hidden');
                    $('.questions-bank').addClass('visible');
                });

                $('.questions-bank .btAddtoAR').click(function () {
                    $('.btAddQuestions').html('<?php echo Jtext::_('COM_JOOMDLE_EDIT_QUESTIONS_LIST'); ?>');
                    $('.questions-bank').removeClass('visible');
                    $('.joomdle-quiz-ar').removeClass('hidden');
                });

                $('.questions-bank .question').click(function () {
                    var oldv = $('.hdSelectedQuestions').val();
                    var count = parseInt($('.selectedCount').html());
                    if (!$(this).hasClass('active')) {
                        $('.selectedCount').html(count + 1);
                        var arr = oldv.split(',');
                        arr.push($(this).attr('qid'));
                        $('.hdSelectedQuestions').val(arr.toString());
                    } else {
                        $('.selectedCount').html(count - 1);
                        var arr = oldv.split(',');
                        var index = arr.indexOf($(this).attr('qid'));
                        if (index > -1) {
                            arr.splice(index, 1);
                        }
                        $('.hdSelectedQuestions').val(arr.toString());
                    }
                    $(this).toggleClass('active');

                });
                $('.optionAttempt').click(function () {
                    $('.optionAttempt').removeClass('active');
                    $(this).addClass('active');
                    $('.hdNumberofAttempts').val($(this).attr('aid'));
                });
                $('.optionGradingMethod').click(function () {
                    $('.optionGradingMethod').removeClass('active');
                    $(this).addClass('active');
                    $('.hdGradingMethod').val($(this).attr('gmid'));
                });

                $('.formTF').submit(function (e) {
                    e.preventDefault();
                });
                $('.formTF .btCancel').click(function () {
                    $('.formTF .form-group').removeClass('hasError');
                    $('.joomdle-create-question .true-false-question').hide();
                    $('.joomdle-course-assessment').fadeIn();
                    $('.back-left').removeClass('hidden');
                    $('.back-question').remove();
                });
                $(document).on('click', '.tf-back-question', function () {
                    $('.formTF .form-group').removeClass('hasError');
                    $('.joomdle-create-question .true-false-question').hide();
                    $('.joomdle-course-assessment').fadeIn();
                    $('.back-left').removeClass('hidden');
                    $('.back-question').remove();
                });

                $('.formTF .btTF .true').click(function () {
                    $('.formTF .btTF > div').removeClass('chosen');
                    $(this).addClass('chosen');
                    $('.hdTF').val('1');
                });
                $('.formTF .btTF .false').click(function () {
                    $('.formTF .btTF > div').removeClass('chosen');
                    $(this).addClass('chosen');
                    $('.hdTF').val('0');
                });

                // Preview question from add question
                $('.true-false-question .btPreview').click(function () {
                    $('.divPreview.preview-true-false').addClass('visible');
                    $('.back-question').removeClass('tf-back-question').addClass('tf-back-preview');
                    $('.back-question a').css('display', 'none');

                    if ($('.txtTFQuestion').val().trim() == '') {
                        $('.divPreview.preview-true-false .questionDescription').html('Empty Question Text');
                    } else {
                        $('.divPreview.preview-true-false .questionDescription').html($('.txtTFQuestion').val());
                    }
                    $('.true-false-question').hide();

                    if ($('.txtTitle').val().trim() == '') {
                        $('.divPreview .assessmentTitle').html('No title');
                    } else {
                        $('.divPreview .assessmentTitle').html($('.txtTitle').val());
                    }
                });

                $('.preview-true-false .previewBack').click(function () {
                    $('.divPreview.preview-true-false').removeClass('visible');
                    $('.true-false-question').fadeIn();
                    $('.back-question').addClass('tf-back-question').removeClass('tf-back-preview');
                    $('.back-question a').css('display', 'block');
                });
                $(document).on('click', '.tf-back-preview', function () {
                    $('.divPreview.preview-true-false').removeClass('visible');
                    $('.true-false-question').fadeIn();
                    $('.back-question').addClass('tf-back-question').removeClass('tf-back-preview');
                    $('.back-question a').css('display', 'block');
                 });

                $('.formTF .btSave').click(function () {
                    var modal = document.getElementById('ass-message');
                    var er = jQuery('#error_mes');
                    $('.back-left').removeClass('hidden');
                    $('.back-question').remove();

                    $('.closetitle').click(function () {
                        modal.style.display = "none";
                    });
                    
                    if ($('.txtTFQuestion').val().trim() == '') {
                        $('.txtTFQuestion').val('').parents('.form-group').addClass('hasError');
                        return;
                    } else $('.txtTFQuestion').parents('.form-group').removeClass('hasError');

                    if (parseInt($('.hdTF').val()) != 1 && parseInt($('.hdTF').val()) != 0) {
                        lgtCreatePopup('oneButton', {content: "Please select right answer."}, function () {
                            lgtRemovePopup();
                        });
                        return;
                    }
                    if (typeof($('.true-false-question').attr('qid')) == 'undefined') {
                        var act = 'create';
                    } else var act = 'update';
                    var sendData = {
                        act: act,
                        name: $('.txtTFQuestion').val(),
                        courseid: <?php echo $this->course_id; ?>,
                        generalfeedback: $('.txtaTFFeedback').val(),
                        feedbacktrue: '',
                        feedbackfalse: '',
                        correctanswer: $('.hdTF').val()
                    };
                    if (act == 'update') {
                        sendData.qid = $('.true-false-question').attr('qid');
                        var url = "<?php echo JUri::base() . "question/edit_" . $this->course_id; ?>_truefalse_" + sendData.qid + ".html";
                    } else {
                        var url = "<?php echo JUri::base() . "question/create_" . $this->course_id; ?>_1.html";
                    }
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: sendData,
                        beforeSend: function () {
                            lgtCreatePopup('', {'content': 'Loading...'});
                        },
                        success: function (data, textStatus, jqXHR) {
                            lgtRemovePopup();
                            var res = JSON.parse(data);

                            if (res.error == 1) {
                                switch (res.comment) {
                                    case 'nopermission':
                                        lgtCreatePopup('oneButton', {content: "You don\'t have permission to create question."}, function () {
                                            lgtRemovePopup();
                                        });
                                        break;
                                    default:
                                        lgtCreatePopup('oneButton', {content: res.comment}, function () {
                                            lgtRemovePopup();
                                        });
                                        break;
                                }
                            } else {
                                console.log(res);
                                addQuestion(res.data, act);
                                $('.joomdle-create-question .true-false-question').hide();
                                $('.joomdle-course-assessment').fadeIn();
                                if ($('.choiceQuesTitle').hasClass("error1")) {
                                    $('.choiceQuesTitle').removeClass("error1");
                                }
                            }
                        }
                    });

                });

                $('.formMC').submit(function (e) {
                    e.preventDefault();
                });

                // Question multiple choice page to course assessment page
                $('.formMC .btCancel').click(function () {
                    $('.formMC .form-group').removeClass('hasError');
                    $('.joomdle-create-question .multiple-choice-question').hide();
                    $('.joomdle-course-assessment').fadeIn();
                    $('.back-left').removeClass('hidden');
                    $('.back-question').remove();
                });
                $(document).on('click', '.multiplechoice-back-question', function () {
                    $('.formMC .form-group').removeClass('hasError');
                    $('.joomdle-create-question .multiple-choice-question').hide();
                    $('.joomdle-course-assessment').fadeIn();
                    $('.back-left').removeClass('hidden');
                    $('.back-question').remove();
                });

                $('.formMC .btAddQuestions').click(function () {
                    var ord = $('.answers > .answer:last-of-type').attr('data-order');
                    if (ord > 9) {
                        var oldurl = $("#iframe-question").attr('src');
                        $("#iframe-question").attr('src', oldurl + '&qnum=' + (parseInt(ord) + 1));
                    }
                    $('.formMC .answers').append('<div class="answer" data-order="' + (parseInt(ord) + 1) + '">' +
                    '<div class="status"></div>' +
                    '<span class="order">' + alphabet[parseInt(ord) + 1] + '.</span>' +
                    '<input type="text" class="txtAnswerContent" />' +
                    '</div>');
                    $('.formMC .responds').append('<div class="respond" data-order="' + (parseInt(ord) + 1) + '">' +
                    '<span class="order">' + alphabet[parseInt(ord) + 1] + '.</span>' +
                    '<div class="divRespondContent">' +
                    '<input type="text" class="txtRespondContent"/>' +
                    '<div class="status"></div>' +
                    '<span>Check if correct answer</span>' +
                    '</div>' +
                    '</div>');
                });
                $(document).on('click', '.answer .status', function () {
                    $(this).parent('.answer').toggleClass('active');
                    var ord = $(this).parent('.answer').attr('data-order');
                    $(this).parent('.answer').find('.txtAnswerContent').removeClass('hasError1');
                    validateMCForm();
                });
                $(document).on('click', '.respond .status', function () {
                    $(this).parent().parent('.respond').toggleClass('active');
                });
                $(document).on('click', '.respond .divRespondContent > span', function () {
                    $(this).prev('.status').click();
                });

                $('.multiple-choice-question .btPreview').click(function () {
                    $('.divPreview.preview-multiple-choice').addClass('visible');
                    $('.back-question').removeClass('multiplechoice-back-question').addClass('multiplechoice-back-preview');
                    $('.back-question a').css('display', 'none');

                    if ($('.txtaMCQuestion').val().trim() == '') {
                        $('.divPreview.preview-multiple-choice .questionDescription').html('Empty Question Text.');
                    } else {
                        $('.divPreview.preview-multiple-choice .questionDescription').html($('.txtaMCQuestion').val());
                    }
                    $('.previewMCOptions').html('');
                    var x = 1;
                    $('.answers > .answer').each(function () {
                        if ($(this).find('.txtAnswerContent').val().trim() != '') {
                            var t = '';
                            $('.previewMCOptions').append(
                                '<div class="previewAnswer">' +
                                '<div class="previewStatus' + t + '"></div>' +
                                '<span class="previewOrder">' + alphabet[x] + '. </span>' +
                                '<span class="previewAnswerText"> ' + $(this).find('.txtAnswerContent').val() + '</span>' +
                                '</div>');
                            x++;
                        }
                    });
                    $('.multiple-choice-question').hide();

                    if ($('.txtTitle').val().trim() == '') {
                        $('.divPreview .assessmentTitle').html('No title');
                    } else {
                        $('.divPreview .assessmentTitle').html($('.txtTitle').val());
                    }
                });

                // Preview page to create question multiple choice page
                $('.preview-multiple-choice .previewBack').click(function () {
                    $('.divPreview.preview-multiple-choice').removeClass('visible');
                    $('.multiple-choice-question').fadeIn();
                 $('.back-question').addClass('multiplechoice-back-question').removeClass('multiplechoice-back-preview');
                 $('.back-question a').css('display', 'block');
                });
                $(document).on('click', '.back-question.multiplechoice-back-preview', function () {
                    $('.divPreview.preview-multiple-choice').removeClass('visible');
                    $('.multiple-choice-question').fadeIn();
                    $('.back-question').addClass('multiplechoice-back-question').removeClass('multiplechoice-back-preview');
                    $('.back-question a').css('display', 'block');
                 });

                $(document).on('keyup', '.formMC .txtAnswerContent', function() {
                    $('.txtAnswerContent').removeClass('hasError1');
                    validateMCForm();
                });
//                $(document).on('keyup', '.txtaMCQuestion', function() {
//                    validateMCForm();
//                });

                $('.formMC .btSave').click(function () {
                    var validate = true;
                    $('.back-left').removeClass('hidden');
                    $('.back-question').remove();
                    var answers = [];
                    var feedbacks = [];
                    var fraction = [];
                    var norightanswer = 0;
                    var countAnswers = 0;
                    var j =0;
                    if ($('.txtaMCQuestion').val().trim() == '') {
                        $('.txtaMCQuestion').val('').parents('.form-group').addClass('hasError');
                     } else $('.txtaMCQuestion').parents('.form-group').removeClass('hasError');
                    $('.txtAnswerContent').each(function () {
                        var value = $(this).val().trim();
                        if (value != '') {
                            emptyAnswerContent = false;
                            countAnswers++;

                            if ($(this).parent().hasClass('active')) {
                                fraction.push(1);
                                norightanswer++;
                            } else fraction.push(0);

                            if ($.inArray($(this).val(), answers) != -1) doubleAnswer = true;
                            answers.push($(this).val());
                        }
                    });

                    if (!validateMCForm(true)) return false;

                    $('.txtRespondContent').each(function () {
                        feedbacks.push($(this).val());
                    });
                    if (typeof($('.multiple-choice-question').attr('qid')) == 'undefined') {
                        var act = 'create';
                    } else var act = 'update';

                    var sendData = {
                        act: act,
                        name: $('.txtaMCQuestion').val(),
                        courseid: <?php echo $this->course_id; ?>,
                        noanswers: $('.options .block').length,
                        answers: answers,
                        feedbacks: feedbacks,
                        fraction: fraction,
                        norightanswer: norightanswer,
                        generalfeedback: $('.txtaMCQuestionFeedback').val()
                    }

                    if (act == 'update') {
                        sendData.qid = $('.multiple-choice-question').attr('qid');
                        var url = "<?php echo JUri::base() . "question/edit_" . $this->course_id; ?>_multichoice_" + sendData.qid + ".html";
                    } else {
                        var url = "<?php echo JUri::base() . "question/create_" . $this->course_id; ?>_2.html";
                    }

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: sendData,
                        beforeSend: function () {
                            lgtCreatePopup('', {'content': 'Loading...'});
                        },
                        success: function (data, textStatus, jqXHR) {
                            lgtRemovePopup();
                            var res = JSON.parse(data);
                            console.log(res);

                            if (res.error == 1) {
                                switch (res.comment) {
                                    case 'nopermission':
                                        lgtCreatePopup('oneButton', {content: "You don\'t have permission to create question."}, function () {
                                            lgtRemovePopup();
                                        });
                                        //$('.lgt-notif-error').html('You don\'t have permission to create question.').addClass('lgt-visible');
                                        break;
                                    default:
                                        lgtCreatePopup('oneButton', {content: res.comment}, function () {
                                            lgtRemovePopup();
                                        });
                                        // $('.lgt-notif-error').html(res.comment).addClass('lgt-visible');
                                        break;
                                }
                            } else {
                                addQuestion(res.data, act);
                                $('.joomdle-create-question .multiple-choice-question').hide();
                                $('.joomdle-course-assessment').fadeIn();
                                if ($('.choiceQuesTitle').hasClass("error1")) {
                                    $('.choiceQuesTitle').removeClass("error1");
                                }
                            }
                        }
                    });

                });

                function validateMCForm(checknumberoption = false) {
                    var validate = true;
                    if ($('.txtaMCQuestion').val().trim() == '') {
//                        $('.txtaMCQuestion').parents('.form-group').addClass('hasError');
                        validate = false;
                    } else $('.txtaMCQuestion').parents('.form-group').removeClass('hasError');
                    var emptyAnswerContent = true;
                    var doubleAnswer = false;
                    var answers = [];
                    var countAnswers = 0;
                    $('.txtAnswerContent').each(function () {
                        var value = $(this).val().trim();
                        if (value) {
                            emptyAnswerContent = false;
                            countAnswers++;
                            if ($.inArray(value, answers) != -1) {
                                doubleAnswer = true;
                                $('.txtAnswerContent').each(function () {
                                    if ($(this).val().trim() == value) {
                                        $(this).addClass('hasError1');
                                    }
                                });
                            }
                            answers.push(value);
                        }
                    });

                    if (countAnswers <= 2 && checknumberoption == true) {
                        $('.txtAnswerContent').each(function () {
                            if ($(this).val().trim() == '') {
                                $(this).val('').addClass('hasError1');
                            }
                        });
                        validate = false;
                    }

                    if ($('.multiple-choice-question .answer.active').length < 1 && checknumberoption == true) {
                        $('.multiple-choice-question .answer .status').each(function () {
                            $(this).addClass("error1");
                            $(this).click(function () {
                                $('.multiple-choice-question .answer .status').removeClass("error1");
                            });
                        });
                        validate = false;
                    } else {
                        $('.multiple-choice-question .answer.active').each(function() {
                            if ($(this).find('.txtAnswerContent').val().trim() == '') {
                                validate = false;
                                $(this).find('.txtAnswerContent').val('').addClass('error');
                            }
                        });
                    }
                    if (doubleAnswer) {
                        validate = false;
                    }
                    if (emptyAnswerContent && checknumberoption == true) {
                        $('.txtAnswerContent').addClass('hasError1');
                        validate = false;
                    }

                    return validate;
                }

                $('.formMatching').submit(function (e) {
                    e.preventDefault();
                });

                // Create question page to course assessment page
                $('.formMatching .btCancel').click(function () {
                    $('.formMatching .form-group').removeClass('hasError');
                    $('.joomdle-create-question .matching-question').hide();
                    $('.joomdle-course-assessment').fadeIn();
                    $('.back-left').removeClass('hidden');
                    $('.back-question').remove();
                });
                $(document).on('click', '.back-question.matching-back-question', function() {
                    $('.formMatching .form-group').removeClass('hasError');
                    $('.joomdle-create-question .matching-question').hide();
                    $('.joomdle-course-assessment').fadeIn();
                    $('.back-left').removeClass('hidden');
                    $('.back-question').remove();
                })

                $('.formMatching .btAddQuestions').click(function () {
                    var ord = $('.options > .block:last-of-type').attr('data-order');

                    $('.formMatching .options').append('<div class="block" data-order="' + (parseInt(ord) + 1) + '">' +
                    '<label class="statementTitle">Statement ' + (parseInt(ord) + 1) + '</label>' +
                    '<input type="text" class="statement"/>' +
                    '<label class="optionTitle">Option ' + (parseInt(ord) + 1) + '</label>' +
                    '<input type="text" class="option"/>' +
                    '</div>');

                    $('.ulSetStatement').append('<li data-order="' + (parseInt(ord) + 1) + '">Option ' + (parseInt(ord) + 1) + '</li>');
                });
                $('.matching-question .btPreview').click(function () {
                    $('.divPreview.preview-matching').addClass('visible');
                    $('.back-question').addClass('matching-back-preview').removeClass('matching-back-question');
                    $('.back-question a').css('display', 'none');

                    if ($('.txtaMatchingQuestion').val().trim() == '') {
                        $('.divPreview.preview-matching .questionDescription').html('Empty Question Text');
                    } else {
                        $('.divPreview.preview-matching .questionDescription').html($('.txtaMatchingQuestion').val());
                    }
                    var k = 1;
                    var previewOptionSelectHTML = '<div class="divPreviewOptionSelect"><input type="text" readonly class="previewOptionInput" placeholder="Option"/>' +
                        '<ul class="ulPreviewOptionSelect">';
                    $('.block .option').each(function () {
                        if ($(this).prev('.statement').val() != '') {
                            previewOptionSelectHTML += '<li class="previewOption">' + $(this).val() + '</li>';
                        }
                    });
                    previewOptionSelectHTML += '</ul>';
                    previewOptionSelectHTML += '</div>';

                    $('.previewMatchingBlocks').html('');
                    $('.formMatching .block').each(function () {
                        if ($(this).find('.statement').val() != '') {
                            $('.previewMatchingBlocks').append('' +
                            '<div class="previewBlock">' +
                            '<p class="previewStatementText">' + $(this).find('.statement').val() + '</p>' +
                            '<p class="previewAnswerText">' +
                            '<span class="">Answer </span>' + previewOptionSelectHTML +
                            '</p>' +
                            '</div>');
                            k++;
                        }
                    });
                    $('.matching-question').hide();
                    if ($('.txtTitle').val().trim() == '') {
                        $('.divPreview .assessmentTitle').html('No title');
                    } else {
                        $('.divPreview .assessmentTitle').html($('.txtTitle').val());
                    }
                });

                // Preview page to create question page
                $('.preview-matching .previewBack').click(function () {
                    $('.divPreview.preview-matching').removeClass('visible');
                    $('.matching-question').fadeIn();
                    $('.back-question').removeClass('matching-back-preview').addClass('matching-back-question');
                    $('.back-question a').css('display', 'block');
                });
                 $(document).on('click','.back-question.matching-back-preview', function() {
                    $('.divPreview.preview-matching').removeClass('visible');
                    $('.matching-question').fadeIn();
                    $('.back-question').removeClass('matching-back-preview').addClass('matching-back-question');
                    $('.back-question a').css('display', 'block');
                });

                $(document).on('click', '.previewOptionInput', function () {
                    $(this).toggleClass('clicked');
                    $(this).next('.ulPreviewOptionSelect').toggle();
                });
                $(document).on('click', '.ulPreviewOptionSelect .previewOption', function () {
                    $(this).parent().prev('.previewOptionInput').removeClass('clicked').val($(this).html());
                    $(this).parent().hide();
                });

                $(document).on('click', '.option .left', function () {
                    $('.option .active').removeClass('active');
                    $(this).addClass('active');
                    $('.matching-question .setStatement, .matching-question .setOption').hide();
                    $('.matching-question .setStatement').show();
                    $('.matching-question .setStatement > p').html('Statement ' + $(this).find('.order').html());
                    $('.txtSetStatement').attr('data-order', $(this).parent('.option').attr('data-order')).val($(this).attr('data-val'));
                    $('.hdSelectOption').val($(this).attr('data-opt'));
                    if (typeof($(this).attr('data-opt')) != 'undefined') $('.txtSelectOption').val('Option ' + $(this).attr('data-opt')); else $('.txtSelectOption').val('');
                });
                $(document).on('click', '.option .right', function () {
                    $('.option .active').removeClass('active');
                    $(this).addClass('active');
                    $('.matching-question .setStatement, .matching-question .setOption').hide();
                    $('.matching-question .setOption').show();
                    $('.matching-question .setOption > p').html('Option ' + $(this).find('.order').html());
                    $('.txtSetOption').attr('data-order', $(this).parent('.option').attr('data-order')).val($(this).attr('data-val'));
                });
                $(document).on('focus', '.divSetStatement, .divSetStatement .glyphicon-menu-down', function () {
                    $('.ulSetStatement').show();
                });
                $(document).on('click', '.ulSetStatement > li', function () {
                    $('.txtSelectOption').val($(this).html());
                    $('.ulSetStatement').hide();
                    $('.hdSelectOption').val($(this).attr('data-order'));
                    if ($('.txtSetStatement').val().trim() != '') {
                        $('.option[data-order="' + $('.txtSetStatement').attr('data-order') + '"] .left').addClass('ok').attr('data-val', $('.txtSetStatement').val()).attr('data-opt', $('.hdSelectOption').val());
                    }
                });

                $('.txtSetOption').blur(function () {
                    if ($(this).val().trim() != '') {
                        $('.option[data-order="' + $(this).attr('data-order') + '"] .right').addClass('ok').attr('data-val', $(this).val());
                    } else {
                        $('.option[data-order="' + $(this).attr('data-order') + '"] .right').removeClass('ok').attr('data-val', '');
                    }
                });
                $('.txtSetStatement').blur(function () {
                    if ($('.hdSelectOption').val() != '') {
                        if ($(this).val().trim() != '') {
                            $('.option[data-order="' + $(this).attr('data-order') + '"] .left').addClass('ok').attr('data-val', $(this).val()).attr('data-opt', $('.hdSelectOption').val());
                        } else {
                            $('.option[data-order="' + $(this).attr('data-order') + '"] .left').removeClass('ok').attr('data-val', '').attr('data-opt', '');
                        }
                    }
                });

                $(document).on('keyup', '.formMatching .statement', function() {
                    $('.statement').removeClass('error');
                    $('.option').removeClass('error');
                    $('.txtaMatchingQuestion').parents('.form-group').removeClass('hasError');
                    validateMatching($('.options .statement'));
                    validateMatching($('.options .option'));
                });
                $(document).on('keyup', '.formMatching .option', function() {
                    $('.option').removeClass('error');
                     $('.statement').removeClass('error');
                     $('.txtaMatchingQuestion').parents('.form-group').removeClass('hasError');
                    validateMatching($('.options .option'));
                    validateMatching($('.options .statement'));
                });
                $(document).on('keyup', '.txtaMatchingQuestion', function() {
                    $('.statement').removeClass('error');
                     $('.option').removeClass('error');
                    validateMatching($('.options .statement'));
                    validateMatching($('.options .option'));
                });
                function validateMatching(a){
                    var invalidA = true;
                    var doubleAnswer = false;
//                    if ($('.txtaMatchingQuestion').val() == '') {
//                        $('.txtaMatchingQuestion').parents('.form-group').addClass('hasError');
//                        invalidA = false;
//                    } else $('.txtaMatchingQuestion').parents('.form-group').removeClass('hasError');

//                    check value 3 element top
//                    $('.options .statement:lt(3)').each(function () {
//                        if ($(this).val() == '') {
//                            $(this).addClass('error');
//                            invalidA = false;
//                        }
//                    });
//                    $('.options .option:lt(3)').each(function () {
//                        if ($(this).val() == '') {
//                            $(this).addClass('error');
//                            invalidA = false;
//                        }
//                    });
                    //check value duplicate
                    var answers = [];
                    a.each(function () {
                        var value = $(this).val().trim();
                        if (value) {
                            if ($.inArray(value, answers) != -1) {
                                doubleAnswer = true;
                                a.each(function () {
                                    if ($(this).val().trim() == value) {
                                        $(this).addClass('error');
                                    }
                                });
                                invalidA = false;
                            }
                            answers.push(value);
                        }
                    });
                    if (doubleAnswer) {
                        invalidA = false;
                    }
                    return invalidA;
                }

                $('.formMatching .btSave').click(function () {
                    var validate = true;
                    $('.back-left').removeClass('hidden');
                    $('.back-question').remove();
                    var modal = document.getElementById('ass-message');
                    var er = jQuery('#error_mes');
                    $('.closetitle').click(function () {
                        modal.style.display = "none";
                    });
                    var subquestions = [];
                    var subanswers = [];

                    $('.block').each(function () {
                        var ord = $(this).attr('data-order');
                        subquestions.push($(this).find('.statement').val());
                        subanswers.push($(this).find('.option').val());
                    });

                    if (!validateMatching($('.options .option')) || !validateMatching($('.options .statement')) ){
                        
                    $('.options .statement:lt(3)').each(function () {
                        if ($(this).val().trim() == '') {
                            $(this).val('').addClass('error');
                             validate = false;
                        }
                        });
                        $('.options .option:lt(3)').each(function () {
                            if ($(this).val().trim() == '') {
                                $(this).val('').addClass('error');
                                 validate = false;
                            }
                        });

                         if ($('.txtaMatchingQuestion').val().trim() == '') {
                            $('.txtaMatchingQuestion').val('').parents('.form-group').addClass('hasError');
                            validate = false;
                        }
//                         if(!validate) return false;
                        return false;
                    } 
                    $('.options .statement:lt(3)').each(function () {
                        if ($(this).val().trim() == '') {
                            $(this).val('').addClass('error');
                             validate = false;
                        }
                        else $(this).removeClass('error');
                    });
                    $('.options .option:lt(3)').each(function () {
                        if ($(this).val().trim() == '') {
                            $(this).val('').addClass('error');
                             validate = false;
                        }
                        else $(this).removeClass('error');
                    });
                    
                     if ($('.txtaMatchingQuestion').val().trim() == '') {
                        $('.txtaMatchingQuestion').val('').parents('.form-group').addClass('hasError');
                        validate = false;
                    } else $('.txtaMatchingQuestion').parents('.form-group').removeClass('hasError');
                    
                    if(!validate) return false;
                    
                    if (typeof($('.matching-question').attr('qid')) == 'undefined') {
                        var act = 'create';
                    } else var act = 'update';
                    var sendData = {
                        act: act,
                        name: $('.txtaMatchingQuestion').val(),
                        courseid: <?php echo $this->course_id; ?>,
                        noanswers: $('.options .block').length,
                        subquestions: subquestions,
                        subanswers: subanswers,
                        generalfeedback: $('.txtaMatchQuestionFeedback').val()
                    }
                    if (act == 'update') {
                        sendData.qid = $('.matching-question').attr('qid');
                        var url = "<?php echo JUri::base() . "question/edit_" . $this->course_id; ?>_match_" + sendData.qid + ".html";
                    } else {
                        var url = "<?php echo JUri::base() . "question/create_" . $this->course_id; ?>_3.html";
                    }

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: sendData,
                        beforeSend: function () {
                            lgtCreatePopup('', {'content': 'Loading...'});
                        },
                        success: function (data, textStatus, jqXHR) {
                            lgtRemovePopup();
                            var res = JSON.parse(data);

                            if (res.error == 1) {
                                switch (res.comment) {
                                    case 'nopermission':
                                        lgtCreatePopup('oneButton', {content: 'You don\'t have permission to create question.'}, function () {
                                            lgtRemovePopup();
                                        });
                                        break;
                                    default:
                                        lgtCreatePopup('oneButton', {content: res.comment}, function () {
                                            lgtRemovePopup();
                                        });
                                        break;
                                }
                            } else {
                                console.log(res);
                                addQuestion(res.data, act);
                                $('.joomdle-create-question .matching-question').hide();
                                $('.joomdle-course-assessment').fadeIn();
                                if ($('.choiceQuesTitle').hasClass("error1")) {
                                    $('.choiceQuesTitle').removeClass("error1");
                                }
                            }
                        }
                    });

                });
                
                $('.formMatching').on('keyup', 'input, textarea', function(e) {
                    if (e.which == 13) {
                       var validate = true;
                        $('.back-left').removeClass('hidden');
                        $('.back-question').remove();
                        var modal = document.getElementById('ass-message');
                        var er = jQuery('#error_mes');
                        $('.closetitle').click(function () {
                            modal.style.display = "none";
                        });
                        var subquestions = [];
                        var subanswers = [];

                        $('.block').each(function () {
                            var ord = $(this).attr('data-order');
                            subquestions.push($(this).find('.statement').val());
                            subanswers.push($(this).find('.option').val());
                        });

                        if (!validateMatching($('.options .option')) || !validateMatching($('.options .statement')) ){

                        $('.options .statement:lt(3)').each(function () {
                            if ($(this).val().trim() == '') {
                                $(this).val('').addClass('error');
                                 validate = false;
                            }
                            });
                            $('.options .option:lt(3)').each(function () {
                                if ($(this).val().trim() == '') {
                                    $(this).val('').addClass('error');
                                     validate = false;
                                }
                            });

                             if ($('.txtaMatchingQuestion').val().trim() == '') {
                                $('.txtaMatchingQuestion').parents('.form-group').addClass('hasError');
                                validate = false;
                            }
    //                         if(!validate) return false;
                            return false;
                        } 
                        $('.options .statement:lt(3)').each(function () {
                            if ($(this).val().trim() == '') {
                                $(this).val('').addClass('error');
                                 validate = false;
                            }
                            else $(this).removeClass('error');
                        });
                        $('.options .option:lt(3)').each(function () {
                            if ($(this).val().trim() == '') {
                                $(this).val('').addClass('error');
                                 validate = false;
                            }
                            else $(this).removeClass('error');
                        });

                         if ($('.txtaMatchingQuestion').val().trim() == '') {
                            $('.txtaMatchingQuestion').parents('.form-group').addClass('hasError');
                            validate = false;
                        } else $('.txtaMatchingQuestion').parents('.form-group').removeClass('hasError');

                        if(!validate) return false;

                        if (typeof($('.matching-question').attr('qid')) == 'undefined') {
                            var act = 'create';
                        } else var act = 'update';
                        var sendData = {
                            act: act,
                            name: $('.txtaMatchingQuestion').val(),
                            courseid: <?php echo $this->course_id; ?>,
                            noanswers: $('.options .block').length,
                            subquestions: subquestions,
                            subanswers: subanswers,
                            generalfeedback: $('.txtaMatchQuestionFeedback').val()
                        }
                        if (act == 'update') {
                            sendData.qid = $('.matching-question').attr('qid');
                            var url = "<?php echo JUri::base() . "question/edit_" . $this->course_id; ?>_match_" + sendData.qid + ".html";
                        } else {
                            var url = "<?php echo JUri::base() . "question/create_" . $this->course_id; ?>_3.html";
                        }

                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: sendData,
                            beforeSend: function () {
                                lgtCreatePopup('', {'content': 'Loading...'});
                            },
                            success: function (data, textStatus, jqXHR) {
                                lgtRemovePopup();
                                var res = JSON.parse(data);

                                if (res.error == 1) {
                                    switch (res.comment) {
                                        case 'nopermission':
                                            lgtCreatePopup('oneButton', {content: 'You don\'t have permission to create question.'}, function () {
                                                lgtRemovePopup();
                                            });
                                            break;
                                        default:
                                            lgtCreatePopup('oneButton', {content: res.comment}, function () {
                                                lgtRemovePopup();
                                            });
                                            break;
                                    }
                                } else {
                                    console.log(res);
                                    addQuestion(res.data, act);
                                    $('.joomdle-create-question .matching-question').hide();
                                    $('.joomdle-course-assessment').fadeIn();
                                    if ($('.choiceQuesTitle').hasClass("error1")) {
                                        $('.choiceQuesTitle').removeClass("error1");
                                    }
                                }
                            }
                        });
                    }
                });
                
                $('.formTR').submit(function (e) {
                    e.preventDefault();
                });
                $('.formTR .btCancel').click(function () {
                    $('.formTR .form-group').removeClass('hasError');
                    $('.joomdle-create-question .text-response-question').hide();
                    $('.joomdle-course-assessment').fadeIn();
                    $('.back-left').removeClass('hidden');
                    $('.back-question').remove();
                });
                $(document).on('click', '.text-response-back-question', function () {
                    $('.formTR .form-group').removeClass('hasError');
                    $('.joomdle-create-question .text-response-question').hide();
                    $('.joomdle-course-assessment').fadeIn();
                    $('.back-left').removeClass('hidden');
                    $('.back-question').remove();
                });

                $('.text-response-question .btPreview').click(function () {
                    $('.divPreview.preview-text-response').addClass('visible');
                    $('.back-question').removeClass('text-response-back-question').addClass('text-response-back-preview');
                    $('.back-question a').css('display', 'none');

                    if ($('.txtaTRQuestion').val().trim() == '') {
                        $('.divPreview.preview-text-response .questionDescription').html('Empty Question Text');
                    } else {
                        $('.divPreview.preview-text-response .questionDescription').html($('.txtaTRQuestion').val());
                    }

                    $('.text-response-question').hide();
                    if ($('.txtTitle').val().trim() == '') {
                        $('.divPreview .assessmentTitle').html('No title');
                    } else {
                        $('.divPreview .assessmentTitle').html($('.txtTitle').val());
                    }
                });

                $('.preview-text-response .previewBack').click(function () {
                    $('.divPreview.preview-text-response').removeClass('visible');
                    $('.text-response-question').fadeIn();
                    $('.back-question').addClass('text-response-back-question').removeClass('text-response-back-preview');
                    $('.back-question a').css('display', 'block');
                });
                $(document).on('click', '.text-response-back-preview', function () {
                    $('.divPreview.preview-text-response').removeClass('visible');
                    $('.text-response-question').fadeIn();
                    $('.back-question').addClass('text-response-back-question').removeClass('text-response-back-preview');
                    $('.back-question a').css('display', 'block');
                 });

                $('.formTR .btSave').click(function () {
                    var validate = true;
                    $('.back-left').removeClass('hidden');
                    $('.back-question').remove();
                    
                    if ($('.txtaTRQuestion').val().trim() == '') {
                        $('.txtaTRQuestion').val('').parents('.form-group').addClass('hasError');
                        validate = false;
                    } else $('.txtaTRQuestion').parents('.form-group').removeClass('hasError');

                    if ($('.txtTRAnswer').val().trim() == '') {
                        $('.txtTRAnswer').val('').parents('.form-group').addClass('hasError');
                        validate = false;
                    } else $('.txtTRAnswer').parents('.form-group').removeClass('hasError');

                    if (!validate) return false;

                    if (typeof($('.text-response-question').attr('qid')) == 'undefined') {
                        var act = 'create';
                    } else var act = 'update';

                    var sendData = {
                        act: act,
                        name: $('.txtaTRQuestion').val(),
                        courseid: <?php echo $this->course_id; ?>,
                        noanswers: 1,
                        feedbacks: [$('.txtTRResponse').val()],
                        answers: [$('.txtTRAnswer').val()]
                    };

                    if (act == 'update') {
                        sendData.qid = $('.text-response-question').attr('qid');
                        var url = "<?php echo JUri::base() . "question/edit_" . $this->course_id; ?>_shortanswer_" + sendData.qid + ".html";
                    } else {
                        var url = "<?php echo JUri::base() . "question/create_" . $this->course_id; ?>_4.html";
                    }

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: sendData,
                        beforeSend: function () {
                            lgtCreatePopup('', {'content': 'Loading...'});
                        },
                        success: function (data, textStatus, jqXHR) {
                            lgtRemovePopup();
                            var res = JSON.parse(data);

                            if (res.error == 1) {
                                switch (res.comment) {
                                    case 'nopermission':
                                        lgtCreatePopup('oneButton', {content: "You don\'t have permission to create question."}, function () {
                                            lgtRemovePopup();
                                        });
                                        break;
                                    default:
                                        lgtCreatePopup('oneButton', {content: res.comment}, function () {
                                            lgtRemovePopup();
                                        });
                                        break;
                                }
                            } else {
                                addQuestion(res.data, act);
                                $('.joomdle-create-question .text-response-question').hide();
                                $('.joomdle-course-assessment').fadeIn();
                                if ($('.choiceQuesTitle').hasClass("error1")) {
                                    $('.choiceQuesTitle').removeClass("error1");
                                }
                            }
                        }
                    });
                });

                function addQuestion(dataText, act) {
                    var data = JSON.parse(dataText);
                    questionsArray[data.id] = dataText;
                    if ($(".question.q" + data.id)[0]) {
                        // $(".question.q" + data.id).remove();
                        $(".question.q" + data.id + " .questionName").html(data.name);

                    } else {
                        var html = '<li class="question q' + data.id + '" qid="' + data.id + '" data-qtype="' + data.qtype + '">' +
                            '<div class="questionName">' + data.name + '</div>' +
                            '<div class="questionArrowDown" onclick="showDown(' + data.id + ')">' +
                            '<span class="glyphicon glyphicon-menu-down"></span>' +
                            '<div class="questionAction qA_' + data.id + '">' +
                            '<ul class="ulQuestionAction">' +
                            '<li class="liEditQuestion">' +
                            '<span><?php echo Jtext::_('QUESTION_BTN_EDIT'); ?></span>' +
                            '</li>' +
                            '<li class="liRemoveQuestion">' +
                            '<span><?php echo Jtext::_('QUESTION_BTN_REMOVE'); ?></span>' +
                            '</li>' +
                            '</ul>' +

                            '</div>' +
                            '<div class="triangle tr_' + data.id + '"></div>' +
                            '</div>' +

                            '<div class="clear"></div>' +
                            '</li>';
                        $('.listQuestions').append(html);
                    }

                    var arr = $('.hdSelectedQuestions').val().split(',');
                    arr.push(data.id);
                    console.log(questionsArray);
                    if (act == 'create') {
                        $('.hdSelectedQuestions').val(arr.toString());
                    }

                    if (!$('.listQuestions').hasClass('active')) $('.listQuestions').addClass('active');
                }

                $(document).on('keyup', 'input, textarea', function() {
                    if ($(this).val().trim() != '') $(this).parents('.form-group').removeClass('hasError');
                });

            })(jQuery);
        </script>

    <?php } elseif (isset($this->action) && $this->action == 'list') {
        $jump_url = JoomdleHelperContent::getJumpURL();
        $count = 0;
        ?>
        <div class="assessment-list">
            <p class="lblTitle"><?php echo Jtext::_('ASSESSMENT_ASSESSMENT'); ?><span class="assessmentCount"></span>
            </p>
            <?php
            foreach ($this->mods as $tema) :
                ?>
                <div class="joomdle_course_section">
                    <div class="joomdle_section_item joomdle_section_list_item">
                        <?php
                        $resources = $tema['mods'];
                        if (is_array($resources)) : ?>
                            <?php
                            foreach ($resources as $id => $resource) {
                                if (!$resource['atype']) $resource ['atype'] = 1;
                                $mtype = JoomdleHelperSystem::get_mtype($resource['mod']);
                                if (!$mtype) continue;

                                echo '<div class="assessment-list-item" data-mid="' . $resource['id'] . '" data-atype="' . $resource['atype'] . '">';
                                $icon_url = JoomdleHelperSystem::get_icon_url($resource['mod'], $resource['type']);
                                if ($icon_url) {
                                    echo '<div class="assessment-icon">';
                                    echo '<img align="center" src="' . JURI::base() . 'media/joomdle/images/AssessmentIcon.png">';
                                    echo '</div>';
                                }

                                if ($resource['mod'] == 'label') {
                                    $label = JoomdleHelperContent::call_method('get_label', $resource['id']);
                                    echo '</P>';
                                    echo $label['content'];
                                    echo '</P>';
                                    continue;
                                }

                                if (($resource['available'])) {
                                    $direct_link = JoomdleHelperSystem::get_direct_link($resource['mod'], $this->course_id, $resource['id'], $resource['type']);
                                    if ($direct_link) {
                                        if ($resource['display'] == 6)
                                            $resource_target = 'target="_blank"';
                                        else
                                            $resource_target = '';

                                        if ($direct_link != 'none')
                                            echo "<a $resource_target  href=\"" . $direct_link . "\">" . $resource['name'] . "</a>";
                                    } else
                                        echo "<a $target href=\"" . JURI::base() . "mod/" . $mtype . "_" . $this->course_id . "_" . $resource['id'] . "_" . $itemid . ".html\">" . $resource['name'] . "</a>";
                                    ?>
                                    <div class="arrowMenuDown">
                                        <span class="glyphicon glyphicon-menu-down"></span>
                                        <div class="menuAction">
                                            <ul class="ulMenuAction">
                                                <a href="javascript: void(0);">
                                                    <li class="liEdit">
                                                        <span><?php echo JText::_('COM_JOOMDLE_EDIT_ASSESSMENT'); ?></span>
                                                    </li>
                                                </a>
                                                <a href="javascript: void(0);">
                                                    <li class="liRemove">
                                                        <span><?php echo JText::_('COM_JOOMDLE_REMOVE_ASSESSMENT'); ?></span>
                                                    </li>
                                                </a>
                                            </ul>
                                        </div>
                                        <div class="triangle"></div>
                                    </div>
                                    <?php
                                    echo '</div>';
                                } else {
                                    echo $resource['name'] . '</div>';
                                    if ((!$resource['available']) && ($resource['completion_info'] != '')) : ?>
                                        <div class="joomdle_completion_info">
                                            <?php echo $resource['completion_info']; ?>
                                        </div>
                                    <?php
                                    endif;
                                }
                                $count++;
                            }
                            ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach;
            if ($count == 0) {
                echo '<p class="textNoAssessment center">' . JText::_('COM_JOOMDLE_NO_ASSESSMENT') . '</p>';
            }
            ?>

            <div class="joomdle-remove">
                <p><?php echo Jtext::_('COM_JOOMDLE_REMOVE_ASSESSMENT_TEXT'); ?></p>
                <button class="yes"><?php echo Jtext::_('COM_JOOMDLE_YES'); ?></button>
                <button class="no"><?php echo Jtext::_('COM_JOOMDLE_NO'); ?></button>
            </div>
            <div class="notification"></div>

        </div>
        <script type="text/javascript">
            (function ($) {
                $('.assessment-list .assessmentCount').html(' (<?php echo $count; ?>)');
                $('.btAddAssessment').click(function () {
                    window.location.href = "<?php echo JUri::base() . "assessment/create_" . $this->course_id; ?>.html";
                });
                $('.assessment-list .arrowMenuDown').click(function () {
                    $('.menuAction').hide();
                    $('.triangle').hide();
                    $(this).find('.menuAction').toggle();
                    $(this).find('.triangle').toggle();
                });
                $('body').click(function (e) {
                    if (!$(e.target).hasClass('ulMenuAction') && !$(e.target).hasClass('menuAction') && !$(e.target).hasClass('arrowMenuDown') && !$(e.target).hasClass('glyphicon-menu-down')) $('.menuAction, .triangle').hide();
                });
                $('.liEdit').click(function () {
                    var aid = $(this).parent().parent().parent().parent().parent('.assessment-list-item').attr('data-mid');
                    var atype = $(this).parent().parent().parent().parent().parent('.assessment-list-item').attr('data-atype');
                    window.location.href = "<?php echo JURI::base();?>assessment/edit_<?php echo $this->course_id;?>_" + atype + '_' + aid + ".html";
                });

                var removeAid, removeAtype;
                $('.liRemove').click(function () {
                    removeAid = $(this).parent().parent().parent().parent().parent('.assessment-list-item').attr('data-mid');
                    removeAtype = $(this).parent().parent().parent().parent().parent('.assessment-list-item').attr('data-atype');

                    $('body').addClass('overlay2');
                    $('.joomdle-remove').fadeIn();
                });

                $('.joomdle-remove .no').click(function () {
                    $('body').removeClass('overlay2');
                    $('.joomdle-remove').fadeOut();
                });
                $('.joomdle-remove .yes').click(function () {
                    var act = 'remove';
                    $.ajax({
                        url: window.location.href,
                        type: 'POST',
                        data: {
                            aid: removeAid,
                            act: act
                        },
                        beforeSend: function () {
                            $('.joomdle-remove').fadeOut();
                            lgtCreatePopup('', {'content': 'Loading...'});
                        },
                        success: function (data, textStatus, jqXHR) {
                            lgtRemovePopup();
                            var res = JSON.parse(data);

                            if (res.error == 1) {
                                lgtCreatePopup('oneButton', {content: res.comment}, function () {
                                    lgtRemovePopup();
                                });
                            } else {
                                $('.joomdle_course_section .assessment-list-item[data-mid="' + removeAid + '"]').fadeOut();
                                var str = $('.assessmentCount').html();
                                str = str.replace("(", "");
                                str = str.replace(")", "");
                                str = parseInt(str) - 1;
                                $('.assessmentCount').html('(' + str + ')');

                            }
                        }
                    });
                });

            })(jQuery);
        </script>
    <?php } elseif (isset($this->action) && $this->action == 'remove') {
        if ($this->assessment['res'] != 0) { ?>

        <?php } else {
            echo '<p class="tcenter" style="padding-top:30px;">' . JText::_('COM_JOOMDLE_ASSESSMENT_REMOVED') . '</p>';
        } ?>
    <?php } elseif (isset($this->action) && !isset($this->assessmentType) && $this->action == 'create') { ?>
        <div class="joomdle-course-assessment <?php echo $this->pageclass_sfx ?>">
            <p class="assessmentDescription">
                <b><?php echo Jtext::_('ASSESSMENT_ASSESS_READINESS'); ?></b>: <?php echo Jtext::_('COM_JOOMDLE_ASSESSMENT_ACCESS_TIP'); ?>
            </p>
            <p class="assessmentDescription">
                <b><?php echo Jtext::_('ASSESSMENT_CHECK_UNDERSTANDING'); ?></b>: <?php echo Jtext::_('COM_JOOMDLE_ASSESSMENT_CHECK_UNDERSTANDING_TIP'); ?>
            </p>
            <p class="assessmentDescription">
                <b><?php echo Jtext::_('ASSESSMENT_CREATE_QUIZ'); ?></b>: <?php echo Jtext::_('COM_JOOMDLE_ASSESSMENT_CREATE_QUIZ_TIP'); ?>
            </p>
            <p class="assessmentDescription">
                <?php echo Jtext::_('ASSESSMENT_SELECT'); ?>
            </p>
            <input type="hidden" value="" name="hdPurpose" class="hdPurpose"/>
            <div class="inputPurpose">
                <div p="1" class="status"></div>
                <span class="spanstatus"><?php echo Jtext::_('ASSESSMENT_ASSESS_READINESS'); ?></span>
            </div>
            <div class="inputPurpose">
                <div p="2" class="status"></div>
                <span class="spanstatus"><?php echo Jtext::_('ASSESSMENT_CHECK_UNDERSTANDING'); ?></span>
            </div>
            <div class="inputPurpose">
                <div p="3" class="status"></div>
                <span class="spanstatus"><?php echo Jtext::_('ASSESSMENT_CREATE_QUIZ'); ?></span>
            </div>
            <button class="btNext" type="button"><?php echo Jtext::_('ASSESSMENT_NEXT'); ?></button>
        </div>
        <script>
            (function ($) {
                $('.joomdle-course-assessment .status').click(function () {
                    $('.joomdle-course-assessment .status').removeClass('selected');
                    $(this).addClass('selected');
                    $('.hdPurpose').val($(this).attr('p'));

                });
                $('.spanstatus').click(function () {
                    $(this).prev('.status').click();
                });
                $('.joomdle-course-assessment .btNext').click(function () {

                    if ($('.hdPurpose').val() == '') {
                        lgtCreatePopup('oneButton', {content: "Please choose purpose."}, function () {
                            lgtRemovePopup();
                        });
                        addClass('lgt-visible');
                    } else
                        window.location.href = "<?php echo JUri::base() . "assessment/create_" . $this->course_id . "_" . $this->sectionid; ?>_" + $('.hdPurpose').val() + '.html';
                });
            })(jQuery);
        </script>
    <?php } ?>
<?php endif; ?>
<style type="text/css">
    .previewQuestions .question .index { float: left; margin-right: 3px; }
    .divPreview.visible .question { line-height: normal; border: none; padding: 5px 0; font-family: 'Open Sans', OpenSans-Light, sans-serif; font-weight: 300; font-size: 16px; color: #5e5e5e;}
    .back-question, .back-left { cursor: pointer; }
</style>
