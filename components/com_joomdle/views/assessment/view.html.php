<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewAssessment extends JViewLegacy {
    function display($tpl = null) {
        $app = JFactory::getApplication();
        $document = JFactory::getDocument();

        $pathway = $app->getPathWay();
        $menus = $app->getMenu();
        $menu = $menus->getActive();

        $params = $app->getParams();
        $this->assignRef('params', $params);

        $user = JFactory::getUser();
        $username = $user->username;

        $id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
        if (!$id) $id = JRequest::getVar( 'course_id' );
        if (!$id) $id = $params->get( 'course_id' );
        if (!$id) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
//            return;
        }
        $id = (int) $id;

        $this->course_id = $id;

        if ($params->get('use_new_performance_method'))
            $course_data = JHelperLGT::getCoursePageData(['id' => $id, 'username' => $username]);
        else
            $course_data = JoomdleHelperContent::call_method('get_course_page_data', (int)$id, $username);

        $this->course_info = json_decode($course_data['course_info'], true);
        $this->mods = json_decode($course_data['mods'], true);
        $this->course_status = $this->course_info['course_status'];
        $this->questions = json_decode($course_data['questions'], true);

        $userRoles = json_decode($course_data['userRoles'], true);
        $this->hasPermission = $userRoles['roles'];

        if (!$this->hasPermission[0]['hasPermission']) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//            return;
        }

        $action =  JRequest::getVar( 'action', null, 'NEWURLFORM' );
        if (!$action) $action = JRequest::getVar( 'action' );
        if ($action == 'create' || $action == 'edit' || $action == 'remove') {
            if ($this->course_status == 'pending_approval' || $this->course_status == 'approved') {
                echo JText::_('COM_JOOMDLE_COURSE_APPROVED_OR_PENDING_APPROVAL');
                return;
            }
            if ($this->course_info['self_enrolment']) {
                echo JText::_('COM_JOOMDLE_COURSE_PUBLISHED');
                return;
            }
        }
        $this->atype = JRequest::getVar('atype', null, 'NEWURLFORM');
        $this->sectionid = JRequest::getVar( 'section_id', null, 'NEWURLFORM' );

        switch ($action) {
            case 'list':
                $document->setTitle(JText::_('COM_JOOMDLE_ASSESSMENT'));
                $this->action = 'list';

                if (isset($_POST['act']) && $_POST['act'] == 'remove') {
                    $act = 3;
                    $q = array();
                    $q['qid'] = (int)$_POST['aid'];

                    $r = JoomdleHelperContent::call_method('create_quiz_activity', $act, $id, $username, $q, 0);

                    $result = array();
                    $result['error'] = 0;
                    $result['comment'] = 'Everything is fine';
                    $result['data'] = $r;
                    echo json_encode($result);
                    die;
                }
                $coursemods = JoomdleHelperContent::call_method ( 'get_course_mods', (int) $id, '', 'quiz');
                if ($coursemods['status']) {
                    $this->mods = $coursemods['coursemods'];
                } else {
                    $this->mods = array();
                }

                $arr = array();
                foreach ($this->mods as $section ) {
                    if (!empty($section['mods'])) {
                        foreach ($section['mods'] as $mod) {
                            $arr[$mod['id']] = $mod;
                        }
                    }
                }
                krsort($arr);
                $this->mods = array( array( 'mods'=>$arr ) );
                break;
            case 'create':
            case 'edit':
                if ($action == 'create') {
                    $this->action = 'create';
                    $document->setTitle(JText::_('ASSESSMENT_ASSESSMENT'));
                } else {
                    $this->action = 'edit';
                    $document->setTitle(JText::_('ASSESSMENT_ASSESSMENT'));
                    $aid = JRequest::getVar('aid', null, 'NEWURLFORM');
                    if (!$aid) $aid = JRequest::getVar('aid');
                    $this->aid = $aid;

                    $q = array('qid' => $aid);
                    if ($params->get('use_new_performance_method'))
                        $this->assessment = JHelperLGT::getModuleInfo('quiz', $aid, $id);
                    else
                        $this->assessment = JoomdleHelperContent::call_method('create_quiz_activity', 0, $id, $username, $q, 0);
                }

                $assessmentType = $this->atype;
                if (!$assessmentType) $assessmentType = JRequest::getVar('atype');
                if ($assessmentType) {
                    $this->wrapperurl = JUri::base()."courses/course/modedit.php?add=quiz&type=&course=$id&section=0&fromjl=1&atype=$assessmentType";
                    $this->assessmentType = $assessmentType;
                }
                if (isset($_POST['act']) && ($_POST['act'] == 'create' || $_POST['act'] == 'update')) {
                    $assessmentType = $_POST['atype'];
                    if ($_POST['act'] == 'update') {
                        $act = 2;
                    } else if ($_POST['act'] == 'create') {
                        $act = 1;
                    }
                    $q = array(
                        'name' => strip_tags($_POST['name']),
                        'des' => strip_tags($_POST['des']),
                        'atype' => $assessmentType,
                        'questions' => $_POST['questions']
                    );
                    if (isset($_POST['gradingscheme']) && $_POST['gradingscheme'] == 1) {
                        $q['gradeboundaryA'] = (int)$_POST['gradeboundaryA'];
                        $q['gradeboundaryB'] = (int)$_POST['gradeboundaryB'];
                        $q['gradeboundaryC'] = (int)$_POST['gradeboundaryC'];
                        $q['gradeboundaryD'] = (int)$_POST['gradeboundaryD'];
                    }
                    $q['gradingscheme'] = (int)$_POST['gradingscheme'];
                    if ($assessmentType == 2) {
                        $q['attempts'] = $_POST['attempts'];
                    } else if ($assessmentType == 3) {
                        $q['timeopen'] = $_POST['timeopen'];
                        $q['timeclose'] = $_POST['timeclose'];
                        $q['timelimit'] = $_POST['timelimit'];
                        $q['timezoneOffset'] = $_POST['timezoneOffset'];
                    }
                    if ($_POST['act'] == 'update') $q['qid'] = $aid;
                    $r = JoomdleHelperContent::call_method('create_quiz_activity', (int)$act, (int)$id, $username, $q, (int)$this->sectionid);

                    $result = array();
                    $result['error'] = 0;
                    $result['comment'] = 'Everything is fine';
                    $result['data'] = $r;
                    echo json_encode($result);
                    die;
                }
                break;

            case 'remove':
                $document->setTitle(JText::_('COM_JOOMDLE_REMOVE_ASSESSMENT'));
                $this->action = 'remove';
                $aid = JRequest::getVar('aid', null, 'NEWURLFORM');
                if (!$aid) $aid = JRequest::getVar('aid');
                $this->aid = $aid;
                $q = array('qid'=>$aid);
                if (isset($_POST['act']) && $_POST['act'] == 'remove') {
                    $act = 3;
                    $q = array();
                    $q['qid'] = $aid;

                    $r = JoomdleHelperContent::call_method('create_quiz_activity', $act, $id, $username, $q, 0);

                    $result = array();
                    $result['error'] = 0;
                    $result['comment'] = 'Everything is fine';
                    $result['data'] = $r;
                    echo json_encode($result);
                    die;
                }

                if ($params->get('use_new_performance_method'))
                    $this->assessment = JHelperLGT::getModuleInfo('quiz', $aid, $id);
                else
                    $this->assessment = JoomdleHelperContent::call_method('create_quiz_activity', 0, $id, $username, $q, 0);

                $this->wrapperurl = JUri::base()."courses/course/mod.php?sr=0&delete=".$aid;
                break;
            default:
                break;
        }

        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

        parent::display($tpl);
    }
}
?>
