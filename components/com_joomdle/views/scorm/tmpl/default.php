<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
$itemid = JoomdleHelperContent::getMenuItem();

$linkstarget = $this->params->get( 'linkstarget' );
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$session  = JFactory::getSession();
$token = md5 ($session->getId());

if ($session->get('device') == 'mobile') {
    ?>
    <style type="text/css">
        .navbar-header {
            background-color: #126db6;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/kottenator/jquery-circle-progress/1.2.0/dist/circle-progress.js"></script>
<?php
}
// require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');

$document = JFactory::getDocument();
$document->addScript(JUri::root() . 'components/com_community/assets/autosize.min.js');
$banner = new HeaderBanner(new JoomdleBanner);

$render_banner = $banner->renderBanner($this->course_info, $this->mods['sections'], $role);
?>
<?php if ($this->hasPermission[0]['hasPermission']) : ?>
    <?php if (isset($this->action) && ($this->action == 'create' || $this->action == 'edit')) {
        $edit = ($this->action == 'edit') ? true : false;
        if (isset($this->scorm['fname']) && $this->scorm['fname'] != '') $hasFile = true; else $hasFile = false;
        ?>
        <div class="joomdle-add-scorm <?php echo $this->pageclass_sfx?>">
            <!-- <h1><?php echo JText::_('COM_JOOMDLE_SCORM'); ?></h1> -->
            <!-- <p><?php echo Jtext::_('SCORM_TIP'); ?></p> -->
            <form action="" method="post" class="formAddScorm" enctype="multipart/form-data">
                <label class="mnTitle" for=""><?php echo Jtext::_('SCORM_TITLE'); ?></label><span class="red">*</span>
                <input type="text"  style="border: 1px solid rgba(130, 130, 130, 0.5);" name="txtTitle" class="txtTitle" value="<?php echo ($edit) ? htmlentities(ucwords($this->scorm['sname'])):''; ?>"/>
                <label class="mnDecription" for=""><?php echo Jtext::_('SCORM_DESCRIPTION'); ?></label><span class="red">*</span>
                <textarea  style="border: 1px solid rgba(130, 130, 130, 0.5);" name="txtaDescription" class="txtaDescription autosize" data-ft="1" /><?php echo ($edit) ? $this->scorm['des']:''; ?></textarea>

                <p class="scormNote"><?php echo Jtext::_('SCORM_NOTICE_MOBILE'); ?></p>

                <div id="uploadedScorm" style="<?php echo ($hasFile) ? 'display: block;' : '';?>">
                     <div id="myProgress" class="progress hidden">
                        <p></p>
                        <div id="bar_100">
                            <div id="myBar" class="bar"></div>
                        </div>
                    </div>
                    <!-- <span></span> -->
                    <span class="mnNameFile" style="<?php echo ($hasFile) ? '': 'display: none' ?>"><?php echo ($hasFile) ? $this->scorm['fname']:''; ?></span>
                    <div class="rm <?php echo ($hasFile) ? '': 'hidden' ?>"><img src="../images/delete.png"></div>
                   
                </div>

                <div class="clear"></div>
                
                <input type="file" name="fileUpload" class="fileUpload" style="display: none;"/>
                <input type="hidden" name="hdUploadFileName" class="hdUploadFileName" value="<?php echo ($edit) ? $this->scorm['fname']:''; ?>" />
                <input type="hidden" name="hdUploadTmpFile" class="hdUploadTmpFile" value="<?php //echo ($edit) ? $this->scorm['fname']:''; ?>" />
                <div class="clear"></div>
                <div class="buttons">
                   <button class="btSave <?php echo ($hasFile) ? '' : 'hidden';?>"><?php echo Jtext::_('SCORM_BTN_SAVE'); ?></button>
                    <button class="btUploadPackage" type="button" <?php echo ($hasFile) ? 'data-disable="1" style="display: none;"' : 'data-disable="0"'; ?>>
                    <!-- <img src="images/icons/uploadscorm30x30.png"> -->
                    <span><?php echo JText::_('COM_JOOMDLE_SCORM_UPLOAD'); ?></span>
                   <!--  <img class="ajax-load" src="<?php echo JURI::base().'components/com_joomdle/views/assignment/tmpl/Upload.png';?>"> -->
                </button>
                   <!--  <button class="btPreview" type="button"><?php echo Jtext::_('SCORM_BTN_PREVIEW'); ?></button> -->
                    <button class="btCancel" type="button"><?php echo Jtext::_('SCORM_BTN_CANCEL'); ?></button>
                </div>
            </form>
            <!-- <div class="divPreviewScorm">
                <p class="previewTitle"></p>
                <div class="previewScorm"><?php echo JText::_('COM_JOOMDLE_SCORM_PREVIEW'); ?></div>
                <p class="previewDescription"></p>
                <button class="previewBack"><?php echo JText::_('COM_JOOMDLE_BACK'); ?></button>
            </div> -->
            <div class="modal" id="ass-message">
            <div class="modal-content hienPopup">
            <p id = "error" class="errorscorm"></p>
                <button class="closetitle" type="button"><?php echo JText::_('COM_JOOMDLE_CLOSE'); ?></button>
            </div>
        </div>
        </div>
        <div class="notification"></div>


        <div class="rmPopup rmFilePopup changePosition">
            <p><?php echo JText::_('COM_JOOMDLE_REMOVE_CONTENT_TEXT'); ?></p>
            <button class="btRMCancel" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
            <button class="btRMYes" type="button"><?php echo JText::_('COM_JOOMDLE_BUTTON_REMOVE'); ?></button>
        </div>

        <div class="rmPopup continueSavePopup changePosition">
            <p><?php echo JText::_('COM_JOOMDLE_REMOVE_CONTENT_TEXT'); ?></p>
            <button class="btRMCancel" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
            <button class="btRMYes" type="button"><?php echo JText::_('COM_JOOMDLE_YES'); ?></button>
        </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/kottenator/jquery-circle-progress/1.2.0/dist/circle-progress.js"></script>
        <script>
            function animateElements() {
                $('.progressbar').each(function () {
                    var elementPos = $(this).offset().top;
                    var topOfWindow = $(window).scrollTop();
                    var percent = $(this).find('.circle').attr('data-percent');
                    var percentage = parseInt(percent, 10) / parseInt(100, 10);
                    var animate = $(this).data('animate');
                    if (elementPos < topOfWindow + $(window).height() - 30 && !animate) {
                        $(this).data('animate', true);
                        $(this).find('.circle').circleProgress({
                            startAngle: Math.PI / 2,
                            value: percentage,
                            thickness: 8,
                            animation: {
                                duration: 3000,
                                easing: 'circleProgressEasing'
                            },
                            fill: {
                                color: '#f8f8f8'
                            },
                            emptyFill: '#d9d9d9'

                        }).on('circle-animation-end', function(event) {
                            $(this).circleProgress({ value: 1.0});
                            }).on('mouseup', function(event){
                                $(this).trigger('circle-animation-end');
                        });
                    }
                });
            }
            // Show animated elements
//            animateElements();
            // $(window).scroll(animateElements);
        
            (function($) {
                $('.autosize').each(function () {
                    autosize(this);
                }).on('input', function () {
                    autosize(this);
                });
                $('.formAddScorm').submit(function(e) {
                    e.preventDefault();
                });

                $('.rm-notification').click(function() {
                    $(this).parent().fadeOut();
                });

                $('.formAddScorm .btCancel').click(function() {
                    window.location.href = '<?php echo JUri::base()."course/".$this->course_id; ?>.html';
                });

                $('.formAddScorm #uploadedScorm > .rm').click(function() {
                    // $('.rmPopup.rmFilePopup').show();
                    //   $('body').addClass('overlay2');
                    lgtCreatePopup('confirm', {
                        'content': '<?php echo JText::_('COM_JOOMDLE_REMOVE_CONTENT_TEXT'); ?>',
                        'yesText': '<?php echo JText::_('COM_JOOMDLE_BUTTON_REMOVE'); ?>'
                    }, function() {
                        $('.formAddScorm #uploadedScorm').hide();
                        $('.btUploadPackage').show();
                        $('.btSave').addClass('hidden');
                        $('.formAddScorm .btUploadPackage').attr('data-disable', 0).fadeIn();
                        $('body').removeClass('overlay2');
                        $('.rmFilePopup').hide();
                        $('.formAddScorm #uploadedScorm > span').html('').hide();
                        $('.hdUploadFileName').val('');
                        $('.hdUploadTmpFile').val('');
                        $('.fileUpload').val('');
                        $('.rm').addClass('hidden');
                    });
                });
                $('.rmPopup .btRMCancel').click(function() {
                    $(this).parent('.rmPopup').hide();
                    $('body').removeClass('overlay2');
                });
                $('.rmPopup.rmFilePopup .btRMYes').click(function() {
                    $('.formAddScorm #uploadedScorm').hide();
                    $('.btUploadPackage').show();
                    $('.btSave').addClass('hidden');
                    $('.formAddScorm .btUploadPackage').attr('data-disable', 0).fadeIn();
                    $('body').removeClass('overlay2');
                    $('.rmFilePopup').hide();
                    $('.formAddScorm #uploadedScorm > span').html('').hide();
                    $('.hdUploadFileName').val('');
                    $('.hdUploadTmpFile').val('');
		            $('.fileUpload').val('');
                });
                $('.formAddScorm .btPreview').click(function() {
                    $('.divPreviewScorm').addClass('visible');
                    if ($('.txtTitle').val().trim() == '') {
                        $('.divPreviewScorm .previewTitle').html('No title');
                    } else {
                        $('.divPreviewScorm .previewTitle').html($('.txtTitle').val());
                    }

                    $('.divPreviewScorm .previewDescription').html($('.txtaDescription').val());

                    $('.joomdle-add-scorm > *').not('.divPreviewScorm').fadeOut();
                });

                $('.divPreviewScorm .previewBack').click(function() {
                    $('.divPreviewScorm').removeClass('visible');
                    $('.joomdle-add-scorm > *').not('.divPreviewScorm').fadeIn();
                });
                $('.formAddScorm .btSave').click(function() {
                    $('.progressbar').removeClass('hidden');
                    var modal = document.getElementById('ass-message');
                    var er = document.getElementById('error');
                    $('.closetitle').click(function(){
                        modal.style.display = "none"; 
                         $('body').removeClass('overlay2');
                         $('.btUploadPack').removeClass('hidden');
                          $('.btSave').addClass('hidden');
                          $('.progressbar').addClass('hidden');
                    });
                    if ( $('.txtTitle').val().trim() == '' || $('.txtaDescription').val().trim() == '') {
                        if ($('.txtTitle').val().trim() == '') {
                            $('.mnTitle').addClass('error1');
                            $('.txtTitle').on('keyup', function(e) {
                                if ($(this).val().trim()) {
                                    $('.mnTitle').removeClass('error1');
                                    $('.txtTitle').removeClass('error1');
                                } else {
                                    $('.mnTitle').addClass('error1');
                                    $('.txtTitle').addClass('error1');
                                }
                            }).val('').addClass('error1');
                        }
                        if ($('.txtaDescription').val().trim() == '') {
                            $('.mnDecription').addClass('error1');
                            $('.txtaDescription').on('keyup', function(e) {
                                if ($(this).val().trim()) {
                                    $('.mnDecription').removeClass('error1');
                                    $('.txtaDescription').removeClass('error1');
                                } else {
                                    $('.mnDecription').addClass('error1');
                                    $('.txtaDescription').addClass('error1');
                                }
                            }).val('').addClass('error1');
                        }
                        return false;
                    }

                    if ($(this).attr('data-disable') == 1) {
                        $('.errorscorm').html( "Please wait some seconds for upload process.");
                        modal.style.display = "block";
                        $('body').addClass('overlay2');
                        //$('.lgt-notif-error').html('Please wait some seconds for upload process.').addClass('lgt-visible');
                    } else if ($('.formAddScorm #uploadedScorm > span').html() == '') {
                         $('.errorscorm').html( "You need to upload a SCORM package to proceed.");
                        modal.style.display = "block";
                        $('body').addClass('overlay2');
                        //$('.lgt-notif-error').html('You need to upload a SCORM package to proceed.').addClass('lgt-visible');
                    } else {
                        var fname = '', tmpfile = '';
                        if ($('.hdUploadFileName').val() != '') {
                            fname = $('.hdUploadFileName').val();
                        }
                        if ($('.hdUploadTmpFile').val() != '') {
                            tmpfile = $('.hdUploadTmpFile').val();
                        }
                        var act = '<?php echo ($edit) ? "update" : "create" ; ?>';
                        $.ajax({
                            url: window.location.href,
                            type: 'POST',
                            data: {
                                <?php echo ($edit) ? "mid: ".$this->sid."," : "" ; ?>
                                act: act,
                                sname: $('.txtTitle').val(),
                                courseid : <?php echo $this->course_id; ?>,
                                des: $('.txtaDescription').val(),
                                fname: fname,
                                tmpfile: tmpfile,
                                sectionid: <?php if($this->sectionid != Null){echo $this->sectionid;} else echo 0;?>
                            },
                            beforeSend: function() {
                                lgtCreatePopup('', {'content': 'Loading...'});
                                $('.progressbar').show();
                                animateElements();
                                // $(window).scroll(animateElements);   
                               // $('.lgt-notif').html('<?php echo JText::_("SCANNING_SCORM");?>').addClass('lgt-visible');
                            },
                            success: function(data, textStatus, jqXHR) {
                                var res = JSON.parse(data);
                                
                                $('.progressbar').addClass('hidden');
                                //$('.lgt-notif').removeClass('lgt-visible');
                                if (res.error == 1) {
                                    lgtRemovePopup();
                                    $('body').removeClass('overlay2');
                                    $('.notification').fadeOut();
                                    $('.errorscorm').html( res.comment);
                                    modal.style.display = "block";
                                    $('.formAddScorm #uploadedScorm').hide();
                                    $('.hdUploadFileName').val('');
                                    $('.hdUploadTmpFile').val('');
                                    $('.fileUpload').val('');
                                    $('#uploadedScorm span.mnNameFile').html('').hide();
                                    //$('.lgt-notif-error').html(res.comment).addClass('lgt-visible');
                                } else {
                                    window.location.href = '<?php echo JUri::base().'course/'.$this->course_id; ?>.html';
                                }
                            }
                        });
                    }
                });

                $('.formAddScorm .btUploadPackage').click(function() {
                    var modal = document.getElementById('ass-message');
                    var er = document.getElementById('error');
                    $('.closetitle').click(function(){
                        modal.style.display = "none"; 
                            $('.btUploadPackage').show();
                            $('.btSave').addClass('hidden');
                            $('.formAddScorm .btUploadPackage').attr('data-disable', 0).fadeIn();
                    });
                    if ($(this).attr('data-disable') != 1)
                        $('.fileUpload').click();
                    else {
                        $('.errorscorm').html( "This button is disabled because an uploaded SCORM file exists.");
                        modal.style.display = "block";
                        // $('.lgt-notif-error').html('This button is disabled because an uploaded SCORM file exists.').addClass('lgt-visible');
                    }
                });
                var filename;
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        
                        filename = input.files[0]['name'];
                        var reader = new FileReader();
                        reader.onload = function (e) {
                        };
                        reader.readAsDataURL(input.files[0]);
                    }
                }


                var files;

                $('.fileUpload').on('change', prepareUpload);

                function prepareUpload(event) {
                    readURL(this);
                    files = event.target.files;
                    if (files.length > 0) {
                        $('.formAddScorm').submit();
                    }
                }

                // $('.formAddScorm').on('submit', uploadFiles);
                $('.formAddScorm').on('submit', function(e) {
                    if ($('.hdUploadFileName').val() == '')
                    uploadFiles(e);
                });
                function uploadFiles(event) {
                     var modal = document.getElementById('ass-message');
                    var er = document.getElementById('error');
                    // $('.closetitle').click(function(){
                    //     modal.style.display = "none"; 
                    //     $('.btSave').addClass('hidden');
                    // });
                     $('.btSave').removeClass('hidden');
                    $('#uploadedScorm').show();
                    $('.formAddScorm .btSave').attr('data-disable', 1);
                    event.stopPropagation();
                    event.preventDefault();

                    var data = new FormData();
                    $.each(files, function(key, value) {
                        data.append(key, value);
                    });
                    data.append('token', '<?php echo $token;?>');
                    
                    $.ajax({
                        url: window.location.href,
                        type: 'POST',
                        data: data,
                        cache: false,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                         
                        },
                        xhr: function () {
                                jQuery('#myProgress p').html(filename);
                                var jqXHR = null;
                                if (window.ActiveXObject) {
                                    jqXHR = new window.ActiveXObject("Microsoft.XMLHTTP");
                                }
                                else {
                                    jqXHR = new window.XMLHttpRequest();
                                    jQuery('#myProgress').removeClass('hidden');
                                    jQuery(".btUploadPackage").hide();
                                }
                                //Upload progress
                                jqXHR.upload.addEventListener("progress", function (evt) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = Math.round((evt.loaded * 100) / evt.total);
                                        //Do something with upload progress
                                        jQuery('#myBar').css({width: percentComplete + '%'});
                                        if (percentComplete == 100) {
                                           
                                        }
                                    }
                                }, false);
                                //Download progress
                                jqXHR.addEventListener("progress", function (evt) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = Math.round((evt.loaded * 100) / evt.total);
                                    }
                                }, false);
                                return jqXHR;
                            },
                        success: function(data, textStatus, jqXHR) {
                            var res = JSON.parse(data);
                            if (res.error == 1) {
                                $('#uploadedScorm > span').html('');
                                $('#uploadedScorm').hide();
                                $('.fileUpload').val('');
                                $('.errorscorm').html( res.comment);
                                modal.style.display = "block";  
                               // $('.lgt-notif-error').html(res.comment).addClass('lgt-visible');
                            } else {
                                jQuery('.rm').removeClass('hidden');
                                jQuery('#myProgress').addClass('hidden');
                                $('.hdUploadFileName').val(res.data[0].name);
                                $('.hdUploadTmpFile').val(res.data[0].tmp_file);
                                var iconUrl = '<?php echo JURI::base();?>'+'images/icons/attach_file30x30.png';
                                $('#uploadedScorm span').html('<span>' + res.
                                    data[0].name + '</span>').fadeIn(100);
                                $('#uploadedScorm').show();
                                $('.formAddScorm .btUploadPackage').attr('data-disable', 1).fadeOut();
                                $('.formAddScorm .btSave').attr('data-disable', 0);
                            }
                        }
                    });
                }
            })(jQuery);
        </script>
    <?php } else if (isset($this->action) && $this->action == 'list') {
        $jump_url =  JoomdleHelperContent::getJumpURL ();
        $count = 0;
        ?>
        <div class="scorm-list">
            <p class="lblTitle"><?php echo Jtext::_('COM_JOOMDLE_SCORM'); ?> <span class="scormCount"></span></p>
            <?php
            foreach ($this->mods as $tema) :
                ?>
                <div class="joomdle_course_section">
                    <div class="joomdle_item_content joomdle_section_list_item_resources">
                        <?php
                        $resources = $tema['mods'];
                        if (is_array($resources)) : ?>
                            <?php
                            foreach ($resources as $id => $resource) {
                                $mtype = JoomdleHelperSystem::get_mtype ($resource['mod']);
                                if (!$mtype) // skip unknow modules
                                    continue;

                                echo '<div class="scorm-list-item" sid="'.$resource['id'].'">';
                                $icon_url = JoomdleHelperSystem::get_icon_url ($resource['mod'], $resource['type']);
                                if ($icon_url) {
                                    echo '<div class="scorm-icon">';
                                    // echo '<img align="center" src="'. $icon_url.'">';
                                    echo '<img align="center" src="/media/joomdle/images/ScormIcon.png">';
                                    //echo '<p class="assessment-name">'.ucfirst($mtype).'</p>';
                                    echo '</div>';
                                }

                                if ($resource['mod'] == 'label')
                                {
                                    $label = JoomdleHelperContent::call_method ('get_label', $resource['id']);
                                    echo '</P>';
                                    echo $label['content'];
                                    echo '</P>';
                                    continue;
                                }

                                if (($resource['available']))
                                {
                                    $direct_link = JoomdleHelperSystem::get_direct_link ($resource['mod'], $this->course_id, $resource['id'], $resource['type']);
                                    if ($direct_link)
                                    {
                                        // Open in new window if configured like that in moodle
                                        if ($resource['display'] == 6)
                                            $resource_target = 'target="_blank"';
                                        else
                                            $resource_target = '';

                                        if ($direct_link != 'none')
                                            echo "<a $resource_target  href=\"".$direct_link."\">".$resource['name']."</a>";
                                    }
                                    else
                                        echo "<a $target href=\"".JUri::base()."mod/scorm_".$this->course_id.'_'.$resource['id'].'_'.$itemid.".html\">".$resource['name']."</a>";
                                    echo '<div class="questionArrowDown">';
                                    echo '<span class="glyphicon glyphicon-menu-down"></span>';
                                    ?>
                                    <div class="questionAction">
                                        <ul class="ulQuestionAction">
                                            <li class="liEditQuestion">
                                                <div class="icon-pencil"></div>
                                                <span><?php echo Jtext::_('COM_JOOMDLE_EDIT_SCORM'); ?></span>
                                            </li>
                                            <li class="liRemoveQuestion">
                                                <div class="icon-delete"></div>
                                                <span><?php echo Jtext::_('COM_JOOMDLE_REMOVE_SCORM'); ?></span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="triangle"></div>
                                    <?php
                                    echo '</div>';
                                    echo '<div class="clear"></div>';
                                    echo '</div>';
                                }
                                else
                                {
                                    echo $resource['name'] .'</div>';
                                    if ((!$resource['available']) && ($resource['completion_info'] != '')) : ?>
                                        <div class="joomdle_completion_info">
                                            <?php echo $resource['completion_info']; ?>
                                        </div>
                                    <?php
                                    endif;
                                }
                                $count++;
                            }
                            ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach;
            if ($count == 0) {
                echo '<p class="textNoScorm">'.JText::_('COM_JOOMDLE_NO_SCORM').'</p>';
            }
            ?>
        </div>

        <div class="joomdle-remove">
            <p><?php echo Jtext::_('COM_JOOMDLE_REMOVE_SCORM_TEXT'); ?></p>
            <button class="yes"><?php echo Jtext::_('SCORM_YES'); ?></button>
            <button class="no"><?php echo Jtext::_('SCORM_NO'); ?></button>
        </div>
        <div class="notification"><span></span></div>
        <script type="text/javascript">
            (function($) {
                <?php //if ($count > 0) { ?>
                $('.scorm-list .scormCount').html('(<?php echo $count; ?>)');
                <?php //} ?>
                $('.liEditQuestion').click(function() {
                    var sid = $(this).parent().parent().parent().parent().attr('sid');
                    window.location.href = "<?php echo JUri::base()."scorm/edit_".$this->course_id; ?>_"+sid+".html";
                });
                var removeSid;
                $('.liRemoveQuestion').click(function() {
                    removeSid = $(this).parent().parent().parent().parent().attr('sid');
                    $('body').addClass('overlay2');
                    $('.joomdle-remove').fadeIn();
                });
                $('.scorm-list .scorm-list-item .questionArrowDown').click(function() {
                    $('.questionAction').hide();
                    $('.questionArrowDown .triangle').hide();
                    $(this).find('.questionAction').toggle();
                    $(this).find('.triangle').toggle();
                });
                $('body').click(function(e) {
                    if ( !$(e.target).hasClass('ulQuestionAction') && !$(e.target).hasClass('questionAction') && !$(e.target).hasClass('questionArrowDown') && !$(e.target).hasClass('glyphicon-menu-down') ) $('.questionAction, .triangle').hide();
                });
                $('.btAddScorm').click(function() {
                    window.location.href = "<?php echo JUri::base()."scorm/create_".$this->course_id; ?>.html";
                });
                $('.joomdle-remove .no').click(function() {
                    $('body').removeClass('overlay2');
                    $('.joomdle-remove').fadeOut();
                });
                $('.joomdle-remove .yes').click(function() {
                    var act = 'remove';
                    $.ajax({
                        url: window.location.href,
                        type: 'POST',
                        data: {
                            mid: removeSid,
                            act: act
                        },
                        beforeSend: function() {
                            $('.joomdle-remove').fadeOut();
                            lgtCreatePopup('', {'content': 'Loading...'});
                        },
                        success: function(data, textStatus, jqXHR) {
                            lgtRemovePopup();
                            var res = JSON.parse(data);

                            if (res.error == 1) {
                                $('.errorscorm').html( res.comment);
                                modal.style.display = "block";  
                               // $('.lgt-notif-error').html(res.comment).addClass('lgt-visible');
                            } else {
                                $('.joomdle_course_section .scorm-list-item[sid="'+removeSid+'"]').fadeOut();
                                var str = $('.scormCount').html();
                                str = str.replace("(", "");
                                str = str.replace(")", "");                                    
                                str = parseInt(str) - 1;
                                $('.scormCount').html('(' + str + ')');
                            }
                        }
                    });
                });
            })(jQuery);
        </script>
    <?php
    } else if (isset($this->action) && $this->action == 'remove') {
        if ($this->scorm['error'] == 0) {
            ?>
            <div class="joomdle-remove">
                <p><?php echo Jtext::_('COM_JOOMDLE_REMOVE_SCORM_TEXT'); ?></p>
                <button class="yes"><?php echo Jtext::_('SCORM_YES'); ?></button>
                <button class="no"><?php echo Jtext::_('SCORM_NO'); ?></button>
            </div>
            <div class="notification"><span></span></div>
            <script type="text/javascript">
                (function($) {
                    $('.joomdle-remove .no').click(function() {
                        window.location.href = "<?php echo JUri::base()."scorm/list_".$this->course_id; ?>.html";
                    });
                    $('.joomdle-remove .yes').click(function() {
                        var act = 'remove';
                        $.ajax({
                            url: window.location.href,
                            type: 'POST',
                            data: {
                                <?php echo ($this->sid) ? "mid: ".$this->sid."," : "" ; ?>
                                act: act
                            },
                            beforeSend: function() {
                               lgtCreatePopup('', {'content': 'Loading...'});
                            },
                            success: function(data, textStatus, jqXHR) {
                                lgtRemovePopup();
                                var res = JSON.parse(data);
                                if (res.error == 1) {
                                    $('.errorscorm').html( res.comment);
                                    modal.style.display = "block";
                                   // $('.lgt-notif-error').html(res.comment).addClass('lgt-visible');
                                } else {
                                    window.location.href = '<?php echo JUri::base().'course/'.$this->course_id; ?>.html';
                                }
                            }
                        });
                    });

                })(jQuery);
            </script>
        <?php
        } else {
            echo '<p class="tcenter" style="padding-top:30px; margin-top:65px;">'.JText::_('COM_JOOMDLE_SCORM_REMOVED').'</p>';
        }
    }
endif;?>
