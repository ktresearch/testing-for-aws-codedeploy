<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewScorm extends JViewLegacy {
    function display($tpl = null) {
        global $mainframe;

        $app = JFactory::getApplication();
        $pathway = $app->getPathWay();
        $document = JFactory::getDocument();
        $menus = $app->getMenu();
        $menu = $menus->getActive();

        $params = $app->getParams();
        $this->assignRef('params', $params);
        $user = JFactory::getUser();
        $username = $user->username;
        
        $id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
        if (!$id) $id = JRequest::getVar( 'course_id' );
        if (!$id) $id = $params->get( 'course_id' );
        if (!$id) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
//            return;
        }
        $id = (int) $id;
        
        $this->hasPermission = JFactory::hasPermission($id, $username);
        if (!$this->hasPermission[0]['hasPermission']) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//            return;
        }

        $this->course_id = $id;

        if ($params->get('use_new_performance_method'))
            $this->course_info = JHelperLGT::getCourseInfo(['id' => $id, 'username' => $username]);
        else
            $this->course_info = JoomdleHelperContent::getCourseInfo($id, $username);

        $this->course_status = $this->course_info['course_status'];

        $action = JRequest::getVar( 'action', null, 'NEWURLFORM' );
        if (!$action) $action = JRequest::getVar( 'action' );
        if ($action == 'create' || $action == 'edit' || $action == 'remove') {
            if ($this->course_status == 'pending_approval' || $this->course_status == 'approved') {
                echo JText::_('COM_JOOMDLE_COURSE_APPROVED_OR_PENDING_APPROVAL');
                return;
            }
            if ($this->course_info['self_enrolment']) {
                echo JText::_('COM_JOOMDLE_COURSE_PUBLISHED');
                return;
            }
        }

        $sid = JRequest::getVar( 'sid', null, 'NEWURLFORM' );
        if (!$sid) $sid = JRequest::getVar( 'sid' );
        if ($sid) $this->sid = $sid;
        $this->sectionid = JRequest::getVar( 'section_id', null, 'NEWURLFORM' );

        if (isset($_POST['act']) && $_POST['act'] == 'remove') {
            $array = array();
            $array['act'] = 3;
            $array['courseid'] = (int)$this->course_id;
            $array['username'] = (strip_tags($username));
            $array['mid'] = (int)$_POST['mid'];

            $r = JoomdleHelperContent::call_method('create_scorm_activity', $array, 0);

			$result = array();
            $result['error'] = $r['error'];
            $result['comment'] = $r['mes'];
            $result['data'] = $r['data'];
            echo json_encode($result);
            die;

        } else if (isset($_POST['act']) && ($_POST['act'] == 'create' || $_POST['act'] == 'update')) {
            $array = array();
            if ($_POST['act'] == 'update') {
                $array['act'] = 2;
                $array['mid'] = (int)$_POST['mid'];
            } else if ($_POST['act'] == 'create') {
                $array['act'] = 1;
            }
            $array['courseid'] = (int)$_POST['courseid'];
            $array['sname'] = (strip_tags($_POST['sname']));
            $array['des'] = (strip_tags($_POST['des']));
            $array['section'] = 0;

            $array['username'] = (strip_tags($username));
            if (isset($_POST['fname'])) $array['fname'] = (strip_tags($_POST['fname']));
            if (isset($_POST['tmpfile'])) $array['tmpfile'] = (strip_tags($_POST['tmpfile']));
            if (isset($_POST['sectionid'])) $sectionid = (int)$_POST['sectionid'];

            $r = JoomdleHelperContent::call_method('create_scorm_activity', $array, $sectionid);
            if (!empty($_POST['tmpfile'])) unlink(dirname($app->getCfg('dataroot')).'/temp/'.$_POST['tmpfile']);
			$result = array();
            $result['error'] = $r['error'];
            $result['comment'] = $r['mes'];
            $result['data'] = $r['data'];
            echo json_encode($result);
            die;

        } else if (!empty($_FILES)) {
//            $zipMIMEType = array('application/zip', 'application/x-zip', 'application/x-zip-compressed' , 'multipart/x-zip' , 'application/x-compressed');
            $zipType = array('zip');
            $file_name = $_FILES[0]['name'];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if ($_FILES[0]['size'] > 100*1024*1024) {
                $result['error'] = 1;
                $result['comment'] = 'File size is too large (< 100MB)';
            } else if (strlen($_FILES[0]['name']) > 250) {
                $result['error'] = 1;
                $result['comment'] = 'File name is too long (< 250char)';
            } else if (!in_array($ext, $zipType)) {
                $result['error'] = 1;
                $result['comment'] = 'Type of this file is invalid.';
            } else {
                $tmpFileName = 'tmp_file'.microtime(true)*10000;
                $tmp_file = dirname($app->getCfg('dataroot')).'/temp/'.$tmpFileName;
                file_put_contents ($tmp_file, file_get_contents($_FILES[0]['tmp_name']));

                $result = array();
                $result['error'] = 0;
                $result['comment'] = 'Everything is fine';
            }
            $result['data'] = $_FILES;
            $result['data'][0]['tmp_file'] = $tmpFileName;
            echo json_encode($result);die;
            die;
        }

        switch ($action) {
            case 'list':
                $document->setTitle(JText::_('COM_JOOMDLE_SCORM'));
//                $this->mods = JoomdleHelperContent::call_method ( 'get_course_mods', (int) $id, '', 'scorm');
                $coursemods = JoomdleHelperContent::call_method ( 'get_course_mods', (int) $id, '', 'scorm');
                if ($coursemods['status']) {
                    $this->mods = $coursemods['coursemods'];
                } else {
                    $this->mods = array();
                }
                $arr = array();
                foreach ($this->mods as $section ) {
                    if (!empty($section['mods'])) {
                        foreach ($section['mods'] as $mod) {
                            $arr[$mod['id']] = $mod;
                        }
                    }
                }
                krsort($arr);
                $this->mods = array( array( 'mods'=>$arr ) );
                $this->action = 'list';
                break;
            case 'create':
                $document->setTitle(JText::_('COM_JOOMDLE_SCORM'));
                $this->wrapperurl = JUri::base()."courses/course/modedit.php?add=scorm&type=&course=$id&section=0&fromjl=1";
                $this->action = 'create';
                break;
            case 'edit':
                $document->setTitle(JText::_('COM_JOOMDLE_SCORM'));
                // $this->wrapperurl = JUri::base().'courses/course/modedit.php?update='.$sid;
                $arr = array('courseid' => $this->course_id, 'mid' => $sid, 'act' => 0, 'username' => $username);
                if ($params->get('use_new_performance_method'))
                    $this->scorm = JHelperLGT::getModuleInfo('scorm', $sid, $id);
                else
                    $this->scorm = JoomdleHelperContent::call_method('create_scorm_activity', $arr, 0);
                
                $this->action = 'edit';
                break;
            case 'remove':
                $document->setTitle(JText::_('COM_JOOMDLE_REMOVE_SCORM'));
                $this->wrapperurl = JUri::base().'courses/course/mod.php?sr=0&delete='.$sid;
                $this->action = 'remove';
                $arr = array('courseid' => $this->course_id, 'mid' => $sid, 'act' => 0, 'username' => $username);
                if ($params->get('use_new_performance_method'))
                    $this->scorm = JHelperLGT::getModuleInfo('scorm', $sid, $id);
                else
                    $this->scorm = JoomdleHelperContent::call_method('create_scorm_activity', $arr, 0);
                break;
            default:
                break;
        }

        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

        parent::display($tpl);
    }
}
?>
