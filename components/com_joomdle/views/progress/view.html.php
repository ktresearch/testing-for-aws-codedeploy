<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class JoomdleViewProgress extends JViewLegacy {
    function display($tpl = null) {
        global $mainframe;

		$app = JFactory::getApplication();
		$pathway = $app->getPathWay();
		
		$menus = $app->getMenu();
		$menu = $menus->getActive();

		$params = $app->getParams();
		$this->assignRef('params', $params);

		$id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
		if (!$id) $id =  JRequest::getVar( 'course_id' );
		if (!$id)
			$id = $params->get( 'course_id' );

		$id = (int) $id;

		if (!$id)
		{
                    header('Location: /404.html');
                    exit;
//			echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
//			return;
		}

		$user = JFactory::getUser();
		$username = $user->username;
                
        if ($params->get('use_new_performance_method'))
            $course_data = JHelperLGT::getCoursePageData(['id' => $id, 'username' => $username]);
        else
            $course_data = JoomdleHelperContent::call_method('get_course_page_data', (int)$id, $username);
                
		$this->course_info = json_decode($course_data['course_info'], true);
		$this->is_enroled = $this->course_info['enroled'];
                
                $this->is_enroled = $this->course_info['enroled'];
                $userRoles = json_decode($course_data['userRoles'], true);
                $this->hasPermission = $userRoles['roles'];
                
                // Check permission
                if ($this->is_enroled == 0 && !$this->hasPermission[0]['hasPermission']) {
                    header('Location: /404.html');
                    exit;
                }
                
                // new api for mod information
//		if ($this->is_enroled)
//			$this->mods = json_decode($course_data['progressmods'], true);
			$this->mods = JHelperLGT::getModProgress($this->course_info['remoteid'], $username);
			
                        $this->allmods = json_decode($course_data['mods'], true);
//			$this->mods = JoomdleHelperContent::call_method ( 'get_course_mods', (int) $id, $username);
//		else
//			$this->mods = JoomdleHelperContent::call_method ( 'get_course_mods', (int) $id, '');
//print_r($this->mods);
		
		/* pathway */
        $cat_slug = $this->course_info['cat_id'].":".$this->course_info['cat_name'];
        $course_slug = $this->course_info['remoteid'].":".$this->course_info['fullname'];

        if(is_object($menu) && $menu->query['view'] != 'course') {
                        $pathway->addItem($this->course_info['cat_name'], 'index.php?view=coursecategory&cat_id='.$cat_slug);
                        $pathway->addItem($this->course_info['fullname'], 'index.php?view=detail&cat_id='.$cat_slug.'&course_id='.$course_slug);
                        $pathway->addItem(JText::_('COM_JOOMDLE_COURSE_CONTENTS'), '');
                }

        $document = JFactory::getDocument();
        $document->setTitle($this->course_info['fullname']);

		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

        parent::display($tpl);
    }
}