<?php defined('_JEXEC') or die('Restricted access');

$course_id = $this->course_info['remoteid'];
$itemid = JoomdleHelperContent::getMenuItem();

JFactory::getDocument()->setTitle(JText::_('PROGRESS_TITLE'));


$user = JFactory::getUser();
$username = $user->username;

$linkstarget = $this->params->get( 'linkstarget' );
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$exist_certificate = false;
$role = $this->hasPermission;
$user_role = array();
if (!empty($role)) {
    foreach (json_decode($role[0]['role']) as $r) {
        $user_role[] = $r->sortname;
    }
}

require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
$my = CFactory::getUser();
require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);

$render_banner = $banner->renderBanner($this->course_info, $this->allmods, $role, false);

$count_compled = 0;
$count_activity = 0;
$check_all_activity_grade_complete = true;

if (is_array ($this->allmods)) {
    $grade = array();
    foreach ($this->allmods as  $tema) :
        $resources = $tema['mods'];
        foreach($resources as $mod) {
            $grade[] = $mod['finalgrade'];
            if (($mod['mod'] != 'forum') && ($mod['mod'] != 'certificate') && ($mod['mod'] != 'feedback') && ($mod['mod'] != 'questionnaire')) {
                $mtype = JoomdleHelperSystem::get_mtype($mod['mod']);
                    $completion = JHelperLGT::getModCompletionStatus(['modid' => $mod['id'], 'username' => $username, 'act_type' => $mod['mod']]);
                if (!$mtype) continue;
                    if ($completion['mod_completion']) $count_compled++;

                $count_activity++;
            }
            if ($mod['mod'] == 'certificate') {
                $exist_certificate = true;
            }
            // check all activity type have grade complete
            if ($mod['mod'] == 'assign' || ($mod['mod'] == 'quiz' && $mod['atype'] == 2)) {
                    if (!$completion['mod_completion']) {
                    $check_all_activity_grade_complete = false;
                }
            }
        }
    endforeach;
}

if ($count_activity > 0) {
    $num_progress_bar = intval(($count_compled / $count_activity) * 100);
} else {
    $num_progress_bar = 0;
}

// =====================This is get link certificate=======================
// =====================This is get link certificate=======================
// $tt = 0;
// $ttt = 0;
// if (is_array ($this->mods['sections'])) {
//     $certArr = array();
//     foreach ($this->mods['sections'] as  $tema) {
//         $resources = $tema['mods'];
//         foreach($resources as $mod) {
//             if ($mod['mod'] != 'forum' && $mod['mod'] != 'certificate') {
//                 $ttt++;
//                 if($mod['mod_completion']==false){
//                     $tt++;
//                 }
//             }
//             if ($mod['mod'] == 'certificate' && $mod['available'] == 1) {
//                 $certid = $mod['id'];
//                 $certurl = JoomdleHelperSystem::get_direct_link ($mod['mod'], $course_id, $mod['id'], $mod['type']);
//                 if (empty($certurl)) {
//                     $certmtype = JoomdleHelperSystem::get_mtype ($mod['mod']);
//                     $certjump_url =  JoomdleHelperContent::getJumpURL ();
//                     $certitemid = JoomdleHelperContent::getMenuItem();
//                     // $certurl = $certjump_url."&mtype=".$certmtype."&id=".$mod['id']."&course_id=".$course_id."&create_user=0&Itemid=".$certitemid."&redirect=".$direct_link;
//                     $certurl = JURI::base()."mod/".$certmtype."_".$course_id."_".$mod['id']."_".$certitemid.".html";
//                 }
//                 $certArr[] = array('id'=>$certid, 'url'=>$certurl);
//             }
//         }
//     }
// }
// =====================This is get link certificate=======================
// =====================This is get link certificate=======================
?>
<script src="components/com_joomdle/js/jquery.animateNumber.min.js"></script>
<div class="joomdle-course <?php echo $this->pageclass_sfx?>">
    <?php if ($tt == 0 && count($certArr)>0 && $ttt != 0) { ?>
        <center>
            <a href="<?php echo JURI::base() . 'component/joomdle/certificate?courseid='.$course_id; ?>"><button type="button" class="btn " style="padding:3px 30px;background:#126DB6;color:#fff;border-radius:20px;" onclick="getCert(<?php echo $value['id']; ?>)">View Certificate </button></a>
        </center>
    <?php } ?>
    <div class="progress-note">
        <?php if ($num_progress_bar == 0 || $num_progress_bar == 100) { ?>
            <p class="note-r1"><?php echo ($num_progress_bar == 0) ? JText::_('COM_JOOMDLE_PROGRESS_NOTE_0') : JText::_('COM_JOOMDLE_PROGRESS_NOTE_COMPLETED');?></p>
        <?php } ?>
        <?php if ($num_progress_bar >= 0 && $num_progress_bar < 100) { ?>
            <p class="note-r2"><?php echo JText::_('COM_JOOMDLE_PROGRESS_NOTE');?></p>
        <?php } ?>
        <?php if ($num_progress_bar == 100 && $user_role[0] == 'student' && $exist_certificate == true) { ?>
            <div class="activity-cert-item">
                <a class="btn-get-certificate"
                   href="<?php echo JURI::base() . 'certificate/' . $course_id . '_1.html'; ?>"><?php echo JText::_('COM_JOOMDLE_GET_CERTIFICATE'); ?></a>
            </div>
        <?php } ?>
    </div>

    <?php

    $grade_letter_course = array(
        ['name' => 'A', 'value' => 90],
        ['name' => 'B', 'value' => 70],
        ['name' => 'C', 'value' => 50],
        ['name' => 'D', 'value' => 30]
    );
    $grade_letter_assign = array(
        ['name' => 'A', 'value' => 93],
        ['name' => 'B', 'value' => 83],
        ['name' => 'C', 'value' => 73],
        ['name' => 'D', 'value' => 60]
    );

    if ($this->mods['finalgradeletters'] != "Ungraded") {
        foreach ($grade_letter_course as $grade_course) {
            if ($this->mods['finalgrade'] >= $grade_course['value']) {
                $finalgradeletters_course = $grade_course['name'];
                break;
            }
        }
    }
    ?>
    <div class="progress-user">
        <div class="joms-list--pic col-xs-3">
            <a href="<?php echo CUrl::build('profile', '', array('userid' => $my->id));?>" alt="<?php echo $my->getDisplayName(); ?>">
                <img src="<?php echo $my->getAvatar(); ?>"/>
            </a>
        </div>
        <div class='joms-list--info col-xs-9'>
            <p class="name"><?php echo $my->getDisplayName(); ?></p>
            <p class="overall-grade">
                <?php
                if($this->mods['hidden'] == 1){
                    echo JText::_('COM_JOOMDLE_COURSE_OVERALLGRADE').": ". JText::_('PROGRESS_UNGRADED');
                } elseif($this->mods['finalgradeletters'] != "Ungraded" && $this->mods['hidden'] == 0 && $this->mods['gradeoffacitator'] && $this->mods['finalgrade'] > 0){
                    echo JText::_('COM_JOOMDLE_COURSE_OVERALLGRADE').": ". $finalgradeletters_course;
                } else {
                    echo JText::_('COM_JOOMDLE_COURSE_OVERALLGRADE').": ". JText::_('PROGRESS_UNGRADED');
                }
                ?></p>
        </div>
    </div>

    <?php if ($this->mods['finalgradeletters'] && isset($this->mods['gradefeedback']) && $this->mods['gradefeedback'] != '') { ?>
        <div class="progress-course_comment">
            <span><?php echo JText::_('COM_JOOMDLE_GRADE_COMMENT'); ?></span>
            <p><?php echo $this->mods['gradefeedback']; ?></p>
        </div>
    <?php } ?>

    <?php
    $jump_url =  JoomdleHelperContent::getJumpURL ();
    $session                = JFactory::getSession();
    $token = md5 ($session->getId());
    $course_id = $this->course_info['remoteid'];
    $direct_link = 1;
    $show_summary = $this->params->get( 'course_show_summary');
    $show_topics_numbers = $this->params->get( 'course_show_numbers');

    if ($this->course_info['guest']) $this->is_enroled = true;

    if (is_array ($this->mods['sections'])) {
        $certArr = array();
        $sec = 0;
        foreach ($this->mods['sections'] as  $tema) : ?>
            <div class="joomdle_course_progress <?php echo 'sec_'.$sec;?>">
                <span class="progress-section-name"><?php echo $tema['sectionname'];?></span>
                <div class="progress-activities">
                    <?php
                    $resources = $tema['mods'];
                    $i = 0;
                    foreach($resources as $mod) {
                        // if ($mod['mod'] != 'forum' && $mod['mod'] != 'certificate' && $mod['mod'] != 'questionnaire') {
                        if (!in_array($mod['mod'], ['forum', 'certificate', 'questionnaire', 'page', 'scorm'])) {
                            $completion = JHelperLGT::getModCompletionStatus(['modid' => $mod['id'], 'username' => $username, 'act_type' => $mod['mod']]);
                            if($completion['mod_completion']) {
                                $status =JText::_("PROGRESS_COM");
                            } else if(empty($mod['mod_lastaccess'])) {
                                $status = JText::_("PROGRESS_NOT_YET");
                            } else {
                                $status = JText::_("PROGRESS_IN_PRO");
                            }
                            // if (isset($mod['completionusegrade']) && $mod['completionusegrade'] == 1) {
                                ?>
                                <div class="progress-activity">
                                    <div class="activities">
                                        <div class="title-activity">
                                            <?php
                                            $icon_url = JoomdleHelperSystem::get_icon_url($mod['mod'], $mod['type']);
                                            if ($icon_url) {
                                                echo '<div class="activity-image ' . $mtype . '" >';
                                                echo '<img align="center" src="' . $icon_url . '"/>';
                                                echo '<h5>' . $mod['name'] . '</h5>';
                                                echo '</div>';
                                            }
                                            ?>
                                            <?php if ($completion['mod_completion']) { ?>
                                                <div>
                                    <span class="activity-grade">
                                <?php
                                if( $mod['gradingscheme'] == 1 && $mod['mod'] == 'quiz' ) {
                                    echo JText::_("PROGRESS_GRADE").': ';
                                    $finalgrade = $mod['finalgrade']*10;
                                    if($completion['mod_completion'] && $mod['hidden'] == 0){
                                        foreach ($mod['lettergradeoption'] as $grade) {
                                            if($finalgrade >= $grade['value']){
                                                echo  $grade['name'];
                                                break;
                                            }
                                            else{
                                                if ($grade['name'] == 'D') echo  "D";
                                            }
                                        }
                                    }
                                    else if ($completion['mod_completion'] && $mod['hidden'] == 1) echo JText::_('PROGRESS_UNGRADED');
                                    else echo '-';
                                } elseif( $mod['mod'] == 'assign') {
                                    if ($mod['grademax'] == 100) $finalGrade = $mod['finalgrade'];
                                    else if ($mod['grademax'] == 10) $finalGrade = $mod['finalgrade'] * 10;

                                    foreach ($grade_letter_assign as $grade_assign) {
                                        if ($finalGrade >= $grade_assign['value']) {
                                            $finalgradeletters_assign = $grade_assign['name'];
                                            break;
                                        }
                                    }

                                    echo JText::_("PROGRESS_GRADE").': ';
                                    echo ($completion['mod_completion'] && $mod['mod_completion_date'] && $mod['finalgrade']) ? $finalgradeletters_assign : JText::_('PROGRESS_UNGRADED');
                                }
                                ?>
                                    </span>
                                                    <span class="time-completed-activity"><?php echo $mod['mod_completion_date']; ?></span>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <?php $class = '';
                                        if ($completion['mod_completion']) {
                                            $class = 'completed';
                                        }
                                        echo '<div class="activity-status status' . $class . '"></div>'; ?>
                                    </div>
                                    <?php if ($completion['mod_completion'] && $mod['feedback'] || isset($mod['feedback'])) { ?>
                                        <div class="feedback"><?php if ($mod['feedback']) echo $mod['feedback']; else echo JText::_('PROGRESS_TIP1'); ?></div>
                                    <?php } ?>
                                </div>
                                <?php
                                $i++;
                            // }
                        }
                        if ($mod['mod'] == 'certificate' && $mod['available'] == 1) {
                            $certid = $mod['id'];
                            $certurl = JoomdleHelperSystem::get_direct_link ($mod['mod'], $course_id, $mod['id'], $mod['type']);
                            if (empty($certurl)) {
                                $certmtype = JoomdleHelperSystem::get_mtype ($mod['mod']);
                                $certjump_url =  JoomdleHelperContent::getJumpURL ();
                                $certitemid = JoomdleHelperContent::getMenuItem();
                                // $certurl = $certjump_url."&mtype=".$certmtype."&id=".$mod['id']."&course_id=".$course_id."&create_user=0&Itemid=".$certitemid."&redirect=".$direct_link;
                                $certurl = JURI::base()."mod/".$certmtype."_".$course_id."_".$mod['id']."_".$certitemid.".html";
                            }
                            $certArr[] = array('id'=>$certid, 'url'=>$certurl);
                        }
                    }
                    ?>
                </div>
                <script>
                    var i = <?php echo $i;?>;
                    if (i == 0) {
                        jQuery('.sec_<?php echo $sec;?> .progress-activities').addClass('hide');
                    }
                </script>
            </div>
            <?php $sec++; endforeach; ?>
    <?php } ?>

    <?php if ($this->params->get('show_back_links')) : ?>
        <div>
            <p align="center">
                <a href="javascript: history.go(-1)"><?php echo JText::_('COM_JOOMDLE_BACK'); ?></a>
            </p>
        </div>
    <?php endif; ?>
</div>

<script>
    /*jQuery(document).ready(function() {
        jQuery('.course_progress_bar').css({'width': '<?php // echo $num_progress_bar ? $num_progress_bar : 0; ?>%'});
        jQuery('#completed-percent').animateNumber({ number: <?php // echo $num_progress_bar ? $num_progress_bar : 0; ?> });
    });*/

    function getCert(modid) {
        //     jQuery.ajax({
        //         url: '<?php echo JUri::base();?>/courses/mod/certificate/view.php',
        //         type: 'post',
        //         data: {
        //             id: modid,
        //             action: 'get'
        //         },
        //         success: function(res) {
        //             console.log(res);
        //             alert('Get certificate successfully!');
        //         }
        //     });
    }
</script>