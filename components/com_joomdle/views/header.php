<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * @pakage: joomdle course header
 */
defined('_JEXEC') or die('Restricted access');
$app = JFactory::getApplication();

$params = $app->getParams();
$this->assignRef('params', $params);

$id = JRequest::getVar('course_id', null, 'NEWURLFORM');
if (!$id) $id = JRequest::getVar('course_id');
if (!$id) $id = $params->get('course_id');

$id = (int)$id;

$type = JRequest::getVar('type');
if ($type == 'appview')
    return;

$user = JFactory::getUser();
$username = $user->username;

if (!isset($this->course_info)) $course_info = JoomdleHelperContent::getCourseInfo($id, $username);
else $course_info = $this->course_info;

if (strcmp((JFactory::getLanguage()->getTag()), 'vi-VN') == 0) {
    $langcurrent = 'vn';
}
if (strcmp((JFactory::getLanguage()->getTag()), 'en-GB') == 0) {
    $langcurrent = 'en';

}

JPluginHelper::importPlugin('joomdlesocialgroups');
$dispatcher = JDispatcher::getInstance();
$course_group = $dispatcher->trigger('get_group_by_course_id', array($id));

if (!empty($course_group)) {
    //$href = 'index.php?option=com_community&view=groups&task=viewgroup&groupid='.$course_group[0];
    $href = 'index.php?option=com_community&view=groups&task=viewdiscussions&groupid='.$course_group[0];
} else {
    $href = '#';
    $class = 'no-group';
}
/*
 * June 25 2016
 * KV team: check permission of user
 */
// API get role of user
// $roles = JoomdleHelperContent::call_method('get_user_role', $id, $username);

if (!isset($this->hasPermission)) $roles = JFactory::hasPermission($id, $username);
else $roles = $this->hasPermission;

$user_role = array();
if (!empty($roles)) {
    foreach (json_decode($roles[0]['role']) as $r) {
        $user_role[] = $r->sortname;
    }
}

$published = false;
require_once(JPATH_SITE . '/components/com_hikashop/helpers/lpapi.php');
require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
$result = LpApiController::isCourseInHika($id);
if (!empty($result)) {
    if ((int)$result->product_published == 1) $published = true;
}
$groups = CFactory::getModel('groups');
$r = $groups->getShareGroups($id);
if (!empty($r)) $published = true;

// views is link progress bar.
// Facilitator, Manager, Course Creator, LP
if ($user_role[0] == 'teacher' || $user_role[0] == 'manager' || $user_role[0] == 'coursecreator') {
    $views = 'overallgrade/' . $id . '.html';
}

// Learner
if ($user_role[0] == 'student') {
    $views = 'progress/' . $id . '.html';
}

// Editting trainer(Course content) ... Hidden ...

?>
<!-- BEGIN COURSE MENU-->
<?php if ($user_role[0] != 'editingteacher' && ($published || ($course_info['visible'] == 1))) { ?>
    <div class="course-menu <?php echo $user_role[0]; ?> ">
        <div class="course-nav">
            <nav>
                <ul>
                    <?php
                    $full_name = $_SERVER['PHP_SELF'];
                    $full_url = $_SERVER['REQUEST_URI'];
                    $name_array = explode('/', $full_name);
                    $count = count($name_array);
                    $page_name = $name_array[$count - 2];

                    switch ($page_name) {
                        case 'course':
                        case 'page':
                            $active = 'active';
                            break;
                        case 'content':
                            $active = 'active2';
                            break;
                        case 'progress':
                            $active_pro = 'active';
                            break;
                        case 'overallgrade':
                            $active_pro = 'active';
                            break;
                        case 'viewgroup':
                            if (isset($course_group)) {
                                $active_group = 'active';
                            } else {
                                $active_group = '';
                            }
                            break;
                        case 'viewdiscussion':
                            $active_group = 'active';
                            break;
                        case 'coursecontent':
                            $active = 'active2';
                            break;
                        default:
                            $active = 'active';
                            break;
                    }
                    if (strpos($full_url, 'overallgrade') !== false) {
                        $active_pro = 'active';
                    }
                    ?>

                    <li class="course-outline <?php echo $active; ?>">
                        <a href="<?php echo JURI::root() . $langcurrent . '/course/' . $id . '.html'; ?>">
                            <center>
                                <div class="icon-bg"></div>
                            </center>
                            <span><?php echo JText::_("COM_JOOMDLE_COURSE_OUTLINE"); ?></span></a><span
                                class="shape"></span>
                    </li>
                    <li class="course-circle  <?php echo $active2; ?> <?php echo $active_group . ' ' . $class; ?>"
                        id="myBtn">
                    <a href="<?php echo $href; ?>" class="<?php echo $class; ?>">
                            <center>
                                <div class="icon-bg"></div>
                            </center>
                            <span><?php echo JText::_("COM_JOOMDLE_COURSE_LEARNING_CIRCLE"); ?></span></a><span
                                class="shape"></span>
                </li>
                    <li class="course-gradebook <?php echo $active_pro; ?>">
                        <a href="<?php echo JURI::base() . $langcurrent . '/' . $views; ?>">
                            <center>
                                <div class="icon-bg"></div>
                            </center>
                            <span><?php echo JText::_("COM_JOOMDLE_COURSE_PROGRESS"); ?></span></a><span
                                class="shape"></span>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="noGroupPopup modal" id="myModal">
            <div class="modal-content">
                <p><?php echo JText::_("COM_JOOMDLE_NO_COURSE_GROUP"); ?></p>
                <div class="rm close" style="opacity: 1;"><img src="../images/deleteIcon.png"></div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <script type="text/javascript">
        (function($) {
            $('html').addClass('noCourseOutline');
        })(jQuery);
    </script>
<?php } ?>
<style>
    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgba(189, 189, 189, 0.47);
    }
</style>

<script tye="text/javascript">
    (function ($) {
        // $('ul li a').on('click', function(){
        //     if($(this).hasClass('no-group') == false) {
        //         $(this).parent().addClass('active').siblings().removeClass('active');
        //     }
        // });
        // if(jQuery('[data-toggle="popover"]').popover()) {
        //     jQuery("a.no-group").removeAttr("href");
        // }

        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];


        $('.course-circle').click(function () {
            if ($(this).hasClass('no-group')) {
                modal.style.display = "block";
                $("a.no-group").removeAttr("href");
            }
        });
        $('.close').click(function () {
            modal.style.display = "none";
        });


        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    })(jQuery);
</script>