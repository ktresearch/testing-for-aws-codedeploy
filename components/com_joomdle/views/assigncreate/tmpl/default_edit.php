<?php defined('_JEXEC') or die('Restricted access'); 
    $mainframe = JFactory::getApplication();
    if($_GET['brief']){
        $brief = $_GET['brief'];
    }
    $course_id = $_GET['course_id'];
    $c = count($this->modss);
    $this->wrapper->url = '';
    $id_module = $_GET['module_id'];
    $session = JFactory::getSession();
    $device = $session->get('device');

    $lengt_intro = count($this->intro);
    if($_POST['name_file']){
        unlink($mainframe->getCfg('dataroot') . '/files/course/' .$course_id.'/assign/'.$_POST['name_file']);
        echo json_encode("delete file assignment brief success");die;
    }
    $date = date_timestamp_get(date_create());
    $date_time =  date('h_i_s_d_m_Y',$date);

    // =========================Eo biet==============================
  
if (!empty($_FILES['assignment_file']['type'])) {
    require_once(JPATH_ROOT.'/libraries/joomla/filesystem/folder.php');
    if (!JFolder::exists( $mainframe->getCfg('dataroot') . '/files')) {
        JFolder::create( $mainframe->getCfg('dataroot') . '/files', 0755);
    }
    if (!JFolder::exists( $mainframe->getCfg('dataroot') . '/files/course')) {
        JFolder::create( $mainframe->getCfg('dataroot') . '/files/course', 0755);
    }
    if (!JFolder::exists( $mainframe->getCfg('dataroot') . '/files/course/' .$course_id)) {
        JFolder::create( $mainframe->getCfg('dataroot') . '/files/course/' .$course_id, 0755);
        JFile::copy( $mainframe->getCfg('dataroot') . '/components/com_community/index.html', $mainframe->getCfg('dataroot').  '/files/course/' .$course_id . '/index.html');
    }
    if (!JFolder::exists( $mainframe->getCfg('dataroot') . '/files/course/' .$course_id.'/assign')){
         JFolder::create( $mainframe->getCfg('dataroot') . '/files/course/' .$course_id.'/assign', 0755);
    }
    $name_file_assignment = $_FILES['assignment_file']['name'];
    $name_file_assignment = str_replace(' ', '_', $name_file_assignment);
    $name_file_assignment = str_replace('-', '', $name_file_assignment);
    $name_file_assignment = str_replace('(', '', $name_file_assignment);
    $name_file_assignment = str_replace(')', '', $name_file_assignment);
    $new_name = preg_replace('/\.(?=.*\.)/', '', $name_file_assignment);
    $new_name = VNtoEN($new_name);
    move_uploaded_file($_FILES['assignment_file']['tmp_name'],$mainframe->getCfg('dataroot') . '/files/course/' .$course_id.'/assign/'.$date_time.'_'.$new_name);
    echo json_encode($date_time.'_'.$new_name);die;
}
function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    return $x;
  }
  else
  {
    $y=substr($x,0,$length) . ' ...';
    return $y;
  }
}
?>
<!-- datepicker and jquery-confirm-->
<link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/css/jquery-confirm.css';?>">
<link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/css/bootstrap-datepicker.css';?>">
<script type="text/javascript" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/js/bootstrap-datepicker.js';?>"></script>
<script type="text/javascript" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/js/jquery-confirm.js';?>"></script>
<script type="text/javascript" src="<?php echo JURI::base();?>/media/editors/tinymce/tinymce.min.js"></script>
<!-- end datepicker and jquery-confirm -->
<link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/css/assign.css';?>">
<?php require_once(JPATH_SITE.'/components/com_joomdle/views/header.php'); ?>
 <script type="text/javascript" src="<?php echo JURI::base();?>/media/editors/tinymce/tinymce.min.js"></script>
<div class="content-assign">
<!-- title Assignment -->

<div class="create-assignment">
    <center class="title" data="<?php echo $c; ?>"> <?php echo Jtext::_('ASSIGN_CREATE'); ?> <?php echo ($c > 0 ) ? '('.$c.')' :'';  ?></center>
</div>

<!-- title Assignment -->

<?php 
    $this->wrapper->url = $params->get( 'MOODLE_URL' ).'/course/modedit.php?update='.$id_module;
    if(strlen($this->intro[1]) > 2){
        $css = 'style="display:block !important;"';
        $name_file = $this->intro[1];
        $css_btn = 'style="background:#9B9D9E;" disabled="disabled"';
    }
?>
<div class="tit-des">
<div class="title-assign">
    <span>Title</span><br>
    <h5 style="color:red;display:none;"><?php echo Jtext::_('ASSIGN_CREATE_NOT_EMPTY'); ?></h5><br>
    <input type="text" placeholder="Add Title" class="tit-assign" require value="<?php echo $this->assign_info['name']; ?>">
</div>
<div class="des">
    <span>Introduction</span><br>
    <h5 style="color:red;display:none;"><?php echo Jtext::_('ASSIGN_CREATE_NOT_EMPTY'); ?></h5><br>
    <textarea class="des-assign" placeholder="Add Introduction"><?php echo $this->intro[0]; ?></textarea>
</div>
<!--345364756868978073534534534534534534534534534543-->
<!--345364756868978073534534534534534534534534534543-->
<!--345364756868978073534534534534534534534534534543-->
<!--345364756868978073534534534534534534534534534543-->
<div class="col-md-7 col-sm-12 col-xs-12 time-quiz">
    <div class=" col-md-4 col-sm-5 col-xs-8 duadate">
        <span>Due Date</span>
        <div class="input-group date ">
            <input type="text" class="form-control dua_date" placeholder="dd/mm/yy" value="<?php echo date('d/m/Y',$this->assign_info['duedate']); ?>" > 
            <div class="input-group-addon addon-calendar">
                <img class="icon-calendar" width="20" height="20" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/calender.png';?>">
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12" style="border-top:1px solid #B8D7E2;"></div>  
<div class="btn-bottom" style="padding-top:15px !important;">
    <div class="file_assignment" <?php echo $css; ?>>
        <div class="file-assignment pull-right" width="100%">
            <?php if($device == 'mobile') { echo custom_echo($name_file,32); }else { echo $name_file; } ?>
        </div>
        <b class="rm-assignment">x</b>
    </div>
    <a><button class="btnn assignment-brief" <?php echo $css_btn; ?>>Assignment Brief  <img class="ajax-load" style="margin-left:7px;margin-top:-5px;" width="15" height="15" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/Upload.png';?>"></button></a>
</div>



<div class="btn-bottom">
    <a href="<?php echo JURI::base().'index.php?option=com_joomdle&view=coursecontent&course_id='.$course_id;?>"><span class="cancel"><?php echo Jtext::_('ASSIGN_CREATE_CANCEL'); ?></span></a>
    <a><button class="btnn create_assign">Save</button></a>
    <a><button class="btnn preview-class">Preview</button></a>
</div>
</div>
<div class="long"></div>


<form method="post" class="uploadFile" action="" enctype='multipart/form-data' style="display:none;">
   <input name="assignment_file" class="fileassignment" id="toan" type="file" onchange="loadFile(event)">
</form>

<!-- End title-des ***-->
  




<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<div class="preview-assignment">
<div class="prev" style="clear:both !important;">
    <div class="title-assign">
    <center class="title" ></center>
    </div>  
</div>
    <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2 col-sm-offset-2 img-preview">
        <img width="100%" height="350" style="border-radius:10px;" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/assignment.png';?>"  alt="">
    </div>
    <div class="intro-preview" style="padding-top:30px;clear:both;">
        <span><b>Introduction</b></span><span class="pull-right"><b>Due: <span class="due_date_preview"></span></b></span>
        <div class=";clear:both !important;" ></div>
        <div class="text-intro-preview" style="text-align: justify;">
        </div>
        <div class="content-preview"></div>
        
    </div>
<div class="btn-bottom" style="padding-top:25px !important;">
            <a disable="disable"><button class="btnn" style="background:#9B9D9E !important;cursor: none;">Assignment Brief  <img style="margin-left:7px;margin-top:-5px;" width="15" height="15" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/Download.png';?>"></button></a>
        </div>
        <div class="btn-bottom" style="padding-top:50px !important;">
            <a disable="disable"><span class="cancel prev-preview" style="background:#4F5355 !important; color:white !important;cursor: pointer;">&larr; Prev</span></a>
            <a disable="disable"><button class="btnn" style="background:#9B9D9E !important;cursor: none;">Submit <i style="margin-left:7px;margin-top:-5px;">→</i> </button></a>
        </div>


</div>
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->





<!-- Iframe -->
<div style="clear:both;"></div>
<div class="iframe">
    <iframe 
        id="iframe-assign" 
        class="autoHeight"
        src="<?php echo $this->wrapper->url; ?>"
        width="<?php echo $this->params->get('width', '100%'); ?>"
        scrolling="<?php //echo $this->params->get('scrolling', 'auto'); ?>yes"
        allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"
        
        <?php if (!$this->params->get('autoheight', 1)) { ?>
            height="<?php echo $this->params->get('height', '700'); ?>"
        <?php
        }
        ?>
        align="top" 
        frameborder="0"
        <?php if ($this->params->get('autoheight', 1)) { ?>
            onload='itspower(this, false, true, 20)'
        <?php
        }
        ?>
    ></iframe>
</div>
<!-- end iframe -->

        













</div>
<script type="text/javascript">
var number_click = 0;
function loadFile(event) {
    var file = jQuery('.fileassignment')[0].files[0];
    if(file.type != 'application/msword' && file.type != 'application/pdf' && file.type != 'application/vnd.ms-excel' && file.type != 'application/vnd.oasis.opendocument.spreadsheet' && file.type != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' && file.type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && file.type != 'text/plain' && file.type != 'application/vnd.oasis.opendocument.text' ){
        /*jQuery.alert({
            title: 'Alert!',
            content: 'The file does not match format.Tip : File is doc,pdf,excel or text.',
            columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
            theme: 'blue',
            title: false,
        });*/
        alert('The file does not match format.Tip : File is doc,pdf,excel or text.');
        jQuery('.file-assignment').html(file.name);
        jQuery('.file_assignment').show();
        jQuery('.file-assignment').show();
        jQuery('.rm-assignment').show();
        jQuery('.assignment-brief').attr('disabled',true);
        jQuery('.assignment-brief').css('background','#9B9D9E');
        jQuery('.assignment-brief').attr('disabled',true);
    }else{
        jQuery('.file-assignment').html(file.name);
        jQuery('.file_assignment').show();
        jQuery('.file-assignment').show();
        jQuery('.rm-assignment').show();
        jQuery('.assignment-brief').attr('disabled',true);
        jQuery('.assignment-brief').css('background','#9B9D9E');
        jQuery('.assignment-brief').attr('disabled',true);
    }
    
};

jQuery('.uploadFile').on('submit', function(event){
    var file = jQuery('.fileassignment')[0].files[0];
    event.stopPropagation();
    event.preventDefault();
    jQuery.ajax({
        url : window.location.href,
        type : 'POST',
        data : new FormData(this),
        contentType: false,       
        cache: false,          
        processData:false,
        beforeSend: function () {
            jQuery('.ajax-load').attr('src','<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/load.gif';?>');
        },
        complete: function () {
            jQuery('.ajax-load').attr('src','<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/Upload.png';?>');
        },
        success : function(res){
            var link = '|_|_|'+res;
            jQuery('.create_assign').attr('disabled',true);
            jQuery('#iframe-assign').contents().find(".feditor #id_introeditor").html(jQuery('.des-assign').val()+link);
            jQuery('#iframe-assign').contents().find('#id_submitbutton2').click();
            setTimeout(function() {
                window.location.href = '<?php echo JUri::base()."index.php?option=com_joomdle&view=assigncreate&course_id=".$course_id."&list=1";?>';
            }, 1000);
        }
    });
});


jQuery(document).ready(function(e){
    jQuery(".icon-calendar").click(function(){
        jQuery(".dua_date").datepicker( "show" );
    });
    <?php if($device == 'mobile') { ?>
        var old_src = jQuery('.navbar-nav li[data-id="372"] a img').attr('src');
        var new_src = old_src.replace('_555555', '');
        jQuery('.navbar-nav li[data-id="372"] a img').attr('src',new_src);
        jQuery('.navbar-nav li[data-id="372"]').addClass('current active');
      <?php } else { ?>
        jQuery('.navbar-nav').find('li[data-id="373"]').addClass('current active');
      <?php  } ?>
    //==============================save===============================
    jQuery('.assignment-brief').click(function(){
        jQuery('.fileassignment').click();
    });
    jQuery('.rm-assignment').click(function(){
        jQuery('.file-assignment').hide();
        jQuery(this).hide();
        jQuery('.assignment-brief').removeAttr('disabled');
        jQuery('.assignment-brief').css('background','#4F5355');
        jQuery('.fileassignment').val('');
        number_click ++;
    });
    jQuery('.create_assign').click(function(){
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = (day<10 ? '0' : '') + day +'/'+ (month<10 ? '0' : '') + month + '/' +d.getFullYear();
        var cont = '';
        var j = 0;
        var q = jQuery('.des-assign').val();
        cont += q + "<br>";
        var dua_date = jQuery('.dua_date').val().split('/');
        var publish_date = output.split('/');
        if(jQuery('.tit-assign').val() == '' || jQuery('.des-assign').val() == '' ){
            alert("Title assignment or Introduction assignment empty.");
        }else if(dua_date == ''){
            alert('Date not be empty.');
        }else if( parseInt(dua_date[2]) < parseInt(publish_date[2]) || parseInt(dua_date[2]) == parseInt(publish_date[2]) && parseInt(dua_date[1]) < parseInt(publish_date[1]) || parseInt(dua_date[2]) == parseInt(publish_date[2]) && parseInt(dua_date[1]) == parseInt(publish_date[1]) && parseInt(dua_date[0]) < parseInt(publish_date[0])){
            alert('Due date not allowed before today');
        }else{
            if(jQuery('.fileassignment').val()==''){
                if(number_click==0){
                    jQuery('.create_assign').attr('disabled',true);
                    jQuery('#iframe-assign').contents().find(".feditor #id_introeditor").html(jQuery('.des-assign').val()+'|_|_|'+'<?php echo $name_file; ?>');
                    jQuery('#iframe-assign').contents().find('#id_submitbutton2').click();
                    setTimeout(function() {
                        window.location.href = '<?php echo JUri::base()."index.php?option=com_joomdle&view=assigncreate&course_id=".$course_id."&list=1";?>';
                    }, 1000);
                }else{
                    jQuery.ajax({
                        url : window.location.href,
                        type : 'POST',
                        data : {
                            name_file : '<?php echo $name_file; ?>'
                        },
                        beforeSend: function () {
                            jQuery('.ajax-load').attr('src','<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/load.gif';?>');
                        },
                        complete: function () {
                            jQuery('.ajax-load').attr('src','<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/Upload.png';?>');
                        },
                        success : function(res){
                        }
                    });
                    jQuery('.create_assign').attr('disabled',true);
                    jQuery('#iframe-assign').contents().find(".feditor #id_introeditor").html(jQuery('.des-assign').val()+'|_|_|');
                    jQuery('#iframe-assign').contents().find('#id_submitbutton2').click();
                    setTimeout(function() {
                        window.location.href = '<?php echo JUri::base()."index.php?option=com_joomdle&view=assigncreate&course_id=".$course_id."&list=1";?>';
                    }, 1000);
                }
                
            }else{
                var file = jQuery('.fileassignment')[0].files[0];
                if(file.type != 'application/msword' && file.type != 'application/pdf' && file.type != 'application/vnd.ms-excel' && file.type != 'application/vnd.oasis.opendocument.spreadsheet' && file.type != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' && file.type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && file.type != 'text/plain' && file.type != 'application/vnd.oasis.opendocument.text' ){
                    alert('The file does not match format.Tip : File is doc,pdf,excel or text.');
                }else{
                    <?php if($name_file){ ?>
                        jQuery.ajax({
                            url : window.location.href,
                            type : 'POST',
                            data : {
                                name_file : '<?php echo $name_file; ?>'
                            },
                            beforeSend: function () {
                                jQuery('.ajax-load').attr('src','<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/load.gif';?>');
                            },
                            complete: function () {
                                jQuery('.ajax-load').attr('src','<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/Upload.png';?>');
                            },
                            success : function(res){
                                jQuery('.uploadFile').submit();
                            }
                        });
                    <?php }else{ ?>
                        jQuery('.uploadFile').submit();
                    <?php } ?>
                }
            }
        }
    });
    jQuery('.prev-preview').click(function(){
        jQuery('.tit-des').show();
        jQuery('.create-assignment').show();
        jQuery('.preview-assignment').attr('style','display:none !important');
    });
    jQuery('.preview-class').click(function(){
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = (day<10 ? '0' : '') + day +'/'+ (month<10 ? '0' : '') + month + '/' +d.getFullYear();
        var cont = '';
        var j = 0;
        var q = jQuery('.des-assign').val();
        cont += q + "<br>";
        var dua_date = jQuery('.dua_date').val().split('/');
        var publish_date = output.split('/');
        if(jQuery('.tit-assign').val() == '' || jQuery('.des-assign').val() == '' ){
            alert('Title assignment or Introduction assignment empty.');
        }else if(dua_date == ''){
            alert('Date not be empty.');
        }else if( parseInt(dua_date[2]) < parseInt(publish_date[2]) || parseInt(dua_date[2]) == parseInt(publish_date[2]) && parseInt(dua_date[1]) < parseInt(publish_date[1]) || parseInt(dua_date[2]) == parseInt(publish_date[2]) && parseInt(dua_date[1]) == parseInt(publish_date[1]) && parseInt(dua_date[0]) < parseInt(publish_date[0])){
            alert('Due date not allowed before today');
        }else{
            jQuery('.prev .title-assign .title').html(jQuery('.tit-assign').val());
            jQuery('.text-intro-preview').html(jQuery('.des-assign').val());
            jQuery('.due_date_preview').html(jQuery('.dua_date').val());
            jQuery('.tit-des').hide();
            jQuery('.create-assignment').hide();
            jQuery('.preview-assignment').attr('style','display:block !important');

        }
    });
    jQuery('.del-assign').click(function(){
        var x = jQuery(this).attr('data');
        jQuery('#iframe-assign').contents().find('#module-'+x+' .mod-indent .commands a img[alt="Delete"]').click();
        var n = jQuery('.title').attr('data');
        var i;
        setInterval(function(){
            var li = jQuery('#iframe-assign').contents().find('#module-'+x).html();
            if(li==null){
                i = parseInt(n-1);
                jQuery('.title').attr('data',i);
                jQuery('#mod-'+x).remove();
                if(i==0){
                    jQuery('.attribute-assignment').html("<center>You have no Assignment yet.</center>");
                    jQuery('.title').html("Assignment");
                }
            }
        }, 1000);
    });
    //==================Time assign====================
   
    jQuery('.dua_date').click(function(){
        setInterval(function(){
                var y = jQuery('.dua_date').val();
                var str_dua = y.split("/");
                jQuery('#iframe-assign').contents().find("#id_duedate_day option[value='"+parseInt(str_dua[0])+"']").attr('selected','selected');
                jQuery('#iframe-assign').contents().find("#id_duedate_month option[value='"+parseInt(str_dua[1])+"']").attr('selected','selected');
                jQuery('#iframe-assign').contents().find("#id_duedate_year option[value='"+parseInt(str_dua[2])+"']").attr('selected','selected');
            }, 1000);
    });
    //==================End Time assign====================
     jQuery(".dua_date").datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight: true,
        autoclose: true,
    });
    
    //====================Lay title====================
    jQuery('.tit-assign').click(function(){
        setInterval(function(){
                
                var t = jQuery('.tit-assign').val();
                if(t==''){
                    jQuery('.title-assign h5').show();
                }else{
                    jQuery('.title-assign h5').hide();
                }
                
                jQuery('#iframe-assign').contents().find(".ftext input[name='name']").val(t);
        }, 1000);
        jQuery('#iframe-assign').contents().find('#id_submissiondrafts option[value="0"]').removeAttr('selected');
        jQuery('#iframe-assign').contents().find('#id_requiresubmissionstatement option[value="0"]').removeAttr('selected');
        jQuery('#iframe-assign').contents().find('#id_submissiondrafts option[value="1"]').attr('selected','selected');
        jQuery('#iframe-assign').contents().find('#id_requiresubmissionstatement option[value="1"]').attr('selected','selected');
    });
    //====================end title====================

    
});
</script>
<?php 
    function VNtoEN($str) {
// In thường
     $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
     $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
     $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
     $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
     $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
     $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
     $str = preg_replace("/(đ)/", 'd', $str);    
// In đậm
     $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
     $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
     $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
     $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
     $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
     $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
     $str = preg_replace("/(Đ)/", 'D', $str);
     return $str; // Trả về chuỗi đã chuyển
}  
?>