<?php defined('_JEXEC') or die('Restricted access'); 
    if($_GET['list']){
        $list = $_GET['list'];
    }
    if($_GET['brief']){
        $brief = $_GET['brief'];
    }
    $course_id = $_GET['course_id'];
    $this->wrapper->url = '';
    $date = date_timestamp_get(date_create());
    $date_time =  date('h_i_s_d_m_Y',$date);
    $session = JFactory::getSession();
    $device = $session->get('device');
    $document = JFactory::getDocument();
    // ========================Get list Assignment=============================
    $this->modss = array();
    $this->mods = JoomdleHelperContent::call_method ( 'get_course_mods_visible', (int) $course_id, '',0, 'assign');
    $this->modds = JoomdleHelperContent::call_method ( 'get_course_mods_visible', (int) $course_id, '',1, 'assign');
    $a = JoomdleHelperContent::call_method ( 'get_course_mods', (int) $course_id, '', 'quiz');
    $t = count($this->mods);
    for($i = 0; $i < $t ; $i++){
        $this->mod_dt[$i] = $this->mods[$i]['mods'];
        for($j = 0; $j < count($this->mod_dt[$i]) ; $j++){
            $this->modss[] = $this->mod_dt[$i][$j];
        }
    }
    $h = count($this->modds);
    for($u = 0; $u < $h ; $u++){
        $this->mod_dt[$u] = $this->modds[$u]['mods'];
        for($o = 0; $o < count($this->mod_dt[$u]) ; $o++){
            $this->modss[] = $this->mod_dt[$u][$o];
        }
    }
    $c = count($this->modss);
    $this->modss = array_reverse($this->modss);
    $this->modss = array_reverse($this->modss, true);




    // =========================Delete assignment==============================
    if(isset($_POST['action_del'])){
        JoomdleHelperContent::call_method ( 'delete_module', (int) $_POST['id_module'] , (int) $course_id );
        echo json_encode('delete success module');die;
    }

    // =========================Delete assignment==============================




    // =========================Eo biet==============================
    if (!empty($_FILES['assignment_file']['type'])) {
    $mainframe = JFactory::getApplication();
    require_once(JPATH_ROOT.'/libraries/joomla/filesystem/folder.php');

    if (!JFolder::exists( $mainframe->getCfg('dataroot') . '/files')) {
        JFolder::create( $mainframe->getCfg('dataroot') . '/files', 0755);
    }
    if (!JFolder::exists( $mainframe->getCfg('dataroot') . '/files/course')) {
        JFolder::create( $mainframe->getCfg('dataroot') . '/files/course', 0755);
    }
    if (!JFolder::exists( $mainframe->getCfg('dataroot') . '/files/course/' .$course_id)) {
        JFolder::create( $mainframe->getCfg('dataroot') . '/files/course/' .$course_id, 0755);
        JFile::copy( $mainframe->getCfg('dataroot') . '/components/com_community/index.html', $mainframe->getCfg('dataroot').  '/files/course/' .$course_id . '/index.html');
    }
    if (!JFolder::exists( $mainframe->getCfg('dataroot') . '/files/course/' .$course_id.'/assign')){
         JFolder::create( $mainframe->getCfg('dataroot') . '/files/course/' .$course_id.'/assign', 0755);
    }
    $name_file_assignment = $_FILES['assignment_file']['name'];
    $name_file_assignment = str_replace(' ', '_', $name_file_assignment);
    $name_file_assignment = str_replace('-', '', $name_file_assignment);
    $name_file_assignment = str_replace('(', '', $name_file_assignment);
    $name_file_assignment = str_replace(')', '', $name_file_assignment);
    $new_name = preg_replace('/\.(?=.*\.)/', '', $name_file_assignment);
    $new_name = VNtoEN($new_name);
    move_uploaded_file($_FILES['assignment_file']['tmp_name'],$mainframe->getCfg('dataroot') . '/files/course/' .$course_id.'/assign/'.$date_time.'_'.$new_name);
    echo json_encode($date_time.'_'.$new_name);die;
}
function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    return $x;
  }
  else
  {
    $y=substr($x,0,$length) . ' ...';
    return $y;
  }
}
    // =========================Eo biet==============================

?>
<!-- datepicker and jquery-confirm-->
<link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/css/jquery-confirm.css';?>">
<link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/css/bootstrap-datepicker.css';?>">
<script type="text/javascript" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/js/bootstrap-datepicker.js';?>"></script>
<script type="text/javascript" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/js/jquery-confirm.js';?>"></script>
<script type="text/javascript" src="<?php echo JURI::base();?>/media/editors/tinymce/tinymce.min.js"></script>
<!-- end datepicker and jquery-confirm -->
<link rel="stylesheet" href="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/css/assign.css';?>">
<?php require_once(JPATH_SITE.'/components/com_joomdle/views/header.php'); ?>
 <script type="text/javascript" src="<?php echo JURI::base();?>/media/editors/tinymce/tinymce.min.js"></script>
<div class="content-assign">
<!-- title Assignment -->



<!-- title Assignment -->



<?php if($list){ 
    $document->setTitle('Assignment List');
?>
<div class="create-assignment">
    <center class="title" data="<?php echo $c; ?>"> <?php echo Jtext::_('ASSIGN_CREATE'); ?><span class="classs" style="font-weight:normal !important;"> <?php echo ($c > 0 ) ? '('.$c.')' :'(0)';  ?></span></center>
</div>
<!-- Assignment page -->
<div class="assignment-page">
<div class="attribute-assignment">
    <?php if($c>0){ for($i = 0; $i < $c ; $i++) { ?>
    <div class="item-attr" id="mod-<?php echo $this->modss[$i]['id'];?>">
        <img src="/media/joomdle/images/AssessmentIcon.png" style="width:30px;height:30px;float:left"/>
            <span>
            <?php if($device == 'mobile') { ?>
                <a href="<?php echo JURI::base().'index.php?option=com_joomdle&view=wrapper&moodle_page_type=assign&id='.$this->modss[$i]['id'].'&course_id='.$course_id.'&Itemid='; ?>">
                    <?php echo custom_echo($this->modss[$i]['name'],20);?>
                </a>
            <?php }else{ ?>
                <a href="<?php echo JURI::base().'index.php?option=com_joomdle&view=wrapper&moodle_page_type=assign&id='.$this->modss[$i]['id'].'&course_id='.$course_id.'&Itemid='; ?>">
                    <?php echo $this->modss[$i]['name'];?>
                </a>
            <?php } ?>
            </span>
        <i class="glyphicon glyphicon-menu-down edit-delete" style="cursor:pointer;">
        <div id="edit-delete">
            <a class="assign-edit" data="<?php echo $this->modss[$i]['id'];?>"><i><img style="margin-top:-7px;" width="13" height="13" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/edit.png';?>"> </i><?php echo Jtext::_('ASSIGN_CREATE_EDIT_ASSIGN'); ?></a>
            <div style="border-bottom:1px solid #ccc;"></div>
            <a class="del-assign" data="<?php echo $this->modss[$i]['id'];?>"><i><img style="margin-top:-7px;" width="13" height="13" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/Delete.png';?>"> </i>  <?php echo " ".Jtext::_('ASSIGN_CREATE_DELETE_ASSIGN'); ?> </a>
        </div>
        </i>
        
    </div>
    <div style="clear:both;"></div>
    <?php }}else{
        echo "<center>You have no Assignment yet.</center>";
    } ?>
</div>
<div class="btn-brief" style="display:none;">
    <a href="<?php echo JURI::base().'index.php?option=com_joomdle&view=certificatecreate';?>"><span class="next">Add Certificate</span></a>
</div> 
</div> 
<script>
    setInterval(function(){
        var t = jQuery('#iframe-assign').contents().find(".breadcrumb-button form input[type='submit']").val();
        if(t == 'Turn editing on'){
            jQuery('#iframe-assign').contents().find(".breadcrumb-button form input[type='submit']").click();
        }
    }, 2000);
</script>
<!-- End Assignment page ***-->


<!-- Brief-item of assignment -->
<?php }else{ 
    $document->setTitle('Add Assignment');
    $this->wrapper->url = $params->get( 'MOODLE_URL' ).'/course/modedit.php?add=assign&type=&course='.$course_id.'&section=0&return=0&sr=0'; // url iframe
?>
<div class="create-assignment">
    <center class="title" data="<?php echo $c; ?>"> <?php echo Jtext::_('ASSIGN_CREATE'); ?></center>
</div>
<div class="tit-des">
<div class="title-assign">
    <span>Title</span><br>
    <h5 style="color:red;display:none;"><?php echo Jtext::_('ASSIGN_CREATE_NOT_EMPTY'); ?></h5><br>
    <input type="text" placeholder="Add Title" class="tit-assign" require>
</div>
<div class="des">
    <span>Introduction</span><br>
    <h5 style="color:red;display:none;"><?php echo Jtext::_('ASSIGN_CREATE_NOT_EMPTY'); ?></h5><br>
    <textarea class="des-assign" placeholder="Add Introduction"></textarea>
</div>
<!--345364756868978073534534534534534534534534534543-->
<!--345364756868978073534534534534534534534534534543-->
<!--345364756868978073534534534534534534534534534543-->
<!--345364756868978073534534534534534534534534534543-->
<div class="col-md-7 col-sm-12 col-xs-12 time-quiz">
    <div class=" col-md-4 col-sm-5 col-xs-8 duadate">
        <span>Due Date</span>
        <div class="input-group date ">
            <input type="text" class="form-control dua_date" placeholder="dd/mm/yy">
            <div class="input-group-addon addon-calendar">
                <img class="icon-calendar" width="20" height="20" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/calender.png';?>">
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12" style="border-top:1px solid #B8D7E2;"></div>  

<div class="btn-bottom" style="padding-top:15px !important;">
    <div class="file_assignment">
        <div class="file-assignment pull-right" width="100%">
        </div>
        <b class="rm-assignment">x</b>
    </div>
    <a><button class="btnn assignment-brief">Assignment Brief  <img class="ajax-load" style="margin-left:7px;margin-top:-5px;" width="15" height="15" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/Upload.png';?>"></button></a>
</div>
<!--345364756868978073534534534534534534534534534543-->
<!--345364756868978073534534534534534534534534534543-->
<!--345364756868978073534534534534534534534534534543-->
<!--345364756868978073534534534534534534534534534543-->

<div class="btn-bottom">
    <a href="<?php echo JURI::base().'index.php?option=com_joomdle&view=coursecontent&course_id='.$course_id;?>"><span class="cancel"><?php echo Jtext::_('ASSIGN_CREATE_CANCEL'); ?></span></a>
    <a><button class="btnn create_assign">Save</button></a>
    <a><span class="btnn preview-class">Preview</span></a>
</div>
</div>
<form method="post" class="uploadFile" action="" enctype='multipart/form-data' style="display:none;">
   <input name="assignment_file" class="fileassignment" id="toan" type="file" onchange="loadFile(event)">
</form>



<!-- End title-des ***-->


<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<div class="preview-assignment">
<div class="prev" style="clear:both !important;">
    <div class="title-assign">
    <center class="title" ></center>
    </div>  
</div>
    <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2 col-sm-offset-2 img-preview">
        <img width="100%" height="350" style="border-radius:10px;" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/assignment.png';?>"  alt="">
    </div>
    <div class="intro-preview" style="padding-top:30px;clear:both;">
        <span><b>Introduction</b></span><span class="pull-right"><b>Due: <span class="due_date_preview">26/10/2016</span></b></span>
        <div class=";clear:both !important;" ></div>
        <div class="text-intro-preview" style="text-align: justify;">
        </div>
        <div class="content-preview"></div>
        
    </div>
<div class="btn-bottom" style="padding-top:25px !important;">
            <a disable="disable"><button class="btnn" style="background:#9B9D9E !important;cursor: none;">Assignment Brief  <img style="margin-left:7px;margin-top:-5px;" width="15" height="15" src="<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/Download.png';?>"></button></a>
        </div>
        <div class="btn-bottom" style="padding-top:50px !important;">
            <a disable="disable"><span class="cancel prev-preview" style="background:#4F5355 !important; color:white !important;cursor: pointer;">&larr; Prev</span></a>
            <a disable="disable"><button class="btnn" style="background:#9B9D9E !important;cursor: none;">Submit <i style="margin-left:7px;margin-top:-5px;">→</i> </button></a>
        </div>


</div>
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->
<!-- This is Preview -->



<!-- Iframe -->
<div style="clear:both;"></div>
<div class="iframe">
    <iframe 
        id="iframe-assign" 
        class="autoHeight"
        src="<?php echo $this->wrapper->url; ?>"
        width="<?php echo $this->params->get('width', '100%'); ?>"
        scrolling="<?php //echo $this->params->get('scrolling', 'auto'); ?>yes"
        allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"
        
        <?php if (!$this->params->get('autoheight', 1)) { ?>
            height="<?php echo $this->params->get('height', '700'); ?>"
        <?php
        }
        ?>
        align="top" 
        frameborder="0"
        <?php if ($this->params->get('autoheight', 1)) { ?>
            onload='itspower(this, false, true, 20)'
        <?php
        }
        ?>
    ></iframe>
</div>
<!-- end iframe -->

<?php } ?> 


        













</div>
<script type="text/javascript">
function loadFile(event) {
    var file = jQuery('.fileassignment')[0].files[0];
    if(file.type != 'application/msword' && file.type != 'application/pdf' && file.type != 'application/vnd.ms-excel' && file.type != 'application/vnd.oasis.opendocument.spreadsheet' && file.type != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' && file.type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && file.type != 'text/plain' && file.type != 'application/vnd.oasis.opendocument.text' ){
        /*Query.alert({
            title: 'Alert!',
            content: 'The file does not match format.Tip : File is doc,pdf,excel or text.',
            columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
            theme: 'blue',
            title: false,
        });*/
        alert('The file does not match format.Tip : File is doc,pdf,excel or text.');
        jQuery('.file-assignment').html(file.name);
        jQuery('.file_assignment').show();
        jQuery('.assignment-brief').attr('disabled',true);
        jQuery('.assignment-brief').css('background','#9B9D9E');
        jQuery('.assignment-brief').attr('disabled',true);
    }else{
        jQuery('.file-assignment').html(file.name);
        jQuery('.file_assignment').show();
        jQuery('.assignment-brief').attr('disabled',true);
        jQuery('.assignment-brief').css('background','#9B9D9E');
        jQuery('.assignment-brief').attr('disabled',true);
    }
    
};
jQuery('.uploadFile').on('submit', function(event){
    var file = jQuery('.fileassignment')[0].files[0];
    event.stopPropagation();
    event.preventDefault();
    jQuery.ajax({
        url : window.location.href,
        type : 'POST',
        data : new FormData(this),
        contentType: false,       
        cache: false,          
        processData:false,
        beforeSend: function () {
            jQuery('.ajax-load').attr('src','<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/load.gif';?>');
            jQuery('.create_assign').attr('disabled',true);
        },
        complete: function () {
            jQuery('.ajax-load').attr('src','<?php echo JURI::base().'components/com_joomdle/views/assigncreate/tmpl/Upload.png';?>');
            jQuery('.create_assign').attr('disabled',true);
        },
        success : function(res){
            var link = '|_|_|'+res;
            jQuery('.create_assign').attr('disabled',true);
            jQuery('#iframe-assign').contents().find(".feditor #id_introeditor").html(jQuery('.des-assign').val()+link);
            jQuery('#iframe-assign').contents().find('#id_submitbutton2').click();
            setTimeout(function() {
                window.location.href = '<?php echo JUri::base()."index.php?option=com_joomdle&view=assigncreate&course_id=".$course_id."&list=1";?>';
            }, 1000);
        }
    });
});
jQuery(document).ready(function(e){

    <?php if($device == 'mobile') { ?>
        var old_src = jQuery('.navbar-nav li[data-id="372"] a img').attr('src');
        var new_src = old_src.replace('_555555', '');
        jQuery('.navbar-nav li[data-id="372"] a img').attr('src',new_src);
        jQuery('.navbar-nav li[data-id="372"]').addClass('current active');
      <?php } else { ?>
        jQuery('.navbar-nav').find('li[data-id="373"]').addClass('current active');
      <?php  } ?>

    //==============================save===============================
    
    jQuery('.assignment-brief').click(function(){
        jQuery('.fileassignment').click();
    });
    jQuery('.rm-assignment').click(function(){
        jQuery(this).parent().hide();
        jQuery('.assignment-brief').removeAttr('disabled');
        jQuery('.assignment-brief').css('background','#4F5355');
        jQuery('.fileassignment').val('');
    });
    jQuery('.create_assign').click(function(){
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = (day<10 ? '0' : '') + day +'/'+ (month<10 ? '0' : '') + month + '/' +d.getFullYear();
        var cont = '';
        var j = 0;
        var q = jQuery('.des-assign').val();
        cont += q + "<br>";
        var dua_date = jQuery('.dua_date').val().split('/');
        var publish_date = output.split('/');
        if(jQuery('.tit-assign').val() == '' || jQuery('.des-assign').val() == '' ){
            alert("Title assignment or Introduction assignment empty.");
        }else if(dua_date == ''){
            alert('Date not be empty.');
        }else if( parseInt(dua_date[2]) < parseInt(publish_date[2]) || parseInt(dua_date[2]) == parseInt(publish_date[2]) && parseInt(dua_date[1]) < parseInt(publish_date[1]) || parseInt(dua_date[2]) == parseInt(publish_date[2]) && parseInt(dua_date[1]) == parseInt(publish_date[1]) && parseInt(dua_date[0]) < parseInt(publish_date[0])){
            alert('Due date not allowed before today');
        }else{
            if(jQuery('.fileassignment').val()==''){
                jQuery('.create_assign').attr('disabled',true);
                jQuery('#iframe-assign').contents().find(".feditor #id_introeditor").html(jQuery('.des-assign').val()+'|_|_|');
                jQuery('#iframe-assign').contents().find('#id_submitbutton2').click();
                setTimeout(function() {
                    window.location.href = '<?php echo JUri::base()."index.php?option=com_joomdle&view=assigncreate&course_id=".$course_id."&list=1";?>';
                }, 1000);
            }else{
                var file = jQuery('.fileassignment')[0].files[0];
                if(file.type != 'application/msword' && file.type != 'application/pdf' && file.type != 'application/vnd.ms-excel' && file.type != 'application/vnd.oasis.opendocument.spreadsheet' && file.type != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' && file.type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && file.type != 'text/plain' && file.type != 'application/vnd.oasis.opendocument.text'  ){
                    alert('The file does not match format.Tip : File is doc,pdf,excel or text.');
                }else{
                    jQuery('.uploadFile').submit();
                }
            }
        }
    });
    jQuery('.prev-preview').click(function(){
        jQuery('#title-mainnav').html('Add Assignment');
        jQuery('.tit-des').show();
        jQuery('.create-assignment').show();
        jQuery('.preview-assignment').attr('style','display:none !important');
    });
    jQuery('.preview-class').click(function(){
        jQuery('#title-mainnav').html('Preview');
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = (day<10 ? '0' : '') + day +'/'+ (month<10 ? '0' : '') + month + '/' +d.getFullYear();
        var cont = '';
        var j = 0;
        var q = jQuery('.des-assign').val();
        cont += q + "<br>";
        var dua_date = jQuery('.dua_date').val().split('/');
        var publish_date = output.split('/');
        if(jQuery('.tit-assign').val() == '' || jQuery('.des-assign').val() == '' ){
            alert('Title assignment or Introduction assignment empty.');
        }else if(dua_date == ''){
            alert('Date not be empty.');
        }else if( parseInt(dua_date[2]) < parseInt(publish_date[2]) || parseInt(dua_date[2]) == parseInt(publish_date[2]) && parseInt(dua_date[1]) < parseInt(publish_date[1]) || parseInt(dua_date[2]) == parseInt(publish_date[2]) && parseInt(dua_date[1]) == parseInt(publish_date[1]) && parseInt(dua_date[0]) < parseInt(publish_date[0])){
            alert('Due date not allowed before today');
        }else{
            jQuery('.prev .title-assign .title').html(jQuery('.tit-assign').val());
            jQuery('.text-intro-preview').html(jQuery('.des-assign').val());
            jQuery('.due_date_preview').html(jQuery('.dua_date').val());
            jQuery('.tit-des').hide();
            jQuery('.create-assignment').hide();
            jQuery('.preview-assignment').attr('style','display:block !important');

        }
    });
    jQuery('.assign-edit').click(function(){
        var x = jQuery(this).attr('data');
        window.location.href = '<?php echo JURI::base().'index.php?option=com_joomdle&view=assigncreate&course_id='.$course_id.'&module_id='; ?>'+x+'&action=edit';
    });
    
    
    jQuery('.edit-delete').hover(function(){
        jQuery('.edit-delete #edit-delete').hide();
        jQuery(this).find('#edit-delete').show();
    },function(){
        jQuery(this).find('#edit-delete').hide();
    });
    //==================Time assign====================
    jQuery('.dua_date').click(function(){
        setInterval(function(){
                var y = jQuery('.dua_date').val();
                var str_dua = y.split("/");
                jQuery('#iframe-assign').contents().find("#id_duedate_day option[value='"+parseInt(str_dua[0])+"']").attr('selected','selected');
                jQuery('#iframe-assign').contents().find("#id_duedate_month option[value='"+parseInt(str_dua[1])+"']").attr('selected','selected');
                jQuery('#iframe-assign').contents().find("#id_duedate_year option[value='"+parseInt(str_dua[2])+"']").attr('selected','selected');
            }, 1000);
    });
    //==================End Time assign====================
    jQuery(".dua_date").datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight: true,
        autoclose: true,
    });
    //====================Lay title====================
    jQuery('.tit-assign').click(function(){
        setInterval(function(){
                
                var t = jQuery('.tit-assign').val();
                if(t==''){
                    jQuery('.title-assign h5').show();
                }else{
                    jQuery('.title-assign h5').hide();
                }
                
                jQuery('#iframe-assign').contents().find(".ftext input[name='name']").val(t);
        }, 1000);
        jQuery('#iframe-assign').contents().find('#id_submissiondrafts option[value="0"]').removeAttr('selected');
        jQuery('#iframe-assign').contents().find('#id_requiresubmissionstatement option[value="0"]').removeAttr('selected');
        jQuery('#iframe-assign').contents().find('#id_submissiondrafts option[value="1"]').attr('selected','selected');
        jQuery('#iframe-assign').contents().find('#id_requiresubmissionstatement option[value="1"]').attr('selected','selected');
    });
    //====================end title====================
    jQuery('.del-assign').click(function(){
        var x = jQuery(this).attr('data');
       
        jQuery.ajax({
            url : window.location.href,
            type : 'POST',
            data : {
                id_module : x,
                action_del : 'yes'
            },
            success : function (res){
                jQuery('#mod-'+x).remove();
                var n = jQuery('.title').attr('data');
                n--;
                jQuery('.title').attr('data',n);
                jQuery('.classs').html(' ('+n+')');
                if(n==0){
                    jQuery('.attribute-assignment').html("<center>You have no Assignment yet.</center>");
                    jQuery('.title').html('Assignment <span class="classs" style="font-weight:normal !important;"> (0)</span>');
                }
            }
        });
        
    });
});
jQuery('.t3-mainbody').click(function(e) {
        jQuery("#edit-delete").hide();
});

jQuery(".icon-calendar").click(function(){
    jQuery(".dua_date").datepicker( "show" );
});
</script>
<?php 
    function VNtoEN($str) {
// In thường
     $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
     $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
     $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
     $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
     $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
     $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
     $str = preg_replace("/(đ)/", 'd', $str);    
// In đậm
     $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
     $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
     $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
     $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
     $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
     $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
     $str = preg_replace("/(Đ)/", 'D', $str);
     return $str; // Trả về chuỗi đã chuyển
}  
?>