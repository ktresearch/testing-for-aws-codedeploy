<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewAssignCreate extends JViewLegacy {
	function display($tpl = null) {
		global $mainframe;
		$app                = JFactory::getApplication();
		$pathway =$app->getPathWay();
        $document = JFactory::getDocument();
		$app        = JFactory::getApplication();
		$menus      = $app->getMenu();
		$menu  = $menus->getActive();
		$params = $app->getParams();
		$this->assignRef('params',$params);
		$course_id =  JRequest::getVar( 'course_id' );
		$id_module =  JRequest::getVar( 'module_id' );
		$action =  JRequest::getVar( 'action' );
		if (!$course_id)
			$course_id = $params->get( 'course_id' );
		$course_id = (int) $course_id;

		if (!$course_id)
		{
                    header('Location: /404.html');
                    exit;
//			echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
//			return;
		}
		
		$user = JFactory::getUser();
		$username = $user->username;
		$this->course_info = JoomdleHelperContent::getCourseInfo($course_id, $username);
		$this->is_enroled = $this->course_info['enroled'];
		$this->list =  JRequest::getVar( 'list' );
		if($this->list){
//			$this->mods = JoomdleHelperContent::call_method ( 'get_course_mods', (int) $course_id, '', 'assign');
                        $coursemods = JoomdleHelperContent::call_method ( 'get_course_mods', (int) $course_id, '', 'assign');
                        if ($coursemods['status']) {
                            $this->mods = $coursemods['coursemods'];
                        } else {
                            $this->mods = array();
                        }
			$t = count($this->mods);
			for($i = 0; $i < $t ; $i++){
				$this->mod_dt[$i] = $this->mods[$i]['mods'];
				for($j = 0; $j < count($this->mod_dt[$i]) ; $j++){
					$this->modss[] = $this->mod_dt[$i][$j];
				}
			}
		}

		if($action=='edit' && $id_module){
			$this->assign_info = JoomdleHelperContent::call_method ( 'get_info_assignment',(int)$id_module);
			$this->intros = $this->assign_info['intro'];
			$this->intro = array();
			$r = explode('|_|_|', $this->intros);
			$this->intro[0] = $r[0];
			$this->intro[1] = $r[1];
			$this->intro[1] = str_replace('"', '', $this->intro[1]);
			// echo "<pre>";print_r($this->intro[1]);die;
			$document->setTitle(JText::_('Edit Assignment'));
			$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
	        parent::display('edit');
	        return;
		}else{
			$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
	        parent::display($tpl);
	        return;
		}


		// echo "<pre>";
		// print_r($this->modss);
		// die;	
		// // ==========================end============================

       
    }
}
?>
