<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewContent extends JViewLegacy
{
    function display($tpl = null)
    {
        global $mainframe;

        $app = JFactory::getApplication();
        $pathway = $app->getPathWay();
        $menus = $app->getMenu();
        $menu = $menus->getActive();

        $params = $app->getParams();
        $this->assignRef('params', $params);
        $user = JFactory::getUser();
        $username = $user->username;

        $id = JRequest::getVar('course_id', null, 'NEWURLFORM');
        if (!$id) $id = JRequest::getVar('course_id');
        if (!$id) $id = $params->get('course_id');
        if (!$id) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
//            return;
        }
        $id = (int)$id;
        if ($params->get('use_new_performance_method'))
            $this->course_info = JHelperLGT::getCourseInfo(['id' => $id, 'username' => $user->username]);
        else
            $this->course_info = JoomdleHelperContent::getCourseInfo($id, $user->username);
        $this->course_status = $this->course_info['course_status'];
        $this->hasPermission = JFactory::hasPermission($id, $username);
        if (!$this->hasPermission[0]['hasPermission']) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//            return;
        }

        $this->course_id = $id;
        $action = JRequest::getVar('action', null, 'NEWURLFORM');
        if (!$action) $action = JRequest::getVar('action');
        if ($action == 'create' || $action == 'edit' || $action == 'remove') {
            if ($this->course_status == 'pending_approval' || $this->course_status == 'approved') {
                echo JText::_('COM_JOOMDLE_COURSE_APPROVED_OR_PENDING_APPROVAL');
                return;
            }
            if ($this->course_info['self_enrolment']) {
                echo JText::_('COM_JOOMDLE_COURSE_PUBLISHED');
                return;
            }
        }

        $mid = JRequest::getVar('mid', null, 'NEWURLFORM');
        if (!$mid) $mid = JRequest::getVar('mid');
        $this->sectionid = JRequest::getVar('section_id', null, 'NEWURLFORM');

        if ($action == "list") {
            $this->action = "list";
            if (isset($_POST['act']) && $_POST['act'] == 'remove') {
                $act = 3;
                $p = array();
                $p['pid'] = $_POST['pid'];

                $r = JoomdleHelperContent::call_method('create_page_activity', $act, $id, $username, $p, 0);

                $result = array();
                $result['error'] = 0;
                $result['comment'] = 'Everything is fine';
                $result['data'] = $r;
                echo json_encode($result);
                die;
            }

            $coursemods = JoomdleHelperContent::call_method('get_course_mods', (int)$id, '', 'page');
            if ($coursemods['status']) {
                $this->mods = $coursemods['coursemods'];
            } else {
                $this->mods = array();
            }
            $arr = array();
            foreach ($this->mods as $section) {
                if (!empty($section['mods'])) {
                    foreach ($section['mods'] as $mod) {
                        $arr[$mod['id']] = $mod;
                    }
                }
            }
            krsort($arr);
            $this->mods = array(array('mods' => $arr));
        } else if ($action == "edit" || $action == 'create') {
            if ($action == "create") {
                $this->action = "create";
                $session = JFactory::getSession();
                $token = md5($session->getId());
                $this->wrapperurl = JUri::base() . "courses/course/modedit.php?add=page&type=&course=$this->course_id&section=0&return=0&sr=0";
            } else {
                $this->action = "edit";
                $this->pid = $mid;
                $this->sectionid = JRequest::getVar('sectionid', null, 'NEWURLFORM');

                if ($params->get('use_new_performance_method')) {
                    $this->mods = JHelperLGT::getCourseMods(['id' => $id, 'username' => $username, 'mtype' => 'page', 'visible' => 1]);
                } else {
                    $coursemods = JoomdleHelperContent::call_method('get_course_mods', (int)$id, '', 'page');
                    $this->mods = $coursemods['status'] ? $coursemods['coursemods'] : [];
                }

                $this->wrapperurl = JUri::base() . "courses/course/modedit.php?update=" . $mid;
                if ($params->get('use_new_performance_method'))
                    $this->content = JHelperLGT::getModuleInfo('page', $mid, $id);
                else
                    $this->content = JoomdleHelperContent::call_method('create_page_activity', 0, $id, $username, array('pid' => $mid), 0);
            }

            if (isset($_POST['act']) && ($_POST['act'] == 'create' || $_POST['act'] == 'update')) {
                if ($_POST['act'] == 'update') {
                    $act = 2;
                } else if ($_POST['act'] == 'create') {
                    $act = 1;
                }
                $p = array(
                    'name' => strip_tags($_POST['name']),
                    'content' => json_encode(($_POST['content'])),
                    'contentArr' => json_encode($_POST['contentArr'])
                );

                if ($_POST['act'] == 'update') $p['pid'] = (int)$mid;
                if (isset($_POST['sectionid'])) $sectionid = (int)$_POST['sectionid'];

                $r = JoomdleHelperContent::call_method('create_page_activity', $act, $id, $username, $p, $sectionid);

                $result = array();
                $result['error'] = $r['error'];
                $result['comment'] = $r['mes'];
                $result['data'] = $r['data'];
                echo json_encode($result);
                die;
            } else if (isset($_POST['act']) && $_POST['act'] == 'addFilevideo') {
                $url = $_POST['url'];
                $result = array();
                $result['error'] = 0;
                $result['comment'] = '';
                $headers = @get_headers($url);
                $url_video = @file_get_contents('https://www.youtube.com/oembed?format=json&url=' . $url);

                $a = strpos($url, 'youtube.com');
                $b = strpos($url, 'dailymotion.com');
                $c = strpos($url, 'dai.ly');
                $d = strpos($url, 'vimeo.com');
                if (strpos($url, 'www.youtube.com') > 0) {

                    if ($url_video) {
                        $result['error'] = 0;
                    } else {
                        $result['error'] = 1;
                        $result['comment'] = "video 545 error";
                    }
                } else {

                    if (strpos($headers[0], 'OK') > 0) {
                        $result['error'] = 0;
                        $result['url'] = $url;
                    } else {
                        $result['error'] = 1;

                        $result['comment'] = "video error";
                    }
                }
                $result['test_a'] = $a;
                $result['test_b'] = $b;
                $result['test_c'] = $c;
                $result['test_d'] = $d;
                $result['value'] = $url;
                $result['url'] = $headers;
                echo json_encode($result);
                die;
            } else if (isset($_POST['act']) && $_POST['act'] == 'deleteFile') {
                $act = 5;
                $p = array();
                $p['itemid'] = (int)$_POST['itemid'];
                $p['fname'] = $_POST['filename'];

                $r = JoomdleHelperContent::call_method('create_page_activity', $act, $id, $username, $p, 0);

                $result = array();
                $result['error'] = $r['error'];
                $result['comment'] = $r['mes'];
                $result['data'] = $p;
                echo json_encode($result);
                die;

            } else if (!empty($_FILES)) {
                $class = $_POST["class"];
                if ($class == "photo" || isset($_FILES['webcam']))
                    $imgEx = array('png', 'jpg', 'jpeg', 'gif', 'bmp', 'tiff');
                else if ($class == "video" || isset($_FILES['video-blob']) )
                    $imgEx = array('avi', 'wmv', 'mov', 'mp4', 'flv', 'webm');
                else
                    $imgEx = array('application/x-ms-dos-executable', 'application/x-msdownload', 'application/x-msi', 'application/octet-stream', 'application/x-java-archive', 'application/javascript');
                $file_tmp = $_FILES[0]['tmp_name'];
                if (isset($_FILES['webcam']['tmp_name'])) {
                    $file_tmp = $_FILES['webcam']['tmp_name'];
                }
                if (isset($_POST['video-filename'])) {
                    $file_tmp = $_FILES['video-blob']['tmp_name'];
                }
                $file_name = VNtoEN($_FILES[0]['name']);
                $namef = $_FILES[0]['name'];
                if (isset($_FILES['webcam']['name'])) {
                    $file_name = $_FILES['webcam']['name'];
                }
                if (isset($_POST['video-filename'])) {
                    $file_name = $_POST['video-filename'];
                }
                $ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));

                if ($_FILES[0]['size'] > 100 * 1024 * 1024) {
                    $result['error'] = 1;
                    $result['comment'] = 'File size is too large (< 100mb)';
                } else if (strlen($_FILES[0]['name']) > 250) {
                    $result['error'] = 1;
                    $result['comment'] = 'File name is too long (< 250char)';
                } else if ($class != "attach" && !in_array($ext, $imgEx)) {
                    $result['error'] = 1;
                    $result['comment'] = 'Invalid file type ('.$ext.'). Please upload file.';
                    $result['files'] = $_FILES;
                    $result['post'] = $_POST;
                    $result['ext'] = $ext;
                    $result['file_name'] = $file_name;
                    $result['namef'] = $namef;
                } else if ($class == 'attach' && in_array($_FILES[0]['type'], $imgEx)) {
                    $result['error'] = 1;
                    $result['comment'] = 'Type of this file is invalid. '.$_FILES[0]['type'];
                } else {
                    // check folder exits
                    $dir = dirname($app->getCfg('dataroot'));
                    if (!is_dir($dir . '/temp')) {
                        mkdir($dir . '/temp', 0755);
                    }
                    if (!is_dir($dir . '/temp/content')) {
                        mkdir($dir . '/temp/content', 0755);
                    }
                    if (!is_dir($dir . '/temp/content/' . $id)) {
                        mkdir($dir . '/temp/content/' . $id, 0755);
                    }
                    if (!is_dir($dir . '/temp/content/' . $id . '/0')) {
                        mkdir($dir . '/temp/content/' . $id . '/0', 0755);
                    }
                    $tmp_file = dirname($app->getCfg('dataroot')) . '/temp/content/' . $id . '/0/' . VNtoEN($file_name);
                    file_put_contents($tmp_file, file_get_contents($file_tmp));

                    $r = JoomdleHelperContent::call_method('create_page_activity', 4, $id, $username, array('fname' => $file_name, 'itemid' => 0), 0);

                    $result = array();
                    $result['error'] = $r['error'];
                    $result['comment'] = $r['mes'];
                    $result['class'] = $class;
                    $result['namef'] = $namef;
                    $result['ext'] = $ext;
                    $fileData = json_decode($r['data']);
                    $result['data'][0]['url'] = $fileData->filepath . $fileData->filename;
                    $result['data'][0]['name'] = $fileData->filename;
                    $result['data'][0]['itemid'] = $fileData->itemid;
                    $result['data'][0]['type'] = $_FILES[0]['type'];
                }
                echo json_encode($result);
                die;
            }
        } else if ($action == "remove") {
            $this->action = "remove";
            $this->pid = $mid;
            $this->wrapperurl = JUri::base() . 'courses/course/mod.php?sr=0&delete=' . $mid;
            $this->content = JoomdleHelperContent::call_method('create_page_activity', 0, $id, $username, array('pid' => $mid), 0);

            if (isset($_POST['act']) && $_POST['act'] == 'remove') {
                $act = 3;
                $p = array();
                $p['pid'] = $mid;

                $r = JoomdleHelperContent::call_method('create_page_activity', $act, $id, $username, $p, 0);

                $result = array();
                $result['error'] = 0;
                $result['comment'] = 'Everything is fine';
                $result['data'] = $r;
                echo json_encode($result);
                die;
            }
        }
        $document = JFactory::getDocument();
        $document->setTitle('Page');
        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
        parent::display($tpl);
    }
}

function VNtoEN($str)
{
// In thường
    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
    $str = preg_replace("/(đ)/", 'd', $str);
    $str = preg_replace("/( )/", '_', $str);
// In đậm
    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
    $str = preg_replace("/(Đ)/", 'D', $str);
    $str = preg_replace("/( )/", '_', $str);
    return $str; // Trả về chuỗi đã chuyển
}

?>
