<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php
$itemid = JoomdleHelperContent::getMenuItem();

$data = array();
$session = JFactory::getSession();
$token = md5($session->getId());
if ($this->action == 'edit') $edit = true; else $edit = false;

$linkstarget = $this->params->get('linkstarget');
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

if ($session->get('device') == 'mobile') {
    ?>
    <style type="text/css">
        .navbar-header {
            background-color: #126db6;
        }
    </style>
    <?php
}
require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');

$document = JFactory::getDocument();
$document->addStyleSheet(JURI::root(true) . '/components/com_joomdle/views/content/tmpl/pageActivity.css');
$banner = new HeaderBanner(new JoomdleBanner);

$render_banner = $banner->renderBanner($this->course_info, $this->mods['sections'], $role);
?>
<?php if ($this->hasPermission[0]['hasPermission']) : ?>
    <script type="text/javascript" src="<?php echo JURI::base(); ?>/media/editors/tinymce/tinymce.min.js"></script>
    <script type="text/javascript"
            src="<?php echo JURI::base(); ?>components/com_joomdle/views/content/js/webcam.js"></script>
    <script type="text/javascript"
            src="<?php echo JURI::base(); ?>components/com_joomdle/views/content/js/RecordRTC.js"></script>
    <script type="text/javascript"
            src="<?php echo JURI::base(); ?>components/com_joomdle/views/content/js/gif-recorder.js"></script>
    <script type="text/javascript"
            src="<?php echo JURI::base(); ?>components/com_joomdle/views/content/js/getScreenId.js"></script>
    <script type="text/javascript"
            src="<?php echo JURI::base(); ?>components/com_joomdle/views/content/js/gumadapter.js"></script>
    <?php if (isset($this->action) && ($this->action == 'create' || $this->action == 'edit')) { ?>
        <div class="joomdle-page-content <?php echo $this->pageclass_sfx ?>">
            <!-- <h1><?php echo JText::_('COM_JOOMDLE_CONTENT'); ?></h1> -->
            <form action="" method="post" class="formAddContent" enctype='multipart/form-data'>
                <div class="title">
                    <label id ="title_lable" for="">Activity Title</label>
                    <span style="color: #bb5a66;">*</span>
                    <input type="text" name="txtTitle" class="txtTitle"

                           value="<?php echo ($edit) ? htmlentities(ucwords($this->content['name'])) : ''; ?>"/>
                    <!-- <label for=""><?php echo JText::_('COM_JOOMDLE_CONTENT'); ?></label> -->
                </div>
                <p class="addResourse"><?php echo JText::_('COM_JOOMDLE_ADD_CONTENT'); ?><span style="color: #bb5a66;">*</span></p>
                <div class="contentTypes">
                    <div class="mainContent" id="mainContent">
                        <?php if ($edit) {
                            $data = json_decode($this->content['params']);
                            $i = 0;
                            foreach ($data as $k => $v) {
                                ?>
                                <?php if ($v->type == 'text') { ?>
                                    <div class="textareaContent moodleContent" data-tinymceid="<?php echo $i; ?>"
                                         id="<?php echo 'txtaContent' . $i; ?>">
                                        <div class="rm"><img src="../images/delete.png"></div>
                                        <textarea id="txtaContent<?php echo $i; ?>"
                                                  class="txtaContent"><?php echo $v->content; ?></textarea></div>
                                    <div class="cline"></div>
                                    <?php $i++;
                                } ?>
                                <?php if ($v->type == 'image') {
                                    if (!isset($v->itemid)) $v->itemid = '';
                                    if (!isset($v->fname)) $v->fname = ''; ?>
                                    <div class="moodleContent imageContent" data-itemid="<?php echo $v->itemid; ?>"
                                         data-filename="<?php echo $v->fname; ?>">
                                        <div class="rm"><img src="../images/delete.png"></div><?php echo $v->content; ?>
                                    </div>
                                    <div class="cline"></div>
                                <?php } ?>
                                <?php if ($v->type == 'video') {
                                    if (!isset($v->itemid)) $v->itemid = '';
                                    if (!isset($v->fname)) $v->fname = ''; ?>
                                    <div class="moodleContent videoContent withCaption"
                                         data-itemid="<?php echo $v->itemid; ?>" data-filename="<?php echo $v->fname; ?>">
                                        <div class="rm"><img src="../images/delete.png"></div><?php echo $v->content; ?>
                                    </div>
                                    <!-- <div class="cline"></div> -->
                                <?php } ?>
                                <?php if ($v->type == 'linkVideo') { ?>
                                    <div class="moodleContent linkVideo withCaption">
                                        <div class="rm"><img src="../images/delete.png"></div><?php echo $v->content; ?>
                                    </div>
                                    <!-- <div class="cline"></div> -->
                                <?php } ?>
                                <?php if ($v->type == 'caption') { ?>
                                    <div class="moodleContent textareaContent captionContent"
                                         data-tinymceid="<?php echo 'a' . $i; ?>"><textarea
                                                id="txtaContent<?php echo 'a' . $i; ?>"
                                                class="txtaContent"><?php echo $v->content; ?></textarea></div>
                                    <div class="cline"></div>
                                    <?php $i++;
                                } ?>
                                <?php if ($v->type == 'embedVideo') { ?>
                                    <div class="moodleContent embedVideo withCaption">
                                        <div class="rm"><img src="../images/delete.png"></div><?php echo $v->content; ?>
                                    </div>
                                    <!-- <div class="cline"></div> -->
                                <?php } ?>
                                <?php if ($v->type == 'attach') {
                                    if (!isset($v->itemid)) $v->itemid = '';
                                    if (!isset($v->fname)) $v->fname = ''; ?>
                                    <div class="moodleContent fileContent" data-itemid="<?php echo $v->itemid; ?>"
                                         data-filename="<?php echo $v->fname; ?>">
                                        <div class="rm"><img src="../images/delete.png"></div><?php echo $v->content; ?>
                                    </div>
                                    <div class="cline"></div>
                                <?php } ?>
                                <?php if ($v->type == 'www') { ?>
                                    <div class="moodleContent embedWebsite">
                                        <?php echo $v->content; ?>
                                    </div>
                                <?php } ?>
                                <?php
                            }
                            ?>
                        <?php } ?>
                    </div>
                    <div class="message_safari hidden">
                        <p><?php echo JText::_('COM_JOOMDLE_NOTE_MESSAGE_WEBSITE'); ?></p>
                        <p><?php echo JText::_('COM_JOOMDLE_SETTING_MESSAGE_WEBSITE'); ?></p>
                    </div>


                    <div class="mntypeicon">

                        <?php if (!$edit && empty($this->content['params']) && empty($this->content['content'])) { ?>
                            <CENTER style="color: #5e5e5e;font-weight: normal;"
                                    class="add_content"><?php echo JText::_('COM_JOOMDLE_ADD_RESOURSE'); ?></CENTER>
                        <?php } else echo ' <div class = "add_line"></div>';
                        ?>
                        <div class="mntypeicon">
                            <div class="contentType text icon-text" align="center" valign="center"><img
                                        src="../images/Texti.png"></div>
                            <div class="contentType image icon-img" align="center" valign="center"><img
                                        src="../images/Imagei.png"></div>
                            <div class="contentType video con-video" align="center" valign="center"><img
                                        src="../images/Videoi.png"></div>
                            <div class="contentType link icon-link" align="center" valign="center"><img
                                        src="../images/Linki.png"></div>
                            <div class="contentType attach icon-file" align="center" valign="center"><img
                                        src="../images/Attachi.png"></div>
                            <div class="contentType www icon-file" align="center" valign="center"><img src="../images/wwwi.png">
                            </div>
                        </div>

                        <div class="clear"></div>
                        <div class="buttons changePosition">
                            <button class="btSave pull-right"><?php echo JText::_('COM_JOOMDLE_SAVE'); ?></button>
                            <button class="btPreview pull-right" type="button"><?php echo JText::_('COM_JOOMDLE_PREVIEW'); ?></button>
                            <button class="btCancel pull-right" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </form>
        </div>
        <div class="divUploadFile changePosition" style="display: none">
            <form action="" method="post" class="ajaxUploadFile" enctype='multipart/form-data'>
                <input type="file" class="fileUpload" accept="image/*;" capture name="fileUpload"
                       style="display: none"/>
            </form>
            <ul>
                <li class="liPhoto" id="take-photo"> <image src = "../images/icons/camera_hd.png"><?php echo JText::_('COM_JOOMDLE_TAKE_PHOTO'); ?></li>
                <li class="liPhoto"
                    id="upload-photo"><image src = "../images/icons/image_hd.png"><?php echo JText::_('COM_JOOMDLE_UPLOAD_FROM_PHOTO_LIBRARY'); ?></li>
                <li class="liVideo takeVideo"><image src = "../images/icons/camera_hd.png"><?php echo JText::_('COM_JOOMDLE_TAKE_VIDEO'); ?></li>
                <li class="liVideo uploadFrom"><image src = "../images/icons/image_hd.png"><?php echo JText::_('COM_JOOMDLE_UPLOAD_FROM_VIDEO_LIBRARY'); ?></li>
                <li class="liVideo linkVideo"><image src = "../images/icons/link_hd.png"><?php echo JText::_('COM_JOOMDLE_LINK_VIDEO'); ?></li>
                <li class="liVideo embed"><image src = "../images/icons/embed_Hd.png"><?php echo JText::_('COM_JOOMDLE_EMBED_VIDEO_CODE'); ?></li>
            </ul>
            <button class="cancel" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
        </div>
        <div class="websitelinkPopup changePosition">
            <p class="textpopup"><?php echo JText::_('COM_JOOMDLE_WEBSITE'); ?></p>
            <input type="text" name="txtWebsiteLink" class="txtWebsiteLink"
            />
            <p><?php echo JText::_('COM_JOOMDLE_PASTE_YOUR_URL_HERE'); ?></p>
            <button class="btAddLinkWeb" type="button"><?php echo JText::_('COM_JOOMDLE_SAVE'); ?></button>
            <button class="mnCancelPage" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
        </div>

        <div class="hyperlinkPopup changePosition">
            <p class="textpopup"><?php echo JText::_('COM_JOOMDLE_HYPERLINK'); ?></p>
            <input type="text" name="txtHyperlink" class="txtHyperlink"
                   placeholder="<?php echo JText::_('COM_JOOMDLE_PASTE_YOUR_URL_HERE'); ?>"/>
            <button class="btAdd" type="button"><?php echo JText::_('COM_JOOMDLE_SAVE'); ?></button>
            <button class="mnCancelPage" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
        </div>
        <div class="hyperlinkPopup1 changePosition">
            <p><?php echo JText::_('COM_JOOMDLE_HYPERLINK'); ?></p>
            <input type="text" name="txtHyperlink1" class="txtHyperlink1"
                   placeholder="<?php echo JText::_('COM_JOOMDLE_PASTE_YOUR_URL_HERE'); ?>"/>
            <button class="btAdd" type="button"><?php echo JText::_('COM_JOOMDLE_SAVE'); ?></button>
            <button class="mnCancelPage" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
        </div>
        <div class="linkVideoPopup changePosition">
            <p class="textpopup"><?php echo JText::_('COM_JOOMDLE_LINK_VIDEO'); ?></p>
            <input type="text" name="txtLinkVideo" class="txtLinkVideo"
                   placeholder="<?php echo JText::_('COM_JOOMDLE_PASTE_YOUR_URL_HERE'); ?>"/>
            <button class="btAdd" type="button"><?php echo JText::_('COM_JOOMDLE_SAVE'); ?></button>
            <button class="mnCancelPage" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>

        </div>
        <div class="embedCodePopup changePosition">
            <p class="textpopup"><?php echo JText::_('COM_JOOMDLE_EMBED_CODE'); ?></p>
            <textarea name="txtaEmbedCode" class="txtaEmbedCode" cols="30" rows="10"
                      placeholder="<?php echo JText::_('COM_JOOMDLE_PASTE_YOUR_CODE_HERE'); ?>"></textarea>
            <button class="btAdd" type="button"><?php echo JText::_('COM_JOOMDLE_SAVE'); ?></button>
            <button class="mnCancelPage" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
        </div>
        <div class="rmPopup changePosition hienPopup">
            <p><?php echo JText::_('COM_JOOMDLE_REMOVE_CONTENT_TEXT'); ?></p>
            <button class="btRMCancel" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
            <button class="btRMYes" type="button"><?php echo JText::_('COM_JOOMDLE_BUTTON_REMOVE'); ?></button>
        </div>
        <div class="rmPopupWww changePosition hienPopup">
            <p><?php echo JText::_('COM_JOOMDLE_REMOVE_CONTENT_TEXT'); ?></p>
            <button class="btRMCancelWww" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
            <button class="btRMYesWww" type="button"><?php echo JText::_('COM_JOOMDLE_BUTTON_REMOVE'); ?></button>
        </div>
        <div class="modal" id="mytitle">
            <div class="modal-content hienPopup">
                <p id="error" class="error"><?php //echo JText::_('COM_JOOMDLE_ERROR_TITLE'); ?></p>
                <button class="closetitle" type="button"><?php echo JText::_('COM_JOOMDLE_CLOSE'); ?></button>
            </div>
        </div>
        <div class="modal_www" id="message_www" style="display: none;">
            <div class="modal-content hienPopup">
                <p id="error_www"><?php echo JText::_('COM_JOOMDLE_CREATE_A_NEW_PAGE_TO_EMBED_WEBSITE'); ?></p>
                <button class="btRMCancelWww" type="button"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                <button  class="btRMYesWww" type="button"><a target="blank" href="<?php echo JUri::base() . 'content/create_' . $this->course_id . '_' . $this->sectionid . '.html'; ?>"><?php echo JText::_('COM_JOOMDLE_CREATE_PAGE'); ?></a></button>
            </div>
        </div>

        <div class="divPreview">
            <p class="previewTitle"></p>
            <p class="previewDescription"></p>
            <button class="previewBack" type="button"><?php echo JText::_('COM_JOOMDLE_EXIT_PREVIEW'); ?></button>
        </div>
        <div class="notification"></div>

        <script type="text/javascript">
            // check device
            if (navigator.mediaDevices && navigator.mediaDevices.enumerateDevices) {
                // Firefox 38+ seems having support of enumerateDevicesx
                navigator.enumerateDevices = function (callback) {
                    navigator.mediaDevices.enumerateDevices().then(callback);
                };
            }
            var MediaDevices = [];
            var isHTTPs = location.protocol === 'https:';
            var canEnumerate = false;

            if (typeof MediaStreamTrack !== 'undefined' && 'getSources' in MediaStreamTrack) {
                canEnumerate = true;
            } else if (navigator.mediaDevices && !!navigator.mediaDevices.enumerateDevices) {
                canEnumerate = true;
            }

            var hasMicrophone = false;
            var hasSpeakers = false;
            var hasWebcam = false;
            var isMicrophoneAlreadyCaptured = false;
            var isWebcamAlreadyCaptured = false;

            function checkDeviceSupport(callback) {
                if (!canEnumerate) {
                    return;
                }

                if (!navigator.enumerateDevices && window.MediaStreamTrack && window.MediaStreamTrack.getSources) {
                    navigator.enumerateDevices = window.MediaStreamTrack.getSources.bind(window.MediaStreamTrack);
                }

                if (!navigator.enumerateDevices && navigator.enumerateDevices) {
                    navigator.enumerateDevices = navigator.enumerateDevices.bind(navigator);
                }

                if (!navigator.enumerateDevices) {
                    if (callback) {
                        callback();
                    }
                    return;
                }

                MediaDevices = [];
                navigator.enumerateDevices(function (devices) {
                    devices.forEach(function (_device) {
                        var device = {};
                        for (var d in _device) {
                            device[d] = _device[d];
                        }

                        if (device.kind === 'audio') {
                            device.kind = 'audioinput';
                        }

                        if (device.kind === 'video') {
                            device.kind = 'videoinput';
                        }

                        var skip;
                        MediaDevices.forEach(function (d) {
                            if (d.id === device.id && d.kind === device.kind) {
                                skip = true;
                            }
                        });

                        if (skip) {
                            return;
                        }

                        if (!device.deviceId) {
                            device.deviceId = device.id;
                        }

                        if (!device.id) {
                            device.id = device.deviceId;
                        }

                        if (!device.label) {
                            device.label = 'Please invoke getUserMedia once.';
                            if (!isHTTPs) {
                                device.label = 'HTTPs is required to get label of this ' + device.kind + ' device.';
                            }
                        } else {
                            if (device.kind === 'videoinput' && !isWebcamAlreadyCaptured) {
                                isWebcamAlreadyCaptured = true;
                            }

                            if (device.kind === 'audioinput' && !isMicrophoneAlreadyCaptured) {
                                isMicrophoneAlreadyCaptured = true;
                            }
                        }

                        if (device.kind === 'audioinput') {
                            hasMicrophone = true;
                        }

                        if (device.kind === 'audiooutput') {
                            hasSpeakers = true;
                        }

                        if (device.kind === 'videoinput') {
                            hasWebcam = true;
                        }

                        // there is no 'videoouput' in the spec.

                        MediaDevices.push(device);
                    });

                    if (callback) {
                        callback();
                    }
                });
            }
            var protocal = document.location.protocol;
            var isMobile = false; //initiate as false
            // device detection
            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
                || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;
        </script>
        <script type="text/javascript">
            jQuery( document ).ready(function() {
                jQuery("video").each(function(){
                    jQuery(this).attr('controlsList','nodownload');
                    jQuery(this).load();
                });
            });
            function downloadFile(filePath) {
                var link = document.createElement('a');
                link.href = filePath;
                link.download = filePath.substr(filePath.lastIndexOf('/') + 1);
                link.click();
            }
            jQuery('.mnCancelPage').click(function(){
                jQuery('body').removeClass('overlay2');
                jQuery(this).parent().hide();

            });
            jQuery('.btRMCancelWww').click(function(){
                jQuery('body').removeClass('overlay2');
                jQuery('.modal_www').hide();
            });
            jQuery('modal_www .btRMCancelWww').click(function(){
                jQuery('body').removeClass('overlay2');
                jQuery('.modal_www').hide();
            });
            (function ($) {
                setTimeout(
                    function () {
                        $('.captionContent iframe').contents().find('body').attr('style', 'background-color: #fff !important')
                        $('.captionContent iframe').attr('style', 'height: 75px !important;width:100% !important');
                        jQuery('.textareaContent iframe').contents().find('body').on('drop', function() {
                            return false;
                        });
                    }, 1000);
                setTimeout(
                    function () {
                        jQuery('.textareaContent iframe').contents().find('a').on('click', function (event) {
                            txtvalue = $(this).attr('href');
                            txtid = $(this).attr('id');
                            $('.hyperlinkPopup1').show();
                            $('.txtHyperlink1').val(txtvalue);
                            $('.txtHyperlink1').attr('name', txtid);
                            $('body').addClass('overlay2');
                        });
                    }, 1000);
                $('.formAddContent').submit(function (e) {
                    e.preventDefault();
                });

                $('.formAddContent .btCancel').click(function () {
                    window.location.href = '<?php echo JUri::base() . "course/" . $this->course_id . ".html"; ?>';
                });

                $('.formAddContent .btPreview').click(function () {
                    $('.divPreview').addClass('visible');
                    if ($('.txtTitle').val().trim() == '') {
                        $('.divPreview .previewTitle').html('No title');
                    } else {
                        $('.divPreview .previewTitle').html($('.txtTitle').val());
                    }

                    var cont = '';
                    var j = 0;
                    $('.moodleContent').each(function () {
                        // Video
                        if ($(this).hasClass('textareaContent') && $(this).hasClass('captionContent')) {
                            cont += tinyMCE.get('txtaContent' + $(this).attr('data-tinymceid')).getContent();
                            cont += '<div class="cline"></div>';
                            j++;
                        } // Text box
                        else if ($(this).hasClass('textareaContent') && $(this).hasClass('captionContent') == false) {
                            cont +='<div class="isContent">';
                            cont += tinyMCE.get('txtaContent' + $(this).attr('data-tinymceid')).getContent();
                            cont += '</div>';
                            cont += '<div class="cline"></div>';
                            j++;
                        } // Website(www)
                        else if ($(this).hasClass('embedWebsite')) {
                            cont += $(this).html().replace('<div class="rmWww"><img src="../images/delete.png"></div>', '');
                            $('.previewDescription').addClass("view_web");
                        }
                        else {
                            cont += $(this).html().replace('<div class="rm"><img src="../images/delete.png"></div>', '');
                            if ($(this).hasClass('withCaption') == false) {
                                cont += '<div class="cline"></div>';
                            }
                        }
                    });

                    $('.divPreview .previewDescription').html(cont);
                    $('.joomdle-page-content').fadeOut();
                });
                $('.divPreview .previewBack').click(function () {
                    $('.divPreview').removeClass('visible');
                    $('.joomdle-page-content').fadeIn();
                });
                var modal = document.getElementById('mytitle');
                var er = document.getElementById('error');
                $('.closetitle').click(function () {
                    modal.style.display = "none";
                });
                window.onclick = function (event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                    }
                }
                $('.formAddContent .btSave').click(function () {

                    if ($('#upload-to-server').length) {
                        $('.formAddContent .btSave').addClass("clicked");
                        $('#upload-to-server').click();
                    } else {
                        if ($('.txtTitle').val().trim() == '') {
                            jQuery('#title_lable').addClass('error1');

                            jQuery('.txtTitle').on('keyup', function() {
                            	if ($(this).val().trim()) {
				                    jQuery('#title_lable').removeClass("error1");
				                    jQuery('.txtTitle').removeClass("error1");
                            	} else {
				                    jQuery('#title_lable').addClass("error1");
			                    	jQuery('.txtTitle').addClass("error1");
                            	}
			                }).val('').addClass('error1');

                            return;
                        }

                        if (!$('.moodleContent').html()){
                            jQuery('.addResourse').addClass('error1');
                            jQuery('.contentTypes').addClass('error1');
                            return;
                        }

                        var cont = '';
                        var contArr = [];
                        var j = 0;
                        var k = 0;
                        $('.moodleContent').each(function () {
                            var tempE = {
                                'no': k
                            };
                            var c = '';
                            if ($(this).hasClass('textareaContent') && (!$(this).hasClass('captionContent'))) {
                                c = tinyMCE.get('txtaContent' + $(this).attr('data-tinymceid')).getContent();
                                console.log(c);
                                cont += c;
                                tempE.type = 'text';
                                tempE.content = c;
                                j++;
                            } else if ($(this).hasClass('imageContent')) {
                                c = $(this).html().replace('<div class="rm"><img src="../images/delete.png"></div>', '');
                                tempE.type = 'image';
                                tempE.content = c;
                                tempE.itemid = $(this).attr('data-itemid');
                                tempE.fname = $(this).attr('data-filename');
                                cont += c;
                            } else if ($(this).hasClass('videoContent')) {
                                c = $(this).html().replace('<div class="rm"><img src="../images/delete.png"></div>', '');
                                tempE.type = 'video';
                                tempE.content = c;
                                tempE.itemid = $(this).attr('data-itemid');
                                tempE.fname = $(this).attr('data-filename');
                                cont += c;
                            } else if ($(this).hasClass('linkVideo')) {
                                c = $(this).html().replace('<div class="rm"><img src="../images/delete.png"></div>', '');
                                tempE.type = 'linkVideo';
                                tempE.content = c;
                                cont += c;
                            } else if ($(this).hasClass('captionContent')) {
                                c = tinyMCE.get('txtaContent' + $(this).attr('data-tinymceid')).getContent();
                                console.log(c);
                                tempE.type = 'caption';
                                tempE.content = c;
                                cont += c;
                                j++;
                            } else if ($(this).hasClass('fileContent')) {
                                c = $(this).html().replace('<div class="rm"><img src="../images/delete.png"></div>', '');
                                tempE.type = 'attach';
                                tempE.content = c;
                                tempE.itemid = $(this).attr('data-itemid');
                                tempE.fname = $(this).attr('data-filename');
                                cont += c;
                            } else if ($(this).hasClass('embedVideo')) {
                                c = $(this).html().replace('<div class="rm"><img src="../images/delete.png"></div>', '');
                                tempE.type = 'embedVideo';
                                tempE.content = c;
                                cont += c;
                            } else if ($(this).hasClass('embedWebsite')) {
                                c = $(this).html().replace('<div class="rm"><img src="../images/delete.png"></div>', '');
                                tempE.type = 'www';
                                tempE.content = c;
                                cont += c;
                            }
                            contArr.push(tempE);
                            k++;
                        });

                        var act = '<?php echo ($edit) ? "update" : "create"; ?>';

                        $.ajax({
                            url: window.location.href,
                            type: 'POST',
                            data: {
                                <?php echo ($edit) ? "pid: " . $this->pid . "," : ""; ?>
                                act: act,
                                name: $('.txtTitle').val(),
                                courseid: <?php echo $this->course_id; ?>,
                                content: cont,
                                contentArr: contArr,
                                sectionid: <?php if ($this->sectionid != Null) {
                                    echo $this->sectionid;
                                } else echo 0;?>
                            },
                            beforeSend: function () {
                                lgtCreatePopup('', {'content': 'Loading...'});
                                // $('.lgt-notif').html('Your page is saving...').addClass('lgt-visible');
                            },
                            success: function (data, textStatus, jqXHR) {
                                var res = JSON.parse(data);
                                // $('.lgt-notif').removeClass('lgt-visible');
                                // $('.lgt-notif').removeClass('lgt-visible');
                                if (res.error == 1) {     
                                    lgtRemovePopup();
                                    $('body').removeClass('overlay2');
                                    $('.linkVideoPopup').fadeOut();
                                    er.innerHTML = res.comment;
                                    modal.style.display = "block";
                                    // $('.lgt-notif-error').html(res.comment).addClass('lgt-visible');
                                } else {
                                    console.log(res);
                                    window.location.href = "<?php echo JUri::base() . "course/" . $this->course_id . ".html"; ?>";
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log('ERRORS: ' + textStatus);
                            }
                        });
                    }
                });

                var i = 0, textareaCount = <?php echo ($this->action == 'edit') ? $i - 1 : '0'; ?>;
                var v = <?php echo ($this->action == 'edit') ? $i - 1 : '0'; ?>;
                tinymce.init({
                    //auto_focus:true,
                    selector: ".txtaContent",
                    preview_styles: "OpenSans",
                    theme: "modern",
                    height: 80,
                    content_style: "body {background-color:#fff !important;color:#828282 !important;font-size: 16px !important;}",
                    body_class: "mceBlackBody",

                    plugins: [
                        "advlist autolink image lists charmap print preview hr anchor pagebreak spellchecker",
                        "link searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table directionality emoticons template paste textcolor"
                    ],
                    toolbar: "fontselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
                    font_formats: 'opensans=OpenSans,Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n'

                });

                $('.contentTypes .text').bind('click', function (e) {
                    textareaCount++;
                    var att = textareaCount - 1;
                    var id_frame = '#txtaContent' + att + '_ifr';
                    var iframeBody = $(id_frame).contents().find('#tinymce');
                    iframeBody.attr('p', true);
                    if (id_frame == '#txtaContent0_ifr' || id_frame == '#txtaContent-1_ifr' || iframeBody.length == 0) {
                        test = 'aaa';
                    }
                    else test = $(iframeBody).text();
                    if (test == '') {
                        textareaCount--;
                        var modal = document.getElementById('mytitle');
                        var er = document.getElementById('error');
                        $('.closetitle').click(function () {
                            modal.style.display = "none";

                        });
                        window.onclick = function (event) {
                            if (event.target == modal) {
                                modal.style.display = "none";
                            }
                        };
                        $('.error').html( 'Add text first.');
                        modal.style.display = "block";

                    }
                    else{
                        i++;
                        $('.mainContent').append('<div class="textareaContent moodleContent" data-tinymceid="' + textareaCount + '" id="txtaContent' + textareaCount + '" ><div class="rm"><img src="../images/delete.png"></div><textarea id="txtaContent' + textareaCount + '" class="txtaContent"><?php //echo JText::_('COM_JOOMDLE_ADD_TEXT'); ?></textarea></div>');
                        if($('.contentTypes').hasClass('error1')){
                            $('.contentTypes').removeClass('error1');
                        }
                        if($('.addResourse').hasClass('error1')){
                            $('.addResourse').removeClass('error1');
                        }
                        var iframe = '#txtaContent' + textareaCount + '_ifr';
                        tinymce.init({
                            //auto_focus:true,
                            selector: ".txtaContent",
                            preview_styles: "OpenSans",
                            theme: "modern",
                            height: 80,
                            content_style: "body {background-color:#fff !important;color:#828282 !important;font-size: 16px !important;}",
                            body_class: "mceBlackBody",
                            init_instance_callback: function (e) {
                                tinymce.activeEditor.focus();
                                $('#'+(this).id).addClass('active');
                                var a = (this).id;
                                window.addEventListener('click', function(e){
                                    if(e.target != document.getElementById(a)) {
                                        $('#'+a).removeClass('active');
                                    }
                                });
                                // e.execCommand('mceAutoResize');
                            },
                            paste_data_images:false,
                            plugins: [
                                "advlist autolink image lists charmap print preview hr anchor pagebreak spellchecker",
                                "link searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                                "save table directionality emoticons template paste textcolor"
                            ],
                            toolbar: "fontselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
                            font_formats: 'opensans=OpenSans,Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n'

                        });
                        setTimeout(
                            function () {
                                jQuery(iframe).bind('load', function () {
                                    e.preventDefault();
                                    e.stopPropagation();
                                    $(iframe).contents().find('body').trigger("focus");
                                });
                                $(iframe).contents().find('body').focus();
                            }, 100);
                        setTimeout(
                            function() {
                                var iframeBody=$('.textareaContent iframe').contents().find('body');
                                iframeBody.attr('p', true);
                                $(iframeBody).on( 'drop',function() {
                                    return false;
                                });
                            }, 100);
                        setTimeout(
                            function() {
                                $(iframe).contents().find('html').click(function(){
                                    $(".textareaContent").removeClass("active");
                                    $(iframe).parent().parent().parent().parent().addClass('active');

                                });
                            }, 100);

                        jQuery('.contentTypes .text').bind('mouseup', function (event) {
                            $(iframe).contents().find('body').show().focus();
                        });
                    }
                    $('.add_content').html('');
                    $('.add_content').append('<div class= "add_line"></div>');
                });
                $('.contentTypes .image').click(function () {
                    $('body').addClass('overlay2');
                    $('.add_content').html('');
                    $('.add_content').append('<div class= "add_line"></div>');
                    $('.linkVideoPopup, .embedCodePopup').hide();
                    $('.divUploadFile').hide().show();
                    $('.divUploadFile > ul').removeClass('photo').removeClass('video').removeClass('attach').addClass('photo');
                });
                $('.contentTypes .video').click(function () {
                    $('body').addClass('overlay2');
                    $('.add_content').html('');
                    $('.add_content').append('<div class= "add_line"></div>');
                    $('.linkVideoPopup, .embedCodePopup').hide();
                    $('.divUploadFile').hide().show();
                    $('.divUploadFile > ul').removeClass('photo').removeClass('attach').removeClass('video').addClass('video');
                });
                $('.contentTypes .attach').click(function() {
                    $('.divUploadFile > ul').removeClass('photo').removeClass('attach').removeClass('video').addClass('attach');
                    $('.fileUpload').click();
                    $('.add_content').html('');
                    $('.add_content').append('<div class= "add_line"></div>');
                });
                $('.contentTypes .www').click(function () {
                    var is_Safari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 && navigator.userAgent && !navigator.userAgent.match('CriOS');
                    if ($('.mainContent').children().hasClass('moodleContent') == true) {
                        if ($('.mainContent').children().hasClass('embedWebsite') == true) {
                            $('.websitelinkPopup').show();
                            // Check Safari browser
                            if (is_Safari) {
                                if ($('.safari_message').hasClass('hidden')) {
                                    $('.safari_message').removeClass('hidden');
                                }
                            }
                            $('body').addClass('overlay2');
                        } else {
                            var modal1 = document.getElementById('message_www');
                            var er = document.getElementById('error_www');
                            modal1.style.display = "block";
                            $('.closetitle').click(function () {
                                modal1.style.display = "none";
                            });
                            window.onclick = function (event) {
                                if (event.target == modal1) {
                                    modal1.style.display = "none";
                                }
                            };
                        }
                    } else {
                        $('.websitelinkPopup').show();
                        // Check Safari browser
                        if (is_Safari) {
                            if ($('.safari_message').hasClass('hidden')) {
                                $('.safari_message').removeClass('hidden');
                            }
                        }
                        $('body').addClass('overlay2');
                    }
                });

                $('.divUploadFile ul > li#upload-photo,.divUploadFile ul > li.uploadFrom').click(function () {
                    $('.fileUpload').click();
                    $('.add_content').html('');
                    $('.add_content').append('<div style=" background-color:#126DB6;height:1px"></div>');

                });
                // take photo
                $('.divUploadFile ul > li#take-photo').click(function () {
                    $('body').removeClass('overlay2');
                    var modal = document.getElementById('mytitle');
                    var er = document.getElementById('error');
                    $('.closetitle').click(function () {
                        modal.style.display = "none";
                    });
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                    $('.add_content').html('');
                    $('.add_content').append('<div class= "add_line"></div>');
                    if (isMobile == false) {
                        checkDeviceSupport(function () {
                            console.log('hasWebcam:' + hasWebcam);
                            if (protocal == "https:") {
                                if (hasWebcam) {
                                    $('.mainContent').append('<div id="results" style="position:relative;"><div class="rm"><img src="../images/delete.png"></div><div id="camera"></div><button id="capture" class="takeImage" onclick="takeSnapshot()">Take a Photo</button></div><div class="cline"></div>');
                                    if($('.contentTypes').hasClass('error1')){
                                        $('.contentTypes').removeClass('error1');
                                    }
                                    if($('.addResourse').hasClass('error1')){
                                        $('.addResourse').removeClass('error1');
                                    }
                                    // Trung KV - remove class="moodleContent imageContent" //
                                    Webcam.set({
                                        width: 320,
                                        height: 240,
                                        image_format: 'jpeg',
                                        flip_horiz: true,
                                        jpeg_quality: 100
                                    });
                                    Webcam.attach('#camera');
                                    $('.divUploadFile').hide();
                                } else {
                                    console.log('Open upload file');
                                    $('body').removeClass('overlay2');
                                    $('.linkVideoPopup').fadeOut();
                                    $('.error').html( 'Your device is not supported.');
                                    modal.style.display = "block";
                                    // $('.lgt-notif-error').html('Your device is not supported.').addClass('lgt-visible');
                                }
                            } else {
                                $('.error').html( 'To use this feature, you should consider switching your application to a secure origin, such as HTTPS');
                                modal.style.display = "block";
                            }
                        });
                    } else {
                        $('.fileUpload').click();
                    }
                });
                // take video
                $('.divUploadFile ul > li.takeVideo').click(function () {
                    $('body').removeClass('overlay2');
                    $('.add_content').html('');
                    $('.add_content').append('<div class= "add_line"></div>');
                    if (isMobile == false) {
                        checkDeviceSupport(function () {
                            if (protocal == "https:") {
                                if (hasWebcam) {
                                    $('.mainContent').append('<div id="video-results"  style="position:relative;"><div class="rm"><img src="../images/delete.png"></div><div id="camera" class="experiment recordrtc"><select class="recording-media" hidden><option value="record-video">Video</option></select><select class="media-container-format" hidden><option>WebM</option></select><video id="player" controls controlslist = "nodownload"></video><br/><button class="takeImage">Start Recording</button><div style="text-align: center; display: none;"><button id="save-to-disk" class="takeImage">Save To Disk</button><button id="open-new-tab" class="takeImage">Open New Tab</button><button id="upload-to-server" class="takeImage">Upload To Server</button></div></div></div><div class="cline"></div>');
                                    if($('.contentTypes').hasClass('error1')){
                                        $('.contentTypes').removeClass('error1');
                                    }
                                    if($('.addResourse').hasClass('error1')){
                                        $('.addResourse').removeClass('error1');
                                    }
                                    $('.divUploadFile').hide();
                                    // Trung KV - Remove class="moodleContent videoContent" +  width="320" height="240" //
                                    // record RTC using record video
                                    var params = {},
                                        r = /([^&=]+)=?([^&]*)/g;

                                    function d(s) {
                                        return decodeURIComponent(s.replace(/\+/g, ' '));
                                    }

                                    var match, search = window.location.search;
                                    while (match = r.exec(search.substring(1))) {
                                        params[d(match[1])] = d(match[2]);

                                        if (d(match[2]) === 'true' || d(match[2]) === 'false') {
                                            params[d(match[1])] = d(match[2]) === 'true' ? true : false;
                                        }
                                    }
                                    window.params = params;

                                    var recordingDIV = document.querySelector('.recordrtc');
                                    var recordingMedia = recordingDIV.querySelector('.recording-media');
                                    var recordingPlayer = recordingDIV.querySelector('video');
                                    var mediaContainerFormat = recordingDIV.querySelector('.media-container-format');
                                    recordingDIV.querySelector('button').onclick = function () {
                                        var modal = document.getElementById('mytitle');
                                        var er = document.getElementById('error');
                                        $('.closetitle').click(function () {
                                            modal.style.display = "none";
                                        });
                                        window.onclick = function (event) {
                                            if (event.target == modal) {
                                                modal.style.display = "none";
                                            }
                                        }
                                        var button = this;

                                        if (button.innerHTML === 'Stop Recording') {
                                            button.disabled = true;
                                            button.disableStateWaiting = true;
                                            setTimeout(function () {
                                                button.disabled = false;
                                                button.disableStateWaiting = false;
                                            }, 2 * 1000);

                                            button.innerHTML = 'Start Recording';

                                            function stopStream() {
                                                if (button.stream && button.stream.stop) {
                                                    button.stream.stop();
                                                    button.stream = null;
                                                }
                                            }

                                            if (button.recordRTC) {
                                                if (button.recordRTC.length) {
                                                    button.recordRTC[0].stopRecording(function (url) {
                                                        if (!button.recordRTC[1]) {
                                                            button.recordingEndedCallback(url);
                                                            stopStream();

                                                            saveToDiskOrOpenNewTab(button.recordRTC[0]);
                                                            return;
                                                        }

                                                        button.recordRTC[1].stopRecording(function (url) {
                                                            button.recordingEndedCallback(url);
                                                            stopStream();
                                                        });
                                                    });
                                                }
                                                else {
                                                    button.recordRTC.stopRecording(function (url) {
                                                        button.recordingEndedCallback(url);
                                                        stopStream();

                                                        saveToDiskOrOpenNewTab(button.recordRTC);
                                                    });
                                                }
                                            }
                                            return;
                                        }

                                        button.disabled = true;

                                        var commonConfig = {
                                            onMediaCaptured: function (stream) {
                                                button.stream = stream;
                                                if (button.mediaCapturedCallback) {
                                                    button.mediaCapturedCallback();
                                                }

                                                button.innerHTML = 'Stop Recording';
                                                button.disabled = false;
                                            },
                                            onMediaStopped: function () {
                                                button.innerHTML = 'Start Recording';
                                                if (!button.disableStateWaiting) {
                                                    button.disabled = false;
                                                }
                                            },
                                            onMediaCapturingFailed: function (error) {
                                                if (error.name === 'PermissionDeniedError' && !!navigator.mozGetUserMedia) {
                                                    InstallTrigger.install({
                                                        'Foo': {
                                                            // https://addons.mozilla.org/firefox/downloads/latest/655146/addon-655146-latest.xpi?src=dp-btn-primary
                                                            URL: 'https://addons.mozilla.org/en-US/firefox/addon/enable-screen-capturing/',
                                                            toString: function () {
                                                                return this.URL;
                                                            }
                                                        }
                                                    });
                                                }

                                                commonConfig.onMediaStopped();
                                            }
                                        };

                                        if (recordingMedia.value === 'record-video') {
                                            captureVideo(commonConfig);

                                            button.mediaCapturedCallback = function () {
                                                button.recordRTC = RecordRTC(button.stream, {
                                                    type: mediaContainerFormat.value === 'Gif' ? 'gif' : 'video',
                                                    disableLogs: params.disableLogs || false,
                                                    canvas: {
                                                        width: params.canvas_width || 320,
                                                        height: params.canvas_height || 240
                                                    },
                                                    frameInterval: typeof params.frameInterval !== 'undefined' ? parseInt(params.frameInterval) : 20 // minimum time between pushing frames to Whammy (in milliseconds)
                                                });

                                                button.recordingEndedCallback = function (url) {
                                                    recordingPlayer.src = null;
                                                    recordingPlayer.srcObject = null;

                                                    if (mediaContainerFormat.value === 'Gif') {
                                                        recordingPlayer.pause();
                                                        recordingPlayer.poster = url;

                                                        recordingPlayer.onended = function () {
                                                            recordingPlayer.pause();
                                                            recordingPlayer.poster = URL.createObjectURL(button.recordRTC.blob);
                                                        };
                                                        return;
                                                    }

                                                    recordingPlayer.src = url;
                                                    recordingPlayer.play();

                                                    recordingPlayer.onended = function () {
                                                        recordingPlayer.pause();
                                                        recordingPlayer.src = URL.createObjectURL(button.recordRTC.blob);
                                                    };
                                                };

                                                button.recordRTC.startRecording();
                                            };
                                        }

                                        if (recordingMedia.value === 'record-audio') {
                                            captureAudio(commonConfig);

                                            button.mediaCapturedCallback = function () {
                                                button.recordRTC = RecordRTC(button.stream, {
                                                    type: 'audio',
                                                    bufferSize: typeof params.bufferSize == 'undefined' ? 0 : parseInt(params.bufferSize),
                                                    sampleRate: typeof params.sampleRate == 'undefined' ? 44100 : parseInt(params.sampleRate),
                                                    leftChannel: params.leftChannel || false,
                                                    disableLogs: params.disableLogs || false,
                                                    recorderType: webrtcDetectedBrowser === 'edge' ? StereoAudioRecorder : null
                                                });

                                                button.recordingEndedCallback = function (url) {
                                                    var audio = new Audio();
                                                    audio.src = url;
                                                    audio.controls = true;
                                                    recordingPlayer.parentNode.appendChild(document.createElement('hr'));
                                                    recordingPlayer.parentNode.appendChild(audio);

                                                    if (audio.paused) audio.play();

                                                    audio.onended = function () {
                                                        audio.pause();
                                                        audio.src = URL.createObjectURL(button.recordRTC.blob);
                                                    };
                                                };

                                                button.recordRTC.startRecording();
                                            };
                                        }

                                        if (recordingMedia.value === 'record-audio-plus-video') {
                                            captureAudioPlusVideo(commonConfig);

                                            button.mediaCapturedCallback = function () {

                                                if (webrtcDetectedBrowser !== 'firefox') { // opera or chrome etc.
                                                    button.recordRTC = [];

                                                    if (!params.bufferSize) {
                                                        // it fixes audio issues whilst recording 720p
                                                        params.bufferSize = 16384;
                                                    }

                                                    var audioRecorder = RecordRTC(button.stream, {
                                                        type: 'audio',
                                                        bufferSize: typeof params.bufferSize == 'undefined' ? 0 : parseInt(params.bufferSize),
                                                        sampleRate: typeof params.sampleRate == 'undefined' ? 44100 : parseInt(params.sampleRate),
                                                        leftChannel: params.leftChannel || false,
                                                        disableLogs: params.disableLogs || false,
                                                        recorderType: webrtcDetectedBrowser === 'edge' ? StereoAudioRecorder : null
                                                    });

                                                    var videoRecorder = RecordRTC(button.stream, {
                                                        type: 'video',
                                                        disableLogs: params.disableLogs || false,
                                                        canvas: {
                                                            width: params.canvas_width || 320,
                                                            height: params.canvas_height || 240
                                                        },
                                                        frameInterval: typeof params.frameInterval !== 'undefined' ? parseInt(params.frameInterval) : 20 // minimum time between pushing frames to Whammy (in milliseconds)
                                                    });

                                                    // to sync audio/video playbacks in browser!
                                                    videoRecorder.initRecorder(function () {
                                                        audioRecorder.initRecorder(function () {
                                                            audioRecorder.startRecording();
                                                            videoRecorder.startRecording();
                                                        });
                                                    });

                                                    button.recordRTC.push(audioRecorder, videoRecorder);

                                                    button.recordingEndedCallback = function () {
                                                        var audio = new Audio();
                                                        audio.src = audioRecorder.toURL();
                                                        audio.controls = true;
                                                        audio.autoplay = true;

                                                        audio.onloadedmetadata = function () {
                                                            recordingPlayer.src = videoRecorder.toURL();
                                                            recordingPlayer.play();
                                                        };
                                                        recordingPlayer.parentNode.appendChild(document.createElement('hr'));
                                                        recordingPlayer.parentNode.appendChild(audio);
                                                        if (audio.paused) audio.play();
                                                    };
                                                    return;
                                                }

                                                button.recordRTC = RecordRTC(button.stream, {
                                                    type: 'video',
                                                    disableLogs: params.disableLogs || false,
                                                    // we can't pass bitrates or framerates here
                                                    // Firefox MediaRecorder API lakes these features
                                                });

                                                button.recordingEndedCallback = function (url) {
                                                    recordingPlayer.srcObject = null;
                                                    recordingPlayer.muted = false;
                                                    recordingPlayer.src = url;
                                                    recordingPlayer.play();

                                                    recordingPlayer.onended = function () {
                                                        recordingPlayer.pause();
                                                        recordingPlayer.src = URL.createObjectURL(button.recordRTC.blob);
                                                    };
                                                };
                                                button.recordRTC.startRecording();
                                            };
                                        }

                                        if (recordingMedia.value === 'record-screen') {
                                            captureScreen(commonConfig);

                                            button.mediaCapturedCallback = function () {
                                                button.recordRTC = RecordRTC(button.stream, {
                                                    type: mediaContainerFormat.value === 'Gif' ? 'gif' : 'video',
                                                    disableLogs: params.disableLogs || false,
                                                    canvas: {
                                                        width: params.canvas_width || 320,
                                                        height: params.canvas_height || 240
                                                    }
                                                });

                                                button.recordingEndedCallback = function (url) {
                                                    recordingPlayer.src = null;
                                                    recordingPlayer.srcObject = null;

                                                    if (mediaContainerFormat.value === 'Gif') {
                                                        recordingPlayer.pause();
                                                        recordingPlayer.poster = url;
                                                        recordingPlayer.onended = function () {
                                                            recordingPlayer.pause();
                                                            recordingPlayer.poster = URL.createObjectURL(button.recordRTC.blob);
                                                        };
                                                        return;
                                                    }
                                                    recordingPlayer.src = url;
                                                    recordingPlayer.play();
                                                };

                                                button.recordRTC.startRecording();
                                            };
                                        }

                                        if (recordingMedia.value === 'record-audio-plus-screen') {
                                            captureAudioPlusScreen(commonConfig);

                                            button.mediaCapturedCallback = function () {
                                                button.recordRTC = RecordRTC(button.stream, {
                                                    type: 'video',
                                                    disableLogs: params.disableLogs || false,
                                                    // we can't pass bitrates or framerates here
                                                    // Firefox MediaRecorder API lakes these features
                                                });

                                                button.recordingEndedCallback = function (url) {
                                                    recordingPlayer.srcObject = null;
                                                    recordingPlayer.muted = false;
                                                    recordingPlayer.src = url;
                                                    recordingPlayer.play();

                                                    recordingPlayer.onended = function () {
                                                        recordingPlayer.pause();
                                                        recordingPlayer.src = URL.createObjectURL(button.recordRTC.blob);
                                                    };
                                                };

                                                button.recordRTC.startRecording();
                                            };
                                        }
                                    };
                                    function captureVideo(config) {
                                        captureUserMedia({video: true}, function (videoStream) {
                                            recordingPlayer.srcObject = videoStream;
                                            recordingPlayer.play();

                                            config.onMediaCaptured(videoStream);

                                            videoStream.onended = function () {
                                                config.onMediaStopped();
                                            };
                                        }, function (error) {
                                            config.onMediaCapturingFailed(error);
                                        });
                                    }

                                    function captureAudio(config) {
                                        captureUserMedia({audio: true}, function (audioStream) {
                                            recordingPlayer.srcObject = audioStream;
                                            recordingPlayer.play();

                                            config.onMediaCaptured(audioStream);

                                            audioStream.onended = function () {
                                                config.onMediaStopped();
                                            };
                                        }, function (error) {
                                            config.onMediaCapturingFailed(error);
                                        });
                                    }

                                    function captureAudioPlusVideo(config) {
                                        captureUserMedia({video: true, audio: true}, function (audioVideoStream) {
                                            recordingPlayer.srcObject = audioVideoStream;
                                            recordingPlayer.play();

                                            config.onMediaCaptured(audioVideoStream);

                                            audioVideoStream.onended = function () {
                                                config.onMediaStopped();
                                            };
                                        }, function (error) {
                                            config.onMediaCapturingFailed(error);
                                        });
                                    }

                                    function captureScreen(config) {
                                        getScreenId(function (error, sourceId, screenConstraints) {
                                            if (error === 'not-installed') {
                                                document.write('<h1><a target="_blank" href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk">Please install this chrome extension then reload the page.</a></h1>');
                                            }

                                            if (error === 'permission-denied') {
                                                alert('Screen capturing permission is denied.');
                                            }

                                            if (error === 'installed-disabled') {
                                                alert('Please enable chrome screen capturing extension.');
                                            }

                                            if (error) {
                                                config.onMediaCapturingFailed(error);
                                                return;
                                            }

                                            captureUserMedia(screenConstraints, function (screenStream) {
                                                recordingPlayer.srcObject = screenStream;
                                                recordingPlayer.play();

                                                config.onMediaCaptured(screenStream);

                                                screenStream.onended = function () {
                                                    config.onMediaStopped();
                                                };
                                            }, function (error) {
                                                config.onMediaCapturingFailed(error);
                                            });
                                        });
                                    }

                                    function captureAudioPlusScreen(config) {
                                        getScreenId(function (error, sourceId, screenConstraints) {
                                            if (error === 'not-installed') {
                                                document.write('<h1><a target="_blank" href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk">Please install this chrome extension then reload the page.</a></h1>');
                                            }

                                            if (error === 'permission-denied') {
                                                alert('Screen capturing permission is denied.');
                                            }

                                            if (error === 'installed-disabled') {
                                                alert('Please enable chrome screen capturing extension.');
                                            }

                                            if (error) {
                                                config.onMediaCapturingFailed(error);
                                                return;
                                            }

                                            screenConstraints.audio = true;

                                            captureUserMedia(screenConstraints, function (screenStream) {
                                                recordingPlayer.srcObject = screenStream;
                                                recordingPlayer.play();

                                                config.onMediaCaptured(screenStream);

                                                screenStream.onended = function () {
                                                    config.onMediaStopped();
                                                };
                                            }, function (error) {
                                                config.onMediaCapturingFailed(error);
                                            });
                                        });
                                    }

                                    function captureUserMedia(mediaConstraints, successCallback, errorCallback) {
                                        navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
                                    }

                                    function setMediaContainerFormat(arrayOfOptionsSupported) {
                                        var options = Array.prototype.slice.call(
                                            mediaContainerFormat.querySelectorAll('option')
                                        );

                                        var selectedItem;
                                        options.forEach(function (option) {
                                            option.disabled = true;

                                            if (arrayOfOptionsSupported.indexOf(option.value) !== -1) {
                                                option.disabled = false;

                                                if (!selectedItem) {
                                                    option.selected = true;
                                                    selectedItem = option;
                                                }
                                            }
                                        });
                                    }

                                    recordingMedia.onchange = function () {
                                        if (this.value === 'record-audio') {
                                            setMediaContainerFormat(['WAV', 'Ogg']);
                                            return;
                                        }
                                        setMediaContainerFormat(['WebM', /*'Mp4',*/ 'Gif']);
                                    };

                                    if (webrtcDetectedBrowser === 'edge') {
                                        // webp isn't supported in Microsoft Edge
                                        // neither MediaRecorder API
                                        // so lets disable both video/screen recording options

                                        console.warn('Neither MediaRecorder API nor webp is supported in Microsoft Edge. You cam merely record audio.');

                                        recordingMedia.innerHTML = '<option value="record-audio">Audio</option>';
                                        setMediaContainerFormat(['WAV']);
                                    }

                                    if (webrtcDetectedBrowser === 'firefox') {
                                        // Firefox implemented both MediaRecorder API as well as WebAudio API
                                        // Their MediaRecorder implementation supports both audio/video recording in single container format
                                        // Remember, we can't currently pass bit-rates or frame-rates values over MediaRecorder API (their implementation lakes these features)

                                        recordingMedia.innerHTML = '<option value="record-audio-plus-video">Audio+Video</option>'
                                            + '<option value="record-audio-plus-screen">Audio+Screen</option>'
                                            + recordingMedia.innerHTML;
                                    }

                                    // disabling this option because currently this demo
                                    // doesn't supports publishing two blobs.
                                    // todo: add support of uploading both WAV/WebM to server.
                                    if (false && webrtcDetectedBrowser === 'chrome') {
                                        recordingMedia.innerHTML = '<option value="record-audio-plus-video">Audio+Video</option>'
                                            + recordingMedia.innerHTML;
                                        console.info('This RecordRTC demo merely tries to playback recorded audio/video sync inside the browser. It still generates two separate files (WAV/WebM).');
                                    }

                                    function saveToDiskOrOpenNewTab(recordRTC) {
                                        recordingDIV.querySelector('#save-to-disk').parentNode.style.display = 'block';
                                        recordingDIV.querySelector('#save-to-disk').onclick = function () {
                                            if (!recordRTC) return alert('No recording found.');

                                            recordRTC.save();
                                        };

                                        recordingDIV.querySelector('#open-new-tab').onclick = function () {
                                            if (!recordRTC) return alert('No recording found.');

                                            window.open(recordRTC.toURL());
                                        };

                                        recordingDIV.querySelector('#upload-to-server').disabled = false;
                                        recordingDIV.querySelector('#upload-to-server').onclick = function () {
                                            if (!recordRTC) return alert('No recording found.');
                                            this.disabled = true;

                                            // document.getElementsByClassName('lgt-notif')[0].innerHTML = 'Uploading...';
                                            // document.getElementsByClassName('lgt-notif')[0].className += ' lgt-visible';

                                            var button = this;
                                            uploadToServer(recordRTC, function (progress, fileURL) {
                                                if (progress === 'ended') {
                                                    button.disabled = false;
                                                    button.innerHTML = 'Click to download from server';
                                                    button.onclick = function () {
                                                        window.open(fileURL);
                                                    };
                                                    return;
                                                }
                                                button.innerHTML = progress;
                                            });
                                        };
                                    }

                                    var listOfFilesUploaded = [];

                                    function uploadToServer(recordRTC, callback) {
                                        var blob = recordRTC instanceof Blob ? recordRTC : recordRTC.blob;
                                        var fileType = blob.type.split('/')[0] || 'audio';
                                        var fileName = (Math.random() * 1000).toString().replace('.', '');

                                        if (fileType === 'audio') {
                                            fileName += '.' + (!!navigator.mozGetUserMedia ? 'ogg' : 'wav');
                                        } else {
                                            fileName += '.webm';
                                        }

                                        // create FormData
                                        var formData = new FormData();
                                        formData.append(fileType + '-filename', fileName);
                                        formData.append(fileType + '-blob', blob);

                                        callback('Uploading ' + fileType + ' recording to server.');

                                        makeXMLHttpRequest(window.location.href, formData, function (progress) {
                                            if (progress !== 'upload-ended') {
                                                callback(progress);
                                                return;
                                            }

                                            var initialURL = location.href.replace(location.href.split('/').pop(), '') + 'uploads/';

                                            callback('ended', initialURL + fileName);

                                            // to make sure we can delete as soon as visitor leaves
                                            listOfFilesUploaded.push(initialURL + fileName);
                                        });
                                    }

                                    function makeXMLHttpRequest(url, data, callback) {
                                        var request = new XMLHttpRequest();
                                        request.onreadystatechange = function () {
                                            if (request.readyState == 4 && request.status == 200) {
                                                callback('upload-ended');
                                                // console.log(request.response);
                                                $('.mainContent #video-results').remove();
                                                var res = JSON.parse(request.response);
                                                for (var i = 0; i < res.data.length; i++) {
                                                    var name = res.data[i].name;
                                                    var type = res.data[i].type;
                                                    var url = res.data[i].url;
                                                    var itemid = res.data[i].itemid;
                                                    $('.mainContent').append('<div class="moodleContent videoContent" data-itemid="' + itemid + '" data-filename="' + name + '"><div class="rm"><img src="../images/delete.png"></div><video webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" oncontextmenu="return false;" frameborder="0" controls="controls"  controlslist = "nodownload" src="' + url + '"></video></div><div class="cline"></div>');
                                                    if($('.contentTypes').hasClass('error1')){
                                                        $('.contentTypes').removeClass('error1');
                                                    }
                                                    if($('.addResourse').hasClass('error1')){
                                                        $('.addResourse').removeClass('error1');
                                                    }
                                                    // Trung KV - remove height="240" width="320" //
                                                    if($('.formAddContent .btSave').hasClass('clicked')){
                                                        $('.formAddContent .btSave').click();
                                                    }
                                                }
                                            }
                                        };

                                        request.upload.onloadstart = function () {
                                            callback('Upload started...');
                                        };

                                        request.upload.onprogress = function (event) {
                                            callback('Upload Progress ' + Math.round(event.loaded / event.total * 100) + "%");
                                        };

                                        request.upload.onload = function () {
                                            callback('progress-about-to-end');
                                        };

                                        request.upload.onload = function () {
                                            // callback('progress-ended');
                                            callback('Upload successfully!');
                                            // document.getElementsByClassName('lgt-notif')[0].classList.remove("lgt-visible");
                                        };

                                        request.upload.onerror = function (error) {
                                            callback('Failed to upload to server');
                                            console.error('XMLHttpRequest failed', error);
                                        };

                                        request.upload.onabort = function (error) {
                                            callback('Upload aborted.');
                                            console.error('XMLHttpRequest aborted', error);
                                        };

                                        request.open('POST', url);
                                        request.send(data);
                                    }
                                } else {
                                    console.log('Open upload file');
                                    $('body').removeClass('overlay2');
                                    $('.linkVideoPopup').fadeOut();
                                    $('.error').html( 'Your device is not supported.');
                                    modal.style.display = "block";

                                }
                            } else {

                                $('.error').html( 'To use this feature, you should consider switching your application to a secure origin, such as HTTPS');
                                modal.style.display = "block";
                            }
                        });
                    } else {
                        $('.fileUpload').click();
                    }
                });

                $('.contentTypes .link').click(function () {
                    var modal = document.getElementById('mytitle');
                    var er = document.getElementById('error');
                    $('.closetitle').click(function () {
                        modal.style.display = "none";
                    });
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                    $('.add_content').html('');
                    $('.add_content').append('<div class= "add_line"></div>');
                    if (tinyMCE.activeEditor == null) {
                        $('.error').html( '<?PHP echo JText::_('COM_JOOMDLE_EMPTY_HIGHTPERLINK')?>');
                        modal.style.display = "block";
                        // $('.lgt-notif-error').html('There is not any textbox in your page content.').addClass('lgt-visible');
                        return;
                    }
                    var highlighted = tinyMCE.activeEditor.selection.getContent();
                    if (highlighted) {
                        $('.hyperlinkPopup').show();
                        $('.txtHyperlink').val('http://');
                        $('body').addClass('overlay2');
                    } else {
                        $('body').removeClass('overlay2');
                        $('.linkVideoPopup').fadeOut();
                        $('.error').html( '<?PHP echo JText::_('COM_JOOMDLE_EMPTY_HIGHTPERLINK')?>');
                        modal.style.display = "block";
                    }
                });
                $('.hyperlinkPopup .btAdd').click(function () {
                    if ($('.txtHyperlink').val() != '' && $('.txtHyperlink').val() != 'http://') {
                        var link = $('.txtHyperlink').val();

                        if (link.indexOf('http://') == -1 && link.indexOf('https://') == -1) link = 'http://' + link;

                        $('.divUploadFile > ul').removeClass('photo').removeClass('video');
//                        tinyMCE.activeEditor.execCommand('mceInsertLink', false, $('.txtHyperlink').val());
                        var highlighted = tinyMCE.activeEditor.selection.getContent();

                        tinyMCE.activeEditor.execCommand('insertHTML', false, '<a id = "' + highlighted + '" style ="cursor: pointer; color:#126db6;" href="' + link + '" target="_blank">' + highlighted + '</a>');
                        //}
                        jQuery('.textareaContent iframe').contents().find('a').on('click', function (event) {
                            txtvalue = $(this).attr('href');
                            txtid = $(this).attr('id');

                            $('.hyperlinkPopup1').show();
                            $('.txtHyperlink1').val(txtvalue);
                            $('.txtHyperlink1').attr('name', txtid);
                            $('body').addClass('overlay2');
                        });
                        $(' .hyperlinkPopup').hide();
                        $('body').removeClass('overlay2');
                    } else {
                        var modal = document.getElementById('mytitle');
                        var er = document.getElementById('error');
                        $('.closetitle').click(function () {
                            modal.style.display = "none";
                        });
                        window.onclick = function (event) {
                            if (event.target == modal) {
                                modal.style.display = "none";
                            }
                        }

                        $('.error').html( 'Please add hyperlink to form input.');
                        modal.style.display = "block";
                        // $('.lgt-notif-error').html('Please add hyperlink to form input.').addClass('lgt-visible');
                        $('body').removeClass('overlay2');
                        $('.linkVideoPopup').fadeOut();
                    }
                });
                $('.hyperlinkPopup1 .btAdd').click(function () {
                    var modal = document.getElementById('mytitle');
                    var er = document.getElementById('error');
                    $('.closetitle').click(function () {
                        modal.style.display = "none";
                    });
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                    if ($('.txtHyperlink1').val() != '' && $('.txtHyperlink1').val() != 'http://') {
                        var link = $('.txtHyperlink1').val();
                        if (link.indexOf('http://') == -1 && link.indexOf('https://') == -1) link = 'http://' + link;
                        $('.divUploadFile > ul').removeClass('photo').removeClass('video');
                        var id = $('.txtHyperlink1').attr('name');
                        jQuery('.textareaContent iframe').contents().find("#" + id).attr('href', link);

                        jQuery('.textareaContent iframe').contents().find("#" + id).attr('data-mce-href', link);
                        $('.hyperlinkPopup1').hide();
                        $('body').removeClass('overlay2');
                    } else {

                        $('.error').html( 'Please add hyperlink to form input.');
                        modal.style.display = "block";
                        // $('.lgt-notif-error').html('Please add hyperlink to form input.').addClass('lgt-visible');
                        $('body').removeClass('overlay2');
                        $('.linkVideoPopup').fadeOut();
                    }
                });
                $('.divUploadFile button.cancel').click(function () {
                    $('.divUploadFile').hide();
                    $('body').removeClass('overlay2');

                });

                $('.divUploadFile ul > li.linkVideo').click(function () {
                    $('.txtLinkVideo').val('');
                    $('.linkVideoPopup, .embedCodePopup, .divUploadFile, .hyperlinkPopup').hide();
                    $('.linkVideoPopup').show();
                    $('body').addClass('overlay2');
                });

                function isUrlValid(url) {
                    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
                }

                $('.websitelinkPopup .btAddLinkWeb').click(function () {
                    var is_Safari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 && navigator.userAgent && !navigator.userAgent.match('CriOS');
                    if ($('.websitelinkPopup').children().hasClass('webLinkVaild') == true) {
                        $(".webLinkVaild").remove();
                    }
                    var linkweb = $('.txtWebsiteLink').val();
                    if (isUrlValid(linkweb)) {
                        if ($('.moodleContent').hasClass('embedWebsite') == true) {
                            $(".embedWebsite").remove();
                        }
                        $(this).parent().hide();
                        $('body').removeClass('overlay2');

//                        $('.mainContent').append('<div class="moodleContent embedWebsite"><div class="rmWww"><img src="../images/deleteIcon.png"></div>' +
//                            '<iframe frameborder="0" width="100%" height="500" src="'+linkweb+'" class="iframeLinkWeb" ></iframe>' +
//                            '</div>');

                        $('.mainContent').append('<div class="moodleContent embedWebsite"><div class="rmWww"><img src="../images/delete.png"></div>' +
                            '<object frameborder="0" width="100%" height="500" data="' + linkweb + '" class="iframeLinkWeb" ></object>' +
                            '</div>');

                        if($('.contentTypes').hasClass('error1')){
                            $('.contentTypes').removeClass('error1');
                        }
                        if($('.addResourse').hasClass('error1')){
                            $('.addResourse').removeClass('error1');
                        }
                        // Check Safari browser
                        if (is_Safari) {
                            if ($('.message_safari').hasClass('hidden')) {
                                $('.message_safari').removeClass('hidden');
                            }
                        }
                        $('.add_content').html('');
                        $('.add_content').append('<div class= "add_line"></div>');
                        $('.view-content .contentTypes .contentType.text img').css("opacity","0.4");
                        $('.view-content .contentTypes .contentType.image img').css("opacity","0.4");
                        $('.view-content .contentTypes .contentType.video img').css("opacity","0.4");
                        $('.view-content .contentTypes .contentType.link img').css("opacity","0.4");
                        $('.view-content .contentTypes .contentType.attach img').css("opacity","0.4");

                        $('.view-content .contentTypes .contentType.text').css("cursor","default");
                        $('.view-content .contentTypes .contentType.image').css("cursor","default");
                        $('.view-content .contentTypes .contentType.video').css("cursor","default");
                        $('.view-content .contentTypes .contentType.link').css("cursor","default");
                        $('.view-content .contentTypes .contentType.attach').css("cursor","default");

                        $('.contentTypes .text').off("click");
                        $('.contentTypes .image').off("click");
                        $('.contentTypes .video').off("click");
                        $('.contentTypes .link').off("click");
                        $('.contentTypes .attach').off("click");

                    } else {
                        $('.btAddLinkWeb').before('<p class="webLinkVaild">Weblink is invalid. Please enter again!</p>');
                    }

                });


                $('.linkVideoPopup .btAdd').click(function() {
                    var modal = document.getElementById('mytitle');
                    var er = document.getElementById('error');
                    $('.closetitle').click(function () {
                        modal.style.display = "none";
                    });
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                    if ($('.txtLinkVideo').val() != '') {
                        var url = $('.txtLinkVideo').val();
                        var vid;
                        if ((url.indexOf('youtu.be') != -1) || (url.indexOf('youtube.com') != -1)) {
                            if (url.indexOf('watch?v=') != -1) {
                                vid = url.substr((url.indexOf('watch?v=') + 8), 11);
                            } else if (url.indexOf('youtu.be/') != -1) {
                                vid = url.substr((url.indexOf('youtu.be/') + 9), 11);
                                var url = "https://www.youtube.com/watch?v=" + vid;
                            } else {
                                $('.error').html( 'Invalid link video');
                                modal.style.display = "block";
                            }

                        } else if (url.indexOf('vimeo.com/') != -1) {
                            if(url.indexOf('vimeo.com/video/') != -1)
                                vid = url.substr((url.indexOf('vimeo.com/video/') + 16), 9);
                            else
                                vid = url.substr((url.indexOf('vimeo.com/') + 10), 9);
                            i++;

                        } else if ((url.indexOf('dai.ly') != -1) || (url.indexOf('dailymotion.com') != -1)) {
                            if (url.indexOf('dai.ly/') != -1) {
                                vid = url.substr((url.indexOf('dai.ly/') + 7), 7);
                                url = "http://www.dailymotion.com/video/"+vid;
                            } else if (url.indexOf('dailymotion.com/video/') != -1) {
                                vid = url.substr((url.indexOf('dailymotion.com/video/') + 22), 7);
                            } else {
                                $('.error').html( 'Invalid link video');
                                modal.style.display = "block";
                            }

                            v++;
                            textareaCounta='a'+v;
                        }
                        else {
                            $('body').removeClass('overlay2');
                            $('.linkVideoPopup').fadeOut();
                            $('.error').html( 'Invalid link video');
                            modal.style.display = "block";
                            return;
                        }
                        var act = 'addFilevideo';

                        $.ajax({
                            url: window.location.href,
                            type: 'POST',
                            data: {
                                act: act,
                                url: url
                            },
                            beforeSend: function () {
                                $('.linkVideoPopup, .embedCodePopup, .divUploadFile, .hyperlinkPopup').hide();
                                lgtCreatePopup('', {'content': 'Loading...'});
                            },
                            success: function (data, textStatus, jqXHR) {
                                lgtRemovePopup();
                                var res = JSON.parse(data);

                                if (res.error == 1) {
                                    $('.linkVideoPopup').fadeOut();
                                    $('.error').html( 'Invalid link video');
                                    modal.style.display = "block";
                                }
                                else {
                                    if (res.test_a > 0) {
                                        i++;
                                        v++;
                                        textareaCounta = 'a' + v;

                                        $('.mainContent').append('<div class="moodleContent linkVideo withCaption"><div class="rm"><img src="../images/delete.png"></div>' +
                                            '<iframe src="//www.youtube.com/embed/' + vid + '?autoplay=0" frameborder="0" class="iframeLinkVideo withCap" allowfullscreen></iframe>' +
                                            '</div>');
                                        if($('.contentTypes').hasClass('error1')){
                                            $('.contentTypes').removeClass('error1');
                                        }
                                        if($('.addResourse').hasClass('error1')){
                                            $('.addResourse').removeClass('error1');
                                        }
                                    } else if (res.test_b > 0 || res.test_c > 0) {
                                        i++;
                                        v++;
                                        textareaCounta = 'a' + v;

                                        $('.mainContent').append('<div class="moodleContent linkVideo withCaption"><div class="rm"><img src="../images/delete.png"></div>' +
                                            '<iframe frameborder="0" src="//www.dailymotion.com/embed/video/' + vid + '" allowfullscreen class="iframeLinkVideo withCap" ></iframe>' +
                                            '</div>');
                                        if($('.contentTypes').hasClass('error1')){
                                            $('.contentTypes').removeClass('error1');
                                        }
                                        if($('.addResourse').hasClass('error1')){
                                            $('.addResourse').removeClass('error1');
                                        }
                                    }
                                    else if (res.test_d > 0) {
                                        i++;

                                        v++;
                                        textareaCounta = 'a' + v;

                                        $('.mainContent').append('<div class="moodleContent linkVideo withCaption"><div class="rm"><img src="../images/delete.png"></div>' +
                                            '<iframe frameborder="0" src="//player.vimeo.com/video/' + vid + '" allowfullscreen class="iframeLinkVideo withCap" ></iframe>' +
                                            '</div>');
                                        if($('.contentTypes').hasClass('error1')){
                                            $('.contentTypes').removeClass('error1');
                                        }
                                        if($('.addResourse').hasClass('error1')){
                                            $('.addResourse').removeClass('error1');
                                        }
                                    }
                                }
                                tinymce.init({
                                    selector: ".txtaContent",
                                    preview_styles: "OpenSans",
                                    theme: "modern",
                                    height: 80,
                                    color: "#efecec",
                                    body_class: "mceBlackBody",
                                    content_style: "body {background-color:#efecec !important;color:#555;}",
                                    plugins: [
                                        "advlist autolink image lists charmap print preview hr anchor pagebreak spellchecker",
                                        "link searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                                        "save table directionality emoticons template paste textcolor"
                                    ],
                                    toolbar: "fontselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
                                    font_formats: 'opensans=OpenSans,Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n'
                                });
                                $('.captionContent iframe').attr('style', 'height: 75px !important;width:100% !important');
                                setTimeout(
                                    function() {
                                        var iframeBody=$('.captionContent iframe').contents().find('body');
                                        iframeBody.attr('p', true);
                                        $(iframeBody).on( 'drop',function() {
                                            return false;
                                        });
                                    }, 1000);

                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                lgtRemovePopup();
                                console.log('ERRORS: ' + textStatus);
                            }
                        });

                        $('.linkVideoPopup, .embedCodePopup, .divUploadFile, .hyperlinkPopup').hide();
                        $('body').removeClass('overlay2');

                    }
                    else {
                        //$('.linkVideoPopup').fadeOut();
                        $('.error').html( 'Please add hyperlink to form input.');
                        modal.style.display = "block";
                        // $('.lgt-notif-error').html('Please add link to form input.').addClass('lgt-visible');
                    }

                });
                $(document).on('click', '.rm', function () {
//                    if ($(this).parent('.moodleContent').hasClass('withCaption')) {
//                        $(this).parent().next().addClass('rmObject');
//                    }
                    $(this).parent().addClass('rmObject');
                    $('.rmPopup').show();
                    $('body').addClass('overlay2');
                });
                $('.rmPopup .btRMCancel').click(function () {
                    $('.rmObject').removeClass('rmObject');
                    $('.rmPopup').hide();
                    $('body').removeClass('overlay2');
                });

                $('.rmPopup .btRMYes').click(function () {
                    var modal = document.getElementById('mytitle');
                    var er = document.getElementById('error');
                    $('.closetitle').click(function () {
                        modal.style.display = "none";
                    });
                    $('body').removeClass('overlay2');
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                    var itemid = $('.rmObject').attr('data-itemid');
                    var filename = $('.rmObject').attr('data-filename');

                    if (itemid != 'undefined') {
                        var act = 'deleteFile';
                        $.ajax({
                            url: window.location.href,
                            type: 'POST',
                            data: {
                                act: act,
                                itemid: itemid,
                                filename: filename
                            },
                            success: function (data, textStatus, jqXHR) {
                                var res = JSON.parse(data);
                                if (res.error == 1) {
                                    $('.error').html( res.comment);
                                    modal.style.display = "block";
                                    // $('.lgt-notif-error').html(res.comment).addClass('lgt-visible');
                                } else {
                                    console.log(res);
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log('ERRORS: ' + textStatus);
                            }
                        });
                    }

                    $('.rmObject').remove();
                    $('.rmPopup').hide();
                });


                $(document).on('click', '.rmWww', function () {
                    $('.rmPopupWww').show();
                    $('body').addClass('overlay2');
                });

                $('.rmPopupWww .btRMCancelWww').click(function () {
                    $('.rmPopupWww').hide();
                    $('body').removeClass('overlay2');
                });

                $('.rmPopupWww .btRMYesWww').click(function () {
                    $('body').removeClass('overlay2');
                    var is_Safari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 && navigator.userAgent && !navigator.userAgent.match('CriOS');
                    $('.rmPopupWww').hide();
                    if ($('.mainContent').children().hasClass('embedWebsite') == true) {
                        $('.embedWebsite').remove();
                        if (is_Safari) {
                            if ($('.message_safari').hasClass('hidden') == false) {
                                $('.message_safari').addClass('hidden');
                            }
                        }
                        $('.view-content .contentTypes .contentType.text img').css("opacity", "1");
                        $('.view-content .contentTypes .contentType.image img').css("opacity", "1");
                        $('.view-content .contentTypes .contentType.video img').css("opacity", "1");
                        $('.view-content .contentTypes .contentType.link img').css("opacity", "1");
                        $('.view-content .contentTypes .contentType.attach img').css("opacity", "1");

                        $('.view-content .contentTypes .contentType.text').css("cursor", "pointer");
                        $('.view-content .contentTypes .contentType.image').css("cursor", "pointer");
                        $('.view-content .contentTypes .contentType.video').css("cursor", "pointer");
                        $('.view-content .contentTypes .contentType.link').css("cursor", "pointer");
                        $('.view-content .contentTypes .contentType.attach').css("cursor", "pointer");

                        var i = 0, textareaCount = <?php echo ($this->action == 'edit') ? $i - 1 : '0'; ?>;
                        var v = <?php echo ($this->action == 'edit') ? $i - 1 : '0'; ?>;
                        tinymce.init({
                            //auto_focus:true,
                            selector: ".txtaContent",
                            preview_styles: "OpenSans",
                            theme: "modern",
                            height: 80,
                            body_class: "mceBlackBody",

                            plugins: [
                                "advlist autolink image lists charmap print preview hr anchor pagebreak spellchecker",
                                "link searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                                "save table directionality emoticons template paste textcolor"
                            ],
                            toolbar: "fontselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
                            font_formats: 'opensans=OpenSans,Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n'

                        });
                        $('.contentTypes .text').bind('click', function (e) {
                            textareaCount++;
                            var att = textareaCount - 1;
                            var id_frame = '#txtaContent' + att + '_ifr';
                            var iframeBody = $(id_frame).contents().find('#tinymce');
                            iframeBody.attr('p', true);
                            if (id_frame == '#txtaContent0_ifr' || id_frame == '#txtaContent-1_ifr' || iframeBody.length == 0) {
                                test = 'aaa';
                            }
                            else test = $(iframeBody).text();
                            if (test == '') {
                                textareaCount--;
                                var modal = document.getElementById('mytitle');
                                var er = document.getElementById('error');
                                $('.closetitle').click(function () {
                                    modal.style.display = "none";

                                });
                                window.onclick = function (event) {
                                    if (event.target == modal) {
                                        modal.style.display = "none";
                                    }
                                };
                                $('.error').html( 'Add text first.');
                                modal.style.display = "block";
                            } else {
                                i++;
                                // $('.moodleContent > .txtaContent').length
                                $('.mainContent').append('<div class="textareaContent moodleContent" data-tinymceid="' + textareaCount + '" id="txtaContent' + textareaCount + '" ><div class="rm"><img src="../images/delete.png"></div><textarea id="txtaContent' + textareaCount + '" class="txtaContent"><?php //echo JText::_('COM_JOOMDLE_ADD_TEXT'); ?></textarea></div>');
                                if($('.contentTypes').hasClass('error1')){
                                    $('.contentTypes').removeClass('error1');
                                }
                                if($('.addResourse').hasClass('error1')){
                                    $('.addResourse').removeClass('error1');
                                }
                                var iframe = '#txtaContent' + textareaCount + '_ifr';
                                tinymce.init({
                                    //auto_focus:true,
                                    selector: ".txtaContent",
                                    preview_styles: "OpenSans",
                                    theme: "modern",
                                    height: 80,
                                    content_style: "body {background-color:#fff !important;color:#828282 !important;font-size: 16px !important;}",
                                    body_class: "mceBlackBody",
                                    init_instance_callback: function (e) {
                                        tinymce.activeEditor.focus();
                                        $('#'+(this).id).addClass('active');
                                        var a = (this).id;
                                        window.addEventListener('click', function(e){
                                            if(e.target != document.getElementById(a)) {
                                                $('#'+a).removeClass('active');
                                            }
                                        });
                                        // e.execCommand('mceAutoResize');
                                    },
                                    paste_data_images:false,
                                    plugins: [
                                        "advlist autolink image lists charmap print preview hr anchor pagebreak spellchecker",
                                        "link searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                                        "save table directionality emoticons template paste textcolor"
                                    ],
                                    toolbar: "fontselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
                                    font_formats: 'opensans=OpenSans,Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n'

                                });
                                setTimeout(
                                    function () {
                                        jQuery(iframe).bind('load', function () {
                                            e.preventDefault();
                                            e.stopPropagation();
                                            $(iframe).contents().find('body').trigger("focus");
                                        });
                                        $(iframe).contents().find('body').focus();
                                    }, 100);
                                setTimeout(
                                    function() {
                                        var iframeBody=$('.textareaContent iframe').contents().find('body');
                                        iframeBody.attr('p', true);
                                        $(iframeBody).on( 'drop',function() {
                                            return false;
                                        });
                                    }, 100);
                                setTimeout(
                                    function() {
                                        $(iframe).contents().find('html').click(function(){
                                            $(".textareaContent").removeClass("active");
                                            $(iframe).parent().parent().parent().parent().addClass('active');

                                        });
                                    }, 100);
                                jQuery('.contentTypes .text').bind('mouseup', function (event) {
                                    $(iframe).contents().find('body').show().focus();
                                });
                            }

                            $('.add_content').html('');
                            $('.add_content').append('<div class= "add_line"></div>');
                        });
                        $('.contentTypes .image').click(function () {
                            $('.add_content').html('');
                            $('.add_content').append('<div class= "add_line"></div>');
                            $('.linkVideoPopup, .embedCodePopup').hide();
                            $('.divUploadFile').hide().show();
                            $('.divUploadFile > ul').removeClass('photo').removeClass('video').removeClass('attach').addClass('photo');
                        });
                        $('.contentTypes .video').click(function () {
                            $('.add_content').html('');
                            $('.add_content').append('<div class= "add_line"></div>');
                            $('.linkVideoPopup, .embedCodePopup').hide();
                            $('.divUploadFile').hide().show();
                            $('.divUploadFile > ul').removeClass('photo').removeClass('attach').removeClass('video').addClass('video');
                        });
                        $('.contentTypes .attach').click(function () {
                            $('.fileUpload').click();
                            $('.add_content').html('');
                            $('.add_content').append('<div class= "add_line"></div>');
                            $('.divUploadFile > ul').removeClass('photo').removeClass('attach').removeClass('video').addClass('attach');
                        });
                        $('.contentTypes .link').click(function () {
                            var modal = document.getElementById('mytitle');
                            var er = document.getElementById('error');
                            $('.closetitle').click(function () {
                                modal.style.display = "none";
                            });
                            window.onclick = function (event) {
                                if (event.target == modal) {
                                    modal.style.display = "none";
                                }
                            }
                            $('.add_content').html('');
                            $('.add_content').append('<div class= "add_line"></div>');

                            if (tinyMCE.activeEditor == null) {

                                $('body').removeClass('overlay2');
                                $('.linkVideoPopup').fadeOut();
                                $('.error').html( '<?PHP echo JText::_('COM_JOOMDLE_EMPTY_HIGHTPERLINK')?>');
                                modal.style.display = "block";
                                // $('.lgt-notif-error').html('There is not any textbox in your page content.').addClass('lgt-visible');
                                return;
                            }
                            var highlighted = tinyMCE.activeEditor.selection.getContent();
                            if (highlighted) {
                                $('.hyperlinkPopup').show();
                                $('.txtHyperlink').val('http://');
                                $('body').addClass('overlay2');
                            } else {
                                $('body').removeClass('overlay2');
                                $('.linkVideoPopup').fadeOut();
                                $('.error').html( '<?PHP echo JText::_('COM_JOOMDLE_EMPTY_HIGHTPERLINK')?>');
                                modal.style.display = "block";
                            }
                        });
                    }
                });
                $('.embedCodePopup .btAdd').click(function () {
                    var modal = document.getElementById('mytitle');
                    var er = document.getElementById('error');
                    $('.closetitle').click(function () {
                        modal.style.display = "none";
                    });
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                    var vid;
                    if ($('.txtaEmbedCode').val() != '') {
                        var code = $('.txtaEmbedCode').val();
                        var act = 'addFilevideo';
                        a = code.indexOf('src');

                        b = code.lastIndexOf('\"');
                        if (a == -1)
                            c = code;
                        else
                            c = code.substr(a + 5, 41);
                        // alert(c);
                        if ((c.indexOf('youtu.be') != -1) || (c.indexOf('youtube.com') != -1)) {
                            if (c.indexOf('embed') != -1) {
                                var c = c.substr((c.indexOf('embed/') + 6), 17);
                                vid = "https://www.youtube.com/watch?v=" + c;
                            } else if (c.indexOf('youtu.be/') != -1) {
                                vid = c;
                            }

                        } else if (c.indexOf('vimeo.com/video/') != -1) {
                            c = c.substr((c.indexOf('vimeo.com/video/') + 16),9);
                            vid = "https://player.vimeo.com/video/"+c;

                        } else if ((c.indexOf('dai.ly') != -1) || (c.indexOf('dailymotion.com') != -1)) {
                            if (c.indexOf('dai.ly/') != -1) {
                                vid = "https:" + c;
                            } else if (c.indexOf('dailymotion.com/') != -1) {
                                vid = "https:" + c;
                            } else {
                                $('body').removeClass('overlay2');
                                $('.embedCodePopup').fadeOut();
                                $('.error').html( 'Invalid video code');
                                modal.style.display = "block";
                                return;
                            }
                        }
                        else{
                            $('body').removeClass('overlay2');
                            $('.embedCodePopup').fadeOut();
                            $('.error').html( 'Invalid video code');
                            modal.style.display = "block";
                            return;
                        }
                        $.ajax({
                            url: window.location.href,
                            type: 'POST',
                            data: {
                                act: act,
                                url: vid
                            },
                            beforeSend: function () {
                                $('.linkVideoPopup, .embedCodePopup, .divUploadFile, .hyperlinkPopup').hide();
                                lgtCreatePopup('', {'content': 'Loading...'});
                            },
                            success: function (data, textStatus, jqXHR) {
                                lgtRemovePopup();
                                var res = JSON.parse(data);

                                if (res.error == 1) {
                                    $('.embedCodePopup').fadeOut();
                                    $('.error').html( "Invalid video code");
                                    modal.style.display = "block";
                                    return;
                                } else {
                                    i++;
                                    v++;
                                    textareaCounta='a'+v;

                                    $('.mainContent').append('<div class="moodleContent embedVideo withCaption"><div class="rm"><img src="../images/delete.png"></div>' + code + '</div>');
                                    if($('.contentTypes').hasClass('error1')){
                                        $('.contentTypes').removeClass('error1');
                                    }
                                    if($('.addResourse').hasClass('error1')){
                                        $('.addResourse').removeClass('error1');
                                    }
                                    $('.embedVideo.withCaption iframe').addClass('withCap');
                                    tinymce.init({
                                        selector: ".txtaContent",
                                        preview_styles: "OpenSans",
                                        theme: "modern",
                                        height: 80,
                                        color: "#efecec",
                                        body_class: "mceBlackBody",
                                        content_style: "body {background-color:#efecec !important;color:#555;}",
                                        plugins: [
                                            "advlist autolink image lists charmap print preview hr anchor pagebreak spellchecker",
                                            "link searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                                            "save table directionality emoticons template paste textcolor"
                                        ],
                                        toolbar: "fontselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
                                        font_formats: 'opensans=OpenSans,Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n'

                                    });
                                    $('.captionContent iframe').attr('style', 'height: 75px !important;width:100% !important');

                                    setTimeout(
                                        function() {
                                            var iframeBody=$('.captionContent iframe').contents().find('body');
                                            iframeBody.attr('p', true);
                                            $(iframeBody).on( 'drop',function() {
                                                return false;
                                            });
                                        }, 1000);
                                }

                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log('ERRORS: ' + textStatus);
                            }
                        });

                        $('.linkVideoPopup, .embedCodePopup, .divUploadFile, .hyperlinkPopup').hide();
                        $('body').removeClass('overlay2');

                    } else {
                        $('body').removeClass('overlay2');
                        $('.linkVideoPopup').fadeOut();
                        $('.error').html( 'Please add embed code to form input.');
                        modal.style.display = "block";
                        // $('.lgt-notif-error').html('Please add embed code to form input.').addClass('lgt-visible');
                    }
                });
                if ($('.moodleContent').hasClass('embedWebsite') == true) {

                    $('.view-content .contentTypes .contentType.text img').css("opacity", "0.4");
                    $('.view-content .contentTypes .contentType.image img').css("opacity", "0.4");
                    $('.view-content .contentTypes .contentType.video img').css("opacity", "0.4");
                    $('.view-content .contentTypes .contentType.link img').css("opacity", "0.4");
                    $('.view-content .contentTypes .contentType.attach img').css("opacity", "0.4");
                    $('.view-content .contentTypes .contentType.text').css("cursor", "default");
                    $('.view-content .contentTypes .contentType.image').css("cursor", "default");
                    $('.view-content .contentTypes .contentType.video').css("cursor", "default");
                    $('.view-content .contentTypes .contentType.link').css("cursor", "default");
                    $('.view-content .contentTypes .contentType.attach').css("cursor", "default");
                    $('.contentTypes .text').unbind("click");
                    $('.contentTypes .image').unbind("click");
                    $('.contentTypes .video').unbind("click");
                    $('.contentTypes .link').unbind("click");
                    $('.contentTypes .attach').unbind("click");
                    var edit_web_link = $('.iframeLinkWeb').attr('src');
                    $('.txtWebsiteLink').val(edit_web_link);
                    var is_Safari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 && navigator.userAgent && !navigator.userAgent.match('CriOS');
                    if (is_Safari) {
                        if ($('.message_safari').hasClass('hidden')) {
                            $('.message_safari').removeClass('hidden');
                        }
                    }
                }

                $('.divUploadFile ul > li.embed').click(function () {
                    $('.txtaEmbedCode').val('');
                    $('.linkVideoPopup, .embedCodePopup, .divUploadFile, .hyperlinkPopup').hide();
                    $('.embedCodePopup').show();
                    $('body').addClass('overlay2');
                });
                $('.linkVideoPopup .cl, .embedCodePopup .cl, .hyperlinkPopup .cl, .hyperlinkPopup1 .cl, .websitelinkPopup .cl').click(function () {
                    $(this).parent().hide();
                    $('body').removeClass('overlay2');
                });

                var files;
                $('.fileUpload').on('change', prepareUpload);

                function prepareUpload(event) {
                    files = event.target.files;

                    if (files.length > 0) {
                        for (var i = 0; i < files.length; i++) {
                            var file = files[i];
                            if ('size' in file) {
                                if (file.size > 100 * 1024 * 1024) {
                                    var modal = document.getElementById('mytitle');
                                    var er = document.getElementById('error');
                                    $('.closetitle').click(function () {
                                        modal.style.display = "none";
                                    });
                                    window.onclick = function (event) {
                                        if (event.target == modal) {
                                            modal.style.display = "none";
                                        }
                                    }
                                    $('body').removeClass('overlay2');
                                    //$('.embedCodePopup').fadeOut();
                                    $('.error').html( 'File size is too large (< 100mb)');
                                    modal.style.display = "block";
                                    // alert("File size is too large (< 100mb)");
                                } else {
                                    $('.ajaxUploadFile').submit();
                                }
                            }
                        }
                    }
                }

                $('.ajaxUploadFile').on('submit', uploadFiles);
                function uploadFiles(event) {
                    event.stopPropagation();
                    event.preventDefault();
                    var class_file = $('.divUploadFile > ul').attr('class');
                    var data = new FormData();
                    $.each(files, function (key, value) {
                        data.append(key, value);
                    });
                    data.append('token', '<?php echo $token;?>');
                    data.append('class', class_file);
                    $.ajax({
                        url: window.location.href,
                        type: 'POST',
                        data: data,
                        cache: false,
                        processData: false,
                        contentType: false,
                        beforeSend: function () {
                            $('.linkVideoPopup, .embedCodePopup, .divUploadFile, .hyperlinkPopup').hide();
                            lgtCreatePopup('', {'content': 'Loading...'});
                        },
                        success: function (data, textStatus, jqXHR) {
                            lgtRemovePopup();
                            $('body').removeClass('overlay2');
                            var res = JSON.parse(data);

                            var modal = document.getElementById('mytitle');
                            var er = document.getElementById('error');
                            $('.closetitle').click(function () {
                                modal.style.display = "none";
                            });
                            window.onclick = function (event) {
                                if (event.target == modal) {
                                    modal.style.display = "none";
                                }
                            }
                            if (res.error == 1) {
                                $('.error').html(res.comment);
                                modal.style.display = "block";
                            } else {
                                var imgArr = ['image/png', 'image/jpeg', 'image/gif', 'image/tiff', 'image/bmp'];
                                var videoArr = ['video/mp4', 'video/avi', 'video/wmv', 'video/mov', 'video/flv'];
                                for (var i = 0; i < res.data.length; i++) {
                                    var name = res.data[i].name;
                                    var type = res.data[i].type;
                                    var url = res.data[i].url;
                                    var itemid = res.data[i].itemid;
                                    if (res.class != "attach") {
                                        if (imgArr.indexOf(type) != -1) {
                                            $('.mainContent').append('<div class="moodleContent imageContent" data-itemid="' + itemid + '" data-filename="' + name + '"><div class="rm"><img src="../images/delete.png"></div><img src="' + url + '" alt="' + name + '" class="uploadedImage"></div><div class="cline"></div>');
                                            if($('.contentTypes').hasClass('error1')){
                                                $('.contentTypes').removeClass('error1');
                                            }
                                            if($('.addResourse').hasClass('error1')){
                                                $('.addResourse').removeClass('error1');
                                            }
                                        } else if (videoArr.indexOf(type) != -1) {

                                            i++;
                                            v++;
                                            textareaCounta = 'a' + v;

                                            $('.mainContent').append('<div class="moodleContent videoContent withCaption" data-itemid="' + itemid + '" data-filename="' + name + '"><div class="rm"><img src="../images/delete.png"></div><video class="withCap" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" oncontextmenu="return false;" frameborder="0" controls="controls" controlslist = "nodownload"> <source src="' + url + '" type="' + type + '" /></video></div>');
                                            if($('.contentTypes').hasClass('error1')){
                                                $('.contentTypes').removeClass('error1');
                                            }
                                            if($('.addResourse').hasClass('error1')){
                                                $('.addResourse').removeClass('error1');
                                            }
                                            // Trung KV - remove height="420" width="560" //

                                            tinymce.init({
                                                selector: ".txtaContent",
                                                preview_styles: "OpenSans",
                                                theme: "modern",
                                                height: 80,
                                                color: "#efecec",
                                                body_class: "mceBlackBody",
                                                //   content_style: "body {background-color:#efecec !important;color:#555;}",
                                                plugins: [
                                                    "advlist autolink image lists charmap print preview hr anchor pagebreak spellchecker",
                                                    "link searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                                                    "save table directionality emoticons template paste textcolor"
                                                ],
                                                toolbar: "fontselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
                                                font_formats: 'opensans=OpenSans,Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n'

                                            });
                                            $('.captionContent iframe').attr('style', 'height: 75px !important;width:100% !important');

                                            setTimeout(
                                                function() {
                                                    var iframeBody=$('.captionContent iframe').contents().find('body');
                                                    iframeBody.attr('p', true);
                                                    $(iframeBody).on( 'drop',function() {
                                                        return false;
                                                    });
                                                }, 1000);
                                        }else
                                            $('.mainContent').append('<div class="moodleContent fileContent" data-itemid="' + itemid + '" data-filename="' + name + '"><div class="rm"><img src="../images/delete.png"></div><div class="uploadedFile"><a target="_blank" href="' + url + '">' + name + '</a></div></div><div class="cline"></div>');
                                        if($('.contentTypes').hasClass('error1')){
                                            $('.contentTypes').removeClass('error1');
                                        }
                                        if($('.addResourse').hasClass('error1')){
                                            $('.addResourse').removeClass('error1');
                                        }
                                    } else {
                                        var iconUrl;
                                        var baseUrl = '<?php echo JURI::base();?>';
                                        var fname;
                                        if (name.length > 23) fname = name.substr(0, 23) + '...'; else fname = name;
                                        var namef;
                                        var namefi = res.namef;
                                        if (namefi.length > 23) namef = namefi.substr(0, 23) + '....'+res.ext; else namef = namefi;
                                        switch (res.data[i].type) {
                                            case "text/csv":
                                                iconUrl = baseUrl + '/images/icons/attach_csv30x30.png';
                                                break;
                                            case "application/msword":
                                                iconUrl = baseUrl + '/images/icons/attach_doc30x30.png';
                                                break;
                                            case "application/pdf":
                                                iconUrl = baseUrl + '/images/icons/attach_pdf30x30.png';
                                                break;
                                            case "application/mspowerpoint":
                                            case "application/powerpoint":
                                            case "application/vnd.ms-powerpoint":
                                            case "application/x-mspowerpoint":
                                                if (name.indexOf('ppt') !== -1) iconUrl = baseUrl + '/images/icons/attach_ppt30x30.png';
                                                if (name.indexOf('pps') !== -1) iconUrl = baseUrl + '/images/icons/attach_pps30x30.png';
                                                break;
                                            case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                                                iconUrl = baseUrl + '/images/icons/attach_ppt30x30.png';
                                                break;
                                            case "application/rtf":
                                            case "application/x-rtf":
                                            case "text/richtext":
                                                iconUrl = baseUrl + '/images/icons/attach_rtf30x30.png';
                                                break;
                                            case "image/svg+xml":
                                                iconUrl = baseUrl + '/images/icons/attach_svg30x30.png';
                                                break;
                                            case "text/plain":
                                                iconUrl = baseUrl + '/images/icons/attach_txt30x30.png';
                                                break;
                                            case "application/excel":
                                            case "application/vnd.ms-excel":
                                            case "application/x-excel":
                                            case "application/x-msexcel":
                                                iconUrl = baseUrl + '/images/icons/attach_xls30x30.png';
                                                break;
                                            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                                                iconUrl = baseUrl + '/images/icons/attach_xls30x30.png';
                                                break;
                                            case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                                                iconUrl = baseUrl + '/images/icons/attach_doc30x30.png';
                                                break;
                                            default:
                                                iconUrl = baseUrl + '/media/joomdle/images/icon/courseoutline/CourseContent_Inactive.png';
                                                break;
                                        }

                                        if(res.data[i].type != "application/pdf"){
                                            $('.mainContent').append('<div class="moodleContent fileContent" data-itemid="' + itemid + '" data-filename="' + namef + '"><div class="rm"><img src="../images/delete.png"></div><div class="uploadedFile"><a style = "cursor: pointer;"  onclick = downloadFile("' + url + '")   ><img src="' + iconUrl + '">' + namef + '</a></div></div><div class="cline"></div>');
                                            if($('.contentTypes').hasClass('error1')){
                                                $('.contentTypes').removeClass('error1');
                                            }
                                            if($('.addResourse').hasClass('error1')){
                                                $('.addResourse').removeClass('error1');
                                            }
                                        }else{
                                            $('.mainContent').append('<div class="moodleContent fileContent" data-itemid="' + itemid + '" data-filename="' + namef + '"><div class="rm"><img src="../images/delete.png"></div><div class="uploadedFile"><a target="_blank" href="' + url + '"><img src="' + iconUrl + '">' + namef + '</a></div></div><div class="cline"></div>');
                                            if($('.contentTypes').hasClass('error1')){
                                                $('.contentTypes').removeClass('error1');
                                            }
                                            if($('.addResourse').hasClass('error1')){
                                                $('.addResourse').removeClass('error1');
                                            }
                                        }
                                    }
                                }

                                // $('.lgt-notif').removeClass('lgt-visible');

                                var $el = $('.fileUpload');
                                $el.wrap('<form>').closest('form').get(0).reset();
                                $el.unwrap();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log('ERRORS: ' + textStatus);
                        }
                    });
                }

            })(jQuery);
            function hasGetUserMedia() {
                return !!(navigator.getUserMedia || navigator.webkitGetUserMedia ||
                    navigator.mozGetUserMedia || navigator.msGetUserMedia);
            }
            function takeSnapshot() {
                document.getElementsByClassName('lgt-notif')[0].innerHTML = 'Say cheese!!!';
                document.getElementsByClassName('lgt-notif')[0].className += ' lgt-visible';

                Webcam.snap(function (data_uri) {
                    // upload to server
                    Webcam.upload(data_uri, window.location.href, function (code, text) {
                        // display results in page
                        console.log(text);
                        var div = document.getElementById('results');
                        var res = JSON.parse(text);
                        for (var i = 0; i < res.data.length; i++) {
                            var name = res.data[i].name;
                            var type = res.data[i].type;
                            var url = res.data[i].url;
                            var itemid = res.data[i].itemid;
                            jQuery('#mainContent').append('<div class="moodleContent imageContent" data-itemid="' + itemid + '" data-filename="' + name + '"><div class="rm"><img src="../images/delete.png"></div><img src="' + url + '" alt="' + name + '" class="uploadedImage" /></div><div class="cline"></div>');


                            document.getElementsByClassName('lgt-notif')[0].classList.remove("lgt-visible");
                            Webcam.reset();
                            document.getElementById('results').remove();
                        }
                    });
                });
            }

            <?php
            if ($edit && empty($this->content['params']) && !empty($this->content['content'])) {
                echo 'alert(\'This activity is too old. Please refill its content.\');';
            }
            ?>

        </script>
    <?php } else if (isset($this->action) && $this->action == 'remove') {
        if ($this->content['error'] != 1) {
            ?>
            <div class="joomdle-remove">
                <p><?php echo Jtext::_('COM_JOOMDLE_REMOVE_CONTENT_TEXT'); ?></p>
                <button class="yes"><?php echo Jtext::_('COM_JOOMDLE_YES'); ?></button>
                <button class="no"><?php echo Jtext::_('COM_JOOMDLE_NO'); ?></button>
            </div>
            <div class="notification"></div>

            <script type="text/javascript">
                (function ($) {
                    $('.joomdle-remove .no').click(function () {
                        window.location.href = "<?php echo JUri::base() . "content/list_" . $this->course_id . '.html'; ?>";
                    });
                    $('.joomdle-remove .yes').click(function () {
                        var act = 'remove';
                        $.ajax({
                            url: window.location.href,
                            type: 'POST',
                            data: {
                                <?php echo (isset($this->pid)) ? "pid: " . $this->pid . "," : ""; ?>
                                act: act
                            },
                            beforeSend: function () {
                                lgtCreatePopup('', {'content': 'Loading...'});
                            },
                            success: function (data, textStatus, jqXHR) {
                                lgtRemovePopup();
                                var res = JSON.parse(data);

                                if (res.error == 1) {
                                    $('.linkVideoPopup').fadeOut();
                                    $('.error').html(res.comment);
                                    modal.style.display = "block";
                                    //$('.lgt-notif-error').html(res.comment).addClass('lgt-visible');
                                } else {
                                    window.location.href = "<?php echo JUri::base() . "content/list_" . $this->course_id . '.html'; ?>";
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log('ERRORS: ' + textStatus);
                            }
                        });
                    });

                })(jQuery);

            </script>
            <?php
        } else {
            echo '<p class="tcenter" style="padding-top:30px;">' . JText::_('COM_JOOMDLE_CONTENT_REMOVED') . '</p>';
        }
    } else if (isset($this->mods) && is_array($this->mods)) {
        $jump_url = JoomdleHelperContent::getJumpURL();
        $count = 0;
        ?>
        <div class="page-list">
            <p class="lblTitle"><?php echo JText::_('COM_JOOMDLE_CONTENT'); ?><span class="contentCount"></span></p>
            <?php
            foreach ($this->mods as $tema) :
                ?>
                <div class="joomdle_course_section">
                    <div class="joomdle_item_content joomdle_section_list_item_resources">
                        <?php
                        $resources = $tema['mods'];
                        if (is_array($resources)) : ?>
                            <?php
                            foreach ($resources as $id => $resource) {
                                $mtype = JoomdleHelperSystem::get_mtype($resource['mod']);
                                if (!$mtype) // skip unknow modules
                                    continue;

                                echo '<div class="content-list-item" data-mid="' . $resource['id'] . '">';
                                $icon_url = JoomdleHelperSystem::get_icon_url($resource['mod'], $resource['type']);
                                if ($icon_url) {
                                    echo '<div class="content-icon">';
                                    // echo '<img align="center" src="'. $icon_url.'">';
                                    echo '<img align="center" src="/media/joomdle/images/ContentIcon.png">';
                                    //echo '<p class="assessment-name">'.ucfirst($mtype).'</p>';
                                    echo '</div>';
                                }

                                if ($resource['mod'] == 'label') {
                                    $label = JoomdleHelperContent::call_method('get_label', $resource['id']);
                                    echo '</P>';
                                    echo $label['content'];
                                    echo '</P>';
                                    continue;
                                }

                                if (($resource['available'])) {
                                    $direct_link = JoomdleHelperSystem::get_direct_link($resource['mod'], $this->course_id, $resource['id'], $resource['type']);
                                    if ($direct_link) {
                                        // Open in new window if configured like that in moodle
                                        if ($resource['display'] == 6)
                                            $resource_target = 'target="_blank"';
                                        else
                                            $resource_target = '';

                                        if ($direct_link != 'none')
                                            echo "<a $resource_target  href=\"" . $direct_link . "\">" . $resource['name'] . "</a>";
                                    } else
                                        echo "<a $target href=\"" . JURI::base() . "mod/" . $mtype . "_" . $this->course_id . "_" . $resource['id'] . "_" . $itemid . ".html" . "\">" . $resource['name'] . "</a>";
                                    ?>
                                    <div class="arrowMenuDown">
                                        <span class="glyphicon glyphicon-menu-down"></span>
                                        <div class="menuAction">
                                            <ul class="ulMenuAction">
                                                <li class="liEdit">
                                                    <div class="icon-pencil"></div>
                                                    <span><?php echo JText::_('COM_JOOMDLE_EDIT_CONTENT'); ?></span>
                                                </li>
                                                <li class="liRemove">
                                                    <div class="icon-delete"></div>
                                                    <span><?php echo JText::_('COM_JOOMDLE_REMOVE_CONTENT'); ?></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="triangle"></div>
                                    </div>
                                    <?php
                                    echo '</div>';
                                } else {
                                    echo $resource['name'] . '</div>';
                                    if ((!$resource['available']) && ($resource['completion_info'] != '')) : ?>
                                        <div class="joomdle_completion_info">
                                            <?php echo $resource['completion_info']; ?>
                                        </div>
                                    <?php
                                    endif;
                                }
                                $count++;
                            }
                            ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach;
            if ($count == 0) {
                // echo '<p class="lblTitle">'.JText::_('COM_JOOMDLE_CONTENT').'</p>';
                echo '<p class="textNoContentpage">' . JText::_('COM_JOOMDLE_NO_CONTENT') . '</p>';
            }
            ?>
        </div>
        <div class="joomdle-remove">
            <p><?php echo Jtext::_('COM_JOOMDLE_REMOVE_CONTENT_TEXT'); ?></p>
            <button class="yes"><?php echo Jtext::_('COM_JOOMDLE_YES'); ?></button>
            <button class="no"><?php echo Jtext::_('COM_JOOMDLE_NO'); ?></button>
        </div>
        <div class="notification"></div>
        <script type="text/javascript">
            (function ($) {
                <?php //if ($count > 0) { ?>
                $('.page-list .contentCount').html(' (<?php echo $count; ?>)');
                <?php //} ?>
                $('.btAddContent').click(function () {
                    window.location.href = "<?php echo JUri::base() . "content/create_" . $this->course_id . ".html"; ?>";
                });

                $('.liEdit').click(function () {
                    var mid = $(this).parent().parent().parent().parent('.content-list-item').attr('data-mid');
                    window.location.href = "<?php echo JURI::base();?>content/edit_<?php echo $this->course_id;?>_" + mid + ".html";
                });
                var removeMid;
                $('.liRemove').click(function () {
                    removeMid = $(this).parent().parent().parent().parent('.content-list-item').attr('data-mid');
                    $('body').addClass('overlay2');
                    $('.joomdle-remove').fadeIn();
                });

                $('.page-list .arrowMenuDown').click(function () {
                    $('.menuAction').hide();
                    $('.triangle').hide();
                    $(this).find('.menuAction').toggle();
                    $(this).find('.triangle').toggle();
                });
                $('body').click(function (e) {
                    if (!$(e.target).hasClass('ulMenuAction') && !$(e.target).hasClass('menuAction') && !$(e.target).hasClass('arrowMenuDown') && !$(e.target).hasClass('glyphicon-menu-down')) $('.menuAction, .triangle').hide();
                });

                $('.joomdle-remove .no').click(function () {
                    $('body').removeClass('overlay2');
                    $('.joomdle-remove').fadeOut();
                });
                $('.joomdle-remove .yes').click(function () {
                    var modal = document.getElementById('mytitle');
                    var er = document.getElementById('error');
                    $('.closetitle').click(function () {
                        modal.style.display = "none";
                    });
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                    var act = 'remove';
                    $.ajax({
                        url: window.location.href,
                        type: 'POST',
                        data: {
                            pid: removeMid,
                            act: act
                        },
                        beforeSend: function () {
                            $('.joomdle-remove').fadeOut();
                            lgtCreatePopup('', {'content': 'Loading...'});
                        },
                        success: function (data, textStatus, jqXHR) {
                            lgtRemovePopup();
                            var res = JSON.parse(data);

                            if (res.error == 1) {
                                $('body').removeClass('overlay2');
                                $('.linkVideoPopup').fadeOut();
                                er.innerHTML = res.comment;
                                modal.style.display = "block";
                                //$('.lgt-notif-error').html(res.comment).addClass('lgt-visible');
                            } else {
                                $('.joomdle_course_section .content-list-item[data-mid="' + removeMid + '"]').fadeOut();
                                var str = $('.contentCount').html();
                                str = str.replace("(", "");
                                str = str.replace(")", "");
                                str = parseInt(str) - 1;
                                $('.contentCount').html('(' + str + ')');
                            }
                        }
                    });
                });
            })(jQuery);
        </script>
        <?php
    }
endif; ?>
<script>
    (function($) {

        $(document).ready(function(){
            $(".txtTitle").focus(function(){
                $(".txtTitle").addClass("active");
            });

            $(".txtTitle").focusout(function(){
                $(".txtTitle").removeClass("active");
            });
            $(".txtHyperlink").focus(function(){
                $(".txtHyperlink").addClass("active");
            });

            $(".txtHyperlink").focusout(function(){
                $(".txtHyperlink").removeClass("active");
            });
        });
    })(jQuery);
</script>