<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
$itemid = JoomdleHelperContent::getMenuItem();

$linkstarget = $this->params->get( 'linkstarget' );
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$session  = JFactory::getSession();

if ($session->get('device') == 'mobile') {
    ?>
    <style type="text/css">
        .navbar-header {
            background-color: #126db6;
        }
    </style>
<?php
}
require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
?>
<?php if ($this->hasPermission[0]['hasPermission']) : ?>
    <?php if ( (isset($this->action) && $this->action == 'create') || (isset($this->action) && $this->action == 'edit') ) {
        $edit = ($this->action == 'edit') ? true : false;
        ?>
        <div class="joomdle-add-section <?php echo $this->pageclass_sfx?>">
            <h1><?php echo JText::_('COM_JOOMDLE_TOPIC'); ?></h1>
            <p><?php echo Jtext::_('COM_JOOMDLE_TOPIC_TEXT1'); ?></p>
            <p><?php echo Jtext::_('COM_JOOMDLE_TOPIC_TEXT2'); ?></p>
            <form action="" method="post" class="formAddSection" enctype="multipart/form-data">
                <label for=""><?php echo Jtext::_('COM_JOOMDLE_TITLE'); ?><span class="red">*</span></label>
                <input type="text" name="txtTitle" class="txtTitle" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_TITLE');?>" value="<?php echo ($edit) ? $this->topic['name'] : '';?>"/>
                <label for=""><?php echo Jtext::_('COM_JOOMDLE_DESCRIPTION'); ?></label>
                <textarea <?php echo (!$edit) ? 'data-ft="1"' : '';?> name="txtaDescription" class="txtaDescription" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_DESCRIPTION'); ?>"/><?php echo ($edit) ? strip_tags($this->topic['summary']) : '';?></textarea>
                <label for=""><?php echo Jtext::_('COM_JOOMDLE_LEARNING_OUTCOMES'); ?></label>
                <textarea <?php echo (!$edit) ? 'data-ft="1"' : '';?> name="txtaLearningOutcomes" class="txtaLearningOutcomes" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_LEARNING_OUTCOMES'); ?>"/><?php echo ($edit) ? strip_tags($this->topic['learningoutcomes']) : ''; ?></textarea>

                <div class="clear"></div>
                <div class="buttons">
                    <button class="btCancel" type="button"><?php echo Jtext::_('SCORM_BTN_CANCEL'); ?></button>
                    <button class="btPreview" type="button"><?php echo Jtext::_('SCORM_BTN_PREVIEW'); ?></button>
                    <button class="btSave"><?php echo Jtext::_('SCORM_BTN_SAVE'); ?></button>
                </div>
            </form>
            <div class="notification">
            </div>
            <!--            <div class="divPreviewScorm">-->
            <!--                <p class="previewTitle"></p>-->
            <!--                <div class="previewScorm">--><?php //echo JText::_('COM_JOOMDLE_SCORM_PREVIEW'); ?><!--</div>-->
            <!--                <p class="previewDescription"></p>-->
            <!--                <button class="previewBack">--><?php //echo JText::_('COM_JOOMDLE_BACK'); ?><!--</button>-->
            <!--            </div>-->
        </div>

        <script>
            (function($) {
                $('.formAddSection').submit(function(e) {
                    e.preventDefault();
                });

                $('.formAddSection .btCancel').click(function() {
                    window.location.href = '<?php echo JUri::base()."course/".$this->course_id; ?>.html';
                });

                $('.formAddSection .btPreview').click(function() {
                    $('.divPreviewScorm').addClass('visible');
                    if ($('.txtTitle').val() == '') {
                        $('.divPreviewScorm .previewTitle').html('No title');
                    } else {
                        $('.divPreviewScorm .previewTitle').html($('.txtTitle').val());
                    }

                    $('.divPreviewScorm .previewDescription').html($('.txtaDescription').val());

                    $('.joomdle-add-scorm > *').not('.divPreviewScorm').fadeOut();
                });

                $('.divPreviewScorm .previewBack').click(function() {
                    $('.divPreviewScorm').removeClass('visible');
                    $('.joomdle-add-scorm > *').not('.divPreviewScorm').fadeIn();
                });

                $('.formAddSection .btSave').click(function() {
                    if ($('.txtTitle').val() == '') {
                        alert('Empty Title');
                    } else {
                        $.ajax({
                            url: window.location.href,
                            type: 'post',
                            data: {
                                'createSection':<?php echo (isset($this->action) && $this->action == 'create') ? 1 : 2; ?>,
                                'title': $('.txtTitle').val(),
                                'des': $('.txtaDescription').val(),
                                'lo': $('.txtaLearningOutcomes').val()
                            },
                            beforeSend: function() {
                                $('body').addClass('overlay2');
                                $('.notification').html('Loading...').fadeIn();
                            },
                            success: function(res) {
                                window.location.href = '<?php echo JUri::base().'course/'.$this->course_id.'.html'?>';
                            }
                        });
                    }
                });

            })(jQuery);
        </script>
    <?php } else if (isset($this->action) && $this->action == 'list') {
        ?>
        <div class="topics-list">
            <p class="lblTitle"><?php echo Jtext::_('COM_JOOMDLE_TOPIC'); ?><span class="topicsCount"><?php echo ' ('.count($this->topics['sections']).')'; ?></span></p>
            <?php
            foreach ($this->topics['sections'] as $topic) :
                if ($topic['section'] > $this->topics['numsections']) continue;
                ?>
                <div class="joomdle_sections">
                    <div class="joomdle_section-item" data-sid="<?php echo $topic['id'];?>">
                        <div class="sectionIcon">
                            <img src="/media/joomdle/images/Topicicon.png">
                        </div>
                        <div class="sectionName">
                            <?php echo ($topic['name'] !== null) ? $topic['name'] : JText::_('COM_JOOMDLE_TOPIC').' '.$topic['section'] ;?>
                        </div>
                        <div class="arrowMenuDown">
                            <span class="glyphicon glyphicon-menu-down"></span>

                            <div class="menuAction">
                                <ul class="ulMenuAction">
                                    <li class="liEdit">
                                        <div class="icon-pencil"></div>
                                        <span><?php echo Jtext::_('COM_JOOMDLE_EDIT_TOPIC'); ?></span>
                                    </li>
                                    <li class="liRemove">
                                        <div class="icon-delete"></div>
                                        <span><?php echo Jtext::_('COM_JOOMDLE_REMOVE_TOPIC'); ?></span>
                                    </li>
                                </ul>
                                <!--                                <div class="triangle"></div>-->
                            </div>
                            <div class="triangle"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            <?php endforeach;
            if (count($this->topics['sections']) == 0) {
                echo '<p class="textNoTopic">'.JText::_('COM_JOOMDLE_NO_TOPIC').'</p>';
            }
            ?>
            <!-- <div class="divAddSection">
                <button class="btAddSection">+<?php //echo Jtext::_('SCORM_BTN_ADD'); ?></button>
            </div> -->
        </div>
        <script type="text/javascript">
            (function($) {
                $('.liEdit').click(function() {
                    var sid = $(this).parent().parent().parent().parent().attr('data-sid');
                    window.location.href = "<?php echo JUri::base()."topics/edit_".$this->course_id; ?>_"+sid+".html";
                });
                $('.liRemove').click(function() {
                    var sid = $(this).parent().parent().parent().parent().attr('data-sid');
                    window.location.href = "<?php echo JUri::base()."topics/remove_".$this->course_id; ?>_"+sid+".html";
                });
                $('.topics-list .joomdle_section-item .arrowMenuDown').click(function() {
                    $('.menuAction').hide();
                    $('.triangle').hide();
                    $(this).find('.menuAction').toggle();
                    $(this).find('.triangle').toggle();
                });
                $('body').click(function(e) {
                    if ( !$(e.target).hasClass('ulMenuAction') && !$(e.target).hasClass('menuAction') && !$(e.target).hasClass('arrowMenuDown') && !$(e.target).hasClass('glyphicon-menu-down') ) $('.menuAction, .triangle').hide();
                });
                $('.btAddSection').click(function() {
                    window.location.href = "<?php echo JUri::base()."topics/create_".$this->course_id; ?>.html";
                });
            })(jQuery);
        </script>
    <?php
    } else if (isset($this->action) && $this->action == 'remove') {
        ?>
        <div class="joomdle-remove-question">
            <p><?php echo Jtext::_('COM_JOOMDLE_REMOVE_TOPIC_TEXT'); ?></p>
            <button class="yes"><?php echo Jtext::_('COM_JOOMDLE_YES'); ?></button>
            <button class="no"><?php echo Jtext::_('COM_JOOMDLE_NO'); ?></button>
        </div>
        <div class="notification">
        </div>
        <script type="text/javascript">
            (function($) {
                $('.joomdle-remove-question .no').click(function() {
                    window.location.href = "<?php echo JUri::base()."course/".$this->course_id; ?>.html";
                });
                $('.joomdle-remove-question .yes').click(function() {
                    $.ajax({
                        'url': window.location.href,
                        'type' : 'POST',
                        'data' : {'acceptRemove' : 1},
                        beforeSend: function() {
                            $('body').addClass('overlay2');
                            $('.notification').html('Loading...').fadeIn();
                        },
                        'success': function(res) {
                            window.location.href = '<?php echo JUri::base().'course/'.$this->course_id.'.html'?>';
                        }
                    });
                });

            })(jQuery);
        </script>
    <?php
    }
endif;?>
