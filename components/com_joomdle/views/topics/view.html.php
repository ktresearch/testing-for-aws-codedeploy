<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewTopics extends JViewLegacy {
    function display($tpl = null) {
        global $mainframe;

        $app = JFactory::getApplication();
        $pathway = $app->getPathWay();
        $document = JFactory::getDocument();
        $menus = $app->getMenu();
        $menu = $menus->getActive();

        $params = $app->getParams();
        $this->assignRef('params', $params);
        $user = JFactory::getUser();
        $username = $user->username;

        $id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
        if (!$id) $id = JRequest::getVar( 'course_id' );
        if (!$id) $id = $params->get( 'course_id' );
        if (!$id) {
            echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
            return;
        }
        $id = (int) $id;
        $this->hasPermission = JFactory::hasPermission($id, $username);
        if (!$this->hasPermission[0]['hasPermission']) {
            echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
            return;
        }
        $this->course_id = $id;
        $action = JRequest::getVar( 'action', null, 'NEWURLFORM' );
        if (!$action) $action = JRequest::getVar( 'action' );

        $this->course_info = JoomdleHelperContent::getCourseInfo($id, $username);
        $this->course_status = $this->course_info['course_status'];
        if ($action == 'create' || $action == 'edit' || $action == 'remove') {
            if ($this->course_status == 'pending_approval' || $this->course_status == 'approved') {
                echo JText::_('COM_JOOMDLE_COURSE_APPROVED_OR_PENDING_APPROVAL');
                return;
            }
            if ($this->course_info['self_enrolment']) {
                echo JText::_('COM_JOOMDLE_COURSE_PUBLISHED');
                return;
            }
        }

        if ($action =='edit' || $action=='remove') {
            $sid = JRequest::getVar( 'sid', null, 'NEWURLFORM' );
            if (!$sid) $sid = JRequest::getVar( 'sid' );
            if ($sid) $this->sid = (int) $sid;
        }

        switch ($action) {
            case 'list':
                $document->setTitle(JText::_('COM_JOOMDLE_TOPIC'));
//                $this->topics = JoomdleHelperContent::call_method ( 'update_course_section', (int) $id, 0);
                $topics = JoomdleHelperContent::call_method ( 'update_course_section', (int) $id, 0);
                if ($topics['status']) {
                    $this->topics = $topics['datas'];
                } else {
                    $this->topics = array();
                }
                $this->action = 'list';
                break;
            case 'create':
                $document->setTitle(JText::_('COM_JOOMDLE_CREATE_TOPIC'));
                $this->action = 'create';

                if (isset($_POST['createSection']) && $_POST['createSection'] == 1) {
                    $create = 1;
                    $res = JoomdleHelperContent::call_method ( 'update_course_section', (int) $id, $create, $_POST['title'], $_POST['des'], $_POST['lo']);
                    echo json_encode($res);
                    die;
                }
                break;
            case 'edit':
                $document->setTitle(JText::_('COM_JOOMDLE_EDIT_TOPIC'));
                $this->action = 'edit';
                if (isset($_POST['createSection']) && $_POST['createSection'] == 2) {
                    $edit = 2;
                    $res = JoomdleHelperContent::call_method ( 'update_course_section', (int) $id, $edit, $_POST['title'], $_POST['des'], $_POST['lo'], (int)$sid);
                } else {
                    $view = 0;
//                    $this->topics = JoomdleHelperContent::call_method ( 'update_course_section', (int) $id, 0);
                    $topics = JoomdleHelperContent::call_method ( 'update_course_section', (int) $id, 0);
                    if ($topics['status']) {
                        $this->topics = $topics['datas'];
                    } else {
                        $this->topics = array();
                    }
                    foreach ($this->topics['sections'] as $topic) {
                        if ($topic['id'] == $sid) {
                            $this->topic = $topic;
                            break;
                        }
                    }
                }
                break;
            case 'remove':
                $document->setTitle(JText::_('COM_JOOMDLE_REMOVE_TOPIC'));
                if (isset($_POST['acceptRemove']) && $_POST['acceptRemove'] == 1) {
                    $remove = 3;
                    $res = JoomdleHelperContent::call_method ( 'update_course_section', (int) $id, $remove, null, null, null, (int)$sid);
                }

                $this->action = 'remove';
                break;
            default:
                break;
        }

        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

        parent::display($tpl);
    }
}
?>
