<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewCourseprogram extends JViewLegacy {
    function display($tpl = null) {
        $user = JFactory::getUser();
        $username = $user->username;

        $app = JFactory::getApplication();
        $params = $app->getParams();

        $this->assignRef('params', $params);
    
//		if (($show_buttons) && ($username))
        if ($username)
        {          
            $a =array();
            $i= 0;
            $programid = JRequest::getVar( 'programid', null, 'NEWURLFORM' );
            if (!$programid) $programid =  JRequest::getInt('programid', 0);
            $this->cursos = JoomdleHelperContent::call_method ("my_course_program", $username, (int)$programid); 
      
            foreach ($this->cursos as $b)
            {
                $name_program = $b['cat_name'];
                $a[$i] = $b;
                $i++;
            }
          
           $c = array_unique(array($a));
    
//            if (is_array ($this->cursos) && !empty($this->cursos)) {
//                $i = 1;
//                $recently_accessed_arr = array();
//                $not_yet_started_arr = array();
//                $completed_arr = array();
//                foreach ($this->cursos as  $curso) :
//                    if ($curso['time_completed']>0)
//                    {
//                         $completed_arr[] = $curso;
//                    } elseif ($curso['last_access']>0) {
//                        $recently_accessed_arr[] = $curso;
//                    } else {
//                        $not_yet_started_arr[] = $curso;
//                    }
///**                    switch ($curso['completion_status']) {
////                        case null:
////                            if  (array_key_exists('last_access', $curso) && !is_null($curso['last_access'])) {
////                                $completion_class = "in-progress";
////                                $completion_status = 'In progress';
////                                (array_key_exists('last_access', $curso) && !is_null($curso['last_access'])) ? $time = date('Y-m-d', $curso['last_access']) : 'Unknown';
////                                $completion_des = 'Last viewed: '.$time;
////                                break;
////                            } else {
////                                $completion_class = "not-yet-started";
////                                $completion_status = 'Not yet started';
////                                (array_key_exists('enrol_time', $curso) && !is_null($curso['enrol_time'])) ? $time = date('Y-m-d', $curso['enrol_time']) : 'Unknown';
////                                $completion_des = 'Subscribed: '.$time;
////                                break;
////                            }
////                        case '25':
////                        case '50':
////                        case '75':
////                            $completion_class = "completed";
////                            $completion_status = 'Completed';
////                            (array_key_exists('time_completed', $curso) && !is_null($curso['time_completed'])) ? $time = date('Y-m-d', $curso['time_completed']) : 'Unknown';
////                            $completion_des = 'Completed: '.$time;
////                            break;
////                        default:
////                            $completion_class = "not-yet-started";
////                            $completion_status = 'Not yet started';
////                            (array_key_exists('enrol_time', $curso) && !is_null($curso['enrol_time'])) ? $time = date('Y-m-d', $curso['enrol_time']) : 'Unknown';
////                            $completion_des = 'Subscribed: '.$time;
////                            break;
////                    } **/
//                    $i++;
//                endforeach;
//
//                $this->recently_accessed = $recently_accessed_arr;
//                $this->not_yet_started = $not_yet_started_arr;
//                $this->completed = $completed_arr;
//
//            }
        }
       
        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

        $this->_prepareDocument();

        parent::display($tpl);
    }

    protected function _prepareDocument()
    {
        $app    = JFactory::getApplication();
        $menus  = $app->getMenu();
        $title  = null;

        // Because the application sets a default page title,
        // we need to get it from the menu item itself
        $menu = $menus->getActive();
        if ($menu)
        {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        } else {
            $this->params->def('page_heading', JText::_('COM_JOOMDLE_MY_NEWS'));
        }
    }

}

?>
