<?php defined('_JEXEC') or die('Restricted access'); ?>
<script src="components/com_joomdle/js/jquery-2.1.1.min.js" type="text/javascript"></script>
<?php
$itemid = JoomdleHelperContent::getMenuItem();
$free_courses_button = $this->params->get('free_courses_button');
$paid_courses_button = $this->params->get('paid_courses_button');
$show_buttons = $this->params->get('show_buttons');
$show_description = $this->params->get('show_description');
$session = JFactory::getSession();
$device = $session->get('device');

function custom_echo($x, $length)
{
    if (strlen($x) <= $length) {
        return $x;
    } else {
        $y = substr($x, 0, $length) . '...';
        return $y;
    }
}
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'components/com_joomdle/css/swiper.min.css');
$document->addScript(JUri::root() . 'components/com_joomdle/js/swiper.min.js');
$document->addStyleSheet(JUri::root() . '/templates/t3_bs3_tablet_desktop_template/css/themes/template.css');
foreach ($this->cursos as $b) {
    $name_program = $b['cat_name'];
}

//if ($device == 'mobile') {
//    $device = "";
//    $display_mobie = "";
//    $display_tablet = "display:none";
//} else {
//    $display_mobie = "display:none";
//    $display_tablet = "";
//    $device = "-tablet";
//    $class_tablet = 'course-box';
//    ?>
<!--    <style type="text/css">-->
<!--        .com_joomdle .home {-->
<!--            display: none;-->
<!--        }-->
<!---->
<!--        body {-->
<!---->
<!--            background: #e0e0e0;-->
<!---->
<!--        }-->
<!---->
<!--        .t3-content {-->
<!--            padding-left: 0px;-->
<!--            padding-right: 0px;-->
<!--        }-->
<!--    </style>-->
<?php //} ?>
<?php if ($device == 'mobile') {
    ?>
    <div class="joomdle-mylearning-program">
        <div class="joomdle_mylearning_header changePosition">
        <span class="left">
            <a href="<?php echo 'mylearning/list.html'; ?>"><?php echo JText::_('MY_LEARNING'); ?></a>
        </span>
            <span class="center myCourses">
            <a href="<?php echo 'mycourses/list.html'; ?>"><?php echo JText::_('MY_COURSES'); ?></a>
        </span>
            <span class="right">
            <?php
            if (strcmp((JFactory::getLanguage()->getTag()), 'vi-VN') == 0) {
                ?>
                <a href="<?php echo JRoute::_('/dac-sac'); ?>"><?php echo JText::_('COM_JOOMDLE_LEARNING_STORE'); ?></a>
                <?php
            }
            if (strcmp((JFactory::getLanguage()->getTag()), 'en-GB') == 0) {
                ?>
                <a href="<?php echo JRoute::_('/featured'); ?>"><?php echo JText::_('COM_JOOMDLE_LEARNING_STORE'); ?></a>

                <?php
            }
            ?>
        </span>
        </div>

        <div class="title_pro">
            <?php if (!empty($name_program)) echo $name_program ?>
        </div>

        <div class="joomdle_mylearning_program">
            <div class="list_completed">
                <div class="swiper-container">

                    <div class="swiper-wrapper">
                        <?php
                        $i = 0;
                        $display = "";
                        foreach ($this->cursos as $curso) :
                            $curso['fullname'] = custom_echo($curso['fullname'], 27);
                            if ($curso['coursetype'] == 'program') {
                                $programid = $curso['cat_id'];
                                $url = JURI::base() . 'courseprogram/' . $programid . '.html';
                                $url_course = JURI::base() . 'courseprogram/' . $programid . '.html';
                                $course_button = JText::_('VIEW_PROGRAM_COMPLETION');
                                $button_com = "course_button_pro";
                                $color = "";
                            } else {
                                $display = "display:none;";
                                $course_id = $curso['remoteid'];
                                $url = JURI::base() . 'certificate/' . $course_id . '.html';
                                $url_course = JURI::base() . 'course/' . $course_id . '.html';
                                $course_button = JText::_('VIEW_CERTIFICATE');
                                $button_com = "course_button";
                                $color = "background:#ffffff";
                            }
                            $cat_id = $curso['cat_id'];
                            $cat_slug = JFilterOutput::stringURLSafe($curso['cat_name']);
                            $course_slug = JFilterOutput::stringURLSafe($curso['fullname']);
                            $course_image = $curso['filepath'] . $curso['filename'];

                            (array_key_exists('time_completed', $curso) && !is_null($curso['time_completed'])) ? $time = date('d/m/Y', $curso['time_completed']) : JText::_('UNKNOWN');
                            $completion_des = JText::_('COMPLETED') . ': ' . $time;

                            echo '<div class="swiper-slide">';
                            ?>
                            <div class="box_learning_mobile <?php echo $class_tablet; ?>">
                                <div class="course_content">
                                    <div class="clearfix"></div>
                                    <div class="mylearning-img">
                                        <a href="<?php echo $url_course; ?>">
                                            <div class="left" id="images"
                                                 style="background-image: url('<?php echo $course_image; ?>')"></div>
                                        </a>
                                    </div>
                                    <div class="content_c" style="<?php echo $color ?>">
                                        <div class="clearfix"></div>
                                        <div class="course_title" style="margin-top:10px;">
                                            <?php echo "<a href=\"$url_course\">" . $curso['fullname'] . "</a>"; ?>

                                        </div>
                                        <?php
                                        if (isset($curso['timeend'])) :
                                            echo ($curso['timeend'] == 0) ? "<p class='course-timeend'>" . JText::_('VALID_UNTIL') . ': ' . JText::_('UNLIMITED') . "</p>" : "<p class='course-timeend'>" . JText::_('VALID_UNTIL') . ': ' . date('d/m/Y', $curso['timeend']) . "</p>";
                                        endif;
                                        ?>
                                        <div class="right" style="color:#696969">
                                            <?php
                                            $curso['summary'] = custom_echo($curso['summary'], 54);
                                            if ($curso['summary'] == '') {
                                                echo "<br><br>";
                                            } else if (strlen($curso['summary']) < 35) {
                                                echo $curso['summary'] . "<br/><br/>";
                                            } else {
                                                echo $curso['summary'];
                                            }
                                            ?>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </div>
                </div>
            </div>

        </div>

    </div>
<?php }
else {  ?>
<div class="joomdle-mylearning-program">
    <div class="joomdle_mylearning_header changePosition">
        <span class="left">
            <a href="<?php echo 'mylearning/list.html'; ?>"><?php echo JText::_('MY_LEARNING'); ?></a>
        </span>
        <span class="center myCourses">
            <a href="<?php echo 'mycourses/list.html'; ?>"><?php echo JText::_('MY_COURSES'); ?></a>
        </span>
        <span class="right">
            <?php
            if (strcmp((JFactory::getLanguage()->getTag()), 'vi-VN') == 0) {
                ?>
                <a href="<?php echo JRoute::_('/dac-sac'); ?>"><?php echo JText::_('COM_JOOMDLE_LEARNING_STORE'); ?></a>
                <?php
            }
            if (strcmp((JFactory::getLanguage()->getTag()), 'en-GB') == 0) {
                ?>
                <a href="<?php echo JRoute::_('/featured'); ?>"><?php echo JText::_('COM_JOOMDLE_LEARNING_STORE'); ?></a>

                <?php
            }
            ?>
        </span>
    </div>

    <div class="title_pro">
        <?php if (!empty($name_program)) echo $name_program ?>
    </div>

    <div class="joomdle_mylearning_program">
        <div class="list_completed">
            <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php
                        $i = 0;
                        $display = "";
                        foreach ($this->cursos as $curso) :
                            $curso['fullname'] = custom_echo($curso['fullname'], 26);
                            $display = "";
                            if ($curso['coursetype'] == 'program') {
                                $programid = $curso['cat_id'];
                                $url = JURI::base() . 'courseprogram/' . $programid . '.html';
                                $url_course = JURI::base() . 'courseprogram/' . $programid . '.html';
                                $course_button = JText::_('DETAILS');
                                $color = "";
                                $background = 'style="background:#ccc !important;"';
                            } else {
                                $course_button = JText::_('CONTINUE');
                                $color = "background:#DCDCDC";
                                $display = "display:none;";
                                $course_id = $curso['remoteid'];

                                $url = JURI::base() . 'course/' . $course_id . '.html';
                                $background = '';
                            }
                            $cat_id = $curso['cat_id'];
                            $cat_slug = JFilterOutput::stringURLSafe($curso['cat_name']);
                            $course_slug = JFilterOutput::stringURLSafe($curso['fullname']);
                            $course_image = $curso['filepath'] . $curso['filename'];

                            (array_key_exists('last_access', $curso) && !is_null($curso['last_access'])) ? $time = date('d/m/Y', $curso['last_access']) : JText::_('UNKNOWN');
                            $completion_des = JText::_('LAST_ACCESSED') . ': ' . $time;

                            ?>
                            <div class="swiper-slide">
                                <div class="box_learning <?php echo $class_tablet; ?>" style="<?php echo $color ?>">
                                    <div class="course_content">
                                        <div class="clearfix"></div>
                                        <div class="mylearning-img">
                                            <a href="<?php echo $url; ?>">
                                                <div class="left" id="images"
                                                     style="background-image: url('<?php echo $course_image; ?>')"></div>
                                            </a>
                                        </div>

                                        <div class="content_c" <?php echo $background; ?>>
                                            <div class="clearfix"></div>
                                            <?php echo "<a class=\"course_title\" href=\"$url\">" . $curso['fullname'] . "</a>";
                                            if (isset($curso['timeend'])) :
                                                echo ($curso['timeend'] == 0) ? "<p class='course-timeend'>" . JText::_('VALID_UNTIL') . ': ' . JText::_('UNLIMITED') . "</p>" : "<p class='course-timeend'>" . JText::_('VALID_UNTIL') . ': ' . date('d/m/Y', $curso['timeend']) . "</p>";
                                            endif;
                                            ?>
                                            <div class="right" style="color:#696969">
                                                <?php echo custom_echo($curso['summary'], 80); ?>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </div>
            </div>
        </div>
    </div>

</div>
<?php } ?>
<?php
if ($device == 'mobile') {
    $slideview = '1.1';
    $space = '10';
//            $deviceWidth = '50';
} else if ($device == 'tablet') {
    $slideview = '2.5';
    $space = '15';
//            $deviceWidth = '568';
} else {
    $slideview = '3';
    $space = '15';
//            $deviceWidth = '400';
}
?>
<script type="text/javascript">
    (function ($) {
        var swiper = new Swiper('.swiper-container', {
            pagination: '.my-course .swiper-pagination',
            slidesPerView: <?php echo $slideview;?>,
            nextButton: '.my-course .swiper-button-next',
            prevButton: '.my-course .swiper-button-prev',
            spaceBetween: <?php echo $space; ?>,
            freeMode: true,
            freeModeMomentum: true
        });
    })(jQuery);
</script>
