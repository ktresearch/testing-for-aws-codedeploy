<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php
$itemid = JoomdleHelperContent::getMenuItem();
// get question feedback default (1-8 scale question, 9-10 text response question)
$questions = JoomdleHelperContent::call_method('get_feedback_question', 0);
// get module questionnaire_id created so add question.
$id_module_feedback = JoomdleHelperContent::call_method('get_questionnaire_id', $this->course_id);
$linkstarget = $this->params->get('linkstarget');
$rating = JRequest::getVar('rating');
var_dump($id_module_feedback);
die;
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$session = JFactory::getSession();

if ($session->get('device') == 'mobile') {
    ?>
    <style type="text/css">
        .navbar-header {
            background-color: #126db6;
        }
    </style>
    <?php
}
?>
<?php if (JFactory::getUser()->isMoodleAdmin) : ?>
    <style type="text/css">
        .t3-content {
            background-color: #fff;
            font-family: sans-serif;
        }
    </style>


    <div class="questions-bank-feedback">
        <div id="question-feedback">
            <p>Select and edit feedback questions.</p>
            <p class="selected">Selected: <span class="selectedCount">10</span></p>
            <button class="btGenerateSurvey">Generate Survey</button>
            <div class="clear"></div>
            <p>Likert Scale (Rate the statement from a scale of 1-5)</p>
            <input type="hidden" name="hdSelectedQuestions" class="hdSelectedQuestions"/>
            <div class="inputQuestType">
                <form id="questionForm" class="form" method="post" action="<?php echo JRoute::_('index.php?option=com_joomdle&task=saveQuestion'); ?>"
                >

                    <?php
                    $cout_position = 1;
                    foreach ($questions as $k => $v) {
                        if ($cout_position == 9) { ?>
                            <div id="textResponse"><p>Text Response</p></div>
                            <?php
                        }
                        ?>
                        <div class="optionQuestionMethod active"
                             id="optionQuestionMethod<?php echo $cout_position; ?>"
                             qmid="<?php echo $cout_position; ?>">
                            <div class="status"></div>
                            <div class="formQuestion">
                                <p>Question <?php echo $cout_position; ?></p>
                                <input type="hidden" name="question<?php echo $cout_position; ?>name"
                                       value="<?php echo $v['name']; ?>"/>
                                <input type="hidden" id="numberRating<?php echo $cout_position; ?>"
                                       name="question<?php echo $cout_position; ?>length"
                                       value="<?php echo $rating; ?>"/>
                                <input type="hidden" name="question<?php echo $cout_position; ?>survey"
                                       value="<?php echo($id_module_feedback[0]["instance"] - 1); ?>"/>
                                <input type="hidden" name="question<?php echo $cout_position; ?>type"
                                       value="<?php if ($v['type_id'] == 8) {
                                           echo 8;
                                       }
                                       if ($v['type_id'] == 2) {
                                           echo 2;
                                       } ?>"/>
                                <input type="hidden" name="question<?php echo $cout_position; ?>position"
                                       value="<?php echo $cout_position; ?>"/>
                                <input type="hidden" id="contentQuestion<?php echo $cout_position; ?>"
                                       class="txtInput"
                                       name="question<?php echo $cout_position; ?>content"
                                       value="<?php echo $v['content']; ?>"/>

                                <p id="pContent"><?php echo $v['content']; ?></p>
                            </div>
                        </div>
                        <?php
                        $cout_position++;
                    } ?>
                    <input type="hidden" name="courseid"
                           value="<?php echo $this->course_id; ?>"/>
                    <input type="hidden" name="questionnum" id="questionnum"/>
                    <input class="hidden btSave" type="submit" value="Save"/>
                </form>

            </div>
            <div class="clear"></div>
            <div class="buttons">
                <button class="btCancel">Cancel</button>
                <button class="btSave">Save</button>
                <button class="btPreview">Preview</button>
            </div>

        </div>
    </div>

    <div class="joomdle-preview hidden">
        <h1>Feed Back</h1>
        <div id="group">
            <?php
            $cout_preview = 1;
            foreach ($questions as $k => $v) {
                if ($v['type_id'] == 8) {
                    ?>
                    <div class="countRatingQuestion<?php echo $cout_preview; ?>" id="countRatingQuestion"
                         q="<?php echo $cout_preview; ?>">
                        <p id="pcolor">Question <span
                                class="countQuestion<?php echo $cout_preview; ?>"><?php echo $cout_preview; ?></span>
                        </p>
                        <p id="replaceQuestion<?php echo $cout_preview; ?>"><span
                                class="contentQuestionFreview<?php echo $cout_preview; ?>"
                                id="contentQuestionFreview<?php echo $cout_preview; ?>"><?php echo $v['content']; ?></span>
                        </p>
                        <ul class="scaleRatingQuestion" id="scaleRatingQuestion">
                            <li class="liScale1" id="liScaleRatingQuestion" preid="1">
                                <div class="timestamp">
                        <span class="countRating1">1<span>
                                </div>
                                <div class="statusRatingQuestion" id="question<?php echo $cout_preview; ?>rate1">
                                    <p> Strongly Disagree </p>
                                </div>
                            </li>
                            <li class="liScale2 " id="liScaleRatingQuestion" preid="2">
                                <div class="timestamp">
                        <span class="countRating2">2<span>
                                </div>
                                <div class="statusRatingQuestion" id="question<?php echo $cout_preview; ?>rate2">
                                    <p> Disagree </p>
                                </div>
                            </li>
                            <li class="liScale3 " id="liScaleRatingQuestion" preid="3">
                                <div class="timestamp">
                        <span class="countRating3">3<span>
                                </div>
                                <div class="statusRatingQuestion" id="question<?php echo $cout_preview; ?>rate3">
                                    <p> Neutral </p>
                                </div>
                            </li>
                            <li class="liScale4 " id="liScaleRatingQuestion" preid="4">
                                <div class="timestamp">
                        <span class="countRating4">4<span>
                                </div>
                                <div class="statusRatingQuestion" id="question<?php echo $cout_preview; ?>rate4">
                                    <p> Agree </p>
                                </div>
                            </li>
                            <li class="liScale5 " id="liScaleRatingQuestion" preid="5">
                                <div class="timestamp">
                        <span class="countRating5">5<span>
                                </div>
                                <div class="statusRatingQuestion" id="question<?php echo $cout_preview; ?>rate5">
                                    <p> Strongly Agree </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <?php
                } else if ($v['type_id'] == 2 && $cout_preview == 9) { ?>
                    <div class="countRatingQuestion9 " id="countRatingQuestion" q="9">
                        <p id="pcolor">Question <span class="countQuestion9">9</span></p>
                        <p><span class="contentQuestionFreview9" id="contentQuestionFreview"></span></p>
                        <p id="replaceQuestion9"><span class="contentQuestionFreview9"
                                                       id="contentQuestionFreview9"><?php echo $v['content']; ?></span>
                        </p>
                        <form action="" method="post" id="txtAnswer" name="txtAnswer" class="txtAnswer"
                              enctype='multipart/form-data'>
                            <label for=""></label><textarea name="textareaAnswer" class="textareaAnswer"
                                                            placeholder="Write your feedback to here"
                                                            required></textarea>
                        </form>
                    </div>
                <?php } else { ?>
                    <div class="countRatingQuestion10 " id="countRatingQuestion" q="10">
                        <p id="pcolor">Question <span class="countQuestion10">10</span></p>
                        <p id="replaceQuestion10"><span class="contentQuestionFreview10"
                                                        id="contentQuestionFreview10"><?php echo $v['content']; ?></span>
                        </p>
                        <form action="" method="post" id="txtAnswer" name="txtAnswer" class="txtAnswer"
                              enctype='multipart/form-data'>
                            <label for=""></label><textarea name="textareaAnswer" class="textareaAnswer"
                                                            placeholder="Write your feedback to here"
                                                            required></textarea>
                        </form>
                    </div>
                    <?php
                }
                $cout_preview++;
            }
            ?>


        </div>
        <div class="clear"></div>
        <button class="btEditFeedBack">Edit Feedback</button>
        <div class="clear"></div>
        <button class="btNext" id="next">Next →</button>
        <button class="btPrev" id="prev">← Prev</button><!--    <div id="next"></div>-->
        <!--    <div style="display:none" id="prev">prev</div>-->
        <!--    <button class="btEdit">Edit</button>-->
    </div>
    <script>

    </script>
    <script>

        (function ($) {

            // Check question selected return array option(array sort ASC)
            function load_option_question() {
                var optionsQuestion = [];
                $("[id^='optionQuestionMethod']").each(function () {
                    if ($(this).attr('class').includes('active')) {
                        optionsQuestion.push($(this).attr('qmid'));
                    }
                });
                console.log(optionsQuestion);
                return optionsQuestion;
            }

            //!!!  Start code next prev at preview page
            function updateItems(delta) {
                var optionsQuestion = load_option_question();
                var $items = $('#group').children();
                var $current = $items.filter('.current');
                $current = $current.length ? $current : $items.first();
                var index = $current.index() + delta;
                // Range check the new index
                index = (index < 0) ? 0 : ((index > $items.length) ? $items.length : index);
                $current.removeClass('current');
                $current = $items.eq(index).addClass('current');
                // Hide/show the next/prev
                $("#prev").toggle(!$current.is($items.first()));
                $("#next").toggle(!$current.is($items.last()));
            }

            $("#next").click(function () {
                updateItems(1);
            });
            $("#prev").click(function () {
                updateItems(-1);
            });
            updateItems(0); // Cause initial selection
            //!!! Finish code next prev at preview page

            // Click button Cancel return My Learning this course
            $('.questions-bank-feedback .btCancel').click(function () {
                window.location.href = "<?php echo JUri::base() . "index.php?option=com_joomdle&view=feedback&course_id=" . $this->course_id; ?>";

            });
            // Click button Edit
            $('.joomdle-preview .btEditFeedBack').click(function () {
                var optionsQuestion = load_option_question();
                $('.questions-bank-feedback').removeClass('hidden');
                $('.joomdle-preview').addClass('hidden');
                $("[id^='pContent']").remove();
                for (var i = 1; i <= 10; i++) {
                    document.getElementById('contentQuestion' + i).setAttribute('type', 'text');
                }
                var $items = $('#group').children();
                var $current = $items.filter('.current');
                $current.removeClass('current');
                console.log($current);

                $("[id^='countRatingQuestion']").each(function () {
                    var y = $(this).attr('q');
                    var check = optionsQuestion.indexOf(y);
                    console.log(check);
                    if (check < 0) {
                        $('.countRatingQuestion' + y).addClass('hidden');
                    }
                    console.log(check);
                });
            });

            // Click button Preview
            $('.questions-bank-feedback .btPreview').click(function () {
                var optionsQuestion = load_option_question();
                var a = String(optionsQuestion);
                var rate = Number(<?php echo $rating; ?>);
                var inputFormValue = [];
                var inputPreview = [];
                var questionLength = optionsQuestion.length;
                for (var i = 0; i < questionLength; i++) {
                    // Rate Number ticked at Preview page
                    $("#question" + optionsQuestion[i] + "rate" + rate).addClass('active');
                    // Edit text successful and update at Preview page
                    inputFormValue[optionsQuestion[i]] = document.getElementById("contentQuestion" + optionsQuestion[i]).value;
                    inputPreview[optionsQuestion[i]] = document.getElementById("contentQuestionFreview" + optionsQuestion[i]);
                    inputPreview[optionsQuestion[i]].innerHTML = inputFormValue[optionsQuestion[i]];
//                    console.log(inputFormValue[optionsQuestion[i]]);
                }

                var start = optionsQuestion[0];
                $('.countRatingQuestion' + start).addClass('current');
                var j = 0;
                if (optionsQuestion.length >= 1) {
                    $('.questions-bank-feedback').addClass('hidden');
                    $('.joomdle-preview').removeClass('hidden');
                    $("[id^='countRatingQuestion']").each(function () {
                        var y = $(this).attr('q');
                        var check = optionsQuestion.indexOf(y);
                        console.log(check);
                        if (check < 0) {
                            $('.countRatingQuestion' + y).remove();
                        }
                        if (check >= 0 && $(this).attr('class').includes('hidden')) {
                            $('.countRatingQuestion' + y).removeClass('hidden');
                        }
//                        console.log(check);
                    });
                }
                else alert("You did not choose any question");
            });
            // Click button Save
            $('.questions-bank-feedback .btSave').click(function () {
                var optionsQuestion = load_option_question();
                var questionChoose = String(optionsQuestion);
                document.getElementById("questionnum").value = questionChoose;
                var questionLength = optionsQuestion.length;
                var jsonString = JSON.stringify(optionsQuestion);
                document.getElementById("questionForm").submit();
//                window.location = "<?php //echo JUri::base() . "index.php?option=com_joomdle&view=feedback&course_id=" . $this->course_id; ?>//";
                if (optionsQuestion.length >= 1) {

//                    $.ajax({
//                        type: "POST",
//                        url: "<?php //echo JRoute::_('index.php?option=com_joomdle&task=saveQuestion'); ?>//", //includes full webserver url
//                        data: {data : jsonString},
//                        cache: false,
//                    });

                }
                else alert("You did not choose any question");
            });

            $('.optionQuestionMethod').click(function () {
                var oldv = $('.hdSelectedQuestions').val();
                var count = parseInt($('.selectedCount').html());
                if (!$(this).hasClass('active')) {
                    $('.selectedCount').html(count + 1);
                    var arr = oldv.split(',');
                    arr.push($(this).attr('qmid'));
                    $('.hdSelectedQuestions').val(arr.toString());
                } else {
                    $('.selectedCount').html(count - 1);
                    var arr = oldv.split(',');
                    var index = arr.indexOf($(this).attr('qmid'));
                    if (index > -1) {
                        arr.splice(index, 1);
                    }
                    $('.hdSelectedQuestions').val(arr.toString());
                }
                $(this).toggleClass('active');
                load_option_question();
            });
        })(jQuery);
    </script>
<?php endif; ?>
