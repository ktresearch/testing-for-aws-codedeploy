<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php
$itemid = JoomdleHelperContent::getMenuItem();

// get module questionnaire_id created so add question.
$linkstarget = $this->params->get('linkstarget');
$user = JFactory::getUser();
$username = $user->username;
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$session = JFactory::getSession();

if ($session->get('device') == 'mobile') {
    ?>
    <style type="text/css">
        .navbar-header {
            background-color: #126db6;
        }
    </style>
    <?php
}
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'components/com_joomdle/views/feedback/tmpl/feedback.css');
$document->addScript(JUri::root() . 'components/com_community/assets/autosize.min.js');
$role = $this->hasPermission;

require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);
$render_banner = $banner->renderBanner($this->course_info, $this->mods, $role);
?>

<?php if ($this->hasPermission[0]['hasPermission']) :
    if ($this->action == 'edit') $edit = true; else $edit = false;
    if ($edit) {
        $data = $this->questionnaire;
        $questions = $this->questionnaire['questions'];
    } else {
        $questions = $this->questions;
    }
    $length = 5; //default
    $selectedCount = 0;

    foreach ($questions as $key => $value) {
        if ($value['deleted'] != 'y') $selectedCount++;
        if ($value['type_id'] != 8) continue;
        if ($value['type_id'] == 8) {
            if (isset($value['length'])) {
                $length = $value['length'];
                $ratevalues = json_decode($value['params'], true);
                $ratevalues = str_replace("=",":",$ratevalues);
            }
        }
    }

    if (!$length) $length = 5;
    $activeArr = array();
    for ($i = 1; $i <= 5; $i++) {
        if ($i <= $length) $activeArr[$i] = 1; else $activeArr[$i] = 0;
    }
//    var_dump($ratevalues[1]);die;

    ?>
    <div class="joomdle-feedback">
        <form id="questionForm" class="form" method="post"
              action="<?php echo JRoute::_('index.php?option=com_joomdle&task=saveQuestion'); ?>">
            <div class="joomdle-create-feedback">
                <h1><?php echo JText::_('COM_JOOMDLE_FEEDBACK_COURSE'); ?></h1>
                <div class="feedback-title form-group">
                    <label for="txtTitle"><?php echo JText::_('COM_JOOMDLE_FEEDBACK_TITLE'); ?></label><span
                            class="red">*</span>
                    <input type="text" name="txtTitle" id="txtTitle" class="txtTitle" onkeypress="return event.keyCode != 13;" value="<?php echo ($edit) ? $data['name'] : ''; ?>"/>
                </div>
                <div class="feedback-description form-group">
                    <label for="txtaDescription"><?php echo JText::_('COM_JOOMDLE_FEEDBACK_INSTRUCTIONS'); ?></label><span class="red">*</span>
                    <textarea name="txtaDescription" id="txtaDescription"
                              class="txtaDescription autosize"><?php echo ($edit) ? $data['intro'] : ''; ?></textarea>
                </div>
                <p class="lblRatingMethod"><?php echo JText::_('COM_JOOMDLE_FEEDBACK_RATING_METHOD_TEXT'); ?> <span
                            class="red">*</span></p>
                <input type="hidden" name="hdRatingMethod" class="hdRatingMethod" value="1"/>
                <div class="optionRatingMethod <?php echo ($activeArr[1]) ? 'active' : ''; ?>" id="optionRating1"
                     rmid="1">
                    <div class="status"></div>
                    <input type="text" placeholder="<?php echo JText::_('COM_JOOMDLE_ENTER_RATE_TEXT'); ?>"
                           value="<?php echo ($ratevalues[1]) ? $ratevalues[1] : '1: ' . JText::_('COM_JOOMDLE_STRONGLY_DISAGREE'); ?>"
                           class="firstrate" name="firstrate">
                </div>
                <div class="optionRatingMethod <?php echo ($activeArr[2]) ? 'active' : ''; ?>" id="optionRating2"
                     rmid="2">
                    <div class="status"></div>
                    <input type="text" placeholder="<?php echo JText::_('COM_JOOMDLE_ENTER_RATE_TEXT'); ?>"
                           value="<?php echo ($ratevalues[2]) ? $ratevalues[2] : '2: ' . JText::_('COM_JOOMDLE_DISAGREE'); ?>"
                           class="secondrate" name="secondrate">
                </div>
                <div class="optionRatingMethod <?php echo ($activeArr[3]) ? 'active' : ''; ?>" id="optionRating3"
                     rmid="3">
                    <div class="status"></div>
                    <input type="text" placeholder="<?php echo JText::_('COM_JOOMDLE_ENTER_RATE_TEXT'); ?>"
                           value="<?php echo ($ratevalues[3]) ? $ratevalues[3] : '3: ' . JText::_('COM_JOOMDLE_NEUTRAL'); ?>"
                           class="thirdrate" name="thirdrate">
                </div>
                <div class="optionRatingMethod <?php echo ($activeArr[4]) ? 'active' : ''; ?>" id="optionRating4"
                     rmid="4">
                    <div class="status"></div>
                    <input type="text" placeholder="<?php echo JText::_('COM_JOOMDLE_ENTER_RATE_TEXT'); ?>"
                           value="<?php echo ($ratevalues[4]) ? $ratevalues[4] : '4: ' . JText::_('COM_JOOMDLE_AGREE'); ?>"
                           class="fourthrate" name="fourthrate">
                </div>
                <div class="optionRatingMethod <?php echo ($activeArr[5]) ? 'active' : ''; ?>" id="optionRating5"
                     rmid="5">
                    <div class="status"></div>
                    <input type="text" placeholder="<?php echo JText::_('COM_JOOMDLE_ENTER_RATE_TEXT'); ?>"
                           value="<?php echo ($ratevalues[5]) ? $ratevalues[5] : '5: ' . JText::_('COM_JOOMDLE_STRONGLY_AGREE'); ?>"
                           class="fifthrate" name="fifthrate">
                </div>
                <input type="hidden" id="rate" name="rate" value="<?php echo ($edit) ? $length : ''; ?>"/>
                <input type="hidden" id="username" name="username" value="<?php echo $username; ?>"/>
                <img src="/images/NextIcon.png" class="btNextFeedBack" id="btNextFeedBack">
            </div>

            <div class="questions-bank-feedback hidden">
                <h1><?php echo JText::_('COM_JOOMDLE_FEEDBACK_COURSE'); ?></h1>
                <div id="question-feedback">
                    <p class="questionDes"><?php echo JText::_('COM_JOOMDLE_FB_TEXT1'); ?><span class="red">*</span></p>
                    <p class="selected"><?php echo JText::_('COM_JOOMDLE_SELECTED'); ?>: <span
                                class="selectedCount"><?php echo $selectedCount; ?></span></p>
                    <!--            <button class="btGenerateSurvey">Generate Survey</button>-->
                    <div class="clear"></div>
                    <p class="questionTypeDes"><?php echo JText::_('COM_JOOMDLE_FB_TEXT2'); ?> <span class="ratenumstart">1</span> - <span
                                class="ratenumend">5)</span></p>
                    <input type="hidden" name="hdSelectedQuestions" class="hdSelectedQuestions"/>
                    <div class="inputQuestType">
                        <?php
                        $cout_position = 1;
                        $isTextResponse = true;
                        $trQuestions = array();

                        foreach ($questions as $k => $v) {
                            if ($v['type_id'] == 2) {
                                $trQuestions[] = $v;
                                if ($isTextResponse) {
                                    ?>
                                    <div id="textResponse">
                                        <p class="questionTypeDes"><?php echo JText::_('COM_JOOMDLE_TEXT_RESPONSE'); ?></p>
                                    </div>
                                    <?php
                                }
                                $isTextResponse = false;
                            }
                            ?>
                            <div class="optionQuestionMethod <?php if ($v['deleted'] == 'n') echo 'active'; ?>"
                                 id="optionQuestionMethod<?php echo $cout_position; ?>"
                                 qmid="<?php echo $cout_position; ?>">
                                <div class="status"></div>
                                <div class="formQuestion">
                                    <!-- <p><?php //echo JText::_('COM_JOOMDLE_QUESTION');?> <?php echo $cout_position; ?></p> -->
                                    <input type="hidden" name="question<?php echo $cout_position; ?>name"
                                           class="question<?php echo $cout_position; ?>name"
                                           value="<?php echo $v['name']; ?>"/>
                                    <input type="hidden" name="question<?php echo $cout_position; ?>deleted"
                                           class="question<?php echo $cout_position; ?>deleted"
                                           value="<?php echo $v['deleted']; ?>"/>
                                    <input type="hidden" name="question<?php echo $cout_position; ?>type"
                                           class="question<?php echo $cout_position; ?>type"
                                           value="<?php if ($v['type_id'] == 8) {
                                               echo 8;
                                           }
                                           if ($v['type_id'] == 2) {
                                               echo 2;
                                           } ?>"/>
                                    <input type="hidden" name="question<?php echo $cout_position; ?>position"
                                           class="question<?php echo $cout_position; ?>position"
                                           value="<?php echo $cout_position; ?>"/>
                                    <textarea type="text" id="contentQuestion<?php echo $cout_position; ?>"
                                              class="txtInput autosize"
                                              name="question<?php echo $cout_position; ?>content"
                                    ><?php echo $v['content']; ?></textarea>
                                </div>
                            </div>
                            <?php
                            $cout_position++;
                        } ?>
                        <input type="hidden" name="courseid" value="<?php echo $this->course_id; ?>"/>
                        <input type="hidden" name="questionnum" id="questionnum"/>
                        <?php
                        if ($edit) {
                            echo '<input type="hidden" name="moduleid" id="moduleid" value="' . $data['module_id'] . '"/>';
                        }
                        ?>
                        <input class="hidden btSave" type="submit" value="Save"/>

                    </div>
                    <div class="clear"></div>
                    <div class="buttons">
                        <button type="button" class="btSave pull-right"><?php echo JText::_('COM_JOOMDLE_SAVE'); ?></button>
                        <button type="button" class="btPreview pull-right"><?php echo JText::_('COM_JOOMDLE_PREVIEW'); ?></button>
                        <button type="button" class="btCancel pull-right"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                    </div>
                    <img src="/images/PreviousIcon.png" class="btPrevFeedBack" id="btPrevFeedBack">
                </div>
            </div>
        </form>

        <div class="joomdle-preview hidden">
            <h1><?php echo JText::_('COM_JOOMDLE_FEEDBACK_COURSE'); ?></h1>
            <div id="group" fb_slide="1">
                <p class="pcolor likertpcolor"><?php echo JText::_('COM_JOOMDLE_FB_TEXT3'); ?> <span
                            class="ratenumstart">1</span> - <span class="ratenumend">5</span>.</p>
                <?php
                $cout_preview = 1;
                foreach ($questions as $k => $v) {
                    if ($v['type_id'] == 8) {
                        ?>
                        <div class="countRatingQuestion item" id="countRatingQuestion<?php echo $cout_preview; ?>"
                             data-type='likert' q="<?php echo $cout_preview; ?>">
                            <!-- <p id="pcolor"><?php echo JText::_('COM_JOOMDLE_QUESTION'); ?> <span
                                        class="countQuestion<?php echo $cout_preview; ?>"><?php echo $cout_preview; ?></span>
                            </p> -->

                            <p id="replaceQuestion<?php echo $cout_preview; ?>" class="replaceQuestion"><span
                                        class="contentQuestionFreview<?php echo $cout_preview; ?>"
                                        id="contentQuestionFreview<?php echo $cout_preview; ?>"><?php echo $v['content']; ?></span>
                            </p>
                            <ul class="scaleRatingQuestion" id="scaleRatingQuestion">
                                <li class="liScale1" id="liScaleRatingQuestion" preid="1">
                                    <div class="timestamp">
                        <span class="countRating1">1<span>
                                    </div>
                                    <div class="statusRatingQuestion" id="question<?php echo $cout_preview; ?>rate1">
                                        <p> <?php echo JText::_('COM_JOOMDLE_STRONGLY_DISAGREE'); ?> </p>
                                    </div>
                                </li>
                                <li class="liScale2 " id="liScaleRatingQuestion" preid="2">
                                    <div class="timestamp">
                        <span class="countRating2">2<span>
                                    </div>
                                    <div class="statusRatingQuestion" id="question<?php echo $cout_preview; ?>rate2">
                                        <p> <?php echo JText::_('COM_JOOMDLE_DISAGREE'); ?> </p>
                                    </div>
                                </li>
                                <li class="liScale3 " id="liScaleRatingQuestion" preid="3">
                                    <div class="timestamp">
                        <span class="countRating3">3<span>
                                    </div>
                                    <div class="statusRatingQuestion" id="question<?php echo $cout_preview; ?>rate3">
                                        <p> <?php echo JText::_('COM_JOOMDLE_NEUTRAL'); ?> </p>
                                    </div>
                                </li>
                                <li class="liScale4 " id="liScaleRatingQuestion" preid="4">
                                    <div class="timestamp">
                        <span class="countRating4">4<span>
                                    </div>
                                    <div class="statusRatingQuestion" id="question<?php echo $cout_preview; ?>rate4">
                                        <p> <?php echo JText::_('COM_JOOMDLE_AGREE'); ?> </p>
                                    </div>
                                </li>
                                <li class="liScale5 " id="liScaleRatingQuestion" preid="5">
                                    <div class="timestamp">
                        <span class="countRating5">5<span>
                                    </div>
                                    <div class="statusRatingQuestion" id="question<?php echo $cout_preview; ?>rate5">
                                        <p> <?php echo JText::_('COM_JOOMDLE_STRONGLY_AGREE'); ?> </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <?php
                        $cout_preview++;
                    }
                }
                if (!empty($trQuestions)) {
                    ?>
                    <p class="pcolor trpcolor"><?php echo JText::_('COM_JOOMDLE_COMMENTS'); ?></p>
                    <?php
                    foreach ($trQuestions as $k => $v) {
                        ?>
                        <div class="countRatingQuestion item" id="countRatingQuestion<?php echo $cout_preview; ?>"
                             data-type='textresponse' q="<?php echo $cout_preview; ?>">
                            <p><span class="contentQuestionFreview<?php echo $cout_preview; ?>"
                                     id="contentQuestionFreview"></span></p>
                            <p id="replaceQuestion<?php echo $cout_preview; ?>" class="replaceQuestion"><span
                                        class="contentQuestionFreview<?php echo $cout_preview; ?>"
                                        id="contentQuestionFreview<?php echo $cout_preview; ?>"><?php echo $v['content']; ?></span>
                            </p>
                            <form action="" method="post" id="txtAnswer" name="txtAnswer" class="txtAnswer"
                                  enctype='multipart/form-data'>
                                <label for=""></label>
                                <textarea name="textareaAnswer" class="textareaAnswer autosize"
                                          placeholder="<?php echo JText::_('COM_JOOMDLE_ANY_FEEDBACK_FOR_US'); ?>"></textarea>
                            </form>
                        </div>
                        <?php
                        $cout_preview++;
                    }
                }
                ?>

            </div>
            <!--        Next previous button-->
            <!--  <div class="prev-question-feedback">
                <img width="50" height="50" src="<?php echo JURI::base() . 'images/prev.png'; ?>">
            </div>
            <div class="next-question-feedback">
                <img width="50" height="50" src="<?php echo JURI::base() . 'images/next.png'; ?>">
            </div> -->
            <div class="clear"></div>
            <!-- <div class="class-btEditFeedBack"> -->
            <button class="btEditFeedBack" qedit="0"><?php echo JText::_('COM_JOOMDLE_EXIT_PREVIEW'); ?></button>
            <!-- </div> -->
            <div class="clear"></div>
            <!-- <div class="process-point">
                <ul class="process-ul" id="">
                    <li class="process-li" id="lipoint1" process="1">
                        <div class="statusPoint" id="statusPoint0">
                        </div>
                    </li>
                    <li class="process-li" id="lipoint2" process="2">
                        <div class="statusPoint" id="statusPoint1">
                        </div>
                    </li>
                    <li class="process-li" id="lipoint3" process="3">
                        <div class="statusPoint" id="statusPoint2">
                        </div>
                    </li>
                    <li class="process-li" id="lipoint4" process="4">
                        <div class="statusPoint" id="statusPoint3">
                        </div>
                    </li>
                    <li class="process-li" id="lipoint5" process="5">
                        <div class="statusPoint" id="statusPoint4">
                        </div>
                    </li>
                    <li class="process-li" id="lipoint6" process="6">
                        <div class="statusPoint" id="statusPoint5">
                        </div>
                    </li>
                    <li class="process-li" id="lipoint7" process="7">
                        <div class="statusPoint" id="statusPoint6">
                        </div>
                    </li>
                    <li class="process-li" id="lipoint8" process="8">
                        <div class="statusPoint" id="statusPoint7">
                        </div>
                    </li>
                    <li class="process-li" id="lipoint9" process="9">
                        <div class="statusPoint" id="statusPoint8">
                        </div>
                    </li>
                    <li class="process-li" id="lipoint10" process="10">
                        <div class="statusPoint" id="statusPoint9">
                        </div>
                    </li>

                </ul>

            </div> -->
        </div>
    </div>
    <script>

        (function ($) {
            var rating = <?php echo ($edit) ? $length : 5; ?>;
            // check rate 1-5

            $('.autosize').each(function () {
                autosize(this);
            }).on('input', function () {
                autosize(this);
            });

            $('.optionRatingMethod > .status, .optionRatingMethod > span').click(function () {
                var rmid = $(this).parent('.optionRatingMethod').attr('rmid');
                $('.optionRatingMethod').removeClass('active');
                for (var i = 1; i <= rmid; i++) {
                    $("#optionRating" + i).addClass("active");
                }
                $('#rate').val(rmid);
                rating = rmid;
            });

            $('#questionForm').on('submit', function(e) {
                if (!validateFirstPage()) {
                    e.preventDefault();

                    return false;
                }
            });

            $('.btNextFeedBack').click(function () {
                if (validateFirstPage()) {
                    // if ($("#txtTitle").val().trim() != "" && $("#txtaDescription").val().trim() != "") {
                        var validRatings = true;
                        $('.optionRatingMethod').each(function() {
                            if ($(this).children('input').val().indexOf(":") == -1) validRatings = false;
                        });
                        
                        if (validRatings) {
                            $('.joomdle-create-feedback').addClass('hidden');
                            $('.questions-bank-feedback').removeClass('hidden');

                            $('.ratenumstart').html($('.firstrate').val().split(':')[0]);
                            switch (rating) {
                                case '2':
                                    $('.ratenumend').html($('.secondrate').val().split(':')[0]);
                                    break;
                                case '3':
                                    $('.ratenumend').html($('.thirdrate').val().split(':')[0]);
                                    break;
                                case '4':
                                    $('.ratenumend').html($('.fourthrate').val().split(':')[0]);
                                    break;
                                case '5':
                                    $('.ratenumend').html($('.fifthrate').val().split(':')[0]);
                                    break;
                            }
                            $('#rate').val(rating);

                            jQuery('.txtInput').each(function () {
                                this.setAttribute('style', 'height:'+ this.scrollHeight+ 'px;');
                            }).on('input', function () {
                                this.style.height = 'auto';
                                this.setAttribute('style', 'height:'+ this.scrollHeight+ 'px;');
                            });
                        } else {
                            lgtCreatePopup('oneButton', {content: "Rating Text Format: sometext = sometext"}, function () {
                                lgtRemovePopup();
                            });
                            validateResult = false;
                        }
                    // } else {
                        

                        /*if ($("#txtTitle").val().trim() == "" && $("#txtaDescription").val().trim() != "") {
                            lgtCreatePopup('oneButton', {content: "Please fill out title before click button Next."}, function () {
                                lgtRemovePopup();
                            });
                        } else if ($("#txtaDescription").val().trim() == "" && $("#txtTitle").val().trim() != "") {
                            lgtCreatePopup('oneButton', {content: "Please fill out description before click button Next."}, function () {
                                lgtRemovePopup();
                            });
                        } else {
                            lgtCreatePopup('oneButton', {content: "Please fill out title and description before click button Next."}, function () {
                                lgtRemovePopup();
                            });
                        }*/
                    // }
                }
            });
            $('.btPrevFeedBack').click(function () {
                $('.joomdle-create-feedback').removeClass('hidden');
                $('.questions-bank-feedback').addClass('hidden');
            });

            function validateFirstPage() {
                var validateResult = true;

                if ($('.txtTitle').val().trim() == '') {
                    $('.txtTitle').on('keyup', function() {
                        if ($(this).val().trim() != '') 
                            $(this).parents('.form-group').removeClass('hasError');
                        else $(this).parents('.form-group').addClass('hasError');
                    }).val('').parents('.form-group').addClass('hasError');
                    validateResult = false;
                } else $('.txtTitle').parents('.form-group').removeClass('hasError');

                if ($('.txtaDescription').val().trim() == '') {
                    $('.txtaDescription').on('keyup', function() {
                        if ($(this).val().trim() != '') 
                            $(this).parents('.form-group').removeClass('hasError');
                        else $(this).parents('.form-group').addClass('hasError');
                    }).val('').parents('.form-group').addClass('hasError');
                    validateResult = false;
                } else $('.txtaDescription').parents('.form-group').removeClass('hasError');

                if (rating < 2) {
                    lgtCreatePopup('oneButton', {content: "Please choose 2 or more."}, function () {
                        lgtRemovePopup();
                    });
                    validateResult = false;
                }

                return validateResult;
            }

            // Check question selected return array option(array sort ASC)
            function load_option_question() {
                var optionsQuestion = [];
                $("[id^='optionQuestionMethod']").each(function () {
                    if ($(this).hasClass('active')) {
                        optionsQuestion.push($(this).attr('qmid'));
                    }
                });
                console.log(optionsQuestion);
                return optionsQuestion;
            }

            // Click button Cancel return My Learning this course
            $('.questions-bank-feedback .btCancel').click(function () {
                window.location.href = "<?php echo JUri::base() . 'course/' . $this->course_id . '.html'; ?>";
            });
            // Click button Edit
            $('.joomdle-preview .btEditFeedBack').off().on('click', function () {
//                document.getElementById("qedit").value = 0;
                var optionsQuestion = load_option_question();
                $(".countRatingQuestion").removeClass('current');
                $(".statusPoint").removeClass('active');
                $(".statusRatingQuestion").removeClass('active');
                $("[id^='lipoint']").each(function () {
                    if ($(this).hasClass('hidden')) {
                        $(this).removeClass('hidden');
                    }
                });
                $(".process-li").removeClass('active');
                $('.questions-bank-feedback').removeClass('hidden');
                $('.joomdle-preview').addClass('hidden');
                var $items = $('#group').children();
                var $current = $items.filter('.current');
                $current.removeClass('current');
                $("[id^='countRatingQuestion']").each(function () {
                    var y = $(this).attr('q');
                    var check = optionsQuestion.indexOf(y);
                    if (check < 0) {
                        $('.countRatingQuestion' + y).addClass('hidden');
                    }
                });
            });

            // Click button Preview
            $('.questions-bank-feedback .btPreview').off().on('click', function () {
                $("#statusPoint" + 0).addClass('active');
                $(".next-question-feedback").show();
                $(".prev-question-feedback").hide();
                var optionsQuestion = load_option_question();

                var inputFormValue = [];
                var inputPreview = [];
                var a = String(optionsQuestion);
                var rate = rating;
                var questionLength = optionsQuestion.length;
                for (var i = 0; i < optionsQuestion.length; i++) {
                    var temp = [];
                    // Rate Number ticked at Preview page
                    // Edit text successful and update at Preview page
                    inputFormValue[optionsQuestion[i]] = $("#contentQuestion" + optionsQuestion[i]).val();
                    temp[i] = $("#contentQuestionFreview" + optionsQuestion[i]).text(inputFormValue[optionsQuestion[i]]);
                    temp[i].html(temp[i].html().replace(/\n/g, '<br/>'));
                }

                $("[id^='liScaleRatingQuestion']").each(function () {
                    var y = $(this).attr('preid');
                    switch (y) {
                        case '1':
                            $(this).find('.timestamp .countRating' + y).html($('.firstrate').val().split(':')[0]);
                            $(this).find('.statusRatingQuestion > p').html($('.firstrate').val().split(':')[1]);
                            break;
                        case '2':
                            $(this).find('.timestamp .countRating' + y).html($('.secondrate').val().split(':')[0]);
                            $(this).find('.statusRatingQuestion > p').html($('.secondrate').val().split(':')[1]);
                            break;
                        case '3':
                            $(this).find('.timestamp .countRating' + y).html($('.thirdrate').val().split(':')[0]);
                            $(this).find('.statusRatingQuestion > p').html($('.thirdrate').val().split(':')[1]);
                            break;
                        case '4':
                            $(this).find('.timestamp .countRating' + y).html($('.fourthrate').val().split(':')[0]);
                            $(this).find('.statusRatingQuestion > p').html($('.fourthrate').val().split(':')[1]);
                            break;
                        case '5':
                            $(this).find('.timestamp .countRating' + y).html($('.fifthrate').val().split(':')[0]);
                            $(this).find('.statusRatingQuestion > p').html($('.fifthrate').val().split(':')[1]);
                            break;
                        default:
                            break;
                    }
                    if (rate < y) {
                        $('.liScale' + y).addClass('hidden');
                    }
                });
                $("[id^='lipoint']").each(function () {
                    var t = $(this).attr('process');
                    if (questionLength < t) {
                        $(this).addClass('hidden');
                    }
                });
                var start = optionsQuestion[0];
                $('#countRatingQuestion' + start).addClass('current');
                $(".prev-question-feedback").hide();
                var j = 0;
                if (optionsQuestion.length >= 1) {
                    $('.questions-bank-feedback').addClass('hidden');
                    $('.joomdle-preview').removeClass('hidden');
                    $("[id^='countRatingQuestion']").each(function () {
                        var y = $(this).attr('q');
                        var check = optionsQuestion.indexOf(y);
                        if (check < 0) {
                            $('#countRatingQuestion' + y).addClass('hidden');
                        }
                        if (check >= 0 && $(this).hasClass('hidden')) {
                            $('#countRatingQuestion' + y).removeClass('hidden');
                        }
                        if ($(".countRatingQuestion:not(.hidden)[data-type='likert']").length == 0) {
                            $('.likertpcolor').hide();
                        } else $('.likertpcolor').show();
                        if ($(".countRatingQuestion:not(.hidden)[data-type='textresponse']").length == 0) {
                            $('.trpcolor').hide();
                        } else $('.trpcolor').show();
                    });
                }
                else {
                    lgtCreatePopup('oneButton', {content: "You did not choose any question."}, function () {
                        lgtRemovePopup();
                    });
                }

                var q = 0;
                // check point question first

                $(".next-question-feedback").off().on('click', function () {
                    var optionsQuestion = load_option_question();
                    $('#countRatingQuestion' + optionsQuestion[q]).removeClass('current');
                    $("#statusPoint" + q).removeClass('active');
                    $('#countRatingQuestion' + optionsQuestion[q + 1]).addClass('current');
                    $("#statusPoint" + (q + 1)).addClass('active');
                    ++q;

                    // delta is jump index to position new
                    if (q >= (optionsQuestion.length - 1)) {
                        $(".next-question-feedback").hide();
                        $(".prev-question-feedback").show();
                    }
                    else {
                        $(".next-question-feedback").show();
                        $(".prev-question-feedback").show();
                    }
                });
                $(".prev-question-feedback").off().on('click', function () {
                    var optionsQuestion = load_option_question();
                    $('#countRatingQuestion' + optionsQuestion[q]).removeClass('current');
                    $("#statusPoint" + q).removeClass('active');
                    $('#countRatingQuestion' + optionsQuestion[q - 1]).addClass('current');
                    $("#statusPoint" + (q - 1)).addClass('active');
                    --q;

                    if (q <= 0) {
                        $(".prev-question-feedback").hide();
                        $(".next-question-feedback").show();
                    }
                    else {
                        $(".prev-question-feedback").show();
                        $(".next-question-feedback").show();
                    }
                });

            });


            // Click button Save
            $('.questions-bank-feedback .btSave').off().on('click', function () {
                var optionsQuestion = load_option_question();
                var questionChoose = "1,2,3,4,5,6,7,8,9,10";
                document.getElementById("questionnum").value = questionChoose;
                var questionLength = optionsQuestion.length;
                var jsonString = JSON.stringify(optionsQuestion);
                if (optionsQuestion.length < 1) {
                    lgtCreatePopup('oneButton', {content: "You did not choose any question."}, function () {
                        lgtRemovePopup();
                    });
                } else {
                    document.getElementById("questionForm").submit();
                }
            });
            $('.statusRatingQuestion').click(function () {
                $(this).parents('.scaleRatingQuestion').find('.statusRatingQuestion').removeClass('active');
                $(this).addClass('active');
            });

            $('.inputQuestType .status').click(function () {
                var oldv = $('.hdSelectedQuestions').val();
                var count = parseInt($('.selectedCount').html());
                var qmid = $(this).parent('.optionQuestionMethod').attr('qmid');
                if (!$(this).parent().hasClass('active')) {
                    $('.selectedCount').html(count + 1);
                    var arr = oldv.split(',');
                    arr.push($(this).attr('qmid'));
                    $('.hdSelectedQuestions').val(arr.toString());
                    $('.question' + qmid + 'deleted').val('n');
                } else {
                    $('.selectedCount').html(count - 1);
                    var arr = oldv.split(',');
                    var index = arr.indexOf($(this).parent().attr('qmid'));
                    if (index > -1) {
                        arr.splice(index, 1);
                    }
                    $('.hdSelectedQuestions').val(arr.toString());
                    $('.question' + qmid + 'deleted').val('y');
                }
                $(this).parent().toggleClass('active');
                load_option_question();
            });

        })(jQuery);
    </script>

<?php endif; ?>
