<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewfeedback extends JViewLegacy {
    function display($tpl = null) {
        global $mainframe;

        $document = JFactory::getDocument();
        $app = JFactory::getApplication();
        $pathway = $app->getPathWay();
        $menus = $app->getMenu();
        $menu = $menus->getActive();

        $params = $app->getParams();
        $this->assignRef('params', $params);
        $user = JFactory::getUser();
        $username = $user->username;

        $id =  JRequest::getVar( 'course_id' , null, 'NEWURLFORM' );
        if (!$id) $id =  JRequest::getVar( 'course_id' );
        if (!$id) $id = $params->get( 'course_id' );

        $id = (int) $id;

        if (!$id) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
//            return;
        }

        $this->course_id = (int) $id;

        $this->hasPermission = JFactory::hasPermission($id, $username);
        if (!$this->hasPermission[0]['hasPermission']) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//            return;
        }

        $action = JRequest::getVar( 'action' , null, 'NEWURLFORM' );
        if (!$action) $action =  JRequest::getVar( 'action' );
        $this->action = $action;

        $mid =  JRequest::getVar( 'mid', null, 'NEWURLFORM' );
        if (!$mid) $mid =  JRequest::getVar( 'mid' );
        $this->mid = $mid;

        if ($params->get('use_new_performance_method'))
            $course_data = JHelperLGT::getCoursePageData(['id' => $id, 'username' => $username]);
        else
            $course_data = JoomdleHelperContent::call_method('get_course_page_data', (int)$id, $username);

        $this->course_info = json_decode($course_data['course_info'], true);
        $this->mods = json_decode($course_data['mods'], true);

        if ($action == 'create') {
            $document->setTitle('Add Feedback');
//        $this->course_info = JoomdleHelperContent::getCourseInfo((int)$id, $username);
//        $this->is_enroled = $this->course_info['enroled'];
//        if ($this->is_enroled) {
//            $this->mods = JoomdleHelperContent::call_method('get_course_mods', (int)$id, $username, 'questionnaire');
//        }else {
//            $this->mods = JoomdleHelperContent::call_method('get_course_mods', (int)$id, '','questionnaire');
//        }

            $t = 0;
            if (is_array($this->mods)) {
                foreach ($this->mods['coursemods'] as $tema) {
                    $resources = $tema['mods'];
                    foreach ($resources as $id => $res) {
                        if ($res['mod'] == 'questionnaire') {
                            $t++;
                            break;
//                        $id_ques = $res['id'];
                        }
                    }
                }
                if ($t != 0) { // exist activity feedback
                    echo JText::_('COM_JOOMDLE_COURSE_CAN_NOT_CREATE_FEEDBACK');
                    return;
                }
            }
            // get question feedback default (1-8 scale question, 9-10 text response question)
            $this->questions = JoomdleHelperContent::call_method('get_feedback_question', 0);

        } else if ($action == 'edit') {
            if ($params->get('use_new_performance_method')) {
                $this->questionnaire = JHelperLGT::getModuleInfo('questionnaire', $mid, $id);
//                $this->feedback = JHelperLGT::getInfoFeedback($this->questionnaire['surveyid']);
            } else {
                $this->questionnaire = JoomdleHelperContent::call_method ( 'get_info_questionnaire', (int)$mid, $username);
//                $this->feedback = JoomdleHelperContent::call_method ( 'get_info_feedback', (int)$this->questionnaire['surveyid']);
            }
        } else {
            echo JText::_('COM_JOOMDLE_UNKNOWN_ACTION');
            return;
        }

        $document->setTitle(JText::_('COM_JOOMDLE_FEEDBACK'));
        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

        parent::display($tpl);
    }
}