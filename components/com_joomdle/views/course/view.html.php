<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewCourse extends JViewLegacy
{
    function display($tpl = null) {
        $app = JFactory::getApplication();
        $pathway = $app->getPathWay();
        $document = JFactory::getDocument();
        $menus = $app->getMenu();
        $menu = $menus->getActive();
        $params = $app->getParams();

        $this->assignRef('params', $params);
        /* Get courseid from URL */
        $id = JRequest::getVar('course_id', null, 'NEWURLFORM');
        if (!$id) $id = JRequest::getVar('course_id');
        if (!$id) $id = $params->get('course_id');
        $id = (int)$id;

        $redirect = JRequest::getVar('redirect', null, 'NEWURLFORM');
        if (!$redirect) $redirect = JRequest::getVar('redirect');
        if (!$redirect) $redirect = $params->get('redirect');
        JPluginHelper::importPlugin('joomdlesocialgroups');
        $dispatcher = JDispatcher::getInstance();
        $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($id));
        if (!$id) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
//            return;
        }
        
        /*
         *  Render status
         */
        if(isset($_POST['act']) && $_POST['act'] == 'update_status') {
            $modid = $_POST['id'];
            $username = $_POST['username'];
            $act_type = $_POST['act_type'];
            
            // get mod completion
            
            $data = JHelperLGT::getModCompletionStatus(['modid' => $modid, 'username' => $username, 'act_type' => $act_type]);
            
            $result = array();
            $result['message'] = 'success';
            $result['status'] = true;
            $result['data'] = $data;
            echo json_encode($result);
            die;
         }
         if(isset($_POST['act']) && $_POST['act'] == 'check_completeFeedback') {
             $courseid = $_POST['courseid'];
             $username = $_POST['username'];
             
             $result = JHelperLGT::checkCompleteFeedback(['courseid' => $courseid, 'username' => $username]);
             $data['data'] = $result;
             echo json_encode($data);
             die;
         }
         if(isset($_POST['act']) && $_POST['act'] == 'check_course_percent') {
             $courseid = $_POST['courseid'];
             $username = $_POST['username'];
             $result['data'] = JHelperLGT::checkCoursePercent(['courseid'=>$courseid, 'username'=>$username]);
             echo json_encode($result);
             die;
         }
        
        /* Get data of user */
        $user = JFactory::getUser();
        $username = $user->username;

        /*  Get course data(course information,page, assigment, assessment, scorm ..)
            @param $id: courseid
            @param $username: username */
        if ($params->get('use_new_performance_method'))
            $course_data = JHelperLGT::getCoursePageData(['id' => $id, 'username' => $username]);
        else
            $course_data = JoomdleHelperContent::call_method('get_course_page_data', (int)$id, $username);
    
        $this->course_info = json_decode($course_data['course_info'], true);

        // Check Student complete course feedback
//        $this->checkCompleteFeedback = $course_data['checkCompleteFeedback'];

        /* Check course enrol */
        $this->is_enroled = $this->course_info['enroled'];
        $userRoles = json_decode($course_data['userRoles'], true);
        $a = json_decode($userRoles["roles"][0]['role'], true);
        $user_role = $a[0]['sortname'];

//        /* If role leaner and facilitator check course publish(if course dont publish return message) */
//        if($user_role == 'student' || $user_role == 'teacher') {
//            if($this->course_info['course_status'] == ""){
//                echo JText::_('COM_JOOMDLE_COURSE_HAS_NOT_BEEN_PUBLISHED_YET');
//                return;
//            }
//        }
        $this->hasPermission = $userRoles['roles'];

        $myRoles = array();
        if (!empty($userRoles['roles'])) {
            foreach (json_decode($userRoles['roles'][0]['role']) as $r) {
                if (!in_array($r->sortname, $myRoles)) $myRoles[] = $r->sortname;
            }
        }
        $this->myRoles = $myRoles;
        
        $db = JFactory::getDbo();
        $db->setQuery('SELECT `product_name` ,  `product_id` FROM ' . $db->quoteName('#__hikashop_product') . ' WHERE product_code =' . $id);
        $product_hikashop_id = $db->loadObject();
        if ($this->is_enroled == 0 && $this->course_info['course_status'] == 'published' && $product_hikashop_id->product_id) {     // Link to store
            $url_store = JUri::base() . 'component/hikashop/product/cid-' . $product_hikashop_id->product_id;
            header('Location: '.$url_store);
            exit;
        }
        
        $this->hasPermission = $userRoles['roles'];
        if (($this->is_enroled == 0 && !$this->hasPermission[0]['hasPermission'] && $redirect == null) || (!$publish_group[0] && !in_array('coursecreator', $myRoles) )||
                ((in_array('student', $myRoles) || in_array('teacher', $myRoles)) && empty(array_intersect($myRoles, ['editingteacher','manager', 'coursecreator'])) && $this->course_info['visible'] == 0)) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_COURSE_NOT_ENROLL');
//            return;
        }

        if ($this->is_enroled == 0 && !$this->hasPermission[0]['hasPermission'] && $redirect != null) {
            echo JText::_('COM_JOOMDLE_COURSE_REFRESH_10');
            header("Refresh:10");
            return;
        }
        /* Data activitys */
        $this->mods = json_decode($course_data['mods'], true);
        $this->modsForCreator = json_decode($course_data['modsForCreator'], true);
        $this->questions = array();
        foreach (json_decode($course_data['questions'], true)['questions'] as $key => $value) {
            $this->questions[$value['id']] = $value;
        }
        /* Data topics */
        $topics = json_decode($course_data['topics'], true);
        $this->topics = $topics['status'] ? $topics['datas'] : [];
        /* pathway */
        $cat_slug = $this->course_info['cat_id'] . ":" . $this->course_info['cat_name'];
        $course_slug = $this->course_info['remoteid'] . ":" . $this->course_info['fullname'];
        $this->course_status = $this->course_info['course_status'];
        if (is_object($menu) && $menu->query['view'] != 'course') {
            $pathway->addItem($this->course_info['cat_name'], 'index.php?view=coursecategory&cat_id=' . $cat_slug);
            $pathway->addItem($this->course_info['fullname'], 'index.php?view=detail&cat_id=' . $cat_slug . '&course_id=' . $course_slug);
            $pathway->addItem(JText::_('COM_JOOMDLE_COURSE_CONTENTS'), '');
        }
        /* Check old course(fix visible activity + check course complete setting) */
        $visible = 0;
        $this->mods_not_visible = JoomdleHelperContent::call_method ( 'get_course_mods_visible', (int) $id, '',$visible);
        if (is_array ($this->mods_not_visible)) {
            $moduleIdStatus1 = array();
            $i=0;
            foreach ($this->mods_not_visible as $tema) {
                foreach ($tema['mods'] as $id => $resource) {
                    $moduleIdStatus1[$i]['module_id'] = $resource['id'];
                    $moduleIdStatus1[$i]['status'] = 1;
                    $i++;
                }
            }
            JoomdleHelperContent::call_method('update_multi_activity_module', $moduleIdStatus1, 0,  (int)$this->course_info['remoteid']);
        }

        if (in_array('coursecreator', $myRoles)) $document->setHTMLTagClass('manager-course');
             
        /* View course learner */
        if (in_array('student', $myRoles) && empty(array_intersect($myRoles, ['teacher','editingteacher', 'manager', 'coursecreator']))) {
            $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
            if (!isset($_SERVER['HTTP_REFERER'])) $document->setHTMLTagClass('learning-highlight');
            parent::display('learncourse');
        }
        /* View course facilitator and manager (when course published, pending approval, approved, self enrolment) */
        else if (
            (in_array('teacher', $myRoles) && empty(array_intersect($myRoles, ['editingteacher', 'manager', 'coursecreator'])))
            || (
                // $user_role != 'student' && 
                !empty(array_intersect($myRoles, ['editingteacher', 'manager', 'coursecreator'])) &&
                ($this->course_status == 'pending_approval' || $this->course_status == 'approved'|| $this->course_status == 'published' || $this->course_info['self_enrolment']))) {
            if (!isset($_SERVER['HTTP_REFERER'])) $document->setHTMLTagClass('learning-highlight');
            parent::display('facilitatorcourse');
        } else {
            /*View course manager (course publish not yet) */
            $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
            if (!isset($_SERVER['HTTP_REFERER'])) $document->setHTMLTagClass('manage-highlight');
            parent::display('managecourse');
        }
    }
}
?>