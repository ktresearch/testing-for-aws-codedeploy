<?php defined('_JEXEC') or die('Restricted access');

$itemid = JoomdleHelperContent::getMenuItem();
$linkstarget = $this->params->get('linkstarget');
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'components/com_joomdle/views/course/tmpl/course.css');
$document->addScript(JUri::root() . 'components/com_community/assets/autosize.min.js');

$document->setTitle(JText::_('COM_JOOMDLE_COURSE_OUTLINE'));
$jump_url = JoomdleHelperContent::getJumpURL();

$user = JFactory::getUser();
$username = $user->username;

$session = JFactory::getSession();
$is_mobile = ($session->get('device') == 'mobile') ? true : false;

$course_id = $this->course_info['remoteid'];
$direct_link = 1;
$show_summary = $this->params->get('course_show_summary');
$show_topics_numbers = $this->params->get('course_show_numbers');

$role = $this->hasPermission;
$user_role = array();

if (!empty($role)) {
    foreach (json_decode($role[0]['role'], true) as $r) {
        $user_role[] = $r['sortname'];
    }
}

$enableCourseSurvey = ($this->course_info['coursesurvey']) ? true : false;
$k = 0;
foreach ($this->topics['sections'] as $topic) {
    $k = $topic['section'];
    $array_topics[$k] = ($topic['id']);
}
if ($this->course_info['guest'])
    $this->is_enroled = true;

$check_course_creator = $this->hasPermission[0]['hasPermission'];
if ($check_course_creator) {
    $class_creator = "creator";
}
$count_feedback = 0;
$link_feedback_learner = 0;
if (is_array($this->mods)) {
    foreach ($this->mods as $tema) {
        $resources = $tema['mods'];
        foreach ($resources as $id => $res) {
            if ($res['mod'] == 'questionnaire') {
                $count_feedback++;
                $id_ques = $res['id'];
            }
            if ($res['mod'] == 'page') {
                $id_page = $res['id'];

            }
        }
    }
    if ($count_feedback != 0) { // exist activity feedback
        if ($user_role[0] == 'teacher' || $user_role[0] == 'editingteacher') { // is facilitator
            $link = JUri::base() . "feedbackview/" . $course_id . "_" . $id_ques . "_review.html";
        }
        if ($user_role[0] == 'student') { // is learner
            $link = JUri::base() . "feedbackview/" . $course_id . "_" . $id_ques . "_.html";
        }
        if ($check_course_creator == true) { // is course creator
            $link = JUri::base() . 'feedback/edit_' . $course_id . '_' . $id_ques . '.html';
        }
    }
    // if(not exist and role is course creator)
    if ($count_feedback == 0) { // not exist activity feedback
        if ($check_course_creator == true) { // is course creator
            $link = JUri::base() . 'feedback/create_' . $course_id . '.html';
        }
        if ($check_course_creator == false) { // is not course cretor
            $link = '';
        }
    }
}

$editItems = [];
if (is_array($this->modsForCreator)) {
    foreach ($this->modsForCreator as $value) {
        $editItems[(int)$value['id']] = $value['data']['name'];
    }
}

if ($this->course_info['course_type'] && $this->course_info['course_type'] == 'course_lp') {
    $coursetype = 'lpcourse';
} else if ($this->course_info['course_type'] && $this->course_info['course_type'] == 'course_non') {
    $coursetype = 'noncourse';
} else {
    $coursetype = 'noncourse';
}
require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);
$render_banner = $banner->renderBanner($this->course_info, $this->mods, $role);

$summary = $this->course_info['summary'];
$learningOutcome = $this->course_info['learningoutcomes'];
$tartgetAudience = $this->course_info['targetaudience'];
?>
<script src="components/com_joomdle/js/jquery-1.11.1.js"></script>
<script src="components/com_joomdle/js/jquery-ui.min.js"></script>
<script src="components/com_joomdle/js/jquery.ui.touch-punch.min.js"></script>
<div class="joomdle-course <?php echo $this->pageclass_sfx ?>">
    <form id="formUpdateActivity" class="form" method="post"
          action="<?php echo JRoute::_('index.php?option=com_joomdle&task=updateActivity'); ?>">
        <div class="course-content-detail" style="display: none;">
            <div class="content-summary">
                <div class="sumHeader"><?php echo JText::_('COM_JOOMDLE_COURSE_DESCRIPTION'); ?></div>
                <div class="sumContent">
                    <p class="card-description"><?php echo nl2br($summary); ?></p>
                    <span class="more"></span>
                    <span class="less"></span>
                </div>
            </div>
            <?php if ($learningOutcome != null) { ?>
                <div class="content-learning-outcome limitOutcome">
                    <div class="joomdle_learning_outcome"><?php echo JText::_('COM_JOOMDLE_LEARNING_OUTCOME'); ?></div>
                    <div class="outcomeContent">
                        <p class="card-description"><?php echo nl2br($learningOutcome); ?></p>
                        <span class="more"></span>
                        <span class="less"></span>
                    </div>
                </div>
            <?php }

            if ($tartgetAudience != null) { ?>
                <div class="targetAudience">
                    <div class="targetHeader"><?php echo JText::_('COM_JOOMDLE_COURSE_TARGET_AUDIENCE'); ?></div>
                    <div class="targetAudienceContent">
                        <p class="card-description"><?php echo nl2br($tartgetAudience); ?></p>
                        <span class="more"></span>
                        <span class="less"></span>
                    </div>
                </div>
            <?php } ?>
        </div>

        <script type="text/javascript">
            (function ($) {
                if ($(".t3-content").children().hasClass("course-menu") == false) {
                    $('.joomdle-course ').css("margin-top", "0px");
                }

            })(jQuery);
        </script>

        <div class="joomdle_course_outline">
            <?php echo JText::_('COM_JOOMDLE_COURSE_OUTLINE'); ?>
        </div>
        <?php
        $icon_url_feedback = '';
        $icon_url_cert = '';
        $mtype_feedback = '';
        $mtype_cert = '';
        $target_feedback = '';
        $target_cert = '';
        $url_feedback = '';
        $url_cert = '';

        if (is_array($this->mods)) {
            $section_count = 0;

            echo "<ul class='nested_with_switc' style='padding-left: 0;'>";
            foreach ($this->mods as $tema) :
                $hidden = '';
                $class_sec_0 = '';
                $tam = '';
                if ($section_count == 0) {
                    $class_sec_0 = 'section_default';
                    $tam = "header";
                }
                ?>
                <li class="joomdle_course_section <?php if ($tema['section'] == 0) echo 'topic_default'; ?> <?php if ($tema['visible'] == 0) echo "invisible"; ?>"
                    style="<?php echo $hidden; ?>" sectionid="<?php echo $array_topics[$tema['section']]; ?>"
                    section="<?php echo $section_count; ?>"
                    id="joomdle_course_section<?php echo $array_topics[$tema['section']]; ?>">
                    <?php if ($section_count != 0): ?>
                        <div class="activity-delete  ">
                            <img class="optionCheckSection"
                                 id="optionCheckSection<?php echo $array_topics[$tema['section']]; ?>"
                                 qcount="<?php echo $section_count; ?>"
                                 qsid="<?php echo $array_topics[$tema['section']]; ?>"
                                 align="center" src="<?php echo JUri::base(); ?>/images/view_course/delete1_hd.png">
                        </div>
                    <?php endif; ?>
                    <?php if ($show_topics_numbers) : ?>
                        <div class="joomdle_item_title joomdle_section_list_item_title <?php if ($check_course_creator) echo "creator"; ?>">
                            <?php
                            $title = '';
                            if ($tema['name'])
                                $title = $tema['name'];
                            else {
                                if ($tema['section']) {
                                    $title = JText::_('COM_JOOMDLE_TOPIC') . " ";
                                    $title .= $tema['section'];
                                } else  $title = JText::_('COM_JOOMDLE_INTRO');
                            }

                            if ($check_course_creator) {
                                echo '<span class="pTitleSection creator" style="position: static;" id="pTitleSection' . $array_topics[$tema['section']] . '">' . ucwords($title) . '</span>';
                            } else
                                echo '<span class="pTitleSection" style="position: static;" id="pTitleSection' . $array_topics[$tema['section']] . '" >' . ucwords($title) . '</span>';
                            ?>
                            <input type="text" id="titleSection<?php echo $array_topics[$tema['section']]; ?>"
                                   class="<?php echo $class_sec_0; ?>"
                                   name="titleSection<?php echo $array_topics[$tema['section']]; ?>"
                                   style="font-family: 'Open Sans', OpenSans-Semibold, sans-serif;font-weight: 600;"
                                   value="<?php echo htmlentities(ucwords($title)); ?>"/>
                        </div>
                    <?php endif; ?>
                    <?php if ($section_count != 0): ?>
                        <?php echo '<div class="activity-sort activity-sort-section"><img align="center" class="handle" src="' . JUri::base() . 'images/rearrange_new.png"></div>'; ?>
                    <?php endif; ?>

                    <div class="joomdle_item_content joomdle_section_list_item_resources"
                         id="joomdle_item_content<?php echo $array_topics[$tema['section']]; ?>">
                        <?php
                        if ($show_summary)
                            if ($tema['summary'])
                                echo $tema['summary'];

                        $resources = $tema['mods'];
                        if (is_array($resources)) : ?>
                            <ul id="sortable<?php echo $section_count; ?>" class="connectedSortable ui-sortable <?php echo empty($resources) ? 'empty-list' : '' ?>"
                                sectionida="<?php echo $array_topics[$tema['section']]; ?>" style="min-height: 10px;">
                                <?php
                                foreach ($resources as $id => $resource) {
                                    $mtype = JoomdleHelperSystem::get_mtype($resource['mod']);
                                    if (!$mtype || ($resource['mod'] == 'questionnaire' && !$enableCourseSurvey)) // skip unknow modules
                                        continue;

                                    $css = '';
                                    if (($resource['mod'] == 'forum') || ($resource['mod'] == 'certificate') || ($resource['mod'] == 'feedback') || ($resource['mod'] == 'questionnaire')) {
                                        $css = 'display: none;';
                                    }
                                    if ($resource['mod'] == 'questionnaire') {
                                        $icon_url_feedback = JoomdleHelperSystem::get_icon_url($resource['mod'], $resource['type']);
                                        $mtype_feedback = ucfirst($mtype);
                                    } else if ($resource['mod'] == 'certificate') {
                                        $icon_url = JoomdleHelperSystem::get_icon_url($resource['mod'], $resource['type']);
                                        $activity_name = 'Certificate';
                                    } else {
                                        echo '<li class=" activity-list-item activity_normal ' . $class_creator . '" id="activity-list-item' . $resource['id'] . '" style="' . $css . '" moduleid="' . $resource['id'] . '" >';

                                        /* Delete activity */
                                        if ($check_course_creator) {
                                            echo '<div class="activity-delete "><img class="optionCheckModule" id="optionCheckModule' . $resource['id'] . '" qmid="' . $resource['id'] . '" align="center" src="' . JUri::base() . 'images/view_course/delete1_hd.png"></div>';
                                        }

                                        $icon_url = JoomdleHelperSystem::get_icon_url($resource['mod'], $resource['type']);
                                        if ($icon_url) {
                                            if ($mtype == 'quiz') {
                                                $activity_name = 'Assessment';
                                                $edit_activity_link = 'assessment/edit_' . $course_id . '_' . $resource['atype'] . '_' . $resource['id'] . '.html';
                                            }
                                            if ($mtype == 'assign') {
                                                $activity_name = 'Assignment';
                                                $edit_activity_link = 'assignment/edit_' . $course_id . '_' . $resource['id'] . '.html';
                                            }
                                            if ($mtype == 'page' || $mtype == 'forum' || $mtype == 'url') {
                                                $activity_name = 'Page';
                                                if ($mtype == 'page') {
                                                    $edit_activity_link = 'content/edit_' . $course_id . '_' . $resource['id'] . '_' . $array_topics[$tema['section']] . '.html';
                                                }
                                            }
                                            if ($mtype == 'scorm') {
                                                $activity_name = 'Scorm';
                                                $edit_activity_link = 'scorm/edit_' . $course_id . '_' . $resource['id'] . '.html';
                                            }
                                            if ($mtype == 'questionnaire') {
                                                $activity_name = 'Questionnaire';
                                            }
                                            if ($mtype == 'certificate') {
                                                $activity_name = 'Certificate';
                                            }
                                            if ($mtype == 'skillsoft') {
                                                $activity_name = 'Skillsoft';
                                            }
                                            if ($mtype == 'resource') {
                                                $activity_name = 'Resource';
                                            }
                                            echo '<a href="' . $edit_activity_link . '" style="position: static; align-self: flex-start;">';
                                            echo '<div class="activity-image ' . $mtype . '" >';
                                            echo '<img class="activity-icon" src="' . $icon_url . '">';
                                            echo '<div class="activity-name-box">';
//                                        echo '<p class="activity-name">' . ucfirst($activity_name) . '</p>';
                                            echo '</div>';
                                            echo '</div>';
                                            echo '</a>';
                                        }
                                    }
                                    if ($resource['mod'] == 'label') {
                                        $label = JoomdleHelperContent::call_method('get_label', (int)$resource['id']);
                                        echo '</P>';
                                        echo $label['content'];
                                        echo '</P>';
                                        continue;
                                    }

                                    if ((($this->is_enroled) && ($resource['available'])) || $check_course_creator) {
                                        $direct_link = JoomdleHelperSystem::get_direct_link($resource['mod'], $course_id, $resource['id'], $resource['type']);
                                        if ($direct_link) {
                                            // Open in new window if configured like that in moodle
                                            if ($resource['display'] == 6)
                                                $resource_target = 'target="_blank"';
                                            else
                                                $resource_target = '';

                                            if ($direct_link != 'none') {
                                                if ($resource['mod'] == 'questionnaire') {
                                                    $url_feedback = $direct_link;
                                                    $target_feedback = $resource_target;
                                                } elseif ($resource['mod'] == 'certificate') {
                                                    $url_cert = $direct_link;
                                                    $target_cert = $resource_target;
                                                } else {
                                                    echo "<a $resource_target  href=\"" . $direct_link . "\">" . $resource['name'] . "</a>";
                                                }
                                            }
                                        } else if ($resource['mod'] == 'questionnaire') {
                                            $url_feedback = JURI::base() . "mod/" . $mtype . "_" . $course_id . "_" . $resource['id'] . "_" . $itemid . ".html";
                                            $target_feedback = $target;
                                        } elseif ($resource['mod'] == 'certificate') {
                                            $url_cert = JURI::base() . "mod/" . $mtype . "_" . $course_id . "_" . $resource['id'] . "_" . $itemid . ".html";
                                            $module_id_certificate = $resource['id'];
                                            $target_cert = $target;
//                                            if ($check_course_creator) {
//                                                echo "<a $target_cert href=\"" . JURI::base() . "mod/" . $mtype . "_" . $course_id . "_" . $resource['id'] . "_" . $itemid . ".html\" id='activity-link" . $resource['id'] . "' class='activity-link'>" . ucwords($resource['name']) . "</a>";
//                                                ?>
                                            <!---->
                                            <!--                                                --><?php
//                                            }
                                        } else {
                                            echo "<a $target href=\"" . JURI::base() . "mod/" . $mtype . "_" . $course_id . "_" . $resource['id'] . "_" . $itemid . ".html\" id='activity-link" . $resource['id'] . "' class='activity-link'>" . ucwords(isset($editItems[(int)$resource['id']]) ? $editItems[(int)$resource['id']] : $resource['name']) . "</a>";
                                            ?>
                                            <textarea type="text" id="titleModule<?php echo $resource['id']; ?>"
                                                      class="textModule" getid="<?php echo $resource['id']; ?>"
                                                      name="titleModule<?php echo $resource['id']; ?>"><?php echo ucwords(isset($editItems[(int)$resource['id']]) ? $editItems[(int)$resource['id']] : $resource['name']); ?></textarea>

                                            <?php
                                            $class = '';
                                            if ($resource['mod_completion']) {
                                                $class = 'completed';
                                            }
                                            if (!$check_course_creator) echo '<div class="status' . $class . '"></div>';
                                        }
                                    } else {
                                        echo $resource['name'] . '</div>';
                                        if ((!$resource['available']) && ($resource['completion_info'] != '')) : ?>
                                            <div class="joomdle_completion_info">
                                                <?php echo $resource['completion_info']; ?>
                                            </div>
                                        <?php
                                        endif;
                                    }
                                    /* Sort activity */
                                    if ($check_course_creator && $resource['mod'] != 'certificate' && $resource['mod'] != 'questionnaire' && $resource['mod'] != 'feedback' && $resource['mod'] != 'forum') {
                                        echo '<div class="activity-sort "><img align="center" id="sortCheckModule' . $resource['id'] . '" class="handle" src="' . JUri::base() . 'images/rearrange_new.png"></div>';
                                    }
                                    if ($resource['mod'] == 'certificate') {
                                        $exist_certificate = true;
                                    }
                                    ?>
                                    <?php
                                    echo '</li>';
                                }
                                ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                    <div class="create_activity row" id="create_activity<?php echo $array_topics[$tema['section']]; ?>">
                        <div class="create_page col-md-3 col-sm-3 col-xs-6">
                            <a href="<?php echo JUri::base() . 'content/create_' . $course_id . '_' . $array_topics[$tema['section']] . '.html'; ?>">
                                <img class="iconCreateActivity" id="iconCreatePage" align="center" src="<?php echo JUri::base(); ?>images/view_course/addpage_hd.png">
                                <div class="create-activity-name-box">
                                    <p class="activity-name">Activity</p>
                                </div>
                            </a>
                        </div>
                        <div class="create_scorm col-md-3 col-sm-3 col-xs-6" sectionid="<?php echo $array_topics[$tema['section']]; ?>">
                            <a href="<?php echo JUri::base() . 'scorm/create_' . $course_id . '_' . $array_topics[$tema['section']] . '.html'; ?>">
                                <img class="iconCreateActivity" id="iconCreatePage" align="center"
                                     src="<?php echo JUri::base(); ?>images/view_course/addscorm_hd.png">
                                <div class="create-activity-name-box">
                                    <p class="activity-name">SCORM</p>
                                </div>
                            </a>
                        </div>
                        <div class="create_assessment col-md-3 col-sm-3 col-xs-6" sectionid="<?php echo $array_topics[$tema['section']]; ?>">
                            <a href="<?php echo JUri::base() . 'assessment/create_' . $course_id . '_' . $array_topics[$tema['section']] . '.html'; ?>">
                                <img class="iconCreateActivity" id="iconCreatePage" align="center" src="<?php echo JUri::base(); ?>images/view_course/addassessment_hd.png">
                                <div class="create-activity-name-box">
                                    <p class="activity-name">Assessment</p>
                                </div>
                            </a>
                        </div>
                        <div class="create_assignment col-md-3 col-sm-3 col-xs-6" sectionid="<?php echo $array_topics[$tema['section']]; ?>">
                            <a href="<?php echo JUri::base() . 'assignment/create_' . $course_id . '_' . $array_topics[$tema['section']] . '.html'; ?>">
                                <img class="iconCreateActivity" id="iconCreatePage" align="center" src="<?php echo JUri::base(); ?>images/view_course/addassignment_hd.png">
                                <div class="create-activity-name-box">
                                    <p class="activity-name">Assignment</p>
                                </div>                            
                            </a>
                        </div>
                    </div>
                    <div class="divToggle"></div>
                </li>
                <?php
                $section_count++;
            endforeach;
            echo "</ul>";

        }
        ?>

        <input type="hidden" name="courseid" id="courseid" value="<?php echo $course_id; ?>"/>
        <input type="hidden" name="username" id="username" value="<?php echo $username; ?>"/>
        <input type="hidden" name="arr_section_id" id="arr_section_id"/>
        <input type="hidden" name="moduleIdNumHidden" id="moduleIdNumHidden"/>
        <input type="hidden" name="sectionIdNumDelete" id="sectionIdNumDelete"/>
        <?php foreach ($this->topics['sections'] as $topic) : ?>
            <input type="hidden" name="<?php echo $topic['id']; ?>" id="<?php echo $topic['id']; ?>"/>
        <?php endforeach; ?>
    </form>

    <?php if ($check_course_creator && ($this->course_status != 'pending_approval' && $this->course_status != 'approved' && !$this->course_info['self_enrolment'])) : ?>
        <div class="for-admins-buttons">
            <div class="btAddAnActivity">
                <img class="btAddAnActivityText" src="<?php echo JUri::base(); ?>images/view_course/add_hd.png">
                <div class="form-group div-input-topic-title">
                    <input type="text" placeholder="<?php echo JText::_('ADD_TOPIC_HERE'); ?>" name="topic_title" id="topic_title"/>
                </div>
                <div class="create_activity row">
                    <div class="create_page col-md-3 col-sm-3 col-xs-6">
                        <img class="iconCreateActivity" id="iconCreatePage" align="center" src="<?php echo JUri::base(); ?>images/view_course/addpage_hd.png">
                        <div class="create-activity-name-box">
                            <p class="activity-name">Activity</p>
                        </div>
                    </div>
                    <div class="create_scorm col-md-3 col-sm-3 col-xs-6">
                        <img class="iconCreateActivity" id="iconCreatePage" align="center" src="<?php echo JUri::base(); ?>images/view_course/addscorm_hd.png">
                        <div class="create-activity-name-box">
                            <p class="activity-name">SCORM</p>
                        </div>
                    </div>
                    <div class="create_assessment col-md-3 col-sm-3 col-xs-6">
                        <img class="iconCreateActivity" id="iconCreatePage" align="center" src="<?php echo JUri::base(); ?>images/view_course/addassessment_hd.png">
                        <div class="create-activity-name-box">
                            <p class="activity-name">Assessment</p>
                        </div>
                    </div>
                    <div class="create_assignment col-md-3 col-sm-3 col-xs-6">
                        <img class="iconCreateActivity" id="iconCreatePage" align="center" src="<?php echo JUri::base(); ?>images/view_course/addassignment_hd.png">
                        <div class="create-activity-name-box">
                            <p class="activity-name">Assignment</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="addTopic"></div>
    <?php endif; ?>

    <!--        Block certificate-->
    <?php if ($check_course_creator && $exist_certificate == true) : ?>
        <div class="joomdle_item_content joomdle_section_list_item_resources certificate_section">
            <p class="pTitleCertificate"><?php echo JText::_('COM_JOOMDLE_COURSE_CERTIFICATE'); ?></p>
            <div class="activity-list-item">
                <div class="activity-image certificate">
                    <img src="<?php echo JURI::base();?>images/view_course/certificate_hd.png" align="middle">
                </div>
                <p class="description_certificate"><?php echo JText::_('COM_JOOMDLE_AWARD_A_CERTIFICATE_TO_LEARNERS'); ?></p>
            </div>
        </div>
    <?php endif; ?>
    <!--        Block feedback-->
    <?php if ($link != null && $enableCourseSurvey) : ?>
        <div class="div-feedback">
            <div class="joomdle_item_content joomdle_section_list_item_resources feedback_section">
                <p class="pTitleFeedback"><?php echo JText::_('COM_JOOMDLE_COURSE_FEEDBACK'); ?></p>
                <div class="activity-list-item" style="">
                    <div class="activity-image feedback">
                        <img src="<?php echo JURI::base();?>images/view_course/feedback_hd.png" align="middle">
                    </div>
                    <p class="description_feedback"><?php echo JText::_('COM_JOOMDLE_COLLECT_LEARNER_FEEDBACK_ABOUT_THE_COURSE'); ?></p>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="button_control ">
        <div class="btSave pull-right"><?php echo JText::_('COURSE_CONTENT_BTN_SAVE'); ?></div>
        <div class="btPreviewCourse pull-right">
            <?php echo JText::_('COURSE_BUTTON_PREVIEW_COURSE'); ?>
        </div>
        <div class="btCancel2 pull-right"><?php echo JText::_('COURSE_CONTENT_BTN_CANCEL'); ?></div>
    </div>
    <?php if ($this->params->get('show_back_links')) : ?>
        <div>
            <p align="center">
                <a href="javascript: history.go(-1)"><?php echo JText::_('COM_JOOMDLE_BACK'); ?></a>
            </p>
        </div>
    <?php endif; ?>

</div>
<div class="modal_course_outline" id="modal_course_outline" style="display: none;"></div>
<div class="modal_create_topic" id="modal_create_topic" style="display: none;"></div>
<div class="notification">
</div>

<!--Preview Course( preview page, assignment, assessment, scorm) -->
<div class="manage-preview hidden" style="clear: both;">
    <?php
    foreach ($this->modsForCreator as $tema1) {
        if ($tema1['type'] == 'page' || $tema1['type'] == 'forum' || $tema1['type'] == 'url') {
            $id_page = (int)$tema1['id'];
            $data_page = $tema1['data'];
            ?>
            <!--        Page Activity Preview-->
            <div class="preview_page preview_activity" id="preview_<?php echo $id_page; ?>">
                <div class="divPreview">
                    <p class="previewTitle"><?php echo $data_page['name']; ?></p>
                    <p class="previewDescription">
                        <?php
                        $data = json_decode($data_page['params']);
                        $typev = array('linkVideo', 'video', 'embedVideo');
                        foreach ($data as $k => $v) {
                            if ($v->type == 'text') {
                                echo '<div class="isContent">' . $v->content . '</div>';
                                echo '<div class="cline"></div>';
                            } else if ($v->type == 'caption') {
                                echo '<div class="isCaption">' . $v->content . '</div>';
                                echo '<div class="cline"></div>';
                            } else if (!in_array($v->type, $typev)) {
                                echo $v->content;
                                echo '<div class="cline"></div>';
                            } else {
                                echo $v->content;
                            }
                        } ?>
                    </p>
                </div>
            </div>
        <?php } ?>

        <!--        Assignment Activity Preview-->
        <?php if ($tema1['type'] == 'assign') { // ASSIGNMENT
            $id_assign = (int)$tema1['id'];
            $data_assign = $tema1['data'];
//            $data_assign = JoomdleHelperContent::call_method('create_assignment_activity', 0, $course_id, $username, array('aid' => $id_assign));
            if ($data_assign['duedate'] <= 0) {
                $data_duedate = '-';
            } else if (date('d/m/Y', $data_assign['duedate']) == '01/01/1970')
                $data_duedate = date('D, d M Y H:i:s', time());
            else $data_duedate = date('D, d M Y H:i:s', $data_assign['duedate']);
            ?>
            <div class="preview_assignment preview_activity" id="preview_<?php echo $id_assign; ?>">
                <div class="divPreview">
                    <div style="clear:both;"></div>
                    <div class="title"><span class="activity"><?php echo $data_assign['name']; ?></span></div>
                    <div class="content">
                        <div class="intructions">
                            <p class="intructions_title"><?php echo JText::_('COM_JOOMDLE_INSTRUCTIONS'); ?> </p>
                            <div class="text_to_html"><?php echo $data_assign['des']; ?></div>
                        </div>
                        <div class="date">
                            <p><?php echo JText::_('COM_JOOMDLE_DUE_DATE'); ?>: <span class="status1" data-duedate="<?php echo $data_duedate; ?>"></span></p>
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="submission_status">
                        <p><?php echo JText::_('COM_JOOMDLE_ASSIGNMENT_SUBMIT_STATUS'); ?>: <span class="status1">Not Yet Submitted</span></p>
                    </div>
                    <div class="grade_status">
                        <p><?php echo JText::_('COM_JOOMDLE_ASSIGNMENT_GRADING_STATUS'); ?>: <span class="status1">Not Yet Graded</span></p>
                    </div>
                    <div class="bttn">
                        <a class="btn-download"><?php echo strtoupper(JText::_('COM_JOOMDLE_ASSIGNMENT_DOWNLOAD_BRIEF')); ?></a>
                    </div>
                    <div class="btn-upload">
                        <span class="btnUpload"><?php echo JText::_('COM_JOOMDLE_ASSIGNMENT_SUBMIT_ASSIGNMENT'); ?></span>
                    </div>
                </div>
            </div>
        <?php } ?>

        <!--        Assessment Activity Preview-->
        <?php if ($tema1['type'] == 'quiz') { // ASSESSMENT
            $id_assessment = (int)$tema1['id'];
            $data_assessment = $tema1['data'];
//            $data_assessment = JoomdleHelperContent::call_method('create_quiz_activity', 0, $course_id, $username, array('qid' => $id_assessment));
            $arr_question_id = array_map('intval', explode(',', $data_assessment['questions']));
            $dem_ques = 0;
            for ($i = 0; $i < count($arr_question_id); $i++) {
                if ($arr_question_id[$i] != 0) {
                    $dem_ques++;
                }
            }
            ?>
            <div class="preview_assessment preview_activity" id="preview_<?php echo $id_assessment; ?>">
                <div class="joomdle-view-quiz start_assessment" id="start_assessment<?php echo $id_assessment; ?>">
                    <div class="">
                        <div class="previewTitle"><?php echo $data_assessment['name']; ?></div>
                        <div class="col-xs-12 introduction">
                            <span><?php echo JText::_('COM_JOOMDLE_INSTRUCTIONS'); ?></span>
                            <p><?php echo $data_assessment['des']; ?></p>
                            <div class="btn-start-quiz" id="btn-start-quiz<?php echo $id_assessment; ?>">
                                <center class="btn_start_assessment" id="btn_start_assessment<?php echo $id_assessment; ?>" style="color: #fff;">
                                    <?php echo JText::_('QUIZ_START_ASSESSMENT'); ?>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="start_assessment_after hidden" id="start_assessment_after<?php echo $id_assessment; ?>" slide="1" slide_max="<?php echo $dem_ques; ?>">
                    <?php if ($data_assessment['timelimit'] != 0) { ?>
                        <span class="pull-right time-left"><?php echo JText::_('COM_JOOMDLE_TIME_LEFT'); ?>: 00:<?php echo($data_assessment['timelimit'] / 60); ?>
                            min</span>
                    <?php } ?>
                    <?php
                    $t = 0;
                    for ($i = 0; $i < count($arr_question_id); $i++) {
                        if ($arr_question_id[$i] != 0) {
                            $t++;
                            ?>
                            <div id="<?php echo $id_assessment; ?>" class="joomdle-view-quiz question_preview stt_<?php echo $id_assessment; ?>_<?php echo $t; ?> <?php echo ($t == 1) ? "current" : ''; ?> <?php echo $this->pageclass_sfx; ?>" style="clear:both !important;">
                                <div class="">
                                    <center class="previewTitle"><?php echo $data_assessment['name']; ?></center>
                                    <?php
                                    //                                    $data_question = JoomdleHelperContent::call_method('quiz_get_question', $arr_question_id[$i]);
                                    $data_question = $this->questions[$arr_question_id[$i]];
                                    $options = json_decode($data_question['options']);
                                    if ($data_question['qtype'] == 'match') { ?>
                                        <div class="divPreview match_preview">
                                            <div class="previewQuestions">
                                                <p class="questionTitle"><?php echo JText::_('COM_JOOMDLE_QUESTION') . ' ' . $t . ' of ' . $dem_ques; ?></p>
                                                <p class="questionDescription"><?php echo $data_question['name']; ?></p>
                                                <div class="previewMatchingBlocks">
                                                    <ol type="A" style="list-style-position: inside;">
                                                        <?php
                                                        $statement = 0;
                                                        foreach ($options->subquestions as $k => $v) {
                                                            $statement++;
                                                            ?>
                                                            <li class="statement" style="clear:both;"><?php echo $v->questiontext; ?>
                                                                <div class="answer"><?php echo JText::_('COM_JOOMDLE_ANSWER'); ?></div>

                                                                <div class="divPreviewOptionSelect">
                                                                    <input type="text" readonly="" class="previewOptionInput match_answer" placeholder="<?php echo JText::_('COM_JOOMDLE_OPTION'); ?>">
                                                                    <ul class="ulPreviewOptionSelect">
                                                                        <?php
                                                                        $k = 0;
                                                                        foreach ($options->subquestions as $answertext) { ?>
                                                                            <li class="previewOption"><?php echo $answertext->answertext; ?></li>
                                                                            <?php
                                                                            $k++;
                                                                        } ?>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        <?php } ?>
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                    if ($data_question['qtype'] == 'truefalse') { ?>
                                        <div class="divPreview truefalse_preview">
                                            <div class="previewQuestions">
                                                <p class="questionTitle"><?php echo JText::_('COM_JOOMDLE_QUESTION') . ' ' . $t . ' of ' . $dem_ques; ?></p>
                                                <p class="questionDescription"><?php echo $data_question['name']; ?></p>
                                                <p class="questionAnswer"><?php echo JText::_('COM_JOOMDLE_ANSWER'); ?></p>
                                                <div class="previewTFButtons">
                                                    <div class="previewTrue">
                                                        <div class="statusTrue"></div>
                                                        <span><?php echo Jtext::_('QUESTION_TRUE'); ?></span>
                                                    </div>
                                                    <div class="previewFalse">
                                                        <div class="statusFalse"></div>
                                                        <span><?php echo Jtext::_('QUESTION_FALSE'); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                    if ($data_question['qtype'] == 'multichoice') { ?>
                                        <div class="divPreview multichoice_preview">
                                            <div class="previewQuestions">
                                                <p class="questionTitle"><?php echo JText::_('COM_JOOMDLE_QUESTION') . ' ' . $t . ' of ' . $dem_ques; ?></p>
                                                <p class="questionDescription"><?php echo $data_question['name']; ?></p>
                                                <p class="questionAnswer"><?php echo JText::_('COM_JOOMDLE_ANSWER'); ?></p>
                                                <div class="previewMCOptions">
                                                    <?php foreach ($options->answers as $k => $v) { ?>
                                                        <div class="previewAnswer">
                                                            <div class="previewStatus"></div>
                                                            <span class="previewAnswerText"> <?php echo $v->answer; ?></span>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                    if ($data_question['qtype'] == 'shortanswer') { ?>
                                        <div class="divPreview shortanswer_preview">
                                            <div class="previewQuestions">
                                                <p class="questionTitle"><?php echo JText::_('COM_JOOMDLE_QUESTION') . ' ' . $t . ' of ' . $dem_ques; ?></p>
                                                <p class="questionDescription"><?php echo $data_question['name']; ?></p>
                                                <p class="questionAnswer"><?php echo JText::_('COM_JOOMDLE_ANSWER'); ?></p>
                                                <textarea name="previewTextareaAnswer"
                                                          class="previewTextareaAnswer" cols="30"
                                                          rows="10"
                                                          disabled></textarea>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div></div>
                        <?php
                        }
                    } ?>
                    <div style="clear: both;"></div>
                    <button class="previewSubmit" type="button" data-disabled="1"><?php echo JText::_('COM_JOOMDLE_SUBMIT'); ?></button>

                    <div class="process-point-ass" id="<?php echo $id_assessment; ?>">
                        <ul class="process-ul-ass" id="">
                            <?php for ($j = 1; $j <= $t; $j++) { ?>
                                <li class="process-li<?php echo $j; ?>" id="lipoint">
                                    <div class="statusPoint-ass statusPoint_ass_<?php echo $id_assessment; ?>_<?php echo $j; ?>  <?php if ($j == 1) echo "active"; ?>">
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="button_next_prev" style="clear: both;">
                        <div class="next-question-before" id="next-question-before<?php echo $id_assessment; ?>">
                            <img width="50" height="50" src="<?php echo JURI::base() . 'images/PreviousIcon.png'; ?>">
                        </div>
                        <div class="next-question-after" id="next-question-after<?php echo $id_assessment; ?>">
                            <img width="50" height="50" src="<?php echo JURI::base() . 'images/NextIcon.png'; ?>">
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

        <!--        Scorm Activity Preview-->
        <?php if ($tema1['type'] == 'scorm') {
            $id_scorm = (int)$tema1['id'];
            $name_scorm = $tema1['name'];
            $this->wrapper = new JObject ();
            $this->wrapper->load = '';
            $path = '/mod/scorm/view_mobile.php?id=';
            $typeview = "&type=";
            $this->wrapper->url = $this->params->get('MOODLE_URL') . $path . $id_scorm . $typeview;
            ?>

            <div class="preview_scorm preview_activity" id="preview_<?php echo $id_scorm; ?>">
                <p class="previewScormTitle"><?php echo $name_scorm; ?></p>
                <iframe
                    id="blockrandom"
                    class="autoHeight <?php echo $classes; ?>"
                    data-src="<?php echo $this->wrapper->url; ?>"
                    width="<?php echo $this->params->get('width', '100%'); ?>"
                    scrolling="<?php //echo $this->params->get('scrolling', 'auto'); ?>yes"
                    allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"

                    <?php if (!$this->params->get('autoheight', 1)) { ?>
                        height="<?php echo $this->params->get('height', '500'); ?>"
                    <?php
                    }
                    ?>
                    frameborder="0"
                    <?php
                    //if ($this->params->get('autoheight', 1)) {
                    //echo " onload='itspower(this, false, true, 20)' ";
                    //}
                    ?>
                    ></iframe>
            </div>
        <?php } ?>

        <!--        Feedback Activity Preview-->
        <?php if ($tema1['type'] == 'questionnaire') {
            $id_questionnaire = (int)$tema1['id'];
//            $questionnaire = JoomdleHelperContent::call_method('get_info_questionnaire', (int)$resource['id'], $username);
            $questionnaire = $tema1['data'];
            $questions = $questionnaire['questions'];
            $ques_rate = 0;
            ?>

            <div class="preview_feedback preview_activity" id="preview_<?php echo $id_questionnaire; ?>">
                <div class="question_view">
                    <div class="feedback_tittle">Feedback</div>
                    <div id="group" fb_slide="1">
                        <?php
                        $count = 0;
                        foreach ($questions as $key => $value) {
                            $ratevalues = json_decode($value['params'], true);
                            $ratevalues = !empty($ratevalues) ? $ratevalues : [
                                '1' => JText::_('COM_JOOMDLE_STRONGLY_DISAGREE'),
                                '2' => JText::_('COM_JOOMDLE_DISAGREE'),
                                '3' => JText::_('COM_JOOMDLE_NEUTRAL'),
                                '4' => JText::_('COM_JOOMDLE_AGREE'),
                                '5' => JText::_('COM_JOOMDLE_STRONGLY_AGREE'),
                            ];
                            if ($value['type_id'] == 8 && $value['deleted'] == "n") {
                                $ques_rate++;
                                ?>
                                <div class="countRatingQuestion item <?php if ($ques_rate == 1) {
                                    echo "current";
                                } ?>" id="countRatingQuestion<?php echo $ques_rate; ?>">
                                    <p class="type_des">
                                        Rate the following statements on a scale of 1 - 5 :
                                    </p>
                                    <p id="replaceQuestion"><span class="contentQuestionFreview" id="contentQuestionFreview"><?php echo $value['content']; ?></span></p>
                                    <ul class="scaleRatingQuestion" id="scaleRatingQuestion">
                                        <?php for ($j = 1; $j <= $value['length']; $j++) { ?>
                                            <li class="liScale<?php echo $j; ?>" id="liScaleRatingQuestion"
                                                preid="<?php echo $j; ?>">
                                                <div class="timestamp">
                                                                <span class="countRating<?php echo $j; ?>"><?php echo $j; ?>
                                                                    <span>
                                                                            </span></span></div>
                                                <div class="statusRatingQuestion"
                                                     id="question1rate<?php echo $j; ?>">

                                                    <p><?php echo $ratevalues[$j]; ?></p>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                    <?php if ($count == 0) { ?>
                                        <p class="note_des">
                                            Note: You can only submit course feedback once.
                                        </p>
                                    <?php } ?>
                                </div>
                            <?php }
                            if ($value['type_id'] == 2 && $value['deleted'] == "n") {
                                $ques_rate++;
                                ?>
                                <div class="countRatingQuestion item <?php echo ($ques_rate == 1) ? "current" : ''; ?>" id="countRatingQuestion<?php echo $ques_rate; ?>">
                                    <p class="type_des">
                                        Comment
                                    </p>
                                    <p id="replaceQuestion1"><span class="contentQuestionFreview1" id="contentQuestionFreview1"><?php echo $value['content']; ?></span>
                                    </p>
                                    <textarea name="textareaAnswer" class="textareaAnswer" placeholder="Write feedback to here" required=""></textarea>
                                    <?php if ($count == 0) { ?>
                                        <p class="note_des">
                                            Note: You can only submit course feedback once.
                                        </p>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php $count++;
                        } ?>
                        <div class="clear"></div>
                        <div class="process-point">
                            <ul class="process-ul" id="">
                                <?php for ($t = 1; $t <= $ques_rate; $t++) { ?>
                                    <li class="process-li<?php echo $t; ?>" id="lipoint">
                                        <div class="statusPoint <?php if ($t == 1) echo "active"; ?>"
                                             id="statusPoint<?php echo $t; ?>">
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="prev-question-feedback">
                        <img width="50" height="50"
                             src="<?php echo JURI::base() . 'images/PreviousIcon.png'; ?>">
                    </div>
                    <div class="next-question-feedback">
                        <img width="50" height="50"
                             src="<?php echo JURI::base() . 'images/NextIcon.png'; ?>">
                    </div>
                </div>
            </div>

        <?php } ?>
    <?php } ?>
</div>
<div class="divEditCourseInfo">
    <button class="btUnPreviewCourse hidden">
        <?php echo JText::_('COURSE_BUTTON_EXIT_PREVIEW_COURSE'); ?>
    </button>
    <button class="btUnPreviewActivity hidden">
        <?php echo JText::_('COURSE_BUTTON_EXIT_PREVIEW_ACTIVITY'); ?>
    </button>
</div>

<script type="text/javascript">
    (function ($) {
        /*
        jQuery('.textModule').each(function () {
//        39 is size height icon Delete and icon sort
            this.setAttribute('style', 'height:' + this.scrollHeight + 'px;');
        }).on('input', function () {
            this.style.height = 'auto';
            this.setAttribute('style', 'height:' + this.scrollHeight + 'px;');
        });

        jQuery('.textModule').each(function () {
        }).on('input', function () {
           
            jQuery(this).keyup(function () {
                end = $(this).val();
                first = $(this).html()
                if (first !== end)
                    $('#formUpdateActivity').addClass('haschange');
     
            });
        });
        */
        $('.textModule').each(function () {
            autosize(this);
        }).on('input', function () {
            autosize(this);
            jQuery(this).keyup(function () {
                end = $(this).val();
                first = $(this).html()
                if (first !== end)
                    $('#formUpdateActivity').addClass('haschange');
            });
        });

        <?php
        if ($count_feedback != 0) { // exist activity feedback
        if ($user_role[0] != 'student') { // is not learner
        ?>
        $('.div-feedback').click(function (e) {
            e.preventDefault();
            if ($(this).hasClass('preview')) {

                var activity_id = <?php echo $id_ques; ?>;

                $(".btUnPreviewCourse").addClass('hidden');
                $(".btUnPreviewActivity").removeClass('hidden');

                $("#formUpdateActivity").addClass('hidden');
                $(".manage-preview").removeClass('hidden');
                $(".preview_activity").addClass('hidden');
                $("#preview_" + activity_id).removeClass('hidden');
                if (!($(".feedback_section").hasClass('hidden'))) {
                    $(".feedback_section").addClass('hidden');
                }

                // FEEDBACK PREVIEW
                $(".block-feedback-preview").hide();
                $("#countRatingQuestion1").addClass("current");
                $("#statusPoint1").addClass("active");
                $(".next-question-feedback").show();
                $(".prev-question-feedback").hide();
                $(".joomdle-course").hide();

//                if ($(".feedbackview").hasClass('hidden')) {
//                    $(".feedbackview").removeClass('hidden');
//                    $(".question_view").addClass('hidden');
//                }

//                $('.start-questionnaire').off().on('click', function () {
//                    $(".feedbackview").addClass('hidden');
//                    $(".question_view").removeClass('hidden');
//                });
                var ques_max = <?php echo $ques_rate; ?>;
                $(".next-question-feedback").off().on('click', function () {
                    var attrid = $(this).parents('.preview_feedback.preview_activity').attr('id');
                    var ques = $("#" + attrid + " #group").attr('fb_slide');

                    next_feedback(attrid, ques, ques_max);
                });
                $(".prev-question-feedback").off().on('click', function () {
                    var attrid = $(this).parents('.preview_feedback.preview_activity').attr('id');
                    var ques = $("#" + attrid + " #group").attr('fb_slide');

                    prev_feedback(attrid, ques);
                });

                $('.statusRatingQuestion').click(function () {
                    $('.statusRatingQuestion').removeClass('active');
                    $('.statusRatingQuestion') < $(this).addClass('active');
                });
                // FEEDBACK PREVIEW

            } else {
//                if ($('.edit-activity').hasClass('visible')) {
                window.location.href = '<?php echo JUri::base() . 'feedback/edit_' . $course_id . '_' . $id_ques . '.html'; ?>';
//                } else {
//                    window.location.href = '<?php //echo JUri::base() . 'feedbackview/' . $course_id . '_' . $id_ques . '_review.html'; ?>//';
//                }
            }
        });

        <?php
        }
        }
        ?>

        $('.preview_assignment .date span').each(function() {
            if ($(this).attr('data-duedate') == '-') {
                $(this).html('-');
            } else {
                var data_duedate = $(this).attr('data-duedate') + ' GMT';
                var date = new Date(data_duedate);
                $(this).html(("0" + date.getDate()).slice(-2) + '/' + ("0" + parseInt(date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear());
            }
        });

        $('.certificate_section').click(function (e) {
            window.location.href = '<?php echo JURI::base() . "certificatetemplate/detail_" . $course_id . ".html"; ?>';
        });

        $('.btCancel2').click(function () {
            if ($('form').hasClass('haschange')) {
                lgtCreatePopup('confirm', {
                    content: '<?php echo Jtext::_('COM_JOOMDLE_CANCEL_EDIT_COURSE_OUTLINE_TEXT'); ?>',
                    subcontent: '<?php echo Jtext::_('COM_JOOMDLE_CANCEL_EDIT_COURSE_OUTLINE_TEXT2'); ?>',
                    yesText: '<?php echo Jtext::_('COM_JOOMDLE_BUTTON_PROCEED'); ?>'
                }, function() {
                    window.location.href = "<?php echo JUri::base() . "manage"; ?>";
                });
            } else window.location.href = "<?php echo JUri::base() . "manage"; ?>";
        });

        $('.btSave').click(function () {
            var validate = true;
            var count_section_num = document.getElementsByClassName("joomdle_course_section");
            var arr_sectionid_after_sort = [];
            $('li.joomdle_course_section').each(function (i) {
                arr_sectionid_after_sort.push($(this).attr('sectionid'));
            });
            document.getElementById("arr_section_id").value = arr_sectionid_after_sort;
            var arr_moduleid_in_another_section = [];

            for (var i = 0; i < arr_sectionid_after_sort.length; i++) {
                if (!$('#titleSection' + arr_sectionid_after_sort[i]).val().trim()) {
                    $('#titleSection' + arr_sectionid_after_sort[i]).on('keyup change', function(e) {
                        if ($(this).val().trim()) $(this).parents('.joomdle_section_list_item_title').removeClass('hasError');
                        else $(this).parents('.joomdle_section_list_item_title').addClass('hasError');
                    }).val('').parents('.joomdle_section_list_item_title').addClass('hasError');
                    validate = false;
                }
                if ($("[sectionida=" + arr_sectionid_after_sort[i] + "] li").length == 0) {
                    var b = [];
                    arr_moduleid_in_another_section.push(b);
                } else {
                    var c = [];
                    $("[sectionida=" + arr_sectionid_after_sort[i] + "] li").each(function () {
                        var textareaActivityTitle = $(this).find('#titleModule' + $(this).attr('moduleid'));
                        if (!textareaActivityTitle.val().trim()) {
                            textareaActivityTitle.val('')
                                .on('keyup change', function(e) {
                                    if ($(this).val().trim()) $(this).parents('li.activity-list-item').removeClass('hasError');
                                    else $(this).parents('li.activity-list-item').addClass('hasError');
                                });
                            $(this).addClass('hasError');
                            validate = false;
                        }
                        c.push($(this).attr('moduleid'));
                    });
                    arr_moduleid_in_another_section.push(c);
                }
                document.getElementById(arr_sectionid_after_sort[i]).value = arr_moduleid_in_another_section[i];
            }
            if (validate) {
                lgtCreatePopup('', {'content': 'Loading...'});
                $("#formUpdateActivity").submit();
            }
        });

        $(".activity_normal").off('click').on('click', function () {
            if ($('.activity_normal').hasClass('preview')) {
                // button
                $(".btUnPreviewActivity").removeClass('hidden');
                $(".btUnPreviewCourse").addClass('hidden');
                $(".block-feedback-preview").hide();
                $(".joomdle-course").hide();

                var activity_id = $(this).attr('moduleid');
//                var section_id_new = $(this).parent().attr('sectionida');

                $("#formUpdateActivity").addClass('hidden');
                $(".manage-preview").removeClass('hidden');
                $(".preview_activity").addClass('hidden');
                $("#preview_" + activity_id).removeClass('hidden');

                // Block feedback
                if (!($(".feedback_section").hasClass('hidden'))) {
                    $(".feedback_section").addClass('hidden')
                }

                // ASSESSMENT
                if ($('.start_assessment').hasClass('hidden')) {
                    $('.start_assessment').removeClass('hidden');
                    $('.start_assessment_after').addClass('hidden');
                }

            }
            // ASSESSMENT PREVIEW

            $(".stt_" + activity_id + "_1").addClass("current");
            $(".statusPoint_ass_" + activity_id + "_1").addClass("active");
            $('#btn-start-quiz' + activity_id).off().on('click', function () {
                $("#start_assessment" + activity_id).toggleClass('hidden');
                $("#start_assessment_after" + activity_id).toggleClass('hidden');
            });
            $("#next-question-before" + activity_id).addClass('hidden');
            $("#next-question-after" + activity_id).removeClass('hidden');


            var q_max = $("#start_assessment_after" + activity_id).attr('slide_max');
            $("#next-question-after" + activity_id).off().on('click', function () {
                var q = $("#start_assessment_after" + activity_id).attr('slide');
                next_assessment(activity_id, q, q_max);
            });
            $("#next-question-before" + activity_id).off().on('click', function () {
                var q = $("#start_assessment_after" + activity_id).attr('slide');
                prev_assessment(activity_id, q);
            });

            // ASSESSMENT PREVIEW
        });

        function next_assessment(activity_id, q, q_max) {
            $(".stt_" + activity_id + "_" + q).removeClass('current');
            $(".statusPoint_ass_" + activity_id + "_" + q).removeClass('active');
            q++;
            $(".stt_" + activity_id + "_" + q).addClass('current');
            $(".statusPoint_ass_" + activity_id + "_" + q).addClass('active');
            if (q >= (q_max)) {
                $("#next-question-after" + activity_id).addClass('hidden');
                $("#next-question-before" + activity_id).removeClass('hidden');
            }
            else {
                $("#next-question-after" + activity_id).removeClass('hidden');
                $("#next-question-before" + activity_id).removeClass('hidden');
            }
            $("#start_assessment_after" + activity_id).attr('slide', q);
            return;
        }

        function prev_assessment(activity_id, q) {
            $(".stt_" + activity_id + "_" + q).removeClass('current');
            $(".statusPoint_ass_" + activity_id + "_" + q).removeClass('active');
            q--;
            $(".stt_" + activity_id + "_" + q).addClass('current');
            $(".statusPoint_ass_" + activity_id + "_" + q).addClass('active');
            if (q <= 1) {
                $("#next-question-before" + activity_id).addClass('hidden');
                $("#next-question-after" + activity_id).removeClass('hidden');
            }
            else {
                $("#next-question-before" + activity_id).removeClass('hidden');
                $("#next-question-after" + activity_id).removeClass('hidden');
            }
            $("#start_assessment_after" + activity_id).attr('slide', q);
            return;
        }

        function next_feedback(attrid, ques, ques_max) {
            $("#" + attrid + " #countRatingQuestion" + ques).removeClass("current");
            $("#" + attrid + " #statusPoint" + ques).removeClass("active");
            ques++;
            $("#" + attrid + " #countRatingQuestion" + ques).addClass("current");
            $("#" + attrid + " #statusPoint" + ques).addClass("active");

            // delta is jump index to position new
            if (ques >= (ques_max)) {
                $("#" + attrid + " .next-question-feedback").hide();
                $("#" + attrid + " .prev-question-feedback").show();
            }
            else {
                $("#" + attrid + " .next-question-feedback").show();
                $("#" + attrid + " .prev-question-feedback").show();
            }
            $("#" + attrid + " #group").attr('fb_slide', ques);
            return;
        }

        function prev_feedback(attrid, ques) {
            $("#" + attrid + " #countRatingQuestion" + ques).removeClass("current");
            $("#" + attrid + " #statusPoint" + ques).removeClass("active");
            ques--;
            $("#" + attrid + " #countRatingQuestion" + ques).addClass("current");
            $("#" + attrid + " #statusPoint" + ques).addClass("active");
            if (ques <= 1) {
                $("#" + attrid + " .prev-question-feedback").hide();
                $("#" + attrid + " .next-question-feedback").show();
            }
            else {
                $("#" + attrid + " .prev-question-feedback").show();
                $("#" + attrid + " .next-question-feedback").show();
            }
            $("#" + attrid + " #group").attr('fb_slide', ques);
            return;
        }

        function sortableInit() {
            var elements = document.getElementsByClassName("connectedSortable");
            var names = '';

            for (var i = 0; i < elements.length; i++) {
                if (i < elements.length - 1)
                    names += '#' + elements[i].id + ', ';
                else
                    names += '#' + elements[i].id;
            }

            $("ul.nested_with_switc").sortable({
                scroll: true,
                <?php echo $is_mobile ? 'axis: \'y\',' : '' ?>
                appendTo: '.joomdle-course',
                connectWith: 'joomdle_course_section',
                handle: '.handle',

                placeholder: 'sortable-placeholder-sections',
                revert: 100,
                opacity: 1,
                group: 'no-drop',
                drag: false,
                cancel: ".disable-sort-item",
                forcePlaceholderSize: true,
                /*start: function (event, ui) {
                    $('.create_activity').fadeOut(100, function() {
                        ui.item.height('auto');
                        $(ui.placeholder[0]).height(ui.item.height());
                    });
                },
                stop: function (event, ui) {
                    $('.create_activity').stop(true, false).fadeIn(100);
                },*/
                change: function (event, ui) {
                    $('#formUpdateActivity').addClass('haschange');
                }
            });

            $(names).sortable({
                connectWith: ".connectedSortable",
                appendTo: '.joomdle-course',
                scroll: true,
                <?php echo $is_mobile ? 'axis: \'y\',' : '' ?>
                forcePlaceholderSize: true,
                placeholder: 'sortable-placeholder',
                revert: 100,
                opacity: 1,
                handle: '.handle',
                /*start: function (event, ui) {
                    $('.create_activity').animate({opacity: 0}, 100, function() {
                        $(this).animate({height: 0, margin: '0'}, 100);
                    });
                },
                stop: function (event, ui) {
                    $('.create_activity').stop(true, false).animate({height: '84px', margin: '15px 15px 0 20px'}, 100, function() {
                        $(this).animate({opacity: 1}, 100);
                    });
                },
                */
                change: function (event, ui) {
                    var pos_start = ui.helper.parent().attr('id');
                    var pos_new = ui.placeholder.parent().attr('id');
                    var sectionid_new = ui.placeholder.parent().attr('sectionida');
//                  $('#collapse_topic' + sectionid_new).addClass('full_content');
                    $('#create_activity' + sectionid_new).removeClass('hidden');
                    $('#' + pos_new).children().removeClass('hidden');
                    $('#formUpdateActivity').addClass('haschange');

                }
            });
            $(names).disableSelection();
        }

        $(document).on('click', '.previewOptionInput', function () {
            $(this).toggleClass('clicked');
            $(this).next('.ulPreviewOptionSelect').toggle();
        });
        $(document).on('click', '.ulPreviewOptionSelect .previewOption', function () {
            $(this).parent().prev('.previewOptionInput').removeClass('clicked').val($(this).html());
            $(this).parent().hide();
        });

//        // SCORM
        setTimeout(function () {
            jQuery('#blockrandom').contents().find('input:radio[name="mode"]').filter('[value="browse"]').attr('checked', true);
        }, 5000);
        setTimeout(function () {
            jQuery('#blockrandom').contents().find('#scormviewform').submit();
        }, 8000);
//        // SCORM


        // Click icon create activity
        $('.btUnPreviewActivity').click(function () {
            //button
            $(".btUnPreviewActivity").addClass('hidden');
            $(".btUnPreviewCourse").removeClass('hidden');
            $("#formUpdateActivity").removeClass('hidden');
            $(".block-feedback-preview").show();
            $(".joomdle-course").show();

            if ($(".preview_activity").not('hidden')) {
                $(".preview_activity").addClass('hidden');
            }

            if ($(".manage-preview").not('hidden')) {
                $(".manage-preview").addClass('hidden');
            }

            // ASSESSMENT
            $(".start_assessment_after").attr('slide', 1);
            if ($(".question_preview").hasClass('current')) {
                $(".question_preview").removeClass('current');
            }

            if ($(".statusPoint-ass").hasClass('active')) {
                $(".statusPoint-ass").removeClass('active');
            }

            //FEEDBACK
            $("#group").attr('fb_slide', 1);
            if ($(".countRatingQuestion").hasClass('current')) {
                $(".countRatingQuestion").removeClass('current');
            }

            if ($(".statusPoint").hasClass('active')) {
                $(".statusPoint").removeClass('active');
            }

            if ($(".feedback_section").hasClass('hidden')) {
                $(".feedback_section").removeClass('hidden');
            }
        });

        $('.btPreviewCourse').click(function () {
            // update title section preview

            var arr_sectionid = [];
            $('li.joomdle_course_section').each(function () {
                arr_sectionid.push($(this).attr('sectionid'));
            });
            for (var t = 0; t < arr_sectionid.length; t++) {
                $('#pTitleSection' + arr_sectionid[t]).text($("input[type=text][name=titleSection" + arr_sectionid[t] + "]").val());
                $("#titleSection" + arr_sectionid[t]).addClass('hidden');
//                if ($('.ui-sortable[sectionida="' + arr_sectionid[t] + '"]').children().hasClass('hidden') == true) {
//                    $('.ui-sortable[sectionida="' + arr_sectionid[t] + '"]').children().removeClass('hidden');
//                }
            }
            $(".pTitleSection").removeClass('hidden');

            // update title activity preview
            var arr_moduleid = [];

            $('li.activity-list-item').each(function () {
                arr_moduleid.push($(this).attr('moduleid'));
            });

            for (var j = 0; j < arr_moduleid.length; j++) {
                var temp = [];
                temp[j] = $('#activity-link' + arr_moduleid[j]).text($("textarea[name=titleModule" + arr_moduleid[j] + "]").val());
                temp[j].html(temp[j].html().replace(/\n/g, '<br/>'));
                $('#preview_' + arr_moduleid[j] + ' .previewTitle').text($("textarea[name=titleModule" + arr_moduleid[j] + "]").val());
                $("#titleModule" + arr_moduleid[j]).addClass('hidden');
                $('#activity-list-item' + arr_moduleid[j]).height($("textarea[name=titleModule" + arr_moduleid[j] + "]").height() + 18);
            }
            $(".activity-link").removeClass('hidden');
            $(".course-content-detail").show();

            $(".create_activity").addClass('hidden');
            $(".divToggle").addClass('hidden');
            $(".for-admins-buttons").addClass('hidden');
            $(".button_control").addClass('hidden');
//            $(".collapse_topic").addClass('hidden');

            // button
            $(".activity-delete ").addClass('hidden');
            $(".activity-sort ").addClass('hidden');
            $(".btUnPreviewCourse").removeClass('hidden');
            $(".btPreviewCourse").addClass('hidden');
            $(".btEditCourseInfo").addClass('hidden');

            // margin_padding_cursor
            $(".nested_with_switc").addClass('preview');
            $(".joomdle_course_section").addClass('preview');
            $(".joomdle-course").addClass('preview');

            // Certificate hidden
            $(".certificate_section").hide();
            $(".div-feedback").addClass('preview');

            //li link activity
            $(".activity_normal a").addClass("disable");

            //activity, feedback to mode preview
            $(".activity_normal").addClass("preview");

            // Progress bar and check done activity
            $(".activity_normal").append("<div class='status'></div>");
            $(".header-title").before("<div class='joomdle_course_progress_bar'><div class='course_progress_bar'>0%</div></div>");
            $(".div-feedback").append("<div class='block-feedback-preview'><p class='pTitleFeedback'>Course Feedback</p></div>");
            $(".feedback_section").hide();


            $(".hvn_BtEditCourse").hide();
//            $(".joomdle_course_outline").before(" <div class='joomdle_course_progress_bar'><div class='course_progress_bar'>0%</div></div>");
            $(".content-title").before("<div class='content-img' style='background-image: url(\"<?php echo $this->course_info['filepath'] . $this->course_info['filename'] . '?' . (time() * 1000); ?>\");'></div>");

            if ($('.sumContent')[0].scrollHeight < 90) {
                $('.content-summary .expansionWrapperSummary').hide();
            } else {
                $('.content-summary .expansionWrapperSummary').show();
            }
            $('.content-summary .expansionWrapperSummary .expandBtnSummary').on('click', function () {
                $('.sumContent').toggleClass('limitSum');
            });
            var height_form = $('#formUpdateActivity').height();
            var height_button_unpreview = height_form + 300;
            $('.btUnPreviewCourse').css('top', 'auto');

            // Cut string description, learning outcome, targetAudience
            if ($(".sumContent .card-description").height() > 40) {
                $(".sumContent .more").html("show more");
                $(".sumContent .card-description").addClass('less1');
            }
            $(".sumContent .more").on("click", function () {
                if ($(".sumContent .card-description").hasClass('less1')) {
                    $(".sumContent .card-description").addClass('more1');
                    $(".sumContent .card-description").removeClass('less1');
                    $(".sumContent .less").html("show less");
                    $(".sumContent .more").html("");
                }
            });
            $(".sumContent .less").on("click", function () {
                if ($(".sumContent .card-description").hasClass('more1')) {
                    $(".sumContent .card-description").addClass('less1');
                    $(".sumContent .card-description").removeClass('more1');
                    $(".sumContent .more").html("show more");
                    $(".sumContent .less").html("");
                }
            });

            if ($(".outcomeContent .card-description").height() > 40) {
                $(".outcomeContent .more").html("show more");
                $(".outcomeContent .card-description").addClass('less1');
            }
            $(".outcomeContent .more").on("click", function () {
                if ($(".outcomeContent .card-description").hasClass('less1')) {
                    $(".outcomeContent .card-description").addClass('more1');
                    $(".outcomeContent .card-description").removeClass('less1');
                    $(".outcomeContent .less").html("show less");
                    $(".outcomeContent .more").html("");
                }
            });
            $(".outcomeContent .less").on("click", function () {
                if ($(".outcomeContent .card-description").hasClass('more1')) {
                    $(".outcomeContent .card-description").addClass('less1');
                    $(".outcomeContent .card-description").removeClass('more1');
                    $(".outcomeContent .more").html("show more");
                    $(".outcomeContent .less").html("");
                }
            });


            if ($(".targetAudienceContent .card-description").height() > 40) {
                $(".targetAudienceContent .more").html("show more");
                $(".targetAudienceContent .card-description").addClass('less1');
            }
            $(".targetAudienceContent .more").on("click", function () {
                if ($(".targetAudienceContent .card-description").hasClass('less1')) {
                    $(".targetAudienceContent .card-description").addClass('more1');
                    $(".targetAudienceContent .card-description").removeClass('less1');
                    $(".targetAudienceContent .less").html("show less");
                    $(".targetAudienceContent .more").html("");
                }
            });
            $(".targetAudienceContent .less").on("click", function () {
                if ($(".targetAudienceContent .card-description").hasClass('more1')) {
                    $(".targetAudienceContent .card-description").addClass('less1');
                    $(".targetAudienceContent .card-description").removeClass('more1');
                    $(".targetAudienceContent .more").html("show more");
                    $(".targetAudienceContent .less").html("");
                }
            });

            $('#blockrandom').attr('src', $('#blockrandom').attr('data-src'));
        });

        $('.btUnPreviewCourse').click(function () {
            // update title section preview
            var arr_sectionid = [];
            $('li.joomdle_course_section').each(function () {
                arr_sectionid.push($(this).attr('sectionid'));
            });

            $(".hvn_BtEditCourse").show();
            $(".joomdle_course_progress_bar").remove();
            $(".block-feedback-preview").remove();

            for (var t = 0; t < arr_sectionid.length; t++) {
                $("#titleSection" + arr_sectionid[t]).removeClass('hidden');
//                if ($('#collapse_topic' + arr_sectionid[t]).hasClass('full_content') == true) {
//                    $('#collapse_topic' + arr_sectionid[t]).removeClass('full_content');
//
//                }
            }
            $(".pTitleSection").addClass('hidden');

            // update title activity preview
            var arr_moduleid = [];

            $('li.activity-list-item').each(function () {
                arr_moduleid.push($(this).attr('moduleid'));
            });

            for (var j = 0; j < arr_moduleid.length; j++) {
                $("#titleModule" + arr_moduleid[j]).removeClass('hidden');
                $('#activity-list-item' + arr_moduleid[j]).removeAttr('style');
            }
            $(".activity-link").addClass('hidden');
            $(".create_activity").removeClass('hidden');
            $(".divToggle").removeClass('hidden');
            $(".for-admins-buttons").removeClass('hidden');
            $(".button_control").removeClass('hidden');
            $(".course-content-detail").hide();

            // button
            $(".btPreviewCourse").removeClass('hidden');
            $(".btUnPreviewCourse").addClass('hidden');
            $(".btEditCourseInfo").removeClass('hidden');
            $(".activity-delete ").removeClass('hidden');
            $(".activity-sort ").removeClass('hidden');
//            $(".collapse_topic").removeClass('hidden');

            // margin_padding_cursor
            $(".nested_with_switc").removeClass('preview');
            $(".joomdle_course_section").removeClass('preview');
            $(".joomdle-course").removeClass('preview');


            // certificate visible
            $(".certificate_section").show();
            $(".div-feedback").removeClass("preview");
            $(".feedback_section").show();

            // link activity
            $("a").removeClass("disable");

            // remove Progress bar and check done
            $(".status").remove();
//            $(".joomdle_course_progress_bar").remove();
            $(".content-img").remove();

            $(".preview_activity").removeClass('hidden'); // ????

            //activity, feedback to normal
            $(".activity_normal").removeClass("preview");

        });

//        $('.collapse_topic').click(function () {
//            var sectionid = $(this).attr('sectionid');
//            if ($("#create_activity" + sectionid).hasClass('hidden') == true) {
//                $("#create_activity" + sectionid).removeClass('hidden');
//                $('.ui-sortable[sectionida="' + sectionid + '"]').children().removeClass('hidden');
////                $("#joomdle_item_content"+sectionid).removeClass('hidden');
//                $("#collapse_topic" + sectionid).addClass('full_content');
//
//            } else {
//                $("#create_activity" + sectionid).addClass('hidden');
////                $("#joomdle_item_content"+sectionid).addClass('hidden');
//                $('.ui-sortable[sectionida="' + sectionid + '"]').children().addClass('hidden');
//                $("#collapse_topic" + sectionid).removeClass('full_content');
//            }
//        });

        $(".activity-sort").removeClass('hidden');
        $(".joomdle_item_title").css({"display": "inline"});
        $(".joomdle_section_list_item_title").removeClass('creator');
        jQuery('#title-mainnav').html('Edit Course Outline');
        //var t = <?php //echo count($this->mods);?>;

        sortableInit();

        // text box title section
        $(".pTitleSection").addClass('hidden');
        $("[id^='titleSection']").removeClass('hidden');

        // text box title activity
        $(".activity-link").addClass('hidden activity-link-edit');
        $("[id^='titleModule']").removeClass('hidden');
        // Edit title activity

        var moduleIdDelete = [];
        $('.optionCheckModule').click(function () {
            var a = $(this).attr('qmid');
            $("#activity-list-item" + a).fadeOut(200, function() {
                $(this).remove();
                moduleIdDelete.push(a);
                document.getElementById("moduleIdNumHidden").value = moduleIdDelete;
                $('#formUpdateActivity').addClass('haschange');
            });
        });

        function ucwords(str) {
            return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
                return $1.toUpperCase();
            });
        }

        // Ajax delete topic
        var sectionIdDelete = [];
        $(document).on('click', '.optionCheckSection', function () {
            var a = $(this).attr('qsid');
            var b = $(this).attr('qcount');

//            var numrelated = $('#sortable' + b + ' > li:visible').length;
            var numrelated = $('#sortable' + b + ' > li').length;
            $('.num-relatedelements').html(numrelated);
            if (b == 0) {
                lgtCreatePopup('oneButton', {content: "This is the default topic from your course, you cannot delete it."}, function () {
                    lgtRemovePopup();
                });
//                alert("This is the default topic from your course, you cannot delete it.");
                /*var popup_can_not_delete_topic_default = '<div class="modal-content hienPopup">' +
                    '<p>This is the default topic from your course, you cannot delete it.</p>' +
                    '<button class="closetitle"><?php echo JText::_('COM_JOOMDLE_CLOSE'); ?></button>' +
                    '</div>';


                $('.modal_course_outline').append(popup_can_not_delete_topic_default);
                var modal = document.getElementById('modal_course_outline');
                modal.style.display = "block";

                $('.closetitle').click(function () {
                    modal.style.display = "none";
                });
                window.onclick = function (event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                    }
                }
                */
            }
            else if (numrelated != 0 && b != 0) {
                lgtCreatePopup('oneButton', {content: "The topic cannot be deleted because there are activities inside."}, function () {
                    lgtRemovePopup();
                });
//                alert("The topic cannot be deleted because there are activities inside.");
                /*var modal = document.getElementById('modal_course_outline');
                if ($(".modal_course_outline .modal-content").length > 0) {
                    modal.style.display = "block";

                    $('.closetitle').click(function () {
                        modal.style.display = "none";
                    });
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                } else {
                    var popup_can_not_delete_topic_exist_activity = '<div class="modal-content hienPopup">' +
                        '<p>The topic cannot be deleted because there are activities inside.</p>' +
                        '<button class="closetitle"><?php echo JText::_('COM_JOOMDLE_CLOSE'); ?></button>' +
                        '</div>';

                    $('.modal_course_outline').append(popup_can_not_delete_topic_exist_activity);
                    modal.style.display = "block";

                    $('.closetitle').click(function () {
                        modal.style.display = "none";
                    });
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                }*/
            } else {
                $('#formUpdateActivity').addClass('haschange');
                $('#' + a).remove();
                sectionIdDelete.push(a);
                document.getElementById("sectionIdNumDelete").value = sectionIdDelete;

                $("#joomdle_course_section" + a).fadeOut(200, function() {
                    $(this).remove();
                });

                sortableInit();
            }
        });

        var addtopic = false;
        // Ajax create topic
        $('.btAddAnActivityText').click(function () {
            var topicTitle = $('#topic_title').val().trim();
            if (!topicTitle) {
                $('#topic_title').val('');
                lgtCreatePopup('oneButton', {content: "Add topic title first."}, function () {
                    lgtRemovePopup();
                });
                /*
                $('#topic_title').on('keyup change', function(e) {
                    if ($(this).val().trim()) $(this).parents('.form-group').removeClass('hasError');
                    else $(this).parents('.form-group').addClass('hasError');
                }).parents('.form-group').addClass('hasError');
                */
                /*var modal1 = document.getElementById('modal_create_topic');
                if ($(".modal_create_topic .modal-content").length > 0) {
                    modal1.style.display = "block";

                    $('.closetitle').click(function () {
                        modal1.style.display = "none";
                    });
                    window.onclick = function (event) {
                        if (event.target == modal1) {
                            modal1.style.display = "none";
                        }
                    }
                } else {
                    var popup_can_not_create_topic = '<div class="modal-content hienPopup">' +
                        '<p>Add topic title first.</p>' +
                        '<button class="closetitle"><?php echo JText::_('COM_JOOMDLE_CLOSE'); ?></button>' +
                        '</div>';

                    $('.modal_create_topic').append(popup_can_not_create_topic);
                    modal1.style.display = "block";

                    $('.closetitle').click(function () {
                        modal1.style.display = "none";
                    });
                    window.onclick = function (event) {
                        if (event.target == modal1) {
                            modal1.style.display = "none";
                        }
                    }
                }*/
            } else if (addtopic == false) {
                $.ajax({
                    url: '<?php echo JUri::base() . 'topics/create_' . $course_id . '.html'; ?>',
                    type: 'post',
                    data: {
                        'createSection': 1,
                        'title': $('#topic_title').val(),
                        'des': 'Topic Title Here',
                        'lo': 'Topic Title Here'
                    },
                    beforeSend: function () {
                        addtopic = true;
                        lgtCreatePopup('', {'content': 'Loading...'});
                    },
                    success: function (result) {
                        $('body').removeClass('overlay2');
                        $('.notification').fadeOut();
                        lgtRemovePopup();
                        var res = JSON.parse(result);

                        function ucwords(str) {
                            return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
                                return $1.toUpperCase();
                            });
                        }

                        if (res.status) {
                            var s = res.datas.sections[0];
                            var html;
                            html = '<li class="joomdle_course_section" section="' + s.section + '" sectionid="' + s.id + '" id="joomdle_course_section' + s.id + '">' +
                            '<div class="activity-delete ">' +
                            '<img class="optionCheckSection"' +
                            'id="optionCheckSection' + s.id + '"' +
                            'qcount="' + s.section + '" qsid="' + s.id + '"' +
                            'align="center" src="<?php echo JUri::base();?>images/view_course/delete1_hd.png">' +
                            '</div>' +
                            '<div class="joomdle_item_title joomdle_section_list_item_title" style="display: inline;">' +
                            '<span class="pTitleSection creator hidden" id="pTitleSection' + s.id + '">' + ucwords(s.name) + '</span>' +
                            '<input type="text" id="titleSection' + s.id + '"' +
                            'class="" name="titleSection' + s.id + '" style="font-family: \'Open Sans\', OpenSans-Semibold, sans-serif;font-weight: 600;"' +
                            'value="' + ucwords(s.name) + '"/>' +
                            '</div>' +
                            '<div class="activity-sort activity-sort-section"> <img align="center" class="handle" src="images/rearrange_new.png">' +
                            '</div>' +
                                //                                '<span class="  collapse_topic" sectionid="' + s.id + '" id="collapse_topic' + s.id + '">' +
                                //                                '</span>' +
                            '<div class="joomdle_item_content joomdle_section_list_item_resources" id="joomdle_item_content' + s.id + '">' +
                            '<ul id="sortable' + s.section + '" class="connectedSortable ui-sortable" sectionida="' + s.id + '" style="min-height: 10px;">' +
                            '</ul>' +
                            '</div>' +

                            '<div class="create_activity row" id="create_activity' + s.id + '">' +
                            '<div class="create_page col-md-3 col-sm-3 col-xs-6" >' +
                            '<a href="<?php echo JUri::base();?>content/create_<?php echo $course_id?>_' + s.id + '.html">' +
                            '<img class="iconCreateActivity" id="iconCreatePage"  align="center" src="<?php echo JUri::base();?>images/view_course/addpage_hd.png">' +
                            '<div class="create-activity-name-box">' +
                            '<p class="activity-name">Activity</p>' +
                            '</div>' +
                            '</a>' +
                            '</div>' +

                            '<div class="create_page col-md-3 col-sm-3 col-xs-6" sectionid="' + s.id + '">' +
                            '<a href="<?php echo JUri::base();?>scorm/create_<?php echo $course_id?>_' + s.id + '.html">' +
                            '<img class="iconCreateActivity" id="iconCreatePage"  align="center" src="<?php echo JUri::base();?>images/view_course/addscorm_hd.png">' +
                            '<div class="create-activity-name-box">' +
                            '<p class="activity-name">SCORM</p>' +
                            '</div>' +
                            '</a>' +                            
                            '</div>' +

                            '<div class="create_page col-md-3 col-sm-3 col-xs-6" sectionid="' + s.id + '">' +
                            '<a href="<?php echo JUri::base();?>assessment/create_<?php echo $course_id?>_' + s.id + '.html">' +
                            '<img class="iconCreateActivity" id="iconCreatePage"  align="center" src="<?php echo JUri::base();?>images/view_course/addassessment_hd.png">' +
                            '<div class="create-activity-name-box">' +
                            '<p class="activity-name">Assessment</p>' +
                            '</div>' +
                            '</a>' +                            
                            '</div>' +

                            '<div class="create_page col-md-3 col-sm-3 col-xs-6" sectionid="' + s.id + '">' +
                            '<a href="<?php echo JUri::base();?>assignment/create_<?php echo $course_id?>_' + s.id + '.html">' +
                            '<img class="iconCreateActivity" id="iconCreatePage"  align="center" src="<?php echo JUri::base();?>images/view_course/addassignment_hd.png">' +
                            '<div class="create-activity-name-box">' +
                            '<p class="activity-name">Assignment</p>' +
                            '</div>' +
                            '</a>' +                            
                            '</div>' +

                            '</div>' +
                            '<div class="divToggle"></div>' +
                            '</li>';
                            $('.nested_with_switc').append(html);
                            $('#moduleIdNumHidden').after('<input type="hidden" name="' + s.id + '" id="' + s.id + '">');

                            var cs = parseInt($('#countSection').val());
                            $('#countSection').val(cs + 1);
                            if ($('.edit-activity').hasClass('visible')) $('.edit-activity').click();

                            sortableInit();

                            $("#topic_title").val("");
//                            $('#collapse_topic' + s.id).click(function () {
//                                var sectionid = s.id;
//                                if ($("#create_activity" + sectionid).hasClass('hidden') == true) {
//                                    $("#create_activity" + sectionid).removeClass('hidden');
//                                    $('.ui-sortable[sectionida="' + sectionid + '"]').children().removeClass('hidden');
//                                    //                $("#joomdle_item_content"+sectionid).removeClass('hidden');
//                                    $("#collapse_topic" + sectionid).addClass('full_content');
//
//                                } else {
//                                    $("#create_activity" + sectionid).addClass('hidden');
//                                    //                $("#joomdle_item_content"+sectionid).addClass('hidden');
//                                    $('.ui-sortable[sectionida="' + sectionid + '"]').children().addClass('hidden');
//                                    $("#collapse_topic" + sectionid).removeClass('full_content');
//                                }
//                            });
                            /*$(document).on('keyup', "input[name*='titleSection']", function(e1) {
                                console.log(123);
                                if (e1.which == 13) {
                                    var titleSection = $(this).val().trim();
                                    var section_id_rename = $(this).parent().parent().attr('sectionid');
                                    var name_new = $(this).val();

                                    e1.preventDefault();

                                    if (!titleSection) {
                                        $(this).on('keyup change', function(e) {
                                            if ($(this).val().trim()) $(this).parents('.joomdle_section_list_item_title').removeClass('hasError');
                                            else $(this).parents('.joomdle_section_list_item_title').addClass('hasError');
                                        }).parents('.joomdle_section_list_item_title').addClass('hasError');
                                    } else {
                                    $.ajax({
                                        url: '<?php echo JUri::base() . "topics/edit_" . $course_id; ?>_' + section_id_rename + ".html",
                                        type: 'post',
                                        data: {
                                            'createSection': 2,
                                            'title': $(this).val(),
                                            'des': 'Topic Title Here',
                                            'lo': 'Topic Title Here'
                                        },
                                        beforeSend: function () {
                                            lgtCreatePopup('', {'content': 'Loading...'});

                                            $('#titleSection' + section_id_rename).blur();
                                        },
                                        success: function (result) {
                                            lgtRemovePopup();

                                            function ucwords(str) {
                                                return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
                                                    return $1.toUpperCase();
                                                });
                                            }

                                            $('#titleSection' + section_id_rename).val(ucwords(name_new));
                                        }
                                    });
                                    }
                                }
                            });*/
                            addtopic = false;
                        } else {
                            addtopic = false;
                            alert('Error');
                        }
                    }
                });
            }
        });

        $("input[name='topic_title']").keypress(function (e) {
            if (e.which == 13) {
            var topicTitle = $(this).val().trim();
            $(this).blur();
            if (topicTitle && addtopic == false) {
                e.preventDefault();
                $.ajax({
                    url: '<?php echo JUri::base() . 'topics/create_' . $course_id . '.html'; ?>',
                    type: 'post',
                    data: {
                        'createSection': 1,
                        'title': topicTitle,
                        'des': 'Topic Title Here',
                        'lo': 'Topic Title Here'
                    },
                    beforeSend: function () {
                        addtopic = true;
                        lgtCreatePopup('', {'content': 'Loading...'});
                    },
                    success: function (result) {
                        lgtRemovePopup();

                        var res = JSON.parse(result);

                        function ucwords(str) {
                            return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
                                return $1.toUpperCase();
                            });
                        }

                        if (res.status) {
                            var s = res.datas.sections[0];
                            var html;
                            html = '<li class="joomdle_course_section" section="' + s.section + '" sectionid="' + s.id + '" id="joomdle_course_section' + s.id + '">' +
                            '<div class="activity-delete ">' +
                            '<img class="optionCheckSection"' +
                            'id="optionCheckSection' + s.id + '"' +
                            'qcount="' + s.section + '" qsid="' + s.id + '"' +
                            'align="center" src="<?php echo JUri::base();?>images/view_course/delete1_hd.png">' +
                            '</div>' +
                            '<div class="joomdle_item_title joomdle_section_list_item_title" style="display: inline;">' +
                            '<span class="pTitleSection creator hidden" id="pTitleSection' + s.id + '">' + ucwords(s.name) + '</span>' +
                            '<input type="text" id="titleSection' + s.id + '"' +
                            'class="" name="titleSection' + s.id + '" style="font-family: \'Open Sans\', OpenSans-Semibold, sans-serif;font-weight: 600;"' +
                            'value="' + ucwords(s.name) + '"/>' +
                            '</div>' +
                            '<div class="activity-sort activity-sort-section"> <img align="center" class="handle" src="images/rearrange_new.png">' +
                            '</div>' +
                                //                                '<span class="collapse_topic" sectionid="' + s.id + '" id="collapse_topic' + s.id + '">' +
                                //                                '</span>' +
                            '<div class="joomdle_item_content joomdle_section_list_item_resources" id="joomdle_item_content' + s.id + '">' +
                            '<ul id="sortable' + s.section + '" class="connectedSortable ui-sortable" sectionida="' + s.id + '" style="min-height: 10px; ">' +
                            '</ul>' +
                            '</div>' +

                            '<div class="create_activity row" id="create_activity' + s.id + '">' +
                            '<div class="create_page col-md-3 col-sm-3 col-xs-6" >' +
                            '<a href="<?php echo JUri::base();?>content/create_<?php echo $course_id?>_' + s.id + '.html">' +
                            '<img class="iconCreateActivity" id="iconCreatePage"  align="center" src="<?php echo JUri::base();?>images/view_course/addpage_hd.png">' +
                            '<div class="create-activity-name-box">' +
                            '<p class="activity-name">Activity</p>' +
                            '</div>' +
                            '</a>' +                            
                            '</div>' +

                            '<div class="create_page col-md-3 col-sm-3 col-xs-6" sectionid="' + s.id + '">' +
                            '<a href="<?php echo JUri::base();?>scorm/create_<?php echo $course_id?>_' + s.id + '.html">' +
                            '<img class="iconCreateActivity" id="iconCreatePage"  align="center" src="<?php echo JUri::base();?>images/view_course/addscorm_hd.png">' +
                            '<div class="create-activity-name-box">' +
                            '<p class="activity-name">SCORM</p>' +
                            '</div>' +
                            '</a>' +                            
                            '</div>' +

                            '<div class="create_page col-md-3 col-sm-3 col-xs-6" sectionid="' + s.id + '">' +
                            '<a href="<?php echo JUri::base();?>assessment/create_<?php echo $course_id?>_' + s.id + '.html">' +
                            '<img class="iconCreateActivity" id="iconCreatePage"  align="center" src="<?php echo JUri::base();?>images/view_course/addassessment_hd.png">' +
                            '<div class="create-activity-name-box">' +
                            '<p class="activity-name">Assessment</p>' +
                            '</div>' +
                            '</a>' +                            
                            '</div>' +

                            '<div class="create_page col-md-3 col-sm-3 col-xs-6" sectionid="' + s.id + '">' +
                            '<a href="<?php echo JUri::base();?>assignment/create_<?php echo $course_id?>_' + s.id + '.html">' +
                            '<img class="iconCreateActivity" id="iconCreatePage"  align="center" src="<?php echo JUri::base();?>images/view_course/addassignment_hd.png">' +
                            '<div class="create-activity-name-box">' +
                            '<p class="activity-name">Assignment</p>' +
                            '</div>' +
                            '</a>' +                            
                            '</div>' +

                            '</div>' +
                            '<div class="divToggle"></div>' +
                            '</li>';
                            $('.nested_with_switc').append(html);
                            $('#moduleIdNumHidden').after('<input type="hidden" name="' + s.id + '" id="' + s.id + '">');

                            var cs = parseInt($('#countSection').val());
                            $('#countSection').val(cs + 1);
                            if ($('.edit-activity').hasClass('visible')) $('.edit-activity').click();

                            sortableInit();

                            $("#topic_title").val('').blur();
//                            $('#collapse_topic' + s.id).click(function () {
//                                var sectionid = s.id;
//                                if ($("#create_activity" + sectionid).hasClass('hidden') == true) {
//                                    $("#create_activity" + sectionid).removeClass('hidden');
//                                    $('.ui-sortable[sectionida="' + sectionid + '"]').children().removeClass('hidden');
//                                    //                $("#joomdle_item_content"+sectionid).removeClass('hidden');
//                                    $("#collapse_topic" + sectionid).addClass('full_content');
//
//                                } else {
//                                    $("#create_activity" + sectionid).addClass('hidden');
//                                    //                $("#joomdle_item_content"+sectionid).addClass('hidden');
//                                    $('.ui-sortable[sectionida="' + sectionid + '"]').children().addClass('hidden');
//                                    $("#collapse_topic" + sectionid).removeClass('full_content');
//                                }
//                            });
                            /*$(document).on('keyup', "input[name*='titleSection']", function(e1) {
                                console.log(456);
//                              $("input[name*='titleSection']").keypress(function (e1) {
                                if (e1.which == 13) {
                                    var section_id_rename = $(this).parent().parent().attr('sectionid');
                                    var name_new = $(this).val();

                                    e1.preventDefault();
                                    $.ajax({
                                        url: '<?php echo JUri::base() . "topics/edit_" . $course_id; ?>_' + section_id_rename + ".html",
                                        type: 'post',
                                        data: {
                                            'createSection': 2,
                                            'title': $(this).val(),
                                            'des': 'Topic Title Here',
                                            'lo': 'Topic Title Here'
                                        },
                                        beforeSend: function () {
                                            lgtCreatePopup('', {'content': 'Loading...'});
                                            $('#titleSection' + section_id_rename).blur();
                                        },
                                        success: function (result) {
                                            lgtRemovePopup();

                                            function ucwords(str) {
                                                return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
                                                    return $1.toUpperCase();
                                                });
                                            }

                                            $('#titleSection' + section_id_rename).val(ucwords(name_new));
                                        }
                                    });
                                }
                            });*/
                            addtopic = false;
                        } else {
                            addtopic = false;
                            alert('Error');
                        }
                    }
                });
            }
            if (!topicTitle) {
                $(this).val('');
                lgtCreatePopup('oneButton', {content: "Add topic title first."}, function () {
                    lgtRemovePopup();
                });
                /*
                $(this).on('keyup change', function(e) {
                    if ($(this).val().trim()) $(this).parents('.form-group').removeClass('hasError');
                }).parents('.form-group').addClass('hasError');
                */
                /*var modal1 = document.getElementById('modal_create_topic');
                if ($(".modal_create_topic .modal-content").length > 0) {
                    modal1.style.display = "block";

                    $('.closetitle').click(function () {
                        modal1.style.display = "none";
                    });
                    window.onclick = function (event) {
                        if (event.target == modal1) {
                            modal1.style.display = "none";
                        }
                    }
                } else {
                    var popup_can_not_create_topic = '<div class="modal-content hienPopup">' +
                        '<p>Add topic title first.</p>' +
                        '<button class="closetitle"><?php echo JText::_('COM_JOOMDLE_CLOSE'); ?></button>' +
                        '</div>';

                    $('.modal_create_topic').append(popup_can_not_create_topic);
                    modal1.style.display = "block";

                    $('.closetitle').click(function () {
                        modal1.style.display = "none";
                    });
                    window.onclick = function (event) {
                        if (event.target == modal1) {
                            modal1.style.display = "none";
                        }
                    }
                }*/
            }}
        });

        $(document).on('keyup', "input[name*='titleSection']", function(e) {
//          $("input[name*='titleSection']").keypress(function (e) {
            if (e.which == 13) {
            var titleSection = $(this).val().trim();

            if (!titleSection) {
                $(this).on('keyup change', function(e) {
                    if ($(this).val().trim()) $(this).parents('.joomdle_section_list_item_title').removeClass('hasError');
                    else $(this).parents('.joomdle_section_list_item_title').addClass('hasError');
                }).parents('.joomdle_section_list_item_title').addClass('hasError');
                $(this).val('').blur();
                return false;
            } else {
            $('#formUpdateActivity').addClass('haschange');

                var section_id_rename = $(this).parent().parent().attr('sectionid');
                var name_new = $(this).val();
                e.preventDefault();
                $.ajax({
                    url: '<?php echo JUri::base() . "topics/edit_" . $course_id; ?>_' + section_id_rename + ".html",
                    type: 'post',
                    data: {
                        'createSection': 2,
                        'title': $(this).val(),
                        'des': 'Topic Title Here',
                        'lo': 'Topic Title Here'
                    },
                    beforeSend: function () {
                        lgtCreatePopup('', {'content': 'Loading...'});
                        $('#titleSection' + section_id_rename).blur();
                    },
                    success: function (result) {
                        lgtRemovePopup();

                        function ucwords(str) {
                            return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
                                return $1.toUpperCase();
                            });
                        }

                        $('#titleSection' + section_id_rename).val(ucwords(name_new));
                    }
                });
            }
            }
        });

        if (<?php echo isset($check_course_creator) && $check_course_creator ? 1 : 0; ?>) {
            $('.com_joomdle.view-course').toggleClass('manager-course');
        }
    })(jQuery);

</script>