<?php defined('_JEXEC') or die('Restricted access'); ?>
<script src="components/com_joomdle/js/jquery-1.11.1.js"></script>
<script src="components/com_joomdle/js/jquery-ui.min.js"></script>
<script src="components/com_joomdle/js/jquery.ui.touch-punch.min.js"></script>
<?php
$itemid = JoomdleHelperContent::getMenuItem();
$linkstarget = $this->params->get('linkstarget');
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";
JFactory::getDocument()->setTitle(JText::_('COM_JOOMDLE_COURSE_OUTLINE'));
$jump_url = JoomdleHelperContent::getJumpURL();
$user = JFactory::getUser();
$username = $user->username;
$session = JFactory::getSession();
$token = md5($session->getId());
$course_id = $this->course_info['remoteid'];
$direct_link = 1;
$show_summary = $this->params->get('course_show_summary');
$show_topics_numbers = $this->params->get('course_show_numbers');
$role = $this->hasPermission;
$user_role = array();
if (!empty($role)) {
    foreach (json_decode($role[0]['role']) as $r) {
        $user_role[] = $r->sortname;
    }
}

$enableCourseSurvey = ($this->course_info['coursesurvey']) ? true : false;
$k = 0;
foreach ($this->topics['sections'] as $topic) {
    $k = $topic['section'];
    $array_topics[$k] = ($topic['id']);
}
if ($this->course_info['guest'])
    $this->is_enroled = true;
if ($session->get('device') == 'mobile') {
    ?>
    <style type="text/css">
        .navbar-header {
            background-color: #126db6;
        }
    </style>
    <?php
}
$check_course_creator = $this->hasPermission[0]['hasPermission'];
if($check_course_creator){
    $class_creator = "creator";
}
$t = 0;
if (is_array($this->mods)) {
    foreach ($this->mods as $tema) {

        $resources = $tema['mods'];
        foreach ($resources as $id => $res) {
            if ($res['mod'] == 'questionnaire') {
                $t++;
                $id_ques = $res['id'];
            }
        }
    }
    if ($t != 0) { // exist activity feedback
        if ($user_role[0] == 'teacher' || $user_role[0] == 'editingteacher') { // is facilitator
            $link = JUri::base() . "feedbackview/".$course_id."_". $id_ques ."_review.html";
        }
        if ($user_role[0] == 'student') { // is learner
            $link = JUri::base() . "feedbackview/".$course_id."_". $id_ques . "_.html";
        }
        if ($check_course_creator == true) { // is course creator
            $link = JUri::base() . 'feedback/edit_' . $course_id . '_' . $id_ques . '.html';
        }
    }
    // if(not exist and role is course creator)
    if ($t == 0) { // not exist activity feedback
        if ($check_course_creator == true) { // is course creator
            $link = JUri::base() . 'feedback/create_' . $course_id . '.html';
        }
        if ($check_course_creator == false) { // is not course cretor
            $link = '';
        }
    }
}
if ($this->course_info['course_type'] && $this->course_info['course_type'] == 'course_lp') {
    $coursetype = 'lpcourse';
} else if ($this->course_info['course_type'] && $this->course_info['course_type'] == 'course_non') {
    $coursetype = 'noncourse';
} else {
    $coursetype = 'noncourse';
} 
require_once(JPATH_SITE . '/components/com_joomdle/views/header.php');
?>
<div class="joomdle-course <?php echo $this->pageclass_sfx ?>">
    <?php if ($check_course_creator && ($this->course_status != 'pending_approval' && $this->course_status != 'approved' && !$this->course_info['self_enrolment'])) : ?>
        <div class="divEditCourseInfo">
            <button class="btPreviewCourse">
                <?php echo JText::_('COURSE_BUTTON_PREVIEW_COURSE'); ?></button>
            </button>
            <button class="btUnPreviewCourse hidden">
                <?php echo JText::_('COURSE_BUTTON_EXIT_PREVIEW_COURSE'); ?></button>
            </button>
            <button class="btEditCourseInfo"
                    onclick="editCourse(<?php echo $this->course_info['remoteid'] . ',' . $this->course_info['cat_id'] . ",'" . $coursetype . "'"; ?>);">
                <img alt="Edit" src="/images/edit30X30.png">
                <?php echo JText::_('COURSE_BUTTON_EDIT_COURSE_INFO'); ?></button>
        </div>
    <?php endif; ?>
    <form id="formUpdateActivity" class="form" method="post"
          action="<?php echo JRoute::_('index.php?option=com_joomdle&task=updateActivity'); ?>">
        <div class="course-content-detail">
            <div class="content-top">
                <div class="content-img" style="background-image: url('<?php  echo $this->course_info['filepath'] . $this->course_info['filename'].'?'.(time()*1000); ?>');"></div>
                <div class="content-title">
                    <h2 class="title-course"><?php echo $this->course_info['fullname']; ?></h2>
                    <?php
                    if (isset($this->course_info['enrol_timeend']) && !$check_course_creator) :
                        echo ($this->course_info['enrol_timeend'] == 0) ? "<p class='enddate'>" . JText::_('VALID_UNTIL') . ': ' . JText::_('UNLIMITED') . "</p>" : "<p class='enddate'>" . JText::_('VALID_UNTIL') . ': ' . date('d/m/Y', $this->course_info['enrol_timeend']) . "</p>";
                    endif;
                    ?>
                </div>
            </div>
            <div class="content-summary">
                <div class="sumContent limitSum">
                    <?php echo $this->course_info['summary']; ?>
                </div>
                <div class="expansionWrapper">
                    <span class='expandBtn'></span>
                </div>
            </div>
            <?php if($this->course_info['learningoutcomes'] != null){
                if ($session->get('device') == 'mobile') {
                    if(strlen($this->course_info['learningoutcomes']) > 100){
                        $out  =  substr($this->course_info['learningoutcomes'],0,100)."...";
                    }
                }
                else{
                    if(strlen($this->course_info['learningoutcomes']) > 250){
                        $out  =  substr($this->course_info['learningoutcomes'],0,250)."...";
                    }
                }
                ?>
            <div class="content-learning-outcome limitOutcome">
                <div class="joomdle_learning_outcome"><?php echo JText::_('COM_JOOMDLE_LEARNING_OUTCOME'); ?></div>
                <?php if($out != null){ ?>
                <div class="expansionWrapper">
                    <span class='expandBtn'></span>
                </div>
                    <div class="outcomeContent limitOutcome">
                        <div class="limit"><?php  echo $out; ?></div></div>
                        <div class="full hidden"><?php  echo $this->course_info['learningoutcomes']; ?></div>
                    </div>

                <?php }else{ ?>
                    <div class="outcomeContent">
                        <span class="full"><?php  echo $this->course_info['learningoutcomes']; ?></span>
            </div>
            <?php } ?>
        </div>
            <?php } ?>
        </div>
        <script type="text/javascript">
            (function ($) {
                if ($('.sumContent')[0].scrollHeight < 90) {
                    $('.content-summary .expansionWrapper').hide();
                } else {
                    $('.content-summary .expansionWrapper').show();
                }
            })(jQuery);
            (function (w) {
                w.joms_queue || (w.joms_queue = []);
                w.joms_queue.push(function ($) {
                    $('.content-summary .expansionWrapper .expandBtn').on('click', function () {
                        $('.sumContent').toggleClass('limitSum');
                    });
                });
            })(window);
        </script>

        <script type="text/javascript">
            (function ($) {
                var width_screen = $( window ).width();
                console.log(width_screen);

                var a =  jQuery('.full').text().length;
                console.log(a);
                    $('.content-learning-outcome .expansionWrapper .expandBtn').on('click', function () {
                        $('.outcomeContent').toggleClass('limitOutcome');
                        if($('.outcomeContent').hasClass('limitOutcome')){
                            $('.full').addClass('hidden');
                            $('.limit').removeClass('hidden');
                        $('.content-learning-outcome').toggleClass('limitOutcome');
                        $('.content-learning-outcome').toggleClass('fullOutcome');
                        }
                        else{
                            $('.full').removeClass('hidden');
                            $('.limit').addClass('hidden');
                        $('.content-learning-outcome').toggleClass('limitOutcome');
                        $('.content-learning-outcome').toggleClass('fullOutcome');
                        }

                    });

            })(jQuery);
        </script>

        <?php
        $count_compled = 0;
        $count_activity = 0;
        if (is_array($this->mods)) {
            foreach ($this->mods as $tema) {
                $resources = $tema['mods'];
                foreach ($resources as $id => $res) {
                    if (($res['mod'] != 'forum') && ($res['mod'] != 'certificate') && ($res['mod'] != 'feedback') && ($res['mod'] != 'questionnaire')) {
                        $mtype = JoomdleHelperSystem::get_mtype($res['mod']);
                        if (!$mtype) // skip unknow modules
                            continue;
                        if ($res['mod_completion']) {
                            $count_compled++;
                        }
                        $count_activity++;
                    }
                }
            }
        }
        $num_progress_bar = intval(($count_compled / $count_activity) * 100);
        $icon_url_feedback = '';
        $icon_url_cert = '';
        $mtype_feedback = '';
        $mtype_cert = '';
        $target_feedback = '';
        $target_cert = '';
        $url_feedback = '';
        $url_cert = '';
        if ($user_role[0] == 'student') {

            ?>
            <div class="joomdle_course_progress_bar">
                <?php
                if ($num_progress_bar == 0) {
                    ?>
                    <div class="course_progress_bar"><?php echo $num_progress_bar . '%'; ?></div>
                    <?php
                } else {
                    ?>
                    <div class="course_progress_bar"
                         style="width: <?php echo $num_progress_bar; ?>%; background: #126DB6;"><?php echo $num_progress_bar . '%'; ?></div>
                <?php } ?>
            </div>
        <?php } ?>
        <div class="joomdle_course_outline">
            <?php echo JText::_('COM_JOOMDLE_COURSE_OUTLINE'); ?>
            <?php if ($check_course_creator && ($this->course_status != 'pending_approval' && $this->course_status != 'approved'  && !$this->course_info['self_enrolment'])) : ?>
                <span class="edit-activity"><img alt="Edit" src="/images/edit30X30.png"></span>
            <?php endif; ?>
        </div>
        <?php
        //skip intro
        if (is_array($this->mods)) {
            $section_count = 0;
            ?>

            <script>
                $(function () {


                });
            </script>
            <script>$('#widget').draggable();</script>
        <?php
        echo "<ul class='nested_with_switc' style='padding-left: 0px;'>";
        foreach ($this->mods as $tema) :
        $hidden = '';
        $class_sec_0 = '';
        if($section_count == 0){
            $class_sec_0 = 'section_default';
        }
        ?>
            <li class="joomdle_course_section <?php echo $class_sec_0; ?> <?php if($check_course_creator) echo "creator"; ?>" style="<?php echo $hidden; ?>" sectionid="<?php echo $array_topics[$tema['section']]; ?>" section="<?php echo $section_count; ?>"
                id="joomdle_course_section<?php echo $array_topics[$tema['section']]; ?>">
                <?php if($section_count != 0):  ?>
                <div class="activity-delete hidden ">
                    <img class="optionCheckSection"
                         id="optionCheckSection<?php echo $array_topics[$tema['section']]; ?>"
                         qcount="<?php echo $section_count; ?>" qsid="<?php echo $array_topics[$tema['section']]; ?>"
                             align="center" src="<?php echo JUri::base(); ?>/images/Delete30x30.png">
                </div>
                <?php endif; ?>
                <?php if ($show_topics_numbers) : ?>
                    <div class="joomdle_item_title joomdle_section_list_item_title <?php if($check_course_creator) echo "creator"; ?>">
                        <?php
                        $title = '';
                        if ($tema['name'])
                            $title = $tema['name'];
                        else {
                            if ($tema['section']) {
                                $title = JText::_('COM_JOOMDLE_TOPIC') . " ";
                                $title .= $tema['section'];
                            } else  $title = JText::_('COM_JOOMDLE_INTRO');
                        }
                         if ($check_course_creator && ($this->course_status != 'pending_approval' && $this->course_status != 'approved'  && !$this->course_info['self_enrolment'])) : ?>
                            <a class="btCourseContent" style="color: #FFF;"
                               href="<?php echo JUri::base() . "addcourseoutline/" . $course_id . '_' . $array_topics[$tema['section']] . ".html"; ?>">+</a>

                        <?php endif; ?>
                        <?php
                        if ($check_course_creator) {
                            echo '<span class="pTitleSection creator" style="position: static;" id="pTitleSection' . $section_count . '">' . ucwords($title) . '</span>';
                        } else
                            echo '<span class="pTitleSection" style="position: static;" id="pTitleSection' . $section_count . '" >' . ucwords($title) . '</span>';
                        ?>


                        <input type="text" id="titleSection<?php echo $array_topics[$tema['section']]; ?>"
                               class="hidden" name="titleSection<?php echo $array_topics[$tema['section']]; ?>" style='color: #126db6; margin-bottom:10px;'
                               value="<?php echo ucwords($title); ?>"/>

                        <!--                    <p id="pTitleSection">--><?php //echo $title; ?><!--</p>-->

                    </div>
                <?php endif; ?>
                <?php if($section_count != 0):  ?>
                    <?php echo '<div class="activity-sort hidden"><img align="center" class="handle" src="/images/rearrange_30x30.png"></div>'; ?>
                <?php endif; ?>
                <div class="joomdle_item_content joomdle_section_list_item_resources">
                    <?php
                    if ($show_summary)
                        if ($tema['summary'])
                            echo $tema['summary'];
                    ?>
                    <?php
                    $resources = $tema['mods'];
                    //                    var_dump($resources);
                    //                    die;
                    if (is_array($resources)) : ?>
                    <?php
                    // check if mobile app view
                        if(isset($_COOKIE['app'])) {
                            $type = $_COOKIE['app'];
                            $appview = '_appview';
                        }
                    ?>

                    <ul id="sortable<?php echo $section_count; ?>" class="connectedSortable ui-sortable"
                        sectionida="<?php echo $array_topics[$tema['section']]; ?>" style="min-height: 10px; padding-left: 0px;">
                        <?php
                        foreach ($resources as $id => $resource) {

                            $mtype = JoomdleHelperSystem::get_mtype($resource['mod']);
                            if (!$mtype || ($resource['mod'] == 'questionnaire' && !$enableCourseSurvey)) // skip unknow modules
                                continue;

                            $css = '';
                            if (($resource['mod'] == 'forum') || ($resource['mod'] == 'certificate') || ($resource['mod'] == 'feedback') || ($resource['mod'] == 'questionnaire')) {
                                $css = 'display: none;';
                            }
                            if ($resource['mod'] == 'certificate'){
                                $exist_certificate = true;
                            }
                            if ($resource['mod'] == 'questionnaire') {
                                $icon_url_feedback = JoomdleHelperSystem::get_icon_url($resource['mod'], $resource['type']);
                                $mtype_feedback = ucfirst($mtype);
                            } else {

                                echo '<li class="activity-list-item activity_normal '.$class_creator.'" id="activity-list-item' . $resource['id'] . '" style="' . $css . '" moduleid="' . $resource['id'] . '" >';

//                                Delete activity
                                if ($check_course_creator) {
                                    echo '<div class="activity-delete hidden"><img class="optionCheckModule" id="optionCheckModule' . $resource['id'] . '" qmid="' . $resource['id'] . '" align="center" src="/images/Delete30x30.png"></div>';
                                }

                                $icon_url = JoomdleHelperSystem::get_icon_url($resource['mod'], $resource['type']);
                                if ($icon_url) {
                                    if ($mtype == 'quiz') {
                                        $activity_name = 'Assessment';
                                    }
                                    if ($mtype == 'assign') {
                                        $activity_name = 'Assignment';
                                    }
                                    if ($mtype == 'page' || $mtype == 'forum' || $mtype == 'url') {
                                        $activity_name = 'Content';
                                    }
                                    if ($mtype == 'scorm') {
                                        $activity_name = 'Scorm';
                                    }
                                    if ($mtype == 'questionnaire') {
                                        $activity_name = 'Questionnaire';
                                    }
                                    if ($mtype == 'certificate') {
                                        $activity_name = 'Certificate';
                                    }
                                    if ($mtype == 'skillsoft') {
                                        $activity_name = 'Skillsoft';
                                    }
                                    if ($mtype == 'resource') {
                                        $activity_name = 'Resource';
                                    }

                                    echo '<div class="activity-image" >';
//                                    align="center" valign="center"
                                    echo '<img align="center" src="' . $icon_url . '">';
                                    echo '<div class="activity-name-box">';
                                    echo '<p class="activity-name">' . ucfirst($activity_name) . '</p>';
                                    echo '</div>';
                                    echo '</div>';
                                }

                            }

                            if ($resource['mod'] == 'label') {
                                $label = JoomdleHelperContent::call_method('get_label', (int)$resource['id']);
                                echo '</P>';
                                echo $label['content'];
                                echo '</P>';
                                continue;
                            }

                            if ((($this->is_enroled) && ($resource['available'])) || $check_course_creator) {
                                $direct_link = JoomdleHelperSystem::get_direct_link($resource['mod'], $course_id, $resource['id'], $resource['type']);
                                if ($direct_link) {
                                    // Open in new window if configured like that in moodle
                                    if ($resource['display'] == 6)
                                        $resource_target = 'target="_blank"';
                                    else
                                        $resource_target = '';

                                    if ($direct_link != 'none') {
                                        if ($resource['mod'] == 'questionnaire') {
                                            $url_feedback = $direct_link;
                                            $target_feedback = $resource_target;
                                        } elseif ($resource['mod'] == 'certificate') {
                                            $url_cert = $direct_link;
                                            $target_cert = $resource_target;
                                        } else {
                                            echo "<a $resource_target  href=\"" . $direct_link . "\">" . $resource['name'] . "</a>";
                                        }
                                    }
                                } else
                                    if ($resource['mod'] == 'questionnaire') {
                                        $url_feedback = JURI::base() . "mod/" . $mtype . "_" . $course_id . "_" . $resource['id'] . "_" . $itemid . ".html";
                                        $target_feedback = $target;
                                    } elseif ($resource['mod'] == 'certificate') {
                                        $url_cert = JURI::base() . "mod/" . $mtype . "_" . $course_id . "_" . $resource['id'] . "_" . $itemid . ".html";
                                        $module_id_certificate = $resource['id'];
                                        $target_cert = $target;
                                        if ($check_course_creator) {
                                            echo "<a $target_cert href=\"" . JURI::base() . "mod/" . $mtype . "_" . $course_id . "_" . $resource['id'] . "_" . $itemid . ".html\" class='activity-link'>" . ucwords($resource['name']) . "</a>";
                                            ?>
                                            <input type="text" id="titleModule<?php echo $resource['id']; ?>"
                                                   class="hidden" name="titleModule<?php echo $resource['id']; ?>"
                                                   value="<?php echo ucwords($resource['name']); ?>"/>
                                            <?php
                                        }
                                    } else {
                                        // echo "<a $target href=\"".$jump_url."&mtype=$mtype&id=".$resource['id']."&course_id=$course_id&create_user=0&Itemid=$itemid&redirect=$direct_link\">".$resource['name']."</a>";
                                        echo "<a $target href=\"" . JURI::base() . "mod/" . $mtype . "_" . $course_id . "_" . $resource['id'] . "_" . $itemid .$appview. ".html\" class='activity-link'>" . ucwords($resource['name']) . "</a>";
                                        ?>
                                        <input type="text" id="titleModule<?php echo $resource['id']; ?>"
                                               class="hidden" name="titleModule<?php echo $resource['id']; ?>"
                                               value="<?php echo ucwords($resource['name']); ?>"/>
                                        <?php
                                        $class = '';
                                        if ($resource['mod_completion']) {
                                            $class = 'completed';
                                        }
                                        if ($check_course_creator) {
                                        } else
                                            echo '<div class="status' . $class . '"></div>';
                                    }
                            } else {
                                echo $resource['name'] . '</div>';
                                if ((!$resource['available']) && ($resource['completion_info'] != '')) : ?>
                                    <div class="joomdle_completion_info">
                                        <?php echo $resource['completion_info']; ?>
                                    </div>
                                    <?php
                                endif;
                            }
                            //                                Sort activity
                            if ($check_course_creator && $resource['mod'] != 'certificate' && $resource['mod'] != 'questionnaire' && $resource['mod'] != 'feedback' && $resource['mod'] != 'forum') {
                                echo '<div class="activity-sort hidden"><img align="center" class="handle" src="/images/rearrange_30x30.png"></div>';
                            }
                            echo '</li>';

                        }
                        echo '</ul>';
                        ?>
                        <?php endif; ?>
                </div>
            </li>

            <?php
            $section_count++;
        endforeach;
            echo "</ul>";


        }
        ?>
        <?php if ($check_course_creator && ($this->course_status != 'pending_approval' && $this->course_status != 'approved'  && !$this->course_info['self_enrolment'])) : ?>

            <span class="for-admins-buttons hidden" >
            <span class="btAddAnActivity">
                <img class="btAddAnActivityText" src="images/addtopic_30x30.png">
                <input type="text" placeholder="<?php echo JText::_('ADD_TOPIC_HERE'); ?>" name="topic_title" id="topic_title" size="14"/>
        </span>
                </span>
            <script type="text/javascript">
                (function ($) {
//                        $('.popupAddAnActivity .btCancel').click(function () {
//                            $('body').removeClass('overlay2');
//                            $('.popupAddAnActivity').hide();
//                        });
//                        $('.btAddAnActivity').click(function () {
//                            $('.popupAddAnActivity').show();
//                            $('body').addClass('overlay2');
//                        });
                    $('.btAddAnActivityText').click(function () {
                        if(($('#topic_title').val() == "") || ($('#topic_title').val() == null)){
                            $('#topic_title').val("Topic Title");
                        }
                        $.ajax({
                            url: '<?php echo JUri::base() . 'topics/create_' . $course_id . '.html'; ?>',
                            type: 'post',
                            data: {
                                'createSection': 1,
                                'title':  $('#topic_title').val(),
                                'des': 'Topic Title Here',
                                'lo': 'Topic Title Here'
                            },
                            beforeSend: function () {
//                                    $('body').addClass('overlay2');
//                                    $('.notification').html('Operating...').fadeIn();
                            },
                            success: function (result) {
                                var res = JSON.parse(result);
//                                    console.log(res);
//                                    console.log(res.sections);
                                if (res.status) {
                                    var s = res.datas.sections[0];
//                                        console.log(s);
                                    var html;
                                    html = '<li class="joomdle_course_section creator" section="' + s.section + '" sectionid="'+ s.id +'" id="joomdle_course_section' + s.id + '">' +
                                        '<div class="activity-delete hidden ">' +
                                        '<img class="optionCheckSection"' +
                                        'id="optionCheckSection' + s.id + '"' +
                                        'qcount="' + s.section + '" qsid="' + s.id + '"' +
                                        'align="center" src="<?php echo JUri::base();?>images/Delete30x30.png">' +
                                        '</div>' +

                                        '<div class="joomdle_item_title joomdle_section_list_item_title">' +
                                        '<span class="pTitleSection creator" id="pTitleSection' + s.section + '">' + s.name + '</span>' +
                                        '<a class="btCourseContent" style="color: #FFF;" href="<?php echo JUri::base();?>addcourseoutline/<?php echo $course_id; ?>_' + s.id + '.html">+</a>' +
                                        '<input type="text" id="titleSection' + s.id + '"' +
                                        'class="hidden" name="titleSection' + s.id + '" style="color: #126db6;"' +
                                        'value="' + s.name + '"/>' +
                                        '</div>' +
                                        '<div class="activity-sort hidden"> <img align="center" class="handle" src="images/rearrange_30x30.png">' +
                                        '</div>' +
                                        '<div class="joomdle_item_content joomdle_section_list_item_resources">' +
                                        '<ul id="sortable'+s.section+'" class="connectedSortable ui-sortable" sectionida="'+s.id+'" style="min-height: 10px; padding-left: 0px;">'+
                                        '</ul>'+
                                        '</div>' +
                                        '</li>';

                                    $('.nested_with_switc').append(html);
                                    var cs = parseInt($('#countSection').val());
                                    $('#countSection').val(cs + 1);
                                    if ($('.edit-activity').hasClass('visible')) $('.edit-activity').click();

                                    var elements = document.getElementsByClassName("connectedSortable");
                                    var names = '';

                                    for (var i = 0; i < elements.length; i++) {
                                        if (i < elements.length - 1)
                                            names += '#' + elements[i].id + ', ';
                                        else
                                            names += '#' + elements[i].id;
                                    }
                                    $("ul.nested_with_switc").sortable({
                                        scroll: false,
                                        appendTo: '.joomdle-course',
                                        connectWith: 'joomdle_course_section',
                                        handle: '.handle',
                                        placeholder: 'placeholder',
                                        revert: true,
                                        opacity: 1,
                                        group: 'no-drop',
                                        drag: false,
                                        forcePlaceholderSize: true
                                    });
                                    $(names).sortable({
                                        connectWith: ".connectedSortable",
                                        appendTo: '.joomdle-course',
                                        scroll: false,
                                        forcePlaceholderSize: true,
                                        placeholder: 'placeholder',
                                        opacity: 1,
                                        handle: '.handle'
                                    });
                                    $(names).disableSelection();
                                    $("#topic_title").val("");
                                    $("#topic_title").attr('size', 14);

                                } else {
                                    alert('Error');
                                }
                            }
                        });
                    });
                })(jQuery);
                function editCourse(cid, catid, coursetype) {
                    window.location.href = "<?php echo JURI::base();?>mycourses/edit_" + cid + "_" + catid + "_" + coursetype + ".html";
                }
            </script>
        <?php endif; ?>

        <div class="button_control">
            <button class="btCancel2 hidden"
                    onClick="window.location.reload()"><?php echo JText::_('COURSE_CONTENT_BTN_CANCEL'); ?></button>
            <input class="btSave hidden" type="submit" value="<?php echo JText::_('COURSE_CONTENT_BTN_SAVE'); ?>"/>

        </div>
        <?php
        //        if ($icon_url_feedback != '' && $url_feedback != '') {
        if ($num_progress_bar < 100) {
            $class_view_feedback = 'disable-feedback';
        }
        ?>
        <?php if ($check_course_creator) : ?>
            <div class="joomdle_item_content joomdle_section_list_item_resources certificate_section">
                <p class="pTitleCertificate"><?php echo JText::_('COM_JOOMDLE_COURSE_CERTIFICATE'); ?></p>
                <div class="activity-list-item">
                    <div class="activity-image"><img src="/images/certificate600x600.png" align="middle">
                        <div class="activity-name-box">
                            <p class="activity-name"><?php echo JText::_('COM_JOOMDLE_CERTIFICATE'); ?></p>
                        </div>
                    </div>
                    <p class="description_certificate"><?php echo JText::_('COM_JOOMDLE_AWARD_A_CERTIFICATE_TO_LEARNERS'); ?></p>
                </div>
            </div>
        <?php endif; ?>
        <div class="div-feedback">
            <?php if ($link != null && $enableCourseSurvey) : ?>
                <div class="joomdle_item_content joomdle_section_list_item_resources feedback_section">
                    <p class="pTitleFeedback"><?php echo JText::_('COM_JOOMDLE_COURSE_FEEDBACK'); ?></p>
                    <div class="activity-list-item" style="">
                        <div class="activity-image"><img src="/images/feedback600x600.png" align="middle">
                            <div class="activity-name-box">
                                <p class="activity-name"><?php echo JText::_('COM_JOOMDLE_FEEDBACK'); ?></p>
                            </div>
                        </div>
                        <p class="description_feedback"><?php echo JText::_('COM_JOOMDLE_COLLECT_LEARNER_FEEDBACK_ABOUT_THE_COURSE'); ?></p>
                </div>
            <?php endif; ?>
            <?php if ($num_progress_bar == 100 && $user_role[0] == 'student' && $exist_certificate == true) {
//                    $questions = JoomdleHelperContent::call_method('get_feedback_question', 0);
//                    JURI::base() . 'component/joomdle/certificate?courseid='.$course_id;

                ?>
                <div class="activity-cert-item">
                    <a class="btn-get-certificate"
                       href="<?php echo JURI::base() . 'certificate/' . $course_id .'.html'; ?>"><?php echo JText::_('COM_JOOMDLE_GET_CERTIFICATE'); ?></a>
                </div>
            <?php } ?>
        </div>

        <input type="hidden" name="courseid" id="courseid" value="<?php echo $course_id; ?>"/>
        <input type="hidden" name="username" id="username" value="<?php echo $username; ?>"/>
        <input type="hidden" name="arr_section_id" id="arr_section_id"/>
        <input type="hidden" name="section_id_delete" id="section_id_delete"/>
        <input type="hidden" name="moduleIdNumHidden" id="moduleIdNumHidden"/>
        <?php foreach ($this->topics['sections'] as $topic) : ?>
            <input type="hidden" name="<?php echo $topic['id']; ?>" id="<?php echo $topic['id']; ?>"/>
        <?php endforeach; ?>
    </form>
    <?php if ($this->params->get('show_back_links')) : ?>
        <div>
            <P align="center">
                <a href="javascript: history.go(-1)"><?php echo JText::_('COM_JOOMDLE_BACK'); ?></a>
            </P>
        </div>
    <?php endif; ?>

</div>


<script type="text/javascript">

    (function ($) {
        <?php
        if ($t != 0) { // exist activity feedback
        if ($user_role[0] != 'student') { // is not learner
        ?>
        $('.div-feedback a').click(function (e) {
            e.preventDefault();
            if ($('.edit-activity').hasClass('visible')) {
                window.location.href = '<?php echo JUri::base() . 'feedback/edit_' . $course_id . '_' . $id_ques . '.html'; ?>';
            } else window.location.href = $(this).attr('href');
        });
        <?php
        }
        }
        ?>
        $('.certificate_section').click(function (e){
            window.location.href = '<?php echo JURI::base() . "certificatetemplate/detail_" . $course_id . ".html"; ?>';
        });

        var ha = function() {
        $('.feedback_section').click(function (e){
            window.location.href = '<?php echo $link; ?>';
        });
        };
        $( "body" ).on( "click", ".feedback_section", ha );

        function resizeInput() {
            $(this).attr('size', $(this).val().length);
            $("#topic_title").attr('size', 14);
        }
            $('.btSave').click(function () {
                var count_section_num = document.getElementsByClassName("joomdle_course_section");
                var arr_sectionid_after_sort =[];
                $('li.joomdle_course_section').each(function(i)
                {
                    arr_sectionid_after_sort.push($(this).attr('sectionid'));
        });
                document.getElementById("arr_section_id").value = arr_sectionid_after_sort;
                var arr_moduleid_in_another_section=[];

                for(var i=0; i<arr_sectionid_after_sort.length; i++){
                    if($("[sectionida="+arr_sectionid_after_sort[i]+"] li").length == 0){
                        var b=[];
                        arr_moduleid_in_another_section.push(b);
                    }else {
                        var c=[];
                        $("[sectionida=" + arr_sectionid_after_sort[i] + "] li").each(function () {
                            c.push($(this).attr('moduleid'));

                        });
                        arr_moduleid_in_another_section.push(c);
        }
                    document.getElementById(arr_sectionid_after_sort[i]).value = arr_moduleid_in_another_section[i];

                }

                console.log(arr_moduleid_in_another_section);
            });
            $('.btPreviewCourse').click(function(){
                $(".btUnPreviewCourse").removeClass('hidden');
                $(".btPreviewCourse").addClass('hidden');
                $(".btEditCourseInfo").addClass('hidden');
                $(".joomdle_course_section").removeClass('creator');
                $(".joomdle_section_list_item_title").removeClass('creator');
                $(".btCourseContent").addClass('hidden');
                $(".edit-activity").addClass('hidden');
                $(".certificate_section").addClass('hidden');
//                $(".feedback_section").addClass('disable');
                $(".pTitleSection").removeClass('creator');
                $(".activity-list-item").removeClass('creator');
//                $( "a" ).addClass( "disable" );
//                $( "body" ).off( "click", ".feedback_section", ha );
                $(".activity_normal").append("<div class='status'></div>");
                $(".joomdle_course_outline").before(" <div class='joomdle_course_progress_bar'><div class='course_progress_bar'>0%</div></div>");
            });
            $('.btUnPreviewCourse').click(function(){
                $(".btPreviewCourse").removeClass('hidden');
                $(".btUnPreviewCourse").addClass('hidden');
                $(".btEditCourseInfo").removeClass('hidden');
                $(".joomdle_course_section").addClass('creator');
                $(".joomdle_section_list_item_title").addClass('creator');
                $(".btCourseContent").removeClass('hidden');
                $(".edit-activity").removeClass('hidden');
                $(".certificate_section").removeClass('hidden');
//                $(".feedback_section").removeClass('disable');
//                $( "a" ).removeClass( "disable" );
                $(".pTitleSection").addClass('creator');
                $(".activity-list-item").addClass('creator');
//                $( "body" ).on( "click", ".feedback_section", ha );
                $( ".status" ).remove();
                $( ".joomdle_course_progress_bar" ).remove();
            });

        $('.edit-activity').click(function () {
            $(this).addClass('visible');
            $(".activity-delete").removeClass('hidden');
            $(".activity-sort").removeClass('hidden');
            $(".btCancel2").removeClass('hidden');
            $(".for-admins-buttons").removeClass('hidden');
            $(".btSave").removeClass('hidden');
            $(".btCourseContent").addClass('hidden');
            $(".btPreviewCourse").addClass('hidden');
            $(".joomdle_item_title").css({"display": "inline"});
            $(".joomdle_course_section").removeClass('creator');
            $(".joomdle_section_list_item_title").removeClass('creator');
            $(".activity_normal").removeClass('creator');
                jQuery('#title-mainnav').html('Edit Course Outline');
            var t = <?php echo count($this->mods);?>;
            var elements = document.getElementsByClassName("connectedSortable");
            var names = '';

            for (var i = 0; i < elements.length; i++) {
                if (i < elements.length - 1)
                    names += '#' + elements[i].id + ', ';
                else
                    names += '#' + elements[i].id;
            }


            $("ul.nested_with_switc").sortable({
                scroll: false,
                appendTo: '.joomdle-course',
                connectWith: 'joomdle_course_section',
                handle: '.handle',
                placeholder: 'placeholder',
                revert: true,
                opacity: 1,
                group: 'no-drop',
                drag: false,
                forcePlaceholderSize: true
            });


    //          #sortable0, #sortable1, #sortable2, #sortable3, #sortable4, #sortable5, #sortable6, #sortable7, #sortable8, #sortable9, #sortable10
            $(names).sortable({
                connectWith: ".connectedSortable",
                appendTo: '.joomdle-course',
                scroll: false,
                forcePlaceholderSize: true,
                placeholder: 'placeholder',
                opacity: 1,
                handle: '.handle'
            });
            $(names).disableSelection();

            // text box title section
            $(".pTitleSection").addClass('hidden');
            $("[id^='titleSection']").removeClass('hidden');

            // text box title activity
            $(".activity-link").addClass('hidden activity-link-edit');
            $("[id^='titleModule']").removeClass('hidden');

            // box auto width with number character
            $('input[type="text"]')
            // event handler
                .keyup(resizeInput)
                // resize on page load
                .each(resizeInput);

            // Edit title activity
            });

        var moduleIdDelete = [];
        $('.optionCheckModule').click(function () {
            var a = $(this).attr('qmid');
            document.getElementById("activity-list-item" + a).remove();
            moduleIdDelete.push(a);
            document.getElementById("moduleIdNumHidden").value = moduleIdDelete;
        });


        var sectionIdDelete = [];
        $(document).on('click', '.optionCheckSection', function () {
            var a = $(this).attr('qsid');
            var b = $(this).attr('qcount');
            var numrelated = $('#sortable' + b + ' > li:visible').length;
            $('.num-relatedelements').html(numrelated);
            if (b == 0) {
                alert("This is the default topic from your course, you cannot delete it.");
            }
            else if (numrelated != 0 && b != 0) {
                alert("The topic cannot be deleted because there are activities inside.");
            }
            else {
                document.getElementById("joomdle_course_section" + a).remove();
            }
            sectionIdDelete.push(a);
            document.getElementById("section_id_delete").value = sectionIdDelete;

        });

        var check_user = <?php echo $check_course_creator; ?>;
        if (check_user == 1) {
            $('.com_joomdle.view-course').toggleClass('manager-course');
        }
    })(jQuery);

</script>