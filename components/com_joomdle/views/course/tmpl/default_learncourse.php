<?php defined('_JEXEC') or die('Restricted access');

$itemid = JoomdleHelperContent::getMenuItem();
$linkstarget = $this->params->get('linkstarget');
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'components/com_joomdle/views/course/tmpl/course.css');
$document->setTitle(JText::_('COM_JOOMDLE_COURSE_OUTLINE'));

$jump_url = JoomdleHelperContent::getJumpURL();
$user = JFactory::getUser();
$username = $user->username;
$session = JFactory::getSession();

$course_id = $this->course_info['remoteid'];
$direct_link = 1;
$show_summary = $this->params->get('course_show_summary');
$show_topics_numbers = $this->params->get('course_show_numbers');

$enableCourseSurvey = ($this->course_info['coursesurvey']) ? true : false;
$this->is_enroled = $this->course_info['guest'] ? true : false;

$check_course_creator = $this->hasPermission[0]['hasPermission'];
$certificateExists = false;
$feedbackExists = false;

require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);

$render_banner = $banner->renderBanner($this->course_info, $this->mods, $this->hasPermission, false);
$summary = $this->course_info['summary'];
$learningOutcome = $this->course_info['learningoutcomes'];
$tartgetAudience = $this->course_info['targetaudience'];
?>
<script src="components/com_joomdle/js/jquery-1.11.1.js"></script>
<script src="components/com_joomdle/js/jquery-ui.min.js"></script>
<script src="components/com_joomdle/js/jquery.ui.touch-punch.min.js"></script>
<script src="components/com_joomdle/js/jquery.animateNumber.min.js"></script>
<div class="joomdle-course <?php echo $this->pageclass_sfx ?>">
    <form id="formUpdateActivity" class="form" method="post" action="<?php echo JRoute::_('index.php?option=com_joomdle&task=updateActivity'); ?>">
        <div class="course-content-detail">
            <div class="content-summary">
                <div class="sumHeader"><?php echo JText::_('COM_JOOMDLE_COURSE_DESCRIPTION'); ?></div>
                <div class="sumContent">
                    <p class="card-description"><?php echo nl2br($summary); ?></p>
                    <span class="more"></span>
                    <span class="less"></span>
                </div>
            </div>
            <?php if ($learningOutcome != null) { ?>
                <div class="content-learning-outcome limitOutcome">
                    <div class="joomdle_learning_outcome"><?php echo JText::_('COM_JOOMDLE_LEARNING_OUTCOME'); ?></div>
                    <div class="outcomeContent">
                        <p class="card-description"><?php echo nl2br($learningOutcome); ?></p>
                        <span class="more"></span>
                        <span class="less"></span>
                    </div>
                </div>
            <?php } ?>
            <?php if ($tartgetAudience != null) { ?>
                <div class="targetAudience">
                    <div class="targetHeader"><?php echo JText::_('COM_JOOMDLE_COURSE_TARGET_AUDIENCE'); ?></div>
                    <div class="targetAudienceContent">
                        <p class="card-description"><?php echo nl2br($tartgetAudience); ?></p>
                        <span class="more"></span>
                        <span class="less"></span>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="joomdle_course_outline">
            <?php echo JText::_('COM_JOOMDLE_COURSE_OUTLINE_COURESE'); ?>
        </div>
        <?php
        $invisibleMods = array('forum', 'certificate', 'feedback', 'questionnaire');
        if (is_array($this->mods)) {
            $count_compled = 0;
            $count_activity = 0;
            foreach ($this->mods as $tema) {
                if ($tema['visible'] == 0 ) {
                    foreach ($tema['mods'] as $id => $resource) {
                        if ($resource['mod'] == 'questionnaire') {
                            $feedbackExists = true;
                            $id_ques = $resource['id'];
                        }
                        if ($resource['mod'] == 'certificate') $certificateExists = true;
                    }
                } else {
                ?>
                <div class="course-outline">
                    <span class="outline-section-name"><?php echo $tema['name']; ?></span>
                    <?php if (!empty($tema['mods'])) { ?>
                        <div class="outline-activities-list">
                        <?php foreach ($tema['mods'] as $id => $resource) {
                            /* Invisible activity: Forum, Feedback, Certificate, Questionnaire(feedback question)*/
                            $css = in_array($resource['mod'], $invisibleMods) ? 'display: none;' : '';
                                ?>
                                <div class="outline-activity-item" style="<?php echo $css; ?>">
                                    <?php
                                    if (($resource['mod'] != 'forum') && ($resource['mod'] != 'certificate') && ($resource['mod'] != 'feedback') && ($resource['mod'] != 'questionnaire')) {
                                    $mtype = JoomdleHelperSystem::get_mtype($resource['mod']);
                                        if (!$mtype) continue;

                                        if ($resource['mod_completion']) $count_compled++;
                                        $count_activity++;
                                    }
                                    /* Check exist certificate */
                                    if ($resource['mod'] == 'certificate') $certificateExists = true;

                                    /* Check exist questionnaire(Feedback) */
                                    if ($resource['mod'] == 'questionnaire') {
                                        $feedbackExists = true;
                                        $id_ques = $resource['id'];
                                    }

                                    $icon_url = JoomdleHelperSystem::get_icon_url($resource['mod'], $resource['type']);
                                    if ($icon_url) $activity_name = JoomdleHelperSystem::get_activity_name($resource['mod']);

                                    if ($resource['mod'] == 'label') {
                                        $label = JoomdleHelperContent::call_method('get_label', (int)$resource['id']);
                                        echo '<p>'.$label['content'].'</p>';
                                    }

                                    if ((($this->is_enroled) && ($resource['available'])) || $this->course_info['enroled'] || $check_course_creator) {
                                        $direct_link = JoomdleHelperSystem::get_direct_link($resource['mod'], $course_id, $resource['id'], $resource['type']);
                                        if ($direct_link) {
                                            // Open in new window if configured like that in moodle
                                            $resource_target = ($resource['display'] == 6) ? 'target="_blank"' : '';

                                            if ($direct_link != 'none') {
                                                if ($resource['mod'] == 'questionnaire') {
                                                    $url_feedback = $direct_link;
                                                    $target_feedback = $resource_target;
                                                } elseif ($resource['mod'] == 'certificate') {
                                                    $url_cert = $direct_link;
                                                    $target_cert = $resource_target;
                                                } else {
                                                    echo '<div class="activity-image ' . $mtype . '" >';
                                                    echo '<a href="' . $direct_link . '">';
                                                    echo '<img align="center" src="' . $icon_url . '">';
                                                    echo '</a>';
                                                    echo '</div>';
                                                    echo "<div class='activity-name'><a $resource_target  href=\"" . $direct_link . "\">" . $resource['name'] . "</a></div>";
                                                }
                                            }
                                        } else
                                            if ($resource['mod'] == 'questionnaire') {
                                                $url_feedback = JURI::base() . "mod/" . $mtype . "_" . $course_id . "_" . $resource['id'] . "_" . $itemid . ".html";
                                                $target_feedback = $target;
                                            } elseif ($resource['mod'] == 'certificate') {
                                                $url_cert = JURI::base() . "mod/" . $mtype . "_" . $course_id . "_" . $resource['id'] . "_" . $itemid . ".html";
                                                $module_id_certificate = $resource['id'];
                                                $target_cert = $target;
                                                if ($check_course_creator) {
                                                    echo '<div class="activity-image ' . $mtype . '" >';
                                                    echo '<a href="' . JURI::base() . 'mod/' . $mtype . '_' . $course_id . '_' . $resource['id'] . '_' . $itemid . '.html>"';
                                                    echo '<img align="center" src="' . $icon_url . '">';
                                                    echo '</a>';
                                                    echo '</div>';
                                                    echo "<div class='activity-name'><a $target_cert href=\"" . JURI::base() . "mod/" . $mtype . "_" . $course_id . "_" . $resource['id'] . "_" . $itemid . ".html\" class='activity-link'>" . ucwords($resource['name']) . "</a></div>";
                                                }
                                            } elseif ($resource['mod'] == 'assign') {
                                                echo '<div class="activity-image ' . $mtype . '" >';
                                                echo '<a href="' . JURI::base() . 'viewassign/' . $course_id . '_' . $resource['id'] . '.html">';
                                                echo '<img align="center" src="' . $icon_url . '">';
                                                echo '</a>';
                                                echo '</div>';
                                                echo "<div class='activity-name'><a $target href=\"" . JURI::base() . "viewassign/" . $course_id . "_" . $resource['id'] . ".html\" class='activity-link'>" . ucwords($resource['name']) . "</a></div>";
                                            } else {
                                                echo '<div class="activity-image ' . $mtype . '" >';
                                                echo '<a href="' . JURI::base() . 'mod/' . $mtype . '_' . $course_id . '_' . $resource['id'] . '_' . $itemid . '.html">';
                                                echo '<img align="center" src="' . $icon_url . '">';
                                                echo '</a>';
                                                echo '</div>';
                                                echo "<div class='activity-name'><a $target href=\"" . JURI::base() . "mod/" . $mtype . "_" . $course_id . "_" . $resource['id'] . "_" . $itemid . ".html\" class='activity-link'>" . ucwords($resource['name']) . "</a></div>";

                                            }
                                    } else {
                                        echo $resource['name'];
                                        if ((!$resource['available']) && ($resource['completion_info'] != '')) : ?>
                                            <div class="joomdle_completion_info">
                                                <?php echo $resource['completion_info']; ?>
                                            </div>
                                        <?php
                                        endif;
                                    }
                                    /*
                                    $class = $resource['mod_completion'] ? 'completed' : '';
                                    if ($resource['mod'] == 'assign'){
                                        $status_assign = JoomdleHelperContent::call_method ( 'get_assignment_activity_detail', (int)$course_id, $username, (int)$resource['id']);
                                       
                                        if($status_assign['submission_status'] == "Submitted" || $resource['mod_completion']) $class = 'completed';
                                        else $class = '';
                                    }*/
                                    echo '<div id="act-status"><input type="hidden" class="act_val" value="'.$resource['id'].'" /><input type="hidden" class="act_type" value="'.$mtype.'" /></div>';
                                    ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <?php
                }
            }
        }

        $num_progress_bar = intval(($count_compled / $count_activity) * 100);
        ?>
        <div class="outline-footer">
            <!--    Check condition leaner can get certificate        -->
            <?php if ((in_array('student', $this->myRoles)) && $certificateExists) { ?>
                <div class="activity-cert-item">
                    <a class="btn-get-certificate" href="<?php echo JURI::base() . 'certificate/' . $course_id . '.html'; ?>"><?php echo JText::_('COM_JOOMDLE_GET_CERTIFICATE'); ?></a>
                </div>
            <?php }
            // Check condition show feedback block
            if ($feedbackExists) { // exist activity feedback
                if (in_array('student', $this->myRoles)) { // is learner
                    $link = JUri::base() . "feedbackview/" . $course_id . "_" . $id_ques . "_inview.html";
                }
            }
            if ($link != null && $enableCourseSurvey) : ?>
                <div class="activity-list-item">
                    <p class="pTitleFeedback"><?php echo JText::_('COM_JOOMDLE_COURSE_FEEDBACK'); ?></p>
                </div>
            <?php endif; ?>
            <?php if ($this->params->get('show_back_links')) : ?>
                <div>
                    <p align="center">
                        <a href="javascript: history.go(-1)"><?php echo JText::_('COM_JOOMDLE_BACK'); ?></a>
                    </p>
                </div>
            <?php endif; ?>
        </div>
    </form>
</div>
<script type="text/javascript">
    (function ($) {
        /*$(document).ready(function() {
            $('.course_progress_bar').css({'width': '<?php // echo $num_progress_bar ? $num_progress_bar : 0; ?>%'});
            $('#completed-percent').animateNumber({ number: <?php // echo $num_progress_bar ? $num_progress_bar : 0; ?> });
        });*/

        if ($(".sumContent .card-description").height() > 40) {
            $(".sumContent .more").html("show more");
            $(".sumContent .card-description").addClass('less1');
        }
        $(".sumContent .more").on("click", function () {
            if ($(".sumContent .card-description").hasClass('less1')) {
                $(".sumContent .card-description").addClass('more1').removeClass('less1');
                $(".sumContent .less").html("show less");
                $(".sumContent .more").html("");
            }
        });
        $(".sumContent .less").on("click", function () {
            if ($(".sumContent .card-description").hasClass('more1')) {
                $(".sumContent .card-description").addClass('less1').removeClass('more1');
                $(".sumContent .more").html("show more");
                $(".sumContent .less").html("");
            }
        });

        if ($(".outcomeContent .card-description").height() > 40) {
            $(".outcomeContent .more").html("show more");
            $(".outcomeContent .card-description").addClass('less1');
        }
        $(".outcomeContent .more").on("click", function () {
            if ($(".outcomeContent .card-description").hasClass('less1')) {
                $(".outcomeContent .card-description").addClass('more1').removeClass('less1');
                $(".outcomeContent .less").html("show less");
                $(".outcomeContent .more").html("");
            }
        });
        $(".outcomeContent .less").on("click", function () {
            if ($(".outcomeContent .card-description").hasClass('more1')) {
                $(".outcomeContent .card-description").addClass('less1').removeClass('more1');
                $(".outcomeContent .more").html("show more");
                $(".outcomeContent .less").html("");
            }
        });

        if ($(".targetAudienceContent .card-description").height() > 40) {
            $(".targetAudienceContent .more").html("show more");
            $(".targetAudienceContent .card-description").addClass('less1');
        }
        $(".targetAudienceContent .more").on("click", function () {
            if ($(".targetAudienceContent .card-description").hasClass('less1')) {
                $(".targetAudienceContent .card-description").addClass('more1').removeClass('less1');
                $(".targetAudienceContent .less").html("show less");
                $(".targetAudienceContent .more").html("");
            }
        });
        $(".targetAudienceContent .less").on("click", function () {
            if ($(".targetAudienceContent .card-description").hasClass('more1')) {
                $(".targetAudienceContent .card-description").addClass('less1').removeClass('more1');
                $(".targetAudienceContent .more").html("show more");
                $(".targetAudienceContent .less").html("");
            }
        });

        var width_screen = $(window).width();
        var a = jQuery('.full').text().length;
        $('.content-learning-outcome .expansionWrapper .expandBtn').on('click', function () {
            $('.outcomeContent').toggleClass('limitOutcome');
            if ($('.outcomeContent').hasClass('limitOutcome')) {
                $('.full').addClass('hidden');
                $('.limit').removeClass('hidden');
                $('.content-learning-outcome').toggleClass('limitOutcome').toggleClass('fullOutcome');
            }
            else {
                $('.full').removeClass('hidden');
                $('.limit').addClass('hidden');
                $('.content-learning-outcome').toggleClass('limitOutcome').toggleClass('fullOutcome');
            }
        });

        $('.activity-list-item').click(function (e) {
            if ($('.activity-list-item').hasClass('true')) {
                alert("You can only submit feedback one time.");
            } else {
                window.location.href = '<?php echo $link; ?>';
            }
        });
        $('.outline-activity-item #act-status').each(function (index, element) {
            var id = $(this).find('.act_val').val();
            var type = $(this).find('.act_type').val();
            $.ajax({
                type: "POST",
                url: window.location.href,
                context: this,
                data: {id: id, act: 'update_status', username: '<?php echo $username; ?>', act_type: type},
                success: function(data, textStatus, jqXHR) {
                    // update
                    console.log('Update status...');
                    var result = JSON.parse(data);
                    if(result.data['mod_completion'] === true) {
                        $(this).addClass('statuscompleted');
                    } else {
                        $(this).addClass('status');
                    }
                }
            });
        });
        var check_user = <?php echo $check_course_creator; ?>;
        if (check_user == 1) {
            $('.com_joomdle.view-course').toggleClass('manager-course');
        }

        <?php if ($enableCourseSurvey) : ?>
            $(document).ready(function() {
                $.ajax({
                    type: "POST",
                    url: window.location.href,
                    context: this,
                    data: {courseid: <?php echo $course_id; ?>, act: 'check_completeFeedback', username: '<?php echo $username; ?>' },
                    success: function(data) {
                        var res = JSON.parse(data);
                        console.log('success check feedback.' +res.data);
                        $(".outline-footer .activity-list-item").toggleClass(res.data);
        }
                });
            });
        <?php endif; ?>
            $(document).ready(function() {
                if($('.outline-footer').children(".activity-cert-item")) {
                console.log('running...');
                    $.ajax({
                        type: "POST",
                        url: window.location.href,
                        data: {courseid: <?php echo $course_id; ?>, act: 'check_course_percent', username: '<?php echo $username; ?>' },
                        success: function (data) {
                            var res = JSON.parse(data);
                            console.log('check course completion...'+res.data+' %');
                            if(res.data < 100) {
                                $('.activity-cert-item').hide();
                            } else {
                                $('.activity-cert-item').show();
                            }
                        }
                    });
                }
            });
    })(jQuery);
</script>