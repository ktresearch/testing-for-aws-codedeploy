<?php defined('_JEXEC') or die('Restricted access');

$itemid = JoomdleHelperContent::getMenuItem();
$linkstarget = $this->params->get('linkstarget');
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'components/com_joomdle/views/course/tmpl/course.css');
$document->setTitle(JText::_('COM_JOOMDLE_COURSE_OUTLINE'));

$jump_url = JoomdleHelperContent::getJumpURL();
$user = JFactory::getUser();
$username = $user->username;
$session = JFactory::getSession();

$course_id = $this->course_info['remoteid'];
$direct_link = 1;
$show_summary = $this->params->get('course_show_summary');
$show_topics_numbers = $this->params->get('course_show_numbers');

$exist_certificate = false;
$enableCourseSurvey = ($this->course_info['coursesurvey']) ? true : false;
$k = 0;
foreach ($this->topics['sections'] as $topic) {
    $k = $topic['section'];
    $array_topics[$k] = ($topic['id']);
}
if ($this->course_info['guest'])
    $this->is_enroled = true;

$check_course_creator = $this->hasPermission[0]['hasPermission'];
$t = 0;

if (is_array($this->mods)) {
    $mod_arr = array();
    foreach ($this->mods as $tema) {
        $resources = $tema['mods'];
        foreach ($resources as $id => $res) {
            if ($res['mod'] == 'questionnaire') {
                $t++;
                $id_ques = $res['id'];
            }
            $mod_arr[] = $res['mod'];
        }
    }
    if ($t != 0) { // exist activity feedback
        // is facilitator
        $link = JUri::base() . "feedbackview/" . $course_id . "_" . $id_ques . "_review.html";
    }
}

require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');

$banner = new HeaderBanner(new JoomdleBanner);

$render_banner = $banner->renderBanner($this->course_info, $this->mods, $this->hasPermission);
$summary = $this->course_info['summary'];
$learningOutcome = $this->course_info['learningoutcomes'];
$tartgetAudience = $this->course_info['targetaudience'];
?>
<script src="components/com_joomdle/js/jquery-1.11.1.js"></script>
<script src="components/com_joomdle/js/jquery-ui.min.js"></script>
<script src="components/com_joomdle/js/jquery.ui.touch-punch.min.js"></script>
<div class="joomdle-course <?php echo $this->pageclass_sfx ?>">
    <form id="formUpdateActivity" class="form" method="post"
          action="<?php echo JRoute::_('index.php?option=com_joomdle&task=updateActivity'); ?>">
        <div class="course-content-detail">
            <div class="content-summary">
                <div class="sumHeader"><?php echo JText::_('COM_JOOMDLE_COURSE_DESCRIPTION'); ?></div>
                <div class="sumContent">
                    <p class="card-description"><?php echo nl2br($summary); ?></p>
                    <span class="more"></span>
                    <span class="less"></span>
                </div>
            </div>
            <?php if ($learningOutcome != null) { ?>
                <div class="content-learning-outcome limitOutcome">
                    <div class="joomdle_learning_outcome"><?php echo JText::_('COM_JOOMDLE_LEARNING_OUTCOME'); ?></div>
                    <div class="outcomeContent">
                        <p class="card-description"><?php echo nl2br($learningOutcome); ?></p>
                        <span class="more"></span>
                        <span class="less"></span>
                    </div>
                </div>
            <?php } ?>
            <?php if ($tartgetAudience != null) { ?>
                <div class="targetAudience">
                    <div class="targetHeader"><?php echo JText::_('COM_JOOMDLE_COURSE_TARGET_AUDIENCE'); ?></div>
                    <div class="targetAudienceContent">
                        <p class="card-description"><?php echo nl2br($tartgetAudience); ?></p>
                        <span class="more"></span>
                        <span class="less"></span>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="joomdle_course_outline">
            <?php echo JText::_('COM_JOOMDLE_COURSE_OUTLINE_COURESE'); ?>
        </div>
        <?php
        $target_cert = '';
        $mod_hidden = array('certificate', 'questionnaire');
        //skip intro
        if (is_array($this->mods)) {
            foreach ($this->mods as $tema) {
                if ($tema['visible'] == 0 || empty($tema['mods'])) {
                    continue;
                }
                krsort($mod_hidden);
                ?>
                <div class="course-outline">
                    <span class="outline-section-name"><?php echo $tema['name']; ?></span>
                    <?php if (!empty($tema['mods']) && !empty(array_diff($mod_arr, $mod_hidden))) { ?>
                        <div class="outline-activities-list">
                            <?php
                            $resources = $tema['mods'];
                            foreach ($resources as $id => $resource) {
                                /* Invisible activity: Forum, Feedback, Certificate, Questionnaire(feedback question)*/
                                $css = '';
                                if (($resource['mod'] == 'forum') || ($resource['mod'] == 'certificate') || ($resource['mod'] == 'feedback') || ($resource['mod'] == 'questionnaire')) {
                                    $css = 'display: none;';
                                }
                                ?>
                                <div class="outline-activity-item" style="<?php echo $css; ?>">
                                    <?php
                                    $mtype = JoomdleHelperSystem::get_mtype($resource['mod']);

                                    /* Check exist certificate */
                                    if ($resource['mod'] == 'certificate') {
                                        $exist_certificate = true;
                                    }

                                    $icon_url = JoomdleHelperSystem::get_icon_url($resource['mod'], $resource['type']);

                                    if ($resource['mod'] == 'label') {
                                        $label = JoomdleHelperContent::call_method('get_label', (int)$resource['id']);
                                        echo '</P>';
                                        echo $label['content'];
                                        echo '</P>';
                                    }

                                    if ((($this->is_enroled) && ($resource['available'])) || $check_course_creator) {
                                        $direct_link = JoomdleHelperSystem::get_direct_link($resource['mod'], $course_id, $resource['id'], $resource['type']);
                                        if ($direct_link) {
                                            // Open in new window if configured like that in moodle
                                            if ($resource['display'] == 6)
                                                $resource_target = 'target="_blank"';
                                            else
                                                $resource_target = '';
                                            if ($direct_link != 'none') {
                                                if ($resource['mod'] == 'certificate') {
                                                    $target_cert = $resource_target;
                                                } else {
                                                    echo '<div class="activity-image ' . $mtype . '" >';
                                                    echo '<a href="' . JURI::base() . 'mod/' . $mtype . '_' . $course_id . '_' . $resource['id'] . '_' . $itemid . '.html>"';
                                                    echo '<img align="center" src="' . $icon_url . '">';
                                                    echo '</a>';
                                                    echo '</div>';
                                                    echo "<div class='activity-name'><a $resource_target  href=\"" . $direct_link . "\">" . $resource['name'] . "</a></div>";
                                                }
                                            }
                                        } else
                                            if ($resource['mod'] == 'certificate') {
                                                $module_id_certificate = $resource['id'];
                                                $target_cert = $target;
                                                if ($check_course_creator) {
                                                    echo '<div class="activity-image ' . $mtype . '" >';
                                                    echo '<a href="' . JURI::base() . 'mod/' . $mtype . '_' . $course_id . '_' . $resource['id'] . '_' . $itemid . '.html>"';
                                                    echo '<img align="center" src="' . $icon_url . '">';
                                                    echo '</a>';
                                                    echo '</div>';
                                                    echo "<div class='activity-name'><a $target_cert href=\"" . JURI::base() . "mod/" . $mtype . "_" . $course_id . "_" . $resource['id'] . "_" . $itemid . ".html\" class='activity-link'>" . ucwords($resource['name']) . "</a></div>";
                                                }
                                            } elseif ($resource['mod'] == 'assign') {
                                                echo '<div class="activity-image ' . $mtype . '" >';
                                                echo '<a href="' . JURI::base() . 'viewassign/' . $course_id . '_' . $resource['id'] . '.html">';
                                                echo '<img align="center" src="' . $icon_url . '">';
                                                echo '</a>';
                                                echo '</div>';
                                                echo "<div class='activity-name'><a $target href=\"" . JURI::base() . "viewassign/" . $course_id . "_" . $resource['id'] . ".html\" class='activity-link'>" . ucwords($resource['name']) . "</a></div>";
                                            } else {
                                                echo '<div class="activity-image ' . $mtype . '" >';
                                                echo '<a href="' . JURI::base() . 'mod/' . $mtype . '_' . $course_id . '_' . $resource['id'] . '_' . $itemid . '.html">';
                                                echo '<img align="center" src="' . $icon_url . '">';
                                                echo '</a>';
                                                echo '</div>';
                                                echo "<div class='activity-name'><a $target href=\"" . JURI::base() . "mod/" . $mtype . "_" . $course_id . "_" . $resource['id'] . "_" . $itemid . ".html\" class='activity-link'>" . ucwords($resource['name']) . "</a></div>";
                                            }
                                    } else {
                                        echo $resource['name'];
                                        if ((!$resource['available']) && ($resource['completion_info'] != '')) : ?>
                                            <div class="joomdle_completion_info">
                                                <?php echo $resource['completion_info']; ?>
                                            </div>
                                        <?php
                                        endif;
                                    }
                                    ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            <?php
            }
        }
        ?>
        <div class="outline-footer">
            <!--    Check condition show feedback block-->
            <?php if ($link != null && $enableCourseSurvey) : ?>
                <div class="activity-list-item" style="">
                    <p class="pTitleFeedback"><?php echo JText::_('COM_JOOMDLE_COURSE_FEEDBACK'); ?></p>
                </div>
            <?php endif; ?>
            <?php if ($this->params->get('show_back_links')) : ?>
                <div>
                    <p align="center">
                        <a href="javascript: history.go(-1)"><?php echo JText::_('COM_JOOMDLE_BACK'); ?></a>
                    </p>
                </div>
            <?php endif; ?>
        </div>
    </form>
</div>
<script type="text/javascript">
    (function ($) {
        if ($(".sumContent .card-description").height() > 40) {
            $(".sumContent .more").html("show more");
            $(".sumContent .card-description").addClass('less1');
        }
        $(".sumContent .more").on("click", function () {
            if ($(".sumContent .card-description").hasClass('less1')) {
                $(".sumContent .card-description").addClass('more1').removeClass('less1');
                $(".sumContent .less").html("show less");
                $(".sumContent .more").html("");
            }
        });
        $(".sumContent .less").on("click", function () {
            if ($(".sumContent .card-description").hasClass('more1')) {
                $(".sumContent .card-description").addClass('less1').removeClass('more1');
                $(".sumContent .more").html("show more");
                $(".sumContent .less").html("");
            }
        });

        if ($(".outcomeContent .card-description").height() > 40) {
            $(".outcomeContent .more").html("show more");
            $(".outcomeContent .card-description").addClass('less1');
        }
        $(".outcomeContent .more").on("click", function () {
            if ($(".outcomeContent .card-description").hasClass('less1')) {
                $(".outcomeContent .card-description").addClass('more1').removeClass('less1');
                $(".outcomeContent .less").html("show less");
                $(".outcomeContent .more").html("");
            }
        });
        $(".outcomeContent .less").on("click", function () {
            if ($(".outcomeContent .card-description").hasClass('more1')) {
                $(".outcomeContent .card-description").addClass('less1').removeClass('more1');
                $(".outcomeContent .more").html("show more");
                $(".outcomeContent .less").html("");
            }
        });


        if ($(".targetAudienceContent .card-description").height() > 40) {
            $(".targetAudienceContent .more").html("show more");
            $(".targetAudienceContent .card-description").addClass('less1');
        }
        $(".targetAudienceContent .more").on("click", function () {
            if ($(".targetAudienceContent .card-description").hasClass('less1')) {
                $(".targetAudienceContent .card-description").addClass('more1').removeClass('less1');
                $(".targetAudienceContent .less").html("show less");
                $(".targetAudienceContent .more").html("");
            }
        });
        $(".targetAudienceContent .less").on("click", function () {
            if ($(".targetAudienceContent .card-description").hasClass('more1')) {
                $(".targetAudienceContent .card-description").addClass('less1').removeClass('more1');
                $(".targetAudienceContent .more").html("show more");
                $(".targetAudienceContent .less").html("");
            }
        });

        if ($(".t3-content").children().hasClass("course-menu") == false) {
            $('.joomdle-course ').css("margin-top", "0px");
        }

        $('.activity-list-item').click(function (e) {
            window.location.href = '<?php echo $link; ?>';
        });

        var check_user = <?php echo $check_course_creator; ?>;
        if (check_user == 1) {
            $('.com_joomdle.view-course').toggleClass('manager-course');
        }
    })(jQuery);

</script>