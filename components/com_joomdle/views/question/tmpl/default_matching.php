<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
$itemid = JoomdleHelperContent::getMenuItem();

$linkstarget = $this->params->get( 'linkstarget' );
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$session  = JFactory::getSession();
$edit = ($this->action == 'edit') ? true : false;
if ($session->get('device') == 'mobile') {
    ?>
    <style type="text/css">
        .navbar-header {
            background-color: #126db6;
        }
    </style>
<?php
}
require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
?>
<?php if ($this->hasPermission[0]['hasPermission']) : ?>
    <?php
    if ($edit) {
        $options = json_decode($this->question['options']);
    }
    ?>
    <div class="matching-question">
        <form action="" class="formMatching">
            <h1><?php echo Jtext::_('QUESTION_MATCHING'); ?></h1>
            <p><?php echo Jtext::_('QUESTION_MATCHING_TIP'); ?></p>
            <label><?php echo Jtext::_('QUESTION_TITLE'); ?></label>
            <div class="divMatchingQuestion">
                <textarea name="txtaMatchingQuestion" class="txtaMatchingQuestion" cols="30" rows="10" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_QUESTION_TEXT'); ?>"><?php echo (!$edit) ? '' : $this->question['name']; ?></textarea>
            </div>
            <button class="btAddQuestions" type="button">+</button>
            <div class="options">
                <?php
                if ($edit) {
                    $i = 1;
                    foreach ($options->subquestions as $k => $v) {
                        ?>
                        <div class="block" data-order="<?php echo $i;?>">
                            <input type="text" class="statement" placeholder="<?php echo Jtext::_('QUESTION_MATCHING_STATEMENT').' 1'; ?>" value="<?php echo $v->questiontext;?>"/>
                            <input type="text" class="option" placeholder="<?php echo Jtext::_('QUESTION_MATCHING_OPTION').' 1'; ?>" value="<?php echo $v->answertext;?>"/>
                        </div>
                        <?php
                        $i++;
                    }
                } else {
                    ?>
                    <div class="block" data-order="1">
                        <input type="text" class="statement" placeholder="<?php echo Jtext::_('QUESTION_MATCHING_STATEMENT').' 1'; ?>"/>
                        <input type="text" class="option" placeholder="<?php echo Jtext::_('QUESTION_MATCHING_OPTION').' 1'; ?>"/>
                    </div>
                    <div class="block" data-order="2">
                        <input type="text" class="statement" placeholder="<?php echo Jtext::_('QUESTION_MATCHING_STATEMENT').' 2'; ?>"/>
                        <input type="text" class="option" placeholder="<?php echo Jtext::_('QUESTION_MATCHING_OPTION').' 2'; ?>"/>
                    </div>
                    <div class="block" data-order="3">
                        <input type="text" class="statement" placeholder="<?php echo Jtext::_('QUESTION_MATCHING_STATEMENT').' 3'; ?>"/>
                        <input type="text" class="option" placeholder="<?php echo Jtext::_('QUESTION_MATCHING_OPTION').' 3'; ?>"/>
                    </div>
                <?php } ?>
            </div>

            <div class="clear"></div>
            <div class="buttons">
                <button class="btCancel" type="button"><?php echo Jtext::_('QUESTION_CANCEL'); ?></button>
                <button class="btSave"><?php echo Jtext::_('QUESTION_SAVE'); ?></button>
            </div>
            <button class="btPreview" type="button"><?php echo Jtext::_('QUESTION_PREVIEW'); ?></button>
        </form>

    </div>
    <div class="notification"></div>
    <div class="divPreview">
        <p class="previewTitle"><?php echo JText::_('QUESTION_MATCHING'); ?></p>
        <p class="previewDescription"><?php echo JText::_('QUESTION_MATCHING_TIP'); ?></p>
        <div class="previewQuestions">
            <p class="questionTitle"><?php echo JText::_('COM_JOOMDLE_QUESTION').' 1'; ?></p>
            <p class="questionDescription"></p>
            <!--            <p class="questionAnswer">--><?php //echo JText::_('COM_JOOMDLE_ANSWER'); ?><!--</p>-->
            <div class="previewMatchingBlocks">

            </div>
        </div>
        <button class="previewSubmit" type="button" data-disabled="1"><?php echo JText::_('COM_JOOMDLE_SUBMIT'); ?></button>
        <button class="previewBack" type="button"><?php echo JText::_('COM_JOOMDLE_EXIT_PREVIEW'); ?></button>
    </div>
    <script>
        (function($) {
            $('.formMatching').submit(function(e) {
                e.preventDefault();
            });
            $('.formMatching .btCancel').click(function() {
                window.location.href = '<?php echo JUri::base()."question/list_".$this->course_id.".html"; ?>';
            });
            $('.formMatching .btAddQuestions').click(function() {
                var ord = $('.options > .block:last-of-type').attr('data-order');

                $('.formMatching .options').append('<div class="block" data-order="'+(parseInt(ord)+1)+'">'+
                '<input type="text" class="statement" placeholder="<?php echo Jtext::_('QUESTION_MATCHING_STATEMENT').' '; ?>'+(parseInt(ord)+1)+'"/>'+
                '<input type="text" class="option" placeholder="<?php echo Jtext::_('QUESTION_MATCHING_OPTION').' '; ?>'+(parseInt(ord)+1)+'"/>'+
                '</div>');

                $('.ulSetStatement').append('<li data-order="'+(parseInt(ord)+1)+'">Option '+(parseInt(ord)+1)+'</li>');
            });
            $('.matching-question .btPreview').click(function() {
                $('.divPreview').addClass('visible');
                if ($('.txtaMatchingQuestion').val() == '') {
                    $('.divPreview .questionDescription').html('Empty Question Text');
                } else {
                    $('.divPreview .questionDescription').html($('.txtaMatchingQuestion').val());
                }
                var k = 1;
                var previewOptionSelectHTML = '<div class="divPreviewOptionSelect"><input type="text" readonly class="previewOptionInput" placeholder="Option"/>' +
                    '<ul class="ulPreviewOptionSelect">';
                $('.block .option').each(function() {
                    if ($(this).prev('.statement').val() != '') {
                        previewOptionSelectHTML += '<li class="previewOption">'+$(this).val()+'</li>';
                    }
                });
                previewOptionSelectHTML += '</ul>';
                previewOptionSelectHTML += '</div>';

                $('.previewMatchingBlocks').html('');
                $('.block').each(function() {
                    if ($(this).find('.statement').val() !='') {
                        $('.previewMatchingBlocks').append('' +
                        '<div class="previewBlock">'+
                        '<p class="bold">Statement '+k+':</p>'+
                        '<p class="previewStatementText">'+$(this).find('.statement').val()+'</p>'+
                        '<p class="previewAnswerText">' +
                        '<span class="bold">Answer </span>'+previewOptionSelectHTML+
                        '</p>'+
                        '</div>');
                        k++;
                    }
                });
                $('.matching-question').hide();
            });

            $('.previewBack').click(function() {
                $('.divPreview').removeClass('visible');
                $('.matching-question').fadeIn();
            });

            $(document).on('click', '.previewOptionInput', function() {
                $(this).next('.ulPreviewOptionSelect').toggle();
            });
            $(document).on('click', '.ulPreviewOptionSelect .previewOption', function() {
                $(this).parent().prev('.previewOptionInput').val( $(this).html() );
                $(this).parent().hide();
            });

            $(document).on('click', '.option .left', function() {
                $('.option .active').removeClass('active');
                $(this).addClass('active');
                $('.matching-question .setStatement, .matching-question .setOption').hide();
                $('.matching-question .setStatement').show();
                $('.matching-question .setStatement > p').html('Statement '+$(this).find('.order').html() );
                $('.txtSetStatement').attr('data-order', $(this).parent('.option').attr('data-order') ).val( $(this).attr('data-val') );
                $('.hdSelectOption').val($(this).attr('data-opt'));
                if ( typeof($(this).attr('data-opt')) != 'undefined' ) $('.txtSelectOption').val('Option '+$(this).attr('data-opt') ); else $('.txtSelectOption').val('');
            });
            $(document).on('click', '.option .right', function() {
                $('.option .active').removeClass('active');
                $(this).addClass('active');
                $('.matching-question .setStatement, .matching-question .setOption').hide();
                $('.matching-question .setOption').show();
                $('.matching-question .setOption > p').html('Option '+$(this).find('.order').html() );
                $('.txtSetOption').attr('data-order', $(this).parent('.option').attr('data-order') ).val( $(this).attr('data-val') );
            });
            $(document).on('focus', '.divSetStatement, .divSetStatement .glyphicon-menu-down', function() {
                $('.ulSetStatement').show();
            });
            $(document).on('click', '.ulSetStatement > li', function() {
                $('.txtSelectOption').val( $(this).html() );
                $('.ulSetStatement').hide();
                $('.hdSelectOption').val($(this).attr('data-order'));
                if ($('.txtSetStatement').val() != '') {
                    $('.option[data-order="'+$('.txtSetStatement').attr('data-order')+'"] .left').addClass('ok').attr('data-val', $('.txtSetStatement').val()).attr('data-opt', $('.hdSelectOption').val());
                }
            });

            $('.txtSetOption').blur(function() {
                if ($(this).val() != '') {
                    $('.option[data-order="'+$(this).attr('data-order')+'"] .right').addClass('ok').attr('data-val', $(this).val());
                } else {
                    $('.option[data-order="'+$(this).attr('data-order')+'"] .right').removeClass('ok').attr('data-val', '');
                }
            });
            $('.txtSetStatement').blur(function() {
                if ($('.hdSelectOption').val() != '') {
                    if ($(this).val() != '') {
                        $('.option[data-order="'+$(this).attr('data-order')+'"] .left').addClass('ok').attr('data-val', $(this).val()).attr('data-opt', $('.hdSelectOption').val());
                    } else {
                        $('.option[data-order="'+$(this).attr('data-order')+'"] .left').removeClass('ok').attr('data-val', '').attr('data-opt', '');
                    }
                }
            });

            $('.formMatching .btSave').click(function() {
                var invalidA = false;
                var subquestions = [];
                var subanswers = [];
                $('.block').each(function() {
                    var ord = $(this).attr('data-order');
                    if ($(this).find('.statement').val() == '' || $(this).find('.option').val() == '') {
                        invalidA = true;
                    }
                    subquestions.push($(this).find('.statement').val());
                    subanswers.push($(this).find('.option').val());
                });
                if (invalidA) {
                    alert('Empty statement or option.');
                    return;
                }

                var act = '<?php echo (isset($this->question)) ? "update" : "create" ; ?>';
                $.ajax({
                    url: window.location.href,
                    type: 'POST',
                    data: {
                        <?php echo (isset($this->question)) ? "qid: ".$this->qid."," : "" ; ?>
                        act: act,
                        name: $('.txtaMatchingQuestion').val(),
                        courseid : <?php echo $this->course_id; ?>,
                        noanswers: $('.options .block').length,
                        subquestions : subquestions,
                        subanswers : subanswers
                    },
                    beforeSend: function() {
                        $('body').addClass('overlay2');
                        $('.notification').html('Loading...').fadeIn();
                    },
                    success: function(data, textStatus, jqXHR) {
                        var res = JSON.parse(data);

                        $('body').removeClass('overlay2');
                        $('.notification').fadeOut();

                        if (res.error == 1) {
                            switch (res.comment) {
                                case 'nopermission':
                                    alert('You don\'t have permission to create question.');
                                    break;
                                default:
                                    alert('Unknown Error.');
                                    break;
                            }
                        } else {
                            console.log(res);
                            window.location.href = "<?php echo JUri::base()."question/list_".$this->course_id; ?>.html";
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                    }
                });

            });

        })(jQuery);
    </script>
<?php
endif;?>
