<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
$itemid = JoomdleHelperContent::getMenuItem();

$linkstarget = $this->params->get( 'linkstarget' );
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$session  = JFactory::getSession();
if ($session->get('device') == 'mobile') {
    ?>
    <style type="text/css">
        .navbar-header {
            background-color: #126db6;
        }
    </style>
<?php
}
require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
?>
<?php if ($this->hasPermission[0]['hasPermission']) : ?>        
    <?php
    if ($this->action == 'remove') {
        if ($this->question['res'] != 1) {
            ?>
            <div class="joomdle-remove">
                <p><?php echo Jtext::_('QUESTION_ASK_REMOVE'); ?></p>
                <button class="yes" type="button"><?php echo JText::_('COM_JOOMDLE_YES'); ?></button>
                <button class="no" type="button"><?php echo JText::_('COM_JOOMDLE_NO'); ?></button>
            </div>
            <div class="notification"></div>
            <script type="text/javascript">
                (function ($) {
                    $('.joomdle-remove .no').click(function () {
                        window.location.href = "<?php echo JUri::base()."question/list_".$this->course_id.".html"; ?>";
                    });
                    $('.joomdle-remove .yes').click(function () {
                        var act = 'remove';
                        $.ajax({
                            url: window.location.href,
                            type: 'POST',
                            data: {
                                <?php echo (isset($this->qid)) ? "qid: ".$this->qid."," : "" ; ?>
                                act: act
                            },
                            beforeSend: function () {
                                $('body').addClass('overlay2');
                                $('.notification').html('Loading...').fadeIn();
                            },
                            success: function (data, textStatus, jqXHR) {
                                var res = JSON.parse(data);

                                $('body').removeClass('overlay2');
                                $('.notification').fadeOut();
                                if (res.error == 1) {
                                    alert(res.comment);
                                } else {
                                    window.location.href = "<?php echo JUri::base()."question/list_".$this->course_id; ?>.html";
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log('ERRORS: ' + textStatus);
                            }
                        });
                    });

                })(jQuery);
            </script>
        <?php
        } else {
            echo '<p class="tcenter" style="padding-top:30px;">'.JText::_('COM_JOOMDLE_QUESTION_REMOVED').'</p>';
        }
    } else
    if (isset($this->questions)) { ?>
        <div class="joomdle-questions">
            <div class="questionsBank">
                <p class="lblQuestions"><?php echo Jtext::_('QUESTION_QUESTIONS');?> <span class="questionCount"><?php echo '('.count($this->questions).')';?></span>
                </p>
                <?php
                $count = 0;
                foreach ($this->questions as $k => $v) {
                    $count++;
                    ?>
                    <div class="question q<?php echo $v['id'];?>" qid="<?php echo $v['id'];?>" data-qtype="<?php echo $v['qtype'];?>">
                        <div class="questionIcon"><img src="/media/joomdle/images/question.png"></div>
                        <div class="questionName"><?php echo $v['name'];?></div>
                        <div class="questionArrowDown">
                            <span class="glyphicon glyphicon-menu-down"></span>
                            <div class="questionAction">
                                <ul class="ulQuestionAction">
                                    <li class="liEditQuestion">
                                        <div class="icon-pencil"></div>
                                        <span><?php echo Jtext::_('QUESTION_BTN_EDIT'); ?></span>
                                    </li>
                                    <li class="liRemoveQuestion">
                                        <div class="icon-delete"></div>
                                        <span><?php echo Jtext::_('QUESTION_BTN_REMOVE'); ?></span>
                                    </li>
                                </ul>
<!--                                <div class="triangle"></div>-->
                            </div>
                            <div class="triangle"></div>
                        </div>

                        <div class="clear"></div>
                    </div>
                <?php }
                if ($count == 0) {
                    echo '<p class="textNoQuestion center">'.JText::_('COM_JOOMDLE_NO_QUESTION').'</p>';
                }
                ?>              
                
            </div>
        </div>
        <div class="joomdle-remove">
            <p><?php echo Jtext::_('QUESTION_ASK_REMOVE'); ?></p>
            <button class="yes" type="button"><?php echo JText::_('COM_JOOMDLE_YES'); ?></button>
            <button class="no" type="button"><?php echo JText::_('COM_JOOMDLE_NO'); ?></button>
        </div>
        <div class="notification"></div>
        <script type="text/javascript">
            (function($) {
                $('.liEditQuestion').click(function() {
                    var qid = $(this).parent().parent().parent().parent('.question').attr('qid');
                    var qtype = $(this).parent().parent().parent().parent('.question').attr('data-qtype');
                    window.location.href = "<?php echo JUri::base()."question/edit_".$this->course_id; ?>_"+qtype+"_"+qid+".html";
                });
                var removeQid, removeQtype;
                $('.liRemoveQuestion').click(function() {
                    removeQid = $(this).parent().parent().parent().parent('.question').attr('qid');
                    removeQtype = $(this).parent().parent().parent().parent('.question').attr('data-qtype');
                    $('body').addClass('overlay2');
                    $('.joomdle-remove').fadeIn();
                });
                // $('.btAddQuestion').click(function() {
                //     window.location.href = "<?php echo JUri::base()."/question/create_".$this->course_id.".html"; ?>";
                // });

                $('.questionsBank .question .questionArrowDown').click(function() {
                    $('.questionAction').hide();
                    $('.triangle').hide();
                    $(this).find('.questionAction').toggle();
                    $(this).find('.triangle').toggle();
                });
                $('body').click(function(e) {
                    if ( !$(e.target).hasClass('ulQuestionAction') && !$(e.target).hasClass('questionAction') && !$(e.target).hasClass('questionArrowDown') && !$(e.target).hasClass('glyphicon-menu-down') ) $('.questionAction, .triangle').hide();
                });

                $('.joomdle-remove .no').click(function () {
                    $('body').removeClass('overlay2');
                    $('.joomdle-remove').fadeOut();
                });
                $('.joomdle-remove .yes').click(function () {
                    var act = 'remove';
                    $.ajax({
                        url: window.location.href,
                        type: 'POST',
                        data: {
                            qid: removeQid,
                            act: act
                        },
                        beforeSend: function () {
                            $('body').addClass('overlay2');
                            $('.joomdle-remove').fadeOut();
                            $('.notification').html('Loading...').fadeIn();
                        },
                        success: function (data, textStatus, jqXHR) {
                            var res = JSON.parse(data);

                            $('body').removeClass('overlay2');
                            $('.notification').fadeOut();
                            if (res.error == 1) {
                                $('.notification').html(res.comment).fadeIn();
                                setTimeout(function() {
                                    $('.notification').fadeOut();
                                }, 2000);
                            } else {
                                $('.joomdle-questions .question[qid="'+removeQid+'"]').fadeOut();
                                var str = $('.questionCount').html();
                                str = str.replace("(", "");
                                str = str.replace(")", "");                                    
                                str = parseInt(str) - 1;
                                $('.questionCount').html('(' + str + ')');
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log('ERRORS: ' + textStatus);
                        }
                    });
                });
            })(jQuery);
        </script>
    <?php } else {
        ?>
        <div class="slQuestionTypePage">
            <p class="labelQuestion"><?php echo Jtext::_('COM_JOOMDLE_QUESTIONS'); ?></p>
            <p class="descriptionQuestion"><?php echo Jtext::_('COM_JOOMDLE_QUESTION_TIP1'); ?></p>

            <input type="hidden" value="" name="hdQuestType" class="hdQuestType" />
            <div class="inputQuestType">
                <div p="1" class="status"></div><span class="spanstatus"><?php echo Jtext::_('QUESTION_TRUE_FALSE'); ?></span>
                <p><?php echo Jtext::_('QUESTION_TIP2'); ?></p>
            </div>
            <div class="inputQuestType">
                <div p="2" class="status"></div><span class="spanstatus"><?php echo Jtext::_('QUESTION_MULTICHOICE'); ?></span>
                <p><?php echo Jtext::_('QUESTION_TIP3'); ?></p>
            </div>
            <div class="inputQuestType">
                <div p="3" class="status"></div><span class="spanstatus"><?php echo Jtext::_('QUESTION_MATCHING'); ?></span>
                <p><?php echo Jtext::_('QUESTION_TIP4'); ?></p>
            </div>
            <div class="inputQuestType">
                <div p="4" class="status"></div><span class="spanstatus"><?php echo Jtext::_('QUESTION_TEXT_RESPONSE'); ?></span>
                <p><?php echo Jtext::_('QUESTION_TIP5'); ?></p>
            </div>
            <button class="btCreateQuestion" type="button"><?php echo Jtext::_('COM_JOOMDLE_NEXT'); ?></button>
        </div>
        <script>
            (function($) {
                $('.slQuestionTypePage .status').click(function() {
                    $('.slQuestionTypePage .status').removeClass('selected');
                    $(this).addClass('selected');
                    $('.hdQuestType').val($(this).attr('p'));

                });
                $('.spanstatus').click(function() {
                    $(this).prev('.status').click();
                });
                $('.slQuestionTypePage .btCreateQuestion').click(function() {
                    if ($('.hdQuestType').val() == '') alert('<?php echo Jtext::_('QUESTION_CHOOSE'); ?>'); else
                        window.location.href = "<?php echo JUri::base()."question/create_".$this->course_id; ?>_"+$('.hdQuestType').val()+".html";
                });
            })(jQuery);
        </script>

    <?php
    } ?>
<?php endif;?>
