<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
$itemid = JoomdleHelperContent::getMenuItem();

$linkstarget = $this->params->get( 'linkstarget' );
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$session  = JFactory::getSession();
$edit = ($this->action == 'edit') ? true : false;

if ($session->get('device') == 'mobile') {
    ?>
    <style type="text/css">
        .navbar-header {
            background-color: #126db6;
        }
    </style>
<?php
}
require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
?>
<?php if ($this->hasPermission[0]['hasPermission']) : ?>
    <?php
    if ($edit) {
        $options = json_decode($this->question['options']);
    }
    ?>
    <div class="multiple-choice-question">
        <form action="" class="formMC">
            <h1><?php echo Jtext::_('QUESTION_MULTICHOICE'); ?></h1>
            <p><?php echo Jtext::_('QUESTION_MULTICHOICE_TIP'); ?></p>
            <label><?php echo JText::_('QUESTION_TITLE'); ?></label>
            <textarea name="txtaMCQuestion" class="txtaMCQuestion" cols="30" rows="10" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_QUESTION_TEXT'); ?>"><?php echo (isset($this->question)) ? ( ($this->question['questiontext'] == '') ? $this->question['name'] : $this->question['questiontext']) : ''; ?></textarea>
            <label><?php echo Jtext::_('QUESTION_ANSWER'); ?></label>

            <div class="answers">
                <?php
                if ($edit) {
                    $i = 1;
                    $alp = str_split('0ABCDEFGHIJKLMNOPQRSTUVWXYZ');
                    $active = '';

                    foreach ($options->answers as $k => $v) {
                        $active = ($v->fraction > 0) ? $i : $active;
                        ?>
                        <div class="answer <?php echo ($v->fraction > 0) ? 'active' : ''; ?>" data-order="<?php echo $i;?>">
                            <div class="status"></div>
                            <span class="order"><?php echo $alp[$i];?>.</span>
                            <input type="text" class="txtAnswerContent" value="<?php echo $v->answer;?>" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_ANSWER'); ?>" />
                            <input type="text" class="txtRespondContent" value="<?php echo (isset($v->feedback)) ? $v->feedback : ''; ?>"  placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>" />
                        </div>
                        <?php
                        $i++;
                    }
                } else {
                    ?>
                    <div class="answer" data-order="1">
                        <div class="status"></div>
                        <span class="order">A.</span>
                        <input type="text" class="txtAnswerContent" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_ANSWER'); ?>" />
                        <input type="text" class="txtRespondContent" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>" />
                    </div>
                    <div class="answer" data-order="2">
                        <div class="status"></div>
                        <span class="order">B.</span>
                        <input type="text" class="txtAnswerContent" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_ANSWER'); ?>" />
                        <input type="text" class="txtRespondContent" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>" />
                    </div>
                    <div class="answer" data-order="3">
                        <div class="status"></div>
                        <span class="order">C.</span>
                        <input type="text" class="txtAnswerContent" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_ANSWER'); ?>" />
                        <input type="text" class="txtRespondContent" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>" />
                    </div>
                    <div class="answer" data-order="4">
                        <div class="status"></div>
                        <span class="order">D.</span>
                        <input type="text" class="txtAnswerContent" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_ANSWER'); ?>" />
                        <input type="text" class="txtRespondContent" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>" />
                    </div>
                    <div class="answer" data-order="5">
                        <div class="status"></div>
                        <span class="order">E.</span>
                        <input type="text" class="txtAnswerContent" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_ANSWER'); ?>" />
                        <input type="text" class="txtRespondContent" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>" />
                    </div>
                <?php } ?>
            </div>

            <input type="hidden" name="hdAnswer" class="hdAnswer" value="<?php echo ($active) ? $active : ''; ?>"/>
            <!--                <button class="btAddQuestions">--><?php //echo Jtext::_('QUESTION_BTN_ADD'); ?><!--</button>-->
            <p><?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK_TEXT')?></p>
            <div class="divMCQuestionFeedback">
                <textarea name="txtaMCQuestionFeedback" class="txtaMCQuestionFeedback" cols="30" rows="10" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>"><?php echo (isset($this->question)) ? $this->question['generalfeedback'] : ''; ?></textarea>
            </div>
            <div class="clear"></div>
            <div class="buttons">
                <button class="btCancel" type="button"><?php echo Jtext::_('QUESTION_CANCEL'); ?></button>
                <button class="btSave"><?php echo Jtext::_('QUESTION_SAVE'); ?></button>
            </div>
            <button class="btPreview" type="button"><?php echo Jtext::_('QUESTION_PREVIEW'); ?></button>
        </form>
    </div>
    <div class="notification"></div>
    <div class="divPreview">
        <p class="previewTitle"><?php echo JText::_('QUESTION_MULTICHOICE'); ?></p>
        <p class="previewDescription"><?php echo JText::_('QUESTION_MULTICHOICE_TIP'); ?></p>
        <div class="previewQuestions">
            <p class="questionTitle"><?php echo JText::_('COM_JOOMDLE_QUESTION').' 1'; ?></p>
            <p class="questionDescription"></p>
            <p class="questionAnswer"><?php echo JText::_('COM_JOOMDLE_ANSWER'); ?></p>
            <div class="previewMCOptions">

            </div>

        </div>
        <button class="previewSubmit" type="button" data-disabled="1"><?php echo JText::_('COM_JOOMDLE_SUBMIT'); ?></button>
        <button class="previewBack" type="button"><?php echo JText::_('COM_JOOMDLE_EXIT_PREVIEW'); ?></button>
    </div>
    <script>
        (function($) {
            var alphabet = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
            $('.formMC').submit(function(e) {
                e.preventDefault();
            });
            $('.formMC .btCancel').click(function() {
                window.location.href = '<?php echo JUri::base()."question/list_".$this->course_id.".html"; ?>';
            });
            $('.formMC .btAddQuestions').click(function() {
                var ord = $('.answers > .answer:last-of-type').attr('data-order');
                if (ord > 9) {
                    var oldurl = $("#iframe-question").attr('src');
                    $("#iframe-question").attr('src',  oldurl + '&qnum='+ (parseInt(ord)+1) );
                }
                $('.formMC .answers').append('<div class="answer" data-order="'+(parseInt(ord)+1)+'">'+
                '<div class="status"></div>'+
                '<span class="order">'+alphabet[parseInt(ord)+1]+'.</span>'+
                '<input type="text" class="txtAnswerContent" />'+
                '</div>');
                $('.formMC .responds').append('<div class="respond" data-order="'+(parseInt(ord)+1)+'">'+
                '<span class="order">'+alphabet[parseInt(ord)+1]+'.</span>'+
                '<div class="divRespondContent">'+
                '<input type="text" class="txtRespondContent"/>'+
                '<div class="status"></div>'+
                '<span>Check if correct answer</span>'+
                '</div>'+
                '</div>');
            });
            $(document).on('click', '.answer .status', function() {
                //                        $('.answer.active').removeClass('active');
                $(this).parent('.answer').toggleClass('active');
                var ord = $(this).parent('.answer').attr('data-order');
                $('.hdAnswer').val(ord);
            });
            $(document).on('click', '.respond .status', function() {
                $(this).parent().parent('.respond').toggleClass('active');
            });
            $(document).on('click', '.respond .divRespondContent > span', function() {
                $(this).prev('.status').click();
            });

            $('.multiple-choice-question .btPreview').click(function() {
                $('.divPreview').addClass('visible');
                if ($('.txtaMCQuestion').val() == '') {
                    $('.divPreview .questionDescription').html('Empty Question Text.');
                } else {
                    $('.divPreview .questionDescription').html($('.txtaMCQuestion').val());
                }
                $('.previewMCOptions').html('');
                var x = 1;
                $('.answers > .answer').each(function() {
                    if ($(this).find('.txtAnswerContent').val() != '') {
                        var t = '';
                        if ($(this).hasClass('active')) {
                            // t = ' active';
                        }
                        $('.previewMCOptions').append(
                            '<div class="previewAnswer">' +
                            '<div class="previewStatus'+t+'"></div>' +
                            '<span class="previewOrder">'+alphabet[x]+'.</span>' +
                            '<span class="previewAnswerText"> '+$(this).find('.txtAnswerContent').val()+'</span>' +
                            '</div>');
                        x++;
                    }
                });
                $('.multiple-choice-question').hide();
            });
            $(document).on('click', '.previewStatus', function() {
                // $(this).toggleClass('active');
            });

            $('.previewBack').click(function() {
                $('.divPreview').removeClass('visible');
                $('.multiple-choice-question').fadeIn();
            });

            $('.formMC .btSave').click(function() {
                if ($('.multiple-choice-question .answer.active').length < 1) {
                    alert('Please select right answer.');
                    return;
                }
                //                        $('#iframe-question').contents().find('#id_name').val( $('.txtaMCQuestion').val().substr(0, 50) );
                //                        $('#iframe-question').contents().find('#id_questiontext').val($('.txtaMCQuestion').val());
                //
                //                        if ($('.multiple-choice-question .answer.active').length > 1) {
                //                            $('#iframe-question').contents().find('#id_single').val(0);
                //                        } else $('#iframe-question').contents().find('#id_single').val(1);

                var emptyAnswerContent = true;
                var doubleAnswer = false;
                var answers = [];
                var feedbacks = [];
                var fraction = [];
                var norightanswer = 0;
                $('.txtAnswerContent').each(function() {
                    if ($(this).val() != '') {
                        emptyAnswerContent = false;

                        if ($(this).parent().hasClass('active')) {
                            fraction.push(1);
                            norightanswer++;
                        } else fraction.push(0);

                        if ( $.inArray( $(this).val(), answers ) != -1 ) doubleAnswer = true;
                        answers.push( $(this).val());
                    }
                });
                if (doubleAnswer) {
                    alert('Options must be various.');
                    return;
                }
                if (emptyAnswerContent) {
                    alert('Empty Answer Content.');
                    return;
                }

                var act = '<?php echo (isset($this->question)) ? "update" : "create" ; ?>';

                $('.txtRespondContent').each(function() {
//                    if ($(this).val() != '') {
                        feedbacks.push($(this).val());
//                    }
                });
                $.ajax({
                    url: window.location.href,
                    type: 'POST',
                    data: {
                        <?php echo (isset($this->question)) ? "qid: ".$this->qid."," : "" ; ?>
                        act: act,
                        name: $('.txtaMCQuestion').val(),
                        courseid : <?php echo $this->course_id; ?>,
                        noanswers: $('.options .block').length,
                        answers : answers,
                        feedbacks : feedbacks,
                        fraction: fraction,
                        norightanswer: norightanswer,
                        generalfeedback: $('.txtaMCQuestionFeedback').val()
                    },
                    beforeSend: function() {
                        $('body').addClass('overlay2');
                        $('.notification').html('Loading...').fadeIn();
                    },
                    success: function(data, textStatus, jqXHR) {
                        var res = JSON.parse(data);
                        console.log(res);
                        $('body').removeClass('overlay2');
                        $('.notification').fadeOut();

                        if (res.error == 1) {
                            switch (res.comment) {
                                case 'nopermission':
                                    alert('You don\'t have permission to create question.');
                                    break;
                                default:
                                    alert('Unknown Error.');
                                    break;
                            }
                        } else {
                            window.location.href = "<?php echo JUri::base()."question/list_".$this->course_id; ?>.html";
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                    }
                });

            });

        })(jQuery);
    </script>
<?php
endif;?>
