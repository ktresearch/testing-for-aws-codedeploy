<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
$itemid = JoomdleHelperContent::getMenuItem();

$linkstarget = $this->params->get( 'linkstarget' );
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$session  = JFactory::getSession();
$edit = ($this->action == 'edit') ? true : false;
if ($session->get('device') == 'mobile') {
    ?>
    <style type="text/css">
        .navbar-header {
            background-color: #126db6;
        }
    </style>
<?php
}
require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
?>
<?php if ($this->hasPermission[0]['hasPermission']) : ?>
    <div class="true-false-question">
        <form action="" class="formTF">
            <h1><?php echo Jtext::_('QUESTION_TRUE_FALSE'); ?></h1>
            <p><?php echo Jtext::_('QUESTION_TRUE_FALSE_TIP'); ?></p>
            <label><?php echo Jtext::_('QUESTION_TITLE'); ?></label>
            <textarea name="txtTFQuestion" class="txtTFQuestion" cols="30" rows="10" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_QUESTION_TEXT'); ?>"><?php echo (isset($this->question)) ? ( ($this->question['questiontext'] == '') ? $this->question['name'] : $this->question['questiontext']) : ''; ?></textarea>
            <label><?php echo Jtext::_('QUESTION_ANSWER'); ?></label>
            <?php
            if ($edit) {
                $options = json_decode($this->question['options']);
                $corectAnswer = (intval($options->answers->{$options->trueanswer}->fraction) == 1) ? 1 : 0;
            }
            ?>
            <input type="hidden" name="hdTF" class="hdTF" value="<?php echo ($edit) ? $corectAnswer : '1';?>"/>
            <div class="btTF">
                <div class="true <?php echo ($edit) ? (($corectAnswer == 1) ? 'chosen' : '') : 'chosen';?>">
                    <div class="status"></div>
                    <span><?php echo Jtext::_('QUESTION_TRUE'); ?></span>
                </div>
                <div class="false <?php echo ($edit) ? (($corectAnswer == 0) ? 'chosen' : '') : '';?>">
                    <div class="status"></div>
                    <span><?php echo Jtext::_('QUESTION_FALSE'); ?></span>
                </div>
                <!-- <button class="true <?php echo ($edit) ? (($corectAnswer == 1) ? 'chosen' : '') : 'chosen';?>"><?php echo Jtext::_('QUESTION_TRUE'); ?></button>
                <button class="false <?php echo ($edit) ? (($corectAnswer == 0) ? 'chosen' : '') : '';?>"><?php echo Jtext::_('QUESTION_FALSE'); ?></button> -->
            </div>
<!--                <button class="btAddQuestions">Add Questions</button>-->
            <div class="clear"></div>
            <textarea name="txtaTFFeedback" class="txtaTFFeedback" cols="30" rows="10" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_FEEDBACK'); ?>"><?php echo (isset($this->question)) ? $this->question['generalfeedback'] : ''; ?></textarea>
            <div class="buttons">
                <button class="btCancel" type="button"><?php echo Jtext::_('QUESTION_CANCEL'); ?></button>
                <button class="btSave"><?php echo Jtext::_('QUESTION_SAVE'); ?></button>
            </div>
            <button class="btPreview" type="button"><?php echo Jtext::_('QUESTION_PREVIEW'); ?></button>
        </form>
    </div>
    <div class="notification"></div>
    <div class="divPreview">
        <p class="previewTitle"><?php echo JText::_('QUESTION_TRUE_FALSE'); ?></p>
        <p class="previewDescription"><?php echo JText::_('QUESTION_TRUE_FALSE_TIP'); ?></p>
        <div class="previewQuestions">
            <p class="questionTitle"><?php echo JText::_('COM_JOOMDLE_QUESTION').' 1'; ?></p>
            <p class="questionDescription"></p>
            <p class="questionAnswer"><?php echo JText::_('COM_JOOMDLE_ANSWER'); ?></p>
            <div class="previewTFButtons">
                <div class="previewTrue">
                    <div class="status"></div>
                    <span><?php echo Jtext::_('QUESTION_TRUE'); ?></span>
                </div>
                <div class="previewFalse">
                    <div class="status"></div>
                    <span><?php echo Jtext::_('QUESTION_FALSE'); ?></span>
                </div>
            </div>
        </div>
        <button class="previewSubmit" type="button" data-disabled="1"><?php echo JText::_('COM_JOOMDLE_SUBMIT'); ?></button>
        <button class="previewBack" type="button"><?php echo JText::_('COM_JOOMDLE_EXIT_PREVIEW'); ?></button>
    </div>
    <script>
        (function($) {
            $('.formTF').submit(function(e) {
                e.preventDefault();
            });
            $('.formTF .btCancel').click(function() {
                window.location.href = '<?php echo JUri::base()."question/list_".$this->course_id; ?>.html';
            });
            $('.formTF .btTF .true').click(function() {
                $('.formTF .btTF > div').removeClass('chosen');
                $(this).addClass('chosen');
                $('.hdTF').val('1');
            });
            $('.formTF .btTF .false').click(function() {
                $('.formTF .btTF > div').removeClass('chosen');
                $(this).addClass('chosen');
                $('.hdTF').val('0');
            });
            // $('.previewTFButtons > div').click(function() {
            //     $('.previewTFButtons > div').removeClass('active');
            //     $(this).addClass('active');
            // });

            $('.true-false-question .btPreview').click(function() {
                $('.divPreview').addClass('visible');
                if ($('.txtTFQuestion').val() == '') {
                    $('.divPreview .questionDescription').html('Empty Question Text');
                } else {
                    $('.divPreview .questionDescription').html($('.txtTFQuestion').val());
                }
                // $('.previewTFButtons > div').removeClass('active');
                // if ($('.hdTF').val() == 1) {
                //     $('.previewTFButtons > .previewTrue').addClass('active');
                // } else $('.previewTFButtons > .previewFalse').addClass('active');

                $('.true-false-question').hide();
            });

            $('.previewBack').click(function() {
                $('.divPreview').removeClass('visible');
                $('.true-false-question').fadeIn();
            });

            $('.formTF .btSave').click(function() {
                if ($('.txtTFQuestion').val() == '') {
                    alert('Empty Input Value.');
                    return;
                }

                var act = '<?php echo (isset($this->question)) ? "update" : "create" ; ?>';
                $.ajax({
                    url: window.location.href,
                    type: 'POST',
                    data: {
                        <?php echo (isset($this->question)) ? "qid: ".$this->qid."," : "" ; ?>
                        act: act,
                        name: $('.txtTFQuestion').val(),
                        courseid : <?php echo $this->course_id; ?>,
                        generalfeedback: $('.txtaTFFeedback').val(),
                        feedbacktrue: '',
                        feedbackfalse: '',
                        correctanswer : $('.hdTF').val()
                    },
                    beforeSend: function() {
                        $('body').addClass('overlay2');
                        $('.notification').html('Loading...').fadeIn();
                    },
                    success: function(data, textStatus, jqXHR) {
                        var res = JSON.parse(data);

                        $('body').removeClass('overlay2');
                        $('.notification').fadeOut();

                        if (res.error == 1) {
                            switch (res.comment) {
                                case 'nopermission':
                                    alert('You don\'t have permission to create question.');
                                    break;
                                default:
                                    alert('Unknown Error.');
                                    break;
                            }
                        } else {
                            console.log(res);
                            window.location.href = "<?php echo JUri::base()."question/list_".$this->course_id; ?>.html";
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                    }
                });

            });
        })(jQuery);
    </script>
    <?php
endif;?>
