<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
$itemid = JoomdleHelperContent::getMenuItem();

$linkstarget = $this->params->get( 'linkstarget' );
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$session  = JFactory::getSession();
$edit = ($this->action == 'edit') ? true : false;
if ($session->get('device') == 'mobile') {
    ?>
    <style type="text/css">
        .navbar-header {
            background-color: #126db6;
        }
    </style>
<?php
}
require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
?>
<?php if ($this->hasPermission[0]['hasPermission']) : ?>
    <?php
    $answer= '';
    $feedback = '';
    if ($edit) {
        $options = json_decode($this->question['options']);
        foreach($options->answers as $k => $v) {
            if ($v->fraction == '1.0000000') {
                $answer = $v->answer;
                $feedback = $v->feedback;
            }
        }
    }
    ?>
    <div class="text-response-question">
        <form action="" class="formTR">
            <h1><?php echo Jtext::_('QUESTION_TEXT_RESPONSE'); ?></h1>
            <label><?php echo Jtext::_('QUESTION_TITLE'); ?></label>
            <textarea name="txtaTRQuestion" class="txtaTRQuestion" cols="30" rows="10" placeholder="<?php echo JText::_('COM_JOOMDLE_ADD_QUESTION_TEXT'); ?>"><?php echo ($edit) ? ( ($this->question['questiontext'] == '') ? $this->question['name'] : $this->question['questiontext']) : ''; ?></textarea>
            <label><?php echo Jtext::_('QUESTION_ANSWER'); ?></label>
            <input type="text" name="txtTRAnswer" class="txtTRAnswer" placeholder="<?php echo Jtext::_('QUESTION_TYPE_YOUR_HERE'); ?>" value="<?php echo ($answer != '') ? $answer : '';?>"/>
            <label><?php echo Jtext::_('QUESTION_RESPONSE'); ?></label>
            <input type="text" name="txtTRResponse" class="txtTRResponse" placeholder="<?php echo Jtext::_('QUESTION_TYPE_YOUR_HERE'); ?>"  value="<?php echo ($feedback != '') ? $feedback : '';?>"/>

            <!--                <button class="btAddQuestions">Add Questions</button>-->
            <div class="clear"></div>
            <div class="buttons">
                <button class="btCancel" type="button"><?php echo Jtext::_('QUESTION_CANCEL'); ?></button>
                <button class="btSave"><?php echo Jtext::_('QUESTION_SAVE'); ?></button>
            </div>
            <button class="btPreview" type="button"><?php echo Jtext::_('QUESTION_PREVIEW'); ?></button>
        </form>

    </div>
    <div class="notification"></div>
    <div class="divPreview">
        <p class="previewTitle"><?php echo JText::_('QUESTION_TEXT_RESPONSE'); ?></p>
        <!-- <p class="previewDescription"><?php echo JText::_('QUESTION_MATCHING_TIP'); ?></p> -->
        <div class="previewQuestions">
            <p class="questionTitle"><?php echo JText::_('COM_JOOMDLE_QUESTION').' 1'; ?></p>
            <p class="questionDescription"></p>
            <p class="questionAnswer"><?php echo JText::_('COM_JOOMDLE_ANSWER'); ?></p>
            <textarea name="previewTextareaAnswer" class="previewTextareaAnswer" cols="30" rows="10" placeholder="<?php echo JText::_('COM_JOOMDLE_TYPE_YOUR_ANSWER_HERE'); ?>"></textarea>
        </div>
        <button class="previewSubmit" type="button" data-disabled="1"><?php echo JText::_('COM_JOOMDLE_SUBMIT'); ?></button>
        <button class="previewBack" type="button"><?php echo JText::_('COM_JOOMDLE_EXIT_PREVIEW'); ?></button>
    </div>
    <script>
        (function($) {
            $('.formTR').submit(function(e) {
                e.preventDefault();
            });
            $('.formTR .btCancel').click(function() {
                window.location.href = '<?php echo JUri::base()."question/list_".$this->course_id; ?>.html';
            });

            $('.text-response-question .btPreview').click(function() {
                $('.divPreview').addClass('visible');
                if ($('.txtaTRQuestion').val() == '') {
                    $('.divPreview .questionDescription').html('Empty Question Text');
                } else {
                    $('.divPreview .questionDescription').html($('.txtaTRQuestion').val());
                }

                $('.text-response-question').hide();
            });

            $('.previewBack').click(function() {
                $('.divPreview').removeClass('visible');
                $('.text-response-question').fadeIn();
            });

            $('.formTR .btSave').click(function() {
                if ($('.txtaTRQuestion').val() == '') {
                    alert('Empty Input Value.');
                    return;
                }
                var act = '<?php echo (isset($this->question)) ? "update" : "create" ; ?>';
                $.ajax({
                    url: window.location.href,
                    type: 'POST',
                    data: {
                        <?php echo (isset($this->question)) ? "qid: ".$this->qid."," : "" ; ?>
                        act: act,
                        name: $('.txtaTRQuestion').val(),
                        courseid : <?php echo $this->course_id; ?>,
                        noanswers: 1,
                        feedbacks : [$('.txtTRResponse').val()],
                        answers : [$('.txtTRAnswer').val()]
                    },
                    beforeSend: function() {
                        $('body').addClass('overlay2');
                        $('.notification').html('Loading...').fadeIn();
                    },
                    success: function(data, textStatus, jqXHR) {
                        var res = JSON.parse(data);

                        $('body').removeClass('overlay2');
                        $('.notification').fadeOut();

                        if (res.error == 1) {
                            switch (res.comment) {
                                case 'nopermission':
                                    alert('You don\'t have permission to create question.');
                                    break;
                                default:
                                    alert('Unknown Error.');
                                    break;
                            }
                        } else {
                            console.log(res);
                            window.location.href = "<?php echo JUri::base()."question/list_".$this->course_id; ?>.html";
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                    }
                });
            });
        })(jQuery);
    </script>
<?php

endif;?>
