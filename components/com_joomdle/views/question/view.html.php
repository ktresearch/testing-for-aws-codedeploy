<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewQuestion extends JViewLegacy {
    function display($tpl = null) {
        global $mainframe;

        $app = JFactory::getApplication();
        $pathway = $app->getPathWay();
        $menus = $app->getMenu();
        $menu = $menus->getActive();

        $params = $app->getParams();
        $this->assignRef('params', $params);
        $user = JFactory::getUser();
        $username = $user->username;
        $document = JFactory::getDocument();

        $id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
        if (!$id) $id = JRequest::getVar( 'course_id' );
        if (!$id) $id = $params->get( 'course_id' );
        if (!$id) {
            echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
            return;
        }
        $id = (int) $id;

        $this->hasPermission = JFactory::hasPermission($id, $username);
        if (!$this->hasPermission[0]['hasPermission']) {
            echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
            return;
        }

        $this->course_id = $id;
        $this->course_info = JoomdleHelperContent::getCourseInfo($id, $username);
        $this->course_status = $this->course_info['course_status'];

        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
        $document->setTitle(JText::_('QUESTION_TITLE'));

        $qid =  JRequest::getVar( 'qid', null, 'NEWURLFORM' );
        if (!$qid) $qid =  JRequest::getVar( 'qid' );

        $action = JRequest::getVar( 'action', null, 'NEWURLFORM' );
        if (!$action) $action = JRequest::getVar( 'action' );
        if ($action == 'create' || $action == 'edit' || $action == 'remove') {
            if ($this->course_status == 'pending_approval' || $this->course_status == 'approved') {
                echo JText::_('COM_JOOMDLE_COURSE_APPROVED_OR_PENDING_APPROVAL');
                return;
            }
            if ($this->course_info['self_enrolment']) {
                echo JText::_('COM_JOOMDLE_COURSE_PUBLISHED');
                return;
            }
        }
        $this->action = $action;

        $edit = false;
        if (!is_null($qid)) {
            $edit = true;
            $this->qid = $qid;
            $this->question = JoomdleHelperContent::call_method('quiz_get_question', intval($qid));
        }

        if ($action == 'list') {
//            $this->questions = JoomdleHelperContent::call_method('load_questions_bank', $this->course_id);
            if (isset($_POST['act']) && $_POST['act'] == 'remove') {
                $act = 3;
                $q = array();
                $q['qid'] = $_POST['qid'];

                $r = JoomdleHelperContent::call_method('create_question', $act, $id, $username, $q );

                $result = array();
                $result['error'] = $r['error'];
                $result['comment'] = $r['mes'];
                $result['data'] = $r['data'];
                echo json_encode($result);
                die;
            } else if (isset($_POST['act']) && $_POST['act'] == 'removelist') {
                $act = 3;
                $qids = $_POST['qid'];

                $r = JoomdleHelperContent::call_method('delete_questions', $act, $id, $username, $qids );

                $result = array();
                $result['error'] = $r['status'];
                $result['comment'] = $r['message'];
                echo json_encode($result);
                die;
            }

            $question = JoomdleHelperContent::call_method('load_questions_bank', $this->course_id);
            if ($question['status']) {
                $this->questions = $question['questions'];
            } else {
                $this->questions = array();
            }
        } else if ($action == 'remove') {
            if (!is_null($qid)) {
                $this->action = 'remove';
                $this->wrapperurl = JUri::base()."courses/question/edit.php?courseid=$this->course_id&deleteselected=$qid&q$qid=1";
                if (isset($_POST['act']) && $_POST['act'] == 'remove') {
                    $act = 3;
                    $q = array();
                    $q['qid'] = $qid;

                    $r = JoomdleHelperContent::call_method('create_question', $act, $id, $username, $q );

                    $result = array();
                    $result['error'] = $r['error'];
                    $result['comment'] = $r['mes'];
                    $result['data'] = $r['data'];
                    echo json_encode($result);
                    die;
                }
            }
        } else {
            $questionType = JRequest::getVar('qtype', null, 'NEWURLFORM');
            if (!$questionType) $questionType = JRequest::getVar('qtype');

            if ($questionType) {
                switch ($questionType) {
                    case '1':
                    case 'truefalse':
                        if ($edit) {
                            $this->wrapperurl = JUri::base()."courses/question/question.php?courseid=$this->course_id&id=$this->qid";
                        } else
                            $this->wrapperurl = JUri::base()."courses/question/question.php?courseid=$this->course_id&qtype=truefalse&scrollpos=0";

                        if (isset($_POST['act']) && ($_POST['act'] == 'create' || $_POST['act'] == 'update')) {
                            $array = array();
                            if ($_POST['act'] == 'update') {
                                $act = 2;
                            } else if ($_POST['act'] == 'create') {
                                $act = 1;
                            }
                            $q = array(
                                'type' => 'truefalse',
                                'name' => strip_tags($_POST['name']),
                                'questiontext' => strip_tags($_POST['name']),
                                'generalfeedback' => strip_tags($_POST['generalfeedback']),
                                'feedbacktrue' => strip_tags($_POST['feedbacktrue']),
                                'feedbackfalse' => strip_tags($_POST['feedbackfalse']),
                                'correctanswer' => (int)$_POST['correctanswer'],
                            );

                            if ($_POST['act'] == 'update') $q['qid'] = $qid;

                            $r = JoomdleHelperContent::call_method('create_question', $act, $id, $username, $q );

                            $result = array();
                            $result['error'] = $r['error'];
                            $result['comment'] = $r['mes'];
                            $result['data'] = $r['data'];
                            echo json_encode($result);
                            die;
                        }
                        $this->questionType = $questionType;
                        $document->setTitle(JText::_('QUESTION_TRUE_FALSE'));
                        parent::display('tf');
                        return;
                        break;
                    case '2':
                    case 'multichoice':
                        if ($edit) {
                            $this->wrapperurl = JUri::base()."courses/question/question.php?courseid=$this->course_id&id=$this->qid";
                        } else
                            $this->wrapperurl = JUri::base()."courses/question/question.php?courseid=$this->course_id&qtype=multichoice&scrollpos=0&fromjl=1";

                        if (isset($_POST['act']) && ($_POST['act'] == 'create' || $_POST['act'] == 'update')) {
                            $array = array();
                            if ($_POST['act'] == 'update') {
                                $act = 2;
                            } else if ($_POST['act'] == 'create') {
                                $act = 1;
                            }
                            $q = array(
                                'type' => 'multichoice',
                                'name' => strip_tags($_POST['name']),
                                'questiontext' => strip_tags($_POST['name']),
                                'generalfeedback' => strip_tags($_POST['generalfeedback']),
                                'feedbacks' => json_encode($_POST['feedbacks']),
                                'answers' => json_encode($_POST['answers']),
                                'fraction' => json_encode($_POST['fraction']),
                                'norightanswer' => (int)$_POST['norightanswer']
                            );
                            if ($_POST['act'] == 'update') $q['qid'] = $qid;

                            $r = JoomdleHelperContent::call_method('create_question', $act, $id, $username, $q );

                            $result = array();
                            $result['error'] = $r['error'];
                            $result['comment'] = $r['mes'];
                            $result['data'] = $r['data'];
                            echo json_encode($result);
                            die;
                        }

                        $this->questionType = $questionType;
                        $document->setTitle(JText::_('QUESTION_MULTICHOICE'));
                        parent::display('mc');
                        return;
                        break;
                    case '3':
                    case 'match':
                        if ($edit) {
                            $this->wrapperurl = JUri::base()."courses/question/question.php?courseid=$this->course_id&id=$this->qid";
                        } else
                            $this->wrapperurl = JUri::base()."courses/question/question.php?courseid=$this->course_id&qtype=match&scrollpos=0&fromjl=1";

                        if (isset($_POST['act']) && ($_POST['act'] == 'create' || $_POST['act'] == 'update')) {
                            $array = array();
                            if ($_POST['act'] == 'update') {
                                $act = 2;
                            } else if ($_POST['act'] == 'create') {
                                $act = 1;
                            }
                            $q = array(
                                'type' => 'match',
                                'name' => strip_tags($_POST['name']),
                                'questiontext' => strip_tags($_POST['name']),
                                'generalfeedback' => strip_tags($_POST['generalfeedback']),
                                'subquestions' => json_encode($_POST['subquestions']),
                                'subanswers' => json_encode($_POST['subanswers']),
                                'noanswers' => (int)$_POST['noanswers']
                            );
                            if ($_POST['act'] == 'update') $q['qid'] = $qid;

                            $r = JoomdleHelperContent::call_method('create_question', $act, $id, $username, $q );

                            $result = array();
                            $result['error'] = $r['error'];
                            $result['comment'] = $r['mes'];
                            $result['data'] = $r['data'];
                            echo json_encode($result);
                            die;
                        }

                        $this->questionType = $questionType;
                        $document->setTitle(JText::_('QUESTION_MATCHING'));
                        parent::display('matching');
                        return;
                        break;
                    case '4':
                    case 'shortanswer':
                        if ($edit) {
                            $this->wrapperurl = JUri::base()."courses/question/question.php?courseid=$this->course_id&id=$this->qid";
                        } else
                            $this->wrapperurl = JUri::base()."courses/question/question.php?courseid=$this->course_id&qtype=shortanswer&scrollpos=0";

                        if (isset($_POST['act']) && ($_POST['act'] == 'create' || $_POST['act'] == 'update')) {
                            $array = array();
                            if ($_POST['act'] == 'update') {
                                $act = 2;
                            } else if ($_POST['act'] == 'create') {
                                $act = 1;
                            }
                            $q = array(
                                'type' => 'shortanswer',
                                'name' => strip_tags($_POST['name']),
                                'questiontext' => strip_tags($_POST['name']),
                                'generalfeedback' => '',
                                'feedbacks' => json_encode($_POST['feedbacks']),
                                'answers' => json_encode($_POST['answers']),
//                                'noanswers' => (int)$_POST['noanswers']
                                'noanswers' => 1
                            );
                            if ($_POST['act'] == 'update') $q['qid'] = $qid;

                            $r = JoomdleHelperContent::call_method('create_question', $act, $id, $username, $q );

                            $result = array();
                            $result['error'] = $r['error'];
                            $result['comment'] = $r['mes'];
                            $result['data'] = $r['data'];
                            echo json_encode($result);
                            die;
                        }
                        $this->questionType = $questionType;
                        $document->setTitle(JText::_('QUESTION_TEXT_RESPONSE'));
                        parent::display('tr');
                        return;
                        break;
                    default:
                        break;
                }
            }
        }

        parent::display($tpl);
    }
}
?>
