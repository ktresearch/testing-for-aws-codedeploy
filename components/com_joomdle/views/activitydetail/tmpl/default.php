<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('_JEXEC') or die('Restricted access');
$username = JFactory::getUser()->username;

require_once(JPATH_SITE . '/components/com_community/libraries/core.php');
require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);

$render_banner = $banner->renderBanner($this->course_info, $this->mods['sections'], $this->hasPermission);

$icon_url = JoomdleHelperSystem::get_icon_url($this->mod, '');
$grade_letter_assign = array();
$grade_letter_assign[0]['name'] = 'A';
$grade_letter_assign[0]['value'] = 93;

$grade_letter_assign[1]['name'] = 'B';
$grade_letter_assign[1]['value'] = 83;

$grade_letter_assign[2]['name'] = 'C';
$grade_letter_assign[2]['value'] = 73;

$grade_letter_assign[3]['name'] = 'D';
$grade_letter_assign[3]['value'] = 60;

$learner_comp = array();
$learner_notcomp = array();
if (is_array($this->students)) {
    foreach ($this->students[0]['student'] as $student) {
        if ($student['username'] == $username) continue;
        $user_by_username = JFactory::getUser($student['username']);
        
        if (!$user_by_username->username || $user_by_username->username == $username) continue;

        // Check user completed mod
        if (!$student['completed']) {
            $learner_notcomp[] = $student;
        } else {
            $learner_comp[] = $student;
        }
    }
    $hasgrade = $this->students[0]['hasgrade'];
    $topicname = $this->students[0]['topicname'];
}
// get device
$session = JFactory::getSession();
$device = $session->get('device');
if ($device == 'mobile') {
    $cols = 10;
    $rows = 15;
} else {
    $cols = 10;
    $rows = 10;
}

?>
<style type="text/css">
    .gradeActivitySelect .ulGradeSelected li{
        z-index: 1;
        background: #fff;
    }
    .gradeActivitySelect .ulGradeSelected li p {
        margin: 0 10px;
        border-bottom: 1px solid rgba(53, 137, 161, 0.1);
        padding: 3px 0;
    }
    .gradeActivitySelect .ulGradeSelected li:hover{
        background-color: rgb(35, 154, 253) !important;
        color: #fff;
    }

    .gradeActivitySelect .ulGradeSelected li:last-of-type p{
        border-bottom: none;
    }

    .view-activitydetail .joomdle-overview-content .feedback-item .txtFeedback {
        max-height: 345px !important;
    }

    @media screen and (max-width: 768px) {
      .view-activitydetail .joomdle-overview-content .feedback-item .txtFeedback {
        max-height: 227px !important;
      }
      .list-notcompleted { margin-bottom: 50px; }
      .view-activitydetail .joomdle-overview-content .joomdle-list .list-user-grade .mod_div1 .mod_line1 .name {
        width: 100% !important;
      }
      .abc { height: 100px; }
      textarea {
        padding: 2px 12px !important;
      }
    }
</style>
<script src="<?php echo JURI::base(); ?>components/com_community/assets/autosize.min.js"></script>
<div class="joomdle-overview-header">
    <?php
    require_once(JPATH_SITE . '/components/com_joomdle/views/header_facilitator.php');
    ?>
</div>
<div class="joomdle-course joomdle-overview-content <?php echo $this->pageclass_sfx ?>">
    <div class="message-title">
        <?php 
            echo $topicname;
        ?>
    </div>
    <div class="title-mod">
        <div class="title_mod">
            <div><img align="center" src="<?php echo $icon_url; ?>"></div>
            <div><span class="title-course"><?php echo ucfirst($this->students[0]['name']); ?></span></div>
    </div>
        <?php if (!$hasgrade && $this->mod == 'quiz') { ?>
        <div class="note_mod">
            <?php echo JText::_('COM_JOOMDLE_ACTIVITY_NOTE');?>
        </div>
        <?php } ?>
    </div>
    
    <div class="list-completed">
        <div class="title_list">
            <div class="title_text">
                <span class="title_txt"><?php echo JText::_('COM_JOOMDLE_COMPLETED');?></span>&nbsp;
                <span class="title_num"><?php echo count($learner_comp); ?></span>
            </div>
    <?php
    if ($this->mod == 'assign' && count($learner_comp) > 0) { ?>
        <div class="submission-download">
            <a href="<?php echo JRoute::_('index.php?option=com_joomdle&task=downloadSubmission&assignid=' . $this->activities . '&course_id=' . $this->courseid); ?>">
                <img class="btn-download-csv" src="/images/icons/download_icon.png"/>
                <?php echo JText::_('COM_JOOMDLE_BUTTON_ASSIGNMENTS'); ?>
            </a>
        </div><br/>
        <!--<a href="<?php // echo JRoute::_('index.php?option=com_joomdle&task=download&id='.$this->activities); ?>">test</a>-->
    <?php }
        if (count($learner_comp) > 0) {
    ?>
            <div class="title_icons <?php if ($this->mod == 'assign') echo 'icon_assign';?>">
            <span class="icon_dropdown"></span>
            <span class="icon_uparrow"></span>
        </div>
        <?php } ?>
        </div>
            
        <ul class="joomdle-list learner_completed <?php if ($this->mod == 'quiz' || $this->mod == 'assign') echo 'setgrade';?>">
        <?php
        // begin render student in course
            $i = 0;
            if (count($learner_comp)) {
//            $g = array('93'=>'A', '90'=>'A-', '87'=>'B+', '83'=>'B', '80'=>'B-', '77'=>'C+', '73'=>'C', '70'=>'C-', '67'=>'D+', '60'=>'D', '0'=>'F');
                foreach ($learner_comp as $student) {
                    if ($student['username'] == $username) continue;
                    $user_by_username = JFactory::getUser($student['username']);
                    $users = CFactory::getUser($user_by_username->id);
                    if (!$user_by_username->username || $user_by_username->username == $username) continue;
                    
                    if ($this->mod == 'assign') {
                        $assign_info = JoomdleHelperContent::call_method ( 'get_assignment_activity_detail', (int)$this->courseid, $users->username, $this->students[0]['id']);
                    }
                    
                    // get avatar
                    $avatar = $users->getAvatar();

                    $final_grade = $student['finalgrade'];
                    if ($student['grademax'] == 100) 
                        $finalGrade = (int)$final_grade;
                    else if ($student['grademax'] == 10) 
                        $finalGrade = (int)($final_grade * 10);

                    if ($student['gradeid'] > 0 && $student['finalgradeletter'] && $this->mod != 'assign') {
                        $finalgradeletters = strtok($student['finalgradeletter'], " ");
                    } elseif ($student['gradeid'] > 0 && $student['finalgradeletter']  && $this->mod == 'assign' && $student['finalgrade'] > 0) {
                        foreach ($grade_letter_assign as $grade_assign) {
                            if ($finalGrade >= $grade_assign['value']) {
                                $finalgradeletters = $grade_assign['name'];
                                break;
                            }
                        }
                    } else {
                        $finalgradeletters = JText::_('PROGRESS_UNGRADED');
                    }

                    if($this->mod == 'quiz'){
                        $this->quizes = JoomdleHelperContent::call_method ( 'get_course_quizes', $this->courseid, $users->username);
                            
                        foreach ($this->quizes as $key => $value) {
                            foreach ($value['quizes'] as $k => $v) {
                                if ($v['id'] != $this->activities) continue; else $this->quiz = $v;
                            }
                        }
                        $attempt_quiz = $this->quiz['attempt'];
                    }

                    if (!$this->students[0]['hasgrade']) {
                        $cl = 'hidden';
                        $cldisable = 'disable';
                    } else {
                        $cl = '';
                        $cldisable = '';
                    }
                ?>
                    <li class="list-user-grade">
                        <div class="avatar-overall"><a class="nav-avatar"><img alt="<?php echo $users->name; ?>" src="<?php echo $avatar; ?>"/></a></div>
                        <?php if ($device == 'mobile') { ?>
                            <div class="mod_div1">
                                <div class="mod_line1">
                                    <div class="name"><?php echo $users->name; ?></div>
                                    <?php if($this->mod == 'quiz' && $student['timecompleted']) { ?>
                                        <?php if($student['completed']) { ?>
                                        <span class="attach-result" datalink="<?php echo JUri::base().'quiz/'.(int)$this->courseid.'_'.$this->activities.'_endquiz_'.$attempt_quiz.'.html'; ?>">
                                            <image src="images/Assessmenticon.png" alt="icon" />
                                        </span>
                                    <?php } 
                                    } elseif($this->mod == 'assign' && $assign_info['grade_status']!='Not Yet Graded') {
                                    ?>
                                        <a class="result-asign" href="<?php echo JRoute::_('index.php?option=com_joomdle&task=downloadSubmissionUser&userid=' . $users->username . '&course_id=' . $this->courseid. '&assignid=' . $this->activities); ?>">
                                             <span class="attach-result-asign" > <image style="height:18px;" src="images/downloadassi.png" alt="icon" /></span>
                                         </a>
                                    <?php }?>
                                </div>
                                <?php if ($this->mod == 'quiz' || $this->mod == 'assign') { ?>
                                    <div class="mod_line2">
                                        <span class="time-completed-activity"><?php echo $student['timecompleted'] != 0 ? date('m/d/Y', $student['timecompleted']) : ''; ?></span>
                                    </div>
                                <?php } ?>
                            </div>

                            <?php if ($this->mod == 'quiz' || $this->mod == 'assign') { ?>
                                <div class="mod_div2">
                                    <div class="mod_gt"><div class="grade-title <?php echo $cldisable;?>"><?php echo JText::_('PROGRESS_GRADE'); ?></div></div>
                                    <div class="select-grade-student">
                                        <?php
                                        if ($student['gradeitemid'] && $this->mod == 'quiz') {
                                            $k = 0;
                                            $finalGrade = (float)$student['finalgrade'] * 10;
                                            $grade_letters_ass = $this->students[0]['lettergradeoption'];
                                            // Get grade letter of assessment
                                            if ($student['finalgradeletter'] != "Ungraded") {
                                                $finalgradeletters_ass = $student['finalgradeletter'];                                             
                                            }
                                            if($student['finalgradeletters'] == "Ungraded") {
                                                $finalGrade = "";
                                            }

                                        ?>
                                            <div class="gradeActivitySelect">
                                                <div class="gradeSelected <?php echo $cldisable;?>">

                                                    <?php
                                                    if($student['hidden'] == 0){
                                                        if($student['completed']){
                                                            if ($student['finalgradeletter']) {
                                                                echo $finalgradeletters_ass;
                                                                } 
                                                            else if ($student['finalgradeletter'] == 0 && $hasgrade) echo 'D';
                                                            else {
                                                                echo '-';
                                                            } 
                                                        }
                                                        else  echo JText::_('COM_JOOMDLE_OPTION');
                                                     }
                                                    else echo '-';
                                                    ?>
                                                </div>
                                                <ul class="ulGradeSelected <?php echo $cl;?>" style="display: none;">
                                                    <?php
                                                    //check grade numeric or letter
                                                    foreach ($grade_letters_ass as $grade_ass) {
                                                        ?>
                                                        <li value="<?php echo $grade_ass['value']; ?>" <?php if ($finalgradeletters_ass == $grade_ass['name'] && $student['gradeid'] != 0) { ?> class="selected" <?php } ?>>
                                                            <p><?php echo $grade_ass['name']; ?></p>
                                                        </li>
                                                        <?php
                                                    }
                                                    ?>
                                                        <li value="-1" <?php if ($student['gradeid'] == 0) { ?> class="selected" <?php } ?>>
                                                            <p><?php echo '-'; ?></p>
                                                        </li>
                                                    <input type="hidden" name="gradeitem[]" class="gradeitem" value="<?php echo $student['gradeitemid']; ?>"/>
                                                    <input type="hidden" name="gradeid[]" class="gradeid" value="<?php echo $student['gradeid']; ?>"/>
                                                    <input type="hidden" name="username" class="username" value="<?php echo $users->username; ?>"/>
                                                    <input type="hidden" class="grade-option" name="grade_value[]" value="<?php if($student['hidden']==0) echo $finalGrade; else echo '-1'; ?>"/>
                                                    <input type="hidden" class="status-option" name="status[]" value=""/>
                                                </ul>

                                            </div>
                                        <?php } ?>

                                        <?php
                                        if ($student['gradeitemid'] && $this->mod == 'assign') {
                                            if ($student['grademax'] == 100) {
                                                $finalGrade = $student['finalgrade'];
                                            } else if ($student['grademax'] == 10) {
                                                $finalGrade = $student['finalgrade'] * 10;
                                            }

                                            if($student['finalgradeletter'] == "Ungraded"){
                                                $finalGrade = "";
                                            }

                                            if ($student['finalgradeletter'] != "Ungraded") {
                                                foreach ($grade_letter_assign as $grade_assign) {
                                                    if ($finalGrade >= $grade_assign['value']) {
                                                        $finalgradeletters_assign = $grade_assign['name'];
                                                        break;
                                                    }
                                                }
                                            }
                                        ?>

                                            <div class="gradeActivitySelect">
                                                <div class="gradeSelected">
                                                   <?php 
                                                    if($student['hidden'] == 0){
                                                        if ($student['finalgradeletter'] != "Ungraded" && $student['finalgrade'] > 0) {

                                                            echo $finalgradeletters_assign;
                                                        } else {
                                                            echo JText::_('COM_JOOMDLE_OPTION');
                                                        }
                                                    }
                                                    else echo '-';
                                                    ?>
                                                </div>
                                                <ul class="ulGradeSelected" style="display: none;">
                                                    <?php
                                                        //check grade numeric or letter
                                                        foreach ($grade_letter_assign as $grade_assign) {
                                                    ?>
                                                        <li value="<?php echo $grade_assign['value']; ?>" <?php if ($act['finalgrade'] == $grade_assign['value'] && $act['gradeid'] != 0) { ?> class="selected" <?php } ?>>
                                                            <p><?php echo $grade_assign['name']; ?></p>
                                                        </li>
                                                    <?php } ?>
                                                    <li value="-1" <?php if ($student['gradeid'] == 0) { ?> class="selected" <?php } ?>>
                                                        <p><?php echo '-'; ?></p>
                                                    </li>
                                                    <input type="hidden" name="gradeitem[]" class="gradeitem" value="<?php echo $student['gradeitemid']; ?>"/>
                                                    <input type="hidden" name="gradeid[]" class="gradeid" value="<?php echo $student['gradeid']; ?>"/>
                                                    <input type="hidden" name="username" class="username" value="<?php echo $users->username; ?>"/>
                                                    <input type="hidden" class="grade-option" name="grade_value[]" value="<?php if($student['hidden'] == 0) echo $finalGrade; else echo '-1'; ?>"/>
                                                    <input type="hidden" class="status-option" name="status[]" value=""/>
                                                </ul>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <button class="btGiveFeedback" onclick="giveFeedback(<?php echo $i;?>);"> <?php echo JText::_('COM_JOOMDLE_GIVE_FEEDBACK'); ?></button>
                                    <div class="feedback-item feedbackitem_<?php echo $i;?>">
                                        <div class="feedback_title">
                                            <img src="./images/PreviousIcon.png" onclick="popupFeedbackMobi(<?php echo $i;?>);"/>
                                            <span><?php echo JText::_('COM_JOOMDLE_FEEDBACK');?></span>
                                        </div>
                                        <textarea class="txtFeedback autosize txtFeed_<?php echo $i;?>"
                                              name="feedback<?php if ($student['gradeitemid']) { ?>grade<?php } ?>[]"
                                              rows="2"
                                              cols="<?php echo $cols; ?>" placeholder="<?php echo JText::_('COM_JOOMDLE_FEEDBACK_PLACE');?>"><?php echo $student['feedback']; ?></textarea>
                                        <div class="buttons buttons_feedback buttoni_<?php echo $i;?>">
                                            <input type="hidden" class="textarea-value-<?php echo $i;?>" value="<?php echo $student['feedback']; ?>">
                                            <input type="hidden" name="activite[]" value="<?php echo $this->activities; ?>"/>
                                            <input type="hidden" name="username" value="<?php echo $users->username; ?>"/>
                                            <input type="hidden" name="gradeid[]" value="<?php echo $student['gradeid']; ?>"/>
                                            <input type="hidden" name="gradeitem[]" value="<?php echo $student['gradeitemid']; ?>"/>
                                            <input type="hidden" name="courseid" value="<?php echo $this->courseid; ?>"/>
                                            <input type="hidden" name="mod[]" value="<?php echo $this->mod; ?>"/>
                                            <input type="hidden" name="type" value="activity_grade"/>
                                            <button class="btCancel" onclick="popupFeedbackMobi(<?php echo $i;?>);"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                                            <button class="btSave" onclick="saveFeedback(<?php echo $i.','.$student['gradeid'].','. $student['gradeitemid']; ?>,'<?php echo $users->username; ?>');"><?php echo JText::_('COM_JOOMDLE_SAVE'); ?></button>
                                        </div>
                                    </div>
                                    
                                    <div class="cancelFeedback popup_<?php echo $i;?>">
                                        <p class="txt_title"><?php echo JText::_('COM_JOOMDLE_GIVE_FEEDBACK_SAVE1'); ?></p>
                                        <p><?php echo JText::_('COM_JOOMDLE_GIVE_FEEDBACK_SAVE2'); ?></p>
                                        <div class="buttons">
                                            <button class="btCancel" onclick="cancelPopup(<?php echo $i;?>);"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                                            <button class="btProceed" onclick="proceedPopup(<?php echo $i;?>);"><?php echo JText::_('COM_JOOMDLE_BUTTON_PROCEED'); ?></button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="user-grade">
                                <div class="name"><?php echo $users->name; ?></div>
                            </div>

                            <?php if ($this->mod == 'quiz' || $this->mod == 'assign') { ?>
                                <div class="mod-grade">
                                    <div class="grade-title <?php echo $cldisable;?>"><?php echo JText::_('PROGRESS_GRADE'); ?></div>
                                    <div class="select-grade-student">
                                        <?php
                                        if ($student['gradeitemid'] && $this->mod == 'quiz') {
                                            $k = 0;
                                            $finalGrade = (float)$student['finalgrade'] * 10;
                                            $grade_letters_ass = $this->students[0]['lettergradeoption'];
                                            // Get grade letter of assessment
                                            if ($student['finalgradeletter'] != "Ungraded") {
                                                $finalgradeletters_ass = $student['finalgradeletter'];            
                                            }
                                            if($student['finalgradeletter'] == "Ungraded") {
                                                $finalGrade = "";
                                            }
                                        ?>

                                            <div class="gradeActivitySelect">
                                                <div class="gradeSelected <?php echo $cldisable;?>">

                                                    <?php
                                                    if($student['hidden'] == 0){
                                                        if($student['completed']){
                                                            if ($student['finalgradeletter']) {
                                                                echo $finalgradeletters_ass;
                                                            } 
                                                            else if ($student['finalgradeletter'] == 0 && $hasgrade) echo 'D';
                                                            else {
                                                                echo '-';
                                                            } 
                                                        }
                                                        else  echo JText::_('COM_JOOMDLE_OPTION');
                                                     }
                                                    else echo '-';
                                                    ?>
                                                </div>
                                                <ul class="ulGradeSelected <?php echo $cl;?>" style="display: none;">
                                                    <?php
                                                    //check grade numeric or letter
                                                    foreach ($grade_letters_ass as $grade_ass) {
                                                        ?>
                                                        <li value="<?php echo $grade_ass['value']; ?>" <?php if ($finalgradeletters_ass == $grade_ass['name'] && $student['gradeid'] != 0) { ?> class="selected" <?php } ?>>
                                                            <p><?php echo $grade_ass['name']; ?></p>
                                                        </li>
                                                    <?php } ?>
                                                        <li value="-1" <?php if ($student['gradeid'] == 0) { ?> class="selected" <?php } ?>>
                                                            <p><?php echo '-'; ?></p>
                                                        </li>
                                                    <input type="hidden" name="gradeitem[]" class="gradeitem" value="<?php echo $student['gradeitemid']; ?>"/>
                                                    <input type="hidden" name="gradeid[]" class="gradeid" value="<?php echo $student['gradeid']; ?>"/>
                                                    <input type="hidden" name="username" class="username" value="<?php echo $users->username; ?>"/>
                                                    <input type="hidden" class="grade-option" name="grade_value[]" value="<?php if($student['hidden']==0) echo $finalGrade; else echo '-1'; ?>"/>
                                                    <input type="hidden" class="status-option" name="status[]" value=""/>
                                                </ul>
                                            </div>
                                        <?php } ?>

                                        <?php
                                        if ($student['gradeitemid'] && $this->mod == 'assign') {
                                            if ($student['grademax'] == 100) $finalGrade = $student['finalgrade'];
                                            else if ($student['grademax'] == 10) $finalGrade = $student['finalgrade'] * 10;

                                            if($student['finalgradeletter'] == "Ungraded"){
                                                $finalGrade = "";
                                            }

                                            if ($student['finalgradeletter'] != "Ungraded") {
                                                foreach ($grade_letter_assign as $grade_assign) {
                                                    if ($finalGrade >= $grade_assign['value']) {
                                                        $finalgradeletters_assign = $grade_assign['name'];
                                                        break;
                                                    }
                                                }
                                            }
                                        ?>
                                            <div class="gradeActivitySelect">
                                                <div class="gradeSelected">
                                                    
                                                    <?php 
                                                    if($student['hidden'] == 0){
                                                        if ($student['finalgradeletter'] != "Ungraded" && $student['finalgrade'] > 0) {

                                                            echo $finalgradeletters_assign;
                                                        } else {
                                                            echo JText::_('COM_JOOMDLE_OPTION');
                                                        }
                                                    }
                                                    else echo '-';
                                                    ?>
                                                </div>
                                                <ul class="ulGradeSelected" style="display: none;">
                                                    <?php
                                                    //check grade numeric or letter
                                                    foreach ($grade_letter_assign as $grade_assign) {
                                                        ?>
                                                        <li value="<?php echo $grade_assign['value']; ?>" <?php if ($student['finalgrade'] == $grade_assign['value'] && $student['gradeid'] != 0) { ?> class="selected" <?php } ?>>
                                                            <p><?php echo $grade_assign['name']; ?></p>
                                                        </li>
                                                        <?php
                                                    }
                                                    ?>
                                                        <li value="-1" <?php if ($student['gradeid'] == 0) { ?> class="selected" <?php } ?>>
                                                            <p><?php echo '-'; ?></p>
                                                        </li>
                                                    <input type="hidden" class="grade-i" name="grade_item" value="<?php echo $i; ?>"/>
                                                    <input type="hidden" name="username" class="username" value="<?php echo $users->username; ?>"/>
                                                    <input type="hidden" name="gradeitem[]" class="gradeitem" value="<?php echo $student['gradeitemid']; ?>"/>
                                                    <input type="hidden" name="gradeid[]" class="gradeid" value="<?php echo $student['gradeid']; ?>"/>
                                                    <input type="hidden" class="grade-option" name="grade_value[]" value="<?php if($student['hidden']==0) echo $finalGrade; else echo '-1'; ?>"/>

                                                    <?php 
                                                        $status_value = "";
                                                        if( $assign_info['grade_status'] ==='Not Yet Graded') {
                                                            $status_value = "not yet started";
                                                        } elseif ( $assign_info['grade_status'] === 'In Progress') {
                                                            $status_value = "completed";
                                                        } elseif( $assign_info['grade_status'] =='Completed') {
                                                            $status_value = "completed";
                                                        } 
                                                    ?>
                                                    <input type="hidden" class="status-option" name="status[]" value="<?php $status_value ?>"/>
                                                </ul>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php if($this->mod == 'quiz' && $student['timecompleted']) { ?>
                                        <?php if($student['completed']) { ?>
                                        <span class="attach-result" datalink="<?php echo JUri::base().'quiz/'.(int)$this->courseid.'_'.$this->activities.'_endquiz_'.$attempt_quiz.'.html'; ?>">
                                            <image src="images/Assessmenticon.png" alt="icon" />
                                        </span>
                                    <?php } 
                                    } elseif($this->mod == 'assign' && $assign_info['grade_status']!='Not Yet Graded') {
                                    ?>
                                        <a class="result-asign" href="<?php echo JRoute::_('index.php?option=com_joomdle&task=downloadSubmissionUser&userid=' . $users->username . '&course_id=' . $this->courseid. '&assignid=' . $this->activities); ?>">
                                             <span class="attach-result-asign" > <image style="height:18px;" src="images/downloadassi.png" alt="icon" /></span>
                                         </a>
                                    <?php }?>
                                    <!--<span class="attach-result" datalink="<?php // echo JUri::base().'quiz/'.(int)$this->courseid.'_'.$student['id'].'_endquiz_'.$attempt_quiz.'.html'; ?>"><image src="images/Assessmenticon.png" alt="icon" /></span>-->
                                    <span class="time-completed-activity"><?php echo $student['timecompleted'] != 0 ? date('m/d/Y', $student['timecompleted']) : ''; ?></span>
                                </div>
                                <div class="feedback-item">
                                    <span><?php echo JText::_('COM_JOOMDLE_FEEDBACK');?></span>
                                    <textarea class="txtFeedback autosize txtFeed_<?php echo $i;?>"
                                          name="feedback<?php if ($student['gradeitemid']) { ?>grade<?php } ?>[]"
                                          rows="2"
                                          cols="<?php echo $cols; ?>" placeholder="<?php echo JText::_('COM_JOOMDLE_FEEDBACK_PLACE');?>"><?php echo $student['feedback']; ?></textarea>
                                    <div class="buttons buttons_feedback buttoni_<?php echo $i;?>">
                                        <input type="hidden" name="activite[]" value="<?php echo $this->activities; ?>"/>
                                        <input type="hidden" name="username" value="<?php echo $users->username; ?>"/>
                                        <input type="hidden" name="gradeid[]" value="<?php echo $student['gradeid']; ?>"/>
                                        <input type="hidden" name="gradeitem[]" value="<?php echo $student['gradeitemid']; ?>"/>
                                        <input type="hidden" name="courseid" value="<?php echo $this->courseid; ?>"/>
                                        <input type="hidden" name="mod[]" value="<?php echo $this->mod; ?>"/>
                                        <input type="hidden" name="type" value="activity_grade"/>
                                        <input type="hidden" class="feedbackold" value="<?php echo $student['feedback']; ?>"/>
                                        <button class="btCancel" onclick="cancelFeedback(<?php echo $i;?>)"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                                        <button class="btSave" onclick="saveFeedback(<?php echo $i.','.$student['gradeid'].','. $student['gradeitemid']; ?>,'<?php echo $users->username; ?>');"><?php echo JText::_('COM_JOOMDLE_SAVE'); ?></button>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </li>

                    <div id="somediv" class="somediv" title="this is a dialog" style="display:none;">
                      <div class="header">
                            <div class="closetitle" style="opacity: 1;">
                                <img src="/images/<?php echo ($device == 'mobile') ? 'deleteIcon':'deleteiconm' ?>.png">
                            </div>
                          <?php
                            $banner = new HeaderBanner(new JoomdleBanner);
                            $render_banner = $banner->renderBanner($this->course_info, $this->mods_student['sections'], $role);
                          ?>

                        <iframe id="thedialog" width="100%" height="400px"></iframe>
                      </div>
                    </div>
        <?php
                    $i++;
                }
            }
        ?>
    </ul>
    </div>
    
        <!-- ========== Not Completed ========== -->
    <div class="list-notcompleted">
        <div class="title_list">
            <span class="title_txt"><?php echo JText::_('COM_JOOMDLE_NOT_COMPLETE');?></span>
            <span class="title_num"><?php echo count($learner_notcomp); ?></span>
            <?php if (count($learner_notcomp) > 0) { ?>
            <span class="icon_dropdown"></span>
            <span class="icon_uparrow"></span>
            <?php } ?>
        </div>
        <ul class="joomdle-list learner_notcompleted">
    <?php
            // begin render student in course
            $j = 0;
            if (count($learner_notcomp) > 0) {
    //            $g = array('93'=>'A', '90'=>'A-', '87'=>'B+', '83'=>'B', '80'=>'B-', '77'=>'C+', '73'=>'C', '70'=>'C-', '67'=>'D+', '60'=>'D', '0'=>'F');
                foreach ($learner_notcomp as $student) {
                    if ($student['username'] == $username) continue;
                    $user_by_username = JFactory::getUser($student['username']);
                    $users = CFactory::getUser($user_by_username->id);
                    if (!$user_by_username->username || $user_by_username->username == $username) continue;
                    
                    // get avatar
                    $avatar = $users->getAvatar();
                    $final_grade = $student['finalgrade'];
                    if ($student['grademax'] == 100) $finalGrade = (int)$final_grade;
                    else if ($student['grademax'] == 10) $finalGrade = (int)($final_grade * 10);

                    if ($student['gradeid'] > 0 && $student['finalgradeletter'] && $this->mod != 'assign') {
                        $finalgradeletters = strtok($student['finalgradeletter'], " ");
                    } elseif ($student['gradeid'] > 0 && $student['student']['finalgradeletter']  && $this->mod == 'assign' && $student['finalgrade'] > 0) {
                        foreach ($grade_letter_assign as $grade_assign) {
                            if ($finalGrade >= $grade_assign['value']) {
                                $finalgradeletters = $grade_assign['name'];
                                break;
                            }
                        }
                    } else {
                        $finalgradeletters = JText::_('PROGRESS_UNGRADED');
                    }
                    ?>
                    <li class="list-user-grade">
                        <div class="avatar-overall"><a class="nav-avatar"><img alt="<?php echo $users->name; ?>"
                                                                               src="<?php echo $avatar; ?>"/></a></div>
                        <div class="user-grade">
                            <div class="name"><?php echo $users->name; ?></div>
                            </div>
                    </li>
                    <?php
                    $j++;
                }
            }
            ?>
        </ul>
    </div>
    <?php
//    if ($i == 0 && $j == 0) {
//        echo '<div class="bold cred tcenter bg">' . JText::_('COM_JOOMDLE_NO_USERS_ENROLLED') . '</div>';
//    }
    ?>
        <div class="abc"></div>
        <div class="notification"></div>
        <div class="leavePagePopup">
            <p class="txt_title"><?php echo JText::_('COM_JOOMDLE_CHECK_SAVE_PROGRESS'); ?></p>
            <div class="buttons">
                <button class="btCancel" onclick="cancelLeave();"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                <button class="btProceed" onclick="proceedLeave();"><?php echo JText::_('COM_JOOMDLE_BUTTON_PROCEED'); ?></button>
            </div>
        </div>
</div>
<script>
    var change_textarea = false;
jQuery(document).ready(function() {

    jQuery('.back').attr('onclick', '');
    
    jQuery('.autosize').each(function () {
        autosize(this);
    }).on('input', function () {
        autosize(this);
        <?php if ($device != 'mobile') { ?>
            var feedbackold = jQuery(this).next('.buttons_feedback').children('.feedbackold').val(); 
        if (jQuery(this).val() == '' && feedbackold == '') {
            jQuery(this).next('.buttons_feedback').hide();
        } else {
            jQuery(this).next('.buttons_feedback').show();
        }   
        <?php } ?>
    });

    jQuery('.autosize').each(function () {
        autosize(this);
    }).focus(function () {
        autosize(this);
        <?php if ($device != 'mobile') { ?>
            jQuery(this).next('.buttons_feedback').show();
        <?php } ?>
    }).focusout(function() {
        <?php if ($device != 'mobile') { ?>
            var newtext = jQuery(this).val(); 
            var oldtext = jQuery(this).next('.buttons_feedback').children('.feedbackold').val(); 
            if (newtext == oldtext) {
                jQuery(this).next('.buttons_feedback').hide();
            }
        <?php } ?>
    });

    var mob = '<?php echo $device ?>';
    var h = screen.height-140;
    if (mob == 'mobile') {
        jQuery(".attach-result").click(function () {
            window.location.href = jQuery(this).attr("datalink");
        });
    } else {
        jQuery(".attach-result").click(function () {
            jQuery("#thedialog").attr('src', jQuery(this).attr("datalink"));
            jQuery("#somediv").css('display','block');

            jQuery('body').append('<div class="mfp-bg mfp-ready"></div>');
            return false;
        });
    }
    
    jQuery(' .closetitle').click(function(){
        jQuery("#thedialog").attr('src', '');
        jQuery("#somediv").css('display','none');
        jQuery('body').find('.mfp-bg.mfp-ready').remove();
    });
    jQuery('.list-completed .icon_dropdown').click(function () {
        jQuery('.joomdle-list.learner_completed').css('display','inline-block');
        jQuery(this).hide();
        jQuery('.list-completed .icon_uparrow').css('display','inline-block');

        jQuery('textarea').each(function () {
            var txt_height = jQuery(this)[0].scrollHeight;
        
            if (txt_height > 70) {
                jQuery(this).height( txt_height );
            }
        });
    });
    jQuery('.list-completed .icon_uparrow').click(function () {
        jQuery('.joomdle-list.learner_completed').hide();
        jQuery(this).hide();
        jQuery('.list-completed .icon_dropdown').css('display','inline-block');
    });
    jQuery('.list-notcompleted .icon_dropdown').click(function () {
        jQuery('.joomdle-list.learner_notcompleted').css('display','inline-block');
        jQuery(this).hide();
        jQuery('.list-notcompleted .icon_uparrow').css('display','inline-block');
    });
    jQuery('.list-notcompleted .icon_uparrow').click(function () {
        jQuery('.joomdle-list.learner_notcompleted').hide();
        jQuery(this).hide();
        jQuery('.list-notcompleted .icon_dropdown').css('display','inline-block');
    });
    jQuery('.gradeActivitySelect').each(function () {
        jQuery(this).click(function () {
            jQuery(this).find('.ulGradeSelected').toggle();
        });
    });
    jQuery('.gradeActivitySelect .ulGradeSelected li').each(function () {
        jQuery(this).click(function () {
            jQuery(this).parent('.ulGradeSelected').prev('.gradeSelected').html(jQuery(this).html());
            jQuery(this).parent('.ulGradeSelected').find('.grade-option').val(jQuery(this).attr('value'));
            if(jQuery(this).parent('.ulGradeSelected').find('.grade-option').val(jQuery(this).attr('value')) != null){
                jQuery(this).parent('.ulGradeSelected').find('.status-option').val("completed");
            }
            
            var gradeid = jQuery(this).parent('.ulGradeSelected').find('.gradeid').val();
            var gradeitem = jQuery(this).parent('.ulGradeSelected').find('.gradeitem').val();
            var username = jQuery(this).parent('.ulGradeSelected').find('.username').val();
            var gradevalue = jQuery(jQuery(this)).val();
            var feedback = jQuery(this).closest('.list-user-grade').find('.feedback-item').find('textarea').val();
            
            saveGrade(gradeid, gradeitem, username, gradevalue, feedback);
        });
    });
    jQuery('textarea').change(function() {
        change_textarea = true;
    });

    jQuery('.back').click(function () {
        console.log(change_textarea);
        if (change_textarea) {
            lgtCreatePopup('confirm', {
                    'yesText': '<?php echo JText::_('COM_JOOMDLE_BUTTON_PROCEED'); ?>',
                    'content': '<?php echo '<b>' . JText::_('COM_JOOMDLE_CANCEL_GRADE_SAVE1') . '</b><br>' . JText::_('COM_JOOMDLE_CANCEL_GRADE_SAVE2'); ?>'
                }, function () {
                    window.location.href = '<?php echo JUri::base().'/viewactivity/'.$this->courseid.'.html';?>';
                }
            );
        } else window.location.href = '<?php echo JUri::base().'/viewactivity/'.$this->courseid.'.html';?>';
    });
});

function saveFeedback(i, gradeid, gradeitemid, username) {
    var courseid = '<?php echo $this->courseid; ?>';
    var feedback = jQuery('.txtFeed_'+i).val();
    var gradevalue = jQuery('.txtFeed_'+i).closest('.list-user-grade').find('.grade-option').val();
    console.log(gradevalue);
        jQuery.ajax({
            type: "POST",
            data: {
                courseid: courseid, 
                username: username,
                gradeid: gradeid,
                gradeitem: gradeitemid,
                gradevalue: gradevalue > 100 ? gradevalue/10 : gradevalue,
                feedback: feedback,
                act: 'savefeedback'
            },
            url: window.location.href,
            beforeSend: function () {
                jQuery('body').addClass('overlay2');
                jQuery('.notification').html('Loading...').fadeIn();
            },
            success: function (data) {
                var res = JSON.parse(data);

                jQuery('body').removeClass('overlay2');
                jQuery('.notification').fadeOut();
                if (res.status == 0) {
                    alert(res.message);
                } else {
                    jQuery('.buttoni_'+i).hide();
                    jQuery('.buttoni_'+i+' .feedbackold').val(feedback);
                    jQuery(".txtFeed_"+i).html(feedback);
                    jQuery(".txtFeed_"+i).val(feedback);
                    jQuery('.textarea-value-'+i).val(feedback)
                    jQuery('.feedbackitem_'+i).hide();
                    change_textarea = false;
//                    window.location.href = "<?php // echo JURI::base() . 'activitydetail/'.$this->courseid.'_'.$this->activities.'_'.$this->mod.'.html';?>";
                }
            },
            error: function () {
                jQuery('.notification').fadeOut();
            }
        });
}

function saveGrade(gradeid, gradeitemid, username, gradevalue, feedback) {
    var courseid = '<?php echo $this->courseid; ?>';
        jQuery.ajax({
            type: "POST",
            data: {
                courseid: courseid, 
                username: username,
                gradeid: gradeid,
                gradeitem: gradeitemid,
                gradevalue: gradevalue,
                feedback: feedback,
                act: 'savegrade'
            },
            url: window.location.href,
            beforeSend: function () {
                jQuery('body').addClass('overlay2');
                jQuery('.notification').html('Loading...').fadeIn(); 
            },
            success: function (data) {
                var res = JSON.parse(data);

                jQuery('body').removeClass('overlay2');
                jQuery('.notification').fadeOut();
                if (res.status == 0) {
                    alert(res.message);
                } else {
                    console.log(res.message);
                    window.location.href = "<?php echo JURI::base() . 'activitydetail/'.$this->courseid.'_'.$this->activities.'_'.$this->mod.'.html';?>";
                }
            },
            error: function () {
                jQuery('.notification').fadeOut();
            }
        });
}

function cancelFeedback(i) {
    var txt = jQuery('.buttoni_'+i+' .feedbackold').val();
    jQuery('.txtFeed_'+i).val(txt);
    jQuery('.buttoni_'+i).hide();
    var ta = document.querySelector('.txtFeed_'+i);
    autosize(ta);
    var evt = document.createEvent('Event');
    evt.initEvent('autosize:update', true, false);
    ta.dispatchEvent(evt);
    change_textarea = false;
}

function popupFeedbackMobi(i) {
    if (change_textarea) {
        jQuery('body').addClass('overlay2');    
        jQuery('.popup_'+i).show();
    } else {
        jQuery('.feedbackitem_'+i).hide();
        jQuery('.txtFeed_'+i).attr('rows', '2');
    }
}

function cancelPopup(i) {
    jQuery('body').removeClass('overlay2');
    jQuery('.popup_'+i).hide();
}
function proceedPopup(i) {
    jQuery('body').removeClass('overlay2');
    jQuery('.popup_'+i).hide();
    jQuery('.feedbackitem_'+i).hide();
    jQuery('.txtFeed_'+i).val(jQuery('.textarea-value-'+i).val());
    change_textarea = false;
}

function giveFeedback(i) { 
    jQuery('.feedbackitem_'+i).show();
    jQuery('.buttoni_'+i).show();

    txt_height = jQuery('.txtFeed_'+i)[0].scrollHeight;
    console.log(txt_height);
    if (txt_height > 70) {
        jQuery('.txtFeed_'+i).height( txt_height );
    }
}

//function checkSaveProgress() {
//    if (window.location.href != "<?php // echo JURI::base() . 'activitydetail/'.$this->courseid.'_'.$this->activities.'_'.$this->mod.'.html';?>";)
//    jQuery('body').addClass('overlay2');
//    jQuery('.leavePagePopup').show();
//    }
//}
function cancelLeave() {
    jQuery('body').removeClass('overlay2');
    jQuery('.leavePagePopup').hide();
    jQuery(window).off("beforeunload");
    return true;
}
function proceedLeave() {
//    jQuery('body').removeClass('overlay2');
//    jQuery('.leavePagePopup').hide();
    return true;
}

</script>
