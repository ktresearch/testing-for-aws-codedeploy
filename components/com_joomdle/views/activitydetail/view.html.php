<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* View detail activity
/* $author: kydon vn team
 * $date: 29 Jul 2016 
 * 
 */

defined('_JEXEC') or die('Restricted access');

class JoomdleViewActivitydetail extends JViewLegacy {
    public function display($tpl = null) {
        $app = JFactory::getApplication();
        
        $params = $app->getParams();
	    $this->assignRef('params', $params);
        $user = JFactory::getUser();
        $username = $user->username;

        $id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
        if (!$id) $id =  JRequest::getVar( 'course_id' );
        if (!$id)
            $id = $params->get( 'course_id' );

        $id = (int) $id;

        $this->hasPermission = JFactory::hasPermission($id, $username);
        $user_role = array();
        if (!empty($this->hasPermission)) {
            foreach (json_decode($this->hasPermission[0]['role']) as $r) {
                $user_role[] = $r->sortname;
            }
        }
        
        if (empty(array_intersect($user_role, ['teacher', 'editingteacher', 'manager', 'coursecreator']))) {
            JFactory::handleErrors();
        }

        $act = JRequest::getVar( 'activity', null, 'NEWURLFORM' );
        if (!$act) $act =  JRequest::getVar( 'activity' );
        if (!$act)
            $act = $params->get( 'activity' );

        $mod = JRequest::getVar( 'type', null, 'NEWURLFORM' );
        if (!$mod) $mod =  JRequest::getVar( 'type' );
        if (!$mod)
            $mod = $params->get( 'type' );

        $this->mod = $mod;
        $this->activities = $act;
        $this->courseid = $id;
        
        // Save feedback
        if (isset($_POST['act']) && $_POST['act'] == 'savefeedback') {
            if ($_POST['gradeitem'] > 0) {
                $user_name = $_POST['username'];
                $gradeid = $_POST['gradeid'];
                $gradeitemi = $_POST['gradeitem'];
                $grade_value = $_POST['gradevalue'];
                $feedback_grade = $_POST['feedback'];
                $save = JoomdleHelperContent::call_method ('save_grade_user', (string) $user_name, (int) $gradeid, (int)$gradeitemi, (float) $grade_value, (string) $feedback_grade);
                if ($save) {
                    $result = array();
                    $result['message'] = 'success';
                    $result['status'] = true;
                    echo json_encode($result);
                    die;
                } else {
                    $result = array();
                    $result['message'] = 'false';
                    $result['status'] = false;
                    echo json_encode($result);
                    die;
                }
            }
        } else if (isset($_POST['act']) && $_POST['act'] == 'savegrade') {
            if ($_POST['gradeitem'] > 0) {
                $user_name = $_POST['username'];
                $gradeid = $_POST['gradeid'];
                $gradeitemi = $_POST['gradeitem'];
                $grade_value = $_POST['gradevalue'];
                $feedback_grade = $_POST['feedback'];
                $savegrade = JoomdleHelperContent::call_method ('save_grade_user', (string) $user_name, (int) $gradeid, (int)$gradeitemi, (float) $grade_value, (string) $feedback_grade); 

                $completed = JoomdleHelperContent::call_method ('save_completion_activity', (string) $user_name, (int) $act, 'completed', (string) $feedback_grade);

                if ($savegrade && $completed) {
                    $result = array();
                    $result['message'] = 'success';
                    $result['status'] = true;
                    echo json_encode($result);
                    die;
                } else {
                    $result = array();
                    $result['message'] = 'false';
                    $result['status'] = false;
                    echo json_encode($result);
                    die;
                }
            }
        }
        
        // course fullname
        $course_info = JoomdleHelperContent::getCourseInfo($id, $username);        
        $this->course_info = $course_info;
        
        // get grade of activity
        
        $this->students = JoomdleHelperContent::call_method ( 'get_mod_detail', (int) $act, $mod);

        parent::display($tpl);
    }
}