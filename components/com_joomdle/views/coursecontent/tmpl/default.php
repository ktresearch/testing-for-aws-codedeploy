<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
$itemid = JoomdleHelperContent::getMenuItem();

$linkstarget = $this->params->get( 'linkstarget' );
if ($linkstarget == "new")
    $target = " target='_blank'";
else $target = "";

$session  = JFactory::getSession();
$jump_url =  JoomdleHelperContent::getJumpURL ();
$user = JFactory::getUser();
$username = $user->username;
$token = md5 ($session->getId());
$course_id = $this->course_id;
$direct_link = 1;
$show_summary = $this->params->get( 'course_show_summary');
$show_topics_numbers = $this->params->get( 'course_show_numbers');

if ($session->get('device') == 'mobile') {
    ?>
    <style type="text/css">
        .navbar-header {
            background-color: #126db6;
        }
    </style>
    <?php
}
require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
?>
<div class="joomdle-coursecontent <?php echo $this->pageclass_sfx?>">
    <?php
    $topic = 0;
    $content = 0;
    $scorm = 0;
    $question = $this->questions;
    if ($question['status']) {
        $questions = count($question['questions']);
    } else {
        $questions = 0;
    }

    $assessment = 0;
    $assignment = 0;
    if (is_array ($this->mods)) {
        foreach ($this->mods as $tema) :
            $resources = $tema['mods'];
            if (is_array($resources)) :
                foreach ($resources as $id => $resource) {
                    $mtype = JoomdleHelperSystem::get_mtype ($resource['mod']);
                    if (!$mtype) // skip unknow modules
                        continue;
                    switch ($mtype) {
                        case 'quiz':
                            $assessment++;
                            break;
                        case 'assign':
                            $assignment++;
                            break;
                        case 'page':
                            $content++;
                            break;
                        case 'scorm':
                            $scorm++;
                            break;
                        default:
                            # code...
                            break;
                    }
                }
            endif;
        endforeach;
    }
    ?>
    <!-- <div class="moduleType">
        <div class="topPart">
            <div class="moduleIcon"><img src="/media/joomdle/images/Topicicon.png"></div>
            <div class="moduleName"><?php echo Jtext::_('COURSE_BUTTON_TOPIC'); ?> <?php echo '('.$topic.')'; ?></div>
            <a class="btAddModule" href="">+</a>
        </div>
        <div class="bottomPart">
            <p class="moduleDescription"><?php echo Jtext::_('COURSE_INSTRUCTION_TOPIC'); ?></p>
        </div>
    </div> -->
    <script type="text/javascript">
        (function ($) {

            if($( ".t3-content" ).children().hasClass( "course-menu" ) == false){
                $('.joomdle-course ').css("margin-top","0px");
            }

        })(jQuery);
    </script>

    <div class="moduleType">
        <div class="topPart">
             <?php if($this->course_info['course_status'] != 'pending_approval' && $this->course_info['course_status'] != 'approved' && !$this->course_info['self_enrolment']): ?>
            <a class="btAddModule" href="<?php echo JUri::base()."content/create_".$this->course_id.".html";  ?>">+</a>
            <?php endif; ?>
            <div class="moduleIcon"><img src="/images/icons/content30x30.png"></div>
            <a class="moduleName" href="<?php echo JUri::base()."content/list_".$this->course_id.".html";  ?>"><?php echo Jtext::_('COURSE_ACTIVITY_CONTENT'); ?> <?php echo '('.$content.')'; ?></a>
           

        </div>
        <div class="bottomPart">
            <p class="moduleDescription"><?php echo Jtext::_('COURSE_INSTRUCTION_CONTENT'); ?></p>
        </div>

    </div>

    <div class="moduleType">
        <div class="topPart">
             <?php if($this->course_info['course_status'] != 'pending_approval' && $this->course_info['course_status'] != 'approved' && !$this->course_info['self_enrolment']): ?>
            <a class="btAddModule" href="<?php echo JUri::base()."scorm/create_".$this->course_id.".html";  ?>">+</a>
            <?php endif; ?>
            <div class="moduleIcon"><img src="/images/icons/scorm30x30.png"></div>
            <a class="moduleName" href="<?php echo JUri::base()."scorm/list_".$this->course_id.".html";  ?>"><?php echo Jtext::_('COM_JOOMDLE_SCORM'); ?> <?php echo '('.$scorm.')'; ?></a>
           

        </div>
        <div class="bottomPart">
            <p class="moduleDescription"><?php echo Jtext::_('COURSE_INSTRUCTION_SCORM'); ?></p>
        </div>
    </div>

    <div class="moduleType">
        <div class="topPart">
            <?php if($this->course_info['course_status'] != 'pending_approval' && $this->course_info['course_status'] != 'approved' && !$this->course_info['self_enrolment']): ?>
            <a class="btAddModule" href="<?php echo JUri::base()."question/create_".$this->course_id.".html";  ?>">+</a>
            <?php endif; ?>
            <div class="moduleIcon"><img src="/images/icons/questions30x30.png"></div>
            <a class="moduleName" href="<?php echo JUri::base()."question/list_".$this->course_id.".html";  ?>"><?php echo Jtext::_('COURSE_ACTIVITY_QUESTION'); ?> <?php echo '('.$questions.')'; ?></a>
            
        </div>
        <div class="bottomPart">
            <p class="moduleDescription"><?php echo Jtext::_('COURSE_INSTRUCTION_QUESTION'); ?></p>
        </div>
    </div>

    <div class="moduleType">
        <div class="topPart">
            <?php if($this->course_info['course_status'] != 'pending_approval' && $this->course_info['course_status'] != 'approved' && !$this->course_info['self_enrolment']): ?>
            <a class="btAddModule" href="<?php echo JUri::base()."assessment/create_".$this->course_id.".html";  ?>">+</a>
            <?php endif; ?>
            <div class="moduleIcon"><img src="/images/icons/assessment30x30.png"></div>
            <a class="moduleName" href="<?php echo JUri::base()."assessment/list_".$this->course_id.".html";  ?>"><?php echo Jtext::_('COURSE_ACTIVITY_ASSESSMENT'); ?> <?php echo '('.$assessment.')'; ?></a>
            
            
        </div>
        <div class="bottomPart">
            <p class="moduleDescription"><?php echo Jtext::_('COURSE_INSTRUCTION_ASSESSMENT'); ?></p>
        </div>
    </div>

    <div class="moduleType" style="border-bottom: 0px;">
        <div class="topPart">
            <?php if($this->course_info['course_status'] != 'pending_approval' && $this->course_info['course_status'] != 'approved' && !$this->course_info['self_enrolment']): ?>
             <a class="btAddModule" href="<?php echo JUri::base()."assignment/create_".$this->course_id.".html"; ?>">+</a>
            <?php endif; ?>
            <div class="moduleIcon"><img src="/images/icons/assignment30x30.png"></div>
            <a class="moduleName" href="<?php echo JUri::base()."assignment/list_".$this->course_id.".html"; ?>"><?php echo Jtext::_('COURSE_ACTIVITY_ASSIGNMENT'); ?> <?php echo '('.$assignment.')'; ?></a>
            
           
        </div>
        <div class="bottomPart">
            <p class="moduleDescription"><?php echo Jtext::_('COURSE_INSTRUCTION_ASSIGNMENT'); ?></p>
        </div>

    </div>

  <!--   <div class="btn-brief">
        <a class="btAddModule" href="<?php //echo JURI::base().'index.php?option=com_joomdle&view=certificatecreate';?>"><span class="next">Add Certificate</span></a>
    </div> -->

</div>
