<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewCourseContent extends JViewLegacy {
    function display($tpl = null) {
        global $mainframe;

        $app = JFactory::getApplication();
        $menus = $app->getMenu();
        $user = JFactory::getUser();
        $username = $user->username;

        $params = $app->getParams();
        $this->assignRef('params', $params);

        $id =  JRequest::getVar( 'course_id' , null, 'NEWURLFORM' );
        if (!$id) $id =  JRequest::getVar( 'course_id' );
        if (!$id) $id = $params->get( 'course_id' );

        $id = (int) $id;

        if (!$id) {
            echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
            return;
        }

        $course_content_data = JoomdleHelperContent::call_method('get_course_content_page_data', $id, $username);

        $userRoles = json_decode($course_content_data['userRoles'], true);
        if ($userRoles['status']) {
            $this->hasPermission = $userRoles['roles'];
        } else {
            $this->hasPermission = array();
        }

        if (!$this->hasPermission[0]['hasPermission']) {
            echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
            return;
        }

        $this->course_info =  json_decode($course_content_data['course_info'], true);
        $coursemods = json_decode($course_content_data['coursemods'], true);
        if ($coursemods['status']) {
            $this->mods = $coursemods['coursemods'];
        } else {
            $this->mods = array();
        }
        $this->course_id = (int) $id;

        // get module of course have visible = 0;
        $visible = 0;
        $this->mods_not_visible = json_decode($course_content_data['mods_not_visible'], true);

        $this->questions = json_decode($course_content_data['questions'], true);

        $document = JFactory::getDocument();
        $document->setTitle(JText::_('COM_JOOMDLE_COURSE_CONTENT'));

        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

        parent::display($tpl);
    }
}
?>
