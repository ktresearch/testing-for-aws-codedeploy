<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewMycourses extends JViewLegacy {

    function display($tpl = null) {
        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        $username = $user->username;

        require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
        $my = CFactory::getUser();
        if (!$username || $my->_permission) {
            JFactory::handleErrors();
        }

        $pathway = $app->getPathWay();
        $menus = $app->getMenu();
        $menu = $menus->getActive();

        $session = JFactory::getSession();
        $device = $session->get('device');

        $params = $app->getParams();
        $this->assignRef('params', $params);

        $jinput = $app->input;

        $act =  JRequest::getVar( 'action', null, 'NEWURLFORM' );
        if (!$act) $act =  JRequest::getVar( 'action' );

        if (isset($_POST['act']) && ($_POST['act'] == 'create' || $_POST['act'] == 'update')) {
            $array = array();
            if ($_POST['act'] == 'update') $array['id'] = (int)$_POST['id'];
            $array['fullname'] = (strip_tags($_POST['title']));
            $array['shortname'] = (strip_tags($_POST['title']));

            if (strlen(preg_replace("/\"/", "&quot;", $array['fullname'])) > 254) {
                echo json_encode([
                    'error' => 1,
                    'comment' => 'The maximum number of characters allowed in course name field is 254 characters.',
                    'data' => htmlentities($array['fullname'])
                    ]);
                die;
            }
            
            $array['summary'] = (strip_tags($_POST['des']));
            $array['duration'] = (float)$_POST['duration'];
            $array['price'] = (float)$_POST['price'];
            $array['course_lang'] = '';
            $array['startdate'] = time();
            $array['timemodified'] = time();
            $array['idnumber'] = '';
            $array['category'] = (int)$_POST['categoryid'];
            $array['coursesurvey'] = (int)$_POST['coursesurvey'];
            $array['facilitatedcourse'] = (int)$_POST['facilitatedcourse'];
            $array['learningoutcomes'] = strip_tags($_POST['learningoutcomes']);
            $array['targetaudience'] = strip_tags($_POST['targetaudience']);
            $array['certificatecourse'] = (int)$_POST['certificatecourse'];
            $array['validfor'] = (int)$_POST['validfor'];
            $array['username'] = addslashes(strip_tags($username));
            if (!empty($_POST['imgName'])) $array['imgName'] = addslashes(strip_tags($_POST['imgName']));
            if (isset($_POST['tmpfile'])) $array['tmpfile'] = (strip_tags($_POST['tmpfile']));
            $enableCreateChats = $jinput->post->get('enableCreateChats', 1, 'INT');

            if ($_POST['act'] == 'create') $r = JoomdleHelperContent::call_method('create_course', $array);
            else if ($_POST['act'] == 'update') $r = JoomdleHelperContent::call_method('update_course', $array);
            if($r['error'] == 0){
                require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/groups.php');
                if ($_POST['act'] == 'create' && $r['data'] != 0)  {
                    $lpuser = JoomdleHelperContent::call_method('get_lp_of_category', (int)$_POST['categoryid']);
                    if ($lpuser['status']) {
                        $lpusername = $lpuser['username'];
                    } else {
                        $lpusername = null;
                    }
                    if (!empty($_POST['tmpfile'])) {
                        $courseInfo = JoomdleHelperContent::call_method('get_course_info', (int)$r['data'], $username);
                        JoomdleHelperSocialgroups::add_group (htmlentities(strip_tags($_POST['title'])), htmlentities(strip_tags($_POST['des'])), (int)$r['data'], $username, $lpusername, dirname($app->getCfg('dataroot')).'/temp/'.$_POST['tmpfile'], $courseInfo['filename'], $enableCreateChats);
                    }
                } else {
                    if (!empty($_POST['tmpfile'])) {
                        $courseInfo = JoomdleHelperContent::call_method('get_course_info', (int)$array['id'], $username);
                        JoomdleHelperSocialgroups::update_group (htmlentities(strip_tags($_POST['title'])), htmlentities(strip_tags($_POST['des'])), (int)$array['id'], dirname($app->getCfg('dataroot')).'/temp/'.$_POST['tmpfile'], $courseInfo['filename'], $enableCreateChats);
                } else JoomdleHelperSocialgroups::update_group (htmlentities(strip_tags($_POST['title'])), htmlentities(strip_tags($_POST['des'])), (int)$array['id'], '', '', $enableCreateChats);
                }
            }
            if (!empty($_POST['tmpfile'])) unlink(dirname($app->getCfg('dataroot')).'/temp/'.$_POST['tmpfile']);

            $result = array();
            $result['error'] = $r['error'];
            $result['comment'] = $r['mes'];
            $result['data'] = $r['data'];
            echo json_encode($result);
            die;
        }
        else if(isset($_POST['act']) && $_POST['act'] == 'check_status_publish_again'){
            $cid = $_POST['courseid'];

            require_once(JPATH_BASE.'/components/com_community/libraries/core.php');

            $groups = CFactory::getModel('groups');

            $check = $groups->getShareNameGroups($cid, null);
            $result = array();

            if (!$check) {
                $result['check'] = $check;
            } else {
                $result['check'] = $check;
            }

            echo json_encode($result);
            die;

        }
        else if(isset($_POST['act']) && $_POST['act'] == 'course_publish_again'){
            $cid = $_POST['courseid'];
            $gid = $_POST['groupid'];

            require_once(JPATH_BASE.'/components/com_community/libraries/core.php');

            $groups = CFactory::getModel('groups');

            $groups->getShareGroupsAgain($cid, $gid);

            $r = JoomdleHelperContent::call_method('course_enable_self_enrolment', (int)$cid, 'enable', $username);

            $result = array();
            if ($r['status'] == 1) {
                $result['error'] = 0;
            } else {
                $result['error'] = 1;
            }
            $result['course'] = $cid;
            $result['group'] = $gid;
            $result['username'] = $username;
            $result['message'] = $r['message'];
            $result['data'] = isset($r['data']) ? $r['data'] : '';
            echo json_encode($result);
            die;
        }
        else if (isset($_POST['act']) && ($_POST['act'] == 'unsharetocircle')) {
            $cid = $_POST['courseid'];
            $r = JoomdleHelperContent::call_method('course_enable_self_enrolment', (int)$cid, 'unenable', $username);
            $result = array();
            if ($r['status'] == 1) {
                $result['error'] = 0;
                $result['message'] = $r['message'];
                require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
                $groups = CFactory::getModel('groups');
                $groups->deleteCourseShareToGroup($cid);
            } else {
                $result['error'] = 1;
                $result['message'] = $r['message'];
            }
            echo json_encode($result);
            die;
        } else if (isset($_POST['act']) && $_POST['act'] == 'load_more') {
            function custom_echo1($x, $length)
            {
                if (strlen($x) <= $length) {
                    return $x;
                } else {
                    $y = substr($x, 0, $length) . '...';
                    return $y;
                }
            }
            require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
            $my = CFactory::getUser();
            $config = CFactory::getConfig();
            $id = $user->id ? $user->id : $my->id ;

            $groups = CFactory::getModel('groups');
            $lpcircle = 7;
            $socialcircle = 0;

            $typeload = $_POST['type'];
            $page = $_POST['page'];

            if ($device == 'mobile') {
                $limit = (int) JText::_('COM_JOOMDLE_CIRCLE_LIMIT_MOBILE');
            } else {
                $limit = (int) JText::_('COM_JOOMDLE_CIRCLE_LIMIT');
            }
            $position = $limit * ($page - 1);

            if ($typeload == JText::_('COM_JOOMDLE_TYPE_LOAD_MYCIRCLES')) {
                $tmpGroups = $groups->getCircles($id, true, $position, $limit, $socialcircle, 'owner');
                $groups_arr = array();

                if ($tmpGroups) {
                    foreach ($tmpGroups as $row) {
                        $pr = json_decode($row->params);
                        if (isset($pr->course_id) && $pr->course_id > 0) continue;
                        $group = JTable::getInstance('Group', 'CTable');
                        $group->bind($row);
                        $group->updateStats(); //ensure that stats are up-to-date
                        $group->description = CStringHelper::clean(JHTML::_('string.truncate', $group->description, $config->get('tips_desc_length')));
                        $groups_arr[] = $group;
                    }
                    unset($tmpGroups);
                }
            } 
            if ($typeload == JText::_('COM_JOOMDLE_TYPE_LOAD_MYLPCIRCLES')) {
                $mygroups =  $groups->getCircles($id, 0, 0, 15, $lpcircle);
                $groups_arr = array();

                if ($mygroups) {
                    foreach ($mygroups as $value) {
                        if (isset($value->moodlecategoryid) && $value->moodlecategoryid != 0 && $value->ownerid == $user->id) {
                            $group = JTable::getInstance('Group', 'CTable');
                            $group->bind($value);
                            $group->updateStats(); //ensure that stats are up-to-date
                            $group->description = CStringHelper::clean(JHTML::_('string.truncate', $group->description, $config->get('tips_desc_length')));
                            $groups_arr[] = $group;
                        }
                    }
                    unset($mygroups);
                }
            }
            $list_circles = array();
            if (count($groups_arr) > 0) {
                foreach($groups_arr as $key => $gr ){
                    $table = JTable::getInstance('Group', 'CTable');
                    $table->load($gr->id);
                    if ($device == 'mobile') {
                        $a = custom_echo1($gr->name, 60);
                    } else {
                        $a = custom_echo1($gr->name, 40);
                    }
                    if ($device == 'mobile') {
                        $list_circles[$key]['stt'] = $limit*($page - 1) + 3 + $key;
                    }else{
                        $list_circles[$key]['stt'] = $limit*($page - 1) + $key;
                    }
                    $list_circles[$key]['id']= $gr->id;
                    $list_circles[$key]['name']= $a;
                    $list_circles[$key]['avatar']= $table->getAvatar();
                }
            }
            $result = array();
            $result['circles'] = $list_circles;
            echo json_encode($result);
            die;
        } else
            if (!empty($_FILES)) {
                $imgEx = array(
                    'png', 'jpg', 'jpeg', 'gif', 'bmp'
                );
                $ext = strtolower(pathinfo($_FILES[0]['name'], PATHINFO_EXTENSION));

                if ($_FILES[0]['size'] > 88*1024*1024) {
                    $result['error'] = 1;
                    $result['comment'] = 'File size is too large (< 8mb)';
                } else if (strlen($_FILES[0]['name']) > 250) {
                    $result['error'] = 1;
                    $result['comment'] = 'File name is too long (< 250char)';
                } else if (!in_array($ext, $imgEx)) {
                    $result['error'] = 1;
                    $result['comment'] = 'Invalid file type. Please upload image file.';
                } else {
                    $tmpFileName = 'tmp_pic'.microtime(true)*10000;
                    $tmp_file = dirname($app->getCfg('dataroot')).'/temp/'.$tmpFileName;
                    file_put_contents ($tmp_file, file_get_contents($_FILES[0]['tmp_name']));
                    $result = array();
                    $result['error'] = 0;
                    $result['comment'] = 'Everything is fine';
                }
                $result['data'] = $_FILES;
                $result['data'][0]['tmp_file'] = $tmpFileName;
                echo json_encode($result);
                die;
            }

        switch ($act) {
            case 'catalogue':
                $this->action = "catalogue";
                require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
                require_once(JPATH_SITE.'/components/com_hikashop/helpers/lpapi.php');

                $noncat_arr = array();
                $macat_arr = array();
                $lpcat_arr = array();
                $malpcat_arr = array();

                if ($params->get('use_new_performance_method'))
                    $mycategory = JHelperLGT::getMoodleCategories($username);
                else
                    $mycategory = JoomdleHelperContent::call_method('get_my_categories', $username);

                if ($mycategory['status']) {
                    foreach ($mycategory['categories'] as $mycat) {
                        if ($mycat['role'] == 'LP') {
                            $lpcat_arr[] = $mycat['catid'];
                            $malpcat_arr[] = $mycat['catid'];
                        } else if ($mycat['role'] == 'CMA') {
                            $macat_arr[] = $mycat['catid'];
                            $malpcat_arr[] = $mycat['catid'];
                        } else if ($mycat['role'] == 'non-cat') {
                            $noncat_arr[] = $mycat['catid'];
                        }
                    }
                }
                if (!empty($noncat_arr)) $noncat = implode(',', $noncat_arr); else $noncat = '';
                if (!empty($macat_arr)) $macat = implode(',', $macat_arr); else $macat = '';
                if (!empty($lpcat_arr)) $lpcat = implode(',', $lpcat_arr); else $lpcat = '';
                if (!empty($malpcat_arr)) $malpcat = implode(',', $malpcat_arr); else $malpcat = '';

                $this->malpcat = $malpcat_arr;
                $this->lpcat = $lpcat_arr;
                $this->noncat = $noncat_arr;

                // Have to use $malpcat to get all the course for LP categories related to current user.

                if ($params->get('use_new_performance_method'))
                    $courses = JHelperLGT::learningProviderCourses($username, $malpcat);
                else
                    $courses = JoomdleHelperContent::call_method ('learning_provider_courses', $username, $malpcat);

                foreach ($courses['courses'] as $course) {
                    $result = LpApiController::isCourseInHika($course['id']);
                    if (!empty($result)) {
                        if ((int)$result->product_published == 1) {
                            $course['hikashopId'] = $result->product_id;
                            $course['hikashopAlias']=$result->product_alias;
                            $published[] = $course;
                        } else if ((int)$result->product_published == 0) {
                            if ($params->get('use_new_performance_method'))
                                $role = JHelperLGT::getUserRole(['id' => $course['id']]);
                            else
                                $role = JoomdleHelperContent::call_method ('get_user_role', (int)$course['id']);

                            $userroles = [];
                            foreach ($role['roles'] as $value) {
                                $decode_user_roles = json_decode( $value['role']);
                                foreach ($decode_user_roles as $dur) {
                                    $userroles[] = $dur->sortname;
                                }
                            }

                            if (!in_array('editingteacher', $userroles)) {
                                $course['hikashopId'] = $result->product_id;
                                $unpublished[] = $course;
                            }
                        }
                    }
                }
                $this->cursos = $published;
                $this->unpublishedcourse = $unpublished;

                parent::display('catalogue');
                break;
            case 'create':
                $this->action = "create";
                $catid = JRequest::getVar( 'cat_id' , null, 'NEWURLFORM');
                if (!$catid) $catid = JRequest::getVar( 'cat_id' );
                $this->categoryid = $catid;

                $typecourse = JRequest::getVar( 'type_course' , null, 'NEWURLFORM');
                if (!$typecourse) $typecourse = JRequest::getVar( 'type_course' );
                $this->type_course = $typecourse;

                $catArr = [];
                if ($params->get('use_new_performance_method'))
                    $mycategory = JHelperLGT::getMoodleCategories($username);
                else
                    $mycategory = JoomdleHelperContent::call_method('get_my_categories', $username);

                $categoryArr = array();
                if ($mycategory['status']) {
                    foreach ($mycategory['categories'] as $mycat) {
                        if ($catid && $catid > 0) {
                            if ($mycat['catid'] == $catid) {
                                $catArr[$mycat['catid']] = $mycat;
                            }
                        } else {
                            $catArr[$mycat['catid']] = $mycat;
                        }
                        $categoryArr[] = $mycat['catid'];
                    }
                }
                if (!in_array($catid, $categoryArr)) {
                    header('Location: /404.html');
                    exit;
                }

                $this->categories = $catArr;
                $this->wrapperurl = JUri::base()."courses/course/edit.php?category=".key($catArr);
                parent::display('create');
                break;
            case 'remove':
                $this->action = "remove";
                $courseid = JRequest::getVar( 'course_id' , null, 'NEWURLFORM');
                if (!$courseid) $courseid = JRequest::getVar( 'course_id' );

                $info = JoomdleHelperContent::call_method('get_course_info',(int)$courseid, $username);
                $course_name = $info['fullname'];
                $this->wrapperurl = JUri::base()."courses/course/delete.php?id=".$courseid;
                $this->course_id = $courseid;

                if (isset($_POST['act']) && $_POST['act'] == 'remove') {
                    $r = JoomdleHelperContent::call_method('remove_course', (int)$courseid, $username);
                    //delete notification
                    require_once(JPATH_ROOT.'/components/com_community/libraries/core.php');
                    $notifModel = CFactory::getModel('notification');
                    $ere = $notifModel->deleteNotificationcourse($course_name);
                    if ($r['res'] == 1) {
                        require_once(JPATH_ADMINISTRATOR.'/components/com_joomdle/helpers/groups.php');
                        JoomdleHelperSocialgroups::delete_group ($courseid);
                        $groups = CFactory::getModel('groups');
                        $groups->deleteCourseShareToGroup($courseid);
    
                        $result = array();
                        $result['error'] = 0;
                        $result['comment'] = 'Everything is fine';
                        $result['data'] = $r;
                    } else {
                        $result = array();
                        $result['error'] = 1;
                        $result['comment'] = 'Error.';
                    }

                    echo json_encode($result);
                    die;
                } else {
                    $this->course_info = JoomdleHelperContent::getCourseInfo($courseid, $user->username);
                }
                parent::display($tpl);
                break;
            case 'edit':
                $this->action = "edit";
                $isLP = 0;

                $courseid = JRequest::getVar( 'course_id' , null, 'NEWURLFORM');
                if (!$courseid) $courseid = JRequest::getVar( 'course_id' );

                $this->course_id = $courseid;

                $catid = JRequest::getVar( 'cat_id' , null, 'NEWURLFORM');
                if (!$catid) $catid = JRequest::getVar( 'cat_id' );

                $typecourse = JRequest::getVar( 'type_course' , null, 'NEWURLFORM');
                if (!$typecourse) $typecourse = JRequest::getVar( 'type_course' );
                $this->type_course = $typecourse;

                $this->course_info = JoomdleHelperContent::getCourseInfo($courseid, $user->username);
                
                // Check permission edit course
                if ($this->course_info['creator'] != $my->username) {
                    header('Location: /404.html');
                    exit;
                }
                
                if ($this->course_info['self_enrolment']) {
                    echo JText::_('COM_JOOMDLE_COURSE_PUBLISHED');
                    return;
                }

                $editorRoles = [];
                $editorIsContentCreator = false;
                if ($this->course_info['course_type'] == 'course_lp') {
                    $userRoles = json_decode($this->course_info['userRoles'], true)['roles'];
                    foreach ($userRoles as $key => $value) {
                        if ($value['username'] == $username) $editorRoles = json_decode($value['role'], true);
                    }
                    foreach ($editorRoles as $k => $v) {
                        if ($v['sortname'] == 'editingteacher') $editorIsContentCreator = true;
                    }

                }

                $catArr = [];
                if ($params->get('use_new_performance_method'))
                    $mycategory = JHelperLGT::getMoodleCategories($username);
                else
                    $mycategory = JoomdleHelperContent::call_method('get_my_categories', $username);

                if ($mycategory['status']) {
                    foreach ($mycategory['categories'] as $mycat) {
                        if ($catid && $catid > 0) {
                            if ($mycat['catid'] == $catid) {
                                $catArr[$mycat['catid']] = $mycat;
                            }
                        } else {
                            $catArr[$mycat['catid']] = $mycat;
                        }
                    }
                }

                if ($editorIsContentCreator && !isset($catArr[$this->course_info['cat_id']])) {
                    $catArr[$this->course_info['cat_id']] = ['catid'=>$this->course_info['cat_id'], 'name'=>$this->course_info['cat_name'], 'username'=>$username, 'role'=>'tempCC'];
                }

                require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
                $groups = CFactory::getModel('groups');
                $this->circleid = $groups->getLearningCircleId($this->course_info['remoteid']);
                $this->circle = $groups->getGroup($this->circleid);
                $params = json_decode($this->circle->params, true);
                $this->course_info['enableCreateChats'] = isset($params['enableCreateChats']) ? $params['enableCreateChats'] : 1;

                $this->categories = $catArr;
                $this->wrapperurl = JUri::base()."courses/course/edit.php?id=".$courseid;
                parent::display('create');
                break;
            case 'assign':
                $this->action = "assign";

                $courseid = JRequest::getVar( 'course_id' , null, 'NEWURLFORM');
                if (!$courseid) $courseid = JRequest::getVar( 'course_id' );

                $this->course_id = $courseid;
                $this->course_info = JoomdleHelperContent::getCourseInfo($courseid, $user->username);
                $this->hasPermission = JFactory::hasPermission($courseid, $username);
                if (!$this->hasPermission[0]['hasPermission']) {
                    header('Location: /404.html');
                    exit;
//                    echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//                    return;
                }

                if ($params->get('use_new_performance_method'))
                    $userRoles = JHelperLGT::getUserRole(['id' => $courseid]);
                else
                    $userRoles = JoomdleHelperContent::call_method('get_user_role', (int)$courseid);

                $enroledUsers = $userRoles['status'] ? $userRoles['roles'] : [];

                require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
                $jinput = $app->input;
                $my = CFactory::getUser();
                $searchQuery = $jinput->get('q', '', 'STRING');
                $id = $user->id ? $user->id : $my->id ;

                $friends = CFactory::getModel('friends');
                $sorted = $jinput->get->get('sort', 'latest', 'STRING');
                $filter = JRequest::getWord('filter', 'all', 'GET');

                if($searchQuery == ''){
                    $rows = $friends->getFriends($id, $sorted, true, $filter);
                }else{
                    $rows = $friends->getFriends($id, $sorted, true, 'friends', 0, $searchQuery);
                }

                $resultRows = array();
                $friendsName = array();

                foreach ($rows as $row) {
                    $obj = clone($row);
                    $obj->profileLink = CUrlHelper::userLink($row->id);
                    $obj->isFriend = true;
                    $resultRows[] = $obj;
                    $friendsName[] = $obj->username;
                }
                unset($rows);
                $this->friends = $resultRows;
                $this->enroledUsers = array();

                $oldData = array();
                foreach ($enroledUsers as $value) {
                    if (!in_array($value['username'], $friendsName)) continue;
                    $this->enroledUsers[$value['username']] = $value;

                    $roleData = json_decode($value['role']);
                    foreach ($roleData as $r) {
                        $ob = new stdClass();
                        $ob->frname = $value['username'];
                        $ob->rid = (int)$r->roleid;
                        $ob->status = 1;
                        $oldData[] = $ob;
                    }
                }

                if (isset($_POST['act']) && ($_POST['act'] == 'assign')) {
                    if ($_POST['act'] == 'assign') $act = 1;
                    $data = array();
                    foreach ($_POST['data'] as $k=>$v) {
                        $ob = new stdClass();
                        $ob->frname = JFactory::getUser($v['frid'])->username;
                        $ob->rid = (int)$v['rid'];
                        $ob->status = (int)$v['status'];
                        if (in_array($ob, $oldData)) continue;
                        $data[] = $ob;
                    }
                    if (!empty($data)) {
                        $data = json_encode($data);
                        $r = JoomdleHelperContent::call_method('set_user_role', (int)$act, (int)$courseid, $username, $data);
                    } else {
                        $r = null;
                    }

                    $result = array();
                    $result['error'] = 0;
                    $result['comment'] = 'Everything is fine';
                    $result['data'] = $r;
                    $result['error'] = $r['error'];
                    $result['comment'] = $r['mes'];
                    $result['data'] = $r['data'];
                    echo json_encode($result);
                    die;
                }

                JFactory::getDocument()->setTitle(JText::_('COM_JOOMDLE_ASSIGN_ROLES'));
                $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
                $this->_prepareDocument();

                parent::display('assign');
                break;
            case 'lpassign':
                $this->action = "lpassign";

                $moodlecategoryid = JRequest::getVar( 'course_id' , null, 'NEWURLFORM');
                if (!$moodlecategoryid) $moodlecategoryid = JRequest::getVar( 'moodlecategoryid' );

                $this->moodlecategoryid = $moodlecategoryid;
                $this->moodle_category_info = JoomdleHelperContent::call_method('get_user_category_role', (int)$moodlecategoryid, $user->username, 1);

                $hasPer = false;
                $this->roleidArr = array();

                $roles = json_decode($this->moodle_category_info['roles'], true);
                foreach ($roles as $key => $value) {
                    if ($value['rolename'] == 'editingteacher') $this->roleidArr[] = $value['username'];
                }

                foreach ($this->moodle_category_info['role'] as $r) {
                    if ($r['rolename'] == 'manager') $hasPer = true;
                }

                if (!$hasPer) {
                    header('Location: /404.html');
                    exit;
//                    echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//                    return;
                }

                require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
                $jinput = $app->input;
                $my = CFactory::getUser();
                $searchQuery = $jinput->get('q', '', 'STRING');
                $id = $user->id ? $user->id : $my->id ;

                $friends = CFactory::getModel('friends');
                $sorted = $jinput->get->get('sort', 'latest', 'STRING');
                $filter = JRequest::getWord('filter', 'all', 'GET');

                if($searchQuery == ''){
                    $rows = $friends->getFriends($id, $sorted, true, $filter);
                }else{
                    $rows = $friends->getFriends($id, $sorted, true, 'friends', 0, $searchQuery);
                }

                $resultRows = array();
                $friendsName = array();

                foreach ($rows as $row) {
                    $obj = clone($row);
                    $obj->profileLink = CUrlHelper::userLink($row->id);
                    $obj->isFriend = true;
                    $resultRows[] = $obj;
                    $friendsName[] = $obj->username;
                }
                unset($rows);
                $this->friends = $resultRows;

                if (isset($_POST['act']) && ($_POST['act'] == 'assign')) {
                    if ($_POST['act'] == 'assign') $act = 1;
                    $data = array();
                    foreach ($_POST['data'] as $k=>$v) {
                        $ob = new stdClass();
                        $ob->user = JFactory::getUser($v['frid'])->username;
                        $ob->roleid = (int)$v['rid'];
                        $ob->status = (int)$v['status'];
                        $data[] = $ob;
                    }
                    if (!empty($data)) {
                        $data = json_encode($data);
                        $r = JoomdleHelperContent::call_method('set_user_category_role', $username, (int)$moodlecategoryid, $data);
                    } else {
                        $r = null;
                    }

                    $result = array();
                    $result['error'] = $r['error'];
                    $result['comment'] = $r['mes'];
                    $result['data'] = $r['data'];
                    echo json_encode($result);
                    die;
                }

                JFactory::getDocument()->setTitle(JText::_('COM_JOOMDLE_ASSIGN_ROLES'));
                $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
                $this->_prepareDocument();

                parent::display('lpassign');
                break;
            case 'sendapprove':
                $this->action = "sendapprove";
                $courseid = JRequest::getVar( 'course_id' , null, 'NEWURLFORM');
                $categoryid = JRequest::getVar('cat_id', null, "NEWURLFORM");
                if (!$courseid) $courseid = JRequest::getVar( 'course_id' );
                $this->course_id = $courseid;
                $this->hasPermission = JFactory::hasPermission($courseid, $username);
                if (!$this->hasPermission[0]['hasPermission']) {
                    header('Location: /404.html');
                    exit;
//                    echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//                    return;
                }
                if (isset($_POST['action']) && $_POST['action'] == 'sendapprove') {
                    $r = JoomdleHelperContent::call_method('send_course_for_approve', (int)$courseid, $username, (int)$categoryid);

                    if ($r['status'] == 1) {
                        require_once(JPATH_SITE.'/components/com_hikashop/helpers/lpapi.php');
                        $res = array();
                        if (!empty(LpApiController::isCourseInHika($courseid))) {
                            $res = LpApiController::removeCourseFromHikashop($courseid);
                        }

                        $result = $res;
                        $result['error'] = 0;
                        $result['message'] = $r['message'];
                    } else {
                        $result = array();
                        $result['error'] = 1;
                        $result['message'] = $r['message'];
                    }

                    echo json_encode($result);
                    die;
                } else {
                    $this->course_info = JoomdleHelperContent::getCourseInfo($courseid, $user->username);
                }
                parent::display($tpl);
                break;
            case 'approval':
                $this->action = "approval";
                $courseid = JRequest::getVar( 'course_id' , null, 'NEWURLFORM');
                $categoryid = JRequest::getVar('cat_id', null, "NEWURLFORM");
                if (!$courseid) $courseid = JRequest::getVar( 'course_id' );
                $this->course_id = $courseid;
                $this->hasPermission = JFactory::hasPermission($courseid, $username);
                if (!$this->hasPermission[0]['hasPermission']) {
                    header('Location: /404.html');
                    exit;
//                    echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//                    return;
                }
                if (isset($_POST['act']) && ($_POST['act'] == 'approve' || $_POST['act'] == 'unapprove')) {
                    $r = JoomdleHelperContent::call_method('approve_or_unapprove_course', $username, (int)$courseid, (int)$categoryid, $_POST['act']);

                    if ($r['status'] == 1) {
                        $result = array();
                        $result['error'] = 0;
                        $result['message'] = $r['message'];
                    } else {
                        $result = array();
                        $result['error'] = 1;
                        $result['message'] = $r['message'];
                    }

                    echo json_encode($result);
                    die;
                } else {
                    $this->course_info = JoomdleHelperContent::getCourseInfo($courseid, $user->username);
                }
                parent::display($tpl);
                break;
            case 'publish':
                $this->action = "publish";
                $courseid = JRequest::getVar( 'course_id' , null, 'NEWURLFORM');
                $categoryid = JRequest::getVar('cat_id', null, "NEWURLFORM");
                if (!$courseid) $courseid = JRequest::getVar( 'course_id' );
                if (!$categoryid) $categoryid = JRequest::getVar( 'cat_id' );
                $this->course_id = $courseid;
                $this->hasPermission = JFactory::hasPermission($courseid, $username);
                if (!$this->hasPermission[0]['hasPermission']) {
                    header('Location: /404.html');
                    exit;
//                    echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//                    return;
                }

                require_once(JPATH_SITE.'/components/com_hikashop/helpers/lpapi.php');
                $categories = LpApiController::getcategorylist($courseid);

                $this->categories = $categories;
                $this->publishedCats = array();

                if (isset($_POST['act']) && $_POST['act'] == 'publishtohikashop') {
                    $data = array();
                    $hikashopcatids = implode(',', $_POST['data']);
                    $r = JoomdleHelperContent::call_method('publish_unpublish_course', (int)$categoryid, (int)$courseid, $_POST['action'], $username, $hikashopcatids);

                    if ($r['status'] == 1) {
                        $result = array();
                        $result['error'] = 0;
                        $result['message'] = $r['message'];
                    } else {
                        $result = array();
                        $result['error'] = 1;
                        $result['message'] = $r['message'];
                    }

                    echo json_encode($result);
                    die;
                } else {
                    $this->course_info = JoomdleHelperContent::getCourseInfo($courseid, $user->username);
                    require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
                    $groups = CFactory::getModel('groups');
                    $this->groupid = (int)$groups->getLPGroupId($this->course_info['cat_id']);
                }

                JFactory::getDocument()->setTitle(JText::_('COM_JOOMDLE_CHOOSE_CATEGORY'));
                $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
                $this->_prepareDocument();

                parent::display('publish');
                break;
            case 'share':
                $this->action = "share";

                $courseid = JRequest::getVar( 'course_id' , null, 'NEWURLFORM');
                if (!$courseid) $courseid = JRequest::getVar( 'course_id' );

                $this->course_id = $courseid;
                $courseinfo = JoomdleHelperContent::getCourseInfo($courseid, $user->username);
                $this->course_info = $courseinfo;
                $this->hasPermission = JFactory::hasPermission($courseid, $username);
                if (!$this->hasPermission[0]['hasPermission'] && $this->course_info['creator'] != $username) {
                    header('Location: /404.html');
                    exit;
//                    echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//                    return;
                }

                require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
                $my = CFactory::getUser();
                $config = CFactory::getConfig();
                $id = $user->id ? $user->id : $my->id ;

                if ($device == 'mobile') {
                    $limit = (int) JText::_('COM_JOOMDLE_CIRCLE_LIMIT_MOBILE');
                } else {
                    $limit = (int) JText::_('COM_JOOMDLE_CIRCLE_LIMIT');
                }
                
                $groups = CFactory::getModel('groups');
                $lpcircle = 7;
                $socialcircle = 0;
                $countlpcircles =  $groups->getCirclesCount($id, $lpcircle, false, true);
                $mygroups =  $groups->getCircles($id, true, 0, $limit, $lpcircle, true);             
                $learningProviderGroups = array();
                $learningProviderGroupsIds = array();
                $this->learningProviderGroups = array();
                $this->countmylpcircles = (int)$countlpcircles;   

                foreach ($mygroups as $value) {
                    if (isset($value->moodlecategoryid) && $value->moodlecategoryid != 0) {
                        $group = JTable::getInstance('Group', 'CTable');
                        $group->bind($value);
                        $group->updateStats(); //ensure that stats are up-to-date
                        $group->description = CStringHelper::clean(JHTML::_('string.truncate', $group->description, $config->get('tips_desc_length')));
                        $learningProviderGroups[$group->id] = $group;
                        $learningProviderGroupsIds[] = $group->id;
                        if ($value->ownerid == $user->id) $this->learningProviderGroups[] = $group;
                    }
                }
                
                $countmycircle = $groups->getCirclesCount($id, $socialcircle, 'owner');
                $tmpGroups = $groups->getCircles($id, true, 0, $limit, $socialcircle, 'owner');
                $groups_arr = array();
                $mygroupids = array();

                if ($tmpGroups) {
                    foreach ($tmpGroups as $row) {
                        if (in_array($row->id, $learningProviderGroupsIds)) continue;
                        $pr = json_decode($row->params);
                        if (isset($pr->course_id) && $pr->course_id > 0) continue;
                        $group = JTable::getInstance('Group', 'CTable');
                        $group->bind($row);
                        $group->updateStats(); //ensure that stats are up-to-date
                        $group->description = CStringHelper::clean(JHTML::_('string.truncate', $group->description, $config->get('tips_desc_length')));
                        $groups_arr[] = $group;
                        $mygroupids[] = $group->id;
                    }
                    unset($tmpGroups);
                }

                $this->groups = $groups_arr;
                $this->countmycircles = (int)$countmycircle;   
                $this->limit = $limit;
                $this->sharedGroups = array();

                $sharedGroups = $groups->getShareGroups($courseid);
                $oldData = array();
                foreach ($sharedGroups as $value) {
                    if (!in_array($value->groupid, $mygroupids)) continue;

                    $oldData[] = $value->groupid;
                }
                $this->sharedGroups = $oldData;

                if (isset($_POST['act']) && ($_POST['act'] == 'sharetocircle')) {
                    if ($_POST['act'] == 'sharetocircle') $act = 1;
                    $data = array();
                    $LPCIsSelected = array();
                    foreach ($_POST['data'] as $k=>$v) {
                        if ($v['grid'] && (int)$v['status'] == 0) {
                            $groups->deleteCourseShareToGroup($courseid, $v['grid']);
                        }
                        if ($v['grid'] && (int)$v['status'] == 1) {
                            if (in_array($v['grid'], $learningProviderGroupsIds)) {
                                if (isset($learningProviderGroups[$v['grid']])) {
                                    $LPCIsSelected[] = $learningProviderGroups[$v['grid']]->moodlecategoryid;
                                }
                            } else {
                            $check = $groups->getShareGroups($courseid, $v['grid']);
                            if (!$check) {
                                $groups->courseShareToGroup($courseid, $v['grid']);
                            }
                        }
                    }
                    }
                    if (!empty($LPCIsSelected)) {
                        $r = JoomdleHelperContent::call_method('change_course_into_lpcourse', (int)$courseid, json_encode($LPCIsSelected), $username);
                    } else
                        $r = JoomdleHelperContent::call_method('course_enable_self_enrolment', (int)$courseid, 'enable', $username);

                    $result = array();
                    if ($r['status'] == 1) {
                        $result['error'] = 0;
                    } else {
                        $result['error'] = 1;
                    }
                        $result['message'] = $r['message'];
                    $result['data'] = isset($r['data']) ? $r['data'] : '';
                    echo json_encode($result);
                    die;
                }

                JFactory::getDocument()->setTitle(JText::_('COM_JOOMDLE_CHOOSE_CIRCLE'));
                $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
                $this->_prepareDocument();

                parent::display('share');
                break;
            case 'copy':        // Duplicate course
                $this->action = "copy";
                $courseid = JRequest::getVar( 'course_id' , null, 'NEWURLFORM');
                if (!$courseid) $courseid = JRequest::getVar( 'course_id' );
                $this->course_id = $courseid;
                $this->hasPermission = JFactory::hasPermission($courseid, $username);
                if (!$this->hasPermission[0]['hasPermission']) {
                    header('Location: /404.html');
                    exit;
//                    echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
//                    return;
                }
                if (isset($_POST['action']) && $_POST['action'] == 'copy') {
                    $r = JoomdleHelperContent::duplicateCourse((int)$courseid, $username);

                    if ($r->status == 1) {
                        $result = array();
                        $result['error'] = 0;
                        $result['message'] = $r->message;
                    } else {
                        $result = array();
                        $result['error'] = 1;
                        $result['message'] = $r->message;
                    }

                    echo json_encode($result);
                    die;
                } else {
                    $this->course_info = JoomdleHelperContent::getCourseInfo($r['course']['id'], $user->username);
                }
                parent::display($tpl);
                break;
            case 'choosecirlce':    // Choose circle when add new course
                $this->action = "choosecirlce";

                require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
                $my = CFactory::getUser();
                $config = CFactory::getConfig();
                $id = $user->id ? $user->id : $my->id ;

                $groups = CFactory::getModel('groups');
                $lpcircle = 7;
                $mygroups =  $groups->getCircles($id, 0, 0, 15, $lpcircle);
                $learningProviderGroups = array();
                $learningProviderGroupsIds = array();
                $this->learningProviderGroups = array();

                foreach ($mygroups as $value) {
                    if (isset($value->moodlecategoryid) && $value->moodlecategoryid != 0) {
                        $group = JTable::getInstance('Group', 'CTable');
                        $group->bind($value);
                        $group->updateStats(); //ensure that stats are up-to-date
                        $group->description = CStringHelper::clean(JHTML::_('string.truncate', $group->description, $config->get('tips_desc_length')));
                        $learningProviderGroups[$group->id] = $group;
                        $learningProviderGroupsIds[] = $group->id;
                        $this->learningProviderGroups[] = $group;
                    }
                }

                JFactory::getDocument()->setTitle(JText::_('COM_JOOMDLE_CHOOSE_CIRCLE'));
                $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
                $this->_prepareDocument();

                parent::display('lpcircle');
                break;
            default:
                JFactory::getDocument()->setTitle(JText::_('COM_JOOMDLE_COURSES'));

                if (!$params->get('use_new_performance_method') || ($params->get('use_new_performance_method') && JHelperLGT::countMyOwnCourses() <= 10)) {
                    echo $this->showDefaultPage($tpl);
                } else {
                    JFactory::getDocument()->addStyleSheet(JUri::base().'components/com_community/assets/release/css/placeholder-loading.min.css');
                    echo $this->showDefaultPage($tpl, true);
                }

                break;
        }
    }

    function showDefaultPage($tpl = null, $useSample = false) {
        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        $username = $user->username;
        $params = $app->getParams();

        $this->assign('useSample', $useSample);

        if ($useSample) {
            $this->malpcat = [];
            $this->lpcat = [];
            $this->noncat = [];

            $this->isContentCreator = [];
            $this->cursos = [];

            $this->pendingco = [];
            $this->approved = [];
            $this->macourse = [];
            $this->workinprogess = [];
        } else {
            if ($params->get('use_new_performance_method'))
                $manageData = JHelperLGT::getManagePageData();
            else
                $manageData = JoomdleHelperContent::call_method('get_manage_page_data', $username);

            $mycategory = json_decode($manageData['mycategory'], true);
            $cursos = json_decode($manageData['cursos'], true);
            $courses_app = json_decode($manageData['courses_app'], true);
            $macourses = json_decode($manageData['macourses'], true);
            $courses = json_decode($manageData['courses'], true);

            $noncat_arr = array();
            $macat_arr = array();
            $lpcat_arr = array();
            $malpcat_arr = array();
            if ($mycategory['status']) {
                foreach ($mycategory['categories'] as $mycat) {
                    if ($mycat['role'] == 'LP') {
                        $lpcat_arr[] = $mycat['catid'];
                        $malpcat_arr[] = $mycat['catid'];
                    } else if ($mycat['role'] == 'CMA') {
                        $macat_arr[] = $mycat['catid'];
                        $malpcat_arr[] = $mycat['catid'];
                    } else if ($mycat['role'] == 'non-cat') {
                        $noncat_arr[] = $mycat['catid'];
                    }
                }
            }
            $this->malpcat = $malpcat_arr;
            $this->lpcat = $lpcat_arr;
            $this->noncat = $noncat_arr;

            $this->isContentCreator = ($courses['courses'] != null) ? true : false;
            $this->cursos = $cursos['status'] ? $cursos['courses'] : [];

            $pending = array();
            $approved = array();
            $couridapp = array();
            if (isset($courses_app['status']) && $courses_app['status']) {
                foreach ($courses_app['courses'] as $co) {
                    if ($co['pending_approve'] || $co['approved'])
                        $couridapp[] = $co['id'];
                    if ($co['pending_approve']) {
                        $pending[] = $co;
                    }
                    if ($co['approved']) {
                        $approved[] = $co;
                    }
                }
            }

            $this->pendingco = $pending;
            $this->approved = $approved;

            $ma_courses = array();
            $workinprogress = array();
            $cur_coursesid = array();
            if (isset($macourses['courses']) && count($macourses['courses']) > 0) {
                foreach ($macourses['courses'] as $course) {
                    if ($course['has_nonmember'] == 0 && $course['creator'] == $username) {
                        $ma_courses[] = $course;
                    } else {
                        if ((!in_array($course['id'], $couridapp)) && ($course['has_nonmember'] == 1)) {
                            $workinprogress[] = $course;
                            $cur_coursesid[] = $course['id'];
                        }
                    }
                }
            }
            if (!empty($malpcat_arr)) {
                $this->macourse = $macourses['status'] ? $ma_courses : [];
            }

            if (count($courses['courses']) > 0) {
                foreach ($courses['courses'] as $cou) {
                    if (!in_array($cou['id'], $cur_coursesid))
                        $workinprogress[] = $cou;
                }
            }

            $this->workinprogess = $workinprogress;
        }

        return $this->loadTemplate($tpl);
    }

    protected function _prepareDocument() {
        $app    = JFactory::getApplication();
        $menus  = $app->getMenu();
        $title  = null;

        $menu = $menus->getActive();
        if ($menu) {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        } else {
            $this->params->def('page_heading', JText::_('COM_JOOMDLE_MY_COURSES'));
        }
    }

}
?>