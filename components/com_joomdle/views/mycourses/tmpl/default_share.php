<?php
defined('_JEXEC') or die('Restricted access');

$numpages = 0;
?>
<div class="joomdle-add-to-circle">
    <p class="page-title"><?php echo JText::_('COM_JOOMDLE_TITLE_ADD_TO_CIRCLE'); ?></p>
    <!--<p class="message-share"><?php // echo JText::_('COM_JOOMDLE_MES_SHARE'); ?></p>-->
    <div class="tab_typeCircles">
        <?php if (count($this->groups) > 0 && count($this->learningProviderGroups) > 0) { ?>
        <span class="left myCircles active" onclick="showTab(1);">
            <?php echo JText::_('MY_CIRCLES'); ?>
        </span>
        <span class="right myLearningProvider" onclick="showTab(2);">
            <?php echo JText::_('COM_JOOMDLE_MY_LEARNING_PROVIDER'); ?>
        </span>
        <?php } else if (count($this->groups) <= 0 && count($this->learningProviderGroups) > 0) { ?>
        <span class="alone myLearningProvider" onclick="showTab(2);">
            <?php echo JText::_('COM_JOOMDLE_MY_LEARNING_PROVIDER'); ?>
        </span>
        <?php } else { ?>
        <span class="alone myCircles active" onclick="showTab(1);">
            <?php echo JText::_('MY_CIRCLES'); ?>
        </span>
        <?php } ?>
    </div>
    <div class="joomdle-add-to-circle-my-circles active" id="tab1">
        <div class="listMyCircles">
        <?php
        $gridArr = array();
        if (!empty($this->sharedGroups)) {
            $gridArr = $this->sharedGroups;
        }
        if (empty($this->groups)) {
            echo '<p>'.JText::_('COM_JOOMDLE_NO_CIRCLE').'</p>';
        } else {
            $numpages = ceil($this->countmycircles/$this->limit);
            foreach ($this->groups as $group) {
                $table = JTable::getInstance('Group', 'CTable');
                $table->load($group->id);
                ?>
                <div class="group-circle cirid_<?php echo $group->id;?> <?php echo (in_array($group->id, $gridArr)) ? 'active' : '';?>" data-grid="<?php echo $group->id;?>" onclick="choiceCircle(<?php echo $group->id;?>, 'social');">
                    <div class="checkbox-circle">
                        <div class="status"></div>
                    </div>
                    <div class="information">
                        <div class="group-picture">
                            <img src="<?php echo $table->getAvatar(); ?>"/>
                        </div>
                        <div class="group-info">
                            <p class="circle-name"><?php if (strlen($group->name) >= 60) echo substr_replace($group->name, '...', 60); else echo $group->name; ?></p>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
        ?>
        </div>
        <?php if ($this->countmycircles > $this->limit) { ?>
        <div class="loadMore"><span onclick="loadmore('<?php echo JText::_('COM_JOOMDLE_TYPE_LOAD_MYCIRCLES');?>');"><?php echo JText::_('COM_JOOMDLE_SEE_MORE');?></span></div>
        <?php } ?>
        <button class="btCancel" type="button"><?php echo Jtext::_('MYCOURSE_BTN_CANCEL'); ?></button>
        <button class="btAddToCircle" <?php echo (empty($this->groups)) ? 'data-disabled="1"' : '';?> type="button"><?php echo JText::_('COM_JOOMDLE_BT_ADD_TO_CIRCLE'); ?></button>
    </div>
    <div class="joomdle-add-to-circle-learning-provider-circles" id="tab2">
        <div class="listLPMyCircles">
        <?php
        $gridArr = array();
        if (!empty($this->sharedGroups)) {
            $gridArr = $this->sharedGroups;
        }
        if (empty($this->learningProviderGroups)) {
            echo '<p>'.JText::_('COM_JOOMDLE_NO_CIRCLE').'</p>';
        } else {
            $numpages = ceil($this->countmylpcircles/$this->limit);
            foreach ($this->learningProviderGroups as $group) {
                $table = JTable::getInstance('Group', 'CTable');
                $table->load($group->id);
                ?>
                    <div class="group-circle cirid_<?php echo $group->id;?> <?php echo (in_array($group->id, $gridArr)) ? 'active' : '';?>" data-grid="<?php echo $group->id;?>"  onclick="choiceCircle(<?php echo $group->id;?>, 'lp');">
                    <div class="checkbox-circle">
                        <div class="status"></div>
                    </div>
                    <div class="information">
                        <div class="group-picture">
                            <img src="<?php echo $table->getAvatar(); ?>"/>
                        </div>
                        <div class="group-info">
                            <p class="circle-name"><?php echo $group->name; ?></p>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
        ?>
        </div>
        <?php if ($this->countmylpcircles > $this->limit) { ?>
        <div class="loadMore"><span onclick="loadmore('<?php echo JText::_('COM_JOOMDLE_TYPE_LOAD_MYLPCIRCLES');?>');"><?php echo JText::_('COM_JOOMDLE_SEE_MORE');?></span></div>
        <?php } ?>
        <button class="btCancel" type="button"><?php echo Jtext::_('MYCOURSE_BTN_CANCEL'); ?></button>
        <button class="btAddToCircle" <?php echo (empty($this->learningProviderGroups)) ? 'data-disabled="1"' : '';?> type="button"><?php echo JText::_('COM_JOOMDLE_BT_ADD_TO_CIRCLE'); ?></button>
    </div>
</div>
<div class="notification"></div>
<div class="modal" id="ass-message">
    <div class="modal-content hienPopup">
        <p id="error-hv"></p>
            <button class="closetitle" ><?php echo Jtext::_('COM_JOOMDLE_CLOSE'); ?></button>
    </div>
</div>
<script type="text/javascript">
    (function($) {
        $('.btAddToCircle').click(function() {
            var modal = document.getElementById('ass-message');
            var er = document.getElementById('error-hv');
            $('.closetitle').click(function(){
                modal.style.display = "none"; 
                $('body').removeClass('overlay2');
            });
            if ($(this).attr('data-disabled') != 1) {
                var data = [];
                var act = 'sharetocircle';
                var i = 0;
                $('.group-circle').each(function() {
                    var rc = {};
                    rc.grid = $(this).attr('data-grid');
                    if ($(this).hasClass('active')) {
                        rc.status = 1;
                        i++;
                    } else rc.status = 0;
                    data.push(rc);
                });
                if (i == 0) {
                    $('body').addClass('overlay2');
                    er.innerHTML = '<?php echo JText::_("NOTIF_SELECT_CIRCLE");?>';
                    modal.style.display = "block";

                    //$('.lgt-notif-error').html('<?php echo JText::_("NOTIF_SELECT_CIRCLE");?>').addClass('lgt-visible');
                } else
                    $.ajax({
                        url: window.location.href,
                        type: 'POST',
                        data: {
                            act: act,
                            data: data
                        },
                        beforeSend: function() {
                            $('body').addClass('overlay2');
                            $('.notification').html('Loading...').fadeIn();
                            //$('.lgt-notif').html('<?php echo JText::_("REQUEST_PROCESSED");?>').addClass('lgt-visible');
                        },
                        success: function(data, textStatus, jqXHR) {
                            var res = JSON.parse(data);
                           
                            $('body').removeClass('overlay2'); 
                            $('.notification').fadeOut();
                           // $('.lgt-notif').removeClass('lgt-visible');
                            console.log(res);
                            if (res.error == 1) {
                                er.innerHTML = res.message;
                                 modal.style.display = "block";
                                //$('.lgt-notif-error').html(res.message).addClass('lgt-visible');
                            } else {
                                window.location.href = '<?php echo JUri::base(); ?>mycourses/list.html';
                            }
                        }
                    });
            }
        });
        $('.btCancel').click(function () {
            window.location.href = '<?php echo JURI::base() . 'mycourses/list.html';?>';
        });
    })(jQuery);
    
    function choiceCircle( circleid, type ) {
        if (type == 'social') {
            if (jQuery('.group-circle.cirid_'+circleid).hasClass('active')) {
                jQuery('.joomdle-add-to-circle-my-circles .group-circle').removeClass('active');
            }else{
                jQuery('.joomdle-add-to-circle-my-circles .group-circle').removeClass('active');
                jQuery('.joomdle-add-to-circle-my-circles .cirid_'+circleid).addClass('active');
            }
            var myCirclesIsChosen = false;
            jQuery('.joomdle-add-to-circle-my-circles .group-circle').each(function() {
                if (jQuery(this).hasClass('active')) {
                    myCirclesIsChosen = true;
                }
            });
            if (myCirclesIsChosen) {
                jQuery('.joomdle-add-to-circle-learning-provider-circles').addClass('overlay');
                jQuery('.joomdle-add-to-circle-learning-provider-circles .group-circle').removeClass('active');
            } else {
                jQuery('.joomdle-add-to-circle-learning-provider-circles').removeClass('overlay');
                jQuery('.joomdle-add-to-circle-learning-provider-circles .group-circle').removeClass('active');
            }

        } else if (type == 'lp') {
            if (jQuery('.group-circle.cirid_'+circleid).hasClass('active')) {
                jQuery('.joomdle-add-to-circle-learning-provider-circles .group-circle').removeClass('active');
                jQuery('.joomdle-add-to-circle-my-circles').removeClass('overlay');
                jQuery('.joomdle-add-to-circle-my-circles .group-circle').removeClass('active');
            } else {
                jQuery('.joomdle-add-to-circle-learning-provider-circles .group-circle').removeClass('active');
                jQuery('.group-circle.cirid_'+circleid).addClass('active');
                jQuery('.joomdle-add-to-circle-my-circles').addClass('overlay');
                jQuery('.joomdle-add-to-circle-my-circles .group-circle').removeClass('active');
            }
        }
    }
    
    function showTab(tabNum) {
        if (tabNum == 1) {
            jQuery('.myCircles').addClass('active');
            jQuery('.myLearningProvider').removeClass('active');
            jQuery('#tab1').addClass('active');
            jQuery('#tab2').removeClass('active');
        }
        if (tabNum == 2) {
            jQuery('.myLearningProvider').addClass('active');
            jQuery('.myCircles').removeClass('active');
            jQuery('#tab2').addClass('active');
            jQuery('#tab1').removeClass('active');
        }
    }
    
    var track_load = 2;
    var total_page = <?php echo $numpages;?>;
    function loadmore(type) {
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: {
                act: "load_more",
                type: type,
                page: track_load
            },
            beforeSend: function () {
                jQuery(".loadMore").hide();
                if (type == '<?php echo JText::_('COM_JOOMDLE_TYPE_LOAD_MYCIRCLES');?>') {
                    jQuery('.joomdle-add-to-circle-my-circles.active').append('<div class="divLoadMore">Loading...<div class="loading"></div></div>');
                }    
                if (type == '<?php echo JText::_('COM_JOOMDLE_TYPE_LOAD_MYLPCIRCLES');?>') {
                    jQuery('.joomdle-add-to-circle-learning-provider-circles.active').append('<div class="divLoadMore">Loading...<div class="loading"></div></div>');
                }    
            },
            success: function (data, textStatus, jqXHR) {
                var res = JSON.parse(data);
                var b = res.circles;

                jQuery.each(b, function (key, value) {
                    var id = b[key]['id'];
                    var name = b[key]['name'];
                    var avatar = b[key]['avatar'];
                    
                    var html;
                        html = '<div class="group-circle cirid_' + id + '" data-grid="' + id + '" onclick="choiceCircle(' + id + ', \'social\');">' +
                            '<div class="checkbox-circle">' +
                            '<div class="status"></div>' +
                            '</div>' +
                            '<div class="information">' +
                            '<div class="group-picture">' +
                            '<img src="' + avatar + '"/>' +
                            '</div>' +
                            '<div class="group-info">' +
                            '<p class="circle-name">' + name + '</p>' +
                            '</div>' +
                            '</div>' +
                            '</div>';

                    jQuery('.divLoadMore').remove();
                    if (type == '<?php echo JText::_('COM_JOOMDLE_TYPE_LOAD_MYCIRCLES');?>') {
                        jQuery('.listMyCircles').append(html);
                    } else if (type == '<?php echo JText::_('COM_JOOMDLE_TYPE_LOAD_MYLPCIRCLES');?>') {
                        jQuery('.listMyLPCircles').append(html);
                    }
                });
                track_load++; //loaded group increment
                if (track_load > total_page) {
                    jQuery(".loadMore").hide();
                } else {
                    jQuery(".loadMore").show();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
                loading = false;
            }
        });
    }
</script>
