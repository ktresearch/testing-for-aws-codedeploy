<?php
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root().'components/com_joomdle/views/mycourses/tmpl/mycourses.css');

$course = $this->course_info;
?>
<div class="joomdle-publish-to-hikashop">
    <p class="title-course"><?php echo $course['fullname']?></p>
    <p class="tip-course"><?php echo JText::_('COM_JOOMDLE_CHOOSE_A_CATEGORY'); ?></p>

    <?php foreach ($this->categories as $cat) { ?>
        <div class="group-circle <?php echo ($cat->isSelected) ? 'active' : '';?>" data-grid="<?php echo $cat->category_id;?>">
            <div class="checkbox-circle">
                <div class="status"></div>
            </div>
            <div class="information">
                <?php echo $cat->category_name; ?>
            </div>
        </div>
    <?php } ?>

    <div class="terms_and_policies">
        <div class="checkbox-circle">
            <div class="status"></div>
        </div>
        <div class="information">
            <?php echo JText::_('COM_JOOMDLE_TERMS_AND_POLICIES_PUBLISH_STORE'); ?>
        </div>
    </div>

    <button class="btPublishToHikashop" disabled="disabled" type="button" onclick="publish(<?php echo $this->course_id.','.$course['cat_id'].", 'publish'";?>);"><?php echo JText::_('COM_JOOMDLE_BUTTON_PUBLISH'); ?></button>
</div>
<div class="notification"></div>
 <div class="modal" id="ass-message">
     <div class="modal-content hienPopup">
         <p id="error-hv"></p>
             <button class="closetitle" ><?php echo Jtext::_('COM_JOOMDLE_CLOSE'); ?></button>
     </div>
</div>
<script type="text/javascript">
    function publish(courseid, catid, action) {

        var modal = document.getElementById('ass-message');
        var er = document.getElementById('error-hv');
        jQuery('.closetitle').click(function(){
            modal.style.display = "none"; 
        })
        var url = "<?php echo JURI::base();?>index.php?option=com_joomdle&view=mycourses&action=publish&course_id="+courseid+"&catid="+catid;//"<?php // echo JURI::base();?>mycourses/publish_"+courseid+"_"+catid+"_.html";
        var action = action;
        var data = [];
        var act = 'publishtohikashop';
        var i = 0;
        jQuery('.group-circle').each(function() {
            var grid = 0;

            if (jQuery(this).hasClass('active')) {
                grid = jQuery(this).attr('data-grid');
                data.push(grid);
                i++;
            }
        });
        if (i == 0) {
            er.innerHTML = '<?php echo JText::_("NOTIF_SELECT_CIRCLE");?>';
            modal.style.display = "block";
            //jQuery('.lgt-notif-error').html('<?php echo JText::_("NOTIF_SELECT_CIRCLE");?>').addClass('lgt-visible');
        } else
            jQuery.ajax({
                url: url,
                type: 'POST',
                data: {
                    course_id: courseid,
                    action: action,
                    cat_id: catid,
                    act: act,
                    data: data
                },
                beforeSend: function () {
                    jQuery('body').addClass('overlay2');
                    jQuery('.notification').html('Loading...').fadeIn();
                    //jQuery('.lgt-notif').html('<?php echo JText::_("REQUEST_PROCESSED");?>').addClass('lgt-visible');

                },
                success: function (data, textStatus, jqXHR) {
                    var res = JSON.parse(data);
                    jQuery('body').removeClass('overlay2'); 
                    jQuery('.notification').fadeOut();
                    //jQuery('.lgt-notif').removeClass('lgt-visible');
                    if (res.error == 1) {
                        er.innerHTML = res.message;
                        modal.style.display = "block";
                        //jQuery('.lgt-notif-error').html(res.message).addClass('lgt-visible');
                    } else {
                        console.log(res.message);
                        window.location.href = "<?php
                            if (strpos($_SERVER['HTTP_REFERER'], 'circle') !== false && strpos($_SERVER['HTTP_REFERER'], JURI::base()) !== false) {
                                echo JURI::base().'circles/viewabout?groupid='.$this->groupid;
                            } else 
                            echo JURI::base().'mycourses/catalogue.html';
                            ?>";
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                }
            });
    }
    (function($) {
        $('.group-circle').click(function() {
            $(this).toggleClass('active');
        });

        $('.terms_and_policies').click(function () {
            $(this).toggleClass('active');
            if(!$(this).hasClass('active')){
                $('.btPublishToHikashop').attr("disabled", "disabled");
            }else{
                $('.btPublishToHikashop').removeAttr("disabled");
            }
        });
    })(jQuery);
</script>