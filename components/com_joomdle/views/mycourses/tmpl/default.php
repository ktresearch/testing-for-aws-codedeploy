<?php defined('_JEXEC') or die('Restricted access');

require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
JLoader::import('joomla.user.helper');

$itemid = JoomdleHelperContent::getMenuItem();

$session = JFactory::getSession();
$device = $session->get('device');

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . '/components/com_joomdle/css/swiper.min.css');
$document->addScript(JUri::root() . '/components/com_joomdle/js/swiper.min.js');
$document->addStyleSheet(JUri::root() . 'components/com_joomdle/views/mycourses/tmpl/mycourses.css');

$class_tablet = 'course-box';
JPluginHelper::importPlugin('joomdlesocialgroups');
$dispatcher = JDispatcher::getInstance();
?>
<!--    TABLET-->
<div class="joomdle-mylearning">
    <div class="joomdle_mylearning_header changePosition">
        <?php if (count($this->malpcat) <= 0 && !$this->useSample) { ?>
            <a href="<?php echo 'mycourses/list.html'; ?>">
            <span class="alone myCourses active">
                <?php echo JText::_('COM_JOOMDLE_COURSES'); ?>
            </span>
            </a>
        <?php } else { ?>
            <a href="<?php echo 'mycourses/list.html'; ?>">
            <span class="left myCourses active">
                <?php echo JText::_('COM_JOOMDLE_COURSES'); ?>
        </span>
            </a>
            <a href="<?php echo 'mycourses/catalogue.html'; ?>">
        <span class="right catalogue">
                <?php echo JText::_('COM_JOOMDLE_CATALOGUE'); ?>
        </span>
            </a>
        <?php } ?>
    </div>

    <div class="joomdle_mylearning">
        <!-- ===================== Block My Course (Own Course) - Testing ======================= -->
        <div class="joomdle_mycourses">
            <i class="bg-second-img icon-create-course btCreateNewCourse" onclick="addNewCourse(<?php echo $this->noncat[0]; ?>, 'noncourse');"></i>
            <div class="courses-ma-title">
                <span class="title-text">
                    <?php
                    if (count($this->cursos) > 0) {
                        echo JText::sprintf('COM_JOOMDLE_MA_MY_COURSES', count($this->cursos));
                    } else {
                        echo JText::_('COM_JOOMDLE_MA_MY_COURSES_NO');
                    }
                    ?>
                </span>
            </div>
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php if ($this->useSample) { ?>
                        <div class="placeholder-loading">
                            <div class="ph-item">
                                <div class="ph-picture"></div>
                                <div class="ph-col-12">
                                    <div class="ph-row">
                                        <div class="ph-col-6 big"></div>
                                        <div class="ph-col-6 big empty"></div>
                                        <div class="ph-col-12"></div>
                                        <div class="ph-col-12"></div>
                                    </div>
                                </div>
                            </div>
                            <?php if ($device != 'mobile') { ?>
                                <div class="ph-item">
                                    <div class="ph-picture"></div>
                                    <div class="ph-col-12">
                                        <div class="ph-row">
                                            <div class="ph-col-6 big"></div>
                                            <div class="ph-col-6 big empty"></div>
                                            <div class="ph-col-12"></div>
                                            <div class="ph-col-12"></div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } else {
                        if (!is_array($this->cursos) || empty($this->cursos)) {
                            echo '<span class="text-center bg">' . JText::_('COM_JOOMDLE_NO_COURSE_YET') . '.</span>';
                        } else {
                            $i = 0;
                            foreach ($this->cursos as $curso) :
                                $url = JURI::base() . 'course/' . $curso['id'] . '.html';
                                $course_image = $curso['filepath'] . $curso['filename'];

                                (array_key_exists('created', $curso) && !is_null($curso['created'])) ? $time = date('F Y', $curso['created']) : JText::_('UNKNOWN');
                                $completion_des = JText::_('COM_JOOMDLE_RELEASED') . ': ' . $time;

                                if ($curso['self_enrolment']) $self_enrolment = true; else $self_enrolment = false;
                                ?>
                                <div class="swiper-slide" data-cid="<?php echo $curso['id'] ?>">
                                    <div class="<?php echo $class_tablet; ?>">
                                        <?php $publish_group = $dispatcher->trigger('get_publish_group_by_course_id', array($curso['id'])); 
                                        if(!$publish_group[0] && $self_enrolment){
                                        ?>
                                        <div class="content_publish">
                                            <i class="bg-second-img w22px icon-arrow-down dropdown-button"></i>
                                            <ul class="ma-action">
                                                    
                                                    <li>
                                                        <a href="javascript:" onclick="copyCourse(<?php echo $curso['id']; ?>);" class="btCopy copycenter">
                                                            <span><?php echo JText::_('COM_JOOMDLE_BUTTON_COPY');?></span>
                                                        </a>
                                                    </li>
                                                   
                                                    
                                                </ul>
                                        </div>
                                        <?php } ?>
                                         
                                        <div class="course_content">
                                            <div class="clearfix"></div>
                                            <div class="mylearning-img">
                                                <!--<a href="<?php // echo $url; ?>">-->
                                                <div id="images" class="lazyload" data-src="<?php echo $course_image.'?'.(time()*1000); ?>">
                                                 
                                                </div>
                                                <!--</a>-->
                                                <i class="bg-second-img w22px icon-arrow-down dropdown-button dropdown-button-mycourse"></i>
                                                 <?php if(!$publish_group[0] && $self_enrolment) {?> <div class="notePublised"><div class="content"><?php  echo JText::_('COM_JOOMDLE_COURSE_ARCHIVED');?></div></div><?php }?>
                                                <ul class="ma-action">
                                                    
                                                    <?php // if (!$course_group) {?>
                                                    <li>
                                                        <a href="javascript:void(0);" class="btRemove">
                                                            <span><?php echo JText::_('COM_JOOMDLE_BUTTON_REMOVE'); ?></span>
                                                        </a>
                                                    </li>
                                                    <?php // } ?>
                                                    <li>
                                                        <a href="javascript:" onclick="copyCourse(<?php echo $curso['id']; ?>);" class="btCopy copycenter">
                                                            <span><?php echo JText::_('COM_JOOMDLE_BUTTON_COPY');?></span>
                                                        </a>
                                                    </li>
                                                    <?php // if ($self_enrolment) {?>
                                                    <li class="li_publish <?php echo ($self_enrolment) ? 'hidden' : ''; ?>">
                                                        <a href="javascript:" class="btPublish">
                                                            <span><?php echo JText::_('COM_JOOMDLE_BUTTON_PUBLISH');?></span>
                                                        </a>
                                                    </li>
                                                    <?php // } else { ?>
                                                    <li class="li_unpublish <?php echo (!$self_enrolment) ? 'hidden' : ''; ?>">
                                                        <a href="javascript:" class="btUnpublish">
                                                            <span><?php echo JText::_('COM_JOOMDLE_UNPUBLISH');?></span>
                                                        </a>
                                                    </li>
                                                    <?php // } ?>
                                                </ul>
                                            </div>
                                             <div class="clearfix"></div>
                                            
                                            
                                            <div class="content_c">
                                                <div class="clearfix"></div>
                                                <div class="course_title" style="<?php echo (!$publish_group[0]) ? ' z-index: 2222;': ''  ?>">
                                                    <?php echo "<a class=\"\" href=\"$url\">" . $curso['fullname'] . "</a>"; ?>
                                                </div>
                                                <div class="right">
                                                    <p>
                                                        <?php
                                                        $summary = strip_tags($curso['summary']);
                                                        echo $summary;
                                                        ?>
                                                    </p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                $i++;
                            endforeach;
                        }
                    }
                    ?>
                </div>
            </div>
            <?php if (count($this->cursos) != 0 && $device != 'mobile'){?>
                <div class="swiper-button-next1"><i class="bg-second-img icon-next-black"></i></div>
                <div class="swiper-button-prev1"><i class="bg-second-img icon-prev-black"></i></div>
            <?php }?>
            <div class="removeCoursePopup hienPopup">
                <p><?php echo JText::_('COM_JOOMDLE_RMPOPUP_TEXT1'); ?></p>
                <p><?php echo JText::_('COM_JOOMDLE_RMPOPUP_TEXT2'); ?></p>
                <button class="btCancel"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                <button class="btYes"><?php echo JText::_('COM_JOOMDLE_YES'); ?></button>
            </div>
        </div>
        <!-- End Block My Courses -->

        <!--<hr class="jd-inline"/>-->

        <!-- ================= Block Courses Learning Provider ========================= -->
        <?php if (count($this->malpcat) > 0 || count($this->workinprogess) > 0) { ?>
            <div class="joomdle_courseslp lpCourses">
                <div class="swiper-container">
                    <?php if (count($this->malpcat) > 0) { ?>
                        <i class="bg-second-img icon-create-course btCreateNewCourseLP" onclick="addNewCourse(0, 'lpcourse');"></i>
                    <?php } ?>
                    <div class="courses-ma-title">
                            <span class="title-text">
                                <?php
                                $totallpcourses = count($this->macourse) + count($this->workinprogess);
                                if ($totallpcourses > 0) {
                                    echo JText::sprintf('COM_JOOMDLE_MA_LEARNING_PROVIDER', $totallpcourses);
                                } else {
                                    echo JText::_('COM_JOOMDLE_MA_LEARNING_PROVIDER_NO');
                                }
                                ?>
                            </span>
                    </div>
                    <div class="swiper-wrapper">
                        <?php
                        if ($totallpcourses <= 0) {
                            echo '<span class="text-center bg">' . JText::_('COM_JOOMDLE_NO_COURSE_YET') . '.</span>';
                        } else {
                            if (count($this->macourse) > 0) {
                                foreach ($this->macourse as $curso) :
                                    if ($curso['has_nonmember'] == 0) {
                                        $url = JURI::base() . 'course/' . $curso['id'] . '.html';
                                        $course_image = $curso['filepath'].$curso['filename'];

                                        (array_key_exists('created', $curso) && !is_null($curso['created'])) ? $time = date('F Y', $curso['created']) : JText::_('UNKNOWN');
                                        $completion_des = JText::_('COM_JOOMDLE_RELEASED').': '.$time;

                                        $groups = CFactory::getModel('groups');
                                        $groupid = (int)$groups->getLPGroupId($curso['category']);
                                        ?>
                                        <div class="swiper-slide" data-cid="<?php echo $curso['id'] ?>" data-catid="<?php echo $curso['category'] ?>">
                                            <div class="<?php echo $class_tablet; ?>">
                                                <div class="course_content">
                                                    <div class="clearfix"></div>
                                                    <div class="mylearning-img">
                                                        <!--<a href="<?php // echo $url; ?>">-->
                                                        <div id="images" class="lazyload" data-src="<?php echo $course_image.'?'.(time()*1000); ?>"></div>
                                                        <!--</a>-->
                                                        <i class="bg-second-img w22px icon-arrow-down dropdown-button"></i>
                                                        <ul class="ma-action">
                                                            <?php // if (!$course_group) {?>
                                                            <li>
                                                                <a href="javascript:void(0);" class="btRemove">
                                                                    <span><?php echo JText::_('COM_JOOMDLE_BUTTON_REMOVE'); ?></span>
                                                                </a>
                                                            </li>
                                                            <?php // } ?>
                                                            <li>
                                                                <a class="btAssign"  href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=assignRole&r=cc&coid='.$curso['id'].'&grid='.$groupid.'&ma=1') ?>">
                                                                    <span><?php echo JText::_('COM_JOOMDLE_ASSIGN');?></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:" class="btSubmit" onclick="sendForApproval(<?php echo $curso['id'] . ',' . $curso['category']; ?>);">
                                                                    <span><?php echo JText::_('COM_JOOMDLE_SUBMIT');?></span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="content_c">
                                                        <div class="clearfix"></div>
                                                        <div class="course_title">
                                                            <?php  echo "<a class=\"course_title\" href=\"$url\">".$curso['fullname']."</a>"; ?>
                                                        </div>

                                                        <div class="right">
                                                            <p>
                                                                <?php
                                                                $summary = strip_tags($curso['summary']);
                                                                echo $summary;
                                                                ?>
                                                            </p>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                    }
                                endforeach;
                            }
                            if (count($this->workinprogess)) {
                                foreach ($this->workinprogess as $curso) :
                                    if ($curso['has_nonmember'] == 1) {
                                        $url = JURI::base() . 'course/' . $curso['id'] . '.html';
                                        $course_image = $curso['filepath'].$curso['filename'];

                                        ?>
                                        <div class="swiper-slide" data-cid="<?php echo $curso['id']?>">
                                            <div class="<?php echo $class_tablet; ?>">
                                                <div class="course_content">
                                                    <div class="clearfix"></div>
                                                    <div class="mylearning-img">
                                                        <!--<a href="<?php // echo $url; ?>">-->
                                                        <div id="images" class="lazyload" data-src="<?php echo $course_image.'?'.(time()*1000); ?>"></div>
                                                        <!--</a>-->
                                                        <div class="label_workInProgress"><?php echo JText::_('COM_JOOMDLE_MA_WORK_IN_PROGRESS_NO');?></div>
                                                        <div class="joomdle-manage-btn btnright btSendApproval" onclick="sendForApproval(<?php echo $curso['id'] . ',' . $curso['category']; ?>);">
                                                            <?php echo JText::_('COM_JOOMDLE_SUBMIT'); ?>
                                                        </div>
                                                        <div class="course_creator"><?php echo CFactory::getUser($curso['content_creator'])->getDisplayName();?></div>
                                                    </div>

                                                    <div class="content_c">
                                                        <div class="clearfix"></div>
                                                        <div class="course_title">
                                                            <?php  echo "<a class=\"course_title\" href=\"$url\">".$curso['fullname']."</a>"; ?>
                                                        </div>

                                                        <div class="right">
                                                            <p><?php echo $curso['summary']; ?></p>
                                                        </div>

                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                endforeach;
                            }
                        }
                        ?>
                    </div>
                </div>
                <?php if ($totallpcourses != 0 && $device != 'mobile'){?>
                    <div class="swiper-button-next2"><i class="bg-second-img icon-next-black"></i></div>
                    <div class="swiper-button-prev2"><i class="bg-second-img icon-prev-black"></i></div>
                <?php }?>
                <div class="removeCoursePopup hienPopup">
                    <p><?php echo JText::_('COM_JOOMDLE_RMPOPUP_TEXT1'); ?></p>
                    <p><?php echo JText::_('COM_JOOMDLE_RMPOPUP_TEXT2'); ?></p>
                    <button class="btCancel"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                    <button class="btYes"><?php echo JText::_('COM_JOOMDLE_YES'); ?></button>
                </div>
            </div>
        <?php } ?>
        <!-- End Block Courses -->

        <!-- ================= Block Work in Progress ========================= -->

        <!-- End Block Work in Progress -->


        <?php if (count($this->approved) <= 0 && count($this->pendingco) <= 0) {

        } else { ?>
            <!-- ================= Block Pending Approval ========================= -->
            <div class="joomdle_courseslp lpPendingCourse">
                <div class="swiper-container">
                    <div class="courses-ma-title">
                        <span class="title-text">
                            <?php
                            if (count($this->pendingco) > 0) {
                                echo JText::sprintf('COM_JOOMDLE_MA_PENDING_APPROVAL', count($this->pendingco));
                            } else {
                                echo JText::_('COM_JOOMDLE_MA_PENDING_APPROVAL_NO');
                            }
                            ?>
                        </span>
                    </div>
                    <div class="swiper-wrapper">
                        <?php
                        if (!is_array ($this->pendingco) || empty($this->pendingco)) {
                            echo '<span class="text-center bg">'.JText::_('COM_JOOMDLE_NO_COURSE_YET').'.</span>';
                        } else {
                            $i = 0;
                            foreach ($this->pendingco as  $curso) :
                                $url = JURI::base() . 'course/' . $curso['id'] . '.html';
                                $course_image = $curso['filepath'].$curso['filename'];

                                (array_key_exists('created', $curso) && !is_null($curso['created'])) ? $time = date('F Y', $curso['created']) : JText::_('UNKNOWN');
                                $completion_des = JText::_('COM_JOOMDLE_RELEASED').': '.$time;
                                ?>
                                <div class="swiper-slide" data-cid="<?php echo $curso['id']?>">
                                    <div class="<?php echo $class_tablet; ?>">
                                        <div class="course_content">
                                            <div class="clearfix"></div>
                                            <div class="mylearning-img">
                                                <!--<a href="<?php // echo $url; ?>">-->
                                                <div id="images" class="lazyload" data-src="<?php echo $course_image.'?'.(time()*1000); ?>"></div>
                                                <!--</a>-->
                                                <i class="bg-second-img w22px icon-arrow-down dropdown-button"></i>
                                                <ul class="ma-action">
                                                    <?php if (count($this->malpcat) > 0) {?>
                                                        <li>
                                                            <a href="javascript:" class="btUnapprove" onclick="approval(<?php echo $curso['id'] . ',' . $curso['category'] . ", 'approve'"; ?>);">
                                                                <span><?php echo JText::_('COM_JOOMDLE_BUTTON_APPOVE');?></span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" class="btUnapprove" onclick="approval(<?php echo $curso['id'] . ',' . $curso['category'] . ", 'unapprove'"; ?>);">
                                                                <span><?php echo JText::_('COM_JOOMDLE_BUTTON_UNAPPOVE'); ?></span>
                                                            </a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>

                                            <div class="content_c">
                                                <div class="clearfix"></div>
                                                <div class="course_title">
                                                    <?php echo "<a class=\"course_title\" href=\"$url\">" . $curso['fullname'] . "</a>"; ?>
                                                </div>

                                                <div class="right">
                                                    <p><?php echo $curso['summary']; ?></p>
                                                </div>

                                                <div class="clearfix"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <?php
                                $i++;
                            endforeach;
                        }
                        ?>
                    </div>
                </div>
                <?php if (count($this->malpcat) != 0 && $device != 'mobile'){?>
                    <div class="swiper-button-next3"><i class="bg-second-img icon-next-black"></i></div>
                    <div class="swiper-button-prev3"><i class="bg-second-img icon-prev-black"></i></div>
                <?php }?>
                <div class="removeCoursePopup hienPopup">
                    <p><?php echo JText::_('COM_JOOMDLE_RMPOPUP_TEXT1'); ?></p>
                    <p><?php echo JText::_('COM_JOOMDLE_RMPOPUP_TEXT2'); ?></p>
                    <button class="btCancel"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                    <button class="btYes"><?php echo JText::_('COM_JOOMDLE_YES'); ?></button>
                </div>
            </div>
            <!-- End Block Pending Approval -->

            <!-- ================= Block Approved ========================= -->
            <?php if (count($this->malpcat) > 0) { ?>
                <div class="joomdle_courseslp lpApprovedCourse">
                    <div class="swiper-container">
                        <div class="courses-ma-title">
                        <span class="title-text">
                            <?php
                            if (count($this->approved) > 0) {
                                echo JText::sprintf('COM_JOOMDLE_MA_APPROVED', count($this->approved));
                            } else {
                                echo JText::_('COM_JOOMDLE_MA_APPROVED_NO');
                            }
                            ?>
                        </span>
                        </div>
                        <div class="swiper-wrapper">
                            <?php
                            if (!is_array($this->approved) || empty($this->approved)) {
                                echo '<span class="text-center bg">' . JText::_('COM_JOOMDLE_NO_COURSE_YET') . '.</span>';
                            } else {
                                require_once(JPATH_SITE.'/components/com_hikashop/helpers/lpapi.php');
                                $i = 0;
                                foreach ($this->approved as $curso) :
                                    $url = JURI::base() . 'course/' . $curso['id'] . '.html';
                                    $course_image = $curso['filepath'].$curso['filename'];

                                    (array_key_exists('created', $curso) && !is_null($curso['created'])) ? $time = date('F Y', $curso['created']) : JText::_('UNKNOWN');
                                    $completion_des = JText::_('COM_JOOMDLE_RELEASED').': '.$time;

                                    $groups = CFactory::getModel('groups');
                                    $this->groupid = (int)$groups->getLPGroupId($curso['category']);
                                    $lpHikashopCategory = LpApiController::getLPHikaCategory($this->groupid);

                                    ?>
                                    <div class="swiper-slide" data-cid="<?php echo $curso['id'] ?>">
                                        <div class="<?php echo $class_tablet; ?>">
                                            <div class="course_content">
                                                <div class="clearfix"></div>
                                                <div class="mylearning-img">
                                                    <!--<a href="<?php // echo $url; ?>">-->
                                                    <div id="images" class="lazyload" data-src="<?php echo $course_image.'?'.(time()*1000); ?>"></div>
                                                    <!--</a>-->
                                                    <div class="joomdle-manage-btn btnright btPublishMA"
                                                         onclick="publish(<?php echo $curso['id'] . ',' . $curso['category'] . ", 'publish'"; ?>);">
                                                        <?php echo JText::_('COM_JOOMDLE_BUTTON_PUBLISH'); ?>
                                                    </div>
                                                </div>

                                                <div class="content_c">
                                                    <div class="clearfix"></div>
                                                    <div class="course_title">
                                                        <?php  echo "<a class=\"course_title\" href=\"$url\">".$curso['fullname']."</a>"; ?>
                                                    </div>
                                                    <div class="right">
                                                        <p><?php echo $curso['summary']; ?></p>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    $i++;
                                endforeach;
                            }
                            ?>
                        </div>
                    </div>
                    <?php if (count($this->approved) != 0 && $device != 'mobile'){?>
                        <div class="swiper-button-next4"><i class="bg-second-img icon-next-black"></i></div>
                        <div class="swiper-button-prev4"><i class="bg-second-img icon-prev-black"></i></div>
                    <?php }?>
                    <div class="removeCoursePopup hienPopup">
                        <p><?php echo JText::_('COM_JOOMDLE_RMPOPUP_TEXT1'); ?></p>
                        <p><?php echo JText::_('COM_JOOMDLE_RMPOPUP_TEXT2'); ?></p>
                        <button class="btCancel"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                        <button class="btYes"><?php echo JText::_('COM_JOOMDLE_YES'); ?></button>
                    </div>
                </div>
            <?php } ?>
            <!-- End Block Approved -->
        <?php } ?>

    </div>
    <div class="notification"></div>
</div>
<?php
if ($device == 'mobile') {
    $slideview = '1.15';
    $space = '11';
} else if ($device == 'tablet') {
    $slideview = '2';
    $space = '11';
} else {
    $slideview = '2';
    $space = '11';
}
if ($device == 'mobile'){
    $touch = ' allowTouchMove : true,centeredSlides: true,';
    $centerSlides = ' centeredSlides: true,';
} else  {
    $touch = ' allowTouchMove : false,noSwipingClass : \'swiper-slide\',
            noswipingClass: \'swiper-slide\',';
    $centerSlides = ' centeredSlides: false,';
}
?>
<script type="text/javascript">
    (function ($) {
        var title_p = '<?php echo JText::_('COM_JOOMDLE_COURSES'); ?>';
        jQuery('#title-mainnav').html(title_p);

        $('.joomdle-mylearning-status > div').click(function () {
            $('.joomdle-mylearning-status > div').removeClass('active');
            $(this).addClass('active');
            var title = $(this).text();
            $('.navbar-header .navbar-title span').html(title);
            if ($(this).hasClass('recently-accessed')) {
                $('.joomdle-mylearning .joomdle_mylearning > div').removeClass('active');
                $('.joomdle-mylearning .joomdle_mylearning > .list_recently_accessed').addClass('active');
            }
            if ($(this).hasClass('not-yet-started')) {
                $('.joomdle-mylearning .joomdle_mylearning > div').removeClass('active');
                $('.joomdle-mylearning .joomdle_mylearning > .list_not_yet_started').addClass('active');
            }
            if ($(this).hasClass('completed')) {
                $('.joomdle-mylearning .joomdle_mylearning > div').removeClass('active');
                $('.joomdle-mylearning .joomdle_mylearning > .list_completed').addClass('active');
            }
        });
        $('.btRemove').click(function () {
            var cid = $(this).parents('.swiper-slide').attr('data-cid');
            jQuery('.ma-action').hide();
            $('.removeCoursePopup').fadeIn().attr('data-cid', cid);
            $('body').addClass('overlay2');
        });
        $('.removeCoursePopup .btCancel').click(function () {
            $('.removeCoursePopup').fadeOut().attr('data-cid', '');
            $('body').removeClass('overlay2');
        });
        $('.removeCoursePopup .btYes').click(function () {
            var cid = $('.removeCoursePopup').attr('data-cid');
            $('.removeCoursePopup').fadeOut().attr('data-cid', '');
            $('body').removeClass('overlay2');
            deleteCourse(cid);
        });

//        $('.ma-action .btAssign').click(function () {
//            if ($(this).hasClass('lpAssign')) {
//                var catid = $(this).parents('.swiper-slide').attr('data-catid');
//                window.location.href = "<?php echo JUri::base(); ?>mycourses/lpassign_" + catid + ".html";
//            } else {
//                var cid = $(this).parents('.swiper-slide').attr('data-cid');
//                window.location.href = "<?php echo JUri::base(); ?>mycourses/assign_" + cid + ".html";
//            }
//        });
        $('.btPublish').click(function () {
            var cid = $(this).parents('.swiper-slide').attr('data-cid');
            // Check course published or not
            $.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    courseid: cid,
                    act: 'check_status_publish_again'
                },
                beforeSend: function () {
                },
                success: function (data, textStatus, jqXHR) {
                    var res = JSON.parse(data);
                    console.log(res.check);
                    if(res.check[0]){
                        lgtCreatePopup('confirm', {
                            content: '<?php echo JText::_("REPUBLISH_COURSE_TO_CIRCLE1"); ?>' + res.check[0]['name'] + '?',
                            subcontent: '<?php echo JText::_("REPUBLISH_COURSE_TO_CIRCLE2"); ?>',
                            yesText: 'Proceed'
                        }, function () {
                            $.ajax({
                                url: window.location.href,
                                type: 'POST',
                                data: {
                                    courseid: cid,
                                    groupid: res.check[0]['id'],
                                    act: 'course_publish_again'
                                },
                                beforeSend: function () {
                                    lgtCreatePopup('', {'content': 'Loading...'});
                                },
                                success: function (data, textStatus, jqXHR) {
                                    var res1 = JSON.parse(data);
                                    $('.lgtPopup').remove();
                                    if (res.error == 1) {
                                        lgtCreatePopup('withCloseButton', {'content': res1.comment});
                                    } else {
                                        lgtCreatePopup('withCloseButton', {'content':'<span class="circle_title"><?php echo JText::_('REPUBLISH_COURSE_TO_CIRCLE3');?>' + res.check[0]['name'] + '</span>.'});

                                        $('.joomdle_mycourses .swiper-slide[data-cid=' + cid + '] .li_unpublish, ' +
                                            '.joomdle_mycourses .swiper-slide[data-cid=' + cid + '] .li_publish').toggleClass('hidden');
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log('ERRORS: ' + textStatus);
                                }
                            });
                        });
                    } else {
                        window.location.href = "<?php echo JURI::base();?>mycourses/share_" + cid + ".html";
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                }
            });

        });
        $('.joomdle_mycourses .btUnpublish').click(function () {
            var cid = $(this).parents('.swiper-slide').attr('data-cid');
            $.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    courseid: cid,
                    act: 'unsharetocircle'
                },
                beforeSend: function () {
                    lgtCreatePopup('', {'content': 'Loading...'});
                },
                success: function (data, textStatus, jqXHR) {
                    var res = JSON.parse(data);

                    lgtRemovePopup();
                    if (res.error == 1) {
                        lgtCreatePopup('withCloseButton', {content: res.comment});
                    } else {
                        console.log(res);
                        $('.joomdle_mycourses .swiper-slide[data-cid=' + cid + '] .li_publish, ' +
                            '.joomdle_mycourses .swiper-slide[data-cid=' + cid + '] .li_unpublish').toggleClass('hidden');
//                        $('.joomdle_mycourses .swiper-slide[data-cid=' + cid + '] .btCopy').addClass('copycenter');

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                }
            });
        });

        <?php if ($this->useSample) { ?>
        $.ajax({
            url: '<?php echo JUri::base().'index.php?option=com_joomdle&task=ajaxShowManageDefaultPage'?>',
            type: 'post',
            success: function(res) {
                res = JSON.parse(res);
                if (res.success) {
                    $('.joomdle-mylearning .placeholder-loading').fadeOut(0, function() {
                        var a = $(this).parents('.joomdle-mylearning');
                        a.hide();
                        a.after(res.html);
                        a.remove();
                        lazyload();
                    });
                } else {
                    $('.joomdle-mylearning').html("Error").fadeIn(0);
                }
            }
        });
        <?php } ?>

    })(jQuery);

    function deleteCourse(cid) {
        var url = "<?php echo JURI::base();?>mycourses/remove_" + cid + ".html";
        var act = 'remove';
        jQuery.ajax({
            url: url,
            type: 'POST',
            data: {
                courseid: cid,
                act: act
            },
            beforeSend: function () {
                lgtCreatePopup('', {'content': 'Loading...'});
                jQuery('.ma-action').hide();
                // processing = true;
                // lgtCreatePopup('loading', {'header': 'Processing'});
            },
            success: function (data, textStatus, jqXHR) {
                var res = JSON.parse(data);

                lgtRemovePopup();
                // processing = false;
                // lgtRemovePopup();
                if (res.error == 1) {
                    lgtCreatePopup('withCloseButton', {content: res.comment});
                } else {
                    console.log(res.comment);
                    jQuery('.swiper-slide[data-cid="'+cid+'"]').fadeOut();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
                // processing = false;
                // lgtRemovePopup();
            }
        });
    }

    function addNewCourse(catid, checkblock) {
        if (checkblock == 'lpcourse') {
            window.location.href = "<?php echo JUri::base();?>mycourses/choosecirlce.html";
        } else {
            window.location.href = "<?php echo JUri::base();?>mycourses/create_0_" + catid + "_"+ checkblock + "_.html";
        }
        return false;
    }

    function editCourse(cid, catid, checkblock) {
        window.location.href = "<?php echo JURI::base();?>mycourses/edit_" + cid + "_" + catid + "_"+ checkblock + "_.html";
    }

    function sendForApproval(courseid, catid) {
        var url = "<?php echo JURI::base();?>mycourses/sendapprove_" + courseid + "_" + catid + "_.html";
        var act = 'sendapprove';
        jQuery.ajax({
            url: url,
            type: 'POST',
            data: {
                course_id: courseid,
                action: act,
                cat_id: catid
            },
            beforeSend: function () {
                lgtCreatePopup('', {'content': 'Loading...'});
            },
            success: function (data, textStatus, jqXHR) {
                var res = JSON.parse(data);

                lgtRemovePopup();
                if (res.error == 1) {
                    lgtCreatePopup('withCloseButton', {content: res.message});
                } else {
                    console.log(res.message);
                    window.location.href = "<?php echo JURI::base() . 'mycourses/list.html';?>";
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
            }
        });
    }

    function approval(courseid, catid, act) {
        var url = "<?php echo JURI::base();?>mycourses/approval_" + courseid + "_" + catid + "_.html";
        var action = 'approval';
        jQuery.ajax({
            url: url,
            type: 'POST',
            data: {
                course_id: courseid,
                action: action,
                cat_id: catid,
                act: act
            },
            beforeSend: function () {
                lgtCreatePopup('', {'content': 'Loading...'});
            },
            success: function (data, textStatus, jqXHR) {
                var res = JSON.parse(data);

                lgtRemovePopup();
                if (res.error == 1) {
                    lgtCreatePopup('withCloseButton', {content: res.message});
                } else {
                    console.log(res.message);
                    window.location.href = "<?php echo JURI::base() . 'mycourses/list.html';?>";
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
            }
        });
    }

    function publish(courseid, catid, action) {
        var modal = document.getElementById('ass-message');
        var er = document.getElementById('error');
        jQuery('.closetitle').click(function(){
            modal.style.display = "none";
        })
        var url = "<?php echo JURI::base();?>index.php?option=com_joomdle&view=mycourses&action=publish&course_id="+courseid+"&catid="+catid;//"<?php // echo JURI::base();?>mycourses/publish_"+courseid+"_"+catid+"_.html";
        var action = action;
        var data = [<?php echo isset($lpHikashopCategory->category_id) ? $lpHikashopCategory->category_id : '';?>];
        var act = 'publishtohikashop';
        var i = 0;

        jQuery.ajax({
            url: url,
            type: 'POST',
            data: {
                course_id: courseid,
                action: action,
                cat_id: catid,
                act: act,
                data: data
            },
            beforeSend: function () {
                lgtCreatePopup('', {'content': 'Loading...'});
                //jQuery('.lgt-notif').html('<?php echo JText::_("REQUEST_PROCESSED");?>').addClass('lgt-visible');

            },
            success: function (data, textStatus, jqXHR) {
                var res = JSON.parse(data);
                lgtRemovePopup();
                //jQuery('.lgt-notif').removeClass('lgt-visible');
                if (res.error == 1) {
                    er.innerHTML = res.message;
                    modal.style.display = "block";
                    //jQuery('.lgt-notif-error').html(res.message).addClass('lgt-visible');
                } else {
                    console.log(res.message);
                    window.location.href = "<?php
                        if (strpos($_SERVER['HTTP_REFERER'], 'circle') !== false && strpos($_SERVER['HTTP_REFERER'], JURI::base()) !== false) {
                            echo JURI::base().'circles/viewabout?groupid='.$this->groupid;
                        } else
                            echo JURI::base().'mycourses/catalogue.html';
                        ?>";
                }
            }
        });

    }

    function copyCourse(courseid) {
        var url = "<?php echo JURI::base();?>index.php?option=com_joomdle&view=mycourses&action=copy&course_id=" + courseid;
        var act = 'copy';
        jQuery.ajax({
            url: url,
            type: 'POST',
            data: {
                course_id: courseid,
                action: act
            },
            beforeSend: function () {
//                jQuery('body').addClass('overlay2');
//                jQuery('.notification').html('Loading...').fadeIn();
                lgtCreatePopup('', {content: 'Loading...'});
                jQuery('.ma-action').hide();
                // processing = true;
                // lgtCreatePopup('loading', {'header': 'Processing'});
            },
            success: function (data, textStatus, jqXHR) {
                var res = JSON.parse(data);

//                jQuery('body').removeClass('overlay2');
//                jQuery('.notification').fadeOut();
                // processing = false;
                lgtRemovePopup();
                if (res.error == 1) {
                    lgtCreatePopup('withCloseButton', {content: res.message || 'There is something wrong. Please try again later.'});
                } else {
                    window.location.href = "<?php echo JURI::base() . 'mycourses/list.html';?>";
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
                // processing = false;
                // lgtRemovePopup();
            }
        });
    }

    var x = screen.width;
    var a = 0;
    if(x < 390){
        a = 1;
    }
    if(x >= 390 && x<=736){
        a = 1.5;
    }
    if(x>736 && x<=1366){
        a = 2;
    }
    if(x>1366){
        a = 3;
    }
    var device = '<?php echo $device; ?>';
    if (device == 'mobile' ){
        a = 1;
    }

    <?php if (!$this->useSample) { ?>
    var swiper = new Swiper('.joomdle_mycourses .swiper-container', {
        <?php echo $centerSlides;?>
        pagination: '.joomdle_mycourses .swiper-pagination',
        slidesPerView: a,
        nextButton: '.joomdle_mycourses .swiper-button-next1',
        prevButton: '.joomdle_mycourses .swiper-button-prev1',
        spaceBetween: <?php echo $space; ?>
    });
    var swiper = new Swiper('.lpCourses .swiper-container', {
        <?php echo $centerSlides;?>
        pagination: '.lpCourses .swiper-pagination',
        slidesPerView: a,
        nextButton: '.lpCourses .swiper-button-next2',
        prevButton: '.lpCourses .swiper-button-prev2',
        spaceBetween: <?php echo $space; ?>
    });
    var swiper = new Swiper('.lpPendingCourse .swiper-container', {
        <?php echo $centerSlides;?>
        pagination: '.lpPendingCourse .swiper-pagination',
        slidesPerView: a,
        nextButton: '.lpPendingCourse .swiper-button-next3',
        prevButton: '.lpPendingCourse .swiper-button-prev3',
        spaceBetween: <?php echo $space; ?>
    });
    var swiper = new Swiper('.lpApprovedCourse .swiper-container', {
        <?php echo $centerSlides;?>
        pagination: '.lpApprovedCourse .swiper-pagination',
        slidesPerView: a,
        nextButton: '.lpApprovedCourse .swiper-button-next4',
        prevButton: '.lpApprovedCourse .swiper-button-prev4',
        spaceBetween: <?php echo $space; ?>
    });
    <?php } ?>

    jQuery('.course_title').each(function () {
        if (jQuery(this).find('a').height() > 44) {
            jQuery(this).find('a').addClass('limited');
        }
    });
    jQuery('.content_c .right').each(function () {
        if (jQuery(this).find('p').height() > 40) {
            jQuery(this).find('p').addClass('limited');
        }
    });
    // jQuery(document).ready(function () {
            jQuery('.dropdown-button').on('click', function () {
               jQuery(this).next('.ma-action').toggle();
           });
   // }(jQuery));

</script>
