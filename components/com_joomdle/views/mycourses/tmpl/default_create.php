<?php
defined('_JEXEC') or die('Restricted access'); ?>
<?php
$data = ($this->action == 'edit') ? $this->course_info : null;
$session = JFactory::getSession();
$token = md5($session->getId());
$user = JFactory::getUser();
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'components/com_joomdle/views/mycourses/tmpl/mycourses.css');
$document->addScript(JUri::root() . 'components/com_joomdle/js/autosize.min.js');

$is_mobile = $session->get('device') == 'mobile' ? true : false;
?>
<div class="joomdle-my-courses-create">
    <form action="" method="post" class="formCreateCourse" enctype="multipart/form-data">
        <div class="create_course_image_cover">
            <div class="uploadCourseImage">
                <div class="duration_price <?php if ($this->type_course && $this->type_course == 'noncourse') echo "hiddenDurationPrice"; ?>"
                     style="display: none;">
                    <span class="header_price"></span> • <span class="header_duration"></span>
                </div>
                <div class="mainImage  <?php echo (!empty($data)) ? 'visible' : ''; ?>"
                     style="background:url('<?php echo (empty($data)) ? JUri::base() . "courses/theme/parenthexis/pix/nocourseimg.jpg" : $data['filepath'] . $data['filename'] . '?' . (time() * 1000); ?>') no-repeat center center"></div>
                <img src="<?php echo JUri::base(); ?>images/MyLearning.png" class="photoicon1"/>
                <div class="mnPhotoIcon">
                    <img src="/images/icons/photoicon.png" class="photo-icon">
                </div>
                <!--                <img src="-->
                <?php //echo JUri::base(); ?><!--images/icons/photoicon.png" class="photoicon"/>-->
            </div>
            <div class="content_image_header" style="display:none;">
                <div class="header_title">
                    <p class="tittle"></p>
                </div>
            </div>

        </div>
        <div class="content_create_course">
            <div class="container">
                <h1 class="form-title"><?php echo (empty($data)) ? JText::_('COM_JOOMDLE_CREATE_COURSE') : JText::_('COM_JOOMDLE_EDIT_COURSE'); ?></h1>
                <input type="file" name="fileUpload" class="fileUpload" style="display: none;"/>
                <input type="hidden" name="hdUploadFileName" class="hdUploadFileName"
                       value="<?php echo (empty($data['filename']) || $data['filename'] == 'nocourseimg.jpg') ? '' : $data['filename']; ?>"/>
                <input type="hidden" name="hdUploadTmpFile" class="hdUploadTmpFile" value=""/>
                <div class="row">
                    <div class="divCourseTitle">
                        <label class = "mycourse_title"><?php echo Jtext::_('MYCOURSE_TITLE'); ?><span class="red">*</span></label>
                        <textarea type="text" name="txtCourseTitle" maxlength="254"
                                  class="txtCourseTitle"><?php echo (empty($data)) ? '' : $data['fullname']; ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="divCourseCategory">
                        <label><?php echo Jtext::_('MYCOURSE_COURSE_OWNER'); ?> <span class="red">*</span></label>
                        <div class="courseCategory <?php echo (empty($data) && empty($this->categories)) ? 'inputTextSimulation' : ''; ?> <?php echo (($this->action == 'edit') || (count($this->categories) <= 1)) ? 'oneCategory' : ''; ?> "> <?php if (empty($this->categories)) echo 'No Owner...'; else echo (empty($data)) ? ((!empty($this->categories)) ? reset($this->categories)['name'] : JText::_('COM_JOOMDLE_CHOOSE_OWNER')) : $data['cat_name']; ?></div>
                        <?php if (!empty($this->categories)) {
                            echo '<ul class="ulCourseCategory">';
                            if (empty($data)) {
                                $cl = '';
                                if (count($this->categories) == 1) $cl = 'selected';
                                $i = 0;
                                foreach ($this->categories as $k => $v) {
                                    if ($this->categoryid && $this->categoryid > 0 && $this->categoryid == $v['catid']) {
                                        $cl = 'selected';
                                    } else if ((!$this->categoryid || $this->categoryid == 0) && $i == 0) {
                                        $cl = 'selected';
                                    }
                                    echo '<li class="' . $cl . '" data-id="' . $v['catid'] . '">' . $v['name'] . '</li>';
                                    $i++;
                                }
                            } else {
                                foreach ($this->categories as $k => $v) {
                                    if ($v['catid'] == $data['cat_id']) $class = 'selected'; else $class = "";
                                    echo ($v['parent'] == 0) ? '<li class="' . $class . '" data-id="' . $v['catid'] . '">' . $v['name'] . '</li>' : '<li class="' . $class . '" data-id="' . $v['catid'] . '">' . $this->categories[$v['parent']]['name'] . ' / ' . $v['name'] . '</li>';
                                }
                            }
                            echo '</ul>';
                        } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="divCourseDescription">
                        <label class="col-xs-12 col-sm-12 col-md-12 mycourse_description"><?php echo Jtext::_('MYCOURSE_DESCRIPTION'); ?>
                            <span
                                    class="red">*</span></label>
                        <textarea name="txtaDescription" class="txtaDescription" cols="30"
                                  rows="10"><?php echo (empty($data)) ? '' : strip_tags($data['summary']); ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="divCourseLearningOutcomes">
                        <label class="col-xs-12 col-sm-12 col-md-12"><?php echo Jtext::_('MYCOURSE_LEARNING_OUTCOME'); ?><?php if ($this->type_course && $this->type_course == 'lpcourse') { ?>
                            <?php } ?></label>
                        <textarea name="txtaLearningOutcomes" class="txtaLearningOutcomes" cols="30"
                                  rows="10"><?php echo (empty($data)) ? '' : strip_tags($data['learningoutcomes']); ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="divCourseTargetAudience">
                        <label class="col-xs-12 col-sm-12 col-md-12"><?php echo Jtext::_('MYCOURSE_TARGET_LEARNERS'); ?><?php if ($this->type_course && $this->type_course == 'lpcourse') { ?>
                            <?php } ?></label>
                        <textarea class="txtCourseTargetAudience" name="txtCourseTargetAudience" cols="30"
                                  rows="10"><?php echo (empty($data)) ? '' : strip_tags($data['targetaudience']); ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="divCourseDuration">
                        <label class="col-xs-12 col-sm-12 col-md-12"><?php echo Jtext::_('MYCOURSE_DURATION'); ?> <?php if ($this->type_course && $this->type_course == 'lpcourse') { ?>
                                <span class="red">*</span><?php } ?></label>
                        <input class="txtCourseDuration" type="text"
                               class="txtCourseDuration" name="txtCourseDuration"
                               value="<?php echo (empty($data)) ? '' : $data['duration']; ?>"/>
                        <span class="text"><?php echo Jtext::_('MYCOURSE_HOURS'); ?></span>


                    </div>
                </div>
                <div class="row">
                    <div class="divCourseValidFor <?php if ($this->type_course && $this->type_course == 'noncourse') echo "hiddenValidFor"; ?>">
                        <label class="col-xs-12 col-sm-12 col-md-12"><?php echo Jtext::_('MYCOURSE_VALID_FOR'); ?> </label>
                        <input type="text" class="txtCourseValidFor"
                               name="txtCourseValidFor"
                               value="<?php echo (empty($data)) ? '' : (int)($data['enrolperiod'] / (30 * 24 * 3600)); ?>"/>
                        <span class="text"><?php echo Jtext::_('MYCOURSE_MONTHS'); ?></span>

                    </div>
                </div>
                <div class="row">
                    <div class="divCoursePrice <?php if ($this->type_course && $this->type_course == 'noncourse') echo "hiddenPrice"; ?>">
                        <label class="col-xs-12 col-sm-12 col-md-12"><?php echo Jtext::_('MYCOURSE_PRICE'); ?> <span
                                    class="red">*</span></label>

                        <?php if ($this->type_course && $this->type_course == 'lpcourse') { ?>
                            <span class="currency"><?php echo Jtext::_('MYCOURSE_CURRENCY'); ?></span>
                            <input type="text"
                                   name="txtCoursePrice"
                                   class="txtCoursePrice"
                                   value="<?php echo (empty($data)) ? '' : $data['price']; ?>"/>
                        <?php } else { ?>
                            <span class="currency"><?php echo Jtext::_('MYCOURSE_CURRENCY'); ?></span>
                            <input type="text"
                                   name="txtCoursePrice"
                                   class="txtCoursePrice"
                                   value="<?php echo (empty($data)) ? 0 : $data['price']; ?>"/>
                        <?php } ?>

                    </div>
                </div>
                <div class="row">
                    <div class="divToggle"></div>
                </div>
                <div class="row certificate">
                    <div class="divCourseCertificate">
                        <label><?php echo JText::_('COM_JOOMDLE_COURSE_CERTIFICATE_COMPL'); ?></label>
                        <div class="yesnoIcon <?php echo (empty($data)) ? 'no' : (($data['certificatecourse']) ? 'yes' : 'no'); ?>">
                            <div class="block"></div>
                        </div>
                        <input type="hidden" name="hdCourseCertificate" class="hdCourseCertificate"
                               value="<?php echo (empty($data)) ? '0' : (($data['certificatecourse']) ? '1' : '0'); ?>"/>
                    </div>
                </div>
                <div class="row survey">
                    <div class="divCourseSurvey">
                        <label><?php echo JText::_('COM_JOOMDLE_COURSE_SURVEY'); ?></label>
                        <div class="yesnoIcon <?php echo (empty($data)) ? 'no' : (($data['coursesurvey']) ? 'yes' : 'no'); ?>">
                            <div class="block"></div>
                        </div>
                        <input type="hidden" name="hdCourseSurvey" class="hdCourseSurvey"
                               value="<?php echo (empty($data)) ? '0' : (($data['coursesurvey']) ? '1' : '0'); ?>"/>
                    </div>
                </div>
                <div class="row facilitated">
                    <div class="divCourseFacilitated">
                        <label><?php echo JText::_('COM_JOOMDLE_FACILITATED_COURSE'); ?></label>
                        <div class="yesnoIcon <?php echo (empty($data)) ? 'no' : (($data['facilitatedcourse']) ? 'yes' : 'no'); ?>">
                            <div class="block"></div>
                        </div>
                        <input type="hidden" name="hdFacilitatedCourse" class="hdFacilitatedCourse"
                               value="<?php echo (empty($data)) ? '0' : (($data['facilitatedcourse']) ? '1' : '0'); ?>"/>
                    </div>
                </div>
                <div class="row create-course-chats">
                    <div class="divCreateCourseChats">
                        <label><?php echo JText::_('COM_JOOMDLE_CREATE_COURSE_CHATS'); ?></label>
                        <div class="yesnoIcon <?php echo (empty($data)) ? 'yes' : (($data['enableCreateChats']) ? 'yes' : 'no'); ?>">
                            <div class="block"></div>
                        </div>
                        <input type="hidden" name="hdCreateCourseChats" class="hdCreateCourseChats"
                               value="<?php echo (empty($data)) ? '1' : (isset($data['enableCreateChats']) ? $data['enableCreateChats'] : '1'); ?>"/>
                    </div>
                </div>
                <div class="buttons pull-right row">
                    <button class="btCancel" type="button"><?php echo Jtext::_('MYCOURSE_BTN_CANCEL'); ?></button>
                    <button class="btPreview"><?php echo Jtext::_('MYCOURSE_BTN_PREVIEW'); ?></button>
                    <button class="btSave"><?php echo Jtext::_('MYCOURSE_BTN_SAVE'); ?></button>
                </div>
                <div class="notification">
                </div>
                <div class="modal" id="ass-message">
                    <div class="modal-content hienPopup">
                        <p id="error-hv"></p>
                        <button class="closetitle" type="button"><?php echo JText::_('COM_JOOMDLE_CLOSE'); ?></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="create_course_preview" style="display:none;">
            <div class="description_preview">
                <p class="header_title col-xs-12 col-sm-12 col-md-12"><?php echo Jtext::_('MYCOURSE_DESCRIPTION'); ?></p>
                <div class="content_description_preview"></div>
            </div>
            <div class="leanerningOutcomes__preview">
                <p class="header_title col-xs-12 col-sm-12 col-md-12"><?php echo Jtext::_('MYCOURSE_LEARNING_OUTCOME'); ?></p>
                <div class="content_leanerningOutcomes_preview"></div>
            </div>
            <div class="targetAudience_preview">
                <p class="header_title col-xs-12 col-sm-12 col-md-12"><?php echo Jtext::_('MYCOURSE_TARGET_LEARNERS'); ?></p>
                <div class="content_targetAudience_preview"></div>
            </div>
            <div class="courseValidity_preview <?php if ($this->type_course && $this->type_course == 'noncourse') echo "hiddenCourseValidity"; ?>">
                <p class="header_title col-xs-12 col-sm-12 col-md-12"><?php echo Jtext::_('MYCOURSE_VALID_FOR'); ?></p>
                <div class="content_courseValidity_preview"></div>
            </div>
            <button class="text-center btExitPreview"><?php echo Jtext::_('MYCOURSE_BTN_EXIT_PREVIEW'); ?></button>
        </div>
    </form>

</div>
<script type="text/javascript">
    jQuery('.txtCourseTitle').each(function () {
        autosize(this);
    }).on('input', function () {
        autosize(this);
    });

    jQuery('.txtaDescription').each(function () {
        autosize(this);
    }).on('input', function () {
        autosize(this);
    });

    jQuery('.txtaLearningOutcomes').each(function () {
        autosize(this);
    }).on('input', function () {
        autosize(this);
    });
</script>

<script type="text/javascript">
    (function ($) {
        $('.courseCategory').click(function () {
            $('.ulCourseCategory').toggle();
        });
        $('.ulCourseCategory > li').click(function () {
            $('.ulCourseCategory > li').removeClass('selected');
            $(this).addClass('selected');
            $('.courseCategory').removeClass('inputTextSimulation');
            $('.courseCategory').html($(this).text());
            $('.ulCourseCategory').hide();
        });
        $('.yesnoIcon').click(function () {
            if ($(this).hasClass('yes')) {
                $(this).removeClass('yes').addClass('no');
                $(this).next().val(0);
            } else if ($(this).hasClass('no')) {
                $(this).removeClass('no').addClass('yes');
                $(this).next().val(1);
            }
        });
        $('.joomdle-my-courses-create .inputTextSimulation').focus(function () {
            if ($(this).hasClass('inputTextSimulation'))
                $(this).removeClass('inputTextSimulation').val('');
        });

        $('.joomdle-my-courses-create .btCancel').click(function () {
            window.location.href = '<?php echo $this->action == 'create' ? '/mycourses/list.html' : '/course/'.$data['remoteid'].'.html';?>';
        });

        $('.btPreview').click(function () {
            var content_description = $('.txtaDescription').val();
            var content_leanerningOutcomes = $('.txtaLearningOutcomes').val();
            var content_targetAudience = $('.txtCourseTargetAudience').val();
            var content_courseValidity = $('.txtCourseValidFor').val();
            var content_price = $('.txtCoursePrice').val();
            var content_duration = $('.txtCourseDuration').val();
            var content_title = $('.txtCourseTitle').val();
//
            $('.content_create_course').hide();
            $('.create_course_preview').show();
            $('.content_image_header').show();
            $('.mnPhotoIcon').hide();
            $('.photoicon1').hide();
            $('.duration_price').show();

            if (content_description != '') {
                var obj1 = $('.content_description_preview').text(content_description);
                obj1.html(obj1.html().replace(/\n/g, '<br/>'));
            } else {
                $('.description_preview').hide();
            }

            if (content_leanerningOutcomes != '') {
                var obj2 = $('.content_leanerningOutcomes_preview').text(content_leanerningOutcomes);
                obj2.html(obj2.html().replace(/\n/g, '<br/>'));
            } else {
                $('.leanerningOutcomes__preview').hide();
            }

            if (content_targetAudience != '') {
                var obj3 = $('.content_targetAudience_preview').text(content_targetAudience);
                obj3.html(obj3.html().replace(/\n/g, '<br/>'));
            } else {
                $('.targetAudience_preview').hide();
            }


            if (content_courseValidity != '') {
                var obj4 = $('.content_courseValidity_preview').text(content_courseValidity + ' months');
                obj4.html(obj4.html().replace(/\n/g, '<br/>'));
            } else {
                $('.courseValidity_preview').hide();
            }
            if (content_title != '') {
                var obj5 = $('.header_title .tittle').text(content_title);
                obj5.html(obj5.html().replace(/\n/g, '<br/>'));
            } else {
                var obj11 = $('.header_title .tittle').text('Tittle Course');
                obj11.html(obj11.html().replace(/\n/g, '<br/>'));

            }

            if (content_price != '') {
                var obj6 = $('.header_price').text('$' + parseFloat(content_price).toFixed(2));
                obj6.html(obj6.html().replace(/\n/g, '<br/>'));
            } else {
                var obj9 = $('.header_price').text('$0');
                obj9.html(obj9.html().replace(/\n/g, '<br/>'));
            }
            if (content_duration != '') {
                var obj7 = $('.header_duration').text(content_duration + 'h');
                obj7.html(obj7.html().replace(/\n/g, '<br/>'));
            } else {
                var obj10 = $('.header_duration').text('0h');
                obj10.html(obj10.html().replace(/\n/g, '<br/>'));
            }

        });
        $('.btExitPreview').click(function () {
            $('.content_create_course').show();
            $('.create_course_preview').hide();
            $('.content_image_header').hide();
            $('.mnPhotoIcon').show();
            $('.photoicon1').show();
            $('.duration_price').hide();

            $('.description_preview').show();
            $('.leanerningOutcomes__preview').show();
            $('.targetAudience_preview').show();
            $('.courseValidity_preview').show();
        });

        $('.uploadCourseImage').click(function () {
            $('.fileUpload').click();
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.uploadCourseImage .mainImage').attr('style', 'background:url(' + e.target.result + ') no-repeat center center').show();
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".txtCourseTitle").keyup(function () {
            $('.txtCourseTitle').removeClass("error1");
            $('.mycourse_title').removeClass("error1");
        });
        $(".txtaDescription").keyup(function () {
            $('.txtaDescription').removeClass("error1");
            $('.mycourse_description').removeClass("error1");
        });
        $(".txtCourseDuration").keyup(function () {
            $('.txtCourseDuration').removeClass("error1");
        });
        $(".txtCoursePrice").keyup(function () {
            $('.txtCoursePrice').removeClass("error1");
        });
        $(".txtCourseValidFor").keyup(function () {
            $('.txtCourseValidFor').removeClass("error1");
        });
        <?php if ($this->type_course && $this->type_course == 'lpcourse') { ?>
        $('.joomdle-my-courses-create .btSave , .joomdle-my-courses-create .btAssign').click(function () {
            var modal = document.getElementById('ass-message');
            var er = document.getElementById('error-hv');
            $('.closetitle').click(function () {
                modal.style.display = "none";
                $('body').removeClass('overlay2');
            });
            $('body').addClass('overlay2');
            if ($('.hdUploadFileName').val() == '') {
                er.innerHTML = "Please select an image for course.";
                modal.style.display = "block";
            } else if ($('.joomdle-my-courses-create .btSave').attr('data-disabled') == 1) {
                er.innerHTML = "Course image is uploading.";
                modal.style.display = "block";
            } else if ($('.txtCourseTitle').val().trim() == '' || $('.txtaDescription').val().trim() == '') {
                 $('body').removeClass('overlay2');
                if($('.txtCourseTitle').val().trim() == '' || $('.txtCourseTitle').val().trim().length > 254){
                    $(".txtCourseTitle").addClass("error1");
                    $(".mycourse_title").addClass("error1");
                }
                if ($('.txtCourseTitle').val().trim().length > 254) lgtCreatePopup('withCloseButton', {content: 'Maximum number of characters in course name field is 254. Current: ' + $('.txtCourseTitle').val().trim().length});
                if($('.txtaDescription').val().trim() == ''){
                    $(".txtaDescription").addClass("error1");
                    $(".mycourse_description").addClass("error1");
                }
            }
            else if ($('.txtCourseDuration').val().trim() == '') {
//                er.innerHTML = "Please fill out “Duration” field.";
//                modal.style.display = "block";
                $(".txtCourseDuration").addClass("error1");
                //$('.lgt-notif-error').html('Please fill out “Duration” field.').addClass('lgt-visible');
            } else if (!$.isNumeric($('.txtCourseDuration').val())) {
                er.innerHTML = "“Duration” is not numeric.";
                modal.style.display = "block";
                $(".txtCourseDuration").addClass("error1");

            } else if ($('.txtCoursePrice').val().trim() == '') {
//                er.innerHTML = "Please fill out “Price” field.";
//                modal.style.display = "block";
                $(".txtCoursePrice").addClass("error1");

            }
//            else if ($('.txtCourseValidFor').val() == '') {
//
//                er.innerHTML = "Please fill out “Valid For” field.";
//                modal.style.display = "block";
//
//                //$('.lgt-notif-error').html('Please fill out “Valid For” field.').addClass('lgt-visible');
//            }
            else if (!$.isNumeric($('.txtCoursePrice').val())) {

                er.innerHTML = "“Price” is not numeric.";
                modal.style.display = "block";
                $(".txtCoursePrice").addClass("error1");
            } else if (!$.isNumeric($('.txtCourseValidFor').val()) && $('.txtCourseValidFor').val() != '') {
                er.innerHTML = "“Valid For” is not numeric.";
                modal.style.display = "block";
                $(".txtCourseValidFor").addClass("error1");

            } else if ((parseInt(getNumAfterDots('' + $('.txtCourseDuration').val())) > 2 && $('.txtCourseDuration').val().indexOf(".") != '-1')) {

                er.innerHTML = "'Duration' can only be up to two decimal places.";
                modal.style.display = "block";
                $(".txtCourseDuration").addClass("error1");

            } else if (parseInt(getNumAfterDots('' + $('.txtCoursePrice').val())) > 2 && $('.txtCoursePrice').val().indexOf(".") != '-1') {
                er.innerHTML = "'Price' can only be up to two decimal places.";
                modal.style.display = "block";
                $(".txtCoursePrice").addClass("error1");
            } else if ($('.txtCoursePrice').val() < 0 && $('.txtCourseDuration').val() < 0) {
                er.innerHTML = "Please input a positive value for 'Duration' and 'Price'.";
                modal.style.display = "block";
                $(".txtCoursePrice").addClass("error1");
                $(".txtCourseDuration").addClass("error1");
            } else if ($('.txtCourseDuration').val() < 0) {
                er.innerHTML = "Please input a positive value for 'Duration'.";
                modal.style.display = "block";
                $(".txtCourseDuration").addClass("error1");
            } else if ($('.txtCoursePrice').val() < 0) {
                er.innerHTML = "Please input a positive value for 'Price'.";
                modal.style.display = "block";
                $(".txtCoursePrice").addClass("error1");
            } else if ($('.txtCourseValidFor').val() < 0 && $('.txtCourseValidFor').val() != '') {
                er.innerHTML = "Please input a positive value for 'Valid For'.";
                modal.style.display = "block";
                $(".txtCourseValidFor").addClass("error1");
            } else if ($('.txtCourseValidFor').val().indexOf(".") != '-1' && $('.txtCourseValidFor').val() < 0 && $('.txtCourseValidFor').val() != '') {
                er.innerHTML = "Please input a positive value for 'Valid For'.";
                modal.style.display = "block";
                $(".txtCourseValidFor").addClass("error1");
            } else if (typeof ($('.ulCourseCategory > li.selected').attr('data-id')) == 'undefined') {

                er.innerHTML = "Please select category.";
                modal.style.display = "block";

                //$('.lgt-notif-error').html('Please select category').addClass('lgt-visible');
            } else {
                var saveOrAssign;
                if ($(this).hasClass('btSave')) saveOrAssign = 'save'; else if ($(this).hasClass('btAssign')) saveOrAssign = 'assign';
                var tempImgURL = '',
                    imgName = '';
                var tmpfile = '';
                if ($('.hdUploadFileName').val() != '') {
                    imgName = $('.hdUploadFileName').val();
                }
                if ($('.hdUploadTmpFile').val() != '') {
                    tmpfile = $('.hdUploadTmpFile').val();
                }
                var act = '<?php echo (empty($data)) ? "create" : "update"; ?>';

                $.ajax({
                    url: window.location.href,
                    type: 'POST',
                    data: {
                        <?php echo (!empty($data)) ? "id: " . $data['remoteid'] . "," : ""; ?>
                        act: act,
                        title: $('.txtCourseTitle').val(),
                        price: parseFloat($('.txtCoursePrice').val()).toFixed(2),
                        categoryid: $('.ulCourseCategory > li.selected').attr('data-id'),
                        des: $('.txtaDescription').val(),
                        duration: parseFloat($('.txtCourseDuration').val()).toFixed(2),
                        learningoutcomes: $('.txtaLearningOutcomes').val(),
                        targetaudience: $('.txtCourseTargetAudience').val(),
                        coursesurvey: $('.hdCourseSurvey').val(),
                        facilitatedcourse: $('.hdFacilitatedCourse').val(),
                        certificatecourse: $('.hdCourseCertificate').val(),
                        enableCreateChats: $('.hdCreateCourseChats').val(),
                        validfor: $('.txtCourseValidFor').val(),
                        tempImgURL: tempImgURL,
                        tmpfile: tmpfile,
                        imgName: imgName
                    },
                    beforeSend: function () {
                        $('body').addClass('overlay2');
                        $('.notification').html('Loading...').fadeIn();
                        //$('.lgt-notif').html('<?php echo JText::_("REQUEST_PROCESSED");?>').addClass('lgt-visible');

                    },
                    success: function (data, textStatus, jqXHR) {
                        var res = JSON.parse(data);

                        //$('.lgt-notif').removeClass('lgt-visible');
                        if (res.error == 1) {
                            $('body').addClass('overlay2');
                            $('.notification').fadeOut();
                            switch (res.comment) {
                                case 'nopermission':
                                    er.innerHTML = "You don\'t have permission to create course.";
                                    modal.style.display = "block";

                                    break;
                                case 'cantinsertrecord':

                                    er.innerHTML = "Can not insert record to database.";
                                    modal.style.display = "block";

                                    break;
                                case 'disabledallenrolmethod':

                                    er.innerHTML = "Can not enrol to this course. Please check enrol method.";
                                    modal.style.display = "block";

                                    //$('.lgt-notif-error').html('Can not enrol to this course. Please check enrol method.').addClass('lgt-visible');
                                    break;
                                case 'alreadypendingapproval':

                                    er.innerHTML = "Course is pending approval. You can not update this course.";
                                    modal.style.display = "block";

                                    //$('.lgt-notif-error').html('Course is pending approval. You can not update this course.').addClass('lgt-visible');
                                    break;
                                case "shortnametaken":
                                    $(".txtCourseTitle").addClass("error1");
                                    $(".mycourse_title").addClass("error1");
                                    er.innerHTML = "Course name already exists. Please choose another name.";
                                    modal.style.display = "block";
                                    break;
                                default:
                                    er.innerHTML = res.comment;
                                    modal.style.display = "block";
                                    // $('.lgt-notif-error').html(res.comment).addClass('lgt-visible');
                                    break;
                            }
                        } else {
                            console.log(res);
                            if (saveOrAssign == 'save') {
                                window.location.href = '<?php echo JUri::base(); ?>course/' + res.data + '.html';
                            }
                            else if (saveOrAssign == 'assign')
                            <?php if (!empty($data)) { ?>
                                window.location.href = '<?php echo JUri::base(); ?>mycourses/assign_<?php echo $data["remoteid"]; ?>.html';
                            <?php } else { ?>
                            window.location.href = '<?php echo JUri::base(); ?>mycourses/assign_' + res.data + '.html';
                            <?php } ?>
                        }
                    }
                });
            }

        });
        <?php } else { ?>
        $('.joomdle-my-courses-create .btSave , .joomdle-my-courses-create .btAssign').click(function () {
            var modal = document.getElementById('ass-message');
            var er = document.getElementById('error-hv');
            $('.closetitle').click(function () {
                modal.style.display = "none";
                 $('body').removeClass('overlay2');
            });
            $('body').addClass('overlay2');
            if ($('.hdUploadFileName').val() == '') {
                er.innerHTML = "Please select an image for course.";
                modal.style.display = "block";
            } else if ($('.joomdle-my-courses-create .btSave').attr('data-disabled') == 1) {
                er.innerHTML = "Course image is uploading.";
                modal.style.display = "block";
            } else if ($('.txtCourseTitle').val().trim() == '' || $('.txtaDescription').val().trim() == '') {
                 $('body').removeClass('overlay2');
                if($('.txtCourseTitle').val().trim() == '' || $('.txtCourseTitle').val().trim().length > 254){
                    $(".txtCourseTitle").addClass("error1");
                    $(".mycourse_title").addClass("error1");
                }
                if ($('.txtCourseTitle').val().trim().length > 254) lgtCreatePopup('withCloseButton', {content: 'Maximum number of characters in course name field is 254. Current: ' + $('.txtCourseTitle').val().trim().length});
                if($('.txtaDescription').val().trim() == ''){
                    $(".txtaDescription").addClass("error1");
                    $(".mycourse_description").addClass("error1");
                }
            } else if ($('.txtCourseDuration').val() != '' && !$.isNumeric($('.txtCourseDuration').val())) {
                $('body').addClass('overlay2');
                er.innerHTML = "Duration is not numeric.";
                modal.style.display = "block";
                $(".txtCourseDuration").addClass("error1");

            } else if ($('.txtCourseDuration').val() != '' && parseInt(getNumAfterDots('' + $('.txtCourseDuration').val())) > 2) {
                $('body').addClass('overlay2');
                er.innerHTML = "'Duration' can only be up to two decimal places.";
                modal.style.display = "block";
                $(".txtCourseDuration").addClass("error1");

            } else if ($('.txtCourseDuration').val() < 0) {
                $('body').addClass('overlay2');
                er.innerHTML = "Please input a positive value for 'Duration'.";
                modal.style.display = "block";
                $(".txtCourseDuration").addClass("error1");
            }
            else if (typeof ($('.ulCourseCategory > li.selected').attr('data-id')) == 'undefined') {
                $('body').addClass('overlay2');

                er.innerHTML = "Please select category.";
                modal.style.display = "block";

            } else {
                if ($('.txtCourseDuration').val() == '') {
                    var duration = 0;
                } else {
                    var duration = $('.txtCourseDuration').val();
                }
                var saveOrAssign;
                if ($(this).hasClass('btSave')) saveOrAssign = 'save'; else if ($(this).hasClass('btAssign')) saveOrAssign = 'assign';
                var tempImgURL = '',
                    imgName = '';
                var tmpfile = '';
                if ($('.hdUploadFileName').val() != '') {
                    imgName = $('.hdUploadFileName').val();
                }
                if ($('.hdUploadTmpFile').val() != '') {
                    tmpfile = $('.hdUploadTmpFile').val();
                }
                var act = '<?php echo (empty($data)) ? "create" : "update"; ?>';

                $.ajax({
                    url: window.location.href,
                    type: 'POST',
                    data: {
                        <?php echo (!empty($data)) ? "id: " . $data['remoteid'] . "," : ""; ?>
                        act: act,
                        title: $('.txtCourseTitle').val(),
                        price: parseFloat($('.txtCoursePrice').val()).toFixed(2),
                        categoryid: $('.ulCourseCategory > li.selected').attr('data-id'),
                        des: $('.txtaDescription').val(),
                        duration: parseFloat($('.txtCourseDuration').val()).toFixed(2),
                        learningoutcomes: $('.txtaLearningOutcomes').val(),
                        targetaudience: $('.txtCourseTargetAudience').val(),
                        coursesurvey: $('.hdCourseSurvey').val(),
                        facilitatedcourse: $('.hdFacilitatedCourse').val(),
                        certificatecourse: $('.hdCourseCertificate').val(),
                        enableCreateChats: $('.hdCreateCourseChats').val(),
                        validfor: $('.txtCourseValidFor').val(),
                        tempImgURL: tempImgURL,
                        tmpfile: tmpfile,
                        imgName: imgName
                    },
                    beforeSend: function () {
                        //$('.lgt-notif').html('<?php echo JText::_("REQUEST_PROCESSED");?>').addClass('lgt-visible');
                        $('body').addClass('overlay2');
                        $('.notification').html('Loading...').fadeIn();
                    },
                    success: function (data, textStatus, jqXHR) {
                        var res = JSON.parse(data);

                        // $('.lgt-notif').removeClass('lgt-visible');
                        if (res.error == 1) {
                            $('body').addClass('overlay2');
                            $('.notification').fadeOut();
                            switch (res.comment) {
                                case 'nopermission':
                                    er.innerHTML = "You don\'t have permission to create course.";
                                    modal.style.display = "block";

                                    break;
                                case 'cantinsertrecord':
                                    er.innerHTML = "Can not insert record to database.";
                                    modal.style.display = "block";

                                    break;
                                case 'disabledallenrolmethod':
                                    er.innerHTML = "Can not enrol to this course. Please check enrol method.";
                                    modal.style.display = "block";

                                    break;
                                case 'alreadypendingapproval':
                                    er.innerHTML = "Course is pending approval. You can not update this course";
                                    modal.style.display = "block";
                                    break;
                                case "shortnametaken":
                                    $(".txtCourseTitle").addClass("error1");
                                    $(".mycourse_title").addClass("error1");
                                    er.innerHTML = "Course name already exists. Please choose another name.";
                                    modal.style.display = "block";
                                    break;
                                default:
                                    er.innerHTML = res.comment;
                                    modal.style.display = "block";
                                    break;
                            }
                        } else {

                            console.log(res);
                            if (saveOrAssign == 'save') {
                                window.location.href = '<?php echo JUri::base(); ?>course/' + res.data + '.html';
                            }
                            else if (saveOrAssign == 'assign')
                            <?php if (!empty($data)) { ?>
                                window.location.href = '<?php echo JUri::base(); ?>mycourses/assign_<?php echo $data["remoteid"]; ?>.html';
                            <?php } else { ?>
                            window.location.href = '<?php echo JUri::base(); ?>mycourses/assign_' + res.data + '.html';
                            <?php } ?>
                        }
                    }
                });
            }

        });
        <?php } ?>

        function getNumAfterDots(num) {
            var length = num.length;
            var index_dots = num.indexOf(".");
            if (index_dots == -1) return 0;
            else
                return (length - 1 - index_dots);
        }

        var files;

        $('.fileUpload').on('change', prepareUpload);

        function prepareUpload(event) {
            readURL(this);
            files = event.target.files;
            $('.formCreateCourse').submit();
        }

        $('.formCreateCourse').on('submit', uploadFiles);

        function uploadFiles(event) {
            var modal = document.getElementById('ass-message');
            var er = document.getElementById('error-hv');
            $('.closetitle').click(function () {
                modal.style.display = "none";
                 $('body').removeClass('overlay2');
            });
            event.stopPropagation();
            event.preventDefault();

            var data = new FormData();
            $.each(files, function (key, value) {
                data.append(key, value);
            });
            data.append('token', '<?php echo $token;?>');
            console.log(data);
            $.ajax({
                url: window.location.href,
                type: 'POST',
                data: data,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {
//                    $('.joomdle-my-courses-create .btSave').attr('data-disabled', 1);
                    // $('body').addClass('overlay2');
                    // $('.notification').html('Loading...').fadeIn();
                },
                success: function (data, textStatus, jqXHR) {
                    var res = JSON.parse(data);
                    console.log(res);
                    // $('body').removeClass('overlay2');
                    // $('.notification').fadeOut();
//                    $('.joomdle-my-courses-create .btSave').attr('data-disabled', 0);
                    if (res.error == 1) {
                        $('body').addClass('overlay2');
                        er.innerHTML = res.comment;
                        modal.style.display = "block";
                    } else {
                        $('.hdUploadFileName').val(res.data[0].name);
                        $('.hdUploadTmpFile').val(res.data[0].tmp_file);
                    }
                }
            });
        }
    })(jQuery);
</script>
