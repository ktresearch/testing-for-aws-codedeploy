<?php
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root().'components/com_joomdle/views/mycourses/tmpl/mycourses.css');

?>
<div class="joomdle-add-to-circle create-course">
    <p class="message-share"><?php echo JText::_('COM_JOOMDLE_MES_SHARE'); ?></p>
    
    <div class="joomdle-add-to-circle-learning-provider-circles">
        <p class="block-title"><?php echo JText::_('LEARNING_PROVIDER_CIRCLES'); ?></p>
        <?php
            $gridArr = array();
            if (!empty($this->sharedGroups)) {
                $gridArr = $this->sharedGroups;
            }
            if (empty($this->learningProviderGroups)) {
                echo '<p>'.JText::_('COM_JOOMDLE_NO_CIRCLE').'</p>';
            } else {
                foreach ($this->learningProviderGroups as $group) {
                    $table = JTable::getInstance('Group', 'CTable');
                    $table->load($group->id);
        ?>
                <div class="group-circle <?php echo (in_array($group->id, $gridArr)) ? 'active' : '';?>" data-grid="<?php echo $group->moodlecategoryid;?>">
                    <div class="checkbox-circle">
                        <div class="status"></div>
                    </div>
                    <div class="information">
                        <div class="group-picture">
                            <img src="<?php echo $table->getAvatar(); ?>"/>
                        </div>
                        <div class="group-info">
                            <p class="circle-name"><?php echo $group->name; ?></p>
                            <p class="circle-owner"><?php echo JText::_('OWNER').': '.JFactory::getUser($group->ownerid)->name; ?></p>
                            <p class="margin-0 joms--description">
                                <?php if (strlen($group->description) >= 50) echo substr_replace($group->description, '...', 50); else echo $group->description; ?>
                            </p>
                        </div>
                    </div>
                </div>
        <?php
                }
            }
        ?>
        <button class="btAddToCircle" <?php echo (empty($this->learningProviderGroups)) ? 'data-disabled="1"' : '';?> type="button"><?php echo JText::_('COM_JOOMDLE_BT_ADD_TO_CIRCLE'); ?></button>
    </div>
</div>
<div class="notification"></div>
<script type="text/javascript">
    (function($) {
        $('.group-circle').click(function() {
            
            if ($(this).parent().hasClass('joomdle-add-to-circle-my-circles')) {
                $(this).toggleClass('active');
                var myCirclesIsChosen = false;
                $('.joomdle-add-to-circle-my-circles .group-circle').each(function() {
                    if ($(this).hasClass('active')) {
                        myCirclesIsChosen = true;
                    }
                });
                if (myCirclesIsChosen) {
                    $('.joomdle-add-to-circle-learning-provider-circles').addClass('overlay');
                    $('.joomdle-add-to-circle-learning-provider-circles .group-circle').removeClass('active');
                } else {
                    $('.joomdle-add-to-circle-learning-provider-circles').removeClass('overlay');
                    $('.joomdle-add-to-circle-learning-provider-circles .group-circle').removeClass('active');
                }
                
            } else if ($(this).parent().hasClass('joomdle-add-to-circle-learning-provider-circles')) {
                if ($(this).hasClass('active')) {
                    $('.joomdle-add-to-circle-learning-provider-circles .group-circle').removeClass('active');
                    $('.joomdle-add-to-circle-my-circles').removeClass('overlay');
                    $('.joomdle-add-to-circle-my-circles .group-circle').removeClass('active');
                } else {
                    $('.joomdle-add-to-circle-learning-provider-circles .group-circle').removeClass('active');
                    $(this).addClass('active');
                    $('.joomdle-add-to-circle-my-circles').addClass('overlay');
                    $('.joomdle-add-to-circle-my-circles .group-circle').removeClass('active');
                }               
            }

        });
        $('.btAddToCircle').click(function() {
            if ($(this).attr('data-disabled') != 1) {
                var data = [];
                var act = 'sharetocircle';
                var i = 0;
                var grid = 0;
                $('.group-circle').each(function() {
                    if ($(this).hasClass('active')) {
                        grid = jQuery(this).attr('data-grid');
                    } 
                });
                if (grid == 0) {
                    alert('Please select circle.');
                } else {
                    window.location.href = "<?php echo JUri::base();?>mycourses/create_0_" + grid + "_lpcourse_.html";
                    return false;
                }
            }
        });
    })(jQuery);
</script>