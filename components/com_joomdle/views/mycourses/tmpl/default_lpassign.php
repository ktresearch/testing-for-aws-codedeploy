<?php
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root().'components/com_joomdle/views/mycourses/tmpl/mycourses.css');

?>
<div class="joomdle-assign-roles">
    <h1 class="form-title"><?php echo $this->moodle_category_info['name'] ; ?></h1>
    <?php
        $roleidArr = $this->roleidArr;
        foreach ($this->friends as $k => $v) {
    ?>
            <div class="friend" data-frid="<?php echo $v->id;?>">
                <div class="information">
                    <div class="friend-picture">
                        <a href="<?php echo $v->profileLink; ?>">
                            <img src="<?php echo $v->getAvatar(); ?>"/>
                            <p><?php echo $v->getDisplayName(); ?></p>
                        </a>
                    </div>
                </div>
                <div class="roles">

                    <div class="role <?php echo (in_array($v->username, $roleidArr)) ? 'active' : '';?>" data-rid="3">
                        <div class="status"></div>
                        <p class="role-name"><?php echo JText::_('COM_JOOMDLE_CONTENT_CREATOR'); ?></p>
                    </div>

                </div>
            </div>
    <?php
        }
    ?>
    <button class="btAssignRoles" type="button"><?php echo JText::_('COM_JOOMDLE_ASSIGN_ROLES'); ?></button>
</div>
<div class="notification"></div>
<script type="text/javascript">
    (function($) {
        $('.roles .role').click(function() {
            $(this).toggleClass('active');
        });
        $('.btAssignRoles').click(function() {
            var data = [];
            var act = 'assign';
            $('.role').each(function() {
                var rc = {};
                rc.frid = $(this).parent().parent().attr('data-frid');
                rc.rid = $(this).attr('data-rid');
                if ($(this).hasClass('active')) rc.status = 1; else rc.status = 0;
                data.push(rc);
            });
            $.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    act: act,
                    data: data
                },
                beforeSend: function() {
                    $('body').addClass('overlay2');
                    $('.notification').html('Loading...').fadeIn();
                },
                success: function(data, textStatus, jqXHR) {
                    var res = JSON.parse(data);

                    $('body').removeClass('overlay2');
                    $('.notification').fadeOut();
                    if (res.error == 1) {
                        switch (res.comment) {
                                case 'nopermission':
                                alert('You don\'t have permission to create course.');
                                break;
                                case 'notexisteduser':
                                alert('Can not insert record to database.');
                                break;
                                case 'disabledallmethod':
                                alert('Can not enrol to this course. Please check enrol method.');
                                break;
                                case 'invalidrole':
                                alert('The selected role is invalid.');
                                break;
                                case 'assignnotpermitted':
                                alert('Not Permitted.');
                                break;
                                case 'unassignnotpermitted':
                                alert('Not Permitted.');
                                break;
                                default: 
                                alert('Unknown Error.');
                                break;
                            }
                    } else {
                        console.log(res);
                        window.location.href = '<?php echo JUri::base(); ?>mycourses/list.html';
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                }
            });
        });
    })(jQuery);
</script>