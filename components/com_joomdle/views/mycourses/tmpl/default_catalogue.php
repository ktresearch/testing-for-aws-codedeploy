<?php defined('_JEXEC') or die('Restricted access'); ?>
<script src="<?php echo JUri::root().'components/com_joomdle/js/jquery-2.1.1.min.js'; ?>" type="text/javascript"></script>
<?php
require_once(JPATH_BASE . '/components/com_community/libraries/core.php');

JLoader::import('joomla.user.helper');

$itemid = JoomdleHelperContent::getMenuItem();

$session = JFactory::getSession();
$device = $session->get('device');

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root().'/components/com_joomdle/css/swiper.min.css');
$document->addScript(JUri::root().'/components/com_joomdle/js/swiper.min.js');
$document->addStyleSheet(JUri::root().'components/com_joomdle/views/mycourses/tmpl/mycourses.css');

$class_tablet = 'course-box';
?>

<?php if (!empty($this->action) && $this->action == 'remove') {
    if ($this->course_info['remoteid'] != 0) {
        ?>
        <div class="joomdle-remove">
            <p><?php echo Jtext::_('COM_JOOMDLE_REMOVE_COURSE_TEXT'); ?></p>
            <button class="yes"><?php echo Jtext::_('COM_JOOMDLE_YES'); ?></button>
            <button class="no"><?php echo Jtext::_('COM_JOOMDLE_NO'); ?></button>
        </div>
        <div class="notification"></div>

        <script type="text/javascript">
            (function ($) {
                $('.joomdle-remove .no').click(function () {
                    window.location.href = "<?php echo JURI::base().'mycourses/catalogue.html';?>";
                });
                $('.joomdle-remove .yes').click(function () {
                    var act = 'remove';
                    $.ajax({
                        url: "index.php?option=com_hikashop&ctrl=lpapi&task=removecourse&courseid=<?php echo $this->course_id;?>",
                        type: 'POST',
                        data: {
                            <?php echo (isset($this->course_id)) ? "courseid: ".$this->course_id."," : "" ; ?>
                            act: act
                        },
                        beforeSend: function () {
                            $('body').addClass('overlay2');
                            $('.notification').html('Loading...').fadeIn();
                        },
                        success: function (data, textStatus, jqXHR) {
                            var res = JSON.parse(data);

                            $('body').removeClass('overlay2');
                            $('.notification').fadeOut();
                            if(data['status']){
                                console.log(data['message']);
                                window.location.href = "<?php echo JURI::base().'mycourses/catalogue.html';?>";
                            }else{
                                console.log(data['message']);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log('ERRORS: ' + textStatus);
                            $('body').removeClass('overlay2');
                            $('.notification').fadeOut();
                        }
                    });
                });
            })(jQuery);
        </script>
        <?php
    } else {
        echo '<p class="tcenter" style="padding-top:30px;">'.JText::_('COM_JOOMDLE_COURSE_REMOVED').'</p>';
    }
} else { ?>
    <div class="joomdle-mylearning joomdle_course_catalogue">
        <div class="joomdle_mylearning_header changePosition">
            <a href="<?php echo 'mycourses/list.html'; ?>">
                <span class="left myCourses">
                    <?php echo JText::_('COM_JOOMDLE_COURSES'); ?>
            </span>
            </a>
            <a href="<?php echo 'mycourses/catalogue.html'; ?>">
                <span class="right catalogue active">
                    <?php echo JText::_('COM_JOOMDLE_CATALOGUE'); ?>
            </span>
            </a>
        </div>

        <div class="joomdle_mylearning">
            <!-- ===================== Published Courses Block ======================= -->
            <div class="joomdle_mycourses joomdle_published_courses">
                <div class="swiper-container">
                    <div class="courses-ma-title">
                        <span class="title-text">
                            <?php
                            echo (count($this->cursos) > 0) ? JText::_('COM_JOOMDLE_LP_PUBLISH').' <strong>'.count($this->cursos).'</strong>' : JText::_('COM_JOOMDLE_LP_PUBLISH');
                            ?>
                        </span>
                    </div>
                    <div class="swiper-wrapper">
                        <?php
                        if (!is_array ($this->cursos) || empty($this->cursos)) {
                            echo '<span class="bold cred tcenter bg">'.JText::_('COM_JOOMDLE_NO_COURSE_YET').'.</span>';
                        } else {
                            $i = 0;
                            foreach ($this->cursos as  $curso) :
                                $url = JURI::base().'course/'.$curso['id'].'.html';
                                $course_image = $curso['filepath'].$curso['filename'];

                                (array_key_exists('created', $curso) && !is_null($curso['created'])) ? $time = date('F Y', $curso['created']) : JText::_('UNKNOWN');
                                $completion_des = JText::_('COM_JOOMDLE_RELEASED').': '.$time;
                                ?>
                                <div class="swiper-slide" data-cid="<?php echo $curso['id']?>" data-catid="<?php echo $curso['category']; ?>">
                                    <div class="<?php echo $class_tablet;?>">
                                        <div class="course_content">
                                            <div class="clearfix"></div>
                                            <div class="mylearning-img">
                                                <!--<a href="<?php // echo $url; ?>">-->
                                                <div id="images" class="lazyload" data-src="<?php echo $course_image.'?'.(time()*1000); ?>"></div>
                                                <!--</a>-->
                                                <div class="joomdle-manage-btn btnright btPublish">
                                                    <?php echo JText::_('COM_JOOMDLE_UNPUBLISH'); ?>
                                            </div>
                                            </div>
                                            <div class="content_c">
                                                <div class="clearfix"></div>
                                                <div class="course_title">
                                                    <?php
                                                         $link = JUri::base()."providers/product/".$curso['hikashopId']."-".$curso['hikashopAlias'];//hikashop_contentLink('product&task=show&cid='.$course->product_id.'&name='.$course->product_alias);//hikashop_completelink("product&task=show&cid[]=".$course->product_id);//  
                                                        echo "<a class=\"course_title\" href=".$link.">".$curso['fullname']."</a>"; 
                                                    ?>
                                                </div>

                                                <div class="right">
                                                    <p>
                                                        <?php
                                                        $summary = strip_tags($curso['summary']);
                                                        if (strlen($summary) > 68)
                                                            echo substr($summary, 0, 68).'...';
                                                        else
                                                            echo $summary;
                                                        ?>
                                                    </p>
                                                </div>
                                                <div class="clearfix"></div>
                                                </div>

                                        </div>
                                    </div>
                                </div>
                                <?php
                                $i++;
                            endforeach;
                        }
                        ?>
                    </div>
                </div>
                <?php if (count($this->cursos) != 0 && $device != 'mobile'){?>
                    <div class="swiper-button-next1"><img class="icon-next" src="/images/NextIcon.png"></div>
                    <div class="swiper-button-prev1"><img class="icon-next" src="/images/PreviousIcon.png"></div>
                <?php }?>
                <div class="removeCoursePopup">
                    <p><?php echo JText::_('COM_JOOMDLE_RMPOPUP_TEXT1'); ?></p>
                    <p><?php echo JText::_('COM_JOOMDLE_RMPOPUP_TEXT2'); ?></p>
                    <div class="buttons">
                        <button class="btCancel"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                        <button class="btYes"><?php echo JText::_('COM_JOOMDLE_YES'); ?></button>
                    </div>
                </div>
            </div>
            <!-- End Block My Courses -->
            <!--<hr class="jd-inline" />-->

            <!-- ================= Unpublish LP Block ========================= -->
            <div class="joomdle_courseslp joomdle_unpublished_courses">
                <div class="swiper-container">
                    <div class="courses-ma-title">
                        <span class="title-text">
                            <?php
                            echo (count($this->unpublishedcourse) > 0) ? JText::_('COM_JOOMDLE_LP_UNPUBLISH').' <strong>'.count($this->unpublishedcourse).'</strong>' : JText::_('COM_JOOMDLE_LP_UNPUBLISH');
                            ?>
                        </span>
                    </div>
                    <div class="swiper-wrapper">
                        <?php
                        if (!is_array ($this->unpublishedcourse) || empty($this->unpublishedcourse)) {
                            echo '<span class="bold cred tcenter bg">'.JText::_('COM_JOOMDLE_NO_COURSE_YET').'.</span>';
                        } else {
                            $i = 0;
                            foreach ($this->unpublishedcourse as  $curso) :
                                $url = JURI::base().'course/'.$curso['id'].'.html';
                                $course_image = $curso['filepath'].$curso['filename'];

                                (array_key_exists('created', $curso) && !is_null($curso['created'])) ? $time = date('F Y', $curso['created']) : JText::_('UNKNOWN');
                                $completion_des = JText::_('COM_JOOMDLE_RELEASED').': '.$time;
                                
                                $groups = CFactory::getModel('groups');
                                $groupid = (int)$groups->getLPGroupId($curso['category']);
                                ?>
                                <div class="swiper-slide" data-cid="<?php echo $curso['id'];?>" data-catid="<?php echo $curso['category']; ?> " >
                                    <div class="<?php echo $class_tablet;?>">
                                        <div class="course_content">
                                            <div class="clearfix"></div>
                                            <div class="mylearning-img">
                                                <!--<a href="<?php // echo $url; ?>">-->
                                                <div id="images" class="lazyload" data-src="<?php echo $course_image.'?'.(time()*1000); ?>"></div>
                                                <!--</a>-->
                                                <img class="dropdown-button" onclick="" src="<?php echo JURI::root();?>media/joomdle/images/icon/ManageCourses/dropdown.png"/>
                                            
                                                <ul class="ma-action">
                                                    <li>
                                                        <a href="javascript:" class="btRemove">
                                                            <span><?php echo JText::_('COM_JOOMDLE_BUTTON_REMOVE');?></span>
                                                </a>
                                                    </li>
                                                    <li>
                                                        <a class="btAssign" href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=assignRole&r=cc&coid='.$curso['id'].'&grid='.$groupid.'&ma=1') ?>">
                                                            <span><?php echo JText::_('COM_JOOMDLE_ASSIGN'); ?></span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);" class="btSubmit" onclick="sendForApproval(<?php echo $curso['id'] . ',' . $curso['category']; ?>);">
                                                            <span><?php echo JText::_('COM_JOOMDLE_SUBMIT'); ?></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="content_c">
                                                <div class="clearfix"></div>
                                                <div class="course_title">
                                                    <?php  echo "<a class=\"course_title\" href=\"$url\">".$curso['fullname']."</a>"; ?>
                                                </div>

                                                <div class="right">
                                                    <p>
                                                        <?php
                                                        $summary = strip_tags($curso['summary']);
                                                        if (strlen($summary) > 68)
                                                            echo substr($summary, 0, 68).'...';
                                                        else
                                                            echo $summary;
                                                        ?>
                                                    </p>
                                                </div>
                                                <div class="clearfix"></div>
                                                    </div>

                                                    </div>
                                                </div>
                                            </div>

                                <?php
                                $i++;
                            endforeach;
                        }
                        ?>
                    </div>
                </div>
                <?php if (count($this->unpublishedcourse) != 0 && $device != 'mobile'){?>
                    <div class="swiper-button-next2"><img class="icon-next" src="/images/NextIcon.png"></div>
                    <div class="swiper-button-prev2"><img class="icon-next" src="/images/PreviousIcon.png"></div>
                <?php }?>
                <div class="removeCoursePopup">
                    <p><?php echo JText::_('COM_JOOMDLE_RMPOPUP_TEXT1'); ?></p>
                    <p><?php echo JText::_('COM_JOOMDLE_RMPOPUP_TEXT2'); ?></p>
                    <div class="buttons">
                        <button class="btCancel"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                        <button class="btYes"><?php echo JText::_('COM_JOOMDLE_YES'); ?></button>
                    </div>
                </div>
            </div>
            <!-- End Block Pending Approval -->
        </div>
    </div>
    <div class="notification"></div>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            var title_p = '<?php echo JText::_('COM_JOOMDLE_CATALOGUE'); ?>';
            jQuery('.navbar-header .navbar-title span').html(title_p);

            jQuery('.joomdle-mylearning .arrowMenuDown').click(function() {
                jQuery(this).find('.menuAction').toggle();
                jQuery(this).find('.triangle').toggle();
            });
            jQuery('.btRemove').click(function() {
                var cid = jQuery(this).parent().parent().parent().parent().parent('.swiper-slide').attr('data-cid');
                var catid = jQuery(this).parent().parent().parent().parent().parent('.swiper-slide').attr('data-catid');
                jQuery('.removeCoursePopup').fadeIn().attr('data-cid', cid).attr('data-catid',catid);
                jQuery('body').addClass('overlay2');
            });
            jQuery('.removeCoursePopup .btCancel').click(function() {
                jQuery('.removeCoursePopup').fadeOut().attr('data-cid', '').attr('data-catid','');
                jQuery('body').removeClass('overlay2');
            });
            jQuery('.removeCoursePopup .btYes').click(function() {
                removelp(jQuery('.removeCoursePopup').attr('data-cid'),jQuery('.removeCoursePopup').attr('data-catid'));
            });
//            jQuery('.ma-action .btAssign').click(function() {
//                var cid = jQuery(this).parents('.swiper-slide').attr('data-cid');
//                window.location.href = "<?php // echo JUri::base(); ?>mycourses/assign_"+cid+".html";
//            });
            jQuery('.btPublish').click(function() {
                var cid = jQuery(this).parents('.swiper-slide').attr('data-cid');
                var catid = jQuery(this).parents('.swiper-slide').attr('data-catid');
                console.log(catid);
                unpublishlp(cid,catid,"unpublishlp");
            });
            
            jQuery('.dropdown-button').click(function() {
                jQuery(this).next(".ma-action").toggle();
            });
        });

        function removelp(courseid,catid){
            jQuery.ajax({
                url: "index.php?option=com_hikashop&ctrl=lpapi&task=removecourse&courseid="+courseid+"&categoryid="+catid,
                type: 'POST',
                data: {
                    course_id:courseid
                },
                beforeSend: function () {
                    $('body').addClass('overlay2');
                    $('.notification').html('Loading...').fadeIn();
                },
                success: function (data, textStatus, jqXHR) {
                    $('body').removeClass('overlay2');
                    $('.notification').fadeOut();
                    if(data['status']){
                        console.log(data['message']);
                        window.location.href = "<?php echo JURI::base().'mycourses/catalogue.html';?>";
                    }else{
                        console.log(error);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                    $('body').removeClass('overlay2');
                    $('.notification').fadeOut();
                }
            });
        }

        function unpublishlp(courseid, catid, act) {
            var url = "<?php echo JURI::base();?>index.php?option=com_hikashop&ctrl=lpapi&task=unpublishcourse&categoryid="+catid+"&courseid="+courseid;
            var action = 'unpublishlp';
            jQuery.ajax({
                url: url,
                type: 'POST',
                data: {
                    course_id: courseid,
                    action: action,
                    cat_id: catid,
                    act: act
                },
                beforeSend: function () {
                    $('body').addClass('overlay2');
                    $('.notification').html('Loading...').fadeIn();
                },
                success: function (data, textStatus, jqXHR) {
                    $('body').removeClass('overlay2');
                    $('.notification').fadeOut();
                    if(data['status']){
                        console.log(data['message']);
                        window.location.href = "<?php echo JURI::base().'mycourses/catalogue.html';?>";
                    }else{
                        console.log(error);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                }
            });
        }

        function sendForApproval(courseid, catid) {
            var url = "<?php echo JURI::base();?>mycourses/sendapprove_" + courseid + "_" + catid + "_.html";
            var act = 'sendapprove';
            jQuery.ajax({
                url: url,
                type: 'POST',
                data: {
                    course_id: courseid,
                    action: act,
                    cat_id: catid
                },
                beforeSend: function () {
                    $('body').addClass('overlay2');
                    $('.notification').html('Loading...').fadeIn();
                },
                success: function (data, textStatus, jqXHR) {
                    var res = JSON.parse(data);

                    $('body').removeClass('overlay2');
                    $('.notification').fadeOut();
                    if (res.error == 1) {
                        alert(res.message);
                    } else {
                        console.log(res.message);
                        window.location.href = "<?php echo JURI::base() . 'mycourses/list.html';?>";
                    }
                }
            });
        }
    </script>
    <?php
    if($device == 'mobile') {
        $slideview = '1.15';
        $space = '11';
    } else if($device == 'tablet') {
        $slideview = '2';
        $space = '11';
    } else {
        $slideview = '2';
        $space = '11';
    }
    if ($device == 'mobile'){
        $touch = ' allowTouchMove : true,centeredSlides: true,';
        $centerSlides = ' centeredSlides: true,';
    } else  {
        $touch = ' allowTouchMove : false,noSwipingClass : \'swiper-slide\',
                noswipingClass: \'swiper-slide\',';
        $centerSlides = ' centeredSlides: false,';
    }
    ?>
    <script type="text/javascript">
        var x = screen.width;
        var a = 0;
        if (x < 390) a = 1.1;
        if (x >= 390 && x < 481) a = 1.3;
        if (x >= 481 && x <= 736) a = 1.5;
        if (x > 736 && x < 1024) a = 2.5;
        if (x >= 1024 && x <= 1366) a = 3;
        if (x > 1366 && x <= 1800) a = 4;
        if (x > 1800) a = 5;

        var swiper = new Swiper('.joomdle_published_courses .swiper-container',{
            <?php echo $centerSlides;?>
            pagination: '.joomdle_published_courses .swiper-pagination',
            slidesPerView: <?php echo $slideview;?>,
            nextButton: '.joomdle_published_courses .swiper-button-next1',
            prevButton: '.joomdle_published_courses .swiper-button-prev1',
            spaceBetween: <?php echo $space; ?>
        });
        var swiper = new Swiper('.joomdle_unpublished_courses .swiper-container',{
            <?php echo $centerSlides;?>
            pagination: '.joomdle_unpublished_courses .swiper-pagination',
            slidesPerView: <?php echo $slideview;?>,
            nextButton: '.joomdle_unpublished_courses .swiper-button-next2',
            prevButton: '.joomdle_unpublished_courses .swiper-button-prev2',
            spaceBetween: <?php echo $space; ?>
        });
    </script>
<?php } ?>
