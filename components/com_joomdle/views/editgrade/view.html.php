<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class JoomdleViewEditgrade extends JViewLegacy {
    function display($tpl = null) {
        
        $app = JFactory::getApplication();
        $params = $app->getParams();
    	$this->assignRef('params', $params);
        $currentUser = JFactory::getUser();
        $currentUsername = $currentUser->username;

        $course_id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
        if (!$course_id) $course_id =  JRequest::getVar( 'course_id' );
        if (!$course_id)
            $course_id = $params->get( 'course_id' );

        $course_id = (int) $course_id;

    	if (!$course_id) {
            echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
            return;
    	}
        $this->hasPermission = JFactory::hasPermission($course_id, $currentUsername);
        $user_role = array();
        if (!empty($this->hasPermission)) {
            foreach (json_decode($this->hasPermission[0]['role']) as $r) {
                $user_role[] = $r->sortname;
            }
        }
        if ($user_role[0] != 'manager'  && $user_role[0] != 'teacher' && $user_role[0] != 'editingteacher' && $user_role[0] != 'coursecreator') {
            echo JText::_('COM_JOOMDLE_COURSE_CONTENT_NOT_ALLOWED');
            return;
        }

        $user_id = JRequest::getVar( 'user_id', null, 'NEWURLFORM' );
        if (!$user_id)
            $user_id = JRequest::getVar( 'user_id' );
        if (!$user_id)
            $user_id = $params->get('user_id');

        $user_id = (int)$user_id;
        $user = JFactory::getUser ($user_id);
        $username = $user->username;

        $this->user = $username;
        $this->course = $course_id;

        
        $this->grades = JoomdleHelperContent::call_method ( 'get_course_grades_by_category', (int) $course_id, $username);

        parent::display($tpl);
    }
}