<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 
 * @date: 30 june 2016
 * @author: KV team
 * @package: joomdle edit grade (for facilitator)
 */

defined('_JEXEC') or die('Restricted access');


require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
?>
<div class="joomdle-course joomdle-edit <?php echo $this->pageclass_sfx?>">
    <h2 class="title-course"><?php echo JText::_('COM_JOOMDLE_COURSE_OVERALLGRADE'); ?></h2>
    <?php 
            $user = JFactory::getUser($this->user);
            if($user->id <= 0) {
                $avatar = '/components/com_community/assets/user-Male.png';
            } else {
                $users = CFactory::getUser($user->id);
                // get avatar
                $avatar = $users->getAvatar();
            }
            
            $grade_letters = $this->grades[0]['gradeoptionsletters'];
            
    ?>
    <form id="grade-student" action="<?php echo JRoute::_('index.php?option=com_joomdle&task=saveGrade'); ?>" method="post">
     <div class="avatar-overall"><a class="nav-avatar"><img alt="<?php echo $users->name; ?>" src="<?php echo $avatar ?>"/></a></div>
     <div class="user-grade">
          <div class="name"><?php echo $users->name;; ?></div>
          <div class="grade">
              <div class="grade-title"><?php echo JText::_('COM_JOOMDLE_COURSE_OVERALLGRADE'); ?></div>
              <div class="select-grade">
                  <?php if($this->grades[0]['gradeitemid']) { ?>
                <select class="grade-option" name="grade_value">
                    <?php
                    //check grade numeric or letter
                        foreach ($grade_letters as $grade) {
                            ?>
                            <option value="<?php echo (float)$grade['value']; ?>" <?php if($this->grades[0]['finalgradeletters'] == $grade['name']) { ?> selected="selected" <?php } ?>><?php echo JText::_('COM_JOOMDLE_GRADE'); ?>: <?php echo $grade['name']; ?></option>
                            <?php
                        }
                    ?>
                </select>
                  <?php } ?>
              </div>
          </div>
     </div>
     <?php
     // get device
    $session = JFactory::getSession();
    $device = $session->get('device');
    if($device == 'mobile') {
        $cols = 5;
        $rows = 12;
    } else {
        $cols = 10;
        $rows = 15;
    }
     ?>
     <div class="grade-feedback">
         <span><?php echo JText::_('COM_JOOMDLE_GRADE_COMMENT'); ?></span>
         <div class="feedback-content">
             <textarea id="txtFeedback" name="feedback" rows="<?php echo $rows; ?>" cols="<?php echo $cols; ?>"><?php echo $this->grades[0]['gradefeedback']; ?></textarea>
         </div>
     </div>
     <div class="grade-button">
         <input type="hidden" name="username" value="<?php echo $this->user; ?>" />
         <input type="hidden" name="grade_id" value="<?php echo $this->grades[0]['gradeid']; ?>" />
         <input type="hidden" name="gradeitem_id" value="<?php echo $this->grades[0]['gradeitemid']; ?>" />
         <input type="hidden" name="course" value="<?php echo $this->course; ?>" />
         <input type="button" onclick="history.go(-1)" value="<?php echo JText::_('COM_JOOMDLE_GRADE_EDIT_CANCEL'); ?>" class="button-left">
         <input type="submit" value="<?php echo JText::_('COM_JOOMDLE_GRADE_EDIT_SAVE'); ?>" class="button-right">
     </div>
    </form>
</div>