<?php
defined('_JEXEC') or die('Restricted access'); 

$itemid = JoomdleHelperContent::getMenuItem();
$session = JFactory::getSession();
$device = $session->get('device');

require_once(JPATH_BASE . '/components/com_community/libraries/core.php');

$searchString = JRequest::getVar('search');
$searchString = (string)$searchString;
$user = JFactory::getUser();
$username = $user->username;

$groupModel = CFactory::getModel('groups');

function custom_echo($x, $length)
{
    if (strlen($x) <= $length) {
        return $x;
    } else {
        $y = substr($x, 0, $length) . '...';
        return $y;
    }
}

if ($device == 'mobile') {
    $the_first_search_course = (int) JText::_('COM_JOOMDLE_LIMIT_MOBILE');
} else {
    $the_first_search_course = (int) JText::_('COM_JOOMDLE_LIMIT');
}
//      JoomdleHelperContent::call_method('search_courses', $searchString,'','','');
$listCoursesSearch = $groupModel->getCoursesForSearch($user->id);
$courses = array();
$listgroups = array();
if ($listCoursesSearch) {
    foreach ($listCoursesSearch as $course) {
        $courses[] = $course->courseid;
        if (!in_array($course->groupid, $listgroups)) {
            $listgroups[] = $course->groupid;
        }
    }
}
if (count($courses) > 0) {
    $listcourses = implode(',', $courses);
} else {
    $listcourses = '';
}

$list_search_course_all = JoomdleHelperContent::call_method('search_courses', $searchString, 'created', $listcourses, $username, $the_first_search_course);
$sl = $list_search_course_all["total_course"];
$number = ceil($sl/JText::_('COM_JOOMDLE_LIMIT_MORE'));

$f = 'theend';
?>

<!--    TABLET-->
<?php if (!empty($this->action) && $this->action == 'remove') { ?>

<?php } else { ?>
    <script type="text/javascript">
        var width = jQuery(window).width();
        jQuery(document).ready(function() {
            jQuery(".search-all-type-form").show();
            jQuery("#searchlearning").show();
            jQuery(".navbar-brand").hide();
            jQuery('.navbar-toggle').hide();
            jQuery('.navbar-notification').toggleClass('hide');
            jQuery('.navbar-search-all').toggleClass('hide');
            jQuery(".navbar-menu-icon").hide();
            jQuery('#searchlearning .search-text').val('<?php echo $searchString;?>');
            if (width >= 930 && width <= 1005) {
                jQuery(".topmenu-bar .navbar-nav li").last().hide();
            }
            if (width >= 830 && width < 930) {
                jQuery(".topmenu-bar .navbar-nav li").last().hide();
                jQuery(".topmenu-bar .navbar-nav li:nth-last-child(2)").last().hide();
            }
            if (width >= 754 && width < 830) {
                jQuery(".topmenu-bar .navbar-nav li").last().hide();
                jQuery(".topmenu-bar .navbar-nav li:nth-last-child(2)").last().hide();
                jQuery(".topmenu-bar .navbar-nav li:nth-last-child(3)").last().hide();
            }
        });
        var a = 2,
            cl = "",
            n = <?php echo $sl?>;
        var s = 0;
        var dev = "<?php echo $device;?>";
        var loading  = false; //to prevents multipal ajax loads
        var full = <?php echo ($sl <= $the_first_search_course) ? 1 : 0; ?>;
        var total_pages = <?php echo $number;?>;
        
        function theend() {
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    act: "load_more",
                    scourse: "<?php echo $searchString; ?>",
                    id: a
                },
                beforeSend: function () {
                    jQuery('.scoures_wrapper').addClass('loadingggg');
                    jQuery('.scoures_container').append('<div class="divLoadMore">Loading...<div class="loading"></div></div>');
                    if (cl == "") {
                        cl = "load";
                    }
                    else return false;
                },
                success: function (data, textStatus, jqXHR) {
                    var res = JSON.parse(data);
                    var status = res.status;
                    var b = res.courses;

                    jQuery.each(b, function (key, value) {
                        var enroled = b[key]['enroled'];
                        var is_learner = b[key]['isLearner'];
                        var course_img = b[key]['course_img'];
                        var course_id = b[key]['remoteid'];
                        var course_name = b[key]['fullname'];
                        var url = b[key]['course_url'];
                        var owner_name = b[key]['owner_name'];
                        var owner_img = b[key]['owner_img'];
                        var circleid = b[key]['circleid'];
                        var circle_name = b[key]['circle_name'];
                        var circle_url = b[key]['circle_url'];
                        var circle_img = b[key]['circle_img'];
                        var ismembercircle = b[key]['ismembercircle'];
                        var course_des = b[key]['course_des'];
                        var learningoutcomes = b[key]['learningoutcomes'];
                        var targetaudience = b[key]['targetaudience'];
                        var duration = b[key]['duration'];
                        var stt = b[key]['stt'];

                        if (ismembercircle) {
                            var class_m = 'is_member';
                        }

                        s = parseInt(stt) + 1;
                        jQuery('.count_course_s').html(s);
                        if (s == <?php echo $sl;?>) full = 1;

                        var html = '';

                        html = '<div class="scourses_courseinfo" data-cid="' + course_id + '" >' +
                            '<div class="">' +
                                '<div class="course_content">' +
                                '<div class="clearfix"></div>' +
                            '<div class="mylearning-img">';
                        if (is_learner==false && enroled==false) {
                            if (ismembercircle==false) {
                                html += '<div class="left" id="images" style="background-image: url(\'' + course_img + '\')" onclick="poupCourse(' + course_id + ');"></div>' +
                                        '<button class="btSubscribe" onclick="popupJoinCircle(' + circleid + ');" data-id="' + course_id + '"><?php echo JText::_('COM_COMMUNITY_BUTTON_SUBSCRIBE'); ?>' +
                                        '</button>';
                            } else {
                                html += '<div class="left" id="images" style="background-image: url(\'' + course_img + '\')" onclick="poupCourse(' + course_id + ');"></div>' +
                                        '<button class="btSubscribe" onclick="subscribeCourse(' + course_id + ');" data-id="' + course_id + '"><?php echo JText::_('COM_COMMUNITY_BUTTON_SUBSCRIBE'); ?>' +
                                        '</button>';
                            }  
                        } else {
                            html += '<a href="' + url + '">' +
                                    '<div class="left" id="images" style="background-image: url(\'' + course_img + '\')"></div>' +
                                    '</a>';
                            if (is_learner==true) {
                                html += '<button class="btUnsubscribe" data-id="' + course_id + '" onclick="popupUnsubscribeCourse(' + course_id + ');"><?php echo JText::_('COM_COMMUNITY_BUTTON_UNSUBSCRIBE'); ?>' +
                                        '</button>';
                            }
                        }
                        html += '</div>' +
                                '<div class="content_c_search">' +
                                '<div class="clearfix"></div>' +
                            '<div class="course_title">';
                        if (!is_learner && !enroled) { 
                            html += '<span class="course_title_search" onclick="poupCourse(' + course_id + ');">' + course_name + '</span>';
                        } else {
                            html += '<a class="course_title_search" href="' + url + '">' + course_name + '</a>';
                        }
                            html += '</div>' +
                            '<div class="coursecircle_info" >' +
                                '<div class="cc_left">' +
                                    '<div class="right">' +
                                        '<p class="owner_txt"><?php echo JText::_('COM_JOOMDLE_SEARCH_BY') . ' '; ?><span class="owner_course">' + owner_name + '</span></p>' +
                                '</div>' +
                                    '<div class="circle_course">' +
                                        '<p class="circle_txt"><?php echo JText::_('COM_JOOMDLE_SEARCH_POSTED_TO_CIRCLE') . ' '; ?><span>' + circle_name + '</span></p>' +
                                '</div>' +
                                '</div>' +
                                '<a href="' + circle_url + '">' +
                                    '<img class="search_circle_img ' + class_m + '" src="' + circle_img + '"/>';
                                    if (ismembercircle) {
                                        html += '<img class="icon_joined" src="<?php echo JURI::root();?>media/joomdle/images/icon/SearchCourses/joinedcircle.png"/>';
                                    } 
                                html += '</a>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                            '<div class="course_detail_popup coursepopup_' + course_id + '" style="display: none;">' +
                                '<div class="header-cover" id="nav">' +
                                    '<div class="header-cover-image" style = "background: url(\'' + course_img + '\')"></div>' +
                                    '<div class="header-cover-content">' +
                                        '<div class="closetitle" onclick="closePopup(' + course_id + ');"><img src="./media/joomdle/images/icon/SearchCourses/cancel.png" /></div>' +
                                        '<div class="header-title">' +
                                            '<div class="h_title"><span class="course_name">' + course_name + '</span></div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="div_course">' +
                                    '<div class="course_detail">' +
                                        '<div class="course_info">' +
                                            '<label><?php echo JText::_('COM_JOOMDLE_COURSE_DESCRIPTION');?></label>' +
                                            '<p>' + course_des + '</p>' +
                                            '<label><?php echo JText::_('COM_JOOMDLE_LEARNING_OUTCOME');?></label>' +
                                            '<p>' + learningoutcomes + '</p>' +
                                            '<label><?php echo JText::_('COM_JOOMDLE_COURSE_TARGET_AUDIENCE');?></label>' +
                                            '<p>' + targetaudience + '</p>' +
                                            '<label><?php echo JText::_('MYCOURSE_DURATION');?></label>' +
                                            '<p>' + duration + '</p>' +
                                        '</div>' +
                                        '<div class="course_box">' +
                                            '<div class="div_box course_owner_info">' +
                                                '<label><?php echo JText::_('COM_JOOMDLE_SEARCH_COURSE_CREATOR');?></label>' +
                                                '<img class="user_img" src="' + owner_img + '"/>' +
                                                '<span>' + owner_name + '</span>' +
                                            '</div>' +
                                            '<div class="div_box circle_shared_info">' +
                                                '<label><?php echo JText::_('COM_JOOMDLE_SEARCH_POSTED_TO_CIRCLE');?></label>' +
                                                '<img class="circle_img" src="' + circle_img + '"/>' +
                                                '<div class="searchname"><span>' + circle_name + '</span></div>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '</div>';

                        jQuery('.divLoadMore').remove();
                        jQuery('.scoures_wrapper').append(html);
                        
                        jQuery('.content_c_search').each(function () { 
                            if (jQuery(this).find('p.circle_txt').height() > 22) { 
                                jQuery(this).find('p').addClass('limited');
                            }
                            if (jQuery(this).find('.course_title_search').height() > 40) { 
                                jQuery(this).find('.course_title_search').addClass('limited');
                            }
                        });
                    });
                    if (s < n)
                        cl = "";
                    else cl = "load";
                    a = a + 1;
                    jQuery('.scoures_wrapper').removeClass('loadingggg');
                    loading = false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                    loading = false;
                }
            });
        }
    </script>
    <div style="padding: 0px;" class="joomdle_searchcourse">
        <div class="joomdle_search_course">
            <?php if (!is_array($list_search_course_all) || $sl == 0) {
                echo "<div class=\"count_course_not_found\">";
                echo '<span class="search-no-course-title">' . JText::_('SORRY_NO_RESULT_ARE_FOUND') . '</span>';
                echo "</div>";
            } else {
            echo '<div class="count_course" style="text-align: center; margin-top: 10px; margin-bottom:10px; display:none;">';
            $text1 = ($sl == 1) ? JText::_('RESULT_A_COURSE') : JText::_('RESULT_MANY_COURSE');
            echo ($sl > $the_first_search_course) ? '<span class="count_course_stt">' . '<span class="count_course_s">' . $the_first_search_course . '</span>' . '/' . $sl . ' ' . $text1 . '</span>' : '<span class="count_course_stt">' . '<span class="count_course_s">' . $sl . '</span>' . '/' . $sl . ' ' . $text1 . '</span>';

            echo '</div>';
            $i = 0;

            ?>
            <div class='scoures_container'>
                <div class='scoures_wrapper'>
                    <?php
                    foreach ($list_search_course_all["courses"] as $curso) :
                        $course_id = $curso['remoteid'];
                        
                        // User enroled course
                                $url = JURI::base() . 'course/' . $course_id . '.html';
                            $course_image = $curso['courseimg'];

                            $ownerid = JUserHelper::getUserId($curso['creator']);
                            $owner = CFactory::getUser($ownerid);
                            
                            $groups = $groupModel->getShareGroups($curso['remoteid']);
                            if ($groups) {
                                foreach ($groups as $gr) {
                                    if (in_array($gr->groupid, $listgroups)) {
                                        $grid = $gr->groupid;
                                        break;
                                    }
                                }
                            }
//                            $grid = $groups[0]->groupid;
                            if (!isset($grid)) continue;
                            $table =  JTable::getInstance( 'Group' , 'CTable' );
                            $table->load($grid);
                            
                            $isMember = $groupModel->isMember($user->id, $grid);                            
                            if ($isMember > 0) {
                                $class = 'is_member';
                            } else {
                                $class = '';
                            }
                            ?>
                            <div class="scourses_courseinfo" data-cid="<?php echo $curso['remoteid']; ?>">
                                <div class="">
                                    <div class="course_content">
                                        <div class="clearfix"></div>
                                        <div class="mylearning-img">
                                            <?php if (!$curso['isLearner'] && !$curso['enroled']) { ?>
                                            <div class="left" id="images" style="background-image: url('<?php echo $course_image.'?'.(time()*1000); ?>')" onclick="poupCourse(<?php echo $curso['remoteid'];?>);"></div>
                                            <button class="btSubscribe" <?php echo ($isMember <= 0) ? 'onclick="popupJoinCircle('.$grid.');"' : 'onclick="subscribeCourse('.$curso['remoteid'].');"'; ?> data-id="<?php echo $curso['remoteid']; ?>">
                                                <?php echo JText::_('COM_COMMUNITY_BUTTON_SUBSCRIBE'); ?> 
                                            </button>
                                            <?php 
                                                    } else if ($curso['isLearner']) {
                                            ?>
                                            <a href="<?php echo $url; ?>">
                                                <div class="left" id="images" style="background-image: url('<?php echo $course_image.'?'.(time()*1000); ?>')"></div>
                                            </a>
                                            <button class="btUnsubscribe" data-id="<?php echo $curso['remoteid'] ?>" onclick="popupUnsubscribeCourse(<?php echo $curso['remoteid'];?>);">
                                                <?php echo JText::_('COM_COMMUNITY_BUTTON_UNSUBSCRIBE'); ?> 
                                            </button>
                                            <?php } else { ?>
                                            <a href="<?php echo $url; ?>">
                                                <div class="left" id="images" style="background-image: url('<?php echo $course_image.'?'.(time()*1000); ?>')"></div>
                                            </a>
                                            <?php } ?>
                                        </div>

                                        <div class="content_c_search">
                                            <div class="clearfix"></div>
                                            <div class="course_title">
                                                <?php if (!$curso['isLearner'] && !$curso['enroled']) { ?>
                                                <span class="course_title_search" onclick="poupCourse(<?php echo $curso['remoteid'];?>);"><?php echo $curso['fullname'];?></span>
                                                <?php } else { ?>
                                                <a class="course_title_search" href="<?php echo $url; ?>"><?php echo $curso['fullname'];?></a>
                                                <?php } ?>
                                            </div>

                                            <div class="coursecircle_info">
                                                <div class="cc_left">
                                                    <div class="right">
                                                        <p class="owner_txt"><?php echo JText::_('COM_JOOMDLE_SEARCH_BY') . ' '; ?><span class="owner_course"><?php echo $owner->getDisplayName();?></span></p>
                                                    </div>
                                                    <div class="circle_course">
                                                        <p class="circle_txt"><?php echo JText::_('COM_JOOMDLE_SEARCH_POSTED_TO_CIRCLE') . ' '; ?><span><?php echo $table->name; ?></span></p>
                                                    </div>
                                                </div>
                                                <a href="<?php echo CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $grid);?>">
                                                    <img class="search_circle_img <?php echo $class;?>" src="<?php echo $table->getAvatar(); ?>"/>
                                                    <?php if ($isMember > 0) { ?>
                                                    <img class="icon_joined" src="<?php echo JURI::root();?>media/joomdle/images/icon/SearchCourses/joinedcircle.png"/>
                                                    <?php } ?>
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="course_detail_popup coursepopup_<?php echo $curso['remoteid'];?>" style="display: none;">
                                <div class="header-cover" id="nav">
                                    <div class="header-cover-image" style = "background: url('<?php echo $course_image.'?'.(time()*1000); ?>')"></div>
                                    <div class="header-cover-content">
                                        <div class="closetitle" onclick="closePopup(<?php echo $curso['remoteid'];?>);"><img src="./media/joomdle/images/icon/SearchCourses/cancel.png" /></div>
                                        <div class="header-title">
                                            <div class="h_title"><span class="course_name"><?php echo $curso['fullname'];?></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="div_course">
                                    <div class="course_detail">
                                        <div class="course_info">
                                            <label><?php echo JText::_('COM_JOOMDLE_COURSE_DESCRIPTION');?></label>
                                            <p><?php echo $curso['summary'];?></p>
                                            <label><?php echo JText::_('COM_JOOMDLE_LEARNING_OUTCOME');?></label>
                                            <p><?php echo $curso['learningoutcomes'];?></p>
                                            <label><?php echo JText::_('COM_JOOMDLE_COURSE_TARGET_AUDIENCE');?></label>
                                            <p><?php echo $curso['targetaudience'];?></p>
                                            <label><?php echo JText::_('MYCOURSE_DURATION');?></label>
                                            <p><?php echo ($curso['duration']/3600) . ' hours';?></p>
                                            </div>
                                        <div class="course_box">
                                            <div class="div_box course_owner_info">
                                                <label><?php echo JText::_('COM_JOOMDLE_SEARCH_COURSE_CREATOR');?></label>
                                                <img class="user_img" src="<?php echo $owner->getAvatar();?>"/>
                                                <span><?php echo $owner->getDisplayName();?></span>
                                            </div>
                                            <div class="div_box circle_shared_info">
                                                <label><?php echo JText::_('COM_JOOMDLE_SEARCH_POSTED_TO_CIRCLE');?></label>
                                                <img class="circle_img" src="<?php echo $table->getAvatar();?>"/>
                                                <div class="searchname"><span><?php echo $table->name;?></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        $i++;
                    endforeach;
                    }
                    ?>

                </div>
            </div>
        </div>
        <div class="join_circle">
            <p class="txt_title"><?php echo JText::_('COM_JOOMDLE_SEARCH_MSG_JOIN_CIRCLE1'); ?></p>
            <p><?php echo JText::_('COM_JOOMDLE_SEARCH_MSG_JOIN_CIRCLE2'); ?></p>
            <div class="buttons">
                <button class="btCancel"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                <button class="btJoin"><?php echo JText::_('COM_JOOMDLE_SEARCH_BUTTON_JOIN'); ?></button>
            </div>
        </div>
        <div class="notification"></div>
        <div class="message">
            <p><?php echo JText::_('COM_JOOMDLE_SEARCH_MSG_SUBSCRIBE_SUCCESS');?></p>
            <div class="buttons">
                <button class="btClose"><?php echo JText::_('COM_JOOMDLE_SEARCH_BUTTON_CLOSE'); ?></button>
            </div>
        </div>
        <div class="popupunsub">
            <p><?php echo JText::_('COM_JOOMDLE_SEARCH_MSG_UNSUBSCRIBE');?></p>
            <div class="buttons">
                <button class="btCancel"><?php echo JText::_('COM_JOOMDLE_CANCEL'); ?></button>
                <button class="btUnsubs"><?php echo JText::_('COM_JOOMDLE_SEARCH_BUTTON_UNSUBSCRIBE'); ?></button>
            </div>
        </div>
    </div>
<?php } ?>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.content_c_search').each(function () { 
            if (jQuery(this).find('p.circle_txt').height() > 24) { 
                jQuery(this).find('p.circle_txt').addClass('limited');
            }
            if (jQuery(this).find('p.owner_txt').height() > 24) { 
                jQuery(this).find('p.owner_txt').addClass('limited');
            }
            if (jQuery(this).find('.course_title_search').height() > 40) { 
                jQuery(this).find('.course_title_search').addClass('limited');
            }
        });
        
        jQuery('.join_circle .btCancel').click(function () {
            jQuery('.join_circle').fadeOut().attr('data-cid', '');
            jQuery('body').removeClass('overlay2');
        });
        jQuery('.join_circle .btJoin').click(function () {
            var cid = jQuery('.join_circle').attr('data-cid');
            jQuery('.join_circle').fadeOut().attr('data-cid', '');
            jQuery('body').removeClass('overlay2');
            joinCircle(cid);
        });
        jQuery('.message .btClose').click(function () {
            jQuery('body').removeClass('overlay2');
            jQuery('.message').fadeOut();
            window.location.href = "<?php echo JURI::base() . 'courses/search.html?search='.$searchString;?>";
        });
        jQuery('.popupunsub .btCancel').click(function () {
            jQuery('.popupunsub').fadeOut().attr('data-cid', '');
            jQuery('body').removeClass('overlay2');
        });
        jQuery('.popupunsub .btUnsubs').click(function () {
            var cid = jQuery('.popupunsub').attr('data-cid');
            jQuery('.popupunsub').fadeOut().attr('data-cid', '');
            jQuery('body').removeClass('overlay2');
            unSubscribeCourse(cid);
        });
    });

    jQuery(window).scroll(function () {
        if (jQuery(window).scrollTop() + jQuery(window).height() >=
            jQuery('.joomdle_searchcourse').offset().top + jQuery('.joomdle_searchcourse').height()) {
        
            if (a <= total_pages && loading==false) {
                loading = true;
                console.log('test '+a);
                theend();
            }
        }
    });
    
    function poupCourse(courseid) {
        jQuery('body').addClass('overlay2');
//        jQuery('.scourses_courseinfo').hide();
        jQuery('.coursepopup_'+courseid).show();
        var hpopup = jQuery('.coursepopup_'+courseid).outerHeight();
        var device = '<?php echo $device;?>'; 
        if (device == 'mobile') {
            jQuery('.course_detail_popup .div_course').css('height', (hpopup - 180));
            var h1 = jQuery('.coursepopup_'+courseid+' .course_owner_info').outerHeight(true);
            var h2 = jQuery('.coursepopup_'+courseid+' .circle_shared_info').outerHeight(true);
            if (h1 > h2) {
                jQuery('.coursepopup_'+courseid+' .circle_shared_info').css('height', h1);
            } else if (h2 > h1) {
                jQuery('.coursepopup_'+courseid+' .course_owner_info').css('height', h2);
    }
        } else { 
            jQuery('.course_detail_popup .div_course').css('height', (hpopup - 78));
    }
        if (jQuery('.coursepopup_'+courseid+' span.course_name').height() > 34) {
            jQuery('.coursepopup_'+courseid+' span.course_name').addClass('limited');
    }
        if (jQuery('.coursepopup_'+courseid+' .course_owner_info span').height() > 44) {
            jQuery('.coursepopup_'+courseid+' .course_owner_info span').addClass('limited');
    }
        if (jQuery('.coursepopup_'+courseid+' .circle_shared_info span').height() > 44) {
            jQuery('.coursepopup_'+courseid+' .circle_shared_info span').addClass('limited');
        }
    }

    function closePopup(cid) {
        jQuery('.coursepopup_'+cid).hide();
        jQuery('body').removeClass('overlay2');
    }
        
    function popupJoinCircle(circleid) {
        jQuery('.join_circle').fadeIn().attr('data-cid', circleid);
        jQuery('body').addClass('overlay2');
            }
    
    function joinCircle(circleid) {
        var act = 'joincircle';
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: {
                circle_id: circleid,
                act: act
            },
            beforeSend: function () {
                jQuery('body').addClass('overlay2');
                jQuery('.notification').html('Loading...').fadeIn();
            },
            success: function (data, textStatus, jqXHR) {
                var res = JSON.parse(data);

                jQuery('body').removeClass('overlay2');
                jQuery('.notification').fadeOut();
                if (res.status == 0) {
                    alert(res.message);
                } else {
                    console.log(res.message);
                    window.location.href = "<?php echo JURI::base() . 'courses/search.html?search='.$searchString;?>";
        }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
            }
        });
    }
    
    function subscribeCourse(courseid) {
        jQuery.ajax({
            type: "POST",
            data: {courseid: courseid, act: 1},
            url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxEnrolUser&tmpl=component&format=json') ?>",
            dataType: "json",
            beforeSend: function () {
                jQuery('body').addClass('overlay2');
                jQuery('.notification').html('Loading...').fadeIn();
            },
            success: function (data) {
                jQuery('.notification').fadeOut();
                jQuery('.message').fadeIn();
            },
            error: function () {
                lgtRemovePopup();
            }
        });
    }
    
    function popupUnsubscribeCourse(courseid) {  
        jQuery('.popupunsub').fadeIn().attr('data-cid', courseid);
        jQuery('body').addClass('overlay2');
    }
    
    function unSubscribeCourse(courseid) {
        jQuery.ajax({
            type: "POST",
            data: {courseid: courseid, act: 0},
            url: "<?php echo CRoute::_('index.php?option=com_community&view=groups&task=ajaxEnrolUser&tmpl=component&format=json') ?>",
            dataType: "json",
            beforeSend: function () {
                jQuery('body').addClass('overlay2');
                jQuery('.notification').html('Loading...').fadeIn();
            },
            success: function (data) {
                jQuery('.notification').fadeOut();
                window.location.href = "<?php echo JURI::base() . 'courses/search.html?search='.$searchString;?>";
            },
            error: function () {
                lgtRemovePopup();
            }
        });
    }
</script>