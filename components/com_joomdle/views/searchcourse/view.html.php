<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

// die();
/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewSearchCourse extends JViewLegacy {
    function display($tpl = null) {
        $user = JFactory::getUser();
        $username = $user->username;
        $session = JFactory::getSession();
        $device = $session->get('device');

        $app        = JFactory::getApplication();
        $params = $app->getParams();

        require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
        $groupsModel = CFactory::getModel('groups');

        $this->assignRef('params',$params);
        if (isset($_POST['act']) && $_POST['act'] == 'load_more') {
            function custom_echo1($x, $length)
            {
                if (strlen($x) <= $length) {
                    return $x;
                } else {
                    $y = substr($x, 0, $length) . '...';
                    return $y;
                }
            }
            $array = array();
            $array['act'] = 3;
            $array['id'] = (int)$_POST['id'];
            $array['search_string'] = $_POST['scourse'];
            
            if ($device == 'mobile') {
                $limit = (int) JText::_('COM_JOOMDLE_LIMIT_MOBILE');
            } else {
                $limit = (int) JText::_('COM_JOOMDLE_LIMIT');
            }
            $limit_new = (int) JText::_('COM_JOOMDLE_LIMIT_MORE');            
            if ($array['id'] > 1) { // load second page: 8 circles
                $position = (($array['id'] - 1) * $limit_new);
            } else { // load frist page: 4 circles
                $position = (($array['id'] - 1) *  $limit);   
            }
            
            $listCoursesSearch = $groupsModel->getCoursesForSearch($user->id);
            $courses = array();
            $listgroups = array();
            if ($listCoursesSearch) {
                foreach ($listCoursesSearch as $course) {
                    $courses[] = $course->courseid;
                    if (!in_array($course->groupid, $listgroups)) {
                        $listgroups[] = $course->groupid;
                    }
                }
            }
            if (count($courses) > 0) {
                $listcourses = implode(',', $courses);
            } else {
                $listcourses = '';
            }
            
            if ($array['id'] > 1){
                // searched first 2 course ( search more 1 course ajax)
                $search_number_course = $limit_new;
                $list_search_course = JoomdleHelperContent::call_method('search_courses', $array['search_string'], 'created', $listcourses, $username, $limit_new,  (int)$position);
            } else {
                // searched first 4 course ( search more 5 course ajax)
                $search_number_course = $limit;
                $list_search_course = JoomdleHelperContent::call_method('search_courses', $array['search_string'], 'created', $listcourses, $username, $limit, (int)$position);
            }

            foreach($list_search_course["courses"] as $key => $curso ){
                if ($device == 'mobile') {
                    $a = custom_echo1($curso['fullname'], 35);
                }else{
                    $a = custom_echo1($curso['fullname'], 27);
                }
                $course_id = $curso['remoteid'];
                $ownerid = JUserHelper::getUserId($curso['creator']);
                $owner = CFactory::getUser($ownerid);
                $groups = $groupsModel->getShareGroups($curso['remoteid']);
                if ($groups) {
                    foreach ($groups as $gr) {
                        if (in_array($gr->groupid, $listgroups)) {
                            $grid = $gr->groupid;
                            break;
                        }
                    }
                }
                if (!isset($grid)) continue;
//                $grid = $groups[0]->groupid;
                $table =  JTable::getInstance( 'Group' , 'CTable' );
                $table->load($grid);

                $isMember = $groupsModel->isMember($user->id, $grid); 
                $list_search_course['courses'][$key]['remoteid']= $course_id;
                $list_search_course['courses'][$key]['fullname']= $curso['fullname'];
                $list_search_course['courses'][$key]['isLearner']= $curso['isLearner'];
                $list_search_course['courses'][$key]['enroled']= $curso['enroled'] ? true : false;
                $list_search_course['courses'][$key]['course_img']= $curso['courseimg'].'?'.(time()*1000);
                $list_search_course['courses'][$key]['course_url']= JURI::base() . 'course/' . $course_id . '.html';
                $list_search_course['courses'][$key]['course_des']= $curso['summary'];
                $list_search_course['courses'][$key]['learningoutcomes']= $curso['learningoutcomes'];
                $list_search_course['courses'][$key]['targetaudience']= $curso['targetaudience'];
                $list_search_course['courses'][$key]['duration']= ($curso['duration']/3600) . ' hours';
                $list_search_course['courses'][$key]['owner_img']= $owner->getAvatar();
                $list_search_course['courses'][$key]['owner_name']= $owner->getDisplayName();
                $list_search_course['courses'][$key]['circleid']= $grid;
                $list_search_course['courses'][$key]['circle_name']= $table->name;
                $list_search_course['courses'][$key]['circle_url']= CRoute::_('index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $grid);
                $list_search_course['courses'][$key]['circle_img']= $table->getAvatar();
                $list_search_course['courses'][$key]['ismembercircle']= ($isMember > 0) ? true : false;
                        $list_search_course['courses'][$key]['stt']= $search_number_course*($array['id'] - 1) + $key;
                    }
            $result = array();
            $result['courses'] = $list_search_course['courses'];
            $result['status'] = $list_search_course['status'];
            echo json_encode($result);
            die;
        }
        if (isset($_POST['act']) && $_POST['act'] == 'joincircle') {
            if ($_POST['circle_id'] > 0) {
                $join = $groupsModel->addUserToCircle($user->id, $_POST['circle_id']);
                if ($join) {
                    $status = true;
                    $message = 'success';
                } else {
                    $status = false;
                    $message = 'false';
                }
            } else {
                $status = false;
                $message = 'false';
            }
            
            $result = array();
            $result['message'] = $message;
            $result['status'] = $status;
            echo json_encode($result);
            die;
        }
        
        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
//        $this->_prepareDocument();
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('SEARCH_COURSE_RESULT_TITLE'));
        parent::display($tpl);
    }
}

?>