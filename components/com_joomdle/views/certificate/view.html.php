<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewCertificate extends JViewLegacy
{
    function display($tpl = null)
    {
        global $mainframe;
        $user = JFactory::getUser();
        $username = $user->username;
        $this->username = $username;
        $courseid = JRequest::getVar('course_id', null, 'NEWURLFORM');
        $this->courseid = (int)$courseid;
        $config = new JConfig();
        $filename = $config->dataroot . '/certificatetemp/pix/borders/' . $this->courseid . '/';

        // Check course publish( If Course not published return message)
         $course_data = JoomdleHelperContent::call_method('get_course_page_data', (int)$courseid, $username);
        $this->course_info = json_decode($course_data['course_info'], true);
        $this->allmods = json_decode($course_data['mods'], true);
        $userRoles = json_decode($course_data['userRoles'], true);
        $this->hasPermission = $userRoles['roles'];
        if($this->course_info['course_status'] == ""){
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_COURSE_DONT_PUBLISH_LEANER_CAN_NOT_SEE_CERTIFICATE');
//            return;
        }

        // Check role of user(access role leaner view certificate)
        $role =  json_decode($this->hasPermission[0]['role'],true);
        $isLearner = false;
                // roleid = 5 (student)
                // roleid = 4 (teacher)
                // roleid = 3 (content creator)

        if ($role[0]['roleid'] == 5) {
                    $isLearner = true;
                }
        if($isLearner == false){
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_USER_IS_NOT_LEANER_AND_CAN_NOT_SEE_CERTIFICATE');
//            return;
        }

        // Check exist certificate course( if course dont have certificate return message)
        if($this->course_info['certificatecourse'] != 1) {
            header('Location: /404.html');
            exit;
//            echo JText::_('COM_JOOMDLE_COURSE_DONT_HAVE_CERTIFICATE');
//            return;
        }

        /* Check exist forder borders template certificate */
        if (!file_exists($filename)) {
            if (!is_dir($config->dataroot . '/certificatetemp')) {
                mkdir($config->dataroot . '/certificatetemp', 0777);
            }
            if (!is_dir($config->dataroot . '/certificatetemp/pix')) {
                mkdir($config->dataroot . '/certificatetemp/pix', 0777);
            }
            if (!is_dir($config->dataroot . '/certificatetemp/pix/borders')) {
                mkdir($config->dataroot . '/certificatetemp/pix/borders', 0777);
            }
            if (!is_dir($config->dataroot . '/certificatetemp/pix/borders/' . $this->courseid)) {
                mkdir($config->dataroot . '/certificatetemp/pix/borders/' . $this->courseid, 0777);
                copy($config->dirmoodle . '/mod/certificate/pix/borders/parenthesis_certificate_default.png', $config->dataroot . '/certificatetemp/pix/borders/' . $this->courseid . '/parenthesis_certificate_default.png');
            }
            $defalt_cer_img = 'parenthesis_certificate_default.png';
            $this->template_default = JoomdleHelperContent::call_method('get_certificate_template_default', $this->courseid);
            $tmp_default_id = $this->template_default['template']['cer_id'];
            $setDefault = JoomdleHelperContent::call_method('set_default_certificate_template', (int)$tmp_default_id, (int)$this->courseid, $defalt_cer_img, $this->username);
        } else {
            $this->template_default = JoomdleHelperContent::call_method('get_certificate_template_default', $this->courseid);
        }
        parent::display($tpl);
    }
}

?>
