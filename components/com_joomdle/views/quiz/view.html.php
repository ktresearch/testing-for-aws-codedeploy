<?php
/**
 * Joomdle
 *
 * @author Antonio Durán Terrés
 * @package Joomdle
 * @license GNU/GPL
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Joomdle component
 */
class JoomdleViewQuiz extends JViewLegacy {
    function display($tpl = null) {
        $app = JFactory::getApplication();
        $document = JFactory::getDocument();
        $pathway = $app->getPathWay();

        $menus = $app->getMenu();
        $menu = $menus->getActive();

        $params = $app->getParams();
        $this->assignRef('params', $params);

        $user = JFactory::getUser();
        $username = $user->username;

        $course_id =  JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
        if (!$course_id)
            $course_id =  JRequest::getVar( 'course_id' );
        if (!$course_id)
            $course_id = $params->get( 'course_id' );

        $course_id = (int) $course_id;
        $this->courseid = $course_id;

        if ($params->get('use_new_performance_method'))
            $course_data = JHelperLGT::getCoursePageData(['id' => $course_id, 'username' => $username]);
        else
            $course_data = JoomdleHelperContent::call_method('get_course_page_data', (int)$course_id, $username);

        $this->course_info = json_decode($course_data['course_info'], true);
        $this->mods = json_decode($course_data['mods'], true);
        $userRoles = json_decode($course_data['userRoles'], true);
        $this->hasPermission = $userRoles['roles'];
        $this->is_enroled = $this->course_info['enroled'];

        if ($this->is_enroled == 0 && !$this->hasPermission[0]['hasPermission']) {
            header('Location: /404.html');
            exit;
        }

        $module_id =  JRequest::getVar( 'id', null, 'NEWURLFORM' );
        if (!$module_id)
            $module_id =  JRequest::getVar( 'id' );
        if (!$module_id)
            $module_id = $params->get( 'id' );
        $module_id = (int) $module_id;

        if (!$course_id) {
            echo JText::_('COM_JOOMDLE_NO_COURSE_SELECTED');
            return;
        }

        $this->username = $username;
        if (is_array ($this->mods)) {
            foreach ($this->mods as $mod) {
                $activities = $mod['mods'];
                if (is_array($activities)) {
                    foreach ($activities as $act) {
                        if($act['id'] == $module_id) {
                        $this->modname = $mod['name'];
                        $this->namequiz = $act['name'];
                        }
                    }
                }
            }
        }
        $status =  JRequest::getVar( 'status', null, 'NEWURLFORM' );
        if (!$status)
            $status =  JRequest::getVar( 'status' );
        if (!$status)
            $status = $params->get( 'status' );

        $this->pageclass_sfx = '';
        if ($status) {
            switch ($status) {
                case 'start':
                    $document->setTitle($this->quiz['name']);
                    if ($this->quiz['atype'] == 3) parent::display('inquiz');
                    else parent::display('inquiz2');
                    break;
                case 'end':
                    $attemptid =  JRequest::getVar( 'attempt', null, 'NEWURLFORM' );
                    $attemptData = JoomdleHelperContent::call_method ( 'get_quiz_attempt_info', (int)$attemptid, $username);
                    $this->attempt = json_decode($attemptData, true);
                    $document->setTitle('End Of Quiz');
                    parent::display('end');
                    break;
                case 'endquiz':
                    $attemptid =  JRequest::getVar( 'attempt', null, 'NEWURLFORM' );
                    $attemptData = JoomdleHelperContent::call_method ( 'get_quiz_attempt_info', (int)$attemptid, $username);
                    $this->attempt = json_decode($attemptData, true);
                    $document->setTitle('End Of Quiz');
                    parent::display('reviewquiz');
                    break;
                case 'review':
                    $document->setTitle('End Of Test');
                    parent::display('review');
                    break;
                default:
                    break;
            }
        } else {
        	// if ($params->get('use_new_performance_method'))
         //        $this->quizes = JHelperLGT::getCourseQuizes($course_id, $username);
         //    else
                $this->quizes = JoomdleHelperContent::call_method ( 'get_course_quizes', $course_id, $username);

	        foreach ($this->quizes as $key => $value) {
	            foreach ($value['quizes'] as $k => $v) {
	                if ($v['id'] != $module_id) continue; else $this->quiz = $v;
	            }
	        }

	        $this->quiz['course_id'] = $course_id;
	        $this->quiz['questions'] = $this->arr_questions;

	        $coursemods = $this->mods;
	        $mod_arr = array();

	        if (is_array ($coursemods)) {
	            foreach ($coursemods as $mod) {
	                $activities = $mod['mods'];
	                if (is_array($activities)) {
	                    foreach ($activities as $act) {
	                        if($act['mod'] != 'forum' && $act['mod'] != '' && $act['mod'] != 'certificate' && $act['mod'] != 'questionnaire') {
	                            $mod_arr[] = $act;
	                        }
	                    }
	                }
	            }
	        }

	        $this->list_modules = $mod_arr;

            $document->setTitle($this->quiz['name']);
            parent::display($tpl);
        }
    }
}
?>

