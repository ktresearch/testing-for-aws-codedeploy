<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="<?php echo JURI::base().'components/com_joomdle/views/quiz/css/quiz.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo JURI::base().'components/com_joomdle/views/quiz/css/jquery-confirm.css';?>">
	<script type="text/javascript" src="<?php echo JURI::base().'components/com_joomdle/views/quiz/js/jquery-confirm.js';?>"></script>
<?php 
	defined('_JEXEC') or die('Restricted access'); 
	require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
	$itemid = JoomdleHelperContent::getMenuItem();
	$id_course = $this->quiz['course_id']; // id cua module quiz
	$id_module = $this->quiz['id']; // id cua module quiz
	$dance = JoomdleHelperContent::call_method ( 'get_course_quizes',$id_course,$username);
	$count_dance = count($dance);
	for ($i=0; $i < $count_dance; $i++) { 
		$count_quizes_dance = count($dance[$i]['quizes']);
		for ($j=0; $j < $count_quizes_dance; $j++) { 
			if($dance[$i]['quizes'][$j]['id']==$id_module){
				$this->quiz['attempt'] = $dance[$i]['quizes'][$j]['attempt'];
				$this->quiz['time']['state'] = $dance[$i]['quizes'][$j]['time']['state']; // trang thai cua module quiz
				$timestart_quiz = $dance[$i]['quizes'][$j]['time']['timestart'];
				$timelimit_quiz = $dance[$i]['quizes'][$j]['timelimit'];
				$timeopen = $dance[$i]['quizes'][$j]['timeopen'];
				$timeclose = $dance[$i]['quizes'][$j]['timeclose'];
			}
		}
	}
	$name_quiz = $this->quiz['name']; // name cua module quiz
	$intro_quiz = $this->quiz['intro']; // loi gioi thieu cua module quiz
	$attempt_quiz = $this->quiz['attempt']; // attempt cua module quiz
	$questions = $this->quiz['questions']; // danh sach cac cau hoi trong quiz
	$status_quiz = $this->quiz['time']['state'];
	$timeleft_quiz = round(($timestart_quiz+$timelimit_quiz)-date_timestamp_get(date_create()));
	$timeleft_quiz = $timeleft_quiz/60;
	$lenght_questions = count($questions);
	// ----------------------List activity in course----------------
	$list_modules = $this->list_modules;
	$count_list_modules = count($list_modules);
	$this->wrapper->url = $params->get( 'MOODLE_URL' ).'/mod/quiz/summary.php?attempt='.$attempt_quiz;
	$actual_link = JURI::base().'index.php/?option=com_joomdle&view=quiz&module_id='.$id_module.'&course_id='.$id_course.'&status=end';
	$prev = $lenght_questions - 1;
	$url_question_prev = JURI::base().'index.php/?option=com_joomdle&view=quiz&module_id='.$id_module.'&course_id='.$id_course.'&status=start&qtype='.$questions[$prev]['qtype'].'&qid='.$questions[$prev]['id'].'&page='.$prev;
?>
<div class="joomdle-course <?php echo $this->pageclass_sfx;?>">
	<div class="container">
		<span class="pull-right time-left"></span>
		<div class="col-xs-12 col-sm-8 col-sm-offset-4 col-md-4 col-md-offset-8 ">
		</div>
		<center class="title-quiz">End of Test</center>
		You have completed all the questions in this test. You may wish to review your responses to the questions  or select 'End' to exit this test.
		<div class="col-xs-12 introduction" style="margin-top:0px !important;">
			<span>Review Questions</span>
			<div style="clear:both !important;">
				<?php for ($i = 0; $i < $lenght_questions; $i++ ) { 
					if($i==0){
						$link = JURI::base().'index.php/?option=com_joomdle&view=quiz&module_id='.$id_module.'&course_id='.$id_course.'&status=start&qtype='.$questions[$i]['qtype'].'&qid='.$questions[$i]['id'];
					}else{
						$link = JURI::base().'index.php/?option=com_joomdle&view=quiz&module_id='.$id_module.'&course_id='.$id_course.'&status=start&qtype='.$questions[$i]['qtype'].'&qid='.$questions[$i]['id'].'&page='.$i;
					}
				?>
					<div class="col-md-2 col-sm-2 col-xs-4"> <a href="<?php echo $link; ?>">Question <?php echo $i+1; ?></a></div>
				<?php } ?>
			</div>
		</div>



		<div class="next-question-before">
			<img width="50" height="50" src="<?php echo JURI::base().'components/com_joomdle/views/quiz/tmpl/prev.png';?>">
		</div>



		<div class="end-quiz">
			<center>
				<span class="col-xs-12">Click "End Test" if you want to submit your answers.</span>
				<div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 btn-start-quiz" style="margin-top:15px;margin-bottom:15px;">
					<center>
						End Test
					</center>
				</div>
				<div class="all-point">
					<?php for ( $i=0; $i<$lenght_questions; $i++){ ?>
						<span class=" point">ds</span>
					<?php } ?>
					<span class="pointactive">ds</span>
				</div>
			</center>
		</div>
	</div>
</div>







<div class="aaaaa">
 	<iframe 
    	id="iframe-quiz" 
        class="autoHeight"
        src="<?php echo $this->wrapper->url; ?>"
        width="<?php echo $this->params->get('width', '100%'); ?>"
        scrolling="<?php //echo $this->params->get('scrolling', 'auto'); ?>yes"
        allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"
        
		<?php if (!$this->params->get('autoheight', 1)) { ?>
            height="<?php echo $this->params->get('height', '500'); ?>"
        <?php
        }
        ?>
        align="top" 
        frameborder="0"
		<?php if ($this->params->get('autoheight', 1)) { ?>
        <?php
        }
        ?>
    ></iframe>
</div>
<script type="text/javascript">
	jQuery('.btn-start-quiz').click(function(){
		jQuery('#iframe-quiz').contents().find(".singlebutton form input[value='Submit all and finish']").last().click();
		jQuery.confirm({
            content: 'Are you want to finish?',
            confirmButton: 'Yes',
            cancelButton: 'Cancel',
            confirmButtonClass: 'yes',
            cancelButtonClass: 'cancel',
            title: false,
            columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
            theme: 'blue',
            confirm: function(){
               	jQuery('#iframe-quiz').contents().find(".ft .button-group button").last().click();
				window.location.href = '<?php echo $actual_link; ?>';
            },
            cancel: function(){
	        	jQuery('#iframe-quiz').contents().find(".ft .button-group .default").last().click();
            }
        });
	});
	jQuery('.next-question-before img').click(function(){
		window.location.href = '<?php echo $url_question_prev; ?>';
	});
	//8888888888888888888888888888888888888888888888888888888888888
	//8888888888888888888888888888888888888888888888888888888888888
	//8888888888888888888888888888888888888888888888888888888888888
	//8888888888888888888888888888888888888888888888888888888888888
	<?php if($timeleft_quiz>0){ ?>
		var seconds = <?php echo $timeleft_quiz*60; ?>;
		function timer() {
		    var days        = Math.floor(seconds/24/60/60);
		    var hoursLeft   = Math.floor((seconds) - (days*86400));
		    var hours       = Math.floor(hoursLeft/3600);
		    var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
		    var minutes     = Math.floor(minutesLeft/60);
		    var remainingSeconds = seconds % 60;
		    if (remainingSeconds < 10) {
		        remainingSeconds = "0" + remainingSeconds; 
		    }
		    if(minutes < 10){
		    	minutes = "0" + minutes;
		    }
		    if(hours < 10){
		    	hours = "0" + hours;
		    }
		    jQuery('.time-left').html("Time left " + hours + ":" + minutes + ":" + remainingSeconds + " seconds.");
		    if (seconds <= -1) {
		        jQuery('.time-left').html("Time's up");
		    	alert("Time's up.Your results have not been recorded");
		    	jQuery('.btn-start-quiz').attr('disabled',true);
		        clearInterval(countdownTimer);
				window.location.href = '<?php echo JURI::base().'index.php/?option=com_joomdle&view=quiz&module_id='.$id_module.'&course_id='.$id_course; ?>';
		    } else {
		        seconds--;
		    }
		}
		var countdownTimer = setInterval('timer()', 1000);
	<?php } ?>
	
	jQuery(document).ready(function(){
		<?php if($device == 'mobile') { ?>
	        var old_src = jQuery('.navbar-nav li[data-id="256"] a img').attr('src');
	        var new_src = old_src.replace('_555555', '');
	        jQuery('.navbar-nav li[data-id="256"] a img').attr('src',new_src);
	        jQuery('.navbar-nav li[data-id="256"]').addClass('current active');
	    <?php } else { ?>
	        jQuery('.navbar-nav').find('li[data-id="271"]').addClass('current active');
	    <?php  } ?>
});
</script>