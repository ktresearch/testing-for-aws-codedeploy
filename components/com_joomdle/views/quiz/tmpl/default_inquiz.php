<?php
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_SITE.'/components/com_joomdle/views/header.php');
$itemid = JoomdleHelperContent::getMenuItem();
$id_course = $this->quiz['course_id']; // id cua module quiz
$id_module = $this->quiz['id']; // id cua module quiz
$dance = $this->quizes;

$count_dance = count($dance);
for ($i=0; $i < $count_dance; $i++) {
    $count_quizes_dance = count($dance[$i]['quizes']);
    for ($j=0; $j < $count_quizes_dance; $j++) {
        if($dance[$i]['quizes'][$j]['id']==$id_module){
            $this->quiz['attempt'] = $dance[$i]['quizes'][$j]['attempt'];
            $this->quiz['time']['state'] = $dance[$i]['quizes'][$j]['time']['state']; // trang thai cua module quiz
            $timestart_quiz = $dance[$i]['quizes'][$j]['time']['timestart'];
            $timelimit_quiz = $dance[$i]['quizes'][$j]['timelimit'];
            $timeopen = $dance[$i]['quizes'][$j]['timeopen'];
            $timeclose = $dance[$i]['quizes'][$j]['timeclose'];
        }
    }
}

$name_quiz = $this->quiz['name']; // name cua module quiz
$intro_quiz = $this->quiz['intro']; // loi gioi thieu cua module quiz
$attempt_quiz = $this->quiz['attempt']; // attempt cua module quiz

$questions = $this->quiz['questions']; // danh sach cac cau hoi trong quiz
$status_quiz = $this->quiz['time']['state'];
$timeleft_quiz = round(($timestart_quiz+$timelimit_quiz)-date_timestamp_get(date_create()));
$timeleft_quiz = $timeleft_quiz/60;
$lenght_questions = count($questions);
// ----------------------List activity in course----------------
$list_modules = $this->list_modules;
$count_list_modules = count($list_modules);
$this->wrapper->url = $params->get( 'MOODLE_URL' ).'/mod/quiz/attempt.php?attempt='.$attempt_quiz;
$this->wrapper2->url = $params->get( 'MOODLE_URL' ).'/mod/quiz/summary.php?attempt='.$attempt_quiz;
// $actual_link = JURI::base().'index.php/?option=com_joomdle&view=quiz&module_id='.$id_module.'&course_id='.$id_course.'&status=end';
$actual_link = JURI::base().'quiz/'.$id_course.'_'.$id_module.'_end.html';
?>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="<?php echo JURI::base().'components/com_joomdle/views/quiz/css/quiz.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo JURI::base().'components/com_joomdle/views/quiz/css/jquery-confirm.css';?>">
<script type="text/javascript" src="<?php echo JURI::base().'components/com_joomdle/views/quiz/js/jquery-confirm.js';?>"></script>
<div class="joomdle-view-quiz <?php echo $this->pageclass_sfx;?>" style="clear:both !important;">
    <div class="">
        <span class="pull-left time-left"></span>
        <div class="col-xs-12 col-sm-8 col-sm-offset-4 col-md-4 col-md-offset-8 ">
        </div>
        <center class="title-quiz"><?php echo $name_quiz; ?></center>

        <?php for ( $i=0; $i<$lenght_questions; $i++){ ?>
            <!-- ********************************match******************************* -->
            <!-- ********************************match******************************* -->
            <?php
            if($questions[$i]['qtype'] == 'match'){
                $c_question = count($questions[$i]['answers']);
                $arr_answers = array();
                $arr_statements = array();
                for( $j=0; $j<$c_question; $j++){
                    array_push($arr_answers, $questions[$i]['answers'][$j]['answertext']);
                    shuffle($arr_answers);
                    array_push($arr_statements, $questions[$i]['answers'][$j]['questiontext']);
                }
                ?>
                <div class="viewQues question<?php echo $i+1; ?>">
                    <div class="tip-ques"><?php echo JText::_('COM_JOOMDLE_QUIZ_TIP');?></div>
                    <div class="number-ques"><?php echo JText::_('COM_JOOMDLE_QUESTION').' '. ($i+1); ?></div>
                    <div class="answers q<?php echo $i+1; ?>" data="q<?php echo $i+1; ?>">
                        <input type="hidden" class="count_answer" value="<?php echo $c_question; ?>">
                        <?php for($y=0; $y<count($arr_statements); $y++){ ?>
                            <div style="clear:both;">
                                <span class="match-statement"><?php echo JText::_('COM_JOOMDLE_STATEMENT');?> <?php echo $y+1; ?>:</span><br>
                                <b><?php echo $arr_statements[$y]; ?></b><br>
                                <span class="col-md-1 col-sm-1 col-xs-3"><?php echo JText::_('COM_JOOMDLE_ANSWER'); ?></span>
                                <select class="col-md-2 col-sm-3 col-xs-7 match_answer">
                                    <option><?php echo JText::_('COM_JOOMDLE_OPTION'); ?></option>
                                    <?php for($t=0;$t<count($arr_answers);$t++) {?>
                                        <option><?php echo $arr_answers[$t]; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>

            <?php
            if($questions[$i]['qtype'] == 'shortanswer'){
                ?>
                <div class="viewQues question<?php echo $i+1; ?>">
                    <div class="number-ques"><?php echo JText::_('COM_JOOMDLE_QUESTION').' '.($i+1); ?></div>
                    <div class="text-ques"><?php echo $questions[$i]['questiontext']; ?></div>
                    <div class="answers q<?php echo $i+1; ?>" data="q<?php echo $i+1; ?>">
                        <span><?php echo JText::_('COM_JOOMDLE_ANSWER'); ?></span>
                        <textarea class="col-xs-12 shortanswer" placeholder="<?php echo JText::_('COM_JOOMDLE_TYPE_YOUR_ANSWER_HERE');?>" style="background:#EFECEC !important;"></textarea>
                    </div>
                </div>
            <?php } ?>

            <?php
            if($questions[$i]['qtype'] == 'truefalse'){
                ?>
                <div class="viewQues question<?php echo $i+1; ?>">
                    <div class="tip-ques"><?php echo JText::_('COM_JOOMDLE_QUIZ_TF_TIP'); ?></div>
                    <div class="number-ques"><?php echo JText::_('COM_JOOMDLE_QUESTION').' '.($i+1); ?></div>
                    <div class="text-ques"><?php echo $questions[$i]['questiontext']; ?></div>
                    <div class="answers q<?php echo $i+1; ?>" data="q<?php echo $i+1; ?>">
                        <span><?php echo JText::_('COM_JOOMDLE_ANSWER'); ?></span>
                        <div class="inputPurpose">
                            <div p="1" class="status answer_true"></div> <label class="spanstatus answer_true"><?php echo JText::_('COM_JOOMDLE_TRUE'); ?></label>
                        </div>
                        <div class="inputPurpose">
                            <div p="2" class="status answer_false"></div> <label class="spanstatus answer_false"><?php echo JText::_('COM_JOOMDLE_FALSE'); ?></label>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php
            if($questions[$i]['qtype'] == 'multichoice') {
                $singleOrMulti = $options['single'];
                $c_question = count($questions[$i]['answers']);
                ?>
                <div class="viewQues question<?php echo $i+1; ?> <?php echo ($i==0)? 'current':'';?> <?php echo $questions[$i]['qtype'].' '; echo ($singleOrMulti) ? 'single' : 'multi'; ?>"  >
                    <div class="tip-ques"><?php echo JText::_('COM_JOOMDLE_QUIZ_TEXT_TIP'); ?></div>
                    <div class="number-ques"><?php echo JText::_('COM_JOOMDLE_QUESTION').' '.($i+1); ?></div>
                    <div class="text-ques"><?php echo $questions[$i]['questiontext']; ?></div>
                    <div class="answers q<?php echo $i+1; ?>" data="q<?php echo $i+1; ?>">
                        <span><?php echo JText::_('COM_JOOMDLE_ANSWER'); ?></span>
                        <input type="hidden" value="<?php echo $c_question; ?>" class="count_answer">
                        <?php for($y=0; $y<$c_question; $y++){ ?>
                            <div class="inputPurpose answer_multichoice">
                                <div class="status"></div> <label class="spanstatus"><?php echo $questions[$i]['answers'][$y]['answer']; ?></label>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>

            <div class="next-question-before changePosition">
                <img width="50" height="50" src="<?php echo JURI::base().'images/prev.png';?>">
            </div>
            <div class="next-question-after changePosition">
                <img width="50" height="50" src="<?php echo JURI::base().'images/next.png';?>">
            </div>

        <?php } ?>

        <div class="review_quiz">
            <div class="container">
                <?php echo JText::_('COM_JOOMDLE_QUIZ_TIP2'); ?>
                <div class="col-xs-12 introduction" style="margin-top:0px !important;">
                    <span><?php echo JText::_('COM_JOOMDLE_REVIEW_QUESTIONS'); ?></span>
                    <div style="clear:both !important;">
                        <?php for ($i = 0; $i < $lenght_questions; $i++ ) {
                            ?>
                            <div class="col-md-2 col-sm-2 col-xs-4"> <a class="ques_review" data="<?php echo $i+1; ?>"><?php echo JText::_('COM_JOOMDLE_QUESTION').' '.($i+1); ?></a></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="next-question-before changePosition">
                    <img width="50" height="50" src="<?php echo JURI::base().'images/prev.png';?>">
                </div>

                <div class="end-quiz">
                    <center>
                        <span class="col-xs-12"><?php echo JText::_('COM_JOOMDLE_QUIZ_TIP3'); ?></span>
                        <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 btn-start-quiz" style="margin-top:15px;margin-bottom:15px;">
                            <center>
                                <?php echo JText::_('COM_JOOMDLE_END_QUIZ');?>
                                <img src="<?php echo JURI::base().'components/com_joomdle/views/quiz/tmpl/loadques.gif';?>" class="pull-right loadques" style="width:20px;height:20px;margin-right:20px;display:none;">
                            </center>
                        </div>
                    </center>
                </div>
            </div>
        </div>
        <center>
            <div class="buttons" <?php if($timelimit_quiz!=0){ echo "style='display:none !important;'";} ?> >
                <button class="btnQuiz btn-submit"><?php echo JText::_('COM_JOOMDLE_SUBMIT');?> <img src="<?php echo JURI::base().'components/com_joomdle/views/quiz/tmpl/loadques.gif';?>" class="pull-right loadques" style="width:12px;height:12px; margin-left: 5px;margin-top:9px;display:none;"></button>
            </div>
            <div class="all-point changePosition" <?php if($timelimit_quiz!=0){ echo "style='padding-top:35px !important;'"; } ?>>
                <?php $m=0; for ( $i=0; $i<$lenght_questions; $i++){ $m++; ?>
                    <span class="point point<?php echo $i+1; ?>">ds</span>
                <?php } ?>
                <span class="point yyy point<?php echo $m+1; ?>">ds</span>
            </div>
        </center>
    </div>
</div>

<div class="aaaaa" style="clear:both !important;">
    <iframe
        id="iframe-quiz"
        class="autoHeight"
        src="<?php echo $this->wrapper->url; ?>"
        width="<?php echo $this->params->get('width', '100%'); ?>"
        scrolling="<?php //echo $this->params->get('scrolling', 'auto'); ?>yes"
        allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"

        <?php if (!$this->params->get('autoheight', 1)) { ?>
            height="<?php echo $this->params->get('height', '500'); ?>"
        <?php
        }
        ?>
        align="top"
        frameborder="0"
        <?php if ($this->params->get('autoheight', 1)) { ?>
        <?php
        }
        ?>
        ></iframe>
</div>

<div class="aaaaa" style="clear:both !important;">
    <iframe
        id="iframe-toan"
        class="autoHeight"
        src="<?php echo $this->wrapper2->url; ?>"
        width="<?php echo $this->params->get('width', '100%'); ?>"
        scrolling="<?php //echo $this->params->get('scrolling', 'auto'); ?>yes"
        allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"

        <?php if (!$this->params->get('autoheight', 1)) { ?>
            height="<?php echo $this->params->get('height', '500'); ?>"
        <?php
        }
        ?>
        align="top"
        frameborder="0"
        <?php if ($this->params->get('autoheight', 1)) { ?>
        <?php
        }
        ?>
        ></iframe>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(e){

        var Ques;

        function myFunctionQues() {
            Ques = setInterval(function(){
                window.location.href = '<?php echo $actual_link; ?>';
            }, 1500);
        }

        function myStopFunctionQues() {
            clearTimeout(Ques);
        }

        var stt_ques = 1;
        var next = stt_ques + 1;
        var prev = stt_ques - 1;
        var numberOfQuestions = <?php echo $lenght_questions; ?>;
        jQuery('.next-question-before').hide();
        if(stt_ques == numberOfQuestions) {
                     jQuery('.next-question-before').hide();
          }
        if (prev == 0) {
                jQuery('.next-question-before').hide();
            }

        function checkSrc() {
            setInterval(function() {
            var src = jQuery('#iframe-quiz').attr('src');
            if (src.indexOf('page=') != -1) {
                var arr = src.split('page=');
                    var page = parseInt(arr[1]) + 1;
                    var newSrc = arr[0]+'page='+(stt_ques-1);
            } else {
                    var page = (stt_ques-1);
                    var newSrc = src+'&page='+(stt_ques-1);
            }
                if (page != stt_ques) {
            jQuery('#iframe-quiz').attr('src', newSrc);
                }
            }, 1000);
        }

        jQuery('#iframe-quiz').on('load', function(){
            jQuery('.joomdle-view-quiz').removeClass('onLoad');
            jQuery('.btn-submit').attr('data-disabled', '');
        });

        jQuery('.next-question-before img').click(function(){
            if (prev == 0) {
                myStopFunction();
                jQuery('.next-question-before').hide();
            } else {
                myStopFunction();
                jQuery('.viewQues').hide();
                jQuery('.question'+prev).show();
                stt_ques--;
                next = stt_ques + 1;
                prev = stt_ques - 1;
                <?php if($timelimit_quiz == 0){ ?>
                if(prev == 0){
                    jQuery('.next-question-before').hide();
                }
                if(stt_ques < numberOfQuestions){
                    jQuery('.next-question-after').show();
                }

                <?php }elseif($timelimit_quiz != 0){ ?>
                if(stt_ques <= numberOfQuestions){
                    jQuery('.joomdle-view-quiz').addClass('onLoad');
                    jQuery('.btn-submit').attr('data-disabled', '1');
                    jQuery('.next-question-after').show();
                    jQuery('.review_quiz').hide();
                    jQuery('.title-quiz').html('<?php echo $name_quiz; ?>');
                }

                <?php } ?>
                jQuery('.all-point span').removeClass('pointactive');
                jQuery('.point'+stt_ques).addClass('pointactive');
            }
            checkSrc();
        });
        ///     before
        jQuery('.next-question-after img').click(function(){
            stt_ques++;
            jQuery('#iframe-quiz').contents().find(".submitbtns input[value='Next']").click();
             jQuery('.next-question-before').show();
            if (stt_ques <= numberOfQuestions) {
                jQuery('.joomdle-view-quiz').addClass('onLoad');
                jQuery('.btn-submit').attr('data-disabled', '1');
            }
            myStopFunction();
            jQuery('.viewQues').hide();
            jQuery('.question'+next).show();

            next = stt_ques + 1;
            prev = stt_ques - 1;
            <?php if ($timelimit_quiz == 0) { ?>
            if (stt_ques == numberOfQuestions) {
                jQuery('.next-question-after').hide();
            }
            <?php } elseif ($timelimit_quiz != 0) { ?>
            if(stt_ques > numberOfQuestions){
                jQuery('.next-question-after').hide();
                jQuery('.title-quiz').html('<?php echo JText::_('COM_JOOMDLE_END_OF_QUIZ'); ?>');
                jQuery('.review_quiz').show();
            }
            <?php } ?>
            jQuery('.all-point span').removeClass('pointactive');
            jQuery('.point'+stt_ques).addClass('pointactive');
            // setTimeout(function() {
            //     checkSrc();
            // }, 1000);
        });

        jQuery('.viewQues').hide();
        <?php if($timelimit_quiz == 0){ ?>
        jQuery('.yyy').hide();
        <?php }?>
        <?php if($timelimit_quiz != 0){ ?>
        jQuery('.yyy').show();
        <?php }?>
        jQuery('.question1').show();
        jQuery('.review_quiz').hide();
        jQuery('.point1').addClass('pointactive');

        jQuery('.ques_review').click(function(){
            var n = jQuery(this).attr('data');
            jQuery('.question'+n).show();
            jQuery('.all-point span').removeClass('pointactive');
            jQuery('.point'+n).addClass('pointactive');
            jQuery('.review_quiz').hide();
            stt_ques = parseInt(n);
            next = stt_ques + 1;
            prev = stt_ques - 1;
            console.log(stt_ques +' '+' '+prev+' '+next)
            jQuery('.next-question-after').show();
        });

        jQuery('.btn-start-quiz').on('click',function(){
            jQuery('#iframe-toan').contents().find(".singlebutton form input[value='Submit all and finish']").last().click();
                            jQuery('.loadques').show();
                            jQuery('#iframe-quiz').contents().find(".submitbtns input[value='Next']").click().delay(2000).queue(function(){
                                jQuery('#iframe-toan').contents().find(".ft .button-group button").last().click();
                                myFunctionQues();
                                setInterval(function(){
                                    myStopFunctionQues();
                                }, 1500);
                            });
            // jQuery.confirm({
            //     content: '<?php echo JText::_('COM_JOOMDLE_MESSAGE_COMPLETE');?>',
            //     title: false,
            //     columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
            //     theme: 'blue',
            //     buttons: {
            //         confirm: {
            //             text: 'Yes',
            //             btnClass: 'yes',
            //             action: function(){
            //                 jQuery('.loadques').show();
            //                 jQuery('#iframe-quiz').contents().find(".submitbtns input[value='Next']").click().delay(2000).queue(function(){
            //                     jQuery('#iframe-toan').contents().find(".ft .button-group button").last().click();
            //                     myFunctionQues();
            //                     setInterval(function(){
            //                         myStopFunctionQues();
            //                     }, 1500);
            //                 });

            //             }
            //         },
            //         cancel: {
            //             text: 'Cancel',
            //             btnClass: 'cancel',
            //             action: function(){
            //                 jQuery('#iframe-toan').contents().find(".ft .button-group .default").last().click();
            //             }
            //         }
            //     }
            // });

        });

        jQuery('.answers .status').click(function() {
            if (jQuery(this).parents('.viewQues').hasClass('multi')) {
                jQuery(this).toggleClass('selected');
            } else {
            var x = jQuery(this).parent().parent().attr('data');
            jQuery('.'+x+' .status').removeClass('selected');
            jQuery(this).addClass('selected');
            }
        });
        jQuery('.spanstatus').click(function() {
            jQuery(this).prev('.status').click();
        });
        jQuery('.answer_true').click(function(){
            var x = jQuery(this).parent().parent().attr('data');
            jQuery('#iframe-quiz').contents().find('#'+x+' .r0 input[type="radio"]').click();
        });
        jQuery('.answer_false').click(function(){
            var x = jQuery(this).parent().parent().attr('data');
            jQuery('#iframe-quiz').contents().find('#'+x+' .r1 input[type="radio"]').click();
        });

        jQuery('.answer_multichoice').click(function(){
            var x = jQuery(this).parent().attr('data');
            var count_answer = jQuery('.'+x+' .count_answer').val();
            var value = jQuery(this).find('.spanstatus').html();
            var t = jQuery('#iframe-quiz').contents().find("#"+x+" .answer input[type=radio]").attr('name');

            if (typeof(t) == 'undefined') {
                jQuery('#iframe-quiz').contents().find("#"+x+" .answer input[type=checkbox]").each(function() {
                    var txt = jQuery('#iframe-quiz').contents().find("#"+x+" .answer label[for='"+jQuery(this).attr('name')+"']").html().substr(3);
                    if (txt == value) {
                        jQuery('#iframe-quiz').contents().find("#"+x+" .answer label[for='"+jQuery(this).attr('name')+"']").click();
                    }
                });
            } else {
            for(var i = 0; i < count_answer ; i ++) {
                var txt = jQuery('#iframe-quiz').contents().find("#"+x+" .answer label[for='"+t+i+"']").html().substr(3);
                    // if (txt.indexOf(value)!=-1){
                    if (txt == value){
                    jQuery('#iframe-quiz').contents().find("#"+x+" .answer label[for='"+t+i+"']").click();
                }
            }
            }
        });

        jQuery('.answers .match_answer').on('change',function() {
            var x = jQuery(this).parent().parent().attr('data');
            var question = jQuery(this).parent().find('b').html();
            // var answer = jQuery(this).val();
            var answer = jQuery(this).find('option[value="'+jQuery(this).val()+'"]'). html();
            var count_answer = jQuery('.'+x+' .count_answer').val();
            for(var i = 0; i < count_answer; i++) {
                if (jQuery('#iframe-quiz').contents().find('#'+x+' .answer .tr'+i+' .qtxt'+i+' ').html() == question) {
                    var u = jQuery('#iframe-quiz').contents().find('#'+x+' .answer .tr'+i+' .select option ').length;
                    for (var j = 0; j < u ; j++) {
                        // if(jQuery('#iframe-quiz').contents().find('#'+x+' .answer .tr'+i+' .select option[value='+j+'] ').html().indexOf(answer)!=-1) {
                        if(jQuery('#iframe-quiz').contents().find('#'+x+' .answer .tr'+i+' .select option[value='+j+'] ').html() == answer) {
                            jQuery('#iframe-quiz').contents().find('#'+x+' .answer .tr'+i+' .select option[value='+j+'] ').attr('selected','selected');
                        }
                    }
                }
            }
        });

        var myVar;
        var id;

        jQuery('.shortanswer').click(function(){
            id = jQuery(this).parent().attr('data');
            myFunction(id);
        });
        function myFunction(id) {
            myVar = setInterval(function(){
                jQuery('#iframe-quiz').contents().find("#"+id+" .answer input[type='text']").val(jQuery('.'+id+'  .shortanswer').val());
            }, 500);
        }

        function myStopFunction() {
            clearTimeout(myVar);
        }

        jQuery('.btn-submit').on('click',function() {
            if (jQuery(this).attr('data-disabled') != 1) {
            myStopFunction();
            if (stt_ques < numberOfQuestions) {
                jQuery('#iframe-quiz').contents().find(".submitbtns input[value='Next']").click();
                jQuery('.viewQues').hide();
                jQuery('.question'+next).show();
                stt_ques++;
                next = stt_ques + 	1;
                prev = stt_ques - 1;

                <?php if ($timelimit_quiz == 0) { ?>
                    if (stt_ques == numberOfQuestions) {
                    jQuery('.next-question-after').hide();
                }
                <?php } else if($timelimit_quiz != 0) { ?>
                    if (stt_ques > numberOfQuestions) {
                    jQuery('.next-question-after').hide();
                }
                <?php } ?>

                jQuery('.all-point span').removeClass('pointactive');
                jQuery('.point'+stt_ques).addClass('pointactive');
            } else if(stt_ques == numberOfQuestions) {
                jQuery('#iframe-toan').contents().find(".singlebutton form input[value='Submit all and finish']").last().click();
                                jQuery('.loadques').show();
                                jQuery('#iframe-quiz').contents().find(".submitbtns input[value='Next']").click().delay(2000).queue(function(){
                                    jQuery('#iframe-toan').contents().find(".ft .button-group button").last().click();
                                    myFunctionQues();
                                    setInterval(function(){
                                        myStopFunctionQues();
                                    }, 1500);
                                });
                // jQuery.confirm({
                //     content: '<?php echo JText::_('COM_JOOMDLE_MESSAGE_COMPLETE');?>',
                //     title: false,
                //     columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
                //     theme: 'blue',
                //     buttons: {
                //         confirm: {
                //             text: 'Yes',
                //             btnClass: 'yes',
                //             action: function(){
                //                 jQuery('.loadques').show();
                //                 jQuery('#iframe-quiz').contents().find(".submitbtns input[value='Next']").click().delay(2000).queue(function(){
                //                     jQuery('#iframe-toan').contents().find(".ft .button-group button").last().click();
                //                     myFunctionQues();
                //                     setInterval(function(){
                //                         myStopFunctionQues();
                //                     }, 1500);
                //                 });
                //             }
                //         },
                //         cancel: {
                //             text: 'Cancel',
                //             btnClass: 'cancel',
                //             action: function(){
                //                 jQuery('#iframe-toan').contents().find(".ft .button-group .default").last().click();
                //             }
                //         }
                //     }
                // });
                            }
        }
                });
        });
    <?php if($timeleft_quiz>0){ ?>
    var seconds = <?php echo $timeleft_quiz*60; ?>;
    function timer() {
        var days        = Math.floor(seconds/24/60/60);
        var hoursLeft   = Math.floor((seconds) - (days*86400));
        var hours       = Math.floor(hoursLeft/3600);
        var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
        var minutes     = Math.floor(minutesLeft/60);
        var minutesTotal = hours * 60 + minutes;
        var remainingSeconds = seconds % 60;
        if (remainingSeconds < 10) remainingSeconds = "0" + remainingSeconds;
        if (minutes < 10) minutes = "0" + minutes;
        if (minutesTotal < 10) minutesTotal = "0" + minutesTotal;
        if (hours < 10) hours = "0" + hours;

        jQuery('.time-left').html("<?php echo JText::_('COM_JOOMDLE_TIME_LEFT'); ?>: " + minutesTotal  + ":" + remainingSeconds + " <?php //echo JText::_('COM_JOOMDLE_MIN'); ?>");
        if (seconds <= -1) {
            jQuery('.time-left').html("<?php echo JText::_('COM_JOOMDLE_TIMES_UP'); ?>");
            jQuery('.next-question-before').hide();
            jQuery('.next-question-after').hide();
            alert("<?php echo JText::_('COM_JOOMDLE_TIMES_UP_ALERT'); ?>");
            jQuery('.btn-submit').attr('disabled',true);
            clearInterval(countdownTimer);
            window.location.href = '<?php echo JURI::base().'mod/quiz_'.$id_course.'_'.$id_module.'.html'; ?>';
        } else {
            seconds--;
        }
    }
    var countdownTimer = setInterval('timer()', 1000);
    <?php } ?>

</script>
