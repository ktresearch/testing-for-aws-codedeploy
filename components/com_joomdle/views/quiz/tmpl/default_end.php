<?php 
	defined('_JEXEC') or die('Restricted access');
	$itemid = JoomdleHelperContent::getMenuItem();

        $pages = $this->attempt['pages'];
        $rightAnswersCount = 0;
        foreach ($pages as $page) {
          $true =  count($page['correctResponse']);    
        if( $page['qtype'] == 'multichoice' && $true > 1){
            $answers =0;
            for($i =0; $i < count($page['questionAttempt']); $i++ ){
                if($page['questionAttempt']['choice'.$i] == 1)
                    $answers++;
        }
            if ($page['fraction'] > 0.999 && $page['qtype'] != 'shortanswer' && $answers == $true  )  $rightAnswersCount++;
        }
         else   if ($page['fraction'] > 0.999 && $page['qtype'] != 'shortanswer' )  $rightAnswersCount++;
        }

    $length_questions = 0;
    $questions = $this->attempt['questions'];
    foreach ($questions as $key => $question) {
        if ($question['qtype'] != 'shortanswer') {
            $length_questions ++;
        }
    }

    $topic_title = '';

    foreach ($this->mods as $key => $mod) {
        if (isset($mod['mods'])) {
            foreach ($mod['mods'] as $key => $item) {
                if ($item['id'] == $this->attempt['quiz']['cmid']) {
                    $topic_title = $mod['name'];
                }
            }
        }
    }
?>

<link rel="stylesheet" type="text/css" href="<?php echo JURI::base().'components/com_joomdle/views/quiz/css/quiz.css';?>">
<?php
require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);
$render_banner = $banner->renderBanner($this->course_info, $this->mods, $this->hasPermission);
?>
<div class="joomdle-view-quiz <?php echo $this->pageclass_sfx;?>">
	<p class="title-quiz" style="text-align: left !important; font-size: 20px;"><?php echo $topic_title . ': '. $this->attempt['quiz']['name']; ?></p>
        <p class="title-quiz" style="text-align: left !important; margin-top: 10px;"><?php echo JText::_('COM_JOOMDLE_END_OF_TEST_SUMMARY'); ?></p>
        <?php if($length_questions >0) {?>
        <p class="labelClosedQuestions" style="margin-top: 10px;">
            <span class="title-quiz"><?php echo JText::_('COM_JOOMDLE_REVIEW_ANSWERS'); ?></span><br>
            <span class="correctAnswersNumber"><?php echo JText::_('COM_JOOMDLE_YOU_HAVE') .'&nbsp'. $rightAnswersCount.'/'.$length_questions .'&nbsp'. JText::_('COM_JOOMDLE_GRADABLE_QUESTION_CORRECT')  ; ?></span>
        </p>
    <?php }?>
   <?php
$arrChars = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z'];
for ( $i = 0; $i < count($questions); $i++) {
        if($questions[$i]['qtype'] == 'shortanswer' )  {
            $class = true;
            continue;
        }
	$options = isset($questions[$i]['options']) ? json_decode($questions[$i]['options'], true) : array();
	echo '<div class="questionReview">';
    echo '<p style="padding: 10px 0;"><span class="lblQuestion">'.JText::_('COM_JOOMDLE_QUESTION').' '.($i+1).'</span>: <span>'.$questions[$i]['name'].'</span></p>';
	switch ($questions[$i]['qtype']) {
		case 'truefalse':
            $attemptText = $this->attempt['pages'][$i]['questionAttempt'] == 1 ? 'True' : 'False';

            if ($attemptText == $options['answers'][ $questions[$i]['correct_answers'] ]['answer']) {
                echo '<p><span class="lblAnswer">'.JText::_('COM_JOOMDLE_YOUR_ANSWER').'</span>: <span class="">'.$options['answers'][ $questions[$i]['correct_answers'] ]['answer'].'</span><img class="img-tick" src="/images/tick_true.png" alt="True"/></p>';
            } else {
                echo '<p><span class="lblAnswer">'.JText::_('COM_JOOMDLE_YOUR_ANSWER').'</span>: <span class="">'.$attemptText.'</span><img class="img-tick" src="/images/tick_wrong.png" alt="Wrong"/></p>';
                echo '<p class="answerText"><span>'.JText::_('COM_JOOMDLE_CORRECT_ANSWER'). '</span>: <span class="">'.$options['answers'][ $questions[$i]['correct_answers'] ]['answer'].'</span></p>';
            } 
            break;
        case 'multichoice':
            $attemptKey = $this->attempt['pages'][$i]['choices'][ $this->attempt['pages'][$i]['questionAttempt'] ];
            $attemptText = $options['answers'][$attemptKey]['answer'];
            $question_choices = [];
            $question_answers = [];

            // get question choices
            if (is_array($pages[$i]['questionAttempt'])) {
                foreach ($pages[$i]['questionAttempt'] as $key => $value) {
                    if ($value) {
                        $question_choices[] = substr($key, -1);
                    }
                }
            } else {
                $question_choices[] = $pages[$i]['questionAttempt'];
            }
             
            // Get question answers
            foreach ($pages[$i]['correctResponse'] as $key => $value) {
                if ($key == 'answer') {
                    $question_answers[] = $value;
                } else {
                    $question_answers[] = substr($key, -1);
                }
            }

            $cq_ids = [];
            $ca_ids = [];
            foreach ($pages[$i]['choices'] as $key => $value) {
                if (in_array($key, $question_choices)) {
                    $cq_ids[] = $value;
                }
                if (in_array($key, $question_answers)) {
                    $ca_ids[] = $value;
                }
            }                       

//            if (!is_null($questions[$i]['correct_answers']))
//                echo $options['answers'][ $questions[$i]['correct_answers'] ]['answer'];
//            else {
                $ca = array();
                $cq = [];
                $k = 0;
                foreach ($options['answers'] as $value) {
                    if (in_array($value['id'], $ca_ids) && $value['fraction'] > 0) {
                        $ca[] = $arrChars[$k].'. '.$value['answer'];
                    }
                    if (in_array($value['id'], $cq_ids)) {
                        if ($value['fraction'] > 0) {
                            $cq[] = $arrChars[$k].'. '.$value['answer'];
                        } else {
                            $cq[] = $arrChars[$k].'. '.$value['answer'];
                        }
                    }

                    $k++;
                }
                $correctText = implode(" , ", $ca);
                $choicesText = implode(' , ', $cq);
//            }
            
            if ($correctText != $choicesText) {
                 echo '<p><span class="lblAnswer">'.JText::_('COM_JOOMDLE_YOUR_ANSWER').'</span>: <span class="">'.$choicesText.'<img class="img-tick" src="/images/tick_wrong.png" alt="Wrong"/></span>  <span class="answerText"> '.$answerText.' </span></p>';
                echo '<p class="answerText"><span>'.JText::_('COM_JOOMDLE_CORRECT_ANSWER'). '</span>: <span class=""> '.$correctText.' </span></p>';
            } else {
                echo '<p><span class="lblAnswer">'.JText::_('COM_JOOMDLE_YOUR_ANSWER').'</span>: <span class="">'.$correctText.'<img class="img-tick" src="/images/tick_true.png" alt="True"/></span>  <span class="answerText"> '.$answerText.' </span></p>';
            }
            break;
        case 'match':
            $t = 0;
            foreach ($this->attempt['pages'][$i]['correctResponse'] as $key => $value) {
                $attemptKey = $this->attempt['pages'][$i]['choices'][ $value ];
                $attemptText = $options['subquestions'][$attemptKey]['answertext'];
                echo '<div class="matchingAnswer">';
                echo '<p><span class="statementText">'.$arrChars[$t].'. '.$options['subquestions'][$attemptKey]['questiontext'].'</span></p>';

                if ($this->attempt['pages'][$i]['questionAttempt'][$key] == $value) {
                    echo '<p><span class="lblAnswer">'.JText::_('COM_JOOMDLE_YOUR_ANSWER').'</span>: <span class="">'.$attemptText.'</span><img class="img-tick" src="/images/tick_true.png" alt="True"/></p>';
                } else {
                    echo '<p><span class="lblAnswer">'.JText::_('COM_JOOMDLE_YOUR_ANSWER').'</span>: <span class="">'. 
                    $options['subquestions'][$this->attempt['pages'][$i]['choices'][ $this->attempt['pages'][$i]['questionAttempt'][$key] ]
                    ]['answertext'].'</span><img class="img-tick" src="/images/tick_wrong.png" alt="Wrong"/> </br> <span class="answerText">'.JText::_('COM_JOOMDLE_CORRECT_ANSWER').': '.$attemptText.' </span></p>';
                } 
                echo '</div>';

                $t++;
            }
			break;
		case 'shortanswer':
			echo '<p><span class="lblAnswer">'.JText::_('COM_JOOMDLE_YOUR_ANSWER').'</span>: <span class="yourAnswerText">'.$this->attempt['pages'][$i]['questionAttempt'].'</span></p>';
            echo '<p><span class="answerText">'.JText::_('COM_JOOMDLE_CORRECT_ANSWER').': '.$options['answers'][ $questions[$i]['correct_answers'] ]['answer'].'</span></p>';

            break;
		default:
			# code...
			break;
	}

    if (isset($questions[$i]['generalfeedback']) && !empty($questions[$i]['generalfeedback'])) {
        echo '<p style="padding: 10px 0;"><span class="lblFeedback">'.JText::_('COM_JOOMDLE_FEEDBACK').'</span>: <span class="feedbackText">'.$questions[$i]['generalfeedback'].'</span></p>';
    }

	echo '</div>';
}
      // view text responve
    if($class)
        echo '<p class = "textreponsive">'.JText::_('COM_JOOMDLE_REVIEW_ANSWERS_TEXT_RESPONSIVE').'</p>';
        
        for ( $i = 0; $i < count($questions); $i++) {
        if($questions[$i]['qtype'] != 'shortanswer' )  continue;
	$options = isset($questions[$i]['options']) ? json_decode($questions[$i]['options'], true) : array();
	echo '<div class="questionReview">';
        echo '<p style="padding: 10px 0;"><span class="lblQuestion">'.JText::_('COM_JOOMDLE_QUESTION').' '.($i+1).'</span>: <span>'.$questions[$i]['name'].'</span></p>';

    
	switch ($questions[$i]['qtype']) {
		
		case 'shortanswer':
			echo '<p><span class="lblAnswer">'.JText::_('COM_JOOMDLE_YOUR_ANSWER').'</span>: <span class="yourAnswerText">'.$this->attempt['pages'][$i]['questionAttempt'].'</span></p>';
                        echo '<p><span class="answerText">'.JText::_('COM_JOOMDLE_CORRECT_ANSWER').': '.$options['answers'][ $questions[$i]['correct_answers'] ]['answer'].'</span></p>';
            if (isset($options['answers'][ $questions[$i]['correct_answers'] ]['feedback']) && !empty($options['answers'][ $questions[$i]['correct_answers'] ]['feedback'])) {
                echo '<p style="padding: 10px 0;"><span class="lblFeedback">'.JText::_('COM_JOOMDLE_FEEDBACK').'</span>: <span class="feedbackText">'.$options['answers'][ $questions[$i]['correct_answers'] ]['feedback'].'</span></p>';
            }

			break;
		default:
			# code...
			break;
	}

    if (isset($questions[$i]['generalfeedback']) && !empty($questions[$i]['generalfeedback'])) {
        echo '<p style="padding: 10px 0;"><span class="lblFeedback">'.JText::_('COM_JOOMDLE_FEEDBACK').'</span>: <span class="feedbackText">'.$questions[$i]['generalfeedback'].'</span></p>';
    }

	echo '</div>';
}
  
        
      
?>

	<div class="text-center">
		<button class="btnReturnToOutline"><?php echo JText::_('COM_JOOMDLE_RETURN_TO_COURSE_OUTLINE'); ?></button>
	</div>

</div>
<style type="text/css">
    .img-tick {
        width: 20px;
        margin-left: 15px;
        margin-top: -5px;
    }
    .answer-detail { white-space: nowrap; }
    @media screen and (max-width: 479px) and (min-width: 320px) {
        .tab.view-quiz .joomdle-view-quiz { padding-left: 25px; padding-right: 20px; }
        .course-menu { padding-left: 0; padding-right: 0; }
        .joomdle-view-quiz .questionReview { margin: 0 0 15px 0; }
    }
</style>

<script type="text/javascript">
	jQuery(document).ready(function(e){
		jQuery('.btnReturnToOutline').click(function(){
			window.location.href = '<?php echo JURI::base()."course/".$this->courseid.'.html'; ?>';
		});
	});
</script>
