<?php
defined('_JEXEC') or die('Restricted access');

	$menu_display = 'block !important';
	$margin_top = '60px !important';
	if(isset($_COOKIE['app']) && $_COOKIE['app'] == 'appview') {
	    $menu_display = 'none !important';
	    $margin_top = '-60px !important';
	}

$app = JFactory::getApplication();

//update for webview app
if (isset($_SESSION['type']) && $_SESSION['type'] == 'appview') {
    $appview = $_SESSION['type'] == 'appview';
}

$params = $app->getParams();
require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);
$render_banner = $banner->renderBanner($this->course_info, $this->mods, $this->hasPermission);

$itemid = JoomdleHelperContent::getMenuItem();
$id_course = $this->quiz['course_id'];
$id_module = $this->quiz['id'];
$dance = $this->quizes;
$count_dance = count($dance);
for ($i=0; $i < $count_dance; $i++) {
    $count_quizes_dance = count($dance[$i]['quizes']);
    for ($j=0; $j < $count_quizes_dance; $j++) {
        if($dance[$i]['quizes'][$j]['id']==$id_module){
            $this->quiz['attempt'] = $dance[$i]['quizes'][$j]['attempt'];
            $this->quiz['time']['state'] = $dance[$i]['quizes'][$j]['time']['state'];
            $timeopen = $dance[$i]['quizes'][$j]['timeopen'];
            $timeclose = $dance[$i]['quizes'][$j]['timeclose'];
        }
    }
}
$now = date_timestamp_get(date_create());
$intro_quiz = $this->quiz['intro'];
$attempt_quiz = $this->quiz['attempt'];
$timelimit_quiz = $this->quiz['timelimit'];
$questions = $this->quiz['questions'];
$status_quiz = $this->quiz['time']['state'];
$numOfAttempts = $this->quiz['numOfAttempts'];
$attempts = json_decode($this->quiz['attempts'], true);
$quizFinished = true;
$firstAttempt = false;
$inProgress = false;
$reAttempt = false;
if (!empty($attempts)) {
    if (count($attempts) == 1) $firstAttempt = true;
    foreach ($attempts as $key => $value) {
        if ($value['state'] != "finished") {
            $quizFinished = false;
            $inProgress = true;
		}
    }
    if (count($attempts) != $numOfAttempts) {
        $quizFinished = false;
        if (!$inProgress) $reAttempt = true;
    }
} else {
    $quizFinished = false;
    $firstAttempt = true;
}

$length_questions = count($questions);

$list_modules = $this->list_modules;
$count_list_modules = count($list_modules);

// update for appview
if($appview == 'appview' && $count_list_modules == 1) {
    $style = 'display:none;';
}

$this->wrapper->url = $params->get( 'MOODLE_URL' ).'/mod/quiz/view.php?id='.$id_module;
$actual_link = JURI::base().'quiz/'.$id_course.'_'.$id_module.'_start.html';
?>
<link rel="stylesheet" type="text/css" href="<?php echo JURI::base().'components/com_joomdle/views/quiz/css/quiz.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo JURI::base().'components/com_joomdle/views/quiz/css/jquery-confirm.css';?>">
<script type="text/javascript" src="<?php echo JURI::base().'components/com_joomdle/views/quiz/js/jquery-confirm.js';?>"></script>
<style type="text/css">
    .course-menu{
		display:  <?php echo $menu_display; ?>;
    }
    .joomdle-course {
		margin-top: <?php echo $margin_top; ?>;
    }
</style>
<div class="joomdle-view-quiz-start <?php echo $this->pageclass_sfx;?>">
    <?php if (!$quizFinished) : ?>
    <div class="aaaaa">
        <iframe
            id="iframe-quiz"
            class="autoHeight"
            src="<?php echo $this->wrapper->url; ?>"
            width="<?php echo $this->params->get('width', '100%'); ?>"
            scrolling="<?php //echo $this->params->get('scrolling', 'auto'); ?>yes"
            allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"

            <?php if (!$this->params->get('autoheight', 1)) { ?>
                height="<?php echo $this->params->get('height', '500'); ?>"
            <?php
            }
            ?>
            align="top"
            frameborder="0"
            <?php if ($this->params->get('autoheight', 1)) { ?>
            <?php
            }
            ?>
            ></iframe>
    </div>
    <?php else : ?>
        <h2 class="quizTitle"><?php echo $this->quiz['name']; ?></h2>
        <div class="labelInstructions"><?php echo JText::_('COM_JOOMDLE_INSTRUCTIONS'); ?></div>
        <div class="quizDes description"><?php echo $this->quiz['intro']; ?></div>
        <div class="divButtonReview">
            <a class="btReviewQuiz" href="<?php echo JUri::base().'quiz/'.$id_course.'_'.$id_module.'_end_'.$attempt_quiz.'.html'; ?>"><?php echo JText::_('COM_JOOMDLE_REVIEW_ASSESSMENT_TEXT'); ?></a>
        </div>
    <?php endif; ?>
    <div style="<?php echo $style; ?>">
        <div class="introduction">
            <!-- Previous and next activity-->
            <div class="btn-prev-next">
                <?php for($i = 0; $i < $count_list_modules ; $i++){
                    if ($id_module == $list_modules[$i]['id'] && $i == 0) {
                        $next = $i+1;
                        switch ($list_modules[$next]['mod']) {
                            case 'quiz':
                                $url_next = JURI::base().'mod/quiz_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                            case 'assign':
                                $url_next = JURI::base().'viewassign/'.$id_course.'_'.$list_modules[$next]['id'].'.html';
                                break;
                            case 'scorm':
                                $url_next = JURI::base().'mod/scorm_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                            case 'page':
                                $url_next = JURI::base().'mod/page_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                            default:
                                $url_next = JURI::base().'mod/quiz_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                        }?>
                        <div class="buttons">
                            <a class="icon-next" target="_parent" href="<?php echo $url_next;?>" style="float:right;" >
                                <img src="<?php echo JUri::base();?>/images/NextIcon.png">
                            </a>
                        </div>

                    <?php } else if ($id_module == $list_modules[$i]['id'] && $i > 0 && $i <= $count_list_modules-2) {
                        $prev = $i-1;
                        $next = $i+1;
                        switch ($list_modules[$prev]['mod']) {
                            case 'quiz':
                                $url_prev = JURI::base().'mod/quiz_'.$id_course.'_'.$list_modules[$prev]['id'].'_.html';
                                break;
                            case 'assign':
                                $url_prev = JURI::base().'viewassign/'.$id_course.'_'.$list_modules[$prev]['id'].'.html';
                                break;
                            case 'scorm':
                                $url_prev = JURI::base().'mod/scorm_'.$id_course.'_'.$list_modules[$prev]['id'].'_.html';
                                break;
                            case 'page':
                                $url_prev = JURI::base().'mod/page_'.$id_course.'_'.$list_modules[$prev]['id'].'_.html';
                                break;

                            default:
                                $url_next = JURI::base().'mod/quiz_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                        }
                        switch ($list_modules[$next]['mod']) {
                            case 'quiz':
                                $url_next = JURI::base().'mod/quiz_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                            case 'assign':
                                $url_next = JURI::base().'viewassign/'.$id_course.'_'.$list_modules[$next]['id'].'.html';
                                break;
                            case 'scorm':
                                $url_next = JURI::base().'mod/scorm_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                            case 'page':
                                $url_next = JURI::base().'mod/page_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                            default:
                                $url_next = JURI::base().'mod/quiz_'.$id_course.'_'.$list_modules[$next]['id'].'_.html';
                                break;
                        }?>
                        <div class="buttons">
                            <a class="icon-prev" target="_parent" href="<?php echo $url_prev;?>" style="float:left;">
                                <img src="<?php echo JUri::base();?>/images/PreviousIcon.png">
                            </a>
                            <a class="icon-next" target="_parent" href="<?php echo $url_next;?>" style="float:right;" >
                                <img src="<?php echo JUri::base();?>/images/NextIcon.png">
                            </a>
                        </div>


                    <?php } else if ($id_module == $list_modules[$i]['id'] && $i==$count_list_modules-1) {
                        $prev = $i-1;
                        switch ($list_modules[$prev]['mod']) {
                            case 'quiz':
                                $url_prev = JURI::base().'mod/quiz_'.$id_course.'_'.$list_modules[$prev]['id'].'_.html';
                                break;
                            case 'assign':
                                $url_prev = JURI::base().'viewassign/'.$id_course.'_'.$list_modules[$prev]['id'].'.html';
                                break;
                            case 'scorm':
                                $url_prev = JURI::base().'mod/scorm_'.$id_course.'_'.$list_modules[$prev]['id'].'_.html';
                                break;
                            case 'page':
                                $url_prev = JURI::base().'mod/page_'.$id_course.'_'.$list_modules[$prev]['id'].'_.html';
                                break;

                            default:
                                # code...
                                break;
                        }?>
                        <div class="buttons">
                            <a class="icon-prev" target="_parent" href="<?php echo $url_prev;?>" style="float:left;">
                                <img src="<?php echo JUri::base();?>/images/PreviousIcon.png">
                            </a>
                        </div>

                    <?php } }?>
            </div>
            <!-- Previous and next activity-->
        </div>
    </div>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            <?php if (!$quizFinished && !$inProgress) { ?>
            jQuery('.btn-start-quiz').on('click',function(){
                console.log(1);
                <?php if ($timeopen > $now) { ?>
                jQuery.alert({
                    content: "The quiz will not be available until "+ '<?php echo date('d/m/Y H:i',$timeopen); ?>',
                    columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
                    theme: 'blue',
                    title: false
                });
                <?php } else if ($timeclose != 0 && $timeclose < $now) { ?>
                jQuery.alert({
                    content: "The quiz closed on "+ '<?php echo date('d/m/Y ',$timeclose); ?>',
                    columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
                    theme: 'blue',
                    title: false
                });
                <?php } else if ($length_questions == 0) { ?>
                jQuery.alert({
                    content: "The quiz do not have any questions.",
                    columnClass: 'col-md-5 col-md-offset-3 col-sm-10 col-sm-10 col-xs-10',
                    theme: 'blue',
                    title: false
                });
                <?php } else { ?>
                jQuery('#iframe-quiz').contents().find(".quizstartbuttondiv form input[type=submit]").click();

                jQuery('.isload').hide();
                jQuery('.loadques').show();
                setInterval(function(){
                        jQuery('#iframe-quiz').contents().find(".ft .button-group button").last().click().delay(1500).queue(function(){
                                                window.location.href = '<?php echo $actual_link; ?>';
                        });
                },1000);

                <?php } ?>
            });
            <?php } else if ($quizFinished) { ?>
            jQuery('.btn-start-quiz').on('click', function(){
                console.log(2);
                var url = jQuery('#iframe-quiz').contents().find(".quizattemptsummary tbody .lastrow .lastcol a").attr('href');
                jQuery('.introduction h5 , .introduction .btn-start-quiz').hide();
                jQuery('#iframe-quiz').attr('src',url);
                setInterval(function(){
                    var x = jQuery('#iframe-quiz').contents().find(" form ").html();
                    jQuery('.review .answer_review').html(x);
                }, 1000);
                jQuery('.btn-prev-next').hide();
                jQuery('.review').show();
                jQuery('.review-quiz').show();
            });
            jQuery('.review-quiz').on('click',function(){
                jQuery('#iframe-quiz').contents().find(".submitbtns .arrow_link .arrow_text").click();
                if(jQuery('#iframe-quiz').contents().find(".submitbtns a").html()=="Finish review"){
                    window.location.href= '<?php echo JURI::base().'mod/quiz_'.$id_course.'_'.$id_module.'.html';?>';
                }else{
                    jQuery('#iframe-quiz').contents().find(".submitbtns .arrow_link .arrow_text").click();
                }
            });
            <?php } else {?>
            jQuery('.btn-start-quiz').on('click',function(){
                console.log(3);
                jQuery('#iframe-quiz').contents().find(".quizstartbuttondiv form input[type=submit]").click();
                window.location.href = '<?php echo $actual_link; ?>';
            });
            <?php } ?>
            jQuery('#iframe-quiz').load(function() {
                jQuery('#iframe-quiz').css('min-height', jQuery('#iframe-quiz').contents().find('body').height() );
                jQuery('#iframe-quiz').contents().find("#page-mod-quiz-view .quizstartbuttondiv form input[type=submit]").click(function() {
                    jQuery('.introduction').hide();
                    jQuery('.introduction').css("margin-top", "0");
                    jQuery('.introduction .buttons').fadeOut();
                    jQuery('.joomdle-view-quiz-start > .container').fadeOut();
                });
                jQuery('#iframe-quiz').contents().find("#page-mod-quiz-view .btReviewQuiz").click(function() {
                	jQuery('.introduction .buttons').fadeOut();
                    jQuery('.joomdle-view-quiz-start > .container').fadeOut();
            });
        });
        });
    </script>
</div>