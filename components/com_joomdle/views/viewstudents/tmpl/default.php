<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 * @date: 25 june 2016
 * @author: KV team
 * @package: joomdle view progress (for facilitator)
 */

defined('_JEXEC') or die('Restricted access');

JFactory::getDocument()->setTitle('Overview');

$itemid = JoomdleHelperContent::getMenuItem();
$username = JFactory::getUser()->username;

$role = $this->hasPermission;
$user_role = array();
if (!empty($role)) {
    foreach (json_decode($role[0]['role']) as $r) {
        $user_role[] = $r->sortname;
    }
}

require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');
$banner = new HeaderBanner(new JoomdleBanner);

$render_banner = $banner->renderBanner($this->course_info, $this->mods['sections'], $role);

$grade_letter_course = array();
$grade_letter_course[0]['name'] = 'A';
$grade_letter_course[0]['value'] = 90;

$grade_letter_course[1]['name'] = 'B';
$grade_letter_course[1]['value'] = 70;

$grade_letter_course[2]['name'] = 'C';
$grade_letter_course[2]['value'] = 50;

$grade_letter_course[3]['name'] = 'D';
$grade_letter_course[3]['value'] = 30;
?>
<div class="joomdle-overview-header">
    <?php
    require_once(JPATH_SITE.'/components/com_joomdle/views/header_facilitator.php');
    $countuser = count($this->studentscomplete) + count($this->studentsnotcomplete);
    ?>
</div>
<div class="joomdle-course joomdle-overview-content <?php echo $this->pageclass_sfx?>">
    <div class="toltallearner"><span><?php echo JText::_('COM_JOOMDLE_TOTAL_LEARNER').': '.$countuser; ?></span>       
    </div>
    <div class="line"></div>
    <div class="toltalcompleted">
        <div class="titlecomplete">
            <div class="text"><span><?php echo JText::_('COM_JOOMDLE_COMPLETED')?> </span><span style="font-family: 'Open Sans', OpenSans, sans-serif;font-weight: 400;"><?php echo count($this->studentscomplete); ?></span></div>
            <?php if(count($this->studentscomplete) > 0){ ?>
            <div class="btshow"><span  class="glyphicon glyphicon-menu-down btcomplete"></span></div>
            <div class="bthideshow"><span  class="glyphicon glyphicon-menu-up btcomplete"></span></div>
            <?php }?>
        </div>
        <ul class="joomdle-list ">
        <?php
        // begin render student in course
        $i = 0;
        if(is_array($this->studentscomplete)) {
            foreach ($this->studentscomplete as $student) {
                $users_joomla = CFactory::getUser($student['username']);
                if ($student['username'] == $username) continue;
                $user_by_username = JFactory::getUser($student['username']);
                if($user_by_username->id <= 0) {
                    $avatar = '/components/com_community/assets/user-Male.png';
                } else {
                    $users = CFactory::getUser($user_by_username->id);
                    $avatar = $users->getAvatar();
                }
                if (!$user_by_username->username || $user_by_username->username == $username) continue;
                $final_grade = 0;
                $finalgradeletters = '';

                // foreach ($student['grades'] as $grade) {
                $final_grade = $student['grades'][0]['finalgrade'];
                $finalgradeletters = $student['grades'][0]['finalgradeletters'];
                $hidden = $student['grades'][0]['hidden'];
                $gradeoffacitator = $student['grades'][0]['gradeoffacitator'];
                
                if ($finalgradeletters != "Ungraded") {
                    foreach ($grade_letter_course as $grade_course) {
                        if ($final_grade >= $grade_course['value']) {
                            $finalgradeletters_course = $grade_course['name'];
                            break;
                        }

                    }
                }
                // }
                //Trung KKV - Update: first record is main grade category
                // if (isset($student['overallgrade'])) $final_grade = $student['overallgrade'];
                // if (isset($student['overallgradeletter'])) $finalgradeletters = $student['overallgradeletter'];
                ?>
            
                <li class="list-user-grade">
                    
                        <div class="avatar-overall">
                            <a href="<?php echo JUri::base() . "studentdetail/".$users_joomla->id."_". $this->course . ".html";?>" class="nav-avatar"><img alt="<?php echo $student['firstname'].' '.$student['lastname']; ?>" src="<?php echo $avatar; ?>"/></a>
                            <span class="grade">
                                <a href="<?php echo JUri::base() . "studentdetail/".$users_joomla->id."_". $this->course . ".html";?>">
                                <?php if($hidden == 0 && $gradeoffacitator && $finalgradeletters != "Ungraded" && $final_grade > 0){
                                    echo  $finalgradeletters_course;
                                } elseif($hidden == 1){
                                    echo  "-";
                                } else {
                                    echo  "-";
                                }
                                ?>
                                </a>
                            </span>
                        </div>
                        <div class="user-grade">
                            <a href="<?php echo JUri::base() . "studentdetail/".$users_joomla->id."_". $this->course . ".html";?>">
                                <p class="name"><?php echo $student['firstname'].' '.$student['lastname']; ?></p></a>
                        </div>
                     
                </li>
                
                <?php
                $i++;
            }
        }
        ?>
    </ul>
        
    </div>
    <div class="line"></div>
    <div class="toltalnotcompleted">
        <div class="titlenotcomplete">
            <div class="text"><span><?php echo JText::_('COM_JOOMDLE_NOT_COMPLETE')?> </span><span style="font-family: 'Open Sans', OpenSans, sans-serif;font-weight: 400;"><?php echo count($this->studentsnotcomplete); ?></span></div>
            <?php if(count($this->studentsnotcomplete) >0){?>
            <div class="btshow"><span  class="glyphicon glyphicon-menu-down btcomplete"></span></div>
            <div class="bthideshow"><span  class="glyphicon glyphicon-menu-up btcomplete"></span></div>
            <?php }?>
        </div>
        <ul class="joomdle-list ">
        <?php
        // begin render student in course
        $i = 0;
        if(is_array($this->studentsnotcomplete)) {
            foreach ($this->studentsnotcomplete as $student) {
                $users_joomla = CFactory::getUser($student['username']);
                if ($student['username'] == $username) continue;
                $user_by_username = JFactory::getUser($student['username']);
                if($user_by_username->id <= 0) {
                    $avatar = '/components/com_community/assets/user-Male.png';
                } else {
                    $users = CFactory::getUser($user_by_username->id);
                    $avatar = $users->getAvatar();
                }
                if (!$user_by_username->username || $user_by_username->username == $username) continue;
                $final_grade = 0;
                $finalgradeletters = '';

                // foreach ($student['grades'] as $grade) {
                $final_grade = $student['grades'][0]['finalgrade'];
                $finalgradeletters = $student['grades'][0]['finalgradeletters'];
                $hidden = $student['grades'][0]['hidden'];
                $gradeoffacitator = $student['grades'][0]['gradeoffacitator'];
                
                if ($finalgradeletters != "Ungraded") {
                    foreach ($grade_letter_course as $grade_course) {
                        if ($final_grade >= $grade_course['value']) {
                            $finalgradeletters_course = $grade_course['name'];
                            break;
                        }

                    }
                }
                // }
                //Trung KKV - Update: first record is main grade category
                // if (isset($student['overallgrade'])) $final_grade = $student['overallgrade'];
                // if (isset($student['overallgradeletter'])) $finalgradeletters = $student['overallgradeletter'];
                ?>
            
                <li class="list-user-grade">
                    
                        <div class="avatar-overall">
                            <a href="<?php echo JUri::base() . "studentdetail/".$users_joomla->id."_". $this->course . ".html";?>" class="nav-avatar"><img alt="<?php echo $student['firstname'].' '.$student['lastname']; ?>" src="<?php echo $avatar; ?>"/></a>
                            <span class="grade">
                                <a href="<?php echo JUri::base() . "studentdetail/".$users_joomla->id."_". $this->course . ".html";?>">
                                <?php if($hidden == 0 && $gradeoffacitator && $finalgradeletters != "Ungraded" && $final_grade > 0){
                                    echo  $finalgradeletters_course;
                                } elseif($hidden == 1){
                                    echo  "-";
                                } else {
                                    echo  "-";
                                }
                                ?>
                                </a>
                            </span>
                        </div>
                        <div class="user-grade">
                            <a href="<?php echo JUri::base() . "studentdetail/".$users_joomla->id."_". $this->course . ".html";?>">
                                <p class="name"><?php echo $student['firstname'].' '.$student['lastname']; ?></p></a>
                        </div>
                     
                </li>
                
                <?php
                $i++;
            }
        }
        ?>
    </ul>
    </div>
    <?php
//    if ($i == 0) {
//        echo '<div class="bold cred tcenter bg">'.JText::_('COM_JOOMDLE_NO_USERS_ENROLLED').'</div>';
//    }
    ?>
</div>
<script>
    jQuery(document).ready(function () {
        jQuery('.toltalcompleted .btshow').click(function(e) {
            jQuery('.toltalcompleted .bthideshow').show();
            jQuery('.toltalcompleted .btshow').hide();
            jQuery('.toltalcompleted .joomdle-list ').css('display','inline-block');
        });
        jQuery('.toltalcompleted .bthideshow').click(function(e) {
             jQuery('.toltalcompleted .btshow').show();
             jQuery('.toltalcompleted .bthideshow').hide();
             jQuery('.toltalcompleted .joomdle-list ').hide();
        });
         jQuery('.toltalnotcompleted .btshow').click(function(e) {
            jQuery('.toltalnotcompleted .bthideshow').show();
            jQuery('.toltalnotcompleted .btshow').hide();
            jQuery('.toltalnotcompleted .joomdle-list ').css('display','inline-block');
        });
        jQuery('.toltalnotcompleted .bthideshow').click(function(e) {
             jQuery('.toltalnotcompleted .btshow').show();
             jQuery('.toltalnotcompleted .bthideshow').hide();
             jQuery('.toltalnotcompleted .joomdle-list ').hide();
        });
    });
</script>
