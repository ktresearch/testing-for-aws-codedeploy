<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
 * @date: 17 jul 2016
 * @author: KV team
 * @package: joomdle course overview student for Facilitator
 */

class JoomdleViewViewstudents extends JViewLegacy {
    function display($tpl = null) {   
        $app = JFactory::getApplication();
        $params = $app->getParams();
        $this->assignRef('params', $params);
        $user = JFactory::getUser();
        $username = $user->username;

        $id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
        if (!$id) $id =  JRequest::getVar( 'course_id' );
        if (!$id)
            $id = $params->get( 'course_id' );

        $id = (int) $id;

        if (!$id) {
            JFactory::handleErrors();
        }
        $this->hasPermission = JFactory::hasPermission($id, $username);
        $user_role = array();
        if (!empty($this->hasPermission)) {
            foreach (json_decode($this->hasPermission[0]['role']) as $r) {
                $user_role[] = $r->sortname;
            }
        }
        if (empty(array_intersect($user_role, ['teacher', 'editingteacher', 'manager', 'coursecreator']))) {
            JFactory::handleErrors();
        }

        // course fullname
        if ($params->get('use_new_performance_method'))
            $course_info = JHelperLGT::getCourseInfo(['id' => $id, 'username' => $username]);
        else
            $course_info = JoomdleHelperContent::getCourseInfo($id, $username);

        $this->course_info = $course_info;
        $this->coursefullname = $course_info['fullname'];
        $this->course = $id;
                
        if ($params->get('use_new_performance_method')) {
            // call api overview: get student enrolled  into course.
            $this->studentscomplete = JHelperLGT::teacherGetCourseGrades($id, '', '', '', 'complete');
            $this->studentsnotcomplete = JHelperLGT::teacherGetCourseGrades($id, '', '', '', 'notcomplete');

            $this->mods = JHelperLGT::getModProgress($id, $username);
        } else {
            // call api overview: get student enrolled  into course.
            $this->studentscomplete = JoomdleHelperContent::call_method ( 'teacher_get_course_grades', (int) $id, '', '', '',$completed = 'complete');
            $this->studentsnotcomplete = JoomdleHelperContent::call_method ( 'teacher_get_course_grades', (int) $id, '', '', '',$completed = 'notcomplete');

            $this->mods = JoomdleHelperContent::call_method ( 'get_mod_progress', (int) $id, $username);
        }

        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
        
        parent::display($tpl);
    }
}