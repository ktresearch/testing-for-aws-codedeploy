<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class JoomdleViewStudentdetail extends JViewLegacy {

    function display($tpl = null) {
        $app = JFactory::getApplication();
        $pathway = $app->getPathWay();

        $menus = $app->getMenu();
        $menu = $menus->getActive();

        $params = $app->getParams();
        $this->assignRef('params', $params);

        $currentUser = JFactory::getUser();
        $currentUsername = $currentUser->username;

        $course_id = JRequest::getVar( 'course_id', null, 'NEWURLFORM' );
        if (!$course_id) $course_id =  JRequest::getVar( 'course_id' );
        if (!$course_id)
            $course_id = $params->get( 'course_id' );

        $course_id = (int) $course_id;

        $this->hasPermission = JFactory::hasPermission($course_id, $currentUsername);
        $user_role = array();
        if (!empty($this->hasPermission)) {
            foreach (json_decode($this->hasPermission[0]['role']) as $r) {
                $user_role[] = $r->sortname;
            }
        }
        
        if (empty(array_intersect($user_role, ['teacher', 'editingteacher', 'manager', 'coursecreator']))) {
            JFactory::handleErrors();
        }
	
        $user_id = JRequest::getVar( 'user_id', null, 'NEWURLFORM' );
        if (!$user_id)
            $user_id = JRequest::getVar( 'user_id' );
        if (!$user_id)
            $user_id = $params->get('user_id');
        $user_id = (int)$user_id;
        $user = JFactory::getUser ($user_id);
        $username = $user->username;

        $this->user = $username;

        if ($params->get('use_new_performance_method'))
            $this->course_info = JHelperLGT::getCourseInfo(['id' => $course_id, 'username' => $username]);
        else
            $this->course_info = JoomdleHelperContent::getCourseInfo($course_id, $username);

        $this->coursefullname = $this->course_info['fullname'];
        $this->courseid = $this->course_info['remoteid'];

//        $this->overall_grade = JoomdleHelperContent::call_method ( 'get_course_grades_by_category', $course_id, $username);

        if ($params->get('use_new_performance_method')) {
            $this->mods_student = JHelperLGT::getModProgress($course_id, $username);
            $this->studentscomplete =  JHelperLGT::getCourseStudents($course_id, 'complete');
        } else {
            $this->mods_student = JoomdleHelperContent::call_method ( 'get_mod_progress', $course_id, $username);
            $this->studentscomplete =  JoomdleHelperContent::call_method ('get_course_students', $course_id, $completed = 'complete');
        }

        parent::display($tpl);
    }
}
