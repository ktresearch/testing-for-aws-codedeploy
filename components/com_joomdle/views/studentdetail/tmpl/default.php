<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

 * @date: 17 jul 2016
 * @author: KV team
 * @package: joomdle view student (for facilitator)
 */

defined('_JEXEC') or die('Restricted access');

$itemid = JoomdleHelperContent::getMenuItem();

JFactory::getDocument()->setTitle('Overview');

require_once(JPATH_SITE . '/components/com_community/libraries/core.php');
require_once(JPATH_SITE . '/components/com_joomdle/views/header_banner.php');

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'components/com_joomdle/views/studentdetail/tmpl/studentdetail.css');

$session = JFactory::getSession();
$device = $session->get('device');
if ($device == 'mobile') $mobile = 'mobile';
$role = $this->hasPermission;
$user_role = array();
if (!empty($role)) {
    foreach (json_decode($role[0]['role']) as $r) {
        $user_role[] = $r->sortname;
    }
}
$banner = new HeaderBanner(new JoomdleBanner);

$render_banner = $banner->renderBanner($this->course_info, $this->mods_student['sections'], $role);

// get device
$device = $session->get('device');
if ($device == 'mobile') {
    $cols = 5;
    $rows = 2;
} else {
    $cols = 10;
    $rows = 2;
}
?>
<!--autosize-->
<style type="text/css">
    .student-activity .feedback-content #txtFeedback {
        <?php if ($device == 'mobile') { ?>
            max-height: 227px !important;
        <?php } else { ?>
            max-height: 345px !important;
        <?php } ?>
    }

    @media screen and (max-width: 768px) {
        textarea { padding: 2px 12px !important; }
    }
</style>
<script src="<?php echo JURI::base(); ?>components/com_community/assets/autosize.min.js"></script>
<div class="joomdle-overview-header">
    <?php
    require_once(JPATH_SITE . '/components/com_joomdle/views/header_facilitator.php');
    ?>
</div>
<div class="joomdle-course joomdle-student-view <?php echo $this->pageclass_sfx ?>">
    <form action="<?php echo JRoute::_('index.php?option=com_joomdle&task=saveStudentGrade'); ?>" method="post"
          id="student-grade" data="nohas">
        <?php

        $user = JFactory::getUser($this->user);
        if ($user->id <= 0) {
            $avatar = '/components/com_community/assets/user-Male.png';
        } else {
            $users = CFactory::getUser($user->id);
            // get avatar
            $avatar = $users->getAvatar();
        }

        $grade_letters = $this->mods_student['gradeoptionsletters'];
        $grade_letter_course = array();
        $grade_letter_course[0]['name'] = 'A';
        $grade_letter_course[0]['value'] = 90;

        $grade_letter_course[1]['name'] = 'B';
        $grade_letter_course[1]['value'] = 70;

        $grade_letter_course[2]['name'] = 'C';
        $grade_letter_course[2]['value'] = 50;

        $grade_letter_course[3]['name'] = 'D';
        $grade_letter_course[3]['value'] = 30;

        $grade_letter_assign = array();
        $grade_letter_assign[0]['name'] = 'A';
        $grade_letter_assign[0]['value'] = 93;

        $grade_letter_assign[1]['name'] = 'B';
        $grade_letter_assign[1]['value'] = 83;

        $grade_letter_assign[2]['name'] = 'C';
        $grade_letter_assign[2]['value'] = 73;

        $grade_letter_assign[3]['name'] = 'D';
        $grade_letter_assign[3]['value'] = 60;

        ?>
        <div class="student-info">
            <div class="avatar-overall"><a class="nav-avatar"><img alt="<?php echo $users->name; ?>"
                                                                   src="<?php echo $avatar ?>"/></a></div>
            <div class="user-grade">
                <div class="name"><?php echo CActivities::truncateComplex($users->name, 30, true); ?></div>
                <div class="grade"><?php echo JText::_('COM_JOOMDLE_COURSE_OVERALLGRADE'); ?></div>
               
                <?php if ($this->mods_student['gradeitemid']) { ?>

                    <div class="gradeCourseSelect">
                        <div class="gradeSelected">
                            <?php
                            if ($this->mods_student['finalgradeletters'] != "Ungraded") {
                                foreach ($grade_letter_course as $grade_course) {
                                    if ($this->mods_student['finalgrade'] >= $grade_course['value']) {
                                        $finalgradeletters_course = $grade_course['name'];
                                        break;
                                    }
                                }
                            }

                            if ($this->mods_student['hidden'] == 1) {
                                echo "-";
                            } elseif ($this->mods_student['hidden'] == 0 && $this->mods_student['gradeoffacitator'] && $this->mods_student['finalgradeletters'] != "Ungraded" && $this->mods_student['finalgrade'] > 0) {
                                ?><?php
                                echo $finalgradeletters_course;
                                $finalGrade_course = $this->mods_student['finalgrade'];

                            } else {
                                echo JText::_('PROGRESS_SELECT_OPTION');
                                $finalGrade_course = "";
                            } ?>


                        </div>
                        <ul class="ulGradeSelected" style="display: none;">
                            <?php
                            //check grade numeric or letter
                            foreach ($grade_letter_course as $grade) {
                                ?>
                                <li value="<?php echo $grade['value']; ?>" <?php if ($this->mods_student['finalgrade'] == $grade['value']) { ?> selected="selected" <?php } ?>>
                                    <p><?php echo $grade['name']; ?></p>
                                </li>
                                <?php
                            }
                            ?>
                            <li value="-1">
                                <p>-</p>
                            </li>
                            <input type="hidden" id="grade_course_value" name="grade_course_value"
                                   data="<?php echo $finalGrade_course; ?>" value="<?php echo $finalGrade_course; ?>"/>
                        </ul>
                    </div>
                <?php } ?>

            </div>
        </div>
        <div class="completiondate"><?PHP
            $timemax = array();
            foreach ($this->mods_student['sections'] as $mod) {
                $activities = $mod['mods'];
                foreach ($activities as $act) {
                    $timemax[] = $act['mod_completion_date'];
                }
            }
            if ($this->mods_student['timemodified'])
                $time = gmdate("d/m/Y", $this->mods_student['timemodified']);
            else $time = max($timemax);

            $usercomple = array();
            foreach ($this->studentscomplete as $student) {
                $usercomple[] = $student['username'];
            }
            if (in_array($users->username, $usercomple))
                echo '<b>' . JText::_('COM_JOOMDLE_COURSE_COMPLETION') . '</b>: ' . $time;
            else echo '<b>' . JText::_('COM_JOOMDLE_COURSE_COMPLETION') . '</b>: ' . 'Not completed';

            ?></div>
        <div class="feedback-content feedbackcourse">
            <span><?php echo JText::_('COM_JOOMDLE_GRADE_COMMENT'); ?></span>
            <textarea id="txtFeedback-course" style=" resize: none; overflow: hidden"
                      name="feedbackgrade_course"
                      rows="<?php echo $rows; ?>"
                      cols="<?php echo $cols; ?>"
                      placeholder="<?PHP echo JText::_('COM_JOOMDLE_FEEDBACK_PLACE'); ?>"><?php echo $this->mods_student['gradefeedback']; ?></textarea>
        </div>

        <div class="student-activity">
            <?php
            if (is_array($this->mods_student['sections'])) {

                foreach ($this->mods_student['sections'] as $mod) {

                    $activities = $mod['mods'];
                    /*
                     *
                     *  check activity support grade
                     *  1 - Assignment
                     *  2 - Checklist
                     *  3 - Face to face
                     *  4 - Lession
                     *  5 - Quiz
                     *  6 - Skillsoft
                     *  7 - Scorm
                     *  8 - Workshop
                     *  check if support will be show grade and comment with facilitator
                     *  student not show grade and comment only show status
                     *
                     *  grade item *
                     */
                    ?>
                    <div class="joomdle_course_progress <?php echo 'sec_' . $sec; ?>">
                        <span class="progress-section-name"><?php echo $mod['sectionname']; ?></span>
                        <div class="progress-activities">
                            <?php
                            $i = 0;
                            foreach ($activities as $act) {

                                $mtype = JoomdleHelperSystem::get_mtype($act['mod']);
                                if (!$mtype) // skip unknow modules
                                    continue;
                                if (($act['mod'] == 'forum') || ($act['mod'] == 'certificate') || ($act['mod'] == 'questionnaire')) {
                                    continue;
                                }
                                $icon_url = JoomdleHelperSystem::get_icon_url($act['mod'], $act['type']);
//                                if (!isset($act['completionusegrade']) || $act['completionusegrade'] == 0) {
//                                    continue;
//                                }

                                if ($act['mod'] == 'quiz') {
//                                    $this->quizes = JoomdleHelperContent::call_method('get_course_quizes', $id, $users->username);
                                    
//                                    foreach ($this->quizes as $key => $value) {
//                                        foreach ($value['quizes'] as $k => $v) {
//                                            if ($v['id'] != $act['id']) continue; else $this->quiz = $v;
//                                        }
//                                    }
                                    $intro_quiz = $this->quiz['intro'];
                                    $attempt_quiz = $act['attempt_quiz'];
                                    $numOfAttempts = $this->quiz['numOfAttempts'];
                                    $attempts = json_decode($this->quiz['attempts'], true);
                                    
                                    $quizFinished = $act['quizFinished'];
                                    $firstAttempt = $act['firstAttempt'];
                                    $inProgress = $act['inProgress'];
//                                    $reAttempt = false;
//                                    if (!empty($attempts)) {
//                                        if (count($attempts) == 1) $firstAttempt = true;
//                                        foreach ($attempts as $key => $value) {
//                                            if ($value['state'] != "finished") {
//                                                $quizFinished = false;
//                                                $inProgress = true;
//                                            }
//                                        }
//                                        if (count($attempts) != $numOfAttempts) {
//                                            $quizFinished = false;
//                                            if (!$inProgress) $reAttempt = true;
//                                        }
//                                    } else {
//                                        $quizFinished = false;
//                                        $firstAttempt = true;
//                                    }

                                }
                                ?>
                                <div class="student-list-activities">
                                    <div class="activities">
                                        <?php
                                        if ($icon_url) {
                                            echo '<div class="activity-image ' . $mtype . '" >';
                                            echo '<img align="center" src="' . $icon_url . '"/>';
                                            echo '</div>';
                                        }
                                        ?>
                                        <div class="title-activity"><h5><?php echo $act['name']; ?></h5></div>

                                        <?php
                                      
                                        if ($act['mod'] == 'assign') {
//                                            $assign_info = JoomdleHelperContent::call_method('get_assignment_activity_detail', (int)$id, $users->username, $act['id']);
                                            if ($act['submission_status'] != 'Not Yet Submitted')
                                                echo '<div class="statuscompleted"></div>';
                                            else echo '<div class="status"></div>';
                                        } else if ($act['completionusegrade'])
                                            if ($act['mod_completion'] && $act['mod_completion_date'] && ($act['finalgradeletters'] != "Ungraded")) {
                                                echo '<div class="statuscompleted"></div>';
                                            } else echo '<div class="status"></div>';
                                        else
                                            if ($act['mod_completion'])
                                                echo '<div class="statuscompleted"></div>';
                                            else echo '<div class="status"></div>';
                                        ?>

                                        <!--<div class="time-completed-activity"><?php // echo date('d/m/Y',$mod['mod_completion_date']); ?></div>-->
                                        <!--<div class="time-completed-activity"><?php // echo $act['mod_lastaccess']; ?></div>-->
                                    </div>
                                    <div class="note">
                                        <?php if (($act['mod'] == 'quiz') && (!$act['completionusegrade'])) {
                                            echo 'Note: This assessment is non-gradable.';
                                        } ?>
                                    </div>
                                    <?php if (($act['mod'] == 'quiz') || ($act['mod'] == 'assign')) { ?>
                                        <div class="activitie-grades">
                                            <div class="grade-status">
                                                <div class="grade-title">
                                                    <span class="lable-grade"><?php echo JText::_('PROGRESS_GRADE'); ?></span>

                                                    <div class="select-grade-student">
                                                        <?php
                                                        // check if activity not support grade
                                                        // pls hide dropdown grade

                                                        // Note
                                                        // page not support grade
                                                        // assignment, assessment, scorm support grade
                                                        // scorm support grade but facilotator dont need set grade because leaner completed scorm when learner view scorm.
                                                        if ($act['gradeitemid'] && $act['gradingscheme'] == 1 && $act['mod'] == 'quiz') {
                                                            $k = 0;
                                                            $finalGrade = (float)$act['finalgrade'] * 10;
                                                            $grade_letters_ass = $act['lettergradeoption'];
                                                            // Get grade letter of assessment
                                                            if ($act['finalgradeletters'] != "Ungraded") {
                                                                foreach ($grade_letters_ass as $grade_ass) {
                                                                    if ($finalGrade >= $grade_ass['value']) {
                                                                        $finalgradeletters_ass = $grade_ass['name'];
                                                                        break;
                                                                    }
                                                                    else
                                                                         $finalgradeletters_ass = 'D';
                                                                }
                                                            }
                                                            if ($act['finalgradeletters'] == "Ungraded") {
                                                                $finalGrade = "";
                                                            }

                                                            ?>

                                                            <div class="gradeActivitySelect">
                                                                <div class="gradeSelected">

                                                                    <?php
                                                                    if ($act['hidden'] == 0) {
                                                                        if ($act['completionusegrade']) {
                                                                            if ($act['finalgrade'] > 0) {
                                                                                echo $finalgradeletters_ass;
                                                                            }
                                                                            else if($act['mod_completion'] && $act['finalgrade'] == 0) echo 'D';
                                                                            else {
                                                                                echo JText::_('PROGRESS_SELECT_OPTION');
                                                                            }
                                                                        } else  echo '-';
                                                                    } else  echo "-";
                                                                    ?>
                                                                </div>
                                                                <ul class="ulGradeSelected" style="display: none;">
                                                                    <?php
                                                                    //check grade numeric or letter
                                                                    foreach ($grade_letters_ass as $grade_ass) {
                                                                        ?>
                                                                        <li value="<?php echo $grade_ass['value']; ?>" <?php if ($finalgradeletters_ass == $grade_ass['name'] && $act['gradeid'] != 0) { ?> class="selected" <?php } ?>>
                                                                            <p><?php echo $grade_ass['name']; ?></p>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <li value="-1">
                                                                        <p>-</p>
                                                                    </li>
                                                                    <input type="hidden" class="grade-option"
                                                                           data="<?php echo $finalGrade; ?>"
                                                                           name="grade_value[]"
                                                                           value="<?php if ($act['hidden'] == 0) echo $finalGrade; else echo '-1' ;?>"/>
                                                                    <input type="hidden" class="status-option"
                                                                           name="status[]" value="<?php
                                                                    if (empty($act['mod_lastaccess']) && ($act['finalgradeletters'] == "Ungraded")) {
                                                                        echo "not yet started";
                                                                    } elseif ($act['mod_lastaccess'] && ($act['finalgradeletters'] == "Ungraded") && $act['mod_completion'] === false) {
                                                                        echo "in-progress";
                                                                    } elseif ($act['mod_completion'] && $act['mod_completion_date'] && ($act['finalgradeletters'] != "Ungraded")) {
                                                                        echo "completed";
                                                                    } ?>"
                                                                    />
                                                                </ul>

                                                            </div>

                                                            <input type="hidden" name="gradeitem[]"
                                                                   value="<?php echo $act['gradeitemid']; ?>"/>
                                                            <input type="hidden" name="gradeid[]"
                                                                   value="<?php echo $act['gradeid']; ?>"/>

                                                            <?php
                                                        }
                                                        if ($act['gradingscheme'] == 0 && $act['mod'] == 'quiz') {
                                                            if ($act['grademax'] == 100) $finalGrade = $act['finalgrade'];
                                                            else if ($act['grademax'] == 10) $finalGrade = $act['finalgrade'] * 10;

                                                            if ($act['finalgradeletters'] == "Ungraded") {
                                                                $finalGrade = "";
                                                            }

                                                            if ($act['finalgradeletters'] != "Ungraded") {
                                                                foreach ($grade_letter_course as $grade_assign) {
                                                                    if ($finalGrade >= $grade_assign['value']) {
                                                                        $finalgradeletters_quiz = $grade_assign['name'];
                                                                        break;
                                                                    }
                                                                }
                                                            }

                                                            ?>

                                                            <!--   <div class="message">-->
                                                            <!--     Turn off grading scheme-->
                                                            <!--   </div>-->
                                                            <div class="gradeActivitySelect">
                                                                <div class="gradeSelected" style="background:#e9e9e9;">

                                                                    <?php

                                                                    if ($act['finalgradeletters'] > 0)
                                                                        echo $finalgradeletters_quiz;
                                                                    else  echo '-';
                                                                    ?>
                                                                </div>
                                                                <ul class="ulGradeSelected hidden"
                                                                    style="display: none;">
                                                                    <?php
                                                                    //check grade numeric or letter
                                                                    foreach ($grade_letters_ass as $grade_ass) {
                                                                        ?>
                                                                        <li value="<?php echo $grade_ass['value']; ?>" <?php if ($finalgradeletters_ass == $grade_ass['name'] && $act['gradeid'] != 0) { ?> class="selected" <?php } ?>>
                                                                            <p><?php echo $grade_ass['name']; ?></p>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <li value="-1">
                                                                        <p>-</p>
                                                                    </li>
                                                                    <input type="hidden" class="grade-option"
                                                                           data="<?php echo $finalGrade; ?>"
                                                                           name="grade_value[]"
                                                                          value="<?php if ($act['hidden'] == 0) echo $finalGrade; else echo '-1' ;?>"/>
                                                                    <input type="hidden" class="status-option"
                                                                           name="status[]" value="<?php
                                                                    if (empty($act['mod_lastaccess'])) {
                                                                        echo "not yet started";
                                                                    } elseif ($act['mod_lastaccess'] && $act['mod_completion'] === false) {
                                                                        echo "in-progress";
                                                                    } else if ($act['mod_completion'] && $act['mod_completion_date']) {
                                                                        echo "completed";
                                                                    } ?>"

                                                                    />
                                                                </ul>


                                                            </div>

                                                            <input type="hidden" name="gradeitem[]"
                                                                   value="<?php echo $act['gradeitemid']; ?>"/>
                                                            <input type="hidden" name="gradeid[]"
                                                                   value="<?php echo $act['gradeid']; ?>"/>
                                                            <?php
                                                        }
                                                        if ($act['gradeitemid'] && $act['mod'] == 'assign') {
                                                            if ($act['grademax'] == 100) $finalGrade = $act['finalgrade'];
                                                            else if ($act['grademax'] == 10) $finalGrade = $act['finalgrade'] * 10;

                                                            if ($act['finalgradeletters'] == "Ungraded") {
                                                                $finalGrade = "";
                                                            }

                                                            if ($act['finalgradeletters'] != "Ungraded") {
                                                                foreach ($grade_letter_assign as $grade_assign) {
                                                                    if ($finalGrade >= $grade_assign['value']) {
                                                                        $finalgradeletters_assign = $grade_assign['name'];
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            ?>

                                                            <div class="gradeActivitySelect">
                                                                <div class="gradeSelected">

                                                                    <?php
                                                                    if ($act['hidden'] == 0) {
                                                                        if ($act['finalgradeletters'] != "Ungraded" && $act['finalgrade'] > 0) {
                                                                             echo $finalgradeletters_assign;
                                                                        } else {
                                                                            echo JText::_('PROGRESS_SELECT_OPTION');
                                                                        }
                                                                    } else  echo "-";
                                                                    ?>
                                                                </div>
                                                                <ul class="ulGradeSelected" style="display: none;">
                                                                    <?php
                                                                    //check grade numeric or letter
                                                                    foreach ($grade_letter_assign as $grade_assign) {
                                                                        ?>
                                                                        <li value="<?php echo $grade_assign['value']; ?>" <?php if ($act['finalgrade'] == $grade_assign['value'] && $act['gradeid'] != 0) { ?> class="selected" <?php } ?>>
                                                                            <p><?php echo $grade_assign['name']; ?></p>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <li value="-1">
                                                                        <p>-</p>
                                                                    </li>
                                                                    <input type="hidden" class="grade-option"
                                                                           data="<?php echo $finalGrade; ?>"
                                                                           name="grade_value[]"
                                                                           value="<?php if ($act['hidden'] == 0) echo $finalGrade; else echo '-1' ;?>"/>
                                                                    <input type="hidden" class="status-option"
                                                                           name="status[]" value="<?php
                                                                    if ($act['submission_status'] === 'Not Yet Submitted') {
                                                                        echo "not yet started";
                                                                    } elseif ($act['submission_status'] === 'In Progress') {
                                                                        echo "completed";
                                                                    } elseif ($act['submission_status'] == 'Submitted') {
                                                                        echo "completed";
                                                                    } ?>"
                                                                    />
                                                                </ul>

                                                            </div>
                                                            <input type="hidden" name="gradeitem[]"
                                                                   value="<?php echo $act['gradeitemid']; ?>"/>
                                                            <input type="hidden" name="gradeid[]"
                                                                   value="<?php echo $act['gradeid']; ?>"/>


                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <?php if ($act['mod'] == 'quiz' && $act['mod_completion_date']) {
                                                        if ($quizFinished)
                                                            ?>
                                                            <span class="attach-result" datalink = "<?php echo JUri::base() . 'quiz/' . (int)$id . '_' . $act['id'] . '_endquiz_' . $attempt_quiz . '.html'; ?>">
                                                            <image src="images/Assessmenticon.png" alt="icon"/></span>
                                                    <?php } elseif ($act['mod'] == 'assign' && $act['submission_status'] != 'Not Yet Submitted') {
                                                        ?>
                                                        <a class="downloadass"
                                                           href="<?php echo JRoute::_('index.php?option=com_joomdle&task=downloadSubmissionUser&userid=' . $users->username . '&course_id=' . $id . '&assignid=' . $act['id']); ?>">
                                                            <span class="attach-result-asign"> <image
                                                                        style="height:18px;"
                                                                        src="images/downloadassi.png"
                                                                        alt="icon"/></span>
                                                        </a>
                                                    <?php } ?>
                                                    <span class="time-completed-activity"><?php echo $act['mod_completion_date']; ?></span>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="feedback-content">
                                    <textarea id="txtFeedback"
                                              name="feedback<?php if ($act['gradeitemid']) { ?>grade<?php } ?>[]"
                                              rows="<?php echo $rows; ?>"
                                              cols="<?php echo $cols; ?>"
                                              placeholder="<?PHP echo JText::_('COM_JOOMDLE_FEEDBACK_PLACE'); ?>"><?php echo $act['feedback']; ?></textarea>
                                        </div>
                                    <?php } ?>
                                    <?php
                                    if ($act['mod'] == 'page') { ?>
                                        <?php
                                        //grade letter
                                        if ($act['finalgrade'] == null) {
                                        }
                                        ?>
                                        <input type="hidden" name="grade_value[]" value="">
                                        <input type="hidden" name="gradeitem[]"
                                               value="<?php echo $act['gradeitemid']; ?>"/>
                                        <input type="hidden" name="gradeid[]"
                                               value="<?php echo $act['gradeid']; ?>"/>
                                        <input type="hidden" class="status-option"
                                               name="status[]" value="<?php
                                        if ($act['mod_completion']) {
                                            echo "completed";
                                        } else {
                                            echo "not yet started";
                                        }
                                        ?>"
                                        />
                                        <div class="feedback-content hidden">
                                                    <textarea id="txtFeedback"
                                                              name="feedbackgrade[]"
                                                              rows="<?php echo $rows; ?>"
                                                              cols="<?php echo $cols; ?>"
                                                              placeholder="<?PHP echo JText::_('COM_JOOMDLE_FEEDBACK_PLACE'); ?>"><?php echo $act['feedback']; ?></textarea>
                                        </div>
                                    <?php }
                                    if ($act['mod'] == 'scorm') { ?>
                                        <?php
                                        //grade letter
                                        if ($act['finalgrade'] == null) {
                                        }
                                        ?>
                                        <input type="hidden" name="grade_value[]" value="">
                                        <input type="hidden" name="gradeitem[]"
                                               value="<?php echo $act['gradeitemid']; ?>"/>
                                        <input type="hidden" name="gradeid[]"
                                               value="<?php echo $act['gradeid']; ?>"/>
                                        <input type="hidden" class="status-option"
                                               name="status[]" value="<?php
                                        if ($act['mod_completion']) {
                                            echo "completed";
                                        } else {
                                            echo "not yet started";
                                        }
                                        ?>"
                                        />
                                        <div class="feedback-content hidden">
                                                    <textarea id="txtFeedback"
                                                              name="feedback<?php if ($act['gradeitemid']) { ?>grade<?php } ?>[]"
                                                              rows="<?php echo $rows; ?>"
                                                              cols="<?php echo $cols; ?>"
                                                              placeholder="<?PHP echo JText::_('COM_JOOMDLE_FEEDBACK_PLACE'); ?>"><?php echo $act['feedback']; ?></textarea>
                                        </div>
                                    <?php } ?>
                                    <input type="hidden" name="activite[]" value="<?php echo $act['id']; ?>"/>
                                    <input type="hidden" name="mode[]" value="<?php echo $act['mod']; ?>"/>
                                </div>
                                <?php

                                $i++;
                            }
                            ?>
                        </div>
                        <div id="somediv" class="somediv" title="this is a dialog" style="display:none;">
                            <div class="header">
                                <div class="closetitle" style="opacity: 1;">
                                    <img src="/images/<?php echo ($device == 'mobile') ? 'deleteIcon' : 'deleteiconm' ?>.png">
                                </div>
                                <?php
                                $banner = new HeaderBanner(new JoomdleBanner);
                                $render_banner = $banner->renderBanner($this->course_info, $this->mods_student['sections'], $role);
                                ?>

                                <iframe id="thedialog" width="100%" height="400px"></iframe>
                            </div>
                        </div>
                        <script>
                            var i = <?php echo $i;?>;
                            if (i == 0) {
                                jQuery('.sec_<?php echo $sec;?> .progress-activities').addClass('hide');
                            }
                        </script>
                    </div>
                    <?php
                    $sec++;
                }
            }
            ?>
            <div class="grade-button">
                <input type="hidden" name="username" value="<?php echo $users->username; ?>"/>
                <input type="hidden" name="courseid" value="<?php echo $this->courseid; ?>"/>
                <input type="hidden" name="coursegradeitemid"
                       value="<?php echo $this->mods_student['gradeitemid']; ?>"/>
                <input type="hidden" name="coursegradeid" value="<?php echo $this->mods_student['gradeid']; ?>"/>
                <input type="submit" value="<?php echo JText::_('COM_JOOMDLE_GRADE_EDIT_SAVE'); ?>"
                       class="button-right">
                <input type="button" onclick=""
                       value="<?php echo JText::_('COM_JOOMDLE_GRADE_EDIT_CANCEL'); ?>" class="button-right cancel">
            </div>
    </form>
</div>

<script type="text/javascript">

    (function ($) {
        $('.back').attr('onclick', '');

        mob = '<?php echo $mobile ?>';
        h = screen.height - 140;
        if (mob) {
            $(".attach-result").click(function () {
                window.location.href = $(this).attr("datalink");
            });
        }
        else {
            $(".attach-result").click(function () {
                $("#thedialog").attr('src', $(this).attr("datalink"));
                $("#somediv").css('display', 'block');
                $('body').css('overflow', 'hidden');
                $('body').addClass('overlay2');
                return false;
            });
        }
        $('#txtFeedback-course').each(function () {
            autosize(this);
        }).on('input', function () {
            autosize(this);
            $(this).keyup(function () {

                end = $(this).val();
                first = $(this).html()
                if (first !== end)
                    $('#student-grade').attr('data', 'has');
            });
        });
//        var ta = document.querySelector('#txtFeedback');
//        ta.addEventListener('focus', function(){
//          autosize(this);
//        });
        $('textarea').each(function () {

            autosize('#txtFeedback');
        }).on('input', function () {
            autosize(this);
            $(this).keyup(function () {

                end = $(this).val();
                first = $(this).html()
                if (first !== end)
                    $('#student-grade').attr('data', 'has');
            });
        });
        $('.gradeActivitySelect').each(function () {
            $(this).click(function () {
                $(this).find('.ulGradeSelected').toggle();
            });
        });

        $('.gradeCourseSelect').click(function () {
            $(this).find('.ulGradeSelected').toggle();
        });

        $('.gradeActivitySelect .ulGradeSelected li').each(function () {
            $(this).click(function () {
                first = $(this).parent('.ulGradeSelected').find('.grade-option').attr('data');
                end = $(this).attr('value');
                if (first !== end)
                    $('#student-grade').attr('data', 'has');
                $(this).parent('.ulGradeSelected').prev('.gradeSelected').html($(this).html());
                $(this).parent('.ulGradeSelected').find('.grade-option').val($(this).attr('value'));
                if ($(this).parent('.ulGradeSelected').find('.grade-option').val($(this).attr('value')) != null) {
                    $(this).parent('.ulGradeSelected').find('.status-option').val("completed");
                }
            });
        });

        $('.gradeCourseSelect .ulGradeSelected li').click(function () {
            $(this).parent('.ulGradeSelected').prev('.gradeSelected').html($(this).html());
            first = $('#grade_course_value').attr('data');
            end = $(this).attr('value');
            if (first !== end)
                $('#student-grade').attr('data', 'has');
            $('#grade_course_value').val($(this).attr('value'));
            $('#grade_course_value').addClass('gradeoffacilitator');
        });
        $(' .closetitle').click(function () {
            $("#thedialog").attr('src', '');
            $("#somediv").css('display', 'none');
            $('body').css('overflow', 'auto');
            $('body').removeClass('overlay2');
        });
        $('.cancel').click(function () {
            value = $('#student-grade').attr('data');
            if (value == 'has')
                lgtCreatePopup('confirm', {
                        'yesText': '<?php echo JText::_('COM_JOOMDLE_BUTTON_PROCEED'); ?>',
                        'content': '<?php echo '<b>' . JText::_('COM_JOOMDLE_CANCEL_GRADE_SAVE1') . '</b><br>' . JText::_('COM_JOOMDLE_CANCEL_GRADE_SAVE2'); ?>'
                    }, function () {
                        window.location.href = '<?php echo JUri::base().'/viewstudents/'.$this->courseid.'.html';?>';
                    }
                );
            else window.location.href = '<?php echo JUri::base().'/viewstudents/'.$this->courseid.'.html';?>';
        });
        $('.back').click(function () {
            value = $('#student-grade').attr('data');

            if (value == 'has')
                lgtCreatePopup('confirm', {
                        'yesText': '<?php echo JText::_('COM_JOOMDLE_BUTTON_PROCEED'); ?>',
                        'content': '<?php echo '<b>' . JText::_('COM_JOOMDLE_CANCEL_GRADE_SAVE1') . '</b><br>' . JText::_('COM_JOOMDLE_CANCEL_GRADE_SAVE2'); ?>'
                    }, function () {
                        window.location.href = '<?php echo JUri::base().'/viewstudents/'.$this->courseid.'.html';?>';
                    }
                );
            else window.location.href = '<?php echo JUri::base().'/viewstudents/'.$this->courseid.'.html';?>';
        });

        $('textarea').each(function () {
            var txt_height = jQuery(this)[0].scrollHeight;
        
            if (txt_height > 70) {
                jQuery(this).height( txt_height );
            }
        });
        
    })(jQuery);
</script>