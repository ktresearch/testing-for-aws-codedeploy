<?php

defined('_JEXEC') or die;

class UserapiController extends JControllerLegacy 
{
        public function display($cachable = false, $urlparams = false)
	{
              
		JRequest::setVar('view', 'userapi'); // force it to be the search view
                
               
		return parent::display($cachable, $urlparams);
	}
    
        public function signup () {
            $jinput = JFactory::getApplication()->input;
            
            $model = $this->getModel('Userapi');
            // set header
            $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));
            
            $first_name = $jinput->get('firstname', '', 'POST');
            $last_name = $jinput->get('lastname', '', 'POST');
            $email = $jinput->get('email', '', 'POST');
            $password = $jinput->get('password', '', 'POST');
            $repassword = $jinput->get('repassword', '', 'POST');
            if(empty($first_name) || empty($last_name)) {
                $model->setResponseMessage(false, 'First name and last name can not be blank.');
                exit;
            }
            // check email address
            if(empty($email)) {
                $model->setResponseMessage(false, 'Email can not be blank.');
                exit;
            }
            // check email already exits
            if($model->checkEmailExits($email)) {
                $model->setResponseMessage(false, 'Email already exits. Please use other email for register.');
                exit;
            }
            if(empty($password) || empty($repassword)) {
                $model->setResponseMessage(false, 'Password and Verify password can not be blank.');
                exit;
            }
            // check repassword match or not
            if($repassword !== $password) {
                $model->setResponseMessage(false, 'Verify password does\'t match.');
                exit;
            }
            
            $username = trim($email);
            // set Obj user
            $tmpUser = new stdClass();
            $tmpUser->username = $username;
            $tmpUser->name = $first_name.' '. $last_name;
            $tmpUser->email = $email;
            $tmpUser->password = (string) $password;
            
            // check username already exits
            if($model->checkUsernameExits($tmpUser->username)) {
                $model->setResponseMessage(false, 'Username already exits. Please use other username for register.');
                exit;
            }
            
            $user = $this->createUser($tmpUser);
            
            if($user) {
                $model->sendEmail('registration', $user, $password);
                $model->setResponseMessage(false, 'User Register failed. Please try again.');
                exit();
            } else {
                $model->setResponseMessage(true, 'User successfully saved.');
                exit();
            }
        }
        
        public function login_social() {
            $jinput = JFactory::getApplication()->input;
            
            $model = $this->getModel('Userapi');
            // set header
            $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));
            
            $apptype = $jinput->get('appType', '', 'POST');
            $apptoken = $jinput->get('appToken', '', 'POST');
            $appid = $jinput->get('appID', '', 'POST');
            
            $social_user = $this->verifySosocalPlugin($apptype, $apptoken, $appid);
            if($social_user) {
                echo json_encode($social_user);
                exit;
            } else {
                $model->setResponseMessage(false, 'Login social failed. Please try again.');
                exit;
            }
            
        }
        
        public function verifySosocalPlugin($apptype, $apptoken, $appid) {
            $model = $this->getModel('Userapi');
            // set header
            $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));
            switch ($apptype) {
                case 'FB':
                    $url = 'https://graph.facebook.com/me?access_token='.$apptoken;
                    $postreturnvalues = $model->curlOpt($url);
                    $facebookuser = json_decode($postreturnvalues);
                    if(empty($facebookuser)) {
                        $model->setResponseMessage(false, $postreturnvalues);
                        exit;
                    }
                    $id = $facebookuser->id;
                    if($facebookuser->email) {
                     $useremail = $facebookuser->email;
                    } else {
                      $useremail = 'fb_account@facebook.com';
                    }
                    if($facebookuser->first_name || $facebookuser->last_name) {
                        $fullname = $facebookuser->first_name .' '.$facebookuser->last_name;
                    } else {
                        $fullname = $facebookuser->name;
                    }
                    break;
                case 'GG':                    
                    $url = 'https://www.googleapis.com/plus/v1/people/me?access_token='.$apptoken.'&alt=json';
                    $postreturnvalues = $model->curlOpt($url);
                    $postreturnvalues = json_decode($postreturnvalues);
                    if($postreturnvalues->error) {
                        $model->setResponseMessage(false, $postreturnvalues->error->errors[0]->message);
                        exit;
                    }
                    foreach($postreturnvalues->emails as $googleemail) {
                            if($googleemail->type == "account") {
                                $useremail = $googleemail->value;
                            }
                    }
                    $useremail = $postreturnvalues->emails[0]->value;
                    $id = $postreturnvalues->id;
                    $fullname = 'google_account';
                    if(isset($postreturnvalues->name)) {
                        $fullname = $postreturnvalues->name->familyName .' '. $postreturnvalues->name->givenName;
                    }
        //            $url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?acces_token='.$apptoken;
                    break;
                case 'IN':
                    $url = 'https://api.linkedin.com/v1/people/~:(first-name,last-name,email-address,location:(name,country:(code)))?oauth2_access_token='.$apptoken.'&format=json';
                    $postreturnvalues = $model->curlOpt($url);
                    $linkedinuser = json_decode($postreturnvalues);
                    if($linkedinuser->status == 401) {
                        $model->setResponseMessage(false, 'Unable to verify access token');
                        exit;
                    }
                    $useremail = $linkedinuser->emailAddress;
                    $id = $linkedinuser->id;
                    $fullname = $linkedinuser->firstName. ' '. $linkedinuser->lastName;
                    break;
            }
            if( isset($id) && $id != $appid )//Check if this auth token has the same ID sent.
            {
                $model->setResponseMessage(false, 'Invalid ID Sent');
                exit; 
            } else {
               $username = trim($appid);
                // set Obj user
               $tmpUser = new stdClass();
               $tmpUser->username = $username;
               $tmpUser->name = $fullname;
               $tmpUser->email = $useremail;
               $tmpUser->password = (string) $appid; 
               
               // check username already exits
               // if user already exits. Login user
               if($model->checkUsernameExits($tmpUser->username)) {
                   // TODO: remove message
                   // Implement login user
                    $userDetail = $this->doLoginUser((array)$tmpUser);
               } else {
                    $user = $this->createUser($tmpUser);
                    if($user) {
                      $userDetail = $this->doLoginUser((array)$tmpUser);
                    }
               }
               return $userDetail;
            }
        }
        
        // login google for android app
        public function login_google() {
            $jinput = JFactory::getApplication()->input;
            
            $model = $this->getModel('Userapi');
            // set header
            $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));
            
            $appid = $jinput->get('appID', 'STRING', 'POST');
            $apptype = $jinput->get('appType', 'STRING', 'POST');
            $email = $jinput->get('email', 'STRING', 'POST');
            $fullname = $jinput->get('fullname', 'STRING', 'POST');
            
            if(empty($appid)) {
                $model->setResponseMessage(false, 'App id can not be empty.');
                exit;
            }
            if(empty($email)) {
                $model->setResponseMessage(false, 'Email can not be empty.');
                exit;
            }
            if(empty($fullname)) {
                $model->setResponseMessage(false, 'Fullname can not be empty.');
                exit;
            } else {
                $username = trim($appid);
                // set Obj user
                $tmpUser = new stdClass();
                $tmpUser->username = $username;
                $tmpUser->name = $fullname;
                $tmpUser->email = $email;
                $tmpUser->password = (string) $appid; 
                if($model->checkUsernameExits($tmpUser->username)) {
                   // TODO: remove message
                   // Implement login user
                    $userDetail = $this->doLoginUser((array)$tmpUser);
               } else {
                   $user = $this->createUser($tmpUser);
                   if($user) {
                      $userDetail = $this->doLoginUser((array)$tmpUser);
                   }
               }
               if($userDetail) {
                echo json_encode($userDetail);
                exit;
               }
            }
        }
        
        public function createUser($tmpUser) {
            
            $user = new JUser;
            $authorize = JFactory::getACL();
            $usersConfig = JComponentHelper::getParams('com_users');
            $userObj = get_object_vars($tmpUser);

            // Get usertype from configuration. If tempty, user 'Registered' as default
            $newUsertype = $usersConfig->get('new_usertype');

            if (!$newUsertype) {
                $newUsertype = 'Registered';
            }
            $date = JFactory::getDate();
            
            $user->set('id', 0);
            $user->set('usertype', $newUsertype);
            $user->set('gid', ($newUsertype));

            //set group for J1.6
            $user->set('groups', array($newUsertype => $newUsertype));
            
            $user->set('registerDate', $date->toSql());
            //Jooomla 3.2.0 fix. TO be remove in future
            if (version_compare(JVERSION, '3.2.0', '=')) {
                $salt = JUserHelper::genRandomPassword(32);
                $crypt = JUserHelper::getCryptedPassword($userObj['password'], $salt);
                $password = $crypt . ':' . $salt;
            } else {
                // Don't re-encrypt the password
                // JUser bind has encrypted the password
                $password = $userObj['password'];
            }
            
            $user->set('password', $password);
            
            // Bind the data.
            if (!$user->bind($userObj)) {
                $this->setError($user->getError());

                return false;
            }
            // Load the users plugin group.
            JPluginHelper::importPlugin('user');
            
            if (!$user->save()) {
                return $user;
            } else {
                return false;
            }
        }
        
        public function doLoginUser($user) {
            // do something
            $mainframe = JFactory::getApplication('site');
            $options = array ( 'skip_joomdlehooks' => '1', 'silent' => 1);
            $credentials = array ( 'username' => $user['username'], 'password' => $user['password']);
            if ($mainframe->login( $credentials, $options )) {
                $dispatcher = JDispatcher::getInstance();
                JPluginHelper::importPlugin('joomdlehooks');
                $option = array();
                $dispatcher->trigger('onUserLogin', array($user, $option));
            }
            
            jimport('joomla.user.helper');
            $user_id = JUserHelper::getUserId($user['username']);

            $model = CFactory::getModel('profile');
            CFactory::setActiveProfile($user_id);
            $users = CFactory::getUser($user_id);
            $profile = $model->getViewableProfile($user_id);
            $groupmodel = CFactory::getModel('groups');
            $notifModel = CFactory::getModel('notification');
            $userParams   = $users->getParams();

            $result = array();
            $result['user_community_id'] = $users->id;
            $result['username'] = $users->username;
            $result['fullname'] = $users->name;
            $result['profileimageurl'] = $users->getAvatar();
            $result['profilecoverurl'] = $users->getCover();
            $result['status'] = $users->getStatus();
            $result['interest'] = (string) $profile['fields']['Other Information'][0]['value'];
            $result['alias'] = $users->_alias;

            $result['notifycount'] = $notifModel->getNotificationCount($user_id,'0',$userParams->get('lastnotificationlist',''));

            $result['groupscount'] = $groupmodel->getGroupsCount($profile['id']);
            $result['groups'] = $users->_groups;
            $result['birthday'] = (string) $profile['fields']['Basic Information'][1]['value'];
            
            require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
            $course_inprogress = JoomdleHelperContent::call_method('user_course_inprogress', $users->username);
            $response = array();
            $response['status'] = true;
            $response['user'] = $result;
            $response['course_inprogress'] = $course_inprogress;
            return $response;
        }
        
        public function login() {
            $jinput = JFactory::getApplication()->input;
            
            $model = $this->getModel('Userapi');
            // set header
            $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));
            
            $username = $jinput->get('username', 'STRING', 'POST');
            $password = $jinput->get('password', 'STRING', 'POST');
            
            $username = (string) $username;
            $password = (string) $password;
            
            if($username == "") {
                $model->setResponseMessage(false, 'User is blank');
                exit;
            }
            if($password == "") {
                $model->setResponseMessage(false, 'Password is blank');
                exit;
            } 
            else {
                $user = array();
                $user['username'] = $username;
                $user['password'] = $password;
                $userdetail = $this->doLoginUser($user);
                if($userdetail) {
                    echo json_encode($userdetail);
                    exit;
                } else {
                    $model->setResponseMessage(false, 'Login failed. Please try again.');
                    exit;
                }
            }
        }
        
        public function logout() {
            $jinput = JFactory::getApplication()->input;
            
            $model = $this->getModel('Userapi');
            // set header
            $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));
            
            $username = $jinput->get('username', 'STRING', 'POST');
            $password = $jinput->get('password', 'STRING', 'POST');
            
            $username = (string) $username;
            $password = (string) $password;
            
            $user = JFactory::getUser();
            if($user->id == 0) {
                $model->setResponseMessage(true, 'User already logout.');
                exit;
            }
            $this->doLogoutUser($user);
        }
        
        public function logout_social() {
            $mainframe = JFactory::getApplication('site');
            $jinput = JFactory::getApplication()->input;
            
            $model = $this->getModel('Userapi');
            // set header
            $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));
            
            $username = $jinput->get('username', 'STRING', 'POST');
            $password = $jinput->get('password', 'STRING', 'POST');
            
            $username = (string) $username;
            $password = (string) $password;
            $user = JFactory::getUser();
            if($user->id == 0) {
                $model->setResponseMessage(true, 'User already logout.');
                exit;
            }
            $this->doLogoutUser($user);
        }
        
        public function doLogoutUser($user) {
            $mainframe = JFactory::getApplication('site');
            $model = $this->getModel('Userapi');
            $options = array();
            if ($mainframe->logout( $user->id, $options )) {
                $dispatcher = JDispatcher::getInstance();
                JPluginHelper::importPlugin('joomdlehooks');
                $option = array();
                $dispatcher->trigger('onUserLogout', array($user, $option));
                $model->setResponseMessage(true, 'Logout successfull.');
                exit;
            }
        }
}

?>