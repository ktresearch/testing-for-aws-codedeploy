<?php 

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class UserapiModelUserapi extends JModelList
{
    public $header = [];
    
    /*
     * set header for response
     */
    public function setHeader($header) {
        if (is_array($header)){
            foreach ($header as $v) {
                @header($v);
            }
        } else {
            @header($header);
        }
    }
    /*
     * send response message
     */
    public function setResponseMessage($status, $message) {
        echo json_encode(array(
            'status' =>$status,
            'error_message' => $message
        ));
    }


    public function prePareResponse($response) {
        
    }
    /*
     * implement get data
     */
    public function curlOpt($url) {
        $ch = curl_init();
        $request_headers = array();
        $request_headers[] = 'Content-Type: application/json';
        $request_headers[] = 'Connection: Keep-Alive';
        $request_headers[] = 'x-li-format: json';
        
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $postreturnvalues = curl_exec($ch);
        curl_close($ch);
        return $postreturnvalues;
    }
    
    public function checkEmailExits($email) {
        $db = $this->getDBO();
        $found = false;

        $query = 'SELECT ' . $db->quoteName('email');
        $query .= ' FROM ' . $db->quoteName('#__users');
        $query .= ' WHERE UCASE(' . $db->quoteName('email') . ') = UCASE(' . $db->Quote($email) . ')';

        $db->setQuery($query);
        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }
        $result = $db->loadObjectList();
        $found = (count($result) == 0) ? false : true;
        return $found;
    }
    public function checkUsernameExits($username) {
        $db = $this->getDBO();
        $found = false;

        $query = 'SELECT ' . $db->quoteName('username');
        $query .= ' FROM ' . $db->quoteName('#__users');
        $query .= ' WHERE UCASE(' . $db->quoteName('username') . ') = UCASE(' . $db->Quote($username) . ')';

        $db->setQuery($query);
        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }
        $result = $db->loadObjectList();
        $found = (count($result) == 0) ? false : true;
        return $found;
    }
    
    public function sendEmail($type, $user, $password) {
        $mainframe = JFactory::getApplication();
        $sitename = $mainframe->getCfg('sitename');
        $mailfrom = $mainframe->getCfg('mailfrom');
        $fromname = $mainframe->getCfg('fromname');
        $siteURL = JURI::base();
        
        $name = $user->get('name');
        $email = $user->get('email');
        $username = $user->get('username');
        
        if (is_null($password)) {
            $password = $user->get('password');
        }

        //Disallow control chars in the email
        $password = preg_replace('/[\x00-\x1F\x7F]/', '', $password);

        $params = JComponentHelper::getParams('com_users');
        if ($params->get('sendpassword', 1) == 0) {
            $password = '***';
        }
        
        $rows = $this->getAdministratorEmail();
        //getting superadmin email address.
        if (!$mailfrom || !$fromname) {
            foreach ($rows as $row) {
                if ($row->sendEmail) {
                    $fromname = $row->name;
                    $mailfrom = $row->email;
                    break;
                }
            }

            //if still empty, then we just pick one of the admin email
            if (!$mailfrom || !$fromname) {
                $fromname = $rows[0]->name;
                $mailfrom = $rows[0]->email;
            }
        }

        $subject = JText::sprintf('COM_COMMUNITY_ACCOUNT_DETAILS_FOR', $name, $sitename);
        $subject = html_entity_decode($subject, ENT_QUOTES);
        
        $sendashtml = false;
        $copyrightemail = JString::trim($mainframe->getCfg('copyrightemail'));
        foreach ($rows as $row) {
                if ($row->sendEmail) {
                    $message2 = JText::sprintf(
                        JText::_('COM_COMMUNITY_SEND_MSG_ADMIN'),
                        $row->name,
                        $sitename,
                        $name,
                        $email,
                        $username
                    );

                    $message2 = html_entity_decode($message2, ENT_QUOTES);

                    //check if HTML emails are set to ON
                    if ($config->get('htmlemail')) {
                        $sendashtml = true;
                        $tmpl = new CTemplate();
                        $message2 = CString::str_ireplace(array("\r\n", "\r", "\n"), '<br />', $message2);

                        $tmpl->set('name', $row->name);
                        $tmpl->set('email', $row->email);

                        $message2 = $tmpl->set(
                            'unsubscribeLink',
                            CRoute::getExternalURL('index.php?option=com_community&view=profile&task=privacy'),
                            false
                        )
                            ->set('recepientemail', $row->email)
                            ->set('content', $message2)
                            ->set('copyrightemail', $copyrightemail)
                            ->set('sitename', $config->get('sitename'))
                            ->fetch('email.html');
                    }
                    $mail = JFactory::getMailer();
                    $mail->sendMail($mailfrom, $fromname, $row->email, $subject, $message2, $sendashtml);
                }
            }
    }
    
    public function getAdministratorEmail()
    {
        $db = $this->getDBO();

        $query = 'SELECT a.' . $db->quoteName('name') . ', a.' . $db->quoteName('email') . ', a.' . $db->quoteName('sendEmail')
                . ' FROM ' . $db->quoteName('#__users') . ' as a, '
                . $db->quoteName('#__user_usergroup_map') . ' as b'
                . ' WHERE a.' . $db->quoteName('id') . '= b.' . $db->quoteName('user_id')
                . ' AND b.' . $db->quoteName('group_id') . '=' . $db->Quote(8);

        $db->setQuery($query);

        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }

        $result = $db->loadObjectList();
        return $result;

    }
}

?>