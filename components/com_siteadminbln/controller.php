<?php 
/*
 *  Com admin for Manager BLN
 *  $package: siteadminbln
 *  $author: Kydon vietnam team
 *  $date: 22 Feb 2018
 */
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

class SiteadminblnController extends JControllerLegacy {

    public function display($cachable = false, $urlparams = false)
    {
        JFactory::require_login();
        //document object
        $jdoc = JFactory::getDocument();
        //add the stylesheet
        $jdoc->addStyleSheet(JURI::root ().'components/com_siteadminbln/css/siteadminbln.css');
        
        // Make sure we have a default view
        if( !JRequest::getVar( 'view' )) {
		    JRequest::setVar('view', 'siteadminbln' );
        } else {
		$view = JRequest::getVar( 'view' );
		JRequest::setVar('view', $view );
        }

        $mainframe =JFactory::getApplication();
        $document  =JFactory::getDocument();
        $pathway   = $mainframe->getPathway();

        return parent::display($cachable, $urlparams);
    }
    public function account($cachable = false, $urlparams = false) {

        JRequest::setVar('view', 'account');
        $document = JFactory::getDocument();
        $document->addStyleSheet(JURI::root(true) . '/components/com_siteadminbln/css/account.css');
        return parent::display($cachable, $urlparams);
    }

    public function assignRoleAdmin() {
        require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
        $jinput = JFactory::getApplication()->input;
        $userid = $jinput->post->get('uid', 0, 'INT');
        $role = $jinput->post->get('role', '', 'STRING');

        $model = $this->getModel('account');

        $error = 0;
        $response = [];
        if ($userid) {
            switch ($role) {
                case 'ow':
                    if ($model->hasPermission('assigningBLNOwner')) {
                        $r = $model->assignBLNCircleOwner($userid);
                        if ($r) {
                            $cUser = CFactory::getUser($userid);
                            $response['name'] = $cUser->getDisplayName();
                            $response['email'] = $cUser->username;
                        } else $error = 1;
                    } else $error = 1;

                    break;
                case 'ad':
                    if ($model->hasPermission('assigningBLNAdmin')) {
                        $r = $model->assignBLNCircleAdmin($userid);
                        if ($r) {
                            $cUser = CFactory::getUser($userid);
                            $response['name'] = $cUser->getDisplayName();
                            $response['email'] = $cUser->username;
                        } else $error = 1;
                    } else $error = 1;
                    break;
                default:
                    $error = 1;
                    break;
            }
        } else {
            $error = 1;
        }
        $result = array();
        $result['error'] = $error ? 1 : 0;
        $result['comment'] = $error ? 'Fail.' : 'Successfully..' ;
        $result['data'] = $response;
        echo json_encode($result);
        die;
    }
    public function editwriteup() {
        $jinput = JFactory::getApplication()->input;
        $writeupContent = $jinput->post->get('content', '', 'STRING');

        $model = $this->getModel('account');

        $error = 0;
        $comment = '';
        $response = '';
        if ($model->hasPermission('managingLoginPage')) {
            if ($writeupContent) {
                if (!$model->editWriteup($writeupContent)) {
                    $error = 1;
                    $comment = 'There\'s something wrong with saving to database.';
                }
            } else {
                $error = 1;
                $comment = 'Empty Textarea.';
            }
        } else {
            $error = 1;
            $comment = 'You don\'t have permission to do this action.';
        }

        $result = [
            'error' => $error,
            'comment' => $comment,
            'data' => $response
        ];
        echo json_encode($result);
        die;
    }
    public function editbanner() {
        $jinput = JFactory::getApplication()->input;
        $logoUpload = $jinput->files->get('logoUpload');
        $logoFileName = $jinput->post->get('logoFileName', '', 'STRING');
        $bannerUpload = $jinput->files->get('bannerUpload');
        $bannerFileNames = $jinput->post->get('bannerFileName', [], 'ARRAY');

        $logoData = ['logoUpload' => $logoUpload, 'logoFileName' => $logoFileName];
        $bannersData = ['bannerUpload' => $bannerUpload, 'bannerFileNames' => $bannerFileNames];

        $model = $this->getModel('account');

        if ($model->hasPermission('managingLoginPage')) {

            $model->handleImages('logo', $logoData);

            if (!empty($bannerUpload)) {
                $model->handleImages('banner', $bannersData);
            }
        }

        JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_siteadminbln&view=account&action=mlp', false));
    }
    public function getlicenes(){
        $mainframe = JFactory::getApplication();
        $jinput = $mainframe->input;
        $model = $this->getModel('account');

        $limitstart = $jinput->get('limitstart', '', 'INT');
        $limit = $jinput->get('limit', '', 'INT');
        $response = $model->subIncrease($limit,$limitstart);
        foreach ($response as $row) {
            $html.= '<tr class = "datatbl dataless"><td>'.date('d/m/Y', strtotime($row->date_start)) . ' - ' . date('d/m/Y', strtotime($row->date_end))
                .'</td><td>'.$row->new_total.'</td></tr>';
        }
        $sl = count($response)+$limitstart;
        $return = new stdClass();
        $return->html = $html;
        $return->count = $sl;
        echo json_encode($return);
        die;
    }
}
?>