<?php
    $mainframe = JFactory::getApplication();

    $session = JFactory::getSession();
    $device = $session->get('device');
    if ($device == 'mobile') $dev = 1;
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.js"></script>
<div class="sub-page subpage ">
    <div class="header-subpage center">
        <span class="back-icon" onclick="goBackAdmin();">
        </span>
        <span class="title">
            <?php echo JText::_('COM_SITEADMINBLN_SUBCRIPTION'); ?>
        </span>
    </div>
    <div class="total-box">
        <form class="form-checkout" action="<?php echo JRoute::_('index.php?option=com_siteadminbln&view=subscription&action=checkout'); ?>" method="post" >
            <div class="editBox">
                <button type="button" class="btnedit hide"><?php echo JText::_('COM_SITEADMINBLN_SUB_EDIT') ?></button>
                <p class="title"><?php echo JText::_('COM_SITEADMINBLN_SUB_TOTAL') ?></p>
                <div class="totaluser">
                    <div class="titlenew hide"><?php echo JText::_('COM_SITEADMINBLN_SUB_CURRTOTAL'); ?> </div><div class="valuenew"><?php echo $this->info->total_purchased ?></div>
                </div>
            </div>
            <div class="clear"></div>
            <div class = "updateTotal hide">
                <div class="totalTitle" ><?php echo JText::_('COM_SITEADMINBLN_SUB_NEW_TOTAL') ?>
                    <span class="note">*</span> 
                    <span class="newtotal"><input type="text" name="newtotal" class="total" ></span>
                </div>
                <div>
                    <div class="btupdate"><button type="button" class="btnupdate"><?php echo JText::_('COM_SAVE_BLN') ?></button></div>
                    <div class="btcancel"><button type="button" class="btncancel"><?php echo JText::_('COM_SITEADMINBLN_BUTTON_CANCEL') ?></button></div>
            </div>
            </div>
        </form>
    </div>
    <div class="clear"></div>
    <div class="chart-box">
        <div>
        <p class="title"><?php echo JText::_('COM_SITEADMINBLN_SUB_CURRENT') ?></p>
            <p class="valuedate">
        <p class="valuedate"><?php
                if ($this->inscrease->date_created){
                    $date = date_create($this->inscrease->date_end);
                    date_modify($date, "+1 days");
                    echo 'Renews on ' . date('d/m/Y', strtotime(date_format($date, "Y-m-d"))) . ':';
                }
            else {

                $date = date_create($this->info->start_date);
                date_modify($date, "+1 days");
                echo 'Renews on ' . date('d/m/Y', strtotime(date_format($date, "Y-m-d"))) . ':';
            }
            ?>
            <span class="total_purchased"><?php echo ($this->new ) ? $this->inscrease->total_new : $this->info->total_purchased ?></span>
        </p>

        </div>
        <div class="tbLicenes">
            <table class="table-bordered ">
                <tr class="tbTitle">
                    <th width = "50%"><?php echo JText::_('COM_SITEADMINBLN_SUB_DATE'); ?></th>
                    <th><?php echo JText::_('COM_SITEADMINBLN_SUB_SUBSCRIPTION');?></th>
                </tr>
                <?php
//                    var_dump($this->allInscrease);die;

                foreach ($this->allInscrease as $row) {
                    ?>
                    <tr class="datatbl">
                        <td><?php echo date('d/m/Y', strtotime($row->date_start)) . ' - ' . date('d/m/Y', strtotime($row->date_end)); ?></td>
                        <td><?php echo $row->new_total; ?></td>
                    </tr>
                <?php } ?>

            </table>
            <?php
            if (count($this->allInscrease) < $this->countIN) {
                echo '<div class="membersSeeMore btSeeMore">' . strtoupper(JText::_('COM_COMMUNITY_SEE_MORE')) . '</div>';
            }
            ?>
            <div class="membersSeeLess btSeeLess hidden"><?php echo JText::_('COM_COMMUNITY_SEE_LESS'); ?> </div>
        </div>
        <div class="chartSub">
            <div class="circleUser">
                <canvas id="doughnut" width="290" height="290"></canvas>
            </div>
            <img class="hidden" width = "30" height="30" id="addimg" src ="images/Friends_1x.png" alt='minh' >
        </div>
    </div>
    <div class="note-box">
            <p><?php echo JText::_('COM_SITEADMINBLN_SUB_NOTE_EMAIL_FIRSR') ?>
                <a>business@parenthexis.com</a>
                <?php echo JText::_('COM_SITEADMINBLN_SUB_NOTE_EMAIL_END') ?>
            </p>
    </div>
</div>

<script>
    jQuery(document).ready(function () {
        jQuery('.btnedit').click(function () {
            jQuery('.updateTotal').removeClass('hide');
            jQuery('.titlenew').removeClass('hide');
            jQuery('.btnedit').addClass('hide');
        });
        jQuery('.form-checkout').submit(function (e) {
        });
        jQuery('.btnupdate').click(function () {
            var old = '<?php echo $this->info->total_purchased ?>';
            var add = jQuery('.updateTotal .total').val()- old;
    
            if (jQuery('.updateTotal .total').val()) {
                lgtCreatePopup('confirm', {
                    'yesText': '<?php echo JText::_('COM_SITEADMINBLN_SUB_PROCEED'); ?>',
                    'content': '<?php echo '<b>' . JText::_('COM_SITEADMINBLN_SUB_MESSAGE') . '</b><br>' . JText::_('COM_SITEADMINBLN_SUB_MESSAGE_1'); ?>'
                }, function () {
                    var total = jQuery('.newtotal .total').val();
                    var url = '<?php echo JURI::base() . 'index.php?option=com_siteadminbln&view=subscription&action=checkout&act=' ?>' + add;
                    jQuery.ajax({
                        url: '<?php echo JURI::base() . 'index.php?option=com_siteadminbln&view=subscription&action=checkout'; ?>',
                        data: {
                            total: add,
                        },
                        beforeSend: function () {
                            lgtCreatePopup('', {'content': 'Loading...'});
                        },
                        success: function (res) {
                            lgtRemovePopup();
                            if (jQuery.isNumeric(jQuery('.newtotal .total').val()) && jQuery('.newtotal .total').val() >= 0 ) {
                                window.location.href = url;
                                lgtRemovePopup();
                            } else
                                lgtCreatePopup('withCloseButton', {'content': 'Please input a positive value for "New total"'});
                        },
                        error: function (res) {
                            console.log(res);
                            lgtRemovePopup();
                        }
                    });
                }
                );
            } else
                lgtCreatePopup('withCloseButton', {'content': 'Please fill out “New total” field.'});
        });
        jQuery('.btncancel').click(function(){
            jQuery('.titlenew').addClass('hide');
            jQuery('.newtotal .total').val('');
            jQuery('.updateTotal').addClass('hide');
            jQuery('.btnedit').removeClass('hide');
        });
        jQuery('.membersSeeMore').click(function () {
            var length = jQuery('.tbLicenes .datatbl').length;


            jQuery('.membersSeeMore').hide();
            jQuery.ajax({
                url: '<?php echo JURI::base() . 'index.php?option=com_siteadminbln&task=getlicenes'; ?>',
                type: 'POST',
                data: {
                    'limitstart': length,
                    'limit': 5
                },
                beforeSend: function () {
//                    jQuery('.membersSeeMore').after('<div class="circlesLoading" style="display: none;">Loading...</div>');
//                    jQuery('.circlesLoading').fadeIn();
                },
                success: function (result) {
                    var res = JSON.parse(result);
                    console.log(res);
                    jQuery('.circlesLoading').fadeOut('400', function () {
                        jQuery('.circlesLoading').remove();
    });
                    jQuery('.tbLicenes table').append(res.html);
                    if (res.count < <?php echo $this->countIN ?>) {
                        jQuery('.membersSeeMore').fadeIn();
                    } else {
                        jQuery('.membersSeeMore').hide();
                        jQuery('.membersSeeLess').removeClass('hidden');
                    }
                },
                error: function (res) {
                    console.log(res);
                    lgtRemovePopup();
                }
            });

        });
        jQuery('.membersSeeLess').click(function () {
            jQuery('.tbLicenes .dataless').remove();
            jQuery('.membersSeeMore').fadeIn();
            jQuery('.membersSeeLess').addClass('hidden');
        });
    });
</script>

<script type="text/javascript">
    var device = '<?php echo $dev; ?>';
    Chart.types.Doughnut.extend({
        name: "doughnutAlt",
        draw: function () {
            Chart.types.Doughnut.prototype.draw.apply(this, arguments);

            var width = this.chart.width,
                    height = this.chart.height;

            var fontSize = (height / 114).toFixed(2);
            this.chart.ctx.font = 'normal 16px "Open Sans", "OpenSans", sans-serif';
            this.chart.ctx.textAlign = 'center';
            this.chart.ctx.textBaseline = 'middle';
            this.chart.ctx.fillStyle = '#484848';
            this.chart.ctx.save();
            this.chart.ctx.beginPath();

//            img = document.getElementById("addimg");
            var img = new Image();
            img.src = "/images/Friends_1x.png";
            
            if(device) this.chart.ctx.drawImage(img, 110, 65);
            else this.chart.ctx.drawImage(img, 120, 75);
            
            if(device) this.chart.ctx.fillText('<?php echo JText::_('COM_SITEADMINBLN_SUB_USED');?>', 130, 125);
            else this.chart.ctx.fillText('<?php echo JText::_('COM_SITEADMINBLN_SUB_USED');?>', 140, 135);
            this.chart.ctx.font = 'normal 40px "Open Sans", "OpenSans", sans-serif';
            this.chart.ctx.textAlign = 'center';
            this.chart.ctx.textBaseline = 'middle';
            this.chart.ctx.fillStyle = '#126DB6';
            this.chart.ctx.save();
            this.chart.ctx.beginPath();
            var text = '<?php echo $this->info->total_use; ?>';
            if(device) this.chart.ctx.fillText(text, 130, 155);
            else this.chart.ctx.fillText(text, 140, 165);
            this.chart.ctx.font = 'normal 16px "Open Sans", "OpenSans", sans-serif';
            this.chart.ctx.textAlign = 'center';
            this.chart.ctx.textBaseline = 'middle';
            this.chart.ctx.fillStyle = '#484848';
            this.chart.ctx.save();
            this.chart.ctx.beginPath();
            var text = '<?php echo JText::_('COM_SITEADMINBLN_SUB_UNUSED');?> ' + '<?php echo $this->info->total_empty; ?>'
            if(device) this.chart.ctx.fillText(text, 130, 195);
            else this.chart.ctx.fillText(text, 140, 210);
        }
    });

    var chartOptions = {
        onAnimationComplete: function () {
            // Always show Tooltip
            this.showTooltip(this.segments, true);
        },
        customTooltips: function (tooltip) {
            // Tooltip Element
            var tooltipEl = $('#chartjs-tooltip');
            // Hide if no tooltip
            if (!tooltip) {
                tooltipEl.css({
                    opacity: 1
                });
                return;
            }
            // Set caret Position
            tooltipEl.removeClass('above below');
            tooltipEl.addClass(tooltip.yAlign);
            tooltipEl.addClass(tooltip.xAlign);
            // Set Text
            tooltipEl.html(tooltip.text);
            // Find Y Location on page
            var top;
            if (tooltip.yAlign == 'above') {
                top = tooltip.y - tooltip.caretHeight - tooltip.caretPadding;
            } else {
                top = tooltip.y + tooltip.caretHeight + tooltip.caretPadding;
            }
            // Display, position, and set styles for font
            tooltipEl.css({
                opacity: 0,
                left: tooltip.chart.canvas.offsetLeft + tooltip.x + 'px',
                top: tooltip.chart.canvas.offsetTop + top + 'px',
                fontFamily: tooltip.fontFamily,
                fontSize: tooltip.fontSize,
                fontStyle: tooltip.fontStyle,
                xOffset: tooltip.xOffset,
            });
        },
        tooltipEvents: [], // Remove to enable Default Mouseover events
        tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
        tooltipFillColor: "rgba(0,0,0,0.0)",
        tooltipFontColor: "#505050",
        tooltipFontSize: 34,
        tooltipXOffset: 0,
        tooltipXPadding: 0,
        tooltipYPadding: 0,
//    tooltipTemplate: "<%= value %>%",
        legends: true,
        showTooltips: true,
        segmentShowStroke: false,
        percentageInnerCutout: 65,
        animationEasing: "easeInOutQuart",
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    }

    var data = [{
            value: <?php echo $this->info->total_use; ?>,
            color: "#126DB6",
            label: "Data 1"
        }, {
            value: <?php echo $this->info->total_empty; ?>,
            color: "#DBECF8",
            label: "Data 2"
        }, ];
    var doughnutChart = new Chart(document.getElementById("doughnut").getContext("2d")).doughnutAlt(data, chartOptions);
    // document.getElementById('doughnut-legend').innerHTML = doughnutChart.generateLegend();

//  var c = document.getElementById("doughnut");
//var ctx = c.getContext("2d");
//var grd = ctx.createLinearGradient(0, 0, 170, 0);
//grd.addColorStop(0, "black");
//grd.addColorStop(0.5, "red");
//grd.addColorStop(1, "white");
//ctx.fillStyle = grd;
//ctx.fillRect(20, 20, 150, 100);
</script>
