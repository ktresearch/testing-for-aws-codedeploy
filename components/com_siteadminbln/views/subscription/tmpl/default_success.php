<?php
if ($this->info->type_order == "monthly") {
    $end = mktime(0, 0, 0, date("m") + 1, date("d"), date("y"));
    $date_end = date('d/m/Y', $end);
} else {
    $end = mktime(0, 0, 0, date("m") + 1, date("d"), date("y") + 1);
    $date_end = date('d/m/Y', $end);
}

$newtotal = $this->info->total_subscription;
$totalm = $newtotal * 2;
$money = $totalm + $totalm * 0.07;
?>

<div class=" resultpage">
    <div class="headerpage">
        <p><?php
echo JText::_('COM_SITEADMINBLN_SUB_MESSAGE_SUCCESS1') . '<br>'
 . JText::_('COM_SITEADMINBLN_SUB_MESSAGE_SUCCESS2')
?></p>
        <button class="back-sub"><?php echo JText::_('COM_SITEADMINBLN_SUB_BACK') ?></button>
    </div>
    <div class="mainpage">
        <h1 class="title"><?php echo JText::_('COM_SITEADMINBLN_SUB_TITLE_SUCCSESS') ?></h1>
        <div class="order-box">
            <div class="contentorder">
                <span class="nameorder">Order</span>
                <span class="valueorder"><?php echo $this->info->number_order; ?></span>
            </div>
            <div class="dateorder">
                <span class="nameorder">Date</span>
                <span class="valueorder"><?php echo date("d/m/Y") ?></span>
            </div>
        </div>
        <div class="clear"></div>
        <div class="totalpur-box">
            <p class="title"><?php echo JText::_('COM_SITEADMINBLN_SUB_DIS_PAYMENT'); ?></p>
            <p class="value"><?php echo  $this->info->total_subscription; ?></p>
        </div>  
        <div class="typepur-box">
            <p class="title"><?php echo JText::_('COM_SITEADMINBLN_SUB_TYPE'); ?></p>
            <p class="value"><?php echo $this->info->type_order; ?></p>
        </div>  
        <div class="time-box">
            <p class="title"><?php echo JText::_('COM_SITEADMINBLN_SUB_PERIOD'); ?></p>
            <p class="time"><?php echo date("d/m/Y") . ' - ' . $date_end; ?></p>
            <p class="submoney">
                <span class="title">sub total</span> <span class="price">USD <?php echo number_format($totalm, 2) ;?></span>
            </p>
            <p class="submoney">
                <span class="title">tax</span> <span class="price">USD <?php echo number_format($totalm*0.07,2);?></span>
            </p>
        </div>
        <div class="checkout-box">
            <div class="line"></div>

            <div class = "checkout-total">
                <span class="total">Total &nbsp; &nbsp;</span>
                <span class="money"><?php echo 'USD' . str_replace(',', '.', $this->info->price_order); ?></span>
            </div>
            <div class="clear"></div>

        </div>
        <div class="payment-box">
            <span class="nameorder"><?php echo JText::_('COM_SITEADMINBLN_SUB_PAYMENT_MODE'); ?></span>
            <span class="valueorder"><?php echo $this->info->payment_method; ?></span>
        </div>
    </div>
</div>
<script>
    jQuery('.back-sub').click(function () {
        var url = '<?php echo JURI::base() . '?view=subscription' ?>';
        window.location.href = '<?php echo JURI::base() . 'siteadminbln?view=subscription&act=' . $this->info->id_sub_increase ?>';
    });
</script>