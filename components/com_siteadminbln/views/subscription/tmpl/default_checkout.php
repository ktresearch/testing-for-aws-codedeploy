<?php
$newtotal = JRequest::getVar('act', '');
$totalm = $newtotal * 2;
$money = $totalm+$totalm*0.07;
?>
<div class="sub-page subpage ">
    <div class="header-subpage center">
        <span class="back-icon" onclick="goBack();"> </span>
        <span class="title"> <?php echo JText::_('COM_SITEADMINBLN_SUB_TOTAL'); ?></span>
    </div>
    <div class="content-box">
            <div class="payment-box">
                <p class="smalltitle"> <?php echo JText::_('COM_SITEADMINBLN_SUB_DIS_PAYMENT'); ?><span class="note">*</span></p>
                <input type="text" name ="totalnew" disabled class="totalnew" value = "<?php echo $newtotal; ?>">

            </div>
            <div class="type-box">
            <p class="title"><?php echo JText::_('COM_SITEADMINBLN_SUB_TYPE'); ?></p>

                <div class="typepaySelect">
                    <div class="paySelected">
                    monthly
                    </div>
                <!--                    <ul class="ulTypeSelected" style="display: none;">
                        <li value="1">yearly</li>
                        <li value="2">monthly</li>
                        <input type="hidden" id="type_value" name="type_value" value="1">
                                    </ul>-->
                </div>
            </div>
            <div class="time-box">
                <p class="title"><?php echo JText::_('COM_SITEADMINBLN_SUB_PERIOD'); ?></p>
            <p class="time"><?php
                echo ($this->inscrease->date_start) ?
                    date('d/m/Y', strtotime($this->inscrease->date_start)) . ' - ' . date('d/m/Y', strtotime($this->inscrease->date_end)) :
                    date('d/m/Y', strtotime($this->info->start_date)) . ' - ' . date('d/m/Y', strtotime($this->info->end_date))
            ?></p>
            <p class="submoney">
                <span class="title">sub total</span> <span class="price">USD <?php echo number_format($totalm, 2) ;?></span>
            </p>
             <p class="submoney">
                <span class="title">tax</span> <span class="price">USD <?php echo number_format($totalm*0.07,2);?></span>
            </p>
                    
            </div>
            <div class="checkout-box">
                <div class="line"></div>

                <div class = "checkout-total">
                <span class="total">Total &nbsp; &nbsp;</span><span class="money">USD<?php echo number_format($money,2) ?></span>
                </div>
                <div class="clear"></div>
                <div class="checkout-buttons pull-right row">
                    <button class="btCancel" type="button" onclick="goBack()"><?php echo Jtext::_('MYCOURSE_BTN_CANCEL'); ?></button>
                    <button class="btSave"><?php echo Jtext::_('COM_SITEADMINBLN_SUB_CHECKOUT'); ?></button>
                </div>
            </div>

    </div>
</div>
<script>
    jQuery('.typepaySelect').click(function () {
        jQuery(this).find('.ulTypeSelected').toggle();
    });
    jQuery('.ulTypeSelected li').click(function () {
        console.log(jQuery(this).val());

        jQuery('.paySelected').html(jQuery(this).html());
        jQuery('#type_value').val(jQuery(this).html());

        var month = 'USD' + '<?php echo $totalm ?>';
        var year = 'USD' + '<?php echo $totaly ?>';
        if (jQuery(this).val() == 2) {
            jQuery('.money').html(month.replace(',', '.'));
        } else
            jQuery('.money').html(year.replace(',', '.'));

    });
    jQuery('.checkout-buttons .btSave').click(function () {
      
        var type = "monthly";
            var total = '<?php echo $newtotal; ?>';
            var url = '<?php echo JURI::base() . 'index.php?option=com_siteadminbln&view=subscription&action=checkout&act=' ?>' + total;
            
//        if (type) {
            jQuery.ajax({
                type: 'POST',
                url: '<?php echo JURI::base() . 'index.php?option=com_siteadminbln&view=subscription&action=checkout'; ?>',
                data: {
                    total: total,
                    type: type,
                    price: '<?php echo $money;?>'
                },
                beforeSend: function () {
                    lgtCreatePopup('', {'content': 'Loading'});
                },
                success: function (data, textStatus, jqXHR) {
                   var res = JSON.parse(data);
                    console.log(res.data);
                    window.location.href = '<?php echo JURI::base() . 'index.php?option=com_siteadminbln&view=subscription&action=success&orderId=' ?>' + res.data;
                },
                error: function (res) {
                    console.log(res);
                    lgtRemovePopup();
                }
            });
//        } else
//            lgtCreatePopup('withCloseButton', {'content': 'You have not selected type of subscription yet.'});
    });
</script>