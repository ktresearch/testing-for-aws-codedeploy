<?php

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class SiteadminblnViewSubscription extends JViewLegacy {
    
    function display($tpl = null)
    {
        $user = JFactory::getUser();
        $app = JFactory::getApplication();
        $jinput = $app->input;
        $document = JFactory::getDocument();
        require_once(JPATH_SITE.'/components/com_siteadminbln/models/account.php');
        $model = new SiteadminblnModelAccount();
         if (count($model->checkSiteAdminBLn($user->id)) > 0) {
            $isSiteAdmin = true;
        }
        if (!$isSiteAdmin) {
            header('Location: /404.html');
            exit;
//            echo '<div class="message">' . JText::_('COM_SITEADMINBLN_NO_ACCESS') . '</div>';
//            return;
        }
        $act = $jinput->post->get('act', '', 'STRING');
        $action = $jinput->get->get('action', '', 'STRING');
//        var_dump($model->addOrder($action));die;
        switch ($action){
            case "checkout":
                $this->info = $model->getInfosubscription();
                $this->inscrease = $model->insreaseInfo();
                if(isset($_POST['total'])){
                $array = array();
                $array['total'] = $_POST['total'];
                $array['type'] = $_POST['type'];
                $array['price'] = $_POST['price'];
                $data = $model->addOrder($array);
                $result = array();
                $result['comment'] = 'hello';
                $result['data'] = $data;
                echo json_encode($result);
                die;
                }
                parent::display('checkout');
                break;
             case "success":
                $newtotal = JRequest::getVar('orderId', '');
                if($newtotal){
                    $this->info = $model->orderInfo($newtotal);
    
                }
                parent::display('success');
                break;
            default:
                $this->info = $model->getInfosubscription();
                $this->currUsage = $model->countAllUserBLN();                
                if ($this->currUsage != $this->info->total_use) {   // Update table subscription
                    $total_empty = ((int)$this->info->total_purchased) - ((int)$this->currUsage);
                    $model->updateSubscription($this->currUsage, $total_empty);
                    $this->info = $model->getInfosubscription();
                }
                $this->inscrease = $model->insreaseInfo();
                $this->allInscrease = $model->subIncrease();
                $this->countIN = $model->countIncrease();
                $act = JRequest::getVar('act', '');
                if($act)
                    $this->new =  date('d/m/Y',strtotime($this->inscrease->date_start)).' - '.date('d/m/Y',strtotime($this->inscrease->date_end)).':&nbsp&nbsp&nbsp&nbsp+'.$this->inscrease->new_total;
                parent::display($tpl);
                break;
        }

    }
    
    

}

