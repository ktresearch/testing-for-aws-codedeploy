<?php
    defined('_JEXEC') or die();
    
    $mainframe = JFactory::getApplication();

    $session = JFactory::getSession();
    $device = $session->get('device');
    if ($device == 'mobile') { 
        $dev = 1;
        $sizechart = 110;
    } else {
        $sizechart = 130;
    }
    
    require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
    
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.js"></script>
<?php
    $curso = $this->courseinfo;
    
    $url = JURI::base() . 'course/' . $curso['remoteid'] . '.html';
    $class_cl = 'right';
    if ($i % 2 != 0) {
        $class_cl = 'left';
    }
    $ownerid = JUserHelper::getUserId($curso['creator']);
    $owner = CFactory::getUser($ownerid);
    
    $percentage = $curso['count_learner'] ? (round($curso['count_completed']*100/$curso['count_learner'])) : 0;
    $member_notcomplete = $curso['count_learner'] - $curso['count_completed'];    
    if ($curso['count_learner'] == 0 && $member_notcomplete == 0) {
        $member_notcomplete = 1;
    }
?>
<div class="reports-menus-page" id="reports-menus-page">
    <div class="course sabln-page">
        <div class="sabln-page-header">
            <div class="sabln-back-icon" onclick="goBackCourseReport();">
                <img src="<?php echo JUri::base(); ?>images/PreviousIcon.png">
            </div>
            <div class="sabln-page-name">
                <?php echo JText::_('COM_REPORTS_COURSE_REPORT');?>
            </div>
        </div>
        <div class="sabln-page-main">
            <div class="sabln-page-reportresult remoteid_<?php echo $curso['remoteid'] ?>">
                <div class="sabln-page-content">
                    <div class="report-box left course_info">
                        <span class="report-title"><?php echo JText::_('COM_REPORTS_COURSE_COURSE');?></span>
                        <div class="report-line">
                            <div class="icon-left">
                                <img class="icon-course" src="<?php echo $curso['course_img']; ?>"/>
                            </div>
                            <div class="name-right">
                                <span class="report-coursename"><?php echo $curso['fullname'];?></span>
                            </div>
                        </div>
                    </div>
                    <div class="report-box right">
                        <span class="report-title"><?php echo JText::_('COM_REPORTS_COURSE_CREATOR');?></span>
                        <div class="report-line">
                            <div class="icon-left">
                                <img class="icon-owner" src="<?php echo $owner->getAvatar(); ?>"/>
                            </div>
                            <div class="name-right">
                                <span class="report-owner"><?php echo $owner->getDisplayName();?></span>
                            </div>
                        </div>
                    </div>
                    <div class="report-box left createddate">
                        <span class="report-title"><?php echo JText::_('COM_REPORTS_COURSE_CREATION_DATE');?></span>
                        <span class="report-number"><?php echo date('m/d/Y', $curso['created']);?></span>
                    </div>
                    <div class="report-box right chartReport">
                        <span class="report-title"><?php echo JText::_('COM_REPORTS_COURSE_NO_MEMBERS');?></span>
                        <div class="report-line">
                            <img class="icon-member" src="<?php echo JUri::base(); ?>images/icons/members_2x.png"/>
                            <span class="report-number"><?php echo $curso['count_learner'];?></span>
                        </div>
                        <div class="report-line">
                            <div class="chartSub">
                                <div class="circleUser">
                                    <canvas id="doughnut" width="<?php echo $sizechart;?>" height="<?php echo $sizechart;?>"></canvas>
                                </div>
                            </div>
                            <div class="rp-completed">
                                <span class="report-completed"><?php echo JText::_('COM_REPORTS_COURSE_COMPLETED');?></span>
                                <div class="report-line">
                                    <img class="icon-member" src="<?php echo JUri::base(); ?>images/icons/members_2x.png"/>
                                    <span class="report-number"><?php echo $curso['count_completed'];?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sabln-buttons">
                    <button class="btn-download-report" onclick="downloadReport('coursereport', <?php echo $curso['remoteid']; ?>);"><?php echo JText::_('COM_REPORTS_BUTTON_REPORT');?></button>
                </div>
            </div>
        </div>
    </div>
    <div class="notification"></div>
</div>

<script type="text/javascript">
    var device = '<?php echo $dev; ?>';
    Chart.types.Doughnut.extend({
        name: "doughnutAlt",
        draw: function () {
            Chart.types.Doughnut.prototype.draw.apply(this, arguments);

            var width = this.chart.width,
                    height = this.chart.height;

            var fontSize = (height / 114).toFixed(2);
            this.chart.ctx.font = 'normal 16px "Open Sans", "OpenSans", sans-serif';
            this.chart.ctx.textAlign = 'center';
            this.chart.ctx.textBaseline = 'middle';
            this.chart.ctx.fillStyle = '#484848';
            this.chart.ctx.save();
            this.chart.ctx.beginPath();

            var text = '<?php echo $percentage; ?>%';
            if(device) this.chart.ctx.fillText(text, 55, 55);
            else this.chart.ctx.fillText(text, 65, 65);
            this.chart.ctx.font = 'normal 16px "Open Sans", "OpenSans", sans-serif';
            this.chart.ctx.textAlign = 'center';
            this.chart.ctx.textBaseline = 'middle';
            this.chart.ctx.fillStyle = '#484848';
            this.chart.ctx.save();
            this.chart.ctx.beginPath();
        }
    });

    var chartOptions = {
        onAnimationComplete: function () {
            // Always show Tooltip
            this.showTooltip(this.segments, true);
        },
        customTooltips: function (tooltip) {
            // Tooltip Element
            var tooltipEl = $('#chartjs-tooltip');
            // Hide if no tooltip
            if (!tooltip) {
                tooltipEl.css({
                    opacity: 1
                });
                return;
            }
            // Set caret Position
            tooltipEl.removeClass('above below');
            tooltipEl.addClass(tooltip.yAlign);
            tooltipEl.addClass(tooltip.xAlign);
            // Set Text
            tooltipEl.html(tooltip.text);
            // Find Y Location on page
            var top;
            if (tooltip.yAlign == 'above') {
                top = tooltip.y - tooltip.caretHeight - tooltip.caretPadding;
            } else {
                top = tooltip.y + tooltip.caretHeight + tooltip.caretPadding;
            }
            // Display, position, and set styles for font
            tooltipEl.css({
                opacity: 0,
                left: tooltip.chart.canvas.offsetLeft + tooltip.x + 'px',
                top: tooltip.chart.canvas.offsetTop + top + 'px',
                fontFamily: tooltip.fontFamily,
                fontSize: tooltip.fontSize,
                fontStyle: tooltip.fontStyle,
                xOffset: tooltip.xOffset,
            });
        },
        tooltipEvents: [], // Remove to enable Default Mouseover events
        tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
        tooltipFillColor: "rgba(0,0,0,0.0)",
        tooltipFontColor: "#505050",
        tooltipFontSize: 24,
        tooltipXOffset: 0,
        tooltipXPadding: 0,
        tooltipYPadding: 0,
//    tooltipTemplate: "<%= value %>%",
        legends: true,
        showTooltips: true,
        segmentShowStroke: false,
        percentageInnerCutout: 65,
        animationEasing: "easeInOutQuart",
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    }

    var data = [{
            value: <?php echo $curso['count_completed']; ?>,
            color: "#126DB6",
            label: "Data 1"
        }, {
            value: <?php echo $member_notcomplete; ?>,
            color: "#DBECF8",
            label: "Data 2"
        }, ];
    var doughnutChart = new Chart(document.getElementById("doughnut").getContext("2d")).doughnutAlt(data, chartOptions);
    
    function goBackCourseReport() {
        window.location.href = "<?php echo JRoute::_('index.php?option=com_siteadminbln&view=reports#course', false);?>";
    }
    
    function downloadReport(act, courseid) {
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: {act : act, rpid: courseid},
            beforeSend: function() {
                jQuery('body').addClass('overlay2');
                jQuery('.notification').html('Loading...').fadeIn();
            },
            success: function(data, textStatus, jqXHR) {
                var res = JSON.parse(data); 
                if (res.error === 0) {
                    jQuery('body').removeClass('overlay2');
                    jQuery('.notification').fadeOut();
//                            e.preventDefault();  //stop the browser from following
                    window.location.href = '<?php echo $mainframe->getCfg('wwwrootfile').'/temp/';?>'+res.filename+'.xlsx?forcedownload=1';
                } else {
                    jQuery('body').removeClass('overlay2');
                    jQuery('.notification').fadeOut();
                    lgtCreatePopup('oneButton', {
                        content: res.comment
                    },
                    function() {
                        lgtRemovePopup();
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
            }
        });
    }
</script>

