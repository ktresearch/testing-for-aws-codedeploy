<?php
defined('_JEXEC') or die();
$app = JFactory::getApplication();

$session = JFactory::getSession();
$device = $session->get('device');
if ($device == 'mobile') $dev = 1;

$sl = $this->totalcourses;
$number = ceil($sl/20);

$su = $this->totalusers;
$number_u = ceil($su/20);

$scr = $this->totalcircles;
$number_cr = ceil($scr/20);
?>

<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.js"></script>
<div class="reports-menus-page" id="reports-menus-page" style="display:none;">
    <transition name="slide" v-on:before-enter="beforeEnter" v-on:enter="enter" v-on:after-enter="afterEnter" v-on:enter-cancelled="enterCancelled" v-on:before-leave="beforeLeave" v-on:after-leave="afterLeave" v-on:leave-cancelled="leaveCancelled">
        <div class="default-page sabln-page" v-if="this.showing == 'default'" key="default">
            <div class="reportspage-title">
                <span class="back-icon" onclick="goBackAdmin();"></span>
                <span class="title-page"><?php echo JText::_('COM_SITEADMINBLN_REPORTS'); ?></span>
            </div>

            <div class="reports-list" v-for="list in reportList">
                <div class="report-menu" @click="show(list.code)">
                    <span class="report-title">{{ list.text }}</span>
                    <span class="report-icon"></span>
                </div>
            </div>
        </div>
        
        <!-- ================= Network report ======================= -->
        <div class="bln sabln-page" v-if="this.showing == 'bln'" key="bln">
            <div class="sabln-page-header">
                <div class="sabln-back-icon" @click="show">
                    <img src="<?php echo JUri::base(); ?>images/PreviousIcon.png">
                </div>
                <div class="sabln-page-name">
                    <?php echo JText::_('COM_REPORTS_NETWORK_REPORT');?>
                </div>
            </div>

            <div class="sabln-page-main">
                <div class="sabln-page-content">
                    <div class="report-box left">
                        <span class="report-title"><?php echo JText::_('COM_REPORTS_NETWORK_NOOF_MEMBERS');?></span>
                        <div class="report-line">
                            <img class="icon-member" src="<?php echo JUri::base(); ?>images/icons/members_2x.png"/>
                            <span class="report-number"><?php echo $this->blnreport['membercount'];?></span>
                        </div>
                    </div>
                    <div class="report-box right">
                        <span class="report-title"><?php echo JText::_('COM_REPORTS_NETWORK_NOOF_SUBCIRCLES');?></span>
                        <span class="report-number"><?php echo $this->blnreport['subcirclescount'];?></span>
                    </div>
                </div>
                <div class="sabln-buttons">
                    <button class="btn-download-report" @click="downloadReport('networkreport')"><?php echo JText::_('COM_REPORTS_BUTTON_REPORT');?></button>
                </div>
            </div>
        </div>

        <!-- ===================== course report ====================== -->
        <div class="course sabln-page" v-if="this.showing == 'course'" key="course">
            <div class="sabln-page-header">
                <div class="sabln-back-icon" @click="show">
                    <img src="<?php echo JUri::base(); ?>images/PreviousIcon.png">
                </div>
                <div class="sabln-page-name">
                    <?php echo JText::_('COM_REPORTS_COURSE_REPORT');?>
                </div>
            </div>

            <div class="sabln-page-main">      
                <div class="list-report">
                    <div class="sabln-search-box">
                        <form action="#" method="post" @submit="courseSearch" id="form-course-search" class="form-search">
                            <div class="div-search-c div-search-course" :class="{ hasValue: coursekeyword }">
                                <input type="text" name="coursekeyword" class="searchKeyword coursekeyword" v-model="coursekeyword" placeholder="<?php echo JText::_('COM_REPORTS_COURSE_SEARCH_HOLDER'); ?>" autocomplete="off"/>
                                <button class="btn-search-c" type="submit"></button>
                            </div>
                            <div class="div-cancel-search animated" :class="{ 'animated slideInRight hasValue' : coursekeyword,  'animated slideOutRight hasValue' : !coursekeyword }">
                                <button class="btn-cancel-search" type="button"  @click="resetFormCourseSearch"></button>
                            </div>
                        </form>
                    </div>
                    <div class="sabln-page-content list-courses-report">
                        <?php                    
                        if ($this->blncourses) {
                            $i = 1;
                            foreach ($this->blncourses as $curso) {
                                $url = JURI::base().'index.php?option=com_siteadminbln&view=reports&action=courserp&rpid='.$curso['remoteid'];
                                $class_cl = 'right';
                                if ($i % 2 != 0) {
                                    $class_cl = 'left';
                                }
                                $ownerid = JUserHelper::getUserId($curso['creator']);
                                $owner = CFactory::getUser($ownerid);
                            ?>
                            <div class="report-item <?php echo $class_cl;?> course_<?php echo $curso['remoteid']; ?>" data-cid="<?php echo $curso['remoteid'] ?>" @click="showReportCourse(<?php echo $curso['remoteid']; ?>)">
                                <div class="report-item-content">
                                    <div class="clearfix"></div>
                                    <div class="item-img">
                                        <a href="<?php echo $url; ?>">
                                            <div class="image" id="images"
                                                 style="background-image: url('<?php echo $curso['course_img'].'?'.(time()*1000); ?>')"></div>

                                        </a>
                                    </div>

                                    <div class="content_c_search">
                                        <div class="clearfix"></div>
                                        <div class="div_title">
                                            <?php echo "<a class=\"c_title\" href=\"$url\">" . $curso['fullname'] . "</a>"; ?>
                                        </div>

                                        <div class="c_onwer">
                                            <?php echo $owner->getDisplayName(); ?>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                            </div>

                            <?php 
                                $i++;
                            } 
                        } else {
                            echo 'No found courses.';
                        }
                        ?>
                    </div>
                </div>
                    
            </div>
        </div>
        
        <!-- ==================== user report ========================== -->
        <div class="user sabln-page" v-if="this.showing == 'user'" key="user">
            <div class="sabln-page-header">
                <div class="sabln-back-icon" @click="show">
                    <img src="<?php echo JUri::base(); ?>images/PreviousIcon.png">
                </div>
                <div class="sabln-page-name">
                    <?php echo JText::_('COM_REPORTS_USER_REPORT');?>
                </div>
            </div>

            <div class="sabln-page-main">
                <div class="list-report">
                    <div class="sabln-search-box">
                        <form action="#" method="post" @submit="userSearch" id="form-user-search" class="form-search">
                            <div class="div-search-c div-search-user" :class="{ hasValue: userkeyword }">
                                <input type="text" name="userkeyword" class="searchKeyword userkeyword" v-model="userkeyword" placeholder="<?php echo JText::_('COM_REPORTS_USER_SEARCH_HOLDER'); ?>" autocomplete="off"/>
                                <button class="btn-search-c" type="submit"></button>
                            </div>
                            <div class="div-cancel-search animated" :class="{ 'animated slideInRight hasValue' : userkeyword,  'animated slideOutRight hasValue' : !userkeyword }">
                                <button class="btn-cancel-search" type="button"  @click="resetFormUserSearch"></button>
                            </div>
                        </form>
                    </div>
                    <div class="sabln-page-content list-users-report">
                        <?php                    
                        if ($this->allusers) {
                            $i = 1;
                            foreach ($this->allusers as $member) {
                                $user = CFactory::getUser($member->id);
                                $class_cl = 'right';
                                if ($i % 2 != 0) {
                                    $class_cl = 'left';
                                }
                            ?>
                            <div class="report-item <?php echo $class_cl;?> user_<?php echo $user->id; ?>" data-cid="<?php echo $user->id; ?>" @click="showReportUser(<?php echo $user->id . ', ' . $member->date; ?>)">
                                <div class="useritem-image"><img src="<?php echo $user->getAvatar(); ?>" alt="<?php echo $user->name; ?>" title="<?php echo $user->name; ?>"/></div>
                                <div class="useritem-info">
                                    <div class="useritem-name"><p class="name"><?php echo $user->name; ?></p></div>
                                </div>
                            </div>

                            <?php 
                                $i++;
                            } 
                        } else {
                            echo 'No found user.';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- ======================== circle report ======================== -->
        <div class="circleReport sabln-page" v-if="this.showing == 'circleReport'" key="circleReport">
            <div class="sabln-page-header">
                <div class="sabln-back-icon" @click="show">
                    <img src="<?php echo JUri::base(); ?>images/PreviousIcon.png">
                </div>
                <div class="sabln-page-name">
                    <?php echo JText::_('COM_REPORTS_CIRCLE_REPORT');?>
                </div>
            </div>

            <div class="sabln-page-main">
                <div class="list-report">
                    <div class="sabln-search-box">
                        <form action="#" method="post" @submit="circleSearch" id="form-circle-search" class="form-search">
                            <div class="div-search-c div-search-circle" :class="{ hasValue: circlekeyword }">
                                <input type="text" name="circlekeyword" class="searchKeyword circlekeyword" v-model="circlekeyword" placeholder="<?php echo JText::_('COM_REPORTS_CIRCLE_SEARCH_HOLDER'); ?>" autocomplete="off"/>
                                <button class="btn-search-c" type="submit"></button>
                            </div>
                            <div class="div-cancel-search animated" :class="{ 'animated slideInRight hasValue' : circlekeyword,  'animated slideOutRight hasValue' : !circlekeyword }">
                                <button class="btn-cancel-search" type="button"  @click="resetFormCircleSearch"></button>
                            </div>
                        </form>
                    </div>
                    <div class="sabln-page-content list-circles-report">
                        <?php                    
                        if ($this->blncircles) {
                            $i = 1;
                            foreach ($this->blncircles as $circle) {
                                $table =  JTable::getInstance( 'Group' , 'CTable' );
                                $table->load($circle->id);
                                $url = JURI::base() . 'course/' . $curso['remoteid'] . '.html';
                                $class_cl = 'right';
                                if ($i % 2 != 0) {
                                    $class_cl = 'left';
                                }
                                $ownercircle = CFactory::getUser($circle->ownerid);
                                
                                $groupModel = CFactory::getModel('groups');
                                $countMembers = $groupModel->getMembersCount($circle->id, true);
                            ?>
                            <div class="report-item <?php echo $class_cl;?> circle_<?php echo $circle->id; ?>" data-cid="<?php echo $circle->id; ?>" @click="showReportCircle(<?php echo $circle->id; ?>)">
                                <div class="report-item-content">
                                    <div class="clearfix"></div>
                                    <div class="item-img">
                                        <!--<a href="<?php // echo $url; ?>">-->
                                            <div class="image" id="images"
                                                 style="background-image: url('<?php echo $table->getOriginalAvatar().'?'.(time()*1000); ?>')"></div>

                                        <!--</a>-->
                                    </div>

                                    <div class="content_c_search">
                                        <div class="clearfix"></div>
                                        <div class="div_title">
                                            <?php echo "<span class=\"c_title\">" . $circle->name . "</span>"; ?>
                                        </div>

                                        <div class="c_onwer">
                                            <?php echo $ownercircle->getDisplayName(); ?>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                            </div>
                        
                            <div class="sabln-page-reportresult circleid_<?php echo $circle->id; ?>" style="display: none;">
                                <div class="sabln-page-content">
                                    <div class="report-box left circle_info">
                                        <span class="report-title"><?php echo JText::_('COM_REPORTS_CIRCLE_CIRCLE');?></span>
                                        <div class="report-line">
                                            <div class="icon-left">
                                                <img class="icon-circle" src="<?php echo $table->getAvatar(); ?>"/>
                                            </div>
                                            <div class="name-right">
                                                <span class="report-owner"><?php echo $circle->name;?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="report-box right circle_owner">
                                        <span class="report-title"><?php echo JText::_('COM_REPORTS_CIRCLE_OWNER');?></span>
                                        <div class="report-line">
                                            <div class="icon-left">
                                                <img class="icon-owner" src="<?php echo $ownercircle->getAvatar(); ?>"/>
                                            </div>
                                            <div class="name-right">
                                                <span class="report-owner"><?php echo $ownercircle->getDisplayName();?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="report-box left circle_creationdate">
                                        <span class="report-title"><?php echo JText::_('COM_REPORTS_COURSE_CREATION_DATE');?></span>
                                        <span class="report-number"><?php echo date('m/d/Y', strtotime($circle->created));?></span>
                                    </div>
                                    <div class="report-box right circle_nomembers">
                                        <span class="report-title"><?php echo JText::_('COM_REPORTS_COURSE_NO_MEMBERS');?></span>
                                        <div class="report-line">
                                            <img class="icon-member" src="<?php echo JUri::base(); ?>images/icons/members_2x.png"/>
                                            <span class="report-number"><?php echo $countMembers;?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="sabln-buttons">
                                    <button class="btn-download-report" @click="downloadReport('circlereport', <?php echo $circle->id;?>)"><?php echo JText::_('COM_REPORTS_BUTTON_REPORT');?></button>
                                </div>
                            </div>

                            <?php 
                                $i++;
                            } 
                        } else {
                            echo 'No found circles.';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </transition>
    <div class="notification"></div>
</div>

<script>
    jQuery(document).ready(function() {
        
        jQuery('.useritem-name').each(function () {
           if (jQuery(this).find('p.name').height() > 48) {
               jQuery(this).find('p.name').addClass('limited');
           }
        });
        jQuery('.div_title').each(function () { 
            if (jQuery(this).find('a.c_title').height() > 48) { 
                jQuery(this).find('a').addClass('limited');
            }
            if (jQuery(this).find('span.c_title').height() > 48) { 
                jQuery(this).find('span').addClass('limited');
            }
        });
        
        jQuery('.searchKeyword').on('keyup', function() {
            if (jQuery(this).val() != '') {
                jQuery('.div-search-course').addClass('hasValue');
                jQuery('.div-cancel-search').removeClass('slideOutRight').addClass('hasValue').addClass('slideInRight');
            } else {
                jQuery('.div-search-course').removeClass('hasValue');
                jQuery('.div-cancel-search').removeClass('hasValue').removeClass('slideInRight').addClass('slideOutRight');
            }
        });
        jQuery('.userkeyword').on('keyup', function() {
            if (jQuery(this).val() != '') {
                jQuery('.div-search-user').addClass('hasValue');
                jQuery('.div-cancel-search').removeClass('slideOutRight').addClass('hasValue').addClass('slideInRight');
            } else {
                jQuery('.div-search-user').removeClass('hasValue');
                jQuery('.div-cancel-search').removeClass('hasValue').removeClass('slideInRight').addClass('slideOutRight');
            }
        });
    });
    
    var app = new Vue({
        el: '#reports-menus-page',
        data: {
            reportList: {
                bln: {
                    code: 'bln',
                    text: '<?php echo JText::_('COM_REPORTS_MENU_NETWORK');?>'
                },
                course: {
                    code: 'course',
                    text: '<?php echo JText::_('COM_REPORTS_MENU_COURSE');?>'
                },
                'user': {
                    code: 'user',
                    text: '<?php echo JText::_('COM_REPORTS_MENU_USER');?>'
                },
                circleReport: {
                    code: 'circleReport',
                    text: '<?php echo JText::_('COM_REPORTS_MENU_CIRCLE');?>'
                }
            },
            qa: {
                qaFirstName: '',
                qaLastName: '',
                qaEmail: '',
                qaPassword: '',
                qaReenterPassword: ''
            },
            qaErrors: {
                qaFirstNameError: false,
                qaLastNameError: false,
                qaEmailError: false,
                qaPasswordError: false,
                qaReenterPasswordError: false
            },
            coursekeyword: '',
            userkeyword: '',
            circlekeyword: '',
            qrUsersList: [],
            urlArr: [],
            showing: 'default',
            pages: ['bln', 'course', 'user', 'circleReport' ],
        },
        mounted: function() {
            if (window.location.href.indexOf('#') != -1) {
                this.urlArr = window.location.href.split('#');
                this.show(this.urlArr[this.urlArr.length-1]);
            } else this.urlArr.push(window.location.href);

            jQuery('#reports-menus-page').fadeIn();

            // Ajax get data about users list here
            this.qrUsersList = {
 
                }
            //end sample data
        },
        components: {
//            'csvTemplate': csvTemplate
        },
        computed: {
            isAddingUsers: function() {
                switch(this.userManagerShowing) {
                    case 'csv-template':
                    case 'quick-add':
                    case 'quick-add-success':
                        return true;
                        break;
                    case 'ru-csv-template':
                    case 'quick-remove':
                    case 'quick-remove-success':
                        return false;
                        break;
                }
            }
        },
        methods: {
            checkForm: function(e) {
                e.preventDefault();
                if (!this.qa.qaFirstName) this.qaErrors.qaFirstNameError = true; else this.qaErrors.qaFirstNameError = false;
                if (!this.qa.qaLastName) this.qaErrors.qaLastNameError = true; else this.qaErrors.qaLastNameError = false;
                if (!this.qa.qaEmail || !this.validEmail(this.qa.qaEmail)) this.qaErrors.qaEmailError = true; else this.qaErrors.qaEmailError = false;
                if (!this.qa.qaPassword) this.qaErrors.qaPasswordError = true; else this.qaErrors.qaPasswordError = false;
                if (!this.qa.qaReenterPassword) this.qaErrors.qaReenterPasswordError = true; else this.qaErrors.qaReenterPasswordError = false;

                if (this.qa.qaReenterPassword != this.qa.qaPassword) this.qaErrors.qaPasswordError = this.qaErrors.qaReenterPasswordError = true;
                this.userManagerShowing = 'quick-add-success';
            },
            validEmail:function(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            },
            courseSearch: function(e) {
                e.preventDefault();
//                if (this.coursekeyword) { 
                    // let's search
                    var val = this.coursekeyword.toLowerCase();
                    searchCourse(0);
//                    jQuery('.list-courses-report .report-item').hide();
//                    jQuery('.list-courses-report .report-item').each(function () { 
//                        var text = jQuery(this).find('a.c_title').text().toLowerCase();
//                        var textowner = jQuery(this).find('.c_onwer').text().toLowerCase();
//    
//                        if(text.indexOf(val) != -1 || textowner.indexOf(val) != -1)
//                        {
//                            jQuery(this).show();
//                        }
//                    });
//                }
            },
            resetFormCourseSearch: function() {
                jQuery('.searchKeyword').val('');
                jQuery('.div-search-course').removeClass('hasValue');
                jQuery('.div-cancel-search').removeClass('hasValue').removeClass('slideInRight').addClass('slideOutRight');
                this.coursekeyword = '';
//                jQuery('.list-courses-report .report-item').show();
//                searchCourse(0);
            },
            userSearch: function(e) {
                e.preventDefault();
//                if (this.userkeyword) { 
                    var val = this.userkeyword.toLowerCase();
//                    jQuery('.list-users-report .report-item').hide();
//                    jQuery('.list-users-report .report-item').each(function () { 
//                        var text = jQuery(this).find('.useritem-name').text().toLowerCase();
//    
//                        if(text.indexOf(val) != -1)
//                        {
//                            jQuery(this).show();
//                        }
//                    });
//                }
                searchUser(0);
            },
            resetFormUserSearch: function() {
                jQuery('.searchKeyword').val('');
                jQuery('.div-search-user').removeClass('hasValue');
                jQuery('.div-cancel-search').removeClass('hasValue').removeClass('slideInRight').addClass('slideOutRight');
                this.userkeyword = '';
//                jQuery('.list-users-report .report-item').show();
            },
            circleSearch: function(e) { 
                e.preventDefault();
//                if (this.circlekeyword) { 
//                    // let's search
//                    var val = this.circlekeyword.toLowerCase();
//                    jQuery('.list-circles-report .report-item').hide();
//                    jQuery('.list-circles-report .report-item').each(function () { 
//                        var text = jQuery(this).find('span.c_title').text().toLowerCase();
//                        var textowner = jQuery(this).find('.c_onwer').text().toLowerCase();
//    
//                        if(text.indexOf(val) != -1 || textowner.indexOf(val) != -1)
//                        {
//                            jQuery(this).show();
//                        }
//                    });
//                }
                searchCircle(0);
            },
            resetFormCircleSearch: function() {
                jQuery('.searchKeyword').val('');
                jQuery('.div-search-circle').removeClass('hasValue');
                jQuery('.div-cancel-search').removeClass('hasValue').removeClass('slideInRight').addClass('slideOutRight');
                this.circlekeyword = '';
//                jQuery('.list-circles-report .report-item').show();
            },
            show: function (page) {
                if (jQuery.inArray(page, this.pages) == -1)
                    page = 'default';

                if (page == 'default' && jQuery('.circleReport .sabln-search-box').css('display') == 'none') { 
                    loading_c = false; c = 1;
                    jQuery('.report-item').show();
                    jQuery('.sabln-search-box').show();
                    jQuery('.sabln-page-reportresult').hide();
                } else {
                    loading_c = false; c = 1;
                    this.showing = page;
                    if (window.location.href.indexOf('#') != -1) {
                        if (this.urlArr.length > 1) {
                            this.urlArr[this.urlArr.length - 1] = page;
                        } else {
                            this.urlArr.push(page);
                        }
                    } else {
                        this.urlArr.push(page);
                    }
                    window.location.href = this.urlArr.join('#');
                }
            },
            userManagerShow: function (page) {
                if (jQuery.inArray(page, this.userManagerPages) == -1)
                    page = 'csv-template';

                this.userManagerShowing = page;
            },
            qaCancel: function() {
                this.resetQAForm();
                this.userManagerShow();
            },
            qaSuccessBack: function() {
                this.resetQAForm();
                this.userManagerShow();
            },
            showReportCourse: function(data) { 
                window.location.href = '<?php echo JURI::base().'index.php?option=com_siteadminbln&view=reports&action=courserp&rpid=';?>'+data;
            },
            showReportUser: function(userid, date) {
                window.location.href = '<?php echo JURI::base().'index.php?option=com_siteadminbln&view=reports&action=userrp&rpid=';?>'+userid+'<?php echo '&date=';?>'+date;
            },
            showReportCircle: function(circleid) { 
                jQuery('.report-item').hide();
                jQuery('.sabln-search-box').hide();
                jQuery('.circleid_'+circleid).show();
                loading_c = true;
//                window.location.href = '<?php // echo JURI::base().'index.php?option=com_siteadminbln&view=reports&action=circlerp&rpid=';?>'+circleid;
            },
            downloadReport: function(act, id) {
                jQuery.ajax({
                    url: window.location.href,
                    type: 'POST',
                    data: {act : act, rpid: id},
                    beforeSend: function() {
                        jQuery('body').addClass('overlay2');
                        jQuery('.notification').html('Loading...').fadeIn();
                    },
                    success: function(data, textStatus, jqXHR) {
                        var res = JSON.parse(data); 
                        if (res.error === 0) {
                            jQuery('body').removeClass('overlay2');
                            jQuery('.notification').fadeOut();
//                            e.preventDefault();  //stop the browser from following
                            window.location.href = '<?php echo $app->getCfg('wwwrootfile').'/temp/';?>'+res.filename+'<?php echo '.xlsx?forcedownload=1'; ?>';
                        } else {
                            jQuery('body').removeClass('overlay2');
                            jQuery('.notification').fadeOut();
                            lgtCreatePopup('oneButton', {
                                content: res.comment
                            },
                            function() {
                                lgtRemovePopup();
                            });
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                    }
                });
            },
            beforeEnter: function (el) {
            },
            enter: function (el, done) {
            },
            afterEnter: function (el) {
            },
            enterCancelled: function (el) {
            },
            beforeLeave: function (el) {
                jQuery('body').css('overflow-x', 'hidden');
            },
            leave: function (el, done) {
            },
            afterLeave: function (el) {
                jQuery('body').css('overflow-x', 'auto');
                jQuery('.div_title').each(function () { 
                    if (jQuery(this).find('a.c_title').height() > 48) { 
                        jQuery(this).find('a').addClass('limited');
                    }
                    if (jQuery(this).find('span.c_title').height() > 48) { 
                        jQuery(this).find('span').addClass('limited');
                    }
                });
            },
            // leaveCancelled chỉ hoạt động với v-show
            leaveCancelled: function (el) {
            }
        }
    });

    var a = 1;
    var loading  = false; //to prevents multipal ajax loads
    var total_pages = <?php echo $number;?>;
       
    var u = 1;
    var loading_u = false;
    var total_pages_u = <?php echo $number_u;?>;
    
    var c = 1;
    var loading_c = false;
    var total_pages_c = <?php echo $number_cr;?>;
       
    function loadMoreCourse() {
        var scourse = jQuery('.coursekeyword').val();
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: {
                act: "load_more",
                scourse: scourse,
                page: a
            },
            beforeSend: function () {console.log('load more course: ' + a);
//                jQuery('.scoures_wrapper').addClass('loadingggg');
                jQuery('.list-courses-report').append('<div class="divLoadMore">Loading...<div class="loading"></div></div>');
//                if (cl == "") {
//                    cl = "load";
//                }
//                else return false;
            },
            success: function (data, textStatus, jqXHR) {
                var res = JSON.parse(data);
                var status = res.status;
                var b = res.courses;

                jQuery.each(b, function (key, value) {
                    var stt = b[key]['stt'];
                    var course_id = b[key]['remoteid'];
                    var fullname = b[key]['fullname'];
                    var course_image = b[key]['course_img'];
                    var owner = b[key]['owner'];

                    var html;
                    var url = '<?php echo JUri::base(); ?>index.php?option=com_siteadminbln&view=reports&action=courserp&rpid=' + course_id;
                    html = '<div class="report-item course_' + course_id + '" data-cid="' + course_id + '" onclick="showReportCourse(' + course_id + ')">' +
                        '<div class="report-item-content">' +
                        '<div class="clearfix"></div>' +
                        '<div class="item-img">' +
                        '<a href="' + url + '">' +
                        '<div class="image" id="images" style="background-image: url(\'' + course_image + '\')">' + '</div>'
                        +
                        '</a>' +
                        '</div>' +
                        '<div class="content_c_search">' +
                        '<div class="clearfix"></div>' +
                        '<div class="div_title"><a class="c_title" href="' + url + '">' + fullname + '</a></div>' +
                        '<div class="c_onwer" >' +
                        '' + owner + '' +
                        '</div>' +
                        '<div class="clearfix"></div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';

                    jQuery('.divLoadMore').remove();
                    jQuery('.list-courses-report').append(html);
                });
                jQuery('.div_title').each(function () { 
                    if (jQuery(this).find('a.c_title').height() > 48) { 
                        jQuery(this).find('a').addClass('limited');
                    }
                });
                a = a + 1;
                loading = false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
                loading = false;
            }
        });
    }
    
    function loadMoreUser() {
        var suser = jQuery('.userkeyword').val();
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: {
                act: "load_user",
                suser: suser,
                page: u
            },
            beforeSend: function () {console.log('load more user: ' + u);
                jQuery('.list-users-report').append('<div class="divLoadMore">Loading...<div class="loading"></div></div>');
            },
            success: function (data, textStatus, jqXHR) {
                var res = JSON.parse(data);
                var status = res.status;
                var b = res.users;
                
                jQuery.each(b, function (key, value) {
                    var user_id = b[key]['id'];
                    var fullname = b[key]['name'];
                    var user_avatar = b[key]['avatar'];
                    var mdate = b[key]['date'];

                    var html;
                    html = '<div class="report-item user_' + user_id + '" data-cid="' + user_id + '" onclick="showReportUser(' + user_id + ', ' + mdate + ')">' +
                        '<div class="useritem-image">' +
                        '<img src="' + user_avatar + '" alt="' + fullname + '" title="' + fullname + '"/>' +
                        '</div>' +
                        '<div class="useritem-info">' +
                        '<div class="useritem-name"><p class="name">' + fullname + '</p></div>' +
                        '</div>' +
                        '</div>';
                    jQuery('.divLoadMore').remove();
                    jQuery('.list-users-report').append(html);
                });
                jQuery('.useritem-name').each(function () {
                   if (jQuery(this).find('p.name').height() > 48) {
                       jQuery(this).find('p.name').addClass('limited');
                   }
                });
                u = u + 1;
                loading_u = false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
                loading_u = false;
            }
        });
    }
    
    function loadMoreCircle() {
        var scircle = jQuery('.circlekeyword').val();
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: {
                act: "load_circle",
                scircle: scircle,
                page: c
            },
            beforeSend: function () {console.log('load more circle: ' + c);
                jQuery('.list-circles-report').append('<div class="divLoadMore">Loading...<div class="loading"></div></div>');
            },
            success: function (data, textStatus, jqXHR) {
                var res = JSON.parse(data);
                var status = res.status;
                var b = res.circles;
                
                jQuery.each(b, function (key, value) {
                    var circle_id = b[key]['circle_id'];
                    var circle_original = b[key]['circle_original'];
                    var circle_name = b[key]['circle_name'];
                    var owner_name = b[key]['owner_name'];
                    var owner_avatar = b[key]['owner_avatar'];
                    var circle_avatar = b[key]['circle_avatar'];
                    var circle_created = b[key]['circle_created'];
                    var count_member = b[key]['count_member'];

                    var html = '';
                    html += '<div class="report-item circle_' + circle_id + '" data-cid="' + circle_id + '" onclick="showReportCircle(' + circle_id + ')">' +
                        '<div class="report-item-content">' +
                            '<div class="clearfix"></div>' +
                            '<div class="item-img">' +
                                '<div class="image" id="images" style="background-image: url(\'' + circle_original + '\')">' + '</div>' +
                            '</div>' +
                            '<div class="content_c_search">' +
                                '<div class="clearfix"></div>' +
                                '<div class="div_title"><span class="c_title">' + circle_name + '</span></div>' +
                                '<div class="c_onwer" >' + owner_name + '</div>' +
                                '<div class="clearfix"></div>' +
                            '</div>' +
                        '</div>' +
                        '</div>';
                    html += '<div class="sabln-page-reportresult circleid_' + circle_id + '" style="display: none;">' +
                        '<div class="sabln-page-content">' +
                            '<div class="report-box left circle_info">' +
                                '<span class="report-title"><?php echo JText::_('COM_REPORTS_CIRCLE_CIRCLE');?></span>' +
                                '<div class="report-line">' +
                                    '<div class="icon-left"><img class="icon-circle" src="'+circle_avatar+'"/></div>' +
                                    '<div class="name-right"><span class="report-owner">'+circle_name+'</span></div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="report-box right circle_owner">' +
                                '<span class="report-title"><?php echo JText::_('COM_REPORTS_CIRCLE_OWNER');?></span>'+
                                '<div class="report-line">'+
                                    '<div class="icon-left"><img class="icon-owner" src="' + owner_avatar + '"/></div>'+
                                    '<div class="name-right"><span class="report-owner">'+owner_name+'</span></div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="report-box left circle_creationdate">'+
                                '<span class="report-title"><?php echo JText::_('COM_REPORTS_COURSE_CREATION_DATE');?></span>'+
                                '<span class="report-number">'+circle_created+'</span>'+
                            '</div>'+
                            '<div class="report-box right circle_nomembers">'+
                                '<span class="report-title"><?php echo JText::_('COM_REPORTS_COURSE_NO_MEMBERS');?></span>'+
                                '<div class="report-line">'+
                                    '<img class="icon-member" src="<?php echo JUri::base(); ?>images/icons/members_2x.png"/>'+
                                    '<span class="report-number">'+count_member+'</span>'+
                                '</div>'+
                            '</div>' +
                        '</div>' +
                        '<div class="sabln-buttons">' +
                            '<button class="btn-download-report" onclick="downloadReport(\'circlereport\', ' + circle_id + ')"><?php echo JText::_('COM_REPORTS_BUTTON_REPORT');?></button>'+
                        '</div>' +
                        '</div>';
                    jQuery('.divLoadMore').remove();
                    jQuery('.list-circles-report').append(html);
                });
                jQuery('.div_title').each(function () { 
                    if (jQuery(this).find('span.c_title').height() > 48) { 
                        jQuery(this).find('span').addClass('limited');
                    }
                });

                c = c + 1;
                loading_c = false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
                loading_c = false;
            }
        });
    }
    
    jQuery(window).scroll(function () { 
        if ((Math.round(jQuery(window).scrollTop() + jQuery(window).height())) >=
            (Math.round(jQuery('.sabln-page').offset().top + jQuery('.sabln-page').height()))) {
            
            if (a < total_pages && loading==false && window.location.href.indexOf('#course') != -1) {
                loading = true;
                console.log('loading');
                loadMoreCourse();
            }
            
            if (u < total_pages_u && loading_u ==false && window.location.href.indexOf('#user') != -1) {
                loading_u = true;
                console.log('loading_u');
                loadMoreUser();
        }
            
            if (c < total_pages_c && loading_c ==false && window.location.href.indexOf('#circle') != -1) {
                loading_c = true;
                console.log('loading_c');
                loadMoreCircle();
            }
        }
    });

    function goBackAdmin() {
        window.location.href = "<?php echo JRoute::_('index.php?option=com_siteadminbln', false);?>";
    }

    function searchCourse(p) {
        var scourse = jQuery('.coursekeyword').val();
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: {
                act: "load_more",
                scourse: scourse,
                page: p
            },
            beforeSend: function () {

            },
            success: function (data, textStatus, jqXHR) {
                var res = JSON.parse(data);
                var status = res.status;
                var b = res.courses;
                var total = res.total;
                var html = '';
                
                if (total === 0) {
                    html += '<p class="warning-message"><?php echo JText::_('COM_SITEADMINBLN_NO_RESULTS_FOUND');?></p>';
                } else {
                    jQuery.each(b, function (key, value) {
                        var stt = b[key]['stt'];
                        var course_id = b[key]['remoteid'];
                        var fullname = b[key]['fullname'];
                        var course_image = b[key]['course_img'];
                        var owner = b[key]['owner'];


                        var url = '<?php echo JUri::base(); ?>index.php?option=com_siteadminbln&view=reports&action=courserp&rpid=' + course_id;
                        html += '<div class="report-item course_' + course_id + '" data-cid="' + course_id + '" onclick="showReportCourse(' + course_id + ')">' +
                            '<div class="report-item-content">' +
                            '<div class="clearfix"></div>' +
                            '<div class="item-img">' +
                            '<a href="' + url + '">' +
                            '<div class="image" id="images" style="background-image: url(\'' + course_image + '\')">' + '</div>'
                            +
                            '</a>' +
                            '</div>' +
                            '<div class="content_c_search">' +
                            '<div class="clearfix"></div>' +
                            '<div class="div_title"><a class="c_title" href="' + url + '">' + fullname + '</a></div>' +
                            '<div class="c_onwer" >' +
                            '' + owner + '' +
                            '</div>' +
                            '<div class="clearfix"></div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';

    //                    jQuery('.divLoadMore').remove();
    //                    jQuery('.list-courses-report').html(html);
                    });
                }
                jQuery('.list-courses-report').html(html);
                jQuery('.div_title').each(function () { 
                    if (jQuery(this).find('a.c_title').height() > 48) { 
                        jQuery(this).find('a').addClass('limited');
                    }
                });
//                if (s < n)
//                    cl = "";
//                else cl = "load";
                a = 1;
//                jQuery('.scoures_wrapper').removeClass('loadingggg');
                loading = false;
                total_pages = Math.ceil(total/20);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
                loading = false;
            }
        });
    }
    
    function showReportCourse(data) {
        window.location.href = '<?php echo JURI::base().'index.php?option=com_siteadminbln&view=reports&action=courserp&rpid=';?>'+data;
    }

    function searchUser(p) {
        var suser = jQuery('.userkeyword').val(); 
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: {
                act: "load_user",
                suser: suser,
                page: p
            },
            beforeSend: function () {

            },
            success: function (data, textStatus, jqXHR) { 
                var res = JSON.parse(data);
                var status = res.status;
                var b = res.users;
                var total = res.total;
                var html = '';
                
                if (total === 0) {
                    html += '<p class="warning-message"><?php echo JText::_('COM_SITEADMINBLN_NO_RESULTS_FOUND');?></p>';
                } else {
                    jQuery.each(b, function (key, value) {
                        var user_id = b[key]['id'];
                        var fullname = b[key]['name'];
                        var user_avatar = b[key]['avatar'];
                        var mdate = b[key]['date'];

                        html += '<div class="report-item user_' + user_id + '" data-cid="' + user_id + '" onclick="showReportUser(' + user_id + ', ' + mdate + ')">' +
                            '<div class="useritem-image">' +
                            '<img src="' + user_avatar + '" alt="' + fullname + '" title="' + fullname + '"/>' +
                            '</div>' +
                            '<div class="useritem-info">' +
                            '<div class="useritem-name"><p class="name">' + fullname + '</p></div>' +
                            '</div>' +
                            '</div>';

    //                    jQuery('.divLoadMore').remove();
    //                    jQuery('.list-courses-report').html(html);
                    });
                }
                jQuery('.list-users-report').html(html);
                
                jQuery('.useritem-name').each(function () {
                   if (jQuery(this).find('p.name').height() > 48) {
                       jQuery(this).find('p.name').addClass('limited');
                   }
                });
                u = 1;
                loading_u = false;
                total_pages_u = Math.ceil(total/20);
            },
            error: function (jqXHR, textStatus, errorThrown) { 
                console.log('ERRORS: ' + textStatus);
                loading_u = false;
            }
        });
    }
    
    function showReportUser(id, date) {
        window.location.href = '<?php echo JURI::base().'index.php?option=com_siteadminbln&view=reports&action=userrp&rpid=';?>'+id+'<?php echo '&date=';?>'+date;
    }
    
    function searchCircle(p) {
        var scircle = jQuery('.circlekeyword').val(); 
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: {
                act: "load_circle",
                scircle: scircle,
                page: p
            },
            beforeSend: function () {

            },
            success: function (data, textStatus, jqXHR) { 
                var res = JSON.parse(data);
                var status = res.status;
                var b = res.circles;
                var total = res.total;
                var html = '';
                
                if (total === 0) {
                    html += '<p class="warning-message"><?php echo JText::_('COM_SITEADMINBLN_NO_RESULTS_FOUND');?></p>';
                } else {
                    jQuery.each(b, function (key, value) {
                        var circle_id = b[key]['circle_id'];
                        var circle_original = b[key]['circle_original'];
                        var circle_name = b[key]['circle_name'];
                        var owner_name = b[key]['owner_name'];
                        var owner_avatar = b[key]['owner_avatar'];
                        var circle_avatar = b[key]['circle_avatar'];
                        var circle_created = b[key]['circle_created'];
                        var count_member = b[key]['count_member'];

                        html += '<div class="report-item circle_' + circle_id + '" data-cid="' + circle_id + '" onclick="showReportCircle(' + circle_id + ')">' +
                            '<div class="report-item-content">' +
                            '<div class="clearfix"></div>' +
                            '<div class="item-img">' +
                            '<div class="image" id="images" style="background-image: url(\'' + circle_original + '\')">' + '</div>' +
                            '</div>' +
                            '<div class="content_c_search">' +
                            '<div class="clearfix"></div>' +
                            '<div class="div_title"><span class="c_title">' + circle_name + '</span></div>' +
                            '<div class="c_onwer" >' + owner_name + '</div>' +
                            '<div class="clearfix"></div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                        html += '<div class="sabln-page-reportresult circleid_' + circle_id + '" style="display: none;">' +
                            '<div class="sabln-page-content">' +
                                '<div class="report-box left circle_info">' +
                                    '<span class="report-title"><?php echo JText::_('COM_REPORTS_CIRCLE_CIRCLE');?></span>' +
                                    '<div class="report-line">' +
                                        '<div class="icon-left"><img class="icon-circle" src="'+circle_avatar+'"/></div>' +
                                        '<div class="name-right"><span class="report-owner">'+circle_name+'</span></div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="report-box right circle_owner">' +
                                    '<span class="report-title"><?php echo JText::_('COM_REPORTS_CIRCLE_OWNER');?></span>'+
                                    '<div class="report-line">'+
                                        '<div class="icon-left"><img class="icon-owner" src="' + owner_avatar + '"/></div>'+
                                        '<div class="name-right"><span class="report-owner">'+owner_name+'</span></div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="report-box left circle_creationdate">'+
                                    '<span class="report-title"><?php echo JText::_('COM_REPORTS_COURSE_CREATION_DATE');?></span>'+
                                    '<span class="report-number">'+circle_created+'</span>'+
                                '</div>'+
                                '<div class="report-box right circle_nomembers">'+
                                    '<span class="report-title"><?php echo JText::_('COM_REPORTS_COURSE_NO_MEMBERS');?></span>'+
                                    '<div class="report-line">'+
                                        '<img class="icon-member" src="<?php echo JUri::base(); ?>images/icons/members_2x.png"/>'+
                                        '<span class="report-number">'+count_member+'</span>'+
                                    '</div>'+
                                '</div>' +
                            '</div>' +
                            '<div class="sabln-buttons">' +
                                '<button class="btn-download-report" onclick="downloadReport(\'circlereport\', ' + circle_id + ')"><?php echo JText::_('COM_REPORTS_BUTTON_REPORT');?></button>'+
                            '</div>' +
                            '</div>';

    //                    jQuery('.divLoadMore').remove();
    //                    jQuery('.list-courses-report').html(html);
                    });
                }
                jQuery('.list-circles-report').html(html);
                jQuery('.div_title').each(function () { 
                    if (jQuery(this).find('span.c_title').height() > 48) { 
                        jQuery(this).find('span').addClass('limited');
                    }
                });
                c = 1;
                loading_c = false;
                total_pages_c = Math.ceil(total/20);
            },
            error: function (jqXHR, textStatus, errorThrown) { 
                console.log('ERRORS: ' + textStatus);
                loading_c = false;
            }
        });
    }
    
    function showReportCircle(circleid) {
        loading_c = true;
        jQuery('.report-item').hide();
        jQuery('.sabln-search-box').hide();
        jQuery('.circleid_'+circleid).show();
    }
    
    function downloadReport(act, id) {
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: {act : act, rpid: id},
            // beforeSend: function() {
            //     jQuery('body').addClass('overlay2');
            //     jQuery('.notification').html('Loading...').fadeIn();
            // },
            success: function(data, textStatus, jqXHR) {
                var res = JSON.parse(data); 
                if (res.error === 0) {
                    jQuery('body').removeClass('overlay2');
                    jQuery('.notification').fadeOut();
//                            e.preventDefault();  //stop the browser from following
                    window.location.href = '<?php echo $app->getCfg('wwwrootfile').'/temp/';?>'+res.filename+'<?php echo '.xlsx?forcedownload=1'; ?>';
                } else {
                    jQuery('body').removeClass('overlay2');
                    jQuery('.notification').fadeOut();
                    lgtCreatePopup('oneButton', {
                        content: res.comment
                    },
                    function() {
                        lgtRemovePopup();
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
            }
        });
    }

</script>

