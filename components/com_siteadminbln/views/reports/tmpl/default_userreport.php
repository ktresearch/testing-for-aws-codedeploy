<?php
defined('_JEXEC') or die();

$mainframe = JFactory::getApplication();

$user = CFactory::getUser($this->userid);
?>
<div class="reports-menus-page" id="reports-menus-page">
    <div class="course sabln-page">
        <div class="sabln-page-header">
            <div class="sabln-back-icon" onclick="goBackUserReport();">
                <img src="<?php echo JUri::base(); ?>images/PreviousIcon.png">
            </div>
            <div class="sabln-page-name">
                <?php echo JText::_('COM_REPORTS_USER_REPORT');?>
            </div>
        </div>
        <div class="sabln-page-main">
            <div class="sabln-page-reportresult userid_<?php echo $user->id; ?>">
                <div class="sabln-page-content">
                    <div class="report-box left user_info">
                        <span class="report-title"><?php echo JText::_('COM_REPORTS_USER_USER');?></span>
                        <div class="report-line">
                            <div class="icon-left">
                                <img class="icon-owner" src="<?php echo $user->getAvatar(); ?>"/>
                            </div>
                            <div class="name-right">
                                <span class="report-owner"><?php echo $user->getDisplayName();?></span>
                            </div>
                        </div>
                    </div>
                    <div class="report-box right joined_date">
                        <span class="report-title"><?php echo JText::_('COM_REPORTS_USER_JOINED_DATE');?></span>
                        <span class="report-number"><?php echo date('m/d/Y',$this->datejoin);?></span>
                    </div>
                    <div class="report-box left sub-cricles">
                        <span class="report-title"><?php echo JText::_('COM_REPORTS_USER_NO_SUBCIRCLE');?></span>
                        <div class="report-line">
                            <span class="report-label"><?php echo JText::_('COM_REPORTS_USER_OWNED');?></span>
                            <span class="report-number"><?php echo $this->noOwnerSubCir;?></span>
                            <span class="report-label"><?php echo JText::_('COM_REPORTS_USER_JOINED');?></span>
                            <span class="report-number"><?php echo $this->noJoinSubCir;?></span>
                        </div>
                    </div>
                    <div class="report-box right user_course">
                        <span class="report-title"><?php echo JText::_('COM_REPORTS_USER_NO_COURSES');?></span>
                        <div class="report-line">
                            <span class="report-label"><?php echo JText::_('COM_REPORTS_USER_CREATED');?></span>
                            <span class="report-number"><?php echo count($this->userreport['courses_created']);?></span>
                            <span class="report-label"><?php echo JText::_('COM_REPORTS_USER_COMPLETED');?></span>
                            <span class="report-number"><?php echo count($this->userreport['courses_completed']);?></span>
                        </div>
                    </div>
                </div>
                <div class="sabln-buttons">
                    <button class="btn-download-report" onclick="downloadReport('userreport', <?php echo $user->id . ', ' .  $this->datejoin; ?>);"><?php echo JText::_('COM_REPORTS_BUTTON_REPORT');?></button>
                </div>
            </div>
        </div>
    </div>
    <div class="notification"></div>
</div>
<script type="text/javascript">
    function goBackUserReport() {
        window.location.href = "<?php echo JRoute::_('index.php?option=com_siteadminbln&view=reports#user', false);?>";
    }
    
    function downloadReport(act, userid, datejoin) {
        jQuery.ajax({
            url: window.location.href,
            type: 'POST',
            data: {act : act, rpid: userid, date: datejoin},
            beforeSend: function() {
                jQuery('body').addClass('overlay2');
                jQuery('.notification').html('Loading...').fadeIn();
            },
            success: function(data, textStatus, jqXHR) {
                var res = JSON.parse(data); 
                if (res.error === 0) {
                    jQuery('body').removeClass('overlay2');
                    jQuery('.notification').fadeOut();
//                            e.preventDefault();  //stop the browser from following
                    window.location.href = '<?php echo $mainframe->getCfg('wwwrootfile').'/temp/';?>'+res.filename+'.xlsx?forcedownload=1';
                } else {
                    jQuery('body').removeClass('overlay2');
                    jQuery('.notification').fadeOut();
                    lgtCreatePopup('oneButton', {
                        content: res.comment
                    },
                    function() {
                        lgtRemovePopup();
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
            }
        });
    }
</script>
