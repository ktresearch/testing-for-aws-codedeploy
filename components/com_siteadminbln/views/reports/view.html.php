<?php
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class SiteadminblnViewReports extends JViewLegacy {
    
    function display($tpl = null)
    {
        // Check permission
        require_once(JPATH_SITE.'/components/com_siteadminbln/models/account.php');
        require_once(JPATH_SITE.'/components/com_siteadminbln/models/reports.php');
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
        
        $user = JFactory::getUser();
        $groupsModel = CFactory::getModel('groups');
        
        $isSiteAdmin = false;
        $model = new SiteadminblnModelAccount();
        if (count($model->checkSiteAdminBLn($user->id)) > 0) {
            $isSiteAdmin = true;
        }
        if (!$isSiteAdmin) {
            header('Location: /404.html');
            exit;
//            echo '<div class="message">' . JText::_('COM_SITEADMINBLN_NO_ACCESS') . '</div>';
//            return;
        }
        
        $document = JFactory::getDocument();
        $document->addStyleSheet(JURI::root(true) . '/components/com_siteadminbln/css/reports.css');
        
        $act =  JRequest::getVar( 'act', null, 'NEWURLFORM' );
        if (!$act) $act =  JRequest::getVar( 'act' );
        
        $action =  JRequest::getVar( 'action', null, 'NEWURLFORM' );
        if (!$action) $action =  JRequest::getVar( 'action' );
         
        // Get BLN Report info
        $blninfo = $model->getInfobln();
        $this->BLNid = $blninfo->bln_circleid;
        
        $reportsModel = new SiteadminblnModelReports();
        
        if (isset($_POST['act']) && $_POST['act'] == 'networkreport') { // download Network Report
            
            $results = $reportsModel->dowloadNetworkReport($this->BLNid);               
            if ($results) {
                $result = array();
                $result['error'] = 0;
                $result['comment'] = JText::_('COM_REPORTS_DOWNLOAD_POPUP_SUCCESS');
                $result['filename'] = $results;
                echo json_encode($result);
                die;
            } else {
                $result = array();
                $result['error'] = 1;
                $result['comment'] = JText::_('COM_REPORTS_DOWNLOAD_POPUP_FAIL');
                $result['filename'] = '';
                echo json_encode($result);
                die;
            }
            
        } else if (isset($_POST['act']) && $_POST['act'] == 'coursereport') { // download Course Report
            $results = $reportsModel->dowloadCourseReport($_POST['rpid']);
            if ($results) {
                $result = array();
                $result['error'] = 0;
                $result['comment'] = JText::_('COM_REPORTS_DOWNLOAD_POPUP_SUCCESS');
                $result['filename'] = $results;
                echo json_encode($result);
                die;
            } else {
                $result = array();
                $result['error'] = 1;
                $result['comment'] = JText::_('COM_REPORTS_DOWNLOAD_POPUP_FAIL');
                $result['filename'] = '';
                echo json_encode($result);
                die;
            }
        } else if (isset($_POST['act']) && $_POST['act'] == 'userreport') { // download User Report
            $results = $reportsModel->dowloadUserReport($_POST['rpid'], $_POST['date']);
            if ($results) {
                $result = array();
                $result['error'] = 0;
                $result['comment'] = JText::_('COM_REPORTS_DOWNLOAD_POPUP_SUCCESS');
                $result['filename'] = $results;
                echo json_encode($result);
                die;
            } else {
                $result = array();
                $result['error'] = 1;
                $result['comment'] = JText::_('COM_REPORTS_DOWNLOAD_POPUP_FAIL');
                $result['filename'] = '';
                echo json_encode($result);
                die;
            }
        } else if (isset($_POST['act']) && $_POST['act'] == 'circlereport') { // download Circle Report
            $results = $reportsModel->dowloadCircleReport($_POST['rpid']);
            if ($results) {
                $result = array();
                $result['error'] = 0;
                $result['comment'] = JText::_('COM_REPORTS_DOWNLOAD_POPUP_SUCCESS');
                $result['filename'] = $results;
                echo json_encode($result);
                die;
            } else {
                $result = array();
                $result['error'] = 1;
                $result['comment'] = JText::_('COM_REPORTS_DOWNLOAD_POPUP_FAIL');
                $result['filename'] = '';
                echo json_encode($result);
                die;
            }
        }
        
        $limitstart = 0;
        $limit = 20;
        
        if (isset($_POST['act']) && $_POST['act'] == 'load_more') {
            if (isset($_POST['page'])) {
                $page = $_POST['page'];
                $limitstart = $page * $limit;
            }
            if (isset($_POST['scourse'])) {
                $scourse = $_POST['scourse'];
            } else {
                $scourse = '';
            }
            $coursesbln = JoomdleHelperContent::call_method('get_courses_bln', (int) $blninfo->bln_moodlecategoryid, $scourse, $limitstart, $limit);   
            if ($coursesbln['status']) {
                $list_search_course = $coursesbln["data"];
                foreach($list_search_course as $key => $curso ){
                    $course_id = $curso['remoteid'];
                    $list_search_course[$key]['remoteid']= $course_id;
                    $list_search_course[$key]['fullname']= $curso['fullname'];
                    $ownerid = JUserHelper::getUserId($curso['creator']);
                    $owner = CFactory::getUser($ownerid);
                    $list_search_course[$key]['owner']= $owner->getDisplayName();
                     $list_search_course[$key]['course_img']= $curso['course_img'];
                }
                $this->totalcourses = $coursesbln['totalcourses'];
            }
            $result = array();
            $result['courses'] = $list_search_course;
            $result['total'] = $coursesbln['totalcourses'];
            $result['status'] = $coursesbln['status'];
            echo json_encode($result);
            die;
        
        } else {
            $coursesbln = JoomdleHelperContent::call_method('get_courses_bln', (int) $blninfo->bln_moodlecategoryid, '', $limitstart, $limit); 
        }
        
        if (isset($_POST['suser'])) {
            $suser = $_POST['suser'];
        } else {
            $suser = '';
        } 
        $countusers = $model->getAlluser(TRUE, false, false, false, $suser);
        if (isset($_POST['act']) && $_POST['act'] == 'load_user') { 
            if (isset($_POST['page'])) {
                $page = $_POST['page'];
                $limitstart = $page * $limit;
            }
            $allusers = $model->getAlluser(TRUE, false, $limitstart, $limit, $suser);            
            $list_search_user = array();
            if ($allusers) {
                $i = 0;
                foreach($allusers as $key => $us ){
                    $user_id = $us->id;
                    $userm = CFactory::getUser($user_id);

                    $list_search_user[$i]['id']= $user_id;
                    $list_search_user[$i]['name']= $userm->getDisplayName();
                    $list_search_user[$i]['date']= $us->date;
                    $list_search_user[$i]['avatar']= $userm->getAvatar();
                    $i++;
                }
            }

            $result = array();
            $result['users'] = $list_search_user;
            $result['total'] = count($countusers);
            $result['status'] = true;       
            echo json_encode($result);
            die;

        } else {
            $allusers = $model->getAlluser(TRUE, false, $limitstart, $limit, $suser);
        }
        
        if (isset($_POST['scircle'])) {
            $scircle = $_POST['scircle'];
        } else {
            $scircle = '';
        } 
        $circlescount = $groupsModel->getAllCircleBLN((int)$this->BLNid, false, false, $scircle,true);
        if (isset($_POST['act']) && $_POST['act'] == 'load_circle') { 
            if (isset($_POST['page'])) {
                $page = $_POST['page'];
                $limitstart = $page * $limit;
            }
            $circlesBLN = $groupsModel->getAllCircleBLN((int)$this->BLNid, $limitstart, $limit, $scircle,true);          
            $list_search_circle = array();
            if ($circlesBLN) {
                $i = 0;
                foreach($circlesBLN as $circle ){
                    $table =  JTable::getInstance( 'Group' , 'CTable' );
                    $table->load($circle->id);
                    $ownercircle = CFactory::getUser($circle->ownerid);

                    $list_search_circle[$i]['circle_id']= $circle->id;
                    $list_search_circle[$i]['circle_original']= $table->getOriginalAvatar().'?'.(time()*1000);
                    $list_search_circle[$i]['circle_name']= $circle->name;
                    $list_search_circle[$i]['owner_name']= $ownercircle->getDisplayName();
                    $list_search_circle[$i]['owner_avatar']= $ownercircle->getAvatar();
                    $list_search_circle[$i]['circle_avatar']= $table->getAvatar();
                    $list_search_circle[$i]['circle_created']= date('m/d/Y', strtotime($circle->created));
                    $list_search_circle[$i]['count_member']= $groupsModel->getMembersCount($circle->id, true);
                    $i++;
                }
            }

            $result = array();
            $result['circles'] = $list_search_circle;
            $result['total'] = count($circlescount);
            $result['status'] = true;       
            echo json_encode($result);
            die;

        } else {
            $circlesBLN = $groupsModel->getAllCircleBLN((int)$this->BLNid, $limitstart, $limit, $scircle,true);            
        }
        
        switch ($action) {
            case 'courserp':
                $courseinfo = array();
                $courseid =  JRequest::getVar( 'rpid', 0, 'NEWURLFORM' );
                if (!$courseid) $courseid =  JRequest::getVar( 'rpid' );
                if (isset($courseid)) {                 
                    $coursereport = JoomdleHelperContent::call_method('get_course_report', (int)$courseid); 
                    if ($coursereport['status']) {
                        $courserp = $coursereport['data'];  
//                        foreach ($courserp['courseinfo'] as $course) {                         
//                            if ($course['remoteid'] == $courseid) {
                                $courseinfo = $courserp['courseinfo'];
//                            }
//                        }
                    }
                }
                $this->courseinfo = $courseinfo;
                
                parent::display('coursereport');
                break;
            case 'userrp':
                $userinfo = array();
                $userid =  JRequest::getVar( 'rpid', 0, 'NEWURLFORM' );
                if (!$userid) $userid =  JRequest::getVar( 'rpid' );
                
                $date =  JRequest::getVar( 'date', 0, 'NEWURLFORM' );
                if (!$date) $date =  JRequest::getVar( 'date' );
                if (isset($userid)) {   
                    $userr = JFactory::getUser($userid);
                    $userreport = JoomdleHelperContent::call_method('get_user_report', $userr->username); 
                    if ($userreport['status']) {
                        $this->userreport = $userreport['data'];
                    }
                }
                $this->userid = $userid;
                $this->datejoin = $date;
                
                $this->noOwnerSubCir = $groupsModel->getCirclesCount($userid, 0, 'owner', true,true);
                $this->noJoinSubCir = $groupsModel->getCirclesCount($userid, 0, 'member', true,true);
                
                parent::display('userreport');
                break;
            default:
                $blnreports = $reportsModel->getNetworkReport((int)$this->BLNid);
                $this->blnreport = $blnreports;
 
                if ($coursesbln['status']) {
                    $this->blncourses = $coursesbln['data'];
                    $this->totalcourses = $coursesbln['totalcourses'];
                }

                $this->totalusers = count($countusers);
                $this->allusers = $allusers;

                $this->totalcircles = count($circlescount);
                $this->blncircles = $circlesBLN;

                // do nothing display
                parent::display($tpl);
                break;
        }
    
    }
    
}
