<?php

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class SiteadminblnViewAddusers extends JViewLegacy {

    function display($tpl = null) {
        $app = JFactory::getApplication();
        $jinput = $app->input;

        // Check permission
        require_once(JPATH_SITE . '/components/com_siteadminbln/models/account.php');
        $user = JFactory::getUser();

        $session = JFactory::getSession();

        require_once(JPATH_SITE . '/components/com_userapi/controller.php');
        $userapi = new UserapiController();

        $isSiteAdmin = false;
        $model = new SiteadminblnModelAccount();
        if (count($model->checkSiteAdminBLn($user->id)) > 0) {
            $isSiteAdmin = true;
        }
        if (!$isSiteAdmin) {
            header('Location: /404.html');
            exit;
//            echo '<div class="message">' . JText::_('COM_SITEADMINBLN_NO_ACCESS') . '</div>';
//            return;
        }

        $act = JRequest::getVar('action', null, 'NEWURLFORM');
        if (!$act)
            $act = JRequest::getVar('action');

        $skipactivation = $jinput->get('sa', 1, 'POST');

        if (isset($_POST['act']) && $_POST['act'] == 'sendcsv') {
//            $this->sendCSV($_POST['fname'], $_POST['tmpfile']);
            
            $useduser = $model->getInfosubscription();
            $attackment = dirname($app->getCfg('dataroot')) . '/temp/' . $_POST['tmpfile'];
            $users = array();
            $number = 0;
            require_once("./courses/lib/phpexcel/PHPExcel/IOFactory.php");
            $tmpfname = $attackment;
            $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
            $excelObj = $excelReader->load($tmpfname);
            $worksheet = $excelObj->getSheet(0);
            $lastRow = $worksheet->getHighestRow();

           for ($i = 3; $i <= $lastRow; $i++) {
               if (!empty($worksheet->getCell('C'.$i)->getValue()))
                    $number++;
            }
            if ($number <= $useduser->total_empty) {

                require_once(JPATH_ROOT . '/components/com_userapi/models/userapi.php');   
                require_once(JPATH_BASE . '/components/com_community/libraries/core.php');

                $model_userapi = new UserapiModelUserapi();
                // Set data session
                $tmpSession = [];

                for ($row = 3; $row <= $lastRow; $row++) {
                    $firstname = $worksheet->getCell('A'.$row)->getValue();
                    $lastname = $worksheet->getCell('B'.$row)->getValue();
                    $username = strtolower($worksheet->getCell('C'.$row)->getValue());
                    $password = $worksheet->getCell('D'.$row)->getValue();
                    // Check email exist
                    if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
                        if (!empty($firstname) && !empty($lastname) && !empty($username) && !$model_userapi->checkEmailExits($username) && !empty($password)) {

                            $tmpUser = new stdClass();

                            $tmpUser->username = $username;
                            $tmpUser->name = $firstname.' '. $lastname;
                            $tmpUser->username = $username;
                            $tmpUser->email = $username;
                            $tmpUser->password = (string) $password;

                            $user_id = $userapi->createUser($tmpUser, 'signup', $skipactivation);
                            if ($user_id) {
                                // Set data 
                                $data = [
                                    'firstname' => $firstname,
                                    'lastname' => $lastname,
                                    'username' => $username,
                                    'email' => $username,
                                ];
                                // Get BLN root
                                // $groupsModel = CFactory::getModel('groups');
                                // $BLNCircle = $groupsModel->getBLNCircle();

                                // $userapi->syncUserToGLN($data, $password, $BLNCircle->id);

                                // Add BLN circle root
                                // if (!empty($BLNCircle)) {
                                //     $groupsModel->addUserToCircle($user_id, $BLNCircle->id);
                                // }
                                $tmpSession[] = [
                                    'user_id' => $user_id,
                                    'data' => $data,
                                    'password' => $password
                                ];

                            }
                        }
                    }
                }
                
                if (!empty($tmpSession)) {
                    $session->set('list_user', $tmpSession);
                }
                $error = 1;
                $comment = JText::_('COM_SITEADMINBLN_SEND_CSV_SUCCESS');

                /*
                for ($row = 0; $row <= $lastRow; $row++) {
                    $username = strtolower($worksheet->getCell('C'.$row)->getValue());
                    if (!$model->checkEmailExits($worksheet->getCell('C'.$row)->getValue())) {
                        $tmpUser = array();
                        $tmpUser['firstname'] = $worksheet->getCell('A'.$row)->getValue();
                        $tmpUser['lastname'] =  $worksheet->getCell('B'.$row)->getValue();
                        $tmpUser['username'] = $worksheet->getCell('C'.$row)->getValue();
                        $tmpUser['password'] = $worksheet->getCell('D'.$row)->getValue();
                            $add = $userapi->signupauto($tmpUser);
                            $error = 1;
                            $comment = JText::_('COM_SITEADMINBLN_SEND_CSV_SUCCESS');
                    }
                }
                */
            }
            else {
                    $error = 2;
                    $comment = JText::_("COM_SITEADMINBLN_ADD_ONE_USERS_ERROR");
                }
            if (!empty($_POST['tmpfile']))
                unlink(dirname($app->getCfg('dataroot')) . '/temp/' . $_POST['tmpfile']);
            $result = array();
            
            $result['error'] = $error;
            $result['comment'] = $comment;
            echo json_encode($result);
            die;
        } else if (!empty($_FILES) && $_POST['act'] == 'addfile') {
//            $zipMIMEType = array('application/zip', 'application/x-zip', 'application/x-zip-compressed' , 'multipart/x-zip' , 'application/x-compressed');
            $zipType = array('xls','xlsx');
            $file_name = $_FILES[0]['name'];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            
            if ($_FILES[0]['size'] > 100 * 1024 * 1024) {
                $result['error'] = 1;
                $result['comment'] = 'File size is too large (< 100MB)';
            } else if (strlen($_FILES[0]['name']) > 250) {
                $result['error'] = 1;
                $result['comment'] = 'File name is too long (< 250char)';
            } else if (!in_array($ext, $zipType)) {
                $result['error'] = 1;
                $result['comment'] = 'Type of this file is invalid.';
            } else {
                $tmpFileName = $file_name;
                $tmp_file = dirname($app->getCfg('dataroot')) . '/temp/' . $tmpFileName;
                file_put_contents($tmp_file, file_get_contents($_FILES[0]['tmp_name']));

                $result = array();
                $result['error'] = 0;
                $result['comment'] = 'Everything is fine';
            }
            $result['data'] = $_FILES;
            $result['data'][0]['tmp_file'] = $tmpFileName;
            echo json_encode($result);
            die;
        }

        switch ($act) {
            case 'quickadd':
                if (isset($_POST['act']) && ($_POST['act'] == 'addnew')) {
                    $firstname = $jinput->post->get('firstname', '', 'NONE');
                    $lastname = $jinput->post->get('lastname', '', 'NONE');
                    $email = $jinput->post->get('email', '', 'NONE');
                    $password = $jinput->post->get('password', '', 'NONE');
                    $password2 = $jinput->post->get('password2', '', 'NONE');

                    $tmpUser = new stdClass();
                    $tmpUser->username = $email;
                    $tmpUser->password = $password;
                    $tmpUser->password2 = $password2;
                    $tmpUser->firstname = $firstname;
                    $tmpUser->lastname = $lastname;

                    $user_id = $userapi->createUser($tmpUser, 'signup', $skipactivation);

                    $result = array();
                    $result['error'] = 0;
                    $result['comment'] = 'Nice';
                    $result['data'] = $user_id;
                    echo json_encode($result);
                    die;
                }

                parent::display('quickadd');
                break;
            case 'chartadd':
                $this->info = $model->getInfosubscription();
                $this->text = JText::_("COM_SITEADMINBLN_QUICK_ADDS_SUCCESS_TEXT");
                $this->add = 1;
                parent::display('chart');
                break;
            case 'chartaddbulk':
                $blninfo = $model->getInfobln();
                $this->blnid = $blninfo->id;

                $this->info = $model->getInfosubscription();
                $this->text = JText::_("COM_SITEADMINBLN_QUICK_ADDS_SUCCESS_TEXT");
                $this->add = 1;
                parent::display('chart');
                break;
            case 'acreate':
                
                $blninfo = $model->getInfobln();
                $this->blnid = $blninfo->id;
                $this->info = $model->getInfosubscription();
                $this->text = JText::_('COM_SITEADMINBLN_QUICK_ADD_SUCCESS_TEXT');
                $this->add = 2;
                parent::display('chart');
                break;
            case 'chartremove':
                $this->info = $model->getInfosubscription();
                $this->text = JText::_("COM_SITEADMINBLN_SEND_CSV_REMOVES_SUCCESS");
                $this->remove = 1;
                parent::display('chart');
                break;
             case 'chartremovebulk':
                $this->info = $model->getInfosubscription();
                $this->text = JText::_("COM_SITEADMINBLN_SEND_CSV_REMOVES_SUCCESS");
                $this->remove = 1;
                parent::display('chart');
                break;
            case 'aremove':
                $this->info = $model->getInfosubscription();
                $this->text = JText::_("COM_SITEADMINBLN_SEND_CSV_REMOVE_SUCCESS");
                $this->remove = 3;
                parent::display('chart');
                break;
            default:
                parent::display($tpl);
                break;
        }
    }

    /**
     * Private method to create a user in the site.
     * */
    private function sendCSV($filename, $filetmp) {
        $app = JFactory::getApplication();
        $user = JFactory::getUser();

        require_once(JPATH_SITE . '/components/com_siteadminbln/models/account.php');
        $model = new SiteadminblnModelAccount();
        $blninfo = $model->getInfobln();

        $mailfrom = 'support@parenthexis.com';
        $fromname = $user->username;
        $mailto = 'business@parenthexis.com';
        $subject = JText::_('COM_SITEADMINBLN_SENDCSV_ADDUSER_SUBJECT');
        $message = JText::sprintf('COM_SITEADMINBLN_SENDCSV_ADDUSER_BODY', $blninfo->organisation_name);
        $attackment = dirname($app->getCfg('dataroot')) . '/temp/' . $filetmp;

        ob_start();
        $mail = JFactory::getMailer();
        $mail->sendMail($mailfrom, $fromname, $mailto, $subject, $message, false, null, null, $attackment);
        ob_end_clean();
    }

}
