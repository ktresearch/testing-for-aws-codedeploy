<?php
defined('_JEXEC') or die('Restricted access');

?>
<div class="quickadd-page">
    <div class="manusers-bar">
        <div class="manusers-title">
            <span class="back-icon" onclick="goBack();"></span>
            <span class="title-page"><?php echo JText::_('COM_SITEADMINBLN_USER_MANAGEMENT');?></span>
        </div>
        <div class="manusers-tab">
            <a class="active" href="<?php echo JRoute::_("index.php?option=com_siteadminbln&view=addusers");?>"><?php echo JText::_('COM_SITEADMINBLN_ADD_USERS');?></a>
            <a class="" href="<?php echo JRoute::_("index.php?option=com_siteadminbln&view=removeusers");?>"><?php echo JText::_('COM_SITEADMINBLN_REMOVE_USERS');?></a>
        </div>
    </div>
    <div class="quickadd-content">
        <div class="add-users-quick-add sabln-um-page">
            <div class="quickadd-label">
                <span><?php echo JText::_('COM_SITEADMINBLN_QUICK_ADD_LABEL');?></span><span class="red">*</span>
            </div>
            <form id="quickadd_users" class="quickadd_users_form" method="POST" action="<?php echo JURI::base().'index.php?option=com_siteadminbln&view=addusers&action=acreate';?>" enctype="multipart/form-data">
                <input class="first-name" name="first_name" id="first_name" type="text" placeholder="<?php echo JText::_('COM_SITEADMINBLN_FIRST_NAME');?>"/>
                <input class="last-name" name="last_name" id="last_name" type="text" placeholder="<?php echo JText::_('COM_SITEADMINBLN_LAST_NAME');?>"/>
                <input class="email" name="email" id="email" type="email" placeholder="<?php echo JText::_('COM_SITEADMINBLN_EMAIL');?>"/>
                <input class="password" name="password" id="password" type="password" placeholder="<?php echo JText::_('COM_SITEADMINBLN_PASSWORD');?>"/>
                <input class="re-password" name="repassword" id="repassword" type="password" placeholder="<?php echo JText::_('COM_SITEADMINBLN_REENTER_PASSWORD');?>"/>

                <div class="addusers-note">
                    <span><?php echo JText::sprintf('COM_SITEADMINBLN_QUICK_ADD_NOTE', JRoute::_('index.php?option=com_community&view=frontpage&task=terms?banner=2'),
                            JRoute::_('index.php?option=com_community&view=frontpage&task=privacy_policy?banner=2'));?></span>
                </div>

                <div class="quickadd-buttons pull-right row">
                    <button class="btCancel" type="button" onclick="goBack()"><?php echo Jtext::_('MYCOURSE_BTN_CANCEL'); ?></button>
                    <button class="btSave"><?php echo Jtext::_('COM_SITEADMINBLN_BUTTON_CREATE'); ?></button>
                </div>
            </form>
        </div>
        <div class="add-users-quick-add-success sabln-um-page hidden">
            <p class="qa-success-text"><?php echo JText::_('COM_SITEADMINBLN_QUICK_ADD_SUCCESS_TEXT') ?></p>
            <div class="qa-success-buttons">
                <button class="btn-success-back" onclick="goBack()"><?php echo JText::_('COM_SITEADMINBLN_BACK') ?></button>
            </div>
        </div>
    </div>
</div>
<script>
    (function ($) {
        jQuery('.quickadd_users_form .btSave').click(function (e) {
            e.preventDefault();

            var em = jQuery('.email').val();
            if (em != em.toLowerCase()) {
//                lgtCreatePopup('confirm', {
//                    'yesText': 'Save',
//                    'content': 'Email address contains some capital letters. If you keep saving, system will store your email address in lowercase form.'
//                }, function() {
//                });

                jQuery('.email').val(em.toLowerCase());
            }
            saveQuickAdd();
        });

        function saveQuickAdd() {
            var check = true;
            var firstname = jQuery('.first-name'), lastname = jQuery('.last-name'), email = jQuery('.email'), 
                password = jQuery('.password'), repassword = jQuery('.re-password');
            if (firstname.val().trim() == '') {
                firstname.addClass('error');
                check = false;
            }
            if (lastname.val().trim() == '') {
                lastname.addClass('error');
                check = false;
            }
            if (email.val().trim() == '') {
                email.addClass('error');
                check = false;
            }
            if (password.val().trim() == '') {
                password.addClass('error');
                check = false;
            }
            if (repassword.val().trim() == '') {
                repassword.addClass('error');
                check = false;
            }
            if (password.val().trim() != '' && repassword.val().trim() != '' && password.val() != repassword.val()) {
                password.addClass('error');
                repassword.addClass('error');
                check = false;
            }
            if (check == false) {
                return false;
            } else {
                jQuery.ajax({
                    url: '<?php echo JRoute::_('index.php?option=com_userapi&task=signup')?>',
                    type: 'POST',
                    data: {
                        act: 'addnew',
                        firstname: firstname.val(),
                        lastname: lastname.val(),
                        email: email.val(),
                        password: password.val(),
                        repassword: repassword.val(),
                        sa: 1
                    },
                    beforeSend: function () {
                        lgtCreatePopup('', {content: 'Loading...'});
                    },
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        lgtRemovePopup();

                        if (data.status) {

//                            jQuery('.add-users-quick-add').toggleClass('hidden');
//                            jQuery('.add-users-quick-add-success').toggleClass('hidden');
                        jQuery( "form#quickadd_users" ).submit();
//                            window.location.href = "<?php //echo JURI::base().'index.php?option=com_siteadminbln&view=addusers&action=acreate';?>";

                        } else {
                            lgtCreatePopup('oneButton', {content: data.error_message}, function () {
                                lgtRemovePopup();
                            });
                        }
                    },
                    error: function (data, textStatus, jqXHR) {
                        console.log(data);
                        lgtRemovePopup();
                        lgtCreatePopup('oneButton', {content: 'Fail.'}, function () {
                            lgtRemovePopup();
                        });
                    }
                });
            }
        }
        
        jQuery('.first-name, .last-name, .email, .password, .re-password').on('keyup', function () {
            jQuery(this).removeClass('error');
        });
    })(jQuery);

//    function goBack() {
//        // window.history.back();
//        var nav = window.navigator;
//        if( this.phonegapNavigationEnabled &&
//            nav &&
//            nav.app &&
//            nav.app.backHistory ){
//            nav.app.backHistory();
//        } else {
//            window.history.back();
//        }
//    }
    function goBack(){
        window.location.href = '<?php echo JRoute::_('index.php?option=com_siteadminbln&view=addusers'); ?>';
    }
    
//    function validateEmail(sEmail) {
//        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
//        if (filter.test(sEmail)) {
//            return true;
//        }
//        else {
//            return false;
//        }
//    }​

</script>