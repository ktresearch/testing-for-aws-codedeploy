<?php
defined('_JEXEC') or die();

$session = JFactory::getSession();
$device = $session->get('device');
if ($device == 'mobile') $dev = 1;

$jinput = JFactory::getApplication()->input;
$first_name = str_replace("'", "\'", $jinput->get('first_name', '', 'POST')); 
$last_name = str_replace("'", "\'", $jinput->get('last_name', '', 'POST')); 
$email = $jinput->get('email', '', 'POST');
$password = $jinput->get('password', '', 'POST');
$repassword = $jinput->get('repassword', '', 'POST');

?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.js"></script>
<div class="userchart">
    <div class="manusers-bar">
        <div class="manusers-title">
            <span class="back-icon" onclick="backPage();"></span>
            <span class="title-page"><?php echo JText::_('COM_SITEADMINBLN_USER_MANAGEMENT');?></span>
        </div>
        <div class="manusers-tab">
            <a class="<?php echo ($this->add) ? 'active' : '' ?>" href="<?php echo JRoute::_("index.php?option=com_siteadminbln&view=addusers");?>"><?php echo JText::_('COM_SITEADMINBLN_ADD_USERS');?></a>
            <a class="<?php echo ($this->remove) ? 'active' : '' ?>" href="<?php echo JRoute::_("index.php?option=com_siteadminbln&view=removeusers");?>"><?php echo JText::_('COM_SITEADMINBLN_REMOVE_USERS');?></a>
        </div>
    </div>
    <div class="header"><p><?php echo $this->text;?></p></div>
    <div class="chartSub">
        <div class="circleUser">
            <canvas id="doughnut" width="290" height="290"></canvas>
        </div>
        <img class="hidden" width = "30" height="30" id="addimg" src ="images/Friends_1x.png" alt='minh' >
    </div>
    <div class="qa-success-buttons">
                <button class="btn-success-back" onclick="backPage()"><?php echo JText::_('COM_SITEADMINBLN_BACK') ?></button>
            </div>
</div>

<script type="text/javascript">
    
  var page = '<?php echo ($this->add) ? $this->add : $this->remove  ?>';
    function backPage(){
        if(page == 2)
            window.location = "<?php echo JRoute::_('index.php?option=com_siteadminbln&view=addusers&action=quickadd', false);?>";
        else if(page == 3)
             window.location = "<?php echo JRoute::_('index.php?option=com_siteadminbln&view=removeusers&action=quickremove', false);?>";
        else window.history.back();
    }
    var device = '<?php echo $dev; ?>';
    Chart.types.Doughnut.extend({
        name: "doughnutAlt",
        draw: function () {
            Chart.types.Doughnut.prototype.draw.apply(this, arguments);

            var width = this.chart.width,
                    height = this.chart.height;

            var fontSize = (height / 114).toFixed(2);
            this.chart.ctx.font = 'normal 16px "Open Sans", "OpenSans", sans-serif';
            this.chart.ctx.textAlign = 'center';
            this.chart.ctx.textBaseline = 'middle';
            this.chart.ctx.fillStyle = '#484848';
            this.chart.ctx.save();
            this.chart.ctx.beginPath();

            var img = new Image();
            img.src = "/images/Friends_1x.png";

            if (device)
                this.chart.ctx.drawImage(img, 110, 65);
            else
                this.chart.ctx.drawImage(img, 120, 75);

            if (device)
                this.chart.ctx.fillText('Current usage', 130, 125);
            else
                this.chart.ctx.fillText('Current usage', 140, 135);
            this.chart.ctx.font = 'normal 40px "Open Sans", "OpenSans", sans-serif';
            this.chart.ctx.textAlign = 'center';
            this.chart.ctx.textBaseline = 'middle';
            this.chart.ctx.fillStyle = '#126DB6';
            this.chart.ctx.save();
            this.chart.ctx.beginPath();
            var text = '<?php echo $this->info->total_use; ?>';
            if (device)
                this.chart.ctx.fillText(text, 130, 155);
            else
                this.chart.ctx.fillText(text, 140, 165);
            this.chart.ctx.font = 'normal 16px "Open Sans", "OpenSans", sans-serif';
            this.chart.ctx.textAlign = 'center';
            this.chart.ctx.textBaseline = 'middle';
            this.chart.ctx.fillStyle = '#484848';
            this.chart.ctx.save();
            this.chart.ctx.beginPath();
            var text = 'Unused ' + '<?php echo $this->info->total_empty; ?>'
            if (device)
                this.chart.ctx.fillText(text, 130, 195);
            else
                this.chart.ctx.fillText(text, 140, 210);
        }
    });

    var chartOptions = {
        onAnimationComplete: function () {
            // Always show Tooltip
            this.showTooltip(this.segments, true);
        },
        customTooltips: function (tooltip) {
            // Tooltip Element
            var tooltipEl = $('#chartjs-tooltip');
            // Hide if no tooltip
            if (!tooltip) {
                tooltipEl.css({
                    opacity: 1
                });
                return;
            }
            // Set caret Position
            tooltipEl.removeClass('above below');
            tooltipEl.addClass(tooltip.yAlign);
            tooltipEl.addClass(tooltip.xAlign);
            // Set Text
            tooltipEl.html(tooltip.text);
            // Find Y Location on page
            var top;
            if (tooltip.yAlign == 'above') {
                top = tooltip.y - tooltip.caretHeight - tooltip.caretPadding;
            } else {
                top = tooltip.y + tooltip.caretHeight + tooltip.caretPadding;
            }
            // Display, position, and set styles for font
            tooltipEl.css({
                opacity: 0,
                left: tooltip.chart.canvas.offsetLeft + tooltip.x + 'px',
                top: tooltip.chart.canvas.offsetTop + top + 'px',
                fontFamily: tooltip.fontFamily,
                fontSize: tooltip.fontSize,
                fontStyle: tooltip.fontStyle,
                xOffset: tooltip.xOffset,
            });
        },
        tooltipEvents: [], // Remove to enable Default Mouseover events
        tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
        tooltipFillColor: "rgba(0,0,0,0.0)",
        tooltipFontColor: "#505050",
        tooltipFontSize: 34,
        tooltipXOffset: 0,
        tooltipXPadding: 0,
        tooltipYPadding: 0,
//    tooltipTemplate: "<%= value %>%",
        legends: true,
        showTooltips: true,
        segmentShowStroke: false,
        percentageInnerCutout: 65,
        animationEasing: "easeInOutQuart",
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    }

    var data = [{
            value: <?php echo $this->info->total_use; ?>,
            color: "#126DB6",
            label: "Data 1"
        }, {
            value: <?php echo $this->info->total_empty; ?>,
            color: "#DBECF8",
            label: "Data 2"
        }, ];
    var doughnutChart = new Chart(document.getElementById("doughnut").getContext("2d")).doughnutAlt(data, chartOptions);
    // document.getElementById('doughnut-legend').innerHTML = doughnutChart.generateLegend();


    $(document).ready(function() {
        if(page == 2){
            jQuery.ajax({
                url: '<?php echo JRoute::_('index.php?option=com_userapi&task=syncUserToGLNT')?>',
                type: 'POST',
                data: {
                    firstname: '<?php echo $first_name; ?>',
                    lastname: '<?php echo $last_name;?>',
                    email: '<?php echo $email; ?>',
                    password: '<?php echo $password; ?>',
                    repassword: '<?php echo $repassword; ?>',
                    blnid: '<?php echo $this->blnid;?>'
                },
                beforeSend: function () {
    //                        lgtCreatePopup('', {content: 'Loading...'});
                },
                success: function (result, textStatus, jqXHR) {
                                        },
                error: function (data, textStatus, jqXHR) {
                    console.log(data);
                    lgtRemovePopup();
                    lgtCreatePopup('oneButton', {content: 'Fail.'}, function () {
                        lgtRemovePopup();
                    });
                }
            });
        }

        <?php if ($session->has('list_user') && !empty($session)) :?>
                jQuery.ajax({
                url: '<?php echo JRoute::_('index.php?option=com_userapi&task=syncUserToGLN')?>',
                type: 'POST',
                data: {
                    data: '<?php echo mysql_escape_string(json_encode($session->get('list_user'))); ?>',
                    blnid: '<?php echo $this->blnid;?>'
                },
                async: true,
                success: function (result, textStatus, jqXHR) {
                                        },
                error: function (data, textStatus, jqXHR) {
                    console.log(data);
                    lgtRemovePopup();
                    lgtCreatePopup('oneButton', {content: 'Fail.'}, function () {
                        lgtRemovePopup();
                    });
                }
            });
                // Clear session
            <?php    
                $session->clear('list_user');
                endif;
            ?>
    });
//    
    
    
</script>
