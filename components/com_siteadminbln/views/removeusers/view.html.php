<?php

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class SiteadminblnViewRemoveusers extends JViewLegacy {
    
    function display($tpl = null)
    {
        // $db1 = JFactory::getDbo(2);
        // $query1 = "SELECT * FROM".$db1->quoteName('#__user')." WHERE ". $db1->quoteName('username') .'=' .$db1->Quote('ly@gmail.com');
        // $db1->setQuery($query1);
        // var_dump($db1->loadResult());

        $app = JFactory::getApplication();
        $jinput = $app->input;

        // Check permission
        require_once(JPATH_SITE.'/components/com_siteadminbln/models/account.php');
        
        $user = JFactory::getUser();
        $isSiteAdmin = false;
        $model = new SiteadminblnModelAccount();

        if (count($model->checkSiteAdminBLn($user->id)) > 0) {
            $isSiteAdmin = true;
        }
        if (!$isSiteAdmin) {
            header('Location: /404.html');
            exit;
//            echo '<div class="message">' . JText::_('COM_SITEADMINBLN_NO_ACCESS') . '</div>';
//            return;
        }
        
        $act =  JRequest::getVar( 'action', null, 'NEWURLFORM' );
        if (!$act) $act =  JRequest::getVar( 'action' );

        if (isset($_POST['act']) && $_POST['act'] == 'sendcsv') {
            $blninfo = $model->getInfobln();

//            $this->sendCSV($_POST['fname'], $_POST['tmpfile']);
            $attackment = dirname($app->getCfg('dataroot')) . '/temp/' . $_POST['tmpfile'];
            $users = array();
            require_once("./courses/lib/phpexcel/PHPExcel/IOFactory.php");
            $tmpfname = $attackment;
            $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
            $excelObj = $excelReader->load($tmpfname);
            $worksheet = $excelObj->getSheet(0);
            $lastRow = $worksheet->getHighestRow();
            
             for ($row = 3; $row <= $lastRow; $row++) {
                $email = $worksheet->getCell('A'.$row)->getValue();

                  if(!empty($email)) {
                    $user = $model->getByEmail($email);
                    
                    if ($user && ((int)$user[0]->id != (int)$blninfo->bln_owner) && ((int)$user[0]->id != (int)$blninfo->bln_admin)){
                        $model->deleteUserBLN((int)$user[0]->id);
                    }
                }
             }
            if (!empty($_POST['tmpfile'])) unlink(dirname($app->getCfg('dataroot')).'/temp/'.$_POST['tmpfile']);
            $result = array();
            $result['error'] = 0;
            $result['comment'] = JText::_('COM_SITEADMINBLN_SEND_CSV_SUCCESS');
            echo json_encode($result);
            die;

        } else if (isset($_POST['act']) && $_POST['act'] == 'quickremove') { // remove user
            require_once(JPATH_BASE . '/components/com_siteadminbln/models/account.php');
            $model = new SiteadminblnModelAccount();
            
            if ($_POST['uid'] > 0) {
                $results = $model->deleteUserBLN($_POST['uid']);
                if ($results) {
                    $result = array();
                    $result['error'] = 0;
                    $result['comment'] = JText::_('COM_SITEADMINBLN_REMOVE_USER_POPUP_SUCCESS');
                    echo json_encode($result);
                    die;
                } else {
                    $result = array();
                    $result['error'] = 1;
                    $result['comment'] = JText::_('COM_SITEADMINBLN_REMOVE_USER_POPUP_FAIL');
                    echo json_encode($result);
                    die;
                }
            } else {
                $result = array();
                $result['error'] = 1;
                $result['comment'] = JText::_('COM_SITEADMINBLN_REMOVE_USER_POPUP_FAIL1');
                echo json_encode($result);
                die;
            }
            
        } else if (!empty($_FILES) && $_POST['act'] == 'addfile') {
//            $zipMIMEType = array('application/zip', 'application/x-zip', 'application/x-zip-compressed' , 'multipart/x-zip' , 'application/x-compressed');
            $zipType = array('xls','xlsx');
            $file_name = $_FILES[0]['name'];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if ($_FILES[0]['size'] > 100*1024*1024) {
                $result['error'] = 1;
                $result['comment'] = 'File size is too large (< 100MB)';
            } else if (strlen($_FILES[0]['name']) > 250) {
                $result['error'] = 1;
                $result['comment'] = 'File name is too long (< 250char)';
            } else if (!in_array($ext, $zipType)) {
                $result['error'] = 1;
                $result['comment'] = 'Type of this file is invalid.';
            } else {
                $tmpFileName = $file_name;
                $tmp_file = dirname($app->getCfg('dataroot')).'/temp/'.$tmpFileName;
                file_put_contents ($tmp_file, file_get_contents($_FILES[0]['tmp_name']));

                $result = array();
                $result['error'] = 0;
                $result['comment'] = 'Everything is fine';
            }
            $result['data'] = $_FILES;
            $result['data'][0]['tmp_file'] = $tmpFileName;
            echo json_encode($result);
            die;
        }

        switch ($act) {
            case 'quickremove':
                require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
                require_once(JPATH_BASE . '/components/com_siteadminbln/models/account.php');
                $model = new SiteadminblnModelAccount();
                $keyword = $jinput->post->get('qrkeyword', '', 'STRING');

                if ($keyword) {
                    $this->keyword = $keyword;
                    $this->users = $model->searchPeople($keyword, '', '', 0, 0);
                } else
                    $this->users = $model->getAlluser();

                parent::display('quickremove');
                break;
            default:
                parent::display($tpl);
                break;
        }
    }
    
    private function sendCSV ($filename, $filetmp) {
        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        
        require_once(JPATH_SITE.'/components/com_siteadminbln/models/account.php');
        $model = new SiteadminblnModelAccount();
        $blninfo = $model->getInfobln();
        
        $mailfrom = 'support@parenthexis.com';
        $fromname = $user->username;
        $mailto = 'business@parenthexis.com';
        $subject = JText::_('COM_SITEADMINBLN_SENDCSV_REMOVEUSER_SUBJECT');
        $message = JText::sprintf('COM_SITEADMINBLN_SENDCSV_REMOVEUSER_BODY', $blninfo->organisation_name);
        $attackment = dirname($app->getCfg('dataroot')).'/temp/'.$filetmp;
        
        ob_start();
        $mail = JFactory::getMailer();
        $mail->sendMail($mailfrom, $fromname, $mailto, $subject, $message, false, null, null, $attackment);
        ob_end_clean();
    }

}

