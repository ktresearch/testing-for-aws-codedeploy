<?php
defined('_JEXEC') or die('Restricted access');
$users = $this->users;
?>
<div class="quickremove-page">
    <div class="manusers-bar">
        <div class="manusers-title">
            <span class="back-icon" onclick="goBackRM();"></span>
            <span class="title-page"><?php echo JText::_('COM_SITEADMINBLN_USER_MANAGEMENT');?></span>
        </div>
        <div class="manusers-tab">
            <a class="" href="<?php echo JRoute::_("index.php?option=com_siteadminbln&view=addusers");?>"><?php echo JText::_('COM_SITEADMINBLN_ADD_USERS');?></a>
            <a class="active" href="<?php echo JRoute::_("index.php?option=com_siteadminbln&view=removeusers");?>"><?php echo JText::_('COM_SITEADMINBLN_REMOVE_USERS');?></a>
        </div>
    </div>
    <div class="quickremove-content">
        <div class="ru-quick-remove sabln-um-page">
            <div class="qr-search-box search-box">
                <form action="<?php echo JRoute::_('index.php?option=con_siteadminbln&view=removeusers&action=quickremove');?>" method="post" id="form-qr-search" class="form-search">
                    <div class="div-search-user">
                        <input type="text" name="qrkeyword" class="qrkeyword searchKeyword" value="<?php echo $this->keyword ? $this->keyword : ''; ?>" placeholder="<?php echo JText::_('COM_SITEADMINBLN_QR_SEARCH_PLACEHOLDER'); ?>" autocomplete="off"/>
                        <button class="btn-search-user" type="submit"></button>
                    </div>
                    <div class="div-cancel-search animated">
                        <button class="btn-cancel-search" type="button" onclick="resetFormSearch()"></button>
                    </div>
                </form>
            </div>
            <div class="addusers-note">
                <span><?php echo JText::_('COM_SITEADMINBLN_REMOVE_USERS_NOTE');?></span>
            </div>
            <div class="qr-users-list users-list">
                <?php
                if (!empty($users)) {
                foreach ($users as $u) {
                    $user = CFactory::getUser($u->id); ?>
                    <div class="qr-user col-sm-6 user-item" data-uid="<?php echo $user->id; ?>">
                        <div class="user-item-image"><img src="<?php echo $user->getAvatar(); ?>" alt="<?php echo $user->name; ?>" title="<?php echo $user->name; ?>"/></div>
                        <div class="user-item-info">
                            <div class="user-item-name"><?php echo $user->name; ?></div>
                            <div class="user-item-remove-btn">
                                <button class="btn-qr-remove" onclick="qrRemoveUser(<?php echo $user->id; ?>)"><?php echo JText::_('COM_SITEADMINBLN_REMOVE') ?></button>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php } else echo '<p class="warning-message">'.JText::_('COM_SITEADMINBLN_NO_RESULTS_FOUND').'</p>'; ?>
            </div>
        </div>
    </div>
    <div class="notification"></div>
</div>
<script>
    (function($) {
        if( jQuery('.qrkeyword').val() != ''){
            jQuery('.div-search-user').addClass('hasValue');
            jQuery('.div-cancel-search').removeClass('slideOutRight').addClass('hasValue').addClass('slideInRight');
        }
        jQuery('.qrkeyword').on('keyup', function() {
            if (jQuery(this).val() != '') {
                jQuery('.div-search-user').addClass('hasValue');
                jQuery('.div-cancel-search').removeClass('slideOutRight').addClass('hasValue').addClass('slideInRight');
            } else {
                jQuery('.div-search-user').removeClass('hasValue');
                jQuery('.div-cancel-search').removeClass('hasValue').removeClass('slideInRight').addClass('slideOutRight');
            }
        });

        jQuery('#form-qr-search').submit(function(e) {
//            if (jQuery('.qrkeyword').val() != '') {
//
//            } else {
//                e.preventDefault();
//            }
        });
    })(jQuery);

    function qrRemoveUser(uid) {
        lgtCreatePopup('confirm', {
            'yesText': '<?php echo JText::_('COM_SITEADMINBLN_REMOVE'); ?>',
            'content': "<?php echo '<img class=\"iconcaution\" src=\"/images/Caution_1x.png\"><br><b>' .
 JText::_('COM_SITEADMINBLN_REMOVE_USER_POPUP_WARNING3') . '</b><br>' . JText::_('COM_SITEADMINBLN_REMOVE_USER_POPUP_WARNING2');
?>"
        }, function () {
            
            // Run AJAX here
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: {uid: uid, act: 'quickremove'},
                beforeSend: function () {
                    jQuery('body').addClass('overlay2');
                    jQuery('.notification').html('Loading...').fadeIn();
                },
                success: function (data, textStatus, jqXHR) {
                    console.log(data);
                    var res = JSON.parse(data);
                    console.log('dsd ' + res.error);
                    if (res.error === 0) {
                        jQuery('body').removeClass('overlay2');
                        jQuery('.notification').fadeOut();
                        jQuery('.qr-user[data-uid="' + uid + '"]').fadeOut();
//                        lgtCreatePopup('oneButton', {
//                            content: '<?php echo JText::_('COM_SITEADMINBLN_REMOVE_USER_POPUP_SUCCESS'); ?>'
//                        },
//                                function () {
//                                    lgtRemovePopup();
//                                });
                        window.location.href = "<?php echo JURI::base().'index.php?option=com_siteadminbln&view=addusers&action=aremove';?>";

                    } else {
                        jQuery('body').removeClass('overlay2');
                        jQuery('.notification').fadeOut();
                        lgtCreatePopup('oneButton', {
                            content: '<?php echo JText::_('COM_SITEADMINBLN_REMOVE_USER_POPUP_FAIL'); ?>'
                        },
                                function () {
                            lgtRemovePopup();
                        });
            }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                }
        });
            
        });
    }

    function qrSearch(e) {
        e.preventDefault();
    }

    function resetFormSearch() {
        jQuery('.qrkeyword').val('');
        jQuery('.div-search-user').removeClass('hasValue');
        jQuery('.div-cancel-search').removeClass('hasValue').removeClass('slideInRight').addClass('slideOutRight');
    }
    function goBackRM(){
        window.location.href = '<?php echo JRoute::_('index.php?option=com_siteadminbln&view=removeusers'); ?>';
    }
</script>