<?php
defined('_JEXEC') or die();

?>
<div class="addusers-page">
    <div class="manusers-bar">
        <div class="manusers-title">
            <span class="back-icon" onclick="goBackAdmin();"></span>
            <span class="title-page"><?php echo JText::_('COM_SITEADMINBLN_USER_MANAGEMENT');?></span>
        </div>
        <div class="manusers-tab">
            <a class="" href="<?php echo JRoute::_("index.php?option=com_siteadminbln&view=addusers");?>"><?php echo JText::_('COM_SITEADMINBLN_ADD_USERS');?></a>
            <a class="active" href="<?php echo JRoute::_("index.php?option=com_siteadminbln&view=removeusers");?>"><?php echo JText::_('COM_SITEADMINBLN_REMOVE_USERS');?></a>
        </div>
    </div>
    <div class="addusers-content">
        <div class="addusers-step1">
            <div class="step1-title">
                <div class="btn-quick-remove">
                    <span><?php echo JText::_('COM_SITEADMINBLN_QUICK_REMOVE');?></span>
                    <!--<img class="quick-icon" src="/images/NextIcon_HighRes_blue.png"/>-->
                </div>
                <div class = "adduser-text" >
                     <p class = "adduser-it"> <?php echo JText::_('COM_SITEADMINBLN_REMOVE_USERS_INSTRUCTION');?></p>
                    <span class="step-title"><?php echo JText::_('COM_SITEADMINBLN_STEP1');?></span>

            <div class="btn-download">
                <img class="btn-download-csv" src="/images/icons/download_icon.png"/>
                    <span><?php echo JText::_('COM_SITEADMINBLN_CSV_TEMPLATE');?></span>
            </div>
        </div>
            </div>
        </div>
        <div class="addusers-step2">
            <span class="step-title"><?php echo JText::_('COM_SITEADMINBLN_STEP2');?></span>
            <span class="step-content"><?php echo JText::_('COM_SITEADMINBLN_STEP2_CONTENT');?></span>
        </div>
        <div class="addusers-step3">
            <div class="step3-title">
                <span class="step-title"><?php echo JText::_('COM_SITEADMINBLN_STEP3');?></span>
            </div>
            <form action="" method="post" class="formSendCSV" enctype="multipart/form-data">
                <div id="uploadedCSV" style="<?php echo ($hasFile) ? 'display: block;' : '';?>">
                     <div id="myProgress" class="progress hidden">
                        <p></p>
                        <div id="bar_100">
                            <div id="myBar" class="bar"></div>
                        </div>
                    </div>
                    <!-- <span></span> -->
                    <span class="mnNameFile"><?php echo ($hasFile) ? $this->scorm['fname']:''; ?></span>
                    <div class="rm <?php echo ($hasFile) ? '': 'hidden' ?>"><img src="../images/delete.png"></div>

                </div>
                
                <div class="btn-upload">
                    <img class="btn-upload-csv" src="/images/icons/upload_icon.png"/>
                    <span><?php echo JText::_('COM_SITEADMINBLN_CSV_TEMPLATE');?></span>
                </div>
                
                <div class="sendcsv_popup" id="upfile-confirm">
                    <img class="iconcaution" src='../../images/Caution_1x.png'>
                    <p><b><?php echo JText::_('COM_SITEADMINBLN_REMOVE_USER_POPUP_WARNING1');?></b><br>
                        <?php echo JText::_('COM_SITEADMINBLN_REMOVE_USER_POPUP_WARNING2');?>
                    </p>
                    <div class="confirm_buttons" style="opacity: 1;">
                        <input type="file" name="fileUpload" class="fileUpload" style="display: none;"/>
                        <input type="hidden" name="hdUploadFileName" class="hdUploadFileName" value="" />
                        <input type="hidden" name="hdUploadTmpFile" class="hdUploadTmpFile" value="" />
                        <button class="btCancel"><?php echo JText::_('COM_SITEADMINBLN_BUTTON_CANCEL');?></button>
                        <button type="button" class="btYes"><?php echo JText::_('COM_SITEADMINBLN_BUTTON_REMOVE');?></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="addusers-note">
            <span><?php echo JText::_('COM_SITEADMINBLN_REMOVE_USERS_NOTE');?></span>
        </div>
        <div class="manageUser">
            <div class="line1"></div>
            <div class="clear"></div>
            <div class="btnManage">
                    <span><?php echo JText::_('COM_SITEADMINBLN_MANAGE_USER');?></span>
            </div>
            <div class="clear"></div>
            <div class="line2"></div>
        </div>
    </div>
    <div class="modal" id="upfile-success">
        <div class="modal-content">
            <p id = "error" class="messsuccess"></p>
            <button class="closetitle"><?php echo JText::_('COM_SITEADMINBLN_BUTTON_CLOSE');?></button>
        </div>
    </div>
    <div class="modal" id="upfile-message">
        <div class="modal-content">
        <p id = "error" class="erroraddusers"></p>
            <button class="closetitle"><?php echo JText::_('COM_SITEADMINBLN_BUTTON_CLOSE');?></button>
        </div>
    </div>
</div>
<script type="text/javascript">
    (function ($) {
        jQuery('.btn-quick-remove').click(function () {
            window.location.href = "<?php echo JRoute::_('index.php?option=com_siteadminbln&view=removeusers&action=quickremove', false);?>";
        });
        
        jQuery('.btn-upload').click(function() {
            var modal = document.getElementById('upfile-message');
//            var er = document.getElementById('error');
            jQuery('.fileUpload').val('');
            jQuery('.hdUploadFileName').val('');
            jQuery('.hdUploadTmpFile').val('');
            jQuery('.closetitle').click(function(){
                modal.style.display = "none"; 
                    jQuery('.btn-upload').show();
                    jQuery('.btn-upload').attr('data-disable', 0).fadeIn();
            });
            if (jQuery(this).attr('data-disable') != 1)
                jQuery('.fileUpload').click();
            else {
                jQuery('.erroraddusers').html( "This button is disabled because an uploaded CSV file exists.");
                modal.style.display = "block";
                // $('.lgt-notif-error').html('This button is disabled because an uploaded SCORM file exists.').addClass('lgt-visible');
            }
        });
        
        jQuery('.btn-download').click(function(e) {
            e.preventDefault();  //stop the browser from following
            window.location.href = '/images/Template_for_bulk_remove.xls';
        });
        jQuery('.btnManage').click(function() {
            window.location.href = 'index.php?option=com_siteadminbln&view=manageuser';
        });
        jQuery('.sendcsv_popup .btCancel').click(function () { 
            jQuery('.fileUpload').val('');
            jQuery('.hdUploadFileName').val('');
            jQuery('.hdUploadTmpFile').val('');
//            jQuery('.sendcsv_popup').fadeOut();
//            jQuery('body').removeClass('overlay2');
            window.location.href = "<?php echo JRoute::_('index.php?option=com_siteadminbln&view=removeusers', false);?>";
        });
        jQuery('.sendcsv_popup .btYes').click(function () {
            var filename = jQuery('.hdUploadFileName').val();
            var filetmp = jQuery('.hdUploadTmpFile').val(); 
            var modal = document.getElementById('upfile-message');
            var modal2 = document.getElementById('upfile-success');
            
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: {fname: filename, tmpfile : filetmp, act:'sendcsv'},
                beforeSend: function() {
                    jQuery('.sendcsv_popup').fadeOut();
                    lgtCreatePopup('', {'content': 'Loading...'});
                },
                success: function(data, textStatus, jqXHR) {
                    var res = JSON.parse(data);console.log(res.comment); 
                    if (res.error == 1) {
                        jQuery('.sendcsv_popup').fadeOut();
                        jQuery('.erroraddusers').html( res.comment);
                        modal.style.display = "block";  
                    } else {
                window.location.href = "<?php echo JRoute::_('index.php?option=com_siteadminbln&view=addusers&action=chartremovebulk', false);?>";                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                }
            });
        });
        
        var filename;
        function readURL(input) {
            if (input.files && input.files[0]) {
                console.log(input.files[0]['name']);
                filename = input.files[0]['name'];
                var reader = new FileReader();
                reader.onload = function (e) {
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        var files;

        jQuery('.fileUpload').on('change', prepareUpload);

        function prepareUpload(event) {
            readURL(this);
            files = event.target.files;
            if (files.length > 0) {
                jQuery('.formSendCSV').submit();
            }
        }

        // $('.formSendCSV').on('submit', uploadFiles);
        jQuery('.formSendCSV').on('submit', function(e) {
            if (jQuery('.hdUploadFileName').val() == '')
                uploadFiles(e);
        });
        function uploadFiles(event) {
             var modal = document.getElementById('upfile-message');
            var er = document.getElementById('error');
            // $('.closetitle').click(function(){
            //     modal.style.display = "none"; 
            //     $('.btSave').addClass('hidden');
            // });
            jQuery('#uploadedCSV').show();
            event.stopPropagation();
            event.preventDefault();

            var data = new FormData();
            jQuery.each(files, function(key, value) {
                data.append(key, value);
            });
            data.append('act', 'addfile');
            console.log(data);
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: data,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function() {


                },
                xhr: function () {
                        jQuery('#myProgress p').html(filename);
                        var jqXHR = null;
                        if (window.ActiveXObject) {
                            jqXHR = new window.ActiveXObject("Microsoft.XMLHTTP");
                        }
                        else {
                            jqXHR = new window.XMLHttpRequest();
                            jQuery('#myProgress').removeClass('hidden');
                            jQuery(".btn-upload").hide();
                        }
                        //Upload progress
                        jqXHR.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = Math.round((evt.loaded * 100) / evt.total);
                                //Do something with upload progress
                                jQuery('#myBar').css({width: percentComplete + '%'});
                                if (percentComplete == 100) {

                                }
                                // console.log('Uploaded percent', percentComplete);
                            }
                        }, false);
                        //Download progress
                        jqXHR.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = Math.round((evt.loaded * 100) / evt.total);
                            }
                        }, false);
                        return jqXHR;
                    },
                success: function(data, textStatus, jqXHR) {
                    var res = JSON.parse(data);
                    if (res.error == 1) {
                        $('#uploadedCSV > span').html('');
                        $('#uploadedCSV').hide();
                        $('.erroraddusers').html( res.comment);
                        modal.style.display = "block";
                    } else {
                        $('.hdUploadFileName').val(res.data[0].name);
                        $('.hdUploadTmpFile').val(res.data[0].tmp_file);
                        $('.sendcsv_popup').fadeIn();
                        $('body').addClass('overlay2');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                }
            });
        }
        
    })(jQuery);

    function goBack() {
        // window.history.back();
        var nav = window.navigator;
        if( this.phonegapNavigationEnabled &&
            nav &&
            nav.app &&
            nav.app.backHistory ){
            nav.app.backHistory();
        } else {
            window.history.back();
        }
    }
</script>

