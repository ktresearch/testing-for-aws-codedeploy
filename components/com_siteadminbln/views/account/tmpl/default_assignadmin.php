<?php
defined('_JEXEC') or die();

$my = JFactory::getUser();
if ($my->id === $this->ownerbln) {
    $ownerBln = true;
}
$users = $this->users;
$sl = $this->count;
$number = ceil($sl / 20);
if ($this->keyword)
    $search = 1;
else
    $search = 0;
?>
<div class="assign-page sabln-page">
    <div class="header-page">
        <div class="back" onclick="backAcount();">
            <img src="<?php echo JURI::base(); ?>images/Back_2x.png">
        </div>
        <div class="title center">
            <p><?php echo $this->pageName; ?></p>
        </div>
    </div>
    <div class="sabln-page-content">
        <div class="as-search-box search-box">
            <form action="<?php echo JRoute::_('index.php?option=com_siteadminbln&view=account&action=assign&role=' . $this->role); ?>" method="post" id="form-as-search" class="form-search">
                <div class="div-search-user">
                    <input type="text" name="searchKeyword" class="searchKeyword" value="<?php echo $this->keyword ? $this->keyword : ''; ?>" placeholder="<?php echo JText::_('COM_SITEADMINBLN_QR_SEARCH_PLACEHOLDER'); ?>" autocomplete="off"/>
                    <button class="btn-search-user" type="submit"></button>
                </div>
                <div class="div-cancel-search animated">
                    <button class="btn-cancel-search" type="button" onclick="resetFormSearch()"></button>
                </div>
            </form>
        </div>
        <div class="qr-users-list users-list">
            <?php
            if (!empty($users)) {
                foreach ($users as $u) {
                    $user = CFactory::getUser($u->id);
                    ?>
                    <div class="as-user col-sm-6 user-item" onclick="checkStatus('<?php echo $user->id; ?>')" id="<?php echo $user->id; ?>" data-uid="<?php echo $user->id; ?>">
                        <div class="inputCheckbox">
                            <div class="status" id ="<?php echo $user->id; ?>"></div>
                        </div>
                        <div class="user-item-image"><img src="<?php echo $user->getAvatar(); ?>" alt="<?php echo $user->name; ?>" title="<?php echo $user->name; ?>"/></div>
                        <div class="user-item-info">
                            <div class="user-item-name"><?php echo $user->name; ?></div>
                        </div>
                    </div>
    <?php } ?>
<?php } else echo '<p class="warning-message">' . JText::_('COM_SITEADMINBLN_NO_RESULTS_FOUND') . '</p>'; ?>
        </div>
        <?php if(!empty($users)) {?>
            <div class="div-buttons">
                <input class="btCancel standard-button" type="button" value="<?php echo JText::_('COM_SITEADMINBLN_CANCEL'); ?>" />
                <input class="btAssign standard-button" type="submit" name="formSubmit" value="<?php echo JText::_('COM_SITEADMINBLN_ASSIGN'); ?>" />
            </div>
        <?php }?>
    </div>
</div>

<div class="assign-result-page sabln-page hidden">
    <p class="assign-result-page-header"><?php echo $this->role == 'ad' ? JText::_('COM_ADMINBLN_INFOMATION') : JText::sprintf('COM_SITEADMINBLN_ASSIGN_RESULT_HEADER', $this->roleText) ?></p>
    <div class="assign-result-page-content">
        <p class="name-text">
            <span><?php echo JText::_('COM_SITEADMINBLN_NAME') . ': '; ?></span>
            <span class="name"></span>
        </p>
        <p class="email-text">
            <span><?php echo JText::_('COM_SITEADMINBLN_EMAIL') . ': '; ?></span>
            <span class="email"></span>
        </p>
    </div>
    <?php if(!empty($users)) {?>
        <div class="div-buttons">
            <button class="btn-back-to-home standard-button"><?php echo JText::_('COM_SITEADMINBLN_BACK_TO_HOME') ?></button>
        </div>
    <?php }?>
</div>
<script>
    function checkStatus(id) {
        jQuery('.users-list .status').removeClass('active');
        jQuery('#' + id).find('.status').addClass('active');
    }
    jQuery(document).ready(function () {
        var loading = false;
        var a = 1;
        var number = '<?php echo $number; ?>';
        var act = '<?php echo $search; ?>';
        function loadUsers() {
            var suser = jQuery('.searchKeyword').val();
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    act: "load_user",
                    suser: suser,
                    page: a,
                    status: act,
                },
                beforeSend: function () {
                    console.log('load more user: ' + a);
                    jQuery('.qr-users-list').append('<div class="divLoadMore">Loading...<div class="loading"></div></div>');
                },
                success: function (result, textStatus, jqXHR) {
                    var res = JSON.parse(result);
                    var status = res.status;
                    var b = res.users;
                    var sta = '';
                    jQuery.each(b, function (key, value) {
                        var user_id = b[key]['id'];
                        var fullname = b[key]['name'];
                        var user_avatar = b[key]['avatar'];
                        var mdate = b[key]['date'];
                        var html;
                        html = '<div class="as-user col-sm-6 user-item" id = "' + user_id + '" onclick="checkStatus(' + user_id + ')" data-uid="' + user_id + '">' +
                                '<div class="inputCheckbox">' +
                                '<div class="status" id ="' + user_id + '"></div></div>' +
                                '<div class="user-item-image"><img src="' + user_avatar + '" alt="' + fullname + '" title="' + fullname + '"/></div>' +
                                '<div class="user-item-info">' +
                                '<div class="user-item-name">' + fullname + '</div>' +
                                '</div>' +
                                '</div>';

                        jQuery('.divLoadMore').remove();
                        jQuery('.qr-users-list').append(html);
                    });
                    a = a + 1;
                    loading = false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                    loading = false;
                }
            });
        }

        jQuery(window).scroll(function () {
            if ((jQuery(window).scrollTop() + jQuery(window).height()) >=
                    (jQuery('.assign-page').offset().top + jQuery('.assign-page').height())) {
                if (a < number && loading == false) {
                    loading = true;
                    console.log('loading');
                    loadUsers();
                }
            }

        });
        if (jQuery('.searchKeyword').val() != '') {
            jQuery('.div-search-user').addClass('hasValue');
            jQuery('.div-cancel-search').removeClass('slideOutRight').addClass('hasValue').addClass('slideInRight');
        }
        jQuery('.searchKeyword').on('keyup', function () {
            if (jQuery(this).val() != '') {
                jQuery('.div-search-user').addClass('hasValue');
                jQuery('.div-cancel-search').removeClass('slideOutRight').addClass('hasValue').addClass('slideInRight');
            } else {
                jQuery('.div-search-user').removeClass('hasValue');
                jQuery('.div-cancel-search').removeClass('hasValue').removeClass('slideInRight').addClass('slideOutRight');
            }
        });

        jQuery('.form-search').submit(function (e) {
//            if (jQuery('.searchKeyword').val() != '') {
//
//            } else {
//                e.preventDefault();
//            }
        });

        jQuery('.as-user').click(function () {
            jQuery('.users-list .status').removeClass('active');
            jQuery(this).find('.status').addClass('active');
        });

        jQuery('.btAssign').click(function (e) {
            e.preventDefault();
            var url = '<?php echo JURI::base() . 'index.php?option=com_siteadminbln&task=assignRoleAdmin'; ?>';

            var assignedUserID = jQuery('.status.active').parents('.as-user').attr('data-uid');
            if (assignedUserID) {
                var owner = '<?php echo $ownerBln; ?>';
                var popupContent;
<?php
if ($this->isBLNOwner) {
    if ($this->role == 'ad') {
        echo 'popupContent = \'' . JText::_('COM_SITEADMINBLN_POPUP_CONTENT_OWNER_ASSIGN_ADMIN') . '\';';
    } else if ($this->role == 'ow') {
        echo 'popupContent = \'' . JText::_('COM_SITEADMINBLN_POPUP_CONTENT_OWNER_ASSIGN_OWNER') . '\';';
    }
} else if ($this->isBLNAdmin) {
    echo 'popupContent = \'' . JText::_('COM_SITEADMINBLN_POPUP_CONTENT_ADMIN_ASSIGN_ADMIN') . '\';';
}
?>

                lgtCreatePopup('confirm', {
                    'yesText': '<?php echo JText::_('COM_SITEADMINBLN_ASSIGN'); ?>',
                    'header': '<?php echo JText::sprintf('COM_SITEADMINBLN_ASSIGN_POPUP_HEADER', $this->roleText); ?>',
                    'content': popupContent
                }, function () {
                    jQuery.ajax({
                        url: '<?php echo JURI::base() . 'index.php?option=com_siteadminbln&task=assignRoleAdmin'; ?>',
                        type: 'POST',
                        data: {
                            uid: assignedUserID,
                            role: '<?php echo $this->role; ?>'
                        },
                        beforeSend: function () {
                            lgtCreatePopup('', {'content': 'Loading'});
                        },
                        success: function (data) {
                            lgtRemovePopup();
                            res = JSON.parse(data);
                            console.log(res);

                            if (res.error == 0) {
<?php if ($this->role == 'ow' || ($this->role == 'ad' && $this->isBLNAdmin)) { ?>
                                    jQuery('.assign-page').addClass('hidden');
                                    jQuery('.assign-result-page .name').html(res.data.name);
                                    jQuery('.assign-result-page .email').html(res.data.email);
                                    jQuery('.assign-result-page').removeClass('hidden');
<?php
} else {
    if ($this->isBLNOwner) {
        echo 'location.href = "' . JRoute::_('index.php?option=com_siteadminbln&view=account') . '";';
    } else if ($this->isBLNAdmin) {
        echo 'location.href = "' . JUri::base() . '";';
    }
}
?>
                            } else
                                lgtCreatePopup('withCloseButton', {'content': res.comment});
                        },
                        error: function (res) {
                            console.log(res);
                            lgtRemovePopup();
                        }
                    });
                }
                );
            } else
                lgtCreatePopup('withCloseButton', {'content': 'You have not selected anyone yet.'});
        });

        jQuery('.btCancel').click(function () {
                window.location.href = '<?php echo JRoute::_('index.php?option=com_siteadminbln&view=account'); ?>';
        });
        
        jQuery('.btn-back-to-home').click(function () {
            location.href = "<?php echo JUri::base(); ?>";
        });

    });
    
    function resetFormSearch() {
        jQuery('.searchKeyword').val('');
        jQuery('.div-search-user').removeClass('hasValue');
        jQuery('.div-cancel-search').removeClass('hasValue').removeClass('slideInRight').addClass('slideOutRight');
    }
    function backAcount(){
        window.location.href = '<?php echo JRoute::_('index.php?option=com_siteadminbln&view=account'); ?>';
    }

</script>