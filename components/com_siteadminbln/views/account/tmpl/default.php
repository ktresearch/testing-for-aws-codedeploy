<?php
defined('_JEXEC') or die();
$device = JFactory::getSession()->get('device');
$is_mobile = $device == 'mobile' ? true : false;

$blnOwner = JFactory::getUser($this->ownerid);
$blnAdmin = JFactory::getUser($this->adminid);
$info = $this->blnInfo;
?>
<script src="<?php echo JURI::base(); ?>components/com_community/assets/autosize.min.js"></script>
<div class="accountPage account-page sabln-page">
    <div class="header-page">
        <div class="back" onclick="goBackAdmin();">
            <img src="<?php echo JURI::base(); ?>images/Back_2x.png">
        </div>
        <div class="title center">
            <p><?php echo JText::_('COM_SITEADMINBLN_ACCOUNT'); ?></p>
        </div>
    </div>
    <div class="contentPage">
        <p class="titleCompany"><?php echo JText::_('COM_COMPANY_INFOMATION'); ?></p>
        <form class="frmAccount" method = "POST">
            <div class="div-inputs">
                <div class="liName form-group">
                    <label for="txtName" class="title titleName"><?php echo JText::_('COM_ORGANISASION_NAME') ?></label>
                    <span class="red">*</span>
<!--                    <input type="text" class="txtName" name="" value="--><?php //echo $info->organisation_name; ?><!--" >-->
                    <textarea name="txtName" id="txtName" class="txtName autosize" cols="30" rows="10"><?php echo $info->organisation_name; ?></textarea>
                    <span class="notetext"></span>
                </div>
                <div class="liAddress form-group">
                    <label for="txtAddress" class="title titleAddress"><?php echo JText::_('COM_ADDRESS_BLN')?></label>
                    <span class="red">*</span>
<!--                    <input type="text" class="txtAddress" name="" value="--><?php //echo $info->address ;?><!--" >-->
                    <textarea name="txtAddress" id="txtAddress" class="txtAddress autosize" cols="30" rows="10"><?php echo $info->address; ?></textarea>
                </div>
                <div class="liPostal form-group">
                    <label for="txtPostal" class="title titlePostal"><?php echo JText::_('COM_POSTAL_CODE_BLN')?></label>
                    <span class="red">*</span>
<!--                    <input type="text" class="txtPostal" name="" value="--><?php //echo $info->postal_code;?><!--" >-->
                    <textarea name="txtPostal" id="txtPostal" class="txtPostal autosize" cols="30" rows="10"><?php echo $info->postal_code; ?></textarea>
                </div>
                <div class="liContact form-group">
                    <label for="txtContact" class="title titleContact"><?php echo JText::_('COM_CONTACT_BLN')?></label>
                    <span class="red">*</span>
                    <textarea type="text" cols="30" rows="10" class="txtContact autosize" name="" ><?php echo $info->contact ;?></textarea>
                </div>
            </div>
            <div class="boxAdmin">
                <p class="titleAdmin"><?php echo JText::_('COM_ADMINBLNSTRATOR_INFOMATION') ;?></p>
                <?php if ($this->model->hasPermission('assigningBLNOwner')) { ?>
                <a class="buttonAssign standard-button" href="<?php echo JRoute::_('index.php?option=com_siteadminbln&view=account&action=assign&role=ow'); ?>"><?php echo JText::_('COM_SITEADMINBLN_ASSIGN_OWNER') ;?></a>
                <?php } ?>
                <div class="infoOwner">
                    <div class="info-block">
                        <p class="nameAdmin"><?php echo JText::_('COM_NAMEADMIN_BLN') ;?><span><?php echo ' [ '.JText::_('COM_SITEADMINBLN_OWNER').' ]' ;?></span></p>
                        <p class="textName"><?php echo $blnOwner->id ? $blnOwner->name : JText::_('COM_SITEADMINBLN_EMPTY') ?></p>
                    </div>
                    <div class="info-block">
                        <p class="emailAdmin"><?php echo JText::_('COM_EMAILADMIN_BLN') ;?></p>
                        <p class="textEmail"><?php echo $blnOwner->id ? $blnOwner->username : JText::_('COM_SITEADMINBLN_EMPTY') ?></p>
                    </div>
                </div>
                <?php
                if ($this->model->hasPermission('assigningBLNAdmin')) {
                ?>
                <a class="buttonAssign standard-button btAssignAdmin" href="<?php echo JRoute::_('index.php?option=com_siteadminbln&view=account&action=assign&role=ad'); ?>"><?php echo JText::_('COM_SITEADMINBLN_ASSIGN_ADMIN') ;?></a>
                <?php } ?>
                <div class="infoAdmin">
                    <div class="info-block">
                        <p class="nameAdmin"><?php echo JText::_('COM_NAMEADMIN_BLN') ;?></p>
                        <p class="textName"><?php echo $blnAdmin->id ? $blnAdmin->name : JText::_('COM_SITEADMINBLN_EMPTY') ?></p>
                    </div>
                    <div class="info-block">
                        <p class="emailAdmin"><?php echo JText::_('COM_EMAILADMIN_BLN') ;?></p>
                        <p class="textEmail"><?php echo $blnAdmin->id ? $blnAdmin->username : JText::_('COM_SITEADMINBLN_EMPTY') ?></p>
                    </div>
                </div>

            </div>
            <?php
            if ($this->model->hasPermission('managingLoginPage')) {
            ?>
            <div class="div-manage-login-page">
                <a class="btn-manage-login-page" href="<?php echo JRoute::_('index.php?option=com_siteadminbln&view=account&action=mlp'); ?>"><?php echo JText::_('COM_SITEADMINBLN_MANAGE_LOGIN_PAGE') ;?></a>
            </div>
            <?php } ?>
            <div class="buttonAdmin div-buttons">
                <input class="btCancel standard-button" type="button" value="<?php echo JText::_('COM_SITEADMINBLN_CANCEL'); ?>" />
                <input class="btAssign standard-button" type="button" name="formSubmit" value="<?php echo JText::_('COM_SAVE_BLN'); ?>" />
            </div>
        </form>
    </div>
    <div class="notification"></div>
</div>

<script>
    jQuery(document).ready(function() {
        jQuery('.autosize').each(function () {
            autosize(this);
        }).on('input', function () {
            autosize(this);
        });

        jQuery(document).on('keyup', '.div-inputs textarea', function() {
            if (jQuery(this).val().replace(/\s+/, "") == '') {
                jQuery(this).parents('.form-group').addClass('hasError');
            } else {
                // Check string has invalid symbols
                if(jQuery(this).hasClass('txtPostal')) {
                    if (jQuery(this).hasClass('txtPostal') && !/^[a-zA-Z0-9- ]*$/.test(jQuery('.txtPostal').val()) ) {
                        jQuery(this).parents('.form-group').addClass('hasError');
                    } else
                        jQuery(this).parents('.form-group').removeClass('hasError');
                }
                if (jQuery(this).hasClass('txtContact')){
                    if (jQuery(this).hasClass('txtContact') && !/^[^a-zA-Z]*$/.test(jQuery('.txtContact').val()) ) {
                        jQuery(this).parents('.form-group').addClass('hasError');
                    } else
                        jQuery(this).parents('.form-group').removeClass('hasError');
                }
                if (jQuery(this).hasClass('txtAddress')) {
                    jQuery(this).parents('.form-group').removeClass('hasError');
                }
                if (jQuery(this).hasClass('txtName')) {
                    jQuery(this).parents('.form-group').removeClass('hasError');
                }
            }
        });

        jQuery('.btAssign').click(function () {
            var validate = true;

            jQuery('.div-inputs textarea').each(function() {
                if (jQuery(this).val().replace(/\s+/, "") == '') {
                    validate = false;
                    jQuery(this).parents('.form-group').addClass('hasError');
                } else {
                    // Check string has invalid symbols
                    if (jQuery(this).hasClass('txtPostal')) {
                        if (!/^[a-zA-Z0-9- ]*$/.test(jQuery('.txtPostal').val()) ) {
                            validate = false;
                            jQuery(this).parents('.form-group').addClass('hasError');
                        } else
                            jQuery(this).parents('.form-group').removeClass('hasError');
                    }
                    
                    if (jQuery(this).hasClass('txtContact')) {
                        if (!/^[^a-zA-Z]*$/.test(jQuery('.txtContact').val()) ) {
                            validate = false;
                            jQuery(this).parents('.form-group').addClass('hasError');
                        } else
                            jQuery(this).parents('.form-group').removeClass('hasError');
                    }
                }
            });

            if (validate) {
                jQuery.ajax({
                    url: window.location.href,
                    type: 'POST',
                    data: {
                        act:'create',
                        name: jQuery('.txtName').val() ,
                        address: jQuery('.txtAddress').val(),
                        postal: jQuery('.txtPostal').val(),
                        contact: jQuery('.txtContact').val()
                    },
                    beforeSend: function () {
                        lgtCreatePopup('', {'content': 'Loading...'});
                    },
                    success: function (data, textStatus, jqXHR) {
                        var res = JSON.parse(data);
                        console.log(res.data);

                        window.location.href = '<?php echo JRoute::_('index.php?option=com_siteadminbln&view=account&action=view'); ?>';
                    }

                });
            } else {
                var topMenuHeight = <?php echo $is_mobile ? 50 : 80; ?>;
                var divInputsScrollTop = jQuery('.div-inputs').offset().top > topMenuHeight ? (jQuery('.div-inputs').offset().top - topMenuHeight) : jQuery('.div-inputs').offset().top;
                jQuery('html,body').animate({
                        scrollTop: divInputsScrollTop },
                    200);
            }
        });

        jQuery('.btCancel').click(function() {
            if (history.length > 2 )
                goBack();
            else
                window.location.href = '<?php echo JRoute::_('index.php?option=com_siteadminbln&view=account'); ?>';
        });
    });
</script>