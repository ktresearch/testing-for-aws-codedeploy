<?php
$device = JFactory::getSession()->get('device');
$is_mobile = $device == 'mobile' ? true : false;
?>
<script src="<?php echo JURI::base(); ?>components/com_community/assets/autosize.min.js"></script>
<div class="mlp-page mlp-first-page sabln-page">
    <div class="header-page">
        <div class="back" onclick="backAcount();">
            <img src="<?php echo JURI::base(); ?>images/Back_2x.png">
        </div>
        <div class="title center">
            <p><?php echo JText::_('COM_SITEADMINBLN_MANAGE_LOGIN_PAGE'); ?></p>
        </div>
    </div>
    <div class="contentPage">
        <div class="landing-page">
            <section class="mlp-slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php
                        foreach ($this->blnBanners as $bannerImage) { ?>
                            <div class="swiper-slide">
                                <div class="mlp-banner" style="<?php echo $bannerImage ? 'background-image: url(\''.$bannerImage.'?_='.time().'\')' : ''; ?>">
                                    <div class="frontLayer">
                                         <?php if(!empty($this->blnLogo)) {?>
                                            <img src="<?php echo $this->blnLogo.'?_='.time(); ?>" alt="Parenthexis"/>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="btEditSlider pencilBg" onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_siteadminbln&view=account&action=editbanner') ?>'"></div>
            </section>
            <?php
            if (count($this->blnBanners) > 1) {
                ?>
                <div class="swiper-pagination"></div>
            <?php } ?>
            <section class="mlp-main">
                <div class="writeup">
                    <p class="writeup-content"><?php echo !empty($this->blnInfo->bln_writeup) ? nl2br($this->blnInfo->bln_writeup) : JText::_('COM_SITEADMINBLN_MLP_WRITE_UP'); ?></p>
                    <div class="btChangeWriteup pencilBg" <?php echo $is_mobile ? 'onclick="window.location.href=\''.JRoute::_('index.php?option=com_siteadminbln&view=account&action=editwriteup').'\'"' : '' ?>>
                        <?php if (!$is_mobile) { ?>
                            <div class="popupChangeWriteup divBubble">
                                <form class="formChangeWriteup" action="#" method="post">
                                    <div class="form-group">
                                        <label for="txtWriteup"><?php echo JText::_('COM_SITEADMINBLN_WRITE_UP'); ?></label>
                                        <textarea class="autosize" name="txtWriteup" id="txtWriteup" maxlength="140" cols="30" rows="10"><?php echo !empty($this->blnInfo->bln_writeup) ? $this->blnInfo->bln_writeup : ''; ?></textarea>
                                        <textarea class = "hide" id ="inputWriteup" value = ""><?php echo !empty($this->blnInfo->bln_writeup) ? $this->blnInfo->bln_writeup : ''; ?></textarea>
                                    </div>
                                    <p class="countWriteup">140</p>
                                    <div class="div-buttons">
                                        <input class="btCancel standard-button" type="button" value="<?php echo JText::_('COM_SITEADMINBLN_CANCEL'); ?>" />
                                        <input class="btSave standard-button" type="button" value="<?php echo JText::_('COM_SAVE_BLN'); ?>" />
                                    </div>
                                </form>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="formLogin">
                    <div class="form-group">
                        <input class="input-email" type="text" placeholder="<?php echo JText::_('COM_SITEADMINBLN_MLP_EMAIL'); ?>" name="txtUsername" autocapitalize="off">
                    </div>
                    <div class="form-group">
                        <input class="input-password" type="password" placeholder="<?php echo JText::_('COM_SITEADMINBLN_MLP_PASSWORD'); ?>" name="txtPassword" autocapitalize="off">
                    </div>

                    <div class="divLogin">
                        <div class="divButtonLogin">
                            <button class="bln-login standard-button" data-disabled="1"><?php echo JText::_('COM_SITEADMINBLN_MLP_LOG_IN') ?></button>
                        </div>
                        <div class="divForgotPassword">
                            <a href="javascript: void(0);"><?php echo JText::_('COM_SITEADMINBLN_MLP_FORGOT_PASSWORD_LOGIN'); ?></a>
                        </div>
                    </div>

                </div>
                <div class="mlp-footer">
                    <p><span><?php echo JText::_('COM_SITEADMINBLN_MLP_POWERED_BY') ?></span> <img class="icon-px" src="/templates/t3_bs3_tablet_desktop_template/images/Parenthexis_Pantone432.png"></p>
                </div>
            </section>

        </div>



    </div>
</div>
<script>
    jQuery(document).ready(function() {
        jQuery('#txtWriteup').keydown(function(e){
             var a = jQuery(this).val().split("\n").length;
             if(e.keyCode == 13 && a >=5 ) 
                return false;
        });
        jQuery('.autosize').each(function () {
                autosize(this);
            }).on('input', function () {
            autosize(this);
        });
        function limitLines(obj, e) {
            if(e.keyCode == 13 && jQuery(this).val().split("\n").length >= jQuery(this).attr('rows')) { 
            return false;
           }
        }
        <?php if (!$is_mobile) { ?>

        jQuery('.btChangeWriteup').click(function(e) {
            if (jQuery(e.target).is('.btChangeWriteup')) {
                jQuery('body').addClass('lgtOverlay');
                 jQuery('#txtWriteup').empty();
                jQuery('#txtWriteup').val(jQuery('#inputWriteup').val());
                jQuery('.countWriteup').html(140 - parseInt(jQuery('#txtWriteup').val().length));
                jQuery('.popupChangeWriteup').fadeIn(200);
            }
        });
        jQuery('.popupChangeWriteup .btCancel').click(function() {
            jQuery('body').removeClass('lgtOverlay');
            jQuery(this).parents('.popupChangeWriteup').fadeOut(200);
              jQuery('#txtWriteup').parents('.form-group').removeClass('hasError');
        });
        jQuery('.popupChangeWriteup .btSave').click(function() {
            var writeupContent = jQuery('#txtWriteup').val().trim();
            if (writeupContent == '') {
                jQuery('#txtWriteup').parents('.form-group').addClass('hasError');
                return false;
            }
            jQuery.ajax({
                url: '<?php echo JRoute::_('index.php?option=com_siteadminbln&view=account&task=editwriteup', false) ?>',
                type: 'post',
                data: {
                    'content': writeupContent
                },
                beforeSend: function() {
                    jQuery('.popupChangeWriteup').hide();
                    lgtCreatePopup('', {'content': 'Loading...'});
                },
                success: function(data) {
                    lgtRemovePopup();
                    var res = JSON.parse(data);
                    if (res.error == 1) {
                        lgtCreatePopup('withCloseButton', {'content': res.comment});
                    } else {
                        jQuery('.writeup-content').html(writeupContent.replace(/\r\n|\r|\n/g,"<br>"));
                        jQuery('#inputWriteup').val(writeupContent.replace(/\r\n|\r|\n/g,"\n"));
                    }
                },
                error: function() {
                    lgtRemovePopup();
                }
            });
        });
        jQuery('#txtWriteup').keyup(function() {
            if (jQuery('#txtWriteup').val().trim() == '') {
                jQuery('#txtWriteup').parents('.form-group').addClass('hasError');
            } else jQuery('#txtWriteup').parents('.form-group').removeClass('hasError');
            jQuery('.countWriteup').html(140 - parseInt(jQuery(this).val().length));
        });

        <?php } else { ?>

        <?php } ?>

        <?php
        if (count($this->blnBanners) > 1) {
        ?>
        var swiper = new Swiper('.mlp-page .swiper-container', {
            centeredSlides: true,
            pagination: {
                el: '.mlp-page .swiper-pagination',
                clickable: true
            },
            slidesPerView: 1,
            spaceBetween: 10
        });
        <?php } ?>
            
    });
    function backAcount(){
        window.location.href = '<?php echo JRoute::_('index.php?option=com_siteadminbln&view=account'); ?>';
    }
</script>