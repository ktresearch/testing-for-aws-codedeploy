<?php
$device = JFactory::getSession()->get('device');
$is_mobile = $device == 'mobile' ? true : false;

$logoMaxWidth = 338;
$logoMaxHeight = 168;
$logoFileType = 'PNG';

$bannerMaxWidth = 1526;
$bannerMaxHeight = 852;
$bannerFileType = ['PNG', 'JPG'];
?>
<style type="text/css">
    .lgtPopup .popupContent p.popup-header { font-weight: 600 !important; }
</style>
<div class="mlp-page mlp-edit-banner-page sabln-page">
    <div class="header-page">
        <div class="back" onclick="goBack();">
            <img src="<?php echo JURI::base(); ?>images/Back_2x.png">
        </div>
        <div class="title center">
            <p><?php echo JText::_('COM_SITEADMINBLN_EDIT'); ?></p>
        </div>
    </div>
    <div class="contentPage">
        <form action="<?php echo JRoute::_('index.php?option=com_siteadminbln&view=account&task=editbanner', false) ?>" method="post" enctype="multipart/form-data" class="formEditBanner">
        <div class="forLogo">
            <div class="col-sm-6 col-instructions">
                <h3><?php echo JText::_('COM_SITEADMINBLN_LOGO_TEXT'); ?></h3>
                <p><?php echo JText::sprintf('COM_SITEADMINBLN_LOGO_INSTRUCTIONS_TEXT', $logoMaxWidth, $logoMaxHeight, $logoFileType)?></p>
            </div>
            <div class="col-sm-6">
                <div class="logo-container">
                    <div class="logo-wrapper" style="<?php echo !empty($this->blnInfo->bln_logo) ? 'background-image:url(\''. $this->blnLogo .'\')' : ''; ?>">
                        <div class="btn-upload-logo"></div>
                        <?php if (!empty($this->blnInfo->bln_logo)) { ?>
                        <div class="btn-remove-logo"></div>
                        <?php } ?>
                    </div>

                    <input type="file" name="logoUpload" class="logoUpload hidden"/>
                    <input type="hidden" name="logoFileName" class="logoFileName" value="<?php echo (!empty($this->blnInfo->bln_logo)) ? $this->blnInfo->bln_logo : ''; ?>"/>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="forBanner">
            <div class="col-sm-6 col-instructions">
                <h3><?php echo JText::_('COM_SITEADMINBLN_BANNER_TEXT'); ?></h3>
                <p><?php echo JText::sprintf('COM_SITEADMINBLN_BANNER_INSTRUCTIONS_TEXT', $bannerMaxWidth, $bannerMaxHeight, $bannerFileType[0], $bannerFileType[1])?></p>
                <div class="mobile-view">
                    <div></div>
                    <span><?php echo JText::_('COM_SITEADMINBLN_MOBILE_VIEW') ?></span>
                </div>
                <div class="web-view">
                    <div></div>
                    <span><?php echo JText::_('COM_SITEADMINBLN_WEB_VIEW') ?></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="sample-banner-container">
                    <div class="sample-banner-for-mobile">
                        <p><?php echo JText::_('COM_SITEADMINBLN_SAMPLE') ?></p>
                    </div>
                </div>
            </div>
            <div class="clear"></div><div class="clear"></div>
            <div class="block-banners">
                <?php if (!empty($this->blnInfo->bln_first_banner)) {
                    $colName = ['bln_first_banner', 'bln_second_banner', 'bln_third_banner', 'bln_fourth_banner'];
                    $i = 0;
                    foreach ($this->blnBanners as $key => $banner) {
                        ?>
                        <div class="col-sm-6 block-banner" style="<?php //echo !empty($banner) ? 'background-image:url(\''. $banner.'?_='.time() .'\')' : ''; ?>">
                            <img src="<?php echo $banner.'?_='.time(); ?>" alt="hiddenImage" class="hiddenImage hidden"/>
                            <div class="banner-container">
                                <div class="btn-upload-banner" style="display: none"></div>
                                <div class="btn-remove-banner"></div>
                                <div class="lgt-loader" style="display: block"></div>
                            </div>
                            <input type="file" name="bannerUpload[]" class="bannerUpload hidden"/>
                            <input type="hidden" name="bannerFileName[]" class="bannerFileName" value="<?php echo (!empty($this->blnInfo->{$colName[$i]})) ? $this->blnInfo->{$colName[$i]} : ''; ?>"/>
                        </div>
                    <?php
                    $i++;
                    } ?>
                <?php } ?>
                <div class="col-sm-6 block-banner col-new-banner" style="<?php echo (count($this->blnBanners) >= 4) ? 'display: none;' : ''; ?>">
                    <div class="banner-container">
                        <div class="btn-upload-banner"></div>

                        <div class="btn-remove-banner" style="display: none"></div>

                        <div class="lgt-loader" style="display: none"></div>
                    </div>
                    <input type="file" name="bannerUpload[]" class="bannerUpload hidden"/>
                    <input type="hidden" name="bannerFileName[]" class="bannerFileName" value=""/>
                </div>
            </div>
            <!--<div class="col-sm-6 col-new-banner" style="<?php echo (count($this->blnBanners) >= 4) ? 'display: none;' : ''; ?>">
                <div class="new-banner-container">
                    <div class="btn-upload-banner"></div>
                </div>
            </div>
            -->

            <div class="clear"></div>
        </div>
        <div class="div-buttons">
            <input class="btCancel standard-button" type="button" onclick="goBack()" value="<?php echo JText::_('COM_SITEADMINBLN_CANCEL'); ?>" />
            <input class="btSave standard-button" type="submit" value="<?php echo JText::_('COM_SAVE_BLN'); ?>" />
        </div>
        </form>
    </div>
</div>
<script>
    jQuery(document).ready(function() {
        var maxFileSize = 8 * 1024 * 1024;

        jQuery('.btn-upload-logo').click(function() {
            jQuery('.logoUpload').click();
        });

        jQuery('.logoUpload').on('change', function(event) {
            var files = event.target.files;
            if (files[0].type != 'image/png') {
                lgtCreatePopup('oneButton', {'content':'<?php echo JText::_('COM_SITEADMINBLN_UPLOAD_IMAGES_TYPE_WARNING'); ?>'}, function() {
                    lgtRemovePopup();
                });
                jQuery('.logoUpload').val(null);
                return false;
            }
            if (files[0].size > maxFileSize) {
                lgtCreatePopup('oneButton', {'content':'<?php echo JText::sprintf('COM_SITEADMINBLN_UPLOAD_IMAGES_SIZE_WARNING', 8); ?>'}, function() {
                    lgtRemovePopup();
                });
                jQuery('.logoUpload').val(null);
                return false;
            }
            if (files && files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    jQuery('.btn-remove-logo').show();
                    jQuery('.logoFileName').val(files[0].name);
                    jQuery('.logo-wrapper').attr('style', 'background-image:  url(' + e.target.result + ') ').show();
                };
                reader.readAsDataURL(files[0]);
            }
        });

        jQuery('.btn-remove-logo').click(function() {
            var btRemoveLogo = this;
            lgtCreatePopup('confirm', {
                'content' : '<?php echo JText::_('COM_SITEADMINBLN_REMOVE_LOGO_CONFIRM'); ?>',
                'yesText' : '<?php echo JText::_('COM_SITEADMINBLN_REMOVE'); ?>'
            }, function() {
                jQuery(btRemoveLogo).hide();
                jQuery('.logo-wrapper').attr('style', '').show();
                jQuery('.logoUpload').val(null);
                jQuery('.logoFileName').val('');
            });
        });

        function appendNewBannerBlock() {
            jQuery('.block-banners').append('<div class="col-sm-6 block-banner col-new-banner" style="display: none">' +
            '<div class="banner-container">' +
            '<div class="btn-upload-banner"></div>' +
            '<div class="btn-remove-banner" style="display: none"></div>' +
            '<div class="lgt-loader"></div>' +
            '</div>' +
            '<input type="file" name="bannerUpload[]" class="bannerUpload hidden"/>' +
            '<input type="hidden" name="bannerFileName[]" class="bannerFileName" value=""/>' +
            '</div>');
            checkNumberofBlockBanner();
        }

        jQuery(document).on('click', '.btn-remove-banner', function() {
            var btRemoveBanner = this;
            var popupHeader, popupContent;

            if (jQuery('.block-banners .block-banner:not(.col-new-banner)').length != 1) {
                popupHeader = '<?php echo JText::_('COM_SITEADMINBLN_REMOVE_BANNER_CONFIRM'); ?>';
                popupContent = '';
            } else {
                popupHeader = '<?php echo JText::_('COM_SITEADMINBLN_REMOVE_BANNER_CONFIRM'); ?>';
                popupContent = '<?php echo JText::_('COM_SITEADMINBLN_REMOVE_BANNER_LAST_ONE_WARNING'); ?>';
            }
            lgtCreatePopup('confirm', {
                'content'  : popupHeader,
                'subcontent' : popupContent,
                'yesText' : '<?php echo JText::_('COM_SITEADMINBLN_REMOVE'); ?>'
            }, function() {
                jQuery(btRemoveBanner).parents('.block-banner').remove();
                checkNumberofBlockBanner();
            });
        });

        function checkNumberofBlockBanner() {
            if (jQuery('.block-banners .block-banner:not(.col-new-banner)').length < 4) jQuery('.col-new-banner').show(); else jQuery('.col-new-banner').hide();
            if (jQuery('.block-banners .block-banner:not(.col-new-banner)').length <= 0) jQuery('.block-banner:not(.col-new-banner) .btn-remove-banner').hide(); else jQuery('.block-banner:not(.col-new-banner) .btn-remove-banner').show();
        }

        jQuery(document).on('click', '.block-banner .btn-upload-banner', function() {
            jQuery(this).parent().next('.bannerUpload').click();
        });

        jQuery(document).on('change', '.bannerUpload', function(event) {
            var files = event.target.files;
            var currentBlockBanner = jQuery(this).parents('.block-banner');

            if (files[0].type != 'image/png' && files[0].type != 'image/jpeg') {
                lgtCreatePopup('oneButton', {'content':'<?php echo JText::_('COM_SITEADMINBLN_UPLOAD_IMAGES_TYPE_WARNING'); ?>'}, function() {
                    lgtRemovePopup();
                });
                jQuery('.bannerUpload').val(null);
                return false;
            }
            if (files[0].size > maxFileSize) {
                lgtCreatePopup('oneButton', {'content':'<?php echo JText::sprintf('COM_SITEADMINBLN_UPLOAD_IMAGES_SIZE_WARNING', 8); ?>'}, function() {
                    lgtRemovePopup();
                });
                jQuery('.bannerUpload').val(null);
                return false;
            }

            if (files && files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    currentBlockBanner.attr('style', 'background-image:  url(' + e.target.result + ') ').show();
                    currentBlockBanner.find('.bannerFileName').val(files[0].name);
                    if (currentBlockBanner.hasClass('col-new-banner')) {
                        currentBlockBanner.removeClass('col-new-banner');
                        appendNewBannerBlock();
                    }
                };
                reader.readAsDataURL(files[0]);
            }
        });

        jQuery('.formEditBanner .btSave').click(function(e) {
            e.preventDefault();
            var validate = true;
            var warning = '';
            var bannerFileNames = [];
            var tempArrForCp = [];

            jQuery('.bannerFileName').each(function() {
                if (jQuery(this).val() != '') {
                    bannerFileNames.push(jQuery(this).val());
                }
            });

            jQuery.each(bannerFileNames, function(key, value) {
                if (tempArrForCp.indexOf(value) !== -1) {
                    validate = false;
                    warning = 'Hey man... Some images are reduplicated.';
                }
                tempArrForCp.push(value);
            });

            if (validate) {
                lgtCreatePopup('', {'content': 'Loading...'});
                jQuery('.formEditBanner').submit();
            } else {
                lgtCreatePopup('oneButton', {'content': warning}, function() {
                    lgtRemovePopup();
                });
            }
        });

        jQuery('.hiddenImage').one("load", function() {
            jQuery(this).parents('.block-banner').css({'background-image': 'url(\''+jQuery(this).attr('src') + '\')'});

            jQuery(this).parents('.block-banner').find('.btn-upload-banner').fadeIn(200);
            jQuery(this).parents('.block-banner').find('.lgt-loader').fadeOut(200);
        })
        .on('error', function() {
            jQuery(this).parents('.block-banner').css({
                'background-image': "url('/images/icons/tick_red.png')",
                'background-size': '32px',
                'background-position': '10px 10px'
            });
            jQuery(this).parents('.block-banner').find('.btn-upload-banner').fadeIn(200);
            jQuery(this).parents('.block-banner').find('.lgt-loader').fadeOut(200);
        })
        .each(function() {
            console.log(this.complete);
            if(this.complete) jQuery(this).load();
        });

    });
</script>