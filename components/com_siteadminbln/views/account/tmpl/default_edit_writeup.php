<?php
$device = JFactory::getSession()->get('device');
$is_mobile = $device == 'mobile' ? true : false;
?>
<script src="<?php echo JURI::base(); ?>components/com_community/assets/autosize.min.js"></script>
<div class="mlp-page mlp-edit-writeup-page sabln-page">
    <div class="header-page">
        <div class="back" onclick="goBack();">
            <img src="<?php echo JURI::base(); ?>images/Back_2x.png">
        </div>
        <div class="title center">
            <p><?php echo JText::_('COM_SITEADMINBLN_EDIT'); ?></p>
        </div>
    </div>
    <div class="contentPage">
        <form class="formChangeWriteup" action="#" method="post">
            <div class="form-group">
                <label for="txtWriteup"><?php echo JText::_('COM_SITEADMINBLN_WRITE_UP'); ?></label>
                <textarea  class="autosize" name="txtWriteup" id="txtWriteup" maxlength="140" cols="30" rows="4"><?php echo !empty($this->blnInfo->bln_writeup) ? $this->blnInfo->bln_writeup : ''; ?></textarea>
            </div>
            <p class="countWriteup">140</p>
            <div class="div-buttons">
                <input class="btCancel standard-button" type="button" onclick="goBack()" value="<?php echo JText::_('COM_SITEADMINBLN_CANCEL'); ?>" />
                <input class="btSave standard-button" type="button" value="<?php echo JText::_('COM_SAVE_BLN'); ?>" />
            </div>
        </form>
    </div>
</div>
<script>
    jQuery(document).ready(function() {
  
        jQuery('#txtWriteup').keydown(function(e){
             var a = jQuery(this).val().split("\n").length;
             console.log(a);
             if(e.keyCode == 13 && a >=5 ) return false;
            
        });
        jQuery('.autosize').each(function () {
                autosize(this);
            }).on('input', function () {
            autosize(this);
        });
        <?php if ($is_mobile) { ?>
        jQuery('.countWriteup').html(140 - parseInt(jQuery('#txtWriteup').val().length));

        jQuery('.formChangeWriteup .btSave').click(function() {
            var writeupContent = jQuery('#txtWriteup').val().trim();
            if (writeupContent == '') {
                jQuery('#txtWriteup').parents('.form-group').addClass('hasError');
                return false;
            }
            jQuery.ajax({
                url: '<?php echo JURI::root().'index.php?option=com_siteadminbln&view=account&task=editwriteup'; ?>',
                type: 'post',
                data: {
                    'content': writeupContent
                },
                beforeSend: function() {
                    lgtCreatePopup('', {'content': 'Loading...'});
                },
                success: function(data) {
                    lgtRemovePopup();
                    console.log(data);
                    
                    var res = JSON.parse(data);
                    if (res.error == 1) {
                        lgtCreatePopup('withCloseButton', {'content': res.comment});
                    } else {
                        location.href = '<?php echo JRoute::_('index.php?option=com_siteadminbln&view=account&action=mlp', false) ?>';
                    }
                },
                error: function() {
                    lgtRemovePopup();
                }
            });
        });
        jQuery('#txtWriteup').keyup(function() {
            if (jQuery('#txtWriteup').val().trim() == '') {
                jQuery('#txtWriteup').parents('.form-group').addClass('hasError');
            } else jQuery('#txtWriteup').parents('.form-group').removeClass('hasError');
            jQuery('.countWriteup').html(140 - parseInt(jQuery(this).val().length));
        });

        <?php } ?>
    });
</script>