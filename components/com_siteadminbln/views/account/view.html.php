<?php
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.view');

class SiteadminblnViewAccount extends JViewLegacy {
    
    function display($tpl = null) {
        // do nothing display
        $app = JFactory::getApplication();
        $document = JFactory::getDocument();
        $jinput = $app->input;

        $model  = $this->getModel('account');

        $act = $jinput->post->get('act', '', 'STRING');
        $action = $jinput->get->get('action', '', 'STRING');

        // Check permission
        $user = JFactory::getUser();
        
        $isSiteAdmin = false;
        if (count($model->checkSiteAdminBLn($user->id)) > 0) {
            $isSiteAdmin = true;
        }

        $this->adminid = $model->getBLNCircleAdmin();
        $this->ownerid = $model->getBLNCircleOwner();

        $this->blnInfo = $model->blnInfo;
        $this->model = $model;
        $this->isBLNOwner = $model->isBLNOwner;
        $this->isBLNAdmin = $model->isBLNAdmin;

        if ($act == 'create') {
            $array = array();
            $array['nameOR'] = $_POST['name'];
            $array['address'] = $_POST['address'];
            $array['postal'] = $_POST['postal'];
            $array['contact'] = strip_tags($_POST['contact']);

            $this->info = $model->saveBln($array);

            $result = array();
            $result['comment'] = 'hello';
            $result['data'] = $this->info;
            echo json_encode($result);
            die;
        }

        switch ($action) {
            case 'assign':
                require_once(JPATH_SITE.'/components/com_community/libraries/core.php');
                $this->role = $jinput->get->get('role', '', 'STRING');
                $limitstart = 0;
                $limit = 20;
                if (!$isSiteAdmin) {
                    $this->preventAccessing();
                } else {
                    $keyword = $jinput->post->get('searchKeyword', '', 'STRING');
                    if($this->role == 'ow') $page = 'ow';
                        else $page  = 'ad';
                    if ($keyword) {
                        $this->keyword = $keyword;
                       
                        $this->users = $model->getAlluser(TRUE, false, $limitstart, $limit, $keyword,$page);
                        $countusers = $model->getAlluser(TRUE, false, false, false, $keyword, $page);
                        $this->count = count($countusers);
                        
                    } else {
//                      $this->users = $model->getAlluser();
                        $this->users = $model->getAlluser(TRUE, false, $limitstart, $limit, '', $page);
                        $countusers = $model->getAlluser(TRUE, false, false, false, '', $page);
                        $this->count = count($countusers);
                    }
//                    unset($this->users[$user->id]);
                    if (isset($_POST['act']) && $_POST['act'] == 'load_user') {
                        if (isset($_POST['page'])) {
                            $pages = $_POST['page'];
                            $limitstart = $pages * $limit;
                        }
                        if ($_POST['status'] == 0)
                            $allusers = $model->getAlluser(TRUE, false, $limitstart, $limit,'',$page);
                        else $allusers = $model->getAlluser(TRUE, false, $limitstart, $limit, $_POST['suser'],$page);
                            $list_search_user = array();
                        if ($allusers) {
                            $i = 0;
                            foreach ($allusers as $key => $us) {
                                $user_id = $us->id;
                                $userm = CFactory::getUser($user_id);
                                $per = $model->changePermission($user_id, 2);
                                $list_search_user[$i]['id'] = $user_id;
                                $list_search_user[$i]['name'] = $userm->getDisplayName();
                                $list_search_user[$i]['date'] = $us->date;
                                $list_search_user[$i]['avatar'] = $userm->getAvatar();
                                $list_search_user[$i]['per'] = $per;
                                $i++;
                            }
                        }

                        $result = array();
                        $result['users'] = $list_search_user;
                        $result['total'] = count($countusers);
                        $result['status'] = true;
                        $result['page'] = $pages;
                        $result['act'] = $_POST['suser'];
                        echo json_encode($result);
                        die;
                    }

                    switch ($this->role) {
                        case 'ow':
                            $isOwner = false;
                            $ownerId = $model->getBLNCircleOwner();
                            if ($ownerId === $user->id) {
                                $isOwner = true;
                            }
                            if (!$isOwner) {
                                $this->preventAccessing();
                                return;
                            }
                            $this->roleText = JText::_('COM_SITEADMINBLN_OWNER');
                            $this->pageName = JText::_('COM_SITEADMINBLN_ASSIGN_OWNER');
                            break;
                        case 'ad':
                            $this->roleText = JText::_('COM_SITEADMINBLN_ADMIN');
                            $this->pageName = JText::_('COM_SITEADMINBLN_ASSIGN_ADMIN');
                            break;
                    }

                    parent::display('assignadmin');
                }

                break;
            case 'mlp':
                if (!$this->model->hasPermission('managingLoginPage')) {
                    $this->preventAccessing();
                } else {
                    $document->addStyleSheet(JUri::root() . '/components/com_siteadminbln/views/account/css/swiper.css');
                    $document->addScript(JUri::root() . '/components/com_siteadminbln/views/account/js/swiper.min.js');

                    $this->blnLogo = $this->model->getBLNLogo();
                    $this->blnBanners = $this->model->getBLNBanners();

                    parent::display('mlp');
                }

                break;
            case 'editbanner':
                if (!$this->model->hasPermission('managingLoginPage')) {
                    $this->preventAccessing();
                } else {
                    $this->blnLogo = $this->model->getBLNLogo();
                    $this->blnBanners = $this->model->getBLNBanners();

                    parent::display('edit_banner');
                }

                break;
            case 'editwriteup':
                if (!$this->model->hasPermission('managingLoginPage')) {
                    $this->preventAccessing();
                } else

                parent::display('edit_writeup');
                break;
            case 'view':
                parent::display('info');
                break;
            default:
                if (!$this->model->hasPermission('updateCompanyInfo')) {
                    $this->preventAccessing();
                } else

                parent::display($tpl);
                break;
        }
    }

    protected function preventAccessing() {
        header('Location: /404.html');
        exit;
//        echo '<div class="message">' . JText::_('COM_SITEADMINBLN_NO_ACCESS') . '</div>';
//        return;
    }

    protected function _prepareDocument() {
        $app = JFactory::getApplication();
        $menus = $app->getMenu();
        $title = null;
        // Because the application sets a default page title,
        // we need to get it from the menu item itself
        $menu = $menus->getActive();
        if ($menu) {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        } else {
            $this->params->def('page_heading', JText::_('COM_JOOMDLE_MY_NEWS'));
        }
    }
    
}
