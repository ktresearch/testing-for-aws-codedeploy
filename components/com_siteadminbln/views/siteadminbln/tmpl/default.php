<?php
defined('_JEXEC') or die();

?>

<div class="siteadminbln-page">
    <div class="title-page">
        <?php echo JText::_('COM_SITEADMINBLN_ADMIN'); ?>
    </div>
    
    <div class="sadminbln-admin-list">
        <a href="<?php echo JRoute::_("index.php?option=com_siteadminbln&view=account"); ?>"><div class="sadminbln-menu">
                <span class="sadminbln-title"><?php echo JText::_('COM_SITEADMINBLN_ACCOUNT');?></span>
                <span class="sadminbln-icon"></span>
        </div></a>
        <a href="<?php echo JRoute::_("index.php?option=com_siteadminbln&view=subscription"); ?>"><div class="sadminbln-menu">
            <span class="sadminbln-title"><?php echo JText::_('COM_SITEADMINBLN_SUBCRIPTION');?></span>
            <span class="sadminbln-icon"></span>
            </div></a>
        <a href="<?php echo JRoute::_("index.php?option=com_siteadminbln&view=addusers"); ?>"><div class="sadminbln-menu">
                <span class="sadminbln-title"><?php echo JText::_('COM_SITEADMINBLN_USER_MANAGEMENT');?></span>
                <span class="sadminbln-icon"></span>
        </div></a>
        <a href="<?php echo JRoute::_("index.php?option=com_siteadminbln&view=reports"); ?>"><div class="sadminbln-menu">
            <span class="sadminbln-title"><?php echo JText::_('COM_SITEADMINBLN_REPORTS');?></span>
            <span class="sadminbln-icon"></span>
        </div></a>
    </div>
</div>
