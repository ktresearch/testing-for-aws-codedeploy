<?php


defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class SiteadminblnViewSiteadminbln extends JViewLegacy {
    
    function display($tpl = null)
    {
        // Check permission
        require_once(JPATH_SITE.'/components/com_siteadminbln/models/account.php');
        
        $user = JFactory::getUser();
        
        $isSiteAdmin = false;
        $model = new SiteadminblnModelAccount();
        if (count($model->checkSiteAdminBLn($user->id)) > 0) {
            $isSiteAdmin = true;
        }
        if (!$isSiteAdmin) {
            header('Location: /404.html');
            exit;
//            echo '<div class="message">' . JText::_('COM_SITEADMINBLN_NO_ACCESS') . '</div>';
//            return;
        }
        
        // do nothing display
        parent::display($tpl);
    }
    
}
?>