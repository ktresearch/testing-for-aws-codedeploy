<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class SiteadminblnViewManageuser extends JViewLegacy {

    function display($tpl = null) {
        $app = JFactory::getApplication();
        $jinput = $app->input;
        require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
        require_once(JPATH_BASE . '/components/com_siteadminbln/models/account.php');
        $model = new SiteadminblnModelAccount();
        $user = JFactory::getUser();
        $isSiteAdmin = false;
        if (count($model->checkSiteAdminBLn($user->id)) > 0) {
            $isSiteAdmin = true;
        }
        if (!$isSiteAdmin) {
            header('Location: /404.html');
            exit;
//            echo '<div class="message">' . JText::_('COM_SITEADMINBLN_NO_ACCESS') . '</div>';
//            return;
        }

        $keyword = $jinput->post->get('qrkeyword', '', 'STRING');
        $page = 'ad';
        $act = JRequest::getVar('action', null, 'NEWURLFORM');
        if (!$act)
            $act = JRequest::getVar('action');
        if ($_POST['action'] == 'turnoff') {
            $userid = $_POST['uid'];
            $status = 1;
            $model->changeOwnercircle($userid);
            $model->changePermission($userid, $status);
            $result = array();
            $result['error'] = $userid;
            $result['comment'] = JText::_('COM_SITEADMINBLN_SEND_CSV_SUCCESS');
            echo json_encode($result);
            die;
        }
        if ($_POST['action'] == 'turnon') {
            $userid = $_POST['uid'];
            $status = 0;
            $model->changePermission($userid, $status);
            $result = array();
            $result['error'] = $userid;
            $result['comment'] = JText::_('COM_SITEADMINBLN_SEND_CSV_SUCCESS');
            echo json_encode($result);
            die;
        }
        if ($keyword) {


            $this->action = $act;
            $limitstart = 0;
            $limit = 20;

            $this->keyword = $keyword;
//            $this->users = $model->searchPeople($keyword, '', '', 0, 0);

            $this->users = $model->getAlluser(TRUE, false, $limitstart, $limit, $keyword,$page);

            $countusers = $model->getAlluser(TRUE, false, false, false, $keyword,$page);
            $this->count = count($countusers);

            if (isset($_POST['act']) && $_POST['act'] == 'load_user') {
                if (isset($_POST['page'])) {
                    $pages = $_POST['page'];
                    $limitstart = $pages * $limit;
                }
                $allusers = $model->getAlluser(TRUE, false, $limitstart, $limit, $keyword,$page);

                $list_search_user = array();
                if ($allusers) {
                    $i = 0;
                    foreach ($allusers as $key => $us) {
                        $user_id = $us->id;
                        $userm = CFactory::getUser($user_id);
                        $per = $model->changePermission($user_id, 2);
                        $list_search_user[$i]['id'] = $user_id;
                        $list_search_user[$i]['name'] = $userm->getDisplayName();
                        $list_search_user[$i]['date'] = $us->date;
                        $list_search_user[$i]['avatar'] = $userm->getAvatar();
                        $list_search_user[$i]['per'] = $per;
                        $i++;
                    }
                }

                $result = array();
                $result['users'] = $list_search_user;
                $result['total'] = count($countusers);
                $result['status'] = true;
                $result['page'] = $pages;
                echo json_encode($result);
                die;
            }
        } else {
            $this->action = $act;
            $limitstart = 0;
            $limit = 20;
            if (isset($_POST['suser'])) {
                $suser = $_POST['suser'];
            } else {
                $suser = '';
            }
            if ($act == 0 || $act == null) {
                $this->users = $model->getAlluser(TRUE, false, $limitstart, $limit, $suser,$page);
                $countusers = $model->getAlluser(TRUE, false, false, false, $suser,$page);
                $this->count = count($countusers);
            } else if ($act == 1) {
                $this->users = $model->getAlluser(TRUE, $act, $limitstart, $limit, $suser,$page);
                $countusers = $model->getAlluser(TRUE, $act, false, false, $suser,$page);
                $this->count = count($countusers);
                $allusers = $model->getAlluser(TRUE, false, $limitstart, $limit, $suser,$page);
            } else if ($act == 2) {
                $this->users = $model->getAlluser(TRUE, $act, $limitstart, $limit, $suser,$page);
                $countusers = $model->getAlluser(TRUE, $act, false, false, $suser,$page);
                $this->count = count($countusers);
                $allusers = $model->getAlluser(TRUE, $act, $limitstart, $limit, $suser,$page);
            }
            if (isset($_POST['act']) && $_POST['act'] == 'load_user') {
                if (isset($_POST['page'])) {
                    $pages = $_POST['page'];
                    $limitstart = $pages * $limit;
                }

                if ($_POST['per'] == 1)
                    $allusers = $model->getAlluser(TRUE, $act, $limitstart, $limit, $suser,$page);
                else if ($_POST['per'] == 2)
                    $allusers = $model->getAlluser(TRUE, $act, $limitstart, $limit, $suser,$page);
                else
                    $allusers = $model->getAlluser(TRUE, false, $limitstart, $limit, $suser,$page);



                $list_search_user = array();
                if ($allusers) {
                    $i = 0;
                    foreach ($allusers as $key => $us) {
                        $user_id = $us->id;
                        $userm = CFactory::getUser($user_id);
                        $per = $model->changePermission($user_id, 2);
                        $list_search_user[$i]['id'] = $user_id;
                        $list_search_user[$i]['name'] = $userm->getDisplayName();
                        $list_search_user[$i]['date'] = $us->date;
                        $list_search_user[$i]['avatar'] = $userm->getAvatar();
                        $list_search_user[$i]['per'] = $per;
                        $i++;
                    }
                }

                $result = array();
                $result['users'] = $list_search_user;
                $result['total'] = count($countusers);
                $result['status'] = true;
                $result['page'] = $pages;
                echo json_encode($result);
                die;
            }
        }
        parent::display($tpl);
    }

}
