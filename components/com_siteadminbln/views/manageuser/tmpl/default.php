<?php
defined('_JEXEC') or die('Restricted access');
$users = $this->users;
require_once(JPATH_BASE . '/components/com_siteadminbln/models/account.php');
$model = new SiteadminblnModelAccount();
if ($this->action == 1)
    $per = "selected";
else if ($this->action == 2)
    $noper = "selected";
else
    $all = "selected";
$sl = $this->count;
$number = ceil($sl / 20);
if (!$this->action)
    $act = 0;
else
    $act = $this->action;
?>
<div class="mnuser-page">
    <div class="manusers-bar">
        <div class="manusers-title">
            <span class="back-icon" onclick="goBack();"></span>
            <span class="title-page"><?php echo JText::_('COM_SITEADMINBLN_MANAGE_USER'); ?></span>
        </div>
    </div>
    <div class="mu-content">
        <div class="qr-search-box search-box">
            <form action="<?php echo JRoute::_('index.php?option=con_siteadminbln&view=manageuser&action=searchuser'); ?>" method="post" id="form-qr-search" class="form-search">
                <div class="div-search-user">
                    <input type="text" name="qrkeyword" class="qrkeyword searchKeyword" value="<?php echo $this->keyword ? $this->keyword : ''; ?>" placeholder="<?php echo JText::_('COM_SITEADMINBLN_QR_SEARCH_PLACEHOLDER'); ?>" autocomplete="off"/>
                    <button class="btn-search-user" type="submit"></button>
                </div>
                <div class="div-cancel-search animated">
                    <button class="btn-cancel-search" type="button" onclick="resetFormSearch()"></button>
                </div>
            </form>
        </div>

        <div class="sort-user paySelected">
            <form action="<?php echo JRoute::_('index.php?option=con_siteadminbln&view=manageuser&action=searchuser'); ?>" method="post" id="" class="">
                <div class="sort-selected"><?php echo JText::_('COM_SITEADMINBLN_MANAGEUSER_SORT'); ?></div>
                <ul class="ulTypeSelected">
                    <li class="lisort <?php echo $per; ?>" value="1"><?php echo JText::_('COM_SITEADMINBLN_WITH_PERMISSOION'); ?></li>
                    <li class="lisort <?php echo $noper; ?>" value="2"><?php echo JText::_('COM_SITEADMINBLN_WITHOUT_PERMISSOION'); ?></li>
                    <li class="lisort <?php echo $all; ?>" value="0">All</li>
                </ul>
            </form>
        </div>
        <div class="list-user">
            <div class="qr-users-list users-list">
                <?php
                if (!empty($users)) {
                    foreach ($users as $u) {
                        $user = CFactory::getUser($u->id);
                        $per = $model->changePermission($user->id, 2);
                        ?>
                        <div class="qr-user col-sm-6 user-item" data-uid="<?php echo $user->id; ?>">
                            <div class="user-item-image"><img src="<?php echo $user->getAvatar(); ?>" alt="<?php echo $user->name; ?>" title="<?php echo $user->name; ?>"/></div>
                            <div class="user-item-info">
                                <div class="user-item-name"><?php echo $user->name; ?></div>
                                <a onclick="checkPermisstion(<?php echo $user->id; ?>)" class="yesnoIcon <?php echo ($per) ? no : yes ?>"  id="<?php echo $user->id; ?>" >
                                    <div class="block"></div>
                                </a>
                            </div>
                        </div>
    <?php } ?>
<?php } else echo '<p class="warning-message">' . JText::_('COM_SITEADMINBLN_NO_RESULTS_FOUND') . '</p>'; ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function checkPermisstion(uid) {

        if (jQuery('#' + uid).hasClass('yes')) {

            lgtCreatePopup('confirm', {
                'yesText': '<?php echo JText::_('COM_SITEADMINBLN_SUB_PROCEED'); ?>',
                'content': "<?php echo '<b>' . JText::_('COM_SITEADMINBLN_MANAGEUSER_MASSAGE') . '</b>'; ?>"
            }, function () {

                // Run AJAX here
                jQuery.ajax({
                    url: window.location.href,
                    type: 'POST',
                    data: {uid: uid, action: 'turnoff'},
                    beforeSend: function () {
                        lgtCreatePopup('', {'content': 'Loading...'});
                    },
                    success: function (data, textStatus, jqXHR) {
                        var res = JSON.parse(data);
                        console.log(res);
                        jQuery('#' + uid).removeClass('yes').addClass('no');
                        lgtRemovePopup();

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                    }
                });

            });

        } else if (jQuery('#' + uid).hasClass('no')) {
            jQuery('#' + uid).removeClass('no').addClass('yes');
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: {uid: uid, action: 'turnon'},
                beforeSend: function () {
                    lgtCreatePopup('', {'content': 'Loading...'});
                },
                success: function (data, textStatus, jqXHR) {
                    var res = JSON.parse(data);
                    console.log(res);
                    lgtRemovePopup();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                }
            });
        }
    }
    
    jQuery(document).ready(function () {
        if( jQuery('.qrkeyword').val() != ''){
            jQuery('.div-search-user').addClass('hasValue');
            jQuery('.div-cancel-search').removeClass('slideOutRight').addClass('hasValue').addClass('slideInRight');
        }
        var loading = false;
        var a = 1;
        var number = '<?php echo $number; ?>';
        var act = '<?php echo $act; ?>';
        function loadUsers() {
            var suser = jQuery('.qrkeyword').val();
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    act: "load_user",
                    suser: suser,
                    page: a
                },
                beforeSend: function () {
                    console.log('load more user: ' + a);
                jQuery('.qr-users-list').append('<div class="divLoadMore">Loading...<div class="loading"></div></div>');
                },
                success: function (result, textStatus, jqXHR) {
                    var res = JSON.parse(result);
                    console.log(res);
                    var status = res.status;
                    var b = res.users;
                    var sta = '';
                    jQuery.each(b, function (key, value) {
                        var user_id = b[key]['id'];
                        var fullname = b[key]['name'];
                        var user_avatar = b[key]['avatar'];
                        var mdate = b[key]['date'];
                        var per = b[key]['per'];
                        if (per != 0)
                            sta = 'no';
                        else
                            sta = 'yes';
                        var html;

                        html = '<div class="qr-user col-sm-6 user-item" data-cid="' + user_id + '" >' +
                                '<div class="user-item-image"><img src="' + user_avatar + '" alt="' + fullname + '" title="' + fullname + '"/></div>' +
                                '<div class="user-item-info">' +
                                '<div class="user-item-name">' + fullname + '</div>' +
                                '<a onclick="checkPermisstion(' + user_id + ')" class="yesnoIcon ' + sta + '"  id="' + user_id + '" >' +
                                '<div class="block"></div>' +
                                '</a>' +
                                '</div>' +
                                '</div>';
                        jQuery('.divLoadMore').remove();
                        jQuery('.qr-users-list').append(html);
                    });
                    a = a + 1;
                    loading = false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                    loading = false;
                }
            });
        }
        function loadUsersPer() {
            var suser = jQuery('.qrkeyword').val();
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    act: "load_user",
                    suser: suser,
                    page: a,
                    per: act,

                },
                beforeSend: function () {
                    console.log('load more user: ' + act);
                jQuery('.qr-users-list').append('<div class="divLoadMore">Loading...<div class="loading"></div></div>');
                },
                success: function (result, textStatus, jqXHR) {
                    var res = JSON.parse(result);

                    var status = res.status;
                    var b = res.users;
                    var sta = '';
                    jQuery.each(b, function (key, value) {
                        var user_id = b[key]['id'];
                        var fullname = b[key]['name'];
                        var user_avatar = b[key]['avatar'];
                        var mdate = b[key]['date'];
                        var per = b[key]['per'];
                        if (per != 0)
                            sta = 'no';
                        else
                            sta = 'yes';
                        var html;

                        html = '<div class="qr-user col-sm-6 user-item" data-cid="' + user_id + '" >' +
                                '<div class="user-item-image"><img src="' + user_avatar + '" alt="' + fullname + '" title="' + fullname + '"/></div>' +
                                '<div class="user-item-info">' +
                                '<div class="user-item-name">' + fullname + '</div>' +
                                '<a onclick="checkPermisstion(' + user_id + ')" class="yesnoIcon ' + sta + '"  id="' + user_id + '" >' +
                                '<div class="block"></div>' +
                                '</a>' +
                                '</div>' +
                                '</div>';
                        jQuery('.divLoadMore').remove();
                        jQuery('.qr-users-list').append(html);
                    });
                    a = a + 1;
                    loading = false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                    loading = false;
                }
            });
        }

        function loadUsersNotPer() {
            var suser = jQuery('.qrkeyword').val();
            jQuery.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    act: "load_user",
                    suser: suser,
                    page: a,
                    per: act,

                },
                beforeSend: function () {
                    console.log('load more user: ' + act);
                jQuery('.qr-users-list').append('<div class="divLoadMore">Loading...<div class="loading"></div></div>');
                },
                success: function (result, textStatus, jqXHR) {
                    var res = JSON.parse(result);

                    var status = res.status;
                    var b = res.users;
                    var sta = '';
                    jQuery.each(b, function (key, value) {
                        var user_id = b[key]['id'];
                        var fullname = b[key]['name'];
                        var user_avatar = b[key]['avatar'];
                        var mdate = b[key]['date'];
                        var per = b[key]['per'];
                        if (per != 0)
                            sta = 'no';
                        else
                            sta = 'yes';
                        var html;

                        html = '<div class="qr-user col-sm-6 user-item" data-cid="' + user_id + '" >' +
                                '<div class="user-item-image"><img src="' + user_avatar + '" alt="' + fullname + '" title="' + fullname + '"/></div>' +
                                '<div class="user-item-info">' +
                                '<div class="user-item-name">' + fullname + '</div>' +
                                '<a onclick="checkPermisstion(' + user_id + ')" class="yesnoIcon ' + sta + '"  id="' + user_id + '" >' +
                                '<div class="block"></div>' +
                                '</a>' +
                                '</div>' +
                                '</div>';
                        jQuery('.divLoadMore').remove();
                        jQuery('.qr-users-list').append(html);
                    });
                    a = a + 1;
                    loading = false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                    loading = false;
                }
            });
        }

        jQuery(window).scroll(function () {
            if ((jQuery(window).scrollTop() + jQuery(window).height()) >=
                    (jQuery('.mu-content').offset().top + jQuery('.mu-content').height())) {
                if (a < number && loading == false && (act == 0 || act == 'searchuser')) {
                    loading = true;
                    console.log('loading');
                    loadUsers();
                } else if (a < number && loading == false && act == 1) {
                    loading = true;
                    console.log('loading');
                    loadUsersPer();
                } else if (a < number && loading == false && act == 2) {
                    loading = true;
                    console.log('loading');
                    loadUsersNotPer();
                }

            }

        });

        jQuery('.sort-selected').click(function () {
            jQuery(this).next(".ulTypeSelected").toggle();
        });

        jQuery('.ulTypeSelected li').click(function () {
            console.log(jQuery(this).val());
            jQuery('.ulTypeSelected li').removeClass('selected');
            jQuery(this).addClass('selected');
            window.location.href = "<?php echo JRoute::_('index.php?option=com_siteadminbln&view=manageuser&action=', false); ?>" + jQuery(this).val();

        });
        jQuery('.qrkeyword').on('keyup', function () {
            if (jQuery(this).val() != '') {
                jQuery('.div-search-user').addClass('hasValue');
                jQuery('.div-cancel-search').removeClass('slideOutRight').addClass('hasValue').addClass('slideInRight');
            } else {
                jQuery('.div-search-user').removeClass('hasValue');
                jQuery('.div-cancel-search').removeClass('hasValue').removeClass('slideInRight').addClass('slideOutRight');
            }
        });
        
    });
    function resetFormSearch() {
        jQuery('.qrkeyword').val('');
        jQuery('.div-search-user').removeClass('hasValue');
        jQuery('.div-cancel-search').removeClass('hasValue').removeClass('slideInRight').addClass('slideOutRight');
    }
    function goBack(){
        window.location.href = '<?php echo JRoute::_('index.php?option=com_siteadminbln&view=addusers'); ?>';
    }
</script>
