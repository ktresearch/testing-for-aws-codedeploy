<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class SiteadminblnModelReports extends JModelList {
    public function display() {
        
    }
    
    public function getNetworkReport($blnid) {
        $db = JFactory::getDBO();
        
        $result = array();

        require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
        $groupsModel = CFactory::getModel('groups');
        
        $result['membercount'] = $groupsModel->getMembersCount($blnid, true);
        
        $query2 = 'SELECT a.' . $db->quoteName('id')
            . ' FROM ' . $db->quoteName('#__community_groups') . ' a '
            . ' INNER JOIN ' . $db->quoteName('#__users') . ' b ON a.' . $db->quoteName('ownerid') . ' = b.' . $db->quoteName('id')
            . ' WHERE (a.' . $db->quoteName('parentid') . '>' . $db->quote(0)
            . ' OR a.' . $db->quoteName('isBLN') . '=' . $db->quote('1') . ')'
            . ' AND a.path LIKE ' . $db->Quote( '%/' . $blnid . '%' );
        $db->setQuery($query2);
        $subcircles = $db->loadObjectList();
        $result['subcirclescount'] =  count($subcircles);
        
        return $result;
    }
    
    public function dowloadNetworkReport($blnid) {
        require_once("./courses/lib/phpexcel/PHPExcel/IOFactory.php");

        //set the desired name of the excel file
        $fileName = 'Network_Report_'.date('Ymd', time());

        require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
        $groupsModel = CFactory::getModel('groups');

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Me")->setLastModifiedBy("Me")->setTitle("My Excel Sheet")->setSubject("My Excel Sheet")->setDescription("Excel Sheet")->setKeywords("Excel Sheet")->setCategory("Me");

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Add column headers
        $objPHPExcel->getActiveSheet()
                                ->setCellValue('A1', 'Name of member:')
                                ->setCellValue('B1', 'Date of joining:')
                                ;
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);

        $excelData = array();
        $membersbln = $groupsModel->getMembersBLN($blnid);
        if (count($membersbln) > 0) {
            foreach ($membersbln as $mem) {
                $member = array();
                $user = CFactory::getUser($mem->memberid);
                $member[] = $user->getDisplayName();
                $member[] = date('m/d/Y', $mem->date);
               
                $excelData[] = $member;
            }
        }
        
        //Put each record in a new cell
        for($i=0; $i<count($excelData); $i++){
                $ii = $i+2;
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$ii, $excelData[$i][0]);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$ii, $excelData[$i][1]);
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

        // Set worksheet title
        $objPHPExcel->getActiveSheet()->setTitle('Members');
        
        $objPHPExcel->createSheet();
        
        // Second sheet
        $objPHPExcel->setActiveSheetIndex(1);
        
        $genration = $groupsModel->getBLNGenerationCount($blnid);      

        if (count($genration) > 0) {
            $genrationtotal = $genration[0]->level;
        } else { $genrationtotal = 0; }
        
        $blnroot = JTable::getInstance('Group', 'CTable');
        $blnroot->load($blnid);


//        $firstLayerCirclesBLN = $groupsModel->getChildrenBLN($blnid);
        $circlesBLN = $groupsModel->getAllCircleBLN($blnid,false,false,'',true);
        
        // Add column headers
        $alphas = range('A', 'Z');
        $cc = 2;
        for($c=0; $c< (int)$genrationtotal; $c++) {
            if ($c == 0) {
                $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Parent Circle');
                $objPHPExcel->getActiveSheet()->setCellValue('B1', '');
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
                $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle('B1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            } else {
                $objPHPExcel->getActiveSheet()->setCellValue($alphas[$cc].'1', 'Generation '.$c);
                $objPHPExcel->getActiveSheet()->setCellValue($alphas[$cc+1].'1', '');
                $objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$cc])->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$cc+1])->setWidth(15);
                $objPHPExcel->getActiveSheet()->getStyle($alphas[$cc].'1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle($alphas[$cc+1].'1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle($alphas[$cc].'1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($alphas[$cc+1].'1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $cc = $cc+2;
            }
        }
        
        if (count($circlesBLN) > 0) {
            $level = 0;
            $row = 2;
            $col = 0;
            foreach ($circlesBLN as $circle) {

                if ($circle->level > $level) {
                    $level = $circle->level;
                
                    if ($circle->approvals == 0) {
                        $privacy = 'Open';
                    } else {
                        if ($circle->unlisted == 0) {
                            $privacy = 'Private';
                        } else {
                            $privacy = 'Close';
                        }
                    }
                    $user = CFactory::getUser($circle->ownerid);
                    $membercount = $groupsModel->getMembersCount($circle->id, true);

                    $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col].$row, $circle->name);
                    $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].$row, $privacy);
                    $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].($row+1), $user->getDisplayName());
                    $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].($row+2), $membercount);
//                    $objPHPExcel->getActiveSheet()->getStyle($alphas[$col].$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
//                    $objPHPExcel->getActiveSheet()->getStyle($alphas[$col+1].$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
//                    $objPHPExcel->getActiveSheet()->getStyle($alphas[$col+1].($row+1))->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
//                    $objPHPExcel->getActiveSheet()->getStyle($alphas[$col+1].($row+2))->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                    
                    $col = $col+2;
                } else if ($circle->level == $level) {
                    $col = $col-2;
                    $row = $row+3;
                
                    if ($circle->approvals == 0) {
                        $privacy = 'Open';
                    } else {
                        if ($circle->unlisted == 0) {
                            $privacy = 'Private';
                        } else {
                            $privacy = 'Close';
                        }
                    }
                    $user = CFactory::getUser($circle->ownerid);
                    $membercount = $groupsModel->getMembersCount($circle->id, true);

                    $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col].$row, $circle->name);
                    $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].$row, $privacy);
                    $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].($row+1), $user->getDisplayName());
                    $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].($row+2), $membercount);
                    
                    $col = $col+2;
                } else {
                    $row = $row+3;
                    $col = ($circle->level-1)*2;
                    $level = $circle->level;
                    
                    if ($circle->approvals == 0) {
                        $privacy = 'Open';
                    } else {
                        if ($circle->unlisted == 0) {
                            $privacy = 'Private';
                        } else {
                            $privacy = 'Close';
                        }
                    }
                    $user = CFactory::getUser($circle->ownerid);
                    $membercount = $groupsModel->getMembersCount($circle->id, true);

                    $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col].$row, $circle->name);
                    $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].$row, $privacy);
                    $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].($row+1), $user->getDisplayName());
                    $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].($row+2), $membercount);
                    
                    $col = $col+2;
                }
                
            }
            
            $objPHPExcel->getActiveSheet()->getStyle('A1:'.$alphas[($genrationtotal*2) -1].($row+2))->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:'.$alphas[($genrationtotal*2) -1].($row+2))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            
            $objPHPExcel->getActiveSheet()->getStyle('A2:'.$alphas[($genrationtotal*2) -1].($row+2))->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );

            $objPHPExcel->getActiveSheet()->getStyle('A2:'.$alphas[($genrationtotal*2) -1].($row+2))->applyFromArray($style);
        }
        
        // Set worksheet title
        $objPHPExcel->getActiveSheet()->setTitle('All circles');
        
        // $objPHPExcel->createSheet();
        
        // Gen sheet
        /*
        if ((int)$genrationtotal > 1) {
            for($gen=1; $gen< (int)$genrationtotal; $gen++) {
                $index = $gen+1;
                $objPHPExcel->setActiveSheetIndex($index);

                $circlesGen = $groupsModel->getGenerationBLN($gen);

                // Add column headers
//                $alphas = range('A', 'Z');
                $objPHPExcel->getActiveSheet()
                                ->setCellValue('A1', 'Parent Circle')
                                ->setCellValue('B1', '')
                                ->setCellValue('C1', 'Child Circle')
                                ->setCellValue('D1', '')
                        ;
                $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);

                if (count($circlesGen) > 0) {
                    $level = $gen-1;
                    $row = 2;
                    $col = 0;
                    foreach ($circlesGen as $circle) {

                        if ($circle->level > $level) {
                            $level = $circle->level;

                            if ($circle->approvals == 0) {
                                $privacy = 'Open';
                            } else {
                                if ($circle->unlisted == 0) {
                                    $privacy = 'Private';
                                } else {
                                    $privacy = 'Close';
                                }
                            }
                            $user = CFactory::getUser($circle->ownerid);
                            $membercount = $groupsModel->getMembersCount($circle->id, true);

                            $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col].$row, $circle->name);
                            $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].$row, $privacy);
                            $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].($row+1), $user->getDisplayName());
                            $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].($row+2), $membercount);

                            $col = $col+2;
                        } else if ($circle->level == $level) {
                            if ($col > 2)
                                $col = $col-2;
                            $row = $row+3;

                            if ($circle->approvals == 0) {
                                $privacy = 'Open';
                            } else {
                                if ($circle->unlisted == 0) {
                                    $privacy = 'Private';
                                } else {
                                    $privacy = 'Close';
                                }
                            }
                            $user = CFactory::getUser($circle->ownerid);
                            $membercount = $groupsModel->getMembersCount($circle->id, true);

                            $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col].$row, $circle->name);
                            $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].$row, $privacy);
                            $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].($row+1), $user->getDisplayName());
                            $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].($row+2), $membercount);

                            $col = $col+2;
                        } else {
                            $row = $row+3;
                            $col = 0;
                            $level = $circle->level;

                            if ($circle->approvals == 0) {
                                $privacy = 'Open';
                            } else {
                                if ($circle->unlisted == 0) {
                                    $privacy = 'Private';
                                } else {
                                    $privacy = 'Close';
                                }
                            }
                            $user = CFactory::getUser($circle->ownerid);
                            $membercount = $groupsModel->getMembersCount($circle->id, true);

                            $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col].$row, $circle->name);
                            $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].$row, $privacy);
                            $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].($row+1), $user->getDisplayName());
                            $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col+1].($row+2), $membercount);

                            $col = $col+2;
                        }

                    }
                    
                    $objPHPExcel->getActiveSheet()->getStyle('A1:D'.($row+2))->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                    $style = array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A2:'.$alphas[$col+3].($row+2))->applyFromArray($style);
                }

                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                
                // Set worksheet title
                $objPHPExcel->getActiveSheet()->setTitle('Gen '.$gen);
                if ($gen < ((int)$genrationtotal)-1)
                    $objPHPExcel->createSheet();
            }
        }
        */
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(-1);
        $objPHPExcel->setActiveSheetIndex(0);
        
        //save the file to the server (Excel2007)
        $config = new JConfig();
        if (!is_dir($config->dataroot . '/temp')) {
            mkdir($config->dataroot . '/temp', 0775);
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($config->dataroot.'/temp/' . $fileName . '.xlsx');
        
        return $fileName;
    }
    
    public function dowloadCourseReport($courseid) {
        require_once("./courses/lib/phpexcel/PHPExcel/IOFactory.php");
        require_once(JPATH_BASE . '/components/com_community/libraries/core.php');

        $coursereport = JoomdleHelperContent::call_method ('get_course_report', (int) $courseid);        

        if ($coursereport['status']) {
            $data = $coursereport['data'];
            
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Me")->setLastModifiedBy("Me")->setTitle("My Excel Sheet")->setSubject("My Excel Sheet")->setDescription("Excel Sheet")->setKeywords("Excel Sheet")->setCategory("Me");

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Add column headers
        $objPHPExcel->getActiveSheet()
                                    ->setCellValue('A1', 'Course Name:')
                                    ->setCellValue('B1', 'Course Owner:')
                                    ->setCellValue('C1', 'Course Creation Date:')
                                    ->setCellValue('D1', 'Course Manager: (if any)')
                                    ->setCellValue('E1', 'Course Facilitator: (if any)')
                                    ->setCellValue('F1', 'Total Members:')
                                    ->setCellValue('G1', 'Total Completion:')
                                    ->setCellValue('H1', 'Total Completion Percentage:')
                                    ->setCellValue('I1', 'Average time spent on the course:')
                                ;
            $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:I2')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:I2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

            $courseinfo = $data['courseinfo'];

            $fileName = '';

            if (count($courseinfo) > 0) {
                //set the desired name of the excel file
                $fileName = 'Course_Report_'.trim($courseinfo['fullname']).'_'.date('Ymd', time());

                $objPHPExcel->getActiveSheet()->setCellValue('A2', $courseinfo['fullname']);
                $user = CFactory::getUser($courseinfo['creator']);
                $objPHPExcel->getActiveSheet()->setCellValue('B2', $user->getDisplayName());
                $objPHPExcel->getActiveSheet()->setCellValue('C2', date('m/d/Y', $courseinfo['created']));
                    
                if ($courseinfo['course_manager'] != '') {
                    $managers = explode(', ', $courseinfo['course_manager']);
                    $row = 2;
                    foreach ($managers as $m) {
                        $userm = CFactory::getUser($m);
                        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $userm->getDisplayName());
                        $row++;
                    }
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue('D2', '');
                }
                if ($courseinfo['course_facilitator'] != '') {
                    $facilitators = explode(', ', $courseinfo['course_facilitator']);
                    $row = 2;
                    foreach ($facilitators as $fa) {
                        $userf = CFactory::getUser($fa);
                        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $userf->getDisplayName());
                        $row++;
                    }
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue('E2', '');
                }
                
                $objPHPExcel->getActiveSheet()->setCellValue('F2', $courseinfo['count_learner']);
                $objPHPExcel->getActiveSheet()->setCellValue('G2', $courseinfo['count_completed']);
                $percentage = $courseinfo['count_learner'] ? (round($courseinfo['count_completed']*100/$courseinfo['count_learner'])) : 0;
                $objPHPExcel->getActiveSheet()->setCellValue('H2', $percentage.'%');
//                if ($courseinfo['average_time_spent'] < 60*60) {
//                    $ave = ($courseinfo['average_time_spent']/60).' minutes';
//                } else {
                    $ave = round(($courseinfo['average_time_spent']/(60*60)), 1).' hour';
//                }
                $objPHPExcel->getActiveSheet()->setCellValue('I2', $ave);
                
            }
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
                'font'  => array(
                    'name'  => 'Calibri'
                )
            );

            $objPHPExcel->getActiveSheet()->getStyle('A1:I2')->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->getFont()->setSize(11);

            //Put each record in a new cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(16);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(16);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);

            // Set worksheet title
            $objPHPExcel->getActiveSheet()->setTitle('Course Info');

            $objPHPExcel->createSheet();

            // ======================== Second sheet ===========================
            $objPHPExcel->setActiveSheetIndex(1);

            $membersinfo = $data['membersinfo'];        

            // Add column headers
            $objPHPExcel->getActiveSheet()
                                    ->setCellValue('A1', 'Member name:')
                                    ->setCellValue('B1', 'Start date:')
                                    ->setCellValue('C1', 'Completed date: (if not completed, to show date as In-progress)')
                                    ->setCellValue('D1', 'Total Time spent (hour):')
                                    ->setCellValue('E1', 'Overall grade:')
                                    ;
            $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->setSize(12);

            $row = 2;
            if (count($membersinfo) > 0) {
                foreach ($membersinfo as $member) {
                    $usermem = CFactory::getUser($member['username']);
                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $usermem->getDisplayName());
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, date('m/d/Y', $member['enrol_time']));

                    if ($member['time_completed']) {
                        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, date('m/d/Y', $member['time_completed']));
                    } else {
                        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'in progress');
                    }
                    if ($member['time_spent']) {
                        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, round($member['time_spent']/(60*60), 1));
                    }
                    if ($member['overall_grade'] && $member['overall_grade'] != 'Ungraded') {
                        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $member['overall_grade']);
                    }
                    $row++;
                }
            }
            $objPHPExcel->getActiveSheet()->getStyle('A1:'.'E'.($row-1))->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $style2 = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
                'font'  => array(
                    'name'  => 'Calibri'
                )
            );

            $objPHPExcel->getActiveSheet()->getStyle('A1:'.'E'.($row-1))->applyFromArray($style2);
            $objPHPExcel->getActiveSheet()->getStyle('A1:E'.($row-1))->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:E'.($row-1))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(17);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);

            // Set worksheet title
            $objPHPExcel->getActiveSheet()->setTitle('Course Members');
        
            $objPHPExcel->createSheet();

            // ========================== Third sheet =========================
            $objPHPExcel->setActiveSheetIndex(2);

            $feedbackinfo = $data['feedbackinfo'];        

            // Add column headers
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Member name:');
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);

            $alphas = range('A', 'Z');
            if (count($feedbackinfo) > 0) {
                $row = 2;
                $col = 1;
                if (count($feedbackinfo[0]['rank_arr']) > 0) {
                    foreach ($feedbackinfo[0]['rank_arr'] as $rank) {
                        $objPHPExcel->getActiveSheet()->setCellValue($alphas[$col].'1', 'Feedback '.$col);
                        $objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$col])->setAutoSize(true);
                        $objPHPExcel->getActiveSheet()->getStyle('A1:'.$alphas[$col].'1')->getFont()->setBold(true);
                        $col++;
                    }
                }
                foreach ($feedbackinfo as $feedback) {
                    $usermem = CFactory::getUser($feedback['username']);
                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $usermem->getDisplayName());
                    $f = 1;
                    $rank_value = '';                    
                    foreach ($feedback['rank_arr'] as $rank) {
                        switch ($rank['feedback']) {
                            case '0':
                                $rank_value = 'x';
                                break;
                            case '1':
                                $rank_value = 'xx';
                                break;
                            case '2':
                                $rank_value = 'xxx';
                                break;
                            case '3':
                                $rank_value = 'xxxx';
                                break;
                            case '4':
                                $rank_value = 'xxxxx';
                                break;
                            default:
                                $rank_value = $rank['feedback'];
                                break;
                        }
                        $objPHPExcel->getActiveSheet()->setCellValue($alphas[$f].$row, $rank_value);
                        $f++;
                    }
                    $row++;
                }
                
                $objPHPExcel->getActiveSheet()->getStyle('A1:'.$alphas[$col-1].($row-1))->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->getStyle('A1:'.$alphas[$col-1].($row-1))->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $style3 = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                    'font'  => array(
                        'name'  => 'Calibri'
                    )
                );

                $objPHPExcel->getActiveSheet()->getStyle('A1:'.$alphas[$col-1].($row-1))->applyFromArray($style3);
//                $objPHPExcel->getActiveSheet()->getStyle('A1:'.$alphas[$col-1].($row-1))->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('A1:'.$alphas[$col-1].($row-1))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            } else {
                for ($i=1; $i <= 10; $i++) {
                    $objPHPExcel->getActiveSheet()->setCellValue($alphas[$i].'1', 'Feedback '.$i);
                    $objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$i])->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:'.$alphas[$i].'1')->getFont()->setBold(true);
                }
                $objPHPExcel->getActiveSheet()->getStyle('A1:'.$alphas[$i].'1')->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->getStyle('A1:'.$alphas[$i-1].'1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $style4 = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                    'font'  => array(
                        'name'  => 'Calibri'
                    )
                );

                $objPHPExcel->getActiveSheet()->getStyle('A1:'.$alphas[$i].'1')->applyFromArray($style4);
            }
            
            // Set worksheet title
            $objPHPExcel->getActiveSheet()->setTitle('Course Feedback');
            
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(-1);
            $objPHPExcel->setActiveSheetIndex(0);

        //save the file to the server (Excel2007)
        $config = new JConfig();
        if (!is_dir($config->dataroot . '/temp')) {
            mkdir($config->dataroot . '/temp', 0775);
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($config->dataroot.'/temp/' . $fileName . '.xlsx');
        
        return $fileName;
        } else {
            return false;
        }
    }
    
    public function dowloadUserReport($userid, $date) {
        require_once("./courses/lib/phpexcel/PHPExcel/IOFactory.php");
        require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');

        $groupsModel = CFactory::getModel('groups');

        $user = CFactory::getUser($userid);

        //set the desired name of the excel file
        $fileName = 'User_Report_'.trim($user->name).'_'.date('Ymd', time());

        $userreport = JoomdleHelperContent::call_method ('get_user_report', $user->username);   
        $subcircleno = $groupsModel->getCirclesCount($userid, 0, 'owner', true,true);
        $subcircles = $groupsModel->getCircles($userid, false, 0, 0, 0, 'owner',true);
        
        $data = $userreport['data'];
            
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Me")->setLastModifiedBy("Me")->setTitle("My Excel Sheet")->setSubject("My Excel Sheet")->setDescription("Excel Sheet")->setKeywords("Excel Sheet")->setCategory("Me");

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Add column headers
        $objPHPExcel->getActiveSheet()
                                    ->setCellValue('A1', 'User name:')
                                    ->setCellValue('B1', 'Date joined:')
                                    ->setCellValue('C1', 'Last login:')
                                    ->setCellValue('D1', 'No. of circles (owned):')
                                    ->setCellValue('E1', 'Name of circles (owned):')
                                    ->setCellValue('F1', 'No. of courses created:')
                                    ->setCellValue('G1', 'Name of course created:')
                                    ->setCellValue('H1', 'Circle name if course has been published:')
                                ;
            $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);

            $courseinfo = $data['courses_created'];
            $objPHPExcel->getActiveSheet()->setCellValue('A2', $user->getDisplayName());
            $objPHPExcel->getActiveSheet()->setCellValue('B2', date('m/d/y', $date));
            $objPHPExcel->getActiveSheet()->setCellValue('C2', $user->lastvisitDate!='0000-00-00 00:00:00' ? date('m/d/y', strtotime($user->lastvisitDate)) : '');
            $objPHPExcel->getActiveSheet()->setCellValue('D2', $subcircleno);
            
            if ($subcircles) {
                $i = 2;
                foreach ($subcircles as $circle) {
                    if ($circle->isBLN == 0) {
                        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $circle->name);
                        $i++;
        }
                }
            }

            $j = 2;
            if (count($courseinfo) > 0) {
                $objPHPExcel->getActiveSheet()->setCellValue('F2', count($courseinfo));
                
                foreach ($courseinfo as $course) {
                    $objPHPExcel->getActiveSheet()->setCellValue('G'.$j, $course['fullname']);
                    
                    $crpublish = $groupsModel->getShareGroups($course['remoteid']);
                    if ($crpublish) {
                        $r = $j; 
                        $d = 0;
                        foreach ($crpublish as $cr) {
                            $group = JTable::getInstance('Group', 'CTable');
                            $group->load($cr->groupid);
                            
                            $objPHPExcel->getActiveSheet()->setCellValue('H'.$r, $group->name);
                            $r++;
                            $d++;
                        }
                    }
                    $j = $j+$d;
                }
            }
            $objPHPExcel->getActiveSheet()->getStyle('A1:H'.max($i, $j, $r))->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:H'.max($i, $j, $r))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
                'font'  => array(
                    'name'  => 'Calibri'
                )
            );

            $objPHPExcel->getActiveSheet()->getStyle('A1:H'.max($i, $j, $r))->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->getStyle('A2:H'.max($i, $j, $r))->getFont()->setSize(11);

            //Put each record in a new cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(9);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(9);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
            
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);

        // Set worksheet title
            $objPHPExcel->getActiveSheet()->setTitle('Sheet1');
        
        $objPHPExcel->createSheet();
        
            // ======================== Second sheet ===========================
        $objPHPExcel->setActiveSheetIndex(1);

            $coursecompleted = $data['courses_completed'];  
            $nocourseenrolled = $data['no_course_enrolled'];
            $scmember = $groupsModel->getCirclesCount($userid, 0, 'member', true);
            $mycircles= $groupsModel->getCircles($userid, false, 0, 0, 0, 'member');

        // Add column headers
        $objPHPExcel->getActiveSheet()
                                    ->setCellValue('A1', 'No. of circles (member):')
                                    ->setCellValue('B1', 'Name of circles (member):')
                                    ->setCellValue('C1', 'No. of courses enrolled:')
                                    ->setCellValue('D1', 'No. of courses completed:')
                                    ->setCellValue('E1', 'Name of course completed:')
                                    ->setCellValue('F1', 'Circle name course is published in:')
                                    ->setCellValue('G1', 'Overall grade of each course completed:')
                                ;
            $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setSize(12);

            $objPHPExcel->getActiveSheet()->setCellValue('A2', $scmember);
            $c = 2;
            if ($mycircles) {
                foreach ($mycircles as $mc) {
                    if ($mc->isBLN == 0) {
                        $objPHPExcel->getActiveSheet()->setCellValue('B'.$c, $mc->name);
                        $c++;
                    }
                }
            }
            $objPHPExcel->getActiveSheet()->setCellValue('C2', $nocourseenrolled);
            $objPHPExcel->getActiveSheet()->setCellValue('D2', count($coursecompleted));

            $cc = 2;
            if (count($coursecompleted) > 0) {
                foreach ($coursecompleted as $curso) {
                    $objPHPExcel->getActiveSheet()->setCellValue('E'.$cc, $curso['fullname']);
                    $objPHPExcel->getActiveSheet()->setCellValue('G'.$cc, $curso['overall_grade']);
                    
                    $crpublish2 = $groupsModel->getShareGroups($curso['remoteid']);
                    if ($crpublish2) {
                        $row = $cc; 
                        $d = 0;
                        foreach ($crpublish2 as $cr) {
                            $group = JTable::getInstance('Group', 'CTable');
                            $group->load($cr->groupid);
                            
                            $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $group->name);
                            $row++;
                            $d++;
                        }
                    }
                    $cc = $cc+$d;
                }
            }
            $objPHPExcel->getActiveSheet()->getStyle('A1:'.'G'.max($cc, $row, $d, $c))->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $style2 = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
                'font'  => array(
                    'name'  => 'Calibri'
                )
            );

            $objPHPExcel->getActiveSheet()->getStyle('A1:'.'G'.max($cc, $row, $d, $c))->applyFromArray($style2);
            $objPHPExcel->getActiveSheet()->getStyle('A1:G'.max($cc, $row, $d, $c))->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:G'.max($cc, $row, $d, $c))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(9);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(17);
            
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);

            // Set worksheet title
            $objPHPExcel->getActiveSheet()->setTitle('Sheet2');
        
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(-1);
            $objPHPExcel->setActiveSheetIndex(0);

        //save the file to the server (Excel2007)
        $config = new JConfig();
        if (!is_dir($config->dataroot . '/temp')) {
            mkdir($config->dataroot . '/temp', 0775);
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($config->dataroot.'/temp/' . $fileName . '.xlsx');
        
        return $fileName;

    }
    
    public function dowloadCircleReport($circleid) {
        require_once("./courses/lib/phpexcel/PHPExcel/IOFactory.php");
        require_once(JPATH_BASE . '/components/com_community/libraries/core.php');

        $groupsModel = CFactory::getModel('groups');

        $group = JTable::getInstance('Group', 'CTable');
        $group->load($circleid);
        
        //set the desired name of the excel file
        $fileName = 'Circle_Report_'.trim($group->name).'_'.date('Ymd', time());

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Me")->setLastModifiedBy("Me")->setTitle("My Excel Sheet")->setSubject("My Excel Sheet")->setDescription("Excel Sheet")->setKeywords("Excel Sheet")->setCategory("Me");

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Add column headers
        $objPHPExcel->getActiveSheet()
                                ->setCellValue('A1', 'Circle Name:')
                                ->setCellValue('B1', 'Circle Owner:')
                                ->setCellValue('C1', 'Circle Manager: (if any)')
                                ->setCellValue('D1', 'Circle Creation Date:')
                                ->setCellValue('E1', 'About Circle:')
                                ->setCellValue('F1', 'Keywords:')
                                ->setCellValue('G1', 'Total Members:')
                                ->setCellValue('H1', 'Total Courses in circle:')
                                ->setCellValue('I1', 'Total No. of Chats:')
                                ;
        $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true);

        $memberCircles = $groupsModel->getListMembers($circleid, 0, true, true);
        
        $ownercircle = CFactory::getUser($group->ownerid);
        $objPHPExcel->getActiveSheet()->setCellValue('A2', $group->name);
        $objPHPExcel->getActiveSheet()->setCellValue('B2', $ownercircle->getDisplayName());
        $row1 = 2;

        if ($memberCircles) {
            foreach ($memberCircles as $member) {
                if ($member->permissions == 1 && $member->id != $group->ownerid) {
                    $user = CFactory::getUser($member->id);
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$row1, $user->getDisplayName());
                    $row1++;
                } 
            }
        }
        $objPHPExcel->getActiveSheet()->setCellValue('D2', date('m/d/y', strtotime($group->created)));
        $objPHPExcel->getActiveSheet()->setCellValue('E2', $group->description);
        $keywords = explode(',', $group->keyword);
        if (count($keywords) > 0) {
            $row2 = 2;
            foreach ($keywords as $kw) {
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$row2, $kw);
                $row2++;
            }
        }
        
        $courseInCircles = $groupsModel->getPublishCoursesToSocial($circleid);

        if ($courseInCircles) {
            $courses = array();
            foreach ($courseInCircles as $course) {
                $courses[] = $course->courseid;
            }
        }

        $coursesinfo = JoomdleHelperContent::call_method ('get_courses_byids', $courses ? implode(',', $courses) : '');
        
        $modelDiscussions = CFactory::getModel('discussions');
        $discussions = $modelDiscussions->getDiscussionTopics($group->id, 0, 0, DISCUSSION_ORDER_BYLASTACTIVITY);
        
        $objPHPExcel->getActiveSheet()->setCellValue('G2', count($memberCircles));
        $objPHPExcel->getActiveSheet()->setCellValue('H2', count($coursesinfo['data']));
        $objPHPExcel->getActiveSheet()->setCellValue('I2', count($discussions));
        
        $objPHPExcel->getActiveSheet()->getStyle('A1:I'.max($row1, $row2))->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:I'.max($row1, $row2))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font'  => array(
                'name'  => 'Calibri'
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle('A1:I'.max($row1, $row2))->applyFromArray($style);
        $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setSize(12);
        $objPHPExcel->getActiveSheet()->getStyle('A2:I'.max($row1, $row2))->getFont()->setSize(11);

        //Put each record in a new cell
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(23);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(21);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(21);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);

        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);

        // Set worksheet title
        $objPHPExcel->getActiveSheet()->setTitle('Sheet1');

        $objPHPExcel->createSheet();

        // ======================== Second sheet ===========================
        $objPHPExcel->setActiveSheetIndex(1);

        // Add column headers
        $objPHPExcel->getActiveSheet()
                                ->setCellValue('A1', 'Member Names:')
                                ->setCellValue('B1', 'Course Names:')
                                ->setCellValue('C1', 'Chat Names:')
                                ;
        $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setSize(12);

        $row3 = 2;
        if ($memberCircles) {
            foreach ($memberCircles as $mem) {
                $memberUser = CFactory::getUser($mem->id);
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$row3, $memberUser->getDisplayName());
                $row3++;
            }
        }

        if ($coursesinfo['status']) {
            $row4 = 2;
            foreach ($coursesinfo['data'] as $co) {
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$row4, $co['fullname']);
                $row4++;
            }
        }
        
        if ($discussions) {
            $row5 = 2;
            foreach ($discussions as $dis) {
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$row5, $dis->title);
                $row5++;
            }
        }
        
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.'C'.max($row3, $row4, $row5))->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $style2 = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font'  => array(
                'name'  => 'Calibri'
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle('A1:'.'C'.max($row3, $row4, $row5))->applyFromArray($style2);
        $objPHPExcel->getActiveSheet()->getStyle('A1:C'.max($row3, $row4, $row5))->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:C'.max($row3, $row4, $row5))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);

        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);

        // Set worksheet title
        $objPHPExcel->getActiveSheet()->setTitle('Sheet2');
        
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(-1);
        $objPHPExcel->setActiveSheetIndex(0);

        //save the file to the server (Excel2007)
        $config = new JConfig();
        if (!is_dir($config->dataroot . '/temp')) {
            mkdir($config->dataroot . '/temp', 0775);
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($config->dataroot.'/temp/' . $fileName . '.xlsx');
        
        return $fileName;
    }
}

