<?php

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');
jimport('cms.helper.lgt');
require_once(JPATH_PLATFORM .'/cms/helper/lgt.php');

class SiteadminblnModelAccount extends JModelList {

    var $_data = null;
    var $_profile;
    var $_pagination;
    var $_total;
    var $permissions;
    var $isBLNOwner;
    var $isBLNAdmin;
    var $blnInfo;

    public function __construct() {
        parent::__construct();

        $this->grantPermissions();
    }

    public function grantPermissions() {
        $my = JFactory::getUser();
        $this->blnInfo = $this->getInfobln();
        $this->isBLNOwner = $my->id == $this->blnInfo->bln_owner ? true : false;
        $this->isBLNAdmin = $my->id == $this->blnInfo->bln_admin ? true : false;
        $permissions = [];
        $permissions['assigningBLNOwner'] =  $this->isBLNOwner ? true : false;
        $permissions['assigningBLNAdmin'] = ($this->isBLNOwner || $this->isBLNAdmin) ? true : false;
        $permissions['managingLoginPage'] = ($this->isBLNOwner || $this->isBLNAdmin) ? true : false;
        $permissions['updateCompanyInfo'] = ($this->isBLNOwner || $this->isBLNAdmin) ? true : false;
        $this->permissions = $permissions;

        return $permissions;
    }

    public function hasPermission($permission) {
        $permissions = empty($this->permissions) ? $this->grantPermissions() : $this->permissions;

        if (array_key_exists($permission, $permissions)) {
            return $permissions[$permission];
        } else return false;
    }

    public function getAlluser($includeAdmin = false,$permission = false, $limitstart =  false, $limit = false, $suser = '',$page = '') {
        $db = JFactory::getDBO();
        $my = JFactory::getUser();
        $ownerid = $this->getBLNCircleOwner();
        $adminbln = $this->getBLNCircleAdmin();
        $query = 'SELECT' . $db->quoteName('id')
            . 'FROM ' . $db->quoteName('#__community_groups')
            . ' WHERE ' . $db->quoteName('isBLN') . ' = 1';
        $db->setQuery($query);
        $groupid = $db->loadResult();
//        $adminbln = $this->getBLNCircleAdmin();

        $extraSQL = '';
        if($permission){
            if($permission == 1)
            $condition .= ' AND c.'.$db->quoteName('permission') .' = '.$db->Quote(0); 
            else  $condition .= ' AND c.'.$db->quoteName('permission') .' = '.$db->Quote(1); 
        }
        if (!$includeAdmin) {
            $extraSQL .= ' AND a.'.$db->quoteName('permissions') .' = '.$db->Quote(0);
//            $extraSQL .= ' AND a.'.$db->quoteName('memberid').' != '.$db->quote($adminbln);
        }
        //UPDATE by Minh- list user in manage user page 
        if($page == 'ow') $check .= ' AND b.'.$db->quoteName('id') .' != '.$ownerid.' AND b.'.$db->quoteName('id') .' != '.$adminbln;
        else  if($page == 'ad'){
            $check .= ' AND b.'.$db->quoteName('id') .' != '.$ownerid.' AND b.'.$db->quoteName('id') .' != '.$adminbln; 
        }
        // Update by Luyen - Search user
        if ($suser != '') {
            // update search width letter %,'
            $suser = str_replace("'", "\'", $suser);
            $suser = str_replace("%", "\%", $suser);
            $text_search = '\'%' . $suser . '%\'';
            $wheres2 = array();
            $wheres2[] = ' b.name LIKE ' . $text_search;
            $wheres2[] = ' b.username LIKE ' . $text_search;
            // $wheres2[] = ' b.email LIKE ' . $text_search;
            
            $wheres = implode(' OR ', $wheres2);
            $extraSQL .= ' AND ('.$wheres.')';
        }
        $sqllimit = '';
        if ($limit) {
            $sqllimit = " LIMIT $limitstart, $limit ";
        }

        $query = 'SELECT b.id, b.name, c.avatar, a.date,c.permission FROM ' . $db->quoteName('#__community_groups_members').' a INNER JOIN '
            . $db->quoteName('#__users') . ' b INNER JOIN ' . $db->quoteName('#__community_users') . ' c ON b.' . $db->quoteName('id') . ' = c.' . $db->quoteName('userid')
            .' WHERE a.'.$db->quoteName('memberid').' = b.'.$db->quoteName('id')
            .' AND a.'.$db->quoteName('groupid').' = '.$db->Quote($groupid)
            .' AND a.'.$db->quoteName('permissions').' !=' . $db->quote( -1 ) . ' '
            .' AND a.'.$db->quoteName('memberid').' != 0 '
            .' AND a.'.$db->quoteName('approved').' = '.$db->Quote(1)
            .' AND b.'.$db->quoteName('block').' = '.$db->Quote(0)
            . $extraSQL.$condition.$check
            . ' ' . $sqllimit;   
        $db->setQuery($query);
        $db->Query();
        $result = $db->loadObjectList('id');
        return $result;
    }

    public function countAllUserBLN () {
        $db = JFactory::getDBO();
        
        $query = 'SELECT' . $db->quoteName('id')
            . 'FROM ' . $db->quoteName('#__community_groups')
            . ' WHERE ' . $db->quoteName('isBLN') . ' = 1';
        $db->setQuery($query);
        $groupid = $db->loadResult();

        $query = 'SELECT COUNT(*) FROM ' . $db->quoteName('#__community_groups_members').' a '
                . 'INNER JOIN ' . $db->quoteName('#__users') . ' b '
                .' ON a.'.$db->quoteName('memberid').' = b.'.$db->quoteName('id')
                .' WHERE a.'.$db->quoteName('groupid').' = '.$db->Quote($groupid)
                .' AND a.'.$db->quoteName('permissions').' !=' . $db->quote( -1 ) . ' '
                .' AND a.'.$db->quoteName('memberid').' != 0 '
                .' AND a.'.$db->quoteName('approved').' = '.$db->Quote(1)
                .' AND b.'.$db->quoteName('block').' = '.$db->Quote(0);   
        $db->setQuery($query);
        $result = $db->loadResult();
        return $result;
    }
    
    public function updateSubscription($total_use, $total_empty) {
        $db = $this->getDBO();
        
        $query = 'UPDATE ' . $db->quoteName('#__subscription') . ' SET '
                . $db->quoteName('total_use') . ' = ' . $db->Quote($total_use)
               .', '. $db->quoteName('total_empty') . ' = ' . $db->Quote($total_empty)
                . ' WHERE ' . $db->quoteName('id') . ' = ' . $db->Quote(1);        
        $db->setQuery($query);
        $db->query();
        return true;
    }

    public function searchPeople($search, $avatarOnly = '', $friendId = 0, $page, $limit, $includeAdmin = false) {
        $db = $this->getDBO();
        $config = JFactory::getConfig();
        $filter = array();
        $data = array();
        $isEmail = false;
        $my = JFactory::getUser();

        $ownerid = $this->getBLNCircleOwner();
        $adminbln = $this->getBLNCircleAdmin();

        $extraSQL = '';
        if (!$includeAdmin) {
            $extraSQL .= ' AND a.'.$db->quoteName('permissions') .' = '.$db->Quote(0);
//            $extraSQL .= ' AND a.'.$db->quoteName('memberid').' != '.$db->quote($adminbln);
        }

        //select only non empty field
        $query = 'SELECT' . $db->quoteName('id') . 'FROM ' . $db->quoteName('#__community_groups') . ' WHERE ' . $db->quoteName('isBLN') . ' = 1';
        $db->setQuery($query);
        $groupid = $db->loadResult();
        
        $search = str_replace("'", "\'", $search);
        $search = str_replace("%", "\%", $search);
        $query = 'SELECT b.id, b.name, c.avatar FROM ' . $db->quoteName('#__community_groups_members')
            . ' a INNER JOIN ' . $db->quoteName('#__users')
            . ' b INNER JOIN ' . $db->quoteName('#__community_users') . ' c ON b.' . $db->quoteName('id') . ' = c.' . $db->quoteName('userid')
            . ' WHERE (b.' . $db->quoteName('name') . ' like \'%' . $search . '%\''
            . ' OR b.' . $db->quoteName('username') . ' like \'%' . $search . '%\')'
            . ' AND a.'.$db->quoteName('memberid').' = b.'.$db->quoteName('id')
            . ' AND a.'.$db->quoteName('groupid').'= '.$db->Quote($groupid)
            . ' AND a.'.$db->quoteName('approved').' = '.$db->Quote(1)
            . ' AND b.'.$db->quoteName('block').' = '.$db->Quote(0)
            . $extraSQL;

        $db->setQuery($query);
        $result = $db->loadObjectList('id');
        return $result;
    }

    public function assignBLNCircleOwner($userid) {
        $db = $this->getDBO();
        $time = time();
        $my = JFactory::getUser();
        $assignedUser = JFactory::getUser($userid);

        $blnInfo = $this->getInfobln();
        $oldBLNOwnerid = $blnInfo->bln_owner;
        $blnAdminid = $blnInfo->bln_admin;

        if ($userid == $blnAdminid || $userid == $my->id) return false;

        $blnCircleId = $blnInfo->bln_circleid;

        $query = 'UPDATE ' . $db->quoteName('#__community_groups_members')
            . ' SET ' . $db->quoteName('permissions') . ' = 0 '
            . ',' . $db->quoteName('date') . ' = ' . $db->Quote($time)
            . ' WHERE ' . $db->quoteName('groupid') . ' = ' . $blnCircleId
            . ' AND ' . $db->quoteName('memberid') . ' = ' . $oldBLNOwnerid;
        $db->setQuery($query);
        $db->query();

        $query = 'UPDATE ' . $db->quoteName('#__community_groups_members')
            . ' SET ' . $db->quoteName('permissions') . ' = 1 '
            . ',' . $db->quoteName('date') . ' = ' . $db->Quote($time)
            . ' WHERE ' . $db->quoteName('groupid') . ' = ' . $blnCircleId
            . ' AND ' . $db->quoteName('memberid') . ' = ' . $assignedUser->id;
        $db->setQuery($query);
        $db->query();

        $query = 'UPDATE ' . $db->quoteName('#__community_groups')
            . ' SET ' . $db->quoteName('ownerid') . ' = ' . $assignedUser->id
            . ' WHERE ' . $db->quoteName('id') . ' = ' . $blnCircleId;
        $db->setQuery($query);
        $db->query();

        $query = 'UPDATE ' . $db->quoteName('#__bln_info') . 'SET '
            . $db->quoteName('bln_owner') . ' = ' . $assignedUser->id
            . ' WHERE ' . $db->quoteName('id') . ' = ' . $blnInfo->id;
        $db->setQuery($query);

        if ($db->query()) {
            $ob = new stdClass();
            $ob->user = $assignedUser->username;
            $ob->roleid = 1; // roleid for manager
            $ob->status = 1;
            $ob->changeOwner = 1;
            $datamanager[] = $ob;

            $ob2 = new stdClass();
            $ob2->user = JFactory::getUser($oldBLNOwnerid)->username;
            $ob2->roleid = 1; // roleid for manager
            $ob2->status = 0;
            $datamanager[] = $ob;
            require_once(JPATH_ADMINISTRATOR . '/components/com_joomdle/helpers/content.php');
            $response = JoomdleHelperContent::call_method('set_user_category_role', $my->username, (int) $blnInfo->bln_moodlecategoryid, json_encode($datamanager));

            return true;
        } else
            return false;
    }

    public function assignBLNCircleAdmin($userid) {
        $db = $this->getDBO();
        $time = time();
        $my = JFactory::getUser();
        $assignedUser = JFactory::getUser($userid);

        $blnInfo = $this->getInfobln();
        $blnOwnerid = $blnInfo->bln_owner;
        $oldBLNAdminid = $blnInfo->bln_admin;

        if ($userid == $blnOwnerid || $userid == $my->id) return false;

        $blnCircleId = $blnInfo->bln_circleid;

//        $query = 'UPDATE ' . $db->quoteName('#__community_groups_members')
//            . ' SET ' . $db->quoteName('permissions') . ' = 0 '
//            . ',' . $db->quoteName('date') . ' = ' . $db->Quote($time)
//            . ' WHERE ' . $db->quoteName('groupid') . ' = ' . $blnCircleId
//            . ' AND ' . $db->quoteName('memberid') . ' = ' . $oldBLNAdminid;
//        $db->setQuery($query);
//        $db->query();
//
//        $query = 'UPDATE ' . $db->quoteName('#__community_groups_members')
//            . ' SET ' . $db->quoteName('permissions') . ' = 1 '
//            . ',' . $db->quoteName('date') . ' = ' . $db->Quote($time)
//            . ' WHERE ' . $db->quoteName('groupid') . ' = ' . $blnCircleId
//            . ' AND ' . $db->quoteName('memberid') . ' = ' . $assignedUser->id;
//        $db->setQuery($query);
//        $db->query();

        $query = 'UPDATE ' . $db->quoteName('#__bln_info') . 'SET '
            . $db->quoteName('bln_admin') . ' = ' . $assignedUser->id
            . ' WHERE ' . $db->quoteName('id') . ' = ' . $blnInfo->id;
        $db->setQuery($query);

        if ($db->query()) {
            $ob = new stdClass();
            $ob->user = $assignedUser->username;
            $ob->roleid = 1; // roleid for manager
            $ob->status = 1;
            $datamanager[] = $ob;

            $ob2 = new stdClass();
            $ob2->user = JFactory::getUser($oldBLNAdminid)->username;
            $ob2->roleid = 1; // roleid for manager
            $ob2->status = 0;
            $datamanager[] = $ob;
            require_once(JPATH_ADMINISTRATOR . '/components/com_joomdle/helpers/content.php');
            $response = JoomdleHelperContent::call_method('set_user_category_role', $my->username, (int) $blnInfo->bln_moodlecategoryid, json_encode($datamanager));

            return true;
        } else
            return false;
    }

    public function getInfobln() {
        $db = $this->getDBO();
        $query = 'SELECT a.* FROM ' . $db->quoteName('#__bln_info') . ' a INNER JOIN ' . $db->quoteName('#__community_groups') . ' b ON a.bln_circleid = b.id ' .
        ' WHERE b.isBLN = 1';
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public function getBLNCircleOwner() {
        $db = $this->getDBO();
        $query = 'SELECT ' . $db->quoteName('ownerid') . 'FROM ' . $db->quoteName('#__community_groups') . ' WHERE ' . $db->quoteName('isBLN') . ' = 1';
        $db->setQuery($query);
        $ownerid = $db->loadResult();
        return $ownerid;
    }

    public function getBLNCircleAdmin() {
        $db = $this->getDBO();
        $query = 'SELECT ' . $db->quoteName('bln_admin') . 'FROM ' . $db->quoteName('#__bln_info');
        $db->setQuery($query);
        $adminBln = $db->loadResult();
        return $adminBln;
    }

    public function saveBln($info) {
        $db = $this->getDBO();
        $config = JFactory::getConfig();
        $my = JFactory::getUser();
        $circleid = $this->getBLNCircleOwner();

        $query = 'SELECT ' . $db->quoteName('id') . ' FROM ' . $db->quoteName('#__bln_info');

        $db->setQuery($query);
        $isBln = $db->loadResult();

        $bln = new stdClass();
        $bln->organisation_name = $info['nameOR'];
        $bln->address = $info['address'];
        $bln->postal_code = $info['postal'];
        $bln->contact = $info['contact'];

        if ($isBln) {
            $bln->id = $isBln;
            $bln->bln_admin = $result['bln_admin'];
            return $db->updateObject('#__bln_info', $bln, 'id');
        } else {
            return $db->insertObject('#__bln_info', $bln);
        }
        return $isBln;
    }

    public function deleteUserBLN($userid) {
        $db = $this->getDBO();
        $user = new JUser;
        $user->load($userid);

        $userbln = JFactory::getUser($userid);

        $username = $userbln->username;

        // var_dump(JHelperLGT::deleteUser('12343@gmail.com')); die();
        // Load the users plugin group.
        JPluginHelper::importPlugin('user');
        if (!$user->delete()) {
            return false;
        } else {
            // JHelperLGT::deleteUser($username);

            
            /*
             * Updated for app Push notification for app
             * Updated by @Kydonvn
             * Updated date 30 Nov 2018
             * 
             */
            if($userbln->device_id) {
                JPluginHelper::importPlugin('groupsapi');
                $dispatcher = JDispatcher::getInstance();
                $message = array(
                    'mtitle' => 'You have been removed from your organization\'s learning network.\n Log in to the Parenthexis app instead to continue your own learning journey!',
                    'mdesc' => 'Log in to the Parenthexis app instead to continue your own learning journey!'
                );
                $userto_id = $userbln->id;
                $dispatcher->trigger('pushNotification', array ($message, $userto_id, $userto_id, 0, 'remove_user', $userbln->device_id));
            }
            
             // update license
            $query = 'UPDATE '.$db->quoteName('#__subscription') . ' SET '
                . $db->quoteName('total_use') . ' = ' . $db->quoteName('total_use').'-1'
               .', '. $db->quoteName('total_empty') . ' = ' . $db->quoteName('total_empty').'+1'
                . ' WHERE ' . $db->quoteName('id') . ' = ' . $db->Quote(1);
            $db->setQuery($query);
            $db->query();

            // Delete session
            $query = 'DELETE FROM '.$db->quoteName('#__session').' WHERE '.$db->quoteName('userid') .' = ' .$db->quote($userbln->id);
            $db->setQuery($query);
            $db->query();
             
            return true;
        }

        // if ($user) {
        //     $id = $user->id;
           
        //     // Delete user
        //     $query = 'DELETE FROM ' .$db->quoteName('#__users').' WHERE '.$db->quoteName('id') .' = '. $id;
        //     $db->setQuery($query);
        //     $db->query();

        //     // Delete user group
        //     $query = 'DELETE FROM ' .$db->quoteName('#__user_usergroup_map') .' WHERE '. $db->quoteName('user_id') . ' = '. $id;
        //     $db->setQuery($query);
        //     $db->query();

        //     // Delete community user
        //     $query = 'DELETE FROM ' .$db->quoteName('#__community_users'). ' WHERE ' .$db->quoteName('userid') . ' = ' .$id;
        //     $db->setQuery($query);
        //     $db->query();

        //     // update license
        //     $query = 'UPDATE '.$db->quoteName('#__subscription') . ' SET '
        //         . $db->quoteName('total_use') . ' = ' . $db->quoteName('total_use').'-1'
        //        .', '. $db->quoteName('total_empty') . ' = ' . $db->quoteName('total_empty').'+1'
        //         . ' WHERE ' . $db->quoteName('id') . ' = ' . $db->Quote(1);
        //     $db->setQuery($query);
        //     $db->query();
        //     // delete session 
        //     $query = 'DELETE FROM ' .$db->quoteName('#__session'). ' WHERE ' .$db->quoteName('userid') . ' = ' .$id;
        //     $db->setQuery($query);
        //     $db->query();

        //     return true;
        // }
        
        return false;
    }

    public function checkSiteAdminBLn($userid) {
        $db = $this->getDBO();
        $query = 'SELECT * FROM ' . $db->quoteName('#__bln_info')
            . ' WHERE ' . $db->quoteName('bln_owner') . ' = ' . $db->Quote($userid)
            . ' OR ' . $db->quoteName('bln_admin') . ' = ' . $db->Quote($userid);
        $db->setQuery($query);
        $adminBln = $db->loadResult();
        return $adminBln;
    }

    public function getBLNMoodleCategoryId() {
        $db = $this->getDBO();

        $query = 'SELECT ' . $db->quoteName('bln_moodlecategoryid') . ' FROM ' . $db->quoteName('#__bln_info')
            . ' WHERE ' . $db->quoteName('id') . ' = ' . $db->Quote('1');
        $db->setQuery($query);
        $blnmoodlecategoryid = $db->loadResult();
        return $blnmoodlecategoryid;
    }
    public function getInfosubscription() {
        $db = $this->getDBO();
        $config = JFactory::getConfig();

        $query = 'SELECT * FROM ' . $db->quoteName('#__subscription');
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
}
    public function addOrder($data) {
        $db = $this->getDBO();
        $time = time();
        $my = JFactory::getUser();
        $bln = $this->getInfobln();
       
        $sub = $this->getInfosubscription();
        $total_empty = $sub->total_empty + $data['total'];
         $total_purchased = $sub->total_purchased + $data['total'];
        
        if($data['type'] == "monthly"){
            $end= mktime(0, 0, 0, date("m")+1, date("d"), date("y"));
            $date_end = date('Y-m-d',$end);
        }
        else{
            $end= mktime(0, 0, 0, date("m"), date("d"), date("y")+1);
            $date_end = date('Y-m-d',$end);
        }
        $date_create = mktime(0, 0, 0, date("m"), date("d")+1, date("y"));
        //update table subscription
        $query = 'UPDATE ' . $db->quoteName('#__subscription') . 'SET '
                . $db->quoteName('total_purchased') . ' = ' . $db->Quote($total_purchased)
               .', '. $db->quoteName('total_empty') . ' = ' . $db->Quote($total_empty)
                . ' WHERE ' . $db->quoteName('id') . ' = ' . $db->Quote(1);
        $db->setQuery($query);
        $db->query();
        //save data in table subscription increase
        $datasub = new stdClass();
        $datasub->id_sub = $sub->id;
        $datasub->new_total = $data['total'];
        $datasub->type_subcription = $data['type'];
        $datasub->total_old = $sub->total_purchased;
        $datasub->date_start = date("Y-m-d");
        $datasub->date_end = $date_end;
        $datasub->date_created = date('Y-m-d',$date_create);
        $datasub->total_new = $total_purchased;
        $datasub->use_created = $my->id;    
        
        $db->insertObject('#__subscription_increase', $datasub);
        
        //create id subcreation increase new
        $query = 'SELECT MAX(' . $db->quoteName('id') . ') FROM' . $db->quoteName('#__subscription_increase');
        $db->setQuery($query);
        $increaseId= $db->loadResult();
        
        require_once(JPATH_SITE . '/administrator/components/com_hikashop/helpers/helper.php');
        $table = new stdClass();
        
         //create id order new 
        $query = 'SELECT MAX(' . $db->quoteName('id') . ') FROM' . $db->quoteName('#__subscription_order');
        
        $db->setQuery($query);
        $orderId_old= $db->loadResult();
        $tableid = $orderId_old + 1;
       
        $table->id_sub_increase = (int)$increaseId;
        $data['order_id'] = $orderId_old + 1;
        $type = $data['type'] == "monthly" ? 1 : 12;
        $price = $data['total'] * $type * 2000;
        $table->total_subscription = $data['total'];
        $table->type_order = $data['type'];
        $table->price_order = $data['price'];
        $table->number_order = hikashop_encode($data['order_id']);
        $table->order_created = $time;
        $table->payment_method = "PayPal";
        $db->insertObject('#__subscription_order', $table);
        
        
        return $tableid;
    }

    public function orderInfo($orderId) {
        $db = $this->getDBO();
        $query = 'SELECT * FROM '.$db->quoteName('#__subscription_order').
                ' WHERE '.$db->quoteName('id').' = '.$db->quote($orderId);
        $db->setQuery($query);
        $orderIf = $db->loadObject();
        return $orderIf;
    }
    public function insreaseInfo() {
        $db = $this->getDBO();
         $query = 'SELECT MAX(' . $db->quoteName('id') . ') FROM' . $db->quoteName('#__subscription_increase');
        $db->setQuery($query);
        $id= $db->loadResult();
        $query = 'SELECT * FROM '.$db->quoteName('#__subscription_increase').
                ' WHERE '.$db->quoteName('id').' = '.$db->quote($id);
         $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }
    public function subIncrease($limit = false, $start = false) {
        if (!$limit || !$start) {
            $limit = 2;
            $start = 0;
        }
        $db = $this->getDBO();
        $query = 'SELECT * FROM '.$db->quoteName('#__subscription_increase')
            .'ORDER BY'.$db->quoteName('id').'DESC'
            .' LIMIT ' . $start . ',' . $limit ;
        $db->setQuery($query);
        $data = $db->loadObjectList();
        return $data;
    }
    public function countIncrease() {
        $db = $this->getDBO();
        $query = 'SELECT COUNT(*) FROM '.$db->quoteName('#__subscription_increase');
        $db->setQuery($query);
        $data = $db->loadResult();
        return $data;
    }
    public function editWriteup($writeupContent) {
        $db = $this->getDBO();
        $query = 'UPDATE ' . $db->quoteName('#__bln_info') . ' SET ' . $db->quoteName('bln_writeup') . ' = "' . $writeupContent . '"';
        $db->setQuery($query);
        $result = $db->query();

        return $result;
    }
    public function handleImages($type, $data) {
        jimport('joomla.filesystem.folder');
        jimport('joomla.filesystem.file');
        require_once(JPATH_ROOT . '/components/com_community/helpers/image.php');

        $db = $this->getDBO();
        $app = JFactory::getApplication();
        $BLNInfo = $this->blnInfo;

        $logoPath = $app->getCfg('dataroot').'/images/bln/login/logo/';
        $bannerPath = $app->getCfg('dataroot').'/images/bln/login/banner/';
        $maxFileSize = 8 * 1024 * 1024;

        switch ($type) {
            case 'logo':
                $validExts = ['png'];
                $logoUpload = $data['logoUpload'];
                $logoFileName = $data['logoFileName'];

                $e = strtolower(JFile::getExt($logoUpload['name']));
                $logoName = md5($logoUpload['name']) . '.' . $e;

                if (!empty($logoUpload['name']) && $logoName != $BLNInfo->bln_logo && $logoUpload['error'] == 0 && in_array($e, $validExts) && $logoUpload['size'] < $maxFileSize) {
                    if (!JFolder::exists($logoPath)) JFolder::create($logoPath, 0777);

                    if (JFile::exists($logoPath.$BLNInfo->bln_logo)) {
                        JFile::delete($logoPath.$BLNInfo->bln_logo);
                    }
                    if (is_writable($logoPath))
                        JFile::copy($logoUpload['tmp_name'], $logoPath.$logoName);

                    $BLNInfo->bln_logo = $logoName;
                } else {
                    if (empty($logoFileName)) {
                        if (JFile::exists($logoPath.$BLNInfo->bln_logo)) {
                            JFile::delete($logoPath.$BLNInfo->bln_logo);
                        }
                        $BLNInfo->bln_logo = '';
                    }
                }

                break;
            case 'banner':
                if (!JFolder::exists($bannerPath)) JFolder::create($bannerPath, 0777);
                $validExts = ['png', 'jpg', 'jpeg'];
                $tinyPrefix = 'tiny_';
                $colName = ['bln_first_banner', 'bln_second_banner', 'bln_third_banner', 'bln_fourth_banner'];

                $arrFileNames = [$BLNInfo->bln_first_banner, $BLNInfo->bln_second_banner, $BLNInfo->bln_third_banner, $BLNInfo->bln_fourth_banner];
                // Mảng các banner cũ từ db xuất ra

                $arrNewFileNames = []; // Mảng dùng cho việc lưu tên các file ảnh được chấp nhận làm banner mới.

                $bannerUpload = $data['bannerUpload'];
                $bannerFileNames = $data['bannerFileNames'];

                foreach ($bannerFileNames as $k => $fileName) { // Loại bỏ phần tử trống
                    if ($fileName == '') {
                        unset($bannerFileNames[$k]);
                        unset($bannerUpload[$k]);
                    }
                }
                $bannerUpload = array_values($bannerUpload);
                $bannerFileNames = array_values($bannerFileNames);

                if (count($bannerFileNames) !== count(array_unique($bannerFileNames))) { // Nếu có ảnh bị trùng lặp thì bỏ qua.
                    return false;
                }

                $ord = 0;
                foreach ($bannerUpload as $key => $banner) {
                    $ex = strtolower(JFile::getExt($banner['name']));
                    $bannerName = md5($banner['name']).'.'.$ex;

                    if ($banner['error'] != 0) { // Trường hợp: không upload ảnh mới lên
                        if ($bannerFileNames[$key] != $BLNInfo->{$colName[$ord]}) { // Tên ảnh khác banner name trong db => thay đổi thứ tự ảnh
                            if (in_array($bannerFileNames[$key], $arrFileNames)) { // Nếu thay đổi thứ tự ảnh thì filename phải nằm trong mảng các banner names.
                                $BLNInfo->{$colName[$ord]} = $bannerFileNames[$key];
                                $arrNewFileNames[] = $bannerFileNames[$key];
                                $ord++;
                            }
                        } else {
                            $arrNewFileNames[] = $bannerFileNames[$key];
                            $ord++;
                        }
                        continue;
                    }

                    if (!empty($banner['name']) && $bannerName != $BLNInfo->{$colName[$ord]}) { // Trường hợp: có upload ảnh và ảnh này khác banner cũ
                        if (in_array($ex, $validExts) && $banner['size'] < $maxFileSize) {
                            if (is_writable($bannerPath)) {
                                JFile::copy($banner['tmp_name'], $bannerPath . $bannerName);

                                list($currentWidth, $currentHeight) = getimagesize($bannerPath . $bannerName);
                                if ($currentWidth >= $currentHeight) {
                                    if ($this->testResize($currentWidth, $currentHeight, 4, 0, 2, 4)) {
                                        $newWidth = 4;
                                        $newHeight = 0;
                                    } else {
                                        $newWidth = 0;
                                        $newHeight = 2;
                                    }
                                } else {
                                    if ($this->testResize($currentWidth, $currentHeight, 0, 2, 2, 2)) {
                                        $newWidth = 0;
                                        $newHeight = 2;
                                    } else {
                                        $newWidth = 4;
                                        $newHeight = 0;
                                    }
                                }
                                //CImageHelper::createThumb($bannerPath . $bannerName, $bannerPath . 'thumb_'.$bannerName, $banner['type'], 4, 2);
                                CImageHelper::resizeProportional(
                                    $bannerPath . $bannerName,
                                    $bannerPath . $tinyPrefix.$bannerName,
                                    $banner['type'],
                                    $newWidth,
                                    $newHeight
                                );
                            }

                            $BLNInfo->{$colName[$ord]} = $bannerName;
                            $arrNewFileNames[] = $bannerName;
                            $ord++;
                        }
                    } else {
                        $arrNewFileNames[] = $bannerName;
                        $ord++;
                    }
                }

                for ($i = 0; $i < 4; $i++) { // Trường hợp: xóa banner đi.
                    if (!array_key_exists($i, $arrNewFileNames)) {
                        if (JFile::exists($bannerPath . $BLNInfo->{$colName[$i]}) && !in_array($BLNInfo->{$colName[$i]}, $arrNewFileNames)) {
                            JFile::delete($bannerPath . $BLNInfo->{$colName[$i]});
                            JFile::delete($bannerPath . $tinyPrefix.$BLNInfo->{$colName[$i]});
                        }
                        $BLNInfo->{$colName[$i]} = '';
                    }
                }

                foreach ($arrFileNames as $a) { // Xóa các ảnh cũ không sử dụng nữa
                    if (JFile::exists($bannerPath . $a) && !in_array($a, $arrNewFileNames)) {
                        JFile::delete($bannerPath . $a);
                        JFile::delete($bannerPath . $tinyPrefix.$a);
                    }
                }

                break;
        }

        $result = $db->updateObject('#__bln_info', $BLNInfo, 'id');
        return $result;
    }
    protected function testResize($orgW, $orgH, $newW, $newH, $minVal, $maxVal) {
        if ($newH == 0) {
            /* New height value */
            $newValue = round(($newW * $orgH) / $orgW);
        } elseif ($newW == 0) {
            /* New width value */
            $newValue = round(($newH * $orgW) / $orgH);
        } else {
            return false;
        }
        return ($newValue >= $minVal && $newValue <= $maxVal) ? true : false;
    }
    public function getBLNLogo() {
        $app = JFactory::getApplication();
        $logoPath = $app->getCfg('wwwrootfile').'/images/bln/login/logo/';
        $defaultLogo = JURI::root().'/images/login/imagebln/logo.png';

        return $this->blnInfo->bln_logo ? $logoPath . $this->blnInfo->bln_logo : '';
    }
    public function getBLNBanners($includeTinyBanners = false) {
        $app = JFactory::getApplication();
        $bannerPath = $app->getCfg('wwwrootfile').'/images/bln/login/banner/';
        $defaultBanner = JURI::root().'/images/login/image1.jpg';
        $defaultTinyBanner = JURI::root().'/images/login/tiny_image1.jpg';
        $return = [];
        $tinyPrefix = 'tiny_';

        if ($includeTinyBanners) {
            $return[] = $this->blnInfo->bln_first_banner ? ['banner'=>$bannerPath . $this->blnInfo->bln_first_banner, 'tinyBanner'=>$bannerPath . $tinyPrefix.$this->blnInfo->bln_first_banner] : ['banner'=>$defaultBanner, 'tinyBanner'=>$defaultTinyBanner];

            if ($this->blnInfo->bln_second_banner)
                $return[] = ['banner'=>$bannerPath . $this->blnInfo->bln_second_banner, 'tinyBanner'=>$bannerPath . $tinyPrefix.$this->blnInfo->bln_second_banner];

            if ($this->blnInfo->bln_third_banner)
                $return[] = ['banner'=>$bannerPath . $this->blnInfo->bln_third_banner, 'tinyBanner'=>$bannerPath . $tinyPrefix.$this->blnInfo->bln_third_banner];

            if ($this->blnInfo->bln_fourth_banner)
                $return[] = ['banner'=>$bannerPath . $this->blnInfo->bln_fourth_banner, 'tinyBanner'=>$bannerPath . $tinyPrefix.$this->blnInfo->bln_fourth_banner];
        } else {
            $return[] = $this->blnInfo->bln_first_banner ? $bannerPath . $this->blnInfo->bln_first_banner : $defaultBanner;

            if ($this->blnInfo->bln_second_banner)
                $return[] = $bannerPath . $this->blnInfo->bln_second_banner;

            if ($this->blnInfo->bln_third_banner)
                $return[] = $bannerPath . $this->blnInfo->bln_third_banner;

            if ($this->blnInfo->bln_fourth_banner)
                $return[] = $bannerPath . $this->blnInfo->bln_fourth_banner;
        }

        return $return;
    }
        public function checkEmailExits($email) {
        $db = $this->getDBO();
        $found = false;

        $query = 'SELECT ' . $db->quoteName('email');
        $query .= ' FROM ' . $db->quoteName('#__users');
        $query .= ' WHERE UCASE(' . $db->quoteName('email') . ') = UCASE(' . $db->Quote($email) . ')';

        $db->setQuery($query);
        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }
        $result = $db->loadObjectList();
        $found = (count($result) == 0) ? false : true;
        return $found;
    }

    public function getByEmail($email) {
        $db = $this->getDBO();
        $found = false;

        $query = 'SELECT *';
        $query .= ' FROM ' . $db->quoteName('#__users');
        $query .= ' WHERE UCASE(' . $db->quoteName('email') . ') = UCASE(' . $db->Quote($email) . ')';

        $db->setQuery($query);
        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }
        $result = $db->loadObjectList();
        
        if (count($result)) {
            return $result;
        }
        
        return false;
    }

    public function changeOwnercircle($userid) {
        require_once(JPATH_BASE.'/components/com_community/libraries/core.php');
        require_once(JPATH_ADMINISTRATOR . '/components/com_joomdle/helpers/content.php');
        $db = $this->getDBO();
        $blnInfo = $this->getInfobln();
        $blnOwnerid = $blnInfo->bln_owner;
        $blnOwner = JFactory::getUser($blnOwnerid);
        $currentCircleOwner = JFactory::getUser($userid);
        
        $query = 'SELECT id, parentid, ownerid FROM '.$db->quoteName('#__community_groups')
                .' WHERE '.$db->quoteName('ownerid').' = '.$db->quote($userid);
        $db->setQuery($query);
        $db->Query();
        $myOwnCircles = $db->loadObjectList('id');

        foreach ($myOwnCircles as $circle) {
            if (array_key_exists($circle->parentid, $myOwnCircles)) {
                $parentCircleID = $circle->parentid;
                while (array_key_exists($parentCircleID, $myOwnCircles)) {
                    $query = 'SELECT id, parentid, ownerid FROM ' . $db->quoteName('#__community_groups')
                        . ' WHERE ' . $db->quoteName('id') . ' = ' . $parentCircleID;
                    $db->setQuery($query);
                    $db->query();

                    $parentCircleID = $db->loadObject()->parentid;
                }

                $query = 'SELECT id, parentid, ownerid FROM ' . $db->quoteName('#__community_groups')
                    . ' WHERE ' . $db->quoteName('id') . ' = ' . $parentCircleID;
                $db->setQuery($query);
                $db->query();

                $newOwnerID = $db->loadObject()->ownerid;
            } else {
                $query = 'SELECT id, parentid, ownerid FROM ' . $db->quoteName('#__community_groups')
                . ' WHERE ' . $db->quoteName('id') . ' = ' . $circle->parentid;
                $db->setQuery($query);
                $db->query();

                $newOwnerID = $db->loadObject()->ownerid;
            }

            $query = 'UPDATE ' . $db->quoteName('#__community_groups')
                . ' SET '. $db->quoteName('ownerid') . ' = ' . $db->Quote($newOwnerID)
                . ' WHERE ' . $db->quoteName('id') . ' = ' . $circle->id;
            $db->setQuery($query);
            $db->query();
            
            $strSQL = 'DELETE FROM ' . $db->quoteName('#__community_groups_members') . ' '
                . 'WHERE ' . $db->quoteName('groupid') . '=' . $db->Quote($circle->id) . ' '
                . 'AND ' . $db->quoteName('memberid') . '=' . $db->Quote($newOwnerID);

            $db->setQuery( $strSQL );
            $db->query();
            
            // $sql   = 'DELETE FROM ' . $db->quoteName('#__community_groups_members') . ' '
            //     . 'WHERE ' . $db->quoteName('groupid') . '=' . $db->Quote($circle->id) . ' '
            //     . 'AND ' . $db->quoteName('memberid') . '=' . $db->Quote($currentCircleOwner->id);

            // $db->setQuery($sql);
            // $db->query();
            
            $member = JTable::getInstance('GroupMembers', 'CTable');
            $member->load(['groupId' => $circle->id, 'memberId' => $currentCircleOwner->id]);
            $member->permissions = 0;
            $member->store();

            $owner = new stdClass();
            $owner->groupid  = $circle->id;
            $owner->memberid = $newOwnerID;
            $owner->approved = 1;
            $owner->permissions = 1;
            $owner->date = time();

            $db->insertObject('#__community_groups_members', $owner);
        }

        $groupsModel = CFactory::getModel('groups');
        $courseCircles = $groupsModel->getCirclescourse($currentCircleOwner->id, 0, 0, 0, 6);
        foreach ($courseCircles as $key => $value) {
            $courseid = $groupsModel->getIsCourseGroup($value->id);
            $r = JoomdleHelperContent::call_method('course_enable_self_enrolment', (int)$courseid, 'unenable', $currentCircleOwner->username);
            $groupsModel->deleteCourseShareToGroup($courseid);    
        }
        $groupsModel->deleteCourseShareToGroup($cid);

        $response = JoomdleHelperContent::call_method('change_owner_course', $currentCircleOwner->username, $blnOwner->username);
       
        return true;
    }
    
    public function changePermission($userid,$status){
        $db = $this->getDBO();
        if($status == 0){
            $query = 'UPDATE ' . $db->quoteName('#__community_users')
                . ' SET '. $db->quoteName('permission') . ' = ' . $db->Quote(0)
                . ' WHERE ' . $db->quoteName('userid') . ' = ' . $userid;
            $db->setQuery($query);
            $db->query();
            return;
        }
       else if($status == 1){
            $query = 'UPDATE ' . $db->quoteName('#__community_users')
                . ' SET '. $db->quoteName('permission') . ' = ' . $db->Quote(1)
                . ' WHERE ' . $db->quoteName('userid') . ' = ' . $userid;
            $db->setQuery($query);
            $db->query();
            return;
        }
        else{
            $query = 'SELECT '.$db->quoteName('permission').' FROM '.$db->quoteName('#__community_users')
                     . ' WHERE ' . $db->quoteName('userid') . ' = ' . $userid;
            $db->setQuery($query);
            $data = $db->loadResult();
            return $data;
        }
    }


}