<?php

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class UserapiModelUserapi extends JModelList
{
    public $header = [];

    /*
     * set header for response
     */
    public function setHeader($header) {
        if (is_array($header)){
            foreach ($header as $v) {
                @header($v);
            }
        } else {
            @header($header);
        }
    }
    /*
     * send response message
     */
    public function setResponseMessage($status, $message) {
        echo json_encode(array(
            'status' =>$status,
            'error_message' => $message
        ));
    }


    public function prePareResponse($response) {

    }
    /*
     * implement get data
     */
    public function curlOpt($url) {
        $ch = curl_init();
        $request_headers = array();
        $request_headers[] = 'Content-Type: application/json';
        $request_headers[] = 'Connection: Keep-Alive';
        $request_headers[] = 'x-li-format: json';

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $postreturnvalues = curl_exec($ch);
        curl_close($ch);
        return $postreturnvalues;
    }

    public function checkEmailExits($email) {
        $db = $this->getDBO();
        $found = false;

        $query = 'SELECT ' . $db->quoteName('email');
        $query .= ' FROM ' . $db->quoteName('#__users');
        $query .= ' WHERE UCASE(' . $db->quoteName('username') . ') = UCASE(' . $db->Quote($email) . ')';

        $db->setQuery($query);
        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }
        $result = $db->loadObjectList();
        $found = (count($result) == 0) ? false : true;
        return $found;
    }
    public function checkUsernameExits($username, $password) {
        $options = array ( 'skip_joomdlehooks' => '1', 'silent' => 1);
        $credentials = array ( 'username' => $username, 'password' => $password);
        jimport('joomla.user.authentication');

        $authenticate = JAuthentication::getInstance();
        $response = $authenticate->authenticate($credentials, $options);
        return $response;
    }

    public function checkUserActive($username) {
        $db = $this->getDBO();
        $active = false;

        $query = 'SELECT ' . $db->quoteName('block') . ', ' . $db->quoteName('activation');
        $query .= ' FROM ' . $db->quoteName('#__users');
        $query .= ' WHERE UCASE(' . $db->quoteName('username') . ') = UCASE(' . $db->Quote($username) . ')';

        $db->setQuery($query);
        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }
        $result = $db->loadObjectList();
        if ($result[0]->block == 0 && (!isset($result[0]->activation) || $result[0]->activation == '')) {
            $active = true;
        }

        return $active;
    }

    public function sendEmail($type, $username, $password, $skipactivation = 0) {

        $params = JComponentHelper::getParams('com_users');
        $config = JFactory::getConfig();
        $useractivation = $params->get('useractivation');

        JPlugin::loadLanguage('com_users', JPATH_SITE);

        $data['fromname']   = $config->get('fromname');
        $data['mailfrom']   = $config->get('mailfrom');
        $data['sitename']   = $config->get('sitename');
        $data['siteurl']    = JUri::base();

        $user_id = JUserHelper::getUserId($username);
        $user = JFactory::getUser ($user_id);
        $data['username'] = $username;
        $data['password_clear'] = $password;
        $data['name'] = $user->name;
        $data['email'] = $user->email;
        $data['activation'] = $user->activation;
        //Disallow control chars in the email
        $password = preg_replace('/[\x00-\x1F\x7F]/', '', $password);

        if ($params->get('sendpassword', 1) == 0) {
            $password = '***';
        }

        if ($useractivation == 2)
        {
            // Set the link to confirm the user email.
            $uri = JURI::getInstance();
            $base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
            $data['activate'] = $base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$data['activation'], false);

            $emailSubject   = JText::sprintf(
                'COM_USERS_EMAIL_ACCOUNT_DETAILS',
                $data['name'],
                $data['sitename']
            );

            $emailBody = JText::sprintf(
                'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY',
                $data['name'],
                $data['sitename'],
                $data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],
                $data['siteurl'],
                $data['username'],
                $data['password_clear']
            );
        }
        else if ($useractivation == 1)
        {
            // Set the link to activate the user account.
            $uri = JURI::getInstance();
            $base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
            $data['activate'] = $base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$data['activation'], false);

            if ($skipactivation) {
                $emailSubject = JText::sprintf(
                    'PLG_USER_JOOMLA_NEW_USER_EMAIL_SUBJECT',
                    $data['name'],
                    $data['sitename']
                );
                $data['urlfaq'] = JUri::root().'/support';
                $emailBody = JText::sprintf(
                    'PLG_USER_JOOMLA_NEW_USER_EMAIL_BODY',
                    $data['name'],
                    JUri::root(),
                    $data['sitename'],
                    $data['username'],
                    $data['password_clear'],
                    $data['urlfaq']
                );
            } else {
                $emailSubject   = JText::sprintf(
                    'COM_USERS_EMAIL_ACCOUNT_DETAILS',
                    $data['name'],
                    $data['sitename']
                );

                $emailBody = JText::sprintf(
                    'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY',
                    $data['name'],
                    $data['siteurl'],
                    $data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],
                    $data['sitename'],
                    $data['username'],
                    $data['password_clear']
                );
            }
        } else {

            $emailSubject   = JText::sprintf(
                'COM_USERS_EMAIL_ACCOUNT_DETAILS',
                $data['name'],
                $data['sitename']
            );

            $emailBody = JText::sprintf(
                'COM_USERS_EMAIL_REGISTERED_BODY',
                $data['name'],
                $data['sitename'],
                $data['siteurl']
            );
        }

        $return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody,true);
    }

    public function getAdministratorEmail()
    {
        $db = $this->getDBO();

        $query = 'SELECT a.' . $db->quoteName('name') . ', a.' . $db->quoteName('email') . ', a.' . $db->quoteName('sendEmail')
            . ' FROM ' . $db->quoteName('#__users') . ' as a, '
            . $db->quoteName('#__user_usergroup_map') . ' as b'
            . ' WHERE a.' . $db->quoteName('id') . '= b.' . $db->quoteName('user_id')
            . ' AND b.' . $db->quoteName('group_id') . '=' . $db->Quote(8);

        $db->setQuery($query);

        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }

        $result = $db->loadObjectList();
        return $result;

    }

    public function ProcessReset($email) {
        $config = JFactory::getConfig();
        $email = JStringPunycode::emailToPunycode($email);

        // Find the user id for the given email address.
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('id')
            ->from($db->quoteName('#__users'))
            ->where($db->quoteName('email') . ' = ' . $db->quote($email));

        // Get the user object.
        $db->setQuery($query);

        try {
            $userId = $db->loadResult();
        } catch (RuntimeException $e) {
            $this->setResponseMessage(false, 'Error getting the user from the database');

            return false;
        }

        // Check for a user.
        if (empty($userId)) {
            $this->setResponseMessage(false, 'Invalid email address');

            return false;
        }

        // Get the user object.
        $user = JUser::getInstance($userId);

        // Make sure the user isn't blocked.
        if ($user->block) {
            $this->setResponseMessage(false, 'This user is blocked. If this is an error, please contact an administrator.');

            return false;
        }

        // Make sure the user isn't a Super Admin.
        if ($user->authorise('core.admin')) {
            $this->setResponseMessage(false, 'A Super User can\'t request a password reminder. Please contact another Super User or use an alternative method.');

            return false;
        }
        // Set the confirmation token.
        $token = JApplicationHelper::getHash(JUserHelper::genRandomPassword());
        $hashedToken = JUserHelper::hashPassword($token);

        $user->activation = $hashedToken;

        // Save the user to the database.
        if (!$user->save(true)) {
            return $this->setResponseMessage(false, 'Failed to save user');
        }

        // Assemble the password reset confirmation link.
        $mode = $config->get('force_ssl', 0) == 2 ? 1 : (-1);
//        $itemid = UsersHelperRoute::getLoginRoute();
        $itemid = $itemid !== null ? '&Itemid=' . $itemid : '';
        $link = 'index.php?option=com_users&view=reset&layout=confirm&token=' . $token . $itemid;

        // Put together the email template data.
        $data = $user->getProperties();
        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');
        $data['link_text'] = JRoute::_($link, false, $mode);
        $data['link_html'] = JRoute::_($link, true, $mode);
        $data['token'] = $token;


        $subject = 'Your '.$data['sitename'].' password reset request';


        $body = 'Hello, '.'<br/>';
        $body .= 'A request has been made to reset your Parenthexis account password. To reset your password, you will need to submit this verification code in order to verify that the request was legitimate.'.'<br/>';
        $body .= 'The verification code is '.$data['token'].'<br/>';
        $body .= 'Please copy and paste it in the field bellow to complete the password reset on App.'.'<br/>';
        $body .= 'Thank you.';

        // Send the password reset request email.
        $return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $user->email, $subject, $body, true);

        // Check for an error.
        if ($return !== true) {
            return false;
        }

        return true;
    }

    public function processConfirm($email, $token) {

        $email = JStringPunycode::emailToPunycode($email);
        // Find the user id for the given token.
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('activation')
            ->select('id')
            ->select('block')
            ->from($db->quoteName('#__users'))
            ->where($db->quoteName('email') . ' = ' . $db->quote($email));

        // Get the user id.
        $db->setQuery($query);

        try {
            $user = $db->loadObject();
        } catch (RuntimeException $e) {
            $this->setResponseMessage(FALSE, 'Error getting the user from the database');
            return false;
        }

        // Check for a user.
        if (empty($user)) {
            $this->setResponseMessage(false, 'User not found.');
            return false;
        }

        if (!$user->activation) {
            $this->setResponseMessage(false, 'Verification code not found.');
            return false;
        }

        // Verify the token
        if (!(JUserHelper::verifyPassword($token, $user->activation))) {
            $this->setResponseMessage(FALSE, 'Verification code does\'t match.');

            return false;
        }

        // Make sure the user isn't blocked.
        if ($user->block) {
            $this->setResponseMessage(false, 'This user is blocked. If this is an error, please contact an administrator.');
            return false;
        }

        // Push the user data into the session.
        $app = JFactory::getApplication();
        $app->setUserState('com_users.reset.token', $user->activation);
        $app->setUserState('com_users.reset.user', $user->id);

        return true;
    }

    public function processCompleted($password, $re_password) {
        // Get the token and user id from the confirmation process.
        $app = JFactory::getApplication();
        $token = $app->getUserState('com_users.reset.token', null);
        $userId = $app->getUserState('com_users.reset.user', null);

        // Check the token and user id.
        if (empty($token) || empty($userId)) {
            $this->setResponseMessage(FALSE, 'Your password reset confirmation failed because the verification code was missing.');
            return false;
        }

        // Get the user object.
        $user = JUser::getInstance($userId);

        // Check for a user and that the tokens match.
        if (empty($user) || $user->activation !== $token) {
            $this->setResponseMessage(FALSE, 'User not found.');

            return false;
        }

        // Make sure the user isn't blocked.
        if ($user->block) {
            $this->setResponseMessage(FALSE, 'This user is blocked. If this is an error, please contact an administrator.');

            return false;
        }

        // Check if the user is reusing the current password if required to reset their password
        if ($user->requireReset == 1 && JUserHelper::verifyPassword($password, $user->password)) {
            $this->setResponseMessage(FALSE, 'You can\'t reuse your current password, please enter a new password.');

            return false;
        }

        // Update the user object.
        $user->password = JUserHelper::hashPassword($password);
        $user->activation = '';
        $user->password_clear = $password;

        // Save the user to the database.
        if (!$user->save(true)) {
            $this->setResponseMessage(FALSE, 'Failed to save user.');
            return false;
        }

        // Flush the user data from the session.
        $app->setUserState('com_users.reset.token', null);
        $app->setUserState('com_users.reset.user', null);

        return true;
        
    }

    public function addLicenses(){
        $db = $this->getDBO();

        $query = 'UPDATE '.$db->quoteName('#__subscription') . 'SET '
            . $db->quoteName('total_use') . ' = ' . $db->quoteName('total_use').'+1'
            .', '. $db->quoteName('total_empty') . ' = ' . $db->quoteName('total_empty').'-1'
            .','.$db->quoteName('start_date') . ' = ' . $db->quote(date("Y-m-d"))
            . ' WHERE ' . $db->quoteName('id') . ' = ' . $db->Quote(1);
        $db->setQuery($query);
        $db->query();
    }
}

?>