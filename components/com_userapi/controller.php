<?php

defined('_JEXEC') or die;
require_once( JPATH_ROOT .'/components/com_community/libraries/core.php' );
//require_once(JPATH_ROOT . '/components/com_userapi/models/userapi.php');
class UserapiController extends JControllerLegacy
{
    public function display($cachable = false, $urlparams = false)
    {

        JRequest::setVar('view', 'userapi'); // force it to be the search view


        return parent::display($cachable, $urlparams);
    }

    public function signup () {
        $jinput = JFactory::getApplication()->input;

        $model = $this->getModel('Userapi');
        // set header
        $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));

        $first_name = $jinput->get('firstname', '', 'POST');
        $last_name = $jinput->get('lastname', '', 'POST');
        $email = str_replace("'", "\'", $jinput->get('email', '', 'POST'));
        $password = $jinput->get('password', '', 'POST');
        $repassword = $jinput->get('repassword', '', 'POST');
        $skipactivation = $jinput->get('sa', 0, 'POST');

        if(empty($first_name) || empty($last_name)) {
            $model->setResponseMessage(false, 'First name and last name can not be blank.');
            exit;
        }
        // check email address
        if(empty($email)) {
            $model->setResponseMessage(false, 'Email can not be blank.');
            exit;
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
             $model->setResponseMessage(false, 'Please enter a valid email address.');
            exit;
        }
        // check Email already exists
        if($model->checkEmailExits($email)) {
            $model->setResponseMessage(false, 'Email already exists. Please use another email address for registration.');
            exit;
        }
        if(empty($password) || empty($repassword)) {
            $model->setResponseMessage(false, 'Password and Verify password can not be blank.');
            exit;
        }
        // check repassword match or not
        if($repassword !== $password) {
            $model->setResponseMessage(false, 'Verify password does\'t match.');
            exit;
        }

        $username = trim($email);
        // set Obj user
        $tmpUser = new stdClass();
        $tmpUser->username = $username;
        $tmpUser->name = $first_name.' '. $last_name;
        $tmpUser->email = $email;
        $tmpUser->password = (string) $password;
        // check username already exits
        $db = JFactory::getDBO();
        $query = 'SELECT ' . $db->quoteName('username');
        $query .= ' FROM ' . $db->quoteName('#__users');
        $query .= ' WHERE UCASE(' . $db->quoteName('username') . ') = UCASE(' . $db->Quote($username) . ')';

        $db->setQuery($query);
        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }
        $result = $db->loadResult();
        if($result == $username) {
            $model->setResponseMessage(false, 'Username already exits. Please use other username for register.');
            exit;
        }
        require_once(JPATH_SITE . '/components/com_siteadminbln/models/account.php');

        $modelacc = new SiteadminblnModelAccount();
        $useduser = $modelacc->getInfosubscription();

        if($useduser->total_empty > 0)
            // $user = $tmpUser;
            $user = $this->createUser($tmpUser, 'signup', $skipactivation);
        else{
            $model->setResponseMessage(false, JText::_('COM_SITEADMINBLN_ADD_ONE_USERS_ERROR'));
            exit();
        }
        if ($user) {
            /*require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
            $groupsModel = CFactory::getModel('groups');
            $BLNCircle = $groupsModel->getBLNCircle();

            // Sync User to GLN
            $dataUser = array();
            $dataUser['firstname'] = $first_name;
            $dataUser['lastname'] = $last_name;
            $dataUser['email'] = $email;

//            $this->syncUserToGLN($dataUser, $password, $BLNCircle->id);

            // Add user to BLN Circle
            if (!empty($BLNCircle)) {
                $groupsModel->addUserToCircle($user->id, $BLNCircle->id);
            }*/
            $model->setResponseMessage(true, 'User successfully saved.');
            exit();
        } else {
            $model->setResponseMessage(false, 'User Register failed. Please try again.');
            exit();
        }
    }

    //
    public function syncUserToGLNT () {
        $mainframe = JFactory::getApplication();
        $url_BLN = $mainframe->getCfg('url_gln');
        $url = $url_BLN.'/index.php?option=com_userapi&task=signup';
        $jinput = JFactory::getApplication()->input;
        $first_name = $jinput->get('firstname', '', 'POST');
        $last_name = $jinput->get('lastname', '', 'POST');
        $email = $jinput->get('email', '', 'POST');
        $password = $jinput->get('password', '', 'POST');
        $repassword = $jinput->get('repassword', '', 'POST');
        $blnid = $jinput->get('blnid','', 'POST');

        // Postdata for sync user to GLN
        $postdata = array();
        $postdata['firstname'] = $first_name;
        $postdata['lastname'] = $last_name;
        $postdata['email'] = $email;
        $postdata['password'] = $password;
        $postdata['repassword'] = $repassword;
        $postdata['blnurl'] = JURI::root();
        $postdata['blnid'] = $blnid;

        $db = JFactory::getDBO();
        $query = 'SELECT ' . $db->quoteName('id');
        $query .= ' FROM ' . $db->quoteName('#__users');
        $query .= ' WHERE UCASE(' . $db->quoteName('username') . ') = UCASE(' . $db->Quote($postdata['email']) . ')';

        $db->setQuery($query);
        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }
        $id = $db->loadResult();

        require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
        $groupsModel = CFactory::getModel('groups');

        // Add user to BLN Circle
        $groupsModel->addUserToCircle($id,$blnid);

        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_VERBOSE,0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        // Accept certificate
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $user_ids = json_decode(curl_exec($ch));

        curl_close($ch);
        $result = array();
        $result['error'] = 0;
        $result['comment'] = 'Nice';
        $result['data'] = $r;
        echo json_encode($result);
        die;
    }
    //

    public function signupauto ($users=false) {

        $jinput = JFactory::getApplication()->input;

        require_once(JPATH_ROOT . '/components/com_userapi/models/userapi.php');
        $model = new UserapiModelUserapi();

        $first_name = $users['firstname'];
        $last_name = $users['lastname'];
        $email = $users['username'];
        $password = $users['password'];
        $repassword = $users['password'];
        $skipactivation = $jinput->get('sa', 1, 'POST');

        if(empty($first_name) || empty($last_name)) {

            return 0;
        }
        // check email address
        if(empty($email)) {
//            $model->setResponseMessage(false, 'Email can not be blank.');
            return 0;
        }
        // check Email already exists
        if($model->checkEmailExits($email)) {
            return 0;
        }
        if(empty($password) || empty($repassword)) {
            return 0;
        }
        // check repassword match or not
        if($repassword !== $password) {
            return 0;
        }

        $username = trim($email);
        // set Obj user
        $tmpUser = new stdClass();
        $tmpUser->username = $username;
        $tmpUser->name = $first_name.' '. $last_name;
        $tmpUser->email = $email;
        $tmpUser->password = (string) $password;
        // check username already exits
        $db = JFactory::getDBO();
        $query = 'SELECT ' . $db->quoteName('username');
        $query .= ' FROM ' . $db->quoteName('#__users');
        $query .= ' WHERE UCASE(' . $db->quoteName('username') . ') = UCASE(' . $db->Quote($username) . ')';

        $db->setQuery($query);
        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }
        $result = $db->loadResult();
        if($result == $username) {
            $model->setResponseMessage(false, 'Username already exits. Please use other username for register.');
            return 0;
        }

        $user = $this->createUser($tmpUser, 'signup', $skipactivation);
        if ($user) {
            require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
            $groupsModel = CFactory::getModel('groups');
            $BLNCircle = $groupsModel->getBLNCircle();

            // Sync User to GLN
            $dataUser = array();
            $dataUser['firstname'] = $first_name;
            $dataUser['lastname'] = $last_name;
            $dataUser['email'] = $email;

            $this->syncUserToGLN($dataUser, $password, $BLNCircle->id);

            // Add user to BLN Circle
            if (!empty($BLNCircle)) {
                $groupsModel->addUserToCircle($user->id, $BLNCircle->id);
            }
//            $model->setResponseMessage(true, 'User successfully saved.');
            return 0;
        } else {
//            $model->setResponseMessage(false, 'User Register failed. Please try again.');
            return 0;
        }
    }

    public function syncUserToGLN () {
        $mainframe = JFactory::getApplication();
        $url_BLN = $mainframe->getCfg('url_gln');
        $url = $url_BLN.'/index.php?option=com_userapi&task=signup';

        $jinput = JFactory::getApplication()->input;
        $data = $jinput->get('data', '', 'POST');
        $blnid = $jinput->get('blnid','', 'POST');

        if (!empty($data)) {
            $data = json_decode($data);

            foreach ($data as $key => $item) {
                require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
                $groupsModel = CFactory::getModel('groups');

                // Add user to BLN Circle
                $groupsModel->addUserToCircle($item->user_id,$blnid);

                // Postdata for sync user to GLN
                $postdata = array();
                $postdata['firstname'] = $item->data->firstname;
                $postdata['lastname'] = $item->data->lastname;
                $postdata['email'] = $item->data->email;
                $postdata['password'] = $item->password;
                $postdata['repassword'] = $item->password;
                $postdata['blnurl'] = JURI::root();
                $postdata['blnid'] = $blnid;

                $ch = curl_init();
                // set url
                curl_setopt($ch, CURLOPT_URL, $url);

                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_VERBOSE,0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
                // Accept certificate
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                $user_ids = json_decode(curl_exec($ch));

                curl_close($ch);
            }
        }
    }
    /*
        public function syncUserToGLN ($data, $pass, $blnid) {
            $mainframe = JFactory::getApplication();
            $url_BLN = $mainframe->getCfg('url_gln');
            $url = $url_BLN.'/index.php?option=com_userapi&task=signup';

            // Postdata for sync user to GLN
            $postdata = array();
            $postdata['firstname'] = $data['firstname'];
            $postdata['lastname'] = $data['lastname'];
            $postdata['email'] = $data['email'];
            $postdata['password'] = $pass;
            $postdata['repassword'] = $pass;
            $postdata['blnurl'] = JURI::root();
            $postdata['blnid'] = $blnid;

            $ch = curl_init();
            // set url
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_VERBOSE,0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
            // Accept certificate
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $user_ids = json_decode(curl_exec($ch));

            curl_close($ch);
        }
    */
    public function login_social() {
        $jinput = JFactory::getApplication()->input;

        $model = $this->getModel('Userapi');
        // set header
        $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));

        $apptype = $jinput->get('appType', '', 'POST');
        $apptoken = $jinput->get('appToken', '', 'POST');
        $appid = $jinput->get('appID', '', 'POST');
        $appplatform = $jinput->get('appPlatform', '', 'POST');

        $social_user = $this->verifySosocalPlugin($apptype, $apptoken, $appid, $appplatform);
        if ($social_user) {
            echo json_encode($social_user);
            exit;
        } else {
            $model->setResponseMessage(false, 'Login social failed. Please try again.');
            exit;
        }

    }

    public function verifySosocalPlugin($apptype, $apptoken, $appid, $appplatform = '') {
        $model = $this->getModel('Userapi');
        // set header
        $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));

        switch (strtoupper($apptype)) {
            case 'FB':
                $url = 'https://graph.facebook.com/me?access_token='.$apptoken;
                $postreturnvalues = $model->curlOpt($url);
                $facebookuser = json_decode($postreturnvalues);
                if(empty($facebookuser)) {
                    $model->setResponseMessage(false, $postreturnvalues);
                    exit;
                }
                $id = $facebookuser->id;
                if($facebookuser->email) {
                    $useremail = $facebookuser->email;
                } else {
                    $useremail = uniqid().'_fb_account@facebook.com';
                }
                if($facebookuser->first_name || $facebookuser->last_name) {
                    $fullname = $facebookuser->first_name .' '.$facebookuser->last_name;
                } else {
                    $fullname = $facebookuser->name;
                }
                break;
            case 'GG':
                if ($appplatform == 'web') $url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='.$apptoken.'&alt=json'; else
                    $url = 'https://www.googleapis.com/plus/v1/people/me?access_token='.$apptoken.'&alt=json';
                $postreturnvalues = $model->curlOpt($url);
                $responseArr = json_decode($postreturnvalues, true);
                $postreturnvalues = json_decode($postreturnvalues);

                if ($postreturnvalues->error) {
                    $model->setResponseMessage(false, $postreturnvalues->error->errors[0]->message);
                    exit;
                }
                $fullname = 'google_account';
                if ($appplatform == 'web') {
                    if ($responseArr['email_verified']) $useremail = $responseArr['email'];
                    $id = $responseArr['sub'];
                    if (isset($responseArr['name'])) {
                        $fullname = $responseArr['name'];
                    } else $fullname = $responseArr['given_name'] .' '. $responseArr['family_name'];
                } else {
                    foreach($postreturnvalues->emails as $googleemail) {
                        if($googleemail->type == "account") {
                            $useremail = $googleemail->value;
                        }
                    }
                    $useremail = $postreturnvalues->emails[0]->value;
                    $id = $postreturnvalues->id;
                    if(isset($postreturnvalues->name)) {
                        $fullname = $postreturnvalues->name->familyName .' '. $postreturnvalues->name->givenName;
                    }
                }
                //            $url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?acces_token='.$apptoken;
                break;
            case 'IN':
                if ($appplatform == 'web') $url = 'https://api.linkedin.com/v1/people/~:(first-name,last-name,email-address,location:(name,country:(code)))?oauth_token='.$apptoken.'&format=json'; else
                    $url = 'https://api.linkedin.com/v1/people/~:(first-name,last-name,email-address,location:(name,country:(code)))?oauth2_access_token='.$apptoken.'&format=json';
                $postreturnvalues = $model->curlOpt($url);
                $linkedinuser = json_decode($postreturnvalues);
                if ($appplatform == 'web') {
                    $userInfo = json_decode($postreturnvalues, true);
                    if (isset($userInfo['errorCode']) && $userInfo['errorCode'] == 0) {
                        $model->setResponseMessage(false, $userInfo['status'].' - '.$userInfo['message']);
                        exit;
                    }
                    $useremail = $userInfo['emailAddress'];
                    $id = $userInfo['id'];
                    $fullname = $userInfo['firstName']. ' '. $userInfo['lastName'];
                } else {
                    if($linkedinuser->status == 401) {
                        $model->setResponseMessage(false, 'Unable to verify access token');
                        exit;
                    }
                    $useremail = $linkedinuser->emailAddress;
                    $id = $linkedinuser->id;
                    $fullname = $linkedinuser->firstName. ' '. $linkedinuser->lastName;
                }

                break;
        }

        if( isset($id) && $id != $appid ) //Check if this auth token has the same ID sent.
        {
            $model->setResponseMessage(false, 'Invalid ID Sent');
            exit;
        } else {
            $username = trim($appid);
            if ($appplatform == 'web' && strtoupper($apptype) == "IN" ) $username = $useremail;

            // set Obj user
            $tmpUser = new stdClass();
            $tmpUser->username = $username;
            $tmpUser->name = $fullname;
            $tmpUser->email = $useremail;
            $tmpUser->password = (string) $appid;

            $params = array();
            $params['username'] = $tmpUser->username;
            $params['password'] = $tmpUser->password;
            // check username already exits
            // if user already exits. Login user
            $response = $model->checkUsernameExits($username, $tmpUser->password);

            if ($response->status === 1) {
                // TODO: remove message
                // Implement login user
                $this->doLoginUser($params);
            } else {
                if ($model->checkEmailExits($useremail)) {
                    $model->setResponseMessage(false, 'Email already exists. Please use another email address for registration.');
                    exit;
                } else {
                    $user = $this->createUser($tmpUser, null);

                    if ($user) {
                        $this->doLoginUser($params);
                    } else {
                        $model->setResponseMessage(false, 'Social login failed. Please contact site administrator for support.');
                        exit;
                    }
                }
            }
        }
    }

    // update user device token
    public function user_device() {
        $jinput = JFactory::getApplication()->input;
        $model = $this->getModel('Userapi');
        // set header
        $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));

        $username = $jinput->get('username', '', 'POST');
        $device_token = $jinput->get('device_token', '', 'POST');
        $app = $jinput->get('app', '', 'POST');

        if(empty($username)) {
            $model->setResponseMessage(false, 'Username is empty');
            exit;
        }
        if(empty($device_token)) {
            $model->setResponseMessage(false, 'Device token is empty');
            exit;
        }
//        $my = JFactory::getUser($username);
        jimport('joomla.user.helper');
        $user_id = JUserHelper::getUserId($username);
        $my = JFactory::getUser($user_id);
        // update user device
        if(!empty($app)) {
            $objUser['device_id2'] = $device_token;
            $user = new JUser($my->id);
            $user->set('device_id2', $device_token);
        } else {
            $objUser['device_id'] = $device_token;
            $user = new JUser($my->id);
            $user->set('device_id', $device_token);
        }
        // Bind the data.
        if (!$user->bind($objUser)) {
            $this->setError($user->getError());

            return false;
        }
        if($user->save()) {
            $model->setResponseMessage(true, 'Device token updated.');
            exit;
        } else {
            $model->setResponseMessage(false, 'Something wrong for change.');
            exit;
        }
    }

    // login google for android app
    public function login_google() {
        $jinput = JFactory::getApplication()->input;

        $model = $this->getModel('Userapi');
        // set header
        $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));

        $appid = $jinput->get('appID', '', 'POST');
        $apptype = $jinput->get('appType', '', 'POST');
        $email = $jinput->get('email', '', 'POST');
        $fullname = $jinput->get('fullname', '', 'POST');

        if(empty($appid)) {
            $model->setResponseMessage(false, 'App id can not be empty.');
            exit;
        }
        if(empty($email)) {
            $model->setResponseMessage(false, 'Email can not be empty.');
            exit;
        }
        if(empty($fullname)) {
            $model->setResponseMessage(false, 'Fullname can not be empty.');
            exit;
        } else {
            $username = trim($appid);
            // set Obj user
            $tmpUser = new stdClass();
            $tmpUser->username = $username;
            $tmpUser->name = $fullname;
            $tmpUser->email = $email;
            $tmpUser->password = (string) $appid;

            // check user already exits
            $response = $model->checkUsernameExits($username, $tmpUser->password);

            if($response->status === 1) {
                // TODO: remove message
                // Implement login user
                $this->doLoginUser((array)$tmpUser);
            } else {
                $user = $this->createUser($tmpUser, null);
                if($user) {
                    $this->doLoginUser((array)$tmpUser);
                } else {
                    $model->setResponseMessage(false, 'Something wrong. Please contact with administrator site for support.');
                    exit;
                }
            }
        }
    }

    public function createUser($tmpUser, $register_type = null, $skipactivation = 0) {
        $user = new JUser;
        $authorize = JFactory::getACL();
        $usersConfig = JComponentHelper::getParams('com_users');

        $userObj = get_object_vars($tmpUser);

        // Get usertype from configuration. If tempty, user 'Registered' as default
        $newUsertype = $usersConfig->get('new_usertype');

        if (!$newUsertype) {
            $newUsertype = 'Registered';
        }
        $date = JFactory::getDate();

        $user->set('id', 0);
        $user->set('usertype', $newUsertype);
        $user->set('gid', ($newUsertype));

        // Set value tmpUser
        $tmpUser->usertype = $newUsertype;
        $tmpUser->gid = $newUsertype;
        $tmpUser->groups = array($newUsertype => $newUsertype);
        $tmpUser->registerDate = $date->toSql();

        //set group for J1.6
        $user->set('groups', array($newUsertype => $newUsertype));

        $user->set('registerDate', $date->toSql());
        //Jooomla 3.2.0 fix. TO be remove in future
        if (version_compare(JVERSION, '3.2.0', '=')) {
            $salt = JUserHelper::genRandomPassword(32);
            $crypt = JUserHelper::getCryptedPassword($userObj['password'], $salt);
            $password = $crypt . ':' . $salt;
        } else {
            // Don't re-encrypt the password
            // JUser bind has encrypted the password
            $password = $userObj['password'];
        }

        $user->set('password', $password);
        // if ($register_type == 'signup') {
        //     $user->set('activation', md5(JUserHelper::genRandomPassword()));
        //     $user->set('block', '1');
        // }
        // Bind the data.

        if (!$user->bind($userObj)) {
            $this->setError($user->getError());
            return false;
        }
        // Load the users plugin group.
        JPluginHelper::importPlugin('user');

        $tmpUser->password = $user->password;

        try {
            $db = JFactory::getDbo();

            $query = $db->getQuery(true);
            $columns = ['username', 'name', 'email', 'password', 'registerDate'];

            $values = [$db->quote($tmpUser->username), $db->quote($tmpUser->name), $db->quote($tmpUser->email), $db->quote($tmpUser->password), $db->quote($tmpUser->registerDate)];

            $query->insert($db->quoteName('#__users'))->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);
            $db->query();

            $user = $db->insertid();

            // Insert record user group
            $db = JFactory::getDbo();

            $query1 = $db->getQuery(true);
            $columns = ['user_id', 'group_id'];
            $values = [$db->quote($user), $db->quote(2)];

            $query1->insert($db->quoteName('#__user_usergroup_map'))->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query1);
            $db->query();

            $model = $this->getModel('Userapi');
            $model->addLicenses();
            $model->sendEmail('registration', $tmpUser->username, $password, $skipactivation);
            return $user;

        } catch(Exception $e) {
            echo $e;
        }

        // if (!$user->save()) {
        //     return false;
        // } else {
        //     $model = $this->getModel('Userapi');
        //     $model->addLicenses();
        //     $model->sendEmail('registration', $user->username, $password, $skipactivation);
        //     return $user;
        // }
        return false;
    }

    public function doLoginUser($user) {
        // do something
        $mainframe = JFactory::getApplication();
        $model = $this->getModel('Userapi');
        $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));
        $options = array ('login_type' => 'app', 'return_url' => 'index.php?option=com_userapi&task=return_data');
        $credentials = array ( 'username' => $user['username'], 'password' => $user['password']);
        if ($mainframe->login($credentials, $options) === false) {
            // set header
            $model->setResponseMessage(false, 'Login failure. Please contact administrator site for support.');
            exit;
        } else {
            $model->setResponseMessage(true, 'Login successfully!');
            exit;
        }
    }

    public function login() {
        $app    = JFactory::getApplication();
        $jinput = JFactory::getApplication()->input;

        $model = $this->getModel('Userapi');
        // set header
        $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));

        $username = $jinput->get('username', '', 'POST');
        $password = $jinput->get('password', '', 'POST');

        $username = utf8_decode($username);
        $username = strtolower($username);

        if(is_null($username)) {
            $model->setResponseMessage(false, 'User is blank');
            exit;
        }
        if(is_null($password)) {
            $model->setResponseMessage(false, 'Password is blank');
            exit;
        }
        // check user block or not active
        $user_active = JFactory::getUser($username);
        if($user_active->block == 1) {
            $model->setResponseMessage(false, 'Login denied! Your account has either been blocked or you have not activated it yet.');
            exit;
        }

        $response = $model->checkUsernameExits($username, $password);
        $status = (int) $response->status;
        if($status===1) {
            $user = array();
            $user['username'] = $username;
            $user['password'] = $password;

            $token_key = base64_encode(strtolower($username).':'.$password);
            $tmpFileName = 'tmp_'.strtolower($username).'.json';
            if (!is_dir(dirname($app->getCfg('dataroot')).'/temp/tmpkey')) {
                mkdir(dirname($app->getCfg('dataroot')).'/temp/tmpkey', 0775);
            }
            $tmp_file = dirname($app->getCfg('dataroot')).'/temp/tmpkey/'.$tmpFileName;
            file_put_contents($tmp_file, $token_key);

            $this->doLoginUser($user);
        }
        else {
            $model->setResponseMessage(false, 'Login failed. Username and password do not match or you do not have an account yet.');
            exit;
        }
    }

    public function updateUser() {
        $app = JFactory::getApplication();
        $jinput = JFactory::getApplication()->input;

        $model = $this->getModel('Userapi');
        // set header
        $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));

        $email = $jinput->get('username', '', 'POST');
        $password = $jinput->get('newpassword', '', 'POST');
        // check email address
        if(empty($email)) {
            $model->setResponseMessage(false, 'Username can not be blank.');
            exit;
        }
        if(empty($password)) {
            $model->setResponseMessage(false, 'New password can not be blank.');
            exit;
        }

        $username = trim($email);
        // set Obj user
        $tmpUser = new stdClass();
        $tmpUser->username = $username;
        // check username already exits
        $db = JFactory::getDBO();
        $query = 'SELECT ' . $db->quoteName('id');
        $query .= ' FROM ' . $db->quoteName('#__users');
        $query .= ' WHERE UCASE(' . $db->quoteName('username') . ') = UCASE(' . $db->Quote($username) . ')';

        $db->setQuery($query);
        if ($db->getErrorNum()) {
            JError::raiseError(500, $db->stderr());
        }
        $result = $db->loadResult();
        if($result) {
            $user = new JUser;
            $user->load($result);
            $user->password = JUserHelper::hashPassword($password);
            $user->activation = '';
            $user->password_clear = $password;

            // Load the users plugin group.
            JPluginHelper::importPlugin('user');

            if (!$user->save(true)) {
                $model->setResponseMessage(false, 'User Save failed. Please try again.');
                exit();
            } else {
                // Flush the user data from the session.
                $app->setUserState('com_users.reset.token', null);
                $app->setUserState('com_users.reset.user', null);

                $model->setResponseMessage(true, 'User successfully saved.');
                exit();
            }
        } else {
            $model->setResponseMessage(true, 'Username don\'t exits.');
            exit();
        }
    }

    public function switchToGLN () {
        $app    = JFactory::getApplication();

        $user = JFactory::getUser();

        $tmpFileName = 'tmp_' . strtolower($user->username) . '.json';
        $tmp_file = dirname($app->getCfg('dataroot')) . '/temp/tmpkey/' . $tmpFileName;

        $url_BLN = $app->getCfg('url_gln');

        $tokenkey = file_get_contents($tmp_file);

        header("Location: $url_BLN/courses/login/api_sso_bln.php?tokenkey=$tokenkey"); /* Redirect browser */
        exit();
    }

    public function logout() {
        $jinput = JFactory::getApplication()->input;

        $model = $this->getModel('Userapi');
        // set header
        $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));

        $username = $jinput->get('username', '', 'POST');
        $password = $jinput->get('password', '', 'POST');

        $username = (string) $username;
        $password = (string) $password;

        $user = JFactory::getUser();
        if($user->id == 0) {
            $model->setResponseMessage(true, 'User already logout.');
            exit;
        }
        $this->doLogoutUser($user);
    }

    public function logout_social() {
        $mainframe = JFactory::getApplication('site');
        $jinput = JFactory::getApplication()->input;

        $model = $this->getModel('Userapi');
        // set header
        $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));

        $username = $jinput->get('username', '', 'POST');
        $password = $jinput->get('password', '', 'POST');

        $username = (string) $username;
        $password = (string) $password;
        $user = JFactory::getUser();
        if($user->id == 0) {
            $model->setResponseMessage(true, 'User already logout.');
            exit;
        }
        $this->doLogoutUser($user);
    }

    public function doLogoutUser($user) {
        $mainframe = JFactory::getApplication('site');
        $model = $this->getModel('Userapi');
        $options = array();
        if ($mainframe->logout( $user->id, $options )) {
            $dispatcher = JDispatcher::getInstance();
            JPluginHelper::importPlugin('joomdlehooks');
            $option = array();
            $dispatcher->trigger('onUserLogout', array($user, $option));
            $model->setResponseMessage(true, 'Logout successfull.');
            exit;
        }
    }

    public function return_data() {

        $jinput = JFactory::getApplication()->input;
        $username = $jinput->get('username', '', 'GET');
        $model = $this->getModel('Userapi');
        // set header
        $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));

        jimport('joomla.user.helper');
        $user_id = JUserHelper::getUserId($username);
        $Profilemodel = CFactory::getModel('profile');
        CFactory::setActiveProfile($user_id);
        $users = CFactory::getUser($user_id);
        $profile = $Profilemodel->getViewableProfile($user_id);
        $groupmodel = CFactory::getModel('groups');
        $notifModel = CFactory::getModel('notification');
        $userParams = $users->getParams();

        //get default avatar full path
        if(empty($users->_avatar)) {
            $useravatar = JURI::base() . 'components/com_community/assets/user-Male.png';
        } else {
            $useravatar = $users->getAvatar();
        }
        $result = array();
        $result['user_community_id'] = $users->id;
        $result['username'] = $users->username;
        $result['fullname'] = $users->name;
        $result['profileimageurl'] = $useravatar;
        $result['profilecoverurl'] = $users->getCover();
        $result['status'] = $users->getStatus();
        $result['interest'] = (string) $profile['fields']['Other Information'][0]['value'];
        $result['alias'] = $users->_alias;

        $result['notifycount'] = $notifModel->getNotificationCount($users->id, '0', $userParams->get('lastnotificationlist', ''));

        $result['groupscount'] = $groupmodel->getGroupsCount($profile['id']);
        $result['groups'] = $users->_groups;
        // BLN information
        $result['isOwnerBLN'] = false;
        $result['isMangerBLN'] = false;
        $result['isMemberBLN'] = false;
        $result['canCreate'] = true;
        if($users->_permission) {
            $result['canCreate'] = false;
        }
        $infoBLN = $groupmodel->checkBLNCircle($users->id);
        $result['BLNid'] = 0;
        if($infoBLN) {
            $result['isMemberBLN'] = true;
            if($infoBLN->isBLN == 1 && $infoBLN->permissions == 1 && $users->id == $infoBLN->ownerid) {
                $result['isOwnerBLN'] = true;
            }
            if($infoBLN->permissions == 1) {
                $result['isMangerBLN'] = true;
            } else {
                $result['isMangerBLN'] = false;
            }
            $result['BLNid'] = $infoBLN->id;
        }

        $result['birthday'] = (string) $profile['fields']['Basic Information'][1]['value'];

        require_once(JPATH_SITE . '/components/com_joomdle/helpers/content.php');
        $course_inprogress = JoomdleHelperContent::call_method('user_course_inprogress', $users->username);
        $response = array();
        $response['status'] = true;
        $response['user'] = $result;
        $response['course_inprogress'] = $course_inprogress;
        echo json_encode($response);
        exit;
    }
    // added api reset password
    public function reset() {
        $mainframe = JFactory::getApplication('site');
        $jinput = JFactory::getApplication()->input;

        $email = $jinput->get('email', '', 'POST');

        $model = $this->getModel('Userapi');
        $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));
        if(empty($email)) {
            $model->setResponseMessage(true, 'Email can not be empty.');
            exit;
}

        $reset = $model->ProcessReset($email);
        $response = array();
        if($reset) {
            $response['status'] = true;
            $response['error_message'] = 'Reset password successful. Verification code has been sent to your email.';
            echo json_encode($response);
            exit;
        } else {
            exit;
        }
    }

    public function confirm() {
        $mainframe = JFactory::getApplication('site');
        $jinput = JFactory::getApplication()->input;

        $email = $jinput->get('email', '', 'POST');
        $token = $jinput->get('token', '', 'POST');

        $model = $this->getModel('Userapi');
        $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));

        if(empty($email)) {
            $model->setResponseMessage(true, 'Email can not be empty.');
            exit;
        }
        if(empty($token)) {
            $model->setResponseMessage(true, 'Verification code can not be empty.');
            exit;
        }
        $response = array();
        $confirm = $model->processConfirm($email, $token);
        if($confirm) {
            $response['status'] = true;
            $response['error_message'] = 'Confirm successful.';
            echo json_encode($response);
            exit;
        } else {
            exit;
        }

    }

    public function completed() {
        $mainframe = JFactory::getApplication('site');
        $jinput = JFactory::getApplication()->input;

        $password = $jinput->get('password', '', 'POST');
        $re_password = $jinput->get('re_password', '', 'POST');

        $model = $this->getModel('Userapi');
        $model->setHeader(array('Content-type: application/json; charset=UTF-8', 'Accept: application/json'));

        if(empty($password)) {
            $this->setResponseMessage(false, 'Password can not be empty.');
            exit;
        }
        if(empty($re_password)) {
            $this->setResponseMessage(false, 'Re-Password can not be empty.');
            exit;
        }

        if($re_password !== $password) {
            $model->setResponseMessage(false, 'Verify password does\'t match.');
            exit;
        }
        $completed = $model->processCompleted($password, $re_password);

        $response = array();
        if($completed) {
            $response['status'] = true;
            $response['error_message'] = 'Reset password successful. You may now login to the site.';
            echo json_encode($response);
            exit;
        } else {
            exit;
        }
    }
}


?>