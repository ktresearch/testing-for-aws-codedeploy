<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.1
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
$height = $this->newSizes->height;
$width = $this->newSizes->width;
$link = hikashop_contentLink('product&task=show&cid='.$this->row->product_id.'&name='.$this->row->alias.$this->itemid.$this->category_pathway,$this->row);

if(!empty($this->row->extraData->top)) { echo implode("\r\n",$this->row->extraData->top); }
?>
<div style="background-color:rgb(247,246,243);padding:10px;min-height:220px;">
	<div>
		<div style="width:80%;float:left;">
			<strong style="font-size:20px;">
			<!-- PRODUCT NAME -->
				<span class="hikashop_product_name">
					<?php if($this->params->get('link_to_product_page',1)){ ?>
						<a href="<?php echo $link;?>">
					<?php } ?>
							<?php
							echo $this->row->product_name;
							?>
					<?php if($this->params->get('link_to_product_page',1)){ ?>
						</a>
					<?php } ?>
				</span>
			<!-- EO PRODUCT NAME -->
			</strong>
		</div>
		<div style="width:19%;display:inline-block;">
			<span style="font-weight:bold;">
			<!-- Start of Estimated Duration -->
			<?php
				if(!empty($this->row->estimated_duration)){
					$est_time=(double)$this->row->estimated_duration;
					$suffix=" Hours";
					if($est_time <= 1)
						$suffix=" Hour";
					echo $this->row->estimated_duration.$suffix;
				}
			?>
			<!-- End of Estimated Duration -->
		</span>
			<!--  Start of Price -->
			<?php
			if($this->params->get('show_price','-1')=='-1'){
				$config =& hikashop_config();
				$this->params->set('show_price',$config->get('show_price'));
			}
			if($this->params->get('show_price')){
				$this->setLayout('listing_price');
				echo $this->loadTemplate();
			}
			?>
			<!-- End of Price -->
		</div>
		<div style="clear:both;"></div>
	</div>
	<div>
				<!-- Start of PRODUCT IMAGE -->
		<?php if($this->config->get('thumbnail',1)){ ?>
		<div style="height:<?php echo $this->image->main_thumbnail_y;?>px;float:left;margin-right:8px;" class="hikashop_product_image">
			<div style="position:relative;text-align:center;clear:both;width:<?php echo $this->image->main_thumbnail_x;?>px;margin: auto;" class="hikashop_product_image_subdiv">
			<?php if($this->params->get('link_to_product_page',1)){ ?>
				<a href="<?php echo $link;?>" title="<?php echo $this->escape($this->row->product_name); ?>">
			<?php }
				$image_options = array('default' => true,'forcesize'=>$this->config->get('image_force_size',true),'scale'=>$this->config->get('image_scale_mode','inside'));
				$img = $this->image->getThumbnail(@$this->row->file_path, array('width' => $this->image->main_thumbnail_x, 'height' => $this->image->main_thumbnail_y), $image_options);
				if($img->success) {
					echo '<img class="hikashop_product_listing_image" title="'.$this->escape(@$this->row->file_description).'" alt="'.$this->escape(@$this->row->file_name).'" src="'.$img->url.'"/>';
				}
				$main_thumb_x = $this->image->main_thumbnail_x;
				$main_thumb_y = $this->image->main_thumbnail_y;
				if($this->params->get('display_badges',1)){
					$this->classbadge->placeBadges($this->image, $this->row->badges, -10, 0);
				}
				$this->image->main_thumbnail_x = $main_thumb_x;
				$this->image->main_thumbnail_y = $main_thumb_y;

			if($this->params->get('link_to_product_page',1)){ ?>
				</a>
			<?php } ?>
					<!-- End of PRODUCT IMAGE -->
			</div>
		</div>
		<?php } ?>
		<!-- PRODUCT DESCRIPTION -->
		<div class="hikashop_product_desc" style="text-align:<?php echo $this->align; ?>">
			<?php
				if (str_word_count($this->row->product_description, 0) > 45) {
								$words = str_word_count($this->row->product_description, 2);
								$pos = array_keys($words);
							$text = substr($this->row->product_description, 0, $pos[45]) . '...';
						}else{
							$text=$this->row->product_description;
						}
			echo preg_replace('#<hr *id="system-readmore" */>.*#is','',$text);
			?>
		</div>
		<!-- EO PRODUCT DESCRIPTION -->
	</div>
</div>
<?php if(!empty($this->row->extraData->bottom)) { echo implode("\r\n",$this->row->extraData->bottom); } ?>
