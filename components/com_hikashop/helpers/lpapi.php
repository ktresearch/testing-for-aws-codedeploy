<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/*
*
* [TODO] update category image, validate existing category and testing
* [TODO] update product that have been published or unpublished
* [TODO] check things before remove
*
*/
defined('_JEXEC') or die('Restricted access');

error_reporting(0);
//require_once(JPATH_SITE.'/plugins/joomdleshop/joomdleshophikashop/joomdleshophikashop.php');
require_once(JPATH_ADMINISTRATOR.'/'.'components'.'/'.'com_joomdle'.'/'.'helpers'.'/'.'content.php');
require_once( JPATH_ADMINISTRATOR.'/components/com_hikashop/helpers/helper.php' );

class LpApiController extends hikashopController {

    private $shop_integration = 'hikashop';

    function __construct(&$params, $config) {
        parent::__construct($config);
    }

    // function integration_enabled (){
    //     // Don't run if not configured as shop
    //     $params = JComponentHelper::getParams( 'com_joomdle' );
    //     $shop_integration = $params->get( 'shop_integration' );
    //     return  ($shop_integration == $this->shop_integration);
    // }

    public function apiRouter($api, $action, $data) {
        $user = JFactory::getUser();
        $jinput = JFactory::getApplication()->input;
        $response = array();
        if($api == 'lpapi') {
            if($user->id==0){
                $response['status']=false;
                $response['message']='Request not allowed ( access forbidden )';
            }elseif(empty($action)) {
                $response['status'] = false;
                $response['message'] = 'Request not found';
            }
            else {
                if($action == 'listcategory') {
                    $response = $this->getListCategory();
                }
                elseif($action == 'publishcourse') {
                    $courseid = $jinput->get('courseid', 0, 'POST');
                    $categorieslist = $jinput->get('categorieslist',0,'POST');
                    $lpcircleid = $jinput->get('lpcircleid',0,'POST');
                    $response = $this->publishcourse($courseid,$categorieslist,$lpcircleid);
                }elseif($action == 'updatecourse'){
                    $courseid = $jinput->get('courseid',0,'POST');
                    $response = $this->updatecourse($courseid);
                }elseif($action == 'unpublishcourse'){
                    $categoryid = $jinput->get('categoryid',0,'POST');
                    $courseid = $jinput->get('courseid',0,'POST');
                    $response = $this->unpublishcourse($categoryid,$courseid);
                }elseif($action == 'removecourse'){
                    $courseid = $jinput->get('courseid',0,'POST');
                    $categoryid = $jinput->get('categoryid',0,'POST');
                    $response = $this->removecourse($courseid,$categoryid);
                }elseif($action == 'createcategory'){
                    $categoryname = $jinput->get('categoryname',0,'POST');
                    $lpcircleid = $jinput->get('lpcircleid',0,'POST');
                    $response = $this->createcategory($categoryname,$lpcircleid);
                }elseif($action == 'getcategorylist'){
                   $courseid =  $jinput->get('courseid',0,'POST');
                    $response = $this->getcategorylist($courseid);
                }elseif($action == 'getHikashopCourses'){
                    $lpcircleid = $jinput->get('lpcircleid',0,'POST');
                    $response = $this->getHikashopCourses($lpcircleid);
                }
            }
        } else {
            $response['status'] = false;
            $response['message'] = 'Request not found';
        }

        // return data
        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($response);
        exit();
    }

    /*
    * POST index.php?option=com_hikashop&ctrl=lpapi&task=createcategory&categoryname=New LP&lpcircleid=244
    */
    static function createcategory($categoryname, $lpcircleid) {
        $app = JFactory::getApplication();
        $config = hikashop_config();
        $allowed = $config->get('allowedfiles');
        $imageHelper = hikashop_get('helper.image');

        $file_class = hikashop_get('class.file');
        $uploadPath = $file_class->getPath('product','');

        $db  = JFactory::getDBO();
        $querySelect = 'SELECT category_id FROM #__hikashop_category WHERE category_name="Learning Provider" and category_parent_id=1';
        $db->setQuery($querySelect);
        $lp_categories = $db->loadObjectList();

        if(!empty($lp_categories)){
            $category_namekey='product_'.time().'_'.rand();

            $queryFindRight = 'SELECT MAX(category_right) as category_right FROM #__hikashop_category WHERE category_parent_id='.$lp_categories[0]->category_id;
            $db->setQuery($queryFindRight);
            $categoryRight = $db->loadObjectList();

            if (isset($lpcircleid)) {
                require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
                $group = JTable::getInstance( 'Group' , 'CTable' );
                $group->load($lpcircleid);
                $circle_avatar = $group->getOriginalAvatar();
                }

            if (!empty($categoryRight)) {
                $query ='INSERT IGNORE INTO '.hikashop_table('category').' (category_type, category_namekey, category_name, category_description, category_left,category_right,category_parent_id,category_created,category_modified,category_published,learningprovidercircleid) VALUES (\'product\',\''.$category_namekey.'\', \''.$categoryname.'\', \''.$group->description.'\', '.((int)$categoryRight[0]->category_right+1).','.((int)$categoryRight[0]->category_right+2).',\''.$lp_categories[0]->category_id.'\','.time().','.time().',1,'.(int)$lpcircleid.')';
                $db->setQuery($query);
                $num = $db->execute();
                $new_id = $db->insertid();

                if (isset($circle_avatar)) {
//                    if (!empty($circle_avatar)) {
//                        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
//                        $domainName = $_SERVER['HTTP_HOST'];
//                        $pic_url = $protocol.$domainName.'/courses/viewfile.php/'.$circle_avatar;
//                    }
                    $pic_url = $circle_avatar;
                    $pic = JoomdleHelperContent::get_file ($pic_url);

                    $file = new stdClass();
                    $file->file_name = '';
                    $file->file_description = '';

                    $filename = basename ($pic_url);
                    $file_path = strtolower(JFile::makeSafe($filename));

                    if(!preg_match('#\.('.str_replace(array(',','.'),array('|','\.'),$allowed).')$#Ui',$file_path,$extension) || preg_match('#\.(php.?|.?htm.?|pl|py|jsp|asp|sh|cgi)$#Ui',$file_path)){
                        //$app->enqueueMessage(JText::sprintf( 'ACCEPTED_TYPE',substr($file_path,strrpos($file_path,'.')+1),$allowed), 'notice');
                    }
                    $file_path= str_replace(array('.',' '),'_',substr($file_path,0,strpos($file_path,$extension[0]))).$extension[0];

                    file_put_contents ($uploadPath . $file_path,  $pic);

                    $imageHelper->resizeImage($file_path);
                    $imageHelper->generateThumbnail($file_path);

                    $file->file_path = $file_path;
                    $file->file_type = 'category';
                    $file->file_ref_id = $new_id;

                   $image_id = $file_class->save($file);
                }


                $response['status'] = true;
                $response['message'] = 'Category created';
                return $response;
            }

        }

        $response['status'] = false;
        $response['message'] = 'Category did not created';
        return $response;
    }


    /*
    *
    *   Update Hikashop Category whenever Learning Provider Circle information update or
    *   Learning Provider Circle Avatar Update.
    *
    *   Created by NYI 2017/08/18

    *   @param $lpcircleid
    *   @return $return
    *
    */
    static function updatecategory($lpcircleid) {
        $app = JFactory::getApplication();
        $config = hikashop_config();
        $allowed = $config->get('allowedfiles');
        $imageHelper = hikashop_get('helper.image');

        $file_class = hikashop_get('class.file');
        $uploadPath = $file_class->getPath('product','');

        if (isset($lpcircleid) && !empty($lpcircleid)) {
            $db  = JFactory::getDBO();
            $querySelect = 'SELECT * FROM #__hikashop_category WHERE learningprovidercircleid='.(int)$lpcircleid;
            $db->setQuery($querySelect);
            $lpHikaCategory = $db->loadObjectList();

//            $query = "SELECT * FROM #__community_groups WHERE id=".(int)$lpcircleid;
//            $db->setQuery($query);
//            $lpcircle = $db->loadObjectList();

            require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
            $group = JTable::getInstance( 'Group' , 'CTable' );
            $group->load($lpcircleid);

            if (!empty($lpHikaCategory[0]->category_id) && !empty($group->id) ){
                $queryUpdate = "UPDATE #__hikashop_category SET category_name='".$group->name."', category_description='".$group->description."' WHERE category_id=".$lpHikaCategory[0]->category_id;
                $db->setQuery($queryUpdate);
                $db->query();

//                $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
//                $domainName = $_SERVER['HTTP_HOST'];
//                if (!empty($lpcircle[0]->avatar)) {
//                    $pic_url = $protocol.$domainName.'/courses/viewfile.php/'.$lpcircle[0]->avatar;
//                }
                $circle_avatar = $group->getOriginalAvatar();
                $pic_url = $circle_avatar;
                    $file_class->deleteFiles('category', $lpHikaCategory[0]->category_id, false);
                    $pic = JoomdleHelperContent::get_file ($pic_url);
                    $file = new stdClass();
                    $file->file_name = '';
                    $file->file_description = '';

                    $filename = basename ($pic_url);
                    $file_path = strtolower(JFile::makeSafe($filename));

                    if(!preg_match('#\.('.str_replace(array(',','.'),array('|','\.'),$allowed).')$#Ui',$file_path,$extension) || preg_match('#\.(php.?|.?htm.?|pl|py|jsp|asp|sh|cgi)$#Ui',$file_path)){
                        //$app->enqueueMessage(JText::sprintf( 'ACCEPTED_TYPE',substr($file_path,strrpos($file_path,'.')+1),$allowed), 'notice');
                        //continue;
                    }
                    $file_path= str_replace(array('.',' '),'_',substr($file_path,0,strpos($file_path,$extension[0]))).$extension[0];

                    file_put_contents ($uploadPath . $file_path,  $pic);

                    $imageHelper->resizeImage($file_path);
                    $imageHelper->generateThumbnail($file_path);

                    $file->file_path = $file_path;
                    $file->file_type = 'category';
                    $file->file_ref_id = $lpHikaCategory[0]->category_id;
                   $image_id = $file_class->save($file);
               
                $response['status'] = true;
                $response['message'] = 'Category updated';
                return $response;
        }
        }

        $response['status'] = false;
        $response['message'] = 'Category not updated';
        return $response;


    }
    /*
    * POST index.php?option=com_hikashop&ctrl=lpapi&task=getcategorylist
    */
    /* CHRIS: changed prviate to static */
    static function getcategorylist($courseid=''){
        $db  = JFactory::getDBO();
        $querySelect = 'SELECT * FROM #__hikashop_category WHERE category_parent_id=2';
        $db->setQuery($querySelect);
        $categories = $db->loadObjectList();
        $returncategory= array();

        if(!empty($courseid)){
             $query = 'SELECT * ' .
                    ' FROM #__hikashop_product' .
                    ' WHERE product_code =' . $db->Quote($courseid);
            $db->setQuery($query);
            $products = $db->loadObjectList();
            if(!empty($products)){
                      $query = 'SELECT * ' .
                    ' FROM #__hikashop_product_category' .
                    ' WHERE product_id =' . $db->Quote($products[0]->product_id);
            $db->setQuery($query);
            $productsCategory = $db->loadObjectList();

            $catArray=array();
                foreach ($productsCategory as $cat) {
                    array_push($catArray, $cat->category_id);
                }

            $varCat = implode(',', $catArray);
        }
        }
        if(!empty($categories)){
            foreach ($categories as $category) {
                $queryHead = 'SELECT * FROM #__hikashop_category WHERE category_parent_id='.$category->category_id;
                $db->setQuery($queryHead);
                $categoryHead = $db->loadObjectList();
                if(empty($categoryHead)){
                    if(!empty($varCat)){
                                $pos = strpos($varCat, $category->category_id);
                                if($pos || $pos===0){
                                    $category->isSelected=true;
                                    array_push($returncategory, $category);
                                }else{
                                     $category->isSelected=false;
                               array_push($returncategory,$category);
                                }
                            }else{
                                $category->isSelected=false;
                                array_push($returncategory,$category);
                            }
                    
                }else{
                    $querySelect = 'SELECT * FROM #__hikashop_category WHERE category_parent_id='.$category->category_id;
                    $db->setQuery($querySelect);
                    $categorySub = $db->loadObjectList();
                    if(!empty($categorySub)){
                        foreach ($categorySub as $sub) {
                            if(!empty($varCat)){

                                $pos = strpos($varCat, $sub->category_id);
                                if($pos || $pos===0){
                                    $sub->isSelected=true;
                                    array_push($returncategory, $sub);
                                }else{
                                    $sub->isSelected=false;
                                array_push($returncategory, $sub);
                                }
                            }else{
                                $sub->isSelected=false;
                                array_push($returncategory, $sub);
                            }
                            
                        }
                    }
                }
            }
        }
        return $returncategory;
    }

    /*
    * POST index.php?option=com_hikashop&ctrl=lpapi&task=publishcourse&courseid=9973&categorieslist=23,45,44&lpcircleid=212
    */
    /* CHRIS: changed private to static */
    static function publishcourse($courseid,$categorieslist,$lpcircleid){

        //CHRIS: temporarily commented causes error

        /*
        if (!$this->integration_enabled ()){
            return false;
        }
        */


//        $user = JFactory::getUser();
//        $username = $user->get('username');
//
//        if(!isset($username)){
//            $response['status']=false;
//            $response['message']='Unauthorize permission';
//            return $response;
//        }
        $app = JFactory::getApplication();
        $config = hikashop_config();
        $allowed = $config->get('allowedfiles');
        $imageHelper = hikashop_get('helper.image');

        $file_class = hikashop_get('class.file');
        $uploadPath = $file_class->getPath('product','');


        $params = JComponentHelper::getParams( 'com_joomdle' );
        $courses_category = $params->get( 'courses_category' );
        $categoryarray = explode(",", $categorieslist);
        $mergecategory = array_merge(array ($courses_category),$categoryarray);
        $sku=(int) $courseid;
        $db  = JFactory::getDBO();

            $query = 'SELECT product_code ' .
                    ' FROM #__hikashop_product' .
                    ' WHERE product_code =' . $db->Quote($sku);
            $db->setQuery($query);
            $products = $db->loadObjectList();

            /**
            *   
            *
            **/
            if(!empty($lpcircleid)){
                $querySelect = 'SELECT * FROM #__hikashop_category WHERE learningprovidercircleid='.(int)$lpcircleid;
                $db->setQuery($querySelect);
                $lpcategories = $db->loadObjectList();
                if(!empty($lpcategories)){
                    array_push($mergecategory,$lpcategories[0]->category_id);
                }
            }

            if (count ($products))
            {
                /* Product already on Hikashop, just publish it */
                $query = "UPDATE  #__hikashop_product SET product_published = '1' where product_code = ". $db->Quote($sku);
                $db->setQuery($query);
                if (!$db->query()) {
                    return JError::raiseWarning( 500, $db->getError() );
                 }
                 $updateStatus=LpApiController::updatecourse($sku,$mergecategory,"1");
                    if($updateStatus["status"]){
                            $response['status'] = true;
                            $response['message'] = 'Course updated.';
                            return $response;
                    }                
                $response['status'] = false;
                $response['message'] = 'Update Course Creation Error';
                return $response;
            }

            /* New product to add to Hikashop */
           //Course
            $course_info = JoomdleHelperContent::getCourseInfo ($sku);
            $name = $course_info['fullname'];
            $desc = $course_info['summary'];
            if (array_key_exists ('price', $course_info))
                $cost = $course_info['price'];
            else $cost = 0;
            if (array_key_exists ('currency', $course_info))
                $currency = $course_info['currency'];
            else $currency = '';

            $product_class = hikashop_get('class.product');
            $element = new JObject ();
            $element->categories = $mergecategory; //array ($courses_category);
            $element->related = array();
            $element->options = array();
            $element->product_name = $name;
            $element->product_description = $desc;
            $element->product_code = $sku;
            $element->product_published = 1;
            $element->product_max_per_order = 1;
            $element->lesson_objective=$course_info['learningoutcomes'];
            $element->target_audience=$course_info['targetaudience'];
            $element->estimated_duration=empty($course_info['duration']) ? '' : $course_info['duration'];
			$element->course_validity=$course_info['enrolperiod'];

            $query = "SELECT category_id FROM #__hikashop_category WHERE category_namekey='default_tax'";
            $db->setQuery($query);
            $tax_id = $db->loadResult();
            if ($tax_id)
            {
                $element->product_tax_id = $tax_id;

                $query = "SELECT tax_namekey FROM #__hikashop_taxation WHERE category_namekey='default_tax' and taxation_published=1";
                $db->setQuery($query);
                $tax_namekey = $db->loadResult();

                $query = "SELECT tax_rate FROM #__hikashop_tax WHERE tax_namekey=".$db->Quote($tax_namekey);
                $db->setQuery($query);
                $tax_rate = $db->loadResult();

                $div = $tax_rate + 1;
                $price_without_tax = $cost / $div;
                $cost = $price_without_tax;
            }
            $element->prices[0] = new JObject ();
            $element->prices[0]->price_value = $cost;

            $query = "SELECT currency_id FROM #__hikashop_currency WHERE currency_code = '$currency'";
            $db->setQuery($query);
            $currency_id = $db->loadResult();
            $element->prices[0]->price_currency_id = $currency_id;
            $element->prices[0]->price_min_quantity = 0;

            $status = $product_class->save($element);


            // Add images as product media
            // foreach ($course_info['summary_files'] as $file)
            // {
            if (!empty($course_info) || !empty($prog)) {

                if (!empty($course_info)) {
                    $pic_url = $course_info['filepath']. rawurlencode($course_info['filename']);
                }

                $pic = JoomdleHelperContent::get_file ($pic_url);

                $file = new stdClass();
                $file->file_name = '';
                $file->file_description = '';

                $filename = basename ($pic_url);
                $file_path = strtolower(JFile::makeSafe($filename));
                if(!preg_match('#\.('.str_replace(array(',','.'),array('|','\.'),$allowed).')$#Ui',$file_path,$extension) || preg_match('#\.(php.?|.?htm.?|pl|py|jsp|asp|sh|cgi)$#Ui',$file_path)){
                    $app->enqueueMessage(JText::sprintf( 'ACCEPTED_TYPE',substr($file_path,strrpos($file_path,'.')+1),$allowed), 'notice');
                    continue;
                }
                $file_path= str_replace(array('.',' '),'_',substr($file_path,0,strpos($file_path,$extension[0]))).$extension[0];
                file_put_contents ($uploadPath . $file_path,  $pic);

                $imageHelper->resizeImage($file_path);
                $imageHelper->generateThumbnail($file_path);

                $file->file_path = $file_path;
                $file->file_type = 'product';
                $file->file_ref_id = $status;

                $image_id = $file_class->save($file);
                $element->images[] = $image_id;
            }

            LpApiController::updateAlias($courseid);
            
            if ($status)
            {
                $product_class->updateCategories($element,$status);
                $product_class->updatePrices($element,$status);
                $product_class->updateFiles($element,$status,'images');
            }

        $response['status'] = true;
        $response['message'] = 'Course created';
        return $response;
    }

    // POST {domain}/index.php?option=com_hikashop&ctrl=lpapi&task=updatecourse&courseid=9975
    static function updatecourse($courseid, $cateogrylist='',$publish=''){

        $app = JFactory::getApplication();
        $config = hikashop_config();

        $allowed = $config->get('allowedfiles');
        $imageHelper = hikashop_get('helper.image');

        $file_class = hikashop_get('class.file');
        $uploadPath = $file_class->getPath('product','');

        $db  = JFactory::getDBO();

        $query = 'SELECT * ' .
                    ' FROM #__hikashop_product' .
                    ' WHERE product_code =' . $db->Quote($courseid);
        $db->setQuery($query);
        $products = $db->loadObjectList();

        $course_info = JoomdleHelperContent::getCourseInfo ($courseid);

        if(empty($course_info)){
            $response['status'] = false;
            $response['message'] = "This course doesn't exist in system.";
            return $response;
        }

        if(!empty($products)){
            

            $productClass = hikashop_get('class.product');

            $product=$productClass->get($products[0]->product_id);
            $product->product_name = $course_info['fullname'];
            $product->product_description = $course_info['summary'];
            $product->product_code = $courseid;
            $product->product_max_per_order = 1;
            $product->lesson_objective=$course_info['learningoutcomes'];
            $product->target_audience=$course_info['targetaudience'];
            $product->estimated_duration=empty($course_info['duration']) ? '' : $course_info['duration'];

            unset($product->alias);
       

            if(!empty($cateogrylist)){
                $product->categories = $cateogrylist;
            }

            if(!empty($publish) && $publish ==1 ){
                $product->product_published = 1;
            }

            if (array_key_exists ('price', $course_info))
                $cost = $course_info['price'];
            else $cost = 0;
            if (array_key_exists ('currency', $course_info))
                $currency = $course_info['currency'];
            else $currency = '';
            $query = "SELECT category_id FROM #__hikashop_category WHERE category_namekey='default_tax'";
            $db->setQuery($query);
            $tax_id = $db->loadResult();
            if ($tax_id)
            {
                $product->product_tax_id = $tax_id;

                $query = "SELECT tax_namekey FROM #__hikashop_taxation WHERE category_namekey='default_tax' and taxation_published=1";
                $db->setQuery($query);
                $tax_namekey = $db->loadResult();

                $query = "SELECT tax_rate FROM #__hikashop_tax WHERE tax_namekey=".$db->Quote($tax_namekey);
                $db->setQuery($query);
                $tax_rate = $db->loadResult();

                $div = $tax_rate + 1;
                $price_without_tax = $cost / $div;
                $cost = $price_without_tax;
            }
            $product->prices[0] = new JObject ();
            $product->prices[0]->price_value = $cost;
            $productClass->updatePrices($product,$product->product_id);

            $query = "SELECT currency_id FROM #__hikashop_currency WHERE currency_code = '$currency'";
            $db->setQuery($query);
            $currency_id = $db->loadResult();
            $product->prices[0]->price_currency_id = $currency_id;
            $product->prices[0]->price_min_quantity = 0;


          
             if (!empty($course_info)) {
                if (isset($course_info['filename'])) {

                        $query = "SELECT * FROM #__hikashop_file WHERE file_ref_id =".$product->product_id;
                        $db->setQuery($query);
                        $load_files = $db->loadObjectList();


                                 $file_class->deleteFiles('product', $product->product_id, false);
                                    $pic_url = $course_info['filepath'] . rawurlencode($course_info['filename']);
                                    $pic = JoomdleHelperContent::get_file($pic_url);
                                    $file = new stdClass();
                                    $file->file_name = '';
                                    $file->file_description = '';

                                    $filename = basename($pic_url);
                                    $file_path = $product->product_id.strtolower(JFile::makeSafe($filename));
                                    if (!preg_match('#\.(' . str_replace(array(',', '.'), array('|', '\.'), $allowed) . ')$#Ui', $file_path, $extension) || preg_match('#\.(php.?|.?htm.?|pl|py|jsp|asp|sh|cgi)$#Ui', $file_path)) {
                                        $app->enqueueMessage(JText::sprintf('ACCEPTED_TYPE', substr($file_path, strrpos($file_path, '.') + 1), $allowed), 'notice');
                                        continue;
                                    }
                                    $file_path = str_replace(array('.', ' '), '_', substr($file_path, 0, strpos($file_path, $extension[0]))) . $extension[0];
                                    file_put_contents($uploadPath . $file_path, $pic);

                                    $imageHelper->resizeImage($file_path);
                                    $imageHelper->generateThumbnail($file_path);

                                    $file->file_path = $file_path;
                                    $file->file_type = 'product';
                                    $file->file_ref_id = $product->product_id;

                                    $image_id = $file_class->save($file);
                                    $product->images[0] = $image_id;
    
                }



                $productClass->updateFiles($product, $product->product_id, 'images');
            }

            $status=$productClass->save($product);
            $productClass->updateCategories($product,$status);

            LpApiController::updateAlias($courseid);


            $result['status'] =true;
            $result['message'] ='Updated product successfully';

            return $result;

        }

    }
    
    static function updateAlias($courseid){
        $db  = JFactory::getDBO();
        $query = 'SELECT *' .
                    ' FROM #__hikashop_product' .
                    ' WHERE product_code =';
        $query .= $db->Quote((int)$courseid);
        $db->setQuery($query);
        $productList = $db->loadObjectList();

        if(!empty($productList)){
            if(!empty($courseid)){
                $productClass = hikashop_get('class.product');
                $product = $productClass->get((int)$element->product_id);
                $productList[0]->alias = JFilterOutput::stringURLSafe($productList[0]->product_name);
                $productList[0]->product_alias = $productList[0]->alias;
                unset($productList[0]->alias);
                 $status=$productClass->save($productList[0]);
            }
        }
        
    }
    /*
    * POST index.php?option=com_hikashop&ctrl=lpapi&task=unpublishcourse&categoryid=433&courseid=9973
    */
    static function unpublishcourse($categoryid,$courseid){

        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        $user = JFactory::getUser();
        $username = $user->get('username');
        $action = 'unpublish';

        if(!isset($username)){
            $response['status']=false;
            $response['message']='Unauthorize permission';
            return $response;
        }

        $sku=(int) $courseid;
        $db  = JFactory::getDBO();

        try{

            $unpublishMoodle = JoomdleHelperContent::call_method('publish_unpublish_course',(int)$categoryid,(int)$courseid,$action,$username);
            $query = "UPDATE  #__hikashop_product SET product_published = '0' where product_code = ". $db->Quote($sku);
            $db->setQuery($query);

            if($unpublishMoodle['status']){

                if (!$db->query()) {
                    $response['status'] = false;
                    $response['message'] = 'Error during unpublish course';
                    return $response;
                }
                $response['status'] = true;
                $response['message'] = 'course unpublished';
                return $response;
            }


        }catch (Exception $e){
            $response['status'] = false;
            $response['message'] = 'Error during unpublish course';
            return $response;
        }

        $response['status'] = false;
        $response['message'] = 'Error during unpublish course';
        return $response;
  
    }

    /*
    * POST index.php?option=com_hikashop&ctrl=lpapi&task=removecourse&courseid=9973&categoryid=415
    */
    static function removecourse($courseid, $categoryid) {
        // if (!$this->integration_enabled ())
        //     return false;
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        require_once( JPATH_ADMINISTRATOR.'/components/com_hikashop/helpers/helper.php' );
        $db           = JFactory::getDBO();

        $user = JFactory::getUser();
        $username = $user->get('username');

        if (!isset($username)) {
            $response['status']=false;
            $response['message']='Unauthorize permission';
            return $response;
        }

        $ids = array();
        $sku = (int)$courseid;
        $query = 'SELECT product_id' .
                    ' FROM #__hikashop_product' .
                    ' WHERE product_code =';
        $query .= $db->Quote($sku);
        $db->setQuery($query);
        $product_id = $db->loadResult();
        /* Product not on Hikashop, nothing to do */
        if (!$product_id) {
            $response['status']=false;
            $response['message']='This product does not exist.';
            return $response;
        }
        $ids[] = $product_id;

        try {
            if (count ($product_id)) {
                /* Deleting course from Hikashop */
                $product_class = hikashop_get('class.product');
                $num = $product_class->delete ($ids);
                if ($num != 1) {
                    $response['status'] = false;
                    $response['message'] = 'Error during delete course';
                    return $response;
                } else {
                     $responseMoodle = JoomdleHelperContent::call_method('remove_course_platform', (int)$courseid, $username, (int)$categoryid);
                    if ($responseMoodle['status']) {
                        $response['status'] = true;
                        $response['message'] = 'course deleted';
                        return $response;
                    } else {
                        $response['status'] = false;
                        $response['message'] = 'Moodle course has not been deleted yet';
                        return $response;
                     }
                }
            } else {
                $response['status'] = false;
                $response['message'] = 'Course not found';
                return $response;
            }
        }
        catch (Exception $e) {
            $response['status'] = false;
            $response['message'] = 'Error during delete course';
            return $response;
        }
    }

    static function getLPHikaCategory($lpcircleid) {
        $db  = JFactory::getDBO();
        $querySelect = 'SELECT * FROM #__hikashop_category WHERE learningprovidercircleid='.(int)$lpcircleid;
        $db->setQuery($querySelect);
        $lpHikaCategory = $db->loadObjectList();
        if (!empty($lpHikaCategory)) {
            return $lpHikaCategory[0];
        } else return false;
    }

    static function getHikashopCourses($lpcircleid,$ispublished=0){

        $user = JFactory::getUser();
        $username = $user->get('username');

        $db  = JFactory::getDBO();
        if(!empty($lpcircleid)){
            $querySelect = 'SELECT * FROM #__hikashop_category WHERE learningprovidercircleid='.(int)$lpcircleid;
            $db->setQuery($querySelect);
            $categories = $db->loadObjectList();
            if(!empty($categories)){

                if(1==$ispublished){
                    $queryToSelect=$db->getQuery(true)->select('*')->from('par_hikashop_product AS product')->join('INNER','par_hikashop_product_category AS category ON product.product_id=category.product_id')->where('category.category_id='.$categories[0]->category_id)->where("product.product_published=1");
                }else{
                    $queryToSelect=$db->getQuery(true)->select('*')->from('par_hikashop_product AS product')->join('INNER','par_hikashop_product_category AS category ON product.product_id=category.product_id')->where('category.category_id='.$categories[0]->category_id);
                }


                $db->setQuery($queryToSelect);
                $lp_publish_courses = $db->loadObjectList();
                if(!empty($lp_publish_courses)){
                    return $lp_publish_courses;
                }
            }
        }
    }

    static function isCourseInHika($product_code){
        $db  = JFactory::getDBO();
        $query = 'SELECT *' .
                    ' FROM #__hikashop_product' .
                    ' WHERE product_code =';
        $query .= $db->Quote($product_code);
        $db->setQuery($query);
        $product = $db->loadObjectList();

        if(!empty($product)){
            return $product[0];
        }
        return;
    }

    static function removeCourseFromHikashop($product_code) {
        $db  = JFactory::getDBO();
        $query = 'DELETE ' .
                    ' FROM #__hikashop_product' .
                    ' WHERE product_code =';
        $query .= $db->Quote($product_code);
        $db->setQuery($query);
        $res = $db->query();

        if ($res) {
            $response['status'] = true;
            $response['message'] = 'Remove course successfully.';
            return $response;
        } else {
            $response['status'] = false;
            $response['message'] = 'Course not found';
            return $response;
        }
    }
        static function countcoursecart($user_id){
        $db  = JFactory::getDBO();
        $query = 'SELECT *' .
                    ' FROM #__hikashop_cart' .
                    ' c INNER JOIN  `#__hikashop_cart_product` p '.
                    ' ON c.cart_id = p.cart_id'.
                        ' WHERE cart_type = \'cart\' and user_id =';
        $query .= $db->Quote((int)$user_id);
        $db->setQuery($query);
        $count_course = $db->loadObjectList();
        return $count_course;
    }

}
