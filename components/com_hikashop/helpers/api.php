<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('_JEXEC') or die('Restricted access');

class ApiController extends hikashopController {
    
    function __construct(&$params, $config) {
        parent::__construct($config);
    }
    public function apiRouter($api, $action, $data) {
        $jinput = JFactory::getApplication()->input;
        $response = array();
        if($api == 'api') {
            if(empty($action)) {
                $response['status'] = false;
                $response['message'] = 'Request not found';
            }
            else {
                if($action == 'listcategory') {
                    $response = $this->getListCategory();
                }
                elseif($action == 'product') {
                    $type = $jinput->get('type', '', 'POST');
                    $page = $jinput->get('page', 0, 'POST');
                    $limit = $jinput->get('limit', 0, 'POST');
                    $category = $jinput->get('categoryid', 0, 'POST');
                    $category_menu = $jinput->get('category_menu', '', 'POST');
                    $username = $jinput->get('username', '', 'POST');
                    $response = $this->getProducts($type, $page, $limit, $category, $category_menu, $username);
                }
                elseif($action == 'providers') {
                    $page = $jinput->get('page', 0, 'POST');
                    $limit = $jinput->get('limit', 0, 'POST');
                    $response = $this->getProviders($page, $limit);
                }
                elseif($action == 'search') {
                    $page = $jinput->get('page', 0, 'POST');
                    $limit = $jinput->get('limit', 0, 'POST');
                    $keywords = $jinput->get('keyword', 0, 'POST');
                    $response = $this->searchLearning($page, $limit, $keywords);
                }
                elseif($action == 'list_product_lp') {
                    $page = $jinput->get('page', 0, 'POST');
                    $limit = $jinput->get('limit', 0, 'POST');
                    $circle_lp_id = $jinput->get('circle_lp_id', 0, 'POST');
                    $response = $this->getProductLP($circle_lp_id, $page, $limit);
                } elseif($action == 'add_to_cart') {
                    $product_id = $jinput->get('product_id', 0, 'POST');
                    $username = $jinput->get('username', '', 'POST');
                    $response = $this->addToCart($product_id, $username);
                } elseif($action == 'view') {
                    $product_id = $jinput->get('product_id', 0, 'POST');
                    $response = $this->productDetail($product_id);
                }
            }
        } else {
            $response['status'] = false;
            $response['message'] = 'Request not found';
        }
        
        // return data
        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($response);
        exit();
    }
    private function getHikaMenu($alias) {
        $database = JFactory::getDBO();
        $querys = "SELECT  category_id as id, category_name as title, category_parent_id as parent_id, category_alias as alias, category_namekey as params "
                . "FROM #__hikashop_category ";
        if(!$alias) {
            $querys .= " WHERE category_parent_id = 2 ORDER BY id";
        } else {
            $querys .= " WHERE alias = '$alias'";
        }
        $database->setQuery($querys);
        $data = $database->loadObjectList();
        return $data;
    }
    private function getListCategory() {
        $data = $this->getHikaMenu();
        for ($i = 0; $i <= count($data); $i++) {
            if($data[$i]->alias == 'featured') {
                unset($data[$i]);
            }
            if($data[$i]->alias == 'providers') {
                unset($data[$i]);
            }
            if($data[$i]->alias == 'categories') {
                unset($data[$i]);
            }
            
        }
        $tree = $this->buildTree($data);
        $response->status = true;
        $response->message = 'category loaded.';
        $response->category = $tree;
        return $response;
    }
    function buildTree($items) {
        $db = JFactory::getDBO();
        foreach($items as $item) {
            $querySelect = 'SELECT category_id as id, category_name as title, category_parent_id as parent_id, category_alias as alias, category_namekey as params '
                    . 'FROM #__hikashop_category WHERE category_parent_id='.$item->id;
            $db->setQuery($querySelect);
            $categorySub = $db->loadObjectList();
            if(!empty($categorySub)){
                $item->childs = $categorySub;
    }
        }
        return $items;
    }
    
    private function getProviders($page, $limit) {
        $config =& hikashop_config();
        
	$this->assignRef('config', $config);
        $module = hikashop_get('helper.module');
        $module->initialize($this);
        $this->paramBase.='_'.$this->params->get('main_div_name');
        
        $hikamenu = $this->getHikaMenu();
        foreach ($hikamenu as $menu) {
            if($menu->alias == 'providers') {
                $data = $menu->params;
            }
        }
        $params = json_decode($data);
        $param = $params->hk_category;
        
        $filters = array();
        $app = JFactory::getApplication();
        $pageInfo = new stdClass();
        $pageInfo->filter = new stdClass();
        $pageInfo->filter->order = new stdClass();
        $pageInfo->limit = new stdClass();
        $catData = null;
        $database = JFactory::getDBO();
        $defaultParams = $config->get('default_params');
        if (empty($defaultParams['links_on_main_categories']))
            $defaultParams['links_on_main_categories'] = 1;
        if (isset($param) && is_object($param)) {
            $categoryId = (int) $param->category;
            if ($categoryId > 0) {
                $categoryClass = hikashop_get('class.category');
                $cat = $categoryClass->get($categoryId);
                if ($cat->category_type == 'manufacturer')
                    $this->params->set('content_type', 'manufacturer');
            }
            if (!empty($param->category))
                $this->params->set('selectparentlisting', (int) $data->hk_category->category);
        }
        // hard code for api
        $pageInfo->filter->cid = array(22);
        $category_type = 'product';
        if (!empty($pageInfo->filter->cid)) {
            $acl_filters = array();
            hikashop_addACLFilters($acl_filters, 'category_access');
            if (!empty($acl_filters)) {
                if (!is_array($pageInfo->filter->cid)) {
                    $category = hikashop_get('class.category');
                    $catData = $category->get($pageInfo->filter->cid);
                    if (!empty($catData->category_type))
                        $category_type = $catData->category_type;
                    $pageInfo->filter->cid = array($database->Quote($pageInfo->filter->cid));
                }
                $acl_filters[] = 'category_type=\'' . $category_type . '\'';
                $acl_filters[] = 'category_id IN (' . implode(',', $pageInfo->filter->cid) . ')';
                $query = 'SELECT category_id FROM ' . hikashop_table('category') . ' WHERE ' . implode(' AND ', $acl_filters);
                $database->setQuery($query);
                if (!HIKASHOP_J25) {
                    $pageInfo->filter->cid = $database->loadResultArray();
                } else {
                    $pageInfo->filter->cid = $database->loadColumn();
                }
            }
        }
        if($page == 1) {
            $position = 0;
        } else {
            $position = $page * $limit;
        }
        $order = ' ORDER BY a.category_ordering ASC';
        $class = hikashop_get('class.category');
		$class->parentObject =& $this;
        $rows = $class->getChildren($pageInfo->filter->cid,0,$filters,$order,$position,$limit,true);
        $path_image = $app->getCfg('wwwrootfile').'/com_hikashop/upload/';
//        print_r($rows);die;
        if($rows) {
            $ids = array();
            $productClass = hikashop_get('class.product');
            foreach ($rows as $key => $row) {
                if (!is_null($row->category_id)) {
                    $ids[] = $row->category_id;
                    $productClass->addAlias($rows[$key]);
                }
            }
            if (empty($ids))
                $ids = array(0);

            $queryImage = 'SELECT * FROM ' . hikashop_table('file') . ' WHERE file_ref_id IN (' . implode(',', $ids) . ') AND file_type=\'product\' ORDER BY file_ref_id ASC, file_ordering ASC, file_id ASC';
            $database->setQuery($queryImage);
            $images = $database->loadObjectList();
            
            $data = array();
            foreach ($rows as $row) {
                $categories = new stdClass();
                $categories->category_id = $row->category_id;
                $categories->category_parent_id = $row->category_parent_id;
                $categories->category_name = $row->category_name;
                $categories->category_description = $row->category_description;
                $categories->category_description_nohtml = strip_tags($row->category_description);
                $categories->category_published = $row->category_published;
                $categories->category_alias = $row->category_alias;
                // image
                $image_path = '';
                if (!empty($images)) {
                    foreach ($images as $image) {
                        if ($row->category_id != $image->file_ref_id)
                            continue;

                        if (isset($row->category_id)) {
                            foreach (get_object_vars($image) as $key => $name) {
                                $image_path = $path_image.$image->file_path;
                            }
                        } else {
                            if (empty($row->images))
                                $image_path = '';
                        }
                    }
                }
                $categories->category_iamge = $image_path;
                $categories->lp_circle_id = (int)$row->learningprovidercircleid;
                
                $lp_circle_detail = $this->getProviderCircle($row->learningprovidercircleid);
                $categories->owner = '';
                $categories->ismember = false;
                if($lp_circle_detail) {
                    if(isset($lp_circle_detail->ownerid)) {
                        $user = JFactory::getUser($lp_circle_detail->ownerid);
                        $categories->owner = $user->name;
                    }
                    if(isset($lp_circle_detail->approved) == 1) {
                        $categories->ismember = true;
                    }
                }
                $data[] = $categories;
            }
            
            $response->status = true;
            $response->message = 'Providers loaded.';
            $response->providers = $data;
        } else {
            $response->status = false;
            $response->message = 'Providers load failure.';
            $response->providers = $data;
        }
        return $response;

    }
    private function getProducts($type_product, $page, $limit, $category, $categories_menu, $username = '') {
	$app = JFactory::getApplication();
        $database = JFactory::getDBO();
        $config = & hikashop_config();
        $select = '';
        $table = 'b';
        $params = '';
        $menu_alias = '';
        $type_arr = array('featured', 'product_provider', 'product_category');
        if(!in_array($type_product, $type_arr)) {
            $response->status = false;
            $response->message = 'Type not found.';
            return $response;
        }
//        if($type_product == 'product_category') {
//            $menu_alias = $categories_menu;
//        }
        $hikamenu = $this->getHikaMenu($menu_alias);
        foreach ($hikamenu as $menu) {
            $params = $menu->params;
            if($menu->alias == 'featured' && $type_product == 'featured') {
                $params_feature = $menu->params;
                $feature_menu = json_decode($params_feature);
                $featured = $feature_menu->hk_product;
            }
        }
        $data_json = json_decode($params);
        $data = $data_json->hk_product;
        
        $pageInfo = new stdClass();
        $pageInfo->filter = new stdClass();
        $pageInfo->filter->order = new stdClass();
        $pageInfo->limit = new stdClass();
        $filters = array('b.product_published=1');
        
        $categoryClass = hikashop_get('class.category');
	$element = $categoryClass->get(reset($pageInfo->filter->cid),true);

            if ($pageInfo->filter->order->value == 'b.ordering') {
                $pageInfo->filter->order->value = 'a.ordering';
            }
            $b = hikashop_table('product_category') . ' AS a LEFT JOIN ';
            $a = hikashop_table('product') . ' AS b';
            $on = ' ON a.product_id=b.product_id';
        
        $categoryClass->parentObject = & $this;
        $categoryClass->type = $type;
        if($category == 0 && $type_product != 'product_provider') {
            if($type_product == 'featured') {
                $sql1 = "SELECT category_id FROM ". hikashop_table('category') . " WHERE category_alias = 'fe'";
                $database->setQuery($sql1);
                $featured_info = $database->loadObjectList();
                $categoryid = $featured_info[0]->category_id;
            } else {
                $categoryid = $data->category;
            }
            if($categoryid) {
                $pageInfo->filter->cid = array($categoryid);
            } else {
                $pageInfo->filter->cid = array(82);    
            }
        } else {
            $pageInfo->filter->cid = array($category);
        }
        
        if (!empty($pageInfo->filter->cid)) {
            $acl_filters = array();
            hikashop_addACLFilters($acl_filters, 'category_access');
            if (!empty($acl_filters)) {
                if (!is_array($pageInfo->filter->cid)) {
                    $pageInfo->filter->cid = array($database->Quote($pageInfo->filter->cid));
                }
                $acl_filters[] = 'category_id IN (' . implode(',', $pageInfo->filter->cid) . ')';
                $query = 'SELECT category_id FROM ' . hikashop_table('category') . ' WHERE ' . implode(' AND ', $acl_filters);
                $database->setQuery($query);
                if (!HIKASHOP_J25) {
                    $pageInfo->filter->cid = $database->loadResultArray();
                } else {
                    $pageInfo->filter->cid = $database->loadColumn();
                }
            }
        }
        $filters[]='b.product_type = \'main\'';
        if (empty($select)) {
            $parentCategories = implode(',', $pageInfo->filter->cid);
            $catName = 'a.category_id';
            $type = 'product';

            if (!empty($element->category_type) && $element->category_type == 'manufacturer') {
                if ($pageInfo->filter->order->value == 'a.ordering' || $pageInfo->filter->order->value == 'b.ordering') {
                    $pageInfo->filter->order->value = 'b.product_name';
                }
                $type = 'manufacturer';
                $catName = 'b.product_manufacturer_id';
                $b = '';
                $a = hikashop_table('product') . ' AS b';
                $on = '';
                $select = 'SELECT DISTINCT b.*';
            } else {
                if ($pageInfo->filter->order->value == 'b.ordering') {
                    $pageInfo->filter->order->value = 'a.ordering';
                }
                $b = hikashop_table('product_category') . ' AS a LEFT JOIN ';
                $a = hikashop_table('product') . ' AS b';
                $on = ' ON a.product_id=b.product_id';
                $select = 'SELECT DISTINCT b.*';
            }
            
            if ($pageInfo->filter->cid) {
                if (!empty($parentCategories) && $parentCategories != '0')
                    $filters[] = $catName . ' IN (' . $parentCategories . ')';
            }else {
                $categoryClass->parentObject = & $this;
                $categoryClass->type = $type;

                $children = $categoryClass->getChildren($pageInfo->filter->cid, true, array(), '', 0, 0);
                foreach ($children as $child) {
                    $catChild[] = $child->category_id;
                }
                $catChildStr = implode(',', $catChild);
                $filter = $catName . ' IN (';
                $filter .= $catChildStr;
//                foreach ($children as $child) {
//                    $filter .= $child->category_id . ',';
//                }
                $filters[] = $filter . $parentCategories . ')';
            }
        }
        $order = ' ORDER BY a.ordering ASC';
        hikashop_addACLFilters($filters,'product_access','b');
        $select2='';
        
        JPluginHelper::importPlugin('hikashop');
        $dispatcher = JDispatcher::getInstance();
        $dispatcher->trigger('onBeforeProductListingLoad', array(& $filters, & $order, & $this, & $select, & $select2, & $a, & $b, & $on));

        $translationFilter = '';
        if (isset($filters['translation'])) {
            $translationFilter = ' OR ' . $filters['translation'] . ' ';
            unset($filters['translation']);
        }
        
        $query = $select2.' FROM '.$b.$a.$on.' WHERE '.implode(' AND ',$filters).$translationFilter.$order;
        
        $position = ($page - 1) * $limit;
        $database->setQuery($select.$query,(int)$position,(int)$limit);        
        $rows = $database->loadObjectList(); 
        $datas = array();
        if($rows) {
            $ids = array();
		$productClass = hikashop_get('class.product');
                foreach ($rows as $key => $row) {
                    if (!is_null($row->product_id)) {
                        $ids[] = $row->product_id;
                        $productClass->addAlias($rows[$key]);
                    }
                }
                if (empty($ids))
                    $ids = array(0);

                $queryImage = 'SELECT * FROM ' . hikashop_table('file') . ' WHERE file_ref_id IN (' . implode(',', $ids) . ') AND file_type=\'product\' ORDER BY file_ref_id ASC, file_ordering ASC, file_id ASC';
                $database->setQuery($queryImage);
                $images = $database->loadObjectList();
                $path_image = $app->getCfg('wwwrootfile').'/com_hikashop/upload/';
                
            if(empty($username)) {    
            $user = JFactory::getUser();    
            } else {
                jimport('joomla.user.helper');
                $user_id = JUserHelper::getUserId($username);                
                $user = JFactory::getUser($user_id);
            }
            
            foreach ($rows as $k => $row) {
                $product = new stdClass();
                // image
                $image_path = '';
                if (!empty($images)) {
                    foreach ($images as $image) {
                        if ($row->product_id != $image->file_ref_id)
                            continue;

                        if (!isset($row->file_ref_id)) {
                            foreach (get_object_vars($image) as $key => $name) {
                                $rows[$k]->$key = $name;
                                $image_path = $path_image.$image->file_path;
                            }
                        } else {
                            if (empty($row->images))
                                $row->images = array();
                                $row->images[] = $image;
                                $image_path = '';
                        }
                    }
                }
                if (!isset($rows[$k]->file_name)) {
                    $rows[$k]->file_name = $row->product_name;
                }
                
                // get price
                $queryPrice = 'SELECT * FROM ' . hikashop_table('price') . ' WHERE price_product_id = '.$row->product_id;
                $database->setQuery($queryPrice);
                $price = $database->loadObjectList();
                
                $courseid = explode('_', $row->product_code);
                $product->product_is_learner = false;
                if (count($courseid) < 1) {
                    $check_learner = JoomdleHelperContent::call_method('check_learner', $row->product_code, $username);
                    if ($check_learner) {                        
                        $product->product_is_learner = true;
                    }
                }
                
                $already_cart = $this->productAlreadyCart($row->product_id, $user->id);
//                print_r($already_cart);die;
                $product->product_id = $row->product_id;
                $product->product_parent_id = $row->product_parent_id;
                $product->product_name = $row->product_name;
                $product->product_description = $row->product_description;
                $product->product_description_nohtml = strip_tags($row->product_description);
                $product->product_image = $image_path;
                $product->product_quantity = $row->product_quantity;
                $product->product_code = $row->product_code;
                $product->product_price = $price[0]->price_value ? $price[0]->price_value : 0;
                $product->product_published = $row->product_published;
                $product->product_alias = $row->product_alias;
                $product->product_duration = $row->estimated_duration;
                $product->product_sales = $row->product_sales;
                $product->product_already_added_cart = false;
                if($already_cart) {
                    $product->product_already_added_cart = true;
                }
                $datas[] = $product;
            }
            $response->status = true;
            $response->message = 'Featured loaded.';
            $response->featured = $datas;
        } else {
            $response->status = FALSE;
            $response->message = 'Sorry, no results are found.';
            $response->featured = $datas;
        }
        return $response;
    }
    private function searchLearning($page, $limit, $keyword) {
        $app = JFactory::getApplication();
        $database = JFactory::getDBO();
        $query = "SELECT product_id, product_parent_id, product_name, product_description, product_code, product_created, product_sales, product_alias, estimated_duration, product_published, product_type"
                . " FROM " .  hikashop_table('product'). " WHERE  product_name LIKE '%$keyword%' OR product_code LIKE '%$keyword%' AND product_published=1 AND product_type = 'main'"
                . " ORDER BY product_id ASC";
        
        
        $position = ($page - 1) * $limit;
        
        $database->setQuery($query,(int)$position,(int)$limit);
        $rows = $database->loadObjectList();
        if($rows) {
            $ids = array();
		$productClass = hikashop_get('class.product');
                foreach ($rows as $key => $row) {
                    if (!is_null($row->product_id)) {
                        $ids[] = $row->product_id;
                        $productClass->addAlias($rows[$key]);
                    }
                }
                if (empty($ids))
                    $ids = array(0);

                $queryImage = 'SELECT * FROM ' . hikashop_table('file') . ' WHERE file_ref_id IN (' . implode(',', $ids) . ') AND file_type=\'product\' ORDER BY file_ref_id ASC, file_ordering ASC, file_id ASC';
                $database->setQuery($queryImage);
                $images = $database->loadObjectList();
                $path_image = $app->getCfg('wwwrootfile').'/com_hikashop/upload/';
            $datas = array();
            foreach ($rows as $k => $row) {
                $product = new stdClass();
                // image
                $image_path = '';
                if (!empty($images)) {
                    foreach ($images as $image) {
                        if ($row->product_id != $image->file_ref_id)
                            continue;

                        if (!isset($row->file_ref_id)) {
                            foreach (get_object_vars($image) as $key => $name) {
                                $rows[$k]->$key = $name;
                                $image_path = $path_image.$image->file_path;
                            }
                        } else {
                            if (empty($row->images))
                                $row->images = array();
                                $row->images[] = $image;
                                $image_path = '';
                        }
                    }
                }
                if (!isset($rows[$k]->file_name)) {
                    $rows[$k]->file_name = $row->product_name;
                }
                $product->product_id = $row->product_id;
                $product->product_parent_id = $row->product_parent_id;
                $product->product_name = $row->product_name;
                $product->product_description = $row->product_description;
                $product->product_description_nohtml = strip_tags($row->product_description);
                $product->product_image = $image_path;
                $product->product_quantity = $row->product_quantity;
                $product->product_code = $row->product_code;
                $product->product_price = 0;
                $product->product_published = $row->product_published;
                $product->product_alias = $row->product_alias;
                $product->product_duration = $row->estimated_duration;
                $product->product_sales = $row->product_sales;
                $datas[] = $product;
            }
            $response->status = true;
            $response->message = 'Search results.';
            $response->featured = $datas;
        } else {
            $response->status = FALSE;
            $response->message = 'No results is found.';
            $response->featured = $datas;
        }
        return $response;
    }
    
    public function assignRef($key, &$val)
	{
		if (is_string($key) && substr($key, 0, 1) != '_')
		{
			$this->$key = &$val;
			return true;
		}

		return false;
	}
        
  public function getProductLP($circle_lp_id, $page, $limit) {
        $app = JFactory::getApplication();
        $database = JFactory::getDBO();
        $config = & hikashop_config();
        $category = $this->getLPcircleCategory($circle_lp_id);
        if($category->category_id) {
        $select = 'SELECT b.product_id, b.product_parent_id, b.product_name, b.product_description, b.product_code, b.product_created, b.product_sales, b.product_alias, b.estimated_duration, b.product_published, b.product_type';
        $b = hikashop_table('product_category') . ' AS a LEFT JOIN ';
        $a = hikashop_table('product') . ' AS b';
        $on = ' ON a.product_id=b.product_id';
        $where = ' WHERE a.category_id = '.$category->category_id. ' AND b.product_published=1';
        $query = $select.' FROM '.$b.$a.$on.$where;
        
        $position = ($page - 1) * $limit;
        
        $database->setQuery($query,(int)$position,(int)$limit);
        $rows = $database->loadObjectList();
         if($rows) {
            $ids = array();
		$productClass = hikashop_get('class.product');
                foreach ($rows as $key => $row) {
                    if (!is_null($row->product_id)) {
                        $ids[] = $row->product_id;
                        $productClass->addAlias($rows[$key]);
                    }
                }
                if (empty($ids))
                    $ids = array(0);

                $queryImage = 'SELECT * FROM ' . hikashop_table('file') . ' WHERE file_ref_id IN (' . implode(',', $ids) . ') AND file_type=\'product\' ORDER BY file_ref_id ASC, file_ordering ASC, file_id ASC';
                $database->setQuery($queryImage);
                $images = $database->loadObjectList();
                $path_image = $app->getCfg('wwwrootfile').'/com_hikashop/upload/';
            $datas = array();
            foreach ($rows as $k => $row) {
                $product = new stdClass();
                // image
                $image_path = '';
                if (!empty($images)) {
                    foreach ($images as $image) {
                        if ($row->product_id != $image->file_ref_id)
                            continue;

                        if (!isset($row->file_ref_id)) {
                            foreach (get_object_vars($image) as $key => $name) {
                                $rows[$k]->$key = $name;
                                $image_path = $path_image.$image->file_path;
                            }
                        } else {
                            if (empty($row->images))
                                $row->images = array();
                                $row->images[] = $image;
                                $image_path = '';
                        }
                    }
                }
                if (!isset($rows[$k]->file_name)) {
                    $rows[$k]->file_name = $row->product_name;
                }
                $product->product_category_id = $category->category_id;
                $product->product_id = $row->product_id;
                $product->product_parent_id = $row->product_parent_id;
                $product->product_name = $row->product_name;
                $product->product_description = $row->product_description;
                $product->product_description_nohtml = strip_tags($row->product_description);
                $product->product_image = $image_path;
                $product->product_quantity = $row->product_quantity;
                $product->product_code = $row->product_code;
                $product->product_price = 0;
                $product->product_published = $row->product_published;
                $product->product_alias = $row->product_alias;
                $product->product_duration = $row->estimated_duration;
                $product->product_valid_until = '';
                $product->product_sales = $row->product_sales;
                $datas[] = $product;
            }
            $response->status = true;
            $response->message = 'Learning provider circle.';
            $response->course = $datas;
        } else {
           $response->status = true;
           $response->message = 'No course be found.';
           $response->course = array(); 
        }
        } else {
            $response->status = true;
            $response->message = 'Circle of Learning Provider can not be found.';
            $response->course = array(); 
        }
        return $response;
  }
  
  public function getLPcircleCategory($circle_lp_id) {
      $database = JFactory::getDBO();
      $query = "SELECT * FROM ".  hikashop_table('category')." WHERE learningprovidercircleid = ".$circle_lp_id;
      $database->setQuery($query);
      $rows = $database->loadObject();
      return $rows;
  }
  
  public function addToCart($product_id, $username) {
      $app = JFactory::getApplication();
      $database = JFactory::getDBO();
      hikashop_nocache();
      $cart_type = 'cart';
      $cart_type_id = $cart_type.'_id';
      
      $class = hikashop_get('class.cart');
		$class->cart_type = $cart_type;
		$cart_id = 0;
		if($class->hasCart(JRequest::getInt('cart_id',0,'GET'))){
			$cart_id = $class->cart->cart_id;
      }
      $add=1;
      if(empty($product_id)) {
          $response->status = false;
          $response->message = 'Product id can not be empty.';
          return $response;
      }
        jimport('joomla.user.helper');
        $user_id = JUserHelper::getUserId($username);                
        $user = JFactory::getUser($user_id);
      
      if (hikashop_loadUser(false, false, $user->id) != null || $cart_type != 'wishlist') {
            if (!empty($product_id)) {
                $type = JRequest::getWord('type', 'product');
                if ($type == 'product') {
                    $product_id = (int) $product_id;
                }
                $keepEmptyCart = false;
                
                $cart = $class->initCart();
                $this->saveProductToCart($cart->cart_id, $product_id, 1);
                
//                $cartContent = $class->get($cart->cart_id,$keepEmptyCart,$cart_type);
////                $status = $class->update($product_id, $quantity, $add, $type, true, false, $user->id);
//                if(in_array($type, array('product', 'item'))) {
//			$severalMainProducts = false;
//			if(!is_array($product_id)) {
//				$pid =$product_id;
//				$this->mainProduct = $product_id;
//				$product_id = array($product_id => $quantity);
//				$options = JRequest::getVar('hikashop_product_option', array(), '', 'array');
//				if(!empty($options)&& is_array($options)) {
//					foreach($options as $optionElement) {
//						$class->options[$optionElement] = $pid;
//						$product_id[$optionElement] = $quantity;
//					}
//				}
//			} else {
//				$severalMainProducts = true;
//			}
//
//			$updated = false;
//
//			foreach($product_id as $id => $infos) {
//				if($severalMainProducts)
//					$class->mainProduct = $id;
//				$res = $class->updateEntry(1, $cartContent, (int)$id, $add, false, $type, $force);
//
//				if(is_numeric($id) && $res)
//					$updated = true;
//			}

//			if($updated && $resetCartWhenUpdate) {
//				$this->loadCart(0,true);
//				$app->setUserState(HIKASHOP_COMPONENT.'.shipping_method', null);
//				$app->setUserState(HIKASHOP_COMPONENT.'.shipping_id', null);
//				$app->setUserState(HIKASHOP_COMPONENT.'.shipping_data', null);
//			}

//			$dispatcher->trigger('onAfterCartUpdate',array( &$class, &$cart, &$product_id, &$quantity, &$add, &$type, &$resetCartWhenUpdate, &$force, &$updated ));
//			return $updated;
//		}
                    }
                }

        $app->setUserState(HIKASHOP_COMPONENT.'.'.$cart_type.'_new', '1');
        
        $response->status = true;
        $response->message = 'Cart added.';
        return $response;
  }
  
  public function saveProductToCart($cartid, $productid, $quantity) {
      $database = JFactory::getDBO();
      // check product already added
      $q = "SELECT * FROM ".hikashop_table('cart_product')." WHERE cart_id = '$cartid' AND product_id = '$productid'";
      $database->setQuery($q);
      $check = $database->loadResult();
      if($check) {
          $quantity ++;
          $query = 'UPDATE '.hikashop_table('cart_product').' SET cart_product_quantity = '.$quantity.' WHERE cart_product_id = '.(int)$check;
          $database->setQuery($query);
          $database->query();
      } else {
        $query = 'INSERT INTO '.hikashop_table('cart_product').' (cart_id,cart_product_modified,product_id,cart_product_parent_id,cart_product_quantity,cart_product_wishlist_id) VALUES ( '.(int)$cartid.','.(time()).','.(int)$productid.',0,'.(int)$quantity.',0)';
        $database->setQuery($query);
        $database->query();
        $parent = (int)$database->insertid();
      }
      return true;
  }
  
  public function productAlreadyCart($productid, $userid) {
      $app = JFactory::getApplication();
      $database = JFactory::getDBO();
      hikashop_nocache();
      $query = 'SELECT * FROM ' .  hikashop_table('cart').' as c'
              . ' LEFT JOIN '. hikashop_table('cart_product').' as p ON c.cart_id = p.cart_id'
              . ' WHERE c.user_id = '.$userid.' AND p.product_id = '.$productid .' AND c.cart_type="cart"';
      $database->setQuery($query);
      $rows = $database->loadObject();
      return $rows;
  }
  
  public function productDetail($product_id) {
      $app = JFactory::getApplication();
      $user = JFactory::getUser();   
      if(empty($product_id)) {
          return $response = array(
                'status' => false,
                'message' => 'Product ID can not be empty.'
          );
      }
      $filters = array('a.product_id=' . $product_id);
      hikashop_addACLFilters($filters, 'product_access', 'a');
      $query = 'SELECT a.*, b.product_category_id, b.category_id, b.ordering FROM ' . hikashop_table('product') . ' AS a LEFT JOIN ' . hikashop_table('product_category') . ' AS b ON a.product_id = b.product_id WHERE ' . implode(' AND ', $filters) . ' LIMIT 1';
      $database = JFactory::getDBO();
      $database->setQuery($query);
      $element = $database->loadObject();
      if(empty($element)) {
          return $response = array(
                'status' => false,
                'message' => 'Product not found.'
          );
      }
      // begin render
      $response = new stdClass();
      $response->product_id = $element->product_id;
      $response->product_parent_id = $element->product_parent_id;
      $response->product_name = $element->product_name;
      $response->product_description = $element->product_description;
      $response->product_description_nohtml = $element->product_description;
      
      $ids = array($product_id);
      $query = 'SELECT * FROM '.hikashop_table('file').' WHERE file_ref_id IN ('.implode(',',$ids).') AND file_type IN (\'product\',\'file\') ORDER BY file_ordering ASC, file_id ASC';
      $database->setQuery($query);
      $images = $database->loadObjectList();

      $path_image = $app->getCfg('wwwrootfile') . '/com_hikashop/upload/';
      
        // image
        $image_path = '';
        if (!empty($images)) {
            $image_path = $path_image . $images[0]->file_path;  
        }  
        require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
        if(!$image_path && ($element->product_code != 0 || is_number($element->product_code))) {
            $course_infor = JoomdleHelperContent::getCourseInfo($element->product_code, $user->username);
            $image_path = $course_infor['filepath']. $course_infor['filename'];
        }
        // get price
        $queryPrice = 'SELECT * FROM ' . hikashop_table('price') . ' WHERE price_product_id = '.$element->product_id;
        $database->setQuery($queryPrice);
        $price = $database->loadObjectList();
        
      $response->product_image = $image_path;
      $response->product_quantity = $element->product_quantity;
      $response->product_code = $element->product_code;
      $response->product_price = $price[0]->price_value ? $price[0]->price_value : 0; //$element->product_price;
      $response->product_published = $element->product_published;
      $response->product_alias = $element->product_alias;
      $response->product_duration = $element->estimated_duration;
      $response->product_sales = $element->product_sales;
      $response->learning_outcomes = "".$element->learning_outcomes;
      $response->target_audience = "".$element->target_audience;
      if(isset($element->course_validity)) {
          $courseValidity = $element->course_validity / (30 * 24 * 3600);
          $response->product_validity = $courseValidity;
      }
      
      $already_cart = $this->productAlreadyCart($element->product_id, $user->id);
      $response->product_already_added_cart = false;
      if($already_cart) {
           $response->product_already_added_cart = true;
      }
      
        $isAlreadyPurchased =false;
        if(!$user->guest){
                require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');
                $enrolled_courses = JoomdleHelperContent::getCourseList (0,'timestart DESC',0,$user->username,'','','web');
                if(!empty($enrolled_courses)){
                        foreach ($enrolled_courses as $ecourse) {
                                if($ecourse['remoteid']!=0 && $ecourse['remoteid']==$element->product_code){
                                        $isAlreadyPurchased=true;
                                        break;
                                }
                        }
                }
        }
        $response->isAlreadyPurchased = $isAlreadyPurchased;
      
      return $result = array(
          'status' => true,
          'message' => 'Product detail',
          'product' => $response
      );
    }
    
    private function getProviderCircle($circle_id) {
        $database = JFactory::getDBO();
        $query = 'SELECT gr.ownerid, m.approved FROM #__community_groups as gr '
                . ' LEFT JOIN #__community_groups_members as m ON m.groupid = gr.id'
                . ' WHERE '.$database->QuoteName('id').' = '.$database->Quote($circle_id);
        $database->setQuery($query);
        $results = $database->loadObject();
        return $results;
    }
}