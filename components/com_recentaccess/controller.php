<?php 

defined('_JEXEC') or die;

class RecentaccessController extends JControllerLegacy {
    
    public function display($cachable = false, $urlparams = false)
	{
              
		JRequest::setVar('view', 'recentaccess'); // force it to be the search view
                
               
		return parent::display($cachable, $urlparams);
	}
        
  /* public function closedata() {
       // get current user login
        $users = JFactory::getUser();
        $task = JRequest::getCmd('task', '');
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query
                ->select($db->quoteName(array('id', 'userid', 'activities', 'component', 'moduleid', 'redirect', 'datetime')))
                ->from($db->quoteName('#__user_log'))
                ->where($db->quoteName('userid')."=".$db->quote($users->id))
                ->order('datetime DESC');

        $db->setQuery($query, 0, 1);
        $rows = $db->loadObject();
        if($rows) {
          if($task == 'closedata') {
            $query = $db->getQuery(true)
			->update($db->quoteName('#__user_log'))
			->set($db->quoteName('accessagain') . ' = ' . $db->quote(NULL))
			->where($db->quoteName('id') . ' = ' . $db->quote($rows->id));
		try
		{
			$db->setQuery($query)->execute();
		}
		catch (RuntimeException $e)
		{
			return false;
		}
                unset($_COOKIE['redirect_to']);
                setcookie('redirect_to', null, -1, '/');
        }      
        }
        return;
   }*/
   public function clearRecent() {
        $users = JFactory::getUser();
        $app=JFactory::getApplication();
        
        $db = JFactory::getDBO();
        $query = $db->getQuery(true)
				->delete($db->quoteName('#__user_log'))
                                ->where($db->quoteName('userid') . ' = ' . (int) $users->id);
        
        try {
            $db->setQuery($query)->execute();
        } catch (RuntimeException $e) {
            return false;
        }
        $app->redirect('index.php');
   }
   public function recentPage() {
       
   }
}