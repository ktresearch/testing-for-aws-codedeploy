<?php
defined('_JEXEC') or die;

// Create the controller
$controller = JControllerLegacy::getInstance('recentaccess');

// Perform the Request task
$controller->execute(JRequest::getCmd('task'));

// Redirect if set by the controller
$controller->redirect();

?>