<?php defined('_JEXEC') or die;

$session = JFactory::getSession();
$is_mobile = $session->get('device') == 'mobile' ? true : false;

$class = $is_mobile ? '' : 'desktop-tablet';

?>
<div class="my-circle <?php echo $class; ?>">
    <div class="placeholder-loading">
        <div class="circle-header"><span class="title-text"><?php echo JText::_('COM_COMMUNITY_GROUPS_MY_GROUPS').' '; ?></span>&nbsp;<span class="title-number"><?php if($count > 0) echo $count; else echo ''; ?></span></div>
        <div class="swiper-container">
            <div class="swiper-wrapper circle-list">
                <div class="ph-item" style="">
                    <div class="ph-col-12" style="padding: 0">
                        <div class="ph-picture"></div>
                        <div class="ph-row" style="margin-bottom: 25px;">
                            <div class="ph-col-4"></div>
                            <div class="ph-col-8 empty"></div>
                            <div class="ph-col-12"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function() {
        jQuery.ajax({
            url: '<?php echo JUri::base(). 'index.php?option=com_community&view=frontpage&task=ajaxGetBlockAllCircles' ?>',
            type: 'post',
            success: function(res) {
                res = JSON.parse(res);
                if (res.success) {
                    jQuery('.my-circle .placeholder-loading').fadeOut(0, function() {
                        jQuery('.my-circle').html(res.html.HTML).fadeIn(0);
                        // restart lazyload for this block
                        lazyload();
                    });
                } else {
                    jQuery('.my-circle .circle-list').html("Error").fadeIn(0);
                }
            }
        });
    });
</script>