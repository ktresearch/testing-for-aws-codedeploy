<?php defined('_JEXEC') or die;

$session = JFactory::getSession();
$device = $session->get('device');

$class = ($device != 'mobile') ? 'desktop-tablet' : '';

$mainframe = JFactory::getApplication();
$root = $mainframe->getCfg('wwwrootfile');
if ($count == 0) {
    $class = 'no-circle';
}
?>
<div class="my-circle <?php echo $class; ?>">
<div class="circle-header"><span class="title-text"><?php echo JText::_('COM_COMMUNITY_GROUPS_MY_GROUPS').' '; ?></span>&nbsp;<span class="title-number"><?php if($count > 0) echo $count; else echo ''; ?></span></div>
<div class="swiper-container">
        <?php 
        if (!empty($tmpGroups)) {
            ?>
            <div class="swiper-wrapper circle-list">
            <?php 
            foreach ($tmpGroups as $group) {
                $table =  JTable::getInstance( 'Group' , 'CTable' );
                $table->load($group->id);
                $link = 'index.php?option=com_community&view=groups&task=viewabout&groupid=' . $group->id;
            ?>
                <div class="swiper-slide circle-slides">
                    <div class="circle-slide">
                        <div class="circle-avatar-img">
                            <div class="circle-avatar lazyload" data-src="<?php echo $table->getOriginalAvatar(); ?>"></div>
                        </div>
                        <div class="circle-thumb">
                            <div class="circle-title"><span><a href='<?php echo $link; ?>'><?php echo JHtml::_('string.truncate', strip_tags($group->name), 30, true, $allowHtml = true); ?></a></span></div>
                            <div class="circle-description"><span><?php echo JHtml::_('string.truncate', strip_tags($group->description), 50, true, $allowHtml = true); ?></span></div>
                        </div>
                    </div>
                </div>
            <?php 
             }
         ?>
         </div>
        <!-- Add Arrows -->
            <div class="swiper-button-next1"><i class="bg-second-img icon-next-white-shadow"></i></div>
            <div class="swiper-button-prev1"><i class="bg-second-img icon-prev-white-shadow"></i></div>
         <?php
        } else {
            ?>
            <div class="empty-circle">
                <div class="empty-circle-icon">
                    <img class="empty-icon" src="/images/friends_hd.png" />
                </div>
                <div class="empty-text">
                    <span><?php echo JText::_('MOD_CIRCLE_NOT_CIRCLE'); ?></span>
                </div>
                <div class="empty-action">
                    <a class="empty-search-circle" href="#"><?php echo JText::_('LINK_MORE_CIRCLE'); ?></a>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<script>
    (function ($) {
        var swiper = new Swiper('.my-circle .swiper-container', {
          direction: 'horizontal',
          autoplayDisableOnInteraction: true,  
          spaceBetween: 30,
          nextButton: '.swiper-button-next1',
          prevButton: '.swiper-button-prev1',
          touchMoveStopPropagation: false,
          preventClicks: false,
          allowTouchMove : false,
        });
    })(jQuery);
</script>