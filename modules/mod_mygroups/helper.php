<?php
defined( '_JEXEC' ) or die;

class My_Groups {
    public function group($limit) {
        $db = JFactory::getDBO();

        $query = "SELECT *
                  FROM #__community_groups 
                  WHERE published = 1
                  ORDER BY  id DESC
                  LIMIT $limit
                 ";

        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }
    public function group_recent($userid) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query = "SELECT g.*
                  FROM #__community_groups as g
                  LEFT JOIN #__community_groups_members as m ON g.id = m.groupid
                  WHERE m.memberid = ".$userid."
                  ORDER BY UNIX_TIMESTAMP(g.lastaccess) DESC 
                  LIMIT 0,1"; 
                  
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        $count = count($rows);
        $i = 0;
        $lastestId = 0;
        $groups_recent = array();
        if($rows) {
            $lastestGroup = array();
            foreach ($rows as $row) {
                if(++$i === $count) {
                    $lastestId = $row->id;
                    $lastestGroup[] = $row;
                }
            }
        }
        if($lastestId) {
            // get activity of lastest group access
            $query = "SELECT at.*, c.name "
                    . "FROM #__community_activities as at "
                    . "LEFT JOIN #__community_groups as c ON at.groupid = c.id "
                    . "WHERE at.groupid = ".$lastestId." AND at.app <> 'events.wall' AND at.app <> 'groups.like'"
                    . "ORDER BY at.updated_at DESC "
                    . "LIMIT 0,2";
            $db->setQuery($query);
            $activity = $db->loadObjectList();
            
            $groups_recent = array_merge($lastestGroup, $activity);
        }

        return $groups_recent;
    }
        
    public static function groupActivities($id, $userid = null) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        if ($userid == null) {
            $my = CFactory::getUser();
            $userid = $my->id;
        }
//            $query    = 'SELECT COUNT(*)  FROM '. $db->quoteName('#__community_activities') . ' AS a '
//              . 'WHERE a.'.$db->quoteName('groupid').'=' . $db->Quote( $id );
        $query  = "SELECT COUNT(*) FROM ". $db->quoteName('#__community_activities') . " as a "
            . "LEFT JOIN ". $db->quoteName('#__community_groups_members') . " as m "
            . "ON a.groupid = m.groupid "
            . "WHERE m.memberid = ".$userid." AND a.groupid = " . $db->Quote( $id )." AND (a.updated_at >= m.lastaccess OR m.lastaccess IS NULL)"." AND actor NOT IN (" . $db->Quote( $userid ).")";
        $db->setQuery($query);
        $total      = $db->loadResult();
        return $total;
    }
    
    public function getVideo($id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query = "SELECT at.* "
                    . "FROM #__community_videos as at "
                    . "WHERE at.id = ".$id."";
        $db->setQuery($query);
        $video = $db->loadObject();
        return $video;
    }
    public function getPhoto($id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query = "SELECT pt.* "
                    . "FROM #__community_photos as pt "
                    . "WHERE pt.id = ".$id."";
        $db->setQuery($query);
        $photo = $db->loadObject();
        return $photo;
    }
    public function getEvent($id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query = "SELECT e.title, e.summary, e.startdate, e.enddate "
                    . "FROM #__community_events as e "
                    . "WHERE e.id = ".$id."";
        $db->setQuery($query);
        $event = $db->loadObject();
        return $event;
    }
    public function getDiscus($id, $groupid) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query = "SELECT d.title, d.message "
                    . "FROM #__community_groups_discuss as d "
                    . "WHERE d.id = ".$id." AND d.groupid = ".$groupid."";
        $db->setQuery($query);
        $discuss = $db->loadObject();
        return $discuss;
    }
}
