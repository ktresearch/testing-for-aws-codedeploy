<?php
defined('_JEXEC') or die;
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root().'/modules/mod_mygroups/css/style.css');
$document->addStyleSheet(JUri::root().'/modules/mod_joomdle_my_courses/css/swiper.min.css');
$document->addScript(JUri::root().'/modules/mod_joomdle_my_courses/swiper.min.js');

$model = CFactory::getModel('groups');
$user = CFactory::getUser();

if ($model->getGroupsCount($user->id) <= 5) {
    $tmpGroups = $model->getGroups($user->id, null, false);
    $count = count($tmpGroups);
    require(JModuleHelper::getLayoutPath('mod_mygroups', $params->get('layout', 'default')));
} else {
    $count = 0;
    require(JModuleHelper::getLayoutPath('mod_mygroups', $params->get('layout', 'default_ph_loading')));
}

