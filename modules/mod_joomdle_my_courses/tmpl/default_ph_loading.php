<?php defined('_JEXEC') or die;

$session = JFactory::getSession();
$is_mobile = $session->get('device') == 'mobile' ? true : false;

$class = $is_mobile ? '' : 'desktop-tablet';

?>
<div class="my-course <?php echo $class; ?>">
    <div class="placeholder-loading">
        <div class="course-title-block">
            <span class="title-text" href="mylearning/list.html"><?php echo JText::_("MOD_BLOCK_TITLE_MY_LEARNING"); ?></span>
        </div>
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="ph-item" style="">
                    <div class="ph-col-12" style="padding: 0">
                        <div class="ph-picture"></div>
                        <div class="ph-row" style="margin-bottom: 25px;">
                            <div class="ph-col-4"></div>
                            <div class="ph-col-8 empty"></div>
                            <div class="ph-col-12"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function() {
        jQuery.ajax({
            url: '<?php echo JUri::base(). 'index.php?option=com_community&view=frontpage&task=ajaxGetBlockAllCourses' ?>',
            type: 'post',
            success: function(res) {

                res = JSON.parse(res);
                console.log(res);
                if (res.success) {
                    jQuery('.my-course .placeholder-loading').fadeOut(0, function() {
                        jQuery('.my-course').parents('.banner').html(res.html.HTML).fadeIn(0);
                        // restart lazyload for this block
                        lazyload();
                    });
                } else {
                    jQuery('.my-course .swiper-wrapper').html("Error").fadeIn(0);
                }
            }
        });
    });
</script>