<?php
/**
* @version		
* @package		Joomdle
* @copyright	Copyright (C) 2008 - 2010 Antonio Duran Terres
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

JFactory::require_login();

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . '/modules/mod_joomdle_my_courses/css/swiper.min.css');
$document->addScript(JUri::root() . '/modules/mod_joomdle_my_courses/swiper.min.js');

require_once(JPATH_SITE.'/components/com_joomdle/helpers/content.php');

// Include the whosonline functions only once
require_once (dirname(__FILE__).'/helper.php');

$linkto = $params->get( 'linkto' , 'moodle');

$comp_params = JComponentHelper::getParams( 'com_joomdle' );

$moodle_xmlrpc_server_url = $comp_params->get( 'MOODLE_URL' ).'/mnet/xmlrpc/server.php';
$moodle_auth_land_url = $comp_params->get( 'MOODLE_URL' ).'/auth/joomdle/land.php';
$linkstarget = $comp_params->get( 'linkstarget' );
$default_itemid = $comp_params->get( 'default_itemid' );
$joomdle_itemid = $comp_params->get( 'joomdle_itemid' );
$courseview_itemid = $comp_params->get( 'courseview_itemid' );
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$show_unenrol_link = $params->get( 'show_unenrol_link' );

$user = JFactory::getUser();
$username = $user->get('username');

$session = JFactory::getSession();
$token = md5 ($session->getId());

$first_time_login = $session->get('first_time');

$device = $session->get('device');
$limit = ($device != 'mobile') ? 3 : 1;

$order = 'fullname ASC';
$time = time();
$lasttime = time() - 60 * 60 * 24 * 7;
$swhere = ' AND lastaccess BETWEEN '.$lasttime.' AND '.$time.' ';
$enrollable_only = null;

if (JHelperLGT::countMyLearningCourses() <= 5) {
    if ($comp_params->get('use_new_performance_method'))
        $cursos = JHelperLGT::getMyLearningCourses();
    else
        $cursos = JoomdleHelperContent::getCourseList( (int) $enrollable_only,  $order, 0, $username, $where = '', $swhere = '');
    $noCourseFound = $cursos ? false : true;
    require(JModuleHelper::getLayoutPath('mod_joomdle_my_courses', $params->get('layout', 'default')));
} else {
    require(JModuleHelper::getLayoutPath('mod_joomdle_my_courses', $params->get('layout', 'default_ph_loading')));
}
