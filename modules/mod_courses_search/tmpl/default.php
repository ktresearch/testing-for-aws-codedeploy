<?php 
    $session = JFactory::getSession();
    $device = $session->get('device');
?>

<div class="search-box">
    <form class="form-search-box" action="/index.php?option=com_joomdle&view=myprogress" method="post">
        <div class="btSearch"></div>
        <input class="text-search-course" type="text" name="keyword" size="30" id="txtKeyword" placeholder="<?php echo ($device != 'mobile') ? JText::_('JCOURSE_SEARCH_PH') : '';?>"/>
        <input type="hidden" name="type" value="ajaxsort"/>
        <input type="hidden" name="sort" value="keyword"/>
    </form>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.search-box .btSearch').click(function() {
            jQuery('.form-search-box').submit();
        });
        jQuery( "input.text-search-course" ).keyup(function() {
            var value = jQuery( this ).val();
            if (value != '') {
                jQuery(this).addClass('onkeyup');
            } else {
                jQuery(this).removeClass('onkeyup');
            }
        })
        .keyup();
    });
</script>