<?php 
/**
 * @package     Joomla.Site
 * @subpackage  mod_stats
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

    include_once(JPATH_BASE . '/components/com_community/defines.community.php');
    require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
    include_once(COMMUNITY_COM_PATH . '/libraries/activities.php');
    include_once(COMMUNITY_COM_PATH . '/helpers/time.php');
    $svgPath = CFactory::getPath('template://assets/icon/joms-icon.svg');
    include_once $svgPath;
    JFactory::getLanguage()->isRTL() ? CTemplate::addStylesheet('style.rtl') : CTemplate::addStylesheet('style');

// test
require JModuleHelper::getLayoutPath('mod_status_post', $params->get('layout', 'default'));