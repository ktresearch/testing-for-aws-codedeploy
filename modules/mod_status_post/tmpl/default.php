<?php

defined('_JEXEC') or die();

$isAjax = false;
if ( JRequest::getCmd('task', '') == 'azrul_ajax') {
    $isAjax = true;
}
// check user login
$session = JFactory::getSession();
$device = $session->get('device');
$first_time = $session->get('first_time');

$cUser = CFactory::getUser();
$aboutMe = $cUser->getInfo('FIELD_ABOUTME');
$gender = $cUser->getInfo('FIELD_GENDER');
$birthDate = $cUser->getInfo('FIELD_BIRTHDATE');
$avatar = $cUser->_avatar;
$cover = $cUser->_cover;
$interests = $cUser->getInfo('FIELD_INTEREST');
$number = $cUser->getInfo('FIELD_MOBILE');
$address = $cUser->getInfo('FIELD_ADDRESS');
$postcode = $cUser->getInfo('FIELD_POSTCODE');

?>
<script src="./media/jui/js/jquery.min.js"></script>
<script>
    jQuery.noConflict();
</script>

    <?php if (empty($aboutMe) && empty($birthDate) && empty($gender) && empty($avatar) && empty($cover) && empty($interests) ) {
        JFactory::getDocument()->setTitle(
            JText::sprintf('JFRONTPAGE_TITLE', JFactory::getUser()->name)
        );
        ?>
        <div class="myprofile-content-block">
        <div class="ft-create-profile">
            <h2><a href="index.php?option=com_community&view=profile" class=""><?php echo JText::_("JFT_MY_PROFILE"); ?></a></h2>
            <img src="templates/t3_bs3_blank/images/ProfileIcon.png">
            <div class="description"><?php echo JText::_("JFT_CREATE_PROFILE_DES"); ?></div>
            <a href="index.php?option=com_community&view=profile&task=edit" class=""><?php echo JText::_("JFT_LET_GET_STARTED"); ?></a>
        </div>
        </div>

    <!-- <did class="ft-browse-courses">
        <img src="templates/t3_bs3_blank/images/Course.png">
        <div class="description"><?php //echo JText::_("JFT_BROWSE_COURSES_DES"); ?></div>
        <a href="index.php?option=com_joomdle&view=mylearning" class=""><?php //echo JText::_("MOD_LINK_BROWSE_COURSE"); ?></a>
    </did>
    <div class="ft-browse-circles">
        <img src="templates/t3_bs3_blank/images/Circles.png">
        <div class="description"><?php //echo JText::_("JFT_BROWSE_CIRCLES_DES"); ?></div>
        <a href="index.php?option=com_community&view=groups" class=""><?php //echo JText::_("LINK_MORE_CIRCLE"); ?></a>
    </div>
    <div class="ft-search-friends">
        <img src="templates/t3_bs3_blank/images/HandshakeIcon.png">
        <div class="description"><?php //echo JText::_("JFT_SEARCH_FRIENDS_DES"); ?></div>
        <a href="index.php?option=com_community&view=friends" class=""><?php //echo JText::_("JFT_SEARCH_FRIENDS"); ?></a>
    </div> -->

<?php  } ?>

<?php if($device == 'mobile') {?>
<!--<div class="course-search">
    <?php
//        $Module = &JModuleHelper::getModule('mod_courses_search');
//        echo JModuleHelper::renderModule( $Module );
    ?>
</div>-->
<?php } ?>

<?php //if($first_time){ ?>
<!-- <div class="message-login">
   <span>Welcome!</span>
</div> -->
<?php //} else { ?>

<?php //} ?>
