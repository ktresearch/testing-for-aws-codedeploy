<?php
    
    defined('_JEXEC') or die('Restricted access');

    require_once(dirname(__FILE__) . '/helper.php');
    require_once(JPATH_BASE . '/components/com_community/libraries/core.php');
    CWindow::load();
    $document = JFactory::getDocument();
    // $document->addStyleSheet(JURI::root(true) . '/modules/mod_notifications/css/style.css');
    $config = CFactory::getConfig();
    $my = CFactory::getUser();
    $myParams             = $my->getParams();

    $modelNotification = CFactory::getModel('notification');
    $notifications = $modelNotification->getNotification($my->id, '0');
    
    require(JModuleHelper::getLayoutPath('mod_notifications', $params->get('layout', 'default')));
