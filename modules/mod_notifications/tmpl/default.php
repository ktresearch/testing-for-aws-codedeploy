<?php

defined('_JEXEC') or die('Restricted access');
if($notifications) { ?>
    <?php
    foreach( $notifications as $row ) {
        $user = CFactory::getUser( $row->actor );
        if($row->cmd_type != '') {
            $onclick = '';
            $class_read = '';
            if($row->status == 0) {
                $class_read = 'not-read';
            }
            if($row->cmd_type == 'notif_friends_request_connection') {
                require_once( JPATH_ROOT .'/components/com_community/libraries/core.php' );
                $friendPageURL = CRoute::_('index.php?option=com_community&view=friends') ;
//                $onclick = 'onclick="joms.api.friendResponse('.$row->actor.')"';
                $onclick = 'onclick="window.location.href=\''.$friendPageURL.'\'"';
            }
            $action_urls = '';
            ?>
            <div id="<?php echo $row->id; ?>" style="cursor: pointer;" class="joms-stream__container joms-stream--discussion notifications <?php echo $class_read; ?>" <?php echo $onclick; ?>>
                <div class="joms-stream__body-notification">
                    <div class="cStream-Headline"><?php
                        if($row->cmd_type == 'notif_friends_request_connection') { ?>
                            <div class="bg-second-img w16px icon-add-friends-notification"></div>
                            <?php
                            $row->content=JText::_('COM_COMMUNITY_FRIEND_ADD_REQUEST');
                        }
                        if($row->cmd_type == 'notif_groups_member_join' || $row->cmd_type == 'notif_groups_invite') {
                            $url_ntf = json_decode($row->params);
                            $action_urls = $url_ntf->group_url;

                            if ($row->cmd_type == 'notif_groups_invite'){
                                $row->content=JText::_('COM_COMMUNITY_GROUPS_JOIN_INVITATION_MESSAGE');
                            }
                            if ($row->cmd_type == 'notif_groups_member_join') {
                                $cmdData = explode(' ', $row->content);
                                // notification join open circle
                                if($cmdData[1] == "joined" && $cmdData[2] == "the" && $cmdData[3] == "Circle"){
                                    $row->content=JText::_('COM_COMMUNITY_NEWS_GROUP_JOIN');
                                }else if($row->status == 1){ 
                                // notification join private circle and approve, reject    
                                    $row->content=JText::_('COM_COMMUNITY_GROUP_JOIN_REQUEST_SUBJECT');
                                }else{
                                // notification join private circle and clicked approve, reject   
                                    $row->content=JText::_('COM_COMMUNITY_GROUPS_JOIN_REQUEST_MESSAGE');
                                }
                            }
                            ?>
                            <div class="bg-second-img w16px icon-add-members-notification"></div>
                            <?php
                        }
                        if($row->cmd_type == 'notif_groups_discussion_reply' || $row->cmd_type == 'notif_groups_discussion_newfile') {
                            ?>
                            <div class="bg-second-img w16px icon-discussion-notification"></div>
                            <?php
                            $url_ntf = json_decode($row->params);
                            $action_urls = $url_ntf->url;
                            $row->content=JText::_('COM_COMMUNITY_GROUP_NEW_DISCUSSION_REPLY_SUBJECT');
                        }
                        if($row->cmd_type == 'notif_groups_create_news') {
                            ?>
                            <div class="bg-second-img w16px icon-announcement-notification"></div>
                            <?php
                            $url_ntf = json_decode($row->params);
                            $action_urls = $url_ntf->group_url.'&notification=1';
                        }
                        if($row->cmd_type == 'notif_groups_create_event' || $row->cmd_type == 'notif_events_invite') {
                            ?>
                            <span class="glyphicon glyphicon-calendar" aria-hidden="true" style="color:#4F5355;"></span>
                            <?php
                            $row->content=JText::_('COM_COMMUNITY_EVENTS_GROUP_JOIN_INVITE');
                            $url_ntf = json_decode($row->params);
                            $action_urls = $url_ntf->event_url;
                        }
                        if($row->cmd_type == 'notif_course_enrol') {
                            ?>
                            <div class="bg-second-img w16px icon-add-members-notification"></div>
                            <?php
//                            $row->content=JText::_('COM_JOOMDLE_USER_ENROL_NOTIFICATION');
                            $url_ntf = json_decode($row->params);
                            $action_urls = $url_ntf->url;
                        }
                        $html =  CContentHelper::injectTags($row->content,$row->params,true);

                        if ( $row->cmd_type == 'notif_photos_like' ) {
                            preg_match_all('/(albumid|photoid)=(\d+)/', $html, $matches);

                            // Get albumid and photoid.
                            $albumid = false;
                            $photoid = false;
                            foreach ( $matches[1] as $index => $varname ) {
                                if ( $varname == 'albumid' ) {
                                    $albumid = $matches[2][ $index ];
                                } else if ( $varname == 'photoid' ) {
                                    $photoid = $matches[2][ $index ];
                                }
                            }

                            preg_match('/href="[^"]+albumid[^"]+"/', $html, $matches);

                            $html = preg_replace(
                                '/href="[^"]+albumid[^"]+"/',
                                'href="javascript:" onclick="joms.api.photoOpen(\''
                                . ( $albumid ? $albumid : '' ) . '\', \'' . ( $photoid ? $photoid : '' ) . '\');"',
                                $html
                            );
                        }
                        echo $html;
                        ?></div>
                </div>

                <input type="hidden" id="action_url" value="<?php echo $action_urls; ?>" />
            </div>
        <?php }
    } ?>
<?php } ?>
<script>
    (function($) {
        $('div.notifications.not-read').on('click', function() {
            var id = $(this).attr('id');
            var action_url = $(this).children('input').val();
            $.ajax({
                type: 'post' ,
                url: 'index.php?option=com_community&view=profile&task=ajaxUpdateNotification',
                data: {id: id},
                success: function(data) {
                    // nothing
                    if(action_url) {
                        window.location.href = action_url;
                    }
                }
            });
        });
    })(jQuery);
</script>