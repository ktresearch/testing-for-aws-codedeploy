<?php
defined('JPATH_PLATFORM') or die;

class JHelperLGTMoodle {

    protected function getConditions($inputFields, $params) {
        $conditions = [];
        foreach ($inputFields as $k => $v) {
            $conditions[$v] = isset($params[$k]) ? $params[$k] : null;
        }

        return $conditions;
    }

    protected function getResponse($responseFields, $result) {
        $response = [];

        if (is_array($result)) {
            foreach ($responseFields as $field) {
                if (isset($result[$field])) $response[$field] = $result[$field]; else $response[$field] = null;
            }
        } else if (is_object($result)) {
            foreach ($responseFields as $field) {
                if (isset($result->$field)) $response[$field] = $result->$field; else $response[$field] = null;
            }
        }

        return $response;
    }
/*
    public static function get_course_info($params) {
        $inputFields = [
            'id',
            'username'
        ];

        $conditions = self::getConditions($inputFields, $params);

        $result = JHelperLGT::getCourseInfo($conditions);

        $responseFields = [
            'remoteid',
            'cat_id',
            'cat_name',
            'cat_description',
            'visible',
            'cat_parent',
            'sortorder',
            'fullname',
            'shortname',
            'idnumber',
            'summary',
            'fileid',
            'filetype',
            'filepath',
            'filename',
            'startdate',
            'timepublish',
            'numsections',
            'lang',
            'price',
            'currency',
            'duration',
            'enrolstartdate',
            'enrolenddate',
            'enrolperiod',
            'self_enrolment',
            'enroled',
            'enrol_timeend',
            'in_enrol_date',
            'guest',
            'creator',
            'count_learner',
            'learningoutcomes',
            'targetaudience',
            'coursesurvey',
            'facilitatedcourse',
            'certificatecourse',
            'course_status',
            'userRoles',
            'course_type'
        ];

        return self::getResponse($responseFields, $result);
    }

    public static function get_user_role($params) {
        $inputFields = [
            'id',
            'username'
        ];

        $conditions = self::getConditions($inputFields, $params);

        $result = JHelperLGT::getUserRole($conditions);

        $responseFields = [
            'status',
            'message',
            'roles'
        ];

        return self::getResponse($responseFields, $result);
    }


    // public static function get_course_page_data($params) {
    //     $inputFields = [
    //         'id',
    //         'username'
    //     ];

    //     $conditions = self::getConditions($inputFields, $params);

    //     $result = JHelperLGT::getCoursePageData($conditions);

    //     $responseFields = [
    //         'userRoles',
    //         'course_info',
    //         'mods',
    //         'progressmods',
    //         'topics',
    //         'modsForCreator',
    //         'questions',
    //         'checkCompleteFeedback'
    //     ];

    //     return self::getResponse($responseFields, $result);
    // }

    public static function get_course_mods($params) {
        $inputFields = [
            'id',
            'username',
            'visible',
            'mtype'
        ];

        $conditions = self::getConditions($inputFields, $params);

        $result = JHelperLGT::getCourseMods($conditions);

        $responseFields = [
            'section',
            'sectionid',
            'name',
            'summary',
            'mods',
        ];

        return self::getResponse($responseFields, $result);
    }
*/

}