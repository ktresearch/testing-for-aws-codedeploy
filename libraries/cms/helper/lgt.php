<?php
defined('JPATH_PLATFORM') or die;
define('MODULE_CONTEXT_LEVEL', 70);
define('COURSE_CONTEXT_LEVEL', 50);
define('PROGRAM_CONTEXT_LEVEL', 45);
define('COURSECAT_CONTEXT_LEVEL', 40);

class JHelperLGT {
    protected function getMoodleDbo() {
        return JFactory::getDbo(2);
    }

    protected function loadTables() {
        JTable::addIncludePath(JPATH_PLATFORM . '/joomla/table/moodle');
    }

    protected function getTable($table) {
        return JTable::getInstance($table, 'Table', array('dbo' => self::getMoodleDbo()));
    }

    public static function getCourseInfo($conditions) {
        self::loadTables();
        $db = self::getMoodleDbo();

        $courseid = $conditions['id'];
        $username = $conditions['username'] ? $conditions['username'] : JFactory::getUser()->username;

        $courseRecord = $db->get_record_sql(
            "SELECT
            co.id          AS remoteid,
            ca.id          AS cat_id,
            ca.name        AS cat_name,
            ca.description AS cat_description,
            ca.parent AS cat_parent,
            co.sortorder,
            co.fullname,
            co.shortname,
            co.idnumber,
            co.summary,
            co.startdate,
            co.timepublish,
            co.lang,
            co.creator,
            co.duration,
            co.price,
            co.currency,
            co.learningoutcomes,
            co.targetaudience,
            co.coursesurvey,
            co.facilitatedcourse,
            co.certificatecourse,
            co.visible
            FROM
            #__course_categories ca
            JOIN
            #__course co ON
            ca.id = co.category
            WHERE
            co.id = $courseid
            ORDER BY
            sortorder ASC");

        $context = self::getTable('context');
        $context->load(['instanceid' => $courseid, 'contextlevel' => COURSE_CONTEXT_LEVEL]);

        $files = $db->get_records('files', ['contextid' => $context->id, 'component' => 'course', 'filearea' => 'overviewfiles', 'itemid' => 0]);

        if (!empty($files)) {
            foreach ($files as $file) {
                if ($file['filename'] == '.') continue;
                $courseRecord['filename'] = $file['filename'];
                $courseRecord['filepath'] = JUri::base() . 'courses/pluginfile.php/' . $context->id . '/course/overviewfiles/';
            }
        } else {
            $courseRecord['filepath'] = JUri::base() . 'courses/theme/parenthexis/pix/';
            $courseRecord['filename'] = 'nocourseimg.jpg';
        }

        $query = $db->getQuery(true);
        $query->select('*')->from('#__course_sections')->where('course = '.$courseid.' and section != 0 and visible=1');
        $db->setQuery($query);
        $courseRecord['numsections'] = count($db->loadRowList());

        $courseRecord['duration'] = $courseRecord['duration'] ? (float)($courseRecord['duration'] / 3600) : 0;

        $hascertificate = false;
        $creator = self::getTable('user');
        $creator->load($courseRecord['creator']);

        $act_certificate = self::getCourseMods([
            'id' => $courseid,
            'username' => $creator->username,
            'visible' => '1',
            'mtype' => 'certificate',
        ]);

        if (!empty($act_certificate)) {
            foreach ($act_certificate as $mod) {
                $resources = $mod['mods'];
                foreach ($resources as $res) {
                    if ($res['mod'] == 'certificate') {
                        $hascertificate = true;
                    }
                }
            }
        }

        $courseRecord['creator'] = $creator->username;
        $courseRecord['certificatecourse'] = $hascertificate ? 1 : 0;

        $courseRecord['self_enrolment'] = 0;
        $courseRecord['guest'] = 0;
        $courseRecord['in_enrol_date'] = true;
        $now = time();

        $enrol = self::getTable('enrol');
        $enrolMethods = $enrol->loadAssocList(['courseid' => $courseid, 'status' => 0]);
        foreach ($enrolMethods as $instance) {
            $instance = (object)$instance;
            if ($instance->enrolstartdate) $courseRecord['enrolstartdate'] = $instance->enrolstartdate;
            if ($instance->enrolenddate) $courseRecord['enrolenddate'] = $instance->enrolenddate;

            if ($instance->enrolperiod) $courseRecord['enrolperiod'] = $instance->enrolperiod;

            if ($instance->enrol == 'self') $courseRecord['self_enrolment'] = 1;

            if ($instance->enrol == 'guest') $courseRecord['guest'] = 1;

            if ($instance->enrolstartdate && $instance->enrolenddate) {
                $courseRecord['in_enrol_date'] = (($instance->enrolstartdate <= $now) && ($instance->enrolenddate >= $now)) ? true : false;
            } else if ($instance->enrolstartdate) {
                $courseRecord['in_enrol_date'] = ($instance->enrolstartdate <= $now) ? true : false;
            } else if ($instance->enrolenddate) {
                $courseRecord['in_enrol_date'] = ($instance->enrolenddate >= $now) ? true : false;
            }
        }

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $enrolledUsers = [];
        $arrEnrolledUsers = self::getEnrolledUsers(['id' => $courseid]);
        foreach ($arrEnrolledUsers as $key => $value) {
            $enrolledUsers[] = $value->userid;
        }
        $courseRecord['enroled'] = in_array($user->id, $enrolledUsers) ? 1 : 0;

        $query = $db->getQuery(true);
        $query->select('ue.timeend')->from('#__user_enrolments ue')
            ->innerJoin('#__enrol e ON ue.enrolid = e.id')
            ->where('ue.userid = ' . $user->id . ' and e.courseid = '.$courseid.' and e.status = 0');
        $db->setQuery($query);
        $res = $db->loadAssocList();

        $max = 1;
        foreach ($res as $key => $value) {
            if ($max != 0) {
                if ($value['timeend'] == 0) $max = 0; else
                    $max = max($max, $value['timeend']);
            }
        }
        $courseRecord['enrol_timeend'] = $max;

        $courseRecord['userRoles'] = json_encode(self::getUserRole($conditions));

        $countlearner = 0;
        $userRole = self::getUserRole(['id' => $courseid]);
        $userroles = $userRole['status'] ? $userRole['roles'] : [];

        if (!empty($userroles)) {
            foreach ($userroles as $userrole) {
                foreach (json_decode($userrole['role']) as $role) {
                    if ($role->roleid == 5) {
                        $countlearner++;
                    }
                }
            }
        }
        $courseRecord['count_learner'] = $countlearner;
        $courseRecord['course_status'] = self::getCourseStatus($courseid);;

        $coursetype = self::checkCourseType($courseid);
        $courseRecord['course_type'] = $coursetype ? $coursetype['role'] : false;

        return $courseRecord;
    }

    public static function getCourseInforSimple($conditions) {
        self::loadTables();
        $db = self::getMoodleDbo();
        
        $courseid = $conditions['id'];
        $username = $conditions['username'] ? $conditions['username'] : JFactory::getUser()->username;
        
        $courseRecord = $db->get_record_sql(
            "SELECT
            co.id          AS remoteid,
            ca.id          AS cat_id,
            ca.name        AS cat_name,
            ca.description AS cat_description,
            ca.parent AS cat_parent,
            co.sortorder,
            co.fullname,
            co.shortname,
            co.idnumber,
            co.summary,
            co.startdate,
            co.timepublish,
            co.creator,
            co.duration,
            co.price,
            co.currency,
            co.learningoutcomes,
            co.targetaudience,
            co.coursesurvey,
            co.facilitatedcourse,
            co.certificatecourse,
            co.visible,
            cr.status
            FROM
            #__course_categories ca
            JOIN
            #__course co ON
            ca.id = co.category
            LEFT JOIN
            #__course_request_approve cr ON
            cr.courseid = co.id
            WHERE
            co.id = $courseid
            ORDER BY
            sortorder ASC");

        $context = self::getTable('context');
        $context->load(['instanceid' => $courseid, 'contextlevel' => COURSE_CONTEXT_LEVEL]);

        $files = $db->get_records('files', ['contextid' => $context->id, 'component' => 'course', 'filearea' => 'overviewfiles', 'itemid' => 0]);

        if (!empty($files)) {
            foreach ($files as $file) {
                if ($file['filename'] == '.') continue;
                $courseRecord['filename'] = $file['filename'];
                $courseRecord['filepath'] = JUri::base() . 'courses/pluginfile.php/' . $context->id . '/course/overviewfiles/';
            }
        } else {
            $courseRecord['filepath'] = JUri::base() . 'courses/theme/parenthexis/pix/';
            $courseRecord['filename'] = 'nocourseimg.jpg';
        }
        
        $courseRecord['duration'] = $courseRecord['duration'] ? (float)($courseRecord['duration'] / 3600) : 0;

        $creator = self::getTable('user');
        $creator->load($courseRecord['creator']);
        
        $courseRecord['creator'] = $creator->username;
        $courseRecord['self_enrolment'] = 0;
        $courseRecord['guest'] = 0;
        $courseRecord['in_enrol_date'] = true;
        $now = time();

        $enrol = self::getTable('enrol');
        $enrolMethods = $enrol->loadAssocList(['courseid' => $courseid, 'status' => 0]);
        foreach ($enrolMethods as $instance) {
            $instance = (object)$instance;
            if ($instance->enrolstartdate) $courseRecord['enrolstartdate'] = $instance->enrolstartdate;
            if ($instance->enrolenddate) $courseRecord['enrolenddate'] = $instance->enrolenddate;

            if ($instance->enrolperiod) $courseRecord['enrolperiod'] = $instance->enrolperiod;

            if ($instance->enrol == 'self') $courseRecord['self_enrolment'] = 1;

            if ($instance->enrol == 'guest') $courseRecord['guest'] = 1;

            if ($instance->enrolstartdate && $instance->enrolenddate) {
                $courseRecord['in_enrol_date'] = (($instance->enrolstartdate <= $now) && ($instance->enrolenddate >= $now)) ? true : false;
            } else if ($instance->enrolstartdate) {
                $courseRecord['in_enrol_date'] = ($instance->enrolstartdate <= $now) ? true : false;
            } else if ($instance->enrolenddate) {
                $courseRecord['in_enrol_date'] = ($instance->enrolenddate >= $now) ? true : false;
            }
        }
        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $enrolledUsers = [];
        $arrEnrolledUsers = self::getEnrolledUsers(['id' => $courseid]);
        foreach ($arrEnrolledUsers as $key => $value) {
            $enrolledUsers[] = $value->userid;
        }
        $courseRecord['enroled'] = in_array($user->id, $enrolledUsers) ? 1 : 0;

        $query = $db->getQuery(true);
        $query->select('ue.timeend')->from('#__user_enrolments ue')
            ->innerJoin('#__enrol e ON ue.enrolid = e.id')
            ->where('ue.userid = ' . $user->id . ' and e.courseid = '.$courseid.' and e.status = 0');
        $db->setQuery($query);
        $res = $db->loadAssocList();

        $max = 1;
        foreach ($res as $key => $value) {
            if ($max != 0) {
                if ($value['timeend'] == 0) $max = 0; else
                    $max = max($max, $value['timeend']);
            }
        }
        $courseRecord['enrol_timeend'] = $max;


        $countlearner = 0;
        $userRole = self::getUserRole(['id' => $courseid]);
        $userroles = $userRole['status'] ? $userRole['roles'] : [];

        if (!empty($userroles)) {
            foreach ($userroles as $userrole) {
                foreach (json_decode($userrole['role']) as $role) {
                    if ($role->roleid == 5) {
                        $countlearner++;
                    }
                }
            }
        }
        $courseRecord['count_learner'] = $countlearner;
        
        if ($courseRecord['status'] && $courseRecord['status'] == 0) {
            $status = 'pending_approval';
        } else if($courseRecord['status'] && $courseRecord['status'] == 1) {
            $status = 'approved';
        } else if($courseRecord['status'] && $courseRecord['status'] == -1) {
            $status = 'unapproved';
        } else if($courseRecord['status'] && $courseRecord['status'] == 2) {
            $status = 'published';
        } else if($courseRecord['status'] && $courseRecord['status'] == -2) {
            $status = 'unpublished';
        } else {
            $instance = $db->get_record('enrol', ['courseid' => $courseid, 'status' => 0, 'enrol' => 'self']);
            if ($instance) $status = 'published';
            else $status = '';
        }
        $courseRecord['course_status'] = $status;

        $coursetype = self::checkCourseType($courseid);
        $courseRecord['course_type'] = $coursetype ? $coursetype['role'] : false;

        return $courseRecord;

    }

    function checkCourseType ($courseid) {
        self::loadTables();
        $db = self::getMoodleDbo();

        $course = (object)$db->get_record('course', array('id' => $courseid));

        if ($course) {
            $cat = (object)$db->get_record('course_categories', array('id' => $course->category));

            $catcontext = self::getTable('context');
            $catcontext->load(['instanceid' => $cat->id, 'contextlevel' => COURSECAT_CONTEXT_LEVEL]);

            if ($cat->userowner) {
                $rolemanages = $db->get_records('role_assignments', array('contextid' => $catcontext->id, 'userid' => $cat->userowner, 'roleid' => 1));

                if (!empty($rolemanages) && count($rolemanages) > 0) {
                    $result['role'] = 'course_lp';
                    $result['managerid'] = $cat->userowner;
                } else {
                    $result['role'] = 'course_non';
                    $result['managerid'] = $course->creator;
                }
            } else {
                $result['role'] = 'course_non';
                $result['managerid'] = $course->creator;
            }

            return $result;
        } else {
            return false;
        }
    }

    public static function getCourseStatus($id) {
        self::loadTables();
        $db = self::getMoodleDbo();

        $course_request = $db->get_record('course_request_approve', array('courseid'=>$id));

        if ($course_request && $course_request['status'] == 0) {
            $status = 'pending_approval';
        } else if($course_request && $course_request['status'] == 1) {
            $status = 'approved';
        } else if($course_request && $course_request['status'] == -1) {
            $status = 'unapproved';
        } else if($course_request && $course_request['status'] == 2) {
            $status = 'published';
        } else if($course_request && $course_request['status'] == -2) {
            $status = 'unpublished';
        } else {
            $instance = $db->get_record('enrol', ['courseid' => $id, 'status' => 0, 'enrol' => 'self']);
            if ($instance) $status = 'published';
            else $status = '';
        }
        return $status;
    }

    public static function getUserRole($conditions) {
        self::loadTables();
        $db = self::getMoodleDbo();

        $courseid = $conditions['id'];
        $courseRecord = self::getTable('course');
        $courseRecord->load($courseid);

        if (empty($courseRecord->id))
            return [
                'status'=> false,
                'message'=>'Course not found',
                'roles'=>[]
            ];

        $enroledUsers = self::getEnrolledUsers($conditions);
        $enroledUsersIds = [];
        foreach ($enroledUsers as $u) {
            $enroledUsersIds[$u->userid] = $u->userid;
        }

        $users = [];
        if (isset($conditions['username']) && !empty($conditions['username'])) {
            $userRecord = self::getTable('user');
            $userRecord->load(['username' => $conditions['username']]);
            $users[$userRecord->id] = $userRecord->id;
        } else {
            $users = $enroledUsersIds;
        }

        $context = self::getTable('context');
        $context->load(['instanceid' => $courseid, 'contextlevel' => COURSE_CONTEXT_LEVEL]);

        $res = [];
        foreach ($users as $k => $userid) {
            $user = self::getTable('user');
            $user->load($userid);

            $rolesArr = $db->get_records_sql(
                "SELECT ra.*, r.name, r.shortname
                FROM #__role_assignments ra, #__role r
                WHERE ra.userid = $userid
                AND ra.roleid = r.id
                AND ra.contextid = ". $context->id
            );

            $roles_user = [];
            $hasPermission = false;
            if ($rolesArr) {
                foreach ($rolesArr as $role) {
                    $roles_user[] = [
                        'roleid' => $role['roleid'],
                        'sortname' => $role['shortname']
                    ];
                    if (in_array($role['roleid'], [1, 2, 3])) $hasPermission = true;
                }
            }

            $ob = [];
            $ob['username'] = $user->username;

            if ($hasPermission)
                $ob['hasPermission'] = 1;
            else {
                if ($courseRecord->creator == $user->id) {
                    $ob['hasPermission'] = 1;

                    if (array_key_exists($user->id, $enroledUsersIds)) {
                        self::role_assign(2, $user->id, $context->id);
                    } else {
                        self::enrol_user($user->username, $courseid, 2); //2 - Course Creator
                    }

                    $roles_user[] = array('roleid' => 2, 'sortname' => 'coursecreator');
                } else $ob['hasPermission'] = 0;
            }

            $ob['role'] = json_encode($roles_user);

            $res[] = $ob;
        }

        $result['status'] = true;
        $result['message'] = 'Success.';
        $result['roles'] = $res;

        return $result;
    }

    public static function getEnrolledUsers($conditions) {
        self::loadTables();

        $courseid = $conditions['id'];
        $context = self::getTable('context');
        $context->load(['instanceid' => $courseid, 'contextlevel' => COURSE_CONTEXT_LEVEL]);

        $db = self::getMoodleDbo();
        $query = 'SELECT DISTINCT ra.*, r.name, r.shortname
              FROM #__role_assignments ra, #__role r, #__enrol e, #__user_enrolments ue
              WHERE ra.roleid = r.id
              AND e.courseid = ' . $courseid .' AND e.status = 0
              AND ue.status = 0 AND ue.enrolid = e.id
            AND ra.contextid = '. $context->id;
        $run = $db->setQuery($query);
        $rolesArr = $run->loadObjectList();
        return $rolesArr;
    }

    public static function getFastModulesInfo($courseid) {
        self::loadTables();
        $db = self::getMoodleDbo();

        $course = (object)$db->get_record('course', array('id'=>$courseid), '*');
        $info = unserialize($course->modinfo);
        $return = [];
        foreach ($info as $mod) {
            $mod = (object)$mod;
            $cm = new stdClass();
            $cm->id               = $mod->cm;
            $cm->instance         = $mod->id;
            $cm->course           = $course->id;
            $cm->modname          = $mod->mod;
            $cm->idnumber         = isset($mod->idnumber) ? $mod->idnumber : '';
            $cm->name             = $mod->name;
            $cm->visible          = $mod->visible;
            $cm->sectionnum       = $mod->section; // Note weirdness with name here
            $cm->groupmode        = isset($mod->groupmode) ? $mod->groupmode : 0;
            $cm->groupingid       = isset($mod->groupingid) ? $mod->groupingid : 0;
            $cm->groupmembersonly = isset($mod->groupmembersonly) ? $mod->groupmembersonly : 0;
            $cm->coursegroupmodeforce = $course->groupmodeforce;
            $cm->coursegroupmode  = $course->groupmode;
            $cm->indent           = isset($mod->indent) ? $mod->indent : 0;
            $cm->extra            = isset($mod->extra) ? $mod->extra : '';
            $cm->extraclasses     = isset($mod->extraclasses) ? $mod->extraclasses : '';
//            $cm->iconurl          = isset($mod->iconurl) ? new moodle_url($mod->iconurl) : '';
            $cm->iconurl = '';
            $cm->onclick          = isset($mod->onclick) ? $mod->onclick : '';
            $cm->content          = isset($mod->content) ? $mod->content : '';
            $cm->icon             = isset($mod->icon) ? $mod->icon : '';
            $cm->iconcomponent    = isset($mod->iconcomponent) ? $mod->iconcomponent : '';
            $cm->customdata       = isset($mod->customdata) ? $mod->customdata : '';

//            $context = self::getTable('context');
//            $context->load(['instanceid' => $mod->cm, 'contextlevel' => MODULE_CONTEXT_LEVEL]);
//            $cm->context          = $context;

            $cm->showdescription  = isset($mod->showdescription) ? $mod->showdescription : 0;
            $cm->state = 0;

            $cm->section = isset($mod->sectionid) ? $mod->sectionid : 0;
            $cm->module = isset($mod->module) ? $mod->module : 0;
            $cm->added = isset($mod->added) ? $mod->added : 0;
            $cm->score = isset($mod->score) ? $mod->score : 0;
            $cm->visibleold = isset($mod->visibleold) ? $mod->visibleold : 0;

            $cm->completion = isset($mod->completion) ? $mod->completion : 0;
            $cm->completiongradeitemnumber = isset($mod->completiongradeitemnumber)
                ? $mod->completiongradeitemnumber : null;
            $cm->completionview = isset($mod->completionview)
                ? $mod->completionview : 0;
            $cm->completionexpected = isset($mod->completionexpected)
                ? $mod->completionexpected : 0;
            $cm->showavailability = isset($mod->showavailability) ? $mod->showavailability : 0;
            $cm->availablefrom = isset($mod->availablefrom) ? $mod->availablefrom : 0;
            $cm->availableuntil = isset($mod->availableuntil) ? $mod->availableuntil : 0;
            $cm->conditionscompletion = isset($mod->conditionscompletion)
                ? $mod->conditionscompletion : array();
            $cm->conditionsgrade = isset($mod->conditionsgrade)
                ? $mod->conditionsgrade : array();
            $cm->conditionsfield = isset($mod->conditionsfield)
                ? $mod->conditionsfield : array();

            $return[$cm->id] = $cm;
        }

        return $return;
    }

    public static function getCourseMods($conditions) {
        self::loadTables();

        $courseid = $conditions['id'];
        $username = isset($conditions['username']) ? $conditions['username'] : JFactory::getUser()->username;
        $visible = isset($conditions['visible']) ? $conditions['visible'] : 1;
        $mtype = isset($conditions['mtype']) ? $conditions['mtype'] : '';

        $db = self::getMoodleDbo();
        $sections = $db->get_records('course_sections', ['course' => $courseid]);

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $modules = self::getFastModulesInfo($courseid);
//        $cmconditions = ['course' => $courseid];
//        if (!is_null($visible)) $cmconditions['visible'] = $visible;
//        $modules = $db->get_records('course_modules', $cmconditions, '*', 'id');

        $e = [];
        foreach ($sections as $section) {
            $section = (object)$section;
            if ((!$section->visible && $section->section != 0)
//                || $section->section > $courseformatoptions['numsections']
            )
                continue;

            $e[$section->section]['section'] = $section->section;
            $e[$section->section]['visible'] = $section->visible;
            $e[$section->section]['sectionid'] = $section->id;
            $e[$section->section]['name'] = $section->name ? $section->name : 'Topic Title Here';

            $e[$section->section]['mods'] = array();

            $sectionmods = explode(",", $section->sequence);

            if (!empty($sectionmods)) {
                foreach ($sectionmods as $modnumber) {

                    if (array_key_exists($modnumber, $modules)) {

                        if (empty($modules[$modnumber])) continue;

                        $mod = (object)$modules[$modnumber];

                        if ($mod->visible != $visible) continue;
                        if ($mod->modname == 'forum') continue;
                        if ($mtype) if ($mod->modname != $mtype) continue;

                        $resource = [];
                        if ($mod->modname == 'quiz') {
                            $quiz = (object)$db->get_record('quiz', array('id' => $mod->instance));
                            $resource['atype'] = $quiz->atype;
                        }

                        $resource['completion_info'] = '';
//                        $resource['mod_completion'] = false;
                        if ($username) {
                            /*
                             *  Replace with function getModCompletionStatus();
                             * 
                            $mod_completion = $db->get_record('course_modules_completion', array('coursemoduleid' => $mod->id, 'userid' => $user->id, 'completionstate' => '1'));

                            if ($mod_completion['id']) {
                                $resource['mod_completion'] = true;
                            }
                            if ($mod->modname == 'assign') {
                                $assign = $db->get_record('assign_submission', ['assignment' => $mod->id, 'userid' => $user->id]);
                                if ($assign && $assign['status'] == 'submitted') $resource['mod_completion'] = true;
                            }*/
                            $cm = self::getTable('coursemodules');
                            $cm->load(['id' => $mod->id]);

                            $resource['available'] = 1;

                            $resource['id'] = $mod->id;
                            $resource['name'] = $mod->name;
                            $resource['mod'] = $mod->modname;

                            $type = substr($mod->icon, 2);
                            $parts = explode('-', $type);
                            $type = $parts[0];
                            $resource['type'] = $type;
                            $resource['display'] = self::get_display($mod->modname, $mod->instance);

                            switch ($mod->modname) {
                                case "questionnaire" :
                                    $thisurl = JUri::base() . 'index.php?option=com_joomdle&view=feedbackview&module_id=' . $mod->id . '&course_id=' . $courseid;
                                    break;
                                case "quiz" :
                                    $thisurl = JUri::base() . 'index.php?option=com_joomdle&view=quiz&id=' . $mod->id . '&course_id=' . $courseid;
                                    break;
                                case "page" :
                                case "assignment" :
                                case "folder" :
                                case "messages" :
                                case "assign" :
                                case "scorm" :
                                case "skillsoft" :
                                    $thisurl = JUri::base() . 'index.php?option=com_joomdle&view=wrapper&mtype=' . $mod->modname . '&course_id=' . $courseid . '&id=' . $mod->id;
                                    break;
                                default:
                                    if ($mod->modname) {
                                        $path = '/mod/' . $mod->modname . '/view.php?id=';
                                        $thisurl = JUri::base() . 'courses' . $path . $courseid;
                                        break;
                                    } else {
                                        $path = '/?a=1';
                                        $thisurl = JUri::base() . 'courses' . $path;
                                    }
                                    break;
                            }
                            $resource['url'] = urlencode($thisurl);

                            $e[$section->section]['mods'][] = $resource;
                        }
                    }
                }
            }
        }

        return $e;
    }

    public static function getModCompletionStatus($conditions, $containSubmittedAssign = true) {
        self::loadTables();
        
        $modid = $conditions['modid'];
        $username = $conditions['username'];
        $act_type = $conditions['act_type'];
        
        $db = self::getMoodleDbo();

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $resource = $conditions;
        $resource['mod_completion'] = false;
        if ($username) {
            $mod_completion = $db->get_record('course_modules_completion', array('coursemoduleid' => $modid, 'userid' => $user->id, 'completionstate' => '1'));

            if ($mod_completion['id']) {
                $resource['mod_completion'] = true;
            }
            if ($act_type == 'assign' && $containSubmittedAssign) {
                $mod = self::getCourseModuleFromId($act_type, $modid);
                $assign = $db->get_record('assign_submission', ['assignment' => $mod['instance'], 'userid' => $user->id]);
                if ($assign && $assign['status'] == 'submitted')
                    $resource['mod_completion'] = true;
            }
        }
        return $resource;
    }

    public static function getCourseQuizes($courseid, $username = '') { // tam thoi chua dung
        // based on get_courses_quizes function

        self::loadTables();
        $db = self::getMoodleDbo();

        $records = $db->get_records_sql("SELECT cs.id, cs.section, cs.summary
            FROM #__course_sections cs
            WHERE cs.course = $courseid");

        $context = self::getTable('context');
        $context->load(['instanceid' => $courseid, 'contextlevel' => COURSE_CONTEXT_LEVEL]);

        if ($username) {
            $user = self::getTable('user');
            $user->load(['username' => $username]);
        }

        $i = 0;
        $data = array ();

        foreach ($records as $r) {
            $r = (object)$r;
            $e['section'] = $r->section;
            $e['summary'] = $r->summary;

            $query =
                "SELECT cm.id, q.name, q.atype, q.attempts, q.id as quiz_id, q.questions, q.timelimit, q.intro
                FROM
                #__course_modules cm, #__quiz q, #__modules m
                WHERE
                cm.instance = q.id and cm.module = m.id and m.name = 'quiz'
                and cm.course = $courseid
                and cm.section = $r->id ";

            $records_cm =  $db->get_records_sql($query);

            $resources = [];

            foreach ($records_cm as $r_cm) {
                $r_cm = (object)$r_cm;
                $resource['grade'] = (float) 0;
                $resource['passed'] = false;
                if ($username) {
                    $cm = self::getCourseModuleFromId(false, $r_cm->id);
                    /* can xu ly lai
                    if (!coursemodule_visible_for_user ($cm, $user->id))
                        continue;

                    $grades = grade_get_grades ($courseid, 'mod', 'quiz', $r_cm->quiz_id, $user->id);
                    $grade = array_shift ($grades->items[0]->grades);

                    $resource['grade'] = (float) $grade->grade;
                    if ($grade->grade == $grades->items[0]->grademax)
                        $resource['passed'] = true;
                    else */
                        $resource['passed'] = false;
                }

                $resource['id'] = $r_cm->id;
                $resource['name'] = $r_cm->name;
                $resource['intro'] = $r_cm->intro;
                $resource['timelimit'] = $r_cm->timelimit;
                $resource['quiz_id'] = $r_cm->quiz_id;
                $resource['questions'] = $r_cm->questions;
                $resource['atype'] = $r_cm->atype;
                $resource['numOfAttempts'] = (int)$r_cm->attempts;
                //===================================================
                $time_close_open = $db->get_records_sql("SELECT timeopen, timeclose FROM #__quiz  WHERE id = ".$r_cm->quiz_id);
                if (count($time_close_open) > 0) {
                    foreach ($time_close_open as $key ) {
                        $resource['timeopen'] = $key['timeopen'];
                        $resource['timeclose'] = $key['timeclose'];
                    }
                } else {
                    $resource['timeopen'] = 0;
                    $resource['timeclose'] = 0;
                }
                // =======================================================
                $resource['attempts'] = array();

                $record_quiz_time = $db->get_records_sql("SELECT id, state, timestart, timefinish FROM #__quiz_attempts  WHERE quiz = ".$r_cm->quiz_id." AND userid = ".$user->id);
                if ($record_quiz_time != null) {
                    foreach ($record_quiz_time as $key ) {
                        $resource['attempt'] = $key['id'];
                        $resource['time']['state'] = $key['state'];
                        $resource['time']['timestart'] = $key['timestart'];
                        $resource['time']['timefinish'] = $key['timefinish'];
                        $attempt = new stdClass();
                        $attempt->id = $key['id'];
                        $attempt->state = $key['state'];
                        $attempt->timestart = $key['timestart'];
                        $attempt->timefinish = $key['timefinish'];
                        $resource['attempts'][] = $attempt;
                    }
                } else {
                    $resource['attempt'] = 0;
                    $resource['time']['state'] = '';
                    $resource['time']['timestart'] = 0;
                    $resource['time']['timefinish'] = 0;
                }
                $resource['attempts'] = json_encode($resource['attempts']);
                // =======================================================
                $resources[] = $resource;
            }

            $e['quizes'] = $resources;
            $data[$i] = $e;
            $i++;
        }

        return $data;
    }

    public static function getAssignmentActivityDetail($courseid, $username, $aid) {
        // based on get_assignment_activity_detail function

        self::loadTables();
        $db = self::getMoodleDbo();

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $allCourseModules = self::getAllModulesByCourseId($courseid, [], 'id', 'id');
        if (!$db->record_exists("course_modules", array("id"=>$aid )) || !array_key_exists($aid, $allCourseModules)) {
            $return = array(
                'status' => false,
                'message' => 'No exist module.'
            );
            return $return;
        }

        $cm = self::getCourseModuleFromId('', $aid, 0, false);

        $data = $db->get_record_sql("SELECT * FROM #__assign WHERE id =" . $cm['instance']);

        $course_modules_info = $db->get_record('course_modules', array('id'=>$aid), 'section');
        $course_section_info = $db->get_record('course_sections', array('id'=>$course_modules_info['section']), 'name');

        $course_section_info['name'] =  $course_section_info['name'] ?  $course_section_info['name'] : 'Topic title here';

        // get link download brief(**)
        $context = self::getTable('context');
        $context->load(['instanceid' => $aid, 'contextlevel' => MODULE_CONTEXT_LEVEL]);

        $link_dowload_brief = '';
        if (!empty($data['params'])) {
            $prs = json_decode($data['params']);
            $itemid = $prs->itemid;
            $fname = $prs->fname;
            $link_dowload_brief = JUri::base() . 'courses/pluginfile.php/' . $context->id . '/mod_assign/brief/' . $itemid . '/' . $fname;
            if ($itemid == 0 || $fname == '') {
                $link_dowload_brief = '';
            }
        }

        // Check status grade and status submission(***)
        $grade1 = self::getModProgress($courseid, $username);
        $check_grade = false;
        foreach ($grade1['sections'] as $section){
            foreach ($section['mods'] as $mod){
                if($aid == $mod['id']){
                    if($mod['finalgradeletters'] != 'Ungraded' && $mod['gradeid'] > 0){
                        $check_grade = true;
                    }
                }
            }
        }

        $submission = $db->get_records_sql( "SELECT * FROM #__assign_submission WHERE assignment= " . $data['id'] . " AND userid = $user->id");
        if (!empty($submission)) {
            foreach ($submission as $sb) {
                if ($sb['status'] == 'submitted') {
                    $submission_status = 'Submitted';
                    if ($check_grade) {
                        $grade_status = "Completed";
                    } else {
                        $grade_status = "In Progress";
                    }
                } else {
                    $submission_status = 'Not Yet Submitted';
                    $grade_status = "Not Yet Graded";
                }
            }
        } else {
            $submission_status = 'Not Yet Submitted';
            if ($check_grade) {
                $grade_status = "Completed";
            } else {
                $grade_status = "Not Yet Graded";
            }
        }

        $data_json = json_encode($data);
        $return = array(
            'status' => true,
            'message' => 'Success.',
            'assign_information' => $data_json,
            'topic_name' => $course_section_info['name'],
            'submission_status'=> $submission_status,
            'grade_status'=> $grade_status,
            'link_download_brief'=> $link_dowload_brief
        );
        return $return;
    }

    public static function getCoursePageData($conditions) {
        // based on get_course_page_data function

        self::loadTables();
        $db = self::getMoodleDbo();

        $courseid = $conditions['id'];
        $username = $conditions['username'] ? $conditions['username'] : JFactory::getUser()->username;
        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $userRoles = self::getUserRole( $conditions );

//        $course_info = self::getCourseInfo($conditions);
        $course_info = self::getCourseInforSimple($conditions);
        
        
        $mods = self::getCourseMods($conditions);

        /*
        $check_table_course_completion_criteria = $db->get_record_sql(
            "SELECT id FROM #__course_completion_criteria WHERE course = " . $course_info['remoteid'] . " AND module = 'questionnaire'"
        );*/
        $check_table_course_completion_criteria = $db->get_record_sql(
            "SELECT id FROM #__course_completion_criteria WHERE course = " . $courseid . " AND module = 'questionnaire'"
        );

        if ($check_table_course_completion_criteria) {
            $db->delete_records('course_completion_criteria', array('id' => $check_table_course_completion_criteria['id']));
        }

        self::updateLastAccessCourse($conditions);

        $a = json_decode($userRoles["roles"][0]['role'], true);
        $user_role = $a[0]['sortname'];

        $modsForCreator = array();
        $questions = array();
        $topics = array();
        if ($user_role != 'student' && $user_role != 'teacher') {
            $topics = self::getCourseSections($courseid);

            $questions = self::load_questions_bank($courseid, null, 1);
            $validModules = ['page', 'quiz', 'assign', 'scorm', 'questionnaire'];

            if (is_array($mods)) {
                foreach ($mods as $tema) {
                    if (is_array($tema['mods'])) {
                        foreach ($tema['mods'] as $key => $resource) {
                            if (in_array($resource['mod'], $validModules)) {
                                $moduleInfo = self::getModuleInfo($resource['mod'], $resource['id'], $courseid);
                                $modsForCreator[] = array('id' => $resource['id'], 'type' => $resource['mod'], 'data' => $moduleInfo);
                            }
                        }
                    }
                }
            }
        }

//        $completeFeedback = false;
        /*
        if ($user_role == 'student') {
            $allCourseModules = self::getAllModulesByCourseId($courseid, [], 'id', 'id');
            foreach($allCourseModules as $cm) {
                if ($cm['modname'] == 'questionnaire') $questionnaireId = $cm['id'];
            }
            $moduleInfo = self::getModuleInfo('questionnaire', $questionnaireId, $courseid);

            $checkCompleteFeedback = $db->get_record_sql(
                "SELECT * FROM #__questionnaire_response WHERE survey_id = " . $moduleInfo['surveyid'] .
                " AND username = " . $user->id
            );

            $completeFeedback = $checkCompleteFeedback ? true : false;
        }*/

        /*
        $progressmods = self::getModProgress($courseid, $username);
         */

        $result = array(
            'userRoles' => json_encode($userRoles),
            'course_info' => json_encode($course_info),
            'mods' => json_encode($mods),
//            'progressmods' => json_encode($progressmods),
            'topics' => json_encode($topics),
            'modsForCreator' => json_encode($modsForCreator),
            'questions' => json_encode($questions),
//            'checkCompleteFeedback' => $completeFeedback
        );
        return $result;
    }

    
    public static function checkCompleteFeedback($conditions) {
        self::loadTables();
        $db = self::getMoodleDbo();
        
        $courseid = $conditions['courseid'];
        $username = $conditions['username'];
        
        $user = self::getTable('user');
        $user->load(['username' => $username]);
        
        $allCourseModules = self::getAllModulesByCourseId($courseid, [], 'id', 'id');
        foreach ($allCourseModules as $cm) {
            if ($cm['modname'] == 'questionnaire')
                $questionnaireId = $cm['id'];
        }
        $moduleInfo = self::getModuleInfo('questionnaire', $questionnaireId, $courseid);

        $checkCompleteFeedback = $db->get_record_sql(
                "SELECT * FROM #__questionnaire_response WHERE survey_id = " . $moduleInfo['surveyid'] .
                " AND username = " . $user->id
        );

        $completeFeedback = $checkCompleteFeedback ? 'true' : 'false';
        
        return $completeFeedback;
    }

    
    public static function checkCoursePercent($conditions) {
        self::loadTables();
        $db = self::getMoodleDbo();
        
        $username = $conditions['username'];
        $courseid = $conditions['courseid'];
        
        $mods = self::getCourseMods(['id'=>$courseid, 'username' => $username]);
        
        $count_compled = 0;
        $count_activity = 0;
        if (is_array($mods)) {
            foreach ($mods as $tema) {
                    if (is_array($tema['mods'])) {
                        foreach ($tema['mods'] as $id => $resource) {
                            if (($resource['mod'] != 'forum') && ($resource['mod'] != 'certificate') && ($resource['mod'] != 'feedback') && ($resource['mod'] != 'questionnaire')) {
                                $mod_completion = self::getModCompletionStatus(['modid' => $resource['id'], 'username' => $username, 'act_type' => $resource['mod']]);
                                if($mod_completion['mod_completion'] == true) {
                                    $count_compled++;
                                }
                                $count_activity++;
                            }
                        }
                    }
            }
        }
        $num_progress_bar = intval(($count_compled / $count_activity) * 100);
        return $num_progress_bar;
    }

    public static function getModProgress($courseid, $username) {
        // based on get_mod_progress function

        self::loadTables();
        $db = self::getMoodleDbo();

//        $username = JFactory::getUser()->username;
        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $e = array ();
        $sql = "SELECT cm.id, COUNT('x') AS numviews, MAX(time) AS lasttime
                              FROM #__course_modules cm
                                   JOIN #__modules m ON m.id = cm.module
                                   JOIN #__log l     ON l.cmid = cm.id
                             WHERE cm.course = $courseid AND l.action LIKE 'view%' AND m.visible = 1 AND l.userid = $user->id
                          GROUP BY cm.id";
        $views = $db->get_records_sql($sql, 'id');

        //        $modinfo = get_fast_modinfo($id);

//        $sections = $modinfo->get_section_info_all();
        $sections = $db->get_records('course_sections', ['course' => $courseid], 'id, course, section, name, summary, summaryformat, sequence, visible', 'id');

//        $courseformatoptions = course_get_format($id)->get_format_options();
//
//        $mods = get_fast_modinfo($id)->get_cms();
        $mods = self::getFastModulesInfo($courseid);

//        $coursename = $modinfo->get_course()->fullname;
        $courseInfo = self::getCourseInfo(['id' => $courseid]);
        $coursename = $courseInfo['fullname'];
        $e['status'] = true;
        $e['message'] = 'success.';
        $e['coursename'] = $coursename;

        $learneridarr = [];
        $countlearner = 0;
        $userRole = self::getUserRole(['id' => $courseid]);

        $userroles = $userRole['status'] ? $userRole['roles'] : [];

        if (!empty($userroles)) {
            foreach ($userroles as $userrole) {
                $countrole = count(json_decode($userrole['role']));
                foreach (json_decode($userrole['role']) as $role) {
                    if ($role->roleid == 5 && $countrole == 1) {
                        $countlearner++;
                        $user_l = self::getTable('user');
                        $user_l->load(['username' => $userrole['username']]);
                        $learneridarr[] = $user_l->id;
                    }
                }

            }
        }
        $e['count_learner'] = $countlearner;

        if (count($learneridarr) > 1) {
            $learnerid = implode(',', $learneridarr);
            $wh = ' AND userid IN ('.$learnerid.')';
        } else if (count($learneridarr) == 1) {
            $wh = ' AND userid = '.$learneridarr[0];
        } else {
            $wh = '';
        }

        if ($wh != '') {
            // $query_com = "SELECT userid FROM #__course_completions WHERE course = $courseid AND status = 50 $wh";
            // $result_com = $db->count_records_sql($query_com);

            $e['count_completed'] = count(self::getCourseStudents($courseid, 'complete'));
        } else {
            $e['count_completed'] = 0;
        }

        $dataquizs = self::getCourseQuizes($courseid, $username);
        $grades = self::getCourseGrades ($courseid, $username);

        if (!empty($grades)) {
            $e['gradeid']  = (int)$grades[0]['gradeid'];
            $e['gradeitemid'] = (int)$grades[0]['gradeitemid'];
            $e['grademax'] = (float)$grades[0]['grademax'];
            $e['gradefeedback'] = (string)$grades[0]['gradefeedback'];
            $e['finalgrade'] = (float)$grades[0]['finalgrade'];
            $e['finalgradeletters'] = (string)$grades[0]['finalgradeletters'];
            $e['hidden'] = (int)$grades[0]['hidden'];
            $e['timemodified'] = (int)$grades[0]['timemodified'];
            if ($grades[0]['usermodified'] != null){
                $e['gradeoffacitator'] = true;
            } else {
                $e['gradeoffacitator'] = false;
            }

            $e['gradeoptionsletters'] = $grades[0]['gradeoptionsletters'];

        }
        $e['sections'] = array();

        foreach ($sections as $section) {
            $section = (object)$section;
            if ((!$section->visible && $section->section != 0))
                continue;

            $sectionmods = explode(",", $section->sequence);
            $e['sections'][$section->section]['sectionname'] = $section->name ? $section->name : 'Topic Title Here';
            $e['sections'][$section->section]['mods'] = array();
            $imods = 0;

            foreach ($sectionmods as $modnumber) {
                if (empty($mods[$modnumber])) {
                    continue;
                }
                $mod = (object)$mods[$modnumber];

                $cm = (object)self::getCourseModuleFromId(false, $mod->id);
                if ($mod->modname == 'forum' || $mod->modname == 'certificate' || $mod->modname == 'questionnaire') {
                    continue;
                }
                $imods++;
//                if (is_null($cm->completiongradeitemnumber)) continue;

                $resource = [];

                if ($mod->visible == 1) {
                    $resource['completionusegrade'] = is_null($cm->completiongradeitemnumber) ? 0 : 1;

                    $resource['mod_completion'] = false;
                    $resource['mod_completion_date'] = '';
                    if ($username) {
                        $mod_completion = $db->get_record('course_modules_completion', array('coursemoduleid' => $mod->id, 'userid' => $user->id, 'completionstate' => '1' ));
                        if ($mod_completion) {
                            $resource['mod_completion'] = true;
                            $resource['mod_completion_date'] = date('d/m/Y', $mod_completion['timemodified']);
                        }

                        if ($mod->modname == 'assign') {
                            $assign = $db->get_record('assign_submission', ['assignment' => $mod->instance, 'userid' => $user->id]);
                            if ($assign && $assign['status'] == 'submitted') $resource['mod_completion'] = true;
                        }
                        
//                        if (!coursemodule_visible_for_user($cm, $user->id)) {
//                            if (!$mod->showavailability)
//                                continue;
//
//                            $resource['available'] = 0;
//                        } else
                        $resource['available'] = 1;
                    } else
                        $resource['available'] = 1;

                    // Check learner completed
                    if ($wh != '') {
                        $result2 = $db->count_records_sql("SELECT userid FROM #__course_modules_completion WHERE coursemoduleid = $mod->id AND completionstate = 1 $wh");
                        $resource['totalcompleted'] = $result2;

                        if ($mod->modname == 'assign') {
                            $assigns = $db->get_records('assign_submission', ['assignment' => $mod->instance]);

                            if (!empty($assigns)) {
                                foreach ($assigns as $key => $value) {
                                    if (!$db->record_exists('course_modules_completion', array('coursemoduleid' =>
                                        $mod->id, 'userid' => $value['userid']))) {
                                        $resource['totalcompleted']++;
                                    }
                                }
                            }
                        }
                    } else {
                        $resource['totalcompleted'] = 0;
                    }

                    $resource['id'] = $mod->id;
                    $resource['name'] = $mod->name;
                    $resource['mod'] = $mod->modname;

                    $type = substr($mod->icon, 2);
                    $parts = explode('-', $type);
                    $type = $parts[0];
                    $resource['type'] = $type;
                    $resource['mod_lastaccess'] = '';
                    if (isset($views[$cm->id]->lasttime)) {
                        $resource['mod_lastaccess'] = date('d/m/Y', $views[$cm->id]->lasttime);
                    }

                    $cat_item = $db->get_record('grade_items', array('courseid' => $courseid, 'itemmodule' => $mod->modname, 'iteminstance' => $cm->instance));

                    if ($cat_item['id']) {
                        $grade = $db->get_record_sql(
                            "SELECT g.finalgrade, g.rawgrademax, g.feedback, g.id, g.hidden
                        FROM #__grade_grades g
                        WHERE g.itemid = " . $cat_item['id'] . "
                        AND g.userid = $user->id");

                        if ($cat_item['grademax'] == $cat_item['grademin']) {
                            $value = 100;
                        } else {
                            $factor = ($grade['finalgrade'] - $cat_item['grademin']) / ($cat_item['grademax'] - $cat_item['grademin']);
                            $diff = 100 - 0;
                            $standardised_value = $factor * $diff + 0;
                            $value = $standardised_value;
                        }

                        $grade_letter = '-';
                        $letters = self::grade_get_letters();
                        foreach ($letters as $boundary => $letter) {
                            if ($value >= $boundary) {
                                $grade_letter = $letter;
                            }
                        }

                        $resource['finalgradeletters'] = $grade_letter;
                        $resource['gradeid'] = (int)$grade['id'];
                        $resource['gradeitemid'] = (int)$cat_item['id'];
                        $resource['finalgrade'] = (float)$grade['finalgrade'];
                        $resource['hidden'] = (int)$grade['hidden'];
                        $resource['grademax'] = (float)$grade['rawgrademax'];
                        $resource['feedback'] = (string)$grade['feedback'];
                    }

                    $letter_option = [];

                    if ($mod->modname == 'assign') {
                        $submission = $db->get_records_sql( "SELECT * FROM #__assign_submission WHERE assignment = " . $cm->instance . " AND userid = $user->id");

                        if (!empty($submission)) {
                            foreach ($submission as $sb) {
                                if ($sb['status'] == 'submitted') {
                                    $submission_status = 'Submitted';
                                } else {
                                    $submission_status = 'Not Yet Submitted';
                                }
                            }
                        } else {
                            $submission_status = 'Not Yet Submitted';
                        }
                        $resource['submission_status'] = $submission_status;
                    }

                    if ($mod->modname == 'quiz') {
                        foreach ($dataquizs as $key => $value) {
                            foreach ($value['quizes'] as $k => $v) {
                                if ($v['id'] != $mod->id) continue; else $dataquiz = $v;
                            }
                        }
                        $intro_quiz = $dataquiz['intro'];
                        $resource['attempt_quiz']  = $dataquiz['attempt'];
                        $numOfAttempts = $dataquiz['numOfAttempts'];
                        $attempts = json_decode($dataquiz['attempts'], true);
                        $quizFinished = true;
                        $firstAttempt = false;
                        $inProgress = false;
                        $reAttempt = false;
                        if (!empty($attempts)) {
                            if (count($attempts) == 1) $firstAttempt = true;
                            foreach ($attempts as $key => $value) {
                                if ($value['state'] != "finished") {
                                    $quizFinished = false;
                                    $inProgress = true;
                                }
                            }
                            if (count($attempts) != $numOfAttempts) {
                                $quizFinished = false;
                                if (!$inProgress) $reAttempt = true;
                            }
                        } else {
                            $quizFinished = false;
                            $firstAttempt = true;
                        }
                        $resource['firstAttempt'] = $firstAttempt;
                        $resource['inProgress'] = $inProgress;
                        $resource['quizFinished'] = $quizFinished;

                        $quizgrades = $db->get_records_sql("SELECT lowerboundary, letter FROM #__quiz_grade_letters WHERE moduleid = $mod->id");
                        $quiz = $db->get_record_sql("SELECT grade FROM #__quiz_grades WHERE quiz = $mod->instance AND userid = $user->id");
                        $resource['finalgrade'] = (float)$quiz['grade'];

                        if ($quizgrades) {
                            $resource['gradingscheme'] = 1;
                            foreach ($quizgrades as $letter) {
                                $letter_scheme = array();
                                $letter_scheme['value'] = (int) $letter['lowerboundary'];
                                $letter_scheme['name'] = $letter['letter'];
                                $letter_option[] = $letter_scheme;
                            }
                        } else
                            $resource['gradingscheme'] = 0;
                    }

                    $resource['lettergradeoption'] = $letter_option ;
                    $e['sections'][$section->section]['mods'][] = $resource;
                }
            }
            if ($section->section == 0 && $imods == 0) {
                unset($e['sections'][$section->section]);
            }
        }
        return $e;
    }

    public static function getCourseGrades($courseid, $username) {
        // based on get_course_grades_by_category function

        self::loadTables();
        $db = self::getMoodleDbo();

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $letters = self::grade_get_letters();

        $cat_item = $db->get_record_sql("SELECT id, grademin, grademax
            FROM #__grade_items
            WHERE courseid = '$courseid'
            AND itemtype = 'course'
        ");

        $grade = $db->get_record_sql("SELECT g.finalgrade, g.rawgrademax, g.feedback, g.id, g.hidden, g.userid, g.usermodified
            FROM #__grade_grades g
            WHERE g.itemid = " . $cat_item['id'] . "
            AND g.userid = $user->id");

        if ($cat_item['grademax'] == $cat_item['grademin']) {
            $value = 100;
        } else {
            $factor = ($grade['finalgrade'] - $cat_item['grademin']) / ($cat_item['grademax'] - $cat_item['grademin']);
            $diff = 100 - 0;
            $standardised_value = $factor * $diff + 0;
            $value = $standardised_value;
        }

        $grade_letter = '-';
        foreach ($letters as $boundary => $letter) {
            if ($value >= $boundary) {
                $grade_letter = $letter;
            }
        }

        $total['fullname'] = '';
        $total['userid'] = (int) $grade['userid'];
        $total['usermodified'] = (int) $grade['usermodified'];
        $total['gradeitemid'] = (int) $cat_item['id'];
        $total['gradeid'] = (int) $grade['id'];
        $total['finalgrade'] = (float) $grade['finalgrade'];
        $total['finalgradeletters'] = $grade_letter;
        $total['grademax'] = (float) $grade['rawgrademax'];
        $total['gradefeedback'] = $grade['feedback'];
        $total['hidden'] = (int)$grade['hidden'];
        if ($total['usermodified'] != null) {
            $total['gradeoffacitator'] = true;
        } else {
            $total['gradeoffacitator'] = false;
        }
        $total['items'] = array();

        $items = $db->get_records_sql("SELECT gi.id, gi.itemname, gi.grademax, gi.itemmodule, gi.iteminstance
            FROM #__grade_categories gc JOIN #__grade_items gi ON gc.id = gi.categoryid
            WHERE gc.courseid = '$courseid' AND gc.depth = 1 AND gc.hidden = 0");

        $category_items = [];
        foreach ($items as $item) {
            $item = (object)$item;
            $category_item['name'] = $item->itemname;
            $category_item['grademax'] = (float)$item->grademax;

            switch ($item->itemmodule) {
                case 'quiz':
                    $quiz = $db->get_record('quiz', ['id' => $item->iteminstance]);
                    $category_item['due'] = $quiz['timeclose'];
                    break;
                case 'assignment':
                    $assignment = $db->get_record('assignment', ['id' => $item->iteminstance]);
                    $category_item['due'] = $assignment['timedue'];
                    break;
                default:
                    $category_item['due'] = 0;
                    break;
            }

            $grade = $db->get_record_sql("SELECT g.finalgrade, g.feedback
                FROM #__grade_grades g
                WHERE g.itemid = $item->id
                AND g.userid = $user->id");

            if ($grade) {
                $category_item['finalgrade'] = (float) $grade['finalgrade'];
                $category_item['feedback'] = $grade['feedback'];
            } else {
                $category_item['finalgrade'] = (float) 0;
                $category_item['feedback'] = '';
            }

            $category_items[] = $category_item;
        }
        if (!empty($category_items)) $total['items'] = $category_items;

        $letter_option = array();
        foreach ($letters as $key => $value) {
            $letter_option[] = [
                'value' => $key,
                'name' => $value
            ];
        }

        $total['gradeoptionsletters'] = $letter_option;

        $data = [];
        $data[] = $total;
        
        $query =
            "SELECT
            #__grade_categories.id AS grade_cat_id,
            #__grade_categories.fullname AS grade_cat_fullname,
            #__grade_items.id AS grade_item_id,
            #__grade_items.grademax AS grade_item_grademax
            FROM #__grade_categories, #__grade_items
            WHERE #__grade_categories.id = #__grade_items.iteminstance
            AND #__grade_items.courseid = '$courseid'
            AND #__grade_items.itemtype = 'category'
            ";

        $cats = $db->get_records_sql($query);

        foreach ($cats as $r) {
            $r = (object)$r;
            $e['fullname'] = $r->grade_cat_fullname;
            $e['grademax'] = (float) $r->grade_item_grademax;
            $e['gradeitemid'] = (int) $r->grade_item_id;

            $cat_id = $r->grade_cat_id;

            $cat_item = $db->get_record_sql("SELECT id, grademin, grademax
                FROM  #__grade_items
                WHERE iteminstance = '$cat_id'
                ");

            $grade = $db->get_record_sql(
                "SELECT g.finalgrade
                FROM #__grade_grades g
                WHERE g.itemid = " . $cat_item['id'] . "
                AND g.userid = $user->id");
            $e['finalgrade'] = (float) $grade['finalgrade'];
            $e['gradeid'] = (int) $grade['id'];
            $e['gradefeedback'] = $grade['feedback'];

            if ($cat_item['grademax'] == $cat_item['grademin']) {
                $value = 100;
            } else {
                $factor = ($grade['finalgrade'] - $cat_item['grademin']) / ($cat_item['grademax'] - $cat_item['grademin']);
                $diff = 100 - 0;
                $standardised_value = $factor * $diff + 0;
                $value = $standardised_value;
            }

            $grade_letter = '-';
            foreach ($letters as $boundary => $letter) {
                if ($value >= $boundary) {
                    $grade_letter = $letter;
                }
            }

            $e['finalgradeletters'] = $grade_letter;

            $items = $db->get_records_sql("SELECT *
                FROM  #__grade_items
                WHERE categoryid = '$cat_id'
                ");
            $category_items = [];

            foreach ($items as $item) {
                $item = (object)$item;
                $category_item['name'] = $item->itemname;
                $category_item['grademax'] = (float)$item->grademax;

                switch ($item->itemmodule) {
                    case 'quiz':
                        $quiz = $db->get_record('quiz', ['id' => $item->iteminstance]);
                        $category_item['due'] = $quiz['timeclose'];
                        break;
                    case 'assignment':
                        $assignment = $db->get_record('assignment', ['id' => $item->iteminstance]);
                        $category_item['due'] = $assignment['timedue'];
                        break;
                    default:
                        $category_item['due'] = 0;
                        break;
                }

                $grade = $db->get_record_sql("SELECT g.finalgrade, g.feedback
                    FROM #__grade_grades g
                    WHERE g.itemid = $item->id
                    AND g.userid = $user->id");

                if ($grade) {
                    $category_item['finalgrade'] = (float) $grade['finalgrade'];
                    $category_item['feedback'] = $grade['feedback'];
                } else {
                    $category_item['finalgrade'] = (float) 0;
                    $category_item['feedback'] = '';;
                }

                $category_items[] = $category_item;
            }

            $e['items'] = $category_items;
            $e['gradeoptionsletters'] = $letter_option;

            $data[] = $e;
        }

        return $data;
    }

    public static function grade_get_letters() {
        return ['93'=>'A', '90'=>'A-', '87'=>'B+', '83'=>'B', '80'=>'B-', '77'=>'C+', '73'=>'C', '70'=>'C-', '67'=>'D+', '60'=>'D', '0'=>'F'];
    }

    public static function getCourseSections($courseid) {
        // based on update_course_section function case 0;

        self::loadTables();
        $db = self::getMoodleDbo();

        $sections = $db->get_records('course_sections', ['course' => $courseid], 'id, course, section, name, summary, summaryformat, sequence, learningoutcomes', 'id');

        $response = array(
            'status' => 1,
            'message' => 'Success',
            'datas' => array(
                'res' => 1,
                'numsections' => -1,
                'sections' => $sections
            )
        );
        return $response;
    }

    public static function getCourseStudents($id, $completed = '', $search = '', $limitfrom = '', $limitnum = '') {
        // based on get_course_students function

        self::loadTables();
        $db = self::getMoodleDbo();

        $context = self::getTable('context');
        $context->load(['instanceid' => $id, 'contextlevel' => COURSE_CONTEXT_LEVEL]);

        $notStudentsIDs = [];
        $enrolledUsersIDs = [];
        $enrolledUsers = self::getEnrolledUsers(['id' => $id]);

        foreach ($enrolledUsers as $k => $v) {
            $enrolledUsersIDs[] = $v->userid;
            if ($v->shortname != 'student') $notStudentsIDs[] = $v->userid;
        }

        $students = $studentsIDs = array_diff($enrolledUsersIDs, $notStudentsIDs);

        if ($completed) {
            $completedUserIDs = $db->get_records('course_completions', ['status' => 50, 'course' => $id], 'userid');

            if (!empty($completedUserIDs)) {
                $completedUserIDs = array_map(function($value) {
                    return $value['userid'];
                }, $completedUserIDs);
            } else 
                $completedUserIDs = [];

            // if ($completed == 'complete') {
                if (!$completedUserIDs) {
                    $completedStudents = [];
                    $notCompletedStudents = $studentsIDs;
                } else {
                    $completedStudents = array_intersect($studentsIDs, $completedUserIDs);
                    $notCompletedStudents = array_diff($studentsIDs, $completedUserIDs);
                }

                $assignModule = $db->get_record('modules', ['name' => 'assign', 'visible' => 1], 'id');
                if (isset($assignModule['id']) && !empty($notCompletedStudents) && $db->record_exists('course_modules', array('course' => $id, 'module' => $assignModule['id']))) {

                    $allCourseModules = self::getAllModulesByCourseId($id, ['visible' => 1], 'id, instance', 'id');

                    foreach ($notCompletedStudents as $key => $value) {
                        $u = self::getTable('user');
                        $u->load($value);

                        $courseCompleted = true;
                        if ($allCourseModules) {
                            foreach ($allCourseModules as $modid => $mod) {
                                if (!in_array($mod['modname'], ['assign', 'page', 'quiz', 'scorm'])) continue;

                                $thisModCompleted = false;

                                $mod_completion = $db->get_record('course_modules_completion', array('coursemoduleid' => $mod['id'], 'userid' => $u->id, 'completionstate' => '1' ));
                                if ($mod_completion) {
                                    $thisModCompleted = true;
                                    continue;
                                }

                                if ($mod['modname'] == 'assign') {
                                    $assign = $db->get_record('assign_submission', ['assignment' => $mod['instance'], 'userid' => $u->id]);
                                    if ($assign && $assign['status'] == 'submitted') 
                                        $thisModCompleted = true;
                                    else 
                                        $thisModCompleted = false;
                                }
                                if (!$thisModCompleted) {
                                    $courseCompleted = false;
                                    break;
                                }
                            }
                        }                        

                        if ($courseCompleted) {
                            $completedStudents[] = $u->id;
                            $completedUserIDs[] = $u->id;
                        }
                    }
                }
            // }

            if ($completed == 'notcomplete') {
                if (!empty($completedUserIDs))
                    $students = array_diff($studentsIDs, $completedUserIDs);
            } else {
                $students = $completedStudents;
            }
        }

        if ($students) {
            foreach ($students as $k => $v) {
                $user = self::getTable('user');
                $user->load($v);
                $student = [];

                $student['username'] = $user->username;
                $student['firstname'] = $user->firstname;
                $student['lastname'] = $user->lastname;
                $student['id'] = $user->id;

                if ($search) {
                    if ( (stripos ($user->username, $search) === false)
                        && ( stripos ($user->firstname, $search) === false)
                        && (stripos ($user->lastname, $search) === false)
                        && (stripos ($user->idnumber, $search) === false)
                    ) {
                        unset($students[$k]);
                        continue;
                    }
                }

                $students[$k] = $student;
            }
        }

        return ($students);
    }

    public static function getModuleInfo($mtype, $cmid, $courseid = 0) {
        // based on get_info_questionnaire, .....

        self::loadTables();
        $db = self::getMoodleDbo();

        if ($courseid) {
            $allCourseModules = self::getAllModulesByCourseId($courseid, [], 'id', 'id');
            if (!array_key_exists($cmid, $allCourseModules)) {
                return array(
                    'error' => 1,
                    'mes' => 'noexistmodule'
                );
            }
        }

        $cm = self::getCourseModuleFromId('', $cmid, $courseid, false);
        $data = (object)$db->get_record_sql("SELECT * FROM #__" . $mtype . " WHERE id = " . $cm['instance']);

        $return = [];
        switch ($mtype) {
            case 'page':
                $return = array(
                    'error' => 0,
                    'pid' => $data->id,
                    'courseid' => $data->course,
                    'name' => $data->name,
                    'des' => $data->intro,
                    'desformat' => $data->introformat,
                    'content' => $data->content,
                    'contentformat' => $data->contentformat,
                    'params' => $data->params
                );
                break;
            case 'quiz':
                $quizgrades = $db->get_records_sql("SELECT * FROM #__quiz_grade_letters WHERE moduleid = $cmid");

                if ($quizgrades) {
                    foreach ($quizgrades as $quizgrade) {
                        if ($quizgrade['letter'] == 'A') {
                            $gradeA = $quizgrade['lowerboundary'];
                        }
                        if ($quizgrade['letter'] == 'B') {
                            $gradeB = $quizgrade['lowerboundary'];
                        }
                        if ($quizgrade['letter'] == 'C') {
                            $gradeC = $quizgrade['lowerboundary'];
                        }
                        if ($quizgrade['letter'] == 'D') {
                            $gradeD = $quizgrade['lowerboundary'];
                        }
                    }
                }

                $return = array(
                    'status' => true,
                    'message' => 'success',
                    'res' => 1,
                    'aid' => $data->id,
                    'courseid' => $data->course,
                    'name' => $data->name,
                    'atype' => $data->atype,
                    'des' => $data->intro,
                    'desformat' => $data->introformat,
                    'timeopen' => $data->timeopen,
                    'timeclose' => $data->timeclose,
                    'timelimit' => $data->timelimit,
                    'attempts' => $data->attempts,
                    'questions' => $data->questions,
                    'gradingscheme' => ($data->reviewmarks == 0) ? $data->reviewmarks : 1,
                    'gradeboundaryA' => !empty($gradeA) ? (int)$gradeA : 0,
                    'gradeboundaryB' => !empty($gradeB) ? (int)$gradeB : 0,
                    'gradeboundaryC' => !empty($gradeC) ? (int)$gradeC : 0,
                    'gradeboundaryD' => !empty($gradeD) ? (int)$gradeD : 0,
                );
                break;
            case 'assign':
                $return = array(
                    'error' => 0,
                    'aid' => $data->id,
                    'courseid' => $data->course,
                    'name' => $data->name,
                    'des' => $data->intro,
                    'desformat' => $data->introformat,
                    'duedate' => $data->duedate,
//                        'timezoneOffset' => $data->duedate,
                    'params' => $data->params
                );
                break;
            case 'questionnaire':
                $qk2 = $db->get_records_sql("SELECT *
                    FROM #__questionnaire_question
                    WHERE survey_id = " . $data->sid);

                $qk3 = $db->get_records_sql("SELECT *
                  FROM #__questionnaire_question_type ");

                $questions = [];
                if (count($qk2) > 0) {
                    foreach ($qk2 as $value) {
                        $value = (object)$value;

                        $tt['id'] = $value->id;
                        $tt['type_id'] = $value->type_id;
                        $tt['type_ques'] = $value->type_id;
                        foreach ($qk3 as $type) {
                            if ($tt['type_id'] == $type['typeid']) {
                                $tt['type_ques'] = $type['type'];
                            }
                        }
                        $tt['name'] = $value->name;
                        $tt['content'] = $value->content;
                        $tt['required'] = $value->required;
                        $tt['position'] = $value->position;
                        $tt['deleted'] = $value->deleted;
                        $tt['params'] = $value->params;

                        if ($tt['type_ques'] == 'Rate (scale 1..5)') {
                            $tt['length'] = $value->length;
                        } else if ($tt['type_ques'] == 'Text Box') {
                            $tt['length'] = $value->precise;
                        }
                        $questions[] = $tt;
                    }
                }

                $return = array(
                    'error' => 0,
                    'questionnaire_id' => $data->id,
                    'courseid' => $data->course,
                    'name' => $data->name,
                    'intro' => $data->intro,
                    'surveyid' => $data->sid,
                    'questions' => $questions
                );
                break;
            case 'scorm':
                $return = array(
                    'error' => 0,
                    'mes' => 'success',
                    'sid' => $data->id,
                    'courseid' => $data->course,
                    'sname' => $data->name,
                    'scormtype' => $data->scormtype,
                    'fname' => $data->reference,
                    'des' => $data->intro,
                    'desformat' => $data->introformat
                );
                break;
            default:
                break;
        }

        return $return;
    }

    public static function getAllModulesByCourseId($courseid, $conditions = [], $fields = '', $index = '') {
        // based on get_course_mods function

        self::loadTables();
        $db = self::getMoodleDbo();

        $visible = isset($conditions['visible']) ? $conditions['visible'] : 1;
        $select = 'cm.*, m.name as modname';
        if ($fields) {
            $fieldsArr = explode(',', $fields);
            $select = 'cm.'.implode(', cm.', $fieldsArr) . ', m.name as modname';
        }

        return $db->get_records_sql("SELECT $select
            FROM #__modules m, #__course_modules cm
            WHERE cm.course = $courseid AND cm.module = m.id AND m.visible = $visible", $index);
    }

    public static function getCourseModuleFromId($modulename, $cmid, $courseid = 0, $sectionnum = false) {
        // based on get_coursemodule_from_id function

        self::loadTables();
        $db = self::getMoodleDbo();

        if (!$modulename) {

            if (!$modulename = $db->get_record_sql("SELECT md.name
                    FROM #__modules md
                    JOIN #__course_modules cm ON cm.module = md.id
                    WHERE cm.id = $cmid")) {
                return false;
            }
            $modulename = $modulename['name'];
        }

        $courseselect = $courseid ? "AND cm.course = $courseid" : '';

        $sectionfield = $sectionnum ? ", cw.section AS sectionnum" : '';
        $sectionjoin  = $sectionnum ? "LEFT JOIN #__course_sections cw ON cw.id = cm.section" : '';

        $sql = "SELECT cm.*, m.name, md.name AS modname $sectionfield
              FROM #__course_modules cm
                   JOIN #__modules md ON md.id = cm.module
                   JOIN #__".$modulename." m ON m.id = cm.instance
                   $sectionjoin
             WHERE cm.id = $cmid AND md.name = '$modulename'
                   $courseselect";

        return $db->get_record_sql($sql);
    }

    public static function getCourseModuleFromInstance($modulename, $instance, $courseid=0, $sectionnum=false) {
        // based on get_course_module_from_instance function

        self::loadTables();
        $db = self::getMoodleDbo();

        $courseselect = $courseid ? "AND cm.course = $courseid" : '';

        if ($sectionnum) {
            $sectionfield = $sectionnum ? ", cw.section AS sectionnum" : '';
            $sectionjoin  = $sectionnum ? "LEFT JOIN #__course_sections cw ON cw.id = cm.section" : '';
        }

        $sql = "SELECT cm.*, m.name, md.name AS modname $sectionfield
                    FROM #__course_modules cm
                    JOIN #__modules md ON md.id = cm.module
                    JOIN #__".$modulename." m ON m.id = cm.instance
                    $sectionjoin
                    WHERE m.id = $instance AND md.name = '$modulename'
                    $courseselect";

        return $db->get_record_sql($sql);
    }

    public static function getCertificateTemplateDefault($courseid) {
        // based on get_certificate_template_default

        self::loadTables();
        $db = self::getMoodleDbo();

        $cer_template = $db->get_record('certificate', array('course' => $courseid));
        $response = array();
        $data = array();
        if ($cer_template) {
            $data['cer_id'] = $cer_template['id'];
            $data['cer_name'] = $cer_template['borderstyle'];
            $response['status'] = true;
            $response['message'] = 'success.';
            $response['template'] = $data;
        } else {
            $response['status'] = false;
            $response['message'] = 'failed.';
            $response['template'] = $data;
        }

        return $response;
    }

    public static function getInfoFeedback($survey_id) {
        // based on get_info_feedback

        self::loadTables();
        $db = self::getMoodleDbo();
        $data['rank'] = array();
        $data['text'] = array();
        $qk1 = $db->get_records_sql("SELECT *
                FROM #__questionnaire_response
                WHERE survey_id = " . $survey_id);
        if (count($qk1) > 0) {
            foreach ($qk1 as $value) {
                $user = $db->get_record_sql("SELECT *
                    FROM #__user
                    WHERE id = " . $value['username']);
                $t['user'] = $user['username'];
                $tt['user'] = $user['username'];

                $qk2 = $db->get_records_sql("SELECT *
                    FROM #__questionnaire_response_rank
                    WHERE response_id = " . $value['id']);
                foreach ($qk2 as $key) {
                    $t['ques_id'] = $key['question_id'];
                    $qk4 = $db->get_record_sql("SELECT *
                        FROM #__questionnaire_question
                        WHERE id = " . $key['question_id']);
                    $t['ques_text']= $qk4['content'];
                    $t['ques_response']= $key['rank'];
                    $data['rank'][] = $t;
                }

                $qk3 = $db->get_records_sql("SELECT *
                    FROM #__questionnaire_response_text
                    WHERE response_id = " . $value['id']);
                foreach ($qk3 as $toan) {
                    $tt['ques_id']= $toan['question_id'];
                    $qk5 = $db->get_record_sql("SELECT *
                    FROM #__questionnaire_question
                    WHERE id = " . $toan['question_id']);
                    $tt['ques_text']= $qk5['content'];
                    $tt['ques_response']= $toan['response'];
                    $data['text'][] = $tt;
                }
            }
        }

        return $data;
    }

    public static function getLearningPageData() {

        $courses = self::getMyLearningCourses();

        return [
            'courses' => $courses
        ];
    }

    public static function countMyLearningCourses() {
        self::loadTables();
        $db = self::getMoodleDbo();

        $username = JFactory::getUser()->username;
        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $courses = self::getCoursesEnrolled($username, 'id, category, shortname, fullname, visible');

        if (empty($courses)) return 0;

        $myLearningCoursesIds = array_keys($courses);
        $where = ' AND co.id IN ('.implode($myLearningCoursesIds, ',').') AND co.creator <> '.$user->id.' AND ue.userid = '.$user->id ;

        $query =
            "SELECT
                ue.id AS ue_id,
                e.id AS enrol_id,
                co.id AS remoteid,
                co.fullname,
                co.shortname,
                ca.id AS cat_id,
                ca.name AS cat_name
                FROM
                    #__user_enrolments ue
                LEFT JOIN
                    #__enrol e ON e.id = ue.enrolid
                LEFT JOIN
                    #__course co ON e.courseid = co.id
                LEFT JOIN
                    #__course_categories ca  ON ca.id = co.category
                LEFT JOIN
                    #__course_completions cc ON cc.course = co.id AND cc.userid = $user->id
                WHERE
                co.visible = '1' AND e.status = 0
                $where
                ORDER BY
                cc.timecompleted DESC, ue.lastaccess DESC, ue.timestart DESC, cc.status desc
                ";
        $db->setQuery($query);
        $records = $db->get_records_sql($query);
        $return = [];

        foreach ($records as $k => $curso) {
            $userRole = self::getUserRole(['id' => $curso['remoteid'], 'username' => $username]);
            $userroles = $userRole['status'] ? $userRole['roles'] : [];

            $isLearner = false;
            $isFacilitator = false;
            $countlearner = 0;

            if (!empty($userroles)) {
                foreach ($userroles as $userrole) {
                    if ($userrole['username'] != $username) continue;
                    foreach (json_decode($userrole['role']) as $role) {
                        if ($role->roleid == 5) {
                            $countlearner++;
                            $isLearner = true;
                        }
                        if ($role->roleid == 4 || $role->sortname == 'teacher') {
                            $isFacilitator = true;
                        }
                    }
                }
            }

            if (!$isLearner && !$isFacilitator) continue;
            $return[] = $curso;
        }

        return count($return);
    }

    public static function getMyLearningCourses() {
        self::loadTables();
        $db = self::getMoodleDbo();

        $username = JFactory::getUser()->username;
        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $courses = self::getCoursesEnrolled($username, 'id, category, shortname, fullname, visible');

        if (empty($courses)) return [];

        $myLearningCoursesIds = array_keys($courses);
        $where = ' AND co.id IN ('.implode($myLearningCoursesIds, ',').') AND co.creator <> '.$user->id.' AND ue.userid = '.$user->id ;

        $query =
            "SELECT
                ue.id AS ue_id,
                ue.lastaccess,
                ue.timestart,
                ue.timeend,
                e.id AS enrol_id,
                co.id AS remoteid,
                co.sortorder,
                co.fullname,
                co.shortname,
                co.idnumber,
                co.summary,
                co.startdate,
                co.creator,
                co.duration,
                co.timecreated as created,
                co.timemodified as modified,
                co.timepublish,
                co.visible,
                ca.id AS cat_id,
                ca.name AS cat_name,
                ca.description AS cat_description,
                cc.status AS completion_status,
                cc.timecompleted AS time_completed
                FROM
                    #__user_enrolments ue
                LEFT JOIN
                    #__enrol e ON e.id = ue.enrolid
                LEFT JOIN
                    #__course co ON e.courseid = co.id
                LEFT JOIN
                    #__course_categories ca  ON ca.id = co.category
                LEFT JOIN
                    #__course_completions cc ON cc.course = co.id AND cc.userid = $user->id
                WHERE
                co.visible = '1' AND e.status = 0
                $where
                ORDER BY
                cc.timecompleted DESC, ue.lastaccess DESC, ue.timestart DESC, cc.status desc
                ";
        $db->setQuery($query);
        $records = $db->get_records_sql($query);

        $now = time();
        $cursos = [];
        $pro_arr = [];
        $tempUserArr = [];

        foreach ($records as $curso) {
            $curso = (object)$curso;

            $c = get_object_vars ($curso);

            $userRole = self::getUserRole(['id' => $curso->remoteid, 'username' => $username]);
            $userroles = $userRole['status'] ? $userRole['roles'] : [];

            $isLearner = false;
            $isFacilitator = false;
            $countlearner = 0;

            if (!empty($userroles)) {
                foreach ($userroles as $userrole) {
                    if ($userrole['username'] != $username) continue;
                    foreach (json_decode($userrole['role']) as $role) {
                        if ($role->roleid == 5) {
                            $countlearner++;
                            $isLearner = true;
                        }
                        if ($role->roleid == 4 || $role->sortname == 'teacher') {
                            $isFacilitator = true;
                        }
                    }
                }
            }

            if (!$isLearner && !$isFacilitator) continue;

            $c['isLearner'] = $isLearner;
            $c['isFacilitator'] = $isFacilitator;
            $c['count_learner'] = $countlearner;

            $newquery  = "select  p.*
                FROM #__prog AS p
                INNER JOIN #__prog_assignment AS pu ON p.id = pu.programid
                INNER JOIN #__course_categories AS cat ON p.category= cat.id
                INNER JOIN #__prog_courseset AS css ON css.programid= p.id
                INNER JOIN #__prog_courseset_course AS cc ON cc.coursesetid= css.id
                INNER JOIN #__course AS c ON courseid= c.id
                WHERE  pu.assignmenttypeid = $user->id and courseid = $curso->remoteid";
            $db->setQuery($newquery);
            $bundles = $db->get_records_sql($newquery);

            $curso->self_enrolment = 0;
            $curso->guest = 0;

            if (empty($bundles)) {
                if (array_key_exists($curso->remoteid, $cursos)) continue;

                $enrol = self::getTable('enrol');
                $enrolMethods = $enrol->loadAssocList(['courseid' => $curso->remoteid, 'status' => 0]);

                $c['coursetype'] = 'single_course';

                $juser = JFactory::getUser();
                $juserid = $juser->id;

                JPluginHelper::importPlugin( 'joomdlesocialgroups' );
                $dispatcher = JDispatcher::getInstance();
                $group_course = $dispatcher->trigger('get_group_by_course_id', array ($curso->remoteid));

                $c['course_group'] = isset($group_course[0]) ? $group_course[0] : 0;
                if (isset($group_course[0])) {
                    $activity_count = $dispatcher->trigger('get_activity_count_circle', array ($group_course[0], $juserid));
                    $c['activity_count'] = isset($activity_count[0]) ? $activity_count[0] : 0;
                }

                $c['in_enrol_date'] = true;
                $c['cost'] = 0;
                foreach ($enrolMethods as $instance) {
                    $instance = (object)$instance;
                    if (($instance->enrol == 'paypal') || ($instance->enrol == 'joomdle')) {
                        $enrol = $instance->enrol;
                        $query = "SELECT cost, currency
                                    FROM #__enrol
                                    where courseid = $curso->remoteid and enrol = $enrol";

                        $record =  $db->get_record_sql($query);
                        if ($c['cost'] < $record->cost) $c['cost'] = (float) $record->cost;
                        $c['currency'] = $record->currency;
                    }

                    if ($instance->enrol == 'self') $c['self_enrolment'] = 1;

                    if ($instance->enrol == 'guest') $c['guest'] = 1;

                    if (($instance->enrolstartdate) && ($instance->enrolenddate)) {
                        $c['in_enrol_date'] = (($instance->enrolstartdate <= $now) && ($instance->enrolenddate >= $now)) ? true : false;
                    } else if ($instance->enrolstartdate) {
                        $c['in_enrol_date'] = ($instance->enrolstartdate <= $now) ? true : false;
                    } else if ($instance->enrolenddate) {
                        $c['in_enrol_date'] = ($instance->enrolenddate >= $now) ? true : false;
                    }
                }

                $c['enroled'] = in_array($curso->remoteid, $myLearningCoursesIds) ? 1 : 0;

                $c['completion_status'] = (int)$curso->completion_status;
                $c['timepublish'] = (int)$curso->timepublish;

                $query_com = "SELECT timecompleted FROM #__course_completions WHERE userid = $user->id AND course = $curso->remoteid";
                $result_com = $db->get_records_sql($query_com);

                $c['time_completed'] = 0;
                if ($result_com) {
                    foreach ($result_com as $res) {
                        if ($c['time_completed'] < $res['timecompleted']) $c['time_completed'] = (int)$res['timecompleted'];
                    }
                }

                $query = "SELECT ue.lastaccess, ue.timestart FROM #__enrol e LEFT JOIN #__user_enrolments ue ON e.id = ue.enrolid WHERE e.courseid = $curso->remoteid AND ue.userid = $user->id AND e.status = 0";
                $result = $db->get_records_sql($query);
                if ($result) {
                    $lastAccessArr = [];
                    $timeStartArr = [];
                    foreach ($result as $res) {
                        $lastAccessArr[] = $res['lastaccess'] ? $res['lastaccess'] : 0;
                        $timeStartArr[] = $res['timestart'];
                    }
                    $c['last_access'] = max($lastAccessArr);
                    $c['enrol_time'] = min($timeStartArr);
                } else {
                    $c['last_access'] = 0;
                    $c['enrol_time'] = 0;
                }

                if (!array_key_exists($c['creator'], $tempUserArr)) {
                    $user_creator = $db->get_record('user', array('id' => $c['creator']));
                    $tempUserArr[$c['creator']] = $user_creator['firstname'] .' '.$user_creator['lastname'];
                }
                $c['creator'] = $tempUserArr[$c['creator']];
                $c['duration'] = (float)($curso->duration/3600);

                $context = self::getTable('context');
                $context->load(['instanceid' => $curso->remoteid, 'contextlevel' => COURSE_CONTEXT_LEVEL]);

                $c['summary'] = strip_tags($c['summary']);

                $files = $db->get_records('files', ['contextid' => $context->id, 'component' => 'course', 'filearea' => 'overviewfiles', 'itemid' => 0]);

                if (!empty($files)) {
                    foreach ($files as $file) {
                        if ($file['filename'] == '.') continue;
                        $c['filename'] = $file['filename'];
                        $c['fileid'] = $context->id;
                        $c['filetype'] = $file['mimetype'];

                        $c['filepath'] = JUri::base() . 'courses/pluginfile.php/' . $context->id . '/course/overviewfiles/';
                    }
                } else {
                    $c['filepath'] = JUri::base() . 'courses/theme/parenthexis/pix/';
                    $c['filename'] = 'nocourseimg.jpg';
                }

                $cursos[] = $c;

            } else
//            if ( in_array($curso->remoteid, $course_id) && !empty($courses)  )
            {
//                if (in_array($curso->remoteid, $course_com)) {
                $time_com = (int)$curso->time_completed;
//                    $time_las = 0;
//                } elseif(in_array($curso->remoteid, $course_las)) {
//                    $time_com = 0;
                $time_las = $curso->lastaccess;
//                }

                $query = "SELECT ue.lastaccess, ue.timestart FROM #__enrol e
                        LEFT JOIN #__user_enrolments ue ON e.id = ue.enrolid
                        WHERE e.courseid = $curso->remoteid AND ue.userid = $user->id AND e.status = 0";

                $result = $db->get_records_sql($query);
                if (!empty($result)) {
                    $lastAccessArr = array();
                    $timeStartArr = array();
                    foreach ($result as $res) {
                        $lastAccessArr[] = $res['lastaccess'];
                        $timeStartArr[] = $res['timestart'];
                    }
                    $c['enrol_time'] = min($timeStartArr);
                } else {
                    $c['enrol_time'] = 0;
                }

                $course_program = $db->get_records_sql("select  p.*,pu.timeassigned,courseid
                    FROM mdl_prog AS p
                    INNER JOIN mdl_prog_assignment AS pa ON p.id = pa.programid
                    INNER JOIN mdl_prog_user_assignment AS pu ON p.id = pu.programid
                    INNER JOIN mdl_prog_courseset AS css ON css.programid= p.id
                    INNER JOIN mdl_prog_courseset_course AS cc ON cc.coursesetid= css.id
                    WHERE  pa.assignmenttypeid = $user->id and courseid = $curso->remoteid");

                if ($course_program) {
                    foreach ($course_program as $pro) {
                        $pro = (object)$pro;
                        if (!in_array($pro->id, $pro_arr)) {
                            $c['course_group'] = 0;
                            $c['last_access'] = (int)$time_las;
                            $c['coursetype'] = 'program';
                            $c['isFacilitator'] = false;
                            $c['remoteid'] = 0;
                            $c['cat_id'] = $pro->id;
                            $c['cat_name'] = '';
                            $c['cat_description']='';
                            $c['sortorder'] = 0;
                            $c['fullname'] = $pro->fullname;
                            $c['shortname'] = '';
                            $c['idnumber']='';
                            $c['summary'] = strip_tags($pro->summary);
                            $c['startdate'] = 0;
                            $c['modified'] = 0;
                            $c['created'] = 0;
                            $c['completion_status'] = 0;
                            $c['time_completed'] = $time_com;
                            $c['cost'] = 0;
                            $c['duration'] = (float)($curso->duration/3600);
                            $c['currency'] = '';
                            $c['enroled'] = 0;
                            $c['in_enrol_date'] = 0;
                            $c['fileid'] = '';
                            $c['filetype'] = '';
                            $c['filepath'] = 0;
                            $c['filename'] = 0;
                            $c['timepublish'] = (int)$pro->timecreated;

                            if (!array_key_exists($pro->usermodified, $tempUserArr)) {
                                $user_creator = $db->get_record('user', array('id'=>$pro->usermodified), 'id, username, firstname, lastname');
                                $tempUserArr[$c['creator']] = $user_creator['firstname'].' '.$user_creator['lastname'];
                            }
                            $c['creator'] = $tempUserArr[$c['creator']];

                            $res = $db->get_records_sql(
                                "SELECT completiontime, completionevent
                                FROM #__prog_assignment
                                WHERE assignmenttypeid = $user->id and programid = $pro->id"
                            );
                            $max = 0;

                            foreach ($res as $key => $value) {
                                $value = (object)$value;
                                if ($value->completiontime != -1) {
                                    if ($value->completionevent) {
                                        $max = max($max, time() + ($value->completiontime));
                                    } else {
                                        $max = max($max, $value->completiontime);
                                    }
                                }
                            }

                            $c['timeend'] = $max;

                            $context = self::getTable('context');
                            $context->load(['instanceid' => $pro->id, 'contextlevel' => PROGRAM_CONTEXT_LEVEL]);
                            $files = $db->get_records('files', ['contextid' => $context->id, 'component' => 'totara_program', 'filearea' => 'overviewfiles', 'itemid' => 0]);

                            if (!empty($files)) {
                                foreach ($files as $file) {
                                    if ($file['filename'] == '.') continue;
                                    $c['filename'] = $file['filename'];
                                    $c['filepath'] = JUri::base() . 'courses/pluginfile.php/' . $context->id . '/totara_program/overviewfiles/';
                                }
                            } else {
                                $c['filepath'] = JUri::base() . 'courses/theme/parenthexis/pix/';
                                $c['filename'] = 'nocourseimg.jpg';
                            }

                            $cursos[] = $c;
                            $pro_arr[] = $pro->id;
                        }
                    }
                }

            }
        }

        return $cursos;
    }

    public static function getManagePageData() {
        self::loadTables();
        $db = self::getMoodleDbo();

        $username = JFactory::getUser()->username;

        $mycategory = self::getMoodleCategories($username);

        $lpcat_arr = [];
        $macat_arr = [];
        $malpcat_arr = [];
        $noncat_arr = [];
        if ($mycategory['status']) {
            foreach ($mycategory['categories'] as $mycat) {
                if ($mycat['role'] == 'LP') {
                    $lpcat_arr[] = $mycat['catid'];
                    $malpcat_arr[] = $mycat['catid'];
                } else if ($mycat['role'] == 'CMA') {
                    $macat_arr[] = $mycat['catid'];
                    $malpcat_arr[] = $mycat['catid'];
                } else if ($mycat['role'] == 'non-cat') {
                    $noncat_arr[] = $mycat['catid'];
                }
            }
        }

        $noncat = !empty($noncat_arr) ? implode(',', $noncat_arr) : '';
        $malpcat = !empty($malpcat_arr) ? implode(',', $malpcat_arr) : '';

        $cursos = $noncat ? self::myOwnCourses($username, $noncat) : [];

        $courses_app = $malpcat ? self::learningProviderCourses($username, $malpcat) : [];

        $macourses = $malpcat ? self::myOwnCourses($username, $malpcat, 0, 0, 1) : [];

        $courses = self::myCourses($username, 0, 1);

        $result = array(
            'mycategory' => json_encode($mycategory),
            'cursos' => json_encode($cursos),
            'courses_app' => json_encode($courses_app),
            'macourses' => json_encode($macourses),
            'courses' => json_encode($courses)
        );
        return $result;
    }

    public static function myOwnCourses($username, $catid = '', $limit = 0, $offset = 0, $is_malp = 0) {
        self::loadTables();
        $db = self::getMoodleDbo();

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $limitText = $limit ? "LIMIT $limit OFFSET $offset" : '';

        $where_arr = array();

        if ($is_malp) {
            $where_arr[] = "(co.visible = 0 OR co.visibleold = 0)";
        } else {
            $where_arr[] = "co.creator = $user->id";
        }
        if ($catid) {
            $where_arr[] = "ca.id IN ($catid)";
        }

        $where = !empty($where_arr) ? 'WHERE '.implode(' AND ', $where_arr) : '';

        $query =
            "SELECT
            co.id          AS remoteid,
            ca.id          AS cat_id,
            co.fullname,
            co.shortname,
            co.idnumber,
            co.summary,
            co.startdate,
            co.timecreated AS created,
            co.timemodified AS modified,
            co.timepublish,
            co.visible,
            co.creator,
            co.price,
            co.currency,
            co.duration,
            co.learningoutcomes,
            co.targetaudience,
            co.coursesurvey,
            co.facilitatedcourse,
            ca.name        AS cat_name,
            ca.description AS cat_description
            FROM
                #__course_categories ca
            JOIN
                #__course co ON ca.id = co.category
            $where
            ORDER BY modified DESC
            $limitText
            ";

        $records = $db->get_records_sql($query);
        $data = array();
        $courses = array ();
        $cur_coursesid = array();
        $i = 0;

        $tempUserArr = [];
        if ($records) {
            foreach ($records as $course) {
                $course = (object)$course;
                if ($is_malp) {
                    $check_pending = $db->get_records_sql(
                        "SELECT * FROM #__course_request_approve "
                        . "WHERE courseid = " . $course->remoteid . " AND status in (0, 1, 2)"
                    );
                    if ($check_pending && count($check_pending) > 0) continue;
                }
                if (!array_key_exists($course->creator, $tempUserArr)) {
                    $creator = $db->get_record('user', array('id'=>$course->creator), 'id, username, firstname, lastname');
                    $tempUserArr[$course->creator] = $creator['username'];
                }
                $record = array();
                $record['id'] = $course->remoteid;
                $record['fullname'] = $course->fullname;
                $record['shortname'] = $course->shortname;
                $record['idnumber'] = $course->idnumber;
                $record['summary'] = $course->summary;
                $record['summary'] = strip_tags($course->summary);

                $record['startdate'] = $course->startdate;
                $record['created'] = $course->created;
                $record['modified'] = $course->modified;
                $record['timepublish'] = $course->timepublish;

                $record['category'] = $course->cat_id;
                $record['cat_name'] = $course->cat_name;
                $record['cat_description'] = $course->cat_description;
                $record['creator'] = $tempUserArr[$course->creator];
                $record['price'] = $course->price;
                $record['currency'] = $course->currency;
                $record['duration'] = (float)$course->duration/3600;
                $record['learning_outcomes'] = $course->learningoutcomes;
                $record['target_audience'] = $course->targetaudience;
                $record['course_survey'] = $course->coursesurvey;
                $record['course_facilitated'] = $course->facilitatedcourse;

                $result = $db->get_record_sql(
                    "SELECT MAX(enrolperiod) AS enrolperiod
                  FROM #__enrol
                  WHERE status = 0 AND courseid = $course->remoteid"
                );
                $record['enrolperiod'] = $result['enrolperiod'];

                $enrol = self::getTable('enrol');
                $enrolMethods = $enrol->loadAssocList(['courseid' => $course->remoteid, 'status' => 0]);

                $record['self_enrolment'] = 0;
                foreach ($enrolMethods as $instance) {
                    if ($instance['enrol'] == 'self') {
                        $record['self_enrolment'] = 1;
                    }
                }

                $record['has_nonmember'] = 0;
                $record['content_creator'] = '';
                $countlearner = 0;

                $userRole = self::getUserRole(['id' => $course->remoteid]);
                $userroles = $userRole['status'] ? $userRole['roles'] : [];

                if (!empty($userroles)) {
                    foreach ($userroles as $userrole) {
                        foreach (json_decode($userrole['role']) as $role) {
                            if ($role->roleid == 3) {
                                $record['has_nonmember'] = 1;
                                $record['content_creator'] = $userrole['username'];
                            }
                            if ($role->roleid == 5) {
                                $countlearner++;
                            }
                        }
                    }
                }
                $record['count_learner'] = $countlearner;

                $record['course_status'] = self::getCourseStatus($course->remoteid);

                $context = self::getTable('context');
                $context->load(['instanceid' => $course->remoteid, 'contextlevel' => COURSE_CONTEXT_LEVEL]);
                $files = $db->get_records('files', ['contextid' => $context->id, 'component' => 'course', 'filearea' => 'overviewfiles', 'itemid' => 0]);

                if (!empty($files)) {
                    foreach ($files as $file) {
                        if ($file['filename'] == '.') continue;
                        $record['filename'] = $file['filename'];
                        $record['filepath'] = JUri::base() . 'courses/pluginfile.php/' . $context->id . '/course/overviewfiles/';
                    }
                } else {
                    $record['filepath'] = JUri::base() . 'courses/theme/parenthexis/pix/';
                    $record['filename'] = 'nocourseimg.jpg';
                }

//                if ((has_capability('enrol/manual:unenrolself', $context, $user->id)) || (has_capability('enrol/self:unenrolself', $context, $user->id)))
//                    $record['can_unenrol'] = 1;
//                else
                $record['can_unenrol'] = 0;

                $courses[$i] = $record;
                $cur_coursesid[] = $record['id'];
                $i++;
            }
        }

        if ($is_malp) {
            $courses_cc = self::myCourses($username, 0, 1);
            if ($courses_cc['status']) {
                foreach ($courses_cc['courses'] as $course_cc) {
                    if (!in_array($course_cc['id'], $cur_coursesid)) {
                        $courses[$i] = $course_cc;
                    }
                    $i++;
                }
            }
        }
        if (!empty($courses)) {
            $data['status'] = 1;
            $data['message'] = 'Success';
            $data['courses'] = $courses;
        } else {
            $data['status'] = 0;
            $data['message'] = 'Not found.';
            $data['courses'] = $courses;
        }

        return $data;
    }

    public static function countMyOwnCourses() {
        self::loadTables();
        $db = self::getMoodleDbo();

        $username = JFactory::getUser()->username;

        $mycategory = self::getMoodleCategories($username);

        $noncat_arr = [];
        if ($mycategory['status']) {
            foreach ($mycategory['categories'] as $mycat) {
                if ($mycat['role'] == 'non-cat') {
                    $noncat_arr[] = $mycat['catid'];
                }
            }
        }

        $catid = !empty($noncat_arr) ? implode(',', $noncat_arr) : '';

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $where_arr = [];

        $where_arr[] = "co.creator = $user->id";

        if ($catid) {
            $where_arr[] = "ca.id IN ($catid)";
        }

        $where = !empty($where_arr) ? 'WHERE '.implode(' AND ', $where_arr) : '';

        $query =
            "SELECT
                co.id AS remoteid
            FROM
                #__course_categories ca
            JOIN
                #__course co ON ca.id = co.category
            $where
            ";

        return $db->count_records_sql($query);
    }

    public static function myCertificates($username) {
        // based on my_certificates_normal function

        self::loadTables();
        $db = self::getMoodleDbo();

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $cursos = self::getCoursesEnrolled($username);

        if (empty($cursos)) return [];

        $c_ids = array_keys($cursos);
        $ids_str = implode(',', $c_ids);

        $sql_pr = "SELECT cc.courseid as categoryname
            FROM #__prog AS p
            INNER JOIN #__prog_assignment AS pu ON p.id = pu.programid
            INNER JOIN #__course_categories AS cat ON p.category= cat.id
            INNER JOIN #__prog_courseset AS css ON css.programid= p.id
            INNER JOIN #__prog_courseset_course AS cc ON cc.coursesetid= css.id
            WHERE  pu.assignmenttypeid = $user->id";

        $certs = $db->get_records_sql("SELECT 
            c.id, c.course, c.name, c.intro, c.timecreated, ci.userid, ci.certificateid
            FROM #__certificate c
            LEFT JOIN #__certificate_issues ci ON c.id = ci.certificateid
            WHERE ci.userid = $user->id
            AND (c.course in ( $ids_str) OR c.course in ($sql_pr))
            ORDER BY ci.timecreated DESC");

        $c = [];

        foreach ($certs as $cert) {
            $cert = (object)$cert;
            $coursemodule = self::getCourseModuleFromInstance("certificate", $cert->certificateid);
            $certificate['id'] = $coursemodule['id'];
            $certificate['name'] = $cert->name;
            $certificate['date'] = $cert->timecreated;

            $certificate['userid'] = $cert->userid;
            $certificate['courseid'] = $cert->course;

            $course = self::getCourseInfo(['id' => $cert->course, 'username' => $username]);
            $certificate['coursename'] = $course['fullname'];
            $certificate['certificateid'] = $cert->certificateid;

            $cm = self::getCourseModuleFromId('certificate', $coursemodule['id']);

            $context = self::getTable('context');
            $context->load(['instanceid' => $cm['id'], 'contextlevel' => MODULE_CONTEXT_LEVEL]);

            $certrecord = $db->get_records('certificate_issues', array('userid' => $cert->userid, 'certificateid'=> $cert->certificateid), 'id');

            $component = 'mod_certificate';
            $filearea = 'issue';

            $filees = [];
            foreach ($certrecord as $value) {
                $files = $db->get_records('files', ['contextid' => $context->id, 'component' => $component, 'filearea' => $filearea, 'itemid' => $value['id']]);
                $filees = $filees + $files;
            }

            if (!empty($filees)) {
                foreach ($filees as $file) {
                    if ($file['filename'] == '.') continue;
                    $certificate['downloadFiles'] = JUri::base() . 'courses/pluginfile.php/' . $context->id . '/' . $component . '/' . $filearea . '/' . $file['itemid'] . '/' . $file['filename'];
                    $certificate['url'] = JUri::base() . 'courses/mod/certificate/view.php?id=' . $cm['id'] . '&action=get';
                }
            } else {
                $certificate['downloadFiles'] =  '';
                $certificate['url'] = '';
            }

            // $c[$cert->certificateid] = $certificate;
            $c[] = $certificate;
        }
        return $c;
    }

    public static function myCourses($username, $order_by_cat = 0, $is_notmalp = 0) {
        // based on my_courses function

        self::loadTables();
        $db = self::getMoodleDbo();

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $c = self::getCoursesEnrolled($username);

        $results = [];
        $courses = [];
        $i = 0;

        if (!empty($c) && is_array($c)) {
            $tempUserArr = [];
            foreach ($c as $course) {
                $course = (object)$course;

                $record = [];
                $record['has_nonmember'] = 0;
                if ($is_notmalp) {
                    if ($course->visible) continue;
                    $coursesendapp = $db->get_record_sql(
                        "SELECT *
                            FROM #__course_request_approve
                            WHERE (status = 0 OR status = 1) AND courseid = $course->id"
                    );

                    if ($coursesendapp && count($coursesendapp) > 0) continue;
                }

                $userRole = self::getUserRole(['id' => $course->id]);
                $userroles = $userRole['status'] ? $userRole['roles'] : [];

                $check = true;
                $count_learner = 0;
                $record['content_creator'] = '';
                if (!empty($userroles)) {
                    foreach ($userroles as $userrole) {
                        foreach (json_decode($userrole['role']) as $role) {
                            if ($role->roleid == 5) {
                                $count_learner++;
                            }
                            if ($role->roleid == 3) {
                                $record['content_creator'] = $userrole['username'];
                                $check = false;
                                continue;
                            }
                        }
                    }
                }
                if ($check) continue;
                $record['has_nonmember'] = 1;

                if (!array_key_exists($course->creator, $tempUserArr)) {
                    $user_creator = $db->get_record('user', array('id' => $course->creator), 'id, username, firstname, lastname');
                    $tempUserArr[$course->creator] = $user_creator['firstname'].' '.$user_creator['lastname'];
                }

                $record['id'] = $course->id;
                $record['fullname'] = $course->fullname;
                $record['shortname'] = $course->shortname;
                $record['category'] = $course->category;
                $record['cat_name'] = self::getCourseCategoryInfo($course->category)['name'];

                $record['count_learner'] = $count_learner;
                $record['creator'] = $tempUserArr[$course->creator];
                $record['startdate'] = $course->startdate;
                $record['duration'] = $course->duration/3600;
                $record['price'] = $course->price;
                $record['currency'] = $course->currency;
                $record['learning_outcomes'] = $course->learningoutcomes;
                $record['target_audience'] = $course->targetaudience;
                $record['course_survey'] = $course->coursesurvey;
                $record['course_facilitated'] = $course->facilitatedcourse;
                $record['summary'] = $course->summary;
                $record['summary'] = strip_tags($course->summary);

                $result = $db->get_record_sql(
                    "SELECT MAX(enrolperiod) AS enrolperiod
                    FROM #__enrol
                    WHERE status = 0 AND courseid = $course->id"
                );
                $record['enrolperiod'] = $result['enrolperiod'];

                $enrol = self::getTable('enrol');
                $enrolMethods = $enrol->loadAssocList(['courseid' => $course->id, 'status' => 0]);

                $record['self_enrolment'] = 0;
                foreach ($enrolMethods as $instance) {
                    if ($instance['enrol'] == 'self') {
                        $record['self_enrolment'] = 1;
                        break;
                    }
                }

                $context = self::getTable('context');
                $context->load(['instanceid' => $course->id, 'contextlevel' => COURSE_CONTEXT_LEVEL]);

                $files = $db->get_records('files', ['contextid' => $context->id, 'component' => 'course', 'filearea' => 'overviewfiles', 'itemid' => 0]);

                if (!empty($files)) {
                    foreach ($files as $file) {
                        if ($file['filename'] == '.') continue;
                        $record['filename'] = $file['filename'];
                        $record['filepath'] = JUri::base() . 'courses/pluginfile.php/' . $context->id . '/course/overviewfiles/';
                    }
                } else {
                    $record['filepath'] = JUri::base() . 'courses/theme/parenthexis/pix/';
                    $record['filename'] = 'nocourseimg.jpg';
                }
//
//                if ((has_capability('enrol/manual:unenrolself', $context, $user->id)) || (has_capability('enrol/self:unenrolself', $context, $user->id)))
//                    $record['can_unenrol'] = 1;
//                else
                $record['can_unenrol'] = 0;

                $record['course_status'] = self::getCourseStatus($course->id);

                $courses[$i] = $record;
                $i++;
            }
            $results['status'] = 1;
            $results['message'] = 'Success.';
            $results['courses'] = $courses;
        } else {
            $results['status'] = 0;
            $results['message'] = 'Not found.';
            $results['courses'] = array();
        }
        return $results;
    }

    public static function getMoodleCategories($username) {
        // based on get_my_categories function

        self::loadTables();
        $db = self::getMoodleDbo();

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $category = self::getTable('coursecategories');
        $category->load(['name' => $username]);

        if ($category->id) {
            if (empty($category->userowner)) {
                $category->userowner = $user->id;
                $category->store();
            }
        } else {
            $checkownercat = $db->get_records_sql(
                "SELECT id "
                . "FROM #__course_categories "
                . "WHERE userowner = " . $user->id);

            if (count($checkownercat) <= 0) {
                self::create_user_category($username);
            } else {
                foreach ($checkownercat as $cat) {
                    $catcontext = self::getTable('context');
                    $catcontext->load(['instanceid' => $category->id, 'contextlevel' => COURSECAT_CONTEXT_LEVEL]);

                    $rolemanages = $db->get_records('role_assignments', array('contextid' => $catcontext->id, 'userid' => $user->id, 'roleid' => 1));
                    if (!empty($rolemanages) && count($rolemanages) > 0) {
                        self::create_user_category($username);
                    }
                }
            }
        }

        $query1 = "SELECT id, name, userowner "
            . "FROM #__course_categories";

        $allcat = $db->get_records_sql($query1);
        $response = [];
        $categories = [];

        if (!empty($allcat)) {
            foreach ($allcat as $cat) {
                $cat = (object)$cat;
                $category = [];

                $catcontext = self::getTable('context');
                $catcontext->load(['instanceid' => $cat->id, 'contextlevel' => COURSECAT_CONTEXT_LEVEL]);

                if ($cat->userowner == $user->id) {
                    // check user has LP category
                    $rolemanages = $db->get_records('role_assignments', array('contextid' => $catcontext->id, 'userid' => $user->id, 'roleid' => 1));
                    if (!empty($rolemanages) && count($rolemanages) > 0) {
                        $category['catid'] = $cat->id;
                        $category['name'] = $cat->name;
                        $category['username'] = $username;
                        $category['role'] = 'LP';
                    } else {
                        $category['catid'] = $cat->id;
                        $category['name'] = $cat->name;
                        $category['username'] = $username;
                        $category['role'] = 'non-cat';
                    }
                } else {
                    // check user has Manager category
                    $rolemanages = $db->get_records('role_assignments', array('contextid' => $catcontext->id, 'userid' => $user->id, 'roleid' => 1));
                    if (!empty($rolemanages) && count($rolemanages) > 0) {
                        $category['catid'] = $cat->id;
                        $category['name'] = $cat->name;
                        $category['username'] = $username;
                        $category['role'] = 'CMA';
                    }
                }
                if (!empty($category)) {
                    $categories[] = $category;
                }
            }
            $response['status'] = true;
            $response['message'] = 'Success.';
            $response['categories'] = $categories;
            return $response;
        } else {
            $response['status'] = false;
            $response['message'] = 'No category found.';
            $response['categories'] = array();
            return $response;
        }
    }

    public static function create_user_category($username) {
        self::loadTables();
        $db = self::getMoodleDbo();

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $parentcat = $db->get_record('course_categories', ['name' => 'Miscellaneous', 'parent' => 0, 'visible' => 1, 'depth' => 1, 'userowner' => 0]);

        $category = self::getTable('coursecategories');

        $category['name'] = $user->username;

        $category['parent'] = $parentcat['id'];
        $category['depth'] = 2;
        $category['visible'] = 1;
        $category['idnumber'] = '';
        $category['description'] = "Normal category for ".$user->username;
        $category['descriptionformat'] = 1;
        $category['userowner'] = $user->id;

        if ($category->store()) {
            $category['path'] = $parentcat['path'] . '/' . $category->id;
        }

        $category->store();

        $catcontext = self::getTable('context');
        $catcontext->load(['instanceid' => $category->id, 'contextlevel' => COURSECAT_CONTEXT_LEVEL]);

        $role = 2;
        role_assign($role, $user->id, $catcontext->id);

        return true;
    }

    public static function getCoursesEnrolled($username, $fields = '*') {
        self::loadTables();
        $db = self::getMoodleDbo();

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $sql = "SELECT $fields
              FROM #__course c
              JOIN (SELECT DISTINCT e.courseid
                      FROM #__enrol e
                      JOIN #__user_enrolments ue ON (ue.enrolid = e.id AND ue.userid = $user->id)
                      ) en ON (en.courseid = c.id)
          ";

        $db->setQuery($sql);
        $courses = $db->loadAssocList('id');

        return $courses;
    }

    public static function getCourseCategoryInfo($cat_id) {
        $db = self::getMoodleDbo();

        $category = $db->get_record('course_categories', ['id' => $cat_id]);

        return $category;
    }

    public static function learningProviderCourses($username, $categoryids) {
        self::loadTables();
        $db = self::getMoodleDbo();

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $results = [];
        if (empty($categoryids)) {
            $courses_cc = self::myCourses($username);
            if ($courses_cc['status']) {
                foreach ($courses_cc['courses'] as $course_cc) {
                    $cra = $db->get_records_sql(
                        "SELECT id, status
                        FROM #__course_request_approve
                        WHERE courseid = " . $course_cc['id']
                    );
                    if ($cra && count($cra) > 0) {
                        $cr2 = (object)end($cra);
                        if ($cr2->status == 0) {
                            $course_cc['pending_approve'] = true;
                            $course_cc['created'] = $course_cc['timecreated'];
                            $course_cc['modified'] = $course_cc['timemodified'];
                            $course_cc['is_mycourse'] = false;
                            $course_cc['approved'] = false;
                            $course_cc['unapproved'] = false;
                            $course_cc['published'] = false;
                            $course_cc['unpublished'] = false;
                            $userRole = self::getUserRole(['id' => $course_cc['id']]);

                            $userroles = $userRole['status'] ? $userRole['roles'] : [];

                            $course_cc['assignedUsers'] = $userroles;
                            $results[] = $course_cc;
                        }
                    }
                }
            }
        } else {
            $catid_arr = explode(',', $categoryids);

            if (!empty($catid_arr)) {
                foreach ($catid_arr as $categoryid) {
                    $courses = self::getCoursesByCategory($categoryid, 0, $username);
                    if ($courses) {
                        $tempUserArr = [];
                        foreach ($courses as $cour) {
                            $data = array();
                            $data['id'] = $cour['remoteid'];
                            $data['category'] = $categoryid;
                            $data['fullname'] = $cour['fullname'];
                            $data['summary'] = strip_tags($cour['summary']);

                            $data['startdate'] = $cour['startdate'];
                            $data['created'] = $cour['timecreated'];
                            $data['modified'] = $cour['timemodified'];
                            $data['timepublish'] = $cour['timepublish'];

                            $data['is_mycourse'] = $cour['creator'] == $user->id ? true : false;

                            if (!array_key_exists($cour['creator'], $tempUserArr)) {
                                $user_creator = $db->get_record('user', array('id' => $cour['creator']), 'id, username, firstname, lastname');
                                $tempUserArr[$cour['creator']] = $user_creator['firstname'] . ' ' . $user_creator['lastname'];
                            }
                            $data['creator'] = $tempUserArr[$cour['creator']];
                            $data['duration'] = (float)($cour['duration'] / 3600);
                            $data['course_survey'] = $cour['coursesurvey'];
                            $data['course_facilitated'] = $cour['facilitatedcourse'];

                            $data['pending_approve'] = false;
                            $data['approved'] = false;
                            $data['unapproved'] = false;
                            $data['published'] = false;
                            $data['unpublished'] = false;
                            $course_request = $db->get_records_sql(
                                "SELECT id, status
                                FROM #__course_request_approve
                                WHERE courseid = " . $cour['remoteid']
                            );
                            if ($course_request) {
                                $cr = (object)end($course_request);

                                switch ($cr->status) {
                                    case 0:
                                        $data['pending_approve'] = true;
                                        break;
                                    case 1:
                                        $data['approved'] = true;
                                        break;
                                    case -1:
                                        $data['unapproved'] = true;
                                        break;
                                    case 2:
                                        $data['published'] = true;
                                        break;
                                    case -2:
                                        $data['unpublished'] = true;
                                        break;
                                }
                                if (($count = count($course_request)) > 1) {
                                    $i = 1;
                                    foreach ($course_request as $courser) {
                                        if ($i < $count)
                                            $db->delete_record('course_request_approve', array('id' => $courser->id));
                                        $i++;
                                    }
                                }
                            }

                            $result = $db->get_record_sql(
                                "SELECT MAX(enrolperiod) AS enrolperiod
                                FROM #__enrol
                                WHERE status = 0 AND courseid = " . $cour['remoteid']
                            );

                            $data['enrolperiod'] = $result['enrolperiod'];

                            $context = self::getTable('context');
                            $context->load(['instanceid' => $cour['remoteid'], 'contextlevel' => COURSE_CONTEXT_LEVEL]);

                            $files = $db->get_records('files', ['contextid' => $context->id, 'component' => 'course', 'filearea' => 'overviewfiles', 'itemid' => 0]);

                            if (!empty($files)) {
                                foreach ($files as $file) {
                                    if ($file['filename'] == '.') continue;
                                    $data['filename'] = $file['filename'];
                                    $data['filepath'] = JUri::base() . 'courses/pluginfile.php/' . $context->id . '/course/overviewfiles/';
                                }
                            } else {
                                $data['filepath'] = JUri::base() . 'courses/theme/parenthexis/pix/';
                                $data['filename'] = 'nocourseimg.jpg';
                            }

                            $data['timeend'] = date('d/m/Y', ($cour['startdate'] + $data['enrolperiod']));

                            $data['cost'] = $cour['price'];
                            $data['price'] = $cour['price'];
                            $data['currency'] = $cour['currency'];

                            $data['course_status'] = self::getCourseStatus($cour['remoteid']);

                            $data['isFacilitator'] = false;

                            $userRole = self::getUserRole(['id' => $cour['remoteid']]);
                            $userroles = $userRole['status'] ? $userRole['roles'] : [];

                            $data['assignedUsers'] = $userroles;
                            $countlearner = 0;
                            if (!empty($userroles)) {
                                foreach ($userroles as $userrole) {
                                    foreach (json_decode($userrole['role']) as $role) {
                                        if ($role->roleid == 5) {
                                            $countlearner++;
                                        }
                                        if ($userrole['username'] == $username) {
                                            if ($role->roleid == 4 || $role->sortname == 'teacher') {
                                                $data['isFacilitator'] = true;
                                            }
                                        }
                                    }
                                }
                            }
                            $data['count_learner'] = $countlearner;

                            $results[] = $data;
                        }
                    }
                }
            }
        }
        $response['status'] = true;
        $response['message'] = 'Success.';
        $response['courses'] = $results;
        return $response;
    }

    public static function getCoursesByCategory ($category, $available = 0, $username = '') {
        // based on courses_by_category function

        self::loadTables();
        $db = self::getMoodleDbo();

        $where = $available ? " AND co.enrollable = '1'" : '';

        $query =
            "SELECT
            co.id          AS remoteid,
            ca.id          AS cat_id,
            ca.name        AS cat_name,
            ca.description AS cat_description,
            co.sortorder,
            co.fullname,
            co.summary,
            co.startdate,
            co.timecreated,
            co.timemodified,
            co.timepublish,
            co.visible,
            co.price,
            co.currency,
            co.creator, co.coursesurvey, co.facilitatedcourse, co.duration
            FROM
            #__course_categories ca
            JOIN
            #__course co ON
            ca.id = co.category
            WHERE
            ca.id = $category
            $where
            ORDER BY
            sortorder ASC
            ";

        $records = $db->get_records_sql($query);

        $my_courses = [];
        if ($username) {
            $user = self::getTable('user');
            $user->load(['username' => $username]);

            $courses = self::getCoursesEnrolled($username, 'id, category, sortorder, shortname, fullname, idnumber, startdate, visible, groupmode, groupmodeforce');

            $my_courses = array_keys($courses);
        }

        $now = time();

        $cursos = [];
        foreach ($records as $curso) {
            $curso = (object)$curso;
            $c = get_object_vars ($curso);

            $c['self_enrolment'] = 0;
            $c['guest'] = 0;
            $c['in_enrol_date'] = true;

            $enrol = self::getTable('enrol');
            $enrolMethods = $enrol->loadAssocList(['courseid' => $curso->remoteid, 'status' => 0]);

            foreach ($enrolMethods as $instance) {
                $instance = (object)$instance;

                if ($instance->enrol == 'self') $c['self_enrolment'] = 1;

                if ($instance->enrol == 'guest') $c['guest'] = 1;

                if (($instance->enrolstartdate) && ($instance->enrolenddate)) {
                    $c['in_enrol_date'] = (($instance->enrolstartdate <= $now) && ($instance->enrolenddate >= $now)) ? true : false;
                } else if ($instance->enrolstartdate) {
                    $c['in_enrol_date'] = ($instance->enrolstartdate <= $now) ? true : false;
                } else if ($instance->enrolenddate) {
                    $c['in_enrol_date'] = ($instance->enrolenddate >= $now) ? true : false;
                }
            }
            $c['cost'] = $c['price'];

            $c['enroled'] = 0;
            if ($username) {
                if (in_array ($curso->remoteid, $my_courses))
                    $c['enroled'] = 1;
            }

            $c['coursesurvey'] = $curso->coursesurvey;
            $c['facilitatedcourse'] = $curso->facilitatedcourse;
            $c['duration'] = $curso->duration;

            $cursos[] = $c;
        }

        return ($cursos);
    }

    ////////////////////////////////////////////////////////////////////////////////////
    public static function role_assign($roleid, $userid, $contextid, $component = '', $itemid = 0, $timemodified = '') {
        self::loadTables();
        $db = self::getMoodleDbo();

        if ($contextid === 0 or is_numeric($component)) {
            return array(
                'status' => false,
                'message' => 'Invalid call to role_assign(), code needs to be updated to use new order of parameters',
            );
        }

        if (empty($roleid)) {
            return array(
                'status' => false,
                'message' => 'Invalid call to role_assign(), roleid can not be empty'
            );
        }

        if (empty($userid)) {
            return array(
                'status' => false,
                'message' => 'Invalid call to role_assign(), userid can not be empty'
            );
        }

        if ($itemid) {
            if (strpos($component, '_') === false) {
                return array(
                    'status' => false,
                    'message' => 'Invalid call to role_assign(), component must start with plugin type such as"enrol_" when itemid specified', 'component:'.$component
                );
            }
        } else {
            $itemid = 0;
            if ($component !== '' and strpos($component, '_') === false) {
                return array(
                    'status' => false,
                    'message' => 'Invalid call to role_assign(), invalid component string', 'component:'.$component
                );
            }
        }

        if (!$db->record_exists('user', array('id'=>$userid, 'deleted'=>0))) {
            return array(
                'status' => false,
                'message' => 'User ID does not exist or is deleted!', 'userid:'.$userid
            );
        }

        $context = self::getTable('context');
        $context->load($contextid);

        $timemodified = $timemodified ? $timemodified : time();

        // Check for existing entry
        $where = [];
        if (!is_null($roleid)) $where[] = " roleid = $roleid ";
        if (!is_null($context->id)) $where[] = " contextid = $context->id ";
        if (!is_null($userid)) $where[] = " userid = $userid ";
        if ($component != '') $where[] = " component = '$component' ";
        if (!is_null($itemid)) $where[] = " itemid = $itemid ";

        $query = "SELECT *
              FROM #__role_assignments
              WHERE ". implode(' AND ', $where);
        $run = $db->setQuery($query);
        $ras = $run->loadObjectList();

        if ($ras) {
            if (count($ras) > 1) {
                $ra = array_shift($ras);
                foreach ($ras as $r) {
                    $db->delete_record('role_assignments', array('id'=>$r->id));
                }
            } else {
                $ra = reset($ras);
            }

            return $ra->id;
        }

        $ra = new stdClass();
        $ra->roleid       = $roleid;
        $ra->contextid    = $context->id;
        $ra->userid       = $userid;
        $ra->component    = $component;
        $ra->itemid       = $itemid;
        $ra->timemodified = $timemodified;

        $currentuser = self::getTable('user');
        $currentuser->load(['username' => JFactory::getUser()->username]);
        $ra->modifierid   = empty($currentuser->id) ? 0 : $currentuser->id;

        $ra = $db->insert_record('role_assignments', $ra);

        return $ra->id;
    }

    public static function enrol_user($username, $course_id, $roleid = 5, $timestart = 0, $timeend = 0, $cur_username = null) {
        self::loadTables();
        $db = self::getMoodleDbo();

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        if (!$user->id)
            return array(
                'status' => false,
                'message' => 'No user found.',
            );

        $course = self::getTable('course');
        $course->load($course_id);

        if (!$course->id)
            return array(
                'status' => false,
                'message' => 'No course found.',
            );

        if (!$timestart) {
            $today = time();
//            $today = make_timestamp(date('Y', $today), date('m', $today), date('d', $today), date ('H', $today), date ('i', $today), date ('s', $today));
            $timestart = $today;
        }

        $manualMethod = $db->get_record('enrol', ['courseid' => $course_id, 'status' => 0, 'enrol' => 'manual']);
        if (empty($manualMethod))
            return array(
                'status' => false,
                'message' => 'No enrol plugin found.',
            );

        $manualMethod = (object)$manualMethod;

        if ( $manualMethod->enrolperiod != 0)
            $timeend   = $timestart + $manualMethod->enrolperiod;

        $userEnrolment = self::getTable('userenrolments');
        $userEnrolment->load(['enrolid' => $manualMethod->id, 'userid' => $user->id]);

        $cuser = self::getTable('user');
        $cuser->load(['username' => JFactory::getUser()->username]);

        if ($userEnrolment->id) {
            if ($userEnrolment->timestart != $timestart or $userEnrolment->timeend != $timeend) {
                $userEnrolment->timestart    = $timestart;
                $userEnrolment->timeend      = $timeend;
                $userEnrolment->modifierid   = $cuser->id;
                $userEnrolment->timemodified = time();

                $userEnrolment->store();
            }
        } else {
            $userEnrolment->enrolid      = $manualMethod->id;
            $userEnrolment->status       = 0;
            $userEnrolment->userid       = $user->id;
            $userEnrolment->timestart    = $timestart;
            $userEnrolment->timeend      = $timeend;
            $userEnrolment->modifierid   = $cuser->id;
            $userEnrolment->timecreated  = time();
            $userEnrolment->timemodified = $userEnrolment->timecreated;
            $userEnrolment->store();
        }

        if ($roleid) {
            $context = self::getTable('context');
            $context->load(['instanceid' => $course->id, 'contextlevel' => COURSE_CONTEXT_LEVEL]);
            self::role_assign($roleid, $user->id, $context->id, 'enrol_manual', $manualMethod->id);
        }

        if ($roleid == 4 || $roleid == 3) {     // send email to facilitator or content creator
            $rolename = '';
            if ($roleid == 3) { $rolename = 'Content Creator'; }
            if ($roleid == 4) { $rolename = 'Facilitator'; }

//            $this->call_method('courseNotifications', $course_id, $course->shortname, $cur_username, $username, $rolename);
        }
        if ($roleid == 5) {     // send email to course creator or LP when user is subscribed to the course.
            // check course
//            $checkcourse = $this->check_course_type($course_id);
//            if ($checkcourse) {
//                $userto = $user = $DB->get_record('user', array('id' => $checkcourse['managerid']));
//                $this->call_method('courseSubscribeNotif', $course_id, $course->shortname, $username, $userto->username, $checkcourse['role']);
//            }
        }

        return array(
            'status' => true,
            'message' => 'Enroled success.',
        );
    }

    public static function updateLastAccessCourse($conditions) {
        $courseid = $conditions['id'];
        $username = $conditions['username'] ? $conditions['username'] : JFactory::getUser()->username;
        $username = utf8_decode ($username);
        $username = strtolower ($username);

        $user = self::getTable('user');
        $user->load(['username' => $username]);

        $enrol = self::getTable('enrol');
        $enrolMethods = $enrol->loadAssocList(['courseid' => $courseid, 'status' => 0]);

        if (!empty($enrolMethods)) {
            foreach ($enrolMethods as $val) {
                $ue = self::getTable('userenrolments');
                $ue->load(['enrolid' => $val['id'], 'userid' => $user->id]);

                if ($ue->id) {
                    $ue->lastaccess = time();
                    $ue->store();
                }
            }
        }
    }

    public static function get_display($modname, $instance) {
        $db = self::getMoodleDbo();
        switch ( $modname )
        {
            case 'resource':
                // Get display options for resource
                $record = $db->get_record('resource', ['id'=>$instance]);
                $display = $record['display'];
                break;
            case 'url':
                // Get display options for url
                $record = $db->get_record('url', ['id'=>$instance]);
                $display = $record['display'];

                break;
            default:
                $display = 0;
                break;
        }

        return $display;
    }

    public static function load_questions_bank($courseid, $aid = null, $detail = 0) {
        self::loadTables();
        $db = self::getMoodleDbo();

        $page = 0;
        $perpage = 20;
        $course = self::getCourseInfo(['id' => $courseid]);

        $catid = $course['cat_id'];

//        $contexts = array();
//        $contexts[] = context_system::instance();
//        $contexts[] = context_coursecat::instance($catid);
//        $contexts[] = context_course::instance($courseid);


        $context = self::getTable('context');
        $context->load(['instanceid' => $courseid, 'contextlevel' => COURSE_CONTEXT_LEVEL]);


//        require_once($CFG->libdir . '/questionlib.php');

//        $pcontexts = array();
//        foreach ($contexts as $context) {
//            $pcontexts[] = $context->id;
//        }
//        $contextslist = join($pcontexts, ', ');

//        $questionsCategories = get_categories_for_contexts($context->id);
        $questionsCategories = $db->get_records_sql(
            "SELECT c.*, (SELECT count(1) FROM #__question q
                WHERE c.id = q.category AND q.hidden='0' AND q.parent='0') AS questioncount
                FROM #__question_categories c
                WHERE c.contextid IN ($context->id)
                ORDER BY parent, sortorder, name ASC"
        );

        $categoriesArr = [];

        if (empty($questionsCategories)) {
            $category = new stdClass();
            $category->contextid = $context->id;
            $category->name = 'Default for '.$course['fullname'];
            $category->info = 'The default category for questions shared in context '.$course['fullname'];
            $category->parent = 0;
            $category->sortorder = 999;
            $category->stamp = self::make_unique_id_code();
            $questionCat = $db->insert_record('question_categories', $category);
            $categoriesArr[] = $questionCat;
        } else {
            foreach ($questionsCategories as $k => $v) {
                $categoriesArr[] = $v['id'];
            }
        }

        $where = empty($categoriesArr) ? 'parent = 0 AND hidden = 0' : 'parent = 0 AND hidden = 0 AND category IN ('.implode($categoriesArr, ',').') ';
        $sort = 'timecreated DESC';
        $q = $db->get_records_sql(
            "SELECT id, category, name, questiontext, qtype, generalfeedback
            FROM #__question
            WHERE $where
            ORDER BY $sort
            "
        );
//        $q = $db->get_records_select('question', $select, array(), $sort, '', $page, $perpage );

        $result = [];
        $questions = [];

        if ($aid) {
            $mod = $db->get_record_sql(
                "SELECT cm.instance, m.name as modname
                    FROM {modules} m, {course_modules} cm
                    WHERE cm.course = $courseid AND cm.module = m.id AND m.visible = 1 AND cm.id = $aid"
            );
//            $listq = $db->get_record_select('quiz', 'id = '.$mod->instance, null, 'questions');
            $listq = $db->get_record('quiz', ['id' => $mod['instance']], 'questions');

            $questions = explode(',/', $listq['questions']);
        }

        foreach ($q as $k => $v) {
            if (in_array($v['id'], $questions)) {
                $q[$k]['selected'] = 1;
            } else $q[$k]['selected'] = 0;

            if ($detail) {
                $question = $db->get_record('question', array('id' => $v['id']));

                switch ($question['qtype']) {
                    case 'truefalse':
                        $question['options'] = $db->get_record_sql("SELECT * FROM #__question_truefalse WHERE question = " . $question['id']);
                        $question['options']['answers'] = $db->get_records_sql(
                            "SELECT * FROM #__question_answers WHERE question = " . $question['id']
                            . " ORDER BY id ASC");
                        break;
                    case 'multichoice':
                        $question['options'] = $db->get_record_sql("SELECT * FROM #__question_multichoice WHERE question = " . $question['id']);

                        $question['options']['answers'] = $db->get_records_sql(
                            "SELECT * FROM #__question_answers WHERE question = " . $question['id']
                            . " ORDER BY id ASC"
                        );
                        $question['hints'] = $db->get_records_sql(
                            "SELECT * FROM #__question_hints WHERE questionid = " . $question['id']
                            . " ORDER BY id ASC"
                        );
                        break;
                    case 'match':
                        $question['options'] = $db->get_record_sql(
                            "SELECT * FROM #__qtype_match_options WHERE questionid = " . $question['id']
                        );
                        $question['options']['subquestions'] = $db->get_records_sql(
                            "SELECT * FROM #__qtype_match_subquestions WHERE questionid = "
                            . $question['id'] . " ORDER BY id ASC "
                        );
                        $question['options']['answers'] = $db->get_records_sql(
                            "SELECT * FROM #__question_answers WHERE question = " . $question['id']
                            . " ORDER BY id ASC"
                        );
                        $question['hints'] = $db->get_records_sql(
                            "SELECT * FROM #__question_hints WHERE questionid = " . $question['id']
                            . " ORDER BY id ASC"
                        );
                        break;
                    case 'shortanswer':
                        // this question no need options
                        $question['options'] = [];
                        // Minh update option ansers to fix edit shortanser
                        $question['options']['answers'] = $db->get_records_sql(
                            "SELECT * FROM #__question_answers WHERE question = " . $question['id']
                            . " ORDER BY id ASC");
                        break;
                }

                //$question = self::get_question_options($question, true);

                $q[$k]['options'] = json_encode($question['options']);
            }
        }
        if ($q && !empty($q)) {
            $result['status'] = 1;
            $result['message'] = 'Success';
            $result['questions'] = $q;
        } else {
            $result['status'] = 0;
            $result['message'] = 'Not found';
            $result['questions'] = array();
        }

        return $result;
    }

    public static function make_unique_id_code($extra='') {

        $hostname = 'unknownhost';
        if (!empty($_SERVER['HTTP_HOST'])) {
            $hostname = $_SERVER['HTTP_HOST'];
        } else if (!empty($_ENV['HTTP_HOST'])) {
            $hostname = $_ENV['HTTP_HOST'];
        } else if (!empty($_SERVER['SERVER_NAME'])) {
            $hostname = $_SERVER['SERVER_NAME'];
        } else if (!empty($_ENV['SERVER_NAME'])) {
            $hostname = $_ENV['SERVER_NAME'];
        }

        $date = gmdate("ymdHis");

        $random =  self::random_string(6);

        if ($extra) {
            return $hostname .'+'. $date .'+'. $random .'+'. $extra;
        } else {
            return $hostname .'+'. $date .'+'. $random;
        }
    }

    public static function random_string ($length=15) {
        $pool  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pool .= 'abcdefghijklmnopqrstuvwxyz';
        $pool .= '0123456789';
        $poollen = strlen($pool);
        mt_srand ((double) microtime() * 1000000);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($pool, (mt_rand()%($poollen)), 1);
        }
        return $string;
    }

    public static function teacherGetCourseGrades ($id, $search, $limitfrom = '', $limitnum = '', $completed = '') {
        // based on teacher_get_course_grades function
        self::loadTables();
        $db = self::getMoodleDbo();

        if ($search)
            $students = self::getCourseStudents($id, '', $search);
        else if ($completed)
            $students = self::getCourseStudents($id, $completed, '', $limitfrom, $limitnum);
        else
            $students = self::getCourseStudents($id, '', '', $limitfrom, $limitnum);

        $course_grades = [];
        $letters = self::grade_get_letters();

        foreach ($students as $student) {
            $grades = self::getCourseGrades($id, $student['username']);
            if ($grades[0]['usermodified'] != null) {
                $grades[0]['gradeoffacitator'] = true;
            } else {
                $grades[0]['gradeoffacitator'] = false;
            }
            $student['grades'] = $grades;
//            $gradereport = $this->get_grade_user_report($id, $student->username);
            $maxFinalGrade = array();

            if (!empty($grades )) {
                foreach ($grades as $v) {
                    if (isset($v['items']) && !empty($v['items'])) {
                        foreach ($v['items'] as $val) {
                            if ($val['grademax'] == 100) $maxFinalGrade[] = $val['finalgrade'];
                            else if ($val['grademax'] == 10) $maxFinalGrade[] = $val['finalgrade']*10;
                        }
                    }
                }
            }

//            if (!empty($gradereport['data'] )) {
//                foreach ($gradereport['data'] as $v) {
//                    if (isset($v['items']) && !empty($v['items'])) {
//                        foreach ($v['items'] as $val) {
//                            if ($val['grademax'] == 100) $maxFinalGrade[] = $val['finalgrade'];
//                            else if ($val['grademax'] == 10) $maxFinalGrade[] = $val['finalgrade']*10;
//                        }
//                    }
//                }
//            }
            if (empty($maxFinalGrade)) $maxFinalGrade[] = 0;

            $student['overallgrade'] = array_sum($maxFinalGrade)/count($maxFinalGrade);
            $l = array();
            foreach ($letters as $k => $v) {
                if (array_sum($maxFinalGrade)/count($maxFinalGrade) < $k) continue;
                $l[] = $k;
            }
            if (empty($l)) $l[] = 0;
            $student['overallgradeletter'] = $letters[max($l)];

            /* no where use this
            $user_groups = groups_get_user_groups ($id, $student['id']);
            if (!count ($user_groups[0]))
                $student['group'] = '';
            else {
                $group_id = $user_groups[0][0];
                $student['group'] = groups_get_group_name ($group_id);
            }
            */

            $course_grades[] = $student;
        }

        return $course_grades;
    }

    /*
    * Delete user function
    * params: $username
    * return: boolean
    *
    */
    public static function deleteUser($username)
    {
        self::loadTables();
        $db = self::getMoodleDbo();

        // Convert username
        $username = utf8_decode ($username);
        $username = strtolower ($username);

        // get user
        $conditions = ['username' => $username];
        $user = $db->get_record('user', $conditions);

        if ($user) {
            $username = $user['username'];

            // There must be always exactly one guest record,
            // originally the guest account was identified by username only,
            // now we use $CFG->siteguest for performance reasons.
            if ($user->username === 'guest') {
                debugging('Guest user account can not be deleted.');
                return false;
            }

            // Admin can be theoretically from different auth plugin,
            // but we want to prevent deletion of internal accoutns only,
            // if anything goes wrong ppl may force somebody to be admin via
            // config.php setting $CFG->siteadmins.
            if ($user->auth === 'manual') {
                debugging('Local administrator accounts can not be deleted.');
                return false;
            }
            try {
                // Delete grade by user
                $db->delete_record_db('grade_grades', 'userid', $user['id']);

                // unconditionally unenrol from all courses
                $db->delete_record_db('user_enrolments', 'userid', $user['id']);

                // Delete role assigments by user
                $db->delete_record_db('role_assignments', 'userid', $user['id']);

                // remove from all cohorts
                $db->delete_record_db('cohort_members', 'userid', $user['id']);

                // remove from all groups
                $db->delete_record_db('groups_members', 'userid', $user['id']);

                // brute force unenrol from all courses
                $db->delete_record_db('user_enrolments', 'userid', $user['id']);

                // purge user preferences
                $db->delete_record_db('user_preferences', 'userid', $user['id']);

                // purge user extra profile info
                $db->delete_record_db('user_info_data', 'userid', $user['id']);

                // last course access not necessary either
                $db->delete_record_db('user_lastaccess', 'userid', $user['id']);

                // remove all user tokens
                $db->delete_record_db('external_tokens', 'userid', $user['id']);

                // unauthorise the user for all services
                $db->delete_record_db('external_services_users', 'userid', $user['id']);

                // Remove users private keys.
                $db->delete_record_db('user_private_key', 'userid', $user['id']);

                // Delete session
                $db->delete_record_db('sessions', 'userid', $user['id']);

                // Delete user
                $db->delete_record_db('user', 'username', $username);

                // Delete category moodle user
                $usercategory = $db->get_record("course_categories", array('userowner'=>$user['id'], 'depth'=>2));           
                if (count($usercategory) > 0) {
                    $coursesids = $db->get_fieldset_select('course', 'id', 'category = :category ORDER BY sortorder ASC', array('category' => $usercategory->id));
                    $newparentid = $usercategory->parent;
                    if ($coursesids) {
                        if (!move_courses($coursesids, $newparentid)) {
                            return false;
                        }
                    }

                    $query = "DELETE FROM ".$db->quoteName('#__course_categories'). ' WHERE ' .$db->quoteName('userowner'). ' = '.$db->quote($user['id']). ' AND '.$db->quoteName('depth'). ' = '. $db->quote(2);

                    $db->setQuery($query);
                    $db->query();
                    // $db->delete_records_db("course_categories",array('userowner'=>$user->id, 'depth'=>2));
                }

                return true;
            } catch(Exception $ex) {
                echo $ex;
            } 
            
        }

        return false;
    }

    /**
    * check course creator exist
    *
    * @param $courseid
    * @return boolean 
    **/ 
    public static function checkCreatorCourse($courseid)
    {
        self::loadTables();
        $db = self::getMoodleDbo();

        if ($course = $db->get_record('course', ['id' => $courseid])) {            
            if ($creator = $db->get_record('user', ['id' => $course['creator']])) {
                return true;
            }
        }

        return false;
    }
}
