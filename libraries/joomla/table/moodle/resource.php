<?php
/**
 * Created by PhpStorm.
 * User: kydonvn
 * Date: 16/05/2018
 * Time: 10:55
 */

defined('_JEXEC') or die();
require_once(dirname(__FILE__).'/mtable.php');

class TableResource extends MTable
{
    public function __construct($db)
    {
        parent::__construct( '#__resource', 'id', $db );
    }
}