<?php
defined('_JEXEC') or die();

class MTable extends JTable
{
    public function loadAssocList($conditions)
    {
        $db = $this->getDBO();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($this->getTableName());

        $conditionsArr = [];
        foreach ($conditions as $k => $v) {
            $conditionsArr[] = ' '. $k .' = ' . $db->quote($v) . ' ';
        }
        if (!empty($conditionsArr)) $query->where(implode(' AND ', $conditionsArr));

        $db->setQuery($query);
        $list = $db->loadAssocList($this->_tbl_key);

        if ($db->getErrorNum())
        {
            $this->setError($db->getErrorMsg());
            return false;
        }

        // Check that we have a result.

        if (empty($list))
        {
            return false;
        }

        //Return the array
        return $list;
    }
}
?>