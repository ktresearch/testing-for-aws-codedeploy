<?php
/**
 * Created by PhpStorm.
 * User: kydonvn
 * Date: 16/05/2018
 * Time: 10:55
 */

defined('_JEXEC') or die();
require_once(dirname(__FILE__).'/mtable.php');

class TableCourseModulesCompletion extends MTable
{
    public function __construct($db)
    {
        parent::__construct( '#__course_modules_completion', 'id', $db );
    }
}