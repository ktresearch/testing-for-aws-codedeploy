<?php
/**
 * @package     Joomla.Legacy
 * @subpackage  Model
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('JPATH_PLATFORM') or die;


abstract class MModelItem extends JModelItem
{
    public function __construct() {
        parent::__construct();

        $this->setDbo(JFactory::getDbo(2));
    }
}
